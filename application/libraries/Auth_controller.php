<?php
class Auth_controller extends MY_Controller
{

	private $loginData;
	public function __construct()
	{
		parent::__construct();
		$this->loginData = $this->session->userdata('loginData');
		$this->is_allowed();
	}

	public function is_allowed()
	{
		
		if ($this->loginData == NULL) {
			redirect('login');
		}

		$controller = trim(strtolower($this->uri->segment(1)));
		$action = trim(strtolower($this->uri->segment(2)));

		if ($action == NULL) {
			$action = 'index';
		}

		// die(__FUNCTION__);

		if ($controller == 'ajax' || $controller == 'notification') {
			return true;
		}

		$this->db->where('lower(Controller)', $controller);
		$this->db->where('lower(Action)', $action);
		$this->db->where('Client_Id', $this->loginData['Client_Id']);
		$this->db->where('Role_Id', $this->loginData['Role_Id']);
		$result = $this->db->get('tblpermissions')->result();

		if (count($result) < 1) 
		{

			// send to home of user
			$this->db->where('Client_Id', $this->loginData['Client_Id']);
			$this->db->where('Role_Id', $this->loginData['Role_Id']);
			$this->db->where('Is_Home', 1);
			$result = $this->db->get('tblpermissions')->result();

			if (count($result) < 1) {
				$this->session->set_flashdata("er_msg", "Your do not have a default home for login. So you are being sent to logout screen");
				redirect("login");
			}

			$this->session->set_flashdata("er_msg", "The resource does not exist or you do not have permission to access");
			redirect($result[0]->Controller . '/' . $result[0]->Action);
		}
	}

}