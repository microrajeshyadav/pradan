<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaverequest" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">   
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Leave Applied </h4>

         <div class="col-md-2 text-right">
          <a href="<?php echo site_url()."Leaveapplication/index/" .$this->loginData->staffid;?>" class="btn btn-sm btn-primary" title="Add a new leave request? Click on me."  data-toggle="tooltip" data-placement="bottom">Leave Request</a>

        </div>
      </div>
      <hr class="colorgraph"><br>
    </div>

    
    <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tablecampus" class="table table-centered table-striped dt-responsive w-100 no-footer dtr-inline dataTable">
                  <thead>
                   <tr>
                     <th style ="max-width:30px;" class="text-center">S. No.</th>
                     <th class="text-center">Leave Type</th>
                     <th class="text-center">Applied On</th>
                     <th class="text-center">Maternity Due Date</th>
                     <th class="text-center">From Date</th>
                     <th class="text-center">To Date</th> 
                     <th class="text-center">Units</th>
                     <th class="text-center">Reason</th>
                     <th class="text-center">Staus</th>
                     <th style ="max-width:50px;" class="text-center">Reverse</th>
                   </tr> 
                 </thead>
                 <tbody>
                  <?php 
                  $i=0;
                  foreach ($leaverequest as $key => $value) {
                   ?>
                   <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td class="text-center"><?php echo $value->Ltypename;?></td>
                    <td class="text-center"><?php echo $value->appliedon;?></td>
                     <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->maturnityduedate);?> </td>
                    <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->From_date);?> </td>
                    <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->To_date);?> </td>
                    <td class="text-center"><?php echo $value->Noofdays;?> </td>
                    <td class="text-center"><?php echo $value->reason;?></td>
                    <td class="text-center">
                     
                       <?php if($value->stage == 1){      //// 1 submitted case                      
                       ?>
                          <span class="badge badge-pill badge-info col-md-12" style="padding-top: 6%;"> 
                       <?php } ?>
                       <?php if($value->stage == 2){        ////  2 reject case                    
                       ?>
                          <span class="badge badge-pill badge-danger col-md-12" style="padding-top: 6%;"> 
                       <?php } ?>
                       <?php if($value->stage == 3){     //// 3 approved case                       
                       ?>
                          <span class="badge badge-pill badge-success col-md-12" style="padding-top: 6%;"> 
                       <?php } ?>
                       <?php if($value->stage == 4){     //// 4 reverse case    
                       ?>
                          <span class="badge badge-pill badge-warning col-md-12" style="padding-top: 6%;"> 
                       <?php } ?>

                      <label class="badge">
                      <?php echo $value->status ;?> 
                      </label> </span>
                    </td>

                    <td class="text-center">
                      <?php if($value->stage == 1){   ?>
                        <input type="hidden" name="reverseid" id="reverseid"
                         value="<?php echo $value->id; ?>">
                       <a title = "Click here to reverse your leave request." href="" id="empreverse">
                        <i class="fa fa-backward" aria-hidden="true"></i>
                       </a>
                       <?php } ?>
                     </td>
                   </tr>
                   <?php $i++; } ?>
                 </tbody>

               </table>
             </div>
           </div> 
         </div>
       </div>
     </div>   
     <?php } } ?>
   </section>

   <script>

    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();  
      $('#tablecampus').DataTable(); 
    });


  $("#empreverse").on('click',function(){

    var reverseid = $('#reverseid').val();
  
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getreverse_leave_request/'+ reverseid ,
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      aler(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



    // function confirm_delete() {
    //     var r = confirm("Are you sure!! you want to reverse ? !!!");
    //     if (r == true) {
    //       return true;
    //     } else {
    //       return false;
    //     }
    //   }

  </script>  


     
 
 