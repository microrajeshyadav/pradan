<style type="text/css">
tr.group,
tr.group:hover {
  background-color: #ddd !important;
}

</style>

<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaveadjustment" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">   
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">Staff CTC Report</h4>


       </div>
       <hr class="colorgraph"><br>
     </div>


     <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <form name="frmgenratesalary" id="frmgenratesalary" action="" method="post">   
            <div class="row bg-light" style="padding: 20px;">

              <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                Office: 
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <select class="form-control" name="transfer_office" id="transfer_office" required="required">
                  <option value="0">All</option>

                  <?php foreach($all_office as $val){?>
                  <option value="<?php echo $val->officeid;?>" ><?php echo $val->officename;?> </option>
                  <?php  } ?>
                </select>
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                Month: 
              </div>
              <div id="salmonth" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <select name="salmonth" class="form-control">
                  <option value="01"> Jan</option>
                  <option value="02"> Feb</option>
                  <option value="03"> Mar</option>
                  <option value="04"> Apr</option>
                  <option value="05"> May</option>
                  <option value="06"> Jun</option>
                  <option value="07"> Jul</option>
                  <option value="08"> Aug</option>
                  <option value="09"> Sep</option>
                  <option value="10"> Oct</option>
                  <option value="11"> Nov</option>
                  <option value="12"> Dec</option>

                </select>
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                Year: 
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <select id="salyear" name="salyear" class="form-control">
                  <option value=""> Select</option>
                  <?php 
                  $currently_selected = date('Y'); 
                    // Year to start available options at
                  $earliest_year = 2018; 
                    // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
                  $latest_year = date('Y'); 
                  $latest_year = $latest_year + 1;
                  foreach ( range( $latest_year, $earliest_year ) as $i ) {
                          // Prints the option with the next year in range.
                    print '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                  }
                  ?>

                </select>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">

               <button type="Submit" class="btn btn-info btn-sm" name="submit"> <i class="fa fa-search" aria-hidden="true"></i></button>
             </div>

           </div> 
         </form>

         <?php //echo "<pre>";  print_r($staffsalary['result']); ?>

           <div class="row" style="background-color: #FFFFFF; margin-top:20px;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white; overflow: scroll">
              <table id="tablestaff" name ="tablestaff" class="table table-bordered table-striped dataTable no-footer">
                <thead>
                 <tr>
                  <th class="text-left">S.No</th>
                  <th class="text-left">Office</th>
                  <th class="text-left" >level</th>
                  <th class="text-left" >No. of year experince</th>
                  
                  <?php 
                  if (count($getactivehead) >0) {
                    foreach ($getactivehead as $key => $value) { ?> 
                    <th class="text-center"><?php echo $value->fielddesc; ?></th> 
                    <?php } } ?>              
                    
                  </tr> 
                </thead> 



                <tbody>


                  <?php $i=0; 
                  if ($staffsalary['result'] >0) {

                    foreach ($staffsalary['result'] as $key => $value) {

                      ?>
                      <tr>
                       <td><?php echo $i+1; ?></td>
                       <?php $j=0;  foreach ($value as $row) { 

                        if ($j > 3) { // for all amt decimal value ?>
                        <td class="text-right"><label id="otheramt_<?php echo $j; ?>"></label></td>
                        <script>
                        var cellid= "<?php echo $j; ?>"; var cid="#otheramt_"+cellid;
                        $(cid).text(intToNumberBudget("<?php echo $row; ?>"));
                      </script>

                        <?php  }else{ 
                        if($j == 3){ //for basic salary decimal value?> 
                          <td class="text-left"><label id="otheramt_<?php echo $j; ?>"></label></td>
                          <script>
                          var cellid= "<?php echo $j; ?>"; var cid="#otheramt_"+cellid;
                          $(cid).text(intToNumberBudget("<?php echo $row; ?>"));
                          </script>
                        <?php }else{ //static <th> ?>
                          <td class="text-left"> <?php echo $row; ?></td>
                        <?php } ?> 

                        <?php } $j++;}  ?>

                      </tr>
                      <?php $i++; } } ?>


                    </tbody>

                  </table>
                </div>
              </div> 
            </div>
          </div>
        </div>   
        <?php } } ?>
      </section>
      <script>
        $(document).ready(function(){
           $('#tablestaff').DataTable();
          $('[data-toggle="tooltip"]').tooltip();      
          

        });
      </script>  