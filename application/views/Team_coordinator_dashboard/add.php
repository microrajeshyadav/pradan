<section class="content">
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Tc_daship_component_comments" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
       
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                 <h4 class="header" class="field-wrapper required-field" style="color:green">Add Comments</h4>
                 <div class="col-sm-12">
                   <form method="POST" action="">
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Document</label>
                       <a href="<?php echo site_url().'datafiles/dashipcomponent/'.$dacomponent_details[0]->encrypted_document_name;?>" 
                        target="_blank" data-toggle="tooltip" title="DAship Components Document">
                  
                  </a>
                      </div>
                     
                    </div>
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Phase</label>
                      <?php 
                        $options = array('' => 'Select Phase ');
                         $selected_phase = $dacomponent_details[0]->phaseid; 
                        foreach($phase_details as$key => $value) {
                          $options[$value->id] = $value->phase_name;
                        }
                        echo form_dropdown('phaseid', $options, $selected_phase, 'class="form-control" id="phaseid" disable="disable"' );
                        ?>
                      </div>
                       <?php echo form_error("statecode");?>
                    </div>

                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Comments</label>
                        <textarea name="comments" id="comments" class="form-control" ><?php echo set_value('comments')?></textarea>
                      </div>
                        <?php echo form_error("comments");?>
                    </div>
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Status </label>
                      <?php   

                        //$options = array('' => 'Select Status');     
                        $options = array('2' => 'Accepted', '3' => 'Rejected');
                       echo form_dropdown('status', $options, set_value('stateid'), 'class="form-control"'); 
                      ?>
                      </div>
                        <?php echo form_error("status");?>
                    </div>
                      <div style="text-align: -webkit-center;"> 
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("state");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>