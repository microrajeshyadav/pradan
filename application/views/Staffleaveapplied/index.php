
<?php //echo $fromdate; die;?>
<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">   
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12  panel-title pull-left">Staff Leave Details  <?php if ( $this->loginData->RoleID == 20 || $this->loginData->RoleID == 2 ) { echo " / Monthly Closing";}?> </h4>

         
       </div>
       <hr class="colorgraph"><br>
     </div>


     <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
        <?php }  if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="row bg-light text-black" style="padding: 15px;">
            <form method="POST" action="">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px;">
                <h5>Search Filters:</h5>
              </div>

              <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">
                Status: 

                <select id="ddstatus" name="ddstatus" class="form-control"> 
                  <option value="1" > Active</option>
                  <option value="0" > Inactive</option>
                </select>
              </div>
              <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">
                Office: 



                <?php 
                if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20) { ?>

                 <select name="office" id="office" class="form-control">
                   <!--  <option value="0">Select Office</option> -->
                   <?php foreach($office as $row){
                    if($staffofficeid->new_office_id == $row->officeid) {
                      ?>
                      <option value="<?php echo $row->officeid; ?>" SELECTED> <?php echo $row->officename; ?> </option>
                    <?php } } ?>
                  </select>
                <?php }else{ ?>

                  <select name="office" id="office" class="form-control">

                    <?php foreach($office as $row){ ?>
                      <option value="<?php echo $row->officeid; ?>"> <?php echo $row->officename; ?> </option>
                    <?php } ?>
                  </select>

                <?php }?>
              </div>
              <?php 
              if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20) { ?> 

                <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
                  Select Month :
                  <select class="form-control" name="month" id="month" <?php echo set_value('month'); ?>>
                    <!-- <option value="">Select</option> -->
                    <option value="01" <?php echo set_select('month', '01', TRUE); ?>> Jan</option>
                    <option value="02" <?php echo set_select('month', '02', TRUE); ?>> Feb</option>
                    <option value="03" <?php echo set_select('month', '03', TRUE); ?>> Mar</option>
                    <option value="04" <?php echo set_select('month', '04', TRUE); ?>> Apr</option>
                    <option value="05" <?php echo set_select('month', '05', TRUE); ?>> May</option>
                    <option value="06" <?php echo set_select('month', '06', TRUE); ?>> Jun</option>
                    <option value="07" <?php echo set_select('month', '07', TRUE); ?>> Jul</option>
                    <option value="08" <?php echo set_select('month', '08', TRUE); ?>> Aug</option>
                    <option value="09" <?php echo set_select('month', '09', TRUE); ?>> Sep</option>
                    <option value="10" <?php echo set_select('month', '10', TRUE); ?>> Oct</option>
                    <option value="11" <?php echo set_select('month', '11', TRUE); ?>> Nov</option>
                    <option value="12" <?php echo set_select('month', '12', TRUE); ?>> Dec</option>
                  </select>
                </div>
                <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
                 Select Year :
                 <select class="form-control" name="year" id="year"  <?php echo set_value('year'); ?>>
                  <!-- <option value="">Select</option> -->
                  <?php
                  for ($i = date('Y')-1; $i <= date('Y'); $i++) {

                    if(!empty($submit)){
                      if($year == $i){
                        echo "<option value=$i>".$i ."</option>";
                      } } else{ 
                        echo "<option value=$i>".$i ."</option>";
                      } }?>
                    </select>
                  </div>

                <?php } ?>
                <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  From: 

                  <?php 
                  if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20) {                 

                    if(!empty($submit)){ 


                      ?>

                      <input type="text" readonly  name="fromdate" id="fromdate" class="datepicker form-control" value="<?php echo set_value('fromdate'); ?>" >
                    <?php }else{ ?>
                      <input type="text"  readonly autocomplete="off" name="fromdate" id="fromdate" class="datepicker form-control" value="<?php echo date('d/m/Y', strtotime('first day of previous month')); ?>" > <?php }
                    }else{ ?>
                      <input type="text" autocomplete="off" name="fromdate" id="fromdate" class="datepicker form-control" value="<?php echo set_value('fromdate'); ?>">
                    <?php } ?>
                  </div>


                  <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">

                   To:  
                   <?php 
                   if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20) { ?>    
                    <?php
                    if(!empty($submit)){ ?>

                      <input autocomplete="off" readonly type="text" name="todate" id="todate" value="<?php echo set_value('todate'); ?>" class="datepicker form-control">
                    <?php }else{ ?>
                      <input autocomplete="off" readonly type="text" name="todate" id="todate" value="<?php echo date('d/m/Y', strtotime('last day of previous month')); ?>" class="datepicker form-control"> <?php } 
                    }else{ ?>
                      <input autocomplete="off" type="text" name="todate" id="todate" value="<?php echo set_value('todate'); ?>" class="datepicker form-control">
                    <?php } ?>

                  </div>

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                    <button type="Submit" class="btn btn-info btn-sm pull-right" name="submit" value="search" id="search"> <i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>

                </form>
              </div>
              <hr/>
              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <form action="" method="post">
                    <input type="hidden" name="hdnOfficeID" id="hdnOfficeID">
                    <input type="hidden" name="From_date" id="From_date" value="<?php echo set_value('From_date');?>">
                    <input type="hidden" name="to_date" id="to_date" value="">
                    <table id="tblstaffleave" class="table table-bordered table-striped">
                      <thead>
                       <tr>
                         <th style ="max-width:30px;" class="text-center">S. No.</th>
                         <th class="text-left">Staff</th>
                         <th class="text-left">Designation</th>
                         <th class="text-left">Leave Type</th>
                         <th class="text-center">Applied On</th>
                         <th class="text-center">From Date</th>
                         <th class="text-center">To Date</th> 
                         <th class="text-center">Units</th>
                         <th class="text-left">Reason</th>
                         <th class="text-center">Request Staus</th> 


                         <?php 
                         if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20) { ?>
                           <th class="text-center">Closing Status</th> 
                           <th class="text-center">
                             <input type="checkbox" id="checkAll">Leave Closing
                           </th> 
                         <?php } ?>                  
                       </tr> 
                     </thead>


                     <tbody>
                      <?php 
            //  echo "<pre>";
             // print_r($staffleavedetails);
              //die();
                      if (count($staffleavedetails) !=0) {

                        $i=0;
                        foreach ($staffleavedetails as $key => $value) {

                          // echo $value->filename; 

                         ?>
                         <tr>
                          <td class="text-center"><?php echo $i+1;?></td>
                          <td class="text-left"><?php echo $value->employee;?></td>
                          <td class="text-left"><?php echo $value->desname;?></td>
                          <td class="text-left"><?php echo $value->Ltypename;?></td>
                          <td class="text-center"><?php echo $value->appliedon;?></td>
                          <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->From_date);?> </td>
                          <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->To_date);?> </td>
                          <td class="text-center"><?php echo $value->Noofdays;?> </td>
                          <td class="text-left"><?php echo $value->reason;?></td>
                          <td class="text-center">
                            <?php if($value->stage == 3){                          
                              echo '' ;?>
                              <span class="badge badge-pill badge-success" style="padding-top: 3%;"> 
                              <?php } ?>
                              <?php if($value->stage == 1){                          
                               ;?>
                               <span class="badge badge-pill badge-info" style="padding-top: 3%;"> 
                               <?php } ?>
                               <?php if($value->stage == 2){                          
                                 ;?>
                                 <span class="badge badge-pill badge-danger" style="padding-top: 3%;"> 
                                 <?php } ?>
                                 <?php if($value->stage == 4){                          
                                   ;?>
                                   <span class="badge badge-pill badge-warning" style="padding-top: 3%;"> 
                                   <?php } ?>

                                   <label class="badge">
                                    <?php echo $value->status ;?> 
                                  </label>  </span>  
                                  <?php if(!empty($value->stage) && $this->loginData->RoleID!=2 && $this->loginData->RoleID!=20){ ?>
                                    | 
                                    <?php if(!empty($value->filename))
                                    { ?>
                                      <a target="_blank" href="<?php echo base_url().'pdf_offerletters/'.$value->filename.'.pdf'; ?>" class="btn-primary">
                                        <i class="fa fa-file-pdf fa-2x" aria-hidden="true"></i>
                                    <?php } else{ ?>
                                    <a title = "Click here to approval this request submitted." class="btn btn-primary btn-xs" onclick="sanctionltr(<?php echo $value->emp_code ?>,<?php echo $value->mlcid;?>);">Generate sanction letter</a> 
                                    <?php }
                                   } ?>

                                </td>


                                <?php 
                                if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20) { ?>

                                  <td class="text-center">
                                   <?php if($value->mlcstage == 1){ ?>

                                     <span class="badge badge-pill badge-info" style="padding-top: 3%;"> Close By Team Accountant </span>

                                   <?php } elseif($value->mlcstage == 2){ ?>

                                     <span class="badge badge-pill badge-info" style="padding-top: 3%;"> Close By Team Co-ordinator </span>
                                   <?php } else{ ?>

                                    <span class="badge badge-pill badge-danger" style="padding-top: 3%;"> Not Close </span>
                                  <?php } ?>
                                </td>

                                

                                <?php } 
                                 if ($this->loginData->RoleID==20 || $this->loginData->RoleID==2 ){ 
                                ?>
                                
                                  <td class="text-center">
                                    <?php  

                                   
                                      if(($value->mlcstage == 2  || $value->mlcstage == 1 )&& ($this->loginData->RoleID==20))
                                      {

                                      ?>

                                      
                                  <input type="checkbox"  name="cbleaveclosing_<?php echo $i.'_'.$value->emp_code;?>" required <?php if($value->mlcstage == 1|| $value->mlcstage == 2 ){ echo "checked "; } ?>  readonly > 
                                <?php }

                           else if(($value->mlcstage == 2  || $value->mlcstage == 1 )&& ($this->loginData->RoleID==2))
                                {
                                  ?>
                                   <input type="checkbox"  name="cbleaveclosing_<?php echo $i.'_'.$value->emp_code;?>" required <?php if($value->mlcstage == 1|| $value->mlcstage == 2 ){ echo "checked "; } ?>  readonly > 
                                  <?php 
                                }
                                else if( ($this->loginData->RoleID==20 || $this->loginData->RoleID==2))
                                {
                                  ?>
                                   <input type="checkbox"  name="cbleaveclosing_<?php echo $i.'_'.$value->emp_code;?>" required  > 
                                  <?php 
                                }

                            


                                 
                                 
                                 ?>
                                </td>
                              <?php } ?>
                              </tr>
                              <?php $i++; }} ?>
                            </tbody>

                          </table>
                          <div class="col-md-12 text-right">
                            <?php 

                              if(($this->loginData->RoleID == 2)|| ($this->loginData->RoleID == 20))
                              {
                              if((!empty($value->mlcstage)) &&($value->mlcstage == 1) && ($this->loginData->RoleID == 2) ) {  ?>

                                <button type="submit" class="btn btn-primary pull-right" name="approve_leave" id="approve_leave" value="leaveclosing">Submit </button>

                              <?php } 
                            
                              else  if((empty($value->mlcstage)) && ($this->loginData->RoleID == 20 || $this->loginData->RoleID == 2) ){
                                ?>
                                  <button type="submit" class="btn btn-primary pull-right" name="approve_leave" id="approve_leave" value="leaveclosing">Submit </button>
                                <?php

                              }


                               } ?>
                            </div>
                          </form>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>  

                        <!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="<?php echo site_url(); ?>Staffleaveapplied/sendsanctionletter" id="sanction" name="sanction" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Sanction Letter Details</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">           


              <input type="hidden" id="empcode" name="empcode" value="">
              <input type="hidden" id="mlcid" name="mlcid" value="">
              <div class="row" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <label for="Name">Description<span style="color: red;" >*</span></label>
              <textarea class="form-control" data-toggle="" id="descript" name="descript" placeholder="Enter Notes"> I am happy to approve your request for leave as leave with pay with effect from </textarea>
              <?php echo form_error("descript");?>
              
            </div>
          

          <div class="row" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <label for="Name">Fromdate<span style="color: red;" >*</span></label>
              <input type="text" placeholder="Enter Fromdate" autocomplete="off" name="fmdate" id="fmdate" class="datepicker form-control">
              <?php echo form_error("fmdate");?>
              

          </div>


          <div class="row" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <label for="Name">Todate<span style="color: red;" >*</span></label>
              <input autocomplete="off" placeholder="Enter Todate" type="text" name="tdate" id="tdate" value="<?php echo $todate; ?>" class="datepicker form-control">
              <?php echo form_error("Tdate");?>
              

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" onclick="sanctionltrmodel();" value="Save" class="btn btn-success btn-sm">
          </div>
          
          
        </div>
      </form> 
    </div>
  </div>
</div> 
<!-- Modal --> 

              </section>

  <script type="text/javascript">
    $("#approve_leave").click(function () {
     // alert("hello");
     var fromdate= $("#fromdate").val();
     $("#From_date").val(fromdate);
     var todate= $("#todate").val();
     $("#to_date").val(todate);


    //alert(fromdate+todate);
    // $('input:checkbox').not(this).prop('checked', this.checked);
  });

    $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
   });


    $("#month").on('change', function() {
      $("#year").val("");
    });

    $("#year").on('change', function() {
     var mon = $("#month").val();
     //alert(mon);
     var year=$("#year").val();
     //alert(year);
     if(mon !='' && $("#year").val() !='')
     {
      //alert($("#year").val());
      //alert($("#month").val());
      var firstDay =  new Date( $("#year").val(),  parseInt($("#month").val())-1, 1);
     // alert("firstDay="+firstDay);
     var curr_day = firstDay.getDate();
     var curr_month = firstDay.getMonth()+1;
     var curr_year = firstDay.getFullYear();
     firstDay = curr_day +'/' + curr_month + '/' +  curr_year;   
     $("#fromdate").val(firstDay);     
     var lday = lastday(curr_year,mon);      
     var  latsdate = lday +'/' + curr_month + '/' +  curr_year;
     $("#todate").val(latsdate);

   }
   else
   {
    alert('Please select month first!');
  }


});

    var lastday = function(y,m){
      return  new Date(y, m , 0).getDate();
    }
    $("#month").click(function () {

    });
    $(document).ready(function(){

      $('[data-toggle="tooltip"]').tooltip();
      $(".datepicker").datepicker({
       changeMonth: true,
       changeYear: true,
      // maxDate: 'today',
       yearRange: '2000:2030',
       dateFormat : 'dd/mm/yy',
     });  

    });

    $(document).ready(function() {

      $('#hdnOfficeID').val($('#office').val());

     $('#tblstaffleave').DataTable(

      {
       "lengthMenu": [ [500, 1000, 2000, -1], [500, 1000, 2000, "All"] ]
     }

     );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
        table.order( [ groupColumn, 'desc' ] ).draw();
      }
      else {
        table.order( [ groupColumn, 'asc' ] ).draw();
      }
    } );
  } );

    function sanctionltr(emp_code,mlcid){
    document.getElementById("empcode").value = emp_code;
    document.getElementById("mlcid").value = mlcid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }

  function sanctionltrmodel(){
    var descript = document.getElementById('descript').value;
    var fmdate = document.getElementById('fmdate').value;
    var tdate = document.getElementById('tdate').value;
    if(descript == ''){
      $('#descript').focus();
    }
    if(fmdate == ''){
      $('#fmdate').focus();
    }
    if(tdate == ''){
      $('#tdate').focus();
    }
    if(descript !='' && fmdate !=''  && tdate !=''){
      document.forms['sanction'].submit();
    }
  }


    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }


  }


</script>
<script type="text/javascript">
  $(document).ready(function(){

    $("#office").change(function(){
      var office_id = $(this).val();
      var data;
      $.ajax({
        data:{office_id:office_id},

        url: '<?php echo site_url(); ?>Ajax/staff_list/',
        type: 'POST'

      })
      .done(function(data) {

        var obj=JSON.parse(data);

        $.each(obj, function(index) {

          $('#staff').append($('<option></option>').text(obj[index].name).val(obj[index].staff_id));
          var option='';

        });

      })

    });
  });

</script>