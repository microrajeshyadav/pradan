<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave_category" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
            <form id="myform" method="POST" action="">
             <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-7 panel-title pull-left">Add Leave Category</h4>
                 <div class="col-md-5 text-right" style="color: red">
                  * Denotes Required Field 
                </div>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="form-line">
                  <label for="StateNameEnglish" class="field-wrapper required-field">Leave Category<span style="color: red;" >*</span></label>
                  <input type="text" class="form-control alphabateonly" name="leavecategory" minlength="3" maxlength="30" id="Name" placeholder="Please Enter Leave Category" required="">
                </div>
                <?php echo form_error("leavecategory");?>
              </div>

              <div class="form-group">                   
                <div class="form-check">
                  <input class="form-check-input fillreject" type="checkbox" name="Deduct"  id="Deduct">
                  <label class="form-check-label" for="Deduct">
                  Deduct from Salary</label>
                </div>
                <?php echo form_error("Deduct");?>
              </div>

              <div class="form-group">                   
                <div class="form-check">
                  <input class="form-check-input fillreject" type="checkbox" name="carryforward"  id="Deduct">
                  <label class="form-check-label" for="carryforward">
                  Carry Forwad</label>
                </div>
                <?php echo form_error("carryforward");?>
              </div>
              <div class="form-group">                   
                <div class="form-line">
                 <label class="form-label" for="maxcarryforwardunit">
                 Carry Forwad</label>
                 <input class="form-control" type="number" name="maxcarryforwardunit"  id="maxcarryforwardunit" min="0" max="50" />

               </div>
               <?php echo form_error("carryforward");?>
             </div>
               <div class="form-group">                   
                <div class="form-line">
                 <label class="form-label" for="maxleaveinayear">
                 Max. leave: <span style="color: red;" >*</span></label>
                 <input class="form-control" type="number" name="maxleaveinayear"  id="maxleaveinayear" min="0" max="900" required="" />

               </div>
               <?php echo form_error("maxleaveinayear");?>
             </div>
             <div class="form-group">                   
                <div class="form-line">
                 <label class="form-label" for="minleaveinayear">
                 Min. leave: <span style="color: red;" >*</span></label>
                 <input class="form-control" type="number" name="minleaveinayear"  id="minleaveinayear" min="0" max="900" required="" />

               </div>
               <?php echo form_error("minleaveinayear");?>
             </div>
           

             <!-- </div> -->


             <div class="form-group">                   
                <div class="form-line">
                 <label class="form-label" for="minleaveinayear">
                 Long.leave Days: <span style="color: red;" >*</span></label>
                 <input class="form-control" type="number" name="longleavedays"  id="longleavedays" min="0" max="900" required="" />

               </div>
               <?php echo form_error("minleaveinayear");?>
             </div>


             <div class="form-group">
              <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">Remarks<span style="color: red;" >*</span></label>
                <input type="text" class="form-control" name="Remarks" minlength="3" maxlength="100" id="Name" placeholder="Please Enter Remarks" required="">
              </div>
              <?php echo form_error("Remarks");?>
            </div>


            <div class="form-group">
              <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">Status  <span style="color: red;" >*</span></label>
                <?php   

                        //$options = array('' => 'Select Status');     
                $options = array('0' => 'Active', '1' => 'InActive');
                echo form_dropdown('status', $options, set_value('stateid'), 'class="form-control"'); 
                ?>
              </div>
              <?php echo form_error("status");?>
            </div>


          </div>

          <div class="panel-footer text-right"> 
           <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" id="submit" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
           <a href="<?php echo site_url("Leave_category");?>" class="btn btn-dark btn-sm m-t-10 waves-effect"data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
         </div>

       </div>
     </form>
   </div>
 </div>
 <!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>
<script type="text/javascript">
  $(document).ready(function () {
     $(".alphabateonly").keypress(function (e){
      var code =e.keyCode || e.which;
      if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46 and code!=38 and code!=47)  
      {
       alert("Only alphabates are allowed");
       return false;
     }
   });
</script>

<script type="text/javascript">
  $('#myform').submit(function() {
    alert("hello");
    if (carryforwardunit() = false)
    {
      return false;

    }
  });


  function carryforwardunit ()
  {
    var carryforward = $('#carryforward').val();
    var maxcarryforwardunit = $('#maxcarryforwardunit').val();
    if (carryforward == 1 and maxcarryforwardunit <=0)
    {

      alert ('Please enter max unit carry forward.');

      $( "#maxcarryforwardunit" ).focus();
      return false;    
    }
    else
    {
      return true;

    } 
  }


</script>