<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave_category" && $row->Action == "index"){ ?>
 <br>
 <div class="container-fluid">
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>



      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Manage Leave Category</h4>
             <div class="col-md-2 text-right">


               <a data-toggle="tooltip" data-placement="bottom" title="Want to add new leave category? Click on me." href="<?php echo site_url("Leave_category/add/")?>" class="btn btn-primary btn-sm">Add New</a>

             </div>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">

          <table id="tblsyslanguage" class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th class="text-center align-top" style="width: 50px;">S.NO</th>
                <th class="align-top text-center">Leave Category</th>
                <th class="text-center align-top" >Deduct from Salary</th>
                <th class="text-center align-top" >Carry Forward</th>
                <th class="text-center align-top" >Max Carry Forward unit</th>
                <th class="text-center align-top" >Min Leave</th>
                 <th class="text-center align-top" >Max Leave</th>
                  <th class="text-center align-top" >Long Leave Days</th>
                <th class="text-center align-top">Remarks</th>
                <!-- <th class="text-center align-top" >Deleted</th> -->
                <th class="text-center align-top"  style="width: 50px;">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php

                   // print_r($staffcategory_details);
              $i=0; foreach($Leave_details as $row){ ?>
              <tr>
                <td class="text-center"><?php echo $i+1; ?></td>
                <td ><?php echo $row->Ltypename; ?></td>
                <td class="text-center"><?php if ($row->deduct==0){ echo 'No';} else {echo 'Yes';} ?></td>
                 <td class="text-center"><?php if ($row->carryforward==0){ echo 'No';} else {echo 'Yes';} ?></td>
                 <td class="text-center"><?php echo $row->maxcarryforwardunit; ?></td>
                 <td class="text-center"><?php echo $row->minleave; ?></td>
                  <td class="text-center"><?php echo $row->maxleaveinayear; ?></td>
                  <td class="text-center"><?php echo $row->longleavedays; ?></td>
                <td ><?php echo $row->remarks; ?></td>
                <!-- <td class="text-center"><?php  if ($row->isdeleted==0) { echo 'No';} else {echo 'Yes';}  ?></td> -->

               <td class="text-center">
                <a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this leave category." href="<?php echo site_url('Leave_category/edit/'.$row->Ltypeid);?>" style="padding : 4px;" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>
                |
                <a href="<?php echo site_url('Leave_category/delete/'.$row->Ltypeid);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this leave category."><i class="fa fa-trash" style="color:red"></i></a>

              </td>
            </tr>
            <?php $i++; } ?>
          </tbody>
        </table>


      </div>
    </div>
  </div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#tblsyslanguage').DataTable({
      "paging": false,
      "search": false,
    });
  });
  function confirm_delete() {

    var r = confirm("Do you want to delete this leave category?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>




