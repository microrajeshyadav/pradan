
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/jszip.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/pdfmake.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.html5.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/vfs_fonts.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.print.min.js');?>"></script>

<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Selectedcandidates" && $row->Action == "index"){ ?>
    <br>
    <div class="container-fluid">
     <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Shortlisted Candidates List</h4>
        
      </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>

            <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">
             <div class="row" style="background-color: #F8F9FA; padding:  10px;">
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">Campus Name</div>
              <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
               <select name="campusid" id="campusid" class="form-control", required="required">
                <option value="">Select Campus</option>
                <?php foreach ($campusdetails as $key => $value) {
                  $expdatefrom = explode('-', $value->fromdate);
                  $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                  $expdateto = explode('-', $value->todate);
                  $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                  $campval  = $value->campusid.'-'. $value->id; 
                  if ($campval == $campusid) {
                   ?>
                   <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                 <?php  }else{ ?>
                  <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                <?php } } ?>
              </select> 
              <?php echo form_error("campusname");?>
            </div>

            <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right" > <button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-dark btn-sm" data-toggle="tooltip" title="Want to search your changes? Click on me.">Search</button></div>
             <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12" >
               <?php  

                      $expdata = '';
                    @  $expdata = explode('-', $campusid);
                      if ($expdata[0] !=0) {
                       
                    ?>
              <button type="submit" name="send_campus"  id="send_campus" class="btn btn-info btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Import & Send" value="sendcamp" >Send mail to Campus-Incharge</button>
              <?php }  ?> </div>
            </div>
          </form>
          <br>
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left " style="background-color: white;">
              <table id="tableSelectcandidateScore" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
                <thead>
                 <tr>
                  <th class="text-center" style="width: 50px;">S.No.</th>
                  <th>Category</th>
                  <th>Campus Name</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Email Id</th>
                  <th>Stream</th>
                  <th>BDF Status</th>
                </tr> 
              </thead>
              <tbody>
                <?php 
                if (count($selectedcandidatedetails)==0) { ?>
                 <tr>
                   <td colspan="8" class="text-center" style="color: red;"> No Records Found !!!</td>
                 </tr>
               <?php }else{
                 $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
                  <tr>
                    <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                    <td><?php echo $value->categoryname; ?></td>
                     <td><?php echo $value->campusname;?></td>
                    <?php //if ($value->BDFStatusaftergd ==1) { ?>
                      <td><a href="<?php echo site_url().'Selectedcandidates/preview/'.$value->candidateid;?>" class="btn btn-warning btn-xs" target="_blank" data-toggle="tooltip" title="To see  biodata form ? Click here." ><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></a>
                    <?php //}else{ ?>
                      <?php //echo $value->candidatefirstname;?> <?php //echo $value->candidatemiddlename;?> <?php //echo $value->candidatelastname;?></td>
                    <?php //} ?>
                    <td><?php echo $value->gender;?></td>
                    <td><?php echo $value->emailid;?></td>
                    <td><?php echo $value->stream;?></td>
                    <td><?php if ($value->BDFStatusaftergd !=1 ) { ?>
                     <a href="<?php  echo site_url().'Selectedcandidates/SendMail/'.$value->candidateid;?>" class="btn btn-info btn-sm" data-toggle="tooltip" title="Want to send email? Click on me."> Re-send </a>
                   <?php }else{?>
                    <!-- <span class="badge badge-pill badge-success" style="padding-top: 3%;"> -->
                     <span class="badge badge-pill badge-success" style="padding: 8%;" data-toggle="tooltip" title="BDF form Save ">Filled
                     <?php } ?></td>
                   </tr>
                   <?php $i++; } } ?>
                 </tbody>
               </table>
             </div>
           
         </div>
       </div>
     </div> 
   <?php } } ?>  
 </section>

 <!--  JS Code Start Here  -->
<script type="text/javascript">  
  $(document).ready(function(){
 
    $('#tableSelectcandidateScore').DataTable({
       dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'pdfHtml5',
             'print'
        ],
       "bPaginate": true,
       "bInfo": false,
       "bFilter": true,
       "bLengthChange": false
    }); 
  });

  
</script>

<!--  JS Code End Here  -->