<br>
<style type="text/css">
label{

  font-weight: bold;
  font-family: 
}
.bg-lightgreen
{
  background-color: #85a4a5;
}
/*.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
    height: 275px;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}*/

/*.container {
    padding: 2px 16px;
}

 #tbledubackground  {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#tbledubackground td, #tbledubackground th {
    border: 1px solid #ddd;
    padding: 8px;
    text-align: center;
}

#tbledubackground tr:nth-child(even){background-color: #f2f2f2; text-align: center;}

#tbledubackground tr:hover {background-color: #ddd; }


#tbledubackground th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
   /* background-color: #95A5A6;
    color: white;*/
}



 /*#tblForm09  {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#tblForm09 td, #tblForm09 th {
    border: 1px solid #ddd;
    padding: 8px;
}

#tblForm09 tr:nth-child(even){background-color: #f2f2f2;}

#tblForm09 tr:hover {background-color: #ddd;}

#tblForm09 th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    /*background-color: #95A5A6;*/
   
/*}*/
</style>
<div class="container-fluid" id="tblForm09" style="font-family: 'Oxygen' !important;" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >

 <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">Biodata Form</h4>
       
    </div>
    <hr class="colorgraph"><br>
  </div>
         <div class="panel-body">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th  colspan="7" style=" height: 5px;" class="bg-lightgreen text-white">Personal Infomation</th>
      </tr> 
    </thead>
    <tbody>
      <tr style="background-color: #fff;">
        <td rowspan ="6" class="text-left bg-light">

          <?php if ($candidatedetails->encryptedphotoname !='') { ?>
          <img class= "rounded-circle" src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Jane" width="160px" height="160px">
          <?php }else{ ?>
          <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px">
          <?php } ?>

          <h4><?php  
          echo $candidatedetails->candidatefirstname .' '.$candidatedetails->candidatemiddlename.' '.$candidatedetails->candidatelastname;
          ?> 
        </h4>
        <p style= "font-weight: 700"><i class="fa fa-envelope" style="font-size: 14px; color: #17A2B8;"></i> <?php echo $candidatedetails->emailid;?></p>
        <p style= "font-weight: 700"><i class="fa fa-mobile" aria-hidden="true" style="font-size: 14px; color: #17A2B8;"></i>
          <?php echo $candidatedetails->mobile;?></p>



        </td>
        <td><label for="Name">Candidate Name :</label></td>
        <td><?php echo $candidatedetails->candidatefirstname;?></td>
        <td><label for="Name">Middle Name :</label></td>
        <td style="width: auto;"><?php echo $candidatedetails->candidatemiddlename;?></td>
        <td><label for="Name">Last Name :</label></td>
        <td><?php echo $candidatedetails->candidatelastname;?></td>

      </tr>
      <tr style="background-color: #fff;">
        <td><label for="Name">Mother's Name :</label></td>
        <td><?php echo $candidatedetails->motherfirstname;?></td>
        <td><label for="Name">Middle Name :</label></td>
        <td style="width: auto;"><?php echo $candidatedetails->mothermiddlename;?></td>
        <td><label for="Name">Last Name :</label></td>
        <td><?php echo $candidatedetails->candidatelastname;?></td>
      </tr>
      <tr style="background-color: #fff;">
        <td><label for="Name">Father's Name :</label></td>
        <td><?php echo $candidatedetails->fatherfirstname;?></td>
        <td><label for="Name">Middle Name :</label></td>
        <td style="width: auto;"><?php echo $candidatedetails->fathermiddlename;?></td>
        <td><label for="Name">Last Name :</label></td>
        <td><?php echo $candidatedetails->fatherlastname;?> </td>



      </tr>
      <tr style="background-color: #fff;">
        <td><label for="Name">Gender :</label></td>
        <td><?php 
        $options = array("1" => "Male", "2" => "Female", "3" => "Others");
        if ($candidatedetails->gender ==1) {
          echo "Male";
        }elseif ($candidatedetails->gender ==2) {
         echo "Female";
       }elseif ($candidatedetails->gender ==3) {
         echo "Others";
       } ?>
     </td>
     <td><label for="Name">Nationality :</label></td>
     <td style="width: auto;"> <?php echo  $candidatedetails->nationality;?> </td>
     <td><label for="Name">Marital Status :</label></td>
     <td> <?php 
     $options = array("1" => "Single", "2" => "Married", "3" => "Divorced", "4" => "Widow", "5" => "Separated");
     if ($candidatedetails->maritalstatus ==1) {
      echo "Single";
    }else if ($candidatedetails->maritalstatus ==2) {
      echo "Married";
    }else if ($candidatedetails->maritalstatus ==3) {
      echo "Divorced";
    }else if ($candidatedetails->maritalstatus ==4) {
      echo "Widow";
    }else if ($candidatedetails->maritalstatus ==5) {
      echo "Separated";
    }
    ?> 
  </td>
</tr>

<tr style="background-color: #fff;">
  <td><label for="Name">Annual income of parents :</label></td>
  <td><?php 
  if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==1) {
   echo "Below 50,000";
 }else if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==2) {
  echo "50,001-200,000";
}else if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==3) {
  echo "200,001-500,000";
}else if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==4) {
 echo "Above 500,000";
}else{
  echo "N.A.";
}
?>
</td>
<td><label for="Name">No. of male sibling :</label></td>
<td style="width: auto;"><?php  if (!empty($otherinformationdetails->male_sibling) && $otherinformationdetails->male_sibling !='') {
  echo $otherinformationdetails->male_sibling;
}else{
  echo "N.A.";
}  ?></td>
<td><label for="Name">No. of female sibling :</label></td>
<td><?php if (!empty($otherinformationdetails->female_sibling) && $otherinformationdetails->female_sibling !='') {
 echo $otherinformationdetails->female_sibling;
} else{
  echo "N.A.";
} ?> </td>
</tr>
<tr style="background-color: #fff;">
  <td><label for="Name">Date Of Birth :</label></td>
  <td><?php echo $this->model->changedatedbformate($candidatedetails->dateofbirth);?></td>
  <td><label for="Name">Email Id :</label></td>
  <td style="width: auto;"> <?php echo $candidatedetails->emailid;?></td>
  <td><label for="Name">Mobile No. :</label></td>
  <td><?php echo $candidatedetails->mobile;?></td>
</tr>

</tbody>
</table>


<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table table-bordered table-striped" >
      <thead>
       <tr>
         <th colspan="8" class="bg-lightgreen text-white">

          <label class="form-check-label" for="filladdress"><b>Communication Address </b></label>

        </th>
      </tr> 
      <?php  if ($candidatedetails->presenthno==$candidatedetails->permanenthno) {
       $checkdata = 'checked="checked"';
     } ?>  
     <tr >
      <th class="text-center bg-light" colspan="4" style="vertical-align: top; text-align: center; "> Present Mailing Address</th>
      <th class="text-center" colspan="4" style="vertical-align: top; text-align: center;"> Permanent Mailing Address<br>
      </th>
    </tr> 
  </thead>
  <tbody>
   
   <tr style="background-color: #fff;">
     
    <td class="bg-light"> <label for="Name">H.No:</label></td>
    <td class="bg-light"><?php echo $candidatedetails->presenthno;?></td>
    <td class="bg-light"><label for="Name">Street:</label></td>
    <td class="bg-light"><?php echo $candidatedetails->presentstreet;?></td>
    
    <td><label for="Name">H.No :</label></td>
    <td><?php echo $candidatedetails->permanenthno;?></td>
    <td><label for="Name">Street :</label></td>
    <td><?php echo $candidatedetails->permanentstreet;?></td>
  </tr>
  <tr style="background-color: #fff;">
    <td class="bg-light"><label for="Name">State:</label></td>
    <td class="bg-light"><?php echo  $candidatedetails->presentstatename;?></td>
    <td class="bg-light"> <label for="Name">City:</label> </td>
    <td class="bg-light"><?php echo $candidatedetails->presentcity;?></td>
    <td ><label for="Name">State :</label></td>
    <td><?php echo $candidatedetails->permanentstatename;?></td>
    <td><label for="Name">City :</label></td>
    <td><?php echo $candidatedetails->permanentcity;?></td>
  </tr>
  <tr style="background-color: #fff;">
    <td class="bg-light"><label for="Name">District:</label> </td>
    <td class="bg-light"><?php echo $candidatedetails->presentdistrictname;?></td>
    <td class="bg-light"><label for="Name">Pin Code:</label></td>
    <td class="bg-light"><?php echo $candidatedetails->presentpincode;?></td>
    <td><label for="Name">District :</label></td>
    <td><?php echo $candidatedetails->permanentdistrictname;?></td>
    <td><label for="Name">Pin Code :</label></td>
    <td><?php echo $candidatedetails->permanentpincode;?></td>

  </tr>
</tbody>
</table>

        <?php if ($familycount->Fcount != 0){ ?> 
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <table id="tblForm09" class="table table-bordered table-striped" >
              <thead>
               <tr>
                <th colspan="4" class="bg-lightgreen text-white">Family Members </th>
              </tr> 
              <tr>
               <th style="text-align: center;"> Name</th>
               <th style="text-align: center;">Relation with Employee</th>
               <th style="text-align: center;">Date of Birth</th>
             </tr>
           </thead>
           <tbody id="tbodyForm09" >
            <?php $i=0;
            foreach ($familymemberdetails as $key => $val) {  $i++; ?>
              <tr id="tbodyForm09" style="text-align: center; background-color: #fff;">
               <td><?php echo $val->Familymembername ?></td>
               <td><?php echo $val->relationname;?></td>
               <td><?php echo $this->model->changedatedbformate($val->familydob);?></td>
             </tr>
        <?php }  ?>
      </tbody>

    </table>
  </div>
</div>
<?php } ?>
<?php if ($identitycount->Icount !=0) { ?>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

     <table id="tblForm09" class="table table-bordered table-striped" >
      <thead>
       <tr>
        <th colspan="5" class="bg-lightgreen text-white">
          <div class="col-lg-12">
            <div class="col-lg-6">Identity Details</div>
            <div class="col-lg-6 text-right ">

            </div>
          </div>  </th>
        </tr> 
        <tr>
         <th style="text-align: center;">Identity</th>
         <th style="text-align: center;">Number</th>
         <th style="text-align: center">Identity Name</th>
         <th style="text-align: center">Father Name </th>
         <th style="text-align: center">Date of birth</th>
         
       </tr>
     </thead>
     <tbody id="tbodyForm13" >

      <?php $i=0;
      foreach ($identitydetals as $key => $val) { $i++; ?>
        <tr id="tbodyForm13" style="text-align: center;background-color: #fff;" >
          <td><?php echo $val->name; ?></td>
         <td><?php echo $val->identitynumber;?></td>
         <td><?php echo $val->iname;?></td>
         <td><?php echo $val->ifather;?></td>
         <td><?php echo $this->model->changedatedbformate($val->idateofbirth);?></td>
        </tr>
      <?php } ?>
    </tbody>

  </table>
</div>
</div>

<?php }?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; ">

    <table id="tbledubackground" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white">Educational Background</th>
      </tr> 
      <tr>
        <th class="text-left" style="vertical-align: top;">Course</th>

        <th class="text-center" style="vertical-align: top;">Year </th>
        <th class="text-left" style="vertical-align: top;"> Board/ University</th>
        <th class="text-left" style="vertical-align: top;">School/ College/ Institute</th>
        <th class="text-left" style="vertical-align: top;">Place</th>
        <th class="text-left" style="vertical-align: top;">Specialisation</th>

        <th class="text-right" style="vertical-align: top;">Percentage(%)</th>

      </tr> 
    </thead>
    <tbody>
      <tr style="">
        <td><b>10th</b></td>
        <td class="text-center"><?php echo $candidatedetails->metricpassingyear;?>
        </td>
        <td> <?php echo $candidatedetails->metricboarduniversity;?></td>
        <td>  <?php echo $candidatedetails->metricschoolcollege;?></td>

        <td><?php echo $candidatedetails->metricplace;?>
        </td>
        <td><?php echo $candidatedetails->metricspecialisation;?>
        </td>


        <td class="text-right"><?php echo $candidatedetails->metricpercentage.'%';?> </td>


      </tr>
      <tr style="">
        <td><b><?php  if($candidatedetails->hscstream==2)
        { echo "12th"; }
        else { echo "Diploma";}
        ?></b> </td>
        <td class="text-center"><?php echo $candidatedetails->hscpassingyear;?>
        </td>
        <td> <?php  echo $candidatedetails->hscboarduniversity;?></td>

        <td>  <?php echo $candidatedetails->hscschoolcollege;?></td>

        <td><?php echo $candidatedetails->hscplace;?></td>

        <td><?php echo $candidatedetails->hscspecialisation;?></td>
        <td class="text-right"><?php echo $candidatedetails->hscpercentage.'%';?></td>
      </tr>
      <tr style="">
        <td><b><?php echo $candidatedetails->ugdegree; ?></b></td>
        <td class="text-center"><?php echo $candidatedetails->ugpassingyear; ?>
        </td>
        <td> <?php echo $candidatedetails->ugboarduniversity;?></td>
        <td>
          <?php echo $candidatedetails->ugschoolcollege;?>

        </td>
        <td><?php echo $candidatedetails->ugplace;?></td>



        <td>
         <?php echo  $candidatedetails->ugspecialisation;  ?>
       </td>

       <td class="text-right"><?php echo $candidatedetails->ugpercentage.'%';?>
       </td>

     </tr>
     <?php if (!empty($candidatedetails->pgpassingyear) && $candidatedetails->pgpassingyear !=0) {?>
     <tr style="">
      <td><b><?php echo $candidatedetails->pgdegree; ?></b></td>

      <td class="text-center"><?php echo $candidatedetails->pgpassingyear;?>
      </td>

      <td> <?php echo $candidatedetails->pgboarduniversity;?></td>
      <td>  
        <?php echo $candidatedetails->pgschoolcollege;?>

      </td>

      <td><?php echo $candidatedetails->pgplace;?></td>
      <td>

        <?php  echo $candidatedetails->pgspecialisation;  ?></td>




        <td class="text-right"><?php echo $candidatedetails->pgpercentage.'%';?>
        </td>

      </tr>
      <?php } ?>
      <?php if (!empty($candidatedetails->other_degree_specify) && $candidatedetails->other_degree_specify !='') {?>
      <tr style="">
        <td><b><?php echo $candidatedetails->other_degree_specify; ?></b></td>

        <td class="text-center"> <?php echo $candidatedetails->otherpassingyear; ?> </td>
        <td>  <?php echo $candidatedetails->otherboarduniversity; ?> </td>
        <td> <?php echo $candidatedetails->otherschoolcollege; ?>  </td>


        <td><?php echo $candidatedetails->otherplace; ?></td>

        <td> <?php echo $candidatedetails->otherspecialisation; ?> </td>

        <td class="text-right"> <?php echo $candidatedetails->otherpercentage .'%'; ?> </td>

      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
</div>

<?php if ($GapYearCount->GYcount !=0) {   ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">
        Gap Year
    </th>
  </tr> 
  <tr>
    <th class="text-center" style="vertical-align: top; text-align: center;">From Date </th>
    <th class="text-center" style="vertical-align: top; text-align: center;">To Date</th>
    <th class="text-center" style="vertical-align: top; text-align: center;"> Reason</th>
  </tr> 
</thead>
<tbody id="bodytblForm10">
 <?php  $i=0;
 foreach ($gapyeardetals as $key => $val) { $i++; ?>
  <tr id="bodytblForm10" style="text-align: center;background-color: #fff;">
    <td><?php echo $this->model->changedatedbformate($val->fromdate); ?></td>
    <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
     <td><?php echo $val->reason;?></td>
  </tr>
<?php } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>

<?php   if ($TrainingExpcount->TEcount !=0) {   ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">
        Training  Exposure (if any)
    </th>
  </tr> 
  <tr>

    <th class="text-center" style="vertical-align: top; text-align: center;">Nature of Training </th>
    <th class="text-center" style="vertical-align: top; text-align: center;">Organizing Agency</th>
    <th class="text-center" style="vertical-align: top; text-align: center;"> From Date</th>
    <th class="text-center" style="vertical-align: top; text-align: center; ">To Date</th>
  </tr> 
</thead>
<tbody id="bodytblForm10">
 <?php  $i=0;
 foreach ($trainingexposuredetals as $key => $val) { $i++; ?>
  <tr id="bodytblForm10" style="text-align: center;background-color: #fff;">
    <td><?php echo $val->natureoftraining;?></td>
    <td> <?php echo $val->organizing_agency;?></td>
    <td><?php echo $this->model->changedatedbformate($val->fromdate); ?></td>
    <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
  </tr>
<?php } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if ($languageproficiency->Lcount !=0) { ?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm11" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> 
      Language Skill/Proficiency
    </th>
  </tr> 
  <tr>

    <th class="text-center" style="vertical-align: top; text-align: center;">Language</th>
    <th class="text-center" style="vertical-align: top; text-align: center;">Speak</th>
    <th class="text-center" style="vertical-align: top; text-align: center;">Read</th>
    <th class="text-center" style="vertical-align: top; text-align: center;">Write </th>
  </tr> 
</thead>
<tbody id="bodytblForm11">
  <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
  <?php $i= 0;

  foreach ($languagedetalsprint as $key => $val) {
    $i++; ?>
    <tr id="bodytblForm11" style="text-align: center;background-color: #fff;">
      <td>  <?php
      echo $val->lang_name;?> </td>
    <td>
     <?php if($val->lang_speak=='H')
     { echo "High";
   }elseif ($val->lang_speak=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  ?></td>
   <td><?php if($val->lang_read=='H')
     { echo "High";
   }elseif ($val->lang_read=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  
    ?></td>
 <td><?php if($val->lang_write=='H')
     { echo "High";
   }elseif ($val->lang_write=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  
   ?></td>
</tr>
<?php }   ?>
</tbody>
</table>
</div>
</div>
<?php  } ?>
<?php if (!empty($otherinformationdetails->any_subject_of_interest) && $otherinformationdetails->any_subject_of_interest !='') { ?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Subject(s) of Interest </th>
    </tr> 
  </thead>
  <tbody>
    <tr style="background-color: #fff;">
      <td><?php echo $otherinformationdetails->any_subject_of_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php }  ?> 
<?php if(!empty($otherinformationdetails->any_achievementa_awards) && $otherinformationdetails->any_achievementa_awards !='') { ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Achievements /Awards (if any) </th>
    </tr> 

  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_achievementa_awards; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>

<?php } ?>

<?php if ($WorkExperience->WEcount != 0) {?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="7" class="bg-lightgreen text-white">
       Work Experience (if any) 
    </th>
  </tr> 
  <tr>
    <th style="text-align: center;">Organization Name</th>
    <th style="text-align: center;">Designation </th>
    <th style="text-align: center;">Description of Assignment</th>
    <th style="text-align: center;">Duration</th>
    <th style="text-align: center;">Palce of Posting</th>
    <th style="text-align: center;">Last salary drawn (monthly)</th>
  </tr> 
  <tr>
    <th colspan="3"></th>
    <th colspan="1" style="text-align: center;">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="3"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">
    <?php   
    $i=0;
    foreach ($workexperiencedetails as $key => $val) {   ?>
      <tr id="bodytblForm12" style="text-align: center;background-color: #fff;"> 
        <td><?php echo $val->organizationname;  ?></td>
        <td><?php echo $val->designation;?></td>
        <td><?php echo $val->descriptionofassignment;?></td>
        <td>
         <div class="col-lg-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <?php echo $this->model->changedatedbformate($val->fromdate);?>
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
           <?php echo $this->model->changedatedbformate($val->todate);?>
         </div> 
       </div>
     </td>
     <td><?php echo $val->palceofposting;?></td>
     <td><?php echo $val->lastsalarydrawn;?></td>
     

 </tr>

 <?php $i++; } ?>
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if (!empty($otherinformationdetails->any_assignment_of_special_interest) && $otherinformationdetails->any_assignment_of_special_interest !='') {?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" > 
   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Describe any assignment(s) of special interest undertaken by you</th>
    </tr> 

  </thead>
  <tbody>
    <tr style="background-color: #fff;">
      <td><?php echo $otherinformationdetails->any_assignment_of_special_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if (!empty($otherinformationdetails->experience_of_group_social_activities) && $otherinformationdetails->experience_of_group_social_activities !='') { ?>
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white">Experience of Group and/or Social Activities </th>
      </tr> 
    </thead>
    <tbody>
      <tr style="background-color: #fff;">
        <td><?php echo $otherinformationdetails->experience_of_group_social_activities; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

<?php if (!empty($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before)) { 
  ?> 
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tblForm09" class="table table-bordered table-striped">
  <thead>
   <tr>
    <td colspan="8" class="bg-lightgreen text-white"><b>Have you taken part in PRADAN's selection process before?</b>
  </td>
</tr> 
</thead>
<tbody>
  <tr style="background-color: #fff;">
    <td>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 

        <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='yes') {
          echo 'Yes';
        }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='no'){
        echo 'No';
        }
       ?>
</div>
    
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <?php echo $otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when; ?>
        
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
        <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==1) {
           echo 'ON Campus';
          }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==2) {
            echo 'Off Campus';
          } ?>
        
      </div>
        
    </td>
    </tr>
  </tbody>
</table>
</div>
</div>
</div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
      
        $("input[name$='pradan_selection_process_before']").load(function() {
        var selected = $(this).val();
       
        if (selected =='yes') {
          $("#when").removeAttr('disabled','false'); 
          $("#where").removeAttr('disabled','false');
        }else{
          $("#when").prop('disabled','true'); 
          $("#where").prop('disabled','true');
        }
       
    });

      $("input[name$='pradan_selection_process_before']").click(function() {
        var selected = $(this).val();
       
        if (selected =='yes') {
          $("#when").removeAttr('disabled','false'); 
          $("#where").removeAttr('disabled','false');
        }else{
          $("#when").prop('disabled','true'); 
          $("#where").prop('disabled','true');
        }
       
    });

});
</script>
<?php } ?>

<?php if (!empty($otherinformationdetails->know_about_pradan)) {  ?>
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tbltrainingexposure" class="table table-bordered table-striped">
  <thead>
   <tr>
    <td colspan="8" class="bg-lightgreen text-white"><b>From where have you come to know about PRADAN?</b>
    </td>
  </tr> 
</thead>
<tbody>
  <tr style="background-color: #fff;">
    <td>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
        <?php 
        if ($otherinformationdetails->know_about_pradan=='CampusPlacementCell') {
         echo 'Campus Placement Cell';
        } else if ($otherinformationdetails->know_about_pradan=='UniversityProfessors') {
          echo 'University Professors';
        }else if ($otherinformationdetails->know_about_pradan=='CampusAlumni') {
          echo 'Campus Alumni';
        }else if ($otherinformationdetails->know_about_pradan=='friends') {
          echo 'Friends';
        }else if ($otherinformationdetails->know_about_pradan=='other1') {
          echo $otherinformationdetails->know_about_pradan_other_specify;
        }
      ?> 
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $("input[name$='have_you_come_to_know']").click(function() {
        var test = $(this).val();
        if (test =='Other') {
          $("#other_have_you_come_to_know").show();
          $("#specify").prop('required','required');
           $('#specify').removeAttr("disabled");
         
        }else{
          $("#other_have_you_come_to_know").hide();
          $("#specify").prop('disabled','true');
        }
    });
});
</script>
<?php } ?>


<div class="row panel-footer" >
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" >
<a href="<?php echo site_url().'Selectedcandidates'; ?>" class="btn btn-dark text-right">Close</a>

 <br><br>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- End  Candidates Experience Info -->


