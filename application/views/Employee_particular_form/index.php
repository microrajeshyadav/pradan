<!-- <?php print_r($languagedetails);?> -->
<style type="text/css">

#height
{
    height:50px;
}

</style>
<style type="text/css">

[type="checkbox"]:not(:checked), [type="checkbox"]:checked
{
    /*position: absolute; */
    left: -9999px;
    /* opacity: 0; */

}

</style>
<?php 

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="<?php echo site_url('common/backend/css/themes/all-themes.css');?>" rel="stylesheet" />
<link href="<?php echo site_url('common/backend/jquery-ui.css');?>" rel="stylesheet" />


<section class="content" style="background-color: #FFFFFF;">
    <br/>
    <!-- Exportable Table -->
    <?php $page='employee'; require_once(APPPATH.'views/candidate\components\Employee_particular_form\topbar.php');?>

    <div class="container-fluid" style="font-family: 'Oxygen' !important;">
        <div class="row text-center" style="padding: 14px;">

           <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
            <form name="candidatesinfo" id="candidatesinfo" action="" method="post" enctype="multipart/form-data">
              <div class="panel-heading"> <div class="row">
                 <h4 class="col-md-12 panel-title pull-left">EMPLOYEE PARTICULARS FORM
                 </h4>
             </div>
             <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
            <div class="row">
                <div class="col-md-12 text-center" style="margin-bottom: 50px;">
                  <h6>Professional Assistance for Development Action (PRADAN) </h6> 
                  <h5>EMPLOYEE PARTICULARS FORM</h5>
              </div>
          </div>
          <?php 
          $tr_msg= $this->session->flashdata('tr_msg');
          $er_msg= $this->session->flashdata('er_msg');
          if(!empty($tr_msg)){ ?>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
          </div>

      </div>
      <?php } else if(!empty($er_msg)){?>


      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
        </div>
    </div>


    <?php } ?>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"> 
            <table  class="table">
                <tr class="bg-light">
                    <td colspan="2">
                    Basic Information</td>
                </tr>
                <tr id="height">
                    <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">1. Name </td>
                    <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"><?php  echo $candidatedetails->staff_name;?> </td>

                </tr>
                <tr id="height">
                   <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">2. Father’s/Husband’s Name: </td>
                   <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"><?php echo $candidatedetails->fatherfirstname.$candidatedetails->fathermiddlename.$candidatedetails->fatherlastname;?></td>

               </tr>
               <tr id="height">
                   <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">3. Sex:</td>
                   <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"> 
                    <?php if($candidatedetails->gender==1)
                    {
                        echo "Male";
                    }
                    else if($candidatedetails->gender==2)
                    {
                        echo "Female";
                    }
                    else
                    {
                        echo "Others";
                    }?> 
                </td>

            </tr>
            <tr>
               <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">4. Nationality: </td>
               <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"> <?php echo $candidatedetails->nationality;?></td>

           </tr>
           <tr id="height">
               <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">5. Date Of Birth: </td>
               <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"> <?php echo  $this->model->changedate($candidatedetails->dateofbirth);?></td>

           </tr>
           <tr id="height">      
               <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">6. Marital Status: </td>
               <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"> 
                <?php  if($candidatedetails->maritalstatus==1)
                {
                  echo "Single";
              }
              else if($candidatedetails->maritalstatus==2)
              {
                  echo "Married";
              }
              else if($candidatedetails->maritalstatus==3)
              {
                  echo "Divorced";
              }
              else if($candidatedetails->maritalstatus==4)
              {
                  echo "Widow";
              }
              else
              {
                  echo "Separated"; }?>
              </td>

          </tr>
          <tr id="height">
            <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">7. Email Id: </td>                   
            <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"> <?php echo   $candidatedetails->emailid;?>

            </td>
        </tr>
        <tr id="height">
            <td class="col-lg-3 col-md-3 col-sm-12 col-xs-12">8. Home Town: </td>
            <td class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style= "font-weight: 700"> <input type="text"  maxlength="50" data-toggle="tooltip" title="Enter Home Town" name="home_town" id="mobile" class="form-control" data-country="India" value="<?php echo set_value("mobile");?>" placeholder="Enter Home Town" required="required">
            </td>
        </tr>
    </table>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
 <input type="hidden" name="originalfamilymemberphoto[]" id="originalfamilymemberphoto" value="<?php if(!empty($val->originalphotoname)) echo $val->originalphotoname; ?>">
 <input type="hidden" name="oldfamilymemberphoto[]" id="oldfamilymemberphoto" value="<?php if(!empty($val->encryptedphotoname)) echo $val->encryptedphotoname; ?>">
 <?php if ($candidatedetails->encryptedphotoname !='') { ?>
 <img class= "rounded-circle" src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Jane" width="160px" height="160px">
 <?php }else{ ?>
 <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
 <?php } ?>

</div>
</div>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table" >
        <thead>
            <tr class="bg-light">
                <th colspan="8"> 

                    <b>9. Communication Address </b>

                </th>
            </tr> 
            <?php  if ($candidatedetails->presenthno==$candidatedetails->permanenthno) {
                $checkdata = 'checked="checked"';
            } ?>  
            <tr>
                <th class="text-center" colspan="4" style="background-color: #e1e1e1; vertical-align: top; text-align: center; "> <i class="fa fa-address-card" style="color: #F29005;"></i> &nbsp;Present Mailing Address</th>
                <th class="text-center" colspan="4" style="background-color: #d0d0d0; vertical-align: top; text-align: center;">  <i class="fa fa-address-card" style="color: #17A2B8;"></i> &nbsp;Permanent Mailing Address<br>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input"  <?php if(!empty($checkdata)) echo $checkdata;?>  id="filladdress" name="filladdress"> 
                        <label class="form-check-label" for="filladdress"><b>same as present address</b></label>
                    </div></th>
                </tr> 
            </thead>
            <tbody>
                <tr>
                    <td> <label for="Name">H.No</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->presenthno;?></td>
                    <td><label for="Name">Street</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->presentstreet;?></td>
                    <td><label for="Name">H.No</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->permanenthno;?></td>
                    <td><label for="Name">Street</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->permanentstreet;?></td>
                </tr>
                <tr>
                    <td><label for="Name">State</label></td>
                    <td style="font-weight: 700"><?php echo  $candidatedetails->present_state;?></td>
                    <td> <label for="Name">City</label> </td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->presentcity;?></td>
                    <td><label for="Name">State</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->parmanent_state;?></td>
                    <td><label for="Name">City</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->permanentcity;?></td>
                </tr>
                <tr>
                    <td><label for="Name">District</label> </td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->present_district;?></td>
                    <td><label for="Name">Pin Code</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->presentpincode;?></td>
                    <td><label for="Name">District</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->permanent_district;?></td>
                    <td><label for="Name">Pin Code</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->permanentpincode;?></td>
                </tr>
                <tr>
                    <td><label for="Name">Tel No. </label> </td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->present_district;?></td>
                    <td><label for="Name">Mobile No . *</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->mobile;?></td>
                    <td><label for="Name">Tel No</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->permanent_district;?></td>
                    <td><label for="Name">Mobile No</label></td>
                    <td style="font-weight: 700"><?php echo $candidatedetails->mobile;?></td>
                </tr>
            </tbody>
        </table>
    </div> 
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbledubackground" class="table">
            <thead>
             <tr class="bg-light">
                <th colspan="7">11. Educational Background</th>
            </tr> 
            <tr>
                <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate</th>

                <th class="text-center" style="vertical-align: top;">Year </th>
                <th class="text-center" style="vertical-align: top;">Board/ University</th>
                <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
                <th class="text-center" style="vertical-align: top;">Place</th>
                <th class="text-center" style="vertical-align: top;">Specialisation</th>
                <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
            </tr> 
        </thead>
        <tbody>
            <tr>
                <td><b>10th</b></td>
                <td><?php echo $candidatedetails->metricpassingyear;?>
                </td>
                <td> <?php echo $candidatedetails->metricboarduniversity;?></td>
                <td>  <?php echo $candidatedetails->metricschoolcollege;?></td>

                <td><?php echo $candidatedetails->metricplace;?>
                </td>
                <td><?php echo $candidatedetails->metricspecialisation;?>
                </td>
                <td><?php echo $candidatedetails->metricpercentage;?> </td>
            </tr>
            <tr>
                <td><b>12th</b> </td>
                <td><?php echo $candidatedetails->hscpassingyear;?></td>
                <td> <?php  echo $candidatedetails->hscboarduniversity;?></td>
                <td>  <?php echo $candidatedetails->hscschoolcollege;?></td>
                <td><?php echo $candidatedetails->hscplace;?></td>
                <td><?php echo $candidatedetails->hscspecialisation;?></td>
                <td><?php echo $candidatedetails->hscpercentage;?></td>
            </tr>
            <tr>
                <td><b>UG</b></td>
                <td><?php echo $candidatedetails->ugpassingyear; ?></td>
                <td> <?php echo $candidatedetails->ugboarduniversity;?></td>
                <td><?php echo $candidatedetails->ugschoolcollege;?></td>
                <td><?php echo $candidatedetails->ugplace;?></td>
                <td><?php echo  $candidatedetails->ugspecialisation;?></td>
                <td><?php echo $candidatedetails->ugpercentage;?>
                </td>
            </tr>
            <?php if (!empty($candidatedetails->pgpassingyear) && $candidatedetails->pgpassingyear !=0) {?>
            <tr>
                <td><b>PG</b></td>
                <td><?php echo $candidatedetails->pgpassingyear;?></td>
                <td> <?php echo $candidatedetails->pgboarduniversity;?></td>
                <td><?php echo $candidatedetails->pgschoolcollege;?></td>
                <td><?php echo $candidatedetails->pgplace;?></td>
                <td><?php  echo $candidatedetails->pgspecialisation;?></td>
                <td><?php echo $candidatedetails->pgpercentage;?></td>
            </tr>
            <?php } ?>
            <?php if (!empty($candidatedetails->other_degree_specify) && $candidatedetails->other_degree_specify !='') {?>
            <tr>
                <td><b><?php echo $candidatedetails->other_degree_specify; ?></b></td>
                <td> <?php echo $candidatedetails->otherpassingyear; ?> </td>
                <td>  <?php echo $candidatedetails->otherboarduniversity; ?> </td>
                <td> <?php echo $candidatedetails->otherschoolcollege; ?>  </td>
                <td><?php echo $candidatedetails->otherplace; ?></td>
                <td> <?php echo $candidatedetails->otherspecialisation; ?> </td>
                <td> <?php echo $candidatedetails->otherpercentage; ?> </td>
            </tr>
            <?php } ?>                         
        </tbody>
    </table>
</div>
</div>
<?php if(count($familymemberdetails) !=0){?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
            <thead>
                <tr class="bg-light">
                    <th colspan="8">10. Family Member
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>S No.</td>
                  <td>Name</td>
                  <td>Date of Birth</td>
                  <td>Age</td>
                  <td>Relation with Employee</td>
              </tr>

              <?php 
               // print_r($familymemberdetails);
              $i=0; foreach($familymemberdetails as $val) {
               
                  $i++;
                  ?>
                  <tr>
                    <td><?php echo $i;?> </td>
                    <td><?php echo $val->Familymembername;?></td>
                    <td><?php echo $this->model->changedate($val->familydob);?></td>
                    <td><?php 
                    if(!empty($val->familydob))
                    {
                        $cur_year = date('Y');
                        $year=$this->model->changedate($val->familydob);
                        $arr=explode('/', $year);

                        echo $cur_year-$arr[2];
                    }
                    ?></td>
                    <td>
                        <?php echo $val->relationname;?>                    
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php }?>
<!-- <?php //print_r($trainingexposuredetals);?> -->
<?php if(count($trainingexposuredetals) !=0){ ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table id="tblForm10" class="table">
        <thead>
          <tr class="bg-light">
              <th colspan="4">
                <div class="pull-left">12. Training  Exposure (if any)</div>
              
         </th>
     </tr> 
     <tr>
        <th class="text-center" style="vertical-align: top;">Nature of Training </th>
        <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
        <th class="text-center" style="vertical-align: top;">From Date</th>
        <th class="text-center" style="vertical-align: top;">To Date</th>
    </tr> 
</thead>
<tbody id="bodytblForm10">
  <?php foreach($trainingexposuredetals as $val){?>
  <tr id="bodytblForm10">
    <td class="text-center" style="vertical-align: top;"><?php echo $val->natureoftraining;?></td>
    <td class="text-center" style="vertical-align: top;"><?php echo $val->organizing_agency;?></td>
    <td class="text-center" style="vertical-align: top;"><?php echo $this->gmodel->changedatedbformate($val->fromdate);?></td>
    <td class="text-center" style="vertical-align: top;"><?php echo $this->gmodel->changedatedbformate($val->todate);?></td>

</tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<?php }?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
            <thead>
              <tr class="bg-light">
                  <th colspan="8">13. Membership of Professional/Academic Associations</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <td><textarea class="form-control alphabateonly" name="professional_academy"  maxlength="1500" id="professional_academy" cols="12" rows="5" data-toggle="tooltip" placeholder="Enter Membership of Professional/Academic Associations" title="Enter Subject(s) of Interest !" ><?php echo set_value('subjectinterest');?></textarea></td>
          </tr>
      </tbody>
  </table>
</div>
</div>
<?php if(count($otherinformationdetails)== 0){ ?>
  <?php }else{ ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
          <thead>
           <tr class="bg-light">
            <th colspan="8">14. Achievements / Awards (if any) </th>
        </tr> 
    </thead>
    <tbody>
      <tr>
        <td>  <?php if(!empty($otherinformationdetails->any_achievementa_awards)) echo $otherinformationdetails->any_achievementa_awards;?></td>
    </tr>
</tbody>
</table>
</div>
</div>
<?php } ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
            <thead>
             <tr class="bg-light">
              <th colspan="8">15. Hobbies and Interest </th>
          </tr> 
      </thead>
      <tbody>
        <tr>
          <td><textarea class="form-control alphabateonly" name="hobbies" id="hobbies" cols="12" rows="5"   maxlength="1500" data-toggle="tooltip" placeholder="Enter Hobbies and Interest " title="Enter Achievements /Awards (if any) !" ><?php echo set_value('achievementawards');?></textarea></td>
      </tr>
  </tbody>
</table>
</div>
</div>
<?php 
// echo print_r($languagedetails); 
// echo $languagedetails->Lcount;
// die;

// echo "<pre>";
//  print_r($languagedetails); 
 // if($languagedetails->Lcount > 0){

  ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tblForm11" class="table">
          <thead>
            <tr class="bg-light">
                <th colspan="5"> 
                  <div class="pull-left">16. Language Skill/Proficiency </div>
           </th>
       </tr>
       <tr>
        <th class="text-center" style="vertical-align: top;">Language <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;">Speak <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;">Read <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;">Write <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;">Understand <span style="color: red;">*</span></th>
    </tr> 

</thead>
<tbody id="bodytblForm11">
    <?php foreach($languagedetails as $val){?>
      <tr id="bodytblForm11">
        <td> 
         <?php
         $options = array('' => 'Select');
         foreach($syslanguage as $key => $value) {
          $options[$value->lang_cd] = $value->lang_name;
        }
        echo form_dropdown('syslanguage[]', $options, $val->languageid, 'class="form-control" data-toggle="tooltip" required title="Select language !" readonly="readonly"');
        ?>
        <?php echo form_error("syslanguage");?></td>
        <td>
         <?php
         $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
         echo form_dropdown('speak[]',$sysspeak,$val->lang_speak,'class ="form-control" data-toggle="tooltip" required title="Select Speak !" readonly="readonly"');
         ?>
         <?php echo form_error("speak");?>
       </td>
       <td><?php
       $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
       echo form_dropdown('read[]',$sysread,$val->lang_read,'class ="form-control" data-toggle="tooltip" required title="Select Read !" readonly="readonly"');
       ?>
       <?php echo form_error("read");?></td>
       <td><?php
       $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
       echo form_dropdown('write[]',$syswrite,$val->lang_write,'class ="form-control" data-toggle="tooltip" required title="Select Write !" readonly="readonly"');
       ?>
       <?php echo form_error("write");?></td>
       <td>
         <?php
         $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
         echo form_dropdown('understand',$sysspeak, $val->understand, ' class ="form-control" data-toggle="tooltip" required title="Select Speak !" readonly="readonly"  ');
         ?>
         <?php echo form_error("speak");?>
       </td>
     </tr>
     <?php } ?>
   </tbody>
</table>
</div>
</div>
<?php// }?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
          <thead>
           <tr class="bg-light">
            <th colspan="8">17. Publication </th>
        </tr> 
    </thead>
    <tbody>
      <tr>
        <td><textarea class="form-control alphabateonly" name="publication" id="publication" cols="12" rows="5"   maxlength="1500" data-toggle="tooltip" placeholder="Enter Publication" title="Enter Achievements /Awards (if any) !" ><?php echo set_value('achievementawards');?></textarea></td>
    </tr>
</tbody>
</table>
</div>
</div>

<?php
 if($WorkExperience->WEcount > 0){?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tblForm12" class="table">
            <thead>
              <tr class="bg-light">
                  <th colspan="8">

                    <div class="pull-left">18. Work Experience (if any) </div>
                    <!--<div class="pull-right ">
                     <button type="button" id="btnworkexperienceRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                     <button type="button" id="btnworkexperienceAddRow" class="btn btn-warning btn-xs">Add</button>
                 </div>
                 -->

             </th>
         </tr> 
         <tr>
          <th>Organization Name</th>
          <th>Designation</th>
          <th>Description of Assignment</th>
          <th>Duration</th>
          <th>Palce of Posting</th>
          <th>Last salary drawn (monthly)</th>
      </tr> 
      <tr>
          <th colspan="3"></th>
          <th colspan="1">
            <div class="col-lg-12">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
          </div></th>
          <th colspan="3"></th>
      </tr>
  </thead>
  <tbody id="bodytblForm12">  
      <?php   foreach($workexperiencedetails as $val) { ?>

      <tr id="bodytblForm12"> 
        <td><?php echo $val->organizationname;?></td>
        <td><?php echo $val->designation;?></td>
        <td><?php echo $val->descriptionofassignment;?></td>
        <td><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <?php echo $this->model->changedate($val->fromdate);?>
        </div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
            <?php echo $this->model->changedate($val->todate);?>
        </div>
    </td>
    <td><?php echo $val->palceofposting;?></td>
    <td>
      <?php  echo $val->lastsalarydrawn;?></td>
  </tr>
  <?php } ?>
</tbody>
</table>
</div>
</div>
<?php }?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
        <table id="tbltrainingexposure" class="table">
          <thead>
           <tr class="bg-light">
            <th colspan="8">19. Any Other Information that the Applicant May Like to Add: </th>
        </tr> 
    </thead>
    <tbody>
      <tr>
        <td><textarea class="form-control alphabateonly" name="achievementawards" id="achievementawards" cols="12" rows="5"   maxlength="1500" data-toggle="tooltip" placeholder="Enter any Other Information that the Applicant May Like to Add" title="Enter Achievements /Awards (if any) !" ><?php echo set_value('achievementawards');?></textarea></td>
    </tr>
</tbody>
</table>
</div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12" style="margin-bottom: 15px;">
        <table class="table">
            <thead >
              <tr class="bg-light">
                <th class="">
                    20. Date Of joinging PRADAN <span style="color: red;" >*</span>
                </th>
                <th class="">
                    Designation<span style="color: red;" >*</span>
                </th>
               <!--  <th class="">
                    Basic Pay: (In Rs. )<span style="color: red;" >*</span>
                </th> -->
            </tr>
        </thead>
        <tbody>
            <tr>
            <?php foreach($staff_designation as $val){?>
                <td>
                    
                    <input type="text" name="doj" placeholder="Enter Date Of joinging PRADAN " id="presentstreet" class="form-control" maxlength="150" required="required" value="<?php echo $this->model->changedate($val->doj);?>" readonly>
                    
                </td>
                <td>
                   
                   <input type="text" name="designation"  placeholder="Enter Designation" id="presentstreet" class="form-control" maxlength="30"  required="required" value="<?php echo $val->desname;?>" readonly>
                   
               </td>
               <!-- <td>
                   
                   <input type="text" name="basic_salary" id="basic_salary" placeholder="Enter Basic Salary" readonly class="form-control" maxlength="10" readonly  required="required" value="<?php echo $val->Joining_basicpay;?>">
                   
               </td> -->
                <?php } ?>
           </tr>

       </tbody>

   </table>

</div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
            <thead>
              <tr class="bg-light">
                  <td colspan="8"><b>21. Is any of your close relation(s) working with PRADAN: Please tick</b>
                  </td>
              </tr> 
          </thead>
          <tbody>
            <tr>
              <td>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                  <div class="form-check">
                    <input type="radio" class="form-check-input" id="yes" value="yes" name="pradan_selection_process_before">
                    <label class="form-check-label" for="yes"><b>Yes</b></label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" class="form-check-input" id="no" value="no" checked="checked" name="pradan_selection_process_before"  >
                    <label class="form-check-label" for="no"><b>No</b></label>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
             <label for="Name">Name</label>
             <input type="text" name="designation" id="name1" <?php echo set_value('when');?> class="form-control" readonly value="<?php if(!empty($pradanmember->name)) echo $pradanmember->name; ?>">
         </div>
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
            <label for="Name">Designation:</label>
            <input type="text" name="whedn" readonly value="<?php if(!empty($pradanmember->desname)) echo $pradanmember->desname;?>" id="Designation1" <?php echo set_value('when');?> class="form-control">
        </div>

        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
            <label for="Name">Relationship:</label>
            <input type="text" name="whedn" readonly value="<?php if(!empty($pradanmember->relationname)) echo $pradanmember->relationname;?>" id="Relationship1" <?php echo set_value('when');?> class="form-control">
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
            <label for="Name">Place of Posting:</label>
            <input type="text" name="whedn" readonly value="<?php if(!empty($pradanmember->officename)) echo $pradanmember->officename;?>" id="placeofposting1" <?php echo set_value('when');?> class="form-control">
        </div>
    </td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table id="tbltrainingexposure" class="table">
          <thead>
           <tr class="bg-light">
            <td colspan="8"><b>22. I declare that all the particulars furnished above are true and correct to the best of my knowledge and belief and there is no mis-statement or willful concealment of facts.</b>
            </td>

        </tr> 
    </thead>
    <tbody>
      <tr>
        <td>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
            <div class="form-check">
               <?php echo form_error("dateofbirth");?>
             <label for="Name">Date<span style="color: red;" >*</span></label>
             <input type="text" class="form-control datepicker" data-toggle="tooltip" title="Declaration Date!" id="d_date" name="d_date" placeholder="Enter Date" value="<?php echo date('d/m/Y');?>" required="required">
            
         </div>
     </div>

     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
      <div class="form-check">
        <label for="Name">Signature<span style="color: red;" >*</span></label>

        <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>

    <!--     <?php if ($signature->encrypted_signature !='') { ?>
   <img class= "rounded-circle" src="<?php if(!empty($signature->encrypted_signature)) echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
    <?php }else{ ?>
 <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
 <?php } ?> -->
   </div>
</div>

</div>
</td>
<td>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label for="Name">Place<span style="color: red;" >*</span></label>
    <input type="text" name="d_place" id="d_place" placeholder="Enter Place" class="form-control" >
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <label for="Name">Name<span style="color: red;" >*</span></label>
    <input class="form-control" name='d_name' placeholder="Enter Name" type="text"  id="d_name" >
</div>
</td>
</tr>
</tbody>
</table>
</div>

</div>


<div class="panel-footer text-right" style="float:left;width: 100%;"> 
    <button  type="submit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save" name="savebtn" id="save" value="senddatasave">Save</button>
    <button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>
    <a href="<?php echo site_url('staff_dashboard');?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
</div>
</form>
</div>

</div>
</div>
</section>



<script type="text/javascript">
  $(document).ready(function() {
   $("#name1").prop('disabled','true'); 
   $("#Designation1").prop('disabled','true');
   $("#Relationship1").prop('disabled','true');
   $("#placeofposting1").prop('disabled','true');
   $("input[name$='pradan_selection_process_before']").click(function() {
    var selected = $(this).val();

    if (selected =='yes') {
      $("#name1").removeAttr('disabled','false'); 
      $("#Designation1").removeAttr('disabled','false');
      $("#Relationship1").removeAttr('disabled','false');
      $("#placeofposting1").removeAttr('disabled','false');
  }else{
      $("#name1").prop('disabled','true'); 
      $("#Designation1").prop('disabled','true');
      $("#Relationship1").prop('disabled','true');
      $("#placeofposting1").prop('disabled','true');
  }

});

});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
   });
  });
</script>  



<script type="text/javascript">
  $(document).ready(function() {
    $("input[name$='other_specify_degree']").change(function() {
      var test = $('#other').val();
      // alert(test);
      if ( $(this).is(':checked') ) {
        $("#other_other_specify_degree").show();
        $("#specify_degree").prop('required','required');
    } else {
       $("#other_other_specify_degree").hide();
       $("#specify").prop('disabled','true');
   }

});

    $("#knowother").click(function() {
      //var radio = $('#knowother').val();
      var radioButtons = $("input[type='radio'][id='knowother']");

      alert(radioButtons);

      if (radioButtons=='Others') {
        $("#other_have_you_come_to_know_specify").show();
        $("#specify").prop('required','required');
        
    }else{
        $("#other_have_you_come_to_know_specify").hide();
        $("#specify").prop('disabled','true');

    }

});

});
</script>
<script type="text/javascript">

  function ifanyvalidation(){
    var natureoftraining = $('#natureoftraining').val();
    var organizingagency = $('#organizingagency').val();
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var orgname = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var designation = $('#designation').val();
    var last_salary_drawn = $('#last_salary_drawn').val();
    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();
    var gapfromdate = $('#gapfromdate').val();
    var gaptodate = $('#gaptodate').val();
    var gapreason = $('#gapreason').val();

    if (gapfromdate !='') {
      if (gaptodate =='') {

       $('#gaptodate').focus();

       return false;
   }
   if (gapreason =='') {
       $('#gapreason').focus();

       return false;
   }

}

if (natureoftraining !='') {
    if (organizingagency =='') {
         // alert('Please Enter Organizing Agency!!!');
         $('#organizingagency').focus();
         return false;
     }
     if (fromdate =='') {
         $('#fromdate').focus();
         return false;
     }
     if (todate =='') {
         $('#todate').focus();
         return false;
     }

 }

 if (orgname !='') {
   if (designation =='') {
    $('#designation').focus();
    return false;
}
if (descriptionofassignment =='') {
   $('#descriptionofassignment').focus();
   return false;
}
if (work_experience_fromdate =='') {
  $('#work_experience_fromdate').focus();
  return false;
}


if (work_experience_todate =='') {
 $('#work_experience_todate').focus();
 return false;
}
if (palceofposting =='') {
 $('#palceofposting').focus();
 return false;
}

if (last_salary_drawn =='') {
    $('#last_salary_drawn').focus();
    return false;
}

}
}

 /// Upload Buttone /////////

 $(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
 $(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
 
 var matriccertificate_uploadField = document.getElementById("10thcertificate");

 matriccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
};
}; 


var hsccertificate_uploadField = document.getElementById("12thcertificate");

hsccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
};
}; 

var ugcertificate_uploadField = document.getElementById("ugcertificate");

ugcertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
};
}; 

</script>

<script type="text/javascript">
  $(document).ready(function(){
    decimalData();
    adddatepicker();
    workexchangedatepicker(incwe);
    changedatepicker(inctg);
    ifanyvalidation();
    
    $('[data-toggle="tooltip"]').tooltip(); 
    $("#pradan_selection_process_before_details").prop("disabled", "disabled");
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
    } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
    }
});

    $('#contact_name').on('input', function() {
     var input=$(this);
     var is_name=input.val();
     if(is_name){input.removeClass("invalid").addClass("valid");}
     else{input.removeClass("valid").addClass("invalid");}
 });
    

});

  $(function () {
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
    } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
    }
});
});
  

// $("#sendDataWorkExpButton").click(function() {
//     var count_checked = $("[name='selection_process_befor']:checked").length; // count the checked rows
//         if(count_checked == 0) 
//         {
//             alert("Have you taken part in PRADAN's selection process before?.");
//             return false;
//         }
//         return true;
// });


$(document).ready(function(){
  $("#filladdress").on("click", function(){
   if (this.checked) { 
    if ($("#permanenthno").val().length == 0  && $("#presenthno").val().length ==0) {
      alert('Please Fill Present Mailing Address !!!');
      $("#filladdress").removeAttr('checked');
  }else{

      $("#permanenthno").val($("#presenthno").val());
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ $("#presentstateid").val(),
        type: 'POST',
        dataType: 'text',
    })
      .done(function(data) {

            //console.log(data);
            $("#permanentdistrict").html(data);
            $("#permanentdistrict").val($("#presentdistrict").val());

        })

      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 

  }
}
else {
    $("#permanenthno").val('');
    $("#permanentstreet").val('');
    $("#permanentcity").val('');
    $("#permanentstateid").val(''); 

    $("#permanentdistrict").val('');
    $("#permanentpincode").val('');
    $("#permanentstateid").val('');          
}
});

});


function decimalData(){
  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
 }
}, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});

}

$("#btnRemoveRow").click(function() {
  if($('#tblForm09 tr').length-2>1)
    $('#tbodyForm09 tr:last').remove()
});

$('#btnAddRow').click(function() {
  rowsEnter = parseInt(1);

  if (rowsEnter < 1) {
    alert("Row number must be minimum 1.")
    return;
}
insertRows(rowsEnter);
    //addDatePicker();
});



var srNoGlobal=0;
var inc = 0;

function insertRows(count) {
  srNoGlobal = $('#tbodyForm09 tr').length+1;
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;

  for (i = 1; i <= count; i++) {
    inc++;
    cloneRow = lastRow.clone();
    var tableData = '<tr>'
    + ' <td>'
    + '<input type="text" name="familymembername['+inc+']" id="familymembername" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
    + '</td><td>'
    + '<select class="form-control" name="relationwithenployee['+inc+']" required="required">'
    + '<option value="">Select Relation</option>'
    <?php foreach ($sysrelations as $key => $value): ?>
    + '<option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>'
    <?php endforeach ?>
    + '</select>'
    + '</td>'
    + '<td><input type="text" class="form-control datepicker" id="familymemberdob'+inc+'" data-toggle="tooltip" title="Date Of Birth!" name="familymemberdob['+inc+']" placeholder="Enter Date Of Birth " value="" required="required" >'

    + '</td>'
    + '</tr>';
    adddatepicker();
    $("#tbodyForm09").append(tableData)

}

}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
});

  $('#btntrainingexposureAddRow').click(function() {
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
  }
  Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
});



  var srNoGlobal=0;
  var inctg = 0;

  function Insaettrainingexposure(count) {
    srNoGlobal = $('#bodytblForm10 tr').length+1;
    var tbody = $('#bodytblForm10');
    var lastRow = $('#bodytblForm10 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inctg++
      cloneRow = lastRow.clone();
      var tableData1 = '<tr>'
      + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="natureoftraining" name="natureoftraining['+inctg+']" placeholder="Enter Nature of Training" style="min-width: 20%;" value="" data-original-title="Nature of Training !">'
      + '</td>'
      + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="organizingagency['+inctg+']" name="organizingagency['+inctg+']" placeholder="Organizing Agency" style="min-width: 20%;" value="" required="required" data-original-title="Organizing Agency !"></td>'

      + '<td><input type="text" class="form-control " onclick="return changedatepicker(inctg)" id="fromdate_'+inctg+'" '+
      'name="fromdate['+inctg+']" value=""  required="required" />'
      + '</td><td>'
      + '<input type="text" class="form-control " onclick="return changedatepicker(inctg)" id="todate_'+inctg+'" '+
      'name="todate['+inctg+']" value="" required="required" />'
      + '</td>' + '</tr>';
      $("#bodytblForm10").append(tableData1)
      adddatepicker();
      changedatepicker(inctg);
  }

}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnproficiencyRemoveRow").click(function() {
    if($('#tblForm11 tr').length-2>1)
      $('#bodytblForm11 tr:last').remove()
});

  $('#btnproficiencyAddRow').click(function() {
    rowsEnter2 = parseInt(1);
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
  }
  Insaetproficiency(rowsEnter2);
    //addDatePicker();
});




  var srNoGlobal=0;
  var inclp = 0;

  function Insaetproficiency(count) {
    srNoGlobal = $('#bodytblForm11 tr').length+1;
    var tbody = $('#bodytblForm11');
    var lastRow = $('#bodytblForm11 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inclp++
      cloneRow = lastRow.clone();
      var tableData2 = '<tr>'
      + '<td><select class="form-control" id="syslanguage'+inclp+'"   name="syslanguage['+inclp+']" required="required">'
      
      <?php foreach ($syslanguage as $key => $value): ?>
      + '<option value=<?php echo $value->lang_cd;?>><?php echo $value->lang_name;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'
      + '<select class="form-control" name="speak['+inclp+'] id="speak'+inclp+'"required="required">'

      <?php foreach ($sysspeak as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'

      + '<select class="form-control" id="read'+inclp+'" name="read['+inclp+']" required="required">'

      <?php foreach ($sysread as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'

      + '<select class="form-control" id="write'+inclp+'" name="write['+inclp+']" required="required">'

      <?php foreach ($syswrite as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td>'
      + '</tr>';
      $("#bodytblForm11").append(tableData2)
      adddatepicker();
  }

}
  //insertRows();
</script>


<script type="text/javascript">
  $("#btnworkexperienceRemoveRow").click(function() {
    if($('#tblForm12 tr').length-3>1)
      $('#bodytblForm12 tr:last').remove()
});

  $('#btnworkexperienceAddRow').click(function() {
    rowsEnter2 = parseInt(1);
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
  }
  Insertworkexperience(rowsEnter2);
    //addDatePicker();
});


  var srNoGlobal=0;
  var incwe = 0;

  function Insertworkexperience(count) {
    srNoGlobal = $('#bodytblForm12 tr').length+1;
    var tbody = $('#bodytblForm12');
    var lastRow = $('#bodytblForm12 tr:last');
    var cloneRow = null;
    
    for (i = 1; i <= count; i++) {
      incwe++
      
      cloneRow = lastRow.clone();
      var tableData2 = '<tr>'
      + '<td>'
      + '<input type="text" name="orgname['+incwe+']" id="orgname" data-toggle="tooltip"'
      + 'value="" class="form-control alphabateonly"  maxlengt="100"'
      + 'data-original-title="" title="" required="required">'
      
      + '</td><td>'
      + '<input type="text" name="designation['+incwe+']" id="designation" data-toggle="tooltip" value="" class="form-control alphabateonly" maxlengt="50" data-original-title="" title="" required="required">'
      + '</td><td>'
      + '<textarea name="descriptionofassignment['+incwe+']" data-toggle="tooltip" maxlengt="250" id="descriptionofassignment" class="form-control alphabateonly" data-original-title="" required="required" title=""></textarea></td><td>'
      + ' <div class="col-lg-12">'
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">'
      + ' <input type="text"  name="work_experience_fromdate['+incwe+']"  id="work_experience_fromdate_'+incwe+'" onclick="return workexchangedatepicker(incwe)" value="" required="required" class="form-control datepicker">'
      + ' </div>' 
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">'
      + ' <input type="text" name="work_experience_todate['+incwe+']"  id="work_experience_todate_'+incwe+'" onclick="return workexchangedatepicker(incwe)" class="form-control datepicker" value=""></div> </div></td><td>'
      + '<input type="text" maxlengt="50" name="palceofposting['+incwe+']" data-toggle="tooltip" id="palceofposting" value="" required="required" class="form-control alphabateonly" data-original-title="" title="">'
      + '</td><td>'
      + '<input type="text" name="last_salary_drawn['+incwe+']" maxlengt="10" data-toggle="tooltip" id="last_salary_drawn" value="" required="required" class="form-control" data-original-title="" title="">'
      + ' </td>'
      + '</tr>';
      adddatepicker();
      $("#bodytblForm12").append(tableData2)

      workexchangedatepicker(incwe);
  }

}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-2>1)
      $('#tbodyForm13 tr:last').remove()
});

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
  }
  insertIdentityRows(rowsIdentityEnter);
    //addDatePicker();
});

  
  var srNoIdentity=0;
  var incr = 0;
  
  function insertIdentityRows(count) {
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      incr++
      cloneRow = lastRow.clone();
      var tableDataIdentity = '<tr>'
      + ' <td>'
      + '<select class="form-control" id="identityname'+incr+'" name="identityname['+incr+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysidentity as $key => $value): ?>
      + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
      <?php endforeach ?>
      + '</select>'

      + ' <td><input type="text" id="identitynumber'+incr+'" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td>'
      + '</tr>';
      $("#tbodyForm13").append(tableDataIdentity)
      adddatepicker();
  }

}
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit")..prop('disabled', true);

});
</script>  

<script type="text/javascript">
  $("#btnGapReasonRemoveRow").click(function() {
    if($('#tblForm15 tr').length-2>1)
      $('#bodytblForm15 tr:last').remove()
});

  $('#btnGapReasonAddRow').click(function() {
    rowsEnter1 = $('#bodytblForm15 tr').length;
    // if (rowsEnter1 < 1) {
    //   alert("Row number must be minimum 1.")
    //   return;
    // }
    InsGapReason(rowsEnter1);
    //addDatePicker();
});

  var srNoGlobal=0;
  var inctg = 0;

  function InsGapReason(count) {
    srNoGlobal = $('#bodytblForm15 tr').length+1;
    var tbody = $('#bodytblForm15');
    var lastRow = $('#bodytblForm15 tr:last');
    var cloneRow = null;
    var inctg = count;
 // alert(count);
 // for (i = 1; i <= count; i++) {
   //inctg++;
   cloneRow = lastRow.clone();
   var tableData15 = '<tr>'
   + '<td><input type="text" class="form-control datepicker" id="gapfromdate_'+inctg+'" '+
   'name="gapfromdate['+inctg+']" onclick="return changedatepicker(inctg)" value="" required="required"/>'
   + '</td><td>'
   + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="gaptodate_'+inctg+'" name="gaptodate['+inctg+']" value="" required="required" />'
   + '</td><td>' 
   + '<textarea class="form-control alphabateonly"  maxlength="250" name="gapreason['+inctg+']" id="gapreason_'+inctg+'" title="Gap Reason" placeholder="Enter gap reason" required="required" ></textarea>'
   + '</td>'
   + '</tr>';
   adddatepicker();
   $("#bodytblForm15").append(tableData15)
   changegapyeardatepicker(inctg);
//}

}
  //insertRows();
</script>


<script type="text/javascript">
  $(document).ready(function(){

   $("#gapfromdate").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
     // alert(selectedDate);
     jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
        // jQuery("#work_experience_todate_2").datepicker( "option", "minDate", selectedDate );
    }
});

   $("#gaptodate").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      alert(selectedDate);
      jQuery("#gapfromdate").datepicker( "option", "maxDate", selectedDate );
  }
});

   decimalData();
   changegapyeardatepicker(inctg)
   $('[data-toggle="tooltip"]').tooltip();   


   $(function () {
    $('#presentcity').keydown(function (e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
      }
  }
});
});
   $(function () {
    $('#permanentcity').keydown(function (e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
      }
  }
});
});

});

  $(document).ready(function(){

      $(".alphabateonly").keypress(function (e){
        var code =e.keyCode || e.which;
        if((code<65 || code>90)
         &&(code<97 || code>122)&&code!=32&&code!=46)  
        {
         alert("Only alphabates are allowed");
         return false;
     }
 });


      $("#filladdress").on("click", function(){

       if (this.checked) { 
        $("#permanentstreet").val($("#presentstreet").val());
        $("#permanentcity").val($("#presentcity").val());
        $("#permanentstateid").val($("#presentstateid").val()); 
        $("#permanentdistrict").val($("#presentdistrict").val());
        $("#permanentpincode").val($("#presentpincode").val()); 
        $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());

    }
    else {
        $("#permanentstreet").val('');
        $("#permanentcity").val('');
        $("#permanentstateid").val(''); 

        $("#permanentdistrict").val('');
        $("#permanentpincode").val('');
        $("#permanentstateid").val('');          
    }
});

  });

  function decimalData(){
      $('.txtNumeric').keypress(function(event) {
       var $this = $(this);
       if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
        ((event.which < 48 || event.which > 57) &&
          (event.which != 0 && event.which != 8))) {
        event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
 }
}, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});



      $('.txtOnly').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
          return true;
      }
      else
      {
          e.preventDefault();
          $('.error').show();
          $('.error').text('Please Enter Alphabate');
          return false;
      }
  });
  }

</script>

<script type="text/javascript">
 $(document).ready(function(){
   var presentstateid =  $("#presentstateid").val();
   $.ajax({
    url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ presentstateid,
    type: 'POST',
    dataType: 'text',
})
   .done(function(data) {
    console.log(data);
    $("#presentdistrict").html(data);
    $("#permanentdistrict").html(data);
})


   var permanentstateid =  $("#permanentstateid").val();
   $.ajax({
    url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ permanentstateid,
    type: 'POST',
    dataType: 'text',
})
   .done(function(data) {

    console.log(data);
    $("#permanentdistrict").html(data);

})

   $("#presentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
  })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

  })
    .fail(function() {
      console.log("error");
  })
    .always(function() {
      console.log("complete");
  });

});

   $("#permanentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
  })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

  })
    .fail(function() {
      console.log("error");
  })
    .always(function() {
      console.log("complete");
  });

});

});     

</script>

<script type="text/javascript">
  function adddatepicker(){

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      jQuery("#todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
        // jQuery("#work_experience_todate_2").datepicker( "option", "minDate", selectedDate );
    }
});

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#work_experience_fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#gapfromdate").datepicker( "option", "maxDate", selectedDate );
        // jQuery("#work_experience_fromdate_1").datepicker( "option", "maxDate", selectedDate );

    }
});

}



function workexchangedatepicker(incwe){

//alert($("#work_experience_fromdate_"+incwe).val());

$("#work_experience_fromdate_"+incwe).datepicker({
  changeMonth: true,
  changeYear: true,
  maxDate: 'today',
  yearRange: '1920:2030',
  dateFormat : 'dd/mm/yy',
  onClose: function(selectedDate) {
   // alert(selectedDate);
   jQuery("#work_experience_todate_"+incwe).datepicker( "option", "minDate", selectedDate );
}
});

$("#work_experience_todate_"+incwe).datepicker({
 changeMonth: true,
 changeYear: true,
 maxDate: 'today',
 yearRange: '1920:2030',
 dateFormat : 'dd/mm/yy',
 onClose: function(selectedDate) {
 // alert(selectedDate);
 jQuery("#work_experience_fromdate_"+incwe).datepicker( "option", "maxDate", selectedDate );
}
});

}

function changedatepicker(inctg){

 $("#fromdate_"+inctg).datepicker({
  changeMonth: true,
  changeYear: true,
  maxDate: 'today',
  yearRange: '1920:2030',
  dateFormat : 'dd/mm/yy',
  onClose: function(selectedDate) {
         // alert(selectedDate);
         jQuery("#todate_"+inctg).datepicker("option", "minDate", selectedDate);
     }
 });

 $("#todate_"+inctg).datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   yearRange: '1920:2030',
   dateFormat : 'dd/mm/yy',
   onClose: function(selectedDate) {
    jQuery("#fromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
}
});
}

function changegapyeardatepicker(inctg){

 $("#gapfromdate_"+inctg).datepicker({
  changeMonth: true,
  changeYear: true,
  maxDate: 'today',
  dateFormat : 'dd/mm/yy',
  yearRange: '1920:2030',
  onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#gaptodate_"+inctg).datepicker("option", "minDate", selectedDate);
      }
  });

 $("#gaptodate_"+inctg).datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   dateFormat : 'dd/mm/yy',
   yearRange: '1920:2030',
   onClose: function(selectedDate) {
    jQuery("#gapfromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
}
});
}
</script>

