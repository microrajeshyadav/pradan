
<style type="text/css">
  /* input[type=text] {
    background: transparent;
    border: none;
    border-bottom: 1px solid #000000;
    }*/
    textarea {
      resize: none;
    }
    hr{
      border-top: 1px solid black;
    }
  </style>
  <section class="content" style="background-color: #FFFFFF;">
    <br/>
    <?php  $page='Narrative'; require_once(APPPATH.'views\Employee_particular_form/topbar.php');?>
    <div class="container-fluid" style="font-family: 'Oxygen' !important;">
      <div class="row text-center" style="padding: 14px;">
        <form method="POST" action="">
          <div class="panel thumbnail shadow-depth-2 listcontainer">
            <div class="panel-heading">
              <div class="row">
                <h4 class="col-md-12 panel-title pull-left">NARRATIVE SELF PROFILE 
                </h4>

              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">
              <?php 
              $tr_msg= $this->session->flashdata('tr_msg');
              $er_msg= $this->session->flashdata('er_msg');

              if(!empty($tr_msg)){ ?>      
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('tr_msg');?>. </div>
                      </div>
                    </div>
                  </div>        
                <?php } else if(!empty($er_msg)){?>        
                  <div class="row">
                    <div class="col-md-12">
                      <div class="hpanel">
                        <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <?php echo $this->session->flashdata('er_msg');?>. </div>
                        </div>
                      </div>
                    </div>          
                  <?php } ?>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
                   <h6>Professional Assistance for Development Action (PRADAN) </h6>

                   <h5>NARRATIVE SELF PROFILE</h5> </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



                     <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                       <div class="form-group">

                        <?php echo form_error("executive_director");?>
                      </div>
                    </div>
                    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    </div>
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="form-group">


                      Name: <b><?php echo $candidatedetails->staff_name; ?></b>&nbsp;<b></b>
                      <br><br>
                      Employee Code:<b><?php echo $candidatedetails->emp_code; ?></b>
                    </div>
                  </div>


                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="form-group">
                     In the space given below, please write 500 words introducing yourself to colleagues in PRADAN. You may tell us about your family, what made you choose the paths you have taken in life, describe your interests, your life until now and any other aspects you consider significant.
                   </div>
                 </div>
                 <!-- <?print_r($datanarrative);?> -->
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                  <b><?php echo $datanarrative->narrative;?></b>

                </div>

                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                 <div class="form-check text-right">
                  <label for="Name">Name:</label>
                  <b><?php echo $emp_code->name; ?></b>
                </div>
              </div>
              &nbsp;&nbsp;&nbsp;

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                <div class="form-check text-right">
                  <label for="Name">Signature</label>
                   <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
                 <!--  <?php if ($signature->encrypted_signature !='') { ?>
                   <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
                 <?php }else{ ?>
                   <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
                 <?php } ?> -->
               </div>
             </div>


             <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && !empty($flags->flag) && $flags->flag==1) {?>

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-top: 20px; ">
                <!--  <h6 class="bg-light">Approved by team codinator </h6> -->

              </div>
              <input type="hidden" id="id" name="id" value="<?php echo $datanarrative->id;?>">  
              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                  Status For Approve : <b><select name="status" class="form-control ">
                   <option value="">Select The Status</option>
                   <option value="2">Approved</option>
                   <option value="3">Reject</option>
                 </select></b>
               </div>
               <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                Reason For Reject: <b><input type="text" name="reject" id="reject" placeholder="Reject Comment" required="required" class="form-control" size="20" style="max-width:150px;"></b>
              </div>

              <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

               Select The Personal: <b> <select name="p_status" class="form-control ">


                 <option value="">Select The Personal</option>
                 <?php foreach($personal as $value){?>
                   <option value="<?php echo $value->staffid;?>"><?php echo $value->Username;?></option>
                 <?php } ?>
               </select></b>
             </div>


           </div>


         <?php } ?>


         <?php if($this->loginData->RoleID==17 && $flags->flag== 2) {?>

           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-top: 20px; ">
             <!-- <h6 class="bg-light">Approved by Personnel</h6> -->

           </div>
           <input type="hidden" id="id" name="id2" value="<?php echo $datanarrative->id;?>">  
           <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

              Status For Approve : <b><select name="status" class="form-control ">
               <option value="">Select The Status</option>
               <option value="4">Approved</option>
               <option value="3">Reject</option>
             </select></b>
           </div>
           <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

         Reason <span style="color: red;"> *</span>: <b><input type="text" required="required" name="reject" id="reject" placeholder="Comment" class="form-control" size="20" style="max-width:150px;"></b>
           </div>




         </div>


       <?php } ?>
       <div class="col-md-12 panel-footer text-right" style="float;left; width: 100%;">
        <?php 
         
        if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $flags->flag==1) {?>
          <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
        <?php } else if($this->loginData->RoleID==17 && $flags->flag==2) {?>
          <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
        <?php } ?>
        <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) &&  ($flags->flag == 2 || $flags->flag == 4)){ ?>
         <h6 class="bg-light text-left">Approved by Team cooidinator/Integrator</h6>
       <?php }?>
       <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) &&  $flags->flag == 3){ ?>
         <h6 class="bg-light text-left">Rejected by Team cooidinator/Integrator</h6>
       <?php }?>
       <?php if($this->loginData->RoleID==17 &&   $flags->flag == 2){ ?>
         <h6 class="bg-light text-left">Approved by Team cooidinator/Integrator</h6>
       <?php }?>
       <?php if($this->loginData->RoleID==17 &&   $flags->flag == 4){ ?>
         <h6 class="bg-light text-left">Approved by Personnel</h6>
       <?php }?>
       <?php if($this->loginData->RoleID==17 &&   $flags->flag == 3){ ?>
         <h6 class="bg-light text-left">Rejected by Personnel</h6>
       <?php }?>
       <a href="<?php echo site_url('staff_dashboard')?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a>

       <?php 
       $staff = $this->uri->segment(3);
       $candidate_id  = $this->uri->segment(4);
       ?>
       <!--  <a  class="btn btn-success btn-sm" href="<?php echo site_url().'Employee_particular_form/getpdfnarrative'.'/'.$staff.'/'. $candidate_id;?>">PDF</a> -->
     </div>
   </div>
 </div>
</div>
</form>
</div>
</div>
</section>


