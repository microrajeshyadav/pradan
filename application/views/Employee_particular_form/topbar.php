  <style type="text/css">

  ul.nav-wizard {
    background-color: #f1f1f1;
    border: 1px solid #d4d4d4;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 3px;
    position: relative;
    overflow: hidden;
  }
  ul.nav-wizard:before {
    position: absolute;
  }
  ul.nav-wizard:after {
    display: block;
    position: absolute;
    left: 0px;
    right: 0px;
    top: 138px;
    height: 47px;
    border-top: 1px solid #d4d4d4;
    border-bottom: 1px solid #d4d4d4;
    z-index: 11;
    content: " ";
  }
  ul.nav-wizard li {
    position: relative;
    float: left;
    height: 46px;
    display: inline-block;
    text-align: center;
    padding: 0 20px 30px;
    margin: 0;
    
    line-height: 36px;
    background-color: #DEE2E6;
  }
  ul.nav-wizard li a {
    color: #428bca;
    padding: 0;
  }
  ul.nav-wizard li a:hover {
    background-color: transparent;
  }
  ul.nav-wizard li:before {
    position: absolute;
    display: block;
    border: 24px solid transparent;
    border-left: 16px solid #d4d4d4;
    border-right: 0;
    top: -1px;
    z-index: 10;
    content: '';
    right: -16px;
  }
  ul.nav-wizard li:after {
    position: absolute;
    display: block;
    border: 24px solid transparent;
    border-left: 16px solid #DEE2E6;
    border-right: 0;
    top: -1px;
    z-index: 10;
    content: '';
    right: -15px;
  }
  ul.nav-wizard li.active {
    color: #fff;
    background: #17A2B8;
  }
  ul.nav-wizard li.active:after {
    border-left: 16px solid #17A2B8;
  }
  ul.nav-wizard li.active a,
  ul.nav-wizard li.active a:active,
  ul.nav-wizard li.active a:visited,
  ul.nav-wizard li.active a:focus {
    color: #fff;
    background: #17A2B8;
  }
  ul.nav-wizard .active ~ li {
    color: #999999;
    background: #f9f9f9;
  }
  ul.nav-wizard .active ~ li:after {
    border-left: 16px solid #f9f9f9;
  }
  ul.nav-wizard .active ~ li a,
  ul.nav-wizard .active ~ li a:active,
  ul.nav-wizard .active ~ li a:visited,
  ul.nav-wizard .active ~ li a:focus {
    color: #999999;
    background: #f9f9f9;
  }
  ul.nav-wizard.nav-wizard-backnav li:hover {
    color: #468847;
    background: #f6fbfd;
  }
  ul.nav-wizard.nav-wizard-backnav li:hover:after {
    border-left: 16px solid #f6fbfd;
  }
  ul.nav-wizard.nav-wizard-backnav li:hover a,
  ul.nav-wizard.nav-wizard-backnav li:hover a:active,
  ul.nav-wizard.nav-wizard-backnav li:hover a:visited,
  ul.nav-wizard.nav-wizard-backnav li:hover a:focus {
    color: #468847;
    background: #f6fbfd;
  }
  ul.nav-wizard.nav-wizard-backnav .active ~ li {
    color: #999999;
    background: #ededed;
  }
  ul.nav-wizard.nav-wizard-backnav .active ~ li:after {
    border-left: 16px solid #ededed;
  }
  ul.nav-wizard.nav-wizard-backnav .active ~ li a,
  ul.nav-wizard.nav-wizard-backnav .active ~ li a:active,
  ul.nav-wizard.nav-wizard-backnav .active ~ li a:visited,
  ul.nav-wizard.nav-wizard-backnav .active ~ li a:focus {
    color: #999999;
    background: #ededed;
  }

</style>


        <?php 

        $topbar = '';
       
          //echo $this->loginData->staffid;
          // $this->loginData->staffid;
       // $staff = $this->session->userdata('staff', $staff);
        //$candidate_id =$this->session->userdata('candidate_id', $candidate_id); 




        //$staff_id = $this->loginData->staffid;
       // $candidateid=$this->loginData->candidateid; 
        //print_r($this->loginData); die;
        if(isset($staff) && isset($candidateid))
        {

          $staff_id=$staff;
        //echo "staff id".$staff_id;
          $candidateid=$candidateid;
        //echo "candate id=".$candidateid;

        }
        else
        {
        @ $staff_id= $staff;
         //echo "staff".$staff;
        @ $candidateid=$candidate_id;
         //Secho "candidateid=".$candidateid;

       }

// echo "dfgdfgfd";
// print_r($topbar);



       ?>
       <div id="exTab1" class="container-fluid">
        <section>
          <div class="wizard" >
            <ul class="nav nav-wizard">
              <li  class="<?php if($page=='employee'){echo 'active';}?>"> 
                <a href="<?php echo site_url("Employee_particular_form/index/$staff_id/$candidateid");?>"> 
                 <?php if(isset($topbar->employ_flag) && $topbar->employ_flag ==0)
                 {
                  echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                }
                elseif(isset($topbar->employ_flag) && $topbar->employ_flag ==1)
                {
                  echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                }
                elseif(isset($topbar->employ_flag) && $topbar->employ_flag==null)
                {
                  echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                } 
                ?>

              Employee Particulars form</a></li>

              <li  class="<?php if($page=='Narrative'){echo 'active';}?>"> 
                <a href="<?php echo site_url("Employee_particular_form/addnarrative/$staff_id/$candidateid");?>"> 
                 <?php if(isset($topbar->employ_flag) && $topbar->narrative_flage ==0)
                 {
                  echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                }
                elseif(isset($topbar->employ_flag) && $topbar->narrative_flage ==1)
                {
                  echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                }
                elseif(isset($topbar->employ_flag) && $topbar->narrative_flage==null)
                {
                  echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                } 
                ?>



              Narrative Self Profile</a></li>




              <li  class="<?php if($page=='Joining'){echo 'active';}?>"> 
                <a href="<?php echo site_url("Joining_report_on_appointment/index/$staff_id/$candidateid");?>">
                 <?php if(isset($topbar->employ_flag) && $topbar->joining_report_flag ==0)
                 {
                  echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                }
                elseif(isset($topbar->employ_flag) && $topbar->joining_report_flag ==1)
                {
                  echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                }
                elseif(isset($topbar->employ_flag) && $topbar->joining_report_flag==null)
                {
                  echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                } 
                ?>


              Joining Report</a></li>

              <li class="<?php if($page=='ID'){echo 'active';}?>"> 
                <a href="<?php echo site_url("Application_from_for_identity_card/index/$staff/$candidateid");?>">
                  <?php if(isset($topbar->employ_flag) && $topbar->identity_flag ==0)
                  {
                    echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                  }
                  elseif(isset($topbar->employ_flag) && $topbar->identity_flag ==1)
                  {
                    echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                  }
                  elseif(isset($topbar->employ_flag) && $topbar->identity_flag==null)
                  {
                    echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                  } 
                  ?>

                Application for ID Card</a></li>


                <li  class="<?php if($page=='provident'){echo 'active';}?>"> 
                  <a href="<?php echo site_url("Provident_fund_nomination_form/index/$staff_id/$candidateid");?>"><i aria-hidden="true"></i> 


                    <?php if(isset($topbar->employ_flag) && $topbar->provident_flag ==0)
                    {
                      echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                    }
                    elseif(isset($topbar->employ_flag) && $topbar->provident_flag ==1)
                    {
                      echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                    }
                    elseif(isset($topbar->employ_flag) && $topbar->provident_flag==null)
                    {
                      echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                    } 
                    ?>



                  PF Nomination Form</a></li>



                  <li  class="<?php if($page=='graduity'){echo 'active';}?>"> 
                    <a href="<?php echo site_url("Gratuity_nomination_form/index/$staff_id/$candidateid");?>"> 
                     <?php if(isset($topbar->employ_flag) && $topbar->graduity_flag ==0)
                     {
                      echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                    }
                    elseif(isset($topbar->employ_flag) && $topbar->graduity_flag ==1)
                    {
                      echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                    }
                    elseif(isset($topbar->employ_flag) && $topbar->graduity_flag==null)
                    {
                      echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                    } 
                    ?>


                  Gratutity Nomination Form</a></li>

                  <li  class="<?php if($page=='Nomination'){echo 'active';}?>"> 
                    <a href="<?php echo site_url("General_nomination_and_authorisation_form_staff/index/$staff_id/$candidateid");?>"> 
                     <?php if(isset($topbar->employ_flag) && $topbar->nomination_flag ==0)
                     {
                      echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                    }
                    elseif(isset($topbar->employ_flag) && $topbar->nomination_flag  ==1)
                    {
                      echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                    }
                    elseif(isset($topbar->employ_flag) && $topbar->nomination_flag ==null)
                    {
                      echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                    } 
                    ?>

                  General Nomination Form</a></li>

                  <li class="<?php if($page=='Application'){echo 'active';}?>"> 
                    <a href="<?php echo site_url("Membership_applicatioin_from/index/$staff_id/$candidateid");?>">
                      <?php if(isset($topbar->employ_flag) && $topbar->membership_flag ==0)
                      {
                        echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                      }
                      elseif(isset($topbar->employ_flag) && $topbar->membership_flag ==1)
                      {
                        echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                      }
                      elseif(isset($topbar->employ_flag) && $topbar->membership_flag==null)
                      {
                        echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                      } 
                      ?>




                    Membership Application Form</a></li>

                    <li class="<?php if($page=='Declaration'){echo 'active';}?>"> 
                      <a href="<?php echo site_url("Membership_applicatioin_from/add/$staff_id/$candidateid");?>">
                        <?php if(isset($topbar->employ_flag) && $topbar->declarte_flag ==0)
                        {
                          echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                        }
                        elseif(isset($topbar->employ_flag) && $topbar->declarte_flag ==1)
                        {
                          echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                        }
                        elseif(isset($topbar->employ_flag) && $topbar->declarte_flag==null)
                        {
                          echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                        } 
                        ?>



                      Declaration about members of family</a></li>

                      <li class="<?php if($page=='Enhanced'){echo 'active';}?>"> 
                        <a href="<?php echo site_url("Membership_applicatioin_from/edit/$staff_id/$candidateid");?>">

                         <?php if(isset($topbar->employ_flag) && $topbar->ehnaced_flag ==0)
                         {
                          echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                        }
                        elseif(isset($topbar->employ_flag) && $topbar->ehnaced_flag ==1)
                        {
                          echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                        }
                        elseif(isset($topbar->employ_flag) && $topbar->ehnaced_flag==null)
                        {
                          echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                        } 
                        ?>




                      GMP Enhanced Option</a></li>

                     

                        <li class="<?php if($page=='Appointment'){ echo 'active';}?>">
                          <a href="<?php echo site_url("Medical_certificate/index/$staff_id/$candidateid");?>">
                            <?php if(isset($topbar->employ_flag) && $topbar->medical_certifacte_flag ==0)
                            {
                              echo '<i class="fa fa-floppy-o" aria-hidden="true"></i>';

                            }
                            elseif(isset($topbar->employ_flag) && $topbar->medical_certifacte_flag ==1)
                            {
                              echo '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>';            
                            }
                            elseif(isset($topbar->employ_flag) && $value->medical_certifacte_flag==null)
                            {
                              echo '<i class="fa fa-file-o" aria-hidden="true"></i>';
                            } 
                            ?>



                          Appointment For Medical Certificate</a></li>
                          <?php //}?>
                        </ul> 
                      </div>
                    </section>
                  </div>




