
<style type="text/css">
  /* input[type=text] {
    background: transparent;
    border: none;
    border-bottom: 1px solid #000000;
    }*/
    textarea {
      resize: none;
    }
    hr{
      border-top: 1px solid black;
    }
  </style>
  <section class="content" style="background-color: #FFFFFF;">
    <br/>
    <?php  $page='Narrative'; require_once(APPPATH.'views/candidate\components\Employee_particular_form\topbar.php');?>
    <div class="container-fluid" style="font-family: 'Oxygen' !important;">
      <div class="row text-center" style="padding: 14px;">
        <form method="POST" action="">
          <div class="panel thumbnail shadow-depth-2 listcontainer">
            <div class="panel-heading">
              <div class="row">
                <h4 class="col-md-12 panel-title pull-left">NARRATIVE SELF PROFILE 
                </h4>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">
              <!-- Exportable Table -->
              <?php 
              $tr_msg= $this->session->flashdata('tr_msg');
              $er_msg= $this->session->flashdata('er_msg');

              if(!empty($tr_msg)){ ?>     

                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('tr_msg');?>. </div>
                    </div>
                  </div>

                <?php } else if(!empty($er_msg)){?>       

                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('er_msg');?>. </div>
                      </div>
                    </div>

                  <?php } ?>       
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">

                   <h6>Professional Assistance for Development Action (PRADAN) </h6>

                   <h5>NARRATIVE SELF PROFILE</h5> </div>
                   

                   <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="form-group">
                      <?php echo form_error("executive_director");?>
                    </div>
                  </div>

                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="form-group">

                    Name: <b><?php echo $emp_code->name; ?></b>&nbsp;<b></b>
                    <br><br> 
                    Emloyee Code:<b><?php echo $emp_code->emp_code; ?></b>
                  </div>
                </div>


                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                 <div class="form-group">
                   In the space given below, please write 500 words introducing yourself to colleagues in PRADAN. You may tell us about your family, what made you choose the paths you have taken in life, describe your interests, your life until now and any other aspects you consider significant.
                 </div>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                 <textarea type="text"   data-toggle="tooltip" name="narratives"  id="narratives" class="form-control alphabateonly" required="required"></textarea>
               </div>

             </div>
             
             <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
               <div class="form-check text-right">
                <label for="Name">Name:</label>
                <b><?php echo $emp_code->name; ?></b>
              </div>
            </div>
            &nbsp;&nbsp;&nbsp;

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
              <div class="form-check text-right">
                <label for="Name">Signature</label>

                 <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
              <!--   <?php if ($signature->encrypted_signature !='') { ?>
                 <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
               <?php }else{ ?>
                 <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
               <?php } ?> -->
             </div>
           </div>

           <div class="col-md-12 panel-footer text-right" style="float;left; width: 100%;">
             <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
             <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
           </div>
         </div>
       </form>
     </div>
   </div>
 </section>



