<section class="content" style="background-color: #FFFFFF;" >
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-8 panel-title pull-left"> </h4>

       <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
      </div>

    </div>
    <hr class="colorgraph"><br>
  </div>
<?php  //print_r($iom_staff_transfer); ?>

  <form name="Iom_transfer" id="Iom_transfer" action="" method="POST">
     <input type="hidden" name="staffid" id="staffid" value="<?php echo  $iom_staff_transfer->staffid; ?>">
     <input type="hidden" name="tarnsid" id="tarnsid" value="<?php echo  $iom_staff_transfer->transid; ?>">
  <div class="panel-body">
    <div class="row"> 
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
<p style="text-align: center;"><strong>Inter-Office Memo (IOM) for Transfer</strong></p>

<p style="text-align: center;"><em>I</em><em>nter-Office Memo</em></p>
<p style="text-align: center;">&nbsp;</p>
<table style="width: 996px; height: 131px;">
<tbody>
  <tr>
<td style="width: 532px;">
<p>Transfer No.: <input type="text" name="transferno" id="transferno"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" maxlength="15" value="<?php echo $iom_staff_transfer->transferno; ?>" required="required"></p>
</td>
<td style="width: 448px;"></td>
</tr>
<tr>
<td style="width: 532px;">
<p>To: <input type="text" name="to" id="to"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->Staffname;?>" required="required"></p>
</td>
<td style="width: 448px;">
<p>Ref: (from Personal Dossier)
<input type="hidden" name="personnel_id" id="personnel_id"   value="<?php echo$this->loginData->staffid;?>">
 <input type="text" name="ref" id="ref"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $personnel_name;?>" required="required"></p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>(Name, Designation of Employee being transferred)</p>
</td>
<td style="width: 448px;">
<p>Date: <input type="text" class="datepicker" name="letter_date" id="letter_date"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($iom_staff_transfer->letter_date);?>" required="required"></p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>From:
<input type="hidden" name="reportingto" id="reportingto" value="<?php echo $getstaffdetails->reportingto;?>">

 <input type="text" name="from" id="from"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->reportingtoname;?>" required="required"></p>
</td>
<td style="width: 448px;">
<p>Copy: See list below</p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>(Integrator/Executive Director)</p>
</td>
<td style="width: 448px;">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Subject: Your Transfer from 
<input type="hidden" name="transfer_from_id" id="transfer_from_id"  value="<?php echo $getstaffdetails->old_office_id;?>">
  <input type="text" name="transfer_from" id="transfer_from"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->oldoffice;?>" required="required"> to 
<input type="hidden" name="transfer_to_id" id="transfer_to_id"  value="<?php echo $getstaffdetails->new_office_id;?>">
  <input type="text" name="transfer_to" id="transfer_to"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->newoffice;?>" required="required">&nbsp;</p>
<p></p>
<p></p>
<p style="text-align: justify;">I write to inform you that you have been transferred from <strong><input type="text" name="transferred_from" id="transferred_from"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->oldoffice;?>" required="required"></strong> to <strong><input type="text" name="transferred_to" id="transferred_to"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->newoffice;?>" required="required"> .</strong> .</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Please hand over charge of your responsibilities on <input type="text" name="charge_responsibility_on" id="charge_responsibility_on"  style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  class="datepicker" value="<?php echo $this->gmodel->changedatedbformate($iom_staff_transfer->responsibility_on_date);?>" required="required"> (date), and report for work at 
<SELECT name="report_for_work_at_place" id="report_for_work_at_place" style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required">
  <option value="" >Select Place</option>
  <?php  foreach ($getofficedetails as $key => $value) {

    if ($value->officeid == $iom_staff_transfer->report_for_work_place) { ?>
   <option value="<?php echo $value->officeid; ?>" SELECTED><?php echo $value->officename; ?></option>
    
  <?php }else{  ?>
  <option value="<?php echo $value->officeid; ?>"><?php echo $value->officename; ?></option>
 <?php  } } ?>
  
</SELECT>  (place) on 
<input type="text" class="datepicker" name="restransferno" id="restransferno" value="<?php echo $this->gmodel->changedatedbformate($iom_staff_transfer->report_for_work_place_on_date);?>" style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"> (date).</p>
<p style="text-align: justify;"></p>
<p style="text-align: justify;">Before proceeding to your new place of posting, you would need to hand over charge of your current responsibilities to

<SELECT name="current_responsibilities_to" id="current_responsibilities_to" style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
  <option value="" >Select Name</option>
  <?php  foreach ($getstafflist as $key => $val) {

 if ($val->staffid == $iom_staff_transfer->current_responsibility_to) { ?>
    ?>
  <option value="<?php echo $val->staffid; ?>" SELECTED><?php echo $val->name; ?></option>
 <?php  }else{ ?>
  <option value="<?php echo $val->staffid; ?>"><?php echo $val->name; ?></option>
<?php } } ?>
  </SELECT> 

 . Please also ensure that you get a &lsquo;Clearance Certificate&rsquo; from all concerned (as per the enclosed proforma).</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">On arrival at your new place of posting, please hand in your joining report in the prescribed form to&nbsp; 
  <SELECT name="joining_report_prescribed_form_to" id="joining_report_prescribed_form_to" style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
  <option value="" >Select Name</option>
  <?php  foreach ($getstafflist as $key => $val) {
    if ($val->staffid == $iom_staff_transfer->joining_report_prescribed_form_to) { ?>
   
  <option value="<?php echo $val->staffid; ?>" SELECTED><?php echo $val->name; ?></option>
 <?php  }else{ ?>
 <option value="<?php echo $val->staffid; ?>"><?php echo $val->name; ?></option>
 <?php }  } ?>
  </SELECT> . Please also ensure that you send a copy to all concerned.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">If necessary, you may take a cash advance from our 

<SELECT name="cash_advance_from_our" id="cash_advance_from_our" style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
  <option value="" >Select Office</option>
  <?php  foreach ($getofficedetails as $key => $value) {
     if ($value->officeid == $iom_staff_transfer->cash_advance_from_our_office) {
    ?>
  <option value="<?php echo $value->officeid;?>" SELECTED><?php echo $value->officename; ?></option>
 <?php  }else{ ?>
  <option value="<?php echo $value->officeid; ?>"><?php echo $value->officename; ?></option>
<?php } } ?>
</SELECT>
  office to meet the travel expenses to join at the new place of your posting as per PRADAN&rsquo;s Travel Rules.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">You may avail of journey and joining time as per PRADAN's travel rules. You would be eligible to claim reimbursement of fare and local conveyance for yourself and your family on account of travel to your new place of work, actual expenses incurred to transport your vehicle and personal effects there and one month&rsquo;s basic pay as lump sum transfer allowance.&nbsp; Please claim these by filling the enclosed &ldquo;Travel Expenses Bill&rdquo; and submitting it to&nbsp; 
<input type="hidden" name="travel_expenses_bill_submitting_to_id" id="travel_expenses_bill_submitting_to_id"   value="<?php echo $getstaffdetails->reportingto;?>" required="required">
  <input type="text" name="travel_expenses_bill_submitting_to_name" id="travel_expenses_bill_submitting_to_name"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->reportingtoname;?>" required="required">( <em>Designation).</em></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><b>List of enclosures:</b></p>
<ol style="text-align: justify;">
<li>Clearance Certificate</li>
<li>Joining Report</li>
<li>Travel Expenses Bill</li>
<li>Handing Over/Taking over Charge</li>
</ol>
<p style="text-align: justify;"><strong>&nbsp;</strong></p>
<p style="text-align: justify;">cc: - Person concerned to whom charge is being given.</p>
<p style="text-align: justify;"> - Team Coordinator/Integrator at the old and new places of posting</p>
<p style="text-align: justify;">- Finance-Personnel-MIS Unit</p>

</div>  
</div>

 <div class="panel-footer text-right">
  <button  type="submit" name="save" id="save" value="SaveDataSend"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>

   <button  type="submit"  name="submit" id="submit" value="SendDataSave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save & Submit</button>
  <a href="<?php echo site_url("Handed_over_taken_over");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
</div>
</form>
</div>
</div>   
</section>

<script>
  function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      return true;
    } else {
      return false;
    }
  }
  $(document).ready(function(){
   $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate : 0,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });
 

   $('[data-toggle="tooltip"]').tooltip();  
   $('#Inboxtable').DataTable(); 
 });


  //insertRows();

  $(document).ready(function () {
    $('#checkBtn').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        alert("You must check at least one checkbox.");
        return false;
      }else{

        if (!confirm('Are you sure?')) {
          return false;
        }

      }


    });
  });


  $("#chooseletter").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getYPRResponse/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#letter").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

</script>
