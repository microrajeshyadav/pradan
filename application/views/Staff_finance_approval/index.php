<section class="content" style="background-color: #FFFFFF;" >
 <br>
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Transfer Approval List</h4>
     </div>
     <hr class="colorgraph"><br>
   </div>

   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');
   if(!empty($tr_msg)){ ?>
   <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="panel-body">

          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" name="tablebootstrap" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Comments</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th>Status</th>
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($getworkflowdetail) ==0) { ?>
             <tbody>
              
             </tbody>
             <?php  }else{  ?>

               <tbody>
                <?php 
                $i=0;
                foreach ($getworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername;?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $value->Requestdate;?></td>
                    <td><?php echo $value->process_type ;?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-sm" title = "History." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                      <?php if ($value->status==5) { ?>
                      <a class="btn btn-warning btn-sm" data-toggle="tooltip" title = "Click here to 
                       inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>
                      <?php } ?>
                      <?php if ($value->status==4 || $value->status==6) { ?>
                      <a class="btn btn-primary btn-sm" data-toggle="tooltip" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>

                      <?php } ?>
                      <?php if ($value->status == 19) { ?>

                      <a class="btn btn-primary btn-sm" data-toggle="tooltip" title = "Click here to approve this request submitted." onclick="approvemodalshowtransfer(<?php echo $value->transid; ?>);"><i class="fas fa-thumbs-up"></i></a>

                      <?php } ?>
                      <?php if ($value->status >= 19) { ?>

                      <a class="btn btn-warning btn-sm" title = "Click here for transfer claim expenses view." data-toggle="tooltip"  href="<?php echo site_url()."transfer_claim_expense/transfer/".$value->transid;?>"> <i class="fas fa-dolly-flatbed"></i>  
                    </a>

                      <?php } ?>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
          </div>
        </div> 
      </div>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left"> Change Responsibility Approval List</h4>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">

        <div class="row" style="background-color: #FFFFFF;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
            <table id="tablecampus1" name="tablebootstrap"  class="table table-bordered table-striped wrapper display">
              <thead>
               <tr>
                 <th style ="max-width:50px;" class="text-center">S. No.</th>
                 <th>Emp code</th>
                 <th>Name</th>
                 <th>Old Office</th>
                 <th>Proposed Transfer Office</th>
                 <th>Proposed Transfer Date</th>
                 <th>Old Designation</th>
                 <th>New Designation</th>
                 <th>Reason</th>
                 <th>Comments</th>
                 <th>Sender Name</th>
                 <th>Request Status</th>
                 <th>Request Date</th>
                 <th>Status</th>
                 <th style ="max-width:50px;" class="text-center">Action</th>
               </tr> 
             </thead>
             <?php if (count($getpromotionworkflowdetail) ==0) { ?>
             <tbody>

             </tbody>
             <?php  }else{  ?>


             <tbody>
              <?php 
              $i=0;
              foreach ($getpromotionworkflowdetail as $key => $value) {
                $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td><?php echo $value->emp_code;?></td>
                  <td><?php echo $value->name;?></td>
                  <td><?php echo $value->officename;?></td>
                  <td><?php echo $value->newoffice;?> </td>
                  <td><?php echo $proposed_date_bdformat;?> </td>
                  <td><?php echo $value->olddesid;?></td>
                  <td><?php echo $value->newdesid;?></td>
                  <td><?php echo $value->reason;?></td>
                  <td><?php echo $value->scomments;?></td>
                  <td><?php echo $value->sendername;?></td>
                  <td><?php echo $value->flag; ?></td>
                  <td><?php echo $value->Requestdate;?></td>
                  <td><?php echo $value->process_type ;?></td>
                  <td class="text-center">
                    <a class="btn btn-success btn-sm" data-toggle="tooltip" title = "History." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                    <?php if ($value->status==5) { ?>
                    <a class="btn btn-warning btn-sm" data-toggle="tooltip" title = "Click here to 
                     inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>
                    <?php } ?>
                    <?php if ($value->status==4 || $value->status==6) { ?>
                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                    <?php } ?>
                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
              <?php } ?>
            </table>
          </div>
        </div>
      </div> 
    </div>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left"> Transfer & Change Responsibility Approval List</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">

      <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus2" name="tablebootstrap" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
               <th style ="max-width:50px;" class="text-center">S. No.</th>
               <th>Emp code</th>
               <th>Name</th>
               <th>Old Office</th>
               <th>Proposed Transfer Office</th>
               <th>Proposed Transfer Date</th>
               <th>Old Designation</th>
               <th>New Designation</th>
               <th>Reason</th>
               <th>Comments</th>
               <th>Sender Name</th>
               <th>Request Status</th>
               <th>Request Date</th>
               <th>Status</th>
               <th style ="max-width:50px;" class="text-center">Action</th>
             </tr> 
           </thead>
           <?php if (count($gettransferworkflowdetail) ==0) { ?>
           <tbody>
            
           </tbody>
           <?php  }else{  ?>


           <tbody>
            <?php 
            $i=0;
            foreach ($gettransferworkflowdetail as $key => $value) {
              $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
              ?>
              <tr>
                <td class="text-center"><?php echo $i+1;?></td>
                <td><?php echo $value->emp_code;?></td>
                <td><?php echo $value->name;?></td>
                <td><?php echo $value->officename;?></td>
                <td><?php echo $value->newoffice;?> </td>
                <td><?php echo $proposed_date_bdformat;?> </td>
                <td><?php echo $value->olddesid;?></td>
                <td><?php echo $value->newdesid;?></td>
                <td><?php echo $value->reason;?></td>
                <td><?php echo $value->scomments;?></td>
                <td><?php echo $value->sendername;?></td>
                <td><?php echo $value->flag; ?></td>
                <td><?php echo $value->Requestdate;?></td>
                <td><?php echo $value->process_type ;?></td>
                <td class="text-center">
                  <a class="btn btn-success btn-sm" data-toggle="tooltip" title = "History." href="<?php echo site_url()."TCRStaff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                  <?php if ($value->status==5) { ?>
                  <a class="btn btn-warning btn-sm" data-toggle="tooltip" title = "Click here to 
                   inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>
                  <?php } ?>
                  <?php if ($value->status==4 || $value->status==6) { ?>
                  <a class="btn btn-primary btn-sm" data-toggle="tooltip" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>

                  <?php } ?>

                  <?php if ($value->status >= 19) { ?>

                  <a class="btn btn-warning btn-sm" data-toggle="tooltip" title = "Click here to transfer claim expance view." href="<?php echo site_url()."transfer_claim_expense/index/".$value->transid;?>"> <i class="fas fa-dolly-flatbed"></i></a>

                  <?php } ?>
                </td>
              </tr>
              <?php $i++; } ?>
            </tbody>
            <?php } ?>
          </table>
        </div>
      </div>
    </div> 
  </div>


  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Sepration Approval List</h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <div class="panel-body">
     <div class="row" style="background-color: #FFFFFF;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
        <table id="tablecampus3" name="tablebootstrap" class="table table-bordered table-striped wrapper">
          <thead>
           <tr>
             <th style ="max-width:50px;" class="text-center">S. No.</th>
             <th>Emp code</th>
             <th>Name</th>
             <th>Old Office</th>
             <th>Proposed Transfer Office</th>
             <th>Proposed Transfer Date</th>
             <th>Reason</th>
             <th>Comments</th>
             <th>Sender Name</th>
             <th>Request Status</th>
             <th>Request Date</th>
             <th>Status</th>
             <th style ="max-width:50px;" class="text-center">Action</th>
           </tr> 
         </thead>
         <?php if (count($getseprationworkflowdetail) ==0) { ?>
             <tbody>
              
             </tbody>
             <?php  }else{  ?>
         <tbody>
          <?php 
          $i=0;
          foreach ($getseprationworkflowdetail as $key => $value) {
            $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
            ?>
            <tr>
              <td class="text-center"><?php echo $i+1;?></td>
              <td><?php echo $value->emp_code;?></td>
              <td><?php echo $value->name;?></td>
              <td><?php echo $value->officename;?></td>
              <td><?php echo $value->newoffice;?> </td>
              <td><?php echo $proposed_date_bdformat;?> </td>
              <td><?php echo $value->reason;?></td>
              <td><?php echo $value->scomments;?></td>
              <td><?php echo $value->sendername;?></td>
              <td><?php echo $value->flag; ?></td>
              <td><?php echo $value->Requestdate;?></td>
              <td><?php echo $value->process_type ;?></td>
              <td class="text-center">
                <!-- <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a> -->

                <?php   if ($value->status>=13) { ?>
               <!--  <a href="<?php //echo base_url('Staff_sepdischargenotice/index/'.$value->transid);?>" data-toggle="tooltip" title = " DISCHARGE NOTICE" class="btn btn-success btn-sm"><i class="fas fa-eject"></i></a>
                <a href="<?php //echo base_url('Staff_sepservicecertificate/index/'.$value->transid);?>" data-toggle="tooltip" title = "SERVICE CERTIFICATE" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i></a -->
                   <a href="<?php echo base_url('Staff_sepchecksheet/index/'.$value->transid);?>" data-toggle="tooltip" title = "Checksheet" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i></a>
                <a href="<?php echo base_url('Staff_sepclearancecertificate/view/'.$value->tbl_clearance_certificate_id);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fas fa-certificate"></i></a>
                <?php if ($value->status==15 && $this->loginData->RoleID==22) { ?>
                 <a class="btn btn-primary btn-sm" data-toggle="tooltip" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                <?php } } ?>
                <?php if ($value->status==27 || $value->status==9) { ?>
                <a class="btn btn-warning btn-sm" data-toggle="tooltip" title = "Click here to 
                CHECK SHEET for transfer submitted." href="<?php echo site_url()."Staff_sepchecksheet/index/".$value->transid;?>"><i class="fa fa-certificate" aria-hidden="true"></i></a>

                <?php } ?>

              </td>
            </tr>
            <?php $i++; } ?>
          </tbody>
          <?php } ?>
        </table>
      </div>
    </div>
  </div> 
</div>
</div>
</section>


<!-- Modal Transfer Expense claim-->
<div class="container">
  <div class="modal fade" id="myModaltransfer" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="<?php echo site_url(); ?>Staff_finance_approval/transferapprovemodal" id="acceptanceform_transfer" name="acceptanceform_transfer" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" id="receiverstaffid_transfer" name="receiverstaffid_transfer" value="">
              <label for="Name">Notes <span style="color: red;" >*</span></label>
              <textarea class="form-control" data-toggle="" id="comments_transfer" name="comments_transfer" placeholder="Enter Notes"> </textarea>
              <?php echo form_error("comments_transfer");?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <label for="Name">Status <span style="color: red;" >*</span></label>

              <select name="status" id="status" class="form-control" required="required"> 
                <option value="">Select</option>

                <option value="21">Approved</option>
                <option value="22">Rejected</option>
                
              </select>
              <?php echo form_error("status");?>
            </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetransfermodal();">
          </div>
        </div>
      </form> 
    </div>
  </div>
</div> 
<!-- Modal -->

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable({rowReorder: {
            selector: 'td:nth-child(2)'
        },responsive: true}); 
    $('#tablecampus1').DataTable({rowReorder: {
            selector: 'td:nth-child(2)'
        },responsive: true}); 
    $('#tablecampus2').DataTable({rowReorder: {
            selector: 'td:nth-child(2)'
        },responsive: true}); 
    $('#tablecampus3').DataTable({rowReorder: {
            selector: 'td:nth-child(2)'
        },responsive: true}); 
  });

  function approvemodalshowtransfer(staffid){
    document.getElementById("receiverstaffid_transfer").value = staffid;
    $('#myModaltransfer').removeClass('fade');
    $("#myModaltransfer").show();
  }
  function acceptancetransfermodal(){
    var comments = document.getElementById('comments_transfer').value;
    var status = document.getElementById('status').value;
    if(comments.trim() == ''){
      $('#comments_transfer').focus();
    }
    if(status == ''){
      $('#status').focus();
    }
    if(status !=""){
      document.forms['acceptanceform_transfer'].submit();
    }
  }
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>