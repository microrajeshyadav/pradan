<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
  

</style>

     <form method="POST" action="" name="acceptance_regination" id="acceptance_regination">
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
   
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION -   <?php if($acceptance_regination_detail->trans_status=='Resign') 

        { echo "ACCEPTANCE OF RESIGNATION";}
        else if($acceptance_regination_detail->trans_status=='Termination') 

        { echo strtoupper("Termination");
        }

       else if($acceptance_regination_detail->trans_status=='Termination during Probation') 

        { echo strtoupper("Termination during Probation");
        }
        else if($acceptance_regination_detail->trans_status=='Retirement') 

        { echo strtoupper("Discharge");
        }
        else if($acceptance_regination_detail->trans_status=='Death') 

        { echo strtoupper("Death");
        }
         else if($acceptance_regination_detail->trans_status=='Desertion cases') 

        { echo strtoupper("Desertion cases");
        }
        else if($acceptance_regination_detail->trans_status=='Super Annuation') 

        { echo strtoupper("Super Annuation");
        }
         else if($acceptance_regination_detail->trans_status=='Discharge simpliciter/ Dismiss') 

        { echo strtoupper("Discharge simpliciter/ Dismiss");
        }
        ?>
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
         
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <h5>Professional Assistance for Development Action (PRADAN)</h5> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 50px;">
        <p><h4>LETTER FOR  <?php 
         if($acceptance_regination_detail->trans_status=='Resign') 

        { echo "ACCEPTANCE OF RESIGNATION";}
        else if($acceptance_regination_detail->trans_status=='Termination') 

        { echo strtoupper("Termination");}

       else if($acceptance_regination_detail->trans_status=='Termination during Probation') 

        { echo strtoupper("Termination during Probation");
        }
        else if($acceptance_regination_detail->trans_status=='Retirement') 

        { echo strtoupper("Discharge");
        }
        else if($acceptance_regination_detail->trans_status=='Death') 

        { echo strtoupper("Death");
        }
         else if($acceptance_regination_detail->trans_status=='Desertion cases') 

        { echo strtoupper("Desertion cases");
        }
        else if($acceptance_regination_detail->trans_status=='Super Annuation') 

        { echo strtoupper("Super Annuation");
        }
         else if($acceptance_regination_detail->trans_status=='Discharge simpliciter/ Dismiss') 

        { echo strtoupper("Discharge simpliciter/ Dismiss");
        }

      ?></h4></p>


      </div> 
    </div>
     
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
       <input type="hidden" name="change_flag" id="change_flag" value="">
    <div class="row">
      <div class="col-md-6 text-left" style="margin-bottom: 20px;">
       301- Employee code/PDR/<label name="employee_code" class="inputborderbelow"> <?php echo $staff_detail->emp_code; ?></label>
     </div>
     <div class="col-md-6 text-right" style="margin-bottom: 20px;">
       Date: <label name="todaydate" class="inputborderbelow"> <?php echo date('d/m/Y')  ?></label>
     </div>
   </div>  
 
 <div class="row" style="line-height: 2">
  <div class="col-md-12">
    Name  of Employee     : <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->name; ?></label> 

  </div>
  <div class="col-md-12">
    Adress   :  <label name="Address" class="inputborderbelow"> <?php echo $staff_detail->address; ?></label>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
   Dear <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->name; ?></label>  ,
 </div>
</div>
<div class="row" style="line-height: 2 ; margin-top: 10px;">


  <div class="col-md-12" style="margin-top: 20px;">
        <?php //echo $acceptance_regination_detail->trans_status;
        //die;
       if($acceptance_regination_detail->trans_status=='Resign') 

        { ?>
           It is with deep regret that I accept your resignation 
        <?php }
       else if($acceptance_regination_detail->trans_status=='Termination') 

        { ?>
        We confirmed your Termination

      <?php   }

       else if($acceptance_regination_detail->trans_status=='Termination during Probation') 

        { ?>
           We confirmed your Termination during Probation
       <?php  }
        else if($acceptance_regination_detail->trans_status=='Retirement') 

        {?>
         We confirmed your Discharged
        <?php }
        else if($acceptance_regination_detail->trans_status=='Death') 

        { ?>

         We confirmed  Mr/Mrs <?php echo $staff_detail->name;?> has separated. 
       <?php
        }
         else if($acceptance_regination_detail->trans_status=='Desertion cases') 

        { ?>

          We confirmed your separation as desertion cases
      <?php   }
      else if($acceptance_regination_detail->trans_status=='Super Annuation') 

        { ?>

          We confirmed your separation as Super Annuation
      <?php   }


else if($acceptance_regination_detail->trans_status=='Discharge simpliciter/ Dismiss') 

        { ?>
        We confirmed your Discharge simpliciter/ Dismissal

      <?php }

    
       
      
     if($acceptance_regination_detail->trans_status!='Death'){?>

   tendered on <input type="text" class="inputborderbelow datepicker" name="resignation_tendered_on" id="resignation_tendered_on" value="<?php if($acceptance_detail){ echo $this->gmodel->changedatedbformate($acceptance_detail->resignation_tendered_on);}?>" required readonly>. Your last working day is <input type="text" name="working_date" id="working_date" class="inputborderbelow datepicker"  value="<?php if($acceptance_detail){ echo $this->gmodel->changedatedbformate($acceptance_detail->working_date);}?>" required readonly>.
  
 </div>

 <div class="col-md-12" style="margin-top: 20px;">
  You shall be relieved of your post subject to your producing a ‘<i>Clearance Certificate</i>’ from all concerned.
</div>
<div class="col-md-12" style="margin-top: 20px;">
 With all my good wishes, 
</div>
 
</div>
<?php
    }

   else 
   {

   } ?>
<div class="row text-left" style="margin-top: 20px;line-height: 3 ">
 <div class="col-md-12 text-right" style="margin-top: 30px;">
   Yours sincerely,
 </div>
 <div class="col-md-12 text-right">
  <?php 
    
    if($workflow_flag->flag==7 ||$workflow_flag->flag==9 ||$workflow_flag->flag==11 || $workflow_flag->flag==13)
    {
   //    if($workflow_flag->flag==21)
   //    {
   //      continue;
   //      echo $ed_details->edname;

   // //    }
   // //     else
   // //     {
   echo $ed_details->edname;
   //  }


 
                           

        
                          if(!empty($ed_details->sign)) {
                              $image_path='';
                           $image_path=FCPATH.'datafiles\signature/'.$ed_details->sign;
                          
                         
                           if(file_exists($image_path)) {
                            
                            



                        ?>
                      <img src="<?php echo site_url().'datafiles\signature/'.$ed_details->sign;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        }
                        else {
                        ?>
                         <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                        <?php } }?>
</div><div class="col-md-12 text-right">
 Executive Director
</div>
</div>
<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
   <i>Copies to:</i>
 </div>
 <div class="col-md-12" style="margin-bottom: 20px; ">
  1. Name of supervisor, Team Coordinator<br>
  2.  Name of Responsible integrator, Integrator <br>
  3.  Finance-Personnel-MIS Unit <br>
</div> 
</div>
</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($acceptance_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <!-- <button type="submit" name="saveandsubmit" id="saveandsubmit" class="btn btn-success btn-sm" onclick="acceptanceregination();" value="Submit"> Save & Submit</button> -->
  <?php
    }else{

      if($acceptance_detail->flag == 0){
 ?>
  <!-- <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
 <!--  <button type="button" name="saveandsubmit" id="saveandsubmit" class="btn btn-success btn-sm"  onclick="acceptanceregination();">Save & Submit</button> -->
<?php } ?>
 
<?php } ?>

<?php if($this->loginData->RoleID != 18){ ?>
 <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }else{ ?>
  <a href="<?php echo site_url("Ed_staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } ?>
</div><!-- 
</form> -->
</div>
</div>
</section>

<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->

      <!-- <form method="POST" name="acceptance_regination_modal" id="acceptance_regination_modal"> -->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes" style="max-width: 400px;" maxlength="250" > </textarea>
              <?php echo form_error("comments");?>
              <label for="Name">Sent To ED</label>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="button" name="saveandsubmit" value="save & submit" class="btn btn-success btn-sm" onclick="acceptancereginationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
<script type="text/javascript">
  <?php if($acceptance_regination_detail->trans_status!='Death'){?>
  function acceptanceregination(){
    var resignation_tendered_on = document.getElementById('resignation_tendered_on').value;
    var working_date = document.getElementById('working_date').value;
    if(working_date == ''){
      $('#working_date').focus();
    }
    if(resignation_tendered_on == ''){
      $('#resignation_tendered_on').focus();
    }

    if(working_date !='' && resignation_tendered_on !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
<?php }else{ ?>
  function acceptanceregination(){    
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
<?php } ?>
  function acceptancereginationmodal(){
    var dd=1;
    $("#change_flag").val(dd);
    var comments = document.getElementById('comments').value;

    if(comments.trim() == ''){
      $('#comments').focus();
    }else{
      //document.getElementById("acceptance_regination_modal").submit();
      document.getElementById("acceptance_regination").submit();
    }
  }
</script>
<?php 
if ($acceptance_detail){
if($acceptance_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  });
</script>
<?php }} ?>
<script>

  
  /*$(function() {
    $("#acceptance_regination_modal").bootstrapValidator({

    alert('sjkhdfdksj');
      rules: {
        comments: {
          required: true,
          minlength: 8
        },
        action: "required"
      },
      messages: {
        comments: {
          required: "Please enter some data",
          minlength: "Your data must be at least 8 characters"
        },
        action: "Please provide some data"
      }
    });
  });*/
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
   });

  });

</script>

