<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - ACCEPTANCE OF RESIGNATION
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <h5>Professional Assistance for Development Action (PRADAN)</h5> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 50px;">
        <p><h4> LETTER FOR ACCEPTANCE OF RESIGNATION</h4></p>
      </div> 
    </div>
    <div class="row">
      <div class="col-md-6 text-left" style="margin-bottom: 20px;">
       301- Employee code/PDR/________________ 
     </div>
     <div class="col-md-6 text-right" style="margin-bottom: 20px;">
       Date: _______________________
     </div>
   </div>  
 
 <div class="row" style="line-height: 2">
  <div class="col-md-12">
    Name  of Employee     : ______________________________
  </div>
  <div class="col-md-12">
    Adress   : ______________
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
   Dear ____________________,
 </div>
</div>
<div class="row" style="line-height: 2 ; margin-top: 10px;">
  <div class="col-md-12" style="margin-top: 20px;">
   It is with deep regret that I accept your resignation tendered on <u>June 20, 2018</u>. Your last working day is <u>July 28, 2018</u>.
 </div>
 <div class="col-md-12" style="margin-top: 20px;">
  You shall be relieved of your post subject to your producing a ‘Clearance Certificate’ from all concerned.
</div>
<div class="col-md-12" style="margin-top: 20px;">
 With all my good wishes, 
</div>
</div>
<div class="row text-left" style="margin-top: 20px;line-height: 3 ">
 <div class="col-md-12 text-right" style="margin-top: 30px;">
   Yours sincerely,
 </div>
 <div class="col-md-12 text-right">
  _________________________________
</div><div class="col-md-12 text-right">
 Executive Director
</div>
</div>
<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
   <i>Copies to:</i>
 </div>
 <div class="col-md-12" style="margin-bottom: 20px; ">
  1. Name of supervisor, Team Coordinator<br>
  2.  Name of Responsible integrator, Integrator <br>
  3.  Finance-Personnel-MIS Unit <br>
</div> 
</div>
</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>