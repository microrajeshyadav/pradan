<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left"> Letter Informing Employee About Extension of Period of Probation</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row" style="line-height: 3">
        <div class="col-md-6"> 
          Ref: Personal Dossier of Employee
        </div>
        <div class="col-md-6 pull-right text-right"> 
          Date: _______________________
        </div>
      </div>
      <div class="row" style="line-height: 3; margin-top:50px;">
        <div class="col-md-12"> 
          To
        </div>
        <div class="col-md-12"> 
          Name of Probationer: ____________________________
        </div>

        <div class="col-md-12"> Name of Probationer: ____________________________</div>
        <div class="col-md-12"> Employee Code: __________</div>
        <div class="col-md-12"> Designation: _____________________________</div>
        <div class="col-md-12"> Location: ________________________</div>
        
        <div class="col-md-12 text-center" style="margin-top: 50px;"> <h5>Through the Supervisor<h5></div>
          <div class="col-md-12 text-left" style="margin-top: 50px;">Subject: <strong>Extension of Period of Probation</strong></div>
          <div class="col-md-12">Dear ___________________,</div>
          <br/>
          <div class="col-md-12">
            <p>You were appointed in PRADAN as ___________________ (designation) with effect from ____________ (date). As per the terms and conditions of your appointment in PRADAN, you were to be on probation up to ____________ (date).</p>
          </div>
          <div class="col-md-12">
            <p>Based on the review of your performance during above period of six months, we have decided to extend the period of your probation for a period of&nbsp; ______ (months).&nbsp; We hope that this will provide you adequate opportunity to enhance your performance.</p>
          </div>
          <div class="col-md-12">
            <p>The period of your probation is therefore extended up to and including _________ (date).&nbsp; I hope you would view this as a challenge, and give your fullest to your work in the coming few months.</p>
          </div> <br/>
          <div class="col-md-12">Yours sincerely,</div>          
          <div class="col-md-12">(__________________)</div>
          <div class="col-md-12">Executive Director</div>

          <div class="col-md-12">cc: - Team Coordinator</div>
          <div class="col-md-12">- Integrator</div>
          <div class="col-md-12">- Finance-Personnel-MIS Unit</div> 
        </div>
      </div>
       <div class="panel-footer text-right">
        <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
        <a href="" class="btn btn-dark btn-sm"> Go Back</a>
      </div>
    </div>
  </div>
</section>