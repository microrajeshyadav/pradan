<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>

       <div class="container-fluid" style="margin-top: 20px;">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

         <form name="campus" action="" method="post" > 
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-8 panel-title pull-left">Edit Campus</h4>
                 <div class="col-md-4 text-right" style="color: red">
                  * Denotes Required Field 
                </div>
              </div>
              <hr class="colorgraph"><br>
            </div>

            <div class="panel-body"> 
              <input type="hidden" name="campus_id" id="campus_id" value="<?php echo $campusupdate[0]->campusid;?>">
              <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Campus Name <span style="color: red;" >*</span></label>
                <input  type="text" class="form-control alphabateonly" data-toggle="" id="campusname" name="campusname" placeholder="Enter Campus Name " value="<?php echo ucfirst($campusupdate[0]->campusname);?>" >
                <?php echo form_error("campusname");?>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Contact Person 1<span style="color: red;" >*</span></label>
                <input  type="text" name="campusincharge" data-toggle="" minlength="10" maxlength="50" class="form-control alphabateonly" value="<?php echo $campusupdate[0]->campusincharge;?>" placeholder="Enter Campus  Incharge" >
                <?php echo form_error("campusincharge");?>
              </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Contact Person 2</label>
                <input  type="text" name="campusincharge2" data-toggle="" minlength="10" maxlength="50" class="form-control alphabateonly" value="<?php echo $campusupdate[0]->campusincharge2;?>" placeholder="Enter Campus  Incharge" >
                <?php echo form_error("campusincharge2");?>
              </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Address  <span style="color: red;" >*</span></label>
                <input  name="address" minlength="10" maxlength="150" id="address" class="form-control" value="<?php echo $campusupdate[0]->address;?>">
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>
                <input  type="email" minlength="10" maxlength="80"  data-toggle="" name="emailid" id="emailid" class="form-control" value="<?php echo $campusupdate[0]->emailid;?>" placeholder="Enter Email Id" onblur="email_editexists()" onchange="email_editexists()">
                 <span style="color: red;" id="email_error"></span>
                <?php echo form_error("emailid");?>
              </div>   
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Email Id 2<span style="color: red;" ></span></label>
                <input  type="email" minlength="10" maxlength="80"  data-toggle="" name="emailid2" id="emailid2" class="form-control" value="<?php echo $campusupdate[0]->emailid2;?>" placeholder="Enter Email Id" >
                <?php echo form_error("emailid2");?>
              </div>               
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">State <span style="color: red;" >*</span> </label>
                <select name="stateid" id="stateid" class="form-control" data-toggle="" >
                  <option value="">Select State</option>
                  <?php  foreach ($statedetails as $key => $value) { 
                   if ($value->statecode ==$campusupdate[0]->stateid) {
                     ?>
                     <option value="<?php echo $value->statecode;?>" SELECTED><?php echo $value->name;?></option>
                     <?php  }else { ?>
                     <option value="<?php echo $value->statecode;?>"><?php echo $value->name;?></option>
                     <?php } } ?>
                   </select>
                   <?php echo form_error("stateid");?>
                 </div>
              
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Mobile No 1. <span style="color: red;" >*</span></label>
                  <input  type="text" minlength="10" maxlength="10" data-toggle="" name="mobile1" id="mobile1" class="form-control txtNumeric" data-country="India" value="<?php echo $campusupdate[0]->mobile;?>" placeholder="Enter Mobile No">
                  <?php echo form_error("mobile1");?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Mobile No 2.</label>
                  <input  type="text" minlength="10" maxlength="10" data-toggle="" name="mobile2" id="mobile2" class="form-control txtNumeric" data-country="India" value="<?php echo $campusupdate[0]->mobile2;?>" placeholder="Enter Mobile No">
                </div>
           

                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">City <span style="color: red;" >*</span></label>
                <input  type="text"  minlength="4" maxlength="50"  data-toggle="" name="city" id="telephone" class="form-control" value="<?php echo $campusupdate[0]->city;?>" placeholder="Enter Telephone No " >
                <?php echo form_error("city");?>
              </div>

                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Telephone No.</label>
                  <input  type="text" minlength="12" maxlength="12" data-toggle="" name="telephone" id="telephone" class="form-control txtNumeric" value="<?php echo $campusupdate[0]->telephone;?>" placeholder="Enter Telephone No " >
                  <?php echo form_error("telephone");?>
                </div>                 
               
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Fax No.</label>
                  <input  type="text" minlength="12" maxlength="12" data-toggle="" name="fax" id="fax" class="form-control txtNumeric" value="<?php echo $campusupdate[0]->fax;?>" placeholder="Enter Fax No " >
                  <?php echo form_error("fax");?>
                </div>

              </div>
            </div>
            <div class="panel-footer text-right">
             <button type="submit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
             <a href="<?php echo site_url("campus");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
           </div>
         </div><!-- /.panel-->

       </form> 
       
     </div>
     <!-- #END# Exportable Table -->

   </div>
   <?php }  } ?>
 </section>
 <script type="text/javascript">
  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
   }
 }, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});

  $(".alphabateonly").keypress(function (e){
      var code =e.keyCode || e.which;
      if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46)  
      {
       alert("Only alphabates are allowed");
       return false;
     }
   });

</script>

<script>
  function email_editexists() {
    var email=$("#emailid").val();
    var campus_id=$("#campus_id").val();

    $.ajax({
      data:{email:email,campus_id:campus_id},
      url: '<?php echo site_url(); ?>Ajax/email_editexists/',
      
      type: 'POST',
      
    })
    .done(function(data) {
      //alert(data);
      if(data>0)
      {
        $("#email_error").text("email id allready exist, please choose other email id");
        $("#emailid").val('');
        $("#emailid").focusin();


      }
      else
      {
        $("#email_error").text(" ");
      }
      

    })

    


  }
  function email_focus()
  {
    var email=$("#emailid").val();
    
    if(email=='')
    {
      $("#emailid").focus();
    }
  }
</script>
