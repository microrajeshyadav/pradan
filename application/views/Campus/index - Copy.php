<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">

    <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Campus List</b>
        <div class="pull-right">
          <a href="<?php echo site_url()."Campus/add";?>" class="btn btn-xs btn-primary">Add New Campus</a>


        </div>
      </div>
      <div class="panel-body">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>



              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablecampus" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>State</th>
                       <th>City</th>
                       <th>Campus</th>
                       <th>Email Id</th>
                       <th>Mobile No.</th>
                       <th>Campus incharge</th>
                       <th style ="max-width:50px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                   <tbody>

                    <?php 
                    $i=0;
                    foreach ($campuslist as $key => $value) {
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->name;?></td>
                      <td><?php echo $value->city;?></td>
                      <td><?php echo $value->campusname;?> </td>
                      <td><?php echo $value->emailid;?> </td>
                      <td><?php echo $value->mobile;?></td>
                      <td><?php echo $value->campusincharge ;?> </td>
                      <td class="text-center"><a title = "Click here to edit this Campus." href="<?php echo site_url()."Campus/edit/".$value->campusid;?>" ><span class="glyphicon glyphicon-pencil linkdesaible" style="font-size : 15px; margin-top: 8px;" id="usedbatchid"></span></a> | <a title = "Click here to delete this Campus." href="<?php echo site_url()."Campus/delete/".$value->campusid;?>" onclick="return confirm_delete()"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a></td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div> 
          </div>
        </div>
      </div>   
      <?php } } ?>
    </section>

    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
      });
    </script>  


    <script type="text/javascript">
     $(document).ready(function(){
      $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
    });
  </script>
  <script type="text/javascript">
    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>