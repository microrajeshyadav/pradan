
<br>
<div class="container-fluid">
  <div class="panel panel-default" >
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Candidate Rejection </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <?php 
     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');

     if(!empty($tr_msg)){ ?>
     <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php //echo "<pre>"; print_r($selectedcandidatedetails); ?>
          <?php echo form_open();?>
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white;">
             <table id="Inboxtable" class="table table-bordered table-striped table-hover dataTable js-exportable">
              <thead>
               <tr>
                <th  class="text-center" style ="max-width:50px;" >S. No.</th>
                <th>Category</th>
                <th>Campus Name</th>
                <th> Name</th>
                <th>Gender</th>
                <th> Email Id</th>
                <th> Team </th>
                <?php if ($this->loginData->RoleID !=17) { ?>
                <th> FG </th>
                <th> Batch </th>
                <?php } ?>
                <th> ED Comments</th>
                <th> Offer Letter </th>
                <th style ="max-width:100px;" class="text-center"> Offer Letter </th>
                <th>Reject</th>
                <th>Comments for Rejection</th>
              </tr> 
            </thead>
            <?php if (count($selectedcandidatedetails) == 0) { ?>
            <tbody>
              <tr>
                <td colspan="14" style="color: red;">Record not found</td>
              </tr>
            </tbody>
            <?php }else{?>
            <tbody>

              <?php $i=0; 
            // print_r($selectedcandidatedetails); die;
              foreach ($selectedcandidatedetails as $key => $value) {
                ?>
                <tr>
                  <td  class="text-center"><?php echo $i+1; ?></td>
                  <td><?php echo $value->categoryname;?> </td>
                  <td><?php echo $value->campusname;?></td>
                  <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
                  <td><?php echo $value->gender;?></td>
                  <td ><?php echo $value->emailid;?> </td>
                  <td ><?php echo $value->officename;?></td>
                  <?php if ($this->loginData->RoleID !=17) { ?>
                  <td ><?php echo $value->name;?></td>
                  <td ><?php echo $value->batch;?></td>
                  <?php } ?>
                  <td ><?php if ($value->status ==0) { ?>
                   <span class="badge badge-pill badge-success">Approve</span>
                   <?php   }else{ ?>
                   <span class="badge badge-danger">Rejected</span>
                   <?php  } ?></td>

                   <td class="text-center">
                    <a href="<?php echo site_url("pdf_offerletters").'/'.$value->filename.'.pdf';?>" target="_blank" data-toggle="tooltip" title="Download Offer letter" style="vertical-align: top;">
                      <i class="fa fa-download fx-2" aria-hidden="true"></i>
                    </a>
                    <?php if(!empty($value->otherfiles)){
                      $otherfiles_array = explode(',', $value->otherfiles);
                      foreach($otherfiles_array as $res){
                        ?>
                        <a href="<?php echo site_url("pdf_offerletters").'/'.$res;?>" target="_blank" data-toggle="tooltip" title="Download Offer letter" style="vertical-align: top;">
                          <i class="fa fa-download fx-2" aria-hidden="true"></i>
                        </a>
                        <?php } } ?>
                      </td>

                      <?php if ($value->flag=='Send') { ?>
                      <td class="text-center"> <span class="badge badge-success">Offer Letter Sent</span> </td>
                      <?php } else if($value->status == 1){ ?>
                      <td  class="text-center">  <span class="badge badge-danger">Can`t Offer Letter </span> </td>
                      <?php }else{ ?>
                      <td class="text-center">
                        <!-- href="<?php echo site_url().'Hrddashboard/sendofferlettertocandidate/'.$value->candidateid; ?>" data-toggle="modal" data-target="#myModal" -->
                        <a class="btn btn-primary btn-xs m-t-10 " title="Send to Candidate" onclick="others_document(<?php echo $value->candidateid; ?>);">Send</a>
                      </td>
                      <?php } ?>
                      <td>
                        <a class="btn btn-danger btn-xs m-t-10 " title="Send to Candidate" onclick="reject(<?php echo $value->candidateid; ?>);">Click Here</a>


                        <!-- <input type="button" class="btn btn-danger btn-xs"  id="reject" onclick=" reject(<?php echo $value->candidateid; ?>);"  value = "Click Here"> -->
                        <input type="hidden" name="cd" value="<?php echo $value->candidateid; ?>">
                      </td>
                      <td>
                        <input type="text"  name="rejectioncomment_<?php echo $value->candidateid;?>" id="rejectioncomment_<?php echo $value->candidateid;?>" value=""></td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                    <?php } ?>
                  </table>
                </div>
              </div> 
              <?php echo form_close();?>
            </div>
          </div>
        </div>  

        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
            <form method="post" action="<?php echo site_url().'Hrddashboard/sendofferlettertocandidate/'; ?>" enctype="multipart/form-data">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
           <!--  <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">
            <p>Select Documents.</p>
            <input type="hidden" name="token" id="token" value="">
            <input type="file" name="others_document[]" id="others_document"  accept=".doc, .docx, .txt,.pdf" multiple required>
          </div>
          <div class="modal-footer">
            <button type="Submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div> 

  <!--- Modal for appointment letter Others document -->
  <div class="modal fade" id="myModalAppointment" role="dialog">
    <div class="modal-dialog">
      <form method="post" action="<?php echo site_url().'Hrddashboard/sendappointmentlettertocandidate/'; ?>" enctype="multipart/form-data">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
           <!--  <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">
            <p>Select Documents.</p>
            <input type="hidden" name="staffid" id="staffid" value="">
            <input type="file" name="appointment_others_document[]" id="appointment_others_document"  accept=".doc, .docx, .txt,.pdf" multiple required>
          </div>
          <div class="modal-footer">
            <button type="Submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable();   
      $('#InboxtableAppointment').DataTable(); 
    });

    function others_document(candidateid){
      $('#token').val(candidateid);
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
    function appiontment_document(staffid){
      $('#staffid').val(staffid);
      $('#myModalAppointment').removeClass('fade');
      $("#myModalAppointment").show();
    }

    function confirm_delete() {

      var r = confirm("Are you sure you want to delete this State ?");

      if (r == true) {
        return true;
      } else {
        return false;
      }

    }


  </script>  

  <script type="text/javascript">
   function reject(candidateid){
    ConfirmDialog('Are you sure to reject selected candidate ? <br/> <br/> You can also provide comments for Rejection in right side input.',candidateid);



   }

   function ConfirmDialog(message,candidateid) {
    $('<div></div>').appendTo('body')
    .html('<div><h6>' + message + '?</h6></div>')
    .dialog({
      modal: true,
      title: 'Confirmation Message',
      zIndex: 10000,
      autoOpen: true,
      width: 'auto',
      resizable: false,
      buttons: {
        Yes: function() {
          // $(obj).removeAttr('onclick');                                
          // $(obj).parents('.Parent').remove();
          var comment = document.getElementById("rejectioncomment_"+candidateid).value;
      
      $.ajax({
        type: "POST",
        url: '<?php echo site_url('Rejectedcandidate/rejected/');?>',
        data: {candidateid:candidateid, comment:comment },
        success: function(data){
          
          window.location.reload();
        }
      });
      $(this).dialog("close");

     
    },
    No: function() {


      $(this).dialog("close");
    }
  },
  close: function(event, ui) {
    $(this).remove();
  }
});
  }
</script>