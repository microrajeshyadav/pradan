
<br/>
<div class="container-fluid"  style="font-family: 'Oxygen' !important;" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">DA Document Verification</h4>
       

     </div>
     <hr class="colorgraph"><br>
   </div>

   <div class="panel-body">


    <div class="body">
      <form name="joinDA" action="" method="post">
        <div class="row">
          <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">DA Name :</label></div>
          <div class="col-lg-4">
            <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
            <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
          </div>
          <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
          <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control" readonly="readonly"></div>
          <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
          <div class="col-lg-1" id="candidateprofilepic" >

                <?php  

                        if($getcandateverified->encryptedphotoname)
                              {

                           $file_path=''; 
                            $file_path=FCPATH.'datafiles/'.$getcandateverified->encryptedphotoname;

                            
                            if(file_exists($file_path))
                           
                         { ?>


            <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" class= "rounded-circle" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px">
            <?php } else{

              ?>
              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
              <?php } 
            }

              else

            {
            ?>
               <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
               <?php
                }

            ?>

          </div>
          </div>
          <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-4"></div>
            <div class="col-lg-4"></div>
            <div class="col-lg-2"></div>
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <table id="tblForm09" class="table table-bordered dt-responsive table-striped" >
              <thead>
               <tr class="bg-light">
                <th >
                  Documents to be verified 
                </th>
                <th  >
                  Certificate
                </th>
                <th  >
                  Verification
                </th>
              </tr>
            </thead> 

            <tr style="background-color: #fff;">
              <td>10th Standard</td>
              <td style="width: 100px; text-align: center;">
                <?php 
                  if (!empty($getcandateverified->encryptmetriccertificate)) {


                  $matriccertificateArray = explode("|",$getcandateverified->encryptmetriccertificate);
                  $image_path='';

                  foreach($matriccertificateArray as $res)
                  {
                  $image_path='datafiles\educationalcertificate/'.$res;

                  if (file_exists($image_path)) {

                ?>
                <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"></i></a>
              <?php } } } ?>
              </td>
               <td style="width: 100px; text-align: center;">

                <label class="form-check-label" for="metric_certificate_verified">verified</label>

              </td> 
            </tr>
            <tr style="background-color: #fff;">
              <td>12th Standard</td>
              <td style="width: 100px; text-align: center;">
                <?php if (!empty($getcandateverified->encrypthsccertificate)) 
                  {
                  $hsccertificateArray = explode("|",$getcandateverified->encrypthsccertificate);
                  $image_path='';

                  foreach($hsccertificateArray as $res)
                  {
                  $image_path='datafiles\educationalcertificate/'.$res;

                  if (file_exists($image_path)) {
                ?>
                <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
              <?php } } } ?>
              </td>
              <td style="width: 100px; text-align: center;">

                <label class="form-check-label" for="hsc_certificate_verified">verified</label>

              </td>
            </tr>
            <tr style="background-color: #fff;">
              <td>Graduation
                        <?php 
                        if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) { ?> 
                        <br>Migration Marks Sheet <?php 
                        if($getcandateverified->ugmigration_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) { ?> 
                        <br>Migration Cerificate <?php 
                        if($getcandateverified->ugdoc_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);?>)<?php } } ?>
             </td>
              <td style=" text-align: center;">
                          <?php if (!empty($getcandateverified->encryptugcertificate)) { 

                         $ugcertificateArray = explode("|",$getcandateverified->encryptugcertificate);
                            $image_path='';

                            foreach($ugcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugmigration_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);} ?>
                        <br>
                        Migration Marks Sheet 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>




                      <?php if (!empty($getcandateverified->ugdocencryptedImage)) { 

                         $ugdocArray = explode("|",$getcandateverified->ugdocencryptedImage);
                            $image_path='';

                            foreach($ugdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugdoc_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);} ?>
                        <br>
                        Migration Certificate 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>  
                          </td>
              <td style="width: 100px; text-align: center;">

                <label class="form-check-label" for="ug_certificate_verified">verified</label>

              </td>

            </tr>
            <?php if (!empty($getcandateverified->encryptpgcertificate)) {?>  
              <tr style="background-color: #fff;">
                <td>Post Graduation
                        <?php 
                        if (!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?><br> Migration Marks Sheet<?php 
                          if($getcandateverified->pgmigration_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?><br> Migration Certificate<?php 
                          if($getcandateverified->pgdoc_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);?>)<?php } } ?>
                     </td>
                <td style="width: 100px; text-align: center;">
                             <?php if (!empty($getcandateverified->encryptpgcertificate)) {

                          $pgcertificateArray = explode("|",$getcandateverified->encryptpgcertificate);
                            $image_path='';

                            foreach($pgcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgmigration_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);} ?>
                            <br>
                            Migration Marks Sheet 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>



                        <?php if (!empty($getcandateverified->pgdocencryptedImage)) {

                          $pgdocArray = explode("|",$getcandateverified->pgdocencryptedImage);
                            $image_path='';

                            foreach($pgdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgdoc_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);} ?>
                            <br>
                            Migration Certificate 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>
                              </td>
                <td style="width: 100px; text-align: center;">

                  <label class="form-check-label" for="pg_certificate_verified">verified</label>

                </td>
              </tr>
            <?php }  ?>




            <?php   if ($getgapyearcount > 0) {
                          
                           foreach ($getgapyearpverified as $key => $value) {


                            ?> 
              <tr style="background-color: #fff;">
                <td>Gap Year Document</td>
                <td style="width: 100px; text-align: center;"><a href="<?php echo site_url().'datafiles/gapyeardocument/'.$value->encrypteddocumentsname;?>" download ><i class="fa fa-download" ></i></a> </td>
                <td style="width: 100px; text-align: center;">
                  <label class="form-check-label" for="gapyear_certificate_verified">verified</label>
                </td>
              </tr>
            <?php } } ?>



            

            <?php if (!empty($getcandateverified->encryptofferlettername)) {?>  
              <tr style="background-color: #fff;">
                <td>Signed Offer Letter</td>
                <td style="width: 100px; text-align: center;"><a href="<?php echo site_url().'pdf_offerletters/'.$getcandateverified->encryptofferlettername;?>" download ><i class="fa fa-download" ></i></a> </td>
                <td style="width: 100px; text-align: center;">
                  <label class="form-check-label" for="signed_certificate_verified">verified</label>
                </td>
              </tr>
            <?php }  ?>
            
            <?php if (!empty($getcandateverified->encryptothercertificate)) {?>  
              <tr style="background-color: #fff;">
                <td>Others</td>
                <td style="width: 100px; text-align: center;"><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptothercertificate;?>" download ><i class="fa fa-download" ></i></a> </td>
                <td style="width: 100px; text-align: center;">
                  <label class="form-check-label" for="pg_certificate_verified">verified</label>
                </td>
              </tr>
            <?php }  ?>

            <?php if ($getworkexpcount > 0) {
              foreach ($getworkexpverified as $key => $value) {
                ?>
                <tr style="background-color: #fff;">
                  <td>Work Experience Certificate</td>
                  <td style="width: 100px; text-align: center;"><a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download ><i class="fa fa-download" ></i></a> </td>
                  <td style="width: 100px; text-align: center;">

                    <label class="form-check-label" for="workexp_certificate_verified">verified</label>

                  </td>
                </tr>
              <?php } }  ?>
            </table>
          </div>
        </div>

        <?php if($getcandateverified->categoryid==1){?>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <table id="tblForm13" class="table table-bordered dt-responsive table-striped" >
            <thead>
             <tr class="bg-light">
              <th >Form </th>

              <th style="text-align: center;">PDF</th>

              <th >Verification</th>
            </tr> 
          </thead>
          <tbody >
            <tr>
              <td>General Nomination form</td>
              <td style="width: 100px; text-align: center;"> <a href="<?php echo site_url().'pdf_generalnominationform/'.$getgeneralnominationformpdf->filename;?>" download ><i class="fa fa-download" ></i></a></td>
              <td style="width: 100px; text-align: center;"> 
                <label class="form-check-label" for="general_nomination_form" >verified</label>
              </td>
            </tr>
            <tr>
              <td>Joining report for DAs</td>
              <td style="width: 100px; text-align: center;"> <a href="<?php echo site_url().'pdf_joiningreportforda/'.$getjoiningreportstatus->filename;?>" download ><i class="fa fa-download" ></i></a></td>
              <td style="width: 100px; text-align: center;"> 
                <label class="form-check-label" for="joining_report_da">verified</label>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <?php if (isset($countotherdoc) && $countotherdoc >0) { ?>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="" class="table table-bordered dt-responsive table-striped" >
          <thead>

            <tr class="bg-light">

             <th >Document name</th>
             <th style=" text-align: center;">Document for</th>
             <th style=" width: 100px; text-align: center;">PDF</th>
             <th style=" width: 100px; text-align: center;">Verification </th>
           </tr> 
         </thead>
         <tbody id="tbodyForm09">
           <input type="hidden" name="otherdocumentcount" id="otherdocumentcount" value="<?php echo $countotherdoc;?>">
           <?php 
           $i=0;
           foreach ($getotherdocdetails as $key => $val) {
            ?>
            <tr id="tbodyForm09" style="background-color: #fff;">

              <td><?php echo $val->document_name; ?></td>
              <td style="width: 200px; text-align: center;"><?php echo $val->document_type; ?></td>
              <td style="width: 100px; text-align: center;">
                <a href="<?php echo site_url().'otherdocuments/'.$val->documentupload;?>" download ><i class="fa fa-download" ></i></a>               
              </td>
              <td style="width: 100px;    ">
               <div class="form-check">
              <!--  <input type="hidden" name="documentid[]" id="documentid<?php //echo $i+1; ?>" value="<?php //echo $val->id;?>">
               <input type="checkbox" class="filled-in form-check-input" name="otherdocumentupload[]" id="otherdocumentupload<?php //echo $i+1; ?>" > -->
               <label class="form-check-label" for="otherdocumentupload<?php echo $i+1; ?>" required="required">verified </label>
               <span class="checkboxStatus"></span>
             </div>  
           </td>
         </tr>
         <?php $i++; } ?>
       </tbody>
     </table>
   </div>
 </div>
<?php  } }?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <table id="tblcomment" class="table table-bordered dt-responsive table-striped" >
    <thead>
     <tr class="bg-light">
      <th >Comments </th>
    </tr> 
  </thead>
  <tbody >
    <tr style="background-color: #fff;"   >
      <td style="resize: none;text-align: left;"> <?php echo $getverified->comment;?></textarea>
      </td>

    </tr>
  </tbody>
</table>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="row panel-footer">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">


      <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-dark text-center"   href="<?php echo site_url('Tc_verify_documents'); ?>">Go to List</button>


    </div>
  </div>
</div>

</form>


<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
 $(document).ready(function(){
  $("#candidateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px" />');
               })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     
