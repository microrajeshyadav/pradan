<section class="content">
  <div class="container-fluid">
  
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
             <div class="header">
              <h2>Offered candidate verification documents </h2><br>
            </div>
            <div class="body">
              <?php //print_r($getcandateverified); ?>
              <form name="joinDA" action="" method="post">
                <div class="row">
                  <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
                  <div class="col-lg-4">
                    <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
                   <?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>
                  </div>
                  <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                  <div class="col-lg-2"><?php echo $getcandateverified->batch; ?></div>
                  <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                  <div class="col-lg-1" id="candidateprofilepic" >
                    <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-2"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <table id="tblForm09" class="table table-bordered table-striped" >
                      <thead>
                       <tr>
                        <th style="background-color: #3CB371; color: #fff;">
                          Documents to be verified 
                        </th>
                        <th  style="background-color: #3CB371; color: #fff;">
                          Certificate
                        </th>
                        <th  style="background-color: #3CB371; color: #fff;">

                        </th>
                      </tr>
                    </thead> 

                    <tr>
                     <td>10th Standard</td>
                     <td>
                       <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptmetriccertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a></td>
                       <td>
                        
                          <label class="form-check-label" for="metric_certificate_verified">verified</label>
                        
                      </td> 
                    </tr>
                    <tr>
                      <td>12th Standard</td>
                      <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encrypthsccertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                       
                          <label class="form-check-label" for="hsc_certificate_verified">verified</label>
                       
                      </td>
                    </tr>
                    <tr>
                      <td>Under Graduation</td>
                      <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptugcertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                        
                          <label class="form-check-label" for="ug_certificate_verified">verified</label>
                       
                      </td>

                    </tr>
                    <?php if (!empty($getcandateverified->encryptpgcertificate)) {?>  
                      <tr>
                        <td>Post Graduation</td>
                        <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptpgcertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                        <td>
                          
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                         
                        </td>
                      </tr>
                    <?php }  ?>
                    <?php if (!empty($getcandateverified->originalothercertificate)) {?>  
                      <tr>
                        <td>Post Graduation</td>
                        <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->originalothercertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                        <td>
                          
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                        
                        </td>
                      </tr>
                    <?php }  ?>

                    <?php if ($getworkexpcount > 0) {
                      foreach ($getworkexpverified as $key => $value) {
                      ?>
                    <tr>
                      <td>Work Experience Certificate</td>
                      <td><a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                       
                          <label class="form-check-label" for="workexp_certificate_verified">verified</label>
                                              
                      </td>
                    </tr>
                    <?php } }  ?>
                  </table>
                </div>
              </div>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th style="background-color: #3CB371; color: #fff;">Comments </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
            <td><?php echo $getcandateverified->comment;?>
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th style="background-color: #3CB371; color: #fff;">Status </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
            <td>
              <?php if ($getcandateverified->status==1) {?>
              Approved 
              <?php }else{?>
                Rejected 
              <?php } ?>
             
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-4 text-center">
    
      <a href="<?php echo site_url('Hrd_document_checklist'); ?>" class="btn btn-danger" >Back</a>
     
    </div>
    <div class="col-lg-4"></div>
  </div>
</form>

</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
 $(document).ready(function(){
  $("#candidateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
               })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>
<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-1>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertIdentityRows(rowsIdentityEnter);
    //addDatePicker();
  });

  // var identcount = $('#identcount').val();

  // if (identcount =='NULL') {
  //   var srNoIdentity=0;
  //   var incr = 0;
  // }else{
  //   var srNoIdentity = identcount;
  //   var incr = identcount;
  // }
  

  function insertIdentityRows(count) {
    alert();
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      incr++
      cloneRow = lastRow.clone();
      var tableDataIdentity = '<tr>'
      + ' <td>'
      + '<select class="form-control" name="identityname['+incr+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysidentity as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
      <?php endforeach ?>
      + '</select>'

      + ' <td><input type="text" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td><td>'

      +' <div class="form-group">'
      +'   <input type="file" name="identityphoto['+incr+']"  id="identityphoto" class="file">'
      +' </div>'
      + '</td>' + '</tr>';
      $("#tbodyForm13").append(tableDataIdentity)
    }

  }
</script>