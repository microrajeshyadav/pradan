<style type="text/css">
.checkboxStatus{
  color:red;
}
</style>
<br/>
<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php  //print_r($getcandateverified); die(); ?>
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">

                <div class="panel-heading" style="padding: 20px;">
                  <div class="row">
                   <h4 class="col-md-7 panel-title pull-left">Candidate Document Verification</h4>
                    <!--  <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                  </div> -->
                </div>
                <hr class="colorgraph"><br>
              </div>
              <?php //echo "<pre>"; print_r($getcandateverified); die(); ?>
                <div class="body">
                  <form name="dadocumentverification" id="dadocumentverification" action="" method="post" enctype="multipart/form-data" >
                    <div class="row">
                       <input type="hidden" name="cate" id="cate" value="<?php echo $getcandateverified->categoryid; ?>">
                      <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
                      <div class="col-lg-4">
                        <input type="hidden" name="candidateid" id="candidateid" value="<?php if(!empty($getcandateverified->candidateid))
                        echo $getcandateverified->candidateid;?>">
                        <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php if(!empty($getcandateverified->candidatefirstname)) echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
                      </div>
                      <?php if (!empty($getcandateverified->batch)) { ?>
                      <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                      <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control"  readonly="readonly"></div>
                      <?php  } ?>


                      <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                        
                      <div class="col-lg-1" id="candidateprofilepic" >
                        <img src="<?php if(!empty($getcandateverified->encryptedphotoname)) echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" class= "rounded-circle" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px">
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-2"></div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <table id="tblForm09" class="table table-bordered dt-responsive table-striped" >
                          <thead>
                           <tr style="background-color: #eee;">
                            <th style="width:50%;" >
                              Documents to be verified 
                            </th>
                            <th style="width:25%; text-align: center;">
                              Certificate file
                            </th>
                            <th style="width:25%; text-align: center;">
                              Verification
                            </th>
                          </tr>
                        </thead> 
                        <tr style="background-color: #fff;">
                          <td>10th Standard</td>
                          <td style=" text-align: center;">
                            <?php 
                            if (!empty($getcandateverified->encryptmetriccertificate)) {


                            $matriccertificateArray = explode("|",$getcandateverified->encryptmetriccertificate);
                            $image_path='';

                            foreach($matriccertificateArray as $res)
                            {
                            $image_path='datafiles\educationalcertificate/'.$res;

                            if (file_exists($image_path)) {

                          ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res; ?>" download ><i class="fa fa-download" style="font-size:15px;"></i></a>
                          <?php } } } ?>
                          </td>
                           <td style=" text-align: center;">
                             <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input" name="metric_certificate_verified" id="metric_certificate_verified" required="required" >
                              <label class="form-check-label" for="metric_certificate_verified">Verified </label>
                              <span class="checkboxStatus"></span>
                            </div>
                          </td> 
                        </tr>
                        <tr style="background-color: #fff;">
                          <td>12th Standard</td>
                          <td style=" text-align: center;">
                            <?php if (!empty($getcandateverified->encrypthsccertificate)) 
                              {
                              $hsccertificateArray = explode("|",$getcandateverified->encrypthsccertificate);
                              $image_path='';

                              foreach($hsccertificateArray as $res)
                              {
                              $image_path='datafiles\educationalcertificate/'.$res;

                              if (file_exists($image_path)) {
                            ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" style="font-size:15px;"></i></a>
                          <?php } } } ?>
                          </td>
                          <td style="text-align: center;">
                            <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input" name="hsc_certificate_verified" id="hsc_certificate_verified" required="required">
                              <label class="form-check-label" for="hsc_certificate_verified">Verified</label>
                              <span class="checkboxStatus"></span>
                            </div>
                          </td>
                        </tr>
                        <tr style="background-color: #fff;">
                          <td>Graduation
                        <?php 
                        if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) { ?> 
                        <br>Migration Marks Sheet <?php 
                        if($getcandateverified->ugmigration_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) { ?> 
                        <br>Migration Cerificate <?php 
                        if($getcandateverified->ugdoc_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);?>)<?php } } ?>
                         </td>
                         <td style=" text-align: center;">
                          <?php if (!empty($getcandateverified->encryptugcertificate)) { 

                         $ugcertificateArray = explode("|",$getcandateverified->encryptugcertificate);
                            $image_path='';

                            foreach($ugcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugmigration_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);} ?>
                        <br>
                        Migration Marks Sheet 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>




                      <?php if (!empty($getcandateverified->ugdocencryptedImage)) { 

                         $ugdocArray = explode("|",$getcandateverified->ugdocencryptedImage);
                            $image_path='';

                            foreach($ugdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugdoc_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);} ?>
                        <br>
                        Migration Certificate 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?> 
                          </td>
                          <td style="text-align: center;">
                            <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input" name="ug_certificate_verified" id="ug_certificate_verified" required="required">
                              <label class="form-check-label" for="ug_certificate_verified">Verified</label>
                              <span class="checkboxStatus"></span>
                            </div>
                          </td>
                        </tr>
                        <?php if (!empty($getcandateverified->encryptpgcertificate)) {?>  
                        <tr style="background-color: #fff;">
                          <td>Post Graduation
                        <?php 
                        if (!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?><br> Migration Marks Sheet<?php 
                          if($getcandateverified->pgmigration_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?><br> Migration Certificate<?php 
                          if($getcandateverified->pgdoc_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);?>)<?php } } ?>
                       </td>

                             <td style="width: 100px; text-align: center;">
                              <?php if (!empty($getcandateverified->encryptpgcertificate)) {

                          $pgcertificateArray = explode("|",$getcandateverified->encryptpgcertificate);
                            $image_path='';

                            foreach($pgcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgmigration_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);} ?>
                            <br>
                            Migration Marks Sheet 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>



                        <?php if (!empty($getcandateverified->pgdocencryptedImage)) {

                          $pgdocArray = explode("|",$getcandateverified->pgdocencryptedImage);
                            $image_path='';

                            foreach($pgdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgdoc_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);} ?>
                            <br>
                            Migration Certificate 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>
                              </td>
                                <td style="text-align: center;">
                                 <input type="hidden" name="pg_certificate" id="pg_certificate" value="1">
                                 <div class="form-check">
                                  <input type="checkbox" class="filled-in form-check-input" name="pg_certificate_verified" id="pg_certificate_verified" required="required">
                                  <label class="form-check-label" for="pg_certificate_verified">Verified</label>
                                  <span class="checkboxStatus"></span>
                                </div>
                              </td>
                            </tr>
                            <?php }  










                            if ($getgapyearcount > 0) {
                           foreach ($getgapyearpverified as $key => $value) {?>
                            <tr style="background-color: #fff;">
                              <td>Gap Year Document</td>
                              <td style=" text-align: center;"><a href="<?php if(!empty($value->gapyeardocument)) echo site_url().'datafiles/gapyeardocument/'.$value->gapyeardocument;?>" download ><i class="fa fa-download" style="font-size:15px;"></i></a> </td>
                              <td style="text-align: center;">
                               <input type="hidden" name="gapyear_certificate" id="gapyear_certificate" value="1">
                               <div class="form-check">
                                <input type="checkbox" class="filled-in form-check-input" name="gapyear_certificate_verified" id="gapyear_certificate_verified" required="required">
                                <label class="form-check-label" for="gapyear_certificate_verified">Verified</label>
                                <span class="checkboxStatus"></span>
                              </div>
                            </td>
                          </tr>
                          <?php } } ?>

                          <?php  if (!empty($getcandateverified->encryptofferlettername)) {?>  
                          <tr style="background-color: #fff;">
                            <td>Signed Offer Letter</td>
                            <td style="text-align: center;"><a href="<?php if(!empty($getcandateverified->encryptofferlettername)) echo site_url().'pdf_offerletters/'.$getcandateverified->encryptofferlettername;?>" download ><i class="fa fa-download" style="font-size:15px;"></i></a> </td>
                            <td style=" text-align: center;">
                             <input type="hidden" name="signed_certificate" id="signed_certificate" value="1">
                             <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input" name="signed_certificate_verified" id="signed_certificate_verified" required="required">
                              <label class="form-check-label" for="signed_certificate_verified">Verified</label>
                              <span class="checkboxStatus"></span>
                            </div>
                          </td>
                        </tr>
                        <?php }  ?>

                        <?php if (!empty($getcandateverified->encryptothercertificate)) { ?>  
                        <tr style="background-color: #fff;">


                          <td>Other </td>
                          <td style=" text-align: center;">
                           <input type="hidden" name="other_certificate" id="other_certificate" value="1">
                           <a href="<?php if(!empty($getcandateverified->encryptothercertificate)) echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptothercertificate;?>" download ><i class="fa fa-download" style="font-size:15px;"></i></a> </td>
                           <td style=" text-align: center;">
                            <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input" name="other_certificate_verified" id="other_certificate_verified" required="required">
                              <label class="form-check-label" for="other_certificate_verified">Verified</label>
                              <span class="checkboxStatus"></span>
                            </div>
                          </td>
                        </tr>
                        <?php } ?>

                        <?php if ($getworkexpcount > 0) {

                          foreach ($getworkexpverified as $key => $value) {
                      //  print_r($value);
                            ?>
                            <tr style="background-color: #fff;">
                              <td>Work Experience Certificate</td>
                              <td style="width: 100px; text-align: center;">
                                <a href="<?php if(!empty($value->encrypteddocumnetname)) echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download ><i class="fa fa-download" ></i></a> |


                                <a href="<?php if(!empty($value->encryptedsalaryslip1)) echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip1;?>" download ><i class="fa fa-download" ></i></a>| 

                                <a href="<?php if(!empty($value->encryptedsalaryslip2)) echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip2;?>" download ><i class="fa fa-download" ></i></a>| 

                                <a href="<?php if(!empty($value->encryptedsalaryslip3)) echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip3;?>" download ><i class="fa fa-download" ></i></a> 



                              </td>
                              <td style="width: 100px; text-align: center;">
                                <input type="hidden" name="workexp_certificate" id="workexp_certificate" value="1">
                                <div class="form-check">
                                  <input type="checkbox" class="filled-in form-check-input" name="workexp_certificate_verified[]" id="workexp_certificate_verified_<?php echo $key;?>" required="required">
                                  <label class="form-check-label" for="workexp_certificate_verified_<?php echo $key;?>">Verified </label>
                                  <span class="checkboxStatus"></span>
                                </div>                              
                              </td>
                            </tr>
                            <?php } }  ?>
                          </table>
                        </div>
                      </div>

                     
                       <?php //print_r($getjoiningreportstatus);
                       // print_r($candidate);
                       // die;

                       // echo $candidate->categoryid;
                       // die;
                     if($getcandateverified->categoryid==1)
                     {
                        
                       ?>

                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <table id="tblForm13" class="table table-bordered dt-responsive table-striped" >
                          <thead>
                           <tr style="background-color: #eee;">
                            <th style="width:50%; ">Form Name</th>
                            <th style="width:25%; text-align: center;">PDF</th>
                            <th style="width:25%; text-align: center;">Verification</th>
                          </tr> 
                        </thead>
                        <tbody >
                          <tr style="background-color: #fff;">

                            <td>General Nomination form</td>
                            <td style="text-align: center;">
                        <?php if(!empty($getgeneralnominationformpdf->filename)) ?>
                        <a href="<?php echo site_url().'pdf_generalnominationform/'.$getgeneralnominationformpdf->filename;?>" download ><i class="fa fa-download"  ></i></a>

                            </td >
                            <td style=" text-align: center;"> <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input" name="general_nomination_form" id="general_nomination_form" >
                              <label class="form-check-label" for="general_nomination_form" required="required">Verified </label>
                              <span class="checkboxStatus"></span>
                            </div>   
                          </td>
                        </tr>
                        <tr style="background-color: #fff;">

                         <td>Joining report for DAs</td>
                         <td style="text-align: center;">
                    <?php if(!empty($getjoiningreportstatus->filename)) ?>      
                    <a href="<?php echo site_url().'pdf_joiningreportforda/'.$getjoiningreportstatus->filename;?>" download ><i class="fa fa-download"  ></i></a>
                        </td>
                        <td style="text-align: center;"> <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="joining_report_da" id="joining_report_da" >
                          <label class="form-check-label" for="joining_report_da" required="required">verified </label>
                          <span class="checkboxStatus"></span>
                        </div>   
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <?php if (isset($countotherdoc) && $countotherdoc >0) { ?>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <table id="" class="table table-bordered dt-responsive table-striped" >
                <thead>

                  <tr style="background-color: #eee;">

                   <th style="width: 40%; text-align: left;" >Document name</th>
                   <th style="width: 20%; text-align: center;">Document for</th>
                   <th style="width: 20%; text-align: center;">PDF</th>
                   <th style="width: 120%; text-align: center;">Verification </th>
                 </tr> 
               </thead>
               <tbody id="tbodyForm09">
                 <input type="hidden" name="otherdocumentcount" id="otherdocumentcount" value="<?php if(!empty($countotherdoc)) echo $countotherdoc;?>">
                 <?php 
                 $i=0;
                 foreach ($getotherdocdetails as $key => $val) {
                  ?>
                  <tr id="tbodyForm09" style="background-color: #fff;">
                    <input type="hidden" name="documentname" id="documentname" value="1">
                    <td><?php echo $val->document_name; ?></td>
                    <td style="width: 200px; text-align: center;"><?php echo $val->document_type; ?></td>
                    <td style="width: 100px; text-align: center;">
                      <a href="<?php if(!empty($val->documentupload)) echo site_url().'otherdocuments/'.$val->documentupload;?>" download ><i class="fa fa-download" style="font-size:15px;"></i></a>
                    </td>
                    <td style="width: 100px;  text-align: center;">
                     <div class="form-check">
                       <input type="hidden" name="documentid[]" id="documentid<?php echo $i+1; ?>" value="<?php echo $val->id;?>">
                       <input type="checkbox" class="filled-in form-check-input" name="otherdocumentupload[]" id="otherdocumentupload<?php echo $i+1; ?>" >
                       <label class="form-check-label" for="otherdocumentupload<?php echo $i+1; ?>" required="required">Verified </label>
                       <span class="checkboxStatus"></span>
                     </div>  
                   </td>
                 </tr>
                 <?php $i++; } ?>
               </tbody>
             </table>
           </div>
         </div>
         <?php  } }?>
         <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <table id="tblcomment" class="table table-bordered dt-responsive table-striped" >
            <thead>
             <tr style="background-color: #eee;">
              <th >Status </th>
              <th >Comments </th>
            </tr> 
          </thead>
          <tbody >
            <tr style="background-color: #fff;">
              <td> <select name="verified_status" id="verified_status" class="form-control" required="required">
                <option value="">Select </option>
                <option value="0">Not Approved</option>
                <option value="1">Approved</option> 
              </select></td>
              <td><textarea name="comments" id="comments" style="resize: none;" class="form-control" required="required" ></textarea>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-right">
      <button class="btn btn-success btn-sm md-10" type="submit" onclick="return 

      ()" name="submitbtn" id="submitbtn" value="senddatasubmit">Submit</button>

      <a href="<?php echo site_url("Tc_verify_documents");?>" class="btn btn-dark btn-sm m-t-10" data-toggle="tooltip" title="Close">Go to List </a> 
    </div>


  </div>
</form>

</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>

<script type="text/javascript">
  function checkgernalandjoiningform(){
    var cate=$("#cate").val();
    alert(cate);ssss

    if(!$('#metric_certificate_verified').is(':checked')){
     alert("Please verified 10th standard certificate !!!");
     $('#metric_certificate_verified').focus();
     return false;
   }

   if(!$('#hsc_certificate_verified').is(':checked')){
     alert("Please verified 12th standard certificate !!!");
     $('#hsc_certificate_verified').focus();
     return false;
   }

   if(!$('#ug_certificate_verified').is(':checked')){
     alert("Please verified graduation certificate !!!");
     $('#ug_certificate_verified').focus();
     return false;
   }



   if($('#pg_certificate').val()==1){
    if(!$('#pg_certificate_verified').is(':checked')){
     alert("Please verified post graduation certificate !!!");
     $('#pg_certificate_verified').focus();
     return false;
   }
 }


 if($('#other_certificate').val()==1){
  if(!$('#other_certificate_verified').is(':checked')){
   alert("Please verified other certificate !!!");
   $('#other_certificate_verified').focus();
   return false;
 }
}

if($('#workexp_certificate').val()==1){
  if(!$('#workexp_certificate_verified_0').is(':checked')){
   alert("Please verified work experience certificate !!!");
   $('#workexp_certificate_verified_0').focus();
   return false;
 }
}


if(!$('#general_nomination_form').is(':checked')){
 alert("Please verified general nomination !!!");
 $('#general_nomination_form').focus();
 return false;
}

else if(empty(getgeneralnominationformpdf) && cate==2 ){
  $("#general_nomination_form").prop("required", false);
  

}
else if(!$('#joining_report_da').is(':checked')){
 alert("Please verified joining report !!!");
 $('#joining_report_da').focus();
 return false;
}

else if(empty(getgeneralnominationformpdf) && cate==2 ){
  $("#joining_report_da").prop("required", false);
  

}



if($('#documentname').val()==1){
  if(!$('#otherdocumentupload1').is(':checked')){
   alert("Please verified other document !!!");
   $('#otherdocumentupload1').focus();
   return false;
 }
}

}   





</script>
