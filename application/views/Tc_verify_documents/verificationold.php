<style type="text/css">
  .checkboxStatus{
    color:red;
  }
  .checkboxStatussuccess{
    color:green;
  }
</style>
<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <?php //echo $getworkexpcount; print_r($getcandateverified); ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
             <div class="header">
              <h2>Offered candidate document verification</h2><br>
            </div>
            <div class="body">
              <form name="dadocumentverification" id="dadocumentverification" action="" method="post" enctype="multipart/form-data" >
                <div class="row">
                  <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
                  <div class="col-lg-4">
                    <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
                    <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
                  </div>
                  <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                  <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control"  readonly="readonly"></div>
                  <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                  <div class="col-lg-1" id="candidateprofilepic" >
                    <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-2"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <table id="tblForm09" class="table table-bordered table-striped" >
                      <thead>
                       <tr>
                        <th style="background-color: #3CB371; color: #fff;">
                          Documents to be verified 
                        </th>
                        <th  style="background-color: #3CB371; color: #fff;">
                          Certificate
                        </th>
                        <th  style="background-color: #3CB371; color: #fff;">

                        </th>
                      </tr>
                    </thead> 
                    <tr>
                     <td>10th Standard</td>
                     <td align="center">
                       <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptmetriccertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a></td>
                       <td align="center">
                         <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="metric_certificate_verified" id="metric_certificate_verified" required="required" >
                          <label class="form-check-label" for="metric_certificate_verified">verified </label><br>
                           <span class="checkboxStatus"></span>
                        </div>
                      </td> 
                    </tr>
                    <tr>
                      <td>12th Standard</td>
                      <td align="center"><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encrypthsccertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                        <div class="form-check" align="center">
                          <input type="checkbox" class="filled-in form-check-input" name="hsc_certificate_verified" id="hsc_certificate_verified" required="required">
                          <label class="form-check-label" for="hsc_certificate_verified">verified</label><br>
                           <span class="checkboxStatus"></span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Under Graduation</td>
                      <td align="center"><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptugcertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                        <div class="form-check" align="center">
                          <input type="checkbox" class="filled-in form-check-input" name="ug_certificate_verified" id="ug_certificate_verified" required="required">
                          <label class="form-check-label" for="ug_certificate_verified">verified</label><br>
                          <span class="checkboxStatus"></span>
                        </div>
                      </td>

                    </tr>
                    <?php if (!empty($getcandateverified->encryptpgcertificate)) {?>  
                      <tr>
                        <td>Post Graduation</td>
                        <td align="center"><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptpgcertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                        <td>
                          <div class="form-check" align="center">
                            <input type="checkbox" class="filled-in form-check-input" name="pg_certificate_verified" id="pg_certificate_verified" required="required">
                            <label class="form-check-label" for="pg_certificate_verified">verified</label><br>
                            <span class="checkboxStatus"></span>
                          </div>
                        </td>
                      </tr>
                    <?php }  ?>
                    <?php if (!empty($getcandateverified->originalothercertificate)) {?>  
                      <tr>
                        <td>Post Graduation</td>
                        <td align="center"><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->originalothercertificate;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                        <td align="center">
                          <div class="form-check">
                            <input type="checkbox" class="filled-in form-check-input" name="other_certificate_verified" id="other_certificate_verified" required="required">
                            <label class="form-check-label" for="pg_certificate_verified">verified</label><br>
                             <span class="checkboxStatus"></span>
                          </div>
                        </td>
                      </tr>
                    <?php }  ?>

                    <?php if ($getworkexpcount > 0) {

                      foreach ($getworkexpverified as $key => $value) {
                      ?>
                    <tr>
                      <td>Work Experience Certificate</td>
                      <td align="center"><a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" target=_blank><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td align="center">
                        <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="workexp_certificate_verified[]" id="workexp_certificate_verified_<?php echo $key;?>" required="required">
                          <label class="form-check-label" for="workexp_certificate_verified_<?php echo $key;?>">verified </label><br>
                          <span class="checkboxStatus"></span>
                        </div>                              
                      </td>
                    </tr>
                    <?php } }  ?>
                  </table>
                </div>
              </div>
        <?php //print_r($getjoiningreportstatus); ?>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th style="background-color: #3CB371; color: #fff;">Comments </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
            <td><textarea name="comments" id="comments" class="form-control" required="required"><?php echo set_value('comments');?></textarea>
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  </div>

        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th style="background-color: #3CB371; color: #fff;">Status </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
            <td>
              <select name="verified_status" id="verified_status" class="form-control" required="required">
                <option value="">Select</option> 
              <option value="1">Approved</option> 
              <option value="0">Not Approved</option>
             </select>
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-4 text-center">
      <input class="btn btn-warning" type="reset" value="Reset">
     <!--  <button class="btn btn-primary" type="submit" name="savebtn" value="senddatasave" id="savebtn">Save</button> -->
      <button class="btn btn-success" type="submit" name="submitbtn" id="submitbtn" value="senddatasubmit">Verified</button>
    </div>
    <div class="col-lg-4"></div>
  </div>
</form>

</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>

<script type="text/javascript">
 $(function(){
   $("#submitbtn").click(function(){
        if($('[type="checkbox"]').is(":checked")){
          $('.checkboxStatussuccess').html("")
            return true;
        }else{
            $('.checkboxStatus').html("Please check checkbox to verified document !!!");
             return false;
         }
         
   })
    
});

</script>