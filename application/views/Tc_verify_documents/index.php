<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
  <style type="text/css">
  .btn1{
    margin-left: 197px;
    margin-top: -93px;
    height: 35px;
  }
</style>
<div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
     <h4 class="col-md-10 panel-title pull-left">List of candidates Join a team</h4>
    
   </div>
   <hr class="colorgraph"><br>
 </div>
  
      <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

           <?php  //echo "<pre>"; print_r($candidatebdfformsubmit); ?>
              
             <div class="row" style="background-color: #FFFFFF;">
               <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
              
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                    <table id="tableAssignteam" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
                      <thead>
                      <tr>
                        <th class="text-center" style="width: 50px;">S.No.</th>
                        <th class="text-center">Category</th>
                        <th class="text-center">Campus Name</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Email id</th>
                        <th class="text-center">Batch</th>
                        <th class="text-center">Photo</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                         <th class="text-center">Joining</th>
                      </tr> 
                    </thead>
                    <tbody>
                    <?php if (count($candidatebdfformsubmit)==0) { ?>
                      <tr>
                      <td colspan="8" style="color:red; text-align: center;">No Record Found !!!</td>
                    </tr>
                 <?php    }
                 else{
                   
                  
                  $i=0; foreach($candidatebdfformsubmit as $key => $val){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $val->categoryname;?></td>
                      <td class="text-center"><?php echo $val->campusname;?></td>
                      <td class="text-center"><?php echo $val->candidatefirstname.' '.$val->candidatemiddlename.' '.$val->candidatelastname; ?></td>
                      <td class="text-center"><?php echo $val->gender;?></td>
                       <td class="text-center"><?php echo $val->emailid;?></td>
                      <td class="text-center"><?php echo $val->batch;?></td>
                       <td class="text-center" >
                        <?php  

                        if($val->encryptedphotoname)
                              {

                           $file_path=''; 
                            $file_path=FCPATH.'datafiles/'. $val->encryptedphotoname;

                            
                            if(file_exists($file_path))
                           
                         { ?>
                         <img src="<?php echo site_url().'datafiles/'.$val->encryptedphotoname;?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
                         <?php } else { ?>
                        
                         <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
                         <?php }  }
                          else 
                          {
                         ?>
                         <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
                         <?php  } ?>
                       </td>
                       <td class="text-center">
                       <?php 
                       if($val->categoryid==1)
                       {
                       if ($val->status==0) { ?>
                       <span class="badge badge-pill badge-danger">Not Approved</span>
                     <?php }else { ?>
                       <span class="badge badge-pill badge-success">Approved</span>
                     <?php } 
                   }
                   else 
                   {
                      if($val->status==0) {
                     ?>
                      <span class="badge badge-pill badge-danger">Not Approved</span>
                     <?php
                     } 
                     else 
                     {
                      ?>
                       <span class="badge badge-pill badge-success">Approved</span>
                       <?php
                     }
                   }
                     ?>
                   </td>
                      <td class="text-center">
                         <?php if ($val->status==0)   { ?>
                        <a href="<?php echo site_url('Tc_verify_documents/verification/'.$val->candidateid);?>" style="padding : 4px;" title="verification"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>
                      <?php }
                      else if ($val->status==1) {?>
                         <a href="<?php echo site_url('Tc_verify_documents/view/'.$val->candidateid);?>" style="padding : 4px;" title="view">
                         <i class="fa fa-eye"></i></a>
                      <?php } 
                      ?>

                     
                      



                                         
                      </td>
                      <td>
                      <?php 
                      if ($val->type==8 && $val->status==1 ) {?>
                         <a href="<?php echo site_url('Tc_verify_documents/verify_joininginduction/'.$val->candidateid);?>" style="padding : 4px;" title="view">
                         <i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></i></a>
                      <?php } 
                      else if($val->type==8 && $val->status==3 )
                      {


                      ?>
                      <a href="<?php echo site_url('Tc_verify_documents/view_joininginduction/'.$val->candidateid);?>" style="padding : 4px;" title="view">
                         <i class="fa fa-eye"></i></a>

                    <?php } ?>
                    </td>
                    </tr>
                    <?php $i++; } 
                    } ?>
                  </tbody>
                    </table>
                  </div>

                </div>
              </form> 
            </div>
          </div>
        </div>  
       
      </section>
      <!--  JS Code Start Here  -->
         
<script type="text/javascript">
 $(document).ready(function(){
            $("#candidateid").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
                    type: 'POST',
                    dataType: 'json',
                })
                .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                   $("#batchid").val(data.batch);
                   $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });
      
        });     

          
          $('[data-toggle="tooltip"]').tooltip();  
          $('#tableAssignteam').DataTable({
             dom: 'Bfrtip',
        buttons: [
            'excel', 'print'
        ],
           "bPaginate": true,
           "bInfo": true,
           "bFilter": true,
           "bLengthChange": true
         }); 
      
         // $('#Generateofferletter_3').attr('disabled','disabled');
         // $('#Generateofferletter_4').attr('disabled','disabled');
        // $('#sendtoed_3').attr('disabled','disabled');
        // $('#sendtoed_4').attr('disabled','disabled');
      
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>