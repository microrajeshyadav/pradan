<style type="text/css">
.checkboxStatus{
  color:red;
}
</style>
<br/>
<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php  //print_r($getcandateverified); die(); ?>
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">

                <div class="panel-heading" style="padding: 20px;">
                  <div class="row">
                   <h4 class="col-md-7 panel-title pull-left">Candidate Joining Induction Form Verification</h4>
                    <!--  <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                  </div> -->
                </div>
                <hr class="colorgraph"><br>
              </div>
              <?php //echo "<pre>"; print_r($getcandateverified); die(); ?>
                <div class="body">
                  <form name="dadocumentverification" id="dadocumentverification" action="" method="post" enctype="multipart/form-data" >
                    <div class="row">
                       <input type="hidden" name="cate" id="cate" value="<?php echo $getcandateverified->categoryid; ?>">
                      <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
                      <div class="col-lg-4">
                        <input type="hidden" name="candidateid" id="candidateid" value="<?php if(!empty($getcandateverified->candidateid))
                        echo $getcandateverified->candidateid;?>">
                        <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php if(!empty($getcandateverified->candidatefirstname)) echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
                      </div>
                      <?php if (!empty($getcandateverified->batch)) { ?>
                      <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                      <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control"  readonly="readonly"></div>
                      <?php  } ?>


                      <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                        
                      <div class="col-lg-1" id="candidateprofilepic" >
                        <img src="<?php if(!empty($getcandateverified->encryptedphotoname)) echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" class= "rounded-circle" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px">
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-2"></div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <table id="tblForm09" class="table table-bordered dt-responsive table-striped" >
                          
                          </table>
                        </div>
                      </div>

                     
                       <?php //print_r($getjoiningreportstatus);
                       // print_r($candidate);
                       // die;

                       // echo $candidate->categoryid;
                       // die;
                       ?>
                    

                     
         <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <table id="tblcomment" class="table table-bordered dt-responsive table-striped" >
            <thead>
             <tr style="background-color: #eee;">
              <th >Status </th>
              <th >Comments </th>
            </tr> 
          </thead>
          <tbody >
            <tr style="background-color: #fff;">
              <td> <select name="verified_status" id="verified_status" class="form-control" required="required">
                <option value="">Select </option>
                <option value="0">Not Approved</option>
                <option value="1">Approved</option> 
              </select></td>
              <td><textarea name="comments" id="comments" style="resize: none;" class="form-control" required="required" ></textarea>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-right">
      <button class="btn btn-success btn-sm md-10" type="submit" onclick="return 

      ()" name="submitbtn" id="submitbtn" value="senddatasubmit">Submit</button>

      <a href="<?php echo site_url("Tc_verify_documents");?>" class="btn btn-dark btn-sm m-t-10" data-toggle="tooltip" title="Close">Go to List </a> 
    </div>


  </div>
</form>

</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>

<script type="text/javascript">
  function checkgernalandjoiningform(){
    var cate=$("#cate").val();
    alert(cate);ssss

    if(!$('#metric_certificate_verified').is(':checked')){
     alert("Please verified 10th standard certificate !!!");
     $('#metric_certificate_verified').focus();
     return false;
   }

   if(!$('#hsc_certificate_verified').is(':checked')){
     alert("Please verified 12th standard certificate !!!");
     $('#hsc_certificate_verified').focus();
     return false;
   }

   if(!$('#ug_certificate_verified').is(':checked')){
     alert("Please verified graduation certificate !!!");
     $('#ug_certificate_verified').focus();
     return false;
   }



   if($('#pg_certificate').val()==1){
    if(!$('#pg_certificate_verified').is(':checked')){
     alert("Please verified post graduation certificate !!!");
     $('#pg_certificate_verified').focus();
     return false;
   }
 }


 if($('#other_certificate').val()==1){
  if(!$('#other_certificate_verified').is(':checked')){
   alert("Please verified other certificate !!!");
   $('#other_certificate_verified').focus();
   return false;
 }
}

if($('#workexp_certificate').val()==1){
  if(!$('#workexp_certificate_verified_0').is(':checked')){
   alert("Please verified work experience certificate !!!");
   $('#workexp_certificate_verified_0').focus();
   return false;
 }
}


if(!$('#general_nomination_form').is(':checked')){
 alert("Please verified general nomination !!!");
 $('#general_nomination_form').focus();
 return false;
}

else if(empty(getgeneralnominationformpdf) && cate==2 ){
  $("#general_nomination_form").prop("required", false);
  

}
else if(!$('#joining_report_da').is(':checked')){
 alert("Please verified joining report !!!");
 $('#joining_report_da').focus();
 return false;
}

else if(empty(getgeneralnominationformpdf) && cate==2 ){
  $("#joining_report_da").prop("required", false);
  

}



if($('#documentname').val()==1){
  if(!$('#otherdocumentupload1').is(':checked')){
   alert("Please verified other document !!!");
   $('#otherdocumentupload1').focus();
   return false;
 }
}

}   





</script>
