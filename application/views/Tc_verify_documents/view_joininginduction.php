
<br/>
<div class="container-fluid"  style="font-family: 'Oxygen' !important;" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">DA Document Verification</h4>
       

     </div>
     <hr class="colorgraph"><br>
   </div>

   <div class="panel-body">


    <div class="body">
      <form name="joinDA" action="" method="post">
        <div class="row">
          <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">DA Name :</label></div>
          <div class="col-lg-4">
            <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
            <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
          </div>
          <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
          <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control" readonly="readonly"></div>
          <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
          <div class="col-lg-1" id="candidateprofilepic" >

                <?php  

                        if($getcandateverified->encryptedphotoname)
                              {

                           $file_path=''; 
                            $file_path=FCPATH.'datafiles/'.$getcandateverified->encryptedphotoname;

                            
                            if(file_exists($file_path))
                           
                         { ?>


            <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" class= "rounded-circle" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px">
            <?php } else{

              ?>
              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
              <?php } 
            }

              else

            {
            ?>
               <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
               <?php
                }

            ?>

          </div>
          </div>
          

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <table id="tblcomment" class="table table-bordered dt-responsive table-striped" >
    <thead>
     <tr class="bg-light">
      <th >Comments </th>
    </tr> 
  </thead>
  <tbody >
    <tr style="background-color: #fff;"   >
      <td style="resize: none;text-align: left;"> <?php echo $getverified->comment;?></textarea>
      </td>

    </tr>
  </tbody>
</table>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="row panel-footer">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">


      <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-dark text-center"   href="<?php echo site_url('Tc_verify_documents'); ?>">Go to List</button>


    </div>
  </div>
</div>

</form>


<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
 $(document).ready(function(){
  $("#candidateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px" />');
               })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     
