
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <form name="staffreviewfrm" id="staffreviewfrm" method="POST" action="">
       <div class="panel-body">
       
        <div class="row" style="line-height: 3">
          <div class="col-md-12">
            1. Name : <input type="text" name="staffname" id="staffname"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly"  value="<?php echo $getstaffprobationreview->name; ?>">
          </div>
          <div class="col-md-12">
           2. Designation : <input type="text" name="staffdesignation" id="staffdesignation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->desname; ?>" readonly="readonly">
         </div>     
         <div class="col-md-12">
          3. Employee Code : <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->emp_code; ?>" readonly="readonly">
        </div>
    
      <div class="col-md-12">
        5. Location : <input type="text" name="stafflocation" id="stafflocation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->officename; ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        6. Date of Appointment: <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->date_of_appointment); ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        7. Period of Review : From <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_from); ?>" readonly="readonly"> (date) To <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_to); ?>" readonly="readonly"> (date)
      </div>
    </div>
    <p></p>
    <p></p>
    <p><strong>TO BE REVIEWED BY</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1. Name :<input type="text" name="review_by" id="review_by" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->name;?>" readonly="readonly">
      </div>

      <div class="col-md-12">
       2. Designation :<input type="text" name="review_designation" id="review_designation" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->desname;?>" readonly="readonly">
     </div>

     <div class="col-md-12">
      3. Employee Code : <input type="text" name="review_emp_code" id="review_emp_code" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->emp_code;?>" readonly="readonly">
    </div>

    <div class="col-md-12">
      5. Location : <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->officename;?>" readonly="readonly">
    </div>
  </div>
  <hr/>
  
  <div class="text-center">
    <p><h5> Reviewing By Supervisor</h5></p>
  
  </div>
  

  
 <p><strong>Comment</strong></p>
  

  <textarea name="work_habits_and_attitudes" readonly="readonly" maxlength="500" id="work_habits_and_attitudes" cols="40" rows="10" style="width:1000px;"><?php echo $getstaffprobationreview->work_habits_and_attitudes;?></textarea>

 
  <div class="container-fluid">
    <div class="row text-left">
      1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation indicated above, has been (please tick &uuml;):
    </div>
    <div class="row text-left">
      <div class="col-md-3"> (a)  Satisfactory</div>
      <div class="col-md-9 text-left"><input type="radio" name="satisfactory" id="satisfactory" value="satisfactory" <?php if ($getstaffprobationreview->satisfactory =="satisfactory") {
       echo "checked=checked";
     }?>></div>
     <div class="col-md-12">  OR</div>
     <div class="col-md-3"> (b)  Not satisfactory</div>
     <div class="col-md-9"><input type="radio" name="satisfactory" id="notsatisfactory" value="notsatisfactory" <?php if ($getstaffprobationreview->satisfactory =="notsatisfactory") {
       echo "checked=checked";
     }?>></div>

   </div>
   
   
 <div class="row" style="line-height: 3">
        <div class="col-md-6 text-right">
          
        </div>
        <div class="col-md-6 text-left">
           <img src="<?php if(!empty($reportingto_image->encrypted_signature)) echo site_url().'/datafiles/signature/'.$reportingto_image->encrypted_signature;?>" height="50" width="140">
        </div>
       
      </div>
       <div class="row" style="line-height: 3">
  <div class="col-md-6 pull-left">
    Location: <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffpersonneldetals->newofficename;?>" readonly="readonly">
  </div>

  <div class="col-md-6 pull-right">
    Signature of Reviewing Supervisor
  </div>
  <div class="col-md-6 pull-left">
    Date:<input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
  </div>
  <div class="col-md-6 pull-right">
    Name:  <?php echo $getstaffpersonneldetals->name;?>
  </div>
  <div class="col-md-6 pull-left">

  </div>
  <div class="col-md-6 pull-right">
   Designation: <?php echo $getstaffpersonneldetals->desname;?>
 </div>
</div>
</div>

<?php if(($getstaffprobationreview->satisfactory =="notsatisfactory" && $getstaffprobationreview->probation_completed =="no")|| ($getstaffprobationreview->probation_completed =="not_recommended") || (!empty($getstaffprobationreview->probation_extension_date)))
{

?>
<div class="text-center">
  <p><h5>Part - IV</h5></p>
  <p><h5>Executive Director&rsquo;s Note </h5></p>
  <p>(<em>required only in cases where probation is proposed to be extended OR absorption is <u>not </u>recommended by the Reviewing Supervisor</em>)<em>.</em></p> </div>
  <p></p>
  <p>(i) I agree/do not agree with the above recommendations.</p>
  <br>
  
  <input type="radio" name="agree" id="agree" value="yes" <?php if ($getstaffprobationreview->agree =="yes") {
   echo "checked=checked";
 }?>><em> I agree</em><strong><em> </em></strong></p>
 <p><strong><em> </em></strong>or</p>
 <p><strong><em>  </em><input type="radio" name="agree" id="agree" value="no" <?php if ($getstaffprobationreview->agree =="no") {
   echo "checked=checked";
 }?> value="no"></strong><em> do not agree</em><em> <strong> </strong></em></p>
 <p></p>
 
 <br>
 <br>
 <br>



  
  <p>(ii) The reasons for not agreeing to above recommendations are:</p>
  <textarea name="ed_comments" maxlength="500" id="ed_comments" cols="40" rows="10" style="width:1000px;"  ><?php echo $getstaffprobationreview->ed_comments; ?></textarea>
  
  <p></p>
  <div class="row" style="line-height: 3">
        <div class="col-md-6 text-right">
          
        </div>
        <div class="col-md-6 text-left">
           <img src="<?php if(!empty($ed_image->encrypted_signature)) echo site_url().'/datafiles/signature/'.$ed_image->encrypted_signature;?>" height="50" width="140">
        </div>
       
      </div>
  <div class="row" style="line-height: 3">
    <div class="col-md-6 pull-left">
      Date:  <input type="text" name="ed_date" id="ed_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
    </div>
    <div class="col-md-6 pull-right">
     Signature of ED :  <input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $geted_detals->name;?>">
   </div>      
 </div> 
 
 <br>
 <br>
 

<?php } ?>
 

 



<div class="panel-footer text-right">
 
 
 <a href="<?php echo site_url().'Staff_personnel_midterm_review/'  ?>" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</form>
</div>
</div>
</section>
<script type="text/javascript">
  $( document ).ready(function() {
    $('#edstatus').change(function(){
      var sendtoperal = $('#edstatus').val();
      if (sendtoperal==26) {
       $('#astristhideshow').show();
       $('#comments').attr('required', true);
     }else if(sendtoperal==25) {
      $('#astristhideshow').hide();
      $('#comments').attr('required', false);
    }else if(sendtoperal==8) {
      $('#astristhideshow').show();
      $('#comments').attr('required', true);
    }else if(sendtoperal==7) {
      $('#astristhideshow').hide();
      $('#comments').attr('required', false);
    }
  });
    $('#satisfactory').prop("disabled", "disabled");
    $('#notsatisfactory').prop("disabled", "disabled");
    $('#completed').prop("disabled", "disabled");
    $('#extended_completed').prop("disabled", "disabled");
    $('#not_recommended').prop("disabled", "disabled");
    $('#probation_extension_date').prop("disabled", "disabled");
  });
  


</script>
<!-- <script>
$('#satisfactory').on('click', function() {
  if ($(this).val() === 'satisfactory') {
    $('#extended_completed').prop('checked', false);
    $('#not_recommended').prop('checked', false);
    $('#completed').removeProp("disabled");    
    $('#extended_completed').prop("disabled", "disabled");
    $('#not_recommended').prop("disabled", "disabled");
    $('#probation_extension_date').prop("disabled", "disabled");
    $('#probation_extension_date').val("");
      $('#probation_extension_date').prop("required", false);
  }
});

  $('#notsatisfactory').on('change', function() {
    if ($(this).val() === 'notsatisfactory') {
      alert('cxjhgvkj');
      $('#completed').prop('checked', false);
      $('#completed').prop("disabled", "disabled");
      $('#extended_completed').removeProp("disabled");
      $('#not_recommended').removeProp("disabled");
    }
  });


  $('#extended_completed').on('click', function() {
    if ($(this).val() === 'no') {
      $('#probation_extension_date').removeProp("disabled");
      $('#probation_extension_date').prop("required", true);
    }else {
      $('#probation_extension_date').prop("disabled", "disabled");
    }
  });
  $('#not_recommended').on('click', function() {
    if ($(this).val() === 'not_recommended') {
      $('#not_recommended').removeProp("disabled");
      $('#probation_extension_date').prop("disabled", "disabled");
      $('#probation_extension_date').val("");
      $('#probation_extension_date').prop("required", false);
    }
  });
</script> -->
