<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="text-center">
        <p><h5>Part- I</h5></p>
        <p>(<em>to be filled in by the Finance-Personnel-MIS Unit)</em></p>
      </div>
      <div class="row" style="line-height: 3">
        <div class="col-md-12">
          1. Name : ___________________________________
        </div>
        <div class="col-md-12">
         2. Designation : ___________________________________
       </div>     
       <div class="col-md-12">
        3. Employee Code : ___________
      </div>
      <div class="col-md-12">
        4. Project  : ___________
      </div>
      <div class="col-md-12">
        5. Location : ___________________________________
      </div>
      <div class="col-md-12">
        6. Date of Appointment: _______________
      </div>
      <div class="col-md-12">
        7. Period of Review : From _______________ (date) To _______________ (date)
      </div>
    </div>
    <p></p>
    <p></p>
    <p><strong>TO BE REVIEWED BY</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1. Name : ___________________________________
      </div>

      <div class="col-md-12">
       2. Designation : ___________________________________
     </div>

     <div class="col-md-12">
      3. Employee Code : ___________
    </div>

    <div class="col-md-12">
      4. Project  : ___________
    </div>

    <div class="col-md-12">
      5. Location : ___________________________________
    </div>
  </div>
  <hr/>
  <p><strong>Notes for the Reviewer</strong></p>
  <p></p>
  <ol>
    <li>Please assess the employee&rsquo;s performance during the period of probation mentioned above (Item 6).</li>
  </ol>
  <p></p>
  <ol start="2">
    <li>Please be objective in your assessment. Each factor should be assessed independently, uninfluenced by assessment of the other factor(s).</li>
  </ol>
  <p></p>
  <ol start="3">
    <li>Please return the duly filled in assessment report in a closed cover marked <em>confidential</em> to</li>
  </ol>
  <p>the Finance-Personnel-MIS Unit latest by _______________ (date).</p>
  <p></p>
  <div class="text-center">
    <p><h5>Part-II</h5></p>
    <p><h5>REVIEW</h5></p>
    <p>(<em>to be filled in by the Reviewing Supervisor</em>)</p>
  </div>
  <p>The probation has provided you an opportunity to interact with and observe the probationer in the work setting for six months. This is a significant period of time, for both sides.</p>
  <p></p>
  <p>&ldquo;<strong>How do you see the Probationer fulfilling the primary responsibilities s/he would need to shoulder to be an effective employee of this organization?</strong>&rdquo;</p>
  <p><strong></strong></p>
  <p>Could you please write about <em>500 words</em> addressing the issue (primary responsibilities s/he is expected to handle). Please give anecdotes/examples wherever possible.</p>

 
  <p><strong>Work Habits and Attitudes</strong></p>
  <p></p>
  <p>How would you rate the probationer&rsquo;s interest in and aptitude for work? What is the quality of her/his work? Does s/he demonstrate proaction and take initiative? Is s/he serious about work, and shows a sense of commitment to it? How resourceful is the probationer? Is s/he punctual, disciplined and regular at work? How does s/he fit into PRADAN&rsquo;s work culture? What is her/his attitude to the communities we work with? How does s/he deal with the conditions of work, absence of regular timings, physical hardship and unstructured work situation?</p>

  <p><strong>Conduct and Social Maturity</strong></p>
  <p></p>
  <p>What is the probationer&rsquo;s general behaviour like? Does s/he possess the requisite inter-personal competence? What is her/his demeanour such as&mdash;e.g., is s/he moody, short-tempered, adjusting, reserved, talkative, a gossip, etc.?</p>

  <p><strong>Integrity</strong></p>
 
  <p>Is the probationer's integrity<em> </em><em> Questionable</em><strong><em> </em></strong></p>
  <p><strong><em> </em></strong>or</p>
  <p><strong><em>  </em></strong><em>Above Board</em><em> <strong> </strong></em></p>

  <p><strong>Any Other Observations</strong></p>
  <p>Please share any other observations you would like to make about the probationer. For instance, does s/he have any areas that s/he could improve upon? Is there anything significant about her/him that has not been captured in the above?</p>
  <div class="text-center">
    <p><strong><br /> </strong></p>
    <p><h5>Part III</h5></p>
    <p><h5>OVERALL REVIEW AND RECOMMENDATIONS</h5></p>
  </div>
  <div class="container-fluid">
    <div class="row text-left">
      1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation indicated above, has been (please tick &uuml;):
    </div>
    <div class="row text-left">
      <div class="col-md-12"> (a)  Satisfactory</div>
      <div class="col-md-12">  OR</div>
      <div class="col-md-12"> (b)  Not satisfactory</div>

    </div>
    <div class="row text-left">
      2. Her/his probation:
    </div>
    <div class="row text-left">
      <div class="col-md-12"> (a)  May be completed</div>
      <div class="col-md-12">  OR</div>
      <div class="col-md-12"> (b) May not be completed now as her/his performance needs to be observed further for a period of _____ months.</div>
      <div class="col-md-12">  (c)  Not recommended for absorption in PRADAN</div>
    </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
        Place: _________________________
      </div>

      <div class="col-md-6 pull-right">
        Signature of Reviewing Supervisor
      </div>
      <div class="col-md-6 pull-left">
        Date: ________________
      </div>
      <div class="col-md-6 pull-right">
        Name: ______________________________
      </div>
      <div class="col-md-6 pull-left">

      </div>
      <div class="col-md-6 pull-right">
       Designation: _____________________________
     </div>
   </div>
 </div>
 <div class="text-center">
  <p><h5>Part - IV</h5></p>
  <p><h5>Executive Director&rsquo;s Note </h5></p>
  <p>(<em>required only in cases where probation is proposed to be extended OR absorption is <u>not </u>recommended by the Reviewing Supervisor</em>)<em>.</em></p> </div>
  <p></p>
  <p>(i) I agree/do not agree with the above recommendations.</p>
  <p></p>
  <p>(ii) The reasons for not agreeing to above recommendations are:</p>
  <p>_______________________________________________________________________</p>
  <p> _______________________________________________________________________</p>
  <p> _______________________________________________________________________</p>
  <p> _______________________________________________________________________</p>
  <p> _______________________________________________________________________</p>
  <p></p>
 <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
        Date: _________________________
      </div>
      <div class="col-md-6 pull-right">
       Signature of ED : ________________________
      </div>      
   </div> 
  <div class="text-center">
    <p><h5>Part - V</h5></p>
    <p>(<em>for use in the Finance-Personnel-MIS Unit)</em></p>
    <p></p>
    <p>Necessary action has been taken and all concerned have been informed accordingly. The Probationer Register has also been completed as above.</p>
    <p></p> </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
       Name: ____________________________
      </div>
      <div class="col-md-6 pull-right">
       Designation: _____________________________
      </div>
       <div class="col-md-6 pull-left">
       Signature :________________________
      </div>
      <div class="col-md-6 pull-right">
       Date: ________________
      </div>      
   </div>
  </div>
  <div class="panel-footer text-right">
        <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
        <a href="" class="btn btn-dark btn-sm"> Go Back</a>
      </div>
</div>
</div>
</section>