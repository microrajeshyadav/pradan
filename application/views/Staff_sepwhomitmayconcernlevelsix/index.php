<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;
  
}
textarea{
  width: 600px !important;
}


</style>
<section class="content" style="background-color: #FFFFFF;" >
<?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div> 
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <form role="form" method="post" action="">
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE FOR (Level 5-6)
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5> EXPERIENCE CERTIFICATE FOR (Level 5-6) </h5> 

        </div>
        <div class="col-md-12 text-center">
          <h4>TO WHOM IT MAY CONCERN</h4> 
        </div>          
      </div>
      

      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         301-2117/PDR/<input type="text" name="sep_no" maxlength="50" value="<?php if($tbl_levelwisetermination){ echo $tbl_levelwisetermination->sep_no;}?>" class="inputborderbelow" required>  
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         May 9, 2018
       </div>
    </div> 
   
   
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    Mr/Ms. <label class="inputborderbelow"><?php echo $staff_detail->name; ?></label> was employed by this organization as <label class="inputborderbelow" ><?php echo $staff_detail->joindesig; ?></label> from <label class="inputborderbelow"> <?php echo $this->gmodel->changedatedbformate($staff_detail->joiningdate); ?></label> to <label class="inputborderbelow"> December 14, 2014</label> at <label class="inputborderbelow">Khunti, Jharkhand</label> as <label class="inputborderbelow">Team Coordinator</label> from <label class="inputborderbelow">December 15, 2014</label> to <label class="inputborderbelow">February 5, 2017</label> at <label class="inputborderbelow">Bangla of Jangal Mahal Development Cluster , West Bengal</label> . Prior to that he participated in our in-house training programme for Development Apprentices for <label class="inputborderbelow">12 months</label> in <label class="inputborderbelow">Hazaribagh, Jharkhand</label>. This programme enables young professionals to develop the knowledge, skills and attitude necessary for a career in rural development. 
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
    PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has <input type="text" maxlength="4" name="total_staff" value="<?php if($tbl_levelwisetermination){ echo $tbl_levelwisetermination->total_staff;} ?>" class="inputborderbelow txtNumeric" required /> staff, including <input type="text" maxlength="4" name="total_professional" value="<?php if($tbl_levelwisetermination){ echo $tbl_levelwisetermination->total_professional;} ?>" class="inputborderbelow txtNumeric" required/> professionals. It works in seven of the poorest States in India. 
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
   Mr/Ms. <label class="inputborderbelow"> <?php echo $staff_detail->name; ?> </label> has amply demonstrated his ability to work under difficult conditions in remote villages. He displays empathy and maturity in working with poor people. Mr/Ms. <label class="inputborderbelow"><?php echo $staff_detail->name; ?></label> was able to use his technical knowledge and skills to help poor people improve their livelihoods.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
   I wish her very best in her life and career.
   </div>   
 </div>
 <hr/>
 <div class="row ">
   <div class="col-md-12 text-right" style="margin-top: 20px;">
   (___________________________)
   </div>
   <div class="col-md-12 text-right" style="margin-top: 20px;">
  Executive Director
   </div>

 </div>

</div>
<div class="panel-footer text-right">
  <?php
  if(empty($tbl_levelwisetermination)){
    ?>
    <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
    <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
    <?php
  }else{
    if($tbl_levelwisetermination->flag == 0){
     ?>
     <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
     <?php } ?>
     <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
     <?php } ?>
     <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
   </div>
</div>
</div>
</form>
</section>

<?php 
if($tbl_levelwisetermination){
  if($tbl_levelwisetermination->flag == 1){ ?>
  <script type="text/javascript">
    $(document).ready(function(){
     
      $('#saveandsubmit').hide() ;
      $("form input[type=text]").prop("disabled", true);

      $("form input[type=radio]").prop("disabled", true);
      $("form textarea").prop("disabled", true);
      $("form textarea").css("width", "600px;");
      $("form textarea").attr("style", "width:600px;");
    });
    
  </script>
  <?php } }?>
  <script type="text/javascript">
    $('.txtNumeric').keypress(function(event) {
      var $this = $(this);
      if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
        ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
            event.preventDefault();
      }

      var text = $(this).val();
      if ((event.which == 46) && (text.indexOf('.') == -1)) {
         setTimeout(function() {
             if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                 $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
             }
         }, 1);
      }

      if ((text.indexOf('.') != -1) &&
         (text.substring(text.indexOf('.')).length > 2) &&
         (event.which != 0 && event.which != 8) &&
         ($(this)[0].selectionStart >= text.length - 2)) {
             event.preventDefault();
      }      
    });



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
  </script>