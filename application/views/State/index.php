<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "State" && $row->Action == "index"){ ?>
 <br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <?php  ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">State List</h4>
      <div class="col-md-2 text-right">
      
         <a data-toggle="tooltip" data-placement="bottom" title="Want to add new state? Click on me." href="<?php echo site_url("state/add/")?>" class="btn btn-primary btn-sm">Add New State</a> 

      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
            <table id="tblstate" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
              <thead>
                <tr>
                  <th class="text-center" style="width: 50px;">S.No.</th>
                  <th style="max-width: 100px;" class="text-center" >State Code</th>
                  <th class="text-center">State Name</th>
                  <th class="text-center" style="width: 50px;">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach($state_details as $row){ ?>
                <tr>
                  <td class="text-center"><?php echo $i+1; ?></td>
                  <td class="text-center"><?php echo $row->statecode; ?></td>
                  <td class="text-center"><?php echo $row->name;?></td>
                  <td class="text-center">
                    <a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Batch."href="<?php echo site_url('state/edit/'.$row->id);?>" style="padding : 4px;" title="Edit"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> |

                    <a data-toggle="tooltip" data-placement="bottom" title="Click here to delete this state." href="<?php echo site_url('State/delete/'.$row->id);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" title="Delete"><i class="fa fa-trash" style="color:red"></i></a>

                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>
<script>

  $(document).ready(function() {
    $('#tblstate').DataTable({
      "paging": false,
      "search": false,
    });
  });

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this State ?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>