
<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Gdscore" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">GD Score</h4>

       </div>
       <hr class="colorgraph"><br>
     </div>



     <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

          <form name="formgdscoresearch" id="formgdscoresearch" method="post" action="" >
            <div class="row" style="background-color: #FFFFFF;">
             <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
             <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right">   
               <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
               <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 " style="background-color: white;">  
                <select name="campusid" id="campusid" class="form-control" required="required">
                  <option value="">Select Campus</option>
                  <?php foreach ($campusdetails as $key => $value) {
                    $expdatefrom = explode('-', $value->fromdate);
                    $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                    $expdateto = explode('-', $value->todate);
                    $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];

                    $campval  = $value->campusid.'-'. $value->id; 
                    if ($campval == $campusid) {
                     ?>
                     <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                     <?php }else{ ?>
                     <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                     <?php } } ?>
                   </select> 
                   <?php echo form_error("campusid");?>
                 </div>
                 <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"><button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-round btn-dark btn-sm">Search</button></div>
                 <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;"></div>
               </div>
             </form>
             <br>

             <form name="formgdscore" id="formgdscore" method="post" action="" >
              <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
              <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 text-right" style="background-color: white; padding-top: 10px;"><label >Cutoff Grade </label>
              </div>

              <div class=" col-md-3 col-lg-3 col-xs-12 col-sm-12" style="background-color: white;"> 
               <input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>">

               <select name="cutoffmarks" id="cutoffmarks"  required="required"
                        class="form-control isNumberKey" style="text-align:right; max-width:100px;" minlength="1"  maxlength="2">
                       <option value="">Grade</option>
                       <?php if ($campuscutoffmarks) {

                        ?>
                       <option value="81" <?php if($campuscutoffmarks->campus_gd_cutoffmarks>=81) {?> selected<?php } ?>>A</option>
                       <option value="61" <?php if($campuscutoffmarks->campus_gd_cutoffmarks>=61 && $campuscutoffmarks->campus_gd_cutoffmarks<81) {?> selected<?php } ?>>B</option>
                       <option value="51" <?php if($campuscutoffmarks->campus_gd_cutoffmarks>=51 && $campuscutoffmarks->campus_gd_cutoffmarks<61) {?> selected<?php } ?>>C1</option>
                       <option value="41" <?php if($campuscutoffmarks->campus_gd_cutoffmarks>=41 && $campuscutoffmarks->campus_gd_cutoffmarks<51) {?> selected<?php } ?>>C2</option>
                       <option value="21" <?php if($campuscutoffmarks->campus_gd_cutoffmarks>=21 && $campuscutoffmarks->campus_gd_cutoffmarks<41) {?> selected<?php } ?>>D</option>
                       <option value="1" <?php if($campuscutoffmarks->campus_gd_cutoffmarks>=1 && $campuscutoffmarks->campus_gd_cutoffmarks<21) {?> selected<?php } ?>>E</option>
                       <?php } else {?>
                       <option value="81">A</option>
                       <option value="61" >B</option>
                       <option value="51" >C1</option>
                       <option value="41" >C2</option>
                       <option value="21" >D</option>
                       <option value="1">E</option>
                       <?php } ?>
                     </select>


               <!-- <input type="text" name="cutoffmarks" id="cutoffmarks" class="form-control th" style="text-align:right;" minlength="1" maxlength="2" placeholder="Marks " value="<?php if($campuscutoffmarks) echo $campuscutoffmarks->campus_gd_cutoffmarks;?>" required="required" > -->
               <?php echo form_error("cutoffmarks");?>
             </div>
             <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12" style="background-color: white;">
             </div>

             <br> 
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 

             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tableGDScore" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
                <thead>
                 <tr>
                  <th class="text-center" style="width: 30px;"  class="text-center">S. No.</th>
                  <th>Category</th>
                  <th>Campus Name</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Email Id</th>
                  <th>Stream</th>
                  <th>Percentage</th>
                  <th>Sociometry Score</th>
                  <th>Recruiters Score</th>
                  
                  <th>GD Score</th>
                  <th style="width: 30px;" class="text-center">Send Intimation</th>
                </tr> 
              </thead>
              <?php  if(count($selectedcandidatedetails)==0){ ?>
              <tbody>
              </tbody>
              <?php  }else{

               $i=0; foreach ($selectedcandidatedetails as $key => $value) { 

                 $selectedcandidategdscore =  $this->model->getSelectCandidGDScore($value->candidateid);
                 ?>
                 <tbody>
                  <tr>
                    <td class="text-center"><?php echo $i+1; ?></td>
                    <td><?php echo $value->categoryname;?></td>
                    <td><?php echo $value->campusname;?></td>
                    <td> <input type="hidden" name="candidatefname[<?php echo $value->candidateid;?>]" value="<?php echo $value->candidatefirstname;?>"><input type="hidden" name="candidatemname[<?php echo $value->candidateid;?>]" value="<?php echo $value->candidatemiddlename;?>"><input type="hidden" name="candidatelname[<?php echo $value->candidateid;?>]" value="<?php echo $value->candidatelastname;?>">
                     <?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
                     <td><?php echo $value->gender;?></td>
                     <td><input type="hidden" name="emailid[<?php echo $value->candidateid;?>]" value="<?php echo $value->emailid;?>"> <?php echo $value->emailid;?></td>
                     <td><?php echo $value->stream;?></td>
                     <td> <input type="text" name="gdpercentage[<?php echo $value->candidateid;?>]" id="gdpercentage_<?php echo $value->candidateid;?>" 
                      value="<?php   if(!empty($selectedcandidategdscore->gdpercentage)) echo $selectedcandidategdscore->gdpercentage; ?>" class="form-control "  style="text-align:right; max-width: 90px;" onblur="selectedpercentage(this.id)" >
                    </td>
                    <td class="text-center">
                      <select name="ssscore[<?php echo $value->candidateid;?>]" id="ssscore_<?php echo $value->candidateid;?>"  
                       " class="form-control isNumberKey" style="text-align:right; max-width:90px;" minlength="1"  maxlength="2"  >
                       <option value="">Grade</option>
                       <?php if (count($selectedcandidategdscore)== 1) { ?>
                       <option value="81" <?php if($selectedcandidategdscore->ssscore==81) {?> selected<?php } ?>>A(81-100)</option>
                       <option value="61" <?php if($selectedcandidategdscore->ssscore==61) {?> selected<?php } ?>>B(61-81)</option>
                       <option value="51" <?php if($selectedcandidategdscore->ssscore==51 ) {?> selected<?php } ?>>C1(51-60)</option>
                       <option value="41" <?php if($selectedcandidategdscore->ssscore==41 ) {?> selected<?php } ?>>C2(41-50)</option>
                       <option value="21" <?php if($selectedcandidategdscore->ssscore==21 ) {?> selected<?php } ?>>D(21-40)</option>
                       <option value="0" <?php if($selectedcandidategdscore->ssscore==1) {?> selected<?php } ?>>E(0-20)</option>
                       <?php } else {?>
                       <option value="81">A(81-100)</option>
                       <option value="61" >B(61-81)</option>
                       <option value="51" >C1(51-60)</option>
                       <option value="41" >C2(41-50)</option>
                       <option value="21" >D(21-40)</option>
                       <option value="1">E(0-20)</option>
                       <?php } ?>
                     </select>

                   </td>
                   <td class="text-center">

                     <select name="rsscore[<?php echo $value->candidateid;?>]" id="rsscore_<?php echo $value->candidateid;?>"  
                      class="form-control isNumberKey" style="text-align:right; max-width: 90px;" minlength="1"  maxlength="2"   onchange="return GetLowerValue(this.id);">
                      <option value="">Grade</option>
                      <?php if (count($selectedcandidategdscore)== 1) { ?>
                      <option value="81" <?php if($selectedcandidategdscore->rsscore=='81') {?> selected  <?php } ?>>A(81-100)</option>
                      <option value="61" <?php if($selectedcandidategdscore->rsscore=='61') {?> selected  <?php } ?>>B(61-81)</option>
                      <option value="51" <?php if($selectedcandidategdscore->rsscore=='51') {?> selected  <?php } ?>>C1(51-60)</option>
                      <option value="41" <?php if($selectedcandidategdscore->rsscore=='41') {?> selected  <?php } ?>>C2(41-50)</option>
                      <option value="21" <?php if($selectedcandidategdscore->rsscore=='21') {?> selected  <?php } ?>>D(21-40)</option>
                      <option value="1" <?php if($selectedcandidategdscore->rsscore=='1') {?> selected  <?php } ?>>E(0-20)</option>
                      <?php } else {?>
                      <option value="81">A(81-100)</option>
                      <option value="61">B(61-81)</option>
                      <option value="51">C1(51-60)</option>
                      <option value="41">C2(41-50)</option>
                      <option value="21">D(21-40)</option>
                      <option value="1">E(0-20)</option>

                      <?php } ?>
                    </select>
                  </td>
                  

                  <td class="text-center">

                    <input type="hidden" name="gd[<?php echo $value->candidateid;?>]" id="gd_<?php echo $value->candidateid;?>" required="required"
                    value="<?php if(isset($selectedcandidategdscore->ssscore)) { echo $selectedcandidategdscore->gdscore;} ?>">
                    <?php if (count($selectedcandidategdscore)== 1) { ?>
                    <input type="text" name="gdscore[<?php echo $value->candidateid;?>]" id="gdscore_<?php echo $value->candidateid;?>" required="required"
                    value="<?php if($selectedcandidategdscore->gdscore==81) echo 'A'; else if($selectedcandidategdscore->gdscore==61) echo 'B'; else if($selectedcandidategdscore->gdscore==51) echo 'C1';else if($selectedcandidategdscore->gdscore==41) echo 'C2'; else if($selectedcandidategdscore->gdscore==21) echo 'D';else if($selectedcandidategdscore->gdscore==1) echo 'E'; ?>" class="form-control isNumberKey" readonly style="text-align:right; max-width: 90px;" minlength="1"  maxlength="2">
                    <?php } else { ?>
                    <input type="text" name="gdscore[<?php echo $value->candidateid;?>]" id="gdscore_<?php echo $value->candidateid;?>" required="required" readonly
                    value="" class="form-control isNumberKey"  style="text-align:right; max-width: 90px;" minlength="1"  maxlength="2">
                    <?php } ?>

                  </td>

                  <td>
                   <div class="checkbox checkbox-success">


                    <?php 
                  // print_r($selectedcandidategdscore);
                  // echo $value->gdemail_status;

                 // echo $selectedcandidategdscore->gdscore;
                    if($selectedcandidategdscore)
                    { ?>
                       <input id="checkbox_<?php echo $value->candidateid;?>"
                     <?php if(floor($selectedcandidategdscore->gdpercentage) !='') { ?>
                    
                     <?php if (!empty($value->gdemail_status) && $value->gdemail_status==1) {
                      echo 'checked="checked"';
                      echo 'disabled="disabled"';
                    } if (floor($selectedcandidategdscore->gdpercentage) < $selectedcandidategdscore->cutoffmarks) {
                      echo 'disabled="disabled"';
                    }   

                  } ?> type="checkbox"  name="candidateintimation[]"  value="<?php echo $value->candidateid; ?>" onclick="get_sendintimation(this.id)">
                  <?php } else{ ?>
                  <input id="checkbox_<?php echo $value->candidateid;?>" type="checkbox"  name="candidateintimation[]" value="<?php echo $value->candidateid; ?>" disabled="disabled" onclick="get_sendintimation(this.id)">
                  <?php } ?>
                  <label for="checkbox_<?php echo $value->candidateid;?>"  title="Click here to select candidate for mail " class="filled-in" ></label>
                </div>
              </td>

            </tr>
          </tbody>
          <?php $i++; }  } ?>

        </table>
      </div>
      <?php  if(count($selectedcandidatedetails) > 0){ ?>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer" style="text-align: right;">
        <button  type="submit" name="save_gdscore"  id="idsave_gdscore" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save" value="save">Save</button>
        <button  type="submit" name="send_intimation"  id="send_intimation" class="btn btn-warning btn-sm m-t-10 waves-effect" data-toggle="tooltip" disabled="disabled" title="Send Intimation" value="SendIntimation" >Send Intimation</button>
        <a href="<?php echo site_url("Gdscore");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
      </div>
      <?php  } ?>
    </div>
  </form> 
</div>
</div>
</div>  
<?php } } ?> 
</section>
<!--  JS Code Start Here  -->
<script type="text/javascript">

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tableGDScore').DataTable({
     "bPaginate": true,
     "bInfo": true,
     "bFilter": true,
     "bLengthChange": false
   }); 



  });

  $(document).ready(function () {
  //called when key is pressed in textbox
  $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
       return false;
     }
   });

});


  function GetLowerValue(id){

    var indno =  id.replace('rsscore_','');


    var rsscore = $('#rsscore_'+indno).val();
    var ssscore = $('#ssscore_'+indno).val();
    //var gdscore=$('#gd_'+indno);
    var gdscore = $('#gdscore_'+indno);
    var minvalue = Math.min(rsscore,ssscore);
    $gd=$('#gd_'+indno).val(minvalue);

    if(minvalue==81)
    {
     minvalue='A';
   }
   else if(minvalue==61)
   {
    minvalue='B';
  }
  else if(minvalue==51)
  {
    minvalue='C1';

  }
  else if(minvalue==41)
  {
    minvalue='C2';
  }
  else if(minvalue==21)
  {
    minvalue='D';
  }
  else if(minvalue==1)
  {
    minvalue='E';
  }

  // alert("minvalue"+minvalue);
  $("#id option:selected").val();

  gdscore.val(minvalue);

 // $( "#gdscore_ option:selected"+indno ).text();

 var gdscore1=$('#gdscore_'+indno).val();
 var cutoffmarks=$("#cutoffmarks").val();
 if (!empty(gdscore1)) {
  if(cutoffmarks > gdscore1)
  {
    alert("cutoffmarks shuould not less than gdscore1");
  }
}
}

  function selectedpercentage(id)
  {
  
  var res = id.split("_");
  if ($('#'+id).val() > 0) { 
  $('#rsscore_'+res[1]).attr("required", true);
  $('#ssscore_'+res[1]).attr("required", true);
  $('#gdscore_'+res[1]).attr("required", true);
  $('#gd_'+res[1]).attr("required", true);
  
  
 
  }
  else
  {
    $('#rsscore_'+res[1]).attr("required", false);
     $('#ssscore_'+res[1]).attr("required", false);
     $('#gdscore_'+res[1]).attr("required", false);
      $('#gd_'+res[1]).attr("required", true);
  }

   var indno =  id.replace('gdpercentage_','');
 


   var gdpercentage = $('#gdpercentage_'+indno).val();

   $('#gdpercentage_'+indno).val();
   
   var percentage=$('#gdpercentage_'+indno).val();
   
   var newpercantage = 0; 



     if(percentage>=81)
    {
     newpercantage=81;
   }
   else if(percentage>=61 && percentage<81)
   {
     newpercantage=61;
  }
  else if(percentage>=51 && percentage<61)
  {
     newpercantage=51;

  }
  else if(percentage>=41 && percentage<51 )
  {
    newpercantage=41;
  }
  else if(percentage>=21 && percentage<41)
  {
    newpercantage=21;
  }
  else if(percentage>=1 && percentage<21)
  {
    newpercantage=1;
  }
   

     $( "#ssscore_"+indno).val(newpercantage);
    //  $( "#ssscore_"+indno+" option [value= "+ newpercantage +"]");

 




  }
 function GetValue(id){

  var indno =  id.replace('rsscore_','');
  

  var rsscore = $('#rsscore_'+indno).val();
  var ssscore = $('#ssscore_'+indno).val();
  
  var gdscore = $('#gdscore_'+indno);
  var minvalue = Math.min(rsscore,ssscore);
  //alert("minvalue"+minvalue);

  gdscore.val(minvalue);
  var gdscore1=$('#gdscore_'+indno).val();
  var cutoffmarks=$("#cutoffmarks").val();
  
  if(cutoffmarks > gdscore1)
  {
      alert("cutoffmarks shuould not less than gdscore");
  }

}




function get_sendintimation(id){

  var indno =  id.replace('checkbox_','');
  var checkintimation = $('#checkbox_'+indno);

  var gdscore = $('#gdscore_'+indno).val();

  if (gdscore !=0 || gdscore != '') {
   $('#checkbox_'+indno).attr('disabled', false)
   $('#send_intimation').prop('disabled', false);

 }else{
   $('#checkbox_'+indno).attr('disabled', true)
   $('#send_intimation').prop('disabled', true);
 }


           // var intimationid = $('#checkbox_'+indno).val();




         }

         $(document).ready(function () {
          $('.sendintimationcheck').click(function() {

            checked = $("input[type=checkbox]:checked").length;

            if($("input[type=checkbox]:checked")=="checked") {
             $('#send_intimation').prop('disabled', false);
           }else{
            $('#send_intimation').prop('disabled', true);
            return false;
          }

        });
        });


         var checker = document.getElementById('checkme');
         var sendbtn = document.getElementById('sendNewSms');
         checker.onchange = function() {
          sendbtn.disabled = !!this.checked;
        };

        $('.th').keypress(function (event) {
          var keycode = event.which;
          if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
            event.preventDefault();
          }
        });


      </script>






      <!--  JS Code End Here  -->
      <style type="text/css">
      .checkbox {
        padding-left: 20px;
      }

      .checkbox label {
        display: inline-block;
        vertical-align: middle;
        position: relative;
        padding-left: 5px;
      }

      .checkbox label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 17px;
        height: 17px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 3px;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
      }

      .checkbox label::after {
        display: inline-block;
        position: absolute;
        width: 16px;
        height: 16px;
        left: 0;
        top: 0;
        margin-left: -20px;
        padding-left: 3px;
        padding-top: 1px;
        font-size: 11px;
        color: #555555;
      }

      .checkbox input[type="checkbox"] {
        opacity: 0;
        z-index: 1;
      }

      .checkbox input[type="checkbox"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
      }

      .checkbox input[type="checkbox"]:checked + label::after {
        font-family: 'FontAwesome';
        content: "\f00c";
      }

      .checkbox input[type="checkbox"]:disabled + label {
        opacity: 0.65;
      }

      .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed;
      }

      .checkbox.checkbox-circle label::before {
        border-radius: 50%;
      }

      .checkbox.checkbox-inline {
        margin-top: 0;
      }

      .checkbox-primary input[type="checkbox"]:checked + label::before {
        background-color: #428bca;
        border-color: #428bca;
      }

      .checkbox-primary input[type="checkbox"]:checked + label::after {
        color: #fff;
      }

      .checkbox-danger input[type="checkbox"]:checked + label::before {
        background-color: #d9534f;
        border-color: #d9534f;
      }

      .checkbox-danger input[type="checkbox"]:checked + label::after {
        color: #fff;
      }

      .checkbox-info input[type="checkbox"]:checked + label::before {
        background-color: #5bc0de;
        border-color: #5bc0de;
      }

      .checkbox-info input[type="checkbox"]:checked + label::after {
        color: #fff;
      }

      .checkbox-warning input[type="checkbox"]:checked + label::before {
        background-color: #f0ad4e;
        border-color: #f0ad4e;
      }

      .checkbox-warning input[type="checkbox"]:checked + label::after {
        color: #fff;
      }

      .checkbox-success input[type="checkbox"]:checked + label::before {
        background-color: #5cb85c;
        border-color: #5cb85c;
      }

      .checkbox-success input[type="checkbox"]:checked + label::after {
        color: #fff;
      }

      .radio {
        padding-left: 20px;
      }

      .radio label {
        display: inline-block;
        vertical-align: middle;
        position: relative;
        padding-left: 5px;
      }

      .radio label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 17px;
        height: 17px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 50%;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out;
        transition: border 0.15s ease-in-out;
      }

      .radio label::after {
        display: inline-block;
        position: absolute;
        content: " ";
        width: 11px;
        height: 11px;
        left: 3px;
        top: 3px;
        margin-left: -20px;
        border-radius: 50%;
        background-color: #555555;
        -webkit-transform: scale(0, 0);
        -ms-transform: scale(0, 0);
        -o-transform: scale(0, 0);
        transform: scale(0, 0);
        -webkit-transition: -webkit-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        -moz-transition: -moz-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        -o-transition: -o-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        transition: transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
      }

      .radio input[type="radio"] {
        opacity: 0;
        z-index: 1;
      }

      .radio input[type="radio"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
      }

      .radio input[type="radio"]:checked + label::after {
        -webkit-transform: scale(1, 1);
        -ms-transform: scale(1, 1);
        -o-transform: scale(1, 1);
        transform: scale(1, 1);
      }

      .radio input[type="radio"]:disabled + label {
        opacity: 0.65;
      }

      .radio input[type="radio"]:disabled + label::before {
        cursor: not-allowed;
      }

      .radio.radio-inline {
        margin-top: 0;
      }

      .radio-primary input[type="radio"] + label::after {
        background-color: #428bca;
      }

      .radio-primary input[type="radio"]:checked + label::before {
        border-color: #428bca;
      }

      .radio-primary input[type="radio"]:checked + label::after {
        background-color: #428bca;
      }

      .radio-danger input[type="radio"] + label::after {
        background-color: #d9534f;
      }

      .radio-danger input[type="radio"]:checked + label::before {
        border-color: #d9534f;
      }

      .radio-danger input[type="radio"]:checked + label::after {
        background-color: #d9534f;
      }

      .radio-info input[type="radio"] + label::after {
        background-color: #5bc0de;
      }

      .radio-info input[type="radio"]:checked + label::before {
        border-color: #5bc0de;
      }

      .radio-info input[type="radio"]:checked + label::after {
        background-color: #5bc0de;
      }

      .radio-warning input[type="radio"] + label::after {
        background-color: #f0ad4e;
      }

      .radio-warning input[type="radio"]:checked + label::before {
        border-color: #f0ad4e;
      }

      .radio-warning input[type="radio"]:checked + label::after {
        background-color: #f0ad4e;
      }

      .radio-success input[type="radio"] + label::after {
        background-color: #5cb85c;
      }

      .radio-success input[type="radio"]:checked + label::before {
        border-color: #5cb85c;
      }

      .radio-success input[type="radio"]:checked + label::after {
        background-color: #5cb85c;
      }
    </style>