<section class="content">
	<br>
	<div class="container-fluid">
		<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');

		if(!empty($tr_msg)){ ?>
			<div class="content animate-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){?>
				<div class="content animate-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>


				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel thumbnail shadow-depth-2 listcontainer" >
							<div class="panel-heading">
								<div class="row">
									<h4 class="col-md-10 panel-title pull-left">Manage Probation </h4>

								</div>
								<hr class="colorgraph"><br>
							</div>
							<div class="panel-body">
								<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
									<thead>
										<tr>
											<th  class="text-center" style="width: 45px;">S. No.</th>
											<th >Employ Code</th>
											<th >Name</th>
											<th class="text-center">Date of Appointment </th>
											<th class="text-center"> Period Review From Date</th>
											<th class="text-center"> Period Review To Date </th>
											<th>Sender Name</th>
											<th class="text-center">Request Date</th>
											<th class="text-center">Supervisor Request </th>
											<th class="text-center">Stage</th>
											<th id="hraction" class="text-center" style="width: 115px;">Action</th>
										</tr>
									</thead>
									<?php if (count($staff_probation_details) == 0) { ?>
										<tbody>
											<tr>
												<td colspan="15" style="color: red;">Record not found</td>
											</tr>
										</tbody>
									<?php }else{?>

										<tbody>
											<?php 
											$i=0;
											
											foreach($staff_probation_details as $row){ 
												


												$date = explode(" ",$row->Requestdate);
											// echo $date[0];
												?>


												<tr>
													<td class="text-center"><?php echo $i+1; ?></td>
													<td ><?php echo $row->emp_code; ?></td>
													<td ><?php echo $row->name; ?></td>
													<td class="text-center"><?php echo $this->gmodel->changedatedbformate($row->date_of_appointment); ?></td>
													<td class="text-center"><?php echo $this->gmodel->changedatedbformate($row->date_of_appointment); ?></td>
													<td class="text-center"><?php echo $this->gmodel->changedatedbformate($row->probation_date); ?></td>
													<td><?php echo $row->sendername;?></td>
													<td class="text-center"><?php echo $this->gmodel->changedatedbformate($date[0]);?></td>
													<td class="text-center">
														<?php if ($row->Request == 'Probation - Complete') { ?>
															<span class="badge badge-pill badge-success col-md-12"	>
																<?php echo $row->Request; ?>	
															</span>

														<?php } elseif ($row->Request == 'Probation - Extend') {?>
															<span class="badge badge-pill badge-info col-md-12"	>
																<?php echo $row->Request; ?>	
															</span>
														<?php } elseif ($row->Request == 'Pending') {?>
															<span class="badge badge-pill badge-warning col-md-12"	>
																<?php echo $row->Request; ?>	
															</span>
														 <?php } elseif ($row->Request != '') { ?>
															<span class="badge badge-pill badge-danger col-md-12"	>
																<?php echo $row->Request; ?>	
															</span>

														<?php } ?>
													</td>
													<td class="text-center">


														<?php if($row->status==1 )
														{ ?>
															<span class="badge badge-pill badge-info col-md-12"	>
																<?php echo $row->flag; ?>	
															</span>	
														<?php }elseif ($row->status==4 or $row->status==6 or $row->status==8) { ?>	
															<span class="badge badge-p$rowill badge-danger col-md-12"	>
																<?php echo $row->flag; ?>	
															</span>	
														<?php }elseif ($row->status==3 || $row->status==5 || $row->status==7) { ?>	
															<span class="badge badge-pill badge-primary col-md-12"	>
																<?php echo $row->flag; ?>	
															</span>	
														<?php }elseif ($row->status==9) { ?>	
															<span class="badge badge-pill badge-success col-md-12"	>
																<?php echo $row->flag; ?>	
															</span>	
														<?php }?>


													</td>
													<td class="text-center">
														<?php if ($this->loginData->RoleID==17 || ($row->status==1) || $this->loginData->RoleID==10) { ?>
															
															<a href="<?php echo base_url().'Probation_tc_reviewofperformance/add/'.$row->transid;?>" class="btn btn-primary btn-xs" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff transfer.">Review form</a>
														<?php }else{ ?>
															<a href="<?php echo base_url().'Probation_tc_reviewofperformance/view/'.$row->transid;?>" class="btn btn-primary btn-xs" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff transfer.">View </a>
															
														<?php } ?>
													</td>
												</tr>
												<?php $i++; } ?>
											</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
					</div>


					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="panel thumbnail shadow-depth-2 listcontainer" >
								<div class="panel-heading">
									<div class="row">
										<h4 class="col-md-10 panel-title pull-left">Midterm Review</h4>

									</div>
									<hr class="colorgraph"><br>
								</div>
								<div class="panel-body">
									<table id="tbldesignations1" class="table table-bordered table-striped table-hover dataTable js-exportable">
										<thead>
											<tr>
												<th  class="text-center" style="width: 45px;">#</th>
												<th class="text-center" >Employ Code</th>
												<th class="text-center" >Name</th>
												<th class="text-center" >Date of Appointment </th>
												<th class="text-center" >Sender Name</th>
												<th class="text-center" >Request Date</th>
												<th class="text-center" >Status</th>

												<th id="hraction" colspan="2" class="text-center" >Action</th>
											</tr>
										</thead>
										<?php if (count($staff_details)==0) {?>
											<tbody>
												<tr>
													<td colspan="8" style="color:red;" >Record not found!!!!</td>
												</tr>
											</tbody>

										<?php }else { ?>
											<tbody>
												<?php
												
												$i=0; foreach($staff_details as $row){ 
													?>
													<tr>
														<td class="text-center"><?php echo $i+1; ?></td>
														<td class="text-center" ><?php echo $row->emp_code; ?></td>
														<td class="text-center" ><?php echo $row->name;?></td>
														<td class="text-center" ><?php echo $this->gmodel->changedatedbformate($row->date_of_appointment); ?></td>
														<td class="text-center" ><?php echo $row->sendername; ?></td>
														<td class="text-center" ><?php echo $row->Requestdate; ?></td>
														<!-- <td ><?php echo $this->gmodel->changedatedbformate($row->probation_date); ?></td> -->
														<td class="text-center">
															<?php 

															if($row->status==1 ||$row->status==3 || $row->status==5||$row->status==7||$row->status==9) {?>
																<span class="badge badge-pill badge-info"><?php  echo $row->flag;?></span><?php  }

																else 
																	if($row->status==4 || $row->status==6||$row->status==8)
																		{?>
																			<span class="badge badge-pill badge-danger"><?php  echo $row->flag; ?>
																		</span>
																	<?php } ?>
																</td>
																<?php 
																if ($row->levelname == 4) { ?>
																	<td class="text-center" ><?php if ($row->trans_flag == 3||$row->trans_flag == 4||$row->trans_flag == 5||$row->trans_flag == 7) { ?>
																		<a href="<?php echo site_url('Probation_reviewofperformance/view/'.$row->transid);?>" class="btn btn-primary btn-xs"> Mid Term Reviewed </a>

																	<?php }  else
																	{
																		?>
																		<a href="<?php echo site_url('Probation_reviewofperformance/add/'.$row->transid);?>" class="btn btn-primary btn-xs"> Mid Term Review</a>

																	<?php } ?>

																</td>
															<?php } ?>
														</tr>
														<?php $i++; } ?>
													</tbody>
												<?php } ?>
											</table>
										</div>
									</div>
								</div>
							</div>

						</section>
						<script>
							$(document).ready(function() {
								$('[data-toggle="tooltip"]').tooltip(); 
								$('#tbldesignations').DataTable({
									"paging": true,
									"search": true,
								});

								$('#tbldesignations1').DataTable({
									"paging": true,
									"search": true,
								});
							});
							function confirm_delete() {

								var r = confirm("Do you want to delete this Designations");

								if (r == true) {
									return true;
								} else {
									return false;
								}

							}
						</script>]