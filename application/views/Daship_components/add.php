<br/>
<section class="content">
  <div class="container-fluid"  style="font-family: 'Oxygen' !important;" >
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">DAship Components</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>

     <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>


          <!-- Exportable Table -->

          <form name="Dashipcomponents" id="Dashipcomponents" action="" method="POST" enctype="multipart/form-data" >
            <div class="body">
              <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Name</label>&nbsp;&nbsp;&nbsp;
                    <?php echo $daship_details->candidatefirstname.' '.$daship_details->candidatemiddlename.' '.$daship_details->candidatelastname; ?>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Code</label>&nbsp;&nbsp;&nbsp;
                  <?php echo $daship_details->emp_code; ?>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
             <div class="form-group">
              <div class="form-line">
                <label for="TeamName" class="field-wrapper required-field">Batch</label>&nbsp;&nbsp;&nbsp;
                <?php echo $daship_details->batch; ?>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <?php  if ($fetch_method=='edit') { ?>
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><b>Phase </b></div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">

             <select name="phase" id="showphase" class="form-control" required="required" >
              <option value="">Select Phase</option>
              <?php   foreach($phase_details as$key => $value) { 
                if ($value->id == $dashipdocumentdetails->phaseid) {?>
                  <option value="<?php echo $value->id;?>" SELECTED><?php echo $value->phase_name;?></option>
                <?php }else{ ?>
                  <option value="<?php echo $value->id;?>"><?php echo $value->phase_name;?></option>
                <?php } } ?>
              </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <a href="<?php echo  site_url().'datafiles/dashipcomponent/'.$dashipdocumentdetails->encrypted_document_name;?>" download ><?php echo $dashipdocumentdetails->original_document_name;?></a>
              <input type="hidden" name="encrypted_phase_document" id="encrypted_phase_document" value="<?php echo $dashipdocumentdetails->encrypted_document_name;?>">
              <input type="hidden" name="original_phase_document" id="original_phase_document" value="<?php echo $dashipdocumentdetails->original_document_name;?>">
            </div>
          <?php }else if ($fetch_method=='view') { ?>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><b>Phase-Activity</b></div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">
              <select name="phase" id="showphase" class="form-control" disabled="disabled" >
                <option value="">Select Phase-Activity</option>
                <?php   foreach($phase_details as$key => $value) { 
                  if ($value->id == $dashipdocumentdetails->phaseid) {?>
                    <option value="<?php echo $value->id;?>" SELECTED><?php echo $value->phase_name;?></option>
                  <?php }else{ ?>
                    <option value="<?php echo $value->id;?>"><?php echo $value->phase_name;?></option>
                  <?php } } ?>
                </select>

              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="<?php echo  site_url().'datafiles/dashipcomponent/'.$dashipdocumentdetails->encrypted_document_name;?>" download ><i class="fa fa-download" aria-hidden="true" data-toggle="tooltip" title="<?php echo $dashipdocumentdetails->original_document_name;?>"         
                  ></i> </a>
                </div>
              <?php }else{ ?>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><b>Phase-Activity</b></div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">
                  <?php //echo "<pre>"; print_r($phase_details); die; ?>
                  <select name="phase" id="showphase" class="form-control" required="required">
                    <option value=" ">Select Phase-Activity</option>
                    <?php  
                    foreach($phase_details as $key => $value) { ?>

                      <option value="<?php echo $value->phaseid;?>"><?php echo $value->phase_name;?></option>
                    <?php }  ?>
                  </select>
                  <?php echo form_error('phase'); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
              <?php } ?>
            </div>

            <div class="row" id="dvphase_first_seven_day_orintation" style="display: none;">
              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
                <br/>
                <b>Report</b><span style="color:red;">*</span>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                <br/>
                <div class="form-group">
                  <input type="file" class="form-control-file" name="phase_document" id="phase_document" accept="application/msword" required="required">
                  <?php echo form_error('phase_document'); ?>
                </div>
              </div>
            </div>
          </div>

          <div class="panel-footer text-right">
            <div class="row">
            <?php  if ($fetch_method=='view') { ?>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <a href="<?php echo site_url("Daship_components");?>" class="btn btn-dark btn-sm md-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
              </div>
            <?php }else{ ?>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect"> Save & Submit </button>
               <a href="<?php echo site_url("Daship_components");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
             </div>

           <?php } ?>
         </div>
       </div>
     </div>
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
   </form>
 </div>
</div>
</div>
</section>




<script type="text/javascript">
  $(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip(); 
   $("#showphase").change(function(){
    $("#dvphase_first_seven_day_orintation").show();
    // var phaseid = $(this).val();
   
    // if (phaseid == 1) {
    //   $("#dvphase_first_seven_day_orintation").show();
    // } else if(phaseid == 2) {
    //   $("#dvphase_first_seven_day_orintation").show();
    // }else if(phaseid == 4) {
    //   $("#dvphase_first_seven_day_orintation").show();
    // }
  });
 });
</script>
