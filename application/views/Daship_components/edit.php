<section class="content">

  
    <div class="container-fluid"  style="font-family: 'Oxygen' !important;" >
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">DAship Components</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <?php //print_r($dashipcomp_details); ?>
 <form name="Dashipcomponents" id="Dashipcomponents" action="" method="POST" enctype="multipart/form-data" novalidate>
        <!-- Exportable Table -->
        <div class="row clearfix">
         
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           
              
                <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                   <div class="form-group">
                    <div class="form-line">
                      <label for="TeamName" class="field-wrapper required-field">Name</label>
                      <?php echo $daship_details->candidatefirstname.' '.$daship_details->candidatemiddlename.' '.$daship_details->candidatelastname; ?>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Code</label>
                    <?php echo $daship_details->emp_code; ?>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Batch</label>
                  <?php echo $daship_details->batch; ?>
                </div>
              </div>
            </div>

          </div>
          <?php //print_r($dashipdocumentdetails);?>
          <div class="row">
           
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><b>Phase </b></div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">
              <select name="phase" id="showphase" class="form-control" required="required" >
               <?php foreach($phase_details as$key => $value) { 
                if ($value->id==$dashipdocumentdetails->phaseid) {
                  ?>
                  <option value="<?php echo $value->id; ?>"><?php echo $value->phase_name; ?></option>
                <?php } } ?>
                
              </select>

              
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <a href="<?php echo  site_url().'datafiles/dashipcomponent/'.$dashipdocumentdetails->encrypted_document_name;?>" download >
                <i class="fa fa-download" style="font-size:20px" aria-hidden="true" 
                data-toggle="tooltip" title="<?php echo $dashipdocumentdetails->original_document_name;?>" ></i>
              </a>
              <input type="hidden" name="encrypted_phase_document" id="encrypted_phase_document" value="<?php echo $dashipdocumentdetails->encrypted_document_name;?>">
              <input type="hidden" name="original_phase_document" id="original_phase_document" value="<?php echo $dashipdocumentdetails->original_document_name;?>">
            </div>
            
            
          </div>

          <div class="row" id="dvphase_first_seven_day_orintation" style="display: none;">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
              <br/>
             <b>Report</b><span style="color:red;">*</span>
           </div>
           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
             <br/>
             <div class="form-group">
              <input type="file" class="form-control-file" name="phase_document" id="phase_document" required="required">
              
            </div>
          </div>
        </div>
   
  </div>
  
 
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right">
     <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect"> Save & Submit </button>
     <a href="<?php echo site_url("Daship_components");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
   </div>

 
</div>
</form>
</div>
</div>

<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    var phaseid = $("#showphase").val();
    if (phaseid == 1) {
      $("#dvphase_first_seven_day_orintation").show();
    } else if(phaseid == 2) {
      $("#dvphase_first_seven_day_orintation").show();
    }else if(phaseid == 3) {
      $("#dvphase_first_seven_day_orintation").show();
    }

    $("#showphase").change(function(){
      var phaseid = $(this).val();
      if (phaseid == 1) {
        $("#dvphase_first_seven_day_orintation").show();
      } else if(phaseid == 2) {
        $("#dvphase_first_seven_day_orintation").show();
      }else if(phaseid == 3) {
        $("#dvphase_first_seven_day_orintation").show();
      }
    });
  });
</script>

