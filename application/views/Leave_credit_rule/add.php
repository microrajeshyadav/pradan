<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave_credit_rule" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
            <form method="POST" action="">
              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-7 panel-title pull-left">Add leave Credit Period</h4>
                   <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Financial Year:<span style="color: red;" >*</span></label>
                    <select id="Cyear" name="Cyear" class="form-control" required>
                     <?php
                     $dates = range(date('Y'), '2016' );
                     foreach($dates as $date){
                        if (date('m', strtotime($date)) <= 6) {//Upto June
                          $year = ($date-1) . '-' . $date;
                        } else {//After June
                          $year = $date . '-' . ($date + 1);
                        }
                        echo "<option value='$year'>$year</option>";
                      }
                      ?>
                    </select>
                  </div>
                  <?php echo form_error("leave");?>
                </div>

                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">From Month<span style="color: red;" >*</span></label>
                    
                    <select name="datefrom" id="datefrom" class="form-control" required>
                     <option value="">Select</option>
                     <option value="01">Apr</option>
                     <option value="02">May</option>
                     <option value="03">Jun</option>
                     <option value="04">Jul</option>
                     <option value="05">Aug</option>
                     <option value="06">Sep</option>
                     <option value="07">Oct</option>
                     <option value="08">Nov</option>
                     <option value="09">Dec </option>
                     <option value="10">Jan</option>
                     <option value="11">Feb</option>
                     <option value="12">Mar</option> 
                   </select>
                 </div>
                 <?php echo form_error("leave");?>
               </div>


               <div class="form-group">
                <div class="form-line">
                  <label for="StateNameEnglish" class="field-wrapper required-field">To <span style="color: red;" >*</span></label>
                  <select name="dateto"  id="dateto" class="form-control" required>
                  <option value="">Select</option>
                     <option value="01">Apr</option>
                     <option value="02">May</option>
                     <option value="03">Jun</option>
                     <option value="04">Jul</option>
                     <option value="05">Aug</option>
                     <option value="06">Sep</option>
                     <option value="07">Oct</option>
                     <option value="08">Nov</option>
                     <option value="09">Dec </option>
                     <option value="10">Jan</option>
                     <option value="11">Feb</option>
                     <option value="12">Mar</option>  
                 </select>
               </div>
               <?php echo form_error("leaveaccured");?>
             </div>
             <div class="form-group">
              <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">Credit<span style="color: red;" >*</span></label>
                <input type="number"   name="credit" id="credit" class="form-control" data-toggle="tooltip"  maxlength="2" minlength="" title="Credit"   placeholder="Enter Credit" required min="1" max="99">  </input>
              </div>
              <?php echo form_error("leaveaccured");?>
            </div>
            <div class="form-group">
              <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">Status <span style="color: red;" >*</span> </label>
                <?php   

                        //$options = array('' => 'Select Status');     
                $options = array('1' => 'Active', '0' => 'InActive');
                echo form_dropdown('status', $options, set_value('stateid'), 'class="form-control"'); 
                ?>
              </div>
              <?php echo form_error("status");?>
            </div>
          </div>
          <div class="panel-footer text-right"> 
           <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
           <a href="<?php echo site_url("Leave_credit_rule");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
         </div>
       </div>
     </form>
   </div>
 </div>
 <!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>

<script type="text/javascript">

 $(document).ready(function(){

  $('.date-picker').datepicker( {
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'mm-yy',
    onClose: function(dateText, inst) { 
      $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    }
  });


}); 
$('#dateto').change(function(){
      validatedropdown();
    });
$('#datefrom').change(function(){
      validatedropdown();
  });
function validatedropdown ()
{
  var from = $('#datefrom').val();
  var to = $('#dateto').val();
  if (to != '')
  {
    from = parseInt(from);
    to = parseInt(to);
    if (to <= from)
    {
      alert ('To month can not be before or equal to from month.');
       $( "#dateto" ).val('');
       $( "#dateto" ).focus();
    }
  } 
}    

</script>

