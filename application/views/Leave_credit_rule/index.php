<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave_credit_rule" && $row->Action == "index"){ ?>
 <br>
 <div class="container-fluid">
<?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <?php  //print_r($state_details); ?>
      <!-- Exportable Table -->
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-10 panel-title pull-left">Credit Rule List</h4>
               <div class="col-md-2 text-right">

                 <a data-toggle="tooltip" data-placement="bottom" title="Want to add new credit rule? Click on me." href="<?php echo site_url("Leave_credit_rule/add/")?>" class="btn btn-primary btn-sm">Add New Credit Period  </a>

               </div>
             </div>
             <hr class="colorgraph"><br>
           </div>
           <div class="panel-body">



             <table id="tblsyslanguage" class="table table-bordered table-striped table-hover dataTable js-exportable">
              <thead>
                <tr>
                  <th class="text-center" style="width: 50px;">S.NO</th>
                  <th class="text-center">Financial Year</th>
                  <th class="text-center">From</th>
                  <th class="text-center">To</th>
                  <th class="text-center">Credit</th>
                  <!-- <th class="text-center"  style="width: 100px;">Status</th> -->
                  <th class="text-center"  style="width: 50px;">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php

                   // print_r($staffcategory_details);
                $i=0; foreach($Leave_credit as $row){ ?>
                <tr>
                  <td class="text-center"><?php echo $i+1; ?></td>
                  <td class="text-center"><?php echo $row->CYear; ?></td>

                  <td class="text-center"> <?php 
                  echo $row->from_month;
                  ?></td>
                  <td class="text-center">
                    <?php echo $row->to_month;
                    ?>
                  </td>
                  <td class="text-center"><?php echo $row->credit_value; ?></td>
                 <!--  <td class="text-center"><?php  if ($row->status==0) {
                    echo '<span class = "label label-success">Active</span>';
                  }else{
                   echo '<span class = "label label-danger">InActive</span>';
                 } ?></td> -->
                 <td class="text-center">
                  <a href="<?php echo site_url('Leave_credit_rule/edit/'.$row->id);?>" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this leave credit rule."><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>
                  |
                  <a href="<?php echo site_url('Leave_credit_rule/delete/'.$row->id);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to delete."><i class="fa fa-trash" style="color:red"></i></a>
                </td>      

              </tr>
              <?php $i++; } ?>
            </tbody>
          </table>       

        </div>
      </div>
    </div>
  </div>
  <!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>
<script>
 $(document).ready(function() {
  $('#tblsyslanguage').DataTable({
    "paging": false,
    "search": false,
  });
});
 function confirm_delete() {

  var r = confirm("Are you sure you want to delete this item?");

  if (r == true) {
    return true;
  } else {
    return false;
  }

}
</script>