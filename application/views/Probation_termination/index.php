<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">LETTER INFORMING EMPLOYEE ABOUT TERMINATION OF SERVICE DUE TO UNSATISFACTORY PERFORMANCE DURING THE PERIOD OF PROBATION</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row" style="line-height: 3">
        <div class="col-md-6"> 
          Ref: Personal Dossier of Employee
        </div>
        <div class="col-md-6 pull-right text-right"> 
          Date: _______________________
        </div>
      </div>
      <div class="row" style="line-height: 3; margin-top:50px;">
        <div class="col-md-12"> 
          To
        </div>
        <div class="col-md-12"> 
          Name of Probationer: ____________________________
        </div>

        <div class="col-md-12"> Name of Probationer: ____________________________</div>
        <div class="col-md-12"> Employee Code: __________</div>
        <div class="col-md-12"> Designation: _____________________________</div>
        <div class="col-md-12"> Location: ________________________</div>
        
        <div class="col-md-12 text-center" style="margin-top: 50px;"> <h5>Through the Supervisor<h5></div>
          <div class="col-md-12 text-left" style="margin-top: 50px;">Subject: <strong>Termination of Service Due to Unsatisfactory Performance During the Period of Probation</strong></div>
          <div class="col-md-12">Dear ___________________,</div>
          <br/>
          <div class="col-md-12">
            <p>You were appointed in PRADAN as ___________________ (designation) with effect from ____________ (date)were under probation for a period of six months from the said date as per our Offer of Appointment No.____________ dated _________ duly accepted by you.</p>
          </div>
          <div class="col-md-12">
            <p>Based on the review of your performance during the period of probation, I am sorry to inform you that we are not in a position to confirm completion of your probation. I therefore write to inform you that we would no longer require your services with effect from the afternoon of ___________ (date).</p>
          </div>
          <div class="col-md-12">
            <p>I am marking a copy of this letter to the Finance-Personnel-MIS Unit, so that they may help you settle your account.  Dues, if any, will be payable after you have furnished ‘NO DUES CERTIFICATE’ from all concerned.</p>
          </div> <br/>
          <div class="col-md-12">Yours sincerely,</div>          
          <div class="col-md-12">(__________________)</div>
          <div class="col-md-12">Executive Director</div>

          <div class="col-md-12">cc: - Team Coordinator</div>
          <div class="col-md-12">- Integrator</div>
          <div class="col-md-12">- Finance-Personnel-MIS Unit</div> 
        </div>
      </div>
      <div class="panel-footer text-right">
        <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
        <a href="" class="btn btn-dark btn-sm"> Go Back</a>
      </div>
    </div>
  </div>
</section>