<br/>
<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

          <!-- Exportable Table -->
          <form name="joinDA" id="Joincandidatetoda" action="" method="post">
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="panel thumbnail shadow-depth-2 listcontainer" >
                  <div class="panel-heading">
                    <div class="row">

                     <h4 class="col-md-10 panel-title pull-left">Change DA Field Guide/ Placement</h4>
                     <div class="col-md-12 text-right" style="color: red">
                      * Denotes Required Field  
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table id="tablejoininglist" class="table table-bordered table-striped  dataTable js-exportable">
                        <thead>
                          <tr>
                           <th style ="max-width:70px;" class="text-center">Sr.No</th>
                           <th>Campus Name</th>
                           <th class="align-top">Name</th>
                           <th>Gender</th>
                           <th>Email Id</th>
                           <th>Mobile</th>
                           <!-- <th>Degree</th> -->
                           <th>Batch</th>
                           <th>Field guide Name</th>
                           <th>Office</th>
                           <th>Development Cluster</th>
                           <th class="text-center">Photo</th>
                           <th class="text-center">Action</th>
                         </tr>
                       </thead>
                       <?php 
                       if (count($candidatebdfformsubmit)==0) { ?>
                       <tbody>
                       </tbody>
                       <?php    
                     }else{ ?>
                     <?php $i=1; foreach($candidatebdfformsubmit as $key => $val){ ?>
                     <tbody>
                      <tr>
                        <td class="text-center" style ="max-width:30px;">
                          <?php echo $i;?></td>
                          <td class="text-center"><?php echo $val->campusname; ?></td>
                          <td class="text-center"><?php echo $val->name; ?></td>
                          <td><?php echo $val->gender;?></td>
                          <td><?php echo $val->emailid;?></td>
                          <td ><?php echo $val->mobile;?></td>
                          <!-- <td ><?php echo $val->mobile;?></td> -->
                          <td><?php echo $val->batch;?></td>
                          <td><?php echo $val->fieldguidename;?></td>
                          <td><?php echo $val->officename;?></td>
                          <td><?php echo $val->dc_name;?></td>
                          <td class="text-center" style="border:solid 2px #e1e1e1; width:105px; height:133px; padding: 3px;">

                           <?php
                           if(!empty($val->encryptedphotoname)) 
                           {
                            $image_path='datafiles/'. $val->encryptedphotoname;


                            if (file_exists($image_path)) {

                             ?>
                             <img src="<?php  echo site_url().'datafiles/'. $val->encryptedphotoname;?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                             
                             <?php }
                             else
                             {
                               ?>

                               <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                               
                               <?php } 
                             }
                             else
                               {?>
                                 <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                                 <?php } ?>

                               </td>
                               <td class="text-center" style="">
                                 <a  data-toggle="tooltip" title = "Change Field Guide" class="btn btn-info btn-sm" href="<?php echo site_url().'Changefieldguide/add/'.$val->candidateid;?>"><i class="fa fa-street-view" aria-hidden="true"></i>

                                 </a>
                                 <a data-toggle="tooltip" title = "Change Placement" class="btn btn-info btn-sm" href="<?php echo site_url().'Da_placementchangeletter/add/'.$val->candidateid;?>"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                 </a>
                               </td>
                             </tr>
                           </tbody>
                           <?php $i++; } } ?>
                           
                         </table>
                       </div>
                     </div>
                   </div>

                   
                 </div>
               </div>
             </form>
           </div>
           <!-- #END# Exportable Table -->
           
         </section>
         <script type="text/javascript">
           $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();  
            $('#tablejoininglist').DataTable(); 

            $('#Joincandidatetoda').submit(function() {
              amIChecked = false;
              $('input[type="checkbox"]').each(function() {
                if (this.checked) {
                  amIChecked = true;
                }
              });
              if (amIChecked == false) {
               
                alert('please check one checkbox!');
                return false;
              }
              
            });

          });   
           
        </script>