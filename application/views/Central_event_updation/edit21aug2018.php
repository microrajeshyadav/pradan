<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Central_event" && $row->Action == "add"){ ?>
    <div class="container-fluid">
      <!-- Exportable Table -->
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">
           <div class="header">
            <h2>Central Event Updateion</h2><br>
          </div>
             <?php 
                $tr_msg= $this->session->flashdata('tr_msg');
                $er_msg= $this->session->flashdata('er_msg');

                if(!empty($tr_msg)){ ?>
                  <div class="content animate-panel">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="hpanel">
                          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('tr_msg');?>. </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } else if(!empty($er_msg)){?>
                    <div class="content animate-panel">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="hpanel">
                            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo $this->session->flashdata('er_msg');?>. </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                     
          <form name="centralevent" id="centralevent" method="post" enctype="multipart/form-data" action="">
            <div class="body">
              <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                     <div class="form-group">
                      <div class="form-line">
                         <label for="TeamName" class="field-wrapper required-field">Name <span style="color:red;">*</span></label>
                        <select class="form-control" id="central_event_name" name="central_event_name[]"  readonly="readonly"  required="required">
                        <?php $i=0; foreach($centralevent_details as $key => $value) { 
                            if ($value->eventid == $singleresouceperson_details->name){?>
                            <option value="<?php echo $value->eventid;?>" selected ><?php echo $value->name;?></option>
                       
                      <?php } $i++; } ?>
                </select>
                      </div>
                      <?php echo form_error("central_event_name");?>
                    </div>
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                   <div class="form-group">
                    <div class="form-line">
                      <label for="TeamName" class="field-wrapper required-field">From Date<span style="color:red;">*</span></label>
                      <input type="text" name="central_event_from_date" id="central_event_from_date" value="<?php echo $this->model->changedatedbformate($singleresouceperson_details->from_date);?>" class="form-control " readonly="readonly"> 
                    </div>
                    <?php echo form_error("central_event_from_date");?>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">To Date<span style="color:red;">*</span></label>
                    <input type="text" name="central_event_to_date" id="central_event_to_date" value="<?php echo $this->model->changedatedbformate($singleresouceperson_details->to_date);?>" class="form-control " readonly="readonly">
                  </div>
                  <?php echo form_error("central_event_to_date");?>
                </div>
              </div>
            </div>
            <?php //print_r($paticipants_details); 
           //echo  $paticipants_details[0]->participant_id;
          
            // echo $resouceperson_list[0]->resouce_person_id; ?>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Resouce Person <span style="color:red;">*</span></label>
                   <select class="form-control" id="resouce_person" name="resouce_person[]" multiple="true" readonly="readonly"  required="required">
                  <?php $i=0; foreach($resouceperson_details as $key => $value) { 

                      if ($value->id == $resouceperson_list[$i]->resouce_person_id) { 
                    ?>
                      <option value="<?php echo $value->id;?>" selected="selected" ></option><?php echo $value->name;?></option>
                <?php } else{ ?>
                   <option value="<?php echo $value->id;?>" ><?php echo $value->name;?></option>
                <?php } $i++; } ?>
                </select>


                  
                </div>
              </div>
            </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                  <div class="form-group">
                    <div class="form-line">
                      <label for="TeamName" class="field-wrapper required-field">Place <span style="color:red;">*</span></label> 
                      <textarea name="central_event_place" id="central_event_place" class="form-control"  required="required" readonly="readonly"> <?php echo $singleresouceperson_details->place;?></textarea>
                    </div>
                  </div>
                </div>
                </div>
               <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">
               <div class="form-group">
                <div class="form-line">
                  <div class="container w3-animate-right" style="display: none;">
                 <label for="TeamName" class="field-wrapper required-field">Upload Document <span style="color:red;">*</span></label> 
                 <input type="file" name="upload_material[]" id="upload_material" class="form-control" multiple="true">
                 </div>

                </div>
              </div>
            </div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
             <?php
             $i=0;
           foreach($document_details as $row) { ?>
               
            <span><a href="<?php echo site_url().'datafiles/centraleventdocument/'. $row->encrypted_document_upload; ?>" target=" _blank" data-toggle="tooltip" data-placement="top" title="<?php echo $row->original_document_upload; ?>!"><i class="fa fa-download" aria-hidden="true"></i></a>
           <input type="hidden" name="old_encrypted_upload_material[]" id="old_encrypted_upload_material_<?php echo $i;?>" value="<?php echo $row->encrypted_document_upload; ?>">
          <input type="hidden" name="old_original_upload_material[]" id="old_original_upload_material_<?php echo $i;?>" value="<?php echo $row->original_document_upload; ?>" >
            </br></span> 
            <?php $i++; } ?>
        </div>
          
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                  <div class="form-group">
                    <div class="form-line">
                      <label for="TeamName" class="field-wrapper required-field">Paticipants <span style="color:red;">*</span></label> 
                       <select class="form-control" id="paticipants" name="paticipants[]" multiple="true" required="required">
                  <?php $i=0; foreach($fin_year_batch_da as $key => $value) { 
                      if ($value->candidateid == $paticipants_details[$i]->participant_id) {
                    ?>
                      <option value="<?php echo $value->candidateid;?>" selected="selected" ><?php echo $value->candidatefirstname.''. $value->candidatemiddlename.' '. $value->candidatelastname;?></option>
                <?php } else{ ?>
                   <option value="<?php echo $value->candidateid;?>" ><?php echo $value->candidatefirstname.''. $value->candidatemiddlename.' '. $value->candidatelastname;?></option>
                <?php } $i++; } ?>
                </select>
                    </div>
                  </div>
                </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                   <button type="submit" class="btn btn-primary  btn-sm m-t-10 waves-effect">Save </button>
                   <a href="<?php echo site_url("Central_event");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                 </div>
               </div>
             </div>
           </form>
         </div>
       </div>
       <!-- #END# Exportable Table -->
     </div>
   <?php } } ?>
 </section>
 <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});


  $("#btnRemoveRow").click(function() {
    if($('#tblForm09 tr').length-1>1)
      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);
    
    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    
  });


  var srNoGlobal=0;
  var inc = 0;

  function insertRows(count) {
    srNoGlobal = $('#tbodyForm09 tr').length+1;
    var tbody = $('#tbodyForm09');
    var lastRow = $('#tbodyForm09 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inc++
      cloneRow = lastRow.clone();
      var tableData = '<tr>'
      + '<td><select class="form-control" name="DAname['+inc+']" required="required">'
      + '<option value="">Select DA Name</option>'
      <?php foreach ($candidate_details as $key => $value): ?>
        + '<option value=<?php echo $value->candidateid;?>><?php echo $value->candidatefirstname;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'
      + '<input type="date" name="fromdate['+inc+']" id="fromdate['+inc+']" value="" class="form-control datepicker" data-toggle="tooltip" maxlength="50" title="Enter From Date!" placeholder="Enter From Date" >'
      + '</td>'
      + '<td><input type="date" class="form-control datepicker"'; 
      +'name="todate['+inc+']" id="todate['+inc+']" value=" " maxlength="100" required="required" />'
      + '</td>'
      + '</tr>';
      $("#tbodyForm09").append(tableData)
    }

  }
  //insertRows();
</script>
<script>

  'use strict';

  ;( function( $, window, document, undefined )
  {
    // feature detection for drag&drop upload

    var isAdvancedUpload = function()
      {
        var div = document.createElement( 'div' );
        return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
      }();


    // applying the effect for every form

    $( '.box' ).each( function()
    {
      var $form    = $( this ),
        $input     = $form.find( 'input[type="file"]' ),
        $label     = $form.find( 'label' ),
        $errorMsg  = $form.find( '.box__error span' ),
        $restart   = $form.find( '.box__restart' ),
        droppedFiles = false,
        showFiles  = function( files )
        {
          $label.text( files.length > 1 ? ( $input.attr( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name );
        };

      // letting the server side to know we are going to make an Ajax request
      $form.append( '<input type="hidden" name="ajax" value="1" />' );

      // automatically submit the form on file select
      $input.on( 'change', function( e )
      {
        showFiles( e.target.files );

        
      });


      // drag&drop files if the feature is available
      if( isAdvancedUpload )
      {
        $form
        .addClass( 'has-advanced-upload' ) // letting the CSS part to know drag&drop is supported by the browser
        .on( 'drag dragstart dragend dragover dragenter dragleave drop', function( e )
        {
          // preventing the unwanted behaviours
          e.preventDefault();
          e.stopPropagation();
        })
        .on( 'dragover dragenter', function() //
        {
          $form.addClass( 'is-dragover' );
        })
        .on( 'dragleave dragend drop', function()
        {
          $form.removeClass( 'is-dragover' );
        })
        .on( 'drop', function( e )
        {
          droppedFiles = e.originalEvent.dataTransfer.files; // the files that were dropped
          showFiles( droppedFiles );
          
        });
      }


      // if the form was submitted

      $form.on( 'submit', function( e )
      {
        
        // preventing the duplicate submissions if the current one is in progress
             var cat = $('#Category').val();
              var subcat = $('#Sub_category').val();
              var blockcode = $('#BlockCode').val();
                $('#Category1').val(cat);
                $('#Sub_category1').val(subcat);
                $('#BlockCode1').val(blockcode);


        if (cat =='') {
              alert('Please Select Category !!!');
              return false;
        }else{


       
        if( $form.hasClass( 'is-uploading' ) ) return false;

        $form.addClass( 'is-uploading' ).removeClass( 'is-error' );

        if( isAdvancedUpload ) // ajax file upload for modern browsers
        {
          e.preventDefault();

          // gathering the form data
          var ajaxData = new FormData( $form.get(0) );
          

          if( droppedFiles )
          {
            $.each( droppedFiles, function( i, file )
            {
              ajaxData.append( $input.attr( 'name' ), file );
            
            });
          }


          // ajax request
          $.ajax(
          {
            url:      $form.attr( 'action' ),
            type:     $form.attr( 'method' ),
            data:       ajaxData,
            dataType:   'json',
            cache:      false,
            contentType:  false,
            processData:  false,
            complete: function()
            {
              $form.removeClass( 'is-uploading' );
            },
            success: function( data )
            { 
              console.log(data);
              $form.addClass( data.success == true ? 'is-success' : 'is-error' );
              if( !data.success ) $errorMsg.text( data.error );
            },
            error: function()
            {
              alert( 'Error. Please, contact the webmaster!' );
            }
          });
        
       }
        else // fallback Ajax solution upload for older browsers
        {
          var iframeName  = 'uploadiframe' + new Date().getTime(),
            $iframe   = $( '<iframe name="' + iframeName + '" style="display: none;"></iframe>' );

          $( 'body' ).append( $iframe );
          $form.attr( 'target', iframeName );

          $iframe.one( 'load', function()
          {
            var data = $.parseJSON( $iframe.contents().find( 'body' ).text() );
            $form.removeClass( 'is-uploading' ).addClass( data.success == true ? 'is-success' : 'is-error' ).removeAttr( 'target' );
            if( !data.success ) $errorMsg.text( data.error );
            $iframe.remove();
          });
        }
      }
    });


      // restart the form if has a state of error/success

      $restart.on( 'click', function( e )
      {
        e.preventDefault();
        $form.removeClass( 'is-error is-success' );
        $input.trigger( 'click' );
      });

      // Firefox focus bug fix for file input
      $input
      .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
      .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });

  })( jQuery, window, document );

  $(document).ready(function(){
    $('.container').css('display', 'block');
  });
</script>


<script type="text/javascript">
  $(document).ready(function(){
  $('.container').css('display', 'block');
  });

  $(function() {
  $('#block_div').hide(); 
  $('#Category').change(function(){
    // alert(Category);
    if($('#Category').val() == '4') {
      $('#block_div').show(); 
    } else {
      $('#block_div').hide(); 
    } 
  });

  $('#subcategory_div').hide(); 
  $('#Category').change(function(){
    // alert(Category);
    if($('#Category').val() == '5') {
      $('#subcategory_div').show(); 
    } else {
      $('#subcategory_div').hide(); 
    } 
  });

});

</script>
<script type="text/javascript">

   $('#Category').change(function(){
   var Category = $(this).val();
   // alert(Category);
  $.post('<?php echo site_url('File_upload/Category');?>/'+Category, {}, function(raw){
    $('#Sub_category').html(raw);
  });
   });

</script>
