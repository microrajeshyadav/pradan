
<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Central_event_updation" && $row->Action == "add"){ ?>
    <br>
    <div class="container-fluid">
      <!-- Exportable Table -->
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-8 panel-title pull-left">Add Central Event Updation</h4>
           <div class="col-md-4 text-right" style="color: red">
            * Denotes Required Field 
          </div>
        </div>
        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body">
       <?php 
       $tr_msg= $this->session->flashdata('tr_msg');
       $er_msg= $this->session->flashdata('er_msg');
       if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        
          <form name="centralevent" id="centralevent" method="post" action="" enctype="multipart/form-data">
            <div class="body">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Event Name <span style="color:red;">*</span></label>
                    <?php 
                    $options = array('' => 'Select Central Event ');
                    foreach($centralevent_details as $key => $value) {
                      $options[$value->centraleventid] = $value->name;
                    }
                    echo form_dropdown('central_event_name', $options,  set_value('central_event_name'), 'class="form-control" id="central_event_name" required="required"' );
                    ?>
                  </div>
                  <?php echo form_error("central_event_name");?>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">From Date<span style="color:red;">*</span></label>
                  <input type="text" name="central_event_from_date" id="central_event_from_date" value="" class="form-control" readonly="readonly" required="required">
                </div>
                <?php echo form_error("central_event_from_date");?>
              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
             <div class="form-group">
              <div class="form-line">
                <label for="TeamName" class="field-wrapper required-field">To Date<span style="color:red;">*</span></label>
                <input type="text" name="central_event_to_date" id="central_event_to_date" value="" class="form-control" readonly="readonly" required="required">
              </div>
              <?php echo form_error("central_event_to_date");?>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
           <div class="form-group">
            <div class="form-line">
              <label for="TeamName" class="field-wrapper required-field">Resource Person <span style="color:red;">*</span></label>
              <?php 
              $options = array('' => 'Select Resource Person ');
              foreach($resouceperson_details as$key => $value) {
                $options[$value->id] = $value->name;
              }
              echo form_dropdown('resouce_person[]', $options,  set_value('resouce_person'), 'class="form-control" id="resouce_person" multiple="multiple" height= "500"' );
              ?>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
          <div class="form-group">
            <div class="form-line">
              <label for="TeamName" class="field-wrapper required-field">Place <span style="color:red;">*</span></label> 
              <textarea name="central_event_place" id="central_event_place" class="form-control" readonly="readonly" required="required"> </textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
      <div class="form-group">
        <div class="form-line">
          <label for="TeamName" class="field-wrapper required-field">Paticipants <span style="color:red;">*</span></label> 
          <select class="form-control" id="paticipants" name="paticipants[]" multiple="true" required="required">
            <?php foreach ($fin_year_batch_da as $row) { ?>
              <option value="<?php echo $row->candidateid;?>"><?php echo $row->candidatefirstname.''. $row->candidatemiddlename.' '. $row->candidatelastname;?></option>
            <?php } ?>
          </select>
        </div>
      </div>
    </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
         <div class="form-group">
          <div class="form-line">

            <div class="container">
             <label  class="field-wrapper required-field">Material <span style="color:red;">*</span></label> 
             <input type="file" name="upload_material[]" id="upload_material" class="form-control" accept=".doc,.docx" multiple required="required">
           </div>
         </div>
       </div>
     </div>

  </div>
  <div class="panel-footer text-right">
   <button type="submit" id="submit" name="submit" class="btn btn-primary  btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
   <a href="<?php echo site_url("Central_event_updation");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
 </div>
</div>
</form>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>
<script>
 $("#central_event_name").change(function(){
  $.ajax({
    url: '<?php echo site_url(); ?>ajax/get_Central_Event_Details/'+$(this).val(),
    type: 'POST',
    dataType: 'json',
  })
  .done(function(data) {
    $.each(data, function(index) {
      console.log(data);
      var fdateSplit = data[index].from_date;
      var dateSplit = fdateSplit.split('-');
      var fromDate = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0];
      $("#central_event_from_date").val(fromDate);
      var tdateSplit = data[index].to_date;
      var todateSplit = tdateSplit.split('-');
      var toDate = todateSplit[2] + '/' + todateSplit[1] + '/' + todateSplit[0];

      $("#central_event_to_date").val(toDate);
      $("#central_event_place").val(data[index].place);          
    })
  })

  $.ajax({
    url: '<?php echo site_url(); ?>ajax/get_Central_trans_Details/'+$(this).val(),
    type: 'POST',
    dataType: 'json',
  })

  .done(function(data) {
    var ind =[];
    $.each(data, function(index) {           
      ind.push([data[index].resouce_person_id].toString());

    })
    $("#resouce_person").val(ind);

  })

  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

});


 $(document).ready(function() {

  $(".datepicker").datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   dateFormat : 'dd/mm/yy',
   yearRange: '1920:2030',
   onClose: function(selectedDate) {
    jQuery("#central_event_to_date").datepicker( "option", "minDate", selectedDate );

  }

});

  $(".datepicker").datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   dateFormat : 'dd/mm/yy',
   yearRange: '1920:2030',
   onClose: function(selectedDate) {
    jQuery("#central_event_from_date").datepicker( "option", "maxDate", selectedDate );
  }
});




});
</script>