<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Central_event_updation" && $row->Action == "index"){ ?>
<br>
    <div class="container-fluid">      
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Manage Central Event Updation</h4>
      <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to add central event? Click on me." href="<?php echo site_url()."Central_event_updation/add/";?>" class="btn btn-primary btn-sm">Add Central Event Updation</a>
        
      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

          <!-- Exportable Table -->
          <?php// print_r($centralevent_details); ?> 
          
         
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tabledaevent" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                  <th class="text-center" style="width:30px;">S.No. </th>
                  <th>Event Name </th>
                  <th>From date</th>
                  <th>To date</th>
                  <th>Place</th>
                  <th class="max-width:30px;" class="text-center">Action</th>
               </tr> 
             </thead>
             <tbody>
             <?php if (count($centralevent_list) ==0) { ?>

              <?php }else{


                $i=0;
              foreach ($centralevent_list as $key => $value) {
             ?>
              <tr>
                <td class="text-center" style="width: 50px;"><?php echo $i+1;?></td>
               <td><?php echo $value->name;?></td>
                <td><?php echo $this->model->changedatedbformate($value->from_date);?></td>
                 <td><?php echo $this->model->changedatedbformate($value->to_date);?></td>
                  <td><?php echo $value->place;?></td>
                <td><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit Central Event Updation." href="<?php echo site_url()."Central_event_updation/edit/".$value->centraleventid;?>" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete." href="<?php echo site_url()."Central_event_updation/delete/".$value->centraleventid;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i></a></td>
              </tr>
              <?php $i++; } } ?>
            </tbody>
          </table>
       </div>
</div>
</div>
</div>
</div>
<?php } } ?>
</section>




<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tabledaevent').DataTable(); 
  });
</script>  
<script>

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete  Central Event Updation?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
  
</script>