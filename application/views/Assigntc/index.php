<section class="content" style="background-color: #FFFFFF;" >
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Assigntc" && $row->Action == "index"){ 
   ?>
   <br>
   <div class="container-fluid">
    <div class="panel panel-default" >
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Generate Offer Letter </h4>
           <div class="col-md-2 text-right">
             <!--  <a data-toggle="tooltip" data-placement="bottom" title="Want to add new Assign? Click on me." href="<?php //echo site_url()."Assigntc/add";?>" class="btn btn-primary btn-sm">Add New Assigntc</a> -->
           </div>
         </div>
         <hr class="colorgraph"><br>
       </div>

       <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class=" alert-success alert-dismissable alert"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <?php   //print_r($selectedcandidatedetails); ?>
            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">
               <!--  <a href="<?php //echo site_url()."Recruiters/add/";?>" class="btn btn-sm btn-success" >Add Recruiter</a> -->
               <br>
               <br>
             </div>
           </div>
           <div class="container-fluid">
             <form name="searchassigntc" id="searchassigntc" method="POST" action="">
               <div class="row bg-light" style="padding: 10px; ">

                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 text-right">   
                 <label for="StateNameEnglish" class="field-wrapper required-field" style="padding-top: 5px;"><b>Campus Name </b></label> 
               </div>
               <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">  

                <select name="campusid" id="campusid" class="form-control" required="required">
                  <option value="">Select Campus</option>
                  <?php foreach ($campusdetails as $key => $value) {

                    $expdatefrom = explode('-', $value->fromdate);
                    $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                    $expdateto = explode('-', $value->todate);
                    $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                    $campval  = $value->campusid.'-'. $value->id; 
                    if (!empty($campusid) && $campval == $campusid) {
                     ?>
                     <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                     <?php  }else{ ?>
                     <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                     <?php } } ?>

                   </select> 
                   <?php echo form_error("campusid");?>
                 </div>

                 <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                  <button type="submit" name="search" id="search" class="btn btn-dark btn-sm-10">Search</button> </div>


                </div>
              </form>
              <form name="cand_offer" id="cand_offer" method="POST" action="">
              <div class="row" style="margin-top: 20px;">
                <div class="col-md-12">
                  <table id="tableWrittenScore"  class="table dt-responsive table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
                    <thead>
                     <tr>
                      <th style ="max-width:50px;" class="text-center">S. No.</th>
                      <th>Category</th>
                      <th>Campus Name</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Email Id</th>
                      <th>Stream</th>
                      <th> Team </th>
                      
                      <th style="<?php if ($this->loginData->RoleID ==17) { echo "display:none"; }?>"> FG </th>
                      <th style="<?php if ($this->loginData->RoleID ==17) { echo "display:none"; }?>"> Batch </th>
                      
                      <th style ="width:50px;" class="text-center">Assign Team </th>
                      <th class="text-center"><?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> </th>
                      <?php
                      if($this->loginData->RoleID != 16) {?>
                      <th class="text-center">Personnel Administrator</th><?php } ?>
                      <th class="text-center">Send <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?></th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php  
                    // print_r($selectedcandidatedetails); die;

                    $i=0;
                    if($selectedcandidatedetails){
                     foreach ($selectedcandidatedetails as $key => $value) {
                      ?>
                      <tr>
                        <td class="text-center"><?php echo $i+1; ?></td>
                        <td><?php echo $value->categoryname;?> </td>
                        <td><?php echo $value->campusname;?></td>
                        <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
                        <td><?php echo $value->gender;?></td>
                        <td><?php echo $value->emailid;?> </td>
                        <td><?php echo $value->stream;?> </td>
                        <td><?php echo $value->officename;?></td>
                       
                        <td style="<?php if ($this->loginData->RoleID ==17) { echo "display:none"; }?>"><?php echo $value->name;?></td>
                        <td class="text-center" style="<?php if ($this->loginData->RoleID ==17) { echo "display:none"; }?>"><span class="btn-warning btn-sm"><span class="badge badge-light"><?php if (!empty($value->batch)) {
                         echo strlen($value->batch)== 1?"0".$value->batch: $value->batch;
                       }  else{ echo "NA";}?></span> </span></td>

                      

                       <?php  if(!empty($value->letterid)){ ?>
                       <td class="text-center">
                        <a class="btn btn-success btn-sm" href="<?php echo site_url()."Assigntc/edit/".$value->candidateid;?>" ><i class="fas fa-user-tag" data-toggle="tooltip" data-placement="bottom" title ="Done"></i>  </a> </td>

                        <?php } elseif(  empty($value->officename)){ ?>
                        <td class="text-center">
                          <a class="btn btn-danger btn-sm" href="<?php echo site_url()."Assigntc/edit/".$value->candidateid;?>"  data-toggle="tooltip" data-placement="bottom" title ="Pending" ><i class="fas fa-user-times"></i></a> </td>
                          <?php   }else{ ?>
                          <td class="text-center">
                            <a class="btn btn-success btn-sm" href="<?php echo site_url()."Assigntc/add/".$value->candidateid.'/1';?>"  data-toggle="tooltip" data-placement="bottom" title="Done"><i class="fas fa-user-tag"></i>  </a> </td>
                            <?php  } ?>

                            <?php if ($value->flag =='Generate') { ?>
                            <td class="text-center">  
                              <a class="btn btn-success btn-sm" href="<?php echo site_url().'pdf_offerletters/'.$value->filename.'.pdf';?>"  download data-toggle="tooltip" title="Download">
                              <i class="fas fa-file-download"></i>  
                             </a>
                           </td>
                           <?php } else{ ?> 
                           <td class="text-center">  
                            <button type="button" class="btn btn-danger  btn-sm" data-toggle="tooltip" title="Generate" name="Generateofferletter" id="Generateofferletter_<?php echo  $i+1;  ?>"  
                              onclick="GenerateOfferLetter(this.id)" value="SENDTOED"><i class="fas fa-file-signature"></i> </button> </td>
                              <?php } ?>
                              <?php
                              if($this->loginData->RoleID != 16) {?>
                              <td class="text-left">
                              <?php if ($this->loginData->RoleID == 10 && $value->AdminApproval == 0) { ?>
                              <div class="form-check">                                
                                <input type="radio" class="form-check-input" name = "AdminApproval[<?php echo $value->candidateid; ?>]" id="approve" value="1" >
                                <label class="form-check-label" for="materialUnchecked">Approve</label>
                              </div> 
                              <div class="form-check">
                                <input type="radio" class="form-check-input"  name = "AdminApproval[<?php echo $value->candidateid; ?>]" id="reject" value="2">
                                <label class="form-check-label" for="materialUnchecked">Reject</label>
                              </div>
                              <?php } else{ ?>
                              <div class="form-check">                                
                                <?php if ($value->AdminApproval == "1") {?>
                                <span class="badge badge-pill badge-success col-md-12">Approve </span>
                                <?php } ?>
                              </div> 
                              <div class="form-check">
                                <?php if ($value->AdminApproval == "2") {?>
                                <span class="badge badge-pill badge-danger col-md-12">Reject </span>
                                <?php } ?>
                              </div>
                              <div class="form-check">
                                <?php if ($value->AdminApproval == "0") {?>
                                <span class="badge badge-pill badge-warning col-md-12">Pending </span>
                                <?php } ?>
                              </div>
                              <?php }?>
                            </td>
                            <?php } ?>

                              <td class="text-center">  <input type="hidden" name="candidate" id="candidateid_<?php echo $i+1;?>" value="<?php echo $value->candidateid;?>" >
                                <?php if ($this->loginData->RoleID == 16) { ?>
                                <button type="button" class="btn btn-info btn-sm" name="sendtord" id="sendtoed_<?php echo  $i+1;  ?>"  onclick="SendTOEDOfferLetter(this.id)" value="SENDTOED" data-toggle="tooltip" title="To ED"><i class="fas fa-file-export"></i></button>
                                <?php } ?>

                                <?php if ($this->loginData->RoleID != 10 && $value->AdminApproval == "1") { ?>
                                <button type="button" class="btn btn-info btn-sm" name="sendtord" id="sendtoed_<?php echo  $i+1;  ?>"  onclick="SendTOEDOfferLetter(this.id)" value="SENDTOED" data-toggle="tooltip" title="To ED"><i class="fas fa-file-export"></i></button>
                                <?php } ?>
                              </td>

                            </tr>
                            <?php $i++; } } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <?php

             if (!empty($selectedcandidatedetails) && $this->loginData->RoleID == 10)
             {?>
              <div class = "col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                  <button class="btn btn-primary btn-sm" name="submit" id="submit" value="submit"  >Submit</button>                           

                </div>
            <?php } ?>

            <!-- <?php
            //if (!empty($selectedcandidatedetails)  && $designationid == 16)
              { ?>
                <div class = "col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                  <button class="btn btn-primary btn-sm" name="submit" id="submit" value="submit"  >Submit</button>                           

                </div>
                <?php
              }
              ?> -->
              </form>
                  </div>
                 
              </div>
            </div>
          </div>  
          <?php } } ?> 
        </section>
        <!--  JS Code Start Here  -->
        <script type="text/javascript">
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();  
         //  $('#tableWrittenScore').DataTable({
         //   "bPaginate": false,
         //   "bInfo": false,
         //   "bFilter": true,
         //   "bLengthChange": false
         // }); 
         var groupColumn = 1;
         var table = $('#tableWrittenScore').DataTable({
          "columnDefs": [
          { "visible": false, "targets": groupColumn }
          ],
          "order": [[ groupColumn, 'asc' ]],
          "displayLength": 13,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group font-weight-bold text-uppercase"><td colspan="13">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );
       });





          function GenerateOfferLetter(id){

            var indno =  id.replace('Generateofferletter_','');
            var candidateid = $('#candidateid_'+indno).val();

            $.ajax({
              url: '<?php echo site_url(); ?>Ajax/getGenerateOfferLetter/'+ candidateid,
              type: 'POST',
              dataType: 'text',
            })

            .done(function(data) {

              if (data==0) {
               alert("First !!! Please Choose Team !!");
             }else if(data ==1){
              alert("Offer Letter Already Generated !!");
            }else if(data ==2){
              window.location.replace("<?php echo site_url("Generateofferletter/offerletterdetails/");?>/"+ candidateid);
            }

          })
          }

          function SendTOEDOfferLetter(id){

           var indno =  id.replace('sendtoed_','');
           var candidateid = $('#candidateid_'+indno).val();
           0

           $.ajax({

            url: '<?php echo site_url(); ?>Assigntc/sendofferlettered/'+ candidateid,
            type: 'POST',
            dataType: 'text'
          })

           .done(function(data) {

             if (data=='send') {
               alert('Send Offer Letter To ED');
             }else if(data=='sent'){
              alert('Already send Offer Letter To ED ');
            }else if(data=='fail'){
              alert('Please Try Again Email Not send');
            }else if (data=='notgeneratesend') {
              alert('Please !! First Generate Offer Letter !!!');
            }

          });

         }

       </script>

<!--  JS Code End Here  -->