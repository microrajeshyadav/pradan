<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;
  
}
textarea{
  width: 600px !important;
}


</style>
<section class="content" style="background-color: #FFFFFF;" >
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>

      
      <form role="form" method="post" action="">
        <br>
        <div class="container-fluid">
         <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
         <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-12 panel-title pull-left">STAFF GRADUATING - THE EXIT INTERVIEW FORM
             </h4>
           </div>
           <hr class="colorgraph">
         </div>
         <div class="panel-body">
           <!-- <div class="row">
             <div class="col-md-12 text-center">
              <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
            </div>
            <div class="col-md-12 text-center" style="margin-bottom: 40px;">
              <h4>Inter-Office Memo</h4>
              
            </div> 
          </div>
          <div class="row" style="line-height: 2">
            <div class="col-md-9">
              To: <label  name="employee_name"  class="inputborderbelow" > <?php echo $staff_detail->name; ?></label> <br/>
              (Name of the Graduating Employee)
            </div>
            <div class="col-md-3 text-left">
              Date: <label  name="date"  class="inputborderbelow" ><?php echo date('d/m/Y')  ?></label>
            </div>
          </div>
          

          <div class="row" style="line-height: 2">
            <div class="col-md-9">
              From: <label  name="employee_name"  class="inputborderbelow" > <?php echo  $staff_detail->superwiser; ?></label> <br/>
             (Team Coordinator)
            </div>
            <div class="col-md-3 text-left">
              File: Personal File of Apprentice
            </div>
          </div>
          <div class="row" style="line-height: 2;margin-top: 50px;">
            <div class="col-md-12 text-center">
             Subject  : <strong>The Exit Interview Form </strong>
           </div>
           
         </div>
         <div class="row" style="margin-top: 50px;">
           <div class="col-md-12 text-left">
            <p>As you plan to leave PRADAN soon, I write to request you to fill in the enclosed ‘Exit Interview Form’. We hope, through the information you would provide us in this format, to seek feedback about issues related to the organization and inputs for possible measures to change any aspect of the organization's functioning.

            </p> 
            <p>I would like to reassure you that we would treat the contents of this form as confidential, if you so desire, and use it only for organizational learning.</p>

            <p>I would like to reassure you that we would treat the contents of this form as confidential, if you so desire, and use it only for organizational learning.</p>
            <p>
              We do hope that you make a smooth transition to your new assignment, and sincerely wish you the very best in your life.
            </p>
            <p>
              With regards.
            </p>
            <p style="margin-top: 50px;">
             ( __________________________ )
           </p>
           <p>
            Encl:<strong> As above</strong>
          </p>


        </div> -->
        
      </div>


      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 40px;">
        <h4>THE EXIT INTERVIEW FORM</h4>
        <p>(to be filled by Employees on graduat)</p>
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 40px;">
        <p><strong>Note:</strong> This form is meant to elicit your candid impressions about the apprenticeship programme, with a view to further improve the state of affairs of the programme. 
  
  </p>
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 40px;">
        <h4>PART - I</h4>
      </div> 
    </div>
    <div class="row" style="line-height: 2">
      <div class="col-md-12">
        Name of the Apprentice: <input type="text" name="employee_name" value="<?php echo $staff_detail->name; ?>" class="inputborderbelow" required>
      </div>
      <div class="col-md-12 text-left">
      Apprentice No: <input type="text" name="employee_id" class="inputborderbelow" value="<?php echo $staff_detail->staffid;?>" >
      </div> 
      <div class="col-md-12 text-left">
        Location: <input type="text" name="employee_location" class="inputborderbelow" value="<?php echo $staff_detail->newoffice;?>" >
      </div>     
     
    
    
    <div class="col-md-12">
    Date of Joining Apprenticeship: <label class="inputborderbelow" > <?php echo $this->gmodel->changedatedbformate($join_relive_data->joniningdate); ?></label>
   </div>
   <div class="col-md-12 text-left">
      Date of Completion: <input type="text" name="employee_code" value="<?php echo  $this->gmodel->changedatedbformate($join_relive_data->leavingdate); ?>" class="inputborderbelow" required>
    </div>
    
  
  

</div>
<div class="row" style="line-height: 2 ; margin-top: 50px;">
  <div class="col-md-12 text-center">
    <h4>PART - II</h4>
    <p>(To be filled by the Graduating Apprentice)</p>
  </div>
</div>
<div class="row" style="line-height: 2 ; margin-top: 50px;">

  <div class="col-md-12" style="margin-top: 20px;">
    A.  Please list THREE things that you have liked the <strong><i>most as Apprentice</i></strong>:
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    (i)&nbsp;&nbsp;&nbsp;<textarea class="inputborderbelow" cols="50" name="da_liked_most_1" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_liked_most_1;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
  (ii)&nbsp;&nbsp;<textarea class="inputborderbelow" name="da_liked_most_2" cols="50" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_liked_most_2;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
  (iii)&nbsp;<textarea class="inputborderbelow" name="da_liked_most_3" cols="50" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_liked_most_3;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    B.  Please list THREE things that you have liked the <strong><i>least as Apprentice</i></strong>:
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    (i)&nbsp;&nbsp;&nbsp;<textarea class="inputborderbelow" cols="50" name="da_liked_least_1" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_liked_least_1;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
  (ii)&nbsp;&nbsp;<textarea class="inputborderbelow" name="da_liked_least_2" cols="50" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_liked_least_2;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
  (iii)&nbsp;<textarea class="inputborderbelow" name="da_liked_least_3" cols="50" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_liked_least_3;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    C.  Please give at least THREE suggestions for improving the state of affairs of the apprenticeship programme:
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    (i)&nbsp;&nbsp;&nbsp;<textarea class="inputborderbelow" cols="50" name="da_suggestion_1" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_suggestion_1;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
  (ii)&nbsp;&nbsp;<textarea class="inputborderbelow" name="da_suggestion_2" cols="50" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_suggestion_2;} ?></textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
  (iii)&nbsp;<textarea class="inputborderbelow" name="da_suggestion_3" cols="50" maxlength="255" required><?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->da_suggestion_3;} ?></textarea>
  </div>

  

</div>

<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12" style="margin-bottom: 20px; ">
    <strong>(Signature of the Development Apprentice) </strong>
  </div>
  <div class="col-md-6 text-left">
    <label class="inputborderbelow"> 

      <?php 

if($da_image->encrypted_signature)
{

    $file_path=''; 
                            $file_path=FCPATH.'datafiles/signature/'. $da_image->encrypted_signature;
                            
                            if(file_exists($file_path))
                            {
                          ?>
                     
                         <img src="<?php echo site_url().'datafiles/signature/'. $da_image->encrypted_signature;?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
                         
                         <?php } 
                          else
                        {
                          ?>
                           <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
                           <?php 
                        }
}
else
{
  ?>
  <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
  <?php
}

    ?></label> 
  </div>
  <div class="col-md-6 text-right">
    Date : <input type="text" name="date_below_sign" value="<?php echo date('d/m/Y'); ?>" class="inputborderbelow" required> 
  </div>
  

</div>
<hr/>

<div class="panel-footer text-right">
  <?php
  if(empty($staff_sepemployeeexitform)){
    ?>
    <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
    <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
    <?php
  }else{
    if($staff_sepemployeeexitform->flag == 0){
     ?>
     <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
     <?php } ?>
     <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
     <?php } ?>
     <?php if($this->loginData->RoleID == 16) {?>
     
     <a href="<?php echo site_url("Staff_dashboard");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
     <?php }
     else { ?>
     <a href="<?php echo site_url("Staff_dashboard");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
     
     <?php } ?>
   </div>
 </div>
</div>
</form>
</section>

<?php 
if($staff_sepemployeeexitform){
  if($staff_sepemployeeexitform->flag == 1){ ?>
  <script type="text/javascript">
    $(document).ready(function(){
     
      $('#saveandsubmit').hide() ;
      $("form input[type=text]").prop("disabled", true);

      $("form input[type=radio]").prop("disabled", true);
      $("form textarea").prop("disabled", true);
      $("form textarea").css("width", "600px;");
      $("form textarea").attr("style", "width:600px;");
    });
  </script>
  <?php } }?>