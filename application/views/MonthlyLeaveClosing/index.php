<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">   
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12  panel-title pull-left">Monthly Leave Closing</h4>

         
       </div>
       <hr class="colorgraph"><br>
     </div>


     <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="row bg-light text-black" style="padding: 15px;">
            <form method="POST" action="">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px;">
                <h5>Search Filters:</h5>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group col-md-3">
                  Office: 
                  <select name="office" id="office" class="form-control">
                    <option value="0">All</option> 
                   <?php foreach($office as $row){
                    // if($staffofficeid->new_office_id == $row->officeid) {
                    ?>
                    <option value="<?php echo $row->officeid; ?>" SELECTED> <?php echo $row->officename; ?> </option>
                  <?php } //} ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                Select Month :
                <select class="form-control" name="month" id="month" >
                 
                  
                   <option value="01"> Jan</option>
                    <option value="02"> Feb</option>
                    <option value="03"> Mar</option>
                    <option value="04"> Apr</option>
                    <option value="05"> May</option>
                    <option value="06"> Jun</option>
                    <option value="07"> Jul</option>
                    <option value="08"> Aug</option>
                    <option value="09"> Sep</option>
                    <option value="10"> Oct</option>
                    <option value="11"> Nov</option>
                    <option value="12"> Dec</option>
                </select>
              </div>
              <div class="form-group col-md-3"> 
               Select Year :
               <select class="form-control" name="year" id="year" >
                
                <?php

                for ($i = date('Y')-1; $i <= date('Y'); $i++) {
                  echo "<option value=$i>".$i ."</option>";

                } ?>
              </select>
            </div>
            <div class="col-md-3"> 
              <button type="Submit" class="btn btn-info btn-sm pull-right mt-2" name="submit" value="search"> <i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
          </div>

          <input type="hidden"  readonly autocomplete="off" name="fromdate" id="fromdate" class="datepicker form-control" value="<?php  echo date('d/m/Y', strtotime('first day of previous month')); ?>" >

          <input autocomplete="off" readonly type="hidden" name="todate" id="todate" value="<?php echo date('d/m/Y', strtotime('last day of previous month')); ?>" class="datepicker form-control">


          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">

          </div>

        </form>
      </div>
      <hr/>
      <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <form action="" method="post">
            <input type="hidden" name="hdnOfficeID" id="hdnOfficeID">
            <table id="tblstaffleave" class="table table-bordered table-striped wrapper">
              <thead>
               <tr>
                 <th class="text-center">S. No.</th>
                 <th>Office</th>
                 <th>Leave Closing</th> 
               </tr> 
             </thead>


             <tbody>              

              <?php 
                  // echo "<pre>";
                  // print_r($staffleavedetails);
              if (count($staffleavedetails) !=0) {

                $i=0;
                foreach ($staffleavedetails as $key => $value) {

                 ?>
                 <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td class="text-center"><?php echo $value->officename;?></td>
                  <td class="text-center">                          
                    <?php if($value->stage == 2){ ?>
                      <label class="badge badge-pill badge-success">Yes</label>
                    <?php } 
                    else{ ?>
                      <label class="badge badge-pill badge-danger">No</label>
                    <?php } ?>
                  </td>
                </tr>
                <?php $i++; }} ?>
              </tbody>

            </table>
          </form>
        </div>
      </div> 
    </div>
  </div>
</div>   

</section>

<script>
                    /*$(document).ready(function(){
                      $('[data-toggle="tooltip"]').tooltip();  
                      $('#tablecampus').DataTable(); 
                    });*/
                  </script>  


   <!--  <script type="text/javascript">
     $(document).ready(function(){
      $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
    });
  </script> -->
  <script type="text/javascript">

    $("#month").on('change', function() {
      // alert(1);
      $("#year").val("");
    });

    $("#year").on('change', function() {
     var mon = $("#month").val();
     // alert(mon);
     if(mon !='' && $("#year").val() !='')
     {
      var firstDay =  new Date( $("#year").val(),  $("#month").val(), 1);
      var curr_day = firstDay.getDate();
      var curr_month = firstDay.getMonth();
      var curr_year = firstDay.getFullYear();
      firstDay = curr_day +'/' + curr_month + '/' +  curr_year;   
      $("#fromdate").val(firstDay);     
      var lday = lastday(curr_year,mon);      
      var  latsdate = lday +'/' + curr_month + '/' +  curr_year;
      $("#todate").val(latsdate);
      // alert($("#todate").val());
    }
    else
    {
      alert('Please select month first!');
    }
    

  });

    var lastday = function(y,m){
      return  new Date(y, m , 0).getDate();
    }
    $(document).ready(function(){

      $('[data-toggle="tooltip"]').tooltip();
      $(".datepicker").datepicker({
       changeMonth: true,
       changeYear: true,
       maxDate: 'today',
       yearRange: '2000:2030',
       dateFormat : 'dd/mm/yy',
     });  

    });

    $(document).ready(function() {

      $('#hdnOfficeID').val($('#office').val());




      $('#tblstaffleave').DataTable();

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
        table.order( [ groupColumn, 'desc' ] ).draw();
      }
      else {
        table.order( [ groupColumn, 'asc' ] ).draw();
      }
    } );
  } );


    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }


  }


</script>
<script type="text/javascript">
  $(document).ready(function(){

    // $("#office").change(function(){
    //   var office_id = $(this).val();
    //   var data;
    //   $.ajax({
    //     data:{office_id:office_id},

    //     url: '<?php echo site_url(); ?>Ajax/staff_list/',
    //     type: 'POST'

    //   })
    //   .done(function(data) {

    //     var obj=JSON.parse(data);

    //     $.each(obj, function(index) {

    //       $('#staff').append($('<option></option>').text(obj[index].name).val(obj[index].staff_id));
    //       var option='';

    //     });

    //   })

    // });
  });

</script>