<section class="content">
   <?php foreach ($role_permission as $row) { if ($row->Controller == "State" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                 <h4 class="header" class="field-wrapper required-field" style="color:green">Resource Person </h4>
                 <div class="col-sm-12">
                   <form method="POST" action="">
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Name :</label>
                        <input type="text" class="form-control" name="resource_person_name" maxlength="50" minlength="3" id="resource_person_name" placeholder="Please Enter Person Name" value="<?php echo set_value('resource_person_name'); ?>"  required="required">
                      </div>
                       <?php echo form_error("resource_person_name");?>
                    </div>

                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Mobile No. :</label>
                        <input type="text" class="form-control txtNumeric" maxlength="10" name="mobile" id="mobile" placeholder="Please Enter Mobile" value="<?php echo set_value('mobile');   ?>" >
                      </div>
                        <?php echo form_error("mobile");?>
                    </div>
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Location :</label>
                        <input type="text" class="form-control" maxlength="50" minlength="3" name="location" id="location" placeholder="Please Enter Location " value="<?php echo set_value('location');   ?>" >
                      </div>
                        <?php echo form_error("location");?>
                    </div>
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Status </label>
                      <?php   

                       // $options = array('' => 'Select Status');     
                       $options = array('0' => 'Active', '1' => 'InActive');
                       echo form_dropdown('status', $options, set_value('status'), 'class="form-control" required="required"'); 
                      ?>
                      </div>
                        <?php echo form_error("status");?>
                    </div>
                      <div style="text-align: -webkit-center;"> 
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("Resourceperson");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>

  <script type="text/javascript">
$('.txtNumeric').bind("paste", function(e) {
var text = e.originalEvent.clipboardData.getData('Text');
if ($.isNumeric(text)) {
   if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
       e.preventDefault();
       $(this).val(text.substring(0, text.indexOf('.') + 3));
  }
}
else {
       e.preventDefault();
    }
});
  </script>