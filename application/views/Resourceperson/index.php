<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Resourceperson" && $row->Action == "index"){ ?>

  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


<?php  //print_r($state_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

             <div class="header">
              <h2>Manage Resource Person </h2><br>
              <a href="<?php echo site_url("Resourceperson/add/")?>" class="btn btn-success">Add New</a>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center">Id</th>
                       <th class="text-center">Resource Person Name</th>
                      <th class="text-center">Mobile No.</th>
                      <th class="text-center">Location </th>
                      <th class="text-center">Command</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=0; foreach($resource_person_details as $row){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $row->name; ?></td>
                       <td class="text-center"><?php echo $row->mobile;?></td>
                      <td class="text-center"><?php echo $row->location;?></td>
                      <td class="text-center">
                        <a href="<?php echo site_url('Resourceperson/edit/'.$row->id);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>

                        <a href="<?php echo site_url('Resourceperson/delete/'.$row->id);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>

                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>
<script>
  
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>