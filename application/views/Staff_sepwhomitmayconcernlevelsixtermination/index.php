<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;
  
}
textarea{
  width: 600px !important;
}


</style>
<section class="content" style="background-color: #FFFFFF;" >
<?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <form role="form" method="post" action="">
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE FOR (Level 5-6)  
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="col-md-12">
                 <div class="">
                  <div class="">
                    <label for="" class="field-wrapper required-field"></label>
                      <textarea class='summernote' id='summernote' name="lettercontent" rows='10' ><?php echo $content; ?></textarea>

                    </div>
                    <?php echo form_error("Content");?>
                  </div>
                </div>
<div class="panel-footer text-right">
  <div class="">
    <div class="">
  <?php
  if(empty($tbl_levelwisetermination)){
    ?>
    <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
    <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
    <?php
  }else{
    if($tbl_levelwisetermination->flag == 0){
     ?>
     <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
     <?php } ?>
     <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
     <?php } ?>
     <?php if($this->loginData->RoleID != 18){ ?>
 <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }else{ ?>
 <a href="<?php echo site_url("Ed_staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } ?>
</div>
</div>
   </div>
</div>
</div>
</form>
</section>
<?php 
if($tbl_levelwisetermination){
  if($tbl_levelwisetermination->flag == 1){ ?>
  <script type="text/javascript">
    $(document).ready(function(){
     
      $('#saveandsubmit').hide() ;
      $("form input[type=text]").prop("disabled", true);

      $("form input[type=radio]").prop("disabled", true);
      $("form textarea").prop("disabled", true);
      $("form textarea").css("width", "600px;");
      // $("form textarea").attr("style", "width:600px;");
    });
  </script>
  <?php } }?>

    <!-- include summernote -->

