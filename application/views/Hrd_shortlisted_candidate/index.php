
<style type="text/css">
   #errmsg
{
color: red;
}
 </style>
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
      <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>List of shortlist candidates for campus incharge  </b></div>
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row" style="background-color: #FFFFFF;">
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
         <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right">   
         <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
        <div class=" col-md-6 col-lg-6 col-xs-12 col-sm-12" style="background-color: white;">  
         <?php 
        $options = array('' => 'Select Campus');

        foreach($campusdetails as$key => $value) {
            $options[$value->campusid] = $value->campusname;
        }
        echo form_dropdown('campusid', $options, set_value('campusid'), 'class="form-control", required="required" id="campusid"');
        ?>
      <?php echo form_error("campusid");?>
       </div>
        <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;">
          <button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-warning">Search</button>
        </div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;"><b></b></div>
       </form>
        <br>
        <br>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
     
      <form name="formwrittenscore" id="formwrittenscore" method="post" action="" >

         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">

          <?php if (isset($searchcampusid) && $searchcampusid==0) { ?>
           <table id="writtenscoreselectecandidate" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width:100%">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Candidate</th>
              <th> Email Id</th>
           </tr> 
         </thead>
         <tbody>
          <?php 
             $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
            <tr>
              <td><?php echo $i+1; ?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->emailid;?></td>
             <?php $i++; } ?>
            </tr>
          </tbody>
        </table>
        <?php } else{  ?>
          <input type="hidden" name="campusid" value="<?php if(isset($searchcampusid)) echo $searchcampusid;?>">
          <table id="writtenscoreselectecandidate" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width:100%">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Candidate</th>
              <th> Email Id</th>
              <th> Campus Incharge Status</th>
              <th> Status </th>
           </tr> 
         </thead>
         <tbody>
          <?php 
             $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
            <tr>
              <td><?php echo $i+1; ?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->emailid;?></td>

              <td><?php 
                if (isset($shortlistcandidate[$key]->status) && $shortlistcandidate[$key]->status !='') {
                 echo ucfirst($shortlistcandidate[$key]->status);
                }
             ?></td>
              <td> 
                <?php if (isset($shortlistcandidate[$key]->hrstatus) && $shortlistcandidate[$key]->hrstatus !='') { ?>
                  <select name="status[<?php echo $value->candidateid;?>]" id="status_<?php echo $i;?>" class="form-control">
                  <option value="accepted"<?php if(isset($shortlistcandidate[$key]->hrstatus) && $shortlistcandidate[$key]->hrstatus=='accepted') {
                  echo 'selected="selected"';
                  }?>>Accepted</option>
                  <option value="rejected" <?php if(isset($shortlistcandidate[$key]->hrstatus) && $shortlistcandidate[$key]->hrstatus=='rejected') {
                  echo 'selected="selected"';
                  }?>>Rejected</option>

                <?php } else{ ?>
                <select name="status[<?php echo $value->candidateid;?>]" id="status_<?php echo $i;?>" class="form-control">
                  <option value="accepted"<?php if(isset($shortlistcandidate[$key]->status) && $shortlistcandidate[$key]->status=='accepted') {
                  echo 'selected="selected"';
                  }?>>Accepted</option>
                  <option value="rejected" <?php if(isset($shortlistcandidate[$key]->status) && $shortlistcandidate[$key]->status =='rejected') {
                  echo 'selected="selected"';
                  }?>>Rejected</option>
                </select>
              <?php } ?>
                </td>
             <?php $i++; } ?>
            </tr>
          </tbody>
        </table>
      <?php } ?>
       </div>
       <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12" style="background-color: white;"></div> 
       <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12" style="background-color: white;">
        <?php  if (count($selectedcandidatedetails) > 0) {
          ?>
         <button type="submit" class="btn btn-success" name="btnaccept" id="btnaccept" value="AcceptData" >Save</button> 
          <button type="submit" class="btn btn-success" name="btnaccept" id="btnaccept" value="SubmitData" >Submit</button> 
        <?php } ?>
        <button type="submit" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</button></div>
       </div> 

         <!-- <a href="<?php //echo site_url('Writtenscore/generate_pdf_selected_candidate/'.$searchcampusid);?>">Genratepdf</a> | 
         <a href="<?php //echo site_url('Writtenscore/send/'.$searchcampusid);?>">Send</a> -->

       </div> 
       <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12" style="background-color: white;"></div> 
      </div> 
  
     </form>
     </div>
    </div>
  </div>   
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('#writtenscoreselectecandidate').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'print'
        ]
    } );
} );
</script>

<!--  JS Code Start Here  -->
<script type="text/javascript">



  
  //$(document).ready(function(){
    //$('[data-toggle="tooltip"]').tooltip();  
    //$('#tableWrittenScore').DataTable({
      // "bPaginate": true,
       //"bInfo": true,
       //"bFilter": true,
       //"bLengthChange": true
    //}); 
  //}); 


$("#SendEmail").on("click", function(){
    
   var campid =  $("#campusid").val();
     if(campid ==''){   ////Check campus id 

    alert('First !! Please Select campus !!!');

   }else{

 $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: "<?php echo base_url(); ?>Writtenscore/generate_pdf_selected_candidate/" + campid,
      
       async : false,
       cache : false,
       dataType : 'json',
       contentType : false,
       processData : false,
       success: function(data){
        console.log(data);
         if (data) {
          //location.reload();
          //alert("Data saved successfully");
         // $("#btnsubmit").prop('disabled', false);

        }else{
          alert("Data not saved successfully");
        }
      }

    });


   }

});

 

</script>

<!--  JS Code End Here  -->