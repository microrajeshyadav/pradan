
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">  
   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">Office
       </h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
   <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <form method="POST" action="" name="experience_certificate" id="experience_certificate">
       <input type="hidden" name="staffid" value="">
       <div class="panel-body">
        <div class="row text-left" style="margin-top: 20px; ">
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table id="tableoffice" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th style ="max-width:60px;" class="text-center">S. No.</th>
              <th>Type</th>
              <th>Name</th>
              <th style="width: 85px !important;">Street </th>
              <th>State </th>
              <th>Supervisor </th>
              <!-- <th class="text-center">Closed </th> -->
              <!-- <th style ="max-width:50px;" class="text-center">Action</th> -->
            </tr> 
          </thead>
          <tbody>
            <?php 
            $i=0;
            foreach ($lpooffice as $key => $value) { ?>
            <tr>
              <td class="text-center"><?php echo $i+1;?></td>
              <td><?php echo $value->officetype;?></td>
              <td><?php echo $value->officename;?></td>
              <td><?php echo $value->street;?></td>
              <td><?php echo $value->statename;?> </td>
              <td><?php echo $value->staffname;?> </td>
              <!-- <td><?php echo $value->closed;?> </td> -->
              <!-- <td class="text-center"><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Batch." href="" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Batch." href="" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
              </a></td> -->
            </tr>
            <?php $i++; } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="panel-footer text-right">
    <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
  </div>
</form>
</div>
</div>
</section>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tableoffice').DataTable();
    }); 
</script>

