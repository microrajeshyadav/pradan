<section class="content">
 
  <br>
  
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>.

          </div>
        </div>
      </div>
    </div>
  </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <?php  //print_r($state_details); ?>
      <!-- Exportable Table -->
      <div class="container-fluid">
        <div class="panel panel-default" >
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-10 panel-title pull-left"> Staff Search</h4>
               <div class="col-md-2 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>

          <div class="panel-body">
            <form action="" method="post">
             <div class="row bg-light" style="padding: 20px; margin-bottom: 20px;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px;">
                <h5>Filters:</h5>
              </div>

              <div class="form-inline">
               <div class="form-group">

                <label for="Staff">&nbsp;Staff : &nbsp;<span style="color: red;" >*</span></label>
                <select class="form-control" id="staffid2" name="staffid2" required="">
                 <option value="">--select--</option>
                 <option value="0">ALL</option>
                 <?php foreach($staff_list as $row){?>
                 <option value="<?php echo $row->staffid;?>" <?php echo (trim($this->input->post('staffid')) == $row->staffid?"selected":"");?>><?php echo $row->emp_code .' - '. $row->name;?></option>
                 <?php } ?>
               </select>
             </div>
           </div>

           <div class="form-inline">
            <div class="form-group">
              <label for="From">&nbsp;From : &nbsp;</label>
              <input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
              id="fromdate" name="fromdate" placeholder="From Date" style="min-width: 20%;">
            </div></div>

            <div class="form-inline">
              <div class="form-group">
                <label for="To">&nbsp;To : &nbsp;</label>
                <input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                id="todate" name="todate" placeholder="To Date" style="min-width: 20%;">
              </div>
            </div> 

            <div class="form-inline" style="margin-top: 20px;">
             <div class="form-group ">

              <label>&nbsp;Type&nbsp; <span style="color: red;" >*</span></label>
              <div class="form-check form-control" required="">

                <label class="radio-inline" for="materialGroupExample1" >
                 <input type="radio" class="form-check-input" value="Transfer" id="materialGroupExample1" name="groupOfMaterialRadios" checked=""> 
               Transfer</label>
               
               <label class="radio-inline" for="materialGroupExample2">
                <input type="radio" class="form-check-input" value="Promotion" id="materialGroupExample2" name="groupOfMaterialRadios" >
              Promotion</label>

              <label class="radio-inline" for="materialGroupExample2">
                <input type="radio" class="form-check-input" value="Both" id="materialGroupExample2" name="groupOfMaterialRadios" >
              Transfer & Promotion</label>
              <label class="radio-inline" for="materialGroupExample3">
                <input type="radio" class="form-check-input" value="Sepertion" id="materialGroupExample3" name="groupOfMaterialRadios">
              Sepertion</label>
            </div>
          </div>
        </div>



          <div class="col-md-12 text-right" style="padding-top: 10px;">
            <button data-toggle='tooltip' id="search" type="button" class="btn btn-info btn-sm" value="SearchCampus" title="Click me to process your search." >
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      </form>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="table">
        </div>
      </div>
    </div>

  </div>
</div>
<!-- #END# Exportable Table -->
</div>


</section>

<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 

  });
  $(document).ready(function() {
    
    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '2000:2030',
     dateFormat : 'dd/mm/yy',
   });
  });

</script>

<script type="text/javascript">

  $(document).ready(function(){

    $("#search").click(function(){

      var staff=$('#staffid2').val();
  //var type=document.querySelector('input[name="groupOfMaterialRadios"]:checked').value;
  //alert(staff);
  if(staff !='')
  {

    var type=document.querySelector('input[name="groupOfMaterialRadios"]:checked').value;
    var from=$('#fromdate').val();
    var to=$('#todate').val();
  //alert(type);

  $.ajax({
   url:'<?php echo base_url(); ?>Search_transfer_promotion_history/fetch_data',
   method:'POST',
   datatype:'text',
   data:{staff_id:staff,types:type,froms:from,tos:to},
       done:function(data) {
   // success:function(data)
   // {
    //console.log(data);
    if(data){
     $('#table').html(data);
     $('#tbltransferprohistory').DataTable({
      "paging": true,
      "pageLength": 50,
      "search": true,
      "destroy": true

    });
   }else{
    $('#table').html(date);
  }
}
});

}
else
{
  alert("Please Select staff & Type Both");
}

});


  });



</script>



