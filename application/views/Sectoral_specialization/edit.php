<section class="content">
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Operation" && $row->Action == "edit"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                  <h4 class="header" class="field-wrapper required-field" style="color:green">Edit Sectoral Specialization</h4>
                  <div class="col-sm-12">
                    <form method="POST" action="">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="StateCode" class="field-wrapper required-field"> Sectoral Name <span style="color: red;" >*</span></label>
                          <input type="text" class="form-control" required="" id="sectoralname" name="sectoralname" minlength="3" maxlength="50" value="<?php echo $sectorspecialization_details->name;?>">
                        </div>
                         <?php echo form_error("name");?>
                      </div>
                                          
                      <div class="form-group">
                        <div class="form-line">
                          <label for="SNS" class="field-wrapper required-field">Status <span style="color: red;" >*</span></label>
                          <?php   

                        //$options = array('' => 'Select Status');     
                        $options = array('0' => 'Active', '1' => 'InActive');
                       echo form_dropdown('status', $options, $operation_details->status, 'class="form-control"'); 
                      ?>
                      </div>
                        <?php echo form_error("status");?>
                      </div>
                      <div style="text-align: -webkit-center;">
                        <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                        <a href="<?php echo site_url("Sectoral_specialization/index");?>" class="btn btn-danger btn-sm m-t-10 waves-effect">Go to List</a>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Exportable Table -->
      </div>
    <?php } } ?>
    </section>