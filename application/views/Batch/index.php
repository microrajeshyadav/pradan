<?php foreach ($role_permission as $row) { if ($row->Controller == "Batch" && $row->Action == "index"){ ?>
<br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Batch List</h4>
      <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new batch? Click on me." href="<?php echo site_url()."Batch/add";?>" class="btn btn-primary btn-sm">Add New Batch</a>
      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>


            <div class="container-fluid">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="tablecampus" class="table table-bordered table-striped dt-responsive">
                  <thead>
                   <tr>
                    <th style ="max-width:50px;" class="text-center">S. No.</th>
                    <th class="text-center">Financial Year</th>
                    <th class="text-center">Batch</th>
                    <th class="text-center">Date of Joining </th>
                      <th class="text-center">Stage </th>
                    <!-- <th  style ="max-width:70px;" class="text-center"> Status </th> -->

                    <th style ="max-width:50px;" class="text-center">Action</th>
                  </tr> 
                </thead>
                <tbody>
                  <?php 
                  $i=0;
                  foreach ($batchlist as $key => $value) { ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td class="text-center"><?php echo $value->financialyear;?></td>
                    <td class="text-center"><?php echo $value->batch;?></td>
                    <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->dateofjoining);?></td>
                    <td class="text-center"><?php echo $value->batchstage;?></td>
                  <!--   <td><?php
                    if ($value->status==0) {
                      echo "Active"; 
                    }else{
                      echo "InActive"; 
                    }

                    ?></td> -->
                    <td class="panel-footer text-center"><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Batch." href="<?php echo site_url()."Batch/edit/".$value->id;?>" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Batch." href="<?php echo site_url()."Batch/delete/".$value->id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
</a></td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>
      </div>
    </div>   
    <?php } } ?>
    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
        var indno =  id.replace('sendtoed_','');
        var candidateid = $('#candidateid_'+indno).val();
//alert(candidateid);

$.ajax({
  url: '<?php echo site_url(); ?>Assigntc/sendofferlettered/'+ candidateid,
  type: 'POST',
  dataType: 'text',
})

.done(function(data) {

  if (data==1) {
   alert('Send Offer Letter To ED');
 }else{
  alert('Already send Offer Letter To ED ');
}

})

});


      function confirm_delete() {
        var r = confirm("Do you want to delete this Batch?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }


    </script>
