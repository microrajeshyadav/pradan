
<?php foreach ($role_permission as $row) { if ($row->Controller == "Batch" && $row->Action == "edit"){ ?>
<div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>

      <div class="container-fluid" style="margin-top: 20px;">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

         <div class="row clearfix doctoradvice">
          <form method="POST" action="">          
           <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-8 panel-title pull-left">Edit Batch</h4>
               <div class="col-md-4 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>
          <div class="panel-body">  
            <div class="row">
              <div class="col-md-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="batch" class="field-wrapper required-field">Batch <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control txtNumeric" maxlength="10" minlength="1" name="batch" id="batch" placeholder="Please Enter Batch" value="<?php echo $batchupdate[0]->batch;?>">
                </div>
                <?php echo form_error("batch");?>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
               <div class="form-line">
                <label for="financialyear" class="field-wrapper required-field">Financial Year <span style="color: red;" >*</span></label>
                <select name="financialyear" id="financialyear" class="form-control">
                 <option value="">Select Financial Year</option>
                 <?php foreach ($getfinancialyear as $key => $value) {
                  if ($batchupdate[0]->financial_year == $value->id) { 
                   ?>
                   <option value="<?php echo $value->id; ?>" Selected><?php echo $value->financialyear; ?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $value->id; ?>" ><?php echo $value->financialyear; ?></option>

                   <?php } }?>
                 </select>

                 <!--  <input type="text" class="form-control inputelement" name="financialyear" id="financialyear" placeholder="Please Enter Financial Year" value="<?php //echo $batchupdate[0]->financial_year;?>" > -->
               </div>
               <?php echo form_error("batch");?>
             </div>
           </div>
           <div class="col-md-4">
            <div class="form-group">
              <div class="form-line">
                <label for="dateofjoining" class="field-wrapper required-field">Date of joining <span style="color: red;" >*</span></label>
                <input type="text" class="form-control datepicker" name="dateofjoining" id="dateofjoining" readonly placeholder="Please Enter Date of joining " value="<?php echo $this->gmodel->changedatedbformate($batchupdate[0]->dateofjoining);?>">
              </div>
              <?php echo form_error("dateofjoining");?>
            </div>
          </div>
          <div class="col-md-4">
           <div class="form-group">
            <div class="form-line">
              <label for="stage" class="field-wrapper required-field">Stage  <span style="color: red;" >*</span></label>
              <select name="stage" id="status" class="form-control" data-toggle="">

                <option value="1" <?php if ($batchupdate[0]->batchstage==1) { echo "SELECTED"; }else{ echo " "; }
                ?> >Will Join</option>
                <option value="2" <?php if ($batchupdate[0]->batchstage==2) { echo "SELECTED"; }else{ echo " "; }
                ?> >Ongoing</option>
                <option value="3" <?php if ($batchupdate[0]->batchstage==3) { echo "SELECTED"; }else{ echo " "; }
                ?> >Gratuated</option>
              </select>
              <?php echo form_error("stage");?>
            </div>
          </div> 
        </div>
        <div class="col-md-4">
         <div class="form-group">
           <div class="form-line">
            <label for="status" class="field-wrapper required-field">Status <span style="color: red;" >*</span> </label>
            <select name="status" id="status" class="form-control" data-toggle="" >
              <option value="">Select Status</option>
              <option value="0" <?php if ($batchupdate[0]->status==0) { echo "SELECTED"; }else{ echo " "; }
              ?> >Active</option>
              <option value="1" <?php if ($batchupdate[0]->status==1) { echo "SELECTED"; }else{ echo " "; }
              ?> >InActive</option>
            </select>
            <?php echo form_error("status");?>
          </div> 
        </div> </div>
        <div class="col-md-12 text-right">
          <div style="text-align: -webkit-center;"> 

          </div>
        </div>


      </div>

    </div>
    <div class="panel-footer text-right">
      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
      <a href="<?php echo site_url("batch");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
    </div>

  </div>

</form>
</div>

</div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php } } ?>
<script type="text/javascript">

  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
   }
 }, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});


  $('.inputelement').keyup(function () {
    if (!this.value.match(/[0-9]/)) {
      this.value = this.value.replace(/[^0-9]/g, '');
    }
  });
</script>