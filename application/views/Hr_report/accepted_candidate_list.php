<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Report</h1>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-bar-chart-o fa-fw"></i> Offered accepted list
            </div>
            <!-- /.panel-heading -->
            <form action="" method="post" name="search" id="reportsearch">
        
            <div class="panel-body">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
             </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-right"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
            <br>
            <br>
            <br>
              <table id="statedashboard" class="table table-bordered table-striped">
                <thead>
                 <tr>
                  <th>S.No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Written Score</th>
                  <th colspan="3" style="text-align: center;">GD </th>
                  </tr> 
                <tr>
                  <td colspan="4"></td>
                  <td><b>GD Score </b></td>
                  <td><b>SS Score </b></td>
                  <td><b>Over All</b></td>
                </tr> 
              </thead>
              <tbody>
                <?php if(count($offeredacceptrdlist) == 0){  ?>
                 <tr>
                  <td colspan="7">No Record Found !!!</td>
                </tr>
                <?php } else{ 
                  $i=0;
                  foreach ($offeredacceptrdlist as $key => $value) {
                  ?>
               <tr>
                 <td><?php echo $i+1; ?></td>
                  <td><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname; ?></td>
                  <td><?php echo $value->emailid; ?></td>
                  <td><?php echo $value->writtenscore; ?></td>
                  <td><?php echo $value->rsscore; ?></td>
                  <td><?php echo $value->ssscore; ?></td>
                  <td><?php echo $value->gdscore; ?></td>
                </tr>
              <?php $i++; } } ?> 
              </tbody>
            </table>
          </div>
          <!-- /.panel-body -->
        </div>


            



      </div>
      <!-- /.col-lg-8 -->
    
    </div>
    <!-- /.row -->
  </div>

</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
  $('#statedashboard').DataTable({
     "bPaginate": false,
     "bInfo": false,
     "bFilter": false,
     "bLengthChange": false
  }); 




 $("#financialyear").change(function(){
               
                $.ajax({
                    url: '<?php echo site_url(); ?>Ajax/getbatch/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   
                    console.log(data);
                    $("#batch").html(data);
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

    
  });






</script>