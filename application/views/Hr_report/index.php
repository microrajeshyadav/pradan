
<section class="content" style="">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="page-header">Workarea</h3>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <?php  if($this->loginData->RoleID == '3') {?>
        <div class="row tile_count bg-light">
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Staff</span>
            <div class="count green"> <?php echo $dashboardstat->totaltaff;?></div> 
            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>4% </i>From last month</span>             
          </div>

          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Male Staff</span>
            <div class="count green"> <?php echo  $dashboardstat->malecount;?></div>
            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>2% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Female Staff</span>
            <div class="count red"> <?php echo  $dashboardstat->femalecount;?></div>
            <span class="count_bottom"><i class="red"><i class="fas fa-angle-down"></i>1% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fas fa-clock"></i> Last Month Average Attendance</span>
            <div class="count green">90.23</div>

            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>13% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-rupee-sign""></i> Jan, 2019 Gross Salary</span>
            <div class="count green">26,00,315</div>
            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>6% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total separation in this month</span>
            <div class="count red"> <?php echo  $dashboardstat->totalsep;?></div>
            <span class="count_bottom"><i class="red"><i class="fas fa-angle-down"></i>3% </i> From last month</span>
          </div>
        </div>
      <?php  } ?>
      <div class="container-fluid">
       <div id="counter" class="row sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <div class="count"> <p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/leave.jpg'); ?>"> </p>
                <p class="count_top" style="font-weight: 600;">Leave Request</p>
                <P><a href="<?php echo site_url('leaverequest/index/'.$this->loginData->staffid)?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to make request.">Click</a></P></div>
              </div>
              <div class="col-md-2">
                <div class="count"> <p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/Recommendation_IconsLayer_6-512.png'); ?>"> </p>
                  <p class="count_top" style="font-weight: 600;">Approvals</p>
                  <P><a href="<?php echo site_url().'Staff_approval_process/';?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here, might be some approval pendings." >Click</a></P></div>
                </div>
                <div class="col-md-2">
                  <div class="count"> <p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/transfer-10-67552.png'); ?>"> </p>
                    <p class="count_top" style="font-weight: 600;">Transfer Request</p>
                    <P><a href="#" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to make request.">Click</a></P></div>
                  </div>
                  <div class="col-md-2">
                    <div class="count text-center"><div class="progress blue">
                      <span class="progress-left">
                        <span class="progress-bar"></span>
                      </span>
                      <span class="progress-right">
                        <span class="progress-bar"></span>
                      </span>
                      <div class="progress-value">10</div>
                    </div>
                    <p class="count_top" style="font-weight: 600;">Employees on Leave</p>
                    <P><a href="#" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to check employees." >Click</a></P></div>
                  </div>
                  <div class="col-md-2">
                    <div class="count text-center"><p style="margin-left: 35px; height:60px;width: 60px;background-color: #000; color: #fff;  font-size: 40px; font-family: 'Oxygen;'" class="rounded-circle"> <i class="fa fa-history black"> </i> </p>
                      <p class="count_top" style="font-weight: 600;">History</p>
                      <P>
                        <a href="<?php echo site_url().'Staff_personel_records/';?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to know history.">Click</a>

                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="count" style="color: #d4d4d4;"><p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/terminated-employee-662412.png'); ?>"> </p>
                        <p class="count_top" style="font-weight: 600;">Resignation</p>
                        <P><a data-toggle="tooltip" data-placement="bottom" title="Click here to make request." href="Staff_seperation/add/<?php echo $this->loginData->staffid  ?>" class="btn btn-light btn-sm">Click</a></P></div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="row" style="margin-top: 20px;">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #FBF859; padding: 20px;">
                   <div class="col-sm-4 py-2">
                    <div class="card text-white" style="background: #2A9FB8">
                      <div class="card-body">
                       <div id="calendar">
                       </div>
                     </div>
                   </div>
                 </div>
                    <div class="col-sm-4 py-2">
                      <div class="card text-white bg-dafault" style="min-height: 333px;">
                        <div class="card-body"  style="background-image:url('./common/backend/images/plogo.jpg');  ">
                          <p class="text-center" style="font-size: 22px; font-weight: bold; padding: 10px; font-family: 'Oxygen'; color: #000; background-color: #fff;"><i class="fa fa-bell-o fa-lg " aria-hidden="true"></i> &nbsp;<span style="font-size: 22px; color: #000; text-transform: uppercase;">Policies </span><br>
                          </p>

                          <div>
                           <?php 
                           if($policylist){
                            foreach($policylist as $res){
                             ?>
                             <?php  if (file_exists (site_url().'datafiles/policy/'.$res->docpath)) { ?>
                               <div class="alert alert-dark">

                                <a href="<?php echo 'datafiles/policy/'.$res->docpath; ?>" download="<?php  echo 'datafiles/policy/'.$res->docpath; ?>" style="margin-top:-5Px" class="btn btn-xs btn-success pull-right btn-sm">Download</a>
                                <strong>Policy:</strong> <?php echo $res->policy_name; ?>

                              </div>
                            <?php }}} ?>
                <!-- <div class="alert alert-dark">
                    <a href="#" style="margin-top:-5Px" class="btn btn-xs btn-success pull-right btn-sm">Download</a>
                    <strong>Policy:</strong> HR Policy
                  </div> -->

                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-4 py-2">
            <div class="card text-white bg-info" style="height: 332px;">
              <div class="card-body text-center cnt-block">
               <figure>
                <?php $ci =&get_instance(); $ci->load->model('stafffullinfo_model');    $candidatedetails= $ci->stafffullinfo_model->getCandidateDetailsPreview($this->loginData->staffid); //print_r($candidatedetails); die(); ?>

                <?php if(!empty($candidatedetails->encryptedphotoname) && $candidatedetails->encryptedphotoname !='')  { ?>
                  <img class= "rounded-circle" src="<?php if(!empty($candidatedetails->encryptedphotoname)) echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Profile Picture" style="height: 120px; width: 120px;">
                <?php }else{ ?>
                  <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Profile Picture" style="height: 120px; width: 120px;">
                <?php } ?>
              </figure>
              <br>
              <h3 style="font-family: 'Oxygen';"><b><?php  if(!empty($candidatedetails->candidatefirstname)) {
               echo $candidatedetails->candidatefirstname .' '.$candidatedetails->candidatemiddlename.' '.$candidatedetails->candidatelastname;
             }else{
              if(!empty($candidatedetails->staff_name)) echo $candidatedetails->staff_name;
            }   ?></b></h3>
            <p class="title"><?php if(!empty($candidatedetails->desname)) echo $candidatedetails->desname;
            ?></p>
            <p><a href="<?php echo site_url().'Stafffullinfo/view/'.$this->loginData->staffid;?>" class="btn btn-light"data-toggle="tooltip" data-placement="bottom" title="Click here to check your complete profile.">Profile</a></p>
            <!--                        <p><a href="talha08.github.io" class="btn btn-light"data-toggle="tooltip" data-placement="bottom" title="Click here to check your complete profile.">Profile</a></p> -->
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="row" style="margin-top: 20px; ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #F0F0F0; padding: 20px;">
      <div class="panel panel-default">
        <div class="panel-heading">
         <h5> Summary of offered / offered accepted</h5>
       </div>
       <!-- /.panel-heading -->
       <form action="" method="post" name="search" id="reportsearch">
        <div class="panel-body">
          <div class="row bg-dark" style="padding:15px; margin-bottom: 10px; color: #fff;">
            <div class="container-fluid ">
              <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-left"><label class="count_top">Fin. Year :</label> </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
                <select name="financialyear" id="financialyear" class="form-control form-control-sm" required="required">
                 <option value="">Select</option>
                 <?php  foreach ($batchlist as $key => $value) { ?>
                   <option value="<?php echo $value->id;?>"><?php echo  $value->financialyear;?></option>
                 <?php } ?>
               </select>
             </div>

             <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-right"><label class="count_top"> Batch :</label></div>
             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
              <select name="batch" id="batch" class="form-control form-control-sm" required="required">
               <option value="">Select</option>
             </select>
           </div>
           <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12 text-left"><button type="submit" id="search" name="search" class="btn btn-light btn-sm">Search</button></div>
         </div> </div>

       </div>
     </form>
     <!-- /.panel-body -->
   </div>
 </div>
</div>
<div class="row" style="margin-top: 20px; ">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #F0F0F0; padding: 20px;">
    <div class="panel panel-default">
      <div class="panel-heading">
       <h5>Team Coordinator</h5>
     </div>
   </div>
 </div>
</div>

<!-- remonder for document upload -->
<div class="row" style="margin-top: 20px; ">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #F0F0F0; padding: 20px;">
    <div class="panel panel-default">
      <div class="panel-heading">
       <h5>Reminder for Document uploads</h5>
     </div>
   </div>
   <div class="panel-body">
     <table class="table table-bordered table-striped">
       <thead>
         <tr>
          <th class="text-center" style="width: 50px;">S.No.</th>
          <th>Name</th>
          <th>Gender</th>
          <th>Email ID</th>
          <th>Campus</th>
          <th>Degree</th>
          <th>Date</th>
          <th>Contact NO</th>
          <th>Action</th>
        </tr>
      </thead>
      <?php if (count($candidate_details) == 0) { ?>
        <tbody>
          <tr>
            <td colspan="9" style="color: red;">Record not found</td>
          </tr>
        </tbody>
      <?php }else{?>
        <tbody>
         <?php $i=0; foreach($candidate_details as $row) { ?>
           <tr>
            <td class="text-center"><?php echo $i+1; ?></td>
            <td><?php if(!empty($row->FullName)) echo $row->FullName;?></td>
            <td><?php if(!empty($row->gender)) echo $row->gender;?></td>
            <td><?php if(!empty($row->emailid)) echo $row->emailid;?></td>
            <td><?php if(!empty($row->campustype)) echo $row->campustype;?></td>
            <td><?php if(!empty($row->degree)) echo $row->degree;?></td>
            <td><?php if(!empty($row->dta)) echo $this->gmodel->changedatedbformate($row->dta);?></td>
            <td><?php if(!empty($row->mobile)) echo $row->mobile;?></td>
            <td><a data-toggle="tooltip" data-placement="bottom" href="#" style="padding : 4px;">Send Mail</a></td>
          </tr>
          <?php $i++; } ?>
        </tbody>
        <?php } ?>
      </table>
    </div>
  </div>
</div>
<!-- /.col-lg-8 -->

</div>
<!-- /.row -->
</div>

</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
  $(document).ready(function(){


    $('#statedashboard').DataTable({
     "responsive": true
     
   }); 

    $("#financialyear").change(function(){

      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getbatch/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {

        console.log(data);
        $("#batch").html(data);

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    });
  });
</script>


