<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Report</h1>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
     <!--  <div class="row " >
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3">
                  <i class="fa fa-comments fa-2x"></i>
                </div>
                <div class="col-xs-9 text-right">
                  <div class="huge">26</div>
                  <div>New Comments!</div>
                </div>
              </div>
            </div>
            <a href="#">
              <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-green">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3">
                  <i class="fa fa-tasks fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                  <div class="huge">12</div>
                  <div>New Tasks!</div>
                </div>
              </div>
            </div>
            <a href="#">
              <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-yellow">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3">
                  <i class="fa fa-shopping-cart fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                  <div class="huge">124</div>
                  <div>New Orders!</div>
                </div>
              </div>
            </div>
            <a href="#">
              <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-red">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3">
                  <i class="fa fa-support fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                  <div class="huge">13</div>
                  <div>Support Tickets!</div>
                </div>
              </div>
            </div>
            <a href="#">
              <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
              </div>
            </a>
          </div>
        </div>
      </div> -->
     
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-bar-chart-o fa-fw"></i> Join candidate list
            </div>
            <!-- /.panel-heading -->
            <form action="" method="post" name="search" id="reportsearch">
        
            <div class="panel-body">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
             </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-right"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
            <br>
            <br>
            <br>
              <table id="statedashboard" class="table table-bordered table-striped">
                <thead>
                 <tr>
                  <th>S.No.</th>
                  <th>Emp. Code</th>
                  <th>DA Name</th>
                  <th>Email</th>
                  <th>Date Of Joining</th>
                </tr> 
              </thead>
              <tbody>
                <?php //print_r($joincandidatelist); ?>
                <?php if(count($joincandidatelist) == 0){  ?>
                 <tr>
                  <td colspan="5">No Record Found !!!</td>
                </tr>
                <?php } else{ 
                  $i=0;
                  foreach ($joincandidatelist as $key => $value) {
                  ?>
               <tr>
                 <td><?php echo $i+1; ?></td>
                  <td><?php echo $value->emp_code; ?></td>
                  <td><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname; ?></td>
                  <td><?php echo $value->emailid; ?></td>
                  <td><?php echo date('d/m/Y', strtotime($value->createdon)); ?></td>
                </tr>
              <?php $i++; } } ?> 
              </tbody>
            </table>
          </div>
          <!-- /.panel-body -->
        </div>


            



      </div>
      <!-- /.col-lg-8 -->
    
    </div>
    <!-- /.row -->
  </div>

</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
  $('#statedashboard').DataTable({
     "bPaginate": false,
     "bInfo": false,
     "bFilter": false,
     "bLengthChange": false
  }); 




 $("#financialyear").change(function(){
               
                $.ajax({
                    url: '<?php echo site_url(); ?>Ajax/getbatch/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   
                    console.log(data);
                    $("#batch").html(data);
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

    
  });






</script>