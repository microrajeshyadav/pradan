<style type="">
  input[type='text']:disabled
  {
    background: #fff !important;

  }
textarea:disabled
  {
    background: #fff !important;
  }
  textarea{
    width: 600px !important;
  }
 </style>

<form name="transferclaimform" id="transferclaimform" method="POST" action="" enctype="multipart/form-data">
<!--<div class="container">-->
  <div class="container-fluid">
    <!-- Exportable Table -->
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-8 panel-title pull-left"> </h4>

       <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
      </div>

      

    </div>
    <hr class="colorgraph"><br>
  </div>
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
<style>
.card{
  padding:15px;
}
.heading_section{
  padding:15px 0px;
}
</style>
          
            <div class="row" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <div class="card" >
                  <div class="body" style="margin-left: 10px;">
                    <div class="row">
                    <div class="col-md-12 heading_section">
                     <h4 class="header" class="field-wrapper required-field" style="text-align: center;"> <label class="field-wrapper required-field">Professional Assistance for Development Action (PRADAN)  <br>TRANSFER EXPENSES CLAIM </label></h4>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
                      <input type="hidden" name="token" value="<?php echo $token; ?>">
                    
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" ></div>
                    </div>
                   <?php //print_r($candidatedetils); echo $candidatedetils->doj; ?>
                   <div class="panel-body">
                     <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class=" col-md-6" >Name :</div>
           <div class="col-md-6" > <input type="text" name="staff_name" id="staff_name" placeholder=" Please Enter Place"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $staff_details->staff_name;?>" required="required" readonly>  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <div class="col-md-6 text-right">Transferred from </div>
          <div class="col-lg-6 col-md-6 text-right" >  <input type="text" name="transfer_from" id="transfer_from"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $staff_details->new_name; ?>" readonly> </div>
          </div>
        </div>
        </div>
         <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-6 col-md-6" >Employee Code :</div>
           <div class="col-lg-6 col-md-6 " > <input type="text" name="emp_code" id="emp_code" placeholder=" Please Enter Place"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $staff_details->emp_code;;?>" required="required" readonly>  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            
          <div class="col-lg-6 col-md-6 text-right">Transferred to </div>
          <div class="col-lg-6 col-md-6  text-right" >  <input type="text" name="transfer_to" id="transfer_to"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $staff_details->old_name;; ?>" readonly> </div>
          </div>
        </div>
        </div>
         <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-6 col-md-6 " >Designation :</div>
           <div class="col-lg-6 col-md-6 " > <input type="text" name="designation" id="designation" placeholder=" Please Enter Place"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $staff_details->desname;?>" required="required" readonly>  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            
          <div class="col-lg-6 col-md-6 text-right">Date of Release </div>
            <div class="col-lg-6 col-md-6 text-right" >  
            <input type="text" name="date_release" id="date_release" class="form-control" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $this->gmodel->changedatedbformate($staff_details->date_of_transfer);?>" readonly>
            </div>
          </div>
        </div>
        </div>
         <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            
          <div class="col-lg-6 col-md-6 text-left">Location</div>
          <div class="col-lg-6 col-md-6 text-left" >  <input type="text" name="location" id="location"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $staff_details->old_name; ?>" readonly> </div>
          </div>
          <div class="col-md-6">
          <style>
             .subheading{
              padding:25px 0px;
             }
          </style>
          </div>
        </div>
        </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <label class="subheading"><b>Particulars of Claim</b></label>

        </div>
        

        

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

      <ol>
<li><strong>Travel Fare</strong> (<em>for Self and Dependents</em>)</li>
</ol>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
 <div class="col-lg-6 col-md-6 ">
<p> Please attach Travel Expenses Bill 
</p></div>


<div class="col-lg-6 col-md-6 ">
  <strong>Total </strong>= Rs
  <input type="text" min="0" class="txtNumeric" name="travel_exp" id="travel_exp" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->travel_fare; ?>" required></p>
   <?php if($transfer_claim_expense_detail){ ?>
    <a  href="<?php if(!empty($transfer_claim_expense_detail->attach_travel_exp)) echo site_url().'datafiles/educationalcertificate/'.
       $transfer_claim_expense_detail->attach_travel_exp; ?>" download  class="btn-primary">
       <i class="fa fa-download" aria-hidden="true"></i>
     </a>
  <?php }else{ ?>
  <input type="file" name="attach_travel_exp" id="attach_travel_exp" required>
<?php } ?>
 </div>
<p>&nbsp;</p>
<style>
 .localconvey{
  padding-left:18px;
 }
 input[type="checkbox"]{
  margin-right:10px;
 }
</style>
<ol class="localconvey" start="2">
<li><strong>Local Conveyance</strong> (<em>for Self and Dependents</em>)</li>
</ol>
<div class="col-lg-6 col-md-6">
<p> Please attach Local Conveyance Bill </p>
</div>

<div class="col-lg-6 col-md-6">
  <strong>Total </strong>= Rs
  <input type="text" name="local_conveyance" class="txtNumeric" id="local_conveyance" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->local_conveyance; ?>" required>
  <?php if($transfer_claim_expense_detail){ ?>
    <a  href="<?php if(!empty($transfer_claim_expense_detail->attach_local_conveyance)) echo site_url().'datafiles/educationalcertificate/'.
       $transfer_claim_expense_detail->attach_local_conveyance; ?>" download  class="btn-primary">
       <i class="fa fa-download" aria-hidden="true"></i>
     </a>
  <?php }else{ ?>
  <input type="file" name="attach_local_conveyance" id="attach_local_conveyance" required>
<?php } ?>
</div>
                <p>&nbsp;</p>
<ol start="3" class="localconvey">
<li><strong>Conveyance of Personal Effects</strong>:</li>
</ol>
<div class="col-lg-6 col-md-6">
<p> <em>Mode of Conveyance Amount Claimed </em></p></div>


<div>
<p> <em> Amount Claimed </em></p>
</div>
<table width="800">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td ><input type="checkbox" name="train" id="train" value="" <?php if($transfer_claim_expense_detail) if($transfer_claim_expense_detail->conveyance_of_personal_train) echo "checked"; ?>>Train</td>



<td><input type="text" name="train_claim" id="train_claim"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->conveyance_of_personal_train; ?>" disabled ></td>
                
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td ><input type="checkbox" name="truck_claim" id="truck_claim" value="" <?php if($transfer_claim_expense_detail) if($transfer_claim_expense_detail->conveyance_of_personal_truck) echo "checked"; ?>>Truck</td>



<td><input type="text" name="truck_con_claim" id="truck_con_claim"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->conveyance_of_personal_truck; ?>" disabled></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td ><input type="checkbox" name="any_other" id="any_other" value="" <?php if($transfer_claim_expense_detail) if($transfer_claim_expense_detail->conveyance_of_personal_other) echo "checked"; ?>>Any Other</td>



<td><input type="text" name="any_other_claim" id="any_other_claim"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->conveyance_of_personal_other; ?>" disabled></td>
</tr>

<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
                <td > Total = Rs<input type="text" name="total" id="total"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->total; ?>" readonly>
                </td>
</tr>
<style>
.fourthlist{
  padding-left:5px;
}
</style>

</tbody>
</table>
<p>&nbsp;</p>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
<ol class="fourthlist" start="4">
<li><strong>Conveyance of Vehicle:</strong>:</li>
</ol></div>


<div>
<p> <em> Amount Claimed </em></p>
</div>
<table width="800">
<tbody>
<tr>

<td ><input type="checkbox" name="car" id="car" value=""<?php if($transfer_claim_expense_detail) if($transfer_claim_expense_detail->conveyance_of_vehicle_car) echo "checked"; ?>> Car</td>
<td>&nbsp;</td>



<td><input type="text" name="car_claim" id="car_claim"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->conveyance_of_vehicle_car; ?>" disabled></td></tr>
                
<tr>

<td ><input type="checkbox" name="motor_cycle" id="motor_cycle" value="" <?php if($transfer_claim_expense_detail) if($transfer_claim_expense_detail->conveyance_of_vehicle_motor) echo "checked"; ?>>Motor cycle/Scooter/Moped</td>
<td>&nbsp;</td>



<td><input type="text" name="motor_cycle_claim" id="motor_cycle_claim"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->conveyance_of_vehicle_motor; ?>" disabled ></td>
</tr>
<tr>

<td ><input type="checkbox" name="bicycle_claim_exp" id="bicycle_claim_exp" value="" <?php if($transfer_claim_expense_detail) if($transfer_claim_expense_detail->conveyance_of_vehicle_bicycle) echo "checked"; ?>>Bicycle</td>
<td>&nbsp;</td>



<td><input type="text" name="bicycle_claim" id="bicycle_claim"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->conveyance_of_vehicle_bicycle; ?>" disabled ></td>
</tr>

<tr>
  <td colspan="3" > Total = Rs<input type="text" name="v_con_total" id="v_con_total"
  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
  value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->total_vehicle_con; ?>" readonly>
  </td>
</tr>

</tbody>
</table>

<p>&nbsp;</p>

<div class="col-lg-6 col-md-6 ">
<ol start="5" class="fourthlist">
<li><strong>Lumpsum Transfer Allowance</strong>
</li>
</ol>
</div>
<div class="col-lg-6 col-md-6 ">
: Rs 
<input type="text" name="l_allowance" id="l_allowance" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->lumpsum_transfer_allowance; ?>" required>
</div>
<div class="col-md-6"></div>
  <div class="col-md-6">
  <p><strong>Grand Total </strong>(1+2+3+4+5) Rs 
    <input type="text" name="grand_total" id="grand_total" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->grand_total; ?>" required readonly>
  </p>
</div>
</p>



</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
<ol start="6" class="fourthlist">
<li><strong>Less</strong> Amount of Transfer TA Advance </li>
</ol>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
Rs 
<input type="text" name="ta_advance" id="ta_advance" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->less_amount_of_transfer_ta_advance; ?>" required>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
<ol start="7" class="fourthlist">
<li><strong>Net amount payable </strong></li>
</ol>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
Rs 
<input type="text" name="payable_amount" id="payable_amount" style="min-width: 5px;max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php if($transfer_claim_expense_detail) echo $transfer_claim_expense_detail->net_amount_payable; ?>" readonly>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "><div class="col-md-12">
<p>Certified that I have actually spent an amount as claimed above, and have not claimed this amount from any other source. All the statements made herein are true and correct. All receipts, bills, vouchers, documentary evidence, are attached herewith.</p>
</div>
<div class="col-md-3"
<p>Date:
  <input type="text" name="date" id="date" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y'); ?>" readonly></p>
</div>
<div class="col-md-3">
  <input type="text" name="sign_employ" id="sign_employ" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
    value="" >
<p>(Signature of Employee)</p>
</div>
<div class="col-md-3">
<input type="text" name="v_fpm" id="v_fpm" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" >
<p>(Verified by Finance-Personnel-MIS Unit)</p>
</div>
<div class="col-md-3">
<input type="text" name="approvedby" id="approvedby" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" >
<p>(Approved by)</p>
</div>

<div class="col-md-12 text-center">
<div class="panel-footer text-right">
<?php 
if ($transfer_claim_expense_detail){
if($transfer_claim_expense_detail->flag != 1){ ?>
         <!-- <button  type="submit" name="savetbtn" id="savetbtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button> -->
                 
         <button  type="button" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit" onclick="transferclaimexpense();">Submit</button>
<?php }else{ ?>
<!-- <a href="<?php echo site_url("Staff_personel_records");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> -->
<?php }}else{ ?>
  <button  type="button" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit" onclick="transferclaimexpense();">Submit</button>
  
<?php } ?>
<?php if($this->loginData->RoleID == 3){ ?>
<a href="<?php echo site_url("Staff_personel_records");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
<?php } else if($this->loginData->RoleID == 2){?>
<a href="<?php echo site_url("Staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
  <?php }else{ ?>
<a href="<?php echo site_url("Staff_finance_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
  <?php } ?>
  </div>
  </div>
  </div>
        </div>
      </div>

    </div>
    </div>
  </div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes" > </textarea>
              <?php echo form_error("comments");?>
            
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="button" name="btnsubmit" id="btnsubmit" value="Save" class="btn btn-success btn-sm" onclick="transferclaimexpensemodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('input[name=travel_exp]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=local_conveyance]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=train_claim]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=truck_con_claim]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=any_other_claim]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=car_claim]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=motor_cycle_claim]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=bicycle_claim]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=l_allowance]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $('input[name=ta_advance]').change(function() {
    var total = totalAmount();
    var ConveyanceEffects = ConveyanceofPersonalEffects();
    var ConveyanceVehicle = ConveyanceofVehicle();
    var netAmount = netAmountPayable();
    $('input[name=total]').val(ConveyanceEffects);
    $('input[name=v_con_total]').val(ConveyanceVehicle);
    $('input[name=grand_total]').val(total);
    $('input[name=payable_amount]').val(netAmount);
  });
  $("#train").click(function () {
    if ($(this).is(":checked")) {
      $("#train_claim").removeAttr('disabled',false);
      $("#train_claim").attr('required',true);      
    } else {
      $("#train_claim").attr('disabled',true);
      $("#train_claim").val('');
      var total = totalAmount();
      var ConveyanceEffects = ConveyanceofPersonalEffects();
      var ConveyanceVehicle = ConveyanceofVehicle();
      var netAmount = netAmountPayable();
      $('input[name=total]').val(ConveyanceEffects);
      $('input[name=v_con_total]').val(ConveyanceVehicle);
      $('input[name=grand_total]').val(total);
      $('input[name=payable_amount]').val(netAmount);
    }
  });
  $("#truck_claim").click(function () {
    if ($(this).is(":checked")) {
      $("#truck_con_claim").removeAttr('disabled',false);
      $("#truck_con_claim").attr('required',true);        
    } else {
      $("#truck_con_claim").attr('disabled',true);
      $("#truck_con_claim").val('');
      var total = totalAmount();
      var ConveyanceEffects = ConveyanceofPersonalEffects();
      var ConveyanceVehicle = ConveyanceofVehicle();
      var netAmount = netAmountPayable();
      $('input[name=total]').val(ConveyanceEffects);
      $('input[name=v_con_total]').val(ConveyanceVehicle);
      $('input[name=grand_total]').val(total);
      $('input[name=payable_amount]').val(netAmount);
    }
  });        
  $("#any_other").click(function () {
    if ($(this).is(":checked")) {
      $("#any_other_claim").removeAttr('disabled',false);
      $("#any_other_claim").attr('required',true);        
    } else {
      $("#any_other_claim").attr('disabled',true);
      $("#any_other_claim").val('');
      var total = totalAmount();
      var ConveyanceEffects = ConveyanceofPersonalEffects();
      var ConveyanceVehicle = ConveyanceofVehicle();
      var netAmount = netAmountPayable();
      $('input[name=total]').val(ConveyanceEffects);
      $('input[name=v_con_total]').val(ConveyanceVehicle);
      $('input[name=grand_total]').val(total);
      $('input[name=payable_amount]').val(netAmount); 
    }
  });

  $("#car").click(function () {
    if ($(this).is(":checked")) {
      $("#car_claim").removeAttr('disabled',false);
      $("#car_claim").attr('required',true);        
    } else {
      $("#car_claim").attr('disabled',true);
      $("#car_claim").val('');
      var total = totalAmount();
      var ConveyanceEffects = ConveyanceofPersonalEffects();
      var ConveyanceVehicle = ConveyanceofVehicle();
      var netAmount = netAmountPayable();
      $('input[name=total]').val(ConveyanceEffects);
      $('input[name=v_con_total]').val(ConveyanceVehicle);
      $('input[name=grand_total]').val(total);
      $('input[name=payable_amount]').val(netAmount);       
    }
  });

  $("#motor_cycle").click(function () {
    if ($(this).is(":checked")) {
      $("#motor_cycle_claim").removeAttr('disabled',false);
      $("#motor_cycle_claim").attr('required',true);      
    } else {
      $("#motor_cycle_claim").attr('disabled',true);
      $("#motor_cycle_claim").val('');
      var total = totalAmount();
      var ConveyanceEffects = ConveyanceofPersonalEffects();
      var ConveyanceVehicle = ConveyanceofVehicle();
      var netAmount = netAmountPayable();
      $('input[name=total]').val(ConveyanceEffects);
      $('input[name=v_con_total]').val(ConveyanceVehicle);
      $('input[name=grand_total]').val(total);
      $('input[name=payable_amount]').val(netAmount);     
    }
  });


  $("#bicycle_claim_exp").click(function () {
    if ($(this).is(":checked")) {
      $("#bicycle_claim").removeAttr('disabled',false);
      $("#bicycle_claim").attr('required',true);
    } else {
      $("#bicycle_claim").attr('disabled',true);
      $("#bicycle_claim").val('');
      var total = totalAmount();
      var ConveyanceEffects = ConveyanceofPersonalEffects();
      var ConveyanceVehicle = ConveyanceofVehicle();
      var netAmount = netAmountPayable();
      $('input[name=total]').val(ConveyanceEffects);
      $('input[name=v_con_total]').val(ConveyanceVehicle);
      $('input[name=grand_total]').val(total);
      $('input[name=payable_amount]').val(netAmount);
       
    }
  });
  function totalAmount(){
    var travel_exp          = $('input[name=travel_exp]').val() >0?$('input[name=travel_exp]').val():0;
    var local_conveyanceval = $('input[name=local_conveyance]').val() >0?$('input[name=local_conveyance]').val():0;
    var train               = ($('input[name=train]').is(":checked") && $('input[name=train_claim]').val() > 0)? $('input[name=train_claim]').val():0;
    var truck_claim         = ($('input[name=truck_claim]').is(":checked") && $('input[name=truck_con_claim]').val() > 0)? $('input[name=truck_con_claim]').val():0;
    var any_other           = ($('input[name=any_other]').is(":checked") && $('input[name=any_other_claim]').val() > 0)? $('input[name=any_other_claim]').val():0;
    var car                 = ($('input[name=car]').is(":checked") && $('input[name=car_claim]').val() > 0)? $('input[name=car_claim]').val():0;
    var motor_cycle         = ($('input[name=motor_cycle]').is(":checked") && $('input[name=motor_cycle_claim]').val() > 0)? $('input[name=motor_cycle_claim]').val():0;
    var bicycle_claim_exp   = ($('input[name=bicycle_claim_exp]').is(":checked") && $('input[name=bicycle_claim]').val() > 0)? $('input[name=bicycle_claim]').val():0;
    var l_allowance         = $('input[name=l_allowance]').val() >0?$('input[name=l_allowance]').val():0;
    var totalval            = parseFloat(train) + parseFloat(truck_claim) + parseFloat(any_other) + parseFloat(car) + parseFloat(motor_cycle) + parseFloat(bicycle_claim_exp) + parseFloat(travel_exp) + parseFloat(local_conveyanceval) + parseFloat(l_allowance);
    return totalval;
  }
  function ConveyanceofPersonalEffects(){
    var train                            = ($('input[name=train]').is(":checked") && $('input[name=train_claim]').val() > 0)? $('input[name=train_claim]').val():0;
    var truck_claim                      = ($('input[name=truck_claim]').is(":checked") && $('input[name=truck_con_claim]').val() > 0)? $('input[name=truck_con_claim]').val():0;
    var any_other                        = ($('input[name=any_other]').is(":checked") && $('input[name=any_other_claim]').val() > 0)? $('input[name=any_other_claim]').val():0;
    var totalConveyanceofPersonalEffects = parseFloat(train) + parseFloat(truck_claim) + parseFloat(any_other);
    return totalConveyanceofPersonalEffects;
  }
  function ConveyanceofVehicle(){
    var car                      = ($('input[name=car]').is(":checked") && $('input[name=car_claim]').val() > 0)? $('input[name=car_claim]').val():0;
    var motor_cycle              = ($('input[name=motor_cycle]').is(":checked") && $('input[name=motor_cycle_claim]').val()> 0)? $('input[name=motor_cycle_claim]').val():0;
    var bicycle_claim_exp        = ($('input[name=bicycle_claim_exp]').is(":checked") && $('input[name=bicycle_claim]').val() > 0)? $('input[name=bicycle_claim]').val():0;
    var totalConveyanceofVehicle = parseFloat(car) + parseFloat(motor_cycle) + parseFloat(bicycle_claim_exp);
    return totalConveyanceofVehicle;
  }
  function netAmountPayable(){
    var ta_advance = $('input[name=ta_advance]').val() >0?$('input[name=ta_advance]').val():0;
    var total      = totalAmount();
    var netAmount  = parseFloat(total) - parseFloat(ta_advance);
    return netAmount;
  }
});
</script>

<script type="text/javascript">
  $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
        ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
            event.preventDefault();
     }

     var text = $(this).val();
     if ((event.which == 46) && (text.indexOf('.') == -1)) {
         setTimeout(function() {
             if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                 $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
             }
         }, 1);
     }

     if ((text.indexOf('.') != -1) &&
         (text.substring(text.indexOf('.')).length > 2) &&
         (event.which != 0 && event.which != 8) &&
         ($(this)[0].selectionStart >= text.length - 2)) {
             event.preventDefault();
     }      
  });

function transferclaimexpense(){
    var travel_exp = document.getElementById('travel_exp').value; 
    var local_conveyance = document.getElementById('local_conveyance').value;  
    var total = document.getElementById('total').value;     
    var l_allowance = document.getElementById('l_allowance').value;   
    var grand_total = document.getElementById('grand_total').value;    
    var ta_advance = document.getElementById('ta_advance').value;      
    var payable_amount = document.getElementById('payable_amount').value;   
    // alert(name_of_location);

    if(payable_amount == ''){
      $('#payable_amount').focus();
    } 
    if(ta_advance == ''){
      $('#ta_advance').focus();
    } 

    if(grand_total == ''){
      $('#grand_total').focus();
    } 

    if(l_allowance == ''){
      $('#l_allowance').focus();
    }
    if($("#bicycle_claim_exp").prop('checked') == true){
      var bicycle_claim = document.getElementById('bicycle_claim').value;
      if(bicycle_claim == ''){
        $('#bicycle_claim').focus();
      }
    }
    if($("#motor_cycle").prop('checked') == true){
      var motor_cycle_claim = document.getElementById('motor_cycle_claim').value;
      if(motor_cycle_claim == ''){
        $('#motor_cycle_claim').focus();
      }
    }
    if($("#car").prop('checked') == true){
      var car_claim = document.getElementById('car_claim').value;
      if(car_claim == ''){
        $('#car_claim').focus();
      }
    }
    if(total == ''){
      $('#total').focus();
    }

    if($("#any_other").prop('checked') == true){
      var any_other_claim = document.getElementById('any_other_claim').value;
      if(any_other_claim == ''){
        $('#any_other_claim').focus();
      }
    }
    if($("#truck_claim").prop('checked') == true){
      var truck_con_claim = document.getElementById('truck_con_claim').value;
      if(truck_con_claim == ''){
        $('#truck_con_claim').focus();
      }
    }
    //$("#train").prop('checked') == true
    if($("#train").prop('checked') == true){
      var train_claim = document.getElementById('train_claim').value;
      if(train_claim == ''){
        $('#train_claim').focus();
      }
    }
    
    if(local_conveyance == ''){
      $('#local_conveyance').focus();
    }

    if(travel_exp == ''){
      $('#travel_exp').focus();
    }
    
    if(travel_exp != '' && local_conveyance !='' && total !='' && l_allowance !='' && grand_total !='' && ta_advance !='' && payable_amount !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
  function transferclaimexpensemodal(){
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }
    if(comments.trim() !=''){
      document.forms['transferclaimform'].submit();
    }
  }

 $(document).ready(function(){





 
  decimalData();
  // $("#submitbtn").prop("disabled", "disabled");
  $('[data-toggle="tooltip"]').tooltip(); 
  // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
  // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

  $("#selection_process_befor").load(function () {
    if ($(this).is(":checked")) {
      $("#pradan_selection_process_before_details").removeAttr("disabled");
    } else {
      $("#pradan_selection_process_before_details").prop("disabled", "disabled");
    }
  });

 //$('#pradan_selection_process_before_details').prop("disabled",true); 



 
  

  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }

});

 </script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    //$("#btnsubmit")..prop('disabled', true);

  });
</script>  

<script type="text/javascript">
// $(function(){

 //   $("#joiningreport").submit(function(){
  
 //     var form = $('#joiningreport')[0];
 //     var data = new FormData(form);
    
 //     $.ajax({
 //       type: "POST",
 //       enctype: 'multipart/form-data',
 //       url: "<?php echo base_url(); ?>candidate/Joining_report_form/save",
 //       data : data,
 //       async : false,
 //       cache : false,
 //       dataType : 'json',
 //       contentType : false,
 //       processData : false,
 //       success: function(data){
 //        alert(data);
 //         if (data==1) {
 //           location.reload();
 //          alert("Data saved successfully !!!");
 //        }else if(data==0){
 //          alert("Data not saved successfully !!!");
 //        }else if(data==2){
 //           location.reload();
 //          alert("Joining report Already Sunmitted !!!");
 //        }
 //      }

 //    });

 //         return false;  //stop the actual form post !important!

 //       });
 // });


 
  // var condition = "";
  // $("#sendDataButton").click(function() {
  //   condition = $("#sendDataButton").val();
   
  // })
  // $("#sendNextDataButton").click(function() {
  //   condition = $("#sendNextDataButton").val();
   
  // })
  
  // $("#joiningreport")
  //     .submit(
  //         function() {
  //           var form = $('#joiningreport')[0];
  //           var data = new FormData(form);
  //           if (condition == 'saveSubmit') {
  //             $
  //                 .ajax({
  //                   url : "<?php echo base_url(); ?>candidate/Joining_report_form/save",
  //                   type : 'POST',
  //                   enctype : 'multipart/form-data',
  //                   data : data,
  //                   dataType : 'json',
  //                   async : false,
  //                   cache : false,
  //                   contentType : false,
  //                   processData : false,
  //                   complete : function(data) {
  //                    alert(data);
  //                    if (data==1) {
  //                      location.reload();
  //                     alert("Data saved successfully !!!");
  //                   }else if(data==0){
  //                     alert("Data not saved successfully !!!");
  //                   }else if(data==2){
  //                      location.reload();
  //                     alert("Joining report Already Sunmitted !!!");
  //                   }
  //                     //if (data.responseJSON['status'] == "save") {
  //                     //   alert("Registration data saved successfully");
  //                     //   // window.location.replace("${pageContext.request.contextPath}/user/form09/form09Page");
  //                     // }
  //                   }
  //                 })

  //           } else if (condition == 'saveAndNextSubmit') {
  //             $
  //                 .ajax({
  //                   url : "<?php echo base_url(); ?>candidate/Joining_report_form/edit/",
  //                   type : 'POST',
  //                   enctype : 'multipart/form-data',
  //                   data : data,
  //                   dataType : 'json',
  //                   async : true,
  //                   cache : false,
  //                   contentType : false,
  //                   processData : false,
  //                   success : function(data) {
  //                    alert(data);
  //                     // var str = data['status'].split("?")
  //                     // if (str[0] == "edit") {
  //                     //   // window.location
  //                     //   //     .replace("${pageContext.request.contextPath}/user/firmRegistration/firmRegistrationApproval");
  //                     // }
  //                     // if (data['status'] == "submit") {
  //                     //   // window.location
  //                     //   //     .replace("${pageContext.request.contextPath}/user/odsDashBoard/odsDashBoardPage");
  //                     // }
  //                   }
  //                 })

  //           }
  //           return false;
  //         })
</script>

<?php 
if ($transfer_claim_expense_detail){
if($transfer_claim_expense_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  $("form input[type=checkbox]").prop("disabled", true);
  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");
  });
</script>
<?php }} ?>
