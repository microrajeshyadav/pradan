<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Pgeducation" && $row->Action == "index"){ ?>
 <br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


<?php  //print_r($state_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Staff Probation from upload and send to concerned Supervisor  </b>
             <div class="pull-right">
              <!--  <a href="<?php //echo site_url("Pgeducation/add/")?>" class="btn btn-primary btn-xs">Add New Post Graduation </a> -->
             </div>
           </div>
           <div class="panel-body">
             
                     
                <table id="tblprobationfeedback" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.No.</th>
                      <th>Staff</th>
                      <th>Gender</th>
                      <th>DOJ</th>
                      <th>Current Designation</th>
                      <th>Supervisor Name </th>
                      <th>Remarks by Personal</th>
                      <th class="text-center" style="width: 100px;">Status</th>
                      <th>Probation Last Date</th>
                      <th>Feedback due Date</th>
                       <th>Upload From Send/</th>
                    
                    </tr>
                  </thead>
                  <tbody>
                    <?php

                   // print_r($staffcategory_details);
                     $i=0; foreach($mststaffprobation_details as $row){ ?>
                    <tr>
                      <td class="text-"><?php echo $i+1; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="text-center"></td>
                      <td></td>  
                      <td></td>
                      <td></td>
                     <td> <a " data-toggle="tooltip"  id="statedl"  style="padding : 4px;" title="Send "><span class="glyphicon glyphicon-send" style="font-size : 15px; margin-top: 8px;"></span></a></td>
                     
                      
                   
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>
<script>
  $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip(); 
            $('#tblprobationfeedback').DataTable({
              "paging": true,
              "search": true,
            });
          });

</script>