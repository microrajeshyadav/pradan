<section class="content">
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

             <div class="header">
              <h4>Manage ED Summary END Of The Day </h4><br>
              <small><h2>Following  Issue Pending</h2> </small><br>
           
            </div>
            <div class="body">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table id="edsummayeod" class="table table-bordered table-striped wrapper">
                  <thead>
                    <tr>
                      <th class="text-center">Sr. No </th>
                      <th class="text-center">Category</th>
                      <th class="text-center">Candidate Name </th>
                      <th class="text-center">Created Date</th>
                      <th class="text-center">Offer Letter</th>
                      <th class="text-center">Status</th>
                      
                    </tr>
                  </thead>
                  <tbody>   
                    <?php //print_r($listisssuepending) ?>
                   <?php $i=0; foreach ($listisssuepending as $key => $value) {  ?>
                <tr>
                <td><?php echo $i+1; ?></td> 
                <td>DA</td> 
                <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>   
                <td><?php echo $value->createdon;?></td> 
                <td> <a href="<?php echo site_url().'pdf_offerletters/'.$value->filename.'.pdf';?>"  class="btn btn-primary" target="_blank">
                Download</i></a></td> 
                <?php if($value->sendflag=='0') ?>  
                <td><button type="button" class="btn btn-danger">Pending</button></td>
                </tr>
                   <?php } ?>
                  </tbody>
                </table>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#edsummayeod').DataTable(); 
  });
</script>