<style type="text/css">
  input { 
    text-align: left; 
  }
</style>
<script type="text/javascript">

  $.noConflict();
  function formulaOk(){    
    var finalformula = $('#finalformulaHtml').text();

    if(finalformula !=''){
      //$('#myModal123').modal('hide');
     // $('#myModal123').fadeOut();
     $('#myModal123').modal('hide');
     document.getElementById("salaryFormula").value = finalformula;

   }
 }
 function ldformulaOk(){
  var finalformula = $('#ldfinalformulaHtml').text();

  if(finalformula !=''){
      // $('#locationDependentFormulaModal').modal('toggle');
      $('#locationDependentFormulaModal').modal('hide');

      document.getElementById("ldsalaryFormula").value = finalformula;
    }
  }
  function ldformulaSubmit(){
    var selected_field_no = document.getElementById('selected_field_no').textContent;

    if(selected_field_no){
      var ferror = false;
      selected_field_no = selected_field_no.replace('Field No =','');
    }else{
      document.getElementById('errormessage_selectedfield').innerHTML = 'Select any fields.';
      var ferror = true;
    }
    //alert(selected_field_no);
    var officeid  = document.getElementById('officeid').value;
    var ldsalaryFormula = document.getElementById('ldsalaryFormula').value;
    var modalfixedvalue = document.getElementById('modalfixedvalue').value;
    var ldlowerrange    = document.getElementById('ldlowerrange').value;
    var ldupperrange    = document.getElementById('ldupperrange').value;

    var modalfixedcolumn=modalformulacolumn=0;
    if($('#modalfixedcolumn').prop('checked'))
      modalfixedcolumn = 1; 
    if($('#modalformulacolumn').prop('checked'))
      modalformulacolumn = 1;

///// Commented on 6 march by amit kumar////////
    // if(ldlowerrange > ldupperrange || ldlowerrange < 0 || (ldlowerrange == 0 && ldupperrange == 0) || ldupperrange ==''){
    //   var lerror = true;
    //   $("#ldlowerrange").addClass("is-invalid");
    // }else{
    //   var lerror = false;
    //   $("#ldlowerrange").removeClass("is-invalid");
    // }
    // if(ldupperrange < 0 || (ldlowerrange == 0 && ldupperrange == 0) || ldupperrange ==''){
    //   var uerror = true;
    //   $("#ldupperrange").addClass("is-invalid");
    // }else{
    //   var uerror = false;
    //   $("#ldupperrange").removeClass("is-invalid");
    // }

///// Commented on 6 march by amit kumar////////

// if(ldsalaryFormula == ''){
//   var sferror = true;
//   $("#ldsalaryFormula").addClass("is-invalid");
// }else{
//   var sferror = false;
//   $("#ldsalaryFormula").removeClass("is-invalid");
// }
if(officeid == ''){
  var oferror = true;
  $("#officeid").addClass("is-invalid");
}else{
  var oferror = false;
  $("#officeid").removeClass("is-invalid");
}
if(oferror == false ){
  $.ajax({
    url : '<?php echo site_url(); ?>Ajax/LDSalaryHead',
    type : 'POST',
        //dataType : 'json',
        data : {'selected_field_no':selected_field_no, 'salary_formula':ldsalaryFormula, 'lowerrange':ldlowerrange, 'upperrange':ldupperrange, 'officeid':officeid, 'modalfixedvalue':modalfixedvalue,'modalfixedcolumn':modalfixedcolumn, 'modalformulacolumn':modalformulacolumn},
      })
  .done(function(data){
    document.getElementById('errormessage_upperlimit').innerHTML = '';
        if(data == 'update'){
          document.getElementById('modal_success_message').innerHTML = 'Successfully updated';
        }else{
          document.getElementById('modal_success_message').innerHTML = 'Successfully inserted';
        }
        $.ajax({
          url: '<?php echo site_url(); ?>Ajax/ld_head_detail/'+ selected_field_no.trim(),
          type: 'POST',
            success: function(data) { //salaryheadlist

              $("#salaryheadlist").html(data);
            }
          });
      })

}else{

  if(lerror == true && ldlowerrange > 0){
    document.getElementById('errormessage_upperlimit').innerHTML = 'Lower range is greater than upper range.';
  }else{
    document.getElementById('errormessage_upperlimit').innerHTML = '';
  }
}
}
</script>

<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">

   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
        <h4 class="col-md-12 panel-title pull-left">SALARY HEAD CONFIGURATION
        </h4>
      </div>
      <hr class="colorgraph"><br>
    </div>



    <div class="panel-body">
      <div class="col-md-12" style="padding: 10px; margin-bottom: 10px; background: #e1e1e1;">
        <div class="col-md-9">
          <div class="form-group">
            <div class="form-inline">
              <label>Financial Year : </label>
              <select name="financialyear" id="financialyear" class="form-control col-md-4">
                <option value="<?php echo date('Y')-1; ?>-<?php echo date('Y'); ?>" <?php if($financial_year == (date('Y')-1).'-'.date('Y')) echo "selected"; ?> ><?php echo date('Y')-1; ?>-<?php echo date('Y'); ?></option>
                <option value="<?php echo date('Y'); ?>-<?php echo date('Y')+1; ?>" <?php if($financial_year == date('Y').'-'.(date('Y')+1)) echo "selected"; ?>><?php echo date('Y'); ?>-<?php echo date('Y')+1; ?></option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-3 text-left">
          <label id="success_message" class="pull-left" style="color: green; margin:5px;"></label>
          <label style="color: red; margin:5px;" id="errormessage_upperlimit" class="pull-left"></label>
          <button class="btn btn-success pull-right" onclick="slaryHeadSubmit()">Save</button>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
        <div class="table-wrapper-scroll-y" style="height:500px; overflow:auto;">
          <table id="statedashboardxyz" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Field No</th>
                <th>Field Desc</th>
              </tr> 
            </thead>
            <tbody>
              <?php
              $result_set = array_combine($content_lists['list'], $content_lists['list_value']);
              foreach($result_set as $key=>$value){
               ?>
               <tr>
                <td><a onclick="head_detail('<?php echo $key; ?>')" href="#" > <?php echo $key; ?></a></td>
                <td><?php echo $value; ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table> 
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
      <div class="row">
        <label id="errormessage_selectedfield" style="color: red"></label>
        <table id="statedashboard" class="table table-bordered table-striped responsive nowrap">
          <tbody>
            <tr>
              <th>Field No:</th>
              <td><label id="selected_field_no"></label></td>
            </tr>
            <tr>
              <th>Financial Year</th>
              <td><label id="selected_financial_year"></label></td>
            </tr>
            <tr>
              <th>Head Description</th>
              <td><input type="text" class="form-control" name="head_description" id="head_description" placeholder="Enter Description"></td>
            </tr> 
            <tr>
              <th>Abbreviation</th>
              <td><input type="text" class="form-control" name="head_abbreviation" id="head_abbreviation" placeholder="EPEN"></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="row">
        <table id="statedashboard" class="table table-bordered table-striped responsive nowrap">
          <thead>
            <tr>
              <th>Lower Range>=</th>
              <td><input type="text" placeholder="0" maxlength="10" onblur="intTo(this)" name="lowerrange" id="lowerrange" class="form-control text-right" ></td>
              <th>Upper Range<=</th>
              <td><input type="text" placeholder="0" maxlength="10" onblur="intTo(this)" name="upperrange" id="upperrange" class="form-control text-right" ></td>
            </tr> 
          </thead>
        </table>
        <div>

        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-inline">
           <input type="radio" id="fixedcolumn" name="formularadio" value="0" maxlength="10" class="form-check-input form-control" onclick="return fixedsalaryFormulaenable();" >
           <label for="fixedcolumn">Fixed value:</label> 
           <input type="text" class="form-control text-right" placeholder="0" maxlength="10" onblur="intTo(this)" name="fixedvalue" id="fixedvalue" style="margin-left:15px;" >
         </div>
       </div>
       <div class="col-md-12" style="margin-top: 10px;">
        <div class="form-inline">
          <input type="radio" id="formulacolumn" name="formularadio" value="1" class="form-check-input form-control"  onclick="return fixedsalaryFormulaenable();">
          <label for="formulacolumn">Formula:</label> 
          <input type="text" class="form-control " placeholder="" id="salaryFormula" name="salaryFormula" size="50" value="" style="margin-left:30px;" >
          <input type="hidden"  placeholder="" id="maxlimit" name="maxlimit" size="50" value="0" style="margin-left:30px;" > &nbsp
          <label class="btn button formulacreator btn-sm" 
          style="background-color: #ddd;" data-toggle="modal" 
          data-target="#myModal123" id="formulamodal">.....</label>
        </div>
      </div>




      <div class="col-md-12" style="margin-top: 10px;">
        <div class="form-inline">
          <input type="radio" id="monthlyInput" name="formularadio" value="2" class="form-check-input form-control"  onclick="return fixedsalaryFormulaenable();">
          <label for="monthlyInput">On Input(while calculate)</label> 

        </div>
      </div>

      <div class="col-md-12" style="margin-top: 10px;">
        <div class="form-inline">
          <input type="radio" id="salarySlab" name="formularadio" value="3" class="form-check-input form-control"  onclick="return fixedsalaryFormulaenable();">
          <label for="slab">Slab </label> 

        </div>
      </div>




    </div>
    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 text-left" style="width:100%;">
    </div>
    <div>

    </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
    <table id="statedashboard" class="table">
      <tbody>
        <tr style="display: none;">
         <td>RoundUp To :</td>
         <td class="text-left" >
          <div class="form-inline">
            <div class="form-group">
              <input type="text" size="2" style="margin-left:100px;" id="round_upto" value="0"/>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>Active Field </td>
        <td lass="text-left">
          <div class="form-inline">
            <div class="form-group">
              <input type="checkbox" class="form-check-input form-control" id="activeField"></div>
            </div>
          </td>
        </tr>
        <tr style="display: none;">
         <td>Working Days Dependent</td>
         <td > 
          <div class="form-inline">
            <div class="form-group">
              <input type="checkbox" class="form-check-input form-control" id="workingDaysDependent">
            </div>
          </div>
        </td>
      </tr>

      <tr>
       <td>Carry Forward</td>
       <td >
        <div class="form-inline">
          <div class="form-group">
            <input type="checkbox" class="form-check-input form-control" id="carryForward">
          </div>
        </div>
      </td>
    </tr>
    <tr>
     <td>Location Dependent</td>
     <td class="text-left" >
      <div class="form-inline">
        <div class="form-group">
          <input type="checkbox" class="form-check-input form-control" id="locationDependent" onchange="valueChanged()">
          <label class="btn button locatinDependentformulacreator btn-sm" style="background-color: #ddd; margin-top: 7px; display: none;" data-toggle="modal" data-target="#locationDependentModal">....</label>
        </div>
      </div>
    </td>
  </tr>
<!-- <tr style="display:none" id="locationdependenttr">
 <td>

 </td>
</tr> -->
<tr>
 <td>Rounding To Higher </td>
 <td > 
  <div class="form-inline">
    <div class="form-group">
      <input type="checkbox" class="form-check-input form-control" id="roundingToHeigher">
    </div>
  </div>
</td>
</tr>




</tbody>
</table>


</div>

</div>

</div>
</div>
</section>

<!-- Main Screen frormula Modal -->
<div class="modal fade" id="myModal123" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 2px 0px 0px 7px;">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h5 class="modal-title" id="myModalLabel">Formula Editor</h5>


        <hr style="margin-top:0px">
      </div>
      <div class="modal-body" style="padding: 0px 8px;">
        <div class="row">
          <div class="col-sm-12 col-xs-12" style="height: 230px;">
            <div class="col-md-4 col-xs-4">
              <div class="row">
                <div class="col-md-12 col-xs-12">
                 Percentage
                 <input type="number" min="0" max="99" name="percent_value" id="percent_value" class="form-control"  required>                 
               </div>
               <br>
               <!-- <div class="col-md-12 col-xs-12" style="margin-top: 10px;">
                <select id="plus_minus" class="form-control" >
                  <option value="+">+</option>
                  <option value="-">-</option>
                </select>
              </div>
              <br> -->
              <div class="col-md-12 col-xs-12" style="margin-top: 10px;">
                <select id="pay_type" class="form-control">
                  <option value="Basic">Basic</option>
                   <!--  <option value="Special Pay">Special Pay</option>
                    <option value="Fixed DA">Fixed DA</option><br> -->
                  </select>
                </div>
              </div>              
            </div>
            <div class="col-md-2 col-xs-12" style="margin-top: 20px;">
              <div class="row">
                <button type="button" class="btn btn-primary btn-sm" id="add_in_formula" style="min-width: 78px;">Add <span>&raquo;</span></button>
              </div>
              <div class="row">
                <button type="button" class="btn btn-primary btn-sm" id="remove_from_formula"><span>&laquo;</span> Remove</button>
              </div>
            </div>
            <div class="col-md-6 col-xs-12" style="height: 200px;margin-top: 5px; padding: 10px;  ">
              <div id="formula_creation_detail" style="font-size: 15px;font-style: italic; border: Solid 1px #e1e1e1; height: 200px;"></div>
            </div>

          </div>

        </div>
        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
         <div class="col-md-12">
          <div class="col-md-12"  id="finalformulaHtml">

          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer" style="text-align: left;padding: 5px;">
     <button type="button" class="btn btn-primary" onclick="formulaOk()">Ok</button>
     <button type="button" class="btn btn-default" data-dismiss="modal"  style="text-align: left !important;">Close</button>
   </div>
 </div>
</div>
</div>
<!-- Main Screen frormula Modal End -->
<!-- Location Dependent Screen Modal -->
<div class="modal fade" id="locationDependentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 2px 0px 0px 7px;">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h5 class="modal-title" id="myModalLabel">Formula Editor</h5>
        <label id="modal_success_message" class="pull-left" style="color: green; margin:5px;"></label>
          <label style="color: red; margin:5px;" id="errormessage_upperlimit" class="pull-left"></label>
        <hr style="margin-top:0px">
      </div>
      <div class="modal-body" style="padding: 10px 8px;">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="form-inline">
            
              <label for="formulacolumn">Office:</label> &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <select id="officeid" name="officeid" onchange="ajaxeditldheaddetail(this.value);" class="form-control">
                <option value="">Select Office</option>
                <?php foreach($officelist as $res){ ?>
                  <option value="<?php echo $res->officeid; ?>"><?php echo $res->officename; ?></option>
                <?php } ?>
              </select>
            
          </div>
        </div>
      </div>
      
      <div class="row" >
        <div class="col-md-12" style="margin-top: 10px;">
          <div class="form-inline">
           <input type="radio" id="modalfixedcolumn" name="modalformularadio" value="0" class="form-check-input form-control" onclick="return modalfixedsalaryFormulaenable();" >
           <label for="fixedcolumn">Fixed value:</label> 
           <input type="text" class="form-control text-right" placeholder="0" onblur="intTo(this)" name="modalfixedvalue" id="modalfixedvalue" minlength="1" maxlength="10" style="margin-left:15px;" disabled="disabled">
         </div>
       </div>
       <div class="col-md-12 col-xs-12" style="margin-top: 10px;">
        <div class="form-inline">
         <div class="form-group">
          <input type="radio" id="modalformulacolumn" name="modalformularadio" value="1" class="form-check-input form-control"  onclick="return modalfixedsalaryFormulaenable();">
          <label for="formulacolumn">Formula:</label> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text"  placeholder="" id="ldsalaryFormula" class="form-control" size="67" disabled="disabled">  &nbsp;
          <label class="btn button formulacreator btn-sm" style="background-color: #ddd;" data-toggle="modal" data-target="#locationDependentFormulaModal" id="modallocationDependentFormulaModal">....</label>
        </div>
      </div>
    </div>


  </div>
  <div class="row" style="margin-top: 10px;">
    <div class="col-sm-12 col-xs-12 col-md-6">
      <table id="statedashboard" class="table table-bordered table-striped responsive nowrap">
        <thead>
          <tr>
            <th>Lower Range>=</th>
            <td><input type="text" placeholder="0" maxlength="10" onblur="intTo(this)" name="ldlowerrange" id="ldlowerrange" class="form-control text-right" ></td>
            <th>Upper Range<=</th>
            <td><input type="text" placeholder="0" maxlength="10" onblur="intTo(this)" name="ldupperrange" id="ldupperrange" class="form-control text-right" ></td>
          </tr> 
        </thead>
      </table>
      <div>
        <label style="color: red;" id="lderrormessage_upperlimit"></label>
      </div>
    </div> 
  </div> 

</div>
<div class="modal-footer" style="text-align: left;padding: 5px;">
 <button type="button" class="btn btn-primary" onclick="ldformulaSubmit()">Ok</button>
 <button type="button" class="btn btn-default" data-dismiss="modal"  style="text-align: left !important;">Close</button>

</div>
<div id="salaryheadlist" style="overflow: auto; max-height: 300px;">

</div>
</div>
</div>
</div>
<!-- Location Dependent Screen Modal End -->
<!-- Location Dependent frormula Screen Modal -->
<div class="modal fade" id="locationDependentFormulaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 2px 0px 0px 7px;">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h5 class="modal-title" id="myModalLabel">Formula Editor</h5>
        <hr style="margin-top:0px">
      </div>
      <div class="modal-body" style="padding: 0px 8px;">
        <div class="row">
          <div class="col-sm-12 col-xs-12" style="height: 230px;">
            <div class="col-md-4 col-xs-4">
              <div class="row">
                <div class="col-md-12 col-xs-12">
                 Percentage
                 <input type="number" min="0" max="99" name="ld_percent_value" id="ld_percent_value" class="form-control" required>                 
               </div>
               <br>
               <!-- <div class="col-md-12 col-xs-12" style="margin-top: 10px;">
                <select id="ld_plus_minus" class="form-control" >
                  <option value="+">+</option>
                  <option value="-">-</option>
                </select>
              </div>
              <br> -->
              <div class="col-md-12 col-xs-12" style="margin-top: 10px;">
                <select id="ld_pay_type" class="form-control">
                  <option value="Basic">Basic</option>
                   <!--  <option value="Special Pay">Special Pay</option>
                    <option value="Fixed DA">Fixed DA</option><br> -->
                  </select>
                </div>
              </div>              
            </div>
            <div class="col-md-2 col-xs-12" style="margin-top: 20px;">
              <div class="row">
                <button type="button" class="btn btn-primary btn-sm" id="ld_add_in_formula" style="min-width: 78px;">Add <span>&raquo;</span></button>
              </div>
              <div class="row">
                <button type="button" class="btn btn-primary btn-sm" id="ld_remove_from_formula"><span>&laquo;</span> Remove</button>
              </div>
            </div>
            <div class="col-md-6 col-xs-12" style="height: 200px;margin-top: 5px; padding: 10px;  ">
              <div id="ld_formula_creation_detail" style="font-size: 15px;font-style: italic; border: Solid 1px #e1e1e1; height: 200px;"></div>
            </div>

          </div>

        </div>
        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
         <div class="col-md-12">
          <div class="col-md-12"  id="ldfinalformulaHtml">

          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer" style="text-align: left;padding: 5px;">
     <button type="button" class="btn btn-primary" onclick="ldformulaOk()">Ok</button>
     <button type="button" class="btn btn-default" data-dismiss="modal"  style="text-align: left !important;">Close</button>
   </div>
 </div>
</div>
</div>
<!-- Location Dependent frormula Screen Modal End -->
<?php /*} }*/ ?>
</section>
<script type="text/javascript">
  /*$('#statedashboardxyz').dataTable({
    "dom": '<"top"i>rt<"bottom"flp><"clear">'
  });*/
  



  $(function() {
    var selected_financial_year = $('option:selected', this).val();
    $('#selected_financial_year').text(selected_financial_year);
    $("#financialyear").change(function() {
      var financialyear = $('option:selected', this).val();
      if(financialyear){
        window.location.href=financialyear;
      }
    });
  });

  function fixedsalaryFormulaenable(){

   var radioValue = $("input[name='formularadio']:checked").val();
    
   if (radioValue==0) {
    $('#fixedvalue').attr('disabled', false);
    $('#formulamodal').hide();
    $('#salaryFormula').attr('disabled', true);
    $('#salaryFormula').val('');
    $('#modalfixedcolumn').prop('checked', true);
    $('#modalformularadio').prop('checked', false);
    $('#modalfixedvalue').attr('disabled', false);
    $('#ldsalaryFormula').attr('disabled', true);
  }else if (radioValue==1) {
    $('#fixedvalue').attr('disabled', true);
    $('#formulamodal').show();
    $('#salaryFormula').attr('disabled', false);
    
    $('#modalfixedcolumn').prop('checked', false);
    $('#modalformularadio').prop('checked', true);
    $('#modalfixedvalue').attr('disabled', true);
    $('#ldsalaryFormula').attr('disabled', false);
  }else if (radioValue==2) {
    $('#fixedvalue').attr('disabled', true);
    $('#formulamodal').hide();
    $('#salaryFormula').attr('disabled', true);
     
    $('#modalfixedcolumn').prop('checked', false);
    $('#modalformularadio').prop('checked', false);
    $('#modalfixedvalue').attr('disabled', true);
    $('#ldsalaryFormula').attr('disabled', true);
  }else if (radioValue==3) {
    $('#fixedvalue').attr('disabled', true);
    $('#formulamodal').hide();
    $('#salaryFormula').attr('disabled', true);

    $('#modalfixedcolumn').prop('checked', false);
    $('#modalformularadio').prop('checked', false);
    $('#modalfixedvalue').attr('disabled', true);
    $('#ldsalaryFormula').attr('disabled', true);
  }
}

function carryforwordenable(){
 var radioValue = $("input[id='formularadio']:checked").val();
 if (radioValue==0) {
  $('#fixedvalue').attr('disabled', false);
  $('#formulamodal').hide();
  $('#salaryFormula').attr('disabled', true);
  $('#salaryFormula').val('');
  $('#modalfixedcolumn').prop('checked', true);
  $('#modalformularadio').prop('checked', false);
  $('#modalfixedvalue').attr('disabled', false);
  $('#ldsalaryFormula').attr('disabled', true);
}else{
  $('#fixedvalue').attr('disabled', true);
  $('#formulamodal').show();
  $('#salaryFormula').attr('disabled', false);
  $('#fixedvalue').val('');
  $('#modalfixedcolumn').prop('checked', false);
  $('#modalformularadio').prop('checked', true);
  $('#modalfixedvalue').attr('disabled', true);
  $('#ldsalaryFormula').attr('disabled', false);
}
}

function modalfixedsalaryFormulaenable(){
 var radioValue = $("input[name='modalformularadio']:checked").val();
 if (radioValue==0) {
  $('#modalfixedvalue').attr('disabled', false);
  $('#modallocationDependentFormulaModal').hide();
  $('#ldsalaryFormula').attr('disabled', true);
  $('#ldsalaryFormula').val('');
}else{
  $('#modalfixedvalue').attr('disabled', true);
  $('#modallocationDependentFormulaModal').show();
  $('#ldsalaryFormula').attr('disabled', false);
  $('#modalfixedvalue').val('');
}
}

function head_detail(val){
    //$('#officeid').prop('checked', false);
    //$('#officeid').find('option:selected').remove();
    var headfinancialyear = document.getElementById("financialyear").value;
    $('#officeid').html($('#officeid').html().replace('selected',''));
    document.getElementById("ldlowerrange").value = '';
    document.getElementById("ldupperrange").value = '';
    document.getElementById("ldsalaryFormula").value = '';
    document.getElementById('errormessage_selectedfield').innerHTML ='';
    document.getElementById('success_message').innerHTML ='';
    $("#head_description").removeClass("is-invalid");
    $("#head_abbreviation").removeClass("is-invalid");
    $("#lowerrange").removeClass("is-invalid");
    $("#upperrange").removeClass("is-invalid");
    $("#maxlimit").removeClass("is-invalid");
    $("#salaryFormula").removeClass("is-invalid");
    $(this).toggleClass("highlight");
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/head_detail/'+ val+'/'+headfinancialyear,
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
      document.getElementById("selected_field_no").innerHTML = 'Field No = '+val;
      var res = JSON.parse(JSON.stringify(data));     

      if(data){
       document.getElementById("head_description").value = res.fielddesc;
       document.getElementById("head_abbreviation").value = res.abbreviation;
       document.getElementById("lowerrange").value = intToNumberBudget(res.lowerrange);
       document.getElementById("upperrange").value = intToNumberBudget(res.upperrange);
       document.getElementById("maxlimit").value = res.maxlimit;
       document.getElementById("salaryFormula").value = res.lookuphead;
       document.getElementById("round_upto").value = res.roundingupto;
       document.getElementById("locationDependent").value = res.mT;
       document.getElementById("fixedvalue").value = intToNumberBudget(res.ct);

       if(document.getElementById("locationDependent").value == 1){
        $('.locatinDependentformulacreator').show();
      }else{
        $('.locatinDependentformulacreator').hide();
      }

      if(res.formulacolumn == 1){

        $('#formulacolumn').prop('checked', true);
        $('#fixedvalue').attr('disabled', true);
        $('#formulamodal').show();
        $('#salaryFormula').attr('disabled', false);
        $('#fixedvalue').val('');
      }
      else{
        $('#formulacolumn').prop('checked', false);
        $('#fixedvalue').attr('disabled', false);
        $('#formulamodal').hide();
        $('#salaryFormula').attr('disabled', true);
        $('#salaryFormula').val('');
      }

      fixedsalaryFormulaenable();

      if(res.activefield == 1)
        $('#activeField').prop('checked', true);
      else
        $('#activeField').prop('checked', false);

      if(res.attendancedep == 1)
        $('#workingDaysDependent').prop('checked', true);
      else
        $('#workingDaysDependent').prop('checked', false);

      if(res.monthlyinput == 1)
        $('#monthlyInput').prop('checked', true);
      else
        $('#monthlyInput').prop('checked', false);

      if(res.carryforward == 1)
        $('#carryForward').prop('checked', true);
      else
        $('#carryForward').prop('checked', false);

      if(res.dw == 1)
        $('#salarySlab').prop('checked', true);
      else
        $('#salarySlab').prop('checked', false);

      if(res.roundtohigher == 1)
        $('#roundingToHeigher').prop('checked', true);
      else
        $('#roundingToHeigher').prop('checked', false);
      if(res.c == 1)
      {

        $('#fixedcolumn').prop('checked', true);
        $('#fixedvalue').attr('disabled', false);
        document.getElementById("fixedvalue").value = intToNumberBudget(res.ct);
        $('#formulamodal').hide();
        $('#salaryFormula').attr('disabled', true);
        $('#salaryFormula').val('');
      }
      else{
        $('#fixedcolumn').prop('checked', false);
        $('#fixedvalue').attr('disabled', true);
        $('#formulamodal').show();
        $('#salaryFormula').attr('disabled', false);
        $('#fixedvalue').val('');
      }
      if(res.mT == 1){
        $('#locationDependent').prop('checked', true);
        $('#locationdependenttr').show();
        $("#tblmodalstatedashboard tbody tr").each(function () {

          $(this).remove();
        });
        $.ajax({
          url: '<?php echo site_url(); ?>Ajax/ld_head_detail/'+ val,
          type: 'POST',
            success: function(data) { //salaryheadlist

              $("#salaryheadlist").html(data);


            }
          });
      }
      else{

        if ($('#tblmodalstatedashboard').length > 0) {
          $("#tblmodalstatedashboard tbody tr").each(function () {
            $(this).remove();
          });
        }

        $('#locationDependent').prop('checked', false);
        document.getElementById( 'locationdependenttr' ).style.display = 'none';


        $("#salaryheadlist").html(' ');
      }

    }else{

      document.getElementById("head_description").value = '';
      document.getElementById("head_abbreviation").value = '';
      document.getElementById("lowerrange").value = '';
      document.getElementById("upperrange").value = '';
      document.getElementById("maxlimit").value = '';
      document.getElementById("fixedvalue").value = '';
      document.getElementById("salaryFormula").value = '';
      document.getElementById("round_upto").value = '';

      $('#formulacolumn').prop('checked', false);
      $('#activeField').prop('checked', false);
      $('#workingDaysDependent').prop('checked', false);
      $('#monthlyInput').prop('checked', false);
      $('#carryForward').prop('checked', false);
      $('#roundingToHeigher').prop('checked', false);
      $('#locationDependent').prop('checked', false);
      $('#salarySlab').prop('checked', false);
      document.getElementById( 'locationdependenttr' ).style.display = 'none';
      $("#salaryheadlist").html(' ');
    }
  })
}

//input to comma separated with decimal
function intTo(nStr) {
 var cid='#'+nStr.id;
 var x1 = $(cid).val(intToNumberBudget(nStr.value));
 }


function intToFormatBudget(nStr) {
 nStr += '';
 x = nStr.split('.');
 x1 = x[0];
 x2 = x.length > 1 ? '.' + x[1] : '';
 var rgx = /(\d+)(\d{3})/;
 var z = 0;
 var len = String(x1).length;
 var num = parseInt((len / 2) - 1);

 while (rgx.test(x1)) {
   if (z > 0) {
     x1 = x1.replace(rgx, '$1' + ',' + '$2');
   }
   else {
     x1 = x1.replace(rgx, '$1' + ',' + '$2');
     rgx = /(\d+)(\d{2})/;
   }
   z++;
   num--;
   if (num == 0) {
     break;
   }
 }
 x2 = x.length > 1 ? '.' + x[1] : '';

                   // My addition for decimal point diplay
                   if (x2.length == 0) {
                     x2 = '.00';
                   } else if (x2.length == 2) {
                     x2 = '.' + x[1] + '0';
                   } else {
                     x2 = '.' + x[1];
                   }



                   return x1 + x2;
                 }

                 function changeformat(id) {
                   var val = document.getElementById(id).value;
                   val = val.replace(/,/g, '');
                   var cnt = parseInt(val.split(".").length - 1);
                   if (parseInt(cnt) > 1) {
                     val = val.replace(/./g, '');
                     alert('Invalid value.');
                     document.getElementById(id).value = "0";
                   }
                   else {
                     document.getElementById(id).value = intToNumberBudget(val);
                   }


                 }
                 function changeformatNumber(id) {
                   var val = document.getElementById(id).value;
                   val = val.replace(/,/g, '');
                   var cnt = parseInt(val.split(".").length - 1);
                   if (parseInt(cnt) > 1) {
                     val = val.replace(/./g, '');
                     alert('Invalid value.');
                     document.getElementById(id).value = "0";
                   }
                   else {
                     document.getElementById(id).value = intToNumberBudget(val);
                   }


                 }

                 function isNumberKey(evt) {
                   var charCode = (evt.which) ? evt.which : event.keyCode
                   if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                     return false;

                   return true;
                 }




   //               $(".isNumberKey").keypress(function (e) {
   //   //if the letter is not digit then display error and don't type anything
   //   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
   //      //display error message
   //     // $("#errmsg").html("Digits Only").show().fadeOut("slow");
   //     return false;
   //   }
   // });

// Formula making script for main screen.
var arraySet = [];
$('#add_in_formula').click(function(){
  var data = {};
  var percent_value = document.getElementById('percent_value').value;
  // var plus_minus = document.getElementById('plus_minus').value;
  var pay_type = document.getElementById('pay_type').value;
  if(percent_value == ''){
    $("#percent_value").addClass("is-invalid");
  }else{
    $("#percent_value").removeClass("is-invalid");
    data.payType = pay_type;
    // data.Operator = plus_minus;
    data.Percent = percent_value;
    if(arraySet.length > 0)
    {
      if (typeExists(data.payType, arraySet) == true)
      {
        arraySet.push(data);
      }
    }
    else
    {
      arraySet.push(data);
    }
  }
  formulaHtml();
  finalformulaHtml();
});
$('#remove_from_formula').click(function(){
  var data = {};
  var percent_value = document.getElementById('percent_value').value;
  // var plus_minus = document.getElementById('plus_minus').value;
  var pay_type = document.getElementById('pay_type').value;
  if(percent_value == ''){
    $("#percent_value").addClass("is-invalid");
  }else{
    $("#percent_value").removeClass("is-invalid");
    data.payType = pay_type;
    // data.Operator = plus_minus;
    data.Percent = percent_value;
    if(arraySet.length > 0)
    {
      if (typeExists(data.payType, arraySet) == false)
      {
        for(var i = 0; i < arraySet.length; i++) {
          if(arraySet[i].payType == data.payType) {
            arraySet.splice(i, 1);
            break;
          }
        }
      }
    }
    else
    {
      alert('No data added yet.');
    }
  }
  formulaHtml();
  finalformulaHtml();
});
function formulaHtml(){
  if(arraySet.length > 0){
    var formulaString = '';
    var counter = 1;
    arraySet.forEach(function(element) {
      // if(counter == 1){
      //   if(element.Operator != '+' ){
      //     formulaString = formulaString + element.Operator;
      //   }
      // }else{
      //   formulaString = formulaString + element.Operator;
      // }
      formulaString = formulaString + '|' + element.payType + '|<br>'; 
      counter++;
    });
    document.getElementById("formula_creation_detail").innerHTML = formulaString;
  }else{
    document.getElementById("formula_creation_detail").innerHTML = '';
  }
}

function finalformulaHtml(){
  if(arraySet.length > 0){
    let formulaString = '';
    let counter = 1;
    arraySet.forEach(function(element) {
      // if(counter == 1){
      //   if(element.Operator != '+' ){
      //     formulaString = formulaString + element.Operator;
      //   }
      // }else{
      //   formulaString = formulaString + element.Operator;
      // }
      formulaString = formulaString + '|' + element.payType + '|'; 
      counter++;
    });
    var percent_value = document.getElementById('percent_value').value;
    document.getElementById("finalformulaHtml").innerHTML = percent_value + '% ' + ' of (' +formulaString + ')';
  }else{
    document.getElementById("finalformulaHtml").innerHTML = '';
  }
}

function typeExists(payType, arraySet){
  let i=0;
  for(i=0; i<arraySet.length; i++ ){
    if(arraySet[i].payType == payType){
      return false;
    }
  }

  return true;
}
// Formula making script for main screen End.


// Formula making script for Location Dependent screen.
var LDarraySet = [];
$('#ld_add_in_formula').click(function(){
  var ld_data = {};
  var ld_percent_value = document.getElementById('ld_percent_value').value;
  // var ld_plus_minus = document.getElementById('ld_plus_minus').value;
  var ld_pay_type = document.getElementById('ld_pay_type').value;
  if(ld_percent_value == ''){
    $("#ld_percent_value").addClass("is-invalid");
  }else{
    $("#ld_percent_value").removeClass("is-invalid");
    ld_data.payType = ld_pay_type;
    // ld_data.Operator = ld_plus_minus;
    ld_data.Percent = ld_percent_value;
    if(LDarraySet.length > 0)
    {
      if (typeExists(ld_data.payType, LDarraySet) == true)
      {
        LDarraySet.push(ld_data);
      }
    }
    else
    {
      LDarraySet.push(ld_data);
    }
  }
  ldformulaHtml();
  ldfinalformulaHtml();
});
$('#ld_remove_from_formula').click(function(){
  var ld_data = {};
  var ld_percent_value = document.getElementById('ld_percent_value').value;
  // var ld_plus_minus = document.getElementById('ld_plus_minus').value;
  var ld_pay_type = document.getElementById('ld_pay_type').value;
  if(percent_value == ''){
    $("#percent_value").addClass("is-invalid");
  }else{
    $("#percent_value").removeClass("is-invalid");
    ld_data.payType = ld_pay_type;
    // ld_data.Operator = ld_plus_minus;
    ld_data.Percent = ld_percent_value;
    if(LDarraySet.length > 0)
    {
      if (typeExists(ld_data.payType, LDarraySet) == false)
      {
        for(var i = 0; i < LDarraySet.length; i++) {
          if(LDarraySet[i].payType == ld_data.payType) {
            LDarraySet.splice(i, 1);
            break;
          }
        }
      }
    }
    else
    {
      alert('No data added yet.');
    }
  }
  ldformulaHtml();
  ldfinalformulaHtml();
});
function ldformulaHtml(){
  if(LDarraySet.length > 0){
    var ldformulaString = '';
    var ldcounter = 1;
    LDarraySet.forEach(function(element) {
      // if(ldcounter == 1){
      //   if(element.Operator != '+' ){
      //     ldformulaString = ldformulaString + element.Operator;
      //   }
      // }else{
      //   ldformulaString = ldformulaString + element.Operator;
      // }
      ldformulaString = ldformulaString + '|' + element.payType + '|<br>'; 
      ldcounter++;
    });
    document.getElementById("ld_formula_creation_detail").innerHTML = ldformulaString;
  }else{
    document.getElementById("ld_formula_creation_detail").innerHTML = '';
  }
}

function ldfinalformulaHtml(){
  if(LDarraySet.length > 0){
    let ldformulaString = '';
    let ldcounter = 1;
    LDarraySet.forEach(function(element) {
      // if(ldcounter == 1){
      //   if(element.Operator != '+' ){
      //     ldformulaString = ldformulaString + element.Operator;
      //   }
      // }else{
      //   ldformulaString = ldformulaString + element.Operator;
      // }
      ldformulaString = ldformulaString + '|' + element.payType + '|'; 
      ldcounter++;
    });
    var ld_percent_value = document.getElementById('ld_percent_value').value;
    document.getElementById("ldfinalformulaHtml").innerHTML = ld_percent_value + '% ' + ' of (' +ldformulaString + ')';
  }else{
    document.getElementById("ldfinalformulaHtml").innerHTML = '';
  }
}
// Formula making script for Location Dependent screen End.
function slaryHeadSubmit(){
  var headfinancialyear = document.getElementById("financialyear").value;
// alert(headfinancialyear);
var selected_field_no = document.getElementById('selected_field_no').textContent;
if(selected_field_no){
  var ferror = false;
  selected_field_no = selected_field_no.replace('Field No =','');
}else{
  document.getElementById('errormessage_selectedfield').innerHTML = 'Select any fields.';
  var ferror = true;
}
var head_description  = document.getElementById('head_description').value;
var head_abbreviation = document.getElementById('head_abbreviation').value;
var salary_formula    = document.getElementById('salaryFormula').value;
var round_upto        = document.getElementById('round_upto').value;
var lowerrange        = document.getElementById('lowerrange').value;
var upperrange        = document.getElementById('upperrange').value;
var fixed_value       = document.getElementById('fixedvalue').value; 
var maxlimit          = document.getElementById('maxlimit').value;
    //var error             = '';
    //alert(salary_formula);
    if(head_description == ''){
      var hderror = true;
      $("#head_description").addClass("is-invalid");
    }else{
      var hderror = false;
      $("#head_description").removeClass("is-invalid");
    }
    if(head_abbreviation == ''){
      var haerror = true;
      $("#head_abbreviation").addClass("is-invalid");
    }else{
      var haerror = false;
      $("#head_abbreviation").removeClass("is-invalid");
    }

    ///// Commented on 6 march by amit kumar////////
    // if(lowerrange > upperrange || lowerrange < 0 || (lowerrange == 0 && upperrange == 0)){
    //   var lerror = true;
    //   $("#lowerrange").addClass("is-invalid");
    // }else{
    //   var lerror = false;
    //   $("#lowerrange").removeClass("is-invalid");
    // }
    // if(upperrange < 0 || (lowerrange == 0 && upperrange == 0)){
    //   var uerror = true;
    //   $("#upperrange").addClass("is-invalid");
    // }else{
    //   var uerror = false;
    //   $("#upperrange").removeClass("is-invalid");
    // }
     ///// Commented on 6 march by amit kumar////////

    // if(maxlimit < 0 || maxlimit < upperrange){
    //   var uerror = true;
    //   $("#maxlimit").addClass("is-invalid");
    // }else{
    //   var uerror = false;
    //   $("#maxlimit").removeClass("is-invalid");
    // }
    // if(salary_formula == ''){
    //   var sferror = true;
    //   $("#salaryFormula").addClass("is-invalid");
    // }else{
    //   var sferror = false;
    //   $("#salaryFormula").removeClass("is-invalid");
    // }


    var formulacolumn = activeField = workingDaysDependent = monthlyInput = carryForward = roundingToHeigher = locationDependent = fixedcolumn = salarySlab = 0;
    if($('#formulacolumn').prop('checked'))
      formulacolumn = 1;
    if($('#activeField').prop('checked'))
      activeField = 1;
    if($('#workingDaysDependent').prop('checked'))
      workingDaysDependent = 1;
    if($('#monthlyInput').prop('checked'))
      monthlyInput = 1;
    if($('#carryForward').prop('checked'))
      carryForward = 1;
    if($('#roundingToHeigher').prop('checked'))
      roundingToHeigher = 1;
    if($('#locationDependent').prop('checked'))
      locationDependent = 1; 
    if($('#fixedcolumn').prop('checked'))
      fixedcolumn = 1;
    if($('#salarySlab').prop('checked'))
      salarySlab = 1;
    //var inpuArray = '';
    // alert(lerror);
    if(ferror == false && hderror == false && haerror == false){
      $.ajax({
        url : '<?php echo site_url(); ?>Ajax/SalaryHead',
        type : 'POST',
        //dataType : 'json',
        data : {'selected_field_no':selected_field_no,'head_description':head_description, 'head_abbreviation':head_abbreviation, 'salary_formula':salary_formula, 'round_upto':round_upto, 'formulacolumn':formulacolumn, 'activeField':activeField, 'workingDaysDependent':workingDaysDependent, 'monthlyInput':monthlyInput, 'carryForward':carryForward, 'roundingToHeigher':roundingToHeigher, 'locationDependent':locationDependent, 'lowerrange':lowerrange, 'upperrange':upperrange, 'maxlimit':maxlimit, 'fixed_value':fixed_value, 'fixedcolumn':fixedcolumn,'salarySlab':salarySlab, 'headfinancialyear':headfinancialyear},
      })
      .done(function(data){
        document.getElementById('errormessage_upperlimit').innerHTML = '';
        if(data == 'update'){
          document.getElementById('success_message').innerHTML = 'Successfully updated!';
        }else{
          document.getElementById('success_message').innerHTML = 'Successfully inserted!';
        }
      })
    }else{
      if(lerror == true && lowerrange > 0){
        document.getElementById('errormessage_upperlimit').innerHTML = 'Lower range can not be greater than upper range.';
      }else{
        document.getElementById('errormessage_upperlimit').innerHTML = '';
      }
    }
  }

  $(".formulacreator").click(function() {
    //alert('testing success');
  });
  $("#statedashboardxyz td").click(function() {
    $("#statedashboardxyz td").removeClass("highlight");
    var selected = $(this).hasClass("highlight");
    if(!selected)
      $(this).addClass("highlight");
  });
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  

  // $('#statedashboardxyz').DataTable({
  //   "responsive": true,
  //   "bPaginate": true,
  //   "bInfo": false,
  //   "bFilter": false,
  //   "bLengthChange": false,
  //   "dom": '<lf<t>ip>',
  //   "iDisplayLength": 10
  //   /*"lengthMenu": [ [2, 4, 8, -1], [2, 4, 8, "All"] ]*/
  // }); 
  $.fn.DataTable.ext.pager.numbers_length = 3;
  
});
  function editldheaddetail(officeid){
    $('#officeid').find('option').each(function(i,e){
      if($(e).val() == officeid){
        $('#officeid').prop('selectedIndex',i);
      }
    });
    if (document.getElementById('formulafixed'+officeid).textContent) {
      $('#modalfixedcolumn').prop('checked', true);
      $('#modalfixedvalue').prop('disabled', false);
      $('#ldsalaryFormula').prop('disabled', true);
      $('#modallocationDependentFormulaModal').hide();
      
    }else{
      $('#modalformulacolumn').prop('checked', true);
      $('#modalfixedvalue').prop('disabled', true);
      $('#ldsalaryFormula').prop('disabled', false);
      $('#modallocationDependentFormulaModal').show();
    }
    document.getElementById("ldsalaryFormula").value = document.getElementById('formula'+officeid).textContent;
    document.getElementById("modalfixedvalue").value = document.getElementById('formulafixed'+officeid).textContent;
    document.getElementById("ldlowerrange").value = document.getElementById('formulalrange'+officeid).textContent;
    document.getElementById("ldupperrange").value = document.getElementById('formulaurange'+officeid).textContent;
  }
  function ajaxeditldheaddetail(officeid){
    var selected_field_no = document.getElementById('selected_field_no').textContent;
    if(selected_field_no){
      var ferror = false;
      selected_field_no = selected_field_no.replace('Field No =','');
    }else{
      document.getElementById('errormessage_selectedfield').innerHTML = 'Select any fields.';
      var ferror = true;
    }
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/ajaxeditldheaddetail/'+ officeid+'/'+selected_field_no.trim(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
      var res = JSON.parse(JSON.stringify(data));
      if(data){
        document.getElementById("ldsalaryFormula").value = res.lookuphead;
        document.getElementById("ldlowerrange").value = intToNumberBudget(res.lowerrange);
        document.getElementById("ldupperrange").value = intToNumberBudget(res.upperrange);
      }else{
        document.getElementById("ldsalaryFormula").value = '';
        document.getElementById("ldlowerrange").value = '';
        document.getElementById("ldupperrange").value = '';
      }
    })
  }
  function deleteldheaddetail(officeid){
    var selected_field_no = document.getElementById('selected_field_no').textContent;
    if(selected_field_no){
      var ferror = false;
      selected_field_no = selected_field_no.replace('Field No =','');
    }else{
      document.getElementById('errormessage_selectedfield').innerHTML = 'Select any fields.';
      var ferror = true;
    }
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/ajaxdeleteldheaddetail/'+ officeid+'/'+selected_field_no.trim(),
      type: 'POST',
    })
    .done(function(data) {
      alert('Record deleted successfully.');
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/ld_head_detail/'+ selected_field_no.trim(),
        type: 'POST',
        success: function(data) { //salaryheadlist

          $("#salaryheadlist").html(data);
        }
      });
    })
  }
  function valueChanged(){
    if($('#locationDependent').is(':checked')){
      $('.locatinDependentformulacreator').show();
    }else{
      $('.locatinDependentformulacreator').hide();
    }
  }
</script>