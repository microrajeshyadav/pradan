<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
        <h4>EXPERIENCE CERTIFICATE</h4>

      </div>  
      <div class="col-md-6 pull-left"><em>No. ____________________</em></div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
        <p><h4>TO WHOM IT MAY CONCERN</h4></p>

      </div>  

    </div>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
       Ms/ Mr. ________________ (name) was employed by this organization as Executive from _________ (date) to __________ (date). Prior to that s/he participated in our in-house training programme for Development Apprentices for 12 months in _____________ (Project/Location). This programme enables young professionals to develop the knowledge, skills and attitude necessary for a career in rural development.
     </div>
     <div class="col-md-12" style="margin-top: 20px;">
      PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has ____ (no. of regular employee) staff, including ______ (no. of professional). It works in seven of the poorest States in India. Ms. /Mr. ____________ (name) was part of a team of professionals implementing various development programme in __________ (Project/Location).
    </div>     
    <div class="col-md-12" style="margin-top: 20px;">
      Ms. /Mr.____________ (name) has amply demonstrated his/her ability to work under difficult conditions in remote villages. S/he displays empathy and maturity in working with poor people. Ms. /Mr. ___________ (name) was able to use his/her technical knowledge and skills to help poor people improve their livelihoods.
    </div> 
    <div class="col-md-12" style="margin-top: 20px;">
      I wish him/her very best in his/her life and career.
    </div>

  </div>

  <div class="row text-left" style="margin-top: 20px; ">
    <div class="col-md-6">
      Place: ________________________
    </div>
    <div class="col-md-6">
      Authorised Signatory    : __________________________
    </div>
  </div>

  <div class="row text-left" style="margin-top: 20px; ">
   <div class="col-md-6">
    Date : ________________
  </div>
  <div class="col-md-6">

  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    cc: - Personal Dossier (PD)
  </div>
</div>
</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>