<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
</style>
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
      <form method="POST" action="" name="experience_certificate" id="experience_certificate">
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
        <h4>EXPERIENCE CERTIFICATE</h4>

      </div>  
      <div class="col-md-6 pull-left"><em>No. <input type="text" name="experience_ceritficate_no" id="" value="<?php if($experience_detail){ echo $experience_detail->experience_ceritficate_no;}?>" class="inputborderbelow"></em></div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
        <p><h4>TO WHOM IT MAY CONCERN</h4></p>

      </div>  

    </div>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
       Ms/ Mr.<label class="inputborderbelow"> <?php echo $experience_certificate->name;?></label> (name) was employed by this organization as Executive from <label class="inputborderbelow"><?php echo $this->gmodel->changedatedbformate($experience_certificate->joiningdate);?></label>  (date) to <label class="inputborderbelow"><?php echo $this->gmodel->changedatedbformate($experience_certificate->seperatedate);?></label>  (date). Prior to that s/he participated in our in-house training programme for Development Apprentices for 12 months in <label class="inputborderbelow" value="" ><?php echo $experience_certificate->newoffice;?></label>  (Project/Location). This programme enables young professionals to develop the knowledge, skills and attitude necessary for a career in rural development.
     </div>
     <div class="col-md-12" style="margin-top: 20px;">
      PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has <input type="text" class="inputborderbelow" name="no_of_regular_employee" value="<?php if($experience_detail){ echo $experience_detail->no_of_regular_employee;}?>" required> (no. of regular employee) staff, including <input type="text" class="inputborderbelow" value="<?php if($experience_detail){ echo $experience_detail->no_of_professional;}?>" name="no_of_professional" required> (no. of professional). It works in seven of the poorest States in India. Ms. /Mr. <label class="inputborderbelow"><?php echo $experience_certificate->name;?></label>(name) was part of a team of professionals implementing various development programme in <label class="inputborderbelow" value="" ><?php echo $experience_certificate->newoffice;?></label>(Project/Location).
    </div>     
    <div class="col-md-12" style="margin-top: 20px;">
      Ms. /Mr.<label class="inputborderbelow"><?php echo $experience_certificate->name;?></label> (name) has amply demonstrated his/her ability to work under difficult conditions in remote villages. S/he displays empathy and maturity in working with poor people. Ms. /Mr. <label class="inputborderbelow" value="" ><?php echo $experience_certificate->name;?></label> (name) was able to use his/her technical knowledge and skills to help poor people improve their livelihoods.
    </div> 
    <div class="col-md-12" style="margin-top: 20px;">
      I wish him/her very best in his/her life and career.
    </div>

  </div>

  <div class="row text-left" style="margin-top: 20px; ">
    <div class="col-md-6">
      Place: <label class="inputborderbelow" value="" ></label> 
    </div>
  <!--   <div class="col-md-6">
      Authorised Signatory    : <label class="inputborderbelow" value="" ></label> 
    </div> -->
  </div>

  <div class="row text-left" style="margin-top: 20px; ">
   <div class="col-md-6">
    Date : <label class="inputborderbelow"> <?php echo date('d/m/Y');?></label> 
  </div>
  <div class="col-md-6">

  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    cc: - Personal Dossier (PD)
  </div>
</div>
</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($experience_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
  <?php
    }else{

      if($experience_detail->flag == 0){
 ?>
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
  <!-- <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
<?php } ?>
 
<?php } ?>
 <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</form>
</div>
</div>
</section>
<?php 
if($experience_detail){
if($experience_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  });
</script>
<?php } } ?>
