<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Sysrelation" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
          <form method="POST" action="">
             <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-7 panel-title pull-left">Edit Relation Type</h4>
               <div class="col-md-45 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>
          <div class="panel-body">  
              <div class="form-group">
                <div class="form-line">
                  <label for="StateCode" class="field-wrapper required-field">Relation Type <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control alphabateonly" id="name" name="name" minlength="3" maxlength="30" value="<?php echo $operation_details->relationname;?>">
                </div>
                <?php echo form_error("name");?>
              </div>

            <!--   <div class="form-group">
                <div class="form-line">
                  <label for="SNS" class="field-wrapper required-field">Status <span style="color: red;" >*</span></label>
                  <?php   

                        //$options = array('' => 'Select Status');     
                  $options //= array('0' => 'Active', '1' => 'InActive');
                 // echo form_dropdown('status', $options, $operation_details->status, 'class="form-control"'); 
                  ?>
                </div>
                <?php //echo form_error("status");?>
              </div> -->

            </div>
            <div class="panel-footer text-right"> 
              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
              <a href="<?php echo site_url("Sysrelation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>

<script type="text/javascript">
  $(document).ready(function () {
  //called when key is pressed in textbox

  $(".alphabateonly").keypress(function (e){
      var code =e.keyCode || e.which;
      if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46)  
      {
       alert("Only alphabates are allowed");
       return false;
     }
   }); 

});
</script>