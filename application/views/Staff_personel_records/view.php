<section class="content" style="background-color: #FFFFFF;" >
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-8 panel-title pull-left"> </h4>

       <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
      </div>

    </div>
    <hr class="colorgraph"><br>
  </div>
   <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

  <form name="Iom_transfer" id="Iom_transfer" action="" method="POST">
     <input type="hidden" name="staffid" id="staffid" value="<?php echo $staffid; ?>">
     <input type="hidden" name="tarnsid" id="tarnsid" value="<?php echo $tarnsid; ?>">
  <div class="panel-body">
    <div class="row"> 
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
<p style="text-align: center;"><strong>Inter-Office Memo (IOM) for Transfer</strong></p>

<p style="text-align: center;"><em>I</em><em>nter-Office Memo</em></p>
<p style="text-align: center;">&nbsp;</p>
<table style="width: 996px; height: 131px;">
<tbody>
  <tr>
<td style="width: 532px;">
<p>Transfer No.: 
<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $iom_staff_transfer->transferno; ?></label>
 </p>
</td>

<td style="width: 448px;"></td>
</tr>
<tr>
<td style="width: 532px;">
<p>To: <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">  <?php echo $getstaffdetails->Staffname;?></label></p>
</td>
<td style="width: 448px;">
<p>Ref: (from Personal Dossier)
<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"> <?php echo $personnel_name;?></label></p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>(Name, Designation of Employee being transferred)</p>
</td>
<td style="width: 448px;">
<p>Date: <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $this->gmodel->changedatedbformate($iom_staff_transfer->letter_date);?></label></p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>From:
<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $getstaffdetails->reportingtoname;?></label>
</p>
</td>
<td style="width: 448px;">
<p>Copy: See list below</p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>(Integrator/Executive Director)</p>
</td>
<td style="width: 448px;">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Subject: Your Transfer from 
<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
  <?php echo $getstaffdetails->oldoffice;?></label> to 

  <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $getstaffdetails->newoffice;?></label>&nbsp;</p>
<p></p>
<p></p>
<p style="text-align: justify;">I write to inform you that you have been transferred from <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
<?php echo $getstaffdetails->oldoffice;?></label> to <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
<?php echo $getstaffdetails->newoffice;?></label> .</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Please hand over charge of your responsibilities on <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $this->gmodel->changedatedbformate($iom_staff_transfer->responsibility_on_date);?>
</label> (date), and report for work at 
<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $iom_single_staff_transfer->officename;?>
</label>
  
 (place) on 
<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $this->gmodel->changedatedbformate($iom_staff_transfer->report_for_work_place_on_date);?>
</label> (date).</p>
<p style="text-align: justify;"></p>
<p style="text-align: justify;">Before proceeding to your new place of posting, you would need to hand over charge of your current responsibilities to

<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $iom_single_staff_transfer->currentresponsibilityto;?>
</label>

 . Please also ensure that you get a &lsquo;Clearance Certificate&rsquo; from all concerned (as per the enclosed proforma).</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">On arrival at your new place of posting, please hand in your joining report in the prescribed form to&nbsp; 
 <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $iom_single_staff_transfer->joiningreportprescribedformto;?>
</label> . Please also ensure that you send a copy to all concerned.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">If necessary, you may take a cash advance from our 

<label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $iom_single_staff_transfer->cashadvancefromouroffice;?>
</label>
  office to meet the travel expenses to join at the new place of your posting as per PRADAN&rsquo;s Travel Rules.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">You may avail of journey and joining time as per PRADAN's travel rules. You would be eligible to claim reimbursement of fare and local conveyance for yourself and your family on account of travel to your new place of work, actual expenses incurred to transport your vehicle and personal effects there and one month&rsquo;s basic pay as lump sum transfer allowance.&nbsp; Please claim these by filling the enclosed &ldquo;Travel Expenses Bill&rdquo; and submitting it to&nbsp; 

  <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
<?php echo $getstaffdetails->reportingtoname;?></label>( <em>Designation).</em></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><b>List of enclosures:</b></p>
<ol style="text-align: justify;">
<li>Clearance Certificate</li>
<li>Joining Report</li>
<li>Travel Expenses Bill</li>
<li>Handing Over/Taking over Charge</li>
</ol>
<p style="text-align: justify;"><strong>&nbsp;</strong></p>
<p style="text-align: justify;">cc: - Person concerned to whom charge is being given.</p>
<p style="text-align: justify;"> - Team Coordinator/Integrator at the old and new places of posting</p>
<p style="text-align: justify;">- Finance-Personnel-MIS Unit</p>

</div>  
</div>

 <div class="panel-footer text-right">
  <?php  if ($iom_single_staff_transfer->sendmail != 1) { ?>
  <a href="<?php echo site_url().'Staff_personnel_approval/genrate_transfer_letter/'.$token; ?>"  name="submit" id="submit" value="SendDataSave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to genrate pdf letter? Click on me.">Genrate PDF </a>
<?php } ?>
  <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
</div>
</form>
</div>
</div>   
</section>

<script>
  function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      return true;
    } else {
      return false;
    }
  }
  $(document).ready(function(){
   $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate : 0,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });
 

   $('[data-toggle="tooltip"]').tooltip();  
   $('#Inboxtable').DataTable(); 
 });


  //insertRows();

  $(document).ready(function () {
    $('#checkBtn').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        alert("You must check at least one checkbox.");
        return false;
      }else{

        if (!confirm('Are you sure?')) {
          return false;
        }

      }


    });
  });


  $("#chooseletter").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getYPRResponse/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#letter").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

</script>
