
<section class="content" style="background-color: #FFFFFF;" >
 <br>
   <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>


  <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Transfer </h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
      
              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablecampus" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                      
                       <th>Old Office</th>
                       <th>Proposed Transfer Office</th>
                       <th>Proposed Transfer Date</th>
                       <th>Reason</th>
                       <th>Sender Name</th>
                       <th>Request Status</th>
                       <th>Request Date</th>
                       <th style ="max-width:50px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                   <tbody>
                    <?php 
                    //print_r($getworkflowdetail);
                    $i=0;
                    foreach ($getworkflowdetail as $key => $value) {
                      //print_r($value);

                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->officename;?></td>
                      <td><?php echo $value->newoffice;?> </td>
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->reason;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td><?php echo $value->flag; ?></td>
                      <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                      <td class="text-center">
                        <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a><br>
                          <?php if ($value->status==5) {?>
                       | <a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personel_records/add_iom/".$value->staffid.'/'.$value->transid;?>">INTER-OFFICE MEMO (IOM) FOR TRANSFER</a><br>


                         | <a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Handed_over_taken_over/index/".$value->staffid.'/'.$value->transid;?>">HANDING OVER/TAKING OVER CHARGE</a><br>


                         | <a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Transfer_claim_expense/index/".$value->staffid.'/'.$value->transid;?>">TRANSFER EXPENSES CLAIM</a>

                        <?php }
                          if ($value->status==9 or $value->status==11) {
                            ?>

<a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Handed_over_taken_over_transfer/index/".$value->transid;?>">HANDING OVER/TAKING OVER CHARGE</a>
                        <?php 
                      }
                      if ($value->status==13 && !empty($value->tbl_hand_over_taken_over_charge_id)) {
                            ?>

<a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Handed_over_taken_over_transfer/view/".$value->transid.'/'.$value->tbl_hand_over_taken_over_charge_id;?>">HANDING OVER/TAKING OVER CHARGE</a>
                        <?php 
                      }

                    
                    
                     ?>
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
            </div> 
          </div>
        


        <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Change Responsibility </h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
        
              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablecampus" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>Date</th>
                       <th>Old Designation</th>
                       <th>New Designation</th>
                       <th>Reason</th>
                       <th>Comments</th>
                       <th>Sender Name</th>
                       <th>Status</th>
                       <th style ="max-width:50px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                <?php if (count($getpromotionworkflowdetail) ==0) { ?>
                 <tbody>
                   <tr>
                     <td colspan="13">
                       Record not found!!!
                     </td>
                   </tr>
                 </tbody>
               <?php  }else{  ?>
                   <tbody>
                    <?php 
                    $i=0;
                    foreach ($getpromotionworkflowdetail as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                     
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?> </td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td><?php echo $value->flag; ?></td>
                      <td class="text-center">
                        <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>
                          <?php if ($value->status==5) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Changeresponsibility_handed_over_taken_over/index/".$value->transid;?>">Handed Over</a>
                    <?php } ?>
                         <?php if ($value->status==4 || $value->status==6) { ?>
                         |<a class="btn btn-primary btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>">Approval</a>

                        <?php } ?>
                       
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                <?php } ?>
                </table>
              </div>
            </div>
            </div> 
          </div>
        

  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Transfer & Change Responsibility </h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
       

              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablecampus" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                      
                       <th>Old Office</th>
                       <th>Proposed Transfer Office</th>
                       <th>Proposed Transfer Date</th>
                       <th>Old Designation</th>
                       <th>New Designation</th>
                       <th>Reason</th>
                       <th>Comments</th>
                       <th>Sender Name</th>
                       <th>Request Status</th>
                       <th>Request Date</th>
                       <th>Status</th>
                       <th style ="max-width:50px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                <?php if (count($gettransferworkflowdetail) ==0) { ?>
                 <tbody>
                   <tr>
                     <td colspan="15">
                       Record not found!!!
                     </td>
                   </tr>
                 </tbody>
               <?php  }else{  ?>


                   <tbody>
                    <?php 
                    $i=0;
                   // print_r($gettransferworkflowdetail);
                    foreach ($gettransferworkflowdetail as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                     
                      <td><?php echo $value->officename;?></td>
                      <td><?php echo $value->newoffice;?> </td>
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?></td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td><?php echo $value->flag; ?></td>
                      <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                      <td><?php echo $value->process_type ;?></td>
                      <td class="text-center">
                        <a class="btn btn-success btn-xs" target="_blank" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>
                          <?php if ($value->status==5) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." target="_blank" href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->staffid.'/'.$value->transid;?>"> INTER-OFFICE MEMO (IOM) FOR TRANSFER</a>
                    <?php } ?>
                         <?php if ($value->status==4 || $value->status==6) { ?>
                         <a class="btn btn-primary btn-xs" title = "Click here to approvle this request submitted."  href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>">Approval</a>

                        <?php } ?>

                         <?php if ($value->status==9) { ?>
                         <a class="btn btn-primary btn-xs" title = "Click here to approvle this request submitted."  href="<?php echo site_url()."Handed_over_taken_over/index/".$value->transid;?>">Handed over</a>

                        <?php } ?>
                        <?php if ($value->status==15 || $value->status==17) { ?>
                         <a class="btn btn-primary btn-xs" title = "Click here to approvle this request submitted."  href="<?php echo site_url()."Transfer_claim_expense/index/".$value->transid;?>">TRANSFER EXPENSES</a>

                        <?php } ?>
                       
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                <?php } ?>
                </table>
              </div>
            </div>
            </div> 
          </div>
        </div>


     
   </section>

    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
      });
    </script>  


    <script type="text/javascript">
     $(document).ready(function(){
      $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
    });
  </script>
  <script type="text/javascript">
    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>