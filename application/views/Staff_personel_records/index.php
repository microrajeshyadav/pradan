<?php //echo "hsddghdg"; ?>
<section class="content" style="background-color: #FFFFFF;" >
 <br>
 <?php 
 $tr_msg= $this->session->flashdata('tr_msg');
 $er_msg= $this->session->flashdata('er_msg');

 if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>


      <div class="container-fluid">
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left"> Transfer </h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
          
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <tbody>
                <?php if (count($getworkflowdetail) ==0) { ?>                 
                
                 <?php 
               }else{
                $i=0;

                foreach ($getworkflowdetail as $key => $value) {
                      //print_r($value);

                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->sendername;?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."TStaff_history/index/".$value->transid;?>">History</a><br>
                      </td>
                    </tr>
                    <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div> 
        </div>
        


        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left"> Change Responsibility </h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
          
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus1" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Date</th>
                   <th>Old Designation</th>
                   <th>New Designation</th>
                   <th>Reason</th>
                   <th>Comments</th>
                   <th>Sender Name</th>
                   <th>Status</th>
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($getpromotionworkflowdetail) ==0) { ?>
                 <tbody>
                   
                 </tbody>
               <?php  }else{  ?>
                 <tbody>
                  <?php 
                  $i=0;
                  foreach ($getpromotionworkflowdetail as $key => $value) {
                    $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?> </td>
                      <td><?php echo $value->scomments;?></td>
                      <td ><?php echo $value->sendername;?></td>
                      <td class="text-center"><span class="badge badge-pill badge-info col-md-12"><?php echo $value->flag; ?></span></td>
                      <td class="text-center">
                        <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>

                       
                     </td>
                   </tr>
                   <?php $i++; } ?>
                 </tbody>
               <?php } ?>
             </table>
           </div>
         </div>
       </div> 
     </div>
     

     <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left"> Transfer & Change Responsibility </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
       

      <div class="row" style="background-color: #FFFFFF; ">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white; overflow-x:auto;">
          <table id="tablecampus2" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
               <th style ="max-width:50px;" class="text-center">S. No.</th>
               
               <th>Name</th>
               
               <th>Old Office</th>
               <th>Proposed Transfer Office</th>
               <th>Proposed Transfer Date</th>
               <th>Old Designation</th>
               <th>New Designation</th>
               <th>Reason</th>
               <th>Comments</th>
               <th>Sender Name</th>
               <th>Request Status</th>
               <th>Request Date</th>
               <th>Status</th>
               <th style ="max-width:50px;" class="text-center">Action</th>
             </tr> 
           </thead>
           <?php if (count($gettransferworkflowdetail) ==0) { ?>
             <tbody>
              
             </tbody>
           <?php  }else{  ?>


             <tbody>
              <?php 
              $i=0;
                   // print_r($gettransferworkflowdetail);
              foreach ($gettransferworkflowdetail as $key => $value) {
                $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  
                  <td><?php echo $value->name;?></td>
                  <td><?php echo $value->officename;?></td>
                  <td><?php echo $value->newoffice;?> </td>
                  <td><?php echo $proposed_date_bdformat;?> </td>
                  <td><?php echo $value->olddesid;?></td>
                  <td><?php echo $value->newdesid;?></td>
                  <td><?php echo $value->reason;?></td>
                  <td><?php echo $value->scomments;?></td>
                  <td><?php echo $value->sendername;?></td>
                  <td><span class="badge badge-pill badge-success"><?php echo $value->flag; ?></span></td>
                  <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                  <td><?php echo $value->process_type ;?></td>
                  <td class="text-center">
                    <a class="btn btn-success btn-xs" target="_blank" title = "Click here to approvle this request submitted." href="<?php echo site_url()."TCRStaff_history/index/".$value->transid;?>">History</a>

                   
                 </td>
               </tr>
               <?php $i++; } ?>
             </tbody>
           <?php } ?>
         </table>
       </div>
     </div>
   </div> 
 </div>
 <div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
      <h4 class="col-md-7 panel-title pull-left">Seperation Approval List</h4>
    </div>
    <hr class="colorgraph"><br>
  </div>
  <div class="panel-body">
    <table id="staff_transfer_history" class="table table-bordered table-striped table-hover dataTable js-exportable">
      <thead>
        <tr>
          <th class="text-center" style="width: 50px;">S.NO</th>
          <th class="text-center">Seperate Date</th>
          <th class="text-center">Staff</th>
          <th class="text-center">Type</th>
          <th class="text-center">Sender</th>
          <th class="text-center">Recevier</th>
          <th class="text-center">Request Date</th>
          <th class="text-center">Status</th>
          <th class="text-center">Comment</th>
          <th class="text-center">Action</th>
        </tr>
      </thead>
      <?php if (count($staff_seperation)==0) { ?>
        <tbody>
         
        </tbody>
      <?php }else{ ?>
        <tbody>
          <?php
          $i=0; foreach($staff_seperation as $grow){ 
        //print_r($view_history);
            ?>
            <tr>
              <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
              <td class="text-center"><?php echo $this->gmodel->changedatedbformate($grow->proposeddate);?></td>
              <td class="text-center"><?php echo $grow->name; ?></td>
              <td class="text-center"><?php echo $grow->process_type; ?></td> 
              <td class="text-center"><?php echo $grow->sendername; ?></td> 
              <td class="text-center"><?php echo $grow->receivername; ?></td> 
              <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($grow->Requestdate); ?> </td> 
              <td class="text-center"><span class="badge badge-pill badge-info col-md-12"><?php echo $grow->flag; ?> </span></td> 
              <td class="text-center"><?php echo $grow->scomments;?> </td> 
              <td class="text-center">

                
              </td>
            </tr>
            <?php  $i++; } ?>
          </tbody>
        <?php  } ?>
      </table>
    </div>
  </div>
</div>



</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable({
      "paging": true,
      "search": true, 
    });
    $('#tablecampus1').DataTable({
      "paging": true,
      "search": true, 
    });
     $('#tablecampus2').DataTable({
      "paging": true,
      "search": true, 
    });
      $('#staff_transfer_history').DataTable({
      "paging": true,
      "search": true, 
    });
    
  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>