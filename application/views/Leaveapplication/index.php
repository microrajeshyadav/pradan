<section class="content" style="background-color: #FFFFFF;" >
	<!-- <?php echo phpinfo(); ?> -->
	<?php foreach ($role_permission as $row) { if ($row->Controller == "Leaverequest" && $row->Action == "index"){ ?>
		<br>
		<div class="container-fluid">
			<div class="panel thumbnail shadow-depth-2 listcontainer" >
				<form method="post" action="" name="applicationform">
					<div class="panel-heading">
						<div class="row">
							<h4 class="col-md-9 panel-title pull-left">Leave Application </h4>

							<div class="col-md-3 text-right">
								<a href ="<?php echo site_url('leaverequest/index/'.$this->loginData->staffid)?>" class="btn btn-sm btn-dark" >Back</a>
								<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-question-circle" aria-hidden="true"></i>
								</button>


								<button type="submit" name="Apply" class="btn btn-sm btn-primary" title="Add a new leave request? Click on me."  data-toggle="tooltip" data-placement="bottom" id="apply">Apply</button>

							</div>
						</div>
						<hr class="colorgraph"><br>
					</div>
					<div class="panel-body" style="font-family: 'Oxygen'">
						<?php 
						$tr_msg= $this->session->flashdata('tr_msg');
						$er_msg= $this->session->flashdata('er_msg');

						if(!empty($tr_msg)){ ?>
							<div class="content animate-panel">
								<div class="row">
									<div class="col-md-12">
										<div class="hpanel">
											<div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
												<b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
											</div>
										</div>
									</div>
								</div>
							<?php } else if(!empty($er_msg)){?>
								<div class="content animate-panel">
									<div class="row">
										<div class="col-md-12">
											<div class="hpanel">
												<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								<div class="row" style="background-color: #FFFFFF;">
									<input type="hidden" name="office_id" id="office_id" value="<?php echo $getreportingdetails->new_office_id;?>">
									<div class="col-xs-12 col-sm-12 col-md-12 bg-dark text-white" style="margin-bottom: 20px; padding: 20px;">
										<div class="col-md-4">
											<strong>Staff: </strong>
											<input type="hidden" name="hdnempcode" id="hdnempcode" value="<?php echo $getreportingdetails->empcode; ?>">
											<label name="Staffname"><?php echo $getreportingdetails->empcode.' - '. $getreportingdetails->staffname;?></label>
										</div>
										<div class="col-md-4">
											<strong>Designation: </strong>
											<label name="Staffdesignation"><?php echo $getreportingdetails->staffdesignation;?></label>
										</div>
										<div class="col-md-4">
											<strong>Office:  </strong>
											<label name="Staffoffice"><?php echo $getreportingdetails->staffofficename;?></label>
										</div>

									</div>
									<div class="col-md-8">
										<div class="col-md-12">
											<div class="card" style="min-height: 34px;">
												<div class="card-body bg-light">
													<table class="table" id="tableleave">
														<thead>
															<tr class="bg-info text-white">
																<th class="text-center">Sr.No</th>
																<th class="text-left">Leave Type</th>
																<th class="text-right">OP Bal</th>
																<th class="text-right">Accured</th>
																<th class="text-right">Availed</th>
																<th class="text-right">Balance</th> 
															</tr>
														</thead>
														<tbody>
															<?php 
															$i=0;
															foreach ($leavebalance as $key => $value) {
																?>
																<tr>
																	<td class="text-center"><?php echo $i+1;?>. </td>
																	<td class="text-left"><?php echo $value->Ltypename;?></td>
																	<td class="text-right"><?php echo $value->OB;?></td>
																	<td class="text-right"><?php echo $value->Accured;?></td>
																	<td class="text-right"><?php echo $value->Availed;?></td>
																	<td class="text-right">
																		<input type="hidden" name="availbalance" id="availbalance" value="<?php echo $value->Balance;?>" ><?php echo $value->Balance;?>
																	</td> 
																</tr>
																<?php $i++; } ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="col-md-12" style="margin-top: 20px;">
												<div class="card" style="min-height:290px;">
													<div class="card-body"> 
														<div class="row">                      
															<div class="col-md-2">
																<label labelfor="leavetype" style="">
																	Leave Type <span style="color:red">*</span> :
																</label>
															</div>
															<div class="col-md-4">
																<select class="form-control" style="" name="leavetype" id="leavetype" required >
																	<option value="">--Select--</option>
																	<?php foreach($leavecategory as $row){ ?>
																		<option value="<?php echo $row->Ltypeid; ?>"> <?php echo $row->Ltypename; ?></option>
																	<?php } ?>

																</select>
															</div>
															<div  id = "maternityrow" class=" form-inline col-md-4 d-none">
																<label labelfor="leavefrom" style="" >
																	Due Date  <span style="color:red">*</span>: 
																</label>
																<input type="text" autocomplete="off" class="form-control datepicker"  name="inputmaternityduedate" id="inputmaternityduedate" value="" >

															</div>

														</div>
														<div class="row" style="margin-top: 20px;">
															<div class="col-md-2">
																<label labelfor="leavefrom" style="" >
																	From  <span style="color:red">*</span>: 
																</label>
															</div>
															<div class="col-md-4">
																<input type="text" autocomplete="off" class="form-control datepicker"  name="fromdate" id="fromdate" value="" required>  
															</div> 
															<div class="col-md-1">                      
																<label labelfor="leaveto" style="">
																	To  <span style="color:red">*</span>: 
																</label>
															</div>
															<div class="col-md-5">
																<input type="text" autocomplete="off" class="form-control datepicker" name="todate" id="todate" value=""    required>
															</div>
														</div>
														<div class="row" style="margin-top: 20px;">
															<div class="col-md-2">
																<label labelfor="leavetype" style="margin-right: 40px; ">
																	Purpose  <span style="color:red">*</span>:
																</label>
															</div>
															<div class="col-md-4">
																<select class="form-control" style="" name="purpose" 
																id="purpose"   required>
																<option value="">--Select--</option>
																<option value="1">Medical - Self</option><option value="9">Medical - Family</option>
																<option value="2">Bereavement - Family</option>
																<option value="3">Bereavement - Others</option>
																<option value="4">Wedding - Immediate Family</option>
																<option value="5">Wedding - Others</option>
																<option value="6">Visiting Home</option>
																<option value="7">Out of Station</option>
																<option value="8">Vacation</option>
																<option value="9">Maternity</option>
																<option value="10">Others</option>
															</select>
														</div>
														<div class="col-md-1">
															<label labelfor="leavetype" style="">
																Reason  <span style="color:red">*</span>: 

															</label>
														</div>
														<div class="col-md-5">
															<input name="reason" maxlength="100" class="form-control" style="" required>

														</div>
													</div>
													<div class="row" style="margin-top: 20px;"> 
														<div class="col-md-2">
															<label labelfor="leavetype" style="">
																No of days: 

															</label>
														</div>
														<div class="col-md-4">
															<input type="text" class="form-control" style="" name="noofdays" id="noofdays" value=""  readonly="readonly" required>
															<input  type="hidden" id="hdnnoofdays">
														</div>  
														<div class="col-md-6">

															<table class="table table-stripped" id="tblnodays">


															</table>
														</div>                     

													</div>

												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="col-md-12">
											<div class="card text-white" style="background: #2A9FB8">
												<div class="card-body" style="padding-bottom: 25px; padding-top: 25px;">
													<div id="calendar" class="calendar1">

													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="card">
												<div class="card-body bg-info text-white">

													<div class="row">
														<div class="col-md-5">
															<label labelfor="labelsupervisor" style="">
																Supervisor Name: 
															</label>
														</div>
														<div class="col-md-7">
															<label labelfor="supervisorname" class="">
																<strong> <?php echo $getreportingdetails->reportingname;?> </strong>
															</label>                   
														</div>
													</div>
													<div class="row" style="margin-top: 20px;">
														<div class="col-md-5">
															<label labelfor="labelcontactnumber" style="">
																Contact Number:  
															</label>
														</div>
														<div class="col-md-7">
															<label for="contactnumber" class="">
																<strong><?php echo $getreportingdetails->contact;?> </strong>
															</label>
														</div>
													</div>
													<div class="row" style="margin-top: 20px;">
														<div class="col-md-5">
															<label labelfor="labelcontactnumber" style="">
																Address: 
															</label>
														</div>
														<div class="col-md-7">
															<strong>
																<label for="contactnumber" class="" style="word-wrap: break-word;">

																	<?php echo str_replace(",",", ",$getreportingdetails->permanenthno.','. $getreportingdetails->permanentstreet.','.$getreportingdetails->permanentcity.','.$getreportingdetails->permanentpincode) ;?> </label>
																</strong> 
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>




									</div> 
								</div>
							</form>
						</div>
					</div>
				<?php } } ?>
			</section>

			<!-- Modal -->

			<div class="modal fade" id="ModalDueDate" role="dialog">
				<div class="modal-dialog">

					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<!-- <h4 class="modal-title">Modal Header</h4> -->
						</div>
						<div class="modal-body">          <!-- <div class="row"> -->
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<label for="Name">Maternity Due Date <span style="color: red;" >*</span></label>
								<input class="form-control datepicker" data-toggle="" id="m_duedate" name="m_duedate" type="text" placeholder="Enter Maternity Due Date" style="max-width: 400px;" maxlength="250" > 
								<?php echo form_error("m_duedate");?>

							</div>
						</div>
						<!-- </div> -->
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<input type="button" name="saveandsubmit" value="save & submit" class="btn btn-success btn-sm" onclick="">
						</div>
					</div>

				</div>
			</div>


			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">FAQ</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>

						</div>
						<div class="modal-body">
							<p><strong>Leave less than 30 days</strong> - Leave upto maximum 30 days (excluding Sundays and declared holidays during the period) in one spell is granted by supervisor.</p>
							<p><strong>Leave for more than 30 days</strong> - Leave exceeding 30 days (excluding Sundays and declared holidays during the period) in one spell need approval from Executive Director on recommendation of Team Coordinator and /or Integrator.</p>
							<p><strong>Maternity Leave</strong>- Maternity leave is for twenty six weeks (including Sundays and Holidays) of which not more than eight weeks should precede the date of her expected delivery Eligibility for Maternity leave- Woman employee who has worked for a period of not less than 182 days in the twelve months immediately preceding the date of her expected delivery. <span style="color:red"> (Note- If your required more than 182 days leaves then you can use your general leaves if available otherwise you can use LWP.) </span>
							</p>
							<p><strong>Paternity leave</strong>- Male employee may claim paternity leave for 7 days during the period from 15 days before to six month after the child birth.</p>
							<p><strong>Sabbatical/ study leave</strong>- May be granted to only those who have completed 3 yrs in PRADAN ( Excluding DAship and Probation)</p>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>

			<script type="text/javascript">

				$(document).ready(function(){

					$('#apply').on('click',function() {

						var fromdate=$("#fromdate").val();
						//alert(fromdate);
						var todate=$("#todate").val();
						var office_id=$("#office_id").val();

						//alert("hello"+office_id);

						  $.ajax({
          data:{fromdate:fromdate,todate:todate,office_id:office_id},
          url: '<?php echo site_url(); ?>Ajax/leaveclosing/',
          type: 'POST'
        })
        .done(function(data) {
        	//alert(data);
        	if(data>0)
        	{
        		alert("Your leave has been closed");
        		$('#noofdays').val('');
                  $('#hdnnoofdays').val('');
                  ("#fromdate").val('');
                  $("#todate").val('');
        	}

        	 return  false;
    });
   // });




					});

					var d = new Date();
					d.setDate(d.getDate() + 56);  

					$('#inputmaternityduedate').on('change',function() {
						duedate=$('#inputmaternityduedate').val();
						var date1 = duedate.split('/')
						var newDate = date1[1] + '/' +date1[0] +'/' +date1[2];  
						var date = new Date(newDate);
						date.setMonth(date.getMonth()+2);
						var year = date.getFullYear();
						var month = `${date.getMonth() + 1}`.padStart(2, 0);
						var day = `${date.getDate()}`.padStart(2, 0);
						var finalDate = [day, month, year].join("/") ;
						$('#fromdate').val(finalDate);
						 date.setDate(date.getDay()+182);
						 year = date.getFullYear();
						 month = `${date.getMonth() + 1}`.padStart(2, 0);
						 day = `${date.getDate()}`.padStart(2, 0);
						 finalDate = [day, month, year].join("/") ;
					   $('#todate').val(finalDate);

             var todate = $('#todate').val();
    var fromdate=$("#fromdate").val();
    var office_id=$("#office_id").val();
    var availbalance=$("#availbalance").val();
    var hdnempcode=$("#hdnempcode").val();
    var leavetype=$("#leavetype").val();


             $.ajax({
          data:{fromdate:fromdate,todate:todate,office_id:office_id,leavetype:leavetype},
          url: '<?php echo site_url(); ?>Ajax/leave_day/',
          type: 'POST'
        })
        .done(function(data) {
          var obj=JSON.parse(data);
          $('#noofdays').val(obj.leave_day);
          $('#hdnnoofdays').val(obj.leave_day);

          $.ajax({
            data:{leavetype:leavetype},
            url: '<?php echo site_url(); ?>Ajax/leave_cal/',
            type: 'POST'
          })
          .done(function(data) {

            var obj1=JSON.parse(data);

            var maxleavedays;
            if (leavetype==9)
            {
              maxleavedays = availbalance;
            }
            else
            {
              maxleavedays =obj1.maxleave;
              minleave=obj1.minleave;

            }
            if(parseFloat(minleave)>parseFloat($('#hdnnoofdays').val())   )
            {
              alert('In this leave category Minimum leave you can apply is ' + minleave +'.');
              $('#noofdays').val('');
              $('#hdnnoofdays').val('');
              $("#todate").val('');
              var rowCount = $('#tblnodays >tbody >tr').length;

              if (rowCount>0)
              {
                $("#tblnodays tbody tr").each(function () {

                  $(this).remove();
                });

              }
              return  false;
            }
            if(parseFloat(maxleavedays)<parseFloat($('#hdnnoofdays').val())  &&  parseFloat(maxleavedays) !=0 )
            {
              alert('In this leave category maximum leave you can apply is ' + maxleavedays +'.');

              $('#noofdays').val('');
              $('#hdnnoofdays').val('');
              $("#todate").val('');
              var rowCount = $('#tblnodays >tbody >tr').length;

              if (rowCount>0)
              {
                $("#tblnodays tbody tr").each(function () {

                  $(this).remove();
                });

              }
              return  false;
            }
            else
            {

              $.ajax({
                data:{fromdate:fromdate,todate:todate,office_id:office_id,leavetype:leavetype},
                url: '<?php echo site_url(); ?>Ajax/Checkholidaylist/',
                type: 'POST'

              })
              .done(function(data) {
                var obj2=JSON.parse(data);

                if(obj2.check==0)
                {
                  alert('Hoiday list not prepared for dates you are selected, kindly contact to your supervisor for same!');
                  $('#noofdays').val('');
                  $('#hdnnoofdays').val('');
                  ("#fromdate").val('');
                  $("#todate").val('');
                  var rowCount = $('#tblnodays >tbody >tr').length;

                  if (rowCount>0)
                  {
                    $("#tblnodays tbody tr").each(function () {

                      $(this).remove();
                    });

                  }
                  return  false;
                }

              });

              if(obj.leave_day>1)
              {
                $("#tblnodays tbody tr").each(function () {

                  $(this).remove();
                });

                var tablebody='<tbody><tr><td>';
                tablebody+=fromdate;
                tablebody+='</td><td>'; 
                tablebody+='<select name="firstrow" class="form-control" id="ddlfirst" onchange="calculate_noofdays();"> <option name="firstoption" value="full">Full Day</option><option name="secoundoption" value="second" required>Second Half</option></select>';
                tablebody+='</td></tr><tr><td>';
                tablebody+=todate;
                tablebody+='</td><td>';
                tablebody+='<select name="secondrow" class="form-control" id="ddlsecond" onchange="calculate_noofdays();"> <option name="firstoption" value="full">Full Day</option><option name="secoundoption" value="first" required>First Half</option></select>';
                tablebody+='</td></tr></tbody>';
                $('#tblnodays').append(tablebody);

              }
              if(obj.leave_day==1)
              {
                $("#tblnodays tbody tr").each(function () {

                  $(this).remove();
                });
                var tablebody='<tbody><tr><td>';
                tablebody+=fromdate;
                tablebody+='</td><td>';
                tablebody+='<select name="firstrow" class="form-control"> <option name="firstoption" value="full">Full Day</option><option name="secoundoption" value="first">First Half</option><option name="secoundoption" value="second" required>Second Half</option></select>';
                tablebody+='</td>';
                '</tr></tbody>';
                $('#tblnodays').append(tablebody);

              }
              else
              {
                $('#tblnodays').clear();
              }

            }

          })
        })

					} )

					$('#leavetype').on('change', function() {
						cleardetails();
						var leavetype=$("#leavetype").val();
   // alert("leavetype="+leavetype);
   if(leavetype==5)
   {
   	var availbalance=$("#availbalance").val();
    //alert(availbalance);
    if(availbalance>0)
    {
    	alert('You have balance in your General Leave '+availbalance+', so you can not apply LWP(Leave without pay). Kindly first consume your General Leave.');
    }
}
if (leavetype=="3")
{  
	$('#fromdate').datepicker("option", "maxDate", 56);
	$('#maternityrow').removeClass("d-none");
	$('#inputmaternityduedate').attr("required",true);

}else{
	$('#fromdate').datepicker("option", "maxDate", '');
	$('#maternityrow').addClass("d-none");
	$('#inputmaternityduedate').attr("required",false);

}




});

					$('#fromdate').datepicker({
						dateFormat: 'dd/mm/yy',
						minDate: '-1m',   
						//yearRange: '-0:+5',
						changeMonth: true,
						changeYear: true,
					});
   // $('#fromdate').datepicker("option", "minDate", -30);

   $('#inputmaternityduedate').datepicker({
   	dateFormat: 'dd/mm/yy',
   // minDate: '+2m',  
    // $('#fromdate').datepicker("option", "maxDate", 56); 
    yearRange: '-0:+5',
    // minDate: '+2m',
    changeMonth: true,
    changeYear: true,
});
   // $('#inputmaternityduedate').datepicker("option", "maxDate", 60);


   $('#todate').datepicker({
   	dateFormat: 'dd/mm/yy',
   	minDate: '-1m',
   	maxDate:1095,
   	// yearRange: '-0:+5',
   	changeMonth: true,
   	changeYear: true,
   });

   $('#fromdate').datepicker().bind("change", function () {
   	var leavetype=$("#leavetype").val();
   	if (leavetype=="")
   	{
   		alert ('Please select leave type.');
   		$('#fromdate').val('');
   		return false;
   	}
   	var minValue = $(this).val();
   	minValue = $.datepicker.parseDate("dd/mm/yy", minValue);
   	$('#todate').datepicker("option", "minDate", minValue);
   	calculate();
   });

   $('#todate').change(function(){
   	var todate = $('#todate').val();
   	var fromdate=$("#fromdate").val();
   	var office_id=$("#office_id").val();
   	var availbalance=$("#availbalance").val();
   	var hdnempcode=$("#hdnempcode").val();

   	var leavetype=$("#leavetype").val();


   	if (leavetype=="")
   	{
   		alert ('Please select leave type.');
   		$('#noofdays').val('');
   		$('#hdnnoofdays').val('');
   		$("#todate").val('');
   		var rowCount = $('#tblnodays >tbody >tr').length;

   		if (rowCount>0){
   			$("#tblnodays tbody tr").each(function () {
   				$(this).remove();
   			});
   		}
   		return false;
   	}

    $.ajax({
          data:{fromdate:fromdate,todate:todate,office_id:office_id},
          url: '<?php echo site_url(); ?>Ajax/leaveclosing/',
          type: 'POST'
        })
        .done(function(data) {
       // 	alert(data);
        	if(data>0)
        	{
        		alert("Your leave has been closed");
        	}

        });


   	$.ajax({
   		data:{fromdate:fromdate,todate:todate,office_id:office_id,leavetype:leavetype,hdnempcode:hdnempcode},
   		url: '<?php echo site_url(); ?>Ajax/leave_days_availed/',
   		type: 'POST'
   	})
   	.done(function(data) {
   		if(data > 0){
   			//alert(data);
   			alert('You already applied leaves with these selected dates, so you can not apply leave with these dates. To confirm please check in leave aplied or leave history for same.');
   			cleardetails();
   		}else{
   			$.ajax({
   				data:{fromdate:fromdate,todate:todate,office_id:office_id,leavetype:leavetype},
   				url: '<?php echo site_url(); ?>Ajax/leave_day/',
   				type: 'POST'
   			})
   			.done(function(data) {
   				var obj=JSON.parse(data);
   				$('#noofdays').val(obj.leave_day);
   				$('#hdnnoofdays').val(obj.leave_day);

   				$.ajax({
   					data:{leavetype:leavetype},
   					url: '<?php echo site_url(); ?>Ajax/leave_cal/',
   					type: 'POST'
   				})
   				.done(function(data) {

   					var obj1=JSON.parse(data);

   					var maxleavedays;
   					if (leavetype==9)
   					{
   						maxleavedays = availbalance;
   					}
   					else
   					{
   						maxleavedays =obj1.maxleave;
   						minleave=obj1.minleave;

   					}
   					if(parseFloat(minleave)>parseFloat($('#hdnnoofdays').val())   )
   					{
   						alert('In this leave category Minimum leave you can apply is ' + minleave +'.');
   						$('#noofdays').val('');
   						$('#hdnnoofdays').val('');
   						$("#todate").val('');
   						var rowCount = $('#tblnodays >tbody >tr').length;

   						if (rowCount>0)
   						{
   							$("#tblnodays tbody tr").each(function () {

   								$(this).remove();
   							});

   						}
   						return  false;
   					}
   					if(parseFloat(maxleavedays)<parseFloat($('#hdnnoofdays').val())  &&  parseFloat(maxleavedays) !=0 )
   					{
   						alert('In this leave category maximum leave you can apply is ' + maxleavedays +'.');

   						$('#noofdays').val('');
   						$('#hdnnoofdays').val('');
   						$("#todate").val('');
   						var rowCount = $('#tblnodays >tbody >tr').length;

   						if (rowCount>0)
   						{
   							$("#tblnodays tbody tr").each(function () {

   								$(this).remove();
   							});

   						}
   						return  false;
   					}
   					else
   					{

   						$.ajax({
   							data:{fromdate:fromdate,todate:todate,office_id:office_id,leavetype:leavetype},
   							url: '<?php echo site_url(); ?>Ajax/Checkholidaylist/',
   							type: 'POST'

   						})
   						.done(function(data) {
   							var obj2=JSON.parse(data);

   							if(obj2.check==0)
   							{
   								alert('Hoiday list not prepared for dates you are selected, kindly contact to your supervisor for same!');
   								$('#noofdays').val('');
   								$('#hdnnoofdays').val('');
   								("#fromdate").val('');
   								$("#todate").val('');
   								var rowCount = $('#tblnodays >tbody >tr').length;

   								if (rowCount>0)
   								{
   									$("#tblnodays tbody tr").each(function () {

   										$(this).remove();
   									});

   								}
   								return  false;
   							}

   						});

   						if(obj.leave_day>1)
   						{
   							$("#tblnodays tbody tr").each(function () {

   								$(this).remove();
   							});

   							var tablebody='<tbody><tr><td>';
   							tablebody+=fromdate;
   							tablebody+='</td><td>'; 
   							tablebody+='<select name="firstrow" class="form-control" id="ddlfirst" onchange="calculate_noofdays();"> <option name="firstoption" value="full">Full Day</option><option name="secoundoption" value="second" required>Second Half</option></select>';
   							tablebody+='</td></tr><tr><td>';
   							tablebody+=todate;
   							tablebody+='</td><td>';
   							tablebody+='<select name="secondrow" class="form-control" id="ddlsecond" onchange="calculate_noofdays();"> <option name="firstoption" value="full">Full Day</option><option name="secoundoption" value="first" required>First Half</option></select>';
   							tablebody+='</td></tr></tbody>';
   							$('#tblnodays').append(tablebody);

   						}
   						if(obj.leave_day==1)
   						{
   							$("#tblnodays tbody tr").each(function () {

   								$(this).remove();
   							});
   							var tablebody='<tbody><tr><td>';
   							tablebody+=fromdate;
   							tablebody+='</td><td>';
   							tablebody+='<select name="firstrow" class="form-control"> <option name="firstoption" value="full">Full Day</option><option name="secoundoption" value="first">First Half</option><option name="secoundoption" value="second" required>Second Half</option></select>';
   							tablebody+='</td>';
   							'</tr></tbody>';
   							$('#tblnodays').append(tablebody);

   						}
   						else
   						{
   							$('#tblnodays').clear();
   						}

   					}

   				})
   			})
}
})


})
});


function cleardetails()
{
	$('#noofdays').val('');
	$('#hdnnoofdays').val('');
	$("#todate").val('');
	$("#fromdate").val('');
	var rowCount = $('#tblnodays >tbody >tr').length;

	if (rowCount>0)
	{
		$("#tblnodays tbody tr").each(function () {

			$(this).remove();
		});

	}


}

function calculate_noofdays(){
	var tdays=0;
	$('#tblnodays').find('tr').each(function (rowIndex, r) {

		$(this).find('td .form-control').each(function (colIndex, c) {


			if ($(this).val() !='full')
			{
                 //$('#hdnnoofdays').val(
                 tdays += .5;
             }

         });

	});
	$('#noofdays').val(parseInt($('#hdnnoofdays').val())-tdays);
}

</script>

