<section class="content">
<?php foreach ($role_permission as $row) { if ($row->Controller == "user" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                  <h4 class="header" class="field-wrapper required-field" style="color:#f44336">Add User</h4>
                  <div class="col-sm-12">
                    <form method="POST" action="">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">User Name</label>
                          <input type="text" class="form-control" name="Username" id="Username" placeholder="Enter User Name" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">Password</label>
                          <input type="Password" class="form-control" name="Password" id="Password" placeholder="Enter Password" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">User First Name</label>
                          <input type="text" class="form-control" name="UserFirstName" id="UserFirstName" placeholder="Enter User First Name" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">User Middle Name</label>
                          <input type="text" class="form-control" name="UserMiddleName" id="UserMiddleName" placeholder="Enter User Middle Name" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">User Last Name</label>
                          <input type="text" class="form-control" name="UserLastName" id="UserLastName" placeholder="Enter User Last Name" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">Phone Number</label>
                          <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" placeholder="Enter Phone Number" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">Email ID</label>
                          <input type="email" class="form-control" name="EmailID" id="EmailID" placeholder="Enter Email ID" required>
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">Role</label>
                          <select name="RoleID" id="RoleID" class="form-control" required>
                            <option value="">Select</option>
                            <?php foreach($role_list as $row){ ?>
                            <option value="<?php echo $row->Acclevel_Cd; ?>" <?php echo ($row->Acclevel_Cd == set_value('RoleID') ? "selected" : "");?>><?php echo $row->Acclevel_Name; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      </div>
                      <div style="text-align: -webkit-center;">
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("user/index");?>" class="btn btn-success btn-sm m-t-10 waves-effect">Close</a>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Exportable Table -->
      </div>
        <?php } } ?>
    </section>