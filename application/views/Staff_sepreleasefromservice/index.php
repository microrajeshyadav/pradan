f<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
</style>
<form method="POST" action="" name="sep_releaseform" id="sep_releaseform">
 <section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - RELIEVING SEPARATING EMPLOYEE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5>Professional Assistance for Development Action (PRADAN) </h5>           
        </div>
        <div class="col-md-12 text-center">
          <h4> LETTER FOR RELIEVING SEPARATING EMPLOYEE</h4> 
        </div>          
      </div>
      
  <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
      
      <div class="col-md-12">
                 <div class="">
                  <div class="">
                    <label for="" class="field-wrapper required-field"></label>
                    <!-- <textarea class="input-block-level" id="summernote" name="content" rows="18"> -->
                      <textarea class='summernote' id='summernote' name="lettercontent"><?php echo $content; ?></textarea>

                    </div>
                    <?php echo form_error("Content");?>
                  </div>
                </div>
<hr/>
 
<?php 
$check = $this->session->userdata('login_data');
if($check->RoleID == 3){
?>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    cc: - Ms./Mr. <label class="inputborderbelow"> <?php echo $staff_detail->name;?></label> (separating employee through  <label class="inputborderbelow"><?php echo $staff_detail->supervisor;?></label> , Supervisor)
- Finance-Personnel-MIS Unit

  </div>
</div>
<?php }?>


</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($release_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="button" name="saveandsubmit" value="save & submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
  <?php
    }else{

      if($release_detail->flag == 0){
 ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="button" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
  <!-- <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
<?php } ?>
 
<?php } ?>
 <?php if($this->loginData->RoleID != 18){ ?>
 <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }else{ ?>
 <a href="<?php echo site_url("Ed_staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } ?>
  </div>
  </div>
  </div>
 </section>

<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->

      <!-- <form method="POST" name="acceptance_regination_modal" id="acceptance_regination_modal"> -->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Approved</h4>
          <button type="button" class="close" onclick="hidemodal();">&times;</button>
          
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments"  required> </textarea>
              <?php echo form_error("comments");?>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hidemodal();">Close</button>
          <input type="button" name="saveandsubmit" value="save & submit" class="btn btn-success btn-sm" onclick="acceptancereginationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>

<script type="text/javascript">
  function acceptanceregination(){
  
  var letter_no = document.getElementById('letter_no').value;
  var letter_date = document.getElementById('letter_date').value;
  var wef_date = document.getElementById('wef_date').value;
  var relieved_date = document.getElementById('relieved_date').value;
  
  if(relieved_date == ''){
    $('#relieved_date').focus();
  }
  if(wef_date == ''){
    $('#wef_date').focus();
  }
  if(letter_date == ''){
    $('#letter_date').focus();
  }
  if(letter_no == ''){
    $('#letter_no').focus();
  }
  
  if(relieved_date != '' && wef_date !='' && letter_date !='' && letter_no !=''){
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
}

function acceptancereginationmodal(){
    var comments = document.getElementById('comments').value;

    if(comments.trim() == ''){
      $('#comments').focus();
    }else{
      //document.getElementById("acceptance_regination_modal").submit();
      document.getElementById("sep_releaseform").submit();
    }
  }
</script>
<?php 
if ($release_detail){
if($release_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  });
</script>
<?php }} ?>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
$(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
   });

  });

  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
</script>