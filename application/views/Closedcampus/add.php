 <section class="content" style="background-color: #FFFFFF;" >
  <br>
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Closedcampus" && $row->Action == "add"){ ?>
  <div class="container-fluid">
   
    <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Schedule campus closed</b></div>
      <div class="panel-body">
       <?php 
       $tr_msg= $this->session->flashdata('tr_msg');
       $er_msg= $this->session->flashdata('er_msg');
       if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php } ?>
              <form name="formIntemation" id="formIntemation" action="" method="POST">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                         <br>
                        <label for="Name">Campus Name <span style="color: red;" >*</span></label>
                       <select name="campusid" id="campusid" class="form-control", required="required">
                        <option value="">Select Campus</option>
                        <?php foreach ($campusdetails as $key => $value) {
                            $expdatefrom = explode('-', $value->fromdate);
                            $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                            $expdateto = explode('-', $value->todate);
                            $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                             $campval  = $value->id; 
                            if ($campval == $campusid) {
                         ?>
                          <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                       <?php  } }?>
                       
                    
                        
                      </select> 
                        <?php echo form_error("campusid");?>
                      </div>
                      
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                         <br>
                       <label for="Name">Campus Status <span style="color: red;" >*</span></label>
                       <span id="campusincharge">
                        <select name="status" id="status" class="form-control" required="required"> 
                          <option value="">Select</option>
                           <option value="1">Closed</option>
                       
                        </select>
                         <?php echo form_error("campusinchargename");?>
                       </span>
                     </div>

                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <br>
                     <br>
                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                      
                      <button type="submit" name="intemationssave" id="intemationsave" value="save" class="btn btn-success btn-sm m-t-10 waves-effect">Save </button>
                    
                      <a href="<?php echo site_url("Closedcampus");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                    </div>  
                  </div>
                  
                </div>
              </div>
            </form>
          </div> 
        </div>
      </div>
    </div>   
  <?php } } ?>  
</section>
