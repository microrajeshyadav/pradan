<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
       
        <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Candidates Details</b></div>
        <div class="panel-body">
    <form name="CandidatesDetails" id="CandidatesDetails" action="" method="Post" >
        
    <div class="row" style="background-color: #FFFFFF;">
      <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b>Candidate Name:</b> </div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left">Amit Kumar</div>
      <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b>Campus Name:</b> </div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">Krishna institute of engineering and technology</div>
     
  </div>
   <br>
   <div class="row" style="background-color: #FFFFFF;">
     <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b>Date of Interview: </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left" >13 April 2018 </div>
      <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b>Offer Letter: </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left"> <a href="<?php echo site_url()."common/backend/certificate/Form2.pdf";?>" target="_blank" ><i class="fa fa-download fa-3x" aria-hidden="true"></i></a></div>
</div>
<br>
 <div class="row" style="background-color: #FFFFFF;">
     <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-left"><h4><b>Photo Uploads: </b></h4></div>
      
</div>

<br>
   <div class="row" style="background-color: #FFFFFF;">
     <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b>Photo <span style="color:red;">*</span>  : </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left" > 
        <input type="file" name="photo" id="photo" value="" required="required" ></div>
      <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b> </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left"> </div>
</div>

 <br>
   <div class="row" style="background-color: #FFFFFF;">
     <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-left"><h4><b>Document Uploads: </b></h4></div>
      
</div>
  <br>
   <div class="row" style="background-color: #FFFFFF;">
     <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b> 10th <span style="color:red;">*</span> :  </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left">
        <div class="form-group">
           <input type="file" name="highschool" id="highschool" value="" required="required" >
          </div>
      </div>
      <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b> 12th <span style="color:red;">*</span> :</b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left"> 
         <div class="form-group">
       <input type="file" name="interschool" id="interschool" value="" required="required" >
     </div>
      </div>
</div>

 <br>
   <div class="row" style="background-color: #FFFFFF;">
     <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b>Graduartion <span style="color:red;">*</span> : </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left"> <div class="form-group">
         <input type="file" name="graduartion" id="graduartion" value="" required="required">
     </div> </div>
      <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12"><b> Post Graduartion  : </b></div>
      <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-left"> <div class="form-group">
         <input type="file" name="postgraduartion" id="postgraduartion" value="">
     </div> </div>
</div>


 <br>
<div class="row" style="background-color: #FFFFFF;">
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10" style="background-color: white;">  </div> 
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 pull-right" style="background-color: white;">   
<button type="button" id ="btnAddRow" class="btn btn-success btn-sm text-left">Add </button> 
<button type="button" id="btnRemoveRow" class="btn btn-danger btn-sm text-right">Remove</button>
</div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">

       <table id="tableRecruiters" class="table table-bordered table-striped" id="tblForm09">
            <thead>
                 <tr>
                        <th >Work Experience</th>
                        <th ></th>
                     </tr> 
                   </thead>
              <tbody id="tbodyForm09">
                    <tr id="tblForm09">
                      <td><b> Work Experience certificate</b> </td>
                      <td>
                 <input type="file" name="Workexperiencecertificate" id="Workexperiencecertificate" value="" required="required"> </td>
              </tr>
                  </tbody>
                </table>
      </div>
    
    <div class="col-md-5 col-lg-5 col-xs-12 col-sm-12 text-left"></div>
     
</div>
<br>
<br>
<br>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-6"></div>
          <div class="col-lg-6 text-right">
               <button type="submit" class="btn btn-success">Save </button>
              <button type="reset" class="btn btn-warning">Reset </button>
          </div>  
    </div>
   </div>
</form>
    </div> 
    </div>
    </div>
    </div>   



</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tableRecruiters').DataTable({
      "searching": false,
       "info":    false,
       "paging":  false
    }); 
  });
</script>  

  
<script type="text/javascript">
$("#btnRemoveRow").click(function() {
  // alert($('#tblForm09 tr').length-1);
    if($('#tblForm09 tr').length >1)
      alert($('#tblForm09 tr').length-1);

      $('#tbodyForm09 tr:last').remove()
  });


  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);
    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
      
  });

  var srNoGlobal=0;
  var inc = 0;

  function insertRows(count){
   srNoGlobal = $('#tbodyForm09 tr').length+1;
    inc = $('#tbodyForm09 tr').length-1;
    
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;
  for(i = 1; i <= count; i++){
      inc++
      cloneRow = lastRow.clone();
    tbody.append(cloneRow);
  }

}
insertRows()
  
</script>