 <section class="content" style="background-color: #FFFFFF;" >
    <br>
     <?php foreach ($role_permission as $row) { if ($row->Controller == "Campusinchargeintimation" && $row->Action == "send"){ ?>
    <div class="container-fluid">
        <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Campus Incharge Intemation</b></div>
        <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      
          <form name="formIntemation" id="formIntemation" action="" method="POST">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
              <label for="Name">Campus Name <span style="color: red;" >*</span></label>
              <select name="campusid" id="campusid" class="form-control">
                <option >Select</option>
                <?php  foreach ($campusdetails as $key => $value) {
                  if($value->campusid == $getcampusinchargeintemationdetail[0]->campusid){
                ?>
                <option value="<?php echo $value->campusid;?>" SELECTED > <?php echo $value->campusname;?></option>
                <?php } else{ ?>
                 <option value="<?php echo $value->campusid;?>" ><?php echo $value->campusname;?></option>
                  <?php } } ?>
              </select>
                   
                  <?php echo form_error("campusid");?>
                  </div>
           
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <label for="Name">Campus Incharge Name <span style="color: red;" >*</span></label>
               <span id="campusincharge">
                   <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Campus Incharge Name" name="campusinchargename" id="campusinchargename" class="form-control" value="<?php echo set_value("campusinchargename");?>" placeholder="Enter First Name"  required="required">
                  <?php echo form_error("campusinchargename");?>
                </span>
            </div>


              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                  <br>
                <label for="Name">Schedule From Date <span style="color: red;" >*</span></label>
               <input type="text" data-toggle="tooltip" title="Schedule From Date" name="schedule_fromdate" id="schedule_fromdate" class="form-control datepicker" value="<?php echo set_value("fromdate");?>" placeholder="Schedule From Date" onclick="adddatepicker()"  required="required">
                 <?php echo form_error("schedule_fromdate");?>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
               <br>
                <label for="Name">Schedule To Date <span style="color: red;" >*</span></label>
                <input type="text" data-toggle="tooltip" title="Schedule To Date" name="schedule_todate" id="schedule_todate" class="form-control datepicker" onclick="adddatepicker()" value="<?php echo set_value("todate");?>" placeholder="Schedule To Date"  required="required">
                 <?php echo form_error("schedule_todate");?>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <br>
               <label for="Name">Meaasge <span style="color: red;" >*</span></label>

               <textarea  name="message" id="name" class="form-control" cols="10" rows="10"><?php echo $getcampusinchargeintemationdetail[0]->messsage;?></textarea>
            </div>


             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <br>
            <br>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
            
                <button type="submit" name="intemationssave" id="intemationsave" value="save" class="btn btn-success btn-sm m-t-10 waves-effect">Save </button>
                <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-warning btn-sm m-t-10 waves-effect">Save & Send </button>
                <a href="<?php echo site_url("Campusinchargeintemation");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
              </div>  
            </div>
            
          </div>
        </div>
      </form>
    </div> 
    </div>
    </div>
    </div>  
    <?php } } ?> 
</section>

 <script type="text/javascript">
   $(document).ready(function(){

     adddatepicker();

     $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
    });


    $("#campusid").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getCampusInchargeName/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })

      .done(function(data) {
        console.log(data);

        $("#campusincharge").html(data);
        $("#campusintimationid").html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

  


$("#schedule_todate").change(function(){
      var campusid = $('#campusid').val();
      var schedule_fromdate = $('#schedule_fromdate').val();
      var schedule_todate = $('#schedule_todate').val();
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getCampusIntimationToDateDetail/'+ campusid +'_'+ schedule_fromdate+'_'+ schedule_todate,
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
        console.log(data);
        $("#message").html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });
  });     



</script>

<script type="text/javascript">
  function adddatepicker(){
   $("#schedule_fromdate").datepicker({
     changeMonth: true,
     changeYear: true,
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
     onClose: function(selectedDate) {
      jQuery("#schedule_todate").datepicker( "option", "minDate", selectedDate);
      }
    });

   $("#schedule_todate").datepicker({
     changeMonth: true,
     changeYear: true,
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
     onClose: function(selectedDate) {
      jQuery("#schedule_fromdate").datepicker( "option", "maxDate", selectedDate);
      }
    });

 }
</script>


