 
 <section class="content" style="background-color: #FFFFFF;" >
<?php foreach ($role_permission as $row) { if ($row->Controller == "Campusinchargeintimation" && $row->Action == "index"){ ?>
    <br>
    <div class="container-fluid">
        <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Schedule campus closed</b></div>
        <div class="panel-body">
   <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        <?php  //print_r($campusinchargedetails); ?>
          <div class="row" style="background-color: #FFFFFF;">
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">
           <!--  <a href="<?php //echo site_url()."Campusinchargeintimation/add";?>" class="btn btn-sm btn-success">Add Campus Incharge Intimation</a> -->
                <br>
                <br>
           </div>
       </div>
      
        <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Campus Name</th>
               <th>Email Id</th>
               <th>Schedule From Date</th>
               <th>Schedule To Date</th>
               <th>Campus Incharge Name </th>
               <th>Status </th>
              <th>Action</th>
           </tr> 
         </thead>
         <tbody>
          <?php $i=0; foreach ($campusinchargedetails as $key => $value) { ?>
          <tr>
           <td><?php echo $i+1;?></td>
           <td><?php echo $value->campusname;?></td>
           <td><?php echo $value->emailid;?></td>
           <td><?php echo $this->model->changedatedbformate($value->fromdate);?></td>
           <td><?php echo $this->model->changedatedbformate($value->todate);?></td>
           <td><?php echo $value->campusincharge;?></td>

           <td>
           
            <?php if ($value->campus_status ==0) {
           echo 'Open';
           }else if($value->campus_status ==1){
            echo 'Close';
           }?></td>
           <?php if ($value->campus_status ==0) { ?>
           <td> <a href="<?php echo site_url().'Closedcampus/add/'.$value->id;?>">   
          <span class="glyphicon glyphicon-pencil"></span></a>
        </td>
      <?php } ?>
          </tr>
  
           
  <?php $i++; } ?>
        </tbody>
      </table>
       </div>
    </div> 
    </div>
    </div>
    </div>  
    <?php } } ?> 
</section>

<script>
  $(document).ready(function(){

    $("#b1").hover(function () {
    $('#modal1').modal({
        show: true,
        backdrop: false
    })
});


    // $('[data-toggle="tooltip"]').tooltip();  
     $('[data-toggle="popover"]').popover({
       html: true, 
  content: function() {
          return $('#popover-content').html();
        }
});;   
    $('#tablecampus').DataTable(); 
  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
function yesnoCheck() {
   
    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
    }
  }
</script>
<script>
function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>