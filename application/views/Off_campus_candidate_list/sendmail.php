
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">

    <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Off Campus Selected Candidates List</b></div>
      <div class="panel-body">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } else if(!empty($er_msg)){?>
                <div class="content animate-panel">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="hpanel">
                        <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                <?php //echo "<pre>"; print_r($selectedcandidatedetails); ?>
                <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >
                  <div class="row" style="background-color: #FFFFFF;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                      
                     <table id="Inboxtable" class="table table-bordered table-striped wrapper">
                      <thead>
                       <tr>
                        <th>S.No.</th>
                        <th>Candidate Name</th>
                        <th>Email Id</th>
                         <th>Check</th>

                      </tr> 
                    </thead>
                    <tbody>
            <?php $i=0; foreach ($selectedcandidatedetails as $key => $value) {
                      ?>
                      <tr>
                      <td><?php echo $i+1; ?></td>
                      <td><?php echo $value->candidatefirstname;?>
                         <input type="hidden" name="candidatefirstname[]" value="<?php echo $value->candidatefirstname;?>">
                      </td>
                      <td><?php echo $value->emailid;?>
                      <input type="hidden" name="emailid[]" value="<?php echo $value->emailid;?>"> </td>
                      <td>
                        <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="candidateid[]" value="<?php echo $value->candidateid; ?>" id="defaultCheck_candidateid_<?php echo $i; ?>">
                      <label class="form-check-label" for="defaultCheck_candidateid_<?php echo $i; ?>">
                       
                      </label>
                    </div>
                      </td>
                   </tr>
                  <?php $i++; } ?>
                    </tbody>
                  </table>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;"><button type="submit" class="btn btn-success" name="btnaccept" id="btnaccept" value="AcceptData" >Send Mail</button> 
                <button type="button" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</button></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              </div> 
               </form>
            </div>
          </div>
        </div>   
      </section>

      <script>
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();  
          $('#Inboxtable').DataTable(); 
        });

	
      </script>  
 <script type="text/javascript">
    $(document).ready(function () {
      $('#btnaccept').click(function() {
        checked = $("input[type=checkbox]:checked").length;

        if(!checked) {
          alert("You must check at least one checkbox.");
          return false;
        }

      });
    });
</script>