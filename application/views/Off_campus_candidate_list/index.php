<section class="content" style="background-color: #FFFFFF;" >
  <br>

 <div class="container-fluid">
    <div class="panel panel-default" >
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Selected Candidates List For Off Campus</h4>
      
    </div>
     <hr class="colorgraph"><br>
    </div>

  

      <div class="panel-body">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } else if(!empty($er_msg)){?>
                <div class="content animate-panel">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="hpanel">
                        <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                <?php //echo "<pre>"; print_r($selectedcandidatedetails); ?>
                <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >
                  <div class="row" style="background-color: #FFFFFF;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                      
                     <table id="Inboxtable" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
                      <thead>
                       <tr>
                        <th class="text-center">Accept </th>
                        <th class="text-center">Reject </th>
                        <th style ="max-width:30px;" class="text-center">S. No.</th>
                        <th>Category</th>
                        <th>Campus Name</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email Id</th>
                        <th>Mobile </th>
                        <th>Stream</th>
                        <th>Gap Year</th>
                        <th>10th % </th>
                        <th>12th %</th>
                        <th>UG %</th>
                        <th>PG %</th>
                        <th>State </th>
                        <th>City</th>
                        
                        
                      </tr> 
                    </thead>
                    <tbody>
            <?php $i=0; foreach ($selectedcandidatedetails as $key => $value) {
                      ?>
                      <tr>
                         <td class="text-center">
                        <div class="form-check">
                        <input class="form-check-input fillaccept text"  type="checkbox" name="accept[]" data-toggle="tooltip" title=" Accept" value="<?php echo $value->candidateid; ?>" id="defaultCheck_accept_<?php echo $i; ?>" >
                        <label class="form-check-label" for="defaultCheck_accept_<?php echo $i; ?>">
                         
                        </label>
                      </div>
                      </td>
                      <td class="text-center">
                        <div class="form-check">
                    <input class="form-check-input fillreject" type="checkbox" name="reject[]" data-toggle="tooltip" title=" Reject" value="<?php echo $value->candidateid; ?>" id="defaultCheck_reject_<?php echo $i; ?>">
                    <label class="form-check-label" for="defaultCheck_reject_<?php echo $i; ?>">
                    </label>
                  </div>
                </td>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td><?php echo $value->categoryname;?></td>
                      <td><?php echo $value->campusname;?></td>
                      <td><a href="<?php echo site_url().'Off_campus_candidate_list/view/'.$value->candidateid;?>" target="_blank" class="btn btn-primary"><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname;?></a></td>
                      <td><?php echo $value->gender;?></td>
                      <td><?php echo $value->emailid;?> </td>
                      <td><?php echo $value->mobile;?></td>
                      <td><?php echo $value->stream;?></td>
                      <td><?php echo $value->DateDiff;?></td>
                      <td><?php echo $value->metricpercentage;?></td>
                      <td><?php echo $value->hscpercentage;?></td>
                      <td><?php echo $value->ugpercentage;?></td>
                      <td><?php echo $value->pgpercentage;?></td>
                      <td><?php echo $value->statename;?></td>
                      <td><?php echo $value->permanentcity;?></td>
                      
                     
                </tr>
                  <?php $i++; } ?>
                    </tbody>
                  </table>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;"><button type="submit" class="btn btn-success" name="btnaccept" id="btnaccept" value="Submit" disabled="disabled" >Submit</button> 
                <!-- <button type="submit" class="btn btn-warning" disabled="disabled" name="btnreject" id="btnreject" value="RejectData">Reject</button> -->
               <!--  <button type="submit" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</button></div> -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              </div> 
               </form>
            </div>
          </div>
        </div>   
      </section>

      <script>
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();  
          $('#Inboxtable').DataTable(); 
        });
      </script>  

 <script type="text/javascript">
    $(document).ready(function () {

    
     $(".fillaccept").click(function(){
      
      if ($(".fillaccept").is(':checked')) {
         $("#btnaccept").prop("disabled", false);
      }
    });

    $(".fillreject").click(function(){
       if ($(".fillreject").is(':checked')) {
         $("#btnaccept").prop("disabled", false);
      }
    });

   

   });

      

$(".form-check-input").click(function() {
    
            
            if ($(this).prop('checked')==true){ 
              var check1 =  $(this).attr('id');
              
               if(check1.search("_accept_") != -1) {
                    
                   if ($('#'+check1.replace("_accept_", "_reject_")).prop('checked')==true){  $('#'+check1.replace("_accept_", "_reject_")).prop('checked',false);
                   }
               }
               else
               {
                if ($('#'+check1.replace("_reject_", "_accept_")).prop('checked')==true){  $('#'+check1.replace("_reject_", "_accept_")).prop('checked',false);
                   }
               
                  
               }
              
            }
      
});
</script>