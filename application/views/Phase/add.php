<section class="content">
  <br>
  <?php 

  foreach ($role_permission as $row) { if ($row->Controller == "Phase" && $row->Action == "add"){ ?>
    <div class="container-fluid">
      <!-- Exportable Table -->

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

          <form method="POST" action="">
            <div class="container-fluid" style="margin-top: 20px;">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row clearfix doctoradvice">

                  <div class="panel thumbnail shadow-depth-2 listcontainer col-md-12" >
                   <div class="panel-heading">
                    <div class="row">
                      <h4 class="col-md-8 panel-title pull-left">Add Phase-Activity</h4>
                     <div class="col-md-4 text-right" style="color: red">
                      * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <div class="form-line">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Phase-Activity</label>
                      <input type="text" class="form-control" name="phasename" id="phasename" placeholder="Please Enter Phase-Activity" value="<?php echo set_value('phasename');   ?>" >
                    </div>
                    <?php echo form_error("phasename");?>
                  </div>
                  <div class="form-group">
                    <div class="form-line">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Status </label>
                      <?php   

                        //$options = array('' => 'Select Status');     
                      $options = array('0' => 'Active', '1' => 'InActive');
                      echo form_dropdown('status', $options, set_value('phaseid'), 'class="form-control"'); 
                      ?>
                    </div>
                    <?php echo form_error("status");?>
                  </div>

                </div>

                <div class="panel-footer text-right"> 
                  <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                  <a href="<?php echo site_url("Phase");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                </div>


              </div>

            </div>

          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>