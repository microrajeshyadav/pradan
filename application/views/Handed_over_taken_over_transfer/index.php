<?php   //print_r($staff_details);?>
<form name="handed_taken" id="handed_taken" action="" method="POST">
<section class="content" style="background-color: #FFFFFF;" >
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer">
    <div class="panel-heading">
      <div class="row">
        <h4 class="col-md-8 panel-title pull-left"> </h4>
        <div class="col-md-4 text-right" style="color: red">
          * Denotes Required Field 
        </div>
      </div>
    <hr class="colorgraph"><br>
  </div>
  <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?> 
  <div class="panel-body">
    <div class="row"> 
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
        <p style="text-align: center;"><strong><span style="font-size: 14.0pt; font-variant: small-caps;">Handing Over/Taking Over Charge</span></strong></p>
        <p style="text-align: center;">&nbsp;</p>
        <p style="text-align: justify; tab-stops: -49.5pt;">As per the Letter of Transfer No.<input type="text" name="transferno" id="transferno"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->transferno;?>" required="required" readonly> dated <input type="text" name="tdate" id="tdate"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo date('d/m/Y'); ?>"  required="required" readonly>, transferring me to <input type="text" name="transferto" id="transferto"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->officename;?>" required="required" readonly> (<span style="font-size: 10.0pt;">place</span>), I have handed over the charge of my responsibilities to <input type="text" name="change_responsibility_name" id="change_responsibility_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  required="required" value="<?php echo $staff_details->currentresponsibilityto;?>" readonly> (<span style="font-size: 10.0pt;">name</span>)<input type="text" name="change_responsibility_date" id="change_responsibility_date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  class="datepicker " value="" required="required"> (<span style="font-size: 10.0pt;">date</span>). The list of items handed over/taken over 
            <select style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" name="handed" id="handed" required="required">
              <option value="">Select</option>
             <option value="1">Handed Over</option>
              <option value="2">Taken Over</option>
            </select>
        is as under.</p>
        <p style="text-align: justify; tab-stops: -49.5pt;">&nbsp;</p>
    <?php if(!empty($transfer_expeness_details)){ ?>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
         <table id="tblForm101" class="table table-bordered table-striped">
          <thead>
           <tr class="bg-light">
            <th colspan="2"> Items

              
          </th>
          </tr> 
          <tr>
           <th class="text-center" style="vertical-align: top;">Items </th>
           <th class="text-center" style="vertical-align: top;">Description</th>
         </tr> 
       </thead>
       <tbody id="bodytblForm101">
       
           
        <?php 
          foreach($transfer_expeness_details as $val){
        ?>
          <!-- <input type="hidden" name="handed_id[]" value="<?php echo $val->id;?>">
          <tr id="bodytblForm10"> -->
          <td><label  style="min-width: 20%;" > 
          <?php echo $val->item;?>  </label></td>
          <td> <label  style="min-width: 20%;"  ><?php echo $val->description;?></td>
          </tr>


        <?php
          }
        ?>

          
          </tbody>
        </table>
      </div>
    <?php } ?>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
         <table id="tblForm10" class="table table-bordered table-striped">
          <thead>
           <tr class="bg-light">
            <th colspan="2"> Items

              <div class="col-lg-6 text-right pull-right">
                <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
              </div>
          </th>
          </tr> 
          <tr>
           <th class="text-center" style="vertical-align: top;">Items </th>
           <th class="text-center" style="vertical-align: top;">Description</th>
         </tr> 
       </thead>
       <tbody id="bodytblForm10">

       <?php if($expense->Cexp>0)
       {
       ?>
        <tr id="bodytblForm10">
          <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="items !" minlength="5" maxlength="50"
            id="items"  maxlength="150" name="items[]" placeholder="Enter Items" style="min-width: 20%;"  
            value="<?php echo set_value('items');?>"  required></td>
            <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="items description!" 
              id="items_description" name="items_description[]"  placeholder="Enter Description" style="min-width: 20%;" value="" required></td>
            </tr>
            <?php } else{ ?>

            <tr id="bodytblForm10">
          <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="items !" minlength="5" maxlength="50"
            id="items"  maxlength="150" name="items[]" placeholder="Enter Items" style="min-width: 20%;"  
            value=""  required></td>
            <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="items description!" 
              id="items_description" name="items_description[]"  placeholder="Enter Description" style="min-width: 20%;" value="" required></td>
            </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
     <p>&nbsp;</p>
     <div id="hidehandedover" style="display: none;">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><strong>Handed Over</strong></div>
       <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Name:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_name" id="handed_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->staff_name;?>" required="required" ></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Employee Code:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_employee_code" id="handed_employee_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->emp_code;?>" required="required"></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Designation:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_designation" id="handed_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->staffdesignation;?>" required="required"></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Location:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_designation" id="handed_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->oldofice;?>" required="required"></div>
     </div>
   </div>

<br>
 <div id="hidetakenover" style="display: none;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><strong>Taken Over</strong></div>
       <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Name:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text"  name="taken_name" id="taken_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $staff_details->currentresponsibilityto;?>"></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Employee Code:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text"  name="taken_employee_code" id="taken_employee_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $staff_details->empcode;?>"></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Designation:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="taken_designation" id="taken_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->currentresponsibilitytodesignation;?>" ></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Location:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input name="taken_location" id="taken_location"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $staff_details->oldofice;?>"></div>

     </div>
   </div>
      
      <p><strong>&nbsp;</strong></p>
      <p style="text-align: center;"><strong>Countersigned</strong></p>
      <p style="text-align: center;">&nbsp;</p>
      <p style="text-align: center;"><input type="text" name="countersigned" id="countersigned"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="" required="required"></p>
      <p style="text-align: center;">(Signature of Location In-charge with Date)</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><strong>Note</strong>:In case a Team Coordinator is transferred<strong> t</strong>he new person who has been nominated to take charge will confirm to Finance-Personnel-MIS Unit that procedure for transfer of Team Coordinator as mentioned in appendix 29.1&nbsp; have been completed.</p>
      <p>&nbsp;</p>
      <p><strong>&nbsp;</strong></p>
      <p>cc: - Personal Dossier</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Finance-Personnel-MIS Unit</p>
    </div>
  </div>
</div>

<div class="panel-footer text-right">
 <!--  <button  type="submit" name="savetbtn" id="savetbtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button> -->
                 
  <button  type="button" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit" onclick="handedovertaken();">Submit</button>
</div>
</div>
</div>   
</section>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4>Approval</h4>
          <button type="button" class="close" onclick="hidemodal();">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" name="change_flag" id="change_flag" value="">

             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes" > </textarea>
              <?php echo form_error("comments");?>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hidemodal();">Close</button>
          <input type="button" name="btnsubmit" id="btnsubmit" value="Save" class="btn btn-success btn-sm" onclick="handedovertakenmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->

</form>

<script>
  function handedovertaken(){
    var change_responsibility_date = document.getElementById('change_responsibility_date').value; 
    var handed = document.getElementById('handed').value;  
    var items = document.getElementById('items').value;   
    var items_description = document.getElementById('items_description').value;   
    var countersigned = document.getElementById('countersigned').value; 
    // alert(name_of_location);

    if(countersigned == ''){
      $('#countersigned').focus();
    }

    if(items_description == ''){
      $('#items_description').focus();
    }

    if(items == ''){
      $('#items').focus();
    }
    
    if(handed == ''){
      $('#handed').focus();
    }

    if(change_responsibility_date == ''){
      $('#change_responsibility_date').focus();
    }
    
    if(change_responsibility_date != '' && handed !='' && items !='' && items_description !='' && countersigned !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
  function handedovertakenmodal(){
    var dd=1;
    $("#change_flag").val(dd);
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }
    if(comments.trim() !=''){
      document.forms['handed_taken'].submit();
    }
  }
   $('#handed').change(function(){
       var handedid = $('#handed').val();
       //alert(handedid);

       if (handedid ==1) {
          $('#hidehandedover').show();
          $('#hidetakenover').hide();
           $('#taken_name').attr('disabled', true);
          $('#taken_employee_code').attr('disabled', true);
          $('#taken_designation').attr('disabled', true);
          $('#taken_location').attr('disabled', true);
       }else{
         $('#hidetakenover').show();
         $('#hidehandedover').hide();
        $('#handed_name').attr('disabled', true);
        $('#handed_employee_code').attr('disabled', true);
        $('#handed_designation').attr('disabled', true);
        $('#handed_designation').attr('disabled', true);
       }

    });

 
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
   
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });

  var srNoGlobal=0;
  var inctg = 0;

  function Insaettrainingexposure(count) {
    srNoGlobal = $('#bodytblForm10 tr').length+1;
    var tbody = $('#bodytblForm10');
    var lastRow = $('#bodytblForm10 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inctg++
      cloneRow = lastRow.clone();
      var tableData1 = '<tr>'
      + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="items" name="items['+inctg+']" placeholder="Enter Items" style="min-width: 20%;" value="" data-original-title="Items!">'
      + '</td>'
      + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="items_description['+inctg+']" name="items_description['+inctg+']" placeholder="Items Description" style="min-width: 20%;" value="" required="required" data-original-title="Items Description !"></td>'
      + '</td>' + '</tr>';
      $("#bodytblForm10").append(tableData1)
    
    }

  }
  //insertRows();

  $(document).ready(function () {
 $("#change_responsibility_date").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
  });

  });
  

</script>
