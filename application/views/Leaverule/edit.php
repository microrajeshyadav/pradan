<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaverule" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
          <form method="POST" action="">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-7 panel-title pull-left">Edit Leave Rule</h4>
                  <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <?php //print_r($Leave_details); echo $Leave_details->CYear; ?>
              <div class="panel-body">  
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Year:<span style="color: red;" >*</span></label>
                    <select id="Cyear" name="Cyear" class="form-control" required>
                     <?php
                     $dates = range(date('Y'), '2016' );
                     foreach($dates as $date){
                        if (date('m', strtotime($date)) <= 4) {//Upto June
                          $year = ($date-1) . '-' . $date;
                        } else {//After June
                          $year = $date . '-' . ($date + 1);
                        }
                        if ($year == $Leave_details->CYear)
                        {
                          echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                        }
                        else
                        {
                         echo '<option value="'.$year.'">'.$year.'</option>';
                       }                         
                     }
                     ?>                    
                   </select>

                 </div>
                 <?php echo form_error("leave");?>
               </div>
               <div class="form-group">
                <div class="form-line">
                  <label for="StateCode" class="field-wrapper required-field">Leave limit for the financial year<span style="color: red;" >*</span></label>
                  <input type="text" class="form-control" id="name" name="leavemax" minlength="1" maxlength="3" value="<?php echo $Leave_details->Maxcf_yearleave;?>" onkeypress="return isNumber(event)" required="">
                </div>
                <?php echo form_error("leavemax");?>
              </div>

              <div class="form-group">
                <div class="form-line">
                  <label for="StateCode" class="field-wrapper required-field">Total individual leave limit (all years)<span style="color: red;" >*</span></label>
                  <input type="text" class="form-control" id="name" name="leaveaccured" minlength="1" required="" maxlength="3" value="<?php echo $Leave_details->maxleaveaccrued;?>">
                </div>
                <?php echo form_error("leaveaccured");?>
              </div>

            </div>
            <div class="panel-footer text-right"> 
              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
              <a data-toggle="tooltip" title="Click here to move on list." href="<?php echo site_url("Leaverule/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect">Go to List</a>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>


<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  $( document ).ready(function() {
    $("#Cyear").prop('disabled', 'disabled');
  });

</script>