<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaverule" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

       

       
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-10 panel-title pull-left">Leave Rule List</h4>
                 <div class="col-md-2 text-right">
                   <a data-toggle="tooltip" data-placement="bottom" title="Want to add leave rule for new year? Click on me." href="<?php echo site_url("Leaverule/add/")?>" class="btn btn-primary btn-sm">Add Leave Rule</a>
                 </div>
               </div>
               <hr class="colorgraph"><br>
             </div>
             <div class="panel-body">
               <table id="tblsyslanguage" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                  <tr>
                   <th class="text-center" style="width: 50px;">S.No.</th>
                    <th class="text-center">Financial Year</th>
                   <th class="text-center">Leave limit for less than 10 year experience</th>
                   <th class="text-center">Leave limit for more than 10 year experience</th>
                   <th class="text-center" style="width: 50px;">Action</th>
                 </tr>
               </thead>
               <tbody>
                <?php

                   // print_r($staffcategory_details);
                $i=0; foreach($Leave_details as $row){ ?>
                <tr>
                  <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                  <td class="text-center"><?php echo $row->CYear; ?></td>
                  <td class="text-center"><?php echo $row->Maxcf_yearleave; ?></td>
                  <td class="text-center"><?php echo $row->maxleaveaccrued; ?></td>
                  <td class="text-center">
                    <a  data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Batch." href="<?php echo site_url('Leaverule/edit/'.$row->CYear);?>" style="padding : 4px;" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>

                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#tblsyslanguage').DataTable({
      "paging": false,
      "search": false,
    });
  });
  function confirm_delete() {

    var r = confirm("Do you want to delete this Under Graduation");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>