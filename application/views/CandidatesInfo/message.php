
  <div class="container-fluid">
    <!-- Exportable Table -->
    <h4 style="margin-left:10px;">Message </h4> 
     <hr class="colorgraph">
        <div class="row clearfix">

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">


                <div class="panel panel-default" >


                
            <div class="panel-body">
            <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel" style="padding:20px;">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    You have been successfully registered.</div>
                  </div>
                </div>
              </div>
            </div>
         <div class="panel-heading" style="vertical-align: top;text-align: right;margin-right: 10px;">
               
                
                    <a href="http://www.pradan.net/" class="btn btn-danger pull-right" style="vertical-align: top;" data-toggle="tooltip" title="Close">Close</a> 

                 </div>
                </div>

              </div><!-- /.panel-->
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  
<script type="text/javascript">
  
$(document).ready(function(){
  decimalData();
    $('[data-toggle="tooltip"]').tooltip();   
  
});

$(document).ready(function(){
    $("#filladdress").on("click", function(){

         // if (filladdress =='') {
         //      alert('Please fill Present Address !!!');

         //          return false;
         //  }else{                                      
     
         if (this.checked) { 
                  $("#permanentstreet").val($("#presentstreet").val());
                $("#permanentcity").val($("#presentcity").val());
                $("#permanentstateid").val($("#presentstateid").val()); 
                $("#permanentdistrict").val($("#presentdistrict").val());
                $("#permanentpincode").val($("#presentpincode").val()); 
                $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());
                                 
    }
    else {
        $("#permanentstreet").val('');
        $("#permanentcity").val('');
        $("#permanentstateid").val(''); 

        $("#permanentdistrict").val('');
        $("#permanentpincode").val('');
        $("#permanentstateid").val('');          
    }

  //}

    });

});


function decimalData(){
$('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});

$('.txtNumeric').bind("paste", function(e) {
var text = e.originalEvent.clipboardData.getData('Text');
if ($.isNumeric(text)) {
   if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
       e.preventDefault();
       $(this).val(text.substring(0, text.indexOf('.') + 3));
  }
}
else {
       e.preventDefault();
    }
});
}
</script>
