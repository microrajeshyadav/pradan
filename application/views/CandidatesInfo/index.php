<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
       <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="panel panel-default" >
                  <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>List Of Candidate Info</b></div>
                 <div class="panel-body">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right"> <a href="<?php echo site_url("CandidatesInfo/add/")?>" class="btn btn-success">Add Candidates</a></div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white; overflow-x: scroll;">
          <table id="tablecandidate" class="table table-bordered table-striped warp" >
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Name</th>
              <th>Mother Name</th>
              <th>Fateher Name </th>
              <th>Email Id</th>
              <th>Mobile No.</th>
              <th>Date Of Birth</th>
              <th>10th School/College</th>
              <th>10th Board /University</th>
              <th>10th Passing Year </th>
              <th>10th Place</th>
              <th>10th Specialisation</th>
              <th>10th Percentage</th>
              <th>12th School/College</th>
              <th>12th Board /University</th>
              <th>12th Passing Year </th>
              <th>12th Place</th>
              <th>12th Specialisation</th>
              <th>12th Percentage</th>
              <th>UG School/College</th>
              <th>UG Board /University</th>
              <th>UG Passing Year </th>
              <th>UG Place</th>
              <th>UG Specialisation</th>
              <th>UG Percentage</th>
              <th>Action</th>
           </tr> 
         </thead>
         <tbody>
        <?php $i=0;  foreach ($selectedcandidatedetails as $key => $value) { ?>
          <tr>
            <td><?php echo $i+1; ?></td>
            <td><?php  echo $value->candidatefirstname;?> <?php  echo $value->candidatemiddlename;?><?php  echo $value->candidatelastname;?> </td>
            <td><?php  echo $value->motherfirstname;?> <?php  echo $value->mothermiddlename;?><?php  echo $value->motherlastname;?> </td>
            <td><?php  echo $value->fatherfirstname;?> <?php  echo $value->fathermiddlename;?><?php  echo $value->fatherlastname;?></td>
            <td><?php  echo $value->emailid;?> </td>
            <td><?php  echo $value->mobile;?></td>
            <td><?php  echo $value->dateofbirth;?></td>
            <td><?php  echo $value->metricschoolcollege;?></td>
            <td><?php  echo $value->metricboarduniversity;?></td>
            <td><?php  echo $value->metricpassingyear;?></td>
            <td><?php  echo $value->metricplace;?></td>
            <td><?php  echo $value->metricspecialisation;?></td>
            <td><?php  echo $value->metricpercentage;?></td>
            <td><?php  echo $value->hscschoolcollege;?></td>
            <td><?php  echo $value->hscboarduniversity;?></td>
            <td><?php  echo $value->hscpassingyear;?></td>
            <td><?php  echo $value->hscplace;?></td>
            <td><?php  echo $value->hscspecialisation;?></td>
            <td><?php  echo $value->hscpercentage;?></td>
            <td><?php  echo $value->ugschoolcollege;?></td>
            <td><?php  echo $value->ugboarduniversity;?></td>
            <td><?php  echo $value->ugpassingyear;?></td>
            <td><?php  echo $value->ugplace;?></td>
            <td><?php  echo $value->ugspecialisation;?></td>
            <td><?php  echo $value->ugpercentage;?></td>
            <td><a href="<?php echo site_url()."CandidatesInfo/view/".$value->candidateid;?>" >View</a> </td>
          </tr>
          <?php $i++; } ?>
         </tbody>
      </table>
       </div>         
        


         
                </div>

              </div><!-- /.panel-->
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>

  
  <script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecandidate').DataTable(); 
  });
</script>