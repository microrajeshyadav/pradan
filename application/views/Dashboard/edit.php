<section class="content">
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "edit"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
        <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Campus Details</b></div>

        <div class="panel-body">
           <form name="campus" action="" method="post" > 
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">State <span style="color: red;" >*</span> </label>
                <select name="stateid" id="stateid" class="form-control" data-toggle="" >
                <option value="">Select State</option>
                    <?php  foreach ($statedetails as $key => $value) { 
                       if ($value->id ==$campusupdate[0]->stateid) {
                       ?>
                      <option value="<?php echo $value->id;?>" SELECTED><?php echo $value->name;?></option>
                    <?php  }else { ?>
                     <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                    <?php } } ?>
                    </select>
              <?php echo form_error("stateid");?>
                </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">City <span style="color: red;" >*</span></label>
                <input type="text"  minlength="4" maxlength="50"  data-toggle="" name="city" id="telephone" class="form-control" value="<?php echo $campusupdate[0]->city;?>" placeholder="Enter Telephone No " >
                <?php echo form_error("city");?>
                </div>
                
            
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Campus Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" data-toggle="" id="campusname" name="campusname" placeholder="Enter Campus Name " value="<?php echo $campusupdate[0]->campusname;?>" >
                 <?php echo form_error("campusname");?>
                </div>
             
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Campus  Incharge <span style="color: red;" >*</span></label>
                 <input type="text" name="campusincharge" data-toggle="" minlength="10" maxlength="50" class="form-control" value="<?php echo $campusupdate[0]->campusincharge;?>" placeholder="Enter Campus  Incharge" >
                  <?php echo form_error("campusincharge");?>
                </div>
                                  
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>
                 <input type="email" minlength="10" maxlength="80"  data-toggle="" name="emailid" id="emailid" class="form-control" value="<?php echo $campusupdate[0]->emailid;?>" placeholder="Enter Email Id" >
                  <?php echo form_error("emailid");?>
                </div>

               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Telephone No.<!-- (use hyphen in Telephone number) --> <span style="color: red;" >*</span></label>
                   <input type="text"  maxlength="12"  data-toggle="" name="telephone" id="telephone" class="form-control txtNumeric" value="<?php echo $campusupdate[0]->telephone;?>" placeholder="Enter Telephone No " >
                     <?php echo form_error("telephone");?>
                </div>

                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Fax No.<!-- (use hyphen in Telephone number) --> <span style="color: red;" >*</span></label>
                   <input type="text"  maxlength="12"  data-toggle="" name="fax" id="fax" class="form-control txtNumeric" value="<?php echo $campusupdate[0]->fax;?>" placeholder="Enter Fax No " >
                     <?php echo form_error("fax");?>
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Mobile No 1. <span style="color: red;" >*</span></label>
                 <input type="text"  maxlength="10" data-toggle="" name="mobile1" id="mobile1" class="form-control txtNumeric" data-country="India" value="<?php echo $campusupdate[0]->mobile;?>" placeholder="Enter Mobile No">
                  <?php echo form_error("mobile1");?>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Mobile No 2.</label>
                 <input type="text"  maxlength="10" data-toggle="" name="mobile2" id="mobile2" class="form-control txtNumeric" data-country="India" value="<?php echo $campusupdate[0]->mobile2;?>" placeholder="Enter Mobile No">
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Address  <span style="color: red;" >*</span></label>
                 <textarea name="address" id="address" class="form-control"><?php echo $campusupdate[0]->address;?></textarea>
                </div>
              
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                         <button type="submit" class="btn btn-success">Save </button>
                         <button type="reset" class="btn btn-warning">Reset </button>
                      </div>  
                </div>
               </div>
                 </form> 
                </div>
              </div><!-- /.panel-->
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->

    </div>
  <?php }  } ?>
  </section>
  <script type="text/javascript">
    $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});

  </script>