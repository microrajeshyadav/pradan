<section class="content">
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <div class="row clearfix">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

              
              <div class="body">
                <div class="row clearfix doctoradvice">
                 <h4 class="header" class="field-wrapper required-field" style="color:green">Add Leave Category</h4>
                 <div class="col-sm-12">
                   <form method="POST" action="">
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Leave Category<span style="color: red;" >*</span></label>
                        <input type="text" class="form-control" name="leavecategory" minlength="3" maxlength="30" id="Name" placeholder="Please Enter Leave Category" required="">
                      </div>
                       <?php echo form_error("leavecategory");?>
                    </div>

                     

                      <div class="form-group">
                      <!-- <div class="form-line"> -->
                    <!--     <label for="StateNameEnglish" class="field-wrapper required-field">Deduct<span style="color: red;" >*</span></label>
                     -->    
                        <div class="form-check">

                    <input class="form-check-input fillreject" type="checkbox" name="Deduct"  id="Deduct">
                     <label class="form-check-label" for="Deduct">
                    Deduct</label>
                   
                  </div>
                        </div>
                       <?php echo form_error("Deduct");?>
                    <!-- </div> -->



                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Remarks<span style="color: red;" >*</span></label>
                        <input type="text" class="form-control" name="Remarks" minlength="3" maxlength="255" id="Name" placeholder="Please Enter Remarks" required="">
                      </div>
                       <?php echo form_error("Remarks");?>
                    </div>



                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Status  <span style="color: red;" >*</span></label>
                      <?php   

                        //$options = array('' => 'Select Status');     
                       $options = array('0' => 'Active', '1' => 'InActive');
                       echo form_dropdown('status', $options, set_value('stateid'), 'class="form-control"'); 
                      ?>
                      </div>
                        <?php echo form_error("status");?>
                    </div>
                      <div style="text-align: -webkit-center;"> 
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("Leave");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Go to List</a> 
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>

    <script type="text/javascript">
      

      $(document).ready(function () {
  //called when key is pressed in textbox
  $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });

});
      
    </script>
