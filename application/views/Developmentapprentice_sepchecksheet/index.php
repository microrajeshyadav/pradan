<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
  

</style>

<form role="form" method="post" action="" name="formsepchecksheet" id="formsepchecksheet">
<section class="content" style="background-color: #FFFFFF;" >
<?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

    <br>
    <div class="container-fluid">
       <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="row">
         <div class="col-md-12 text-center">
          <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
        </div>


        <div class="col-md-12 text-center" style="margin-bottom: 20px;">
          <h4>CHECK SHEET</h4>
          <p>(for Processing Cases of Separation from PRADAN)</p>
        </div> 
      </div>
      
     
     

    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1.  Name of Separating Employee: <label class="inputborderbelow"> <?php echo $staff_detail->name; ?></label>
      </div>
      <div class="col-md-12">
       2. Employee Code : <label class="inputborderbelow"> <?php echo $staff_detail->emp_code; ?></label>
     </div>
     <div class="col-md-12">
      3.  Designation   : <label class="inputborderbelow"> <?php echo $staff_detail->joindesig; ?></label>
    </div>
    <div class="col-md-12">
      4.  Date of Joining PRADAN  : <label class="inputborderbelow"> <?php echo $this->gmodel->changedatedbformate($staff_detail->joiningdate); ?></label>
    </div>
    <div class="col-md-12">
      5.  Location  : <label class="inputborderbelow"> <?php echo $staff_detail->address; ?></label>
    </div>
    <div class="col-md-12">
     6. Reasons for Separation  : <input type="text" name="reason_for_separation" class="inputborderbelow" value="<?php echo $staff_transaction->trans_status; ?>"  readonly/>
   </div>
   <div class="col-md-12" style="font-size: 12px;">
    <i>(Resignation/Retirement/Premature Retirement/Death/Name Struck Off/Dismissal, etc.)</i>
  </div>
  <div class="col-md-12">
   7. Date on Which Resignation Submitted (if applicable): <input type="text" name="date_resignation_submitted_7" id="date_resignation_submitted_7" value="<?php 
   if($tbl_check_sheet){
    if($tbl_check_sheet->date_resignation_submitted_7)
    { echo $this->gmodel->changedatedbformate($tbl_check_sheet->date_resignation_submitted_7); 
    }else {
     echo $this->gmodel->changedatedbformate($staff_transaction->date_of_transfer);
      }}else{
        echo $this->gmodel->changedatedbformate($staff_transaction->date_of_transfer);
      }?>" class="inputborderbelow " required readonly/>
 </div>
 <div class="col-md-12">
   8. Date from which Separation Takes Effect: <input type="text" name="date_from_which_separation_takes_effect_8" id="date_from_which_separation_takes_effect_8" class="inputborderbelow" value="<?php 
   if($tbl_check_sheet){
    if($tbl_check_sheet->date_from_which_separation_takes_effect_8){
     echo $this->gmodel->changedatedbformate($tbl_check_sheet->date_from_which_separation_takes_effect_8); 
   }else { echo $this->gmodel->changedatedbformate($staff_transaction->effective_date); }}else{ echo $this->gmodel->changedatedbformate($staff_transaction->effective_date); }?>" required readonly/>
 </div>
 <div class="col-md-12">
  9.  Whether any disciplinary action is pending against
the separating Apprentice?:
  <input type="radio" name="whether_under_probation_9" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->whether_under_probation_9 == 1) echo "checked"; } ?>  required> Yes 
  <input type="radio" name="whether_under_probation_9" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->whether_under_probation_9 == 0) echo "checked"; } ?>   required> No
</div>
<div class="col-md-12">
  10. Whether occupying PRADAN's owned/leased/
rented accommodation?:
 <input type="radio" name="if_not_under_probation_10" value="1"  <?php if($tbl_check_sheet){ if($tbl_check_sheet->if_not_under_probation_10 == 1) echo "checked"; } ?>  required> Yes
  <input type="radio" name="if_not_under_probation_10" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->if_not_under_probation_10 == 0) echo "checked"; } ?> required> No
</div>



<div class="col-md-6">
  <div class="card">
    <span class="text-center bg-light">Recoveries involved on account of the following as on date of release:</span>
    <table class="table" id="amount_recover">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        Vehicle Loan
      </td>
      <td><input class="form-control txtNumeric" name="Vehicle_Loan" id="Vehicle_Loan" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->Vehicle_Loan) {echo $tbl_check_sheet->Vehicle_Loan; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td>Stipend in lieu of excess leave</td>
    <td><input class="form-control txtNumeric" name="stipend_in_lieu_of_excess_leave" id="stipend_in_lieu_of_excess_leave" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->stipend_in_lieu_of_excess_leave) {echo $tbl_check_sheet->stipend_in_lieu_of_excess_leave; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required/></td>
  </tr>
  <tr>
   <td>3.</td>
   <td>Other loans and advances</td>
  <td><input class="form-control txtNumeric" name="other_loans_and_advances" id="other_loans_and_advances" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->other_loans_and_advances) {echo $tbl_check_sheet->other_loans_and_advances; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
</tr>
<tr>
 <td>4.</td>
 <td>Outstanding dues</td>
<td><input class="form-control txtNumeric" name="outstanding_dues" id="outstanding_dues" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->outstanding_dues) {echo $tbl_check_sheet->outstanding_dues; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
</tr>

</tbody>

</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-light">Amount due and payable to separating employee on account of: (as on date of release): </span>
   <table class="table" id="amount_dues">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        Stipend
      </td>
      <td><input class="form-control" name="stipend" id="amount7" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->stipend) {echo $tbl_check_sheet->stipend; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td>Personal Claim</td>
     <td><input class="form-control txtNumeric" name="personal_claim" id="amount8" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->personal_claim) {echo $tbl_check_sheet->personal_claim; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>3.</td>
     <td>Any Other</td>
     <td><input class="form-control txtNumeric" name="any_other" id="amount9" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->any_other) {echo $tbl_check_sheet->any_other; } else { echo '';}  ?>"  maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   
   
  
   
 </tbody>
</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-info">Summary </span>
   <table class="table" id="">     
    <tbody>
     <tr>
       <td>(i)</td>
       <td> 
        Payments due to the apprentice
      </td>
      <td><input class="form-control txtNumeric" name="payments_due_to_employee" id="payments_due_to_employee" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->payments_due_to_employee) {echo $tbl_check_sheet->payments_due_to_employee; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>(ii)</td>
     <td>Amounts recoverable</td>
     <td><input class="form-control txtNumeric" name="amounts_recoverable" id="amounts_recoverable" value="<?php if($tbl_check_sheet) if($tbl_check_sheet->amounts_recoverable) {echo $tbl_check_sheet->amounts_recoverable; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>(iii)</td>
     <td>Net payable/recoverable</td>
     <td><input class="form-control txtNumeric" name="net_payable_recoverable" id="net_payable_recoverable" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->net_payable_recoverable){ echo $tbl_check_sheet->net_payable_recoverable; } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
  
 </tbody>

</table>
</div>
</div>


</div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
  <div class="col-md-12 text-left" >
  <span style="border-bottom:Solid 2px #000; ">Any other information/remarks relevant in this case:</span>
</div>
 <!-- <div class="col-md-4 text-left">
   <strong>Checked (Team Coordinator)</strong> <br>
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?></label> </p>
    <p>Place: _________________________________ </p>

 </div> -->
 <!-- <div class="col-md-4 text-left">
   <strong>Checked (HRD Unit)</strong> <br>
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?> </p>
    <p>Place: _________________________________ </p>

 </div> -->
<input type="hidden" name="curuserid" id="curuserid" value="<?php echo $this->loginData->staffid; ?>">
<?php if($staff_transaction->trans_flag >=4){ ?> 
<div class="col-md-4 text-left">
  <strong>Checked (Accounts Unit)</strong> <br>
  <p>Signature:  </p>
  <p>Name: 
    <?php
      if($tbl_check_sheet){
        if($tbl_check_sheet->financename)
         echo $tbl_check_sheet->financename;
        } else {
          echo $curUserDetails->name;
        } ?> </p>
  <p>Date: <input type="text" class="form-control" name="financeapprovaldate" id="financeapprovaldate" value="<?php if($tbl_check_sheet){ if($tbl_check_sheet->financeapprovaldate) echo $this->gmodel->changedatedbformate($tbl_check_sheet->financeapprovaldate); } else { echo date('d/m/Y'); } ?>" readonly> </p>
  <p>Place: <?php if($tbl_check_sheet){ if($tbl_check_sheet->financeoffice) echo $tbl_check_sheet->financeoffice; } else { echo $curUserDetails->officename; } ?> </p>
 </div>
<?php } ?>


<?php if(($staff_transaction->trans_flag ==5 && !empty($tbl_check_sheet) && ($this->loginData->staffid == $superviserreceiver->receiver)) || ($staff_transaction->trans_flag >=6)){  
  // if($tbl_check_sheet->financename){
?>
<div class="col-md-4 text-left">
  <strong>Checked (Team Coordinator)</strong> <br>
  <p>Signature:  </p>
  <p>Name:
  <?php
    if($tbl_check_sheet){
      if($tbl_check_sheet->supervisername){
        echo $tbl_check_sheet->supervisername;
      }else{
        echo $curUserDetails->name;
      }
      } else {
        echo $curUserDetails->name;
      }
  ?> </p>
  <p>Date: 
    <input type="text" class="form-control" name="superviserapprovaldate" id="superviserapprovaldate" value="<?php if($tbl_check_sheet){ if($tbl_check_sheet->superviserapprovaldate) { echo $this->gmodel->changedatedbformate($tbl_check_sheet->superviserapprovaldate); }else { echo date('d/m/Y'); } } ?>" readonly> </p>
  <p>Place: 
    <?php
      if($tbl_check_sheet){
        if($tbl_check_sheet->superviseroffice) {
          echo $tbl_check_sheet->superviseroffice;
        } else{
          echo $curUserDetails->officename;
        }
      }
    ?> </p>
 </div> 
<?php  } ?>
<?php if(($staff_transaction->trans_flag ==6 && !empty($tbl_check_sheet) && ($this->loginData->staffid == $hrreceiver->sender)) || ($staff_transaction->trans_flag >=7)){  
  // if($tbl_check_sheet->financename){
?>
<div class="col-md-4 text-left">
  <strong>Checked (HRD Unit)</strong> <br>
  <p>Signature:  </p>
  <p>Name:
  <?php
    if($tbl_check_sheet){
      if($tbl_check_sheet->hrname){
        echo $tbl_check_sheet->hrname;
      }else{
        echo $curUserDetails->name;
      }
      } else {
        echo $curUserDetails->name;
      }
  ?> </p>
  <p>Date: 
    <input type="text" class="form-control" name="hrdapprovaldate" id="hrdapprovaldate" value="<?php if($tbl_check_sheet){ if($tbl_check_sheet->hrdapprovaldate) { echo $this->gmodel->changedatedbformate($tbl_check_sheet->hrdapprovaldate); }else { echo date('d/m/Y'); } } ?>" readonly> </p>
  <p>Place: 
    <?php
      if($tbl_check_sheet){
        if($tbl_check_sheet->hroffice) {
          echo $tbl_check_sheet->hroffice;
        } else {
          echo $curUserDetails->officename;
        }
      }
    ?> </p>
 </div> 
<?php  } ?>


</div>

<!-- <div class="row text-left" style="margin-top: 20px; line-height: 3">
  <div class="col-md-2 text-right">
    Select HR :
  </div>
  <div class="col-md-4 text-left">
    <select class="form-control" name="hrid" id="hrid">
      <option value="">Select HR</option>
      <?php if($hrlist){
        foreach($hrlist as $res){
      ?>
      <option value="<?php echo $res->staffid; ?>" <?php if($selectedhr) if($selectedhr->receiver == $res->staffid) echo "selected"; ?>><?php echo $res->UserFirstName.' '.$res->UserLastName; ?></option>
      <?php
        }
      } ?>
    </select>
  </div>
</div> -->

</div>
<div class="panel-footer text-right">
  <?php
   if(empty($tbl_check_sheet)){
  ?>
  <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
  <?php
    }else{
      if($tbl_check_sheet->flag == 0){
 ?>
  <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
<?php } 
else if($tbl_check_sheet->flag == 1)
{
  ?>
   <!-- <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a> -->
   <?php
}

?>
  <!-- <input type="button" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();"> -->
<?php } ?>

<?php
  if($superviserreceiver){
    if(($this->loginData->staffid == $superviserreceiver->receiver) && ($staff_transaction->trans_flag ==5)){
?>
  <!-- <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm"> -->
  <input type="submit" name="saveandsubmit" value="Approve" class="btn btn-success btn-sm">
<?php   }

} ?>
<?php
  if($hrreceiver){
    if(($this->loginData->staffid == $hrreceiver->sender) && ($staff_transaction->trans_flag ==6)){
?>
  <!-- <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm"> -->
  <input type="submit" name="saveandsubmit" value="Approve" class="btn btn-success btn-sm">
<?php   }

} ?>
<?php if($this->loginData->RoleID==20){ ?>
 <a href="<?php echo site_url("Proposed_probation_separation_fc/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }elseif($this->loginData->RoleID==16){ ?>
  <a href="<?php echo site_url("Proposed_probation_separation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } ?>
</div>
</div>
</div>
</section>

</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#amount_recover input[type="text"]').on('change', calculateDues);
  $('#amount_dues input[type="text"]').on('change', calculateDues);
});
  function calculateDues()
  {
    var amount_recover = 0;
    var amount_dues = 0;calculateDues
    var finalpayment = 0;
    $('#amount_recover input[type="text"]').each(function(){
      if($(this).val()){
        amount_recover += parseFloat($(this).val());
      }
    })
    $('#amount_dues input[type="text"]').each(function(){
      if($(this).val()){
        amount_dues += parseFloat($(this).val());
      }
    })

    $('#payments_due_to_employee').val(amount_dues);
    $('#amounts_recoverable').val(amount_recover);
    finalpayment = amount_dues-amount_recover;
    $('#net_payable_recoverable').val(finalpayment);
    if(finalpayment < 0)
    {
      $('#net_payable_recoverable').css('border', '1px solid red');
    }else{
      $('#net_payable_recoverable').css('border', '');  
    }
  }



  
function acceptanceregination(){
  $("#hrid").prop('required',true);
  var date_resignation_submitted_7 = document.getElementById('date_resignation_submitted_7').value;
  var date_from_which_separation_takes_effect_8 = document.getElementById('date_from_which_separation_takes_effect_8').value; 
  var Vehicle_Loan = document.getElementById('Vehicle_Loan').value; 
  var stipend_in_lieu_of_excess_leave = document.getElementById('stipend_in_lieu_of_excess_leave').value; 
  var other_loans_and_advances = document.getElementById('other_loans_and_advances').value;  
  var outstanding_dues = document.getElementById('outstanding_dues').value; 
  var amount7 = document.getElementById('amount7').value; 
  var amount8 = document.getElementById('amount8').value; 
  var amount9 = document.getElementById('amount9').value;
  var payments_due_to_employee = document.getElementById('payments_due_to_employee').value;
  var amounts_recoverable = document.getElementById('amounts_recoverable').value;
  var net_payable_recoverable = document.getElementById('net_payable_recoverable').value;
  var hrid = document.getElementById('hrid').value;
  
  if(hrid == ''){
    $('#hrid').focus();
  }
  if(net_payable_recoverable == ''){
    $('#net_payable_recoverable').focus();
  }
  if(amounts_recoverable == ''){
    $('#amounts_recoverable').focus();
  }
  if(payments_due_to_employee == ''){
    $('#payments_due_to_employee').focus();
  }
  /*if(amount13 == ''){
    $('#amount13').focus();
  }
  if(amount12 == ''){
    $('#amount12').focus();
  }
  if(amount11 == ''){
    $('#amount11').focus();
  }
  if(amount10 == ''){
    $('#amount10').focus();
  }*/
  if(amount9 == ''){
    $('#amount9').focus();
  }
  if(amount8 == ''){
    $('#amount8').focus();
  }
  if(amount7 == ''){
    $('#amount7').focus();
  }
  if(outstanding_dues == ''){
    $('#outstanding_dues').focus();
  }
  if(other_loans_and_advances == ''){
    $('#other_loans_and_advances').focus();
  }
  if(stipend_in_lieu_of_excess_leave == ''){
    $('#stipend_in_lieu_of_excess_leave').focus();
  }
  if(Vehicle_Loan == ''){
    $('#Vehicle_Loan').focus();
  }
  if(date_from_which_separation_takes_effect_8 == ''){
    $('#date_from_which_separation_takes_effect_8').focus();
  }
  if(date_resignation_submitted_7 == ''){
    $('#date_resignation_submitted_7').focus();
  }
  
  if(date_resignation_submitted_7 != '' && date_from_which_separation_takes_effect_8 !='' && Vehicle_Loan !='' && stipend_in_lieu_of_excess_leave !='' && other_loans_and_advances !='' && outstanding_dues !='' && amount7 !='' && amount8 !='' && amount9 !='' && payments_due_to_employee !='' && amounts_recoverable !='' && net_payable_recoverable !='' && hrid !=''){
    document.forms['formsepchecksheet'].submit();
  }
}
function acceptancereginationmodal(){
  //alert('dkfhsk');
  var comments = document.getElementById('comments').value;
  var acceept = document.getElementById('status').value;
  var personal_administration = document.getElementById('personal_administration').value;
  if(acceept == ''){
    $('#status').focus();
  }else if(acceept == 9 && personal_administration ==''){
    $('#personal_administration').focus();
  }
  if(comments.trim() == ''){
    $('#comments').focus();
  }


  if(comments.trim() !='' && acceept != '' && personal_administration !=''){
    document.forms['formsepchecksheet'].submit();
  }
}

  $(document).ready(function(){
    $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    //maxDate: 'today',
    yearRange: '1920:2030',
    dateFormat : 'dd/mm/yy',
    //defaultDate: new Date(2018, 00, 01)
    });
  });
</script>
<?php  if($tbl_check_sheet){ if($tbl_check_sheet->flag == 1) { ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  $("#superviserapprovaldate").prop("disabled", false);
  $("#hrdapprovaldate").prop("disabled", false);
  $("form select").prop("disabled", true);
  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");
  });

  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
</script>
<?php }} ?>