<br/>
<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

          <!-- Exportable Table -->
          <form name="joinDA" id="Joincandidatetoda" action="" method="post">
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="panel thumbnail shadow-depth-2 listcontainer" >
                  <div class="panel-heading">
                    <div class="row">

                     <h4 class="col-md-10 panel-title pull-left">Manage Joining Candidate</h4>
                     <div class="col-md-12 text-right" style="color: red">
                      * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table id="tablejoininglist" class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                          <tr>
                           <th style ="max-width:70px;" class="text-center">Status</th>
                           <th>Campus Name</th>
                           <th>Name</th>
                           <th>Gender</th>
                           <th>Email Id</th>
                           <th>Mobile</th>
                           <!-- <th>Degree</th> -->
                           <th>Batch</th>
                           <th>Field guide Name</th>
                           <th>Office</th>
                           <th>Development Cluster</th>
                           <th class="text-center">Photo</th>
                         </tr>
                       </thead>
                       <?php 
                       if (count($candidatebdfformsubmit)==0) { ?>
                       <tbody>
                       </tbody>
                       <?php    }else{ ?>
                       <?php $i=0; foreach($candidatebdfformsubmit as $key => $val){ ?>
                       <tbody>
                        <tr>
                          <td class="text-center" style ="max-width:30px;">
                            <input type="checkbox" name="candidate_team[]" class="checkbox"  id="candidate_team_<?php echo $i; ?>" value="<?php echo $val->candidateid; ?>"></td>
                            <td class="text-center"><?php echo $val->campusname; ?></td>
                            <td class="text-center"><?php echo $val->name; ?></td>
                            <td><?php echo $val->gender;?></td>
                            <td><?php echo $val->emailid;?></td>
                            <td ><?php echo $val->mobile;?></td>
                            <!-- <td ><?php echo $val->mobile;?></td> -->
                            <td><?php echo $val->batch;?></td>
                            <td><?php echo $val->fieldguidename;?></td>
                            <td><?php echo $val->officename;?></td>
                            <td><?php echo $val->dc_name;?></td>
                            <td class="text-center" style="border:solid 2px #e1e1e1;  padding: 3px;">
                             <?php   
                             $image_path='';
                             if(!empty($val->encryptedphotoname)) {
                               $image_path='datafiles/'.$val->encryptedphotoname;
                               if (file_exists($image_path)) {

                                ?>
                                <img src="<?php echo site_url().'datafiles/'.$val->encryptedphotoname;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                                <?php }
                                else
                                {
                                  ?>
                                  <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">

                                  <?php
                                } } else {
                                  ?>
                                  <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                                  <?php } ?>


                                </td>
                              </tr>
                            </tbody>
                            <?php $i++; } } ?>

                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="panel-footer text-right">
                      <div class="sticky-container">
                        <ul class="sticky">
                          <?php if (count($candidatebdfformsubmit) > 0) { ?> 
                          <div class="row text-right">
                            <button  type="submit"  id="joinbtn" class="btn btn-success btn-lg" data-toggle="tooltip" title="Join Team" name="joinbtn" > <i class="fa fa-sign-in-alt"> </i></button>
                          </div>
                          <div class="row text-right">
                            <button class="btn btn-warning btn-lg" type="reset"  data-toggle="tooltip" title="Reset"><i class="fa fa-undo "> </i> </button>

                          </div>
                          <?php } ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- #END# Exportable Table -->

          </section>
          <script type="text/javascript">
           $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();  
            $('#tablejoininglist').DataTable(); 

            $('#Joincandidatetoda').submit(function() {
              amIChecked = false;
              $('input[type="checkbox"]').each(function() {
                if (this.checked) {
                  amIChecked = true;
                }
              });
              if (amIChecked == false) {

                alert('please check one checkbox!');
                return false;
              }

            });

          });   

        </script>