 
<style>
textarea {
  resize: none;
}
thead>tr:hover
{
  border-bottom: solid 1px red;
}   

label{

  font-weight: bold;
  font-family: 
}
.bg-lightgreen
{
  background-color: #85a4a5;
}
</style>



<style type="text/css" media="screen">
.error{
  color: red;
}  
</style>
<br/>
<script type="text/javascript">
 $("#basicinfo").find("input,textarea,select").attr("autocomplete", "off");
</script>

<section class="content">
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');
  if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>

          <?php } ?>
          <?php //echo "<pre>"; print_r($candidatedetails); ?>

          <div class="container-fluid" id="tblForm09" style="font-family: 'Oxygen' !important;" >
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-10 panel-title pull-left">Candidate Edit Profile for <?php echo $category; ?></h4>
                 <div class="col-md-12 text-right" style="color: red">
                  * Denotes Required Field 
                </div>

              </div>
              <hr class="colorgraph"><br>
            </div>    
            <div class="tab-content">    <!-- Start  Candidates Basic Info     -->
              <div id="home">
                <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
                  <input type="hidden" name="campusid" id="campusid" value="<?php echo $campusdecodeid; ?>">
                  <input type="hidden" name="categoryid" id="categoryid" value="<?php  echo $campuscatid; ?>">
                  <input type="hidden" name="campusintimationid" id="campusintimationid" value="<?php  echo $campusintimationid; ?>">

                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <table id="tbltrainingexposure" class="table table-bordered table-striped">
                      <thead>
                       <tr style="background-color: #eee;">
                        <th colspan="3" class="bg-lightgreen text-white">Personal Infomation</th>
                      </tr> 
                    </thead>
                    <tbody>
                      <tr style="background-color: #fff;">
                        <td colspan="3">
                          <div class="row">
                           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label for="Name">Candidate's Name<span style="color: red;">*</span></label>
                            
                            <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control alphabateonly" value="<?php echo $candidatedetails->candidatefirstname;?>" placeholder="Enter First Name" required="required">
                            
                            <?php echo form_error("candidatefirstname");?>
                          </div>

                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                            <label for="Name"> </label>
                            
                            <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control alphabateonly" value="<?php echo $candidatedetails->candidatemiddlename;?>" placeholder="Enter Middle Name" >
                            
                            <?php echo form_error("candidatemiddlename");?>
                          </div>

                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                            <label for="Name"></label>
                            
                            <input type="text"  minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="<?php echo $candidatedetails->candidatelastname;?>" required="required" >
                            
                            <?php echo form_error("candidatelastname");?>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
                            
                            <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control alphabateonly" value="<?php echo $candidatedetails->motherfirstname;?>" placeholder="Enter First Name" required="required" >
                            
                            <?php echo form_error("motherfirstname");?>
                          </div>

                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                            <label for="Name"> </label>
                            
                            <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control alphabateonly" value="<?php echo $candidatedetails->mothermiddlename;?>" placeholder="Enter Middle Name" >
                            
                            <?php echo form_error("mothermiddlename");?>

                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                            <label for="Name"></label>
                            <input type="text" minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="<?php echo $candidatedetails->motherlastname;?>" required="required">
                            <?php echo form_error("motherlastname");?>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Father's Name <span style="color: red;" >*</span></label>
                          
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control alphabateonly" value="<?php echo $candidatedetails->fatherfirstname;?>" placeholder="Enter First Name" required="required" >
                          
                          <?php echo form_error("fatherfirstname");?>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                         <label for="Name"></label>
                         <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control alphabateonly" value="<?php echo $candidatedetails->fathermiddlename;?>" placeholder="Enter Middle Name">
                         <?php echo form_error("fathermiddlename");?>

                       </div>
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                        <label for="Name"></label>
                        <input type="text" class="form-control alphabateonly" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="<?php echo $candidatedetails->fatherlastname;?>" required="required">
                        <?php echo form_error("fatherlastname");?>
                      </div>
                    </div>
                    <br>

                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Annual income of parents <span style="color: red;" >*</span></label>
                        <select name="annual_income" id="annual_income" required="required" class="form-control">
                          <option value="">Select</option>
                          <option value="1" <?php if ($otherinformationdetails->annual_income==1) {
                           echo "SELECTED";
                         } ?> >Below 50,000</option>
                         <option value="2" <?php   if ($otherinformationdetails->annual_income==2) {
                          echo "SELECTED";
                        }  ?>>50,001-200,000</option>
                        <option value="3" <?php if ($otherinformationdetails->annual_income==3) {
                          echo "SELECTED";
                        } ?> >200,001-500,000</option>
                        <option value="4" <?php if ($otherinformationdetails->annual_income==4) {
                         echo "SELECTED";
                       } ?>>Above 500,000</option>
                     </select>
                     <?php echo form_error("annual_income");?>
                   </div>

                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">No. of male sibling  <span style="color: red;" >*</span></label>
                    <input type="text"  maxlength="2" data-toggle="tooltip" title="Male sibling!" name="no_of_male_sibling" id="no_of_male_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo $otherinformationdetails->male_sibling; ?>" placeholder="Enter sibling" required="required">
                    <?php echo form_error("no_of_male_sibling");?>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">No. of female sibling  <span style="color: red;" >*</span></label>
                    <input type="text"  maxlength="2" data-toggle="tooltip" title="Female sibling!" name="no_of_female_sibling" id="no_of_female_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo $otherinformationdetails->female_sibling; ?>" placeholder="Enter sibling " required="required">
                    <?php echo form_error("no_of_female_sibling");?>
                  </div>
                  
                </div>
                <br>
                <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Gender <span style="color: red;" >*</span></label>

                  <?php 
                  $options = array("" => "Select", "1" => "Male", "2" => "Female","3" => "Other");

                  echo form_dropdown('gender', $options, $candidatedetails->gender, 'class="form-control" ');
                  ?>
                  <?php echo form_error("gender");?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Nationality  </label>
                  <?php 
                  $options = array("" => "Select", "indian" => "Indian", "other" => "Other");
                  echo form_dropdown('nationality', $options,$candidatedetails->nationality, 'class="form-control" ');
                  ?>
                  <?php echo form_error("nationality");?>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Marital Status </label>
                  <?php 
                  $options = array("" => "Select", "1" => "Single", "2" => "Married", "3" => "Divorced","4" => "Widow","5" => "Separated");
                  $selected = $candidatedetails->maritalstatus;
                  echo form_dropdown('maritalstatus', $options, $selected, 'class="form-control" ');
                  ?>
                  <?php echo form_error("maritalstatus");?>
                </div>
              </div>
              <br>

              <div class="row">
                <?php  $dob = $this->model->changedatedbformate($candidatedetails->dateofbirth); ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" name="dateofbirth" placeholder="Enter Date Of Birth " value="<?php echo $dob;?>" required="required" >
                  <?php echo form_error("dateofbirth");?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Email Id <span style="color: red;" >*</span></label>
                  <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo $candidatedetails->emailid;?>" required="required"  onblur="checkemail()" onchange="checkemail()">
                  <?php echo form_error("emailid");?>
                  <span style="color: red;" id="email_error"></span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                  <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="<?php echo $candidatedetails->mobile;?>" placeholder="Enter Mobile No" required="required" onkeypress="email_focus()" onchange="email_focus()">
                  <?php echo form_error("mobile");?>
                </div>
              </div>
              <br>
            </td>
          </tr>
        </tbody>
      </table>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table id="tbledubackground" class="table table-bordered table-striped" >
        <thead>
         <tr style="background-color: #eee;">
          <th colspan="8" class="bg-lightgreen text-white"> 
           Communication Address 
           
         </th>
       </tr> 
       <tr>
        <th class="text-center" colspan="4" style="vertical-align: top;">Mailing Address</th>
        <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Address<br>

          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="filladdress"> 
            <label class="form-check-label" for="filladdress"><b>same as Mailing Address</b></label>
          </div>
          
        </th>
      </tr> 
    </thead>
    <tbody>
     <tr style="background-color: #fff;">
      <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
      <td><input type="text" name="presenthno" id="presenthno" maxlength="50"class="form-control" 
        value="<?php echo $candidatedetails->presenthno;?>" placeholder="Enter H.No" required="required"></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><textarea name="presentstreet" id="presentstreet" maxlength="150" class="form-control" 
          placeholder="Enter H.No/Street" required="required"><?php echo $candidatedetails->presentstreet;?></textarea></td>
          <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
          <td><input type="text" name="permanenthno" id="permanenthno"  maxlength="50" class="form-control" value="<?php echo $candidatedetails->permanenthno;?>"  placeholder="Enter H.No/Street" required="required" ></td>
          <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
          <td><textarea name="permanentstreet" id="permanentstreet" maxlength="150" class="form-control"   placeholder="Enter H.No/Street" required="required"><?php echo $candidatedetails->permanentstreet;?></textarea></td>
        </tr>
        <tr>
          <td><label for="Name">City<span style="color: red;" >*</span></label> </td>
          <td><input type="text" name="presentcity" id="presentcity" maxlength="50" class="form-control txtOnly" placeholder="Enter City" value="<?php echo $candidatedetails->presentcity;?>"  required="required"></td>

          <td><label for="Name">State<span style="color: red;" >*</span></label> </td>
          <td><?php 
          $options = array('' => 'Select Present State');
          $selected = $candidatedetails->presentstateid;
          foreach($statedetails as$key => $value) {
            $options[$value->statecode] = $value->name;
          }
          echo form_dropdown('presentstateid', $options, $selected, 'class="form-control" id="presentstateid" data-toggle="tooltip" title=" Select State !" ');
          ?>

          <?php echo form_error("presentstateid");?>
        </td>

        
        <td><label for="Name">City<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentcity" id="permanentcity" maxlength="50" placeholder="Enter City" class="form-control txtOnly" value="<?php echo $candidatedetails->permanentcity;?>" required="required" ></td>

        <td><label for="Name">State<span style="color: red;" >*</span></td>
          <td> <?php 
          $options = array('' => 'Select Permanent State');
          $selected = $candidatedetails->permanentstateid;
          foreach($statedetails as$key => $value) {
            $options[$value->statecode] = $value->name;
          }
          echo form_dropdown('permanentstateid', $options, $selected, 'class="form-control" id="permanentstateid" data-toggle="tooltip" title=" Select State !" ');
          ?>

          <?php echo form_error("permanentstateid");?></td>
        </tr>
        <tr style="background-color: #fff;">
          <td><label for="Name">District<span style="color: red;" >*</span></label></td>
          <td>   <select name="presentdistrict" id="presentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
           <option value="">Select District </option>
           <?php foreach ($getdistrict as $key => $value) {
            if ( $value->districtid==$candidatedetails->presentdistrict) {
             ?>
             <option value="<?php echo $value->districtid; ?>" selected><?php echo $value->name; ?></option>
           <?php }else{ ?>
             <option value="<?php echo $value->districtid; ?>"><?php echo $value->name; ?></option>
           <?php } }?>

         </select></td>
         <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
         <td><input type="text" name="presentpincode" maxlength="6" id="presentpincode" class="form-control txtNumeric" value="<?php echo $candidatedetails->presentpincode; ?>" required="required" placeholder="Enter PinCode"></td>
         <td><label for="Name">District<span style="color: red;" >*</span></label></td>
         <td><select name="permanentdistrict" id="permanentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
           <?php foreach ($getdistrict as $key => $value) {
            if ( $value->districtid==$candidatedetails->permanentdistrict) {
             ?>
             <option value="<?php echo $value->districtid; ?>" selected><?php echo $value->name; ?></option>
           <?php }else{ ?>
             <option value="<?php echo $value->districtid; ?>"><?php echo $value->name; ?></option>
           <?php } }?>

         </select></td>

         <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
         <td><input type="text" name="permanentpincode"  maxlength="6" id="permanentpincode" class="form-control txtNumeric" value="<?php echo $candidatedetails->permanentpincode; ?>" required="required" placeholder="Enter PinCode"></td>
       </tr>
     </tbody>
   </table>
 </div>  
 <?php //echo "<pre>"; print_r($candidatedetails); ?>

 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">

  <table id="tbledubackground" class="table table-bordered table-striped">
    <thead>
     <tr style="background-color: #eee;">
      <th colspan="8" class="bg-lightgreen text-white">Educational Background</th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate</th>

      <th class="text-center" style="vertical-align: top;">Year </th>
      <th class="text-center" style="vertical-align: top;"> Board/ University</th>
      <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
      <th class="text-center" style="vertical-align: top;">Place</th>
      <th class="text-center" style="vertical-align: top;">Specialisation</th>
      <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
    </tr> 
  </thead>
  <tbody>
    <tr style="background-color: #fff;">
      <td><b>10th</b></td>

      <td><input type="text" class="form-control txtNumeric" required="required" data-toggle="tooltip" title="Year !" 
        id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" style="min-width: 20%;"  value="<?php echo $candidatedetails->metricpassingyear;?>" >
        <?php echo form_error("10thpassingyear");?></td>

        <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" required="required"
          id="10thboarduniversity" name="10thboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->metricboarduniversity;?>" >
          <?php echo form_error("10thboarduniversity");?></td>

          <td>  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="School/ College/ Institute !" required="required"
            id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo  $candidatedetails->metricschoolcollege;?>" >
            <?php echo form_error("10thschoolcollegeinstitute");?></td>

            <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" required="required" title="Place !" 
              id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo $candidatedetails->metricplace;?>" >
              <?php echo form_error("10thplace");?></td>
              <td><input type="text" class="form-control alphabateonly" required="required" data-toggle="tooltip" title="Specialisation !" 
                id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->metricspecialisation;?>" >
                <?php echo form_error("10thspecialisation");?></td>

                <td><input type="text" class="text-right form-control txtNumeric" required="required" data-toggle="tooltip" title="Percentage !"
                  id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo $candidatedetails->metricpercentage;?>"  >
                  <?php echo form_error("10thpercentage");?></td>
                </tr>
                <tr>
                  <td>
                   <select name="twelth" id="twelth" class="form-control" required="required">
                    <option value="2" <?php if ($candidatedetails->hscstream==2){ echo 'Selected="SELECTED"'; } ?> >12th</option>
                    <option value="10" <?php if ($candidatedetails->hscstream==10){ echo 'Selected="SELECTED"'; } ?>>Diploma</option>
                  </select>
                </td>

                <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" required="required"
                  id="12thpassingyear" name="12thpassingyear" placeholder="Enter Year" value="<?php echo $candidatedetails->hscpassingyear;?>" >
                  <?php echo form_error("12thpassingyear");?></td>

                  <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !"  required="required"
                    id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->hscboarduniversity;?>" >
                    <?php echo form_error("12thboarduniversity");?></td>

                    <td>  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="School/ College/ Institute !" required="required"
                      id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->hscschoolcollege;?>" >
                      <?php echo form_error("12thschoolcollegeinstitute");?></td>

                      <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !"  required="required"
                        id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo $candidatedetails->hscplace;?>" >
                        <?php echo form_error("12thplace");?></td>

                        <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Specialisation !" required="required"
                          id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->hscspecialisation;?>" >
                          <?php echo form_error("12thspecialisation");?></td>

                          <td><input type="text" class="text-right form-control txtNumeric" required="required"
                            id="12thpercentage" name="12thpercentage" placeholder="Percentage" value="<?php echo $candidatedetails->hscpercentage;?>" >
                            <?php echo form_error("12thpercentage");?></td>
                          </tr>
                          <tr style="background-color: #fff;">
                            <td>  <?php
                            $options = array('' => 'Select');
                            foreach($ugeducationdetails as $key => $value) {
                              $options[$value->pgname] = $value->pgname;
                            }
                            echo form_dropdown('ugstream', $options, $candidatedetails->ugdegree, 'class="form-control" data-toggle="tooltip" title="Up Stream !" required="required"');
                            ?>
                            <?php echo form_error("ugstream");?></td>

                            <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" required="required"
                              id="ugpassingyear" name="ugpassingyear" placeholder="Enter Year" value="<?php echo $candidatedetails->ugpassingyear;?>" >
                              <?php echo form_error("ugpassingyear");?></td>

                              <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" required="required"
                                id="ugboarduniversity" name="ugboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->ugboarduniversity;?>" >
                                <?php echo form_error("ugboarduniversity");?></td>

                                <td>
                                  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="School/ College/ Institute !" required="required"
                                  id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute " value="<?php echo $candidatedetails->ugschoolcollege;?>" >
                                  <?php echo form_error("ugschoolcollegeinstitute");?>
                                </td>
                                <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !" required="required"
                                  id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo $candidatedetails->ugplace;?>" >
                                  <?php echo form_error("ugplace");?></td>

                                  <td>
                                   <input type="text" class="form-control alphabateonly"  required="required" data-toggle="tooltip" title="Specialisation !" required="required"
                                   id="ugspecialisation" name="ugspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->ugspecialisation;?>"></td>

                                   <td><input type="text" class="text-right form-control txtNumeric" required="required" id="ugpercentage" name="ugpercentage" placeholder="Percentage" value="<?php echo $candidatedetails->ugpercentage;?>" >
                                    <?php echo form_error("ugpercentage");?></td>
                                  </tr>
                                  <tr style="background-color: #fff;">
                                    <td>
                                      <?php
                                      $options = array('' => 'Select');
                                      $selected = $candidatedetails->pgdegree;
                                      foreach($pgeducationdetails as $key => $value) {
                                        $options[$value->pgname] = $value->pgname;
                                      }
                                      echo form_dropdown('pgupstream', $options,  $selected, 'class="form-control" id="pgupstream"');
                                      ?>
                                      <?php echo form_error("pgupstream");?> 
                                    </td>

                                    <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                      id="pgpassingyear" name="pgpassingyear" placeholder="Enter Year" value="<?php echo $candidatedetails->pgpassingyear;?>" >
                                    </td>

                                    <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                      id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->pgboarduniversity;?>" >
                                    </td>
                                    <td>  
                                      <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                      id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->pgschoolcollege;?>" >
                                      <?php echo form_error("pgschoolcollegeinstitute");?>

                                    </td>

                                    <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !" 
                                      id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo $candidatedetails->pgplace;?>" >
                                    </td>

                                    <td>
                                      <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Specialisation  !" 
                                      id="pgspecialisation" name="pgspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->pgspecialisation;?>" >  </td>

                                      <td><input type="text" class="text-right form-control txtNumeric" 
                                        id="pgpercentage" name="pgpercentage" placeholder="Percentage" value="<?php echo $candidatedetails->pgpercentage;?>" >
                                      </td>
                                    </tr>

                                    <tr style="background-color: #fff;">
                                      <td>
                                        <div class="form-check">
                                          <input type="checkbox" class="filled-in" id="othe_degree"  value="Other" <?php if ($candidatedetails->otherdegree =='Other'){ echo 'checked="checked"'; } ?> name="othe_degree">
                                          <label class="form-check-label" for="othe_degree"><b>Other (specify)</b></label>
                                          <div id="other_other_specify_degree" style="display: none;">
                                            <input type="text" name="specify_degree" id="specify_degree" value="<?php echo $candidatedetails->other_degree_specify;?>" class="form-control">
                                          </div>
                                        </div>
                                      </td>

                                      <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                        id="otherpassingyear" name="otherpassingyear" placeholder="Enter Year" value="<?php echo $candidatedetails->otherpassingyear;?>" >
                                      </td>

                                      <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                        id="otherboarduniversity" name="otherboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->otherboarduniversity;?>" >
                                      </td>

                                      <td>  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="School/ College/ Institute !" 
                                        id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->otherschoolcollege;?>" >
                                      </td>
                                      <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !" 
                                        id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo $candidatedetails->otherplace;?>" >
                                      </td>

                                      <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Specialisation !" 
                                        id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->otherspecialisation;?>" >
                                      </td>
                                      <td><input type="text" class="text-right form-control txtNumeric" 
                                        id="otherpercentage" name="otherpercentage" placeholder="Percentage" value="<?php echo $candidatedetails->otherpercentage;?>" >
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>


                              <script type="text/javascript">
                                $(document).ready(function() {

                                  if ( $("input[name$='othe_degree']").is(':checked') ) {
                                    $("#other_other_specify_degree").show();
                                    $("#specify_degree").prop('required','required');
                                  } else {
                                   $("#other_other_specify_degree").hide();
                                   $("#specify").prop('disabled','true');
                                 }

                                 $("input[name$='othe_degree']").change(function() {
      //var test = $(this).val();
      if ( $(this).is(':checked') ) {
        $("#other_other_specify_degree").show();
        $("#specify_degree").prop('required','required');
      } else {
       $("#other_other_specify_degree").hide();
       $("#specify").prop('disabled','true');
     }
     
   });

                               });
                             </script>


                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                               <table id="tblForm15" class="table table-bordered table-striped">
                                <thead>
                                 <tr style="background-color: #eee;">
                                  <th colspan="3" class="bg-lightgreen text-white">

                                    <div class="col-lg-6 text-left"> Gap Year </div>
                                    <?php if($candidatedetails->BDFFormStatus !='1') {  ?>
                                      <div class="col-lg-6 text-right ">
                                       <button type="button" id="btnGapReasonRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                                       <button type="button" id="btnGapReasonAddRow" class="btn btn-warning btn-xs">Add</button>
                                     </div>
                                   <?php } ?>
                                 </th>
                               </tr> 
                               <tr>


                                <th class="text-center" style="vertical-align: top;"> From Date</th>
                                <th class="text-center" style="vertical-align: top;">To Date</th>
                                <th>Reason</th>
                              </tr> 
                            </thead>
                            <tbody id="bodytblForm15">
                              <input type="hidden" name="TGapreason" id="TGapreason" value="<?php echo $TrainingExpcount->TEcount; ?>">
                              <?php
                                      // echo $TrainingExpcount->TEcount;
                              if ($GapYearCount->GYcount==0) { ?>
                                <tr id="bodytblForm15" style="background-color: #fff;">

                                 <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
                                  id="gapfromdate" name="gapfromdate[]" placeholder="From Date" style="min-width: 20%;"  value=""  ></td>
                                  <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                                    id="gaptodate" name="gaptodate[]" placeholder="To Date" style="min-width: 20%;" value=""></td>
                                    <td>
                                      <textarea type="text" value="" rows="2" cols="5" name="gapreason[]" id="gapreason" class="form-control alphabateonly" data-toggle="tooltip"  maxlength="250" title="Gap Reason"   placeholder="Enter gap reason" ></textarea>
                                    </td>
                                  </tr>
                                <?php } else{ $i=0;
                         // echo "dfgdfgdfg"; 
                         // print_r($trainingexposuredetals);
                         // die;
                                  foreach ($gapyeardetals as $key => $val) {  ?>
                                    <tr id="bodytblForm15" style="background-color: #eee;">


                                      <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" id="gapfromdate_<?php echo $i;?>" name="gapfromdate[<?php echo $i;?>]" placeholder="From Date" onclick="return changedatepicker(<?php echo $i;?>)"  style="min-width: 20%;"  value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" required="required" ></td>
                                      <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" id="gaptodate_<?php echo $i;?>" name="gaptodate[<?php echo $i;?>]" placeholder="To Date" style="min-width: 20%;" required="required" onclick="return changedatepicker(<?php echo $i;?>)" value="<?php echo $this->model->changedatedbformate($val->todate);?>" ></td>
                                      <td><textarea class="form-control alphabateonly" data-toggle="tooltip"  maxlength="250" name="gapreason[<?php echo $i;?>]" id="gapreason_[<?php echo $i;?>]" title="Gap Reason" placeholder="Enter gap reason"  value="" rows="2" cols="5" required="required" ><?php echo $val->reason;?></textarea>

                                      </td>
                                    </tr>
                                    <?php $i++; } } ?>   
                                  </tbody>
                                </table>
                              </div>



                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
                               <table id="tblForm10" class="table table-bordered table-striped">
                                <thead>
                                 <tr style="background-color: #eee;">
                                  <th colspan="8" class="bg-lightgreen text-white">

                                   <div class="col-lg-12">
                                    <div class="col-lg-6 text-left"> Training  Exposure (if any)</div>
                                    <div class="col-lg-6 text-right ">
                                     <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                                     <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
                                   </div>
                                 </div>

                               </th>
                             </tr> 
                             <tr>
                              <th class="text-center" style="vertical-align: top;">Nature of Training </th>
                              <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
                              <th class="text-center" style="vertical-align: top;"> From Date</th>
                              <th class="text-center" style="vertical-align: top;">To Date</th>
                            </tr> 
                          </thead>
                          <tbody id="bodytblForm10">
                            <input type="hidden" name="TEcount" id="count_te" value="<?php echo $TrainingExpcount->TEcount; ?>">
                            <?php if ($TrainingExpcount->TEcount==0) {   ?>
                              <tr id="bodytblForm10" style="background-color: #fff;">
                                <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="Nature of Training !" minlength="5"   maxlength="50"
                                  id="natureoftraining"  maxlength="150" name="natureoftraining[]" placeholder="Enter Nature of Training" style="min-width: 20%;"  value="<?php echo set_value("natureoftraining");?>"  ></td>
                                  <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="Organizing Agency !" 
                                    id="organizingagency" name="organizingagency[]"  placeholder="Organizing Agency" style="min-width: 20%;"  value="<?php echo set_value("organizingagency");?>" ></td>
                                    <td><input type="text" class="form-control datepicker" data-toggle="tooltip" onchange="adddatepicker()"  title="From Date !" id="fromdate" name="fromdate[]" placeholder="From Date" style="min-width: 20%;"  value="<?php echo set_value("fromdate");?>"  ></td>
                                    <td><input type="text"
                                     class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                                     id="todate" name="todate[]" placeholder="To Date" style="min-width: 20%;"  value="<?php echo set_value("todate");?>"  ></td>
                                   </tr>
                                 <?php }else{ 
                                  $i=0;
                                  foreach ($trainingexposuredetals as $key => $val) {  ?>
                                    <tr id="bodytblForm10" style="background-color: #fff;">

                                      <td><input type="text" class="form-control inputclass alphabateonly" data-toggle="tooltip" title="Nature of Training !" minlength="5"   maxlength="150"
                                        id="natureoftraining" name="natureoftraining[<?php echo $i;?>]" placeholder="Enter Nature of Training"  maxlength="150"style="min-width: 20%;"  
                                        value="<?php echo $val->natureoftraining;?>" ></td>
                                        <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="50" title="Organizing Agency !" 
                                          id="organizingagency" name="organizingagency[<?php echo $i;?>]" placeholder="Organizing Agency" style="min-width: 20%;"  value="<?php echo $val->organizing_agency;?>" ></td>
                                          <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
                                            id="fromdate_<?php echo $i;?>" name="fromdate[<?php echo $i;?>]" placeholder="From Date" style="min-width: 20%;"  
                                            value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" ></td>
                                            <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" id="todate_<?php echo $i;?>" name="todate[<?php echo $i;?>]" placeholder="To Date" style="min-width: 20%;" value="<?php echo $this->model->changedatedbformate($val->todate);?>" ></td>
                                          </tr>
                                          <?php $i++; } } ?>  
                                          
                                        </tbody>
                                      </table>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                                     <table id="tblForm11" class="table table-bordered table-striped">
                                      <thead>
                                       <tr style="background-color: #eee;">
                                        <th colspan="8" class="bg-lightgreen text-white"> 
                                         <div class="col-lg-12">
                                          <div class="col-lg-6 text-left">Language Skill/Proficiency </div>
                                          <div class="col-lg-6 text-right ">
                                           <button type="button" id="btnproficiencyRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                                           <button type="button" id="btnproficiencyAddRow" class="btn btn-warning btn-xs">Add</button>
                                         </div>
                                       </div>
                                     </th>
                                   </tr> 
                                   <tr>

                                    <th class="text-center" style="vertical-align: top;">Language</th>
                                    <th class="text-center" style="vertical-align: top;">Speak</th>
                                    <th class="text-center" style="vertical-align: top;">Read</th>
                                    <th class="text-center" style="vertical-align: top;">Write </th>
                                  </tr> 
                                </thead>
                                <tbody id="bodytblForm11">
                                  <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
                                  <?php if ($languageproficiency->Lcount==0) { ?>
                                    <tr id="bodytblForm11" style="background-color: #fff;">
                                      <td>  <?php
                                      $options = array('' => 'Select');
                                      foreach($syslanguage as $key => $value) {
                                        $options[$value->lang_cd] = $value->lang_name;
                                      }
                                      echo form_dropdown('syslanguage[]', $options, set_value('syslanguage'), 'class="form-control" data-toggle="tooltip" title="Select language !"');
                                      ?>
                                      <?php echo form_error("syslanguage");?></td>
                                      <td>
                                       <?php
                                       $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                                       echo form_dropdown('speak[]',$sysspeak,set_value('speak'),'class ="form-control" data-toggle="tooltip" title="Select Speak !" ');
                                       ?>
                                       <?php echo form_error("speak");?>
                                     </td>
                                     <td><?php
                                     $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                                     echo form_dropdown('read[]',$sysread,set_value('read'),'class ="form-control" data-toggle="tooltip" title="Select Read !" ');
                                     ?>
                                     <?php echo form_error("read");?></td>
                                     <td><?php
                                     $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                                     echo form_dropdown('write[]',$syswrite,set_value('write'),'class ="form-control" data-toggle="tooltip" title="Select Write !" ');
                                     ?>
                                     <?php echo form_error("write");?></td>
                                   </tr>
                                 <?php }else{
                                  $i= 0;

                                  foreach ($languagedetals as $key => $val) {
                                   ?>
                                   <tr id="bodytblForm11" style="background-color: #fff;">
                                    <td>  <?php
                                    $options = array('' => 'Select');
                                    foreach($syslanguage as $key => $value) {
                                      $options[$value->lang_cd] = $value->lang_name;
                                    }
                                    echo form_dropdown('syslanguage['.$i.']', $options, $val->languageid, 'class="form-control" data-toggle="tooltip" title="Select language !" required="required" ');
                                    ?>
                                    <?php echo form_error("syslanguage");?></td>
                                    <td>
                                     <?php
                                     $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                                     echo form_dropdown('speak['.$i.']',$sysspeak,$val->lang_speak,'class ="form-control" data-toggle="tooltip" title="Select Speak !" required="required" ');
                                     ?>
                                     <?php echo form_error("speak");?>
                                   </td>
                                   <td><?php
                                   $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                                   echo form_dropdown('read['.$i.']',$sysread,$val->lang_read,'class ="form-control" data-toggle="tooltip" title="Select Read !" required="required" ');
                                   ?>
                                   <?php echo form_error("read");?></td>
                                   <td><?php
                                   $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                                   echo form_dropdown('write['.$i.']',$syswrite,$val->lang_write,'class ="form-control" data-toggle="tooltip" required="required"  title="Select Write !" ');
                                   ?>
                                   <?php echo form_error("write");?></td>
                                 </tr>

                                 <?php $i++; } }  ?>
                               </tbody>
                             </table>
                           </div>


                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                             <table id="tbltrainingexposure" class="table table-bordered table-striped">
                              <thead>
                               <tr style="background-color: #eee;">
                                <th colspan="8" class="bg-lightgreen text-white">Subject(s) of Interest </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr style="background-color: #fff;">
                                <td><textarea class="form-control alphabateonly" name="subjectinterest"  maxlength="1500" id="subjectinterest" cols="12" rows="5" data-toggle="tooltip" title="Enter Subject(s) of Interest !" ><?php echo $otherinformationdetails->any_subject_of_interest; ?></textarea></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                         <table id="tbltrainingexposure" class="table table-bordered table-striped">
                          <thead>
                           <tr style="background-color: #eee;">
                            <th colspan="8" class="bg-lightgreen text-white"> Achievements /Awards (if any) </th>
                          </tr> 
                        </thead>
                        <tbody>
                          <tr>
                            <td><textarea class="form-control alphabateonly" name="achievementawards" id="achievementawards" cols="12" rows="5"  maxlength="1500" data-toggle="tooltip" title="Enter Achievement /Awards(if any) !" >
                              <?php echo $otherinformationdetails->any_achievementa_awards; ?>
                            </textarea></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                     <table id="tblForm12" class="table table-bordered table-striped">
                      <thead>
                       <tr style="background-color: #eee;">
                        <th colspan="7" class="bg-lightgreen text-white">
                         <div class="col-lg-12">
                          <div class="col-lg-6 text-left">Work Experience (if any) </div>
                          <div class="col-lg-6 text-right ">
                           <button type="button" id="btnworkexperienceRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                           <button type="button" id="btnworkexperienceAddRow" class="btn btn-warning btn-xs">Add</button>
                         </div>
                       </div>
                     </th>
                   </tr> 
                   <tr>
                    <th>Organization Name</th>
                    <th>Designation</th>
                    <th>Description of Assignment</th>
                    <th>Duration</th>
                    <th>Place of Posting</th>
                    <th>Last salary drawn (monthly)</th>

                  </tr> 
                  <tr>
                    <th colspan="3"></th>
                    <th colspan="1">
                      <div class="col-lg-12">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
                      </div></th>
                      <th colspan="3"></th>
                    </tr>
                  </thead>
                  <tbody id="bodytblForm12">
                   <input type="hidden" name="workexpcount" id="workexpcount" value="<?php echo $WorkExperience->WEcount;?>">
                   <?php if ($WorkExperience->WEcount==0) {?>
                     <tr id="bodytblForm12" style="background-color: #fff;"> 
                      <td><input type="text" name="orgname[]" maxlengt="100" id="orgname" data-toggle="tooltip" value="" class="form-control alphabateonly" title="Enter Organization Name !" placeholder="Enter Organization Name "></td>
                      <td><input type="text" name=designation[]" maxlengt="100" id="designation" data-toggle="tooltip" value="" class="form-control" title="Enter Designation !"  placeholder="Enter Designation">
                      </td>

                      <td><textarea  maxlengt="250" name="descriptionofassignment[]" data-toggle="tooltip" id="descriptionofassignment" value="" class="form-control alphabateonly" title="Enter Description of Assignment" placeholder="Enter Description of Assignment " ></textarea></td>
                      <td>
                        <div class="col-lg-12">
                          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                            <input type="text" name="work_experience_fromdate[]" id="work_experience_fromdate" class="form-control datepicker" value=""  placeholder="Enter from date">
                          </div> 
                          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                            <input type="text" name="work_experience_todate[]" id="work_experience_todate" class="form-control datepicker" value="" placeholder="Enter To date">
                          </div> 
                        </div>
                      </td>
                      <td><input type="text" maxlengt="50" name="palceofposting[]" data-toggle="tooltip" id="palceofposting" value="" class="form-control alphabateonly"  title="Place of Posting[]" placeholder="Enter Place of Posting"></td>
                      <td><input type="text" name=last_salary_drawn[]" maxlengt="100" id="last_salary_drawn" data-toggle="tooltip" value="" class="form-control txtNumeric" title="Enter Last salary drawn !"  placeholder="Enter last salary drawn">
                      </td>
                    </tr>
                  <?php  } else{
                    $i=0;
                    foreach ($workexperiencedetails as $key => $val) {   ?>
                      <tr id="bodytblForm12" style="background-color: #fff;"> 
                        <td><input type="text" maxlengt="100" name="orgname[<?php echo $i;?>]" id="orgname" data-toggle="tooltip" value="<?php echo $val->organizationname;  ?>" class="form-control" title="Enter Organization Name !" placeholder="Enter Organization Name " " ></td>
                        <td><input type="text" name=designation[]" maxlengt="100" id="designation" data-toggle="tooltip" value="<?php echo $val->designation;?>" class="form-control" title="Enter Designation !"  placeholder="Enter Designation">
                        </td>
                        <td><textarea maxlengt="250" name="descriptionofassignment[<?php echo $i;?>]" data-toggle="tooltip" id="descriptionofassignment" value="" class="form-control" title="Enter Description of Assignment" placeholder="Enter Description of Assignment "><?php echo $val->descriptionofassignment;?></textarea></td>
                        <td>
                         <div class="col-lg-12">
                          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                            <input type="text" name="work_experience_fromdate[<?php echo $i;?>]" id="work_experience_fromdate[<?php echo $i;?>]" value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" class="form-control datepicker" value=""  placeholder="Enter from date">
                          </div> 
                          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                            <input type="text" name="work_experience_todate[<?php echo $i;?>]" id="work_experience_todate[<?php echo $i;?>]" class="form-control datepicker" value="<?php echo $this->model->changedatedbformate($val->todate);?>" placeholder="Enter To date">
                          </div> 
                        </div>
                      </td>
                      <td><input type="text" maxlength="50" name="palceofposting[<?php echo $i;?>]" data-toggle="tooltip" id="palceofposting" 
                        value="<?php echo $val->palceofposting;?>" class="form-control datepicker" title="Place of Posting[]" placeholder="Enter Place of Posting"></td>
                        <td><input type="text" name="last_salary_drawn[<?php echo $i;?>]" maxlengt="100" id="last_salary_drawn[<?php echo $i;?>] data-toggle="tooltip" value="<?php echo $val->lastsalarydrawn;?>"" class="form-control txtNumeric" title="Enter Last salary drawn !"  placeholder="Enter last salary drawn">
                        </td>
                      </tr>

                      <?php $i++; } } ?>
                    </tbody>
                  </table>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
                 <table id="tbltrainingexposure" class="table table-bordered table-striped">
                  <thead>
                   <tr style="background-color: #eee;">
                    <th colspan="8" class="bg-lightgreen text-white">Describe any assignment(s) of special interest undertaken by you </th>
                  </tr> 

                </thead>
                <tbody>
                  <tr style="background-color: #fff;">
                   <td><textarea class="form-control alphabateonly" data-toggle="tooltip"  maxlength="1500" name="any_assignment_of_special_interest" id="any_assignment_of_special_interest" cols="12" rows="5" title="Describe any Assignment(s) of special interest undertaken by you(if any) " placeholder="Enter Achievement /Awards" ><?php echo $otherinformationdetails->any_assignment_of_special_interest; ?></textarea></td>
                 </tr>
               </tbody>
             </table>
           </div>

           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
             <table id="tbltrainingexposure" class="table table-bordered table-striped">
              <thead>
               <tr style="background-color: #eee;">
                <th colspan="8" class="bg-lightgreen text-white">Experience of Group and/or Social Activities </th>
              </tr> 
            </thead>
            <tbody>
              <tr style="background-color: #fff;">
                <td><textarea class="form-control alphabateonly" data-toggle="tooltip"  maxlength="1500" name="experience_of_group_social_activities" id="experience_of_group_social_activities" cols="12" rows="5" title="Experience of Group and/or Social Activities " placeholder="Enter Experience of Group and/or Social Activities " ><?php echo $otherinformationdetails->experience_of_group_social_activities; ?></textarea></td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
         <table id="tblgap" class="table table-bordered table-striped">
          <thead>
           <tr style="background-color: #eee;">
            <td colspan="8" class="bg-lightgreen text-white"><b>Have you taken part in PRADAN's selection process before?</b> <span style="color:red"> *</span>
            </td>
          </tr> 
        </thead>
        <tbody>
          <tr style="background-color: #fff;">
            <td>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                <div class="form-check">
                  <input type="radio" class="form-check-input" id="yes" value="yes" 
                  <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='yes') {
                    echo 'checked="checked"';
                  }
                  ?>
                  name="pradan_selection_process_before">
                  <label class="form-check-label" for="yes"><b>Yes</b></label>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" class="form-check-input" id="no" value="no" 
                  <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='no') {
                    echo 'checked="checked"';
                  }
                  ?>
                  name="pradan_selection_process_before"  >
                  <label class="form-check-label" for="no"><b>No</b></label>

                </div>

              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <input type="text" name="when" id="when" value="<?php echo $this->model->changedatedbformate($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when); ?>" class="form-control datepicker">
              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
                <select name="where" id="where" class="form-control">
                  <option value="">select</option>
                  <option value="1" <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==1) {
                   echo 'selected="selected"';
                 } ?>>ON Campus</option>
                 <option value="2" <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==2) {
                   echo 'selected="selected"';
                 } ?>>Off Campus</option>
               </select>
             </div>
           </td>
         </tr>
       </tbody>
     </table>
   </div>


<!--  <script type="text/javascript">
  
    $("input[name$='pradan_selection_process_before']").load(function() {
      var selected = $(this).val();
      if (selected =='yes') {
        $("#when").removeAttr('disabled','false'); 
        $("#where").removeAttr('disabled','false');
      }else{
        $("#when").prop('disabled','true'); 
        $("#where").prop('disabled','true');
      }
    });
    $("input[name$='pradan_selection_process_before']").click(function() {
      var selected = $(this).val();
      
      if (selected =='yes') {
        $("#when").removeAttr('disabled','false'); 
        $("#where").removeAttr('disabled','false');
      }else{
       $('#when').attr('value', '');  
       $('#where option').slice(1,2).remove(); 
       $("#when").prop('disabled','true'); 
       $("#where").prop('disabled','true');
     }
   });


 </script> -->


 <script type="text/javascript">
            // $(document).ready(function() {
             // $("#when").prop('disabled','true'); 
             // $("#where").prop('disabled','true');
             // $("input[name$='pradan_selection_process_before']").click(function() {
             //  var selected = $(this).val();

             //  if (selected =='yes') {
             //    $("#when").removeAttr('disabled','false'); 
             //    $("#where").removeAttr('disabled','false');
             //  }else{
             //    $("#when").prop('disabled','true'); 
             //    $("#where").prop('disabled','true');
             //  }

            // });

          // });
        </script>



        <script type="text/javascript">
            // $(document).ready(function() {
             $("#when").prop('disabled','true'); 
             $("#where").prop('disabled','true');
             $("input[name$='pradan_selection_process_before']").click(function() {
              var selected = $(this).val();

              if (selected =='yes') {
                $("#when").removeAttr('disabled','false'); 
                $("#where").removeAttr('disabled','false');
                $("#when").prop('required', 'required');
                $("#where").prop('required', 'required');
              }else{
                $("#when").prop('disabled','true'); 
                $("#where").prop('disabled','true');
              }

            });


          </script>


          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
           <table id="tbltrainingexposure" class="table table-bordered table-striped">
            <thead>
             <tr style="background-color: #eee;">
              <td colspan="8" class="bg-lightgreen text-white"><b>From where have you come to know about PRADAN?</b> <span style="color:red"> *</span>
              </td>
            </tr> 
          </thead>
          <tbody>
            <tr style="background-color: #fff;">
              <td>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                  <div class="form-check">
                    <input type="radio" class="form-check-input" id="campus_placement_cell" <?php if ($otherinformationdetails->know_about_pradan=='CampusPlacementCell') {
                     echo 'checked="checked"';
                   }  ?> value="CampusPlacementCell" name="have_you_come_to_know">
                   <label class="form-check-label" for="campus_placement_cell"><b>Campus Placement Cell</b></label>
                 </div>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
                 <!-- Material checked -->
                 <div class="form-check">
                  <input type="radio" class="form-check-input" id="university_professors"  <?php if ($otherinformationdetails->know_about_pradan=='UniversityProfessors') {
                   echo 'checked="checked"';
                 }  ?> value="UniversityProfessors" name="have_you_come_to_know"  >
                 <label class="form-check-label" for="university_professors"><b>University Professors</b></label>
               </div>
             </div>
             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
               <!-- Material checked -->
               <div class="form-check">
                <input type="radio" class="form-check-input" id="campus_alumni"  <?php if ($otherinformationdetails->know_about_pradan=='CampusAlumni') {
                 echo 'checked="checked"';
               }  ?> value="CampusAlumni" name="have_you_come_to_know" >
               <label class="form-check-label" for="campus_alumni"><b>Campus Alumni</b></label>
             </div>
           </div>
           <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
            <!-- Material checked -->
            <div class="form-check">
              <input type="radio" class="form-check-input" id="friends" <?php if ($otherinformationdetails->know_about_pradan=='friends') {
               echo 'checked="checked"';
             }  ?> value="friends" name="have_you_come_to_know" >
             <label class="form-check-label" for="friends"><b>Friends</b></label>
           </div>
         </div>
         <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2"> 
           <!-- Material checked -->
           <div class="form-check">
            <input type="radio" class="form-check-input" id="other_pradan" <?php if ($otherinformationdetails->know_about_pradan=='other1') {
             echo 'checked="checked"';
           }  ?>  value="other1" name="have_you_come_to_know">
           <label class="form-check-label" for="other"><b>Other (specify)</b></label>
           <div id="other_have_you_come_to_know_specify" style="display: none;">
            <input type="text" name="specify" id="specify" value="<?php echo $otherinformationdetails->know_about_pradan_other_specify ?>" class="form-control">
          </div>
        </div>
      </div>

    </td>
  </tr>
</tbody>
</table>
</div>

<script type="text/javascript">
  $(document).ready(function() {

     $( "input[name='have_you_come_to_know']" ).each(function( index ) { 

        if ($(this).is(':checked') && $(this).attr("id")== 'other_pradan') { 
          $("#other_have_you_come_to_know_specify").show(); 
          $("#specify").prop('required','required'); 
          $("#specify").removeAttr('disabled','false');
        } else { 
          $("#other_have_you_come_to_know_specify").hide(); 
          $("#specify").prop('disabled','true'); 
        } 
      }); 

    $("input[name='have_you_come_to_know']").click(function() { 
      $( "input[name='have_you_come_to_know']" ).each(function( index ) { 

        if ($(this).is(':checked') && $(this).attr("id")== 'other_pradan') { 
          $("#other_have_you_come_to_know_specify").show(); 
          $("#specify").prop('required','required'); 
          $("#specify").removeAttr('disabled','false');
        } else { 
          $("#other_have_you_come_to_know_specify").hide(); 
          $("#specify").prop('disabled','true'); 
        } 
      }); 
    }); 


    // $("input[name$='have_you_come_to_know']").click(function() {

    //   var test = $(this).val();
    //   if (test =='other1') {
    //     $("#other_have_you_come_to_know_specify").show();
    //     $("#specify").prop('required','required');
    //     $('#specify').removeAttr("disabled");

    //   }else{
    //     $("#other_have_you_come_to_know_specify").hide();
    //     $("#specify").prop('disabled','true');

    //   }

    // });

 // var other_pradan = $('#other_pradan').val();

 // //alert(other_pradan);
 //    if (other_pradan =='other1') {
 //      $("#other_have_you_come_to_know").show();
 //      $("#specify").prop('required','required');
 //      $('#specify').removeAttr("disabled");

 //    }else{

 //      $("#other_have_you_come_to_know").hide();
 //      $("#specify").prop('disabled','true');

 //    }



});

  


</script>


</div>

<div class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" style="background-color: #ffffff;">
 <button type="reset" class="btn btn-warning  btn-sm m-t-10 waves-effect">Reset </button>
 <button type="submit" value="saveWorkExpSubmit" id="sendDataWorkExpButton" class="btn btn-success  btn-sm m-t-10 waves-effect" onclick="return ifanyvalidation()">Save </button>
 <a href="http://www.pradan.net/" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 

</div>
</tbody>
</table>
</form>
</div>
<!-- End  Candidates Basic Info  -->
</div>
</div>
</div>


</div>
</div>

</section>

<script type="text/javascript">
 function ifanyvalidation(){
  var natureoftraining        = $('#natureoftraining').val();
  var organizingagency        = $('#organizingagency').val();
  var fromdate                = $('#fromdate').val();
  var todate                  = $('#todate').val();
  var orgname                 = $('#orgname').val();
  var descriptionofassignment = $('#descriptionofassignment').val();
  var designation             = $('#designation').val();
  var last_salary_drawn       = $('#last_salary_drawn').val();
  var work_experience_fromdate = $('#work_experience_fromdate').val();
  var work_experience_todate  = $('#work_experience_todate').val();
  var palceofposting          = $('#palceofposting').val();
  var gapfromdate             = $('#gapfromdate').val();
  var gaptodate               = $('#gaptodate').val();
  var gapreason               = $('#gapreason').val();
  
  if (gapfromdate !='') {
    if (gaptodate =='') {

     $('#gaptodate').focus();
     
     return false;
   }
   if (gapreason =='') {
     $('#gapreason').focus();
     
     return false;
   }

 }

 if (natureoftraining !='') {

  if (organizingagency =='') {
    $('#organizingagency').focus();
    return false;
  }
  if (fromdate =='') {
    $('#fromdate').focus();
    return false;
  }
  if (todate =='') {
    $('#todate').focus();
    return false;
  }

}

if (orgname !='') {
 if (designation =='') {
  $('#designation').focus();
  return false;
}
if (descriptionofassignment =='') {
 $('#descriptionofassignment').focus();
 return false;
}
if (work_experience_fromdate =='') {
  $('#work_experience_fromdate').focus();
  return false;
}


if (work_experience_todate =='') {
 $('#work_experience_todate').focus();
 return false;
}
if (palceofposting =='') {
 $('#palceofposting').focus();
 return false;
}

if (last_salary_drawn =='') {
  $('#last_salary_drawn').focus();
  return false;
}

}


}

 /// Upload Buttone /////////

 $(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
 $(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
 
 var matriccertificate_uploadField = document.getElementById("10thcertificate");

 matriccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 


var hsccertificate_uploadField = document.getElementById("12thcertificate");

hsccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

var ugcertificate_uploadField = document.getElementById("ugcertificate");

ugcertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

</script>

<script type="text/javascript">
  $(document).ready(function(){
    decimalData();
    adddatepicker();
    ifanyvalidation();
    $('[data-toggle="tooltip"]').tooltip(); 
    //$("#pradan_selection_process_before_details").prop("disabled", "disabled");
    $("#selection_process_befor").click(function () {

      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");

      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
    
    if($('#selection_process_befor').prop("checked") == false){
     $("#pradan_selection_process_before_details").prop("disabled", "disabled");
   }

 });

  $(function () {
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
  });
  



  $(document).ready(function(){
    $("#filladdress").on("click", function(){
     if (this.checked) { 
      if ($("#presentstreet").val().length == 0  && $("#permanentstreet").val().length ==0) {
        alert('Please Fill Present Mailing Address !!!');
        $("#filladdress").removeAttr('checked');
      }else{

        $("#permanenthno").val($("#presenthno").val());
        $("#permanentstreet").val($("#presentstreet").val());
        $("#permanentcity").val($("#presentcity").val());
        $("#permanentstateid").val($("#presentstateid").val()); 

        $.ajax({
          url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ $("#presentstateid").val(),
          type: 'POST',
          dataType: 'text',
        })
        .done(function(data) {

            //console.log(data);
            $("#permanentdistrict").html(data);
            $("#permanentdistrict").val($("#presentdistrict").val());
            
          })
        $("#permanentdistrict").val($("#presentdistrict").val());
        $("#permanentpincode").val($("#presentpincode").val()); 
        
      }
    }
    else {
      $("#permanenthno").val('');
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

    if ($("#presentstreet").val() == $("#permanentstreet").val()) {
      $("#filladdress").prop('checked', true);
    }

  });


  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

  }

  $("#btnRemoveRow").click(function() {
    if($('#tblForm09 tr').length-2>1)
      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);

    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    //addDatePicker();
  });



  var srNoGlobal=0;
  var inc = 0;

  function insertRows(count) {
    srNoGlobal = $('#tbodyForm09 tr').length+1;
    var tbody = $('#tbodyForm09');
    var lastRow = $('#tbodyForm09 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inc++;
      cloneRow = lastRow.clone();
      var tableData = '<tr>'
      + ' <td>'
      + '<input type="text" name="familymembername['+inc+']" id="familymembername" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td><td>'
      + '<select class="form-control" name="relationwithenployee['+inc+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysrelations as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>'
      <?php endforeach ?>
      + '</select>'
      + '</td>'
      + '<td><input type="text" class="form-control datepicker" id="familymemberdob'+inc+'" data-toggle="tooltip" title="Date Of Birth!" name="familymemberdob['+inc+']" placeholder="Enter Date Of Birth " value="" required="required" >'

      + '</td>'
      + '</tr>';
      adddatepicker();
      $("#tbodyForm09").append(tableData)

      
    }

  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
    rowsEnter1 = $('#bodytblForm10 tr').length;
    // if (rowsEnter1 < 1) {
    //   alert("Row number must be minimum 1.")
    //   return;
    // }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });
  var count_te = $('#count_te').val();
  if(count_te =='NULL'){
   var srNoGlobal=0;
   var inctg = 0;
 }else{
   var srNoGlobal = count_te;
   var inctg = count_te;
 }


 function Insaettrainingexposure(count) {
  srNoGlobal = $('#bodytblForm10 tr').length+1;
  var tbody = $('#bodytblForm10');
  var lastRow = $('#bodytblForm10 tr:last');
  var cloneRow = null;
  var inctg = count;
    // for (i = 1; i <= count; i++) {
    //   if (count_te==0) {
    //   inctg++;
    // }

    cloneRow = lastRow.clone();
    var tableData1 = '<tr>'
    + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" id="natureoftraining" name="natureoftraining['+inctg+']" placeholder="Enter Nature of Training" style="min-width: 20%;" value="" data-original-title="Nature of Training !" required>'
    + '</td>'
    + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="organizingagency_'+inctg+'" name="organizingagency['+inctg+']" placeholder="Organizing Agency" style="min-width: 20%;" value="" data-original-title="Organizing Agency !" required></td>'

    + '<td><input type="text" class="form-control datepicker " id="fromdate_'+inctg+'" '+
    'name="fromdate['+inctg+']" onclick="return changedatepicker(inctg)" value=""  required="required" />'
    + '</td><td>'
    + '<input type="text" class="form-control datepicker " id="todate_'+inctg+'" '+
    'name="todate['+inctg+']" value="" onclick="return changedatepicker(inctg)"  required="required" />'
    + '</td>' + '</tr>';
    adddatepicker();
    $("#bodytblForm10").append(tableData1)
    changedatepicker(inctg);
   // }

 }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnproficiencyRemoveRow").click(function() {
    if($('#tblForm11 tr').length-2>1)
      $('#bodytblForm11 tr:last').remove()
  });

  $('#btnproficiencyAddRow').click(function() {
    rowsEnter2 = $('#bodytblForm11 tr').length;
    // if (rowsEnter2 < 1) {
    //   alert("Row number must be minimum 1.")
    //   return;
    // }
    Insaetproficiency(rowsEnter2);
    //addDatePicker();
  });




  var srNoGlobal=0;
  var inclp = 0;

  function Insaetproficiency(count) {
    srNoGlobal = $('#bodytblForm11 tr').length+1;
    var tbody = $('#bodytblForm11');
    var lastRow = $('#bodytblForm11 tr:last');
    var cloneRow = null;
    var inclp = count;
    // for (i = 1; i <= count; i++) {
    //   inclp++
    cloneRow = lastRow.clone();
    var tableData2 = '<tr>'

    + '<td><select class="form-control" id="syslanguage'+inclp+'"   name="syslanguage['+inclp+']" required="required">'
    + '<option value="">Select </option>'
    <?php foreach ($syslanguage as $key => $value): ?>
      + '<option value=<?php echo $value->lang_cd;?>><?php echo $value->lang_name;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'
    + '<select class="form-control" name="speak['+inclp+'] id="speak'+inclp+'"required="required">'
    + '<option value="">Select </option>'
    <?php foreach ($sysspeak as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'

    + '<select class="form-control" id="read'+inclp+'" name="read['+inclp+']" required="required">'
    + '<option value="">Select </option>'
    <?php foreach ($sysread as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'

    + '<select class="form-control" id="write'+inclp+'" name="write['+inclp+']" required="required">'
    + '<option value="">Select </option>'
    <?php foreach ($syswrite as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
    <?php endforeach ?>
    + '</select> </td>'
    + '</tr>';
    adddatepicker();
    $("#bodytblForm11").append(tableData2)
    
    //}

  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnworkexperienceRemoveRow").click(function() {
    if($('#tblForm12 tr').length-3>1)
      $('#bodytblForm12 tr:last').remove()
  });

  $('#btnworkexperienceAddRow').click(function() {
    rowsEnter2 = $('#bodytblForm12 tr').length
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insertworkexperience(rowsEnter2);
    //addDatePicker();
  });


  var srNoGlobal=0;
  var incwe = 0;

  function Insertworkexperience(count) {
    srNoGlobal = $('#bodytblForm12 tr').length+1;
    var tbody = $('#bodytblForm12');
    var lastRow = $('#bodytblForm12 tr:last');
    var cloneRow = null;
    var incwe = count
    // for (i = 1; i <= count; i++) {
    //   incwe++
    
    cloneRow = lastRow.clone();
    var tableData2 = '<tr>'
    + '<td>'
    + '<input type="text" name="orgname['+incwe+']" id="orgname_'+incwe+'" data-toggle="tooltip" value="" class="form-control alphabateonly" maxlengt="100" data-original-title="" required="required" title="">'
    
    + '</td><td>'
    + '<input type="text" name="designation['+incwe+']" id="designation" data-toggle="tooltip" required="required" value="" class="form-control" maxlengt="50" data-original-title="" title="">'
    + '</td><td>'
    + '<textarea name="descriptionofassignment['+incwe+']" data-toggle="tooltip" maxlengt="250" id="descriptionofassignment_'+incwe+'" class="form-control alphabateonly"  required="required" > </textarea></td><td>'
    

    + ' <div class="col-lg-12">'
    + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">'
    + '    <input type="text"  name="work_experience_fromdate['+incwe+']"  onclick="return workexchangedatepicker('+incwe+')" data-toggle="tooltip" id="work_experience_fromdate_'+incwe+'" value="" class="form-control datepicker" required="required" value="">'
    + ' </div>' 
    + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">'
    + ' <input type="text" name="work_experience_todate['+incwe+']" onclick="return workexchangedatepicker('+incwe+')"  data-toggle="tooltip" id="work_experience_todate_'+incwe+'" class="form-control datepicker" value="" required="required"></div> </div></td><td>'
    + '<input type="text" maxlengt="50" name="palceofposting['+incwe+']" data-toggle="tooltip" id="palceofposting_'+incwe+'" value="" class="form-control alphabateonly" data-original-title="" required="required" title="">'
    + '</td><td>'
    + '<input type="text" name="last_salary_drawn['+incwe+']" maxlengt="10" data-toggle="tooltip" id="last_salary_drawn_'+incwe+'" value="" class="form-control txtNumeric" data-original-title="" required="required" title="">'
    + ' </td>'
    + '</tr>';  
    adddatepicker();
    $("#bodytblForm12").append(tableData2)
    workexchangedatepicker(incwe);
   // }

 }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-2>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertIdentityRows(rowsIdentityEnter);
    //addDatePicker();
  });

  
  var srNoIdentity=0;
  var incr = 0;
  
  function insertIdentityRows(count) {
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      incr++
      cloneRow = lastRow.clone();
      var tableDataIdentity = '<tr>'
      + ' <td>'
      + '<select class="form-control" id="identityname'+incr+'" name="identityname['+incr+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysidentity as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
      <?php endforeach ?>
      + '</select>'

      + ' <td><input type="text" id="identitynumber'+incr+'" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td>'
      + '</tr>';
      adddatepicker();
      $("#tbodyForm13").append(tableDataIdentity)
      
    }

  }
</script>


<script type="text/javascript">
  $("#btnGapReasonRemoveRow").click(function() {
    if($('#tblForm15 tr').length-2>1)
      $('#bodytblForm15 tr:last').remove()
  });

  $('#btnGapReasonAddRow').click(function() {
    rowsEnter1 = $('#bodytblForm15 tr').length;
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    InsGapReason(rowsEnter1);
    //addDatePicker();
  });
  var  counttraining = $("#count_te").val();
  //alert(counttraining);

  if(counttraining==0){
   var srNoGlobal=0;
   var inctg = 0;
 }else{
   var srNoGlobal= counttraining;
   var inctg = counttraining;
   
 }

 function InsGapReason(count) {
  srNoGlobal = $('#bodytblForm15 tr').length+1;
  var tbody = $('#bodytblForm15');
  var lastRow = $('#bodytblForm15 tr:last');
  var cloneRow = null;
  var inctg = count;
 // alert(count);
 // for (i = 0; i < count; i++) {
  //   if(counttraining==0){
  //  inctg++;
  // }

  cloneRow = lastRow.clone();
  var tableData15 = '<tr>'
  + '<td><input type="text" class="form-control datepicker" id="gapfromdate_'+inctg+'" '+
  'name="gapfromdate['+inctg+']" onclick="return changedatepicker(inctg)" value="" required="required"/>'
  + '</td><td>'
  + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="gaptodate_'+inctg+'" name="gaptodate['+inctg+']" value="" required="required" />'
  + '</td><td>' 
  + '<textarea class="form-control alphabateonly"  maxlength="250" name="gapreason['+inctg+']" id="gapreason_'+inctg+'" title="Gap Reason" placeholder="Enter gap reason" required="required" ></textarea>'
  + '</td>'
  + '</tr>';
  adddatepicker();
  $("#bodytblForm15").append(tableData15)
  changegapyeardatepicker(inctg);
 //}

}
  //insertRows();
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit")..prop('disabled', true);

  });
</script>  




<script type="text/javascript">
  $(document).ready(function(){

    decimalData();
    adddatepicker();
    changegapyeardatepicker(inctg);
    changedatepicker(inctg);
    workexchangedatepicker(incwe);
    $('[data-toggle="tooltip"]').tooltip();   


    $(function () {
      $('#presentcity').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
          e.preventDefault();
        } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
          }
        }
      });
    });
    $(function () {
      $('#permanentcity').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
          e.preventDefault();
        } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
          }
        }
      });
    });

  });

  $(document).ready(function(){

    $(".alphabateonly").keypress(function (e){
      var code =e.keyCode || e.which;
      if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46)  
      {
       alert("Only alphabates are allowed");
       return false;
     }
   });


    $("#filladdress").on("click", function(){

     if (this.checked) { 
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());

    }
    else {
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

  });

  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});



    $('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
        e.preventDefault();
        $('.error').show();
        $('.error').text('Please Enter Alphabate');
        return false;
      }
    });
  }



</script>

<script type="text/javascript">
 $(document).ready(function(){
  $("#presentstateid").load(function(){
     //alert('fdsfds');
     $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
     .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);
      
    })
     .fail(function() {
      console.log("error");
    })
     .always(function() {
      console.log("complete");
    });
     
   });

  $("#permanentstateid").load(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);
      
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });
  
});     

</script>


<script type="text/javascript">
 $(document).ready(function(){
  $("#presentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>

<script type="text/javascript">
  $(document).ready(function() {
    var $submit = $('#btnsubmit');
    $submit.prop('disabled', 'true');
    // $submit.prop('disabled', true);
    // $('input[type="text"]').on('input change', function() { //'input change keyup paste'
    //     $submit.prop('disabled', !$(this).val().length);
    // });
  });


  function adddatepicker(){

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
    }
  });

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#work_experience_fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#gapfromdate").datepicker( "option", "minDate", selectedDate );

    }
  });
 }


 function changedatepicker(inctg){

   $("#fromdate_"+inctg).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
         // alert(selectedDate);
         jQuery("#todate_"+inctg).datepicker("option", "minDate", selectedDate);
       }
     });

   $("#todate_"+inctg).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#fromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
    }
  });
 }


 function workexchangedatepicker(incwe){

   $("#work_experience_fromdate_"+incwe).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
      jQuery("#work_experience_todate_"+incwe).datepicker("option", "minDate", selectedDate);
    }
  });

   $("#work_experience_todate_"+incwe).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#work_experience_fromdate_"+incwe).datepicker("option", "maxDate", selectedDate);
    }
  });

 }



 function changegapyeardatepicker(inctg){

   $("#gapfromdate_"+inctg).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#gaptodate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
      });

   $("#gaptodate_"+inctg).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#gapfromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
    }
  });
 }
</script>
<script>
  function checkemail() {
    var email=$("#emailid").val();

    $.ajax({
      data:{email:email},
      url: '<?php echo site_url(); ?>Ajax/email_exists/',

      type: 'POST',

    })
    .done(function(data) {
      if(data>0)
      {
        $("#email_error").text("email id allready exist, please choose other email id");
        $("#emailid").val('');
        $("#emailid").focusin();


      }
      else
      {
        $("#email_error").text(" ");
      }



    })




  }
  function email_focus()
  {
    var email=$("#emailid").val();
      //alert("data="+email);
      if(email=='')
      {
        $("#emailid").focus();
      }
    }
  </script>

