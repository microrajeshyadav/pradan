<style>
textarea {
  resize: none;
}
thead>tr:hover
{
  border-bottom: solid 1px red;
}   

label{

  font-weight: bold;
  font-family: 
}
.bg-lightgreen
{
  background-color: #85a4a5;
}
</style>



<style type="text/css" media="screen">
.error{
  color: red;
}  
</style>
<br/>
<script type="text/javascript">
 $("#basicinfo").find("input,textarea,select").attr("autocomplete", "off");
</script>
<section class="content">

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');
  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid" id="tblForm09" style="font-family: 'Oxygen' !important;" >
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Registration for <?php echo $category; ?></h4>

             <div class="col-md-12 text-right" style="color: red">
              * Denotes Required Field 
            </div>

          </div>
          <hr class="colorgraph">
        </div>
        <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
          <div class="panel-body">

            <input type="hidden" name="campusid" id="campusid" value="<?php echo $campusdecodeid; ?>">
            <input type="hidden" name="categoryid" id="categoryid" value="<?php  echo $campuscatid; ?>">
            <input type="hidden" name="campusintimationid" id="campusintimationid" value="<?php  echo $campusintimationid; ?>">
            <input type="hidden" name="campustype" id="campustype" value="off">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <table id="tbltrainingexposure" class="table table-bordered">
                <thead>
                 <tr class="bg-light">
                  <th colspan="3" class="bg-lightgreen text-white">Personal Infomation</th>
                </tr> 
              </thead>
              <tbody>
                <tr>
                  <td colspan="3">
                    <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                       <div class="form-group">
                        <div class="form-line">
                          <label for="Name">Candidate's Name <span style="color: red;">*</span></label>
                          <?php if ($method=='add') {?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control alphabateonly" value="<?php echo set_value('candidatefirstname');?>" placeholder="Enter Candidate's First Name" required="required">
                          <?php }else{ ?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control alphabateonly" value="" placeholder="Enter Candidate's Candidate's First Name" required="required">
                          <?php  } ?>
                        </div>
                        <?php echo form_error("candidatefirstname");?>
                      </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                      <label for="Name">Middle Name<span> &nbsp;</span></label>
                      <?php if ($method=='add') {?>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate's Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control alphabateonly" value="<?php echo set_value('candidatemiddlename');?>" placeholder="Enter Candidate's Middle Name" >
                      <?php }else{ ?>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control alphabateonly" value="" placeholder="Enter Candidate's Middle Name" >
                      <?php  } ?>
                      <?php echo form_error("candidatemiddlename");?>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">

                      <label for="Name">Last Name<span style="color: red;">*</span></label>
                      <?php if ($method=='add') {?>
                      <input type="text"  minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Candidate's Last Name " value="<?php echo set_value('candidatelastname');?>" required="required" >
                      <?php }else{ ?>
                      <input type="text"  minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Candidate's Last Name " value="" required="required" >
                      <?php } ?>
                      <?php echo form_error("candidatelastname");?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                      <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
                      <?php if ($method=='add') {?>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control alphabateonly" value="<?php echo set_value('motherfirstname');?>" placeholder="Enter Mother's First Name" required="required" >
                      <?php }else{ ?>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control alphabateonly" value="" placeholder="Enter Mother's First Name" required="required" >
                      <?php } ?>
                      <?php echo form_error("motherfirstname");?>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                      <label for="Name">Middle Name<span>&nbsp;</span> </label>
                      <?php if ($method=='add') {?>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control alphabateonly" value="<?php echo set_value('mothermiddlename');?>" placeholder="Enter Mother's Middle Name" >
                      <?php }else{ ?>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control alphabateonly" value="<?php echo set_value('mothermiddlename');?>" placeholder="Enter Mother's Middle Name" >
                      <?php } ?>
                      <?php echo form_error("mothermiddlename");?>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                      <label for="Name">Last Name<span style="color: red;">*</span></label>
                      <?php if ($method=='add') {?>
                      <input type="text" minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Mother's Last Name " value="<?php echo set_value('motherlastname');?>" required="required">
                      <?php }else{ ?>
                      <input type="text" minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Mother's Last Name " value="" required="required">
                      <?php } ?>
                      <?php echo form_error("motherlastname");?>
                    </div>
                  </div>

                  <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                    <label for="Name">Father's Name <span style="color: red;" >*</span></label>
                    <?php if ($method=='add') {?>
                    <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control alphabateonly" value="<?php echo set_value('fatherfirstname');?>" placeholder="Enter Father's First Name" required="required" >
                    <?php } else{?>
                    <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control alphabateonly" value="" placeholder="Enter Father's First Name" required="required" >
                    <?php } ?>
                    <?php echo form_error("motherlastname");?>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                    <label for="Name">Middle Name<span>&nbsp;</span></label>
                    <?php if ($method=='add') {?>
                    <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control alphabateonly" value="<?php echo set_value('fathermiddlename');?>" placeholder="Enter Father's Middle Name">
                    <?php }else{ ?>
                    <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control alphabateonly" value="" placeholder="Enter Father's Middle Name">
                    <?php } ?>
                    <?php echo form_error("fathermiddlename");?>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
                    <label for="Name">Last Name</label> <span style="color: red;">*&nbsp;</span>
                    <?php if ($method=='add') {?>
                    <input type="text" class="form-control alphabateonly" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Father's Last Name " value="<?php echo set_value('fatherlastname');?>" required="required">
                    <?php }else{ ?>
                    <input type="text" class="form-control alphabateonly" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Father's Last Name " value="" required="required">
                    <?php } ?>
                    <?php echo form_error("fatherlastname");?>
                  </div>
                </div>

                <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Gender <span style="color: red;" >*</span></label>

                  <?php 
                  $options = array("" => "Select", "1" => "Male", "2" => "Female","3" => "Other");
                  echo form_dropdown('gender', $options, set_value('gender'), 'class="form-control" required="required"');
                  ?>
                  <?php echo form_error("gender");?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Nationality  <span style="color: red;" >*</span> </label>
                  <?php 
                  $options = array("" => "Select", "indian" => "Indian", "other" => "Other");
                  echo form_dropdown('nationality', $options,set_value('nationality'), 'class="form-control" required="required" ');
                  ?>
                  <?php echo form_error("nationality");?>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Marital Status  <span style="color: red;" >*</span></label>
                  <?php 
                  $options = array("" => "Select", "1" => "Single", "2" => "Married", "3" => "Divorced","4" => "Widow","5" => "Separated");
                  echo form_dropdown('maritalstatus', $options, set_value('maritalstatus'), 'class="form-control" required="required" ');
                  ?>
                  <?php echo form_error("maritalstatus");?>
                </div>
              </div>

              <?php   
              $options = array("" => "Select", "1" => "Below 50,000", "2" => "50,001-200,000", "3" => "200,001-500,000","4" => "Above 500,000");
              ?>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">


                  <label for="Name">Annual income of parents <span style="color: red;" >*</span></label>

                  <?php 
                  $options = array("" => "Select", "1" => "Below 50,000", "2" => "50,001-200,000", "3" => "200,001-500,000","4" => "Above 500,000");
                  echo form_dropdown('annual_income', $options, set_value('annual_income'), 'class="form-control" required="required" id="annual_income" ');
                  ?>
                  <?php echo form_error("annual_income");?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">No. of male sibling  <span style="color: red;" >*</span></label>
                  <input type="text"  maxlength="2" data-toggle="tooltip" title="Male sibling!" name="no_of_male_sibling" id="no_of_male_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo set_value('no_of_male_sibling');?>" placeholder="Enter sibling" required="required">
                  <?php echo form_error("no_of_male_sibling");?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">No. of female sibling  <span style="color: red;" >*</span></label>
                  <input type="text"  maxlength="2" data-toggle="tooltip" title="Female sibling!" name="no_of_female_sibling" id="no_of_female_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo set_value('no_of_female_sibling');?>" placeholder="Enter sibling " required="required">
                  <?php echo form_error("no_of_female_sibling");?>
                </div>
              </div>

              <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                <input type="text" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" name="dateofbirth" id="dateofbirth" placeholder="Enter Date Of Birth " value="<?php echo set_value('dateofbirth');?>" required="required" >
                <?php echo form_error("dateofbirth");?>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>

                <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo set_value('emailid');?>" required="required"  onblur="checkemail()" onchange="checkemail()">
                <?php echo form_error("emailid");?>
                <span style="color: red;" id="email_error"></span>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="<?php echo set_value('mobile');?>" placeholder="Enter Mobile No" required="required" onkeypress="email_focus()" onchange="email_focus()" >
                <?php echo form_error("mobile");?>
              </div>
            </div>


          </td>
        </tr>
      </tbody>
    </table>
  </div>


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table table-bordered" >
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white"> 
         Communication Address 

       </th>
     </tr> 
     <tr>
      <th class="text-center" colspan="4" style="background-color: #e1e1e1; vertical-align: top; text-align: center; "> <i class="fa fa-address-card" style="color: #F29005;"></i> &nbsp;Present Mailing Address</th>
      <th class="text-center" colspan="4" style="background-color: #d0d0d0; vertical-align: top; text-align: center;"> <i class="fa fa-address-card" style="color: #17A2B8;"></i> &nbsp;Permanent Mailing Address<br>

        <div>
          <input type="checkbox" class="form-check-input" id="filladdress"> 
          <label class="form-check-label" for="filladdress"><b>same as Mailing Address</b></label>
        </div>

      </th>
    </tr>  
  </thead>
  <tbody>
    <tr>
      <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
      <td><input type="text" name="presenthno" id="presenthno" maxlength="50"class="form-control" 
        value="<?php echo set_value('presenthno');?>" placeholder="Enter H.No" required="required"></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><textarea name="presentstreet" id="presentstreet" maxlength="150" class="form-control" 
          placeholder="Enter H.No/Street" required="required"><?php echo set_value('presentstreet');?></textarea></td>
          <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
          <td><input type="text" name="permanenthno" id="permanenthno"  maxlength="50" class="form-control" value="<?php echo set_value('permanenthno');?>"  placeholder="Enter H.No/Street" required="required" ></td>
          <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
          <td><textarea name="permanentstreet" id="permanentstreet" maxlength="150" class="form-control"   placeholder="Enter H.No/Street" required="required"><?php echo set_value('permanentstreet');?></textarea></td>
        </tr>
        <tr>
          <td><label for="Name">City<span style="color: red;" >*</span></label> </td>
          <td><input type="text" name="presentcity" id="presentcity" maxlength="50" class="form-control txtOnly" placeholder="Enter City" value="<?php echo set_value('presentcity');?>"  required="required"></td>

          <td><label for="Name">State<span style="color: red;" >*</span></label> </td>
          <td><?php 
          $options = array('' => 'Select State');
          foreach($statedetails as$key => $value) {
            $options[$value->statecode] = $value->name;
          }
          echo form_dropdown('presentstateid', $options, set_value('presentstateid'), 'class="form-control" id="presentstateid" ');
          ?>
          <?php echo form_error("presentstateid");?>
        </td>

        <td><label for="Name">City<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentcity" id="permanentcity" maxlength="50" placeholder="Enter City" class="form-control txtOnly" value="<?php echo set_value('permanentcity');?>" required="required" ></td>
        <td><label for="Name">State<span style="color: red;" >*</span></label></td>
        <td> <?php 

        $options = array('' => 'Select State');
        foreach($statedetails as$key => $value) {
          $options[$value->statecode] = $value->name;
        }
        echo form_dropdown('permanentstateid', $options, set_value('permanentstateid'), 'class="form-control" id="permanentstateid"');
        ?>
        <?php echo form_error("permanentstateid");?></td>
      </tr>
      <tr>
        <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td> 
          <?php 
          $options = array('' => 'Select District');
          foreach($getdistrict as$key => $value) {
            $options[$value->districtid] = $value->name;
          }
          echo form_dropdown('presentdistrict', $options, set_value('presentdistrict'), 'class="form-control" id="presentdistrict"');
          ?>
          <?php echo form_error("presentdistrict");?>

        <!--  <select name="presentdistrict" id="presentdistrict" required="required" class="form-control" >
           <option value="">Select District </option>
         </select> -->
       </td>
       <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
       <td><input type="text" name="presentpincode" maxlength="6" id="presentpincode" class="form-control txtNumeric" value="<?php echo set_value('presentpincode');?>" required="required" placeholder="Enter PinCode"></td>
       <td><label for="Name">District<span style="color: red;" >*</span></label></td>
       <td>
        <?php 
        $options = array('' => 'Select District');
        foreach($getdistrict1 as$key => $value1) {
          $options[$value1->districtid] = $value1->name;
        }
        echo form_dropdown('permanentdistrict', $options, set_value('permanentdistrict'), 'class="form-control" id="permanentdistrict"');
        ?>
        <?php echo form_error("permanentdistrict");?>
         <!--<select name="permanentdistrict" id="permanentdistrict" required="required" class="form-control" >
           <option value="">Select District </option>
         </select>  -->
       </td>

       <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
       <td><input type="text" name="permanentpincode"  maxlength="6" id="permanentpincode" class="form-control txtNumeric" value="<?php echo set_value('permanentpincode');?>" required="required" placeholder="Enter PinCode"></td>
     </tr>
   </tbody>
 </table>
</div>  


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">

  <table id="tbledubackground" class="table table-bordered">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8" >Educational Background</th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate <span style="color: red;" >*</span></th>

      <th class="text-center" style="vertical-align: top;">Year </th>
      <th class="text-center" style="vertical-align: top;">Board/ University</th>
      <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
      <th class="text-center" style="vertical-align: top;">Place</th>
      <th class="text-center" style="vertical-align: top;">Specialisation</th>
      <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td style="padding-left: 23px;">
        <input type="hidden"  name="tenthstream" value="<?php echo '1'; ?>"><b>10th</b></td>
        <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" required title="Year !" 
          id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" style="min-width: 20%;"  value="<?php echo set_value("10thpassingyear");?>" >
          <?php echo form_error("10thpassingyear");?></td>

          <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
            id="10thboarduniversity" name="10thboarduniversity" required placeholder="Enter Board/ University" value="<?php echo set_value("10thboarduniversity");?>" >
            <?php echo form_error("10thboarduniversity");?></td>
            <td>  <input type="text" class="form-control alphabateonly" required data-toggle="tooltip" title="School/ College/ Institute !" 
              id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("10thschoolcollegeinstitute");?>" >
              <?php echo form_error("10thschoolcollegeinstitute");?></td>
              <td><input type="text" class="form-control alphabateonly" required data-toggle="tooltip" title="Place !" 
                id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo set_value("10thplace");?>" >
                <?php echo form_error("10thplace");?></td>

                <td><input type="text" class="form-control alphabateonly" required data-toggle="tooltip" title="Specialisation !" 
                  id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("10thspecialisation");?>" >
                  <?php echo form_error("10thspecialisation");?></td>
                  <td><input type="text" class="text-right form-control txtNumeric" maxlength="5" minlength="2" required  data-toggle="tooltip" title="Percentage !"
                    id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo set_value("10thpercentage");?>"  >
                    <?php echo form_error("10thpercentage");?></td>
                  </tr>
                  <tr>
                    <td>
                     <select name="twelth" id="twelth" class="form-control"  required="required">
                      <option value="2">12th</option>
                      <option value="10">Diploma</option>
                    </select>
                  </td>
                  <td><input type="text" class="form-control txtNumeric" required data-toggle="tooltip" title="Year !" 
                    id="12thpassingyear" name="12thpassingyear" placeholder="Enter Year" value="<?php echo set_value("12thpassingyear");?>" >
                    <?php echo form_error("12thpassingyear");?></td>

                    <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" required title="Board/ University !" 
                      id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("12thboarduniversity");?>" >
                      <?php echo form_error("12thboarduniversity");?></td>

                      <td>  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" required title="School/ College/ Institute !" 
                        id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("12thschoolcollegeinstitute");?>" >
                        <?php echo form_error("12thschoolcollegeinstitute");?></td>

                        <td>
                          <input type="text" class="form-control alphabateonly" data-toggle="tooltip" required title="Place !" 
                          id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo set_value("12thplace");?>" >
                          <?php echo form_error("12thplace");?></td>

                          <td><input type="text" class="form-control alphabateonly" required data-toggle="tooltip" title="Specialisation !" 
                            id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("12thspecialisation");?>" >
                            <?php echo form_error("12thspecialisation");?></td>


                            <td><input type="text" class="text-right form-control txtNumeric" maxlength="5" minlength="2" 
                              id="12thpercentage" required name="12thpercentage" placeholder="Percentage" value="<?php echo set_value("12thpercentage");?>" >
                              <?php echo form_error("12thpercentage");?></td>
                            </tr>
                            <tr>
                              <td>
                                <?php
                                $options = array('' => 'Select');
                                foreach($ugeducationdetails as $key => $value) {
                                  $options[$value->pgname] = $value->pgname;
                                }
                                echo form_dropdown('ugstream', $options, set_value('ugstream'), 'class="form-control"  data-toggle="tooltip" title="Up Stream !" required="required"');
                                ?>
                                <?php echo form_error("ugstream");?>
                              </td>

                              <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                id="ugpassingyear" name="ugpassingyear"  required="required"placeholder="Enter Year" value="<?php echo set_value("ugpassingyear");?>" >
                                <?php echo form_error("ugpassingyear");?></td>

                                <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                  id="ugboarduniversity" name="ugboarduniversity" required="required" placeholder="Enter Board/ University" value="<?php echo set_value("ugboarduniversity");?>" >
                                  <?php echo form_error("ugboarduniversity");?></td>

                                  <td>
                                    <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="School/ College/ Institute !" 
                                    id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute " required="required" value="<?php echo set_value("ugschoolcollegeinstitute");?>" >
                                    <?php echo form_error("ugschoolcollegeinstitute");?>
                                  </td>

                                  <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !"  required="required"
                                    id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo set_value("ugplace");?>" >
                                    <?php echo form_error("ugplace");?></td>
                                    <td>
                                      <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Specialisation !" required="required"
                                      id="ugspecialisation" name="ugspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("ugspecialisation");?>" >
                                    </td>

                                    <td><input type="text" class="text-right form-control txtNumeric" maxlength="5" minlength="2"  required="required"
                                      id="ugpercentage" name="ugpercentage" placeholder="Percentage" value="<?php echo set_value("ugpercentage");?>" >
                                      <?php echo form_error("ugpercentage");?></td>
                                    </tr>
                                    <tr>
                                      <td>
                                       <?php
                                       $options = array('' => 'Select');
                                       foreach($pgeducationdetails as $key => $value) {
                                        $options[$value->pgname] = $value->pgname;
                                      }
                                      echo form_dropdown('pgupstream', $options, set_value('pgupstream'), 'class="form-control" id="pgupstream"');
                                      ?>
                                      <?php echo form_error("pgupstream");?> 
                                    </td>

                                    <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                      id="pgpassingyear" name="pgpassingyear" placeholder="Enter Year" value="<?php echo set_value("pgpassingyear");?>" >
                                    </td>
                                    <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                      id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("pgboarduniversity");?>" >
                                    </td>

                                    <td>  
                                      <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                      id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>" >
                                      <?php echo form_error("pgschoolcollegeinstitute");?>
                                    </td>

                                    <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !" 
                                      id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo set_value("pgplace");?>" >
                                    </td>

                                    <td>
                                      <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Specialisation  !" 
                                      id="pgspecialisation" name="pgspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("pgspecialisation");?>" >  </td>

                                      <td><input type="text" class="text-right form-control txtNumeric" maxlength="5" minlength="2" 
                                        id="pgpercentage" name="pgpercentage" placeholder="Percentage" value="<?php echo set_value("pgpercentage");?>" >
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>
                                       <div class="form-check">

                                        <input type="checkbox" class="filled-in" id="other"  value="Other" name="other_specify_degree">
                                        <label class="form-check-label" for="other"><b>Other (specify)</b></label>
                                        <div id="other_other_specify_degree" style="display: none;">
                                          <input type="text" name="specify_degree" id="specify_degree" value="<?php echo set_value("specify_degree");?>" class="form-control">
                                        </div>
                                      </div>
                                    </td>

                                    <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                      id="otherpassingyear" name="otherpassingyear" placeholder="Enter Year" value="<?php echo set_value("otherpassingyear");?>" disabled>
                                    </td>
                                    <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
                                      id="otherboarduniversity" name="otherboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("otherboarduniversity");?>" disabled>
                                    </td>
                                    <td>  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="School/ College/ Institute !" 
                                      id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>"  disabled>
                                    </td>

                                    <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Place !" 
                                      id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo set_value("otherplace");?>" disabled >
                                    </td>

                                    <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Specialisation !" 
                                      id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("otherspecialisation");?>" disabled>
                                    </td>
                                    <td><input type="text" class="text-right form-control txtNumeric" maxlength="5" minlength="2" 
                                      id="otherpercentage" name="otherpercentage" placeholder="Percentage" value="<?php echo set_value("otherpercentage");?>" disabled>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>



                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                             <table id="tblForm15" class="table table-bordered">
                              <thead>
                                <tr class="bg-lightgreen text-white">
                                  <th colspan="3" >

                                    <div class="col-lg-6 text-left"> Gap Year </div>

                                    <div class="col-lg-6 text-right ">
                                     <button type="button" id="btnGapReasonRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                                     <button type="button" id="btnGapReasonAddRow" class="btn btn-warning btn-xs">Add</button>
                                   </div>
                                 </th>
                               </tr> 
                               <tr>
                                <th class="text-center" style="vertical-align: top;"> From Date</th>
                                <th class="text-center" style="vertical-align: top;">To Date</th>
                                <th>Reason</th>
                              </tr> 
                            </thead>
                            <tbody id="bodytblForm15">
                              <tr id="bodytblForm15">
                               <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
                                id="gapfromdate" name="gapfromdate[]" placeholder="From Date" style="min-width: 20%;"  value="<?php echo set_value('gapfromdate[0]');?>"  ></td>
                                <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                                  id="gaptodate" name="gaptodate[]" placeholder="To Date" style="min-width: 20%;" value="<?php echo set_value('gaptodate[0]');?>"></td>
                                  <td>
                                    <textarea type="text" rows="2" cols="5" name="gapreason[]" id="gapreason" class="form-control alphabateonly" data-toggle="tooltip"  maxlength="250" title="Gap Reason" placeholder="Enter gap reason"><?php echo set_value('gapreason[0]');?></textarea>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>

                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                           <table id="tblForm10" class="table table-bordered">
                            <thead>
                              <tr class="bg-lightgreen text-white">
                                <th colspan="8" >

                                 <div class="col-lg-12">
                                  <div class="col-lg-6 text-left"> Training  Exposure (if any)</div>
                                  <div class="col-lg-6 text-right ">
                                   <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                                   <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
                                 </div>
                               </div>

                             </th>
                           </tr> 
                           <tr>

                            <th class="text-center" style="vertical-align: top;">Nature of Training </th>
                            <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
                            <th class="text-center" style="vertical-align: top;">From Date</th>
                            <th class="text-center" style="vertical-align: top;">To Date</th>
                          </tr> 
                        </thead>
                        <tbody id="bodytblForm10">
                          <tr id="bodytblForm10">
                            <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="Nature of Training !" minlength="5" maxlength="50"
                              id="natureoftraining"  maxlength="150" name="natureoftraining[]" placeholder="Enter Nature of Training" style="min-width: 20%;"  
                              value="<?php echo set_value('natureoftraining[0]');?>"  ></td>
                              <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="Organizing Agency !" 
                                id="organizingagency" name="organizingagency[]"  placeholder="Organizing Agency" style="min-width: 20%;" value="<?php echo set_value('organizingagency[0]');?>" ></td>
                                <td><input type="text" class="form-control datepicker"   data-toggle="tooltip"  title="From Date !" id="fromdate" name="fromdate[]" placeholder="From Date" style="min-width: 20%;" value="<?php echo set_value('fromdate[0]');?>"  ></td>
                                <td><input type="text"
                                 class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                                 id="todate" name="todate[]" placeholder="To Date" style="min-width: 20%;"  value="<?php echo set_value('todate[0]');?>"></td>
                               </tr>
                             </tbody>
                           </table>
                         </div>

                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                           <table id="tblForm11"  class="table table-bordered">
                            <thead>
                              <tr class="bg-lightgreen text-white">
                                <th colspan="8" > 
                                 <div class="col-lg-12">
                                  <div class="col-lg-6 text-left"> Skill/Proficiency </div>
                                  <div class="col-lg-6 text-right ">
                                   <button type="button" id="btnproficiencyRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                                   <button type="button" id="btnproficiencyAddRow" class="btn btn-warning btn-xs">Add</button>
                                 </div>
                               </div>
                             </th>
                           </tr> 
                           <tr>

                            <th class="text-center" style="vertical-align: top;">Language <span style="color: red;">*</span></th>
                            <th class="text-center" style="vertical-align: top;">Speak <span style="color: red;">*</span></th>
                            <th class="text-center" style="vertical-align: top;">Read <span style="color: red;">*</span></th>
                            <th class="text-center" style="vertical-align: top;">Write <span style="color: red;">*</span></th>
                          </tr> 
                        </thead>
                        <tbody id="bodytblForm11">

                          <tr id="bodytblForm11">
                            <td>  

                              <?php
                              $options = array();
                              foreach($syslanguage as $key => $value) {
                                if($value->lang_cd == 2){
                                  $options[$value->lang_cd] = $value->lang_name;
                                }
                              }
                              $batch = $syslanguage[0]->lang_cd;
                              echo form_dropdown('syslanguage[]', $options, $batch, 'class="form-control" data-toggle="tooltip"  id="syslanguage_0" required title="Select language !"', set_value('syslanguage'));
                              ?>
                              <?php echo form_error("syslanguage");?></td>
                              <td>
                               <?php
                               $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                               echo form_dropdown('speak[]',$sysspeak,set_value('speak'),'class ="form-control" data-toggle="tooltip" required title="Select Speak !" ');
                               ?>
                               <?php echo form_error("speak");?>
                             </td>
                             <td><?php
                             $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                             echo form_dropdown('read[]',$sysread,set_value('read'),'class ="form-control" data-toggle="tooltip" required title="Select Read !" ');
                             ?>
                             <?php echo form_error("read");?></td>
                             <td><?php
                             $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                             echo form_dropdown('write[]',$syswrite,set_value('write'),'class ="form-control" data-toggle="tooltip" required title="Select Write !" ');
                             ?>
                             <?php echo form_error("write");?></td>
                           </tr>
                           <tr id="bodytblForm11">
                            <td>  

                              <?php
                              $options = array();
                              foreach($syslanguage as $key => $value) {
                                if($value->lang_cd == 3){
                                  $options[$value->lang_cd] = $value->lang_name;
                                }
                              }
                              $batch = $syslanguage[1]->lang_cd;
                              echo form_dropdown('syslanguage[]', $options, $batch, 'class="form-control" data-toggle="tooltip"  id="syslanguage_0" required title="Select language !"', set_value('syslanguage'));
                              ?>
                              <?php echo form_error("syslanguage");?></td>
                              <td>
                               <?php
                               $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                               echo form_dropdown('speak[]',$sysspeak,set_value('speak'),'class ="form-control" data-toggle="tooltip" required title="Select Speak !" ');
                               ?>
                               <?php echo form_error("speak");?>
                             </td>
                             <td><?php
                             $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                             echo form_dropdown('read[]',$sysread,set_value('read'),'class ="form-control" data-toggle="tooltip" required title="Select Read !" ');
                             ?>
                             <?php echo form_error("read");?></td>
                             <td><?php
                             $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                             echo form_dropdown('write[]',$syswrite,set_value('write'),'class ="form-control" data-toggle="tooltip" required title="Select Write !" ');
                             ?>
                             <?php echo form_error("write");?></td>
                           </tr>
                         </tbody>
                       </table>
                     </div>


                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
                       <table id="tbltrainingexposure"  class="table table-bordered">
                        <thead>
                          <tr class="bg-lightgreen text-white">
                            <th colspan="8">Subject(s) of Interest </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><textarea class="form-control alphabateonly" name="subjectinterest"  maxlength="1500" id="subjectinterest" cols="12" rows="5" data-toggle="tooltip" title="Enter Subject(s) of Interest !" ><?php echo set_value('subjectinterest');?></textarea></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                     <table id="tbltrainingexposure"  class="table table-bordered">
                      <thead>
                        <tr class="bg-lightgreen text-white">
                          <th colspan="8" >Achievements / Awards (if any) </th>
                        </tr> 
                      </thead>
                      <tbody>
                        <tr>
                          <td><textarea class="form-control alphabateonly" name="achievementawards" id="achievementawards" cols="12" rows="5"   maxlength="1500" data-toggle="tooltip" title="Enter Achievements /Awards (if any) !" ><?php echo set_value('achievementawards');?></textarea></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>


                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                   <table id="tblForm12"  class="table table-bordered">
                    <thead>
                      <tr class="bg-lightgreen text-white">
                        <th colspan="8" >
                         <div class="col-lg-12">
                          <div class="col-lg-6 text-left">Work Experience (if any) </div>
                          <div class="col-lg-6 text-right ">
                           <button type="button" id="btnworkexperienceRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                           <button type="button" id="btnworkexperienceAddRow" class="btn btn-warning btn-xs">Add</button>
                         </div>
                       </div>
                     </th>
                   </tr> 
                   <tr>
                    <th>Organization Name</th>
                    <th>Designation</th>
                    <th>Description of Assignment</th>
                    <th>Duration</th>
                    <th>Place of Posting</th>
                    <th>Last salary drawn (monthly)</th>
                  </tr> 
                  <tr>
                    <th colspan="3"></th>
                    <th colspan="1">
                      <div class="col-lg-12">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
                      </div></th>
                      <th colspan="3"></th>
                    </tr>
                  </thead>
                  <tbody id="bodytblForm12">
                    <tr id="bodytblForm12"> 
                      <td><input type="text" name="orgname[]" maxlengt="100" id="orgname" data-toggle="tooltip" value="<?php echo set_value('orgname[0]');?>" class="form-control alphabateonly" title="Enter Organization Name !"  placeholder="Organization Name ">
                        <span class=""><?php echo form_error("orgname");?></span></td>
                        <td><input type="text" name=designation[]" maxlengt="100" id="designation" data-toggle="tooltip" value="<?php echo set_value('designation[0]');?>" class="form-control alphabateonly" title="Enter Designation !"  placeholder=" Designation">
                          <span class=""><?php echo form_error("designation");?></span></td>
                          <td><textarea  maxlengt="250" name="descriptionofassignment[]" data-toggle="tooltip" id="descriptionofassignment" class="form-control alphabateonly" title="Enter Description of Assignment" placeholder=" Description of Assignment"><?php echo set_value('descriptionofassignment[0]');?></textarea><span class=""><?php echo form_error("descriptionofassignment");?></span></td>
                          <td>
                            <div class="col-lg-12">
                              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                                <input type="text" name="work_experience_fromdate[]" id="work_experience_fromdate" class="form-control datepicker" value="<?php echo set_value('work_experience_fromdate[0]');?>"  placeholder="from date">
                                <span class=""><?php echo form_error("work_experience_fromdate");?></span>
                              </div> 
                              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                <input type="text" name="work_experience_todate[]" id="work_experience_todate" class="form-control datepicker" value="<?php echo set_value('work_experience_todate[0]');?>" placeholder="To date">
                                <span class=""><?php echo form_error("work_experience_todate");?></span>
                              </div> 
                            </div>
                          </td>
                          <td><input type="text" maxlengt="50" name="palceofposting[]" data-toggle="tooltip" id="palceofposting" value="<?php echo set_value('palceofposting[0]');?>" class="form-control alphabateonly" title="Place of Posting[]" placeholder="Place of Posting"> <span class=""><?php echo form_error("palceofposting");?></span></td>
                          <td><input type="text" name=last_salary_drawn[]" maxlengt="100" id="last_salary_drawn" data-toggle="tooltip" value="<?php echo set_value('last_salary_drawn[0]');?>" class="form-control txtNumeric" title="Enter Last salary drawn !"  placeholder="last salary drawn">
                            <span class=""><?php echo form_error("last_salary_drawn");?></span></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
                     <table id="tbltrainingexposure"  class="table table-bordered">
                      <thead>
                        <tr class="bg-lightgreen text-white">
                          <th colspan="8" >Describe any assignment(s) of special interest undertaken by you </th>
                        </tr> 

                      </thead>
                      <tbody>
                        <tr>
                         <td><textarea class="form-control alphabateonly" data-toggle="tooltip"  maxlength="1500" name="any_assignment_of_special_interest" id="any_assignment_of_special_interest" cols="12" rows="5" title="Describe any Assignment(s) of special interest undertaken by you" placeholder="Describe any assignment(s) of special interest undertaken by you" ><?php echo set_value('any_assignment_of_special_interest');?></textarea></td>
                       </tr>
                     </tbody>
                   </table>
                 </div>

                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
                   <table id="tbltrainingexposure"  class="table table-bordered">
                    <thead>
                      <tr class="bg-lightgreen text-white">
                        <th colspan="8">Experience of Group and/or Social Activities </th>
                      </tr> 
                    </thead>
                    <tbody>
                      <tr>
                        <td><textarea class="form-control alphabateonly" data-toggle="tooltip"  maxlength="1500" name="experience_of_group_social_activities" id="experience_of_group_social_activities" cols="12" rows="5" title="Experience of Group and/or Social Activities " placeholder="Enter Experience of Group and/or Social Activities"><?php echo set_value('experience_of_group_social_activities');?></textarea></td>
                      </tr>
                    </tbody>
                  </table>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                 <table id="tbltrainingexposure"  class="table table-bordered">
                  <thead>
                    <tr class="bg-lightgreen text-white">
                      <td colspan="8" ><b>Have you taken part in PRADAN's selection process before?</b>
                        <span style="color:red"> *</span>
                      </td>
                    </tr> 
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                          <div class="form-check">
                            <input type="radio" class="form-check-input" id="yes" value="yes" name="pradan_selection_process_before" required="required">
                            <label class="form-check-label" for="yes"><b>Yes</b></label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" class="form-check-input" id="no" value="no"  name="pradan_selection_process_before"  required="required">
                            <label class="form-check-label" for="no"><b>No</b></label>

                          </div>

                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                          <input type="text" name="when" id="when" <?php echo set_value('when');?> class="form-control datepicker">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
                          <select name="where" id="where" class="form-control" >
                            <option value="">select</option>
                            <option value="1">ON Campus</option>
                            <option value="2">Off Campus</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <script type="text/javascript">
            // $(document).ready(function() {
             $("#when").prop('disabled','true'); 
             $("#where").prop('disabled','true');
             $("input[name$='pradan_selection_process_before']").click(function() {
              var selected = $(this).val();

              if (selected =='yes') {
                $("#when").removeAttr('disabled','false'); 
                $("#where").removeAttr('disabled','false');
                $("#when").prop('required', 'required');
                $("#where").prop('required', 'required');
              }else{
                $("#when").val('');
                $("#where").val('');
                $("#when").prop('disabled','true'); 
                $("#where").prop('disabled','true');
              }

            });

             
          </script>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

           <table id="tbltrainingexposure"  class="table table-bordered">
            <thead>
              <tr class="bg-lightgreen text-white">
                <td colspan="8" ><b>From where have you come to know about PRADAN?</b> <span style="color:red"> *</span>
                </td>
              </tr> 
            </thead>
            <tbody>
              <tr>
                <td>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                    <div class="form-check">
                      <input type="radio" class="form-check-input" id="campus_placement_cell" value="CampusPlacementCell" name="have_you_come_to_know" required="required">
                      <label class="form-check-label" for="campus_placement_cell"><b>Campus Placement Cell</b></label>
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
                   <!-- Material checked -->
                   <div class="form-check">
                    <input type="radio" class="form-check-input" id="university_professors" value="UniversityProfessors" name="have_you_come_to_know"  required="required" >
                    <label class="form-check-label" for="university_professors" required="required"><b>University Professors</b></label>
                  </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                 <!-- Material checked -->
                 <div class="form-check">
                  <input type="radio" class="form-check-input" id="campus_alumni" value="CampusAlumni" name="have_you_come_to_know" required="required" >
                  <label class="form-check-label" for="campus_alumni"><b>Campus Alumni</b></label>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
                <!-- Material checked -->
                <div class="form-check">
                  <input type="radio" class="form-check-input" id="friends" value="friends" name="have_you_come_to_know" required="required" >
                  <label class="form-check-label" for="friends"><b>Friends</b></label>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2"> 
               <!-- Material checked -->
               <div class="form-check">
                <input type="radio" class="form-check-input" id="knowother"  value="other1" name="have_you_come_to_know" required="required">
                <label class="form-check-label" for="other"><b>Other (specify)</b></label>
                <div id="other_have_you_come_to_know_specify" style="display: none;">
                  <input type="text" name="specify" id="specify" value="" class="form-control">
                </div>
              </div>
            </div>

          </td>
        </tr>
      </tbody>
    </table>
  </div>


  <script type="text/javascript">

   $("#other_have_you_come_to_know_specify").prop('disabled','true'); 
   
   $("input[name$='have_you_come_to_know']").click(function() {
    var selected = $(this).val();

    if (selected =='other1') {
      $("#other_have_you_come_to_know_specify").show();
      $("#other_have_you_come_to_know_specify").removeAttr('disabled','false');
      $("#specify").prop('required','true');  
    }else{
     $("#specify").prop('disabled','true'); 
     $("#specify").prop('required','false');
     $("#other_have_you_come_to_know_specify").hide();
   }

 });



   $("input[name='other_specify_degree']").change(function() {

    var test = $('#other').val();
      // alert(test);
      if ( $(this).is(':checked') ) {

        $("#other_other_specify_degree").show();
        $("#otherpassingyear").prop("disabled", false);
        $("#otherpassingyear").prop('required', 'required');

        $("#otherboarduniversity").prop("disabled", false);
        $("#otherboarduniversity").prop('required', 'required');

        $("#otherschoolcollegeinstitute").prop("disabled", false);
        $("#otherschoolcollegeinstitute").prop('required', 'required');

        $("#otherplace").prop("disabled", false);
        $("#otherplace").prop('required', 'required');




        $("#otherplace").prop('required', 'required');
        $("#otherplace").prop('required', 'required');

        $("#otherspecialisation").prop("disabled", false);
        $("#otherspecialisation").prop('required', 'required');

        $("#otherpercentage").prop("disabled", false);
        $("#otherpercentage").prop('required', 'required');
        $("#specify_degree").prop('required','required');
      } else {
       $("#other_other_specify_degree").hide();
       $("#otherpassingyear").prop("disabled", true);
       $("#otherboarduniversity").prop("disabled", true);
       $("#otherschoolcollegeinstitute").prop("disabled", true);
       $("#otherplace").prop("disabled", true);
       $("#otherspecialisation").prop("disabled", true);
       $("#otherpercentage").prop("disabled", true);
       $("#otherpassingyear").disabled();
       $("#specify").prop('disabled','true');
     }

   });
 </script>

</div>

</div>
<div class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" style="background-color: #ffffff;">
 

  <button type="reset" class="btn btn-warning  btn-sm m-t-10 waves-effect">Reset </button>
  <button type="submit" value="saveWorkExpSubmit" id="sendDataWorkExpButton" class="btn btn-success  btn-sm m-t-10 waves-effect" onclick="return ifanyvalidation()" onclick="checkemail()">Save </button>
  <a href="http://www.pradan.net/" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 

</div>
</form>
</div>
</div>
</section>

<script type="text/javascript">
 
  function ifanyvalidation(){

    var natureoftraining = $('#natureoftraining').val();
    var organizingagency = $('#organizingagency').val();
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var orgname = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var designation = $('#designation').val();
    var last_salary_drawn = $('#last_salary_drawn').val();
    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();

    var gapfromdate = $('#gapfromdate').val();
    var gaptodate = $('#gaptodate').val();
    var gapreason = $('#gapreason').val();

    var syslanguage = $('#syslanguage_0').val();
    var syslanguage1 = $('#syslanguage_1').val();

    var pgupstream = $('#pgupstream').val();
    var pgpassingyear = $('#pgpassingyear').val();
    var pgboarduniversity = $('#pgboarduniversity').val();
    var pgschoolcollegeinstitute = $('#pgschoolcollegeinstitute').val();
    var pgplace = $('#pgplace').val();
    var pgspecialisation = $('#pgspecialisation').val();
    var pgpercentage = $('#pgpercentage').val();

//rajat
var pg = [pgpassingyear,pgboarduniversity,pgschoolcollegeinstitute,pgplace,pgspecialisation,pgpercentage];
if ($("#when").val() != '')
    {
     var start= $("#when").val();
    var res = start.split("/");
    start =   new Date(res[1]+ '/'+ res[0] + '/' + res[2]);
    
     var end= new Date();
     
      days = (end - start) / (1000 * 60 * 60 * 24);
       
      if ((parseInt(days)+1)<=365)
      {
        alert("You can not apply, please try after 1 year of last selection process.");

        return false; 

      }
      
    }
    
if(jQuery.inArray('',pg)) {
  $("#pgupstream").attr("required", "true");
  $("#pgpassingyear").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}

if(pgupstream !="")
{
  $("#pgpassingyear").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}


if(pgpassingyear !='')
{
  $("#pgupstream").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}

if(pgboarduniversity !='')
{
  $("#pgupstream").attr("required", "true");
  $("#pgpassingyear").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}

if(pgschoolcollegeinstitute !='')
{
  $("#pgupstream").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgpassingyear").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}

if(pgplace !='')
{
  $("#pgupstream").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgpassingyear").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}

if(pgspecialisation !='')
{
  $("#pgupstream").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgpassingyear").attr("required", "true");
  $("#pgpercentage").attr("required", "true");
}

if(pgpercentage !='')
{
  $("#pgupstream").attr("required", "true");
  $("#pgboarduniversity").attr("required", "true");
  $("#pgschoolcollegeinstitute").attr("required", "true");
  $("#pgplace").attr("required", "true");
  $("#pgspecialisation").attr("required", "true");
  $("#pgpassingyear").attr("required", "true");
}


      //pooja maam

         // if(pgupstream!='')
         //                {
         //                 // alert("Please Select fill All Fields ");
         //            $("#pgpassingyear").attr("required", "true");
         //               $("#pgboarduniversity").attr("required", "true");
         //               $("#pgschoolcollegeinstitute").attr("required", "true");
         //               $("#pgplace").attr("required", "true");
         //            $("#pgspecialisation").attr("required", "true");
         //             $("#pgpercentage").attr("required", "true");
         //                }


         //                else if(pgupstream!='' && pgpassingyear!=''|| pgboarduniversity=='' || pgschoolcollegeinstitute=='' || pgspecialisation=='' || pgpercentage=='' ||   pgplace=='' )

         //                    {



         //                        //alert("Pleast select other fill other fields");

         //              $("#pgboarduniversity").attr("required", "true");
         //              $("#pgschoolcollegeinstitute").attr("required", "true");
         //              $("#pgplace").attr("required", "true");
         //              $("#pgspecialisation").attr("required", "true");
         //              $("#pgpercentage").attr("required", "true");



         //                    }


         //                    else if(pgupstream!='' && pgpassingyear=='' && pgboarduniversity=='' || pgschoolcollegeinstitute=='' || pgspecialisation=='' || pgpercentage=='' ||   pgplace=='' )
         //                    {

         //                        //alert("Pleast select other fill other fields");

         //                          $("#pgschoolcollegeinstitute").attr("required", "true");
         //              $("#pgplace").attr("required", "true");
         //              $("#pgspecialisation").attr("required", "true");
         //              $("#pgpercentage").attr("required", "true");
         //                    }

         //                     else if(pgupstream!='' && pgpassingyear=='' && pgboarduniversity=='' && pgschoolcollegeinstitute!='' || pgspecialisation=='' || pgpercentage=='' ||   pgplace=='' )
         //                    {

         //                        //alert("Pleast select other fill other fields");


         //              $("#pgplace").attr("required", "true");
         //              $("#pgspecialisation").attr("required", "true");
         //              $("#pgpercentage").attr("required", "true");
         //                    }



         //                    else if(pgupstream!='' && pgpassingyear=='' && pgboarduniversity=='' && pgschoolcollegeinstitute!='' && pgspecialisation!='' &&  pgpercentage!='' ||   pgplace=='' )
         //                    {

         //                        //alert("Pleast select other fill other fields");




         //                 $("#pgplace").attr("required", "true");
         //                    }

         //                    else  if(pgupstream!='' && pgpassingyear=='' && pgboarduniversity=='' &&pgschoolcollegeinstitute=='' && pgspecialisation=='' && pgpercentage=='' &&   pgplace=='') {


         //                $("#pgupstream").attr("required", "false");
         //               $("#pgpassingyear").attr("required", "false");
         //                $("#pgboarduniversity").attr("required", "false");
         //               $("#pgschoolcollegeinstitute").attr("required", "false");
         //               $("#pgplace").attr("required", "false");
         //             $("#pgspecialisation").attr("required", "false");
         //               $("#pgpercentage").attr("required", "false");
         //             }







    // var syslangarray = [syslanguage ,syslanguage1];

    // if(!syslangarray.includes(syslanguage))
    // {
    //   alert("hindi");
    //   alert("Please fill mandatory languages - Hindi  And English");
    //   $('#syslanguage_0').focus();
    //   return false;
    // }

    //  if(!syslangarray.includes(syslanguage1))
    // {
    //   return true;
    // }
    // else{
    //   alert("English");
    //   alert("Please fill mandatory languages - Hindi  And English");
    //   $('#syslanguage_0').focus();
    //   return false;
    // }

// rajat
// if (syslanguage !=2)
// {
//  alert("Please fill mandatory languages - Hindi  And English");
//  $('#syslanguage_0').focus();
//  return false; 
// }

// if (syslanguage1 !=3)
// {
//  alert("Please fill mandatory languages - Hindi  And English");
//  $('#syslanguage_1').focus();
//  return false; 
// }


  // if (syslanguage !=2 || syslanguage !=2)
  // {
  //  alert("Please fill mandatory languages - Hindi  And English");
  //  $('#syslanguage_0').focus();
  //  return false; 
  // }

  //  if (syslanguage1 !=3 || syslanguage1 !=3)
  // {
  //  alert("Please fill mandatory languages - Hindi  And English");
  //  $('#syslanguage_1').focus();
  //  return false; 
  // }
  if (gapfromdate !='') {
    if (gaptodate =='') {

     $('#gaptodate').focus();

     return false;
   }
   if (gapreason =='') {
     $('#gapreason').focus();

     return false;
   }

 }

 if (natureoftraining !='') {
  if (organizingagency =='') {
         // alert('Please Enter Organizing Agency!!!');
         $('#organizingagency').focus();
         return false;
       }
       if (fromdate =='') {
         $('#fromdate').focus();
         return false;
       }
       if (todate =='') {
         $('#todate').focus();
         return false;
       }

     }

     if (orgname !='') {
       if (designation =='') {
        $('#designation').focus();
        return false;
      }
      if (descriptionofassignment =='') {
       $('#descriptionofassignment').focus();
       return false;
     }
     if (work_experience_fromdate =='') {
      $('#work_experience_fromdate').focus();
      return false;
    }
    

    if (work_experience_todate =='') {
     $('#work_experience_todate').focus();
     return false;
   }
   if (palceofposting =='') {
     $('#palceofposting').focus();
     return false;
   }

   if (last_salary_drawn =='') {
    $('#last_salary_drawn').focus();
    return false;
  }
}

//rajat
// if (pgupstream !='') {

//   if (pgupstream =='') {

//    $('#pgupstream').focus();

//    return false;
//  }
//  if (pgpassingyear =='') {
//    $('#pgpassingyear').focus();

//    return false;
//  }

//  if (pgboarduniversity =='') {
//    $('#pgboarduniversity').focus();

//    return false;
//  }

//  if (pgschoolcollegeinstitute =='') {
//    $('#pgschoolcollegeinstitute').focus();

//    return false;
//  }

//  if (pgplace =='') {
//    $('#pgplace').focus();

//    return false;
//  }
//  if (pgspecialisation =='') {
//    $('#pgspecialisation').focus();

//    return false;
//  }
//  if (pgpercentage =='') {
//    $('#pgpercentage').focus();

//    return false;
//  }

// }


}

 /// Upload Buttone /////////

 $(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
 $(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
 
 var matriccertificate_uploadField = document.getElementById("10thcertificate");

 matriccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 


var hsccertificate_uploadField = document.getElementById("12thcertificate");

hsccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

var ugcertificate_uploadField = document.getElementById("ugcertificate");

ugcertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

</script>

<script type="text/javascript">
  $(document).ready(function(){
    decimalData();
    adddatepicker();
    workexchangedatepicker(incwe);
    changedatepicker(inctg);
    ifanyvalidation();

    if ( $("input[name='other']").is(':checked') ) {

      $("#other_other_specify_degree").show();
      $("#specify_degree").prop('required','required');
      
    } else {
     $("#other_other_specify_degree").hide();
     $("#specify").prop('disabled','true');
   }
   
   $('[data-toggle="tooltip"]').tooltip(); 
   $("#pradan_selection_process_before_details").prop("disabled", "disabled");
   $("#selection_process_befor").click(function () {
    if ($(this).is(":checked")) {
      $("#pradan_selection_process_before_details").removeAttr("disabled");
    } else {
      $("#pradan_selection_process_before_details").prop("disabled", "disabled");
    }
  });

   $('#contact_name').on('input', function() {
     var input=$(this);
     var is_name=input.val();
     if(is_name){input.removeClass("invalid").addClass("valid");}
     else{input.removeClass("valid").addClass("invalid");}
   });
   

 });

  $(function () 
  {
    $("#selection_process_befor").click(function ()
    {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
  });
  

// $("#sendDataWorkExpButton").click(function() {
//     var count_checked = $("[name='selection_process_befor']:checked").length; // count the checked rows
//         if(count_checked == 0) 
//         {
//             alert("Have you taken part in PRADAN's selection process before?.");
//             return false;
//         }
//         return true;
// });

$("#filladdress").on("click", function(){
// alert('string');
if (this.checked) { 
  if ($("#permanenthno").val().length == 0  && $("#presenthno").val().length ==0) {
    alert('Please Fill Present Mailing Address !!!');
    $("#filladdress").removeAttr('checked');
  }else{

    $("#permanenthno").val($("#presenthno").val());
    $("#permanentstreet").val($("#presentstreet").val());
    $("#permanentcity").val($("#presentcity").val());
    $("#permanentstateid").val($("#presentstateid").val()); 
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ $("#presentstateid").val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

            //console.log(data);
            $("#permanentdistrict").html(data);
            $("#permanentdistrict").val($("#presentdistrict").val());

          })

    $("#permanentdistrict").val($("#presentdistrict").val());
    $("#permanentpincode").val($("#presentpincode").val()); 

  }
}
else {
  $("#permanenthno").val('');
  $("#permanentstreet").val('');
  $("#permanentcity").val('');
  $("#permanentstateid").val(''); 

  $("#permanentdistrict").val('');
  $("#permanentpincode").val('');
  $("#permanentstateid").val('');          
}
});



function decimalData(){
  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
   }
 }, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});

}

$("#btnRemoveRow").click(function() {
  if($('#tblForm09 tr').length-2>1)
    $('#tbodyForm09 tr:last').remove()
});

$('#btnAddRow').click(function() {
  rowsEnter = parseInt(1);

  if (rowsEnter < 1) {
    alert("Row number must be minimum 1.")
    return;
  }
  insertRows(rowsEnter);
    //addDatePicker();
  });



var srNoGlobal=0;
var inc = 0;

function insertRows(count) {
  srNoGlobal = $('#tbodyForm09 tr').length+1;
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;

  for (i = 1; i <= count; i++) {
    inc++;
    cloneRow = lastRow.clone();
    var tableData = '<tr>'
    + ' <td>'
    + '<input type="text" name="familymembername['+inc+']" id="familymembername" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
    + '</td><td>'
    + '<select class="form-control" name="relationwithenployee['+inc+']" required="required">'
    + '<option value="">Select Relation</option>'
    <?php foreach ($sysrelations as $key => $value): ?>
    + '<option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>'
    <?php endforeach ?>
    + '</select>'
    + '</td>'
    + '<td><input type="text" class="form-control datepicker" id="familymemberdob'+inc+'" data-toggle="tooltip" title="Date Of Birth!" name="familymemberdob['+inc+']" placeholder="Enter Date Of Birth " value="" required="required" >'

    + '</td>'
    + '</tr>';
    adddatepicker();
    $("#tbodyForm09").append(tableData)

  }

}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });



  var srNoGlobal=0;
  var inctg = 0;

  function Insaettrainingexposure(count) {
    srNoGlobal = $('#bodytblForm10 tr').length+1;
    var tbody = $('#bodytblForm10');
    var lastRow = $('#bodytblForm10 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inctg++
      cloneRow = lastRow.clone();
      var tableData1 = '<tr>'
      + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="natureoftraining" name="natureoftraining['+inctg+']" placeholder="Enter Nature of Training" style="min-width: 20%;" value="" data-original-title="Nature of Training !">'
      + '</td>'
      + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="organizingagency['+inctg+']" name="organizingagency['+inctg+']" placeholder="Organizing Agency" style="min-width: 20%;" value="" required="required" data-original-title="Organizing Agency !"></td>'

      + '<td><input type="text" class="form-control datepicker" placeholder="From Date" onclick="return changedatepicker(inctg)" id="fromdate_'+inctg+'" '+
      'name="fromdate['+inctg+']" value=""  required="required" />'
      + '</td><td>'
      + '<input type="text" class="form-control datepicker" placeholder="To Date" onclick="return changedatepicker(inctg)" id="todate_'+inctg+'" '+
      'name="todate['+inctg+']" value="" required="required" />'
      + '</td>' + '</tr>';
      adddatepicker();
      $("#bodytblForm10").append(tableData1)
      changedatepicker(inctg);
    }

  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnproficiencyRemoveRow").click(function() {
    if($('#tblForm11 tr').length-3>1)
      $('#bodytblForm11 tr:last').remove()
  });

  $('#btnproficiencyAddRow').click(function() {
    rowsEnter2 = parseInt(1);
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaetproficiency(rowsEnter2);
    //addDatePicker();
  });




  var srNoGlobal=0;
  var inclp = 0;

  function Insaetproficiency(count) {
    srNoGlobal = $('#bodytblForm11 tr').length+1;
    var tbody = $('#bodytblForm11');
    var lastRow = $('#bodytblForm11 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inclp++
      cloneRow = lastRow.clone();
      var tableData2 = '<tr>'
      + '<td><select class="form-control" id="syslanguage_'+inclp+'" name="syslanguage['+inclp+']" required="required">'
      
      <?php foreach ($syslanguage as $key => $value): ?>
      + '<option value=<?php echo $value->lang_cd;?>><?php echo $value->lang_name;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'
      + '<select class="form-control" name="speak['+inclp+']" id="speak'+inclp+'" required="required">'
      
      <?php foreach ($sysspeak as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'

      + '<select class="form-control" id="read'+inclp+'" name="read['+inclp+']" required="required">'
      
      <?php foreach ($sysread as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'

      + '<select class="form-control" id="write'+inclp+'" name="write['+inclp+']" required="required">'

      <?php foreach ($syswrite as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td>'
      + '</tr>';
      $("#bodytblForm11").append(tableData2)
      adddatepicker();
    }

  }
  //insertRows();
</script>

<script type="text/javascript">
    // function checklanguage() {
    //   var language = $('.syslanguage_0').val();
    //   // alert(language);
    //   if ($('.syslanguage_0').val() == 2 || language == 3)
    //   {
    //    alert("Please fill mandatory languages - Hindi  And English");
    //    }
    //    else{
    //    $("#syslanguage").val('');
    //    $("#speak").val('');
    //    $("#read").val('');
    //    $("#write").val('');
    //    }
    // }
  </script>

  <script type="text/javascript">

    $("#btnworkexperienceRemoveRow").click(function() {
     if($('#bodytblForm12 tr').length >1)
      $('#bodytblForm12 tr:last').remove()
  });

    $('#btnworkexperienceAddRow').click(function() {
      rowsEnter12 = parseInt(1);
      if (rowsEnter12 < 1) {
        alert("Row number must be minimum 1.")
        return;
      }
      Insertworkexperience(rowsEnter12);

    });

    var srNoIdentity=0;
    var incr = 0;


    function Insertworkexperience(count) {

      srNoGlobal = $('#bodytblForm12 tr').length+1;
      var tbody = $('#bodytblForm12');
      var lastRow = $('#bodytblForm12 tr:last');
      var cloneRow = null;
      count=$('#bodytblForm12 tr').length;

      cloneRow = lastRow.clone();
      var tableData12 = '<tr>'
      + '<td>'
      + '<input type="text" name="orgname['+count+']" id="orgname_'+count+'" data-toggle="tooltip" value="" class="form-control" maxlengt="100" placeholder="Enter Organization" data-original-title="" required="required" title="">'
      + '</td><td>'
      + '<input type="text" name="designation['+count+']" id="designation_'+count+'" data-toggle="tooltip" value="" class="form-control" placeholder="Enter Designation" maxlengt="50" data-original-title="" required="required" title="">'
      + '</td><td>'
      + '<textarea name="descriptionofassignment['+count+']" placeholder="Enter Description Of Assignment" data-toggle="tooltip" maxlengt="250"  id="descriptionofassignment_'+count+'" required="required" class="form-control" data-original-title="" title=""> '
      + '</textarea></td><td>'

      + ' <div class="col-lg-12">'
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">'
      + '    <input type="text" name="work_experience_fromdate['+count+']" placeholder="From Date"  data-toggle="tooltip" id="work_experience_fromdate_'+count+'"  value="" class="form-control datepicker" size=150>'
      + ' </div>' 
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">'
      + ' <input type="text" name="work_experience_todate['+count+']" "  data-toggle="tooltip" id="work_experience_todate_'+count+'" placeholder="To Date"  class="form-control datepicker" value="" size=150></div> </div>'
      + '</td><td>'
      + '<input type="text" maxlengt="50" name="palceofposting['+count+']" data-toggle="tooltip" id="palceofposting_'+count+'" required="required" value="" placeholder="Enter Place of Posting" class="form-control" data-original-title=""  title="">'
      + ' </td><td>'
      + '<input type="text" maxlengt="10" name="last_salary_drawn['+count+']" placeholder="Enter last salary drawn" data-toggle="tooltip" id="lastsalarydrawn_'+count+'" required="required" value="" class="form-control txtNumeric" data-original-title=""  title="">'
      + '</td></tr>';
      + '</td></tr>';
      adddatepicker()
      $("#bodytblForm12").append(tableData12)
      workexchangedatepicker(count);
    }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-2>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertIdentityRows(rowsIdentityEnter);
    
  });

  
  var srNoIdentity=0;
  var incr = 0;
  
  function insertIdentityRows(count) {
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      incr++
      cloneRow = lastRow.clone();
      var tableDataIdentity = '<tr>'
      + ' <td>'
      + '<select class="form-control" id="identityname'+incr+'" name="identityname['+incr+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysidentity as $key => $value): ?>
      + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
      <?php endforeach ?>
      + '</select>'

      + ' <td><input type="text" id="identitynumber'+incr+'" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td>'
      + '</tr>';
      $("#tbodyForm13").append(tableDataIdentity)
      
    }

  }
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit")..prop('disabled', true);

  });
</script>  

<script type="text/javascript">

  $("#btnGapReasonRemoveRow").click(function() {
    if($('#tblForm15 tr').length-2>1)
      $('#bodytblForm15 tr:last').remove()
  });

  $('#btnGapReasonAddRow').click(function() {
    rowsEnter1 = $('#bodytblForm15 tr').length;
    // if (rowsEnter1 < 1) {
    //   alert("Row number must be minimum 1.")
    //   return;
    // }
    InsGapReason(rowsEnter1);
    
  });

  var srNoGlobal=0;
  var inctg = 0;

  function InsGapReason(count) {
    srNoGlobal = $('#bodytblForm15 tr').length+1;
    var tbody = $('#bodytblForm15');
    var lastRow = $('#bodytblForm15 tr:last');
    var cloneRow = null;
    var inctg = count;
 // alert(count);
 // for (i = 1; i <= count; i++) {
   //inctg++;
   cloneRow = lastRow.clone();
   var tableData15 = '<tr>'
   + '<td><input type="text" class="form-control datepicker" id="gapfromdate_'+inctg+'" '+
   'name="gapfromdate['+inctg+']" placeholder="From Date" onclick="return changedatepicker(inctg)" value="" required="required"/>'
   + '</td><td>'
   + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="gaptodate_'+inctg+'" name="gaptodate['+inctg+']" placeholder="To Date" value="" required="required" />'
   + '</td><td>' 
   + '<textarea class="form-control alphabateonly"  maxlength="250" name="gapreason['+inctg+']" id="gapreason_'+inctg+'" title="Gap Reason" placeholder="Enter gap reason" required="required" ></textarea>'
   + '</td>'
   + '</tr>';
   
   $("#bodytblForm15").append(tableData15)
   changegapyeardatepicker(inctg);
//}

}
  //insertRows();
</script>


<script type="text/javascript">
  $(document).ready(function(){
   $("#gapfromdate").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
     // alert(selectedDate);
     jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
        // jQuery("#work_experience_todate_2").datepicker( "option", "minDate", selectedDate );
      }
    });

   $("#gaptodate").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      alert(selectedDate);
      jQuery("#gapfromdate").datepicker( "option", "maxDate", selectedDate );
    }
  });

   decimalData();
   changegapyeardatepicker(inctg)
   $('[data-toggle="tooltip"]').tooltip();   


   $(function () {
    $('#presentcity').keydown(function (e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
  });
   $(function () {
    $('#permanentcity').keydown(function (e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
  });

 });

  // $(document).ready(function(){

    $(".alphabateonly").keypress(function (e){
     var code =e.keyCode || e.which;
     if((code<65 || code>90 )
       &&(code<97 || code>122)&&code!=32&&code!=46 && code!=44 && code!=39) 

     {
       alert("Only alphabates are allowed");
       return false;
     }
   });


    $("#filladdress").on("click", function(){

     if (this.checked) { 
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());

    }
    else {
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

  // });

  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});



    $('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z_ ]*$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
        e.preventDefault();
        $('.error').show();
        $('.error').text('Please Enter Alphabate');
        return false;
      }
    });
  }

</script>


<!-- Jquery Core Js -->
<script src="<?php echo site_url('common/backend/jquery-1.12.4.js');?>"></script>
<script src="<?php echo site_url('common/backend/jquery-ui.js');?>"></script>
<script type="text/javascript">
 $(document).ready(function(){
  var presentstateid =  $("#presentstateid").val();
  $.ajax({
    url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ presentstateid,
    type: 'POST',
    dataType: 'text',
  })
  .done(function(data) {
    console.log(data);
    $("#presentdistrict").html(data);
    $("#permanentdistrict").html(data);
  })


  var permanentstateid =  $("#permanentstateid").val();
  $.ajax({
    url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ permanentstateid,
    type: 'POST',
    dataType: 'text',
  })
  .done(function(data) {

    console.log(data);
    $("#permanentdistrict").html(data);

  })




  $("#presentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     


 $(".datepicker").datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   yearRange: '1920:2030',
   dateFormat : 'dd/mm/yy',
   onClose: function(selectedDate) {
    jQuery("#todate").datepicker( "option", "minDate", selectedDate );
    jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
    jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
        // jQuery("#work_experience_todate_2").datepicker( "option", "minDate", selectedDate );
      }
    });

 $(".datepicker").datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   yearRange: '1920:2030',
   dateFormat : 'dd/mm/yy',
   onClose: function(selectedDate) {
    jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
    jQuery("#work_experience_fromdate").datepicker( "option", "maxDate", selectedDate );
    jQuery("#gapfromdate").datepicker( "option", "maxDate", selectedDate );
        // jQuery("#work_experience_fromdate_1").datepicker( "option", "maxDate", selectedDate );

      }
    });

 
 function adddatepicker(){
   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
    }
  });

 }






 function workexchangedatepicker(incwe){

//alert($("#work_experience_fromdate_"+incwe).val());

$("#work_experience_fromdate_"+incwe).datepicker({
  changeMonth: true,
  changeYear: true,
  maxDate: 'today',
  yearRange: '1920:2030',
  dateFormat : 'dd/mm/yy',
  onClose: function(selectedDate) {
   // alert(selectedDate);
   jQuery("#work_experience_todate_"+incwe).datepicker( "option", "minDate", selectedDate );
 }
});

$("#work_experience_todate_"+incwe).datepicker({
 changeMonth: true,
 changeYear: true,
 maxDate: 'today',
 yearRange: '1920:2030',
 dateFormat : 'dd/mm/yy',
 onClose: function(selectedDate) {
 // alert(selectedDate);
 jQuery("#work_experience_fromdate_"+incwe).datepicker( "option", "maxDate", selectedDate );
}
});

}

function changedatepicker(inctg){

 $("#fromdate_"+inctg).datepicker({
  changeMonth: true,
  changeYear: true,
  maxDate: 'today',
  yearRange: '1920:2030',
  dateFormat : 'dd/mm/yy',
  onClose: function(selectedDate) {
         // alert(selectedDate);
         jQuery("#todate_"+inctg).datepicker("option", "minDate", selectedDate);
       }
     });

 $("#todate_"+inctg).datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   yearRange: '1920:2030',
   dateFormat : 'dd/mm/yy',
   onClose: function(selectedDate) {
    jQuery("#fromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
  }
});
}

function changegapyeardatepicker(inctg){

 $("#gapfromdate_"+inctg).datepicker({
  changeMonth: true,
  changeYear: true,
  maxDate: 'today',
  dateFormat : 'dd/mm/yy',
  yearRange: '1920:2030',
  onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#gaptodate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
      });

 $("#gaptodate_"+inctg).datepicker({
   changeMonth: true,
   changeYear: true,
   maxDate: 'today',
   dateFormat : 'dd/mm/yy',
   yearRange: '1920:2030',
   onClose: function(selectedDate) {
    jQuery("#gapfromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
  }
});
}
</script>
<script>
  function checkemail() {
    var email=$("#emailid").val();

    $.ajax({
      data:{email:email},
      url: '<?php echo site_url(); ?>Ajax/email_exists/',
      
      type: 'POST',
      
    })
    .done(function(data) {
      if(data>0)
      {
        $("#email_error").text("email id allready exist, please choose other email id");
        $("#emailid").val('');
        $("#emailid").focusin();


      }
      else
      {
        $("#email_error").text(" ");
      }
      

    })

    


  }
  function email_focus()
  {
    var email=$("#emailid").val();
    
    if(email=='')
    {
      $("#emailid").focus();
    }
  }
</script>

