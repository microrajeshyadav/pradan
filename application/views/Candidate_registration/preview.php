
<br>
<style type="text/css">

thead>tr:hover
{
  border-bottom: solid 1px red;
}   

label{

  font-weight: bold;
  font-family: 
}
.bg-lightgreen
{
  background-color: #85a4a5;
}
</style>

<?php //print_r($getresigstatus); die; ?>

<div class="container-fluid" id="tblForm09" style="font-family: 'Oxygen' !important;" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Biodata Form for <?php echo $category; ?></h4>
       
    </div>
    <hr class="colorgraph"><br>
  </div>
   <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <table id="tblForm09" class="table">
        <thead>
         <tr class="bg-light">
          <th colspan="7" class="bg-lightgreen text-white">Personal Infomation</th>
        </tr> 
      </thead>
      <tbody>
        <tr>
         
      <td rowspan ="6" class="text-left">

          <?php if ($candidatedetails->encryptedphotoname !='') { ?>
            <img class= "rounded-circle" src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Jane" width="160px" height="160px">
          <?php }else{ ?>
            <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px">
          <?php } ?>

          <h4><?php  if (!empty($candidatedetails->candidatefirstname)) {
           echo $candidatedetails->candidatefirstname .' '.$candidatedetails->candidatemiddlename.' '.$candidatedetails->candidatelastname;
          }else{
            echo $candidatedetails->staff_name;
          }   ?> 
         </h4>
          <p style= "font-weight: 700"><i class="fa fa-envelope" style="font-size: 14px; color: #17A2B8;"></i> <?php echo $candidatedetails->emailid;?></p>
          <p style= "font-weight: 700"><i class="fa fa-mobile" aria-hidden="true" style="font-size: 14px; color: #17A2B8;"></i>
            <?php echo $candidatedetails->mobile;?></p>

            <?php $options = array("1" => "O+", "2" => "B+", "3" => "AB+", "4" => "O-", "5" => "B-", "6" => "AB-");  ?>
            <?php if (!empty($candidatedetails->bloodgroup) && $candidatedetails->bloodgroup==1) { ?>
              <p style= "font-weight: 700"><i class="fas fa-tint" style="color: red"></i><?php echo "O+"; ?></p>
            <?php }elseif (!empty($candidatedetails->bloodgroup) && $candidatedetails->bloodgroup==2) { ?>
             <p style= "font-weight: 700"><i class="fas fa-tint" style="color: red"></i><?php echo "B+"; ?></p>
           <?php }  ?>

         </td>
        <td><label for="Name">Candidate name :</label></td>
        <td style= "font-weight: 700"><?php echo $candidatedetails->candidatefirstname; ;?></td>
        <td><label for="Name">Middle name :</label></td>
        <td style= "font-weight: 700"><?php echo  $candidatedetails->candidatemiddlename?></td>
        <td><label for="Name">Last Name :</label></td>
        <td style= "font-weight: 700"><?php echo $candidatedetails->candidatelastname;?></td>
        
       </tr>

       <tr>
          <td><label for="Name">Mother's name :</label></td>
          <td><?php echo $candidatedetails->motherfirstname;?></td>
          <td><label for="Name">Middle name :</label></td>
          <td style="width: auto;"><?php echo $candidatedetails->mothermiddlename;?></td>
          <td><label for="Name">Last Name :</label></td>
          <td><?php echo $candidatedetails->motherlastname;?></td>
        </tr>
    <tr>
          <td><label for="Name">Father's name :</label></td>
          <td><?php echo $candidatedetails->fatherfirstname;?></td>
          <td><label for="Name">Middle name :</label></td>
          <td style="width: auto;"><?php echo $candidatedetails->fathermiddlename;?></td>
          <td><label for="Name">Last Name :</label></td>
          <td><?php echo $candidatedetails->fatherlastname;?> </td>
        </tr>

  <tr>
    <td><label for="Name">Gender :</label></td>
          <td><?php 
          $options = array("1" => "Male", "2" => "Female", "3" => "Others");
            if ($candidatedetails->gender ==1) {
              echo "Male";
            }elseif ($candidatedetails->gender ==2) {
             echo "Female";
            }elseif ($candidatedetails->gender ==3) {
             echo "Others";
            } ?>
          </td>
  <td><label for="Name">Nationality :</label></td>
          <td style="width: auto;"> <?php echo  $candidatedetails->nationality;?> </td>
  <td><label for="Name">Marital Status :</label></td>
  <td style= "font-weight: 700">  <?php 
          $options = array("1" => "Single", "2" => "Married", "3" => "Divorced", "4" => "Widow", "5" => "Separated");
              if ($candidatedetails->maritalstatus ==1) {
                echo "Single";
              }else if ($candidatedetails->maritalstatus ==2) {
                echo "Married";
              }else if ($candidatedetails->maritalstatus ==3) {
                echo "Divorced";
              }else if ($candidatedetails->maritalstatus ==4) {
                echo "Widow";
              }else if ($candidatedetails->maritalstatus ==5) {
                echo "Separated";
              }
            ?> 
</td>
</tr>
<?php if($candidatedetails->candidateid!=null)
{
  ?>
  <tr>
    <td><label for="Name">Annual income of parents :</label></td>
    <td><?php 
              if ($otherinformationdetails->annual_income==1) {
               echo "Below 50,000";
              }else if ($otherinformationdetails->annual_income==2) {
                echo "50,001-200,000";
              }else if ($otherinformationdetails->annual_income==3) {
                echo "200,001-500,000";
              }else if ($otherinformationdetails->annual_income==4) {
                 echo "Above 500,000";
              }
            ?>
          </td>
<td><label for="Name">No. of male sibling :</label></td>
<td style= "font-weight: 700"><?php echo $otherinformationdetails->male_sibling;?></td>
<td><label for="Name">No. of female sibling :</label></td>
<td style= "font-weight: 700"><?php echo $otherinformationdetails->female_sibling;?></td>
</tr>

<tr>
  <td><label for="Name">Date Of Birth :</label></td>
    <td><?php echo $this->model->changedatedbformate($candidatedetails->dateofbirth);?></td>
          <td><label for="Name">Email Id :</label></td>
          <td style="width: auto;"> <?php echo $candidatedetails->emailid;?></td>
          <td><label for="Name">Mobile No. :</label></td>
          <td><?php echo $candidatedetails->mobile;?></td>
</tr>
<?php } ?>
</tbody>
</table>
</div>

</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table" >
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8"> 
          <div class="form-check">
            <label class="form-check-label" for="filladdress"><b>Communication Address </b></label>
          </div>
        </th>
      </tr> 
      <?php 
       // print_r($candidatedetails);
      $checkdata='';
       if ($candidatedetails->presenthno==$candidatedetails->permanenthno) {
       $checkdata = 'checked="checked"';
      // echo  $checkdata;
     } ?>  
     <tr>
      <th class="text-center bg-light" colspan="4" style="background-color: #e1e1e1; vertical-align: top; text-align: center; "> <i class="fa fa-address-card" style="color: #F29005;"></i> &nbsp;Present Mailing Address</th>
      <th class="text-center" colspan="4" style="background-color: #d0d0d0; vertical-align: top; text-align: center;"> <i class="fa fa-address-card" style="color: #17A2B8;"></i> &nbsp;Permanent Mailing Address<br>
       <div class="form-check">
        <!-- <input type="checkbox" class="form-check-input"  <?php echo $checkdata;?>  id="filladdress" name="filladdress" disabled> 
        <label class="form-check-label" for="filladdress"><b>same as present address</b></label> -->
      </div></th>
    </tr> 
  </thead>
  <tbody>
   <tr>
    <td class="bg-light"> <label for="Name">H.No</label></td>
    <td class="bg-light" style= "font-weight: 700"><?php echo $candidatedetails->presenthno;?></td>
    <td class="bg-light"><label for="Name">Street</label></td>
    <td class="bg-light" style= "font-weight: 700; border-right: Solid 1px #e1e1e1;"><?php echo $candidatedetails->presentstreet;?></td>

    <td><label for="Name">H.No</label></td>
    <td style= "font-weight: 700"><?php echo $candidatedetails->permanenthno;?></td>
    <td><label for="Name">Street</label></td>
    <td style= "font-weight: 700"><?php echo $candidatedetails->permanentstreet;?></td>
  </tr>
  <td class="bg-light"><label for="Name">State</label></td>
                    <td class="bg-light"><?php echo  $candidatedetails->presentstatename;?></td>
                   <td class="bg-light"> <label for="Name">City</label> </td>
                   <td class="bg-light"><?php echo $candidatedetails->presentcity;?></td>
                    <td><label for="Name">State</label></td>
                    <td><?php echo $candidatedetails->permanentstatename;?></td>
                   <td><label for="Name">City</label></td>
                      <td><?php echo $candidatedetails->permanentcity;?></td>
  <tr>
    <td class="bg-light"><label for="Name">District</label> </td>
                    <td class="bg-light"><?php echo $candidatedetails->presentdistrictname;?></td>
                  <td class="bg-light"><label for="Name">Pin Code</label></td>
                  <td class="bg-light"><?php echo $candidatedetails->presentpincode;?></td>
                  <td><label for="Name">District</label></td>
                  <td><?php echo $candidatedetails->permanentdistrictname;?></td>
                  <td><label for="Name">Pin Code</label></td>
                  <td><?php echo $candidatedetails->permanentpincode;?></td>

  </tr>
</tbody>
</table>
</div>  
</div>
<?php //echo "<pre>"; print_r($candidatedetails); die; ?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; ">

    <table id="tbledubackground" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white">Educational Background</th>
      </tr> 
      <tr>
        <th class="text-left" style="vertical-align: top;">Course</th>

        <th class="text-center" style="vertical-align: top;">Year </th>
        <th class="text-left" style="vertical-align: top;"> Board/ University</th>
        <th class="text-left" style="vertical-align: top;">School/ College/ Institute</th>
        <th class="text-left" style="vertical-align: top;">Place</th>
        <th class="text-left" style="vertical-align: top;">Specialisation</th>

        <th class="text-right" style="vertical-align: top;">Percentage(%)</th>

      </tr> 
    </thead>
    <tbody>
      <tr style="">
        <td><b>10th</b></td>
        <td class="text-center"><?php if(!empty($candidatedetails->metricpassingyear)) echo $candidatedetails->metricpassingyear;?>
        </td>
        <td> <?php if(!empty($candidatedetails->metricboarduniversity)) echo $candidatedetails->metricboarduniversity;?></td>
        <td>  <?php if(!empty($candidatedetails->metricschoolcollege)) echo $candidatedetails->metricschoolcollege;?></td>

        <td><?php if(!empty($candidatedetails->metricplace)) echo $candidatedetails->metricplace;?>
        </td>
        <td><?php if(!empty($candidatedetails->metricspecialisation)) echo $candidatedetails->metricspecialisation;?>
        </td>


        <td class="text-right"><?php if(!empty($candidatedetails->metricpercentage)) echo $candidatedetails->metricpercentage.'%';?> </td>


      </tr>
      <tr style="">
        <td><b><?php  if($candidatedetails->hscstream==2)
        { echo "12th"; }
        else { echo "Diploma";}
        ?></b> </td>
        <td class="text-center"><?php if(!empty($candidatedetails->hscpassingyear)) echo $candidatedetails->hscpassingyear;?>
        </td>
        <td> <?php if(!empty($candidatedetails->hscboarduniversity)) echo $candidatedetails->hscboarduniversity;?></td>

        <td>  <?php if(!empty($candidatedetails->hscschoolcollege)) echo $candidatedetails->hscschoolcollege;?></td>

        <td><?php if(!empty($candidatedetails->hscplace)) echo $candidatedetails->hscplace;?></td>

        <td><?php if(!empty($candidatedetails->hscspecialisation)) echo $candidatedetails->hscspecialisation;?></td>
        <td class="text-right"><?php if(!empty($candidatedetails->hscpercentage)) echo $candidatedetails->hscpercentage.'%';?></td>
      </tr>
      <tr style="">
        <td><b><?php if(!empty($candidatedetails->ugdegree)) echo $candidatedetails->ugdegree; ?></b></td>
        <td class="text-center"><?php if(!empty($candidatedetails->ugpassingyear)) echo $candidatedetails->ugpassingyear; ?>
        </td>
        <td> <?php if(!empty($candidatedetails->ugboarduniversity)) echo $candidatedetails->ugboarduniversity;?></td>
        <td>
          <?php if(!empty($candidatedetails->ugschoolcollege)) echo $candidatedetails->ugschoolcollege;?>

        </td>
        <td><?php if(!empty($candidatedetails->ugplace)) echo $candidatedetails->ugplace;?></td>



        <td>
         <?php if(!empty($candidatedetails->ugspecialisation)) echo  $candidatedetails->ugspecialisation;  ?>
       </td>

       <td class="text-right"><?php if(!empty($candidatedetails->ugpercentage)) echo $candidatedetails->ugpercentage.'%';?>
       </td>

     </tr>
     <?php if (!empty($candidatedetails->pgpassingyear) && $candidatedetails->pgpassingyear !=0) {?>
     <tr style="">
      <td><b><?php if(!empty($candidatedetails->pgdegree)) echo $candidatedetails->pgdegree; ?></b></td>

      <td class="text-center"><?php if(!empty($candidatedetails->pgpassingyear)) echo $candidatedetails->pgpassingyear;?>
      </td>

      <td> <?php if(!empty($candidatedetails->pgboarduniversity)) echo $candidatedetails->pgboarduniversity;?></td>
      <td>  
        <?php if(!empty($candidatedetails->pgschoolcollege)) echo $candidatedetails->pgschoolcollege;?>

      </td>

      <td><?php if(!empty($candidatedetails->pgplace)) echo $candidatedetails->pgplace;?></td>
      <td>

        <?php if(!empty($candidatedetails->pgspecialisation)) echo $candidatedetails->pgspecialisation;  ?></td>




        <td class="text-right"><?php if(!empty($candidatedetails->pgpercentage)) echo $candidatedetails->pgpercentage.'%';?>
        </td>

      </tr>
      <?php } ?>
      <?php if (!empty($candidatedetails->other_degree_specify) && $candidatedetails->other_degree_specify !='') {?>
      <tr style="">
        <td><b><?php if(!empty($candidatedetails->other_degree_specify)) echo $candidatedetails->other_degree_specify; ?></b></td>

        <td class="text-center"> <?php if(!empty($candidatedetails->otherpassingyear)) echo $candidatedetails->otherpassingyear; ?> </td>
        <td>  <?php if(!empty($candidatedetails->otherboarduniversity)) echo $candidatedetails->otherboarduniversity; ?> </td>
        <td> <?php if(!empty($candidatedetails->otherschoolcollege)) echo $candidatedetails->otherschoolcollege; ?>  </td>


        <td><?php if(!empty($candidatedetails->otherplace)) echo $candidatedetails->otherplace; ?></td>

        <td> <?php if(!empty($candidatedetails->otherspecialisation)) echo $candidatedetails->otherspecialisation; ?> </td>

        <td class="text-right"> <?php if(!empty($candidatedetails->otherpercentage)) echo $candidatedetails->otherpercentage .'%'; ?> </td>

      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
</div>

<?php if (count($gapyeardetals) > 0) { ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">
        Gap Year
      </th>
    </tr> 
    <tr>
     
      <th class="text-center" style="vertical-align: top; text-align: center;">From</th>
      <th class="text-center" style="vertical-align: top; text-align: center;">To </th>
      <th class="text-center" style="vertical-align: top; text-align: center; ">Reason</th>
    </tr> 
  </thead>
  <tbody id="bodytblForm10">
   <?php  $i=0;
   foreach ($gapyeardetals as $key => $val) {
    $i++; ?>
    <tr id="bodytblForm10" style="text-align: center;">
      <td> <?php echo $this->model->changedatedbformate($val->fromdate);?></td>
      <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
      <td><?php echo $val->reason; ?></td>
    </tr>
  <?php } ?>   
</tbody>
</table>
</div>
</div>
<?php   } ?>


<?php if (count($trainingexposuredetals) > 0) { ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8" >
        Training  Exposure 
      </th>
    </tr> 
    <tr>
      
      <th class="text-center" style="vertical-align: top; text-align: center;">Nature of Training </th>
      <th class="text-center" style="vertical-align: top; text-align: center;">Organizing Agency</th>
      <th class="text-center" style="vertical-align: top; text-align: center;"> From Date</th>
      <th class="text-center" style="vertical-align: top; text-align: center; ">To Date</th>
    </tr> 
  </thead>
  <tbody id="bodytblForm10">
   <?php  $i=0;
   //print_r($trainingexposuredetals);
   foreach ($trainingexposuredetals as  $val) { $i++; ?>
     <tr id="bodytblForm10" style="text-align: center;">
      
      <td><?php echo $val->natureoftraining;?></td>
      <td> <?php echo $val->organizing_agency;?></td>
      <td><?php echo $this->model->changedatedbformate($val->fromdate); ?></td>
      <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
    </tr>
  <?php } ?>   
</tbody>
</table>
</div>
</div>
<?php  } ?>

<?php
  $languagedetalsprint='';
 if (count($languageproficiency) > 0) { ?>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm11" class="table table-bordered table-striped">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">                  
        Language Skill/ProficiencyHave                 
      </th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Language</th>
      <th class="text-center" style="vertical-align: top;">Speak</th>
      <th class="text-center" style="vertical-align: top;">Read</th>
      <th class="text-center" style="vertical-align: top;">Write </th>
    </tr> 
  </thead>
  <tbody id="bodytblForm11">
    <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
    <?php $i= 0;
   // print_r($languagedetals);
    foreach($languagedetals as  $val) {
      $i++; ?>
      <tr id="bodytblForm11">
        <td class="text-center">  <?php
        echo $val->lang_name;?> </td>
        <td class="text-center">
         <?php if($val->lang_speak=='H')
         { echo "High";
       }elseif ($val->lang_speak=='L') {
        echo "Low";
      }else{
        echo "Moderate";
      }  ?></td>
      <td class="text-center"><?php if($val->lang_read=='H')
      { echo "High";
    }elseif ($val->lang_read=='L') {
      echo "Low";
    }else{
      echo "Moderate";
    }  
    ?></td>
    <td class="text-center"><?php if($val->lang_write=='H')
    { echo "High";
  }elseif ($val->lang_write=='L') {
    echo "Low";
  }else{
    echo "Moderate";
  }  
  ?></td>
</tr>
<?php }  ?>
</tbody>
</table>
</div>
</div>
<?php } ?>

<?php if ($otherinformationdetails->any_subject_of_interest !='') 
{ ?>
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8">Subject(s) of Interest</th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php echo $otherinformationdetails->any_subject_of_interest; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>



<?php if ($otherinformationdetails->any_achievementa_awards !='') 
{ ?>
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8">Achievements /Awards (if any)</th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php echo $otherinformationdetails->any_achievementa_awards; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

<?php if ($WorkExperience->WEcount != 0) {?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="7">
       Work Experience (if any) 
    </th>
  </tr> 
  <tr>
    <th style="text-align: center;">Organization Name</th>
    <th style="text-align: center;">Designation </th>
    <th style="text-align: center;">Description of Assignment</th>
    <th style="text-align: center;">Duration</th>
    <th style="text-align: center;">Place of Posting</th>
    <th style="text-align: center;">Last salary drawn (monthly)</th>
  </tr> 
  <tr>
    <th colspan="3"></th>
    <th colspan="1" style="text-align: center;">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="3"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">
    <?php   
    $i=0;
    foreach ($workexperiencedetails as $key => $val) {   ?>
      <tr id="bodytblForm12" style="text-align: center;"> 
        <td><?php echo $val->organizationname;  ?></td>
        <td><?php echo $val->designation;?></td>
        <td><?php echo $val->descriptionofassignment;?></td>
        <td>
         <div class="col-lg-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <?php echo $this->model->changedatedbformate($val->fromdate);?>
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
           <?php echo $this->model->changedatedbformate($val->todate);?>
         </div> 
       </div>
     </td>
     <td><?php echo $val->palceofposting;?></td>
     <td><?php echo $val->lastsalarydrawn;?></td>
 </tr>
 <?php $i++; } ?>
</tbody>
</table>
</div>
</div>
<?php } ?>


<?php if ($otherinformationdetails->any_assignment_of_special_interest !='') {?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">Describe any assignment(s) of special interest undertaken by you</th>
    </tr> 

  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_assignment_of_special_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>


<?php if ($otherinformationdetails->experience_of_group_social_activities !='') 
{ ?>
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8">Experience of Group and/or Social Activities </th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php echo $otherinformationdetails->experience_of_group_social_activities; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

<?php if (!empty($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before)) { 
  ?> 
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tblForm09" class="table table-bordered table-striped">
  <thead>
   <tr class="bg-lightgreen text-white">
    <td colspan="8"><b>Have you taken part in PRADAN's selection process before?</b>
  </td>
</tr> 
</thead>
<tbody>
  <tr>
    <td>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 

        <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='yes') {
          echo 'Yes';
        }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='no'){
        echo 'No';
        }
       ?>
</div>
    
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <?php echo $this->model->changedatedbformate($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when); ?>
        
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
        <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==1) {
           echo 'ON Campus';
          }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==2) {
            echo 'Off Campus';
          } ?>
        
      
       <?php } ?> 
    </td>
    </tr>
</div>
</td></tr></tbody>



<?php if (!empty($otherinformationdetails->know_about_pradan)) {  ?>
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tbltrainingexposure" class="table table-bordered table-striped">
  <thead>
   <tr class="bg-lightgreen text-white">
    <td colspan="8"><b>From where have you come to know about PRADAN?</b>
    </td>
  </tr> 
</thead>
<tbody>
  <tr>
    <td>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
        <?php 
        if ($otherinformationdetails->know_about_pradan=='CampusPlacementCell') {
         echo 'Campus Placement Cell';
        } else if ($otherinformationdetails->know_about_pradan=='UniversityProfessors') {
          echo 'University Professors';
        }else if ($otherinformationdetails->know_about_pradan=='CampusAlumni') {
          echo 'Campus Alumni';
        }else if ($otherinformationdetails->know_about_pradan=='friends') {
          echo 'Friends';
        }else if ($otherinformationdetails->know_about_pradan=='other1') {
          echo $otherinformationdetails->know_about_pradan_other_specify;
        }
      ?> 
</div>
<?php } ?>
</td>
</tr>

</table>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: #ffffff;">
 <a href="<?php echo site_url().'Candidate_registration/edit/'.$token; ?>" class="btn btn-success btn-sm m-t-10 waves-effect" >Edit</a>
<a href="<?php echo site_url().'Candidate_registration/bdfformsubmit/'.$token;?>" class="btn btn-dark btn-sm m-t-10 waves-effect" >Submit</a>
 </div>

</div>
</div>
</div>
</div>
</form>
<!-- End  Candidates Experience Info -->
<script type="text/javascript">
   function ifanyvalidation(){

    var natureoftraining    = $('#natureoftraining').val();
    var organizingagency    = $('#organizingagency').val();
    var fromdate            = $('#fromdate').val();
    var todate              = $('#todate').val();
    var orgname             = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var designation         = $('#designation').val();
    var lastsalarydrawn     = $('#lastsalarydrawn').val();
    var pgupstream          = $('#pgupstream').val();
    var other               = $('#other').val();
    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();
    var experiencedocument = $('#experiencedocument').val();
    var oldexperiencedocument = $('#oldexperiencedocument').val();
    var gapfromdate = $('#gapfromdate').val();
    var gaptodate = $('#gaptodate').val();
    var gapreason = $('#gapreason').val();

    if (pgupstream !='') {
      if (pgcertificate =='') {
       $('#pgcertificate').focus();
       return false;
     }
   }

   if (other !='') {
    if (otherscertificate =='') {
     $('#otherscertificate').focus();
     return false;
   }
 }

 if (natureoftraining !='') {
  if (organizingagency =='') {
   $('#organizingagency').focus();
   return false;
 }
 if (fromdate =='') {
   $('#fromdate').focus();
   return false;
 }
 if (todate =='') {
   $('#todate').focus();
   return false;
 }

}


if (gapfromdate !='') {
  if (gaptodate =='') {
   $('#gaptodate').focus();
   return false;
 }
 if (gapreason =='') {
   $('#gapreason').focus();
   return false;
 }

}

if (orgname !='') {

  if (designation =='') {
   $('#designation').focus();
   return false;
 }

 if (descriptionofassignment =='') {
   $('#descriptionofassignment').focus();
   return false;
 }
 if (work_experience_fromdate =='') {
   $('#work_experience_fromdate').focus();
   return false;
 }

 if (work_experience_todate =='') {
   $('#work_experience_todate').focus();
   return false;
 }
 if (palceofposting =='') {
   $('#palceofposting').focus();
   return false;
 }
 if (lastsalarydrawn =='') {
  $('#lastsalarydrawn').focus();
  return false;
}
if (oldexperiencedocument =='') {
 $("#experiencedocument").focus();
 return false;
}
}
}
</script>


