<div class="container-fluid" style="margin-top: 20px;">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row text-center">

      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-8 panel-title pull-left">Message</h4>

         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                You have been successfully registered.</div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="panel-footer col-md-12 text-right">
        <a href="http://www.pradan.net/" class="col-md-4 btn btn-danger pull-right" style="vertical-align: top;" data-toggle="tooltip" title="Close">Close</a>
      </div>
    </div>
  </div>
</div>
</div>



