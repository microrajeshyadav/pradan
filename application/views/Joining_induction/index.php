<?php foreach ($role_permission as $row) { if ($row->Controller == "Joining_induction" && $row->Action == "index"){ ?>
<br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Joining induction forms list</h4>
     
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <div class="container-fluid">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="tablecampus" class="table table-bordered table-striped">
                  <thead>
                   <tr>
                    <th style ="max-width:50px;" class="text-center">S. No.</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Email Id</th>
                    <th class="text-center">DOB</th>
                    <th class="text-center">DOJ</th>
                    <th class="text-left">Forms</th>
                    <th style ="max-width:50px;" class="text-center">Action</th>
                  </tr> 
                </thead>
                <tbody>
                  <?php 
                  $i=1;
                  

                  foreach($listing as $val)
                  { 
                  ?>
                  <tr>

                    <td class="text-center" style ="max-width:50px;"><?php echo $i;?></td>
                   
                    <td class="text-center"><?php echo $val->name;?></td>
                    <td class="text-center"><?php echo $val->emailid;?></td>

                     <td class="text-center"><?php if($val->dob) echo $this->gmodel->changedatedbformate($val->dob);?></td>
                      <td class="text-center"><?php if($val->doj) echo $this->gmodel->changedatedbformate($val->doj);?></td>
                     <td class="text-left"><?php echo $val->process_type;?></td>
                    <td class="text-center">
                      <?php
                      if($val->candidateid!='NULL' && $val->candidateid!='0' && $val->cnt >= 11)
                      { 
                      if($val->type==8) 
                      {
                        
                        ?>
                        
                        <a href="<?php echo site_url().'Employee_particular_form/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      else if($val->type==9)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Employee_particular_form/addnarrative/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                        else if($val->type==11)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Joining_report_on_appointment/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                      else if($val->type==12)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Application_from_for_identity_card/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                       else if($val->type==13)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Provident_fund_nomination_form/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                       else if($val->type==14)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Gratuity_nomination_form/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                    
                       else if($val->type==15)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'General_nomination_and_authorisation_form_staff/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                         else if($val->type==16)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Membership_applicatioin_from/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                       else if($val->type==17)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Membership_applicatioin_from/Add/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                       else if($val->type==18)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Membership_applicatioin_from/edit/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      
                      else if($val->type==19)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Medical_certificate/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      else if($val->type==23)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Provident_fund_nomination_form_staff/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      else if($val->type==24)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'Gratuity_nomination_form_staff/view/'.$val->staffid.'/'.$val->graduity_id;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      else if($val->type==25)
                      {
                       
                        ?>
                         <a href="<?php echo site_url().'General_nomination_and_authorisation_form_staff1/index/'.$val->staffid.'/'.$val->candidateid;?>" class="btn btn-primary btn-sm col-xs-3">View and Approve</a>
                        <?php 
                      }
                      ?>

                  <?php } ?>
                    </td>
                  </tr>
                 <?php $i++; }?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>
      </div>
    </div>   
    <?php } } ?>
    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
        var indno =  id.replace('sendtoed_','');
        var candidateid = $('#candidateid_'+indno).val();
//alert(candidateid);

$.ajax({
  url: '<?php echo site_url(); ?>Assigntc/sendofferlettered/'+ candidateid,
  type: 'POST',
  dataType: 'text',
})

.done(function(data) {

  if (data==1) {
   alert('Send Offer Letter To ED');
 }else{
  alert('Already send Offer Letter To ED ');
}

})

});


      function confirm_delete() {
        var r = confirm("Do you want to delete this Batch?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }


    </script>
