<section class="content" style="background-color: #FFFFFF;" >
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Assigntc" && $row->Action == "index"){ 
   ?>
   <br>
   <div class="container-fluid">
    <div class="panel panel-default" >
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Generate Offer Letter </h4>
           <div class="col-md-2 text-right">
             <!--  <a data-toggle="tooltip" data-placement="bottom" title="Want to add new Assign? Click on me." href="<?php //echo site_url()."Assigntc/add";?>" class="btn btn-primary btn-sm">Add New Assigntc</a> -->
           </div>
         </div>
         <hr class="colorgraph"><br>
       </div>

       <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class=" alert-success alert-dismissable alert"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>

            <?php   //print_r($selectedcandidatedetails); ?>
            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">
               <!--  <a href="<?php //echo site_url()."Recruiters/add/";?>" class="btn btn-sm btn-success" >Add Recruiter</a> -->
               <br>
               <br>
             </div>
           </div>
           <div class="container-fluid">
             <form name="searchassigntc" id="searchassigntc" method="POST" action="">
               <div class="row bg-light" style="padding: 10px; ">

                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 text-right">   
                 <label for="StateNameEnglish" class="field-wrapper required-field" style="padding-top: 5px;"><b>Campus Name </b></label> 
               </div>
               <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">  

                <select name="campusid" id="campusid" class="form-control" required="required">
                  <option value="">Select Campus</option>
                  <?php foreach ($campusdetails as $key => $value) {

                    $expdatefrom = explode('-', $value->fromdate);
                    $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                    $expdateto = explode('-', $value->todate);
                    $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                    $campval  = $value->campusid.'-'. $value->id; 
                    if (!empty($campusid) && $campval == $campusid) {
                     ?>
                     <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                   <?php  }else{ ?>
                     <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                   <?php } } ?>

                 </select> 
                 <?php echo form_error("campusid");?>
               </div>

               <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                <button type="submit" name="search" id="search" class="btn btn-dark btn-sm-10">Search</button> </div>

                
              </div>
            </form>
            <div class="row" style="margin-top: 20px;">
              <div class="col-md-12">
                <table id="tableWrittenScore"  class="table table-bordered responsive">
                  <thead>
                   <tr>
                    <th style ="max-width:50px;" class="text-center">S. No.</th>
                    <th>Category</th>
                    <th>Campus Name</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Email Id</th>
                    <th>Stream</th>
                    <th> Team </th>
                    <?php if ($this->loginData->RoleID !=17) { ?>
                      <th> FG </th>
                      <th> Batch </th>
                    <?php } ?>
                    <th style ="width:50px;" class="text-center">Step 1</th>
                    <th class="text-center"> Step 2</th>
                    <th class="text-center">Step 3 - Offer Letter</th>
                  </tr> 
                </thead>
                <tbody>
                  <?php  
                   //  print_r($selectedcandidatedetails); //die;

                  $i=0;
                  if($selectedcandidatedetails){
                   foreach ($selectedcandidatedetails as $key => $value) {
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td><?php echo $value->categoryname;?> </td>
                      <td><?php echo $value->campusname;?></td>
                      <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
                      <td><?php echo $value->gender;?></td>
                      <td><?php echo $value->emailid;?> </td>
                      <td><?php echo $value->stream;?> </td>
                      <td><?php echo $value->officename;?></td>
                      <?php if ($this->loginData->RoleID !=17) { ?>
                        <td><?php echo $value->name;?></td>
                        <td><?php echo $value->batch;?></td>

                      <?php } ?>

                      <?php if(!empty($value->letterid)){ ?>
                        <td class="text-center">
                          <a href="<?php echo site_url()."Assigntc/edit/".$value->candidateid;?>" >Assigned Team </a> </td>

                        <?php } elseif(!empty($value->officename)){ ?>
                          <td class="text-center">
                            <a href="<?php echo site_url()."Assigntc/edit/".$value->candidateid;?>" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i>Assigned Team </a> </td>
                          <?php   }else{ ?>
                            <td class="text-center">
                              <a href="<?php echo site_url()."Assigntc/add/".$value->candidateid;?>" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom">Assigned Team </a> </td>
                            <?php  } ?>
                            <?php if ($value->flag =='Generate') { ?>
                              <td class="text-center">   <a href="<?php echo site_url().'pdf_offerletters/'.$value->filename.'.pdf';?>" class="btn-primary  btn-xs" target="_blank" data-toggle="tooltip" title="Download offer letter">
                               Download Offer letter
                             </a></td>
                           <?php } else{ ?> 
                             <td class="text-center">  
                              <button type="button" class="btn-primary  btn-xs" data-toggle="tooltip" title="Generate offer letter" name="Generateofferletter" id="Generateofferletter_<?php echo  $i+1;  ?>"  
                                onclick="GenerateOfferLetter(this.id)" value="SENDTOED">Generate Offer letter</button> </td>
                              <?php } ?>

                              <td class="text-center">  <input type="hidden" name="candidate" id="candidateid_<?php echo $i+1;?>" value="<?php echo $value->candidateid;?>" >
                                <button type="button" class="btn-primary  btn-xs" name="sendtord" id="sendtoed_<?php echo  $i+1;  ?>"  onclick="SendTOEDOfferLetter(this.id)" value="SENDTOED" data-toggle="tooltip" title="Send to ED">Send to ED</button>
                              </td>

                            </tr>
                            <?php $i++; } } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>

                  </div>
                </form> 
              </div>
            </div>
          </div>  
        <?php } } ?> 
      </section>
      <!--  JS Code Start Here  -->
      <script type="text/javascript">
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();  
          $('#tableWrittenScore').DataTable({
           "bPaginate": false,
           "bInfo": false,
           "bFilter": true,
           "bLengthChange": false
         }); 

         // $('#Generateofferletter_3').attr('disabled','disabled');
         // $('#Generateofferletter_4').attr('disabled','disabled');
        // $('#sendtoed_3').attr('disabled','disabled');
        // $('#sendtoed_4').attr('disabled','disabled');
      });


        function GenerateOfferLetter(id){

          var indno =  id.replace('Generateofferletter_','');
          var candidateid = $('#candidateid_'+indno).val();

          $.ajax({
            url: '<?php echo site_url(); ?>Ajax/getGenerateOfferLetter/'+ candidateid,
            type: 'POST',
            dataType: 'text',
          })

          .done(function(data) {
            console.log(data);
            if (data==0) {
             alert("First !!! Please Choose Team !!");
           }else if(data ==1){
            alert("Offer Letter Already Generated !!");
          }else if(data ==2){
            window.location.replace("<?php echo site_url("Generateofferletter/offerletterdetails/");?>/"+ candidateid);
          }

        })
        }

        function SendTOEDOfferLetter(id){

         var indno =  id.replace('sendtoed_','');
         var candidateid = $('#candidateid_'+indno).val();
       //  alert(candidateid);

         $.ajax({
         
          url: '<?php echo site_url(); ?>Assigntc/sendofferlettered/'+ candidateid,
          type: 'POST',
          dataType: 'text'
        })

         .done(function(data) {
                 //alert(data);
                 if (data=='send') {
                   alert('Send Offer Letter To ED');
                 }else if(data=='sent'){
                  alert('Already send Offer Letter To ED ');
                }else if(data=='fail'){
                  alert('Please Try Again Email Not send');
                }else if (data=='notgenerate') {
                  alert('Please !! First Generate Offer Letter !!!');
                }

              });

       }

     </script>

<!--  JS Code End Here  -->