<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php ?>
        <form name="formteam" id="formteamid" method="POST" action="">
         <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-sm-offset-6">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel panel-default" >
               <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-7 panel-title pull-left">Placement Change</h4>
                 <div class="col-md-5 text-right" style="color: red">
                  * Denotes Required Field 
                </div>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group" >
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Choose New Team <span style="color:red;">*</span></label>
                    <select name="team"  id="team" class="form-control">
                      <?php foreach($teamsdetails as $row) {
                        if($row->officeid == $stafftransdetail->teamid){
                          ?>
                          <option value="<?php echo $row->officeid; ?>" Selected><?php echo $row->officename; ?> </option>
                          <?php }else {?>
                          <option value="<?php echo $row->officeid; ?>" ><?php echo $row->officename; ?> </option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="form-line">
                       <label for="fieldguide" class="field-wrapper required-field">Choose New Field Guide <span style="color:red;">*</span></label>
                       <select class="form-control" Name="fieldguide" id="fieldguide">
                        <?php foreach($teameFGdetail as $row1) {
                          if($row1->staffid== $stafftransdetail->fgid){
                            ?>
                            <option value="<?php echo $row1->staffid; ?>" Selected><?php echo $row1->name; ?> </option>
                            <?php }else {?>
                            <option value="<?php echo $row1->staffid; ?>" ><?php echo $row1->name; ?> </option>
                            <?php } } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="StateNameEnglish" class="field-wrapper required-field">Expected Date of Joining <span style="color:red;">*</span></label>
                          <input type="text" name="effective_date" id="effective_date" class="form-control datepicker" value="" required="required">
                        </div>
                        <?php echo form_error("effective_date");?>
                      </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right"> 
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                      <a href="<?php echo site_url("Changefieldguide");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
    <script type="text/javascript">
      $(document).ready(function() {
        $("#effective_date").datepicker({
          changeMonth: true,
          changeYear: true,
          minDate: 'today',
          dateFormat : 'dd/mm/yy',
          yearRange: '1920:2030',
        });


        $.ajax({
          url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+ $("#team").val(),
          type: 'POST',
          dataType: 'text',
        })
        .done(function(data) {
          console.log(data);
          $("#fieldguide").html(data);
        })

        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
        $.ajax({
          url: '<?php echo site_url(); ?>ajax/getReportingto/'+$("#team").val(),
          type: 'POST',
          dataType: 'text',
        })
        .done(function(data) {

          console.log(data);
          $("#reportingto").html(data);
        })

        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });


      });


      $("#team").change(function(){
        $.ajax({
          url: '<?php echo site_url(); ?>ajax/getReportingto/'+$(this).val(),
          type: 'POST',
          dataType: 'text',
        })
        .done(function(data) {
          console.log(data);
          $("#reportingto").html(data);
        })

        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });



        $.ajax({
          url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
          type: 'POST',
          dataType: 'text',
        })
        .done(function(data) {
          console.log(data);
          $("#fieldguide").html(data);
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
      });
    </script>