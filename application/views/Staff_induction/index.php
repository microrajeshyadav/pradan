
<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Staff_induction" && $row->Action == "index"){ 
    
    ?>
 <br>

  <div class="container-fluid">
    <?php 

    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <?php  ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Verify Joning Induction</h4>
     
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
            <table id="tblstate" class="table table-bordered table-striped table-hover dataTable js-exportable">
              <thead>
                <tr>
                  <th class="text-center" style="width: 50px;">Staff Name.</th>
                  <th style="max-width: 50px;" >Employ Code</th>
                  
                  <th class="text-center" style="width: 50px;">Action</th>
                </tr>
              </thead>
              <tbody>
                
                <tr>
                  
                  <td class="text-center" style="width: 50px;"><?php echo $verify_details->name;?></td>
                  <td style="max-width: 50px;"><?php echo $verify_details->emp_code;?></td>
                  <td class="text-center">
                    <a data-toggle="tooltip" data-placement="bottom" title = "Click here to vrify  this staff A/c."href="<?php echo site_url('Employee_particular_form/index/'.$verify_details->staffid.'/'.$verify_details->candidateid);?>" style="padding : 4px;" title="Edit"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> |

                  

                  </td>
                </tr>
                
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>
<script>

  $(document).ready(function() {
    $('#tblstate').DataTable({
      "paging": false,
      "search": false,
    });
  });

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this State ?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>