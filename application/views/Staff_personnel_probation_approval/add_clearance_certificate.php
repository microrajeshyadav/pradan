
<form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >
  <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">

      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
            <h4 class="col-md-8 panel-title pull-left"></h4>
           <div class="col-md-4 text-right" style="color: red">
            * Denotes Required Field 
          </div>
        </div>
        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel"> 
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              

                <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
                <p style="text-align: center;"><strong>Clearance Certificate on Transfer</strong></p>
                <p style="text-align: center;">&nbsp;</p>
                <div class="row">
                  <div class="col-xs-12">
                    <table width="600">
                      <tbody>
                        <tr>
                          <td width="200">
                            <p><b>Name : </b></p>
                          </td>
                          <td width="400">
                             <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php echo $getsinglestaff->name; ?></label>
                          </td>
                        </tr>
                        <tr>
                          <td width="200">
                            <p><b>Employee Code :</b></p>
                          </td>
                          <td width="400">
                             <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php  echo $getsinglestaff->emp_code; ?></label>
                          </td>
                        </tr>
                        <tr>
                          <td width="200">
                            <p><b>Designation :</b></p>
                          </td>
                          <td width="400">
                            <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php echo $getsinglestaff->sepdesig; ?></label>
                        </tr>
                        <tr>
                          <td width="200">
                            <p><b>Transferred from :</b></p>
                          </td>

                          <td width="400">
                            <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php  echo $getsinglestaff->joindesig; ?></label>
                          </td>
                        </tr>
                        <tr>
                          <td width="200">
                            <p><b>Transferred to :</b></p>
                          </td>

                          <td width="400">
                             <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php  echo $getsinglestaff->newoffice; ?></label>
                          </td>
                        </tr>
                        <tr>
                          <td width="200">
                            <p><b>Date of Release :</b></p>
                          </td>

                          <td width="400">
                            <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php  echo $this->gmodel->changedatedbformate($getsinglestaff->dateoftransfer); ?></label>
                          </td>
                        </tr>
                        <tr>
                          <td width="200">
                            <p><b>Letter of Transfer No. :</b></p>
                          </td>

                          <td width="400">
                            <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php  echo $getstafflist->transferno; ?></label>
                          </td>
                            <tr>
                          <td width="200">
                            <p><b>Letter of Transfer Date :</b></p>
                          </td>

                          <td width="400">
                             <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                           <?php  echo  $this->gmodel->changedatedbformate($getstafflist->letter_date); ?></label>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <p>&nbsp;</p>
                <p><strong>No Dues Certificate</strong></p>
                <p>&nbsp;</p>
                <p>This is to certify that <input type="text" name="certify_that" id="certify_that"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getsinglestaff->name; ?>" required="required" readonly> has cleared all her/his dues as on the date of her/his release, <em>except </em>the items shown in the respective columns. S/he may be released from duty after adjusting the cost of items/articles outstanding against her/him and upon her/his completing the other requisite formalities:</p>
                <p>&nbsp;</p>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                 <table id="tblForm10" class="table table-bordered table-striped">
                  <thead>
                   <tr class="bg-light">
                    <th colspan="4"> Items

                      <div class="col-lg-6 text-right pull-right">
                        <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                        <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
                      </div>
                    </th>
                  </tr> 
                  <tr>
                   <th class="text-center" style="vertical-align: top;">Name of Location </th>
                   <th class="text-center" style="vertical-align: top;">Description of outstanding Items/Articles and Their Value, Wherever Known.&nbsp; In Other Cases, Please Give Details</th>
                   <th class="text-center" style="vertical-align: top;">Value 
                    (if known)
                  </th>
                </tr> 
              </thead>
              <tbody id="bodytblForm10">

                <tr id="bodytblForm10">
                  <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="Name of Location !" minlength="5" maxlength="50"
                    id="name_of_location"  maxlength="150" name="name_of_location[]" placeholder="Enter name of location" style="min-width: 20%;"  
                    value="<?php echo set_value('items');?>"  ></td>
                    <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title=" Description!" 
                      id="description" name="description[]"  placeholder="Enter Description" style="min-width: 20%;" value="" ></td>
                      <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="150" title="Value !" 
                        id="value" name="value[]"  placeholder="Enter value" style="min-width: 20%;" value="" ></td>
                      
                        </tr>
                      </tbody>
                    </table>
                  </div>

                 <p>&nbsp;</p>
                  <p><strong>Note</strong>: Where the value of the item missing/damaged, etc., is not known, the Executive Director will decide the amount.</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>cc: - Personal Dossier (PD)</p>
                  <p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;- Finance-Personnel-MIS Unit</p>


                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer text-right">
                    <button type="submit" class="btn btn-success" name="btnsend" id="checkBtn" value="AcceptSaveData" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button> 
                     <button type="button" class="btn btn-primary" name="btnsubmit" id="btnsubmit" value="AcceptSendData" data-toggle="tooltip" title="Want to save your changes? Click on me." onclick="addclearencecertificate();">Save & Submit</button> 
                    <a href="<?php echo site_url().'Role_assign/'; ?>" class="btn btn-dark" name="btncancel" id="btncancel"data-toggle="tooltip" title="Click here to move on list.">Go to List</a>

                  </div>
              </div>
            </div>
          </div>   
        </section>
  <!-- Modal -->
  <div class="container">
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
               <label for="Name">Notes <span style="color: red;" >*</span></label>
               <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes"> </textarea>
                <?php echo form_error("comments");?>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Status <span style="color: red;" >*</span></label>

                          <select name="status" id="status" class="form-control" required="required"> 
                            <option value="">Select</option>
                          <?php if($this->loginData->RoleID == 2){ ?>
                            <option value="7">Approved</option>
                            <option value="8">Rejected</option>
                          <?php } ?>
                          </select>
                          <?php echo form_error("status");?>
                        </div>
              <div id="executivedirector_admin" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none;" >
                        <label for="Name">Personal Administrator<span style="color: red;" >*</span></label>

                          <select name="personal_administration" id="personal_administration" class="form-control" >
                            <option value="">Select</option>
                            <?php foreach ($personaldetail as $key => $value) {
                             ?>
                            <option value="<?php echo $value->edstaffid;?>">
                              <?php echo $value->name.' - '.'('.$value->emp_code.')';?>
                            </option>
                            <?php } ?>
                            </select>
                          <?php echo form_error("personal_administration");?>
                        </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="btnsubmit" id="btnsubmit" value="Save" class="btn btn-success btn-sm" onclick="addclearencecertificatemodal();">
          </div>
        </div>
        
      </div>
    </div>
  </div> 
  <!-- Modal -->
</form>
<script type="text/javascript">
  $('#status').change(function(){
    var statusval = $('#status').val();
    /*alert('sdfsaf');
    alert(statusval);*/
    if(statusval == 7) {
        $('#executivedirector_admin').show(); 
        $('#executivedirector_administration').attr('disabled',false)
        $('#personnel_administration').prop('required', true); 

    } else {
        $('#executivedirector_admin').hide(); 
          $('#executivedirector_administration').attr('disabled',true)
        $('#executivedirector_administration').prop('required', false); 

    } 
});
  function addclearencecertificate(){ 
    var name_of_location = document.getElementById('name_of_location').value; 
    var description = document.getElementById('description').value;  
    var value = document.getElementById('value').value; 
    //alert(name_of_location);

    if(value == ''){
      $('#value').focus();
    }
    
    if(description == ''){
      $('#description').focus();
    }

    if(name_of_location == ''){
      $('#name_of_location').focus();
    }
    
    if(name_of_location != '' && description !='' && value !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
function addclearencecertificatemodal(){
  var comments = document.getElementById('comments').value;
  var acceept = document.getElementById('status').value;
  var personal_administration = document.getElementById('personal_administration').value;
  //alert(comments);
  if(acceept == ''){
    $('#status').focus();
  }else if(acceept == 7 && personal_administration ==''){
    $('#personal_administration').focus();
  }
  if(comments.trim() == ''){
    $('#comments').focus();
  }


  if(comments.trim() !='' && acceept != '' && personal_administration !=''){
    document.forms['offcampuscandidate'].submit();
  }
}
</script>

      <script>
        $(document).ready(function(){
         $(".datepicker").datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '1980:2030',
          dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });
         $('[data-toggle="tooltip"]').tooltip();  
         $('#Inboxtable').DataTable(); 
       });


        $("#btntrainingexposureRemoveRow").click(function() {
          if($('#tblForm10 tr').length-2>1)
            $('#bodytblForm10 tr:last').remove()
        });

        $('#btntrainingexposureAddRow').click(function() {

          rowsEnter1 = parseInt(1);
          if (rowsEnter1 < 1) {
            alert("Row number must be minimum 1.")
            return;
          }
          Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });

        var srNoGlobal=0;
        var inctg = 0;

        function Insaettrainingexposure(count) {
          srNoGlobal = $('#bodytblForm10 tr').length+1;
          var tbody = $('#bodytblForm10');
          var lastRow = $('#bodytblForm10 tr:last');
          var cloneRow = null;

          for (i = 1; i <= count; i++) {
            inctg++
            cloneRow = lastRow.clone();
            var tableData1 = '<tr>'
            + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="name_of_location" name="name_of_location['+inctg+']" placeholder="Enter name of location " style="min-width: 20%;" value="" data-original-title="Name of Location !">'
            + '</td>'
            + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="description['+inctg+']" name="description['+inctg+']" placeholder="Enter Description" style="min-width: 20%;" value="" required="required" data-original-title="Items Description !"></td>'


            + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="150" title="" id="value['+inctg+']" name="value['+inctg+']" placeholder="Enter value" style="min-width: 20%;" value="" required="required" data-original-title="Value !"></td>'

            + '</tr>';
            $("#bodytblForm10").append(tableData1)

          }

        }

      </script>  


