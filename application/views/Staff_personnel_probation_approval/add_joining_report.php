<?php echo "pooja";?>
<div class="container">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <?php print_r($getstaffdetailslist); ?>
          
            <div class="row" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <div class="card" >
                  <div class="body" style="margin-left: 10px;">
                    <div class="row">
                     <h4 class="header" class="field-wrapper required-field" style="text-align: center;"> <label class="field-wrapper required-field">Professional Assistance for Development Action (PRADAN)  <br><br>JOINING REPORT AT NEW PLACE OF POSTING</label></h4>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
                     <form name="joiningreport" id="joiningreport" method="POST" action="">
                      <input type="hidden" name="token" value="<?php echo $token; ?>">
                      
                   <?php //print_r($candidatedetils); echo $candidatedetils->doj; ?>
                    <div class="row">
                     <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label class="field-wrapper required-field">
                       To, <br>
                       The Executive Director  <br>              
                       PRADAN <br>
                       E 1/A, Ground Floor and Basement <br>
                       Kailash Colony <br>
                       New Delhi - 110 048 <br>
                       Tel: 4040 7700 <br>
                     </label>
                   </div>
                   <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" ></div>
                 </div>

                 <div class="row">

                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                   <div class="form-group">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Subject: Joining Report   </label>
                  </div>
                </div>

                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="field-wrapper required-field">Sir, </label>
                </div>
              </div>
              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <label class="field-wrapper required-field">  In response to the Letter of Transfer No. 
                <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $getstaffdetailslist->transferno; ?>"> &nbsp&nbsp  dated &nbsp&nbsp<input type="text" name="offerdate" id="offerdate"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $this->gmodel->changedatedbformate($getstaffdetailslist->letter_date); ?>"  >I hereby report for duty today, forenoon of  <input type="text" maxlength="50" 
                name=" staff_duty_today_date" id="staff_duty_today_date" placeholder=" Please Enter Date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" value="<?php echo date('d/m/Y'); ?>" >(date).<br><br><br>

            </div>

               
        
        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
       <p> Thanking you<p>

<p >Yours sincerely,</p>
</div>
<div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
<table style="border-collapse: collapse;">
<tbody>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="508">
<p style="line-height: 150%; tab-stops: -67.5pt;">Signature: <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value=""></p>
</td>
<td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="text-align: right; line-height: 150%; tab-stops: -67.5pt;">Employee <span style="line-height: 150%;">Code</span>:<input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffdetailslist->emp_code; ?>"></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Name&nbsp;&nbsp;&nbsp;&nbsp; :
 <input type="text" name="offerno" id="offerno" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $getstaffdetailslist->name; ?>"></p>
</td>
<td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="margin-left: 74.9pt; line-height: 150%; tab-stops: -67.5pt;">Place :<input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value=""></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Designation: <input type="text" name="designation" id="designation"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $getstaffdetailslist->desname; ?>"></p>
</td>
<td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="text-align: right; line-height: 150%; tab-stops: -67.5pt;">Date&nbsp; : <input type="text" name="currentdate" id="currentdate" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo date('d/m/Y'); ?>"></p>
</td>
</tr>
</tbody>
</table>
</div>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p style="tab-stops: -1.5in -49.5pt;">--------------------------------------------------------------------------------------------------------------</p>

<p style="text-align: center; tab-stops: -1.5in -49.5pt;">(<strong>For Use of the Integrator/Team Coordinator </strong>)</p>

<p style="text-align: justify; tab-stops: -1.5in -49.5pt;">&nbsp;</p>

<p style="tab-stops: -1.5in -49.5pt;">
  Reported for duty on <input type="text" name="reporteddutyon" id="reporteddutyon" class="datepicker" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value=""><span style="font-size: 10.0pt;">(date)</span> forenoon.</p>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<p style="text-align: justify; tab-stops: -1.5in -49.5pt;">S/he has been assigned to<input type="text" name="assignedto" id="assignedto"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="">project/programme at <input type="text" name="location" id="location"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="">location and will work under the supervision of  <br><input type="text" name="supervision" id="supervision"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $supervisionname; ?>">&nbsp; (<span style="font-size: 10.0pt;">Name and Designation</span>).</p>
<p style="text-align: justify; tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<table style="border-collapse: collapse;">
<tbody>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Signature:</p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
  <input type="text" name="supervision_name" id="supervision_name" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $supervisionname; ?>"></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Date: <input type="text" name="currentdate" id="currentdate"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo date('d/m/Y'); ?>"></p>
</td>
</tr>
</tbody>
</table>

</div>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<p style="margin-left: .25in; text-indent: -.25in; tab-stops: -1.5in -49.5pt;">cc: - Employee on Transfer</p>
<p style="margin-left: .25in; tab-stops: -1.5in -49.5pt;">- Person under whose guidance s/he will work</p>
<p style="margin-left: .25in; tab-stops: -1.5in -49.5pt;">- Finance-Personnel-MIS Unit (for Personal Dossier after necessary action)</p>


 <div style="text-align: center;"> 
 <button  type="submit" name="savebtn" id="savebtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button>        
<button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>
         

         
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>


<script>
  $(document).ready(function(){

 $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate : 0,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });

    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    //$("#btnsubmit")..prop('disabled', true);

  });
</script>  


