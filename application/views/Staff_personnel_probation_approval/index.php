<section class="content" style="background-color: #FFFFFF;" >
  <?php  //foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "index"){ ?>
    <br>

    <div class="container-fluid">
     <?php 
     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');

     if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
         <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>


        <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Probation Approval List</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">

      <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
               <th style ="max-width:50px;" class="text-center">S. No.</th>
               <th>Emp code</th>
               <th>Name</th>
               <th>Old Office</th>
               <th>Proposed Transfer Office</th>
               <th>Proposed Transfer Date</th>
               <th>Reason</th>
               <th>Comments</th>
               <th>Sender Name</th>
               <th>Request Status</th>
               <th>Request Date</th>
               <th>Status</th>
               <th style ="max-width:50px;" class="text-center">Action</th>
             </tr> 
           </thead>
           <?php if (count($getprobationworkflowdetaillist) == 0) { ?>
           <tbody>
             <tr>
               <td colspan="15" style="color: red;">Record not found</td>
             </tr>
           </tbody>
        <?php }else{?>

           <tbody>
            <?php 

            $i=0;
            foreach ($getprobationworkflowdetaillist as $key => $value) {
              $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
              ?>
              <tr>
                <td class="text-center"><?php echo $i+1;?></td>
                <td><?php echo $value->emp_code;?></td>
                <td><?php echo $value->name;?></td>
                <td><?php echo $value->officename;?></td>
                <td><?php echo $value->newoffice;?> </td>
                <td><?php echo $proposed_date_bdformat;?> </td>
                <td><?php echo $value->reason;?></td>
                <td><?php echo $value->scomments;?></td>
                <td><?php echo $value->sendername; ?></td>
                <td><?php echo $value->flag; ?></td>
                <td><?php echo $value->Requestdate;?></td>
                <td><?php echo $value->process_type ;?></td>
                <td class="text-center">
                  <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>

                  <?php if ($value->status==1 || $value->status==3) { ?>
                    | <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Staff_approval/add/".$value->staffid.'/'.$value->transid;?>" >Approval</a>
                  <?php } else{ ?>

                  <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/add_clearance_certificate/".$value->transid;?>"> Clearance Certificate on Transfer</a>

                  <a class="btn btn-warning btn-xs" title = "Click here to joining report at New place of posting submitted." href="<?php echo site_url()."Staff_approval/list_joining_report/".$value->transid;?>">Joining Report</a>

                  <a class="btn btn-warning btn-xs" title = "Click here to joining report at New place of posting submitted." href="<?php echo site_url()."Joining_report_of_newplace_posting/add/".$value->transid;?>">Joining Report</a>
                <?php } ?>
                  
                </td>
              </tr>
              <?php $i++; } ?>
            </tbody>
          <?php } ?>
          </table>
        </div>
      </div> 
    </div>
  </div>
    <?php //print_r($getworkflowdetail);  ?>

        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Transfer Approval List</h4>
           </div>
           <hr class="colorgraph"><br>
         </div>

         <div class="panel-body">

          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Comments</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th>Status</th>
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($getworkflowdetail) == 0) { ?>
               <tbody>
                 <tr>
                   <td colspan="13" style="color: red;">
                     Record not found!!!
                   </td>
                 </tr>
               </tbody>
             <?php } else{ ?>

               <tbody>
                <?php
                 $i=0;
                foreach ($getworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername; ?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $value->Requestdate;?></td>
                    <td><?php echo $value->process_type ;?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>

                       <?php if ($value->status==1) { ?>
                        <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>">Approval</a>
                      <?php } ?>
                      <?php if ($value->status==5) {  ?>
                      <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Stafftransfer/add_clearance_certificate/".$value->transid;?>"> Clearance Certificate on Transfer</a>
                    <?php } ?>

                    <?php if ($value->status == 11) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to taking over charge." href="<?php echo site_url()."handed_over_taken_over_transfer/index/".$value->transid;?>"> TAKING OVER CHARGE</a>
                      <?php } ?>
                    <?php if ($value->status == 13) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to Joining report of newplace posting." href="<?php echo site_url()."Joining_report_of_newplace_posting/add_transfer/".$value->transid;?>"> JOINING REPORT AT NEW PLACE OF POSTING</a>
                      <?php } ?>
                      <?php if ($value->status == 17) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to transfer claim expance view." href="<?php echo site_url()."transfer_claim_expense/transfer/".$value->transid;?>"> TRANSFER EXPENSES CLAIM</a>
                        <!-- <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" onclick="approvemodalshowtransfer(<?php echo $value->transid; ?>);">Approval</a> -->
                        <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>">Approval</a>
                      <?php } ?> 
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              <?php } ?>
              </table>
            </div>
          </div> 
        </div>
      </div>

       <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Change Responsibility Approval List</h4>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
         <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Old Designation</th>
                   <th>Proposed Transfer Designation</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Remark</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th>Status</th>
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($getpromationworkflowdetail)==0) {?>
                 <tbody>
                  <tr>
                    <td colspan="15" style="color: red;">Record not found</td>
                  </tr>
                </tbody>
              <?php  }else{ ?>
               <tbody>
                <?php 
                   // print_r($getpromationworkflowdetail);
                $i=0;
                foreach ($getpromationworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $value->desnameold;?> </td>
                    <td><?php echo $value->desnewname;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername; ?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $value->Requestdate;?></td>
                    <td><?php echo $value->process_type ;?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>
                      <?php if($value->status==5){ ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Changeresponsibility_handed_over_taken_over_encourage/index/".$value->transid;?>"> Handed Over </a>
                       <?php } ?>
                      <?php if ($value->status==1) { ?>
                        | <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Staff_approval_changerespons/add/".$value->staffid.'/'.$value->transid;?>" >Approval</a>
                      <?php } elseif($value->status==27 && $this->loginData->RoleID==2){ ?>
 <a class="btn btn-warning btn-xs" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Changeresponsibility_taken_over_encourage/index/".$value->transid;?>"> Taken over  </a>
                      <?php }else{ ?>
        <!--   <a class="btn btn-warning btn-xs" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Change_responsibilitypart/index/".$value->transid;?>"> Change Responsibility </a> -->

                      <?php } ?>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              <?php } ?>
            </table>
          </div>
        </div> 
      </div>
    </div>



      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Transfer & Change Responsibility Approval List</h4>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
         <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Old Designation</th>
                   <th>Proposed Transfer Designation</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Remark</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th>Status</th>
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($gettransferpromationworkflowdetail)==0) {?>
                 <tbody>
                  <tr>
                    <td colspan="15" style="color: red;">Record not found</td>
                  </tr>
                </tbody>
              <?php  }else{ ?>
               <tbody>
                <?php 
                   // print_r($gettransferpromationworkflowdetail); die;
                $i=0;
                foreach ($gettransferpromationworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $value->desnameold;?> </td>
                    <td><?php echo $value->desnewname;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername; ?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $value->Requestdate;?></td>
                    <td><?php echo $value->process_type ;?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>">History</a>
                      <?php if ($value->status==2) { ?>
                      <a class="btn btn-warning btn-xs" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"> Change Responsibility </a>

                      <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/add_clearance_certificate/".$value->transid;?>"> Clearance Certificate on Transfer</a>

                      <a class="btn btn-warning btn-xs" title = "Click here to joining report at New place of posting submitted." href="<?php echo site_url()."Staff_approval/list_joining_report/".$value->transid;?>">Joining Report</a>

                      <a class="btn btn-warning btn-xs" title = "Click here to joining report at New place of posting submitted." href="<?php echo site_url()."Joining_report_of_newplace_posting/add/".$value->transid;?>">Joining Report</a>
                    <?php } ?>
                      <?php if ($value->status==1) { ?>
                        <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Stafftransferpromotion/approve/".$value->staffid.'/'.$value->transid;?>" >Approval</a>
                      <?php }  ?>
                      <?php if ($value->status==5) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to 
                     inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->transid;?>">  (IOM) FOR TRANSFER</a><br>
                        <a class="btn btn-warning btn-xs" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"> Change Responsibility </a>
                        <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/add_clearance_certificate/".$value->transid;?>"> Clearance Certificate on Transfer</a>
                      <?php } ?>
                      <?php if ($value->status==7) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/add_clearance_certificate/".$value->transid;?>"> Clearance Certificate on Transfer</a>
                      <?php } ?>
                      <?php if ($value->status == 11) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to taking over charge." href="<?php echo site_url()."handed_over_taken_over/index/".$value->transid;?>"> TAKING OVER CHARGE</a>
                      <?php } ?>
                      <?php if ($value->status == 13) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to JOINING REPORT AT NEW PLACE OF POSTING." href="<?php echo site_url()."Joining_report_of_newplace_posting/add/".$value->transid;?>"> JOINING REPORT AT NEW PLACE OF POSTING</a>
                      <?php } ?>
                      <?php if ($value->status == 17) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to transfer claim expance view." href="<?php echo site_url()."transfer_claim_expense/index/".$value->transid;?>"> TRANSFER EXPENSES CLAIM</a>
                        <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" onclick="approvemodalshow(<?php echo $value->transid; ?>);">Approval</a>
                      <?php } ?>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              <?php } ?>
            </table>
          </div>
        </div> 
      </div>
    </div>



    <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-7 panel-title pull-left">Seperation Approval List</h4>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
                <table id="staff_transfer_history" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.NO</th>
                      <th class="text-center">Seperate Date</th>
                      <th class="text-center">Staff</th>
                      <th class="text-center">Type</th>
                      <th class="text-center">Sender</th>
                      <th class="text-center">Recevier</th>
                      <th class="text-center">Request Date</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Comment</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <?php if (count($staff_seperation)==0) { ?>
                  <tbody>
                    <tr>
                      <td colspan="9" style="color: red;">Record not found !!!</td>
                    </tr>
                  </tbody>
                <?php }else{ ?>
                  <tbody>
                    <?php
                    $i=0; foreach($staff_seperation as $grow){ 
                  //print_r($view_history);
                      ?>
                      <tr>
                        <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                        <td class="text-center"><?php echo $this->gmodel->changedatedbformate($grow->proposeddate);?></td>
                        <td class="text-center"><?php echo $grow->name; ?></td>
                        <td class="text-center"><?php echo $grow->process_type; ?></td> 
                        <td class="text-center"><?php echo $grow->sendername; ?></td> 
                        <td class="text-center"><?php echo $grow->receivername; ?></td> 
                        <td class="text-center"><?php echo $this->gmodel->changedatedbformate($grow->Requestdate); ?> </td> 
                        <td class="text-center"><?php echo $grow->flag;?> </td> 
                         <td class="text-center"><?php echo $grow->scomments;?> </td> 
                        <td class="text-center">
                          <?php if($grow->status == 1){ ?>
                          <a href="<?php echo base_url('Staff_approval/add/'.$grow->staffid.'/'.$grow->transid);?>" class="btn btn-success btn-sm">Approve</a>
                          <?php 
                            }
                            if($grow->status == 11){
                          ?>
                          <a href="<?php echo base_url('Staff_sepdischargenotice/index/'.$grow->transid);?>" class="btn btn-success btn-sm"> DISCHARGE NOTICE</a>
                          <a href="<?php echo base_url('Staff_sepservicecertificate/index/'.$grow->transid);?>" class="btn btn-success btn-sm">SERVICE CERTIFICATE</a>
                          <a href="<?php echo base_url('Staff_sepclearancecertificate/index/'.$grow->transid);?>" class="btn btn-success btn-sm">CLEARANCE CERTIFICATE</a></td>
                        <?php } ?>
                      </td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  <?php  } ?>
                  </table>
                </div>
            </div>
 
</div>   
<?php //} } ?>
</section>
   <!-- Modal -->
  <div class="container">
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <form action="<?php echo site_url(); ?>Staff_approval/approvemodal" id="acceptancetcform" name="acceptancetcform" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <input type="hidden" id="receiverstaffid" name="receiverstaffid" value="">
               <label for="Name">Notes <span style="color: red;" >*</span></label>
               <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments"  require> </textarea>
                <?php echo form_error("comments");?>
              </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetcmodal();">
          </div>
        </div>
       </form> 
      </div>
    </div>
  </div> 
  <!-- Modal -->

   <!-- Modal Transfer Expense claim-->
  <div class="container">
    <div class="modal fade" id="myModaltransfer" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <form action="<?php echo site_url(); ?>Staff_approval/transferapprovemodal" id="acceptancetcform_transfer" name="acceptancetcform_transfer" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" id="receiverstaffid_transfer" name="receiverstaffid_transfer" value="">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments_transfer" name="comments_transfer" placeholder="Enter Comments"  required> </textarea>
              <?php echo form_error("comments_transfer");?>
            </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetransfermodal();">
          </div>
        </div>
       </form> 
      </div>
    </div>
  </div> 
  <!-- Modal -->

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });
  function approvemodalshow(staffid){
    document.getElementById("receiverstaffid").value = staffid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
  function approvemodalshowtransfer(staffid){
    document.getElementById("receiverstaffid_transfer").value = staffid;
    $('#myModaltransfer').removeClass('fade');
    $("#myModaltransfer").show();
  }
      function acceptancetcmodal(){
        var comments = document.getElementById('comments').value;
        if(comments.trim() == ''){
          $('#comments').focus();
        }
        if(comments.trim() !=''){
          document.forms['acceptancetcform'].submit();
        }
      }

      function acceptancetransfermodal(){
        var comments = document.getElementById('comments_transfer').value;
        if(comments.trim() == ''){
          $('#comments_transfer').focus();
        }
        if(comments.trim() !=''){
          document.forms['acceptancetcform_transfer'].submit();
        }
      }
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>