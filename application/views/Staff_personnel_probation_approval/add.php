<section class="content">

  <div class="container-fluid">
   <?php 
    
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
   <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
       <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
           
               <form name="approve_rejected" action="" method="post" >
                <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-7 panel-title pull-left">Approved/Rejected</h4>
                     <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>

                  <div class="panel-body">
                  <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for="Name">Status <span style="color: red;" >*</span></label>
                  <select name="status" id="status" class="form-control" required="required"> 
                    <option value="">Select</option>
                    <option value="1">Initiate</option>
                  </select>
                  <?php echo form_error("status");?>
                </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Notes <span style="color: red;" >*</span></label>
                        <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes" maxlength="250"> </textarea>
                         <?php echo form_error("comments");?>
                      </div>
                   </div>
                  </div>
                   <div class="panel-footer text-right">
                   <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                   <a href="<?php echo site_url().'Staff_personnel_probation/';?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                 </div> 
                 
               </div><!-- /.panel-->
             </form> 

       </div>
       <!-- #END# Exportable Table -->
     </div>
   </div>
  </section>
 