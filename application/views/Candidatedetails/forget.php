<?php 
$tr_msg= $this->session->flashdata('tr_msg');
$log_error= $this->session->flashdata('log_error');

if(!empty($tr_msg)){ ?>
<div class="content animate-panel">
    <div class="row">
        <div class="col-md-12">
            <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else if(!empty($log_error)){?>
    
    <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('log_error');?>. </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <title>Sign In | Pradan</title>
            <!-- Favicon-->
            <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">

            <!-- Google Fonts -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

            <!-- Bootstrap Core Css -->
            <link href="<?php echo site_url('common/backend/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

            <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />


            <!-- Waves Effect Css -->
            <link href="<?php echo site_url('common/backend/vendor/node-waves/waves.css');?>" rel="stylesheet" />

            <!-- Animation Css -->
            <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />

            <!-- Custom Css -->
            <link href="<?php echo site_url('common/backend/css/style.css');?>" rel="stylesheet">
        </head>

        <body class="login-page">
            <div class="login-box">
                <div class="logo text-center">
                  <img src="<?php echo site_url('common/backend/images/logo.png');?>"  alt="logo" class="text-center" /> 
                    <!-- <small>Admin BootStrap Based - Material Design</small> -->
                </div>
                
                <div class="card">
                    <div class="body">

                        <?php 
                        $er_msg = $this->session->flashdata('er_msg');
                        if ($er_msg != NULL) { ?>
                        <div class="msg"><?php echo $er_msg; ?></div>
                        <?php }else{ ?>
                        <div class="msg">Reset Password</div>
                        <?php } ?>


                        <form action="<?php echo base_url('login/forgetpassword');?>" method="post">

                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="email" class="form-control" name="EmailID" placeholder="Enter Email Id" required autofocus>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-xs-4">
                                    <button class="btn btn-block bg-green waves-effect" style="margin-left: 136%;" type="submit">Submit</button>
                                    <a href="<?php echo base_url('login');?>" class="previous">&laquo; Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            

            <!-- Jquery Core Js -->
            <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>

            <!-- Bootstrap Core Js -->
            <script src="<?php echo site_url('common/backend/vendor/bootstrap/js/bootstrap.js');?>"></script>

            <!-- Waves Effect Plugin Js -->
            <script src="<?php echo site_url('common/backend/vendor/node-waves/waves.js');?>"></script>

            <!-- Validation Plugin Js -->
            <script src="<?php echo site_url('common/backend/vendor/jquery-validation/jquery.validate.js');?>"></script>

            <!-- Custom Js -->
            <script src="<?php echo site_url('common/backend/js/admin.js');?>"></script>
            <script src="<?php echo site_url('common/backend/js/pages/examples/sign-in.js');?>"></script>
        </body>

        </html>