<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Pradan</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">

    <!-- Google Fonts -->
<!--     <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> -->

    <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

    <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />


    <!-- Waves Effect Css -->
    <link href="<?php echo site_url('common/backend/vendor/node-waves/waves.css');?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo site_url('common/backend/css/style.css');?>" rel="stylesheet">
    
    <!-- Jquery Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>
</head>

<body class="candidate-page">
<div class="row"> 
    <!--  Header Section   -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
        
    <!-- Top Bar -->
<nav class="navbar" style="box-shadow: none; background-color: #026d0a; ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo site_url('/Candidatedetails/');?>" style="color: #fff;"  >Pradan </a>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse">
    
</div>
</div>
</nav>
<!-- #Top Bar -->


    </div>

     <!--  Body Section   -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
    
    <!-- Exportable Table -->
        
            <div class="card">
              <div class="body">
                <div class="panel panel-default" >
                  <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Personal Info</b></div>
            <div class="panel-body">
            <form name="campus" action="" method="post" > 
            <div class="row">

               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">First Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control" value="<?php echo set_value("candidatefirstname");?>" placeholder="Enter First Name"  required="required">
                  <?php echo form_error("candidatefirstname");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Middle name </label>
                <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control" value="<?php echo set_value("candidatemiddlename");?>" placeholder="Enter Middle Name">
                <?php echo form_error("candidatemiddlename");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Last Name <span style="color: red;" >*</span></label>
                <input type="text"  minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="<?php echo set_value("candidatelastname");?>" required="required">
                 <?php echo form_error("candidatelastname");?>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control" value="<?php echo set_value("motherfirstname");?>" placeholder="Enter First Name" required="required">
                  <?php echo form_error("motherfirstname");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Middle name </label>
                <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control" value="<?php echo set_value("candidatemiddlename");?>" placeholder="Enter Middle Name">
                <?php echo form_error("mothermiddlename");?>
                </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Last Name <span style="color: red;" >*</span></label>
                <input type="text" minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="<?php echo set_value("candidatelastname");?>" required="required">
                 <?php echo form_error("motherlastname");?>
                </div>
             

             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Father's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control" value="<?php echo set_value("fatherfirstname");?>" placeholder="Enter First Name" required="required">
                  <?php echo form_error("fatherfirstname");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Middle name </label>
                <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control" value="<?php echo set_value("fathermiddlename");?>" placeholder="Enter Middle Name" >
                <?php echo form_error("fathermiddlename");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Last Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="<?php echo set_value("fatherlastname");?>" required="required">
                 <?php echo form_error("fatherlastname");?>
                </div>


                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Gender <span style="color: red;" >*</span></label>
               <?php 
               $options = array("1" => "Male", "2" => "Female");
                echo form_dropdown('gender', $options, set_value('gender'), 'class="form-control"');
                ?>
                  <?php echo form_error("gender");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Nationality  </label>
                <?php 
               $options = array("indian" => "Indian", "other" => "Other");
                echo form_dropdown('nationality', $options, set_value('nationality'), 'class="form-control"');
                ?>
                <?php echo form_error("nationality");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Marital Status </label>
                <?php 
               $options = array("1" => "Single", "2" => "Married");
                echo form_dropdown('maritalstatus', $options, set_value('maritalstatus'), 'class="form-control"');
                ?>
                <?php echo form_error("maritalstatus");?>
                </div>
                
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                <input type="date" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" id="dateofbirth" name="dateofbirth" placeholder="Enter Date Of Birth " value="<?php echo set_value("dateofbirth");?>" required="required">
                 <?php echo form_error("dateofbirth");?>
                </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>
                <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo set_value("emailid");?>" required="required" >
                 <?php echo form_error("emailid");?>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                 <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="<?php echo set_value("mobile");?>" placeholder="Enter Mobile No" required="required">
                  <?php echo form_error("mobile");?>
                </div>


            <table id="tbledubackground" class="table table-bordered table-striped">
              <thead>
             <tr>
              <th colspan="8" style="background-color: #e9e9e9;">Communication  Address  
              <div class="form-check">
              <input type="checkbox" class="form-check-input" id="filladdress">
              <label class="form-check-label" for="filladdress"></label>
            </div>
  
              </th>
           </tr> 
             <tr>
              <th class="text-center" colspan="4" style="vertical-align: top;">Present Mailing Address</th>
              <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Mailing Address</th>
             </tr> 
         </thead>
         <tbody>
         <tr>
        <td> <label for="Name">H.No/Street<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="presentstreet" id="presentstreet" class="form-control" 
          value="<?php echo set_value('presentstreet');?>" placeholder="Enter H.No/Street" required="required"></td>

        <td> <label for="Name">City<span style="color: red;" >*</span></label> </td>

        <td><input type="text" name="presentcity" id="presentcity" class="form-control" placeholder="Enter City" value="<?php echo set_value('presentcity');?>"  required="required"></td>
        
        <td><label for="Name">H.No/Street<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentstreet" id="permanentstreet" class="form-control" value="<?php echo set_value('permanentstreet');?>"  placeholder="Enter H.No/Street" required="required" ></td>
        <td><label for="Name">City<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentcity" id="permanentcity" placeholder="Enter City" class="form-control" value="<?php echo set_value('permanentcity');?>" required="required" ></td>
        </tr>
         <tr>
        <td><label for="Name">State<span style="color: red;" >*</span></label></td>
        <td>  <?php 
                  
                  $options = array('' => 'Select Present State');
                  foreach($statedetails as$key => $value) {
                      $options[$value->id] = $value->name;
                  }
                  echo form_dropdown('presentstateid', $options, set_value('presentstateid'), 'class="form-control" id="presentstateid"');
                  ?>

                  <?php echo form_error("presentstateid");?></td>
        <td><label for="Name">District<span style="color: red;" >*</span></label> </td>
        <td><input type="text" name="presentdistrict" id="presentdistrict" class="form-control" value="<?php echo set_value('presentdistrict');?>" required="required" placeholder="Enter District"></td>
        <td><label for="Name">State<span style="color: red;" >*</span></label></td>
        <td>  <?php 
                  
                  $options = array('' => 'Select Permanent State');
                  foreach($statedetails as$key => $value) {
                      $options[$value->id] = $value->name;
                  }
                  echo form_dropdown('permanentstateid', $options, set_value('permanentstateid'), 'class="form-control" id="permanentstateid"');
                  ?>

                  <?php echo form_error("permanentstateid");?></td>
        <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentdistrict" id="permanentdistrict" class="form-control" value="<?php echo set_value('permanentdistrict');?>" required="required" placeholder="Enter District" ></td>
        </tr>
         <tr>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="presentpincode" id="presentpincode" class="form-control txtNumeric" value="<?php echo set_value('presentpincode');?>" required="required" placeholder="Enter PinCode"></td>
        <td> </td>
        <td></td>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentpincode" id="permanentpincode" class="form-control txtNumeric" value="<?php echo set_value('permanentpincode');?>" required="required" placeholder="Enter PinCode"></td>
        <td></td>
        <td></td>
        </tr>
       </tbody>
     </table>
              
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">
                   
          <table id="tbledubackground" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th colspan="8" style="background-color: #e9e9e9;">Educational Background</th>
           </tr> 
             <tr>
              <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate</th>
             
               <th class="text-center" style="vertical-align: top;">Year </th>
              <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
              <th class="text-center" style="vertical-align: top;"> Board/ University</th>
              <th class="text-center" style="vertical-align: top;">Specialisation</th>
              <th class="text-center" style="vertical-align: top;">Place</th>
              <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
           </tr> 
         </thead>
         <tbody>
          <tr>
            <td><b>10th</b></td>
          
             <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" style="min-width: 20%;"  value="<?php echo set_value("10thpassingyear");?>" >
                 <?php echo form_error("10thpassingyear");?></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("10thschoolcollegeinstitute");?>" >
                 <?php echo form_error("10thschoolcollegeinstitute");?></td>
            
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="10thboarduniversity" name="10thboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("10thboarduniversity");?>" >
                 <?php echo form_error("10thboarduniversity");?></td>
             <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
              id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("10thspecialisation");?>" >
                 <?php echo form_error("10thspecialisation");?></td>

           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
              id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo set_value("10thplace");?>" >
                 <?php echo form_error("10thplace");?></td>
           
            <td><input type="text" class="form-control txtNumeric"  data-toggle="tooltip" title="Percentage !"
              id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo set_value("10thpercentage");?>"  >
                 <?php echo form_error("10thpercentage");?></td>
          </tr>
          <tr>
            <td><b>12th</b> </td>
            

            <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="12thpassingyear" name="12thpassingyear" placeholder="Enter Year" value="<?php echo set_value("12thpassingyear");?>" >
                 <?php echo form_error("12thpassingyear");?></td>

            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("12thschoolcollegeinstitute");?>" >
                 <?php echo form_error("12thschoolcollegeinstitute");?></td>
           
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("12thboarduniversity");?>" >
                 <?php echo form_error("12thboarduniversity");?></td>

             <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
              id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("12thspecialisation");?>" >
                 <?php echo form_error("12thspecialisation");?></td>

           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
              id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo set_value("12thplace");?>" >
                 <?php echo form_error("12thplace");?></td>
           
            <td><input type="text" class="form-control txtNumeric" 
              id="12thpercentage" name="12thpercentage" placeholder="Percentage" value="<?php echo set_value("12thpercentage");?>" >
                 <?php echo form_error("12thpercentage");?></td>
          </tr>


          <tr>
            <td><b>UG</b></td>
            
             <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="ugpassingyear" name="ugpassingyear" placeholder="Enter Year" value="<?php echo set_value("ugpassingyear");?>" >
                 <?php echo form_error("ugpassingyear");?></td>

            <td>
            <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute " value="<?php echo set_value("ugschoolcollegeinstitute");?>" >
                 <?php echo form_error("ugschoolcollegeinstitute");?>
            </td>

            <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="ugboarduniversity" name="ugboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("ugboarduniversity");?>" >
                 <?php echo form_error("ugboarduniversity");?></td>
               <td>
               <?php
              $options = array('' => 'Select');
               foreach($ugeducationdetails as $key => $value) {
                      $options[$value->ugname] = $value->ugname;
                  }
                  echo form_dropdown('ugspecialisation', $options, set_value('ugspecialisation'), 'class="form-control" data-toggle="tooltip" title="Board/University !"');
               ?>
             <?php echo form_error("ugspecialisation");?></td>

           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
              id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo set_value("ugplace");?>" >
                 <?php echo form_error("ugplace");?></td>
           
            <td><input type="text" class="form-control txtNumeric" 
              id="ugpercentage" name="ugpercentage" placeholder="Percentage" value="<?php echo set_value("ugpercentage");?>" >
                 <?php echo form_error("ugpercentage");?></td>
          </tr>
          <tr>
            <td><b>PG</b></td>
           
           <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="pgpassingyear" name="pgpassingyear" placeholder="Enter Year" value="<?php echo set_value("pgpassingyear");?>" >
                 </td>


            <td>  
                  <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>" >
               <?php echo form_error("pgschoolcollegeinstitute");?>
            
               </td>
            
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("pgboarduniversity");?>" >
                 </td>
               <td>

              <?php
              $options = array('' => 'Select');
                  foreach($pgeducationdetails as $key => $value) {
                      $options[$value->pgname] = $value->pgname;
                  }
                  echo form_dropdown('pgspecialisation', $options, set_value('pgspecialisation'), 'class="form-control"');
               ?>
                <?php echo form_error("pgspecialisation");?>   </td>

           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
              id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo set_value("pgplace");?>" >
                </td>
           
            <td><input type="text" class="form-control txtNumeric" 
              id="pgpercentage" name="pgpercentage" placeholder="Percentage" value="<?php echo set_value("pgpercentage");?>" >
                </td>
          </tr>

          <tr>
            <td><b>If Others,Specify</b></td>
          
             <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="otherpassingyear" name="otherpassingyear" placeholder="Enter Year" value="<?php echo set_value("otherpassingyear");?>" >
                </td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>" >
                </td>
           
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="otherboarduniversity" name="otherboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("otherboarduniversity");?>" >
                </td>
               <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
              id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("otherspecialisation");?>" >
                 </td>
            
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
              id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo set_value("otherplace");?>" >
                </td>
           
            <td><input type="text" class="form-control txtNumeric" 
              id="otherpercentage" name="otherpercentage" placeholder="Percentage" value="<?php echo set_value("otherpercentage");?>" >
                 </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                <button type="reset" class="btn btn-warning  btn-sm m-t-10 waves-effect">Reset </button>
               <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Save </button>
                <a href="<?php echo site_url("CandidatesInfo");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
            </div>  
      </div>
     </div>
                 </form> 
                </div>

              </div><!-- /.panel-->
            </div>
          </div>
     
    
      <!-- #END# Exportable Table -->







     </div>
   
     <!--  Foootr Section   -->

     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ></div>


 </div>  
</body>


<script type="text/javascript">
$('.btnNext').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.btnPrevious').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
</script>

<!-- Jquery Core Js -->
  

    <!-- Bootstrap Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/bootstrap/js/bootstrap.js');?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo site_url('common/backend/vendor/node-waves/waves.js');?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery-validation/jquery.validate.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo site_url('common/backend/js/admin.js');?>"></script>
    <script src="<?php echo site_url('common/backend/js/pages/examples/sign-in.js');?>"></script>

</html>