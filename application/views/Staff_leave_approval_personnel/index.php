 <!-- <?php print_r($leaverequest);?> -->
<section class="content">
	<br>
	<div class="container-fluid">
		<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');

		if(!empty($tr_msg)){ ?>
			<div class="content animate-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){?>
				<div class="content animate-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel thumbnail shadow-depth-2 listcontainer" >
							<div class="panel-heading">
								<div class="row">
									<h4 class="col-md-10 panel-title pull-left">Staff Leave Approval </h4>

								</div>
								<hr class="colorgraph"><br>
							</div>
							<div class="panel-body">
								<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
									<thead>
										<tr>
											<th  class="text-center" style="width: 45px;">S. No.</th>
											<th >Staff</th>
											<th >Designation </th>
											<th >Leave Type </th>
											<th> From Date</th>
											<th> To Date </th>
											<th> No Of Days</th>
											<th >Reason </th>
											<th >Status </th>
											<th id="hraction" class="text-center" style="width: 115px;">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php if (count($leaverequest) == 0) { ?>
										<tr>
									    <td colspan="10" class="text-center"> Record Not found!!! </td>
									</tr>

									<?php } else{ 
									
										$i=0; foreach($leaverequest as $row){ 
											?>
											<tr>
												<td class="text-center"><?php echo $i+1; ?></td>
												<td>
												<?php echo $row->Emp_code.'-'.$row->name; ?></td>
												<td><?php echo $row->sender_des; ?></td>
												<td><?php echo $row->Ltypename; ?></td>
												<td><?php echo $this->gmodel->changedatedbformate($row->From_date); ?></td>
												<td><?php echo $this->gmodel->changedatedbformate($row->To_date); ?></td>
												<td><?php echo $row->Noofdays; ?></td>
												<td><?php echo $row->reason; ?></td>
												<td class="text-center"> <?php if($row->stage == 1){   ?>
													<span class="btn btn-info btn-sm"> 
													<?php } ?>
								                       <?php if($row->stage == 2){        ////  2 reject case                    
								                       	?>
								                       	<span class="btn btn-danger btn-sm"> 
								                       	<?php } ?>
								                       <?php if($row->stage == 3){     //// 3 approved case                       
								                       	?>
								                       	<span class="btn btn-success btn-sm"> 
								                       	<?php } ?>
								                       <?php if($row->stage == 4){     //// 4 reverse case                      
								                       	;?>
								                       	<span class="btn btn-warning btn-sm"> 
								                       	<?php } ?>
								                       	<label class="badge">
								                       		<?php echo $row->status ;?> 
								                       	</label> </span>		
								                       </td>
								                       <td class="text-center">
				<a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Staff_leave_approval_personnel/sendsanctionletter/".$row->staffid;?>">Approved by personnel</a> 
								                       	</td>
								                   </tr>
								                   <?php $i++; } } ?>
								               </tbody>
								           </table>
								       </div>
								   </div>
								</div>
							</div>
						</section>
						<script>
							$(document).ready(function() {
								$('[data-toggle="tooltip"]').tooltip(); 
								$('#tbldesignations').DataTable({
									"paging": true,
									"search": true,
								});
							});
							function confirm_delete() {

								var r = confirm("Do you want to delete this Designations");

								if (r == true) {
									return true;
								} else {
									return false;
								}

							}
						</script>]