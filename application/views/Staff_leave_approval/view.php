<?php //print_r($id);?>
<section class="content">

  <div class="container-fluid">
   <?php 

   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
     <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

         <form name="approve" action="" method="post" >

          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-7 panel-title pull-left">Agree/Disagree</h4>
               <div class="col-md-5 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>

          <div class="panel-body">


            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="Name">Status <span style="color: red;" >*</span></label>
                <select name="status" id="status" class="form-control" required="required"> 
                  <option value="">Select</option>
                  <?php  if($this->loginData->RoleID==2 || $this->loginData->RoleID==16) {?>
                    <option value="1">Agree</option>

                    <option value="0">Disagree</option>
                  <?php } 

                  else 
                  {
                    if(($this->loginData->RoleID==17) && ($id->hrverify==1 || $id->supervisiorverify==1)) {

                      ?>

                      <option value="1">Agree</option>
                      <?php 
                    }
                    else {
                      ?>
                      <option value="0">Disagree</option>

                      <?php 
                    }
                  }
                  
                  ?>

                </select>

              </div>

            </div>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="Name">Comments/Reason <span style="color: red;" >*</span></label>
                <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments" maxlength="250" required="required"> </textarea>
                <?php echo form_error("comments");?>
              </div>
            </div>

           

         </div><!-- /.panel-->
          <div class="panel-footer text-right">
            
               <button  type="submit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save" name="savebtn"  id="save" value="save">Save</button>

               <a href="<?php echo site_url("Staff_leave_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
             
           </div> 


       </div>
       <!-- #END# Exportable Table -->

     </form> 
   </div>
 </div>
</section>





</script>
</script>