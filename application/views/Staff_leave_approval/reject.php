<?php echo "hello";?>
<section class="content">

  <div class="container-fluid">
   <?php 
    
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
   <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
       <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
           
               <form name="approve_rejected" action="" method="post" >
               <input type="hidden" name="tarnsid" value="<?php echo $tarnsid; ?>" > 
               <input type="hidden" name="staffid" value="<?php echo $staffid; ?>" >
                <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-7 panel-title pull-left">Rejected</h4>
                     <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>

                  <div class="panel-body">

                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Notes <span style="color: red;" >*</span></label>
                        <select name="r_purpose" id="r_purpose" class="form-control" required="required"> 
                          <option value="">Select The Purpose</option>
                        
                          <option value="3">work pressure</option>
                         
                        </select>
                        
                         <?php echo form_error("comments");?>
                      </div>
                     
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Status <span style="color: red;" >*</span></label>

                        <select name="status" id="status23" class="form-control" required="required"> 
                          <option value="">Select</option>
                        
                         
                          <option value="4">Rejected</option>
                        
                        </select>
                        <?php echo form_error("status");?>
                      </div>

                   

                      
                   </div>
                  </div>
                   <div class="panel-footer text-right">
                   <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
                   <a href="<?php echo site_url("Staff_leave_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                 </div> 
                 
               </div><!-- /.panel-->
             </form> 

       </div>
       <!-- #END# Exportable Table -->
     </div>
   </div>
  </section>
 <script type="text/javascript">

 $('#status').change(function(){
  var statusval = $('#status').val();
    /*alert('sdfsaf');
    alert(statusval);*/

        if(statusval == 3 || statusval == 5 || statusval == 7 || statusval == 9 || statusval ==17) {
            $('#executivedirector_admin').show(); 
            $('#executivedirector_administration').attr('disabled',false)
            $('#personnel_administration').prop('required', true);


        } else {
            $('#executivedirector_admin').hide(); 
              $('#executivedirector_administration').attr('disabled',true)
            $('#executivedirector_administration').prop('required', false); 


        } 
    });



  var originalPhoneNumber = "012-8976543";

  function isValid(p) {
    var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
    var digits = p.replace(/\D/g, "");
    return phoneRe.test(digits);
  }


  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
   }
 }, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});



</script>