<section class="content">

  <div class="container-fluid">
   <?php 

   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
     <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

         <form name="approve" action="" method="post" >

          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-7 panel-title pull-left">Approved/Rejected</h4>
               <div class="col-md-5 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>

          <div class="panel-body">


            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="Name">Status <span style="color: red;" >*</span></label>
                <select name="status" id="status" class="form-control" required="required"> 
                  <option value="">Select</option>
                  <option value="3">Approved</option>
                  <option value="2">Rejected</option>
                </select>

              </div>


              
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="Name">Select The Personal:</label>
                  <select name="p_status" class="form-control ">
                   <option value="">Select The Personal</option>
                   <?php foreach($personal as $value){?>
                     <option value="<?php echo $value->staffid;?>"><?php echo $value->Username;?></option>
                   <?php } ?>
                 </select>
               </div>

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="rurpurpose" style="display:none;">
                <label for="Name">purpose <span style="color: red;" >*</span></label>
                <select name="r_purpose" id="r_purpose" class="form-control" > 
                  <option value="">Select The Purpose</option>
                  <option value="3">work pressure</option>

                </select>

                <?php echo form_error("r_purpose");?>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="Name">Comments/Reason <span style="color: red;" >*</span></label>
                <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments" maxlength="250" required="required"> </textarea>
                <?php echo form_error("comments");?>
              </div>


              <div class="panel-footer text-right">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <button  type="button" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save" name="savebtn" onclick="leaveapproval();" id="save" value="save">Save</button>

               <a href="<?php echo site_url("Staff_leave_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
               </div> 
             </div> 

           </div><!-- /.panel-->


         </div>
         <!-- #END# Exportable Table -->

       </form> 
     </div>
   </div>
 </section>
 

 <script>
  $(document).ready(function(){
    $("#status").change(function(){

      var statuspur = $("#status").val();
      
      if(statuspur==2 ||statuspur==4)
      {
       $("#rurpurpose").hide();
     }

   });  
  });

  function leaveapproval(){
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }
    if(comments.trim() !=''){
      document.forms['approve'].submit();
    }
  }

</script>
</script>