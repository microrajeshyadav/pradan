


<section class="content">
<br>
<div class="container-fluid">
<?php 
$tr_msg= $this->session->flashdata('tr_msg');
$er_msg= $this->session->flashdata('er_msg');

if(!empty($tr_msg)){ ?>
<div class="content animate-panel">
<div class="row">
<div class="col-md-12">
<div class="hpanel">
<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo $this->session->flashdata('tr_msg');?>. </div>
</div>
</div>
</div>
</div>
<?php } else if(!empty($er_msg)){?>
<div class="content animate-panel">
<div class="row">
<div class="col-md-12">
<div class="hpanel">
<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo $this->session->flashdata('er_msg');?>. </div>
</div>
</div>
</div>
</div>
<?php } ?>


<!-- Exportable Table -->
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="panel thumbnail shadow-depth-2 listcontainer" >
<div class="panel-heading">
<div class="row">
<h4 class="col-md-10 panel-title pull-left">Staff Leave Approval </h4>

</div>
<hr class="colorgraph"><br>
</div>
<div class="panel-body">
<table id="tbldesignations" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
<thead>
<tr>
<th rowspan="2" class="text-center" style="width: 45px;">S. No.</th>
<th rowspan="2">Staff</th>
<th rowspan="2">Designation </th>
<th rowspan="2">Leave Type </th>
<th rowspan="2"> Maturnity Due  Date</th>
<th rowspan="2"> From Date</th>
<th rowspan="2"> To Date </th>
<th rowspan="2"> No Of Days</th>
<th rowspan="2">Reason </th>
<th> Supervisior Rec.</th>
<th >HR Rec.</th>
<th >Personal Rec.</th>
<th rowspan="2" >Status </th>

<th rowspan="2" id="hraction" class="text-center" style="width: 115px;">Action</th>
</tr>

<tr>

<th colspan="3" >(applicable for long leave only)</th>



</tr>
</thead>
<tbody>
<?php

if (!empty($leaverequest)) {
$i=0; foreach($leaverequest as $row){ ?>
<tr>
<td class="text-center"><?php echo $i+1; ?></td>
<td ><?php echo $row->Emp_code.'-'.$row->name; ?></td>
<td ><?php echo $row->sender_des; ?></td>
<td ><?php echo $row->Ltypename; ?></td>
<td ><?php echo $this->gmodel->changedatedbformate($row->maturnityduedate); ?></td>
<td ><?php echo $this->gmodel->changedatedbformate($row->From_date); ?></td>
<td ><?php echo $this->gmodel->changedatedbformate($row->To_date); ?></td>
<td ><?php echo $row->Noofdays; ?></td>
<td ><?php echo $row->reason; ?></td>
<td class="text-center setid1">
  <?php if($row->longleave) {?>
<a name="comm"  title="<?php echo $row->supervisiorview;?>" href='javascript:;' data-toggle="tooltip" id="setid" ><?php echo $row->supervisiorverify; ?></a><?php } ?>
</td>
<td class="text-center">
  <?php if($row->longleave) {?>
<a  title="<?php echo $row->hrview;?>" data-toggle="tooltip" name="comm" href='javascript:;'  id="hr_setid"  ><?php echo $row->hrverify; ?></a>
<?php } ?>

</td>
<td  class="text-center">
  <?php if($row->longleave) {?>
<a  name="comm" href='javascript:;'  id="per_setid" title="<?php echo $row->personalview;?>" data-toggle="tooltip" data-id="<?php echo $row->id;?>"><?php echo $row->personalverify; ?></a>
<?php } ?>

 </td>
<td class="text-center"> <?php if($row->stage == 1){   ?>
<span class="btn btn-info btn-sm"> 
<?php } ?>
           <?php if($row->stage == 2){        ////  2 reject case                    
           	?>
           	<span class="btn btn-danger btn-sm"> 
           	<?php } ?>
           <?php if($row->stage == 3){     //// 3 approved case                       
           	?>
           	<span class="btn btn-success btn-sm"> 
           	<?php } ?>
           <?php if($row->stage == 4){     //// 4 reverse case                      
           	;?>
           	<span class="btn btn-warning btn-sm"> 
           	<?php } ?>
           	<label class="badge">
           		<?php echo $row->status ;?> 
           	</label> </span>		


           </td>


           <td class="text-center">

           	<?php 
           

           	if ($row->flag ==1 ) { 

           		if($row->longleave==1)

              {



           			

                if($row->supervisiorverify=='No view' && $row->hrverify=='No view' && $row->personalverify=='No view'  && $this->loginData->RoleID==17 ||$this->loginData->RoleID==16||$this->loginData->RoleID==2){
                  ?>

                  <a href="<?php echo base_url().'Staff_leave_approval/view/'.$row->staffid.'/'.$row->id;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to leave Approval." class="btn btn-dark btn-sm">View</a>
                  <?php 
                }
                else if(($row->supervisiorverify=='No' || $row->supervisiorverify=='Yes') &&  ($this->loginData->RoleID==16)){
                  ?>
                   <a href="<?php echo base_url().'Staff_leave_approval/view/'.$row->staffid.'/'.$row->id;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to leave Approval." class="btn btn-dark btn-sm">View</a>
                  <?php

                }
                else if(($row->hrverify=='No' || $row->hrverify=='Yes') &&  ($this->loginData->RoleID==2)){
                  ?>
                   <a href="<?php echo base_url().'Staff_leave_approval/view/'.$row->staffid.'/'.$row->id;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to leave Approval." class="btn btn-dark btn-sm">View</a>
                  <?php

                }
 
                else if(($row->supervisiorverify=='No' || $row->hrverify=='Yes') && ($row->hrverify=='Yes' || $row->hrverify='No') && ($this->loginData->RoleID==17)){
                  ?>
                   <a href="<?php echo base_url().'Staff_leave_approval/view/'.$row->staffid.'/'.$row->id;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to leave Approval." class="btn btn-dark btn-sm">View</a>
                  <?php

                }

           			else if($row->personalverify=='Yes' && $this->loginData->RoleID==18){
           				?>

           			<a href="<?php echo base_url().'Staff_leave_approval/approve/'.$row->staffid.'/'.$row->id;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to leave Approval." class="btn btn-dark btn-sm">Approve/Reject</a>
           				<?php 
           			}

           			




           		}

           		else  
           		{


           			if (($this->loginData->RoleID==2 || $this->loginData->RoleID==18) && ($row->longleave==0)) { 

           				?>

           				<a href="<?php echo base_url().'Staff_leave_approval/approve/'.$row->staffid.'/'.$row->id;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to leave Approval." class="btn btn-dark btn-sm">Approve/Reject</a>
           			<?php  }?>
           		</td>

           	<?php } }

            else { ?>
           		<div class="badge badge-pill badge-success">
           			<strong>Approved</strong>

                
           		</div> | 
              <?php if(!empty($row->filename))
                                    { ?>
                                      <a target="_blank" href="<?php echo base_url().'pdf_offerletters/'.$row->filename.'.pdf'; ?>" class="btn-primary">
                                        <i class="fa fa-file-pdf fa-2x" aria-hidden="true"></i>
                                    <?php } else{ ?>
                                    <a title = "Click here to approval this request submitted." class="btn btn-primary btn-xs" onclick="sanctionltr(<?php echo $row->Emp_code ?>,<?php echo $row->id;?>);">Generate sanction letter</a> 
                                    <?php }
 } ?>
           </tr>
           <?php $i++; } } ?>
       </tbody>
   </table>
</div>
</div>
</div>
</div>
</section>

   <!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="<?php echo site_url(); ?>Staff_leave_approval/sendsanctionletter" id="sanction" name="sanction" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Sanction Letter Details</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">           


              <input type="hidden" id="empcode" name="empcode" value="">
              <input type="hidden" id="mid" name="mid" value="">
              <div class="row" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <label for="Name">Description<span style="color: red;" >*</span></label>
              <textarea class="form-control" data-toggle="" id="descript" name="descript" placeholder="Enter Notes"> I am happy to approve your request for leave as leave with pay with effect from </textarea>
              <?php echo form_error("descript");?>
              
            </div>
          

          <div class="row" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <label for="Name">Fromdate<span style="color: red;" >*</span></label>
              <input type="text" placeholder="Enter Fromdate" autocomplete="off" name="fmdate" id="fmdate" class="datepicker form-control">
              <?php echo form_error("fmdate");?>
              

          </div>


          <div class="row" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <label for="Name">Todate<span style="color: red;" >*</span></label>
              <input autocomplete="off" placeholder="Enter Todate" type="text" name="tdate" id="tdate" class="datepicker form-control">
              <?php echo form_error("Tdate");?>
              

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" onclick="sanctionltrmodel();" value="Save" class="btn btn-success btn-sm">
          </div>
          
          
        </div>
      </form> 
    </div>
  </div>
</div> 
<!-- Modal --> 




<div  class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title"></h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">

<p id="view_command"><strong></strong> </p>


</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>

<div id="ex1" class="modal">
  <p>Thanks for clicking. That felt good.</p>
  <a href="#" rel="modal:close">Close</a>
</div>

<script type="text/javascript">


  function sanctionltr(emp_code,mid){
    document.getElementById("empcode").value = emp_code;
    document.getElementById("mid").value = mid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }

  function sanctionltrmodel(){
    var descript = document.getElementById('descript').value;
    var fmdate = document.getElementById('fmdate').value;
    var tdate = document.getElementById('tdate').value;
    if(descript == ''){
      $('#descript').focus();
    }
    if(fmdate == ''){
      $('#fmdate').focus();
    }
    if(tdate == ''){
      $('#tdate').focus();
    }
    if(descript !='' && fmdate !=''  && tdate !=''){
      document.forms['sanction'].submit();
    }
  }





$(document).ready(function() {
$('[data-toggle="tooltip"]').tooltip(); 
$('#tbldesignations').DataTable({
"paging": true,
"search": true,
});
$("a[name=comm]").click(function() {
//$('#comm').click(function(event){
  // alert("hello");
    // $("#myModal").modal('show');
// $('#myModal').modal();
    // $('#myModal').modal({
    //     show: 'false'
    // }); 

$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
  

  
  //alert(event.target.id);
 // $.ajax({
 //      url: '<?php //echo site_url(); ?>Ajax/getrecomand/'+ $(this).data('id'),
 //      type: 'POST',
 //      dataType: 'text',
 //    })
 //    .done(function(data) {
 //       $("#view_command").html(data);
 //     // alert("data="+data);
     

 //    });


})
});
function confirm_delete() {

var r = confirm("Do you want to delete this Designations");

if (r == true) {
return true;
} else {
return false;
}

}

</script>
