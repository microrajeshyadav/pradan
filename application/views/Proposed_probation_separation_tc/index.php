
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to Separation</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?> 
          <?php //print_r($getprobationdetails); die; ?>
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
            <div class="col-lg-6 col-md-6"></div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tableprodationsepration" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                  <th>S.No.</th>
                  <th>DA Name</th>
                  <th>Intemation Type</th>
                  <th>Comments</th>
                  <th class="text-center">Action</th>
                </tr> 
              </thead>
              <tbody>
                <?php  if (count($getprobationdetails) == 0) {
                  ?>
                  <?php }else{ ?>
                  <?php
                  $count = 1;
                  foreach($getprobationdetails as $row) { ?>
                  <tr>
                    <td><?php echo $count; ?></td> 
                    <td><?php if(!empty($row->name)) echo $row->name; ?></td>
                    <td><?php if(!empty($row->da_status)) echo $row->da_status; ?></td>
                    <td><?php if(!empty($row->scomments)) echo $row->scomments; ?></td>                
                    <td class="text-center">           
                      <a href="<?php echo site_url('Developmentapprentice_sepemployeeexitform/index/'.$row->transid);?>" class = "btn btn-danger btn-sm" title="Exit Interview Form" data-toggle="tooltip"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>  
                      <?php if($row->status >= 5 && !empty($row->tbl_da_clearance_certificate_id) && $row->tbl_da_clearance_certificate_flag == 1){ ?>
                      
                      <a href="<?php if(!empty($row->transid)) echo site_url('Developmentapprentice_sepchecksheet/index/'.$row->transid); ?>" class = "btn btn-info btn-sm" title="Check Sheet Form" data-toggle="tooltip">
                        <i class="fas fa-clipboard-check"></i></a>
                        
                        <a href="<?php if(!empty($row->tbl_da_clearance_certificate_id)) echo site_url('Developmentapprentice_sepclearancecertificate/view/'.$row->tbl_da_clearance_certificate_id); ?>" class = "btn btn-warning btn-sm" title="Clearance Certificate Form" data-toggle="tooltip">
                          <i class="fa fa-certificate" aria-hidden="true"></i>
                        </a>
                        <?php } ?>
                      </td>
                    </td>     
                  </tr>
                  <?php    $count ++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>



      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to graduate</h4>
           <div class="col-md-2 text-right">
           </div>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="row" style="background-color: #FFFFFF;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
          <div class="col-lg-6 col-md-6"></div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
            <table id="tableprodationgraduate" class="table table-bordered table-striped wrapper">
              <thead>
               <tr>
                <th>S.No.</th>
                <th>DA Name</th>
                <th>Intimation Type</th>
                <th>Comments</th>              
                <th class="text-center">Action</th>
              </tr> 
            </thead>        
            <?php  if (count($getgraduatedetails) == 0) {?>
            <tbody>
            </tbody>
            <?php }else{ ?>
            <?php
            $count = 1;
            foreach($getgraduatedetails as $row) { 
              ?>
              <tbody>
                <tr>
                  <td><?php echo $count; ?></td> 
                  <td><?php if(!empty($row->name)) echo $row->name; ?></td>
                  <td><?php if(!empty($row->intimation)) echo $row->intimation; ?></td>
                  <td><?php if(!empty($row->comment)) echo $row->comment; ?></td>

                  <td class="text-center">

                    <?php if($row->checksheetexist) {?>
                    <a href="<?php if(!empty($row->name)) echo site_url('Proposed_probation_separation_tc/dachecksheet/'.$row->transid);?>" class = "btn btn-info btn-sm" title="Check Sheet Form" data-toggle="tooltip"><i class="fas fa-clipboard-check"></i></a>
                    <?php } ?>
                    <?php if($row->clearancecertexist) {?>
                    <a href="<?php if(!empty($row->name)) echo site_url('Proposed_probation_separation_tc/daclearancecertificate/'.$row->id);?>" class = "btn btn-warning btn-sm" title="Clearance Certificate Form" data-toggle="tooltip"><i class="fa fa-certificate" aria-hidden="true"></i></a>
                    <?php } ?>
                  </td>     
                </tr>
                <?php if (isset($row->type) && $row->type==2) {
                 ?>
                 <tr>
                   <td colspan="7"><a href=""> Exit Interview Form</a></td>
                 </tr>
                 <tr>
                  <td colspan="7"><a href="">Check sheet form</a></td>
                </tr>
                <tr>
                  <td colspan="7"><a href="">Clearance certificate form</a></td>
                </tr>
              </tbody>
              <?php }   $count ++; } } ?>

            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to Tranfer</h4>
         <div class="col-md-2 text-right">       
         </div>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
        <div class="col-lg-6 col-md-6"></div>                    
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tableprodationtransfer" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>DA Name</th>
              <th>Intemation Type</th>
              <th>Comments</th>
              <th class="text-center">Action</th>
            </tr> 
          </thead>        
          <?php  if (count($getprobationdetails) == 0) { ?>
          <tbody>
          </tbody>
          <?php }else{ ?>
          <?php
          $count = 1;
          foreach($getprobationdetails as $row) { ?>
          <tbody>
            <tr>
              <td><?php echo $count; ?></td> 
              <td><?php if(!empty($row->name)) echo $row->name; ?></td>
              <td><?php if(!empty($row->intimation)) echo $row->intimation;?>  </td>
              <td><?php if(!empty($row->comment)) echo $row->comment; ?></td>               
              <td class="text-center">            
                <a href="<?php if(!empty($row->id)) echo site_url('Proposed_probation_separation/intimate/'.$row->id);?>"  class= "btn btn-info btn-sm" title="Initiate" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
              </td>     
            </tr>
            <?php if (isset($row->type) && $row->type==2) {
             ?>
             <tr>
               <td colspan="7"><a href=""> Exit Interview form</a></td>
             </tr>
             <tr>
              <td colspan="7"><a href="">Check sheet form</a></td>
            </tr>
            <tr>
              <td colspan="7"><a href="">Clearance certificate form</a></td>
            </tr>
          </tbody>
          <?php }   $count ++; } } ?>

        </table>
      </div>
    </div>
  </div>
</div>


<div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
     <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to Extention</h4>
     <div class="col-md-2 text-right">
     </div>
   </div>
   <hr class="colorgraph"><br>
 </div>
 <div class="panel-body">
  <div class="row" style="background-color: #FFFFFF;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
    <div class="col-lg-6 col-md-6"></div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
      <table id="tableprodationExtention" class="table table-bordered table-striped wrapper">
        <thead>
         <tr>
          <th>S.No.</th>
          <th>DA Name</th>
          <th>Intemation Type</th>
          <th>Comments</th>
          <th class="text-center">Action</th>
        </tr> 
      </thead>
      <tbody>
        <?php  if (count($getprobationdetails) == 0) {
          ?>
          <?php }else{ ?>
          <?php
          $count = 1;
          foreach($getprobationdetails as $row) { ?>
          <tr>
            <td><?php echo $count; ?></td>
            <td><?php if(!empty($row->name)) echo $row->name; ?></td>
            <td><?php if(!empty($row->intimation)) echo $row->intimation; ?></td>
            <td><?php if(!empty($row->comment)) echo $row->comment; ?></td>                
            <td class="text-center">
              <a href="<?php if(!empty($row->id)) echo site_url('Proposed_probation_separation/intimate/'.$row->id);?>" class= "btn btn-info btn-sm" title="Initiate" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
            </td>     
          </tr>
          <?php if (isset($row->type) && $row->type==2) {
           ?>
           <tr>
             <td colspan="7"><a href=""> Exit Interview form</a></td>
           </tr>
           <tr>
            <td colspan="7"><a href="">Check sheet form</a></td>
          </tr>
          <tr>
            <td colspan="7"><a href="">Clearance certificate form</a></td>
          </tr>

          <?php }   $count ++; } } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

</div>
</section>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tableprodationsepration').DataTable();
    $('#tableprodationgraduate').DataTable(); 
    $('#tableprodationtransfer').DataTable(); 
    $('#tableprodationExtention').DataTable(); 
    
  });

  $('#probation_completed').change(function(){
    if($('#probation_completed').val() == 1)
    {
     $('#probatextension').css('display', 'block');
   }
   else if($('#probation_completed').val() == 2)
   {
     $('probatextension').css('display', 'block');
   }
   else{

     $('#probatextension').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
   }
 });
  
  function confirm_delete() {
    var r = confirm("Do you want to delete this Separation ?");
    if (r == true) {
      return true;
    } else {
      return false;
    }
  }


</script>  


