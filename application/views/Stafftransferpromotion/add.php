<section class="content">
  <?php 
  //echo $token;//foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "add"){ ?>
  <div class="container-fluid" style="margin-top: 20px;">
    <!-- Exportable Table -->
    
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
      <form method="POST" action="">
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-7 panel-title pull-left">Transfer and Change Responsibility Request</h4>
             <div class="col-md-5 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
        <div class="panel-body">

         <?php 
         $tr_msg= $this->session->flashdata('tr_msg');
         $er_msg= $this->session->flashdata('er_msg');

         if(!empty($tr_msg)){ ?>
         <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            

            <div class="form-group">
              <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">Staff<span style="color: red;" >*</span></label>
                <input type="text" name="staff" id="staff" value="<?php  if(!empty($staff->name)) echo $staff->emp_code .' - '.$staff->name .' ['.$staff->desname .']' ;?>" class="form-control" disabled="true">


              </div>

            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Old Office<span style="color: red;" >*</span></label>
                    <select class="form-control" name="old_office" id="old_office">
                      <option value="<?php echo $transfer_promption->new_office_id; ?>" ><?php echo $transfer_promption->old_name;?></option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Old Designation<span style="color: red;" >*</span></label>

                    <select class="form-control" name="Presentdesignation" id="Presentdesignation">
                      <option value="<?php echo $transfer_promption->new_designation; ?>"> <?php echo $transfer_promption->desname; ?></option>
                    </select>
                  </div>

                </div>
              </div>
            </div>
            <div class="row" style="border-top: Solid 1px #e1e1e1; margin-top: 10px; padding-top: 10px;">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">New Designation<span style="color: red;" >*</span></label>

                    <select class="form-control" name="new_designation" id="new_designation" required="required">
                      <option value="" >Select </option>
                      <?php foreach($all_designation as $val)
                      {
                        ?>
                        <option value="<?php echo $val->desid;?>"><?php echo $val->desname;?></option>
                        <?php 
                      } 
                      ?>
                    </select>
                  </div>

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field"> New Office<span style="color: red;" >*</span></label>

                    <select class="form-control" name="transfer_office" id="transfer_office" required="required">
                      <option value="">Select Office</option>
                      <?php 
                      foreach($all_office as $val)
                      {
                        ?>
                        <option value="<?php echo $val->officeid;?>" ><?php echo $val->officename;?> </option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>

                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Reporting To <span style="color: red;" >*</span></label>

                    <select class="form-control" name="reportingto" id="reportingto" required="required">
                      <option value="">Select Reporting To</option>
                    </select>
                  </div>

                </div>
              </div>
              <div class="col-md-6">

                <div class="form-group">
                  <div class="form-line">
                   <label for="StateNameEnglish" class="field-wrapper required-field">Proposed Date<span style="color: red;" >*</span></label>

                   <input type="text" class="form-control datepicker " data-toggle="tooltip" 
                   id="fromdate" name="fromdate" placeholder="From Date" style="min-width: 20%;" >
                 </div>

               </div>
             </div>
           </div>
           
<!-- <div class="form-check">
                <input class="form-check-input" type="checkbox" name="changereportingto" id = "changereportingto">
                 <label for="StateNameEnglish" class="field-wrapper required-field">Do you want to change reporting for new office staff on this transfer ? </label>
                
               
             </div> -->
             <div class="form-group">
              <div class="form-line">
               <label for="StateNameEnglish" class="field-wrapper required-field">Reason<span style="color: red;" >*</span></label>
               (<i>Maximum 250 characters allowed</i>)

               <textarea class="form-control" data-toggle="tooltip" 
               name="reason" id="reason" placeholder="Reason" maxlength="250" required="required" style="min-width: 20%;" ></textarea> 
             </div>
           </div>

           <div class="form-group">
            <div class="form-line">
             <label for="StateNameEnglish" class="field-wrapper required-field">Discussion<span style="color: red;" >*</span></label>
             (<i>Maximum 250 characters allowed</i>)

             <textarea class="form-control" data-toggle="tooltip" 
             name="remark" id="remark" placeholder="Discussion" maxlength="250" required="required" style="min-width: 20%;" ></textarea> 
           </div>

         </div>



       </div>

       <div class="panel-footer text-right"> 
        <!-- <a href="<?php echo site_url('Stafftransferpromotion/history/'.$token);?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on view history.">View History</a> --> 

        <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
        <a href="<?php echo site_url("Staff_list");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 

      </form>
    </div>
  </div>
</div>

<!-- #END# Exportable Table -->
</div>
<?php //} } ?>
</section>


<script>

  $(document).ready(function(){

    $("#transfer_office").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/get_Integrator_ED_OnlyteamTC/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })

      .done(function(data) {
        console.log(data);

        $("#reportingto").html(data);

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    });
        //alert(data);
       /* var obj=JSON.parse(data);
        console.log("office_name="+obj.office_name+"designation="+obj.designation+"office_id="+obj.office_id+"designation_id="+obj.designation_id);
        var dd=obj.office_name;
       
        var option='';
            option += `<option value="${obj.office_id}">${obj.office_name}</option>`;
            var option1='';
            option1 += `<option value="${obj.designation_id}">${obj.designation}</option>`;

       

$('#Presentoffice').html(option);
$('#Presentdesignation').html(option1);

*/
$("#fromdate").datepicker({
  changeMonth: true,
  changeYear: true,
  minDate : 0,
  yearRange: '1980:2025',
  dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });

});
  

</script>
