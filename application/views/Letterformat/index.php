<?php foreach ($role_permission as $row) { if ($row->Controller == "Letterformat" && $row->Action == "index"){ ?>
<br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Letter & Email format List</h4>
       <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new batch? Click on me." href="<?php echo site_url()."Letterformat/add";?>" class="btn btn-primary btn-sm">Add New</a>
      </div>
    </div>
    <hr class="colorgraph"><br>
  </div>
  <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <div class="container-fluid">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table id="tableletter" class="table table-bordered dt-responsive table-striped">
              <thead>
               <tr>
                <th style ="max-width:50px;" class="text-center">S. No.</th>
                <th class="text-center">Type</th>
                <th class="text-center">Process</th>
                <th class="text-center">Is Active</th>
                <!-- <th  style ="max-width:70px;" class="text-center"> Status </th> -->

                <th style ="max-width:50px;" class="text-center">Action</th>
              </tr> 
            </thead>
            <tbody>
              <?php 
              $i=0;
              foreach ($letterlist as $key => $value) { ?>
              <tr>
                <td class="text-center"><?php echo $i+1;?></td>
                <td class="text-center"><?php echo $value->type;?></td>
                <td class="text-center"><?php echo $value->process;?></td>
                <td class="text-center"><?php echo $value->status;?></td>
                  <!--   <td><?php
                    if ($value->status==0) {
                      echo "Active"; 
                    }else{
                      echo "InActive"; 
                    }

                    ?></td> -->
                    <td class="panel-footer text-center"><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Letter." href="<?php echo site_url()."Letterformat/edit/".$value->id;?>" ><i class="fa fa-edit" aria-hidden="true" id="processid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Letter." href="<?php echo site_url()."Letterformat/delete/".$value->id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
                    </a></td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>
      </div>
    </div>   
    <?php } } ?>
    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tableletter').DataTable(); });
      function confirm_delete() {
        var r = confirm("Do you want to delete this Letter?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }
      </script>
