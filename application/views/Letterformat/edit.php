  <style type="text/css">
  *{
    padding: 0;
    margin: 0;
    cursor: default;
    color:#333;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
  .container {
    margin: 0 auto;
    padding: 0 20px;
    max-width: 900px;
    min-width: 300px;
  }
/*.row {
  width:100%;
  overflow: none;
}
*/.column {
  float:left;
  width: 50%;
}
.connected-sortable {
  margin: 0 auto;
  list-style: none;
  width: 90%;
}

li.draggable-item {
  width: inherit;
  padding: 15px 20px;
  background-color: #f5f5f5;
  -webkit-transition: transform .25s ease-in-out;
  -moz-transition: transform .25s ease-in-out;
  -o-transition: transform .25s ease-in-out;
  transition: transform .25s ease-in-out;
  
  -webkit-transition: box-shadow .25s ease-in-out;
  -moz-transition: box-shadow .25s ease-in-out;
  -o-transition: box-shadow .25s ease-in-out;
  transition: box-shadow .25s ease-in-out;
  &:hover {
    cursor: pointer;
    background-color: #eaeaea;
  }
}
/* styles during drag */
li.draggable-item.ui-sortable-helper {
  background-color: #e5e5e5;
  -webkit-box-shadow: 0 0 8px rgba(53,41,41, .8);
  -moz-box-shadow: 0 0 8px rgba(53,41,41, .8);
  box-shadow: 0 0 8px rgba(53,41,41, .8);
  transform: scale(1.015);
  z-index: 100;
}
li.draggable-item.ui-sortable-placeholder {
  background-color: #ddd;
  -moz-box-shadow:    inset 0 0 10px #000000;
  -webkit-box-shadow: inset 0 0 10px #000000;
  box-shadow:         inset 0 0 10px #000000;
}
</style>
<?php foreach ($role_permission as $row) { if ($row->Controller == "Letterformat" && $row->Action == "add"){ ?>
<div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row clearfix">
            <form method="POST" action="" class="col-md-12">
              <div class="panel thumbnail  shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-8 panel-title pull-left">Add New Format</h4>
                   <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-4">
                   <div class="form-group">
                    <div class="form-line">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Select Type : <span style="color: red;" >*</span></label>
                      <select name="formattype" id="formattype" class="form-control" required="">
                        <option value=""> Select Type</option>
                        <option value="1" <?php if($getletterformatdetail) if($getletterformatdetail->type == 1) echo "selected"; ?>> Letter</option>
                        <option value="2" <?php if($getletterformatdetail) if($getletterformatdetail->type == 2) echo "selected"; ?>> Email</option>
                      </select>
                    </div>
                    <?php echo form_error("letter");?>
                  </div>

                  <label for="StateNameEnglish" class="field-wrapper required-field">Process : <span style="color: red;" >*</span></label>
                  <select name="processid" id="processid" class="form-control" required>
                    <option value="">Select Process</option>
                    <?php if($processlist){
                      foreach($processlist as $value){
                        ?>
                        <option value="<?php echo $value->processid; ?>" <?php if($getletterformatdetail) if($getletterformatdetail->processid == $value->processid) echo "selected"; ?>><?php echo $value->process; ?></option>
                        <?php
                      }
                    } ?>
                  </select>
                  <?php echo form_error("processid");?>
                </div>
                
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="form-line">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Attributes : <span style="color: red;" >*</span></label>
                      <div class="" style="height: 150px; overflow: auto; border: solid 1px #e1e1e1; padding: 5px;">
                        <ul id="attributelist" class="">
                          <?php if($getattributeslist){
                            foreach($getattributeslist as $value){
                              ?>
                              <li  class="ui-state-default draggable-item"><a href=""><?php echo $value->attributes; ?></a></li>
                              <?php
                            }
                          } ?>
                          <!-- <li class="ui-state-default draggable-item"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 7</li> -->
                        </ul>
                      </div>
                      
                    </div>
                    <?php echo form_error("Attributes");?>
                  </div>
                </div>
                <div class="col-md-12">
                 <div class="">
                  <div class="">
                    <label for="OfferLetter" class="field-wrapper required-field">Content  <span style="color: red;" >*</span></label>
                    <!-- <textarea class="input-block-level" id="summernote" name="content" rows="18"> -->
                    <textarea class='summernote' id='summernote' name="lettercontent" rows='10' >
                        <?php
                        if($getletterformatdetail){
                          echo $getletterformatdetail->lettercontent;
                        }
                        ?>                        
                      </textarea>
                      
                    </div>
                    <?php echo form_error("Content");?>
                  </div>
                </div> 


              </div> 
            </div>
            <div class="panel-footer text-right">
              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Update</button>
              <a href="<?php echo site_url("Letterformat");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
            </div>

          </div>
        </form>
      </div>

    </div>
  </div>
  <!-- #END# Exportable Table -->
</div>





<?php } } ?>

<script>
  
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );

  $( init );
  function init() {
    $( ".droppable-area1, .droppable-area2" ).sortable({
      connectWith: ".connected-sortable",
      stack: '.connected-sortable ul'
    }).disableSelection();
  }
</script>
<script type="text/javascript">


  $('#formattype').change(function(){
    //alert($(this).val());
    /*$('#lettercontent').value('');
    $('#attributelist').html('');*/
    var formattype = $(this).val();
    if(formattype){
      $.ajax({
        type:"POST",
        url : "<?php echo site_url(); ?>Letterformat/processlist",
        data : {'formattype':formattype},
        success : function(response) {
          data = response;
          $('#processid').html(response);
        },
        error: function() {
          alert('Error occured');
        }
      });
    }else{
      $('#processid').html('<option value="">Select Process</option>');
      $('#attributelist').html('');
      $('.note-editable').html('');
    }
  });

  $('#processid').change(function(){
    //alert($(this).val());
    var processid = $(this).val();
    if(processid){
      $.ajax({
        type:"POST",
        url : "<?php echo site_url(); ?>Letterformat/attributelist",
        data : {'processid':processid},
        success : function(response) {
          data = response;
          if(response){
            $('#attributelist').html(response);
          }else{
            $('#attributelist').html('');
          }
        },
        error: function() {
          alert('Error occured');
        }
      });
      $.ajax({
        type:"POST",
        url : "<?php echo site_url(); ?>Letterformat/lettercontentcheck",
        data : {'processid':processid},
        success : function(response) {
          data = response;
            // $('#lettercontent').val(response);
            // alert('dsfsfs');
            $(".note-editable").html(response);
          },
          error: function() {
            alert('Error occured');
          }
        });
    }else{
      $('#attributelist').html('');
      $('.note-editable').html('');
    }
  });


</script>