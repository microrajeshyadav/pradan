<section class="content">
	<?php foreach ($role_permission as $row) { if ($row->Controller == "Revised_provident_fund_nomination_listing" && $row->Action == "index"){ ?>
	<br>
	<div class="container-fluid">
		<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');

		if(!empty($tr_msg)){ ?>
		<div class="content animate-panel">
			<div class="row">
				<div class="col-md-12">
					<div class="hpanel">
						<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('tr_msg');?>. </div>
						</div>
					</div>
				</div>
			</div>
			<?php } else if(!empty($er_msg)){?>
			<div class="content animate-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="hpanel">
							<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('er_msg');?>. </div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>				
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel thumbnail shadow-depth-2 listcontainer" >
							<div class="panel-heading">
								<div class="row">
									<h4 class="col-md-10 panel-title pull-left">Revised Provident Fund Nomination listing</h4>
								</div>
								<hr class="colorgraph"><br>
							</div>
							<div class="panel-body">
								
									<div class="col-md-12 text-right">

											

											<a data-toggle="tooltip" data-placement="bottom" title="Click here to Provident Fund_nomination_form_staff." href="<?php echo base_url().'Provident_fund_nomination_form_staff/index/'.$this->loginData->staffid;?>" class="btn btn-primary btn-sm">Add</a>
											
										</div>	
										
										
									</div>
								

								<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
									<thead>
										<tr>
											<th  class="text-center" style="width: 45px;">#</th>

											<th style="width: 170px;">Staff</th>
											<th >Place </th>
											<th> Gender</th>
											
											<th> Date</th>
											<th>Acton </th>
											<th>Profile Status </th>
											<?php  if ($this->loginData->RoleID==16 || $this->loginData->RoleID==10 || $this->loginData->RoleID==17) {
												?>
												<th id="hraction" class="text-left" style="width: 200px;">Initiate</th>
												<?php }  ?>
												<?php if ($this->loginData->RoleID==17 || $this->loginData->RoleID==10) {
													?>
													<th id="peraction" class="text-center" style="width: 90px;">Action</th>
													<?php } ?>
												</tr>
											</thead>
											
											<tbody>
											<?php
											//print_r($staff_details);
												$i=0; foreach($staff_details as $row){ 
													//print_r($row);
											?>
													
												
													<tr>
														<input type="hidden" id="staff_id" value="<?php echo $row->staffid;?>" name="staff_id">
														<td class="text-center"><?php echo $i+1; ?></td>

														<td ><?php echo   $row->name ." - ". $row->emp_code; ?></td>
														<td ><?php echo $row->place; ?></td>
														<!-- <td ><?php //echo $row->level; ?></td> -->
														<td ><?php echo $row->gender; ?></td>
														
														<td ><?php echo $this->gmodel->changedatedbformate($row->date); ?></td>
														<td class="text-center"><?php  if ($row->status==0) {
															echo '<span class = "badge badge-success">edit</span>';
														}
														else if($row->status==1)
														{
															echo '<span class = "badge badge-success">view</span>';

														}
														else{
															echo '<span class = "badge badge-danger">add</span>';
														} ?></td>
														<td>
															<?php 
															if ($this->loginData->RoleID == 10 && $row->flag=="Approved") { ?>
															<button type="button" name="approved" value="<?php echo $row->staffid; ?>" id="approved" class="btn btn-success text-center btn-xs" >Unlocked</button>
															<?php } elseif($row->flag=="Approved"){
																echo '<span class = "badge badge-success" style="width:60px;">'.$row->flag.'</span>';	
															}
															elseif($row->flag=="Save"){
																echo '<span class = "badge badge-dark" style="width:60px;">'.$row->flag.'</span>';	
															}
															elseif($row->flag=="Submitted"){
																echo '<span class = "badge badge-info" style="width:60px;">'.$row->flag.'</span>';	
															}
															elseif($row->flag=="Rejected"){
																echo '<span class = "badge badge-danger" style="width:60px;">'.$row->flag.'</span>';	
															}
															else{
																echo $row->flag;
															}
															?>
														</td>

														<td class="text-center">

															<?php 

															if ($row->status==1) {

																if ($this->loginData->RoleID==16 || $this->loginData->RoleID==10 || $this->loginData->RoleID==17  ) { 

																	$staffprobationdetails = $this->Staff_list_model->getProbationStatus($row->staffid);


																	if (($this->loginData->RoleID==17 || $this->loginData->RoleID==10)) { ?>

																		<a href="<?php echo base_url().'Probation_reviewofperformance/fullview/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff Probation.">P</a>

																		<?php }else{ ?>
																		<a href="<?php echo base_url().'Probation_reviewofperformance/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff Probation.">P</a>

																		<?php } ?>|

																		<a href="<?php echo base_url().'stafftransfer/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff transfer.">T</a>|

																		<a href="<?php echo base_url().'Staffpromotion/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff promotion.">CR
																		</a>|

																		<a href="<?php echo base_url().'Stafftransferpromotion/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff transfer & promotion.">T.CR</a>|

																		<a href="<?php echo base_url().'Staff_seperation/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff sepration.">S
																			<?php } } ?>

																		</td>

																		<?php if ($this->loginData->RoleID==17 || $this->loginData->RoleID==10) { ?>

																		<td class="text-left">
																			<?php if ($row->flag=='Approved') { ?>
																			<a href="<?php echo site_url().'Stafffullinfo/view/'.$row->staffid.'/'.$row->candidateid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to view this Staff Profile."><i class="fa fa-eye" aria-hidden="true" title="Click here to verify this Staff Profile." data-toggle="tooltip"></i></a>
																			<?php } else if ($row->flag=='Submitted') { ?>
																			<a href="<?php echo site_url().'Stafffullinfo/view/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to verify this Staff Profile."><i class="fa fa-eye" aria-hidden="true" title="Click here to verify this Staff Profile." data-toggle="tooltip"></i></a>
																			<?php }else{ ?>

																			<a href="<?php echo site_url().'Stafffullinfo/view/'.$row->staffid.'/'.$row->candidateid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to view this Staff Profile."><i class="fa fa-eye" aria-hidden="true" id="usedbatchid"></i></a>											


																			<?php } ?>
																			<?php 
																			if($row->joiningandinduction==NULL)
																			{

																			}

																			else if($row->joiningandinduction!=NULL && $row->candidateid!=NULL && $row->status==1)
																			{
																				?>
																				<a href="<?php echo site_url('Employee_particular_form/index/'.$row->staffid.'/'.$row->candidateid)?>" id="statedl" data-toggle="tooltip"  style="padding : 4px;" title="Click here to view Staff Joining  induction Forms"><i class="fa fa-universal-access" aria-hidden="true"></i>

																				</a>


																				<?php }?>
																				<?php /*
																				if($row->status==1)
																					{	?>
																						<a data-toggle="tooltip" title="Click here to Staff Active & Inactive" href="<?php echo site_url('Staff_list/stfflistdectivated/'.$row->staffid.'');?>" 
																							onclick="return confirm('Are you sure?')"
																							><i class="fa fa fa-user"></i></a>
																							<?php }?>
																						</td>

																						<?php */} ?>

																					</tr>
																					
																					<?php $i++; } ?>
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
															<!-- #END# Exportable Table -->
														</div>

														<?php } } ?>
													</section>

													<script>
														$(document).ready(function() {
															$('[data-toggle="tooltip"]').tooltip(); 

														});
														$(document).ready(function() {
															var groupColumn = 2;
															var table = $('#tbldesignations').DataTable({
																"columnDefs": [
																{ "visible": false, "targets": groupColumn }
																],
																"order": [[ groupColumn, 'asc' ]],
																"displayLength": 25,
																"drawCallback": function ( settings ) {
																	var api = this.api();
																	var rows = api.rows( {page:'current'} ).nodes();
																	var last=null;

																	api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
																		if ( last !== group ) {
																			$(rows).eq( i ).before(
																				'<tr class="group font-weight-bold text-uppercase"><td colspan="10">'+group+'</td></tr>'
																				);

																			last = group;
																		}
																	} );
																}
															} );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
    	var currentOrder = table.order()[0];
    	if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
    		table.order( [ groupColumn, 'desc' ] ).draw();
    	}
    	else {
    		table.order( [ groupColumn, 'asc' ] ).draw();
    	}
    } );
} );
														function confirm_delete() {

															var r = confirm("Do you want to delete this Designations");

															if (r == true) {
																return true;
															} else {
																return false;
															}

														}

														$(document).ready(function(){

															$("#approved").on('click', function(){

																var staff_id = $("#approved").val();
         // var new_designation=
          //alert("staff="+staff_id);
          var data;
          data=staff_id;
          
          $.ajax({
          	data:{staff_id:staff_id},
          	url: '<?php echo site_url(); ?>Ajax/staff_approved/',
          	type: 'POST'

          })
          .done(function(data) {
          	alert("Your Account is unlocked");
          	window.location = "<?php echo current_url();?>";

          })

      });
														});


													</script>