
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">List Of Intimation</h4>
      <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to send hrd intemation? Click on me." href="<?php echo site_url()."Hrd_intemation/send_hrd_intemation";?>" class="btn btn-primary btn-sm">Send Intimation</a>
      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
            
          <!-- <div class="row" style="background-color: #FFFFFF;">
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">
            <a href="<?php //echo site_url()."Hrd_intemation/send_hrd_intemation";?>" class="btn btn-sm btn-success">Send Intimation</a>
                <br>
                <br>
           </div>
       </div> -->
      
        <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>DA Code</th>
              <th>Campus Name</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Intemation Type</th>
              <th>Comments</th>
             <!--  <th>Action</th> -->
           </tr> 
         </thead>
         <tbody>
          <?php 
          $i=0; foreach ($getprobationdetails as $key => $value) { ?>
         <tr>
           <td><?php echo $i+1 ?></td>
           <td><?php echo $value->emp_code;?></td>
           <td><?php echo $value->campusname;?></td>
           <td><?php echo $value->name;?></td>
           <td><?php echo $value->gender;?></td>
           <td><?php if ($value->intemation_type==2) {
             echo "Recommended to graduate";
           }elseif ($value->intemation_type==3) {
            echo "Probation Completed";
           }elseif ($value->intemation_type==1){
            echo "Resign";
           }elseif ($value->intemation_type==4){
            echo "Transfer";
           }elseif ($value->intemation_type==5){
            echo "Facilitate to leave";
           }elseif ($value->intemation_type==6){
            echo "Extention";
           }else{
            echo " ";
           }   ?></td>
            
           <td><?php echo $value->comment;?></td>
          <!--  <td>
            <a href="<?php //echo site_url().'Hrd_intemation/edit_send_hrd_intemation/'.$value->id;?>" id="modalcandidate" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this."><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>|
            <a href="<?php //echo site_url().'Hrd_intemation/delete/'.$value->id;?>" id="modalcandidate" onclick="return confirm_delete();" data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this.">
             <i class="fa fa-trash" style="color:red"></i>
            </a>  -->
          
            </td>
          </tr>
         <?php $i++; } ?>
        </tbody>
      </table>

      </div>
       </div>
    </div> 
    </div>
    </div>
    </div>   
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });

 function getcandidateid(id){
      var string = id;
  $('#candidateid').val(string);
 }


</script>
<script type="text/javascript">
$('#intemation_type').change(function(){
if($('#intemation_type').val() == 2)
   {
   $('#probatextension').css('display', 'block');
      }
else if($('#intemation_type').val() == 3)
   {
   $('probatextension').css('display', 'block');
   }
   else{

     $('#probatextension').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
   }
});


function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>



