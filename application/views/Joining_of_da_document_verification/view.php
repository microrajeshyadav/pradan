<br/>
<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="panel thumbnail shadow-depth-2 listcontainer" >
					<div class="panel-heading">
						<div class="row">
							<h4 class="col-md-10 panel-title pull-left">Manage offered candidate verification document list</h4>

						</div>
						<hr class="colorgraph"><br>
					</div>

					<div class="body">
						<form name="joinDA" action="" method="post">
							<div class="row">
								<div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
								<div class="col-lg-4">
									<input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
									<input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
								</div>
								<?php if (!empty($getcandateverified->batch)) { ?>

								<div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
								<div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control" readonly="readonly"></div>
								<?php } ?>
								<div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
								<div class="col-lg-1" >
									<img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" class="rounded-circle" width="93px" hight="132px" title="Default Image" alt="Default Image" boder="2px"></div>
								</div>
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-4"></div>
									<div class="col-lg-4"></div>
									<div class="col-lg-2"></div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table id="tblForm09" class="table table-bordered table-striped" >
											<thead>
												<tr style="background-color: #eee;">
													<th class="col-md-6 text-left" >
														Documents to be verified 
													</th>
													<th class="col-md-3 text-center">
														Certificate
													</th>
													<th class="col-md-3 text-center">
														Verification
													</th>
												</tr>
											</thead> 

											<tr style="background-color: #fff;">
												<td class="col-md-6 text-left">10th Standard</td>
												<td class="col-md-3 text-center">

													
													<?php 
														if (!empty($getcandateverified->encryptmetriccertificate)) {


														$matriccertificateArray = explode("|",$getcandateverified->encryptmetriccertificate);
														$image_path='';

														foreach($matriccertificateArray as $res)
														{
														$image_path='datafiles\educationalcertificate/'.$res;

														if (file_exists($image_path)) {

													?>
													<a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"  ></i></a>
													<?php } } } ?>
												</td>
													<td class="col-md-3 text-center">
														<label class="form-check-label" for="metric_certificate_verified">verified</label>

													</td> 
												</tr>
												<tr style="background-color: #fff;">
													<td class="col-md-6 text-left">12th Standard</td>
													<td class="col-md-3 text-center">
														<?php if (!empty($getcandateverified->encrypthsccertificate)) 
															{
															$hsccertificateArray = explode("|",$getcandateverified->encrypthsccertificate);
															$image_path='';

															foreach($hsccertificateArray as $res)
															{
															$image_path='datafiles\educationalcertificate/'.$res;

															if (file_exists($image_path)) {
														?>
														<a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"  ></i></a> 
														<?php } } } ?>

													</td>
													<td class="col-md-3 text-center">

														<label class="form-check-label" for="hsc_certificate_verified">verified</label>

													</td>
												</tr>
												<tr style="background-color: #fff;">
													<td class="col-md-6 text-left">Under Graduation
														<?php if (!empty($getcandateverified->ugmigration_certificate_date)) {?><br>Migration Certificate 
														<?php 
														if($getcandateverified->ugmigration_certificate_date != '0000-00-00')
															{?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);?>)<?php }} ?>
													</td>
														<td class="col-md-3 text-center">
															<?php if (!empty($getcandateverified->encryptugcertificate)) { 

																$ugcertificateArray = explode("|",$getcandateverified->encryptugcertificate);
																$image_path='';

																foreach($ugcertificateArray as $res)
																{
																$image_path='datafiles\educationalcertificate/'.$res;

																if (file_exists($image_path)) {

															?>
															
															<a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"  ></i></a>

															<?php } } } ?>

															<?php if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
															<br>
															<?php if($getcandateverified->ugmigration_certificate_date != '0000-00-00'){
																?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);} ?>
																<br>
																Migration Certificate
																<?php } ?>  
																<?php  if ($getcandateverified->ugmigration_encrypted_certificate_upload) { ?>
																<a href="<?php  echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a> 
																<?php } ?>
															</td>
																<td class="col-md-3 text-center">

																	<label class="form-check-label" for="ug_certificate_verified">verified</label>

																</td>

															</tr>
															<?php if (!empty($getcandateverified->encryptpgcertificate)) {?>  
															<tr style="background-color: #fff;">
																<td class="col-md-6 text-left">Post Graduation
																	<?php 
																	if (!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) {?><br> Migration Certificate<?php 
																		if($getcandateverified->pgmigration_certificate_date != '0000-00-00')
																			{?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);?>)<?php } } ?></td>
																		<td class="col-md-3 text-center">
																			<?php  if ($getcandateverified->encryptpgcertificate) {
								                        $pgcertificateArray = explode("|",$getcandateverified->encryptpgcertificate);
								                        /*echo "<pre>";
								                        print_r($pgcertificateArray);exit();*/
								                        $image_path='';
								                        foreach($pgcertificateArray as $res)
								                         {
								                           $image_path='datafiles\educationalcertificate/'.$res;
								                           
								                           if (file_exists($image_path)) {

								                      ?>
																			<a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
																			<?php } } } ?>

																			<?php if(!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
																			<br>
																			<?php if($getcandateverified->pgmigration_certificate_date != '0000-00-00'){
																				?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);} ?><br>
																				Migration Certificate
																				<?php } ?>  
																				<?php  if ($getcandateverified->pgmigration_encrypted_certificate_upload) { ?>
																				<a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
																				<?php } ?>
																			</td>
																			<td class="col-md-3 text-center">
																				<label class="form-check-label" for="pg_certificate_verified">verified</label>
																			</td>
																		</tr>
																		<?php }  
  if ($getgapyearcount > 0) {
    																	 foreach ($getgapyearpverified as $key => $value) {?>

																<tr>
																	<td class="col-md-6 text-left">Gap Year Document</td>
																	<td class="col-md-3 text-center"> <a href="<?php echo site_url().'datafiles/gapyeardocument/'.$value->encrypteddocumentsname;?>" download ><i class="fa fa-download"  ></i></a> </td>
																	<td class="col-md-3 text-center">

																		<label class="form-check-label" for="gapyear_certificate_verified">verified</label>

																	</td>
																</tr>
																<?php } } ?>


																		<?php if (!empty($getcandateverified->encryptofferlettername)) { ?>  
																		<tr>
																			<td class="col-md-6 text-left">Signed Offer Letter</td>
																			<td class="col-md-3 text-center"> 
																				<?php  if ($getcandateverified->encryptofferlettername) { ?>
																				<a href="<?php echo site_url().'pdf_offerletters/'.$getcandateverified->encryptofferlettername;?>" download ><i class="fa fa-download"  ></i></a> 
																				<?php } ?>
																			</td>
																			<td class="col-md-3 text-center">

																				<label class="form-check-label" for="signed_certificate_verified">verified</label>

																			</td>
																		</tr>
																		<?php } ?>

																		<?php if (!empty($getcandateverified->originalothercertificate)) {?>  
																		<tr>
																			<td class="col-md-6 text-left">Other</td>
																			<td class="col-md-3 text-center">
																				<?php  if ($getcandateverified->originalothercertificate) { ?>
																				<a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->originalothercertificate;?>" download >
																					<i class="fa fa-download" ></i>
																				</a>
																				<?php } ?>
																			</td>
																			<td class="col-md-3 text-center">                
																				<label class="form-check-label" for="pg_certificate_verified">verified</label>
																			</td>
																		</tr>
																		<?php }  ?>




																		<?php if ($getworkexpcount > 0) {
																			foreach ($getworkexpverified as $key => $value) {
																				?>
																				<tr style="background-color: #fff;">
																					<td class="col-md-6 text-left">Work Experience Certificate & Salary Slip</td>
																					<td class="col-md-3 text-center">
																						<?php if (!empty($value->encrypteddocumnetname)) {?>
																						<a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download ><i class="fa fa-download"  ></i></a><?php } ?>|
																						<?php if (!empty($value->encryptedsalaryslip1)) {?>
																						<a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip1;?>" download ><i class="fa fa-download"  ></i></a> <?php } ?>|
																						<?php if (!empty($value->encryptedsalaryslip2)) {?>
																						<a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip2;?>" download ><i class="fa fa-download"  ></i></a><?php } ?>|
																						<?php if (!empty($value->encryptedsalaryslip2)) {?>
																						<a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip3;?>" download ><i class="fa fa-download"  ></i></a>
																						<?php } ?>


																					</td>
																					<td class="col-md-3 text-center">

																						<label class="form-check-label" for="workexp_certificate_verified">verified</label>

																					</td>
																				</tr>
																				<?php  } }  ?>
																			</table>
																		</div>
																	</div>

																	
																	<div class="row">
																		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																			<table id="tblForm13" class="table table-bordered table-striped" >
																				<thead>
																					<tr style="background-color: #eee;">
																						<th class="col-md-6 text-left">Form Name</th>

																						<th class="col-md-3 text-center">Document</th>

																						<th class="col-md-3 text-center">Verification</th>
																					</tr> 
																				</thead>
																				<tbody >
																					<tr style="background-color: #fff;">
																						<td class="col-md-6 text-left">General Nomination form</td>
																						<td class="col-md-3 text-center">  <a href="<?php echo site_url().'/pdf_generalnominationform/'.$getgeneralnominationformpdf->filename;?>" download ><i class="fa fa-download"  ></i></a></td>
																						<td class="col-md-3 text-center">
																							<label class="form-check-label" for="general_nomination_form" >verified</label>
																						</td>
																					</tr>
																					<tr style="background-color: #fff;">
																						<td class="col-md-6 text-left">Joining report for DAs</td>
																						<td class="col-md-3 text-center">  <a href="<?php echo site_url().'pdf_joiningreportforda/'.$getjoiningreportstatus->filename;?>" download ><i class="fa fa-download"  ></i></a></td>
																						<td class="col-md-3 text-center"> 
																							<label class="form-check-label" for="joining_report_da">verified</label>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</div>
																	</div>

																	<div class="row">
																		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																			<table id="" class="table table-bordered table-striped" >
																				<thead>
																					<tr style="background-color: #eee;">
																						<th class="col-md-12 text-left">
																						Other Document Uploads </th>
																						<th></th>
																					</tr> 
																					<tr style="background-color: #eee;">
																						<th class="col-md-6 text-left">Document Name </th>
																						<th class="col-md-3 text-center" >Document Type </th>
																						<th class="col-md-3 text-center"> Upload </th>
																					</tr> 
																				</thead>
																				<tbody id="tbodyForm09" >
																					<input type="hidden" name="otherdocumentcount" id="otherdocumentcount" value="<?php echo $countotherdoc;?>">
																					<?php if ($countotherdoc == 0) {?>
																					<tr id="tbodyForm09">
																						<td class="col-md-6 text-left">
																							<?php 
																							$options = array('' => 'Select Document Name');
																							foreach($getdocuments as$key => $value) {
																								$options[$value->id] = $value->document_name;
																							}
																							echo form_dropdown('documentname[]', $options, set_value('documentname'), 'class="form-control" data-toggle="tooltip" title="Select  Document !"  id="documentname" disabled="disabled"');
																							?>
																							<?php echo form_error("documentname");?>
																						</td>
																						<td class="col-md-3 text-center">
																							<?php 
																							$options = array('' => 'Select Document Type');
																							foreach($getdocuments as$key => $value) {
																								$options[$value->id] = $value->document_type;
																							}
																							echo form_dropdown('documenttype[]', $options, set_value('documenttype'), 'class="form-control" data-toggle="tooltip" title="Select Document Type !"  id="documenttype" disabled="disabled"');
																							?>
																							<?php echo form_error("documenttype");?>
																						</td>
																						<td class="col-md-3 text-center">
																							<div class="form-group">
																								<input type="file" name="otherdocumentupload[]" id="otherdocumentupload" accept="application/pdf, image/*" disabled="disabled">
																							</div>
																						</td>
																					</tr>
																					<?php }else{ 

																						foreach ($getotherdocdetails as $key => $val) {
																							?>
																							<tr id="tbodyForm09" style="background-color: #fff;">
																								<td class="col-md-6 text-left">
																									<?php 
																									$options = array('' => 'Select Document Name');
																									foreach($getdocuments as$key => $value) {
																										$options[$value->id] = $value->document_name;
																									}
																									echo form_dropdown('documentname[]', $options, $val->documentname, 'class="form-control" data-toggle="tooltip" title="Select  Document !"  id="documentname" disabled="disabled ');
																									?>
																									<?php echo form_error("documentname");?>
																								</td>
																								<td class="col-md-3 text-center">
																									<?php 
																									$options = array('' => 'Select Document Type');
																									foreach($getdocuments as$key => $value) {
																										$options[$value->id] = $value->document_type;
																									}
																									echo form_dropdown('documenttype[]', $options, $val->documenttype, 'class="form-control" data-toggle="tooltip" title="Select Document Type !"  id="documenttype" disabled="disabled ');
																									?>
																									<?php echo form_error("documenttype");?>
																								</td>
																								<td class="col-md-3 text-center">
																									<div class="form-group">
																										<div class="col-xs-12">
																											<div class="col-xs-9 text-left">
																												<input type="hidden" name="oldotherdocumentupload[]" value="<?php echo $val->documentupload;?>">
																												<?php  if ($val->documentupload) { ?>
																												<a href="<?php echo site_url().'otherdocuments/'.$val->documentupload;?>" download ><i class="fa fa-download"  ></i></a> 
																												<?php } ?>                 
																											</div>
																											<div class="col-xs-3">

																											</div>
																										</div>
																									</div>
																								</td>
																							</tr>
																							<?php } } ?>
																						</tbody>
																					</table>
																				</div>
																			</div>

																			<div class="row">
																				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																					<table id="tblcomment" class="table table-bordered table-striped" >
																						<thead>
																							<tr style="background-color: #eee;">
																								<th >Comments </th>
																							</tr> 
																						</thead>
																						<tbody >
																							<tr style="background-color: #fff;" >
																								<td><textarea name="comments" id="comments" class="form-control" required="required" readonly="readonly"><?php echo $getverified->comment;?></textarea>
																								</td>

																							</tr>
																						</tbody>
																					</table>
																				</div>
																			</div>

																			<div class="panel-footer text-right">

																				<a href="<?php echo site_url("Joining_of_da_document_verification");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list Joining_of_da_document_verification.">Go to list</a> 
																			</div>

																		</div>
																	</form>

																</div>
															</div>
														</div>
													</div>
													<!-- #END# Exportable Table -->
												</div>
											</section>
											<script type="text/javascript">
												$(document).ready(function(){
													$("#candidateid").change(function(){
														$.ajax({
															url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
															type: 'POST',
															dataType: 'json',
														})
														.done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
             })
														.fail(function() {
															console.log("error");
														})
														.always(function() {
															console.log("complete");
														});

													});

												});     

											</script>
											<script type="text/javascript">
												$("#btnidentitydetailRemoveRow").click(function() {
													if($('#tblForm13 tr').length-1>1)
														$('#tbodyForm13 tr:last').remove()
												});

												$('#btnidentitydetailAddRow').click(function() {
													rowsIdentityEnter = parseInt(1);
													if (rowsIdentityEnter < 1) {
														alert("Row number must be minimum 1.")
														return;
													}
													insertIdentityRows(rowsIdentityEnter);
    //addDatePicker();
});

  // var identcount = $('#identcount').val();

  // if (identcount =='NULL') {
  //   var srNoIdentity=0;
  //   var incr = 0;
  // }else{
  //   var srNoIdentity = identcount;
  //   var incr = identcount;
  // }
  

  function insertIdentityRows(count) {
  	alert();
  	srNoIdentity = $('#tbodyForm13 tr').length+1;
  	var tbody = $('#tbodyForm13');
  	var lastRow = $('#tbodyForm13 tr:last');
  	var cloneRow = null;

  	for (i = 1; i <= count; i++) {
  		incr++
  		cloneRow = lastRow.clone();
  		var tableDataIdentity = '<tr>'
  		+ ' <td>'
  		+ '<select class="form-control" name="identityname['+incr+']" required="required">'
  		+ '<option value="">Select Relation</option>'
  		<?php foreach ($sysidentity as $key => $value): ?>
  		+ '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
  		<?php endforeach ?>
  		+ '</select>'

  		+ ' <td><input type="text" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
  		+ '</td><td>'

  		+' <div class="form-group">'
  		+'   <input type="file" name="identityphoto['+incr+']"  id="identityphoto" class="file">'
  		+' </div>'
  		+ '</td>' + '</tr>';
  		$("#tbodyForm13").append(tableDataIdentity)
  	}

  }
</script>