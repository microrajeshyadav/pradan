<style type="text/css">

  .checkboxStatus{
    color:red;
  }
  .success{
    color:green;
  }
</style>
<br/>
<section class="content">
 <div class="container-fluid" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Manage offered candidate verification document list</h4>

     </div>

     <?php 
     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');
     if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body"> 

        <?php 

        if (count($getgeneralstatus)==0) {?>

          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <input type="hidden" name="category" id="category" value="<?php echo $getcandateverified->categoryid;?>"> 
                  <?php 
                  if($getcandateverified->categoryid==1)
                  { 
                     ?>
                  <div class="alert alert-danger alert-dismissable alert1">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Candidate <b>“General nomination”</b> form are not submitted. Please coordinate for the same..
                  </div>
                <?php } ?>
                </div>
              </div>
            </div>
          </div>
        <?php }elseif (count($getjoiningreportstatus)==0) {?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Candidate <b>“Joining report”</b> form are not submitted. Please coordinate for the same..
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php  } ?>



        <form name="dadocumentverification" id="dadocumentverification" action="" method="post" enctype="multipart/form-data" >
          <div class="row">
            <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
            <div class="col-lg-4">
              <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
              <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly">
            </div>
            <?php if (!empty($getcandateverified->batch)) { ?>
              <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
              <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php if(!empty($getcandateverified->batch)) echo $getcandateverified->batch; ?>" class="form-control"  readonly="readonly"></div>
            <?php } ?>

            <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
            <div class="col-lg-1" id="candidateprofilepic" style="width:105px; height:133px; padding: 3px;" >
              <img src="<?php if(!empty($getcandateverified->encryptedphotoname)) echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" class="rounded-circle" width="93px" hight="132px"  title="Default Image" alt="Default Image"></div>
            </div>
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-4"></div>
              <div class="col-lg-4"></div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <table id="tblForm09" class="table table-bordered table-striped" >
                <thead>
                 <tr style="background-color: #eee;">
                  <th >
                    Documents to be verified 
                  </th>
                  <th  style="width: 200px; text-align: center">Certificate
                  </th>
                  <th  style="width: 100px;">

                  </th>
                </tr>
              </thead> 
              <tr style="background-color: #fff;">
               <td>10th Standard</td>
               <td style="width: 200px; text-align: center;">



                <?php 
                if (!empty($getcandateverified->encryptmetriccertificate)) {


                  $matriccertificateArray = explode("|",$getcandateverified->encryptmetriccertificate);
                  $image_path='';

                  foreach($matriccertificateArray as $res)
                  {
                   $image_path='datafiles\educationalcertificate/'.$res;

                   if (file_exists($image_path)) {

                    ?>
                    <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"></i></a>
                  <?php } 
                } 
              }
              else 
              {
                ?>
                <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
              <?php } ?>
            </td>

            <td style="width: 200px; text-align: center;">
             <div class="form-check">
              <input type="checkbox" class="filled-in form-check-input doccheck" name="metric_certificate_verified" id="metric_certificate_verified"  required="required" >
              <label class="form-check-label" for="metric_certificate_verified"> 
              Verify </label>
              <!-- <span class="checkboxStatus"></span> -->
            </div>
          </td> 

        </tr>

        <tr style="background-color: #fff;">
          <td>12th Standard</td>
          <td style="width: 100px; text-align: center;">
            <?php if (!empty($getcandateverified->encrypthsccertificate)) 
            {
             $hsccertificateArray = explode("|",$getcandateverified->encrypthsccertificate);
             $image_path='';

             foreach($hsccertificateArray as $res)
             {
               $image_path='datafiles\educationalcertificate/'.$res;

               if (file_exists($image_path)) {
                ?>
                <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>  

              <?php }
              else {   
                ?>

                <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
              <?php } }}?>
            </td>
            <td style="width: 200px; text-align: center;">
              <div class="form-check">
                <input type="checkbox" class="filled-in form-check-input doccheck" name="hsc_certificate_verified" id="hsc_certificate_verified" required="required">
                <label class="form-check-label" for="hsc_certificate_verified">Verify</label>
                <!-- <span class="checkboxStatus"></span> -->
              </div>
            </td>
          </tr>
          <tr style="background-color: #fff;">
            <td style="width: 300px;">Graduation
              <?php 
              if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) { ?> 
                <br>Undertaking  for Marksheet <?php 
                if($getcandateverified->ugmigration_certificate_date != '0000-00-00')
                 {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);?>)<?php } } ?>
              <?php 
              if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) { ?> 
                <br>Undertaking for final Certificate <?php 
                if($getcandateverified->ugdoc_certificate_date != '0000-00-00')
                 {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);?>)<?php } } ?>
             </td>
             
             
                     <td style="width: 100px; text-align: center;">
                      <?php if (!empty($getcandateverified->encryptugcertificate)) { 

                         $ugcertificateArray = explode("|",$getcandateverified->encryptugcertificate);
                            $image_path='';

                            foreach($ugcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?> Final Year Marksheet :
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugmigration_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);} ?>
                        <br>
                        Undertaking for Marksheet 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>




                      <?php if (!empty($getcandateverified->ugdocencryptedImage)) { 

                         $ugdocArray = explode("|",$getcandateverified->ugdocencryptedImage);
                            $image_path='';

                            foreach($ugdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugdoc_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);} ?>
                        <br>
                        Undertaking Certificate 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>
                      </td>
              <td style="width: 200px; text-align: center;">
                <div class="form-check">
                  <input type="checkbox" class="filled-in form-check-input doccheck" name="ug_certificate_verified" id="ug_certificate_verified" required="required">
                  <label class="form-check-label" for="ug_certificate_verified">Verify</label>
                  <!-- <span class="checkboxStatus"></span> -->
                </div>
              </td>

            </tr>
            <?php 

            if (!empty($getcandateverified->encryptpgcertificate)) { ?>  
              <tr style="background-color: #fff;">
                <td>Post Graduation
                  <?php 
                  if (!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) {?><br> Migration Marks Certificate<?php 
                    if($getcandateverified->pgmigration_certificate_date != '0000-00-00')
                      {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);?>)<?php } } ?>
                    <?php 
                        if (!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?><br> Migration Certificate<?php 
                          if($getcandateverified->pgdoc_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);?>)<?php } } ?>
                  </td>
                       <td style="width: 100px; text-align: center;">
                        <?php if (!empty($getcandateverified->encryptpgcertificate)) {

                          $pgcertificateArray = explode("|",$getcandateverified->encryptpgcertificate);
                            $image_path='';

                            foreach($pgcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgmigration_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);} ?>
                            <br>
                            Migration Marks Sheet 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>



                        <?php if (!empty($getcandateverified->pgdocencryptedImage)) {

                          $pgdocArray = explode("|",$getcandateverified->pgdocencryptedImage);
                            $image_path='';

                            foreach($pgdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgdoc_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);} ?>
                            <br>
                            Migration Certificate 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>
                          </td>
                        <td style="width: 200px; text-align: center;">
                          <input type="hidden" name="pg_certificate" id="pg_certificate" value="1">
                          <div class="form-check" >
                            <input type="checkbox" class="filled-in form-check-input doccheck" name="pg_certificate_verified" id="pg_certificate_verified" required="required">
                            <label class="form-check-label" for="pg_certificate_verified">Verify</label>
                            <!--  <span class="checkboxStatus"></span> -->
                          </div>
                        </td>
                      </tr>

                    <?php }  ?>

                    <?php if (!empty($getcandateverified->encryptothercertificate)) {?>  
                      <tr style="background-color: #fff;">
                        <td>Others</td>
                        <td style="width: 100px; text-align: center;">
                          <?php  if ($getcandateverified->encryptothercertificate) { ?>
                            <a href="<?php if(!empty($getcandateverified->encryptothercertificate)) echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptothercertificate;?>" download><i class="fa fa-download"  ></i></a> <?php } ?>
                          </td>
                          <td style="width: 200px; text-align: center;">
                            <input type="hidden" name="other_certificate" id="other_certificate" value="1">
                            <div class="form-check">
                              <input type="checkbox" class="filled-in form-check-input doccheck" name="other_certificate_verified" id="other_certificate_verified" required="required">
                              <label class="form-check-label" for="other_certificate_verified">Verify</label>
                              <!--  <span class="checkboxStatus"></span> -->
                            </div>
                          </td>
                        </tr>
                      <?php }  ?>

                      <?php  if ($getgapyearcount > 0) {
                       foreach ($getgapyearpverified as $key => $value) {


                        ?>  
                        <tr style="background-color: #fff;">
                          <td>Gap Year Document</td>
                          <td style="width: 100px; text-align: center;">
                            <?php  if ($value->encrypteddocumentsname) { ?>
                              <a href="<?php if(!empty($getcandateverified->gapyeardocument)) echo site_url().'datafiles/gapyeardocument/'.$value->encrypteddocumentsname;?>" download><i class="fa fa-download"></i></a> 
                            <?php } ?>
                          </td>
                          <td style="width: 200px; text-align: center;">
                            <input type="hidden" name="gapyear_certificate" id="gapyear_certificate" value="1">
                            <div class="form-check" >
                              <input type="checkbox" class="filled-in form-check-input doccheck" name="gapyear_certificate_verified" id="gapyear_certificate_verified" required="required">
                              <label class="form-check-label" for="gapyear_certificate_verified">Verify</label>
                              <!--  <span class="checkboxStatus"></span> -->
                            </div>
                          </td>
                        </tr>
                      <?php }  }?>

                      <?php if (!empty($getcandateverified->encryptofferlettername)) {?>  
                        <tr style="background-color: #fff;">
                          <td>Signed Offer Letter</td>
                          <td style="width: 100px; text-align: center;">
                           <?php  if ($getcandateverified->encryptofferlettername) { ?>
                            <a href="<?php if(!empty($getcandateverified->encryptofferlettername)) echo site_url().'pdf_offerletters/'.$getcandateverified->encryptofferlettername;?>" download><i class="fa fa-download"></i></a> 
                          <?php } ?>
                        </td>
                        <td style="width: 200px; text-align: center;">
                          <input type="hidden" name="signed_certificate" id="signed_certificate" value="1">
                          <div class="form-check" >
                            <input type="checkbox" class="filled-in form-check-input doccheck" name="signed_certificate_verified" id="signed_certificate_verified" required="required">
                            <label class="form-check-label" for="signed_certificate_verified">Verify</label>
                            <!--  <span class="checkboxStatus"></span> -->
                          </div>
                        </td>
                      </tr>
                    <?php }  ?>


                    <?php if ($getworkexpcount > 0) {

                      foreach ($getworkexpverified as $key => $value) {
                        ?>
                        <tr>
                          <td>Work Experience Certificate , Salary Certificate</td>
                          <td style="width: 100px; text-align: center;">
                            <?php  if ($value->encrypteddocumnetname) { ?>
                              <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download><i class="fa fa-download"  ></i></a> <?php } ?>|
                              <?php  if ($value->encryptedsalaryslip1) { ?> 
                                <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip1;?>" download><i class="fa fa-download"  ></i></a> <?php } ?>| <?php  if ($value->encryptedsalaryslip2) { ?><a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip2;?>" download><i class="fa fa-download"  ></i></a> <?php } ?>|
                                <?php  if ($value->encryptedsalaryslip3) { ?>
                                 <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip3;?>" download><i class="fa fa-download"  ></i></a>   <?php } ?> </td>
                                 <td style="width: 200px; text-align: center;">
                                  <input type="hidden" name="workexp_certificate" id="workexp_certificate" value="1">
                                  <div class="form-check">
                                    <input type="checkbox" class="filled-in form-check-input doccheck" name="workexp_certificate_verified[]" id="workexp_certificate_verified_<?php echo $key;?>" required="required">
                                    <label class="form-check-label" for="workexp_certificate_verified_<?php echo $key;?>">Verify </label>
                                    <!-- <span class="checkboxStatus"></span> -->
                                  </div>                              
                                </td>
                              </tr>
                            <?php } }  ?>
                          </table>
                        </div>
                      </div>
                      <?php 


                  if($getcandateverified->categoryid==1)
                  { 
                    

                      //echo $get_joiningreportstatus->status; echo "fvfdsfdsf" ?>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <table id="tblForm13" class="table table-bordered table-striped" >
                          <thead>
                           <tr style="background-color: #eee;">
                            <th >Form</th>
                            <th style="width: 200px; text-align: center" >PDF</th>
                            <th style="width: 100px;"></th>
                          </tr> 
                        </thead>
                        <tbody >
                         <tr>
                          <td >

                           <?php  if(count($getgeneralnominationformpdf) ==0) { ?>

                             General Nomination
                           <?php }  else if (isset($getgeneralnominationformpdf->status) && $getgeneralnominationformpdf->status == 2) { ?>
                             General Nomination

                           <?php }else{ ?>
                             <a href="<?php echo site_url().'Joining_of_da_document_verification/GeneralNominationWithWitnessform/'.$token;?>" target="_blank">General Nomination</a>
                           <?php } ?>

                         </td>
                         <td style="width: 100px; text-align: center">

                          <?php  if (count($getgeneralnominationformpdf) ==0) { ?>


                            <input type="hidden" name="generalnominationpdf" id="generalnominationpdf" value="0">
                          <?php }  else if (isset($getgeneralnominationformpdf->status) && $getgeneralnominationformpdf->status == 2) { ?>
                            <a href="<?php if(!empty($getgeneralnominationformpdf->filename)) echo site_url().'/pdf_generalnominationform/'.$getgeneralnominationformpdf->filename;?>" download  ><i class="fa fa-download"  ></i></a>
                            <input type="hidden" name="generalnominationpdf" id="generalnominationpdf" value="1">
                          <?php }else{ ?>
                            <!-- <a href="#"><i class="fa fa-download"  ></i></a> -->
                            <input type="hidden" name="generalnominationpdf" id="generalnominationpdf" value="0">
                          <?php } ?>
                        </td>
                        <td style="width: 200px; text-align: center"> 
                          <div class="form-check">
                            <input type="checkbox" class="filled-in form-check-input doccheck" name="general_nomination_form" id="general_nomination_form" required="required">
                            <label class="form-check-label" for="general_nomination_form" >Verify </label>
                            <!--  <span class="checkboxStatus"></span> -->
                          </div>   
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php  if (count($get_joiningreportstatus) ==0) { ?>

                            Joining Report
                          <?php }  else if (isset($get_joiningreportstatus->status) && $get_joiningreportstatus->status==2) {  ?>
                            Joining Report  

                          <?php }else{ ?>
                            <a href="<?php echo site_url().'Joining_of_da_document_verification/Joining_report_form/'.$token;?>" target="_blank" >Joining report for DAs</a>
                          <?php } ?>
                        </td>
                        <td style="width: 100px; text-align: center">
                         <?php   if (count($get_joiningreportstatus) ==0) { ?>

                           <input type="hidden" name="joiningreportda" id="joiningreportda" value="0">
                         <?php }  else if (isset($get_joiningreportstatus->status) && $get_joiningreportstatus->status==2) {  ?>
                           <a href="<?php echo site_url().'pdf_joiningreportforda/'.$get_joiningreportstatus->filename;?>" download ><i class="fa fa-download"></i></a>
                           <input type="hidden" name="joiningreportda" id="joiningreportda" value="1">
                         <?php }else{ ?>
                           <!--  <a href="<?php //echo site_url().'Joining_of_da_document_verification/JoiningReportForm/'.$token;?>" arget="_blank"><i class="fa fa-download"  ></i></a> -->
                           <input type="hidden" name="joiningreportda" id="joiningreportda" value="0">
                         <?php } ?>
                       </td>
                       <td style="width: 200px; text-align: center;"> <div class="form-check">
                        <input type="checkbox" class="filled-in form-check-input doccheck" name="joining_report_da" id="joining_report_da"  required="required">
                        <label class="form-check-label" for="joining_report_da"  disable="desiable">Verify </label>
                        <!-- <span class="checkboxStatus"></span> -->
                      </div>   
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        <?php } ?>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <table id="" class="table table-bordered table-striped" >
              <thead>
               <tr style="background-color: #eee;">
                <th>
                Other Document Uploads </th>
                <th></th>
                <th>
                 <button type="button" id="btnRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                 <button type="button" id="btnAddRow" class="btn btn-warning btn-xs">Add</button>
               </th>
             </tr> 
             <tr style="background-color: #eee;">
               <th >Document Name </th>
               <th >Document For </th>
               <th >Upload </th>
             </tr> 
           </thead>
           <tbody id="tbodyForm09" >
            <tr id="tbodyForm09" style="background-color: #fff;">
              <td>
               <?php 
               $options = array('' => 'Select Document Name');
               foreach($getdocuments as$key => $value) {
                $options[$value->id] = $value->document_name;
              }
              echo form_dropdown('documentname[]', $options, set_value('documentname'), 'class="form-control" data-toggle="tooltip" title="Select  Document !"  id="documentname" ');
              ?>
              <?php echo form_error("documentname");?>
            </td>
            <td style="width: 250px; text-align: center;">
             <?php 
             $options = array('' => 'Select Document Type');
             foreach($getdocuments as$key => $value) {
              $options[$value->id] = $value->document_type;
            }
            echo form_dropdown('documenttype[]', $options, set_value('documenttype'), 'class="form-control" data-toggle="tooltip" title="Select Document Type !"  id="documenttype" ');
            ?>
            <?php echo form_error("documenttype");?>
          </td>
          <td style="width: 200px; text-align: center;">
            <div class="form-group">
              <input type="file" name="otherdocumentupload[]" id="otherdocumentupload"  accept="application/pdf, doc/*" >
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <table id="tblcomment" class="table table-bordered table-striped" >
    <thead>
     <tr style="background-color: #eee;">
      <th>Status <span style="color:red;">*</span> </th>
      <th>Comments <span style="color:red;">*</span></th>
    </tr> 
  </thead>
  <tbody >
    <tr >
      <td>
        <select name="verified_status" id="verified_status" class="form-control" required="required">
          <option value="">Select</option>
          <option value="1">Approved</option> 
          <option value="2">Reject</option>
        </select>
      </td>
      <td>
       <textarea name="comments" id="comments" class="form-control" style="resize: none;" required="required" ></textarea>
     </td>
   </tr>
 </tbody>
</table>
</div>

</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <table id="tblcomment" class="table table-bordered table-striped" >
    <thead>
     <tr style="background-color: #eee;">
      <th>Self Declaration <span style="color:red;">*</span> </th>
    </tr> 
  </thead>
  <tbody >
    <tr >
      <td>
        <div class="form-check">
          <input type="checkbox" class="filled-in  form-check-input doccheck" name="self_declaration_tc" id="self_declaration_tc"  required="required">
          <label class="form-check-label" for="self declaration content">Self Declaretion Content. </label>
        </div>
      </td>
   </tr>
 </tbody>
</table>
</div>

</div>

<div class="panel-footer text-right">
 <?php 



 if (count($getgeneralstatus) > 0 &&  count($getjoiningreportstatus) > 0  && $getcandateverified->categoryid==1) { ?>
   <button class="btn btn-success btn-sm" type="submit" onclick="return checkgernalandjoiningform()" name="submitbtn" id="submitbtn" value="senddatasubmit" >Submit</button>
 <?php  }
 else if($getcandateverified->categoryid==2 || $getcandateverified->categoryid==3){  ?>
   <button class="btn btn-success btn-sm" type="submit" onclick="return checkgernalandjoiningform()" name="submitbtn" id="submitbtn" value="senddatasubmit" >Submit</button>

  <?php  } else {?>
   <button class="btn btn-success btn-sm" type="submit" onclick="return checkgernalandjoiningform()" name="submitbtn" id="submitbtn" value="senddatasubmit" disabled="disabled">Submit</button>
 <?php  } ?>
</div>

</form>

</div>
</div>
</div>

</div>
<!-- #END# Exportable Table -->
</div>
</section>
</section>
<script type="text/javascript">
 $(document).ready(function(){
  $("#candidateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
               })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



}); 




 function checkgernalandjoiningform(){
  var getgeneralnominationformpdf = $('#generalnominationpdf').val();
  var joiningreportdapdf = $('#joiningreportda').val();

  var getdocumentname = $('#documentname').val();
  var cate=$("#category").val();

  // var getdocumentname1 = $('#documentname_1').val();

  // var getdocumentname2 = $('#documentname_2').val();

  if ($('#verified_status').val()==1) {

    if(!$('#metric_certificate_verified').is(':checked')){
     alert("Please verified 10th standard certificate !!!");
     $('#metric_certificate_verified').focus();
     return false;
   }

   if(!$('#hsc_certificate_verified').is(':checked')){
     alert("Please verified 12th standard certificate !!!");
     $('#hsc_certificate_verified').focus();
     return false;
   }

   if(!$('#ug_certificate_verified').is(':checked')){
     alert("Please verified graduation certificate !!!");
     $('#ug_certificate_verified').focus();
     return false;
   }


   if($('#pg_certificate').val()==1){
    if(!$('#pg_certificate_verified').is(':checked')){
     alert("Please verified post graduation certificate !!!");
     $('#pg_certificate_verified').focus();
     return false;
   }
 }


 if($('#other_certificate').val()==1){
  if(!$('#other_certificate_verified').is(':checked')){
   alert("Please verified other certificate !!!");
   $('#other_certificate_verified').focus();
   return false;
 }
}

if($('#workexp_certificate').val()==1){
  if(!$('#workexp_certificate_verified_0').is(':checked')){
   alert("Please verified work experience certificate !!!");
   $('#workexp_certificate_verified_0').focus();
   return false;
 }
}

if(getgeneralnominationformpdf ==0 ){
  alert("Please first genrate pdf for general nomination !!!");
  $('#general_nomination_form').focus();
  return false;

}
else if(empty(getgeneralnominationformpdf) && cate==2 ){
  $("#general_nomination_form").prop("required", false);
  

}

else if(joiningreportdapdf ==0 ){
  alert("Please first genrate pdf for Joining report !!!");
  $('#joining_report_da').focus();
  return false;
}
else if(empty(getgeneralnominationformpdf) && cate==2 ){
  
  $("#joining_report_da").prop("required", false);

}

if(!$('#general_nomination_form').is(':checked')){
 alert("Please verified general nomination !!!");
 $('#general_nomination_form').focus();
 return false;
}
if(!$('#joining_report_da').is(':checked')){
 alert("Please verified joining report !!!");
 $('#joining_report_da').focus();
 return false;
}

if(getdocumentname !=''){
  if ($('#documenttype').val() == '') {
    $('#otherdocumentupload').prop('required',true);
    alert("Please Select Document For !!!");
    $('#documenttype').focus();
    return false;
  }
}

}

}   

$("#verified_status").change(function(){
 var verifiedstatus = $('#verified_status').val();
 if(verifiedstatus ==2){
  $('#generalnominationpdf').val('1');
  $('#joiningreportda').val('1');
  $('.doccheck').attr('disabled', true);
// $('#hsc_certificate_verified').prop();
// $('#metric_certificate_verified')

}
});
</script>

<script type="text/javascript">
  $("#btnRemoveRow").click(function() {
    if($('#tblForm09 tr').length-3>1)
      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);
    
    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    //addDatePicker();
  });

  var srNoGlobal=0;
  var inc = 0;


  function insertRows(count) {
    srNoGlobal = $('#tbodyForm09 tr').length+1;
    var tbody = $('#tbodyForm09');
    var lastRow = $('#tbodyForm09 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inc++
      cloneRow = lastRow.clone();
      var tableData = '<tr>'
      + ' <td>'
      + '<select class="form-control" name="documentname['+inc+']" required="required" id="documentname_'+inc+'" >'
      + '<option value="">Select Document Name</option>'
      <?php foreach ($getdocuments as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->document_name;?></option>'
      <?php endforeach ?>
      + '</select>'
      + '</td>'
      + '<td>'
      +'<select class="form-control" name="documenttype['+inc+']" id="documenttype_'+inc+'" required="required">'
      + '<option value="">Select Document Type</option>'
      <?php foreach ($getdocuments as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->document_type;?></option>'
      <?php endforeach ?>
      + '</select>'
      + '</td>'

      +' <td><div class="form-group">'
      +' <input type="file" name="otherdocumentupload['+inc+']"  class="file" accept="application/pdf, image/*" required="required" id="otherdocumentupload_'+inc+'">'
      +' </div></td>'
      + '</tr>';
      $("#tbodyForm09").append(tableData)
    }

  }


</script>