<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
             <div class="header">
              <h2>DA Document Verification</h2><br>
            </div>
            <div class="body">
              <form name="joinDA" action="" method="post" enctype="multipart/form-data" >
                <div class="row">
                  <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">DA Name :</label></div>
                  <div class="col-lg-4">
                    <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
                    <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
                  </div>
                  <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                  <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control" readonly="readonly"></div>
                  <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                  <div class="col-lg-1" id="candidateprofilepic" >
                    <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-2"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <table id="tblForm09" class="table table-bordered table-striped" >
                      <thead>
                       <tr>
                        <th style="background-color: #3CB371; color: #fff;">
                          Documents to be verified 
                        </th>
                        <th  style="background-color: #3CB371; color: #fff;">
                          Certificate
                        </th>
                        <th  style="background-color: #3CB371; color: #fff;">

                        </th>
                      </tr>
                    </thead> 

                    <tr>
                     <td>10th Standard</td>
                     <td>
                       <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptmetriccertificate;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a></td>
                       <td>
                         <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="metric_certificate_verified" id="metric_certificate_verified" 
                          <?php if($getverified->metric_certificate =='on') {?>
                           Checked="Checked";
                        <?php } ?>
                             >
                          <label class="form-check-label" for="metric_certificate_verified">verified</label>
                        </div>
                      </td> 
                    </tr>
                    <tr>
                      <td>12th Standard</td>
                      <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encrypthsccertificate;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                        <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="hsc_certificate_verified" id="hsc_certificate_verified"  <?php if($getverified->hsc_certificate =='on') {?>
                           Checked="Checked";
                        <?php } ?> >
                          <label class="form-check-label" for="hsc_certificate_verified">verified</label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Under Graduation</td>
                      <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptugcertificate;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                        <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="ug_certificate_verified" id="ug_certificate_verified"  <?php if($getverified->ug_certificate =='on') {?>
                           Checked="Checked";
                        <?php } ?> >
                          <label class="form-check-label" for="ug_certificate_verified">verified</label>
                        </div>
                      </td>

                    </tr>
                    <?php if (!empty($getcandateverified->encryptpgcertificate)) {?>  
                      <tr>
                        <td>Post Graduation</td>
                        <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptpgcertificate;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                        <td>
                          <div class="form-check">
                            <input type="checkbox" class="filled-in form-check-input" name="pg_certificate_verified" id="pg_certificate_verified"  <?php if($getverified->pg_certificate =='on') {?>
                           Checked="Checked";
                        <?php } ?> >
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                          </div>
                        </td>
                      </tr>
                    <?php }  ?>
                    <?php if (!empty($getcandateverified->originalothercertificate)) {?>  
                      <tr>
                        <td>Post Graduation</td>
                        <td><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->originalothercertificate;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                        <td>
                          <div class="form-check">
                            <input type="checkbox" class="filled-in form-check-input" name="other_certificate_verified" id="other_certificate_verified" <?php if($getverified->other_certificate =='on') {?>
                           Checked="Checked";
                        <?php } ?> >
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                          </div>
                        </td>
                      </tr>
                    <?php }  ?>

                    <?php if ($getworkexpcount > 0) {
                      foreach ($getworkexpverified as $key => $value) {
                      ?>
                    <tr>
                      <td>Work Experience Certificate</td>
                      <td><a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a> </td>
                      <td>
                        <div class="form-check">
                          <input type="checkbox" class="filled-in form-check-input" name="workexp_certificate_verified[]" id="workexp_certificate_verified_<?php echo $key;?>"

                          <?php if($getworkexperinceverifiedvalue[$key]->experience_verified =='on') {?>
                           Checked="Checked";
                        <?php } ?> >
                          <label class="form-check-label" for="workexp_certificate_verified_<?php echo $key;?>">verified</label>
                        </div>                              
                      </td>
                    </tr>
                    <?php } }  ?>
                  </table>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <table id="tblForm13" class="table table-bordered table-striped" >
                  <thead>
                   <tr>
                    <th style="background-color: #3CB371; color: #fff;">Form Name</th>

                    <th style="background-color: #3CB371; color: #fff;">Document</th>

                    <th style="background-color: #3CB371; color: #fff;"></th>
                  </tr> 
                </thead>
                <tbody >
                  <tr>
                    <td><a href="<?php echo site_url().'Joining_of_DA_document_verification/GeneralNominationWithWitnessform/'.$token;?>" download>General Nomination form</a></td>

                    <td>  
                      <?php if ($getgeneralnominationformpdf->status == 2) { ?>
                    <a href="<?php echo site_url().'/pdf_generalnominationform/'.$getgeneralnominationformpdf->filename;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a>
                   <?php }else{ ?>
                    <a href="#"><i class="fa fa-download" style="font-size:25px;"></i></a>
                  <?php } ?>
                  </td>
                    <td> <div class="form-check">
                      <input type="checkbox" class="filled-in form-check-input" name="general_nomination_form" id="general_nomination_form" <?php if($getverified->general_nomination_form =='on') {?>
                           Checked="Checked";
                        <?php } ?> >
                      <label class="form-check-label" for="general_nomination_form" >verified</label>
                    </div>   
                  </td>
                </tr>
                <tr>
                  <td>Joining report for DAs</td>
                  <td>
                  <?php if ($getjoiningreportstatus->status==1 && $getjoiningreportstatus->filename !='') {  ?>
                     <a href="<?php echo site_url().'pdf_joiningreportforda/'.$getjoiningreportstatus->filename;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a>
                  <?php }else{ ?>
                     <a href="<?php echo site_url().'Joining_of_DA_document_verification/JoiningReportForm/'.$token;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a>
                  <?php } ?>
  
                   </td>
                  <td> <div class="form-check">
                    <input type="checkbox" class="filled-in form-check-input" name="joining_report_da" id="joining_report_da"  <?php if($getverified->joining_report_da =='on') {?>
                           Checked="Checked";
                        <?php } ?>  >
                    <label class="form-check-label" for="joining_report_da">verified</label>
                  </div>   
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

   <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th  style="background-color: #3CB371; color: #fff;">
             Other Document Uploads </th>
              <th style="background-color: #3CB371; color: #fff;"></th>
               <th style="background-color: #3CB371; color: #fff; text-align: right; ">
               <button type="button" id="btnRemoveRow" class="btn btn-danger btn-xs">Remove</button>
              <button type="button" id="btnAddRow" class="btn btn-warning btn-xs">Add</button>
            </th>
          </tr> 
          <tr>
             <th style="background-color: #3CB371; color: #fff;">Document Name </th>
            <th style="background-color: #3CB371; color: #fff;">Document Type </th>
            <th style="background-color: #3CB371; color: #fff;"> Upload </th>
          </tr> 
        </thead>
        <tbody id="tbodyForm09" >
           <input type="hidden" name="otherdocumentcount" id="otherdocumentcount" value="<?php echo $countotherdoc;?>">
        <?php if ($countotherdoc == 0) {?>
          <tr id="tbodyForm09">
            <td>
               <?php 
                 $options = array('' => 'Select Document Name');
                 foreach($getdocuments as$key => $value) {
                  $options[$value->id] = $value->document_name;
                }
                echo form_dropdown('documentname[]', $options, set_value('documentname'), 'class="form-control" data-toggle="tooltip" title="Select  Document !"  id="documentname"');
                ?>
                <?php echo form_error("documentname");?>
            </td>
            <td>
               <?php 
                 $options = array('' => 'Select Document Type');
                 foreach($getdocuments as$key => $value) {
                  $options[$value->id] = $value->document_type;
                }
                echo form_dropdown('documenttype[]', $options, set_value('documenttype'), 'class="form-control" data-toggle="tooltip" title="Select Document Type !"  id="documenttype"');
                ?>
                <?php echo form_error("documenttype");?>
            </td>
            <td>
              <div class="form-group">
                <input type="file" name="otherdocumentupload[]" id="otherdocumentupload" accept="application/pdf, image/*" >
              </div>
            </td>
          </tr>
        <?php }else{ 

          foreach ($getotherdocdetails as $key => $val) {
            ?>
         <tr id="tbodyForm09">
            <td>
               <?php 
                 $options = array('' => 'Select Document Name');
                 foreach($getdocuments as$key => $value) {
                  $options[$value->id] = $value->document_name;
                }
                echo form_dropdown('documentname[]', $options, $val->documentname, 'class="form-control" data-toggle="tooltip" title="Select  Document !"  id="documentname" ');
                ?>
                <?php echo form_error("documentname");?>
            </td>
            <td>
               <?php 
                 $options = array('' => 'Select Document Type');
                 foreach($getdocuments as$key => $value) {
                  $options[$value->id] = $value->document_type;
                }
                echo form_dropdown('documenttype[]', $options, $val->documenttype, 'class="form-control" data-toggle="tooltip" title="Select Document Type !"  id="documenttype" ');
                ?>
                <?php echo form_error("documenttype");?>
            </td>
            <td>
              <div class="form-group">
                <div class="col-xs-12">
                <div class="col-xs-9">
                   <input type="file" name="otherdocumentupload[]" id="otherdocumentupload" accept="application/pdf, image/*" >                   
                </div>
                <div class="col-xs-3">
                  <input type="hidden" name="oldotherdocumentupload[]" value="<?php echo $val->documentupload;?>">
                  <a href="<?php echo site_url().'otherdocuments/'.$val->documentupload;?>" download><i class="fa fa-download" style="font-size:25px;"></i></a>
                </div>
                </div>
              </div>
            </td>
          </tr>
        <?php } } ?>
        </tbody>
      </table>
    </div>
  </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th style="background-color: #3CB371; color: #fff;">Comments </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
            <td><textarea name="comments" id="comments" class="form-control" required="required"><?php echo $getverified->comment;?></textarea>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th style="background-color: #3CB371; color: #fff;">Status </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
           
            <td>
              <select name="verified_status" id="verified_status" class="form-control" required="required">
              <option>Select </option>
              <?php if ($getverified->status==0) {?>
              <option value="0" selected="selected">Not Approved</option>
               <option value="1">Approved</option> 
            <?php }else{ ?>
              <option value="0" >Not Approved</option>
              <option value="1" selected="selected">Approved</option> 
            <?php } ?>
            </select>
            </td>

          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-4 text-center">
      <input class="btn btn-warning" type="reset" value="Reset">
      <button class="btn btn-primary" type="submit" name="savebtn" value="senddatasave" id="savebtn">Save</button>
      <button class="btn btn-success" type="submit" name="submitbtn" id="submitbtn" value="senddatasubmit">Verified</button>
    </div>
    <div class="col-lg-4"></div>
  </div>
</form>
</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
 $(document).ready(function(){
  $("#candidateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
               })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>
<script type="text/javascript">
  $("#btnRemoveRow").click(function() {
    if($('#tblForm09 tr').length-1>1)
      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);
    
    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    //addDatePicker();
  });

 var srNoGlobal=0;
 var inc = 0;

  var otherdoccount = $('#otherdocumentcount').val();

  if(otherdoccount =='NULL'){
   var srNoGlobal=0;
   var inc = 0;
 }else{
   var srNoGlobal = otherdoccount;
   var inc = otherdoccount;
 }

 

 function insertRows(count) {
  srNoGlobal = $('#tbodyForm09 tr').length+1;
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;

  for (i = 1; i <= count; i++) {
    inc++
    cloneRow = lastRow.clone();
    var tableData = '<tr>'
    + ' <td>'
    + '<select class="form-control" name="documentname['+inc+']" required="required">'
    + '<option value="">Select Document Name</option>'
    <?php foreach ($getdocuments as $key => $value): ?>
      + '<option value=<?php echo $value->id;?>><?php echo $value->document_name;?></option>'
    <?php endforeach ?>
    + '</select>'
    + '</td>'
    + '<td>'
    +'<select class="form-control" name="documenttype['+inc+']" required="required">'
      + '<option value="">Select Document Type</option>'
     <?php foreach ($getdocuments as $key => $value): ?>
      + '<option value=<?php echo $value->id;?>><?php echo $value->document_type;?></option>'
    <?php endforeach ?>
    + '</select>'
    + '</td>'

    +' <td><div class="form-group">'
    +' <input type="file" name="otherdocumentupload['+inc+']" id="otherdocumentupload['+inc+']" class="file" required="required">'

    +' </div></td>'
    + '</tr>';
    $("#tbodyForm09").append(tableData)
  }

}
</script>