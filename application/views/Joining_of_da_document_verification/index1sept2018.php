<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
    ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
          <?php //print_r($candidatebdfformsubmit); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
             <div class="header">
              <h2>Manag DA Verification Document </h2><br>
            </div>
            <div class="body">
           <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center">Id</th>
                       <th class="text-center">Employee Code</th>
                       <th class="text-center">DA Name</th>
                      <th class="text-center">Batch</th>
                      <th class="text-center">Photo</th>
                       <th class="text-center">Comment</th>
                      <th class="text-center">Status</th>
                     <th class="text-center">Command</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (count($candidatebdfformsubmit)==0) { ?>
                      <tr>
                      <td colspan="8" style="color:red; text-align: center;">No Record Found !!!</td>
                    </tr>
                 <?php    }else{ ?>
                    
                    <?php $i=0; foreach($candidatebdfformsubmit as $key => $val){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $val->emp_code; ?></td>
                      <td class="text-center"><?php echo $val->candidatefirstname.' '.$val->candidatemiddlename.' '.$val->candidatelastname; ?></td>
                      <td class="text-center"><?php echo $val->batch;?></td>
                       <td class="text-center">
                         <img src="<?php echo site_url().'datafiles/'. $val->encryptedphotoname;?>" width="50px" hight="50px" title="Profile Image">
                       </td>
                       <td class="text-center"><?php echo $val->comment;?></td>
                       <?php if ($val->status==0) { ?>
                       <td class="text-center"><button type="button" class="btn btn-danger">Not Verified</button></td>
                     <?php }else{ ?>
                       <td class="text-center"><button type="button" class="btn btn-success">Verified</button></td>
                     <?php } ?>
                      
                      <td class="text-center">
                         <?php if ($val->status==0) { ?>
                        <a href="<?php echo site_url('Joining_of_da_document_verification/verification/'.$val->candidateid);?>" style="padding : 4px;" title="verification"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
                      <?php }else{ ?>

                         <a href="<?php echo site_url('Joining_of_da_document_verification/view/'.$val->candidateid);?>" style="padding : 4px;" title="verification"><span class="glyphicon glyphicon-eye-open" style="font-size : 15px; margin-top: 8px;"></span>
                      <?php } ?>
                   
                      </td>
                    </tr>
                    <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>
<script type="text/javascript">
 $(document).ready(function(){
            $("#candidateid").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
                    type: 'POST',
                    dataType: 'json',
                })
                .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                   $("#batchid").val(data.batch);
                   $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });
      
        });     
  
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>