  <div class="container-fluid">
    <!-- Exportable Table -->
    <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px;">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ 
          ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('tr_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('er_msg');?>. </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>

              <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                 <h4 class="col-md-7 panel-title pull-left"> Document Verification </h4>

               </div>
               <hr class="colorgraph"><br>
             </div>
             <div class="body">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="table-responsive">
                <table id="dadocumentverification" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center">S.No.</th>
                      <th class="text-center">Category</th>
                      <th class="text-center">Campus Name</th>
                      <th class="text-center">Name</th>
                      <th class="text-center">Gender</th>
                      <th class="text-center">Batch</th>
                      <th class="text-center">Field guide Name</th>
                      <th class="text-center">Photo</th>
                      <th class="text-center">Comment</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Command</th>
                    </tr>
                  </thead>

                  <?php if (count($candidatebdfformsubmit)==0) { ?>
                   <tbody>
                   </tbody>

                 <?php }else{ ?>

                  <?php $i=0; foreach($candidatebdfformsubmit as $key => $val){ ?>
                   <tbody>
                    <tr style="background-color: #fff;">
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $val->categoryname; ?></td> 
                      <td class="text-center"><?php echo $val->campusname;?></td>
                      <td class="text-center"><?php echo $val->candidatefirstname.' '.$val->candidatemiddlename.' '.$val->candidatelastname; ?></td>
                      <td class="text-center"><?php echo $val->gender;?></td>
                      <td class="text-center"><?php echo $val->batch;?></td>
                      <td class="text-center"><?php echo $val->name;?></td>
                      <td class="text-center" >
                      <?php  
                              if($val->encryptedphotoname)
                              {
                             $file_path=''; 
                            $file_path=FCPATH.'datafiles/'. $val->encryptedphotoname;

                            
                            if(file_exists($file_path))
                           { 

                              //echo "hello";
                              ?>
                                <img src="<?php echo site_url().'datafiles/'. $val->encryptedphotoname;?>"  class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
                          <?php
                            }
                            else 
                            {
                              ?>
                              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
                        
                              
                             <?php 
                            }
                          }
                            else
                            {
                              ?>
                              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" class= "rounded-circle" width="93px" hight="132px" title="Profile Image">
                        
                              
                             <?php 
   
                            }
                            ?>
                       
                       <?php//} else { ?>
                        
                          <?php //} 

                         ?>
                         
                     </td>
                     <td class="text-center"><?php echo $val->comment;?></td>

                     <?php if ($val->status==1) { ?>
                       <td class="text-center"> <span class="badge badge-pill badge-success">Verified</span></td>
                     <?php }elseif ($val->status==2) { ?>
                       <td class="text-center"><span class="badge badge-pill badge-danger">Reject</span></td>
                     <?php }else{ ?>
                       <td class="text-center"><span class="badge badge-pill badge-danger">N.A.</span></td>
                     <?php  } ?>
                     
                     <td class="text-center">
                       <?php if ($val->status==0) { ?>
                        <a href="<?php echo site_url('Joining_of_da_document_verification/verification/'.$val->candidateid);?>" style="padding : 4px;" title="verification"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"> <i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></span></a>
                      <?php }else{ ?>

                       <a href="<?php echo site_url('Joining_of_da_document_verification/view/'.$val->candidateid);?>" style="padding : 4px;" title="verification"><span class="glyphicon glyphicon-eye-open" style="font-size : 15px; margin-top: 8px;"><i class="fa fa-eye" aria-hidden="true" id="usedbatchid"></i></span></a>
                       <?php } ?>

                     </td>
                   </tr>
                 </tbody>
                 <?php $i++; } } ?>
                 
               </table>
             </div>
           </div>
           </div>
         </div>
       </div>
     </div>
     <!-- #END# Exportable Table -->
   </div>

   <script type="text/javascript">
     $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();  
       $('#dadocumentverification').DataTable(); 



     });     


   </script>