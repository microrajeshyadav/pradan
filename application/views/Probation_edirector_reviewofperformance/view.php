  
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <?php 

     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');

     if(!empty($tr_msg)){ ?>   
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>       
      <?php } else if(!empty($er_msg)){?>    
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>          
        <?php } ?>
        <?php //print_r($getstaffprovationreviewperformance); ?> 
        <form name="staffreviewfrm" id="staffreviewfrm" method="POST" action="">
         <div class="panel-body">
          <div class="text-center">
            <p><h5>Part- I</h5></p>
            <p>(<em>to be filled in by the Finance-Personnel-MIS Unit)</em></p>
          </div>
          <div class="row" style="line-height: 3">
            <div class="col-md-12">
              1. Name : <input type="text" name="staffname" id="staffname"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly"  value="<?php if(!empty($getstaffprovationreviewperformance->name)) echo $getstaffprovationreviewperformance->name; ?>">
            </div>
            <div class="col-md-12">
             2. Designation : <input type="text" name="staffdesignation" id="staffdesignation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprovationreviewperformance->desname)) echo $getstaffprovationreviewperformance->desname; ?>" readonly="readonly">
           </div>     
           <div class="col-md-12">
            3. Employee Code : <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprovationreviewperformance->emp_code)) echo $getstaffprovationreviewperformance->emp_code; ?>" readonly="readonly">
          </div>
     <!--  <div class="col-md-12">
        4. Project  :<input type="text" name="staffproject" id="staffproject"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->emp_project; ?>" readonly="readonly">
      </div> -->
      <div class="col-md-12">
        5. Location : <input type="text" name="stafflocation" id="stafflocation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprovationreviewperformance->officename)) echo $getstaffprovationreviewperformance->officename; ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        6. Date of Appointment: <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprobationreview->date_of_appointment)) echo $this->gmodel->changedatedbformate($getstaffprobationreview->date_of_appointment); ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        7. Period of Review : From <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_from); ?>" readonly="readonly"> (date) To <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprobationreview->period_of_review_to)) echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_to); ?>" readonly="readonly"> (date)
      </div>
    </div>
    <p></p>
    <p></p>
    <p><strong>TO BE REVIEWED BY</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1. Name :<input type="text" name="review_by" id="review_by" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffpersonneldetals->name)) echo $getstaffpersonneldetals->name;?>" readonly="readonly">
      </div>

      <div class="col-md-12">
       2. Designation :<input type="text" name="review_designation" id="review_designation" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffpersonneldetals->desname)) echo $getstaffpersonneldetals->desname;?>" readonly="readonly">
     </div>

     <div class="col-md-12">
      3. Employee Code : <input type="text" name="review_emp_code" id="review_emp_code" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffpersonneldetals->emp_code)) echo $getstaffpersonneldetals->emp_code;?>" readonly="readonly">
    </div>

    <div class="col-md-12">
      5. Location : <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffpersonneldetals->officename)) echo $getstaffpersonneldetals->officename;?>" readonly="readonly">
    </div>
  </div>
  <hr/>
  <p><strong>Notes for the Reviewer</strong></p>
  <p></p>
  <ol>
    <li>Please assess the employee&rsquo;s performance during the period of probation mentioned above (Item 6).</li>
  </ol>
  <p></p>
  <ol start="2">
    <li>Please be objective in your assessment. Each factor should be assessed independently, uninfluenced by assessment of the other factor(s).</li>
  </ol>
  <p></p>
  <ol start="3">
    <li>Please return the duly filled in assessment report in a closed cover marked <em>confidential</em> to</li>
  </ol>
  <p>the Finance-Personnel-MIS Unit latest by <input type="text" name="latestby" id="latestby" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffpersonneldetals->latestby)) echo $this->gmodel->changedatedbformate($getstaffpersonneldetals->latestby);?>" readonly="readonly"> (date).</p>
  <p></p>
  <?php if ($this->loginData->RoleID ==2 || $this->loginData->RoleID ==18) { ?>
    <div class="text-center">
      <p><h5>Part-II</h5></p>
      <p><h5>REVIEW</h5></p>
      <p>(<em>to be filled in by the Reviewing Supervisor</em>)</p>
    </div>
    <p>The probation has provided you an opportunity to interact with and observe the probationer in the work setting for six months. This is a significant period of time, for both sides.</p>
    <p></p>
    <p>&ldquo;<strong>How do you see the Probationer fulfilling the primary responsibilities s/he would need to shoulder to be an effective employee of this organization?</strong>&rdquo;</p>
    <p><strong></strong></p>
    <p>Could you please write about <em>500 words</em> addressing the issue (primary responsibilities s/he is expected to handle). Please give anecdotes/examples wherever possible.</p>


    <p><strong>Work Habits and Attitudes</strong></p>
    <p></p>
    <p>How would you rate the probationer&rsquo;s interest in and aptitude for work? What is the quality of her/his work? Does s/he demonstrate proaction and take initiative? Is s/he serious about work, and shows a sense of commitment to it? How resourceful is the probationer? Is s/he punctual, disciplined and regular at work? How does s/he fit into PRADAN&rsquo;s work culture? What is her/his attitude to the communities we work with? How does s/he deal with the conditions of work, absence of regular timings, physical hardship and unstructured work situation?</p>

    <textarea name="work_habits_and_attitudes" readonly="readonly" maxlength="500" id="work_habits_and_attitudes" cols="40" rows="10" style="width:1000px;"><?php if(!empty($getstaffprobationreview->work_habits_and_attitudes)) echo $getstaffprobationreview->work_habits_and_attitudes;?></textarea>

    <p><strong>Conduct and Social Maturity</strong></p>
    <p></p>
    <p>What is the probationer&rsquo;s general behaviour like? Does s/he possess the requisite inter-personal competence? What is her/his demeanour such as&mdash;e.g., is s/he moody, short-tempered, adjusting, reserved, talkative, a gossip, etc.?</p>
    <textarea name="conduct_and_social_maturity" maxlength="500" readonly="readonly" id="conduct_and_social_maturity" cols="40" rows="10" style="width:1000px;"><?php if(!empty($getstaffprobationreview->conduct_and_social_maturity)) echo $getstaffprobationreview->conduct_and_social_maturity;?></textarea>
    <p><strong>Integrity</strong></p>

    <p>Is the probationer's integrity<em> </em><br>

     <input type="radio" name="questionable" id="questionable" value="questionable" <?php if ($getstaffprobationreview->integrity =="questionable") {
       echo "checked=checked";
     }?>><em> Questionable</em><strong><em> </em></strong></p>
     <p><strong><em> </em></strong>or</p>
     <p><strong><em>  </em><input type="radio" name="questionable" id="aboveBoard" <?php if ($getstaffprobationreview->integrity =="aboveBoard") {
       echo "checked=checked";
     }?> value="aboveBoard"></strong><em>Above Board</em><em> <strong> </strong></em></p>

     <p><strong>Any Other Observations</strong></p>
     <p>Please share any other observations you would like to make about the probationer. For instance, does s/he have any areas that s/he could improve upon? Is there anything significant about her/him that has not been captured in the above?</p>
     <textarea name="conduct_and_social_maturity" maxlength="500" readonly="readonly" id="conduct_and_social_maturity" cols="40" rows="10" style="width:1000px;"><?php if(!empty($getstaffprobationreview->any_other_observations)) echo $getstaffprobationreview->any_other_observations;?></textarea>

     <div class="text-center">
      <p><strong><br /> </strong></p>

      <p><h5>Part III</h5></p>
      <p><h5>OVERALL REVIEW AND RECOMMENDATIONS</h5></p>
    </div>
    <div class="container-fluid">
      <div class="row text-left">
        1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation indicated above, has been (please tick &uuml;):
      </div>
      <div class="row text-left">
        <div class="col-md-3"> (a)  Satisfactory</div>
        <div class="col-md-9 text-left"><input type="radio" disabled="disabled" name="satisfactory" id="satisfactory" value="satisfactory" <?php if ($getstaffprobationreview->satisfactory =="satisfactory") {
         echo "checked=checked";
       }?>></div>
       <div class="col-md-12">  OR</div>
       <div class="col-md-3"> (b)  Not satisfactory</div>
       <div class="col-md-9"><input type="radio" disabled="disabled" name="satisfactory" id="notsatisfactory" value="notsatisfactory" <?php if ($getstaffprobationreview->satisfactory =="notsatisfactory") {
         echo "checked=checked";
       }?>></div>

     </div>
     <div class="row text-left">
      2. Her/his probation:
    </div>
    <div class="row text-left">
      <div class="col-md-3"> (a)  May be completed</div>
      <div class="col-md-9"> <input type="radio" disabled="disabled" name="completed" id="completed" value="yes" <?php if ($getstaffprobationreview->probation_completed =="yes") {
       echo "checked=checked";
     }?>></div>
     <div class="col-md-12">  OR</div>
     <div class="col-md-12"> (b) May not be completed now as her/his performance needs to be observed further for a period of <input type="text" name="probation_extension_date" id="probation_extension_date"   style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->probation_extension_date); ?>" readonly="readonly"> months.<input type="radio" disabled="disabled" name="completed" id="extended_completed" value="no" <?php if ($getstaffprobationreview->probation_completed =="no") {
       echo "checked=checked";
     }?>></div>
     <div class="col-md-12">  (c)  Not recommended for absorption in PRADAN <input type="radio" disabled="disabled" name="completed" id="completed" value="not_recommended" <?php if ($getstaffprobationreview->probation_completed =="not_recommended") {
       echo "checked=checked";
     }?>></div>
   </div>

   <div class="row" style="line-height: 3">
    <div class="col-md-6 pull-left">
      Place: <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffpersonneldetals->officename)) echo $getstaffpersonneldetals->officename;?>" readonly="readonly">
    </div>

    <div class="col-md-6 text-left">
                            <?php   
                       
                             
                      if(!empty($getcandateverified->encryptedphotoname)) {
                        
                          

                        ?>
                      <img src="<?php echo site_url().'datafiles/signature/'.$getcandateverified->encryptedphotoname;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        
                        ?>

<!--      <img src="<?php //if(!empty($staffsignature->encrypted_signature)) echo site_url().'/datafiles/signature/'.$staffsignature->encrypted_signature;?>" height="50" width="140"> -->
   </div>

   
   <div class="col-md-6 text-right">
    
   </div>

   <div class="col-md-6 pull-right">
    Signature of Reviewing Supervisor
  </div>
  <div class="col-md-6 pull-left">
    Date:<input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
  </div>
  <div class="col-md-6 pull-right">
    Name:  <?php if(!empty($getstaffpersonneldetals->name)) echo $getstaffpersonneldetals->name;?>
  </div>
  <div class="col-md-6 pull-left">

  </div>
  <div class="col-md-6 pull-right">
   Designation: <?php if(!empty($getstaffpersonneldetals->desname)) echo $getstaffpersonneldetals->desname;?>
 </div>
</div>
</div>
<?php } ?>
<input type="hidden" id="ed_agree" value="<?php echo $getstaffprobationreview->agree; ?>">

<?php  if($workflowdetail->flag>=3 && $this->loginData->RoleID==18 || $this->loginData->RoleID==17) { ?> 
 <div class="text-center">
  <p><h5>Part - IV</h5></p>

  <p><h5>Executive Director&rsquo;s Note </h5></p>
  <p>(<em>required only in cases where probation is proposed to be extended OR absorption is <u>not </u>recommended by the Reviewing Supervisor</em>)<em>.</em></p> </div>
  <p></p>
  <p>(i) I agree/do not agree with the above recommendations.</p>
  <br>
  
  <input type="radio" required="required" name="recommendations" id="recommendations" value="yes" <?php if ($getstaffprobationreview->agree =="yes") {
   echo "checked=checked";
 }?>>
 <em> I agree</em><strong><em> </em></strong></p>
 <p id="or"><strong><em> </em></strong>or</p>
 <p><strong><em>  </em><input type="radio" required="$this->loginData->RoleID==17" name="recommendations" value="no" id="recommendations1" <?php if ($getstaffprobationreview->agree =="no") {
   echo "checked=checked";
 }?>></strong><em id="notagree"> do not agree</em><em> <strong> </strong></em></p>
 <p></p>
 <?php if(!empty($getstaffprobationreview->ed_comments)) {?>
 <p id='reson_norecom'>(ii) The reasons for not agreeing to above recommendations are:</p>
 <textarea name="ed_comments" maxlength="500" id="ed_comments" required="required" cols="40" rows="10" style="width:1000px;" required="required"><?php if(!empty($getstaffprobationreview->ed_comments)) echo $getstaffprobationreview->ed_comments; ?></textarea>
 <?php } ?>


 <p></p>
 
 <div class="row" style="line-height: 3">
  <div class="col-md-6 pull-left">
    Date:  <input type="text" name="ed_date" id="ed_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
  </div>
  <!-- <div class="col-md-6 text-left">
    Signature of ED: 
                            <?php   
                       
                      if(!empty($edsignature->encrypted_signature)) {
                          

                        ?>
                      <img src="<?php echo site_url().'/datafiles/signature/'.$edsignature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php
                         }
                        
                          
                        ?>
                       

   
  </div>   -->

  
  <div class="col-md-6 text-right">
    
  </div>
  <div class="col-md-6 pull-right">
   Name of ED :  <input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo  $this->loginData->UserFirstName.' '. $this->loginData->UserMiddleName .' ' .$this->loginData->UserLastName ?>">
 </div>      
</div> 
<?php } ?>


 <br><br>

<?php if ($this->loginData->RoleID==16) { ?>
 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
  <select class="form-control" name="edstatus" id="edstatus" required="required">
    <option value="">select</option>
    <option value="25"> Approved</option>
    <option value="26"> Rejected</option>
  </select>
</div>
<?php } ?>
<!-- <?php //echo $pname->name;?> -->
<?php  if($workflowdetail->receiver == $ed->edid && $workflowdetail->flag==3) {  ?>
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
   Status   <span style="color: red;">*</span>
 </div>
 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
  <select class="form-control" name="edstatus" id="edstatus" required="required">
    <option value="">select</option>
    <option value="5"> Approved</option>
    <option value="6"> Rejected</option>
  </select>
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
  Send To Personnel <span style="color: red;">*</span>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
 <select class="form-control" name="sendtohtd" id="sendtopersonnal" required="required">
  <option value="">select</option>
  
  <option value="<?php echo $pname->staffid;?>"><?php echo $pname->name;?></option>
</select>
<br>
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
 <Comments></Comments> <i id="astristhideshow" style="display: none;">
  <span style="color: red;">*</span> </i>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
 <textarea name="comments" maxlength="500" id="comments" cols="20" rows="3" required="required" style="width:800px;" ></textarea>
</div>
<?php } ?>

<div class="col-lg-1 col-md-1 col-sm-3 col-xs-3"></div>

<br>
<br>




<!-- </div> -->
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>
<br>
<br>
<br>

<br>
<?php //} ?>

<br>
<br>
<br>
<br>
<br>

<?php  if ($this->loginData->RoleID==17) { ?>
  <div class="text-center">
    <p><h5>Part - V</h5></p>
    <p>(<em>for use in the Finance-Personnel-MIS Unit)</em></p>
    <p></p>
    <p>Necessary action has been taken and all concerned have been informed accordingly. The Probationer Register has also been completed as above.</p>
    <p></p> </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
       Name:  <input type="text" name="personnel_name" id="personnel_name"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffpersonneldetals->name;?>">
     </div>
     <div class="col-md-6 pull-right">
       Designation: <input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffpersonneldetals->desname;?>">
     </div>
     <div class="col-md-6 pull-left">
       Signature :________________________
     </div>
     <div class="col-md-6 pull-right">
       Date: <input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
     </div>      
   </div>
 <?php } ?>
</div>
<div class="panel-footer text-right">
  <?php if($workflowdetail->receiver == $ed->edid && $workflowdetail->flag==3) {  ?>
    <input type="submit" name="submit" value="Submit" value="SaveDataSend" class="btn btn-success btn-sm">
  <?php   }  ?>

  <a href="<?php echo site_url().'Ed_staff_approval/'  ?>" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</form>
</div>
</div>
</section>

<script type="text/javascript">
  $( document ).ready(function() {
    $('#edstatus').change(function(){
      var sendtoperal = $('#edstatus').val();
      if (sendtoperal==26) {
       $('#astristhideshow').show();
       $('#comments').attr('required', true);
     }else if(sendtoperal==25) {
      $('#astristhideshow').hide();
      $('#comments').attr('required', false);
    }else if(sendtoperal==8) {
      $('#astristhideshow').show();
      $('#comments').attr('required', true);
    }else if(sendtoperal==7) {
      $('#astristhideshow').hide();
      $('#comments').attr('required', false);
    }
  });
});



 



</script>

<script type="text/javascript">
  $( document ).ready(function() {

  var ed_agree=$("#recommendations").val();

$("#recommendations").click(function() {

   $("#ed_comments").hide();

   $("#ed_comments").prop("required", false);
  $("#reson_norecom").hide();
});

  $("#recommendations1").click(function() {

  $("#ed_comments").show();
   $("#ed_comments").prop("required", true);
  $("#reson_norecom").show();
});

if(ed_agree=="yes")
{
  $("#ed_comments").prop("required", false);
}


});

// });


</script>
