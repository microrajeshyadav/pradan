
<form name="handed_taken" id="handed_taken" action="" method="POST">
<section class="content" style="background-color: #FFFFFF;" >
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-8 panel-title pull-left"> </h4>

       <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
      </div>
    </div>
    <hr class="colorgraph"><br>
  </div>
  <div class="panel-body">
    <div class="row"> 
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
        <p style="text-align: center;"><strong><span style="font-size: 14.0pt; font-variant: small-caps;">Taking Over Charge</span></strong></p>
        <p style="text-align: center;">&nbsp;</p>
        <p style="text-align: justify; tab-stops: -49.5pt;">As per the Letter of Seperation No.
          <input type="text" name="seperationno" id="seperationno"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if($token) echo $token;?>" required="required" readonly>
           dated 
          <input type="text" name="tdate" id="tdate"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($handed_over_charge_date->trans_date); ?>"  required="required" readonly><!-- ,
           transferring me to 
           <input type="text" name="transferto" id="transferto"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->officename;?>" required="required" readonly> (<span style="font-size: 10.0pt;">place</span>) -->,
            I have taken over the charge of my responsibilities on 
             <!-- <select style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" name="change_responsibility_name" id="change_responsibility_name" required="required">
                  <option value="">Select</option>
                  <?php foreach($getstafflist as $value){ ?>
                  <option value="<?php echo $value->staffid ?>"><?php echo $value->name ?></option>
                <?php } ?>
                </select>
             (<span style="font-size: 10.0pt;">name</span>) -->
            <input type="text" name="change_responsibility_date" id="change_responsibility_date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  class="datepicker " value="" required="required" readonly>
             (<span style="font-size: 10.0pt;" >date</span>).
              The list of items 
                <select style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" name="handed" id="handed" required="required">
                  <!-- <option value="">Select</option> -->
                  <option value="2">Taken Over</option>
                </select>
        is as under.</p>
        <p style="text-align: justify; tab-stops: -49.5pt;">&nbsp;</p>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
         <table id="tblForm10" class="table table-bordered table-striped">
          <thead>
           <tr class="bg-light">
            <th colspan="2"> Items

              <div class="col-lg-6 text-right pull-right">
                <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
              </div>
          </th>
          </tr> 
          <tr>
           <th class="text-center" style="vertical-align: top;">Items </th>
           <th class="text-center" style="vertical-align: top;">Description</th>
         </tr> 
       </thead>
       <tbody id="bodytblForm10">

       <?php if($expense->Cexp>0)
       {
        
        foreach($transfer_expeness_details as $val)
        {

       ?>
        <tr id="bodytblForm10">
          <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="items !" minlength="5" maxlength="50"
            id="items"  maxlength="150" name="items[]" placeholder="Enter Items" style="min-width: 20%;"  
            value="<?php echo $val->item;?>"  required></td>
            <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="items description!" 
              id="items_description" name="items_description[]"  placeholder="Enter Description" style="min-width: 20%;" value="<?php echo $val->description;?>" required></td>
            </tr>
            <?php } }
            else{ ?>

            <tr id="bodytblForm10">
          <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="items !" minlength="5" maxlength="50"
            id="items"  maxlength="150" name="items[]" placeholder="Enter Items" style="min-width: 20%;"  
            value="<?php echo set_value('items');?>"  required></td>
            <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="items description!" 
              id="items_description" name="items_description[]"  placeholder="Enter Description" style="min-width: 20%;" value="" required></td>
            </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
     <p>&nbsp;</p>
    <!-- <div id="hidehandedover" style="display: none;">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><strong>Handed Over</strong></div>
       <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Name:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_name" id="handed_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->staff_name;?>" required="required" readonly></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Employee Code:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_employee_code" id="handed_employee_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->emp_code;?>" required="required" readonly></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Designation:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_designation" id="handed_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->staffdesignation;?>" required="required" readonly></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Location:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="handed_designation" id="handed_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->oldofice;?>" required="required" readonly></div>
     </div>
   </div> -->

<br>
  <div id="hidetakenover" >
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><strong>Taken Over</strong></div>
       <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Name:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text"  name="taken_name" id="taken_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $taken_staff_details->currentresponsibilityto;?>" readonly></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Employee Code:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text"  name="taken_employee_code" id="taken_employee_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $taken_staff_details->empcode;?>" readonly></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Designation:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input type="text" name="taken_designation" id="taken_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $taken_staff_details->currentresponsibilitytodesignation;?>" readonly></div>

         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">Location:</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><input name="taken_location" id="taken_location"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $taken_staff_details->officename;?>" readonly></div>

     </div>
   </div>
   
      
      <p><strong>&nbsp;</strong></p>
      <p style="text-align: center;"><strong>Countersigned</strong></p>
      <p style="text-align: center;">&nbsp;</p>
      <p style="text-align: center;"><input type="text" name="countersigned" id="countersigned"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="" required="required"></p>
      <p style="text-align: center;">(Signature of Location In-charge with Date)</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><strong>Note</strong>: In case a Team Coordinator is transferred<strong> t</strong>he new person who has been nominated to take charge will confirm to Finance-Personnel-MIS Unit that procedure for transfer of Team Coordinator as mentioned in appendix 29.1&nbsp; have been completed.</p>
      <p>&nbsp;</p>
      <p><strong>&nbsp;</strong></p>
      <p>cc: - Personal Dossier</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Finance-Personnel-MIS Unit</p>
    </div>
  </div>
</div>

<div class="panel-footer text-right">
  <!-- <button  type="submit" name="savetbtn" id="savetbtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button> -->
                 
  <button  type="button" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit" onclick="handedovertaken();">Submit</button>
</div>
</div>
</div>   
</section>
<!-- Modal -->
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="hidemodal();">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments"  required> </textarea>
              <?php echo form_error("comments");?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Status <span style="color: red;" >*</span></label>

                        <select name="status" id="status" class="form-control" required="required"> 
                          <option value="">Select</option>
                        <?php if($this->loginData->RoleID == 2 || $this->loginData->RoleID==21){ ?>
                          <option value="23">Approved</option>
                          <!-- <option value="14">Rejected</option> -->
                        <?php } ?>
                        </select>
                        <?php echo form_error("status");?>
                      </div>
            <div id="executivedirector_admin" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none;" >
                      <label for="Name">Personal Administrator<span style="color: red;" >*</span></label>

                        <select name="executivedirector_administration" id="executivedirector_administration" class="form-control" >
                          <option value="">Select</option>
                          <?php foreach ($persondetail as $key => $value) {
                           ?>
                          <option value="<?php echo $value->personnelstaffid;?>">
                            <?php echo $value->name.' - '.'('.$value->emp_code.')';?>
                              
                            </option>
                          <?php } ?>
                          </select>
                        <?php echo form_error("personnel_administration");?>
                      </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hidemodal();">Close</button>
          <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="handedovertakenmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
<!-- Modal -->

</form>

<script>
  $('#status').change(function(){
  var statusval = $('#status').val();
    /*alert('sdfsaf');
    alert(statusval);*/

        if(statusval == 23) {
            $('#executivedirector_admin').show(); 
            $('#executivedirector_administration').attr('disabled',false)
            $('#personnel_administration').prop('required', true); 

        } else {
            $('#executivedirector_admin').hide(); 
              $('#executivedirector_administration').attr('disabled',true)
            $('#executivedirector_administration').prop('required', false); 

        } 
    });
  function handedovertaken(){
    var change_responsibility_date = document.getElementById('change_responsibility_date').value; 
    var handed = document.getElementById('handed').value;  
    var items = document.getElementById('items').value;   
    var items_description = document.getElementById('items_description').value;   
    var countersigned = document.getElementById('countersigned').value; 
    // alert(name_of_location);

    if(countersigned == ''){
      $('#countersigned').focus();
    }

    if(items_description == ''){
      $('#items_description').focus();
    }

    if(items == ''){
      $('#items').focus();
    }
    
    if(handed == ''){
      $('#handed').focus();
    }

    if(change_responsibility_date == ''){
      $('#change_responsibility_date').focus();
    }
    
    if(change_responsibility_date != '' && handed !='' && items !='' && items_description !='' && countersigned !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
  function handedovertakenmodal(){
    var comments = document.getElementById('comments').value;
    var acceept = document.getElementById('status').value;
    var executivedirector_administration = document.getElementById('executivedirector_administration').value;
    if(acceept == ''){
      $('#status').focus();
    }else if(acceept == 23 && executivedirector_administration ==''){
      $('#executivedirector_administration').focus();
    }
    if(comments.trim() == ''){
      $('#comments').focus();
    }


    if(comments.trim() !='' && acceept != '' && executivedirector_administration !=''){
      document.forms['handed_taken'].submit();
    }
  }
   $('#handed').change(function(){
       var handedid = $('#handed').val();
       //alert(handedid);

       if (handedid ==1) {
          $('#hidehandedover').show();
          $('#hidetakenover').hide();
           $('#taken_name').attr('disabled', true);
          $('#taken_employee_code').attr('disabled', true);
          $('#taken_designation').attr('disabled', true);
          $('#taken_location').attr('disabled', true);
       }else{
         $('#hidetakenover').show();
         $('#hidehandedover').hide();
        $('#handed_name').attr('disabled', true);
        $('#handed_employee_code').attr('disabled', true);
        $('#handed_designation').attr('disabled', true);
        $('#handed_designation').attr('disabled', true);
       }

    });

 
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
   
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });

  var srNoGlobal=0;
  var inctg = 0;

  function Insaettrainingexposure(count) {
    srNoGlobal = $('#bodytblForm10 tr').length+1;
    var tbody = $('#bodytblForm10');
    var lastRow = $('#bodytblForm10 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inctg++
      cloneRow = lastRow.clone();
      var tableData1 = '<tr>'
      + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="items" name="items['+inctg+']" placeholder="Enter Items" style="min-width: 20%;" value="" data-original-title="Items!">'
      + '</td>'
      + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="items_description['+inctg+']" name="items_description['+inctg+']" placeholder="Items Description" style="min-width: 20%;" value="" required="required" data-original-title="Items Description !"></td>'
      + '</td>' + '</tr>';
      $("#bodytblForm10").append(tableData1)
    
    }

  }
  //insertRows();

  $(document).ready(function () {
 $("#change_responsibility_date").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
  });

  });
  

</script>
