<section class="content" style="background-color: #FFFFFF;" >
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-8 panel-title pull-left"> </h4>

       <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
      </div>
   </div>
    <hr class="colorgraph"><br>
  </div>
  <form name="handed_taken" id="handed_taken" action="" method="POST">
  <div class="panel-body">
    <div class="row"> 
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 

        <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
        <p style="text-align: center;"><strong><span style="font-size: 14.0pt; font-variant: small-caps;">Handing Over/Taking Over Charge</span></strong></p>
        <p style="text-align: center;">&nbsp;</p>
        <p style="text-align: justify; tab-stops: -49.5pt;">As per the Letter of Transfer No.<input type="text" name="transferno" id="transferno"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($handed_expeness->transfernno))echo $handed_expeness->transfernno;?>" required="required"> dated <input type="text" name="tdate" id="tdate"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo date('d/m/Y'); ?>"  required="required">, transferring me to <input type="text" name="transferto" id="transferto"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php //echo $staff_details->newoffice;?>" required="required"> (<span style="font-size: 10.0pt;">place</span>), I have handed over the charge of my responsibilities to <input type="text" name="change_responsibility_name" id="change_responsibility_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  required="required" value="<?php echo $staff_details->currentresponsibilityto;?>"> (<span style="font-size: 10.0pt;">name</span>) <input type="text" name="change_responsibility_date" id="change_responsibility_date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  class="form-control datepicker " value="" required="required"> (<span style="font-size: 10.0pt;">date</span>). The list of items  <select style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" name="handed" id="handed" required="required">
              <option value="">Select</option>
             <option value="1">Handed Over</option>
              <option value="2">Taken Over</option>
            </select> is as under.</p>
        <p style="text-align: justify; tab-stops: -49.5pt;">&nbsp;</p>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

         <table id="tblForm10" class="table table-bordered table-striped">
          <thead>
           <tr class="bg-light">
            <th colspan="2"> Items

              <div class="col-lg-6 text-right pull-right">
                <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
              </div>
          </th>
          </tr> 
          <tr>
           <th class="text-center" style="vertical-align: top;">Items </th>
           <th class="text-center" style="vertical-align: top;">Description</th>
         </tr> 
       </thead>
       <tbody id="bodytblForm10">
       <input type="hidden" name="handedcount" id="handedcount" value="<?php echo $expense->Cexp;?>">
       <?php if($expense->Cexp==0)
       {
              
       ?>
        <tr id="bodytblForm10">
          <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="items !" minlength="5" maxlength="50"
            id="items"  maxlength="150" name="items[]" placeholder="Enter Items" style="min-width: 20%;"  
            value=""  ></td>
            <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="items description!" 
              id="items_description" name="items_description[]"  placeholder="Enter Description" style="min-width: 20%;" value="" ></td>
            </tr>
            <?php 
          }  

          else
          { 
              $i=0;

            

            foreach($transfer_expeness_details as $val)
              {
               //print_r($val);
                
            
         
            ?>
            <input type="hidden" name="handed_id[]" value="<?php echo $val->id;?>">
               <tr id="bodytblForm10">
          <td><input type="text" class="form-control alphabateonly" maxlength="150" data-toggle="tooltip" title="items !" minlength="5" maxlength="50"
            id="items"  maxlength="150" name="items[<?php echo $i;?>]" placeholder="Enter Items" style="min-width: 20%;"  
            value="<?php echo $val->item;?>"  ></td>
            <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title="items description!" 
              id="items_description" name="items_description[<?php echo $i;?>]"  placeholder="Enter Description" style="min-width: 20%;" value="<?php echo $val->description;?>" ></td>
            </tr>

             
            <?php 
            $i++;
          }
          
          }
          ?>

          
          </tbody>
        </table>
      </div>



      <p>&nbsp;</p>
      <table style="width: 968px; height: 329px; margin-left: auto; margin-right: auto;" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr style="height: 48px;">
            <td style="width: 332.5px; height: 48px;">
              <p><strong>Handed Over</strong></p>
            </td>
            <td style="width: 329.5px; height: 48px;">
              <p>&nbsp;</p>
            </td>
            <td style="width: 299px; height: 48px;">
              <p><strong>Taken Over</strong></p>
            </td>
          </tr>
         <!-- <tr style="height: 48px;">
            <td style="width: 332.5px; height: 48px;">
              <p>Signature:<input type="text" name="handed_signature" id="handed_signature"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="" required="required"></p>
            </td>
            <td style="width: 329.5px; height: 48px;">
              <p>&nbsp;</p>
            </td>
            <td style="width: 299px; height: 48px;">
              <p>Signature:<input type="text" name="taken_signature" id="taken_signature"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="" required="required"></p>
            </td>
          </tr>-->
          <tr style="height: 48px;">
            <td style="width: 332.5px; height: 48px;">
              <p>Name&nbsp;&nbsp; &nbsp;&nbsp; :<input type="text" name="handed_name" id="handed_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->staff_name;?>" required="required"></p>
            </td>
            <td style="width: 329.5px; height: 48px;">
              <p>&nbsp;</p>
            </td>
            <td style="width: 299px; height: 48px;">
              <p>Name&nbsp;&nbsp; &nbsp;&nbsp; :<input type="text"  name="taken_name" id="taken_name"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $staff_details->currentresponsibilityto;?>">
                              </p>
            </td>
          </tr>
          <tr style="height: 48px;">
            <td style="width: 332.5px; height: 48px;">
              <p>Employee Code: <input type="text" name="handed_employee_code" id="handed_employee_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->emp_code;?>" required="required"></p>
            </td>
            <td style="width: 329.5px; height: 48px;">
              <p>&nbsp;</p>
            </td>
            <td style="width: 299px; height: 48px;">
              <p>Employee Code: <input type="text"  name="taken_employee_code" id="taken_employee_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php echo $staff_details->empcode;?>">
             
              </p>
            </td>
          </tr>
          <tr style="height: 48px;">
            <td style="width: 332.5px; height: 48px;">
              <p>Designation:<input type="text" name="handed_designation" id="handed_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->desname;?>" required="required"></p>
            </td>
            <td style="width: 329.5px; height: 48px;">
              <p>&nbsp;</p>
            </td>
            <td style="width: 299px; height: 48px;">
              <p>Designation:<input type="text" name="taken_designation" id="taken_designation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $staff_details->currentresponsibilitytodesignation;?>" >
             </p>
            </td>
          </tr>
          <tr style="height: 49px;">
            <td style="width: 332.5px; height: 49px;">
              <p>Location: <input type="text" name="handed_location" id="handed_location"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($staff_details->currentroffice))echo $staff_details->currentroffice;?>" required="required"></p>
            </td>
            <td style="width: 329.5px; height: 49px;">
              <p>&nbsp;</p>
            </td>
            <td style="width: 299px; height: 49px;">
              <p>Location: <input name="taken_location" id="taken_location"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   required="required" value="<?php if(!empty($staff_details->newoffice))echo $staff_details->newoffice;?>">
              
              
              </p>
            </td>
          </tr>
        </tbody>
      </table>
      <p><strong>&nbsp;</strong></p>
      <p style="text-align: center;"><strong>Countersigned</strong></p>
      <p style="text-align: center;">&nbsp;</p>
      <p style="text-align: center;"><input type="text" name="countersigned" id="countersigned"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="" required="required"></p>
      <p style="text-align: center;">(Signature of Location In-charge with Date)</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><strong>Note</strong>:In case a Team Coordinator is transferred<strong> t</strong>he new person who has been nominated to take charge will confirm to Finance-Personnel-MIS Unit that procedure for transfer of Team Coordinator as mentioned in appendix 29.1&nbsp; have been completed.</p>
      <p>&nbsp;</p>
      <p><strong>&nbsp;</strong></p>
      <p>cc: - Personal Dossier</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Finance-Personnel-MIS Unit</p>

    </div>
  </div>
</div>

 <div class="panel-footer text-right">
  <button  type="submit" name="savetbtn" id="savetbtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button>
                 
         <button  type="submit" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>
</div>
</form>
</div>
</div>   
</section>

<script>
  function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {

      return true;
    } else {
      return false;
    }
  }


  $(document).ready(function(){

   $("#change_responsibility_date").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });


  // $('[data-toggle="tooltip"]').tooltip();  
   //$('#Inboxtable').DataTable(); 
 });


  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
   
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });

  var srNoGlobal=0;
  var inctg = 0;

  function Insaettrainingexposure(count) {
    srNoGlobal = $('#bodytblForm10 tr').length+1;
    var tbody = $('#bodytblForm10');
    var lastRow = $('#bodytblForm10 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inctg++
      cloneRow = lastRow.clone();
      var tableData1 = '<tr>'
      + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="items" name="items['+inctg+']" placeholder="Enter Items" style="min-width: 20%;" value="" data-original-title="Items!">'
      + '</td>'
      + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="items_description['+inctg+']" name="items_description['+inctg+']" placeholder="Items Description" style="min-width: 20%;" value="" required="required" data-original-title="Items Description !"></td>'
      + '</td>' + '</tr>';
      $("#bodytblForm10").append(tableData1)
    
    }

  }
  //insertRows();

  $(document).ready(function () {
    $('#checkBtn').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        alert("You must check at least one checkbox.");
        return false;
      }else{

        if (!confirm('Are you sure?')) {
          return false;
        }

      }


    });
  });


  $("#chooseletter").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getYPRResponse/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#letter").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


  
    $(document).ready(function(){

        $("#taken_name").change(function(){
          //alert("hello");

          var staff_id = $(this).val();
         // var new_designation=
         // alert("staff="+staff_id);
          var data;
          data=staff_id;
          
           $.ajax({
          data:{staff_id:staff_id},
          url: '<?php echo site_url(); ?>Ajax/getstaff/',
          type: 'POST'
      
             })
      .done(function(data) {
       //alert(data);

        var obj=JSON.parse(data);
        //console.log(obj.office_name);
        //alert("office name="+obj.office_name);
        //console.log("office_name="+obj.office_name+"designation="+obj.designation+"office_id="+obj.office_id+"designation_id="+obj.designation_id);
        var dd=obj.office_name;
        //alert("name="+dd);
       
        var option='';
            option += `<option value="${obj.office_id}">${obj.office_name}</option>`;
            var option1='';
            option1 += `<option value="${obj.designation_id}">${obj.designation}</option>`;
            var option2='';
            option2 += `<option value="${obj.employ_code}">${obj.employ_code}</option>`;


       

$('#taken_location').html(option);
$('#taken_designation').html(option1);
$('#taken_employee_code').html(option2);

        
      })

        });
      });

</script>
