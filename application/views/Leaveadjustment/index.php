<style type="text/css">
  tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

</style>

<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaveadjustment" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">   
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">Staff Leave Adjustment </h4>

         
      </div>
      <hr class="colorgraph"><br>
    </div>

    
    <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tablestaff" class="display">
                  <thead>
                   <tr>
                     
                     <th class="text-left">Code</th>
                     <th class="text-left">Staff</th>
                     <th class="text-left">Designation</th>
                     <th class="text-left">Office</th>
                   </tr> 
                 </thead>
                 <tbody>
                  <?php 
                  $i=0;
                  foreach ($stafflist as $key => $value) {
                   ?>
                   <tr>
                    
                    <td class="text-left"><?php echo $value->emp_code;?></td>
                    <td class="text-left">
                      <a href="<?php echo base_url().'Leaveadjustment/adjustment/'.$value->staffid;?>">
                      <?php echo $value->name;?> </a>
                        
                      </td>
                    <td class="text-left"><?php echo $value->desname;?></td>
                     <td class="text-left"><?php echo $value->office;?></td>
                    
                   </tr>
                   <?php $i++; } ?>
                 </tbody>

               </table>
             </div>
           </div> 
         </div>
       </div>
     </div>   
     <?php } } ?>
   </section>

   <script>
    $(document).ready(function(){
     
      $('[data-toggle="tooltip"]').tooltip();  
       
    });
      
      $(document).ready(function() {
    var groupColumn = 2;
    var table = $('#tablestaff').DataTable({
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="3">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );
} );
  </script>  


   <!--  <script type="text/javascript">
     $(document).ready(function(){
      $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
    });
  </script> -->
  <script type="text/javascript">
    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>