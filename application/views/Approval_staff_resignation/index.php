<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave" && $row->Action == "index"){ ?>

  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


<?php  //print_r($state_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

             <div class="header">
              <h2>Manage Approval for Staff Resignation by Supervisor</h2><br>
              
            </div>
            <div class="body">
             
                <table id="tblapprovalstaff" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.No.</th>
                      <th class="text-center">Staff Name </th>
                      <th class="text-center">Designation</th>
                      <th class="text-center">Affected Date</th>
                      <th class="text-center">Notice Period</th>
                      <th class="text-center">Reason</th>
                       <th class="text-center">Status</th>
                       <th class="text-center">Remarks</th>
                    </tr>
                    </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                </table>
              
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>
<script>
   $(document).ready(function() {
            $('#tblapprovalstaff').DataTable({
              "paging": false,
              "search": false,
            });
          });
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>