  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');
  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <section class="content" style="background-color: #FFFFFF; overflow-x:auto; " >
         <br>
         <div class="container-fluid">
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-10 panel-title pull-left"> Transfer Approval List</h4>
             </div>
             <hr class="colorgraph"><br>
           </div>
           <div class="panel-body">

            <div class="row" style="background-color: #FFFFFF; overflow-x:scroll;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tablecampus" class="table table-bordered table-striped dt-responsive wrapper">
                  <thead>
                   <tr>
                     <th style ="max-width:50px;" class="text-center">S. No.</th>
                     <th>Emp code</th>
                     <th>Name</th>
                     <th>Old Office</th>
                     <th>Proposed Transfer Office</th>
                     <th class="text-center">Proposed Transfer Date</th>
                     <th>Reason</th>
                     <th>Comments</th>
                     <th>Sender Name</th>
                     <th class="text-center">Request Status</th>
                     <th class="text-center">Request Date</th>
                     <!--  <th>Status</th> -->
                     <th style ="max-width:50px;" class="text-center">Action</th>
                   </tr> 
                 </thead>
                 <?php if (count($getworkflowdetail) ==0) { ?>
                 <tbody>

                 </tbody>
                 <?php  }else{  ?>
                 <tbody>
                  <?php 
                  $i=0;

                  foreach ($getworkflowdetail as $key => $value) {
                    $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->emp_code;?></td>
                      <td><?php echo $value->name;?></td>
                      <td><?php echo $value->officename;?></td>
                      <td><?php echo $value->newoffice;?> </td>
                      <td class="text-center"><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->reason;?></td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td class="text-center"><?php echo $value->flag; ?></td>
                      <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate); ?></td>
                      <!--         <td><?php //echo $value->process_type ;?></td> -->
                      <td class="text-center">
                        <a class="btn btn-success btn-sm" title = "History." href="<?php echo site_url()."TStaff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                        <?php if($value->status== 3 || $value->status== 5) { ?>
                        <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Stafftransfer/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>
                        <?php } ?>
                        <?php if ($value->status==7) { ?>
                        
                        <a title = "Click here to approve this request submitted." class="btn btn-primary btn-sm" onclick="approvemodalshowtransfer(<?php echo $value->transid; ?>);"><i class="fas fa-thumbs-up"></i></a>
                        <?php } ?>
                        <?php if( $value->status==7 && !empty($value->tbl_clearance_certificate_id)){ ?>
                        <a class="btn btn-warning btn-sm" title = "Clearance Certificate" href="<?php echo site_url()."Stafftransfer/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> <i class="fa fa-certificate" aria-hidden="true"></i>
</a>
                        <?php } ?>

                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                  <?php } ?>
                </table>
              </div>
            </div>
          </div> 
        </div>
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left"> Change Responsibility Approval List</h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">

          <div class="row" style="background-color: #FFFFFF; overflow-x:auto;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus1" class="table table-bordered table-striped dt-responsive wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <!-- <th>Proposed Transfer Office</th> -->
                   <th class="text-center">Proposed Transfer Date</th>
                   <th>Old Designation</th>
                   <th>New Designation</th>
                   <th>Reason</th>
                   <th>Comments</th>
                   <th>Sender Name</th>
                   <th class="text-center">Request Status</th>
                   <th class="text-center">Request Date</th>
                   <!-- <th>Status</th> -->
                   <th style ="max-width:50px;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($getpromotionworkflowdetail) ==0) { ?>
               <tbody>
                 
               </tbody>
               <?php  }else{  ?>


               <tbody>
                <?php 
                $i=0;
                foreach ($getpromotionworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <!-- <td><?php echo $value->newoffice;?> </td> -->
                    <td class="text-center"><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->olddesid;?></td>
                    <td><?php echo $value->newdesid;?></td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername;?></td>
                    <td class="text-center"><?php echo $value->flag; ?></td> 
                    <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                    <!-- <td><?php echo $value->process_type ;?></td> -->
                    <td class="text-center">
                      <a class="btn btn-success btn-sm" title = "History." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                      <?php if ($value->status==5) { ?>
                      <a class="btn btn-primary btn-sm" title = "Inter-Office Memo" href="<?php echo site_url()."Change_responsibility_encourage/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>
                      <?php } ?>
                      <?php if ($value->status==3 || $value->status==6) { ?>
                      <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Change_responsibility_encourage/index/".$value->transid;?>"><i class="fas fa-retweet"></i>
</a>

                      <?php } ?>

                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
          </div>
        </div> 
      </div>



      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left"> Transfer & Change Responsibility Approval List</h4>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">

        <div class="row" style="background-color: #FFFFFF; overflow-x:auto;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
            <table id="tablecampus2" class="table table-bordered table-striped dt-responsive wrapper">
              <thead>
               <tr>
                 <th style ="max-width:50px;" class="text-center">S. No.</th>
                 <th>Emp code</th>
                 <th>Name</th>
                 <th>Old Office</th>
                 <th>Proposed Transfer Office</th>
                 <th class="text-center">Proposed Transfer Date</th>
                 <th>Old Designation</th>
                 <th>New Designation</th>
                 <th>Reason</th>
                 <th>Comments</th>
                 <th>Sender Name</th>
                 <th class="text-center">Request Status</th>
                 <!-- <th>Request Date</th> -->
                 <!--  <th>Status</th> -->
                 <th style ="max-width:50px;" class="text-center">Action</th>
               </tr> 
             </thead>
             <?php if (count($gettransferworkflowdetail) ==0) { ?>
             <tbody>
             
             </tbody>
             <?php  }else{  ?>


             <tbody>
              <?php 
              $i=0;
              foreach ($gettransferworkflowdetail as $key => $value) {
                $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td><?php echo $value->emp_code;?></td>
                  <td><?php echo $value->name;?></td>
                  <td><?php echo $value->officename;?></td>
                  <td><?php echo $value->newoffice;?> </td>
                  <td class="text-center"><?php echo $proposed_date_bdformat;?> </td>
                  <td><?php echo $value->olddesid;?></td>
                  <td><?php echo $value->newdesid;?></td>
                  <td><?php echo $value->reason;?></td>
                  <td><?php echo $value->scomments;?></td>
                  <td><?php echo $value->sendername;?></td>
                  <td class="text-center"><?php echo $value->flag; ?></td>
                      <!-- <td><?php echo $value->Requestdate;?></td>
                        <td><?php echo $value->process_type ;?></td> -->
                        <td class="text-center">
                          <a class="btn btn-success btn-sm" title = "History." href="<?php echo site_url()."TCRStaff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                          <?php if ($value->status==3 || $value->status==5) { ?>
                          <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a><br>
                          <a class="btn btn-warning btn-sm" title = "Click here to change responsibility on submitted." href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"> <i class="fas fa-retweet"></i>
 </a>
                          <?php } ?>
                          <?php if ($value->status==7) { ?>

                          <a title = "Click here to approve this request submitted." class="btn btn-primary btn-sm" onclick="approvemodalshow(<?php echo $value->transid; ?>);"><i class="fas fa-thumbs-up"></i></a>
                          <?php } ?>
                          <?php if ($value->status==7) { ?>
                          <a class="btn btn-warning btn-sm" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> <i class="fa fa-certificate" aria-hidden="true"></i></a>
                          <?php } ?>
                          <?php if ($value->status==4 || $value->status==6) { ?>
                          <!--href="<?php echo site_url()."Stafftransferpromotion/approve/".$value->staffid.'/'.$value->transid;?>"data-toggle="modal" data-target="#myModal" <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>">Approval</a> -->
                          <?php } ?>

                        </td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                    <?php } ?>
                  </table>
                </div>
              </div>
            </div> 
          </div>


          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
                <h4 class="col-md-10 panel-title pull-left"> Separation Approval List</h4>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">
             <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white; overflow-x:auto;">
                <table id="tablecampus3" class="table table-bordered table-striped dt-responsive wrapper">
                  <thead>
                    <tr>
                      <th style ="max-width:50px;" class="text-center">S. No.</th>
                      <th>Emp code</th>
                      <th>Name</th>
                      <th>Old Office</th>
                      <th>Proposed Transfer Office</th>
                       <th>Document</th>
                      <th class="text-center">Proposed Transfer Date</th>
                      <th>Reason</th>
                      <th>Comments</th>
                      <th>Sender Name</th>
                      <th>Request Status</th>
                      <th class="text-center">Request Date</th>
                      <th class="text-center">Status</th>
                      <th style ="max-width:50px;" class="text-center">Action</th>
                    </tr> 
                 </thead>
                 <tbody>
                  <?php 
                  $i=0;
                  if($getseprationworkflowdetail){
                   
                    foreach ($getseprationworkflowdetail as $key => $value) {
                    // echo "<pre>";
                    //  print_r($value);
                    //die;
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                      ?>
                      <tr>
                        <td class="text-center"><?php echo $i+1;?></td>
                        <td><?php echo $value->emp_code;?></td>
                        <td><?php echo $value->name;?></td>
                        <td><?php echo $value->officename;?></td>
                        <td><?php echo $value->newoffice;?> </td>
                         <td><?php 
                          if(!empty($value->relieved_document))
                          {
                              $matriccertificateArray =explode("|",$value->relieved_document);
      $image_path='';

      foreach($matriccertificateArray as $res1){

        $image_path='pdf_separationletters/'.$res1;
                               
                              if (file_exists($image_path)) {
                                ?>
                               <a href="<?php echo site_url().'pdf_separationletters/'.$res1;?>" target=_blank><i class="fa fa-download" ></i></a>  
                              
                            <?php }
                            else
                            {

      ?> 
      <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download  ><i class="fa fa-download" aria-hidden="true"></i> </a>

   
     <?php } } }
                        //  }

                         ?> </td>
                        <td class="text-center"><?php echo $proposed_date_bdformat;?> </td>
                        <td><?php echo $value->reason;?></td>
                        <td><?php echo $value->scomments;?></td>
                        <td><?php echo $value->sendername;?></td>
                        <td class="text-center"><?php echo $value->flag; ?></td>
                        <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                        <td><?php echo $value->process_type ;?></td>
                        <td class="text-center">
                          <a class="btn btn-success btn-sm" title = "History." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>


                          <?php 

                             if($value->status == 9)
                             {
                              ?>
                              <a href="<?php echo base_url('Staff_sepdischargenotice/index/'.$value->transid);?>" data-toggle="tooltip" title = " DISCHARGE NOTICE" class="btn btn-success btn-sm"><i class="fas fa-eject"></i> </a>

                              <a class="btn btn-warning btn-sm" title = "Click here to 
                            inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepcacceptanceofresignation/index/".$value->transid;?>">






                           <?php
                           if($value->trans_status=='Resign') 

                            { echo "ACCEPTANCE OF RESIGNATION";}
                          else if($value->trans_status=='Termination') 

                            { echo strtoupper("Termination Letter");
                        }

                        else if($value->trans_status=='Termination during Probation') 

                          { echo strtoupper("Termination Letter during Probation");
                      }
                      else if($value->trans_status=='Retirement') 

                        { echo strtoupper("Discharge Letter");
                    }
                    else if($value->trans_status=='Death') 

                      { echo strtoupper("Death Letter");
                  }
                  else if($value->trans_status=='Desertion cases') 

                    { echo strtoupper("Letter for Desertion cases");
                }
                else if($value->trans_status=='Super Annuation') 

                    { echo strtoupper("Letter for Super Annuation");
                }
                else if($value->trans_status=='Discharge simpliciter/ Dismiss') 

                  { echo strtoupper("Letter Discharge simpliciter/ Dismissal");
              }
              ?>
            </a>
                 <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepiomformalities/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>


                              <?php 
                             }

                          if (($value->status == 5 ) &&($value->trans_status =='Termination' || $value->trans_status =='Super Annuation' || $value->trans_status =='Resign' || $value->trans_status =='Termination during Probation' || $value->trans_status =='Death' || $value->trans_status =='Retirement' || $value->trans_status =='Discharge simpliciter/ Dismiss'  || $value->trans_status =='Desertion cases')) {


                           ?> 

                            <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                           
            
            <?php } ?>
            <?php if (($value->status == 7 || $value->status == 5) && $value->trans_status =='Retirement') {

             ?> 
            <!-- <a class="btn btn-warning btn-sm" title = "Click here to 
              sample inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepinformtionforsup/index/".$value->transid;?>">INFORMATION FOR SUPERANNUATION</a> -->
              <?php } ?>

             
              <?php if ($value->status==7 || $value->status==11) { ?>
                




              <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepiomformalities/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

              <?php } 
              else if($value->status==17||$value->status==19 || $value->status==25 ||$value->status==27|| $value->status==28){ ?>
              <a class="btn btn-warning btn-sm" title = "Click here to CHECK SHEET ." href="<?php echo site_url()."Staff_sepchecksheet/index/".$value->transid;?>"><i class="fas fa-clipboard-check"></i></a>
              <?php if(!empty($value->clearnes_id)) { ?>
                <a href="<?php echo base_url('Staff_sepclearancecertificate/view/'.$value->clearnes_id);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fa fa-certificate" aria-hidden="true"></i></a>
              <?php }
                if($value->status==17){ ?>
              <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
            <?php }  

            if($value->status==25 && $value->trans_status!='Death'){ 
            ?>
            <a href="<?php echo base_url('Handed_over_taken_over_seperation/viewtaken/'.$value->transid.'/'.$value->tbl_handed_id);?>" data-toggle="tooltip" title = "Staff Handed over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-right"></i></a>
            <a href="<?php echo base_url('Staff_sepemployeeexitform/index/'.$value->transid);?>" data-toggle="tooltip" title = "Staff Exitfoam" class="btn btn-success btn-sm"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>


              <?php }} 
               if($value->status==27 && $value->trans_status!='Death'){ 
            ?>
             <a href="<?php echo base_url('Handed_over_taken_over_seperation/viewtaken/'.$value->transid.'/'.$value->tbl_handed_id);?>" data-toggle="tooltip" title = "Staff Handed over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-right"></i></a>   
            <a href="<?php echo base_url('Staff_sepemployeeexitform/index/'.$value->transid);?>" data-toggle="tooltip" title = "Staff Exitfoam" class="btn btn-success btn-sm"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>
             <a class="btn btn-warning btn-sm" title = "Click here to CHECK SHEET ." href="<?php echo site_url()."Staff_sepchecksheet/index/".$value->transid;?>"><i class="fas fa-clipboard-check"></i></a>
           <?php } 

           else if($value->trans_status=='Death')
                      {
                    ?>
                     <a class="btn btn-warning btn-sm" title = "Click here for Condolence Letter." href="<?php echo site_url()."Staff_sepcondolence/index/".$value->transid;?>"><i class="fas fa-envelope-open-text"></i></i>
</a>
                  <?php }
                  else if($value->status==29)
                      {
                        ?>
                        
                        <a class="btn btn-primary btn-xs m-t-10 " title="document Upload" onclick="document_relivingletter(<?php echo $value->transid; ?>);">Send Releaving Letter</a>

                        <div class="modal fade" id="myModalAppointment" role="dialog">
  <div class="modal-dialog">
    <form method="post" action="<?php echo site_url().'Staff_personnel_approval/document_upload/'.$value->transid; ?>" enctype="multipart/form-data">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
           <!--  <button type="button" class="close" data-dismiss="modal">&times;</button>-->
            <h4 class="modal-title">Select Documents.</h4>
          </div>
          <div class="modal-body" align="pull-left">
           <!--  <p>Select Documents.</p> -->
           
            <input type="file" name="othersreleving_document[]" id="othersreleving_document"  accept=".pdf" multiple>
          </div>
          <div class="modal-footer">
            <button type="Submit" class="btn btn-primary">Submit</button>
           
          
          </div>
        </div>
      </form>
    </div>
  </div>

                        <?php 
                      

                      }


                 
  if($value->status>=27 && $value->level <= 3 ){ 
 
                  ?>
        
                     <a href="<?php echo base_url('Staff_sepservicecertificate/index/'.$value->transid);?>" data-toggle="tooltip" title = "SERVICE CERTIFICATE" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i>
                  <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepwhomitmayconcernlevelthreetermination/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

                  <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>"><i class="fas fa-envelope-open-text"></i></a>
                  <?php }  else if($value->level == 4 && $value->status>=27) {?>
                      <a href="<?php echo base_url('Staff_sepservicecertificate/index/'.$value->transid);?>" data-toggle="tooltip" title = "SERVICE CERTIFICATE" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i>
                    <a class="btn btn-warning btn-sm" title = "Click here to 
                     inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepwhomitmayconcernlevelfourtermination/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

                    <a class="btn btn-warning btn-sm" title = "Click here to  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>"><i class="fas fa-envelope-open-text"></i></a>
                    <?php } 

              else if(($value->level == 5 || $value->level == 6 )&& ($value->status>=27) ){?>
                      <a href="<?php echo base_url('Staff_sepservicecertificate/index/'.$value->transid);?>" data-toggle="tooltip" title = "SERVICE CERTIFICATE" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i>
                    <a class="btn btn-warning btn-sm" title = "Click here for  inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepwhomitmayconcernlevelsixtermination/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

                    <a class="btn btn-warning btn-sm" title = "Click here for Releving Letter." href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>

                    <?php } 


                    ?>
             
 

                </td>
              </tr>
              <?php $i++; } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div> 
  </div>
</div>

</section>
<!-- Modal for transfer and promotion-->



<!-- Modal -->

<!-- Modal for transfer -->

  <div class="modal fade" id="myModaltransfer" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="<?php echo site_url(); ?>Staff_personnel_approval/approvemodaltransfer" id="acceptancepersonalformtransfer" name="acceptancepersonalformtransfer" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Approval</h4>
            <button type="button" class="close" onclick="hidemodal();">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <input type="hidden" id="receiverstaffidtransfer" name="receiverstaffidtransfer" value="">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="commentstransfer" name="commentstransfer" placeholder="Enter Notes" class="form-control col-md-12"> </textarea>
             <?php echo form_error("commentstransfer");?>
           </div>
         </div>
         <!-- </div> -->
         <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" onclick="hidemodal();">Close</button>
          <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancepersonalmodaltransfer();">
        </div>
      </div>
    </form> 
  </div>
</div>

<!-- Modal -->

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $('#tablecampus1').DataTable();
    $('#tablecampus2').DataTable();
    $('#tablecampus3').DataTable();
  });
  function approvemodalshow(staffid){
    document.getElementById("receiverstaffid").value = staffid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
  function acceptancepersonalmodal(){
    var acceept = document.getElementById('status').value;

  if(acceept == ''){
    $('#status').focus();
  }else{
    document.forms['acceptancepersonalform'].submit();
  }
  }
  function approvemodalshowtransfer(staffid){
    document.getElementById("receiverstaffidtransfer").value = staffid;
    $('#myModaltransfer').removeClass('fade');
    $("#myModaltransfer").show();
  }
  function acceptancepersonalmodaltransfer(){
    var comments = document.getElementById('commentstransfer').value;
    if(comments.trim() == ''){
      $('#commentstransfer').focus();
    }
    if(comments.trim() !=''){
      document.forms['acceptancepersonalformtransfer'].submit();
    }
  }
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">

   function document_relivingletter(transid){
    
      $('#myModalAppointment').removeClass('fade');
      $("#myModalAppointment").show();
    }
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>