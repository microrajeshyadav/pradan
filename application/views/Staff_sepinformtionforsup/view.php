<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - INFORMATION FOR SUPERANNUATION
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h4> LETTER PROVIDING INFORMATION FOR SUPERANNUATION</h4> 
        </div>         
      </div>
      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         No.: _______________
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         Date: _______________________
       </div>
    </div> 
    <div class="row" style="line-height: 2">
      <div class="col-md-12">
       ___________________________ (Name)
      </div>
      <div class="col-md-12">
       _________________________________________________________ (Address)
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
      Dear
     </div>
   </div>
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
      We would be formally parting with you on __________ (date) upon your attaining the age of superannuation. On behalf of all of us in PRADAN, I would like to say that our association over these years has been meaningful.
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
      I hope you are taking necessary action to plan your retirement. I also take this opportunity to assist you in timely completion of pre-retirement formalities so as to finalise your retirement benefits, etc. I am writing to our Finance-Personnel-MIS Unit to keep your account up to and including ___________ (date) ready and also to let you know the formalities that are required to be completed by you in this regard. Please also arrange to get a ‘Clearance Certificate’ from all concerned (in duplicate) in the prescribed proforma enclosed, well in time to facilitate your release and settlement of your account.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
     For any assistance in this regard, please contact our Finance-Personnel-MIS Unit.</strong>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
    With best wishes.
   </div>
   <div class="col-md-12" style="margin-top: 20px;">
    Yours sincerely,
   </div>
   <div class="col-md-12" style="margin-top: 20px;">
    <p>(_________________________________) </p>
    <p>Executive Director</p>
   </div>
 </div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
  <b>Encl.</b>: As above
 </div>
 
</div>
</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>