<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
</style>
     <form method="POST" action="" name="sep_information" id="sep_information">
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
   <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - INFORMATION FOR SUPERANNUATION
       </h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
    <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
   <div class="panel-body">
    <div class="row">
      <div class="col-md-12 text-center">
        <h4> LETTER PROVIDING INFORMATION FOR SUPERANNUATION</h4> 
      </div>         
    </div>
    <div class="row">
      <div class="col-md-6 text-left" style="margin-bottom: 20px;">
       No.:<input type="text" class="inputborderbelow" value="<?php if($annuation_detail){ echo $annuation_detail->sep_annuation_no;}?>" name="sep_annuation_no" id="sep_annuation_no" maxlength="10" required> 
       
     </div>
     <div class="col-md-6 text-right" style="margin-bottom: 20px;">
       Date: <label name="attaining_age_date" class="inputborderbelow"> <?php echo date('d/m/Y')  ?></label>
     </div>
   </div> 
   <div class="row" style="line-height: 2">
    <div class="col-md-12">
     <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->name;?> </label> (Name)
   </div>
   <div class="col-md-12">
     <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->permanentstreet;?></label> (Address)
   </div>
 </div>

 <div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    Dear
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    We would be formally parting with you on <input type="text" class="inputborderbelow datepicker" name="attaining_age_date" id="attaining_age_date" value="<?php if($annuation_detail){ echo $this->gmodel->changedatedbformate($annuation_detail->attaining_age_date);}?>" required> (date) upon your attaining the age of superannuation. On behalf of all of us in PRADAN, I would like to say that our association over these years has been meaningful.
  </div>
</div>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    I hope you are taking necessary action to plan your retirement. I also take this opportunity to assist you in timely completion of pre-retirement formalities so as to finalise your retirement benefits, etc. I am writing to our Finance-Personnel-MIS Unit to keep your account up to and including <input type="text" class="inputborderbelow datepicker" name="finance_personnel_date" id="finance_personnel_date" value="<?php if($annuation_detail){ echo $this->gmodel->changedatedbformate($annuation_detail->finance_personnel_date);}?>" required>(date) ready and also to let you know the formalities that are required to be completed by you in this regard. Please also arrange to get a ‘Clearance Certificate’ from all concerned (in duplicate) in the prescribed proforma enclosed, well in time to facilitate your release and settlement of your account.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
  For any assistance in this regard, please contact our Finance-Personnel-MIS Unit.</strong>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  With best wishes.
</div>
<div class="col-md-12" style="margin-top: 20px;">
  Yours sincerely,
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <p>(_________________________________) </p>
  <p>Executive Director</p>
</div>
</div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
  <b>Encl.</b>: As above
</div>

</div>
</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($annuation_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <button type="submit" name="saveandsubmit" id="saveandsubmit" value="saveandsubmit" class="btn btn-success btn-sm" onclick="acceptancesuperannuation();"> Save & Submit</button>
  <?php
    }else{

      if($annuation_detail->flag == 0){
 ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <button type="submit" name="saveandsubmit" id="saveandsubmit" value="saveandsubmit" class="btn btn-success btn-sm"  onclick="acceptancesuperannuation();">Save & Submit</button>
<?php } ?>
 
<?php } ?>
 <a href="<?php echo site_url("Hr_report");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</div>
</div>
</section>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->

      <!-- <form method="POST" name="acceptance_regination_modal" id="acceptance_regination_modal"> -->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments" style="max-width: 400px;" maxlength="250"  require> </textarea>
              <?php echo form_error("comments");?>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="button" name="saveandsubmit" value="save & submit" class="btn btn-success btn-sm" onclick="acceptancesuperannuationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>

<script type="text/javascript">
  /*function acceptancesuperannuation(){
    
    var sep_annuation_no = document.getElementById('sep_annuation_no').value;
    var attaining_age_date = document.getElementById('attaining_age_date').value;
    var finance_personnel_date = document.getElementById('finance_personnel_date').value;

    if(finance_personnel_date == ''){
      $('#finance_personnel_date').focus();
    }
    if(attaining_age_date == ''){
      $('#attaining_age_date').focus();
    }
    if(sep_annuation_no == ''){
      $('#sep_annuation_no').focus();
    }

    if(attaining_age_date !='' && finance_personnel_date !='' && sep_annuation_no !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }

  }*/

  function acceptancesuperannuation(){
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }else{
      document.getElementById("sep_information").submit();
    }
  }
</script>

<?php 
if ($annuation_detail){
if($annuation_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#saveandsubmit').hide();
    $("form input[type=text]").prop("disabled", true);
  });
</script>
<?php }} ?>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
$(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
   });

  });

</script>