 <section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Permissions" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Access Rights</h4>
         <div class="col-md-2 text-right">

         </div>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
       <?php 
       $tr_msg= $this->session->flashdata('tr_msg');
       $er_msg= $this->session->flashdata('er_msg');
       if(!empty($tr_msg)){ ?>
       <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <form method="post">

                  <div class="row bg-light" style="padding: 10px;">
                    <div class="col-md-4 col-xs-6 col-xs-4">
                      <div class="form-group">

                        <label for="text" style="float:left;margin-right:5px;">Select Role</label>
                        <div class="input-group">   
                          <select  name="RoleID" id="RoleID" type="text" class="text-input form-control" required>
                            <option value="">Select</option>
                            <?php foreach($role_list as $row){ ?>
                            <option value="<?php echo $row->Acclevel_Cd;?>" <?php echo (set_value('RoleID') == $row->Acclevel_Cd? "selected":"");?>><?php echo $row->Acclevel_Name;?></option>
                            <?php } ?>
                          </select>                     
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <button type="submit" name="submit" class="btn btn-dark waves-effect btn-round btn-sm" name="flag" value="index" style="margin-top : 40px;">GO</button>
                    </div>
                  </div>

                </div>
              </div>

              <div class="row">
               <form action="" method="post">
                <?php $roleid = set_value('RoleID');
                if(!empty($roleid)){ ?>
                <input type="hidden" name="RoleID" value="<?php echo $roleid;?>">
                <input type="hidden" name="flag" value="update">
                <?php }?>   

                <div class="col-sm-12">
                 <div class="row bg-primary" style="margin-left: 1px; margin-right: 1px;padding:10px; font-weight: bold; font-family: 'Oxygen'">
                  <div class="col-md-11">
                   <h5>Dashboard </h5>
                 </div>

                 <label for="managedasboard" style="float: right;">
                  <input type="checkbox" id="managedasboard" class="checkAll" />
                  Check All
                </label>
              </div>
              <div class="demo-checkbox">
                <table class="table table-bordered table-primary">
                  <tr>
                    <td>
                      <b>Campus Incharge Intemation</b> 
                    </td><td>
                      <input type="checkbox" id="Campusinchargeintimationindex" name="selected[Campusinchargeintimation][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Campusinchargeintimation" && $row->Action == "index"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="Campusinchargeintimationindex">View</label>
                    </td><td>
                      <input type="checkbox" id="Campusinchargeintimationadd" name="selected[Campusinchargeintimation][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Campusinchargeintimation" && $row->Action == "add"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="Campusinchargeintimationadd">Add</label>
                    </td><td>
                      <input type="checkbox" id="CampusinchargeintimationSendMail" name="selected[Campusinchargeintimation][SendMail]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Campusinchargeintimation" && $row->Action == "SendMail"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="CampusinchargeintimationSendMail">Send Mail</label> 
                    </td><td>
                      <input type="checkbox" id="CampusinchargeintimationSend" name="selected[Campusinchargeintimation][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Campusinchargeintimation" && $row->Action == "delete"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="CampusinchargeintimationSend">Delete</label> 
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <b>YPR Response</b> 
                    </td><td>
                      <input type="checkbox" id="Ypresponseindex" name="selected[Ypresponse][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Ypresponse" && $row->Action == "index"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="Ypresponseindex">View</label>
                    </td><td>
                    </td><td>
                    </td><td>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Off campus candidate list</b> 
                    </td><td>
                      <input type="checkbox" id="Off_campus_candidate_listindex" name="selected[Off_campus_candidate_list][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Off_campus_candidate_list" && $row->Action == "index"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="Off_campus_candidate_listindex">View</label>
                    </td><td>
                    </td><td>
                    </td><td>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>HRD document checklist</b> 
                    </td><td>
                      <input type="checkbox" id="Hrd_document_checklistindex" name="selected[Hrd_document_checklist][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Hrd_document_checklist" && $row->Action == "index"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="Hrd_document_checklistindex">Index</label>
                    </td><td>
                      <input type="checkbox" id="Hrd_document_checklistverification" name="selected[Hrd_document_checklist][verification]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                      <?php 
                      foreach ($permissions_list as $row) {
                        if ($row->Controller == "Hrd_document_checklist" && $row->Action == "verification"){
                          echo "checked";
                        }
                      }
                      ?> >
                      <label for="Hrd_document_checklistverification">Verification</label>

                    </td><td>
                     <input type="checkbox" id="Hrd_document_checklistview" name="selected[Hrd_document_checklist][view]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                     <?php 
                     foreach ($permissions_list as $row) {
                      if ($row->Controller == "Hrd_document_checklist" && $row->Action == "view"){
                        echo "checked";
                      }
                    }
                    ?> >
                    <label for="Hrd_document_checklistview">View</label>
                  </td><td>
                  </td>
                </tr>


                <tr>
                  <td>
                    <b>Tc verify documents</b> 
                  </td><td>
                    <input type="checkbox" id="Tc_verify_documentsindex" name="selected[Tc_verify_documents][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                    <?php 
                    foreach ($permissions_list as $row) {
                      if ($row->Controller == "Tc_verify_documents" && $row->Action == "index"){
                        echo "checked";
                      }
                    }
                    ?> >
                    <label for="Tc_verify_documentsindex">Index</label>
                  </td><td>
                    <input type="checkbox" id="Tc_verify_documentsverification" name="selected[Tc_verify_documents][verification]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                    <?php 
                    foreach ($permissions_list as $row) {
                      if ($row->Controller == "Tc_verify_documents" && $row->Action == "verification"){
                        echo "checked";
                      }
                    }
                    ?> >
                    <label for="Tc_verify_documentsverification">Verification</label>
                  </td><td>
                   <input type="checkbox" id="Tc_verify_documentsview" name="selected[Tc_verify_documents][view]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                   <?php 
                   foreach ($permissions_list as $row) {
                    if ($row->Controller == "Tc_verify_documents" && $row->Action == "view"){
                      echo "checked";
                    }
                  }
                  ?> >
                  <label for="Tc_verify_documentsview">View</label>
                </td><td>
                </td>
              </tr>

              <tr>
                <td>
                  <b>Offer letter to candidate</b> 
                </td><td>
                  <input type="checkbox" id="Hrddashboardindex" name="selected[Hrddashboard][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                  <?php 
                  foreach ($permissions_list as $row) {
                    if ($row->Controller == "Hrddashboard" && $row->Action == "index"){
                      echo "checked";
                    }
                  }
                  ?> >
                  <label for="Hrddashboardindex">View</label>
                </td><td>
                </td><td>
                </td><td>
                </td>
              </tr>

              <tr>
                <td>
                  <b>Offer letter generation</b> 
                </td><td>
                  <input type="checkbox" id="Assigntcindex" name="selected[Assigntc][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                  <?php 
                  foreach ($permissions_list as $row) {
                    if ($row->Controller == "Assigntc" && $row->Action == "index"){
                      echo "checked";
                    }
                  }
                  ?> >
                  <label for="Assigntcindex">View</label>
                </td><td>

                </td><td>

                </td><td>

                </td>
              </tr>


              <tr>
                <td>
                  <b>Written Score</b> 
                </td><td>
                 <input type="checkbox" id="Selecttowrittenexamcandidatesindex" name="selected[Writtenscore][Selecttowrittenexamcandidates]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                 <?php 
                 foreach ($permissions_list as $row) {
                  if ($row->Controller == "Writtenscore" && $row->Action == "Selecttowrittenexamcandidates"){
                    echo "checked";
                  }
                }
                ?> >
                <label for="Selecttowrittenexamcandidatesindex">Eligible candidates for written exam</label>
              </td><td>
                <input type="checkbox" id="Writtenscoreindex" name="selected[Writtenscore][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                <?php 
                foreach ($permissions_list as $row) {
                  if ($row->Controller == "Writtenscore" && $row->Action == "index"){
                    echo "checked";
                  }
                }
                ?> >
                <label for="Writtenscoreindex">Add</label>
              </td><td>

              </td><td> </td>
            </tr>
            <tr>
              <td>
                <b>GD Score</b> 
              </td><td>
                <input type="checkbox" id="Gdscoreindex" name="selected[Gdscore][Selecttogdscoreexamcandidates]" class="filled-in" value="<?php echo set_value('RoleID');?>"
                <?php 
                foreach ($permissions_list as $row) {
                  if ($row->Controller == "Gdscore" && $row->Action == "Selecttogdscoreexamcandidates"){
                    echo "checked";
                  }
                }
                ?> >
                <label for="Gdscoreindex">List of the candidates who eligible for GD</label>
              </td><td>
               <input type="checkbox" id="Hrscoreindex" name="selected[Hrscore][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
               <?php 
               foreach ($permissions_list as $row) {
                if ($row->Controller == "Gdscore" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Hrscoreindex">Add</label>
            </td><td>

            </td><td>

            </td>
          </tr>
          <tr>
            <td>
              <b>Personal Interview</b> 
            </td><td>
              <input type="checkbox" id="Hrscoreselect_personal_interview_candidates" name="selected[Hrscore][select_personal_interview_candidates]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Hrscore" && $row->Action == "select_personal_interview_candidates"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Hrscoreselect_personal_interview_candidates">List of eligible candidates for personal interview</label>
            </td><td>
             <input type="checkbox" id="Hrscoreindex" name="selected[Hrscore][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
             <?php 
             foreach ($permissions_list as $row) {
              if ($row->Controller == "Hrscore" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Hrscoreindex">Add</label>
          </td><td>

          </td><td>

          </tr>

          <tr>
            <td>
              <b>List Of Selected Candidates</b> 
            </td><td>
              <input type="checkbox" id="Selectedcandidatesindex" name="selected[Selectedcandidates][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Selectedcandidates" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Selectedcandidatesindex">View</label>
            </td><td>

            </td><td>

            </td><td>

            </td>
          </tr>


          <tr>
            <td>
              <b>Ed Summary End of the Day</b> 
            </td><td>
              <input type="checkbox" id="EDsummaryeodindex" name="selected[EDsummaryeod][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "EDsummaryeod" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="EDsummaryeodindex">View</label>
            </td><td>

            </td><td>

            </td><td>

            </td>
          </tr>


          <tr>
            <td>
              <b>ED Dashboard</b> 
            </td><td>
              <input type="checkbox" id="Edashboardindex" name="selected[Edashboard][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Edashboard" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Edashboardindex">View</label>
            </td>
            <td>
              <input type="checkbox" id="EdashboardOffercomments" name="selected[Edashboard][Offercomments]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "EDashboard" && $row->Action == "Offercomments"){
                  echo "checked";
                }
              }
              ?> >
              <label for="EdashboardOffercomments">Offer Comments</label>
            </td>
            <td>


            </td>
          </tr>


          <tr>
            <td>
              <b>Candidates</b> 
            </td><td>
              <input type="checkbox" id="Candidatesindex" name="selected[Candidates][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Candidates" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Candidatesindex">View</label>
            </td><td>
              <input type="checkbox" id="Candidatesadd" name="selected[Candidates][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Candidates" && $row->Action == "add"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Candidatesadd">Add</label>
            </td><td>
              <input type="checkbox" id="Candidatesedit" name="selected[Candidates][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Candidates" && $row->Action == "edit"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Candidatesedit">Edit</label>
            </td><td>
              <input type="checkbox" id="Candidatesdelete" name="selected[Candidates][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Candidates" && $row->Action == "delete"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Candidatesdelete">Delete</label>
            </td>
          </tr>


          <tr>
            <td>
              <b>Central Event</b> 
            </td><td>
              <input type="checkbox" id="Central_eventindex" name="selected[Central_event][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_eventindex">View</label>
            </td><td>
              <input type="checkbox" id="Central_eventadd" name="selected[Central_event][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event" && $row->Action == "add"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_eventadd">Add</label>
            </td><td>
              <input type="checkbox" id="Central_eventedit" name="selected[Central_event][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event" && $row->Action == "edit"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_eventedit">Edit</label>
            </td><td>
              <input type="checkbox" id="Central_eventdelete" name="selected[Central_event][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event" && $row->Action == "delete"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_eventdelete">Delete</label>
            </td>
          </tr>



          <tr>
            <td>
              <b>Central Event Updatioin</b> 
            </td><td>
              <input type="checkbox" id="Central_event_updationindex" name="selected[Central_event_updation][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event_updation" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_event_updationindex">View</label>
            </td><td>
              <input type="checkbox" id="Central_event_updationadd" name="selected[Central_event_updation][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event_updation" && $row->Action == "add"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_event_updationadd">Add</label>
            </td><td>
              <input type="checkbox" id="Central_event_updationedit" name="selected[Central_event_updation][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event_updation" && $row->Action == "edit"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_event_updationedit">Edit</label>
            </td><td>
              <input type="checkbox" id="Central_event_updationdelete" name="selected[Central_event_updation][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Central_event_updation" && $row->Action == "delete"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Central_event_updationdelete">Delete</label>
            </td>
          </tr>

          <tr>
            <td>
              <b>Prejoining intimation</b> 
            </td><td>
              <input type="checkbox" id="Prejoiningindex" name="selected[Prejoining][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Prejoining" && $row->Action == "index"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Prejoiningindex">View</label>
            </td><td>
              <input type="checkbox" id="Prejoiningadd" name="selected[Prejoining][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Prejoining" && $row->Action == "add"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Prejoiningadd">Add</label>
            </td><td>
              <input type="checkbox" id="Prejoiningedit" name="selected[Prejoining][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Prejoining" && $row->Action == "edit"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Prejoiningedit">Edit</label>
            </td><td>
              <input type="checkbox" id="Prejoiningdelete" name="selected[Prejoining][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
              <?php 
              foreach ($permissions_list as $row) {
                if ($row->Controller == "Prejoining" && $row->Action == "delete"){
                  echo "checked";
                }
              }
              ?> >
              <label for="Prejoiningdelete">Delete</label>
            </td>
          </tr>





        </table>
      </div>
      <div class="row bg-warning" style="margin-left: 1px; margin-right: 1px;padding:10px; font-weight: bold; font-family: 'Oxygen'">
        <div class="col-md-11">
         <h5>Masters </h5>
       </div>

       <label for="managemaster" style="float: right;">
         <input type="checkbox" id="managemaster" class="checkAll" />
         Check All
       </label>
     </div>


     <div class="demo-checkbox">
      <table class="table table-bordered table-warning">
        <tr>
          <td>
            <b>Staff to Recruitment team mapping</b> 
          </td><td>
            <input type="checkbox" id="Staffrecruitmentteammappingindex" name="selected[Staffrecruitmentteammapping][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffrecruitmentteammapping" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffrecruitmentteammappingindex">View</label>
          </td><td>
            <input type="checkbox" id="Staffrecruitmentteammappingadd" name="selected[Staffrecruitmentteammapping][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffrecruitmentteammapping" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffrecruitmentteammappingadd">Add</label>
          </td><td>
            <input type="checkbox" id="Staffrecruitmentteammappingedit" name="selected[Staffrecruitmentteammapping][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffrecruitmentteammapping" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffrecruitmentteammappingedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Staffrecruitmentteammappingdelete" name="selected[Staffrecruitmentteammapping][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffrecruitmentteammapping" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffrecruitmentteammappingdelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Campus to staff mapping</b> 
          </td><td>
            <input type="checkbox" id="Campusstaffmappingindex" name="selected[Campusstaffmapping][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campusstaffmapping" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusstaffmappingindex">View</label>
          </td><td>
            <input type="checkbox" id="Campusstaffmappingadd" name="selected[Campusstaffmapping][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campusstaffmapping" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusstaffmappingadd">Add</label>
          </td><td>
            <input type="checkbox" id="Campusstaffmappingedit" name="selected[Campusstaffmapping][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campusstaffmapping" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusstaffmappingedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Campusstaffmappingdelete" name="selected[Campusstaffmapping][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campusstaffmapping" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusstaffmappingdelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Recruiters to recruiters head mapping</b> 
          </td><td>
            <input type="checkbox" id="Recruitersheadmappingindex" name="selected[Recruitersheadmapping][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Recruitersheadmapping" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Recruitersheadmappingindex">View</label>
          </td><td>
            <input type="checkbox" id="Recruitersheadmappingadd" name="selected[Recruitersheadmapping][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Recruitersheadmapping" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Recruitersheadmappingadd">Add</label>
          </td><td>
            <input type="checkbox" id="Recruitersheadmappingedit" name="selected[Recruitersheadmapping][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Recruitersheadmapping" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Recruitersheadmappingedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Recruitersheadmappingdelete" name="selected[Recruitersheadmapping][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Recruitersheadmapping" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Recruitersheadmappingdelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Campus</b> 
          </td><td>
            <input type="checkbox" id="Campusindex" name="selected[Campus][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campus" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusindex">View</label>
          </td><td>
            <input type="checkbox" id="Campusadd" name="selected[Campus][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campus" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusadd">Add</label>
          </td><td>
            <input type="checkbox" id="Campusedit" name="selected[Campus][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campus" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Campusdelete" name="selected[Campus][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Campus" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Campusdelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Batch</b> 
          </td><td>
            <input type="checkbox" id="Batchindex" name="selected[Batch][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Batch" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Batchindex">View</label>
          </td><td>
            <input type="checkbox" id="Batchadd" name="selected[Batch][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Batch" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Batchadd">Add</label>
          </td><td>
            <input type="checkbox" id="Batchedit" name="selected[Batch][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Batch" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Batchedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Batchdelete" name="selected[Batch][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Batch" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Batchdelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>State</b> 
          </td><td>
            <input type="checkbox" id="Stateindex" name="selected[State][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "State" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Stateindex">View</label>
          </td><td>
            <input type="checkbox" id="Stateadd" name="selected[State][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "State" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Stateadd">Add</label>
          </td><td>
            <input type="checkbox" id="Stateedit" name="selected[State][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "State" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Stateedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Statedelete" name="selected[State][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "State" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Statedelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Resource Person</b> 
          </td><td>
            <input type="checkbox" id="Resourcepersonindex" name="selected[Resourceperson][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Resourceperson" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Resourcepersonindex">View</label>
          </td><td>
            <input type="checkbox" id="Resourcepersonadd" name="selected[Resourceperson][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Resourceperson" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Resourcepersonadd">Add</label>
          </td><td>
            <input type="checkbox" id="Resourcepersonedit" name="selected[Resourceperson][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Resourceperson" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Resourcepersonedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Resourcepersondelete" name="selected[Resourceperson][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Resourceperson" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Resourcepersondelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Relation</b> 
          </td><td>
            <input type="checkbox" id="Sysrelationindex" name="selected[Sysrelation][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysrelation" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysrelationindex">View</label>
          </td><td>
            <input type="checkbox" id="Sysrelationadd" name="selected[Sysrelation][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysrelation" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysrelationadd">Add</label>
          </td><td>
            <input type="checkbox" id="Sysrelationedit" name="selected[Sysrelation][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysrelation" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysrelationedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Sysrelationdelete" name="selected[Sysrelation][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysrelation" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysrelationdelete">Delete</label>
          </td>
        </tr>

        <tr>
          <td>
            <b>Identity</b> 
          </td><td>
            <input type="checkbox" id="Sysidentityindex" name="selected[Sysidentity][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysidentity" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysidentityindex">View</label>
          </td><td>
            <input type="checkbox" id="Sysidentityadd" name="selected[Sysidentity][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysidentity" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysidentityadd">Add</label>
          </td><td>
            <input type="checkbox" id="Sysidentityedit" name="selected[Sysidentity][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysidentity" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysidentityedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Sysidentitydelete" name="selected[Sysidentity][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Sysidentity" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Sysidentitydelete">Delete</label>
          </td>
        </tr>


        <tr>
          <td>
            <b>Staff Category</b> 
          </td><td>
            <input type="checkbox" id="Staffcategoryindex" name="selected[Staffcategory][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffcategory" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffcategoryindex">View</label>
          </td><td>
            <input type="checkbox" id="Staffcategoryadd" name="selected[Staffcategory][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffcategory" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffcategoryadd">Add</label>
          </td><td>
            <input type="checkbox" id="Staffcategoryedit" name="selected[Staffcategory][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffcategory" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffcategoryedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Staffcategorydelete" name="selected[Staffcategory][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Staffcategory" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Staffcategorydelete">Delete</label>
          </td>
        </tr>



        <tr>
          <td>
            <b>Language</b> 
          </td><td>
            <input type="checkbox" id="Syslanguageindex" name="selected[Syslanguage][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Syslanguage" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Syslanguageindex">View</label>
          </td><td>
            <input type="checkbox" id="Syslanguageadd" name="selected[Syslanguage][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Syslanguage" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Syslanguageadd">Add</label>
          </td><td>
            <input type="checkbox" id="Syslanguageedit" name="selected[Syslanguage][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Syslanguage" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Syslanguageedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Syslanguagedelete" name="selected[Syslanguage][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Syslanguage" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Syslanguagedelete">Delete</label>
          </td>
        </tr>


        <tr>
          <td>
            <b>Post Graduate</b> 
          </td><td>
            <input type="checkbox" id="Pgeducationindex" name="selected[Pgeducation][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Pgeducation" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Pgeducationindex">View</label>
          </td><td>
            <input type="checkbox" id="Pgeducationadd" name="selected[Pgeducation][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Pgeducation" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Pgeducationadd">Add</label>
          </td><td>
            <input type="checkbox" id="Pgeducationedit" name="selected[Pgeducation][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Pgeducation" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Pgeducationedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Pgeducationdelete" name="selected[Pgeducation][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Pgeducation" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Pgeducationdelete">Delete</label>
          </td>
        </tr>





        <tr>
          <td>
            <b>Graduate</b> 
          </td><td>
            <input type="checkbox" id="Ugeducationindex" name="selected[Ugeducation][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Ugeducation" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Ugeducationindex">View</label>
          </td><td>
            <input type="checkbox" id="Ugeducationadd" name="selected[Ugeducation][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Ugeducation" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Ugeducationadd">Add</label>
          </td><td>
            <input type="checkbox" id="Ugeducationedit" name="selected[Ugeducation][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Ugeducation" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Ugeducationedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Ugeducationdelete" name="selected[Ugeducation][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Ugeducation" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Ugeducationdelete">Delete</label>
          </td>
        </tr>


        <tr>
          <td>
            <b>Sectoral Specialization</b> 
          </td><td>
            <input type="checkbox" id="Operationindex" name="selected[Operation][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Operation" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Operationindex">View</label>
          </td><td>
            <input type="checkbox" id="Operationadd" name="selected[Operation][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Operation" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Operationadd">Add</label>
          </td><td>
            <input type="checkbox" id="Operationedit" name="selected[Operation][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Operation" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Operationedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Operationdelete" name="selected[Operation][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Operation" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Operationdelete">Delete</label>
          </td>
        </tr>




        <tr>
          <td>
            <b>Designations</b> 
          </td><td>
            <input type="checkbox" id="Designationsindex" name="selected[Designations][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Designations" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Designationsindex">View</label>
          </td><td>
            <input type="checkbox" id="Designationsadd" name="selected[Designations][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Designations" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Designationsadd">Add</label>
          </td><td>
            <input type="checkbox" id="Designationsedit" name="selected[Designations][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Designations" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Designationsedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Designationsdelete" name="selected[Designations][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Designations" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Designationsdelete">Delete</label>
          </td>
        </tr>



        <tr>
          <td>
            <b>District</b> 
          </td><td>
            <input type="checkbox" id="Districtindex" name="selected[District][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "District" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Districtindex">View</label>
          </td><td>
            <input type="checkbox" id="Districtadd" name="selected[District][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "District" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Districtadd">Add</label>
          </td><td>
            <input type="checkbox" id="Districtedit" name="selected[District][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "District" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Districtedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Districtdelete" name="selected[District][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "District" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Districtdelete">Delete</label>
          </td>
        </tr>









      </table>
    </div>
     <div class="row bg-danger" style="margin-left: 1px; margin-right: 1px;padding:10px; font-weight: bold; font-family: 'Oxygen'">
        <div class="col-md-11">
         <h5>Manage User </h5>
       </div>

       <label for="manageuser"  style="float: right;">
        <input type="checkbox" id="manageuser" class="checkAll" />
         Check All
       </label>
     </div>

    
    <div class="demo-checkbox">
      <table class="table table-bordered table-danger">
        <tr>
          <td>
            <b>User Manage</b> 
          </td><td>
            <input type="checkbox" id="userindex" name="selected[user][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "user" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="userindex">View</label>
          </td><td>
            <input type="checkbox" id="useradd" name="selected[user][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "user" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="useradd">Add</label>
          </td><td>
            <input type="checkbox" id="useredit" name="selected[user][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "user" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="useredit">Edit</label>
          </td><td>
            <input type="checkbox" id="userdelete" name="selected[user][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "user" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="userdelete">Delete</label>
          </td>
        </tr>       
      </table>
    </div>

     <div class="row bg-success" style="margin-left: 1px; margin-right: 1px;padding:10px; font-weight: bold; font-family: 'Oxygen'">
        <div class="col-md-11">
         <h5>Manage Permissions </h5>
       </div>

      <label for="managepermission" style="float: right;">
        <input type="checkbox" id="managepermission" class="checkAll" />
         Check All
       </label>
     </div>

    <div class="demo-checkbox">
      <table class="table table-bordered table-success">
        <tr>
          <td>
            <b>Permissions Manage</b> 
          </td><td>
            <input type="checkbox" id="Permissionsindex" name="selected[Permissions][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsindex">View</label>
          </td><td>
            <input type="checkbox" id="Permissionsadd" name="selected[Permissions][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsadd">Add</label>
          </td><td>
            <input type="checkbox" id="Permissionsedit" name="selected[Permissions][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Permissionsdelete" name="selected[Permissions][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsdelete">Delete</label>
          </td>
        </tr>       
      </table>
    </div>

    <div class="row bg-info" style="margin-left: 1px; margin-right: 1px;padding:10px; font-weight: bold; font-family: 'Oxygen'">
        <div class="col-md-11">
         <h5>Manage Executive Director</h5>
       </div>

       <label for="manageexecutivedirector" style="float: right;">
       <input type="checkbox" id="manageexecutivedirector" class="checkAll" />
         Check All
       </label>
     </div>
    <div class="demo-checkbox">
      <table class="table table-bordered table-info">
        <tr>
          <td>
            <b>Manage Executive Director Manage</b> 
          </td><td>
            <input type="checkbox" id="Permissionsindex" name="selected[Permissions][index]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "index"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsindex">View</label>
          </td><td>
            <input type="checkbox" id="Permissionsadd" name="selected[Permissions][add]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "add"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsadd">Add</label>
          </td><td>
            <input type="checkbox" id="Permissionsedit" name="selected[Permissions][edit]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "edit"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsedit">Edit</label>
          </td><td>
            <input type="checkbox" id="Permissionsdelete" name="selected[Permissions][delete]" class="filled-in" value="<?php echo set_value('RoleID');?>"
            <?php 
            foreach ($permissions_list as $row) {
              if ($row->Controller == "Permissions" && $row->Action == "delete"){
                echo "checked";
              }
            }
            ?> >
            <label for="Permissionsdelete">Delete</label>
          </td>
        </tr>       
      </table>
    </div>

    <div class="col-md-12" style="margin-bottom: 20px;">
      <div align="center"> 
        <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
        <a href="<?php echo site_url("Hr_report");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
      </form>
    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
</div> 
<?php } } ?>  
</section>
<script type="text/javascript">

  $('.checkAll').on('click',function(){
    if($(this).is(':checked')){
      $(this).parent().parent().next('.demo-checkbox').find('input[type="checkbox"]').prop('checked','checked');
    }else{        
      $(this).parent().parent().next('.demo-checkbox').find('input[type="checkbox"]').prop('checked','');
    }
  });
</script>