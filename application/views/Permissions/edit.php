<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
     
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                 <h4 class="header" class="field-wrapper required-field" style="color: green">Edit Batch</h4>
                 <div class="col-sm-12">
                   <form method="POST" action="">
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Batch</label>
                        <input type="text" class="form-control" name="batch" id="batch" placeholder="Please Enter Batch" value="<?php echo $batchupdate[0]->batch;?>" >
                      </div>
                       <?php echo form_error("batch");?>
                    </div>

                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Date of joining</label>
                        <input type="date" class="form-control datepicker" name="dateofjoining" id="dateofjoining" placeholder="Please Enter Date of joining " value="<?php echo $batchupdate[0]->dateofjoining;?>" >
                      </div>
                        <?php echo form_error("dateofjoining");?>
                    </div>
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Status </label>
                     <select name="status" id="status" class="form-control" data-toggle="" >
                      <option value="">Select Status</option>
                        <option value="0" <?php if ($batchupdate[0]->status==0) { echo "SELECTED"; }else{ echo " "; }
                         ?> >Active</option>
                          <option value="1" <?php if ($batchupdate[0]->status==1) { echo "SELECTED"; }else{ echo " "; }
                         ?> >InActive</option>
                          </select>
                    <?php echo form_error("status");?>
                    </div>
                      <div style="text-align: -webkit-center;"> 
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("batch");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>


