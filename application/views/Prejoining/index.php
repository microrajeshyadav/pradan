<section class="content" style="background-color: #FFFFFF;" >
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Prejoining" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
  <style type="text/css">
  .btn1{
    margin-left: 197px;
    margin-top: -93px;
    height: 35px;
  }
</style>
<div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
     <h4 class="col-md-10 panel-title pull-left">List of candidates for pre-joining</h4>
     <div class="col-md-2 text-right">
      

       <a href="<?php echo site_url()."Prejoining/add/";?>" data-toggle="tooltip" data-placement="bottom" title="Want to add Pre Joining Intimation? Click on me."  class="btn btn-primary btn-sm">Pre Joining Intimation</a>

     </div>
   </div>
   <hr class="colorgraph"><br>
 </div>
  
      <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

           <?php   //print_r($selectedcandidatedetails); ?>
            <div class="row" style="background-color: #FFFFFF;">
           
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                    <div style="font-size: 15px; font-weight: 700;" > 
                      <a href="<?php echo site_url()."Prejoining/sendmail/";?>" data-toggle="tooltip" data-placement="bottom" title="Want to sent to supervisior? Click on me."  class="btn btn-primary btn-sm">Send mail to Supervisior for expected candidates joining</a></div><br> 

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                    <table id="tableAssignteam" class="table table-bordered table-wrapper ">
                      <thead>
                       <tr>
                        <th class="text-center" style ="max-width:30px;">S. No.</th>
                        <th>Category</th>
                        <th>Campus Name</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Degree</th>
                        <th>Specialization</th>
                        <th>Field Guide</th>
                        <th>Batch</th>
                        <th>Email ID</th>
                        <th>Contact NO</th>
                        <th>Letter</th>
                        <th class="text-center" style ="max-width:50px;">Action</th>
                      </tr> 
                    </thead>
                    <tbody>
                      <?php  
                      $i=0; foreach ($prejoiningdetails as $key => $value) {
                        ?>
                        <tr>
                          <td class="text-center"><?php echo $i+1; ?></td>
                           <td><?php echo $value->categoryname;?></td>
                           <td><?php echo $value->campusname;?></td>
                          <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
                          <td><?php echo $value->gender;?></td>
                          <td><?php echo $value->ugdegree;?></td>
                          <td><?php echo $value->specialization;?></td>
                          <td><?php echo $value->name;?></td>
                          <td><?php echo $value->batch;?></td>
                          <td><?php echo $value->emailid;?></td>
                          <td><?php echo $value->mobile;?></td>
                          <td> <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal_<?php echo $key;?>">Letter</button></td>
                          
                          <td class="text-center"><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this prejoining." href="<?php echo  site_url().'Prejoining/edit/'.$value->id;?>" traget="_blank"><i class="fa fa-edit" aria-hidden="true"></i></a><!-- |<a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this prejoining." href="<?php echo  site_url().'Prejoining/delete/'.$value->id;?>" traget="_blank" onclick="confirm_delete();"> <i class="fa fa-trash" style="color:red"></i></a> --></td>
                        </tr>
                <div id="myModal_<?php echo $key;?>" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title pull-left">Letter</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      
                    </div>
                    <div class="modal-body">
                      <p><?php echo $value->prejoining_letter;?></p>
                    </div>

                  <?php $attachments =  $this->model->getPrejoiningDocumentDetails($value->id);
                        foreach ($attachments as $key => $val) {
                        ?>

                     <div class="modal-body">
                      <p>
                       <a title="click her" href="<?php echo  site_url().'datafiles/prejoiningdocument/'.  $val->encrypted_prejoining_document_name;?>" traget="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                      </p>
                    </div>
                  <?php }  ?>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                      </div>
                    </div>
                  </div>
                   <?php $i++; }  ?>
                      </tbody>
                    </table>
                  </div>

                </div>
              
            </div>
          </div>
        </div>  
        <?php } } ?> 
      </section>
      <!--  JS Code Start Here  -->
      <script type="text/javascript">
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();  
          $('#tableAssignteam').DataTable({
           "bPaginate": true,
           "bInfo": true,
           "bFilter": true,
           "bLengthChange": true
         }); 
   });

     
function confirm_delete() {
    var r = confirm("Do you want to delete this Prejoining record?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
    }
</script>

<!--  JS Code End Here  -->