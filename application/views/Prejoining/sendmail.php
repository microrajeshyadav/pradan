  <section class="content" style="background-color: #FFFFFF;" >
  <br>
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Prejoining" && $row->Action == "sendmail"){ ?>
  <div class="container-fluid">
    <form name="formIntemation" id="formIntemation" action="" method="POST">
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-8 panel-title pull-left">Send to Supervisors</h4>
           <div class="col-md-4 text-right" style="color: red">
            * Denotes Required Field 
          </div>
        </div>
        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body">
       <?php 
       $tr_msg= $this->session->flashdata('tr_msg');
       $er_msg= $this->session->flashdata('er_msg');
       if(!empty($tr_msg)){ ?>
       <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

             <div class="row">
              
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br>
            <label for="Name">Batch<span style="color: red;" >*</span></label>
           <?php 
                      $options = array('' => 'Select Batch ');
                       // $selected_batch = $batch_details->id; 
                      foreach($batch_details as $key => $value) {
                        $options[$value->batch] = $value->batch;
                      }
                      echo form_dropdown('batch', $options, set_value('batch'), 'class="form-control" id="batch" required="required"' );
                      ?>
          
           
       </div>
     </div>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br>
            <label for="Name">Subject<span style="color: red;" >*</span></label>
            <input type="text" name="subject" id="subject" class="form-control" value="<?php echo $subject;?>">
          </div>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br>
            <label for="Name">Message <span style="color: red;" >*</span> Kindly provide date in message </label>
            <!-- <textarea name="message" id="message" class="form-control" cols="10" rows="10"><?php //echo $body;?></textarea> -->
            <textarea class='summernote' id='summernote' name="lettercontent" rows='10' ><?php echo $body;?></textarea>
          </div>
       </div>
     </div>
     
   </div>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="panel-footer text-right">
    <button type="submit" name="send" id="send" value="send" class="btn btn-warning btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to send mail to ? Click on me.">Send</button>
    <a href="<?php echo site_url("/Prejoining/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to list</a> 
  </div>  
</div>
</div>
</form>
</div>
</div>   
<?php } } ?>  
</section>

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.min.js"></script>

<script type="text/javascript">
 $(document).ready(function(){

 });     



</script>

  <script type="text/javascript">
  $(document).ready(function() {
    var editor = $('#summernote');
    editor.summernote({
        height: ($(window).height()),
        focus: false,
        toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
      ],
        oninit: function() {
            // Add "open" - "save" buttons $('#foo').addClass('connected-sortable droppable-area2');
            /*var noteBtn = '<button id="makeSnote" type="button" class="btn btn-default btn-sm btn-small" title="Identify a music note" data-event="something" tabindex="-1"><i class="fa fa-music"></i></button>';            
            var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
            $(fileGroup).appendTo($('.note-toolbar'));*/
            // Button tooltips
            $('#makeSnote').tooltip({container: 'body', placement: 'bottom'});
            // Button events
            $('#makeSnote').click(function(event) {
                var highlight = window.getSelection(),  
                    spn = document.createElement('span'),
                    range = highlight.getRangeAt(0)
                
                spn.innerHTML = highlight;
                spn.className = 'snote';  
                spn.style.color = 'blue';
            
                range.deleteContents();
                range.insertNode(spn);
            });
         },        
    });  
});

      $('#batch').change(function() {
       var b = $("#batch").val();
       var sub = $("#subject").val();
       var newsub = sub.replace("**",b);
       $("#subject").val(newsub);
   });
</script>








