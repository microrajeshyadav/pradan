<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Prejoining" && $row->Action == "index"){ ?>
  <br>

  <div class="container-fluid">
  <style type="text/css">
  .btn1{
    margin-left: 197px;
    margin-top: -93px;
    height: 35px;
  }
</style>
<div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
     <h4 class="col-md-10 panel-title pull-left">Pre joining</h4>
     
   </div>
   <hr class="colorgraph"><br>
 </div>

      <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            
              <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" enctype="multipart/form-data" >
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" > <label for="Name">DA Name<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
              <select class="form-control" name="daname" id="daname" required="required">
                <option value="" >Select DA </option>
              <?php foreach ($dadetails as $key => $value) {?>
                <option value="<?php echo $value->candidateid;?>"><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename .' '.$value->candidatelastname;?></option>
              <?php } ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            </div>
            <br><br>
            <div id="listbatch">
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Batch <span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
           <input type="text" name="batch" id="batch" readonly maxlength="2" minlength="1" value="" class="form-control sd" required="">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
             <br>
             <br>
            
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Letter</label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
             <!-- <textarea class="form-control" name="letter" maxlength="1500" minlength="5" id="letter" cols="20" rows="10" required=""></textarea> -->
            <textarea class="summernote" id="summernote" name="summernote" cols="20" rows="10"></textarea>  
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
           </div>
           
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <br>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Attachment <span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
            <input type="file" name="prejoiningdocument[]" id="prejoiningdocument" class="form-control" accept=".doc,.docx" multiple="multiple" required="required" autocomplete="off">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
                 <br>
                 <br>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="background-color: white;"></div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer text-right" style="background-color: white;">
                <button type="submit" class="btn btn-success xs-10" name="btnsend" id="checkBtn" value="AcceptData" data-toggle="tooltip" title="Want to save your changes? Click on me.">Send</button> 
                <a href="<?php echo site_url().'Prejoining/'; ?>" class="btn btn-dark xs-10" name="btncancel" id="btncancel" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>

              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
           
          </form>
        </div>
      </div>
    </div>   
  </section>
   <?php } } ?> 

  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable(); 
    });
  </script> 

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.min.js"></script>

   <script type="text/javascript">
  $(document).ready(function() {
    var editor = $('#summernote');
    editor.summernote({
        height: ($(window).height()),
        focus: false,
        toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
      ],
        oninit: function() {
            // Add "open" - "save" buttons $('#foo').addClass('connected-sortable droppable-area2');
            /*var noteBtn = '<button id="makeSnote" type="button" class="btn btn-default btn-sm btn-small" title="Identify a music note" data-event="something" tabindex="-1"><i class="fa fa-music"></i></button>';            
            var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
            $(fileGroup).appendTo($('.note-toolbar'));*/
            // Button tooltips
            $('#makeSnote').tooltip({container: 'body', placement: 'bottom'});
            // Button events
            $('#makeSnote').click(function(event) {
                var highlight = window.getSelection(),  
                    spn = document.createElement('span'),
                    range = highlight.getRangeAt(0)
                
                spn.innerHTML = highlight;
                spn.className = 'snote';  
                spn.style.color = 'blue';
            
                range.deleteContents();
                range.insertNode(spn);
            });
         },        
    });  
});
</script> 

  <script type="text/javascript">
   
     $("#daname").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getDAbatch/'+$(this).val(),
        type: 'POST',
        dataType: 'json',
      })
      .done(function(data) {
         console.log(data[0].batch);
          $("#batch").val(data[0].batch);  
          $(".note-editable").html(data[1]);  

        // $.each(data, function(index) {
        //     console.log(data);
        //     $("#letter").val(data[0].msgbody);
        //     $("#listbatch").val(data[1].batch);          
        //   })
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

  </script>

<script type="text/javascript">
  
  $('.sd').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});


</script>