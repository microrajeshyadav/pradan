
<?php 

$this->loginData = $this->session->userdata('login_data');

//print_r($fetchresult);
?>;
<style type="text/css">
  /* input[type=text] {
    background: transparent;
    border: none;
    border-bottom: 1px solid #000000;
    }*/
    textarea {
    	resize: none;
    }
    hr{
    	border-top: 1px solid black;
    }
</style>
<section class="content" style="background-color: #FFFFFF;">
	<br/>	
	<?php $page='Joining'; require_once(APPPATH.'views/Employee_particular_form/topbar.php');?>

	
	
	
	<div class="container-fluid" style="font-family: 'Oxygen' !important;">
		<div class="row text-center" style="padding: 14px;">	

			<div class="panel thumbnail shadow-depth-2 listcontainer">

				<div class="panel-heading">

					<div class="row">
						<h4 class="col-md-12 panel-title pull-left">JOINING REPORT ON APPOINTMENT 
						</h4>
					</div>
					<hr class="colorgraph"><br>
				</div>
				<div class="panel-body">					
					<div class="row">	
						<form class ="col-md-12" method="POST" action="" enctype="multipart/form-data">							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
								<h6>Professional Assistance for Development Action (PRADAN) </h6>
								<h5>JOINING REPORT ON APPOINTMENT</h5> 
							</div>	
							<div  class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<label for="ExecutiveDirector" class="field-wrapper required-field">To:<br>The Executive Director,<br>
										PRADAN.
									</label>
									<p><?php echo $officename_list->officename; ?></p>
								</div>
							</div>
							<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right">
								<label for="Name">Address:&nbsp;</label>
								<textarea name="address" id="address"  placeholder=" Enter Address" class="form-control inputborderbelow" required="required"> <?php echo $candidatedetails->permanenthno,$candidatedetails->permanentstreet. $candidatedetails->permanentcity.$candidatedetails->permanentpincode;?> </textarea>
							</div>
							<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">				
								Dear Sir, 							
							</div>
							<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">
								<div class="form-group">
									Subject: <b>Joining Report</b>
								</div>
							</div>
							<!-- <?php print_r($fetchresult); ?> -->
							
							<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
								<div class="form-group">
									
									Please refer to your offer of appointment no &nbsp;<b><?php if(!empty($fetchresult->offerno)) echo  $fetchresult->offerno; ?></b>&nbsp;dated
									&nbsp;<b><?php if(!empty($fetchresult->doj)) echo $fetchresult->doj; ?></b>&nbsp;
									offering me appointment as&nbsp;<b><?php if(!empty($fetchresult->desname)) echo  $fetchresult->desname; ?></b>&nbsp; at
									&nbsp;<b><?php if(!empty($fetchresult->officename)) echo $fetchresult->officename; ?></b>&nbsp;
									I hereby report for duty in the forenoon of today,<br><br> the <input type="text" class="inputborderbelow datepicker" name="dutydate" id="dutydate" value="" required="required">(date/month/year). I shall inform you of any change in my address, given above, when it occurs.
									<br><br>
									Yours faithfully,
									<br>
									<br>

									<!-- <?php print_r($fetchresult);?> -->
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">



										Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="inputborderbelow"><b><?php if(!empty($candidatedetailwithaddress->staff_name)) echo $candidatedetailwithaddress->staff_name; ?></span>
										</div>




										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
											<input type="text" name="declarationplace" required="" maxlength="20" minlength="2" class="form-control inputborderbelow" placeholder="Enter Place" value="<?php if(!empty($candidatedetailwithaddress->officename)) echo $candidatedetailwithaddress->officename;?>">
											<input type="hidden" name="daplace" value="<?php if(!empty($candidatedetailwithaddress->new_office_id)) echo $candidatedetailwithaddress->new_office_id;?>">
											<input type="text" name="declarationdate" class="form-control inputborderbelow datepicker" required=""  placeholder="Select Date">
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
											<div class="form-check">
												<label for="Name">Signature<span style="color: red;" >*</span></label>
												 <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
												<!-- <?php if ($signature->encrypted_signature !='') { ?>
													<img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
												<?php }else{ ?>
													<img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
												<?php } ?> -->
											</div>
										</div>
										
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right" style="float;left; width: 100%;">
									<button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
									<button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
								</div>
							</form>

						</div>
						<?php 
						if($this->loginData->RoleID==2 || $this->loginData->RoleID==17)
							{?>	
								<div class="row">
									<form  class ="col-md-12" method="POST" action="" enctype="multipart/form-data">

										<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px; margin-top: 30px;">
											<b>Countersigned:</b> (by Employee Responsible for Induction)
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

											<input type="text" name="Countersignedname" required="" maxlength="30" minlength="2" required="" class="form-control inputborderbelow" placeholder="Enter Countersigned Person Name">

										</div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right" style="margin-bottom: 50px;">
											<input type="text" name="Countersignedplace" required="" maxlength="20" minlength="2" class="form-control inputborderbelow" placeholder="Enter Place">	
											<input type="text" class="form-control inputborderbelow datepicker" name="Countersigneddate" required=""  placeholder="Select Date">
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<p style="float: left;"></p>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<p style="float: left;margin-left: 5px;">cc: - Supervisor</p>
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<p style="float: left;"> &emsp;&emsp;- Finance-Personnel-MIS Unit</p>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<p style="float: left;"> &emsp;&emsp;- Personal Dossier (Location)</p>
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right" style="float;left; width: 100%;">
											<button  type="submit" disabled class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
										</div>							
									</form>
								</div>

							<?php } 
							if($this->loginData->RoleID== 17)
								{ ?>
									<div  class="row">
										<form class="col-lg-12 col-md-12" method="POST" action="" enctype="multipart/form-data">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;"> <b>(For use in the Finance-Personnel-MIS Unit)</b></div>
											<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

												Ms./Mr. <span class="inputborderbelow"><b><?php


												echo $roww->candidatefirstname; ?></b>&nbsp;<b><?php echo $roww->candidatelastname; ?></b></span> has joined at                    
												<input type="text" name="newoffice" required="" maxlength="30" minlength="2" required="" class="inputborderbelow" placeholder="Enter Joined Office Name">                 

												noted in Probation register.
											</div>

											<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
												<div class="form-check">
													<input type="checkbox" class="form-check-input" readonly="" name="NotedinProbationregister" id="NotedinProbationregister" >
													<label class="form-check-label" for="NotedinProbationregister">(Please<span class="glyphicon glyphicon-ok"></span>)</label>
												</div>
											</div>
											<br><br>
											<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
												<div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
													<input  class= "form-control inputborderbelow" type="text" name="FinancePersonnelMISUnit">
												</div>
											</div>
											<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
												(Finance-Personnel-MIS Unit)
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right" style="float;left; width: 100%;">
												<button  type="submit" disabled  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
											</div>
										</form>
									<?php } ?>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script>
			$(document).ready(function(){
		//$('[data-toggle="tooltip"]').tooltip();
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1980:2030',
			dateFormat : 'dd/mm/yy',
			maxDate: 'today',
		});
	});
</script>
