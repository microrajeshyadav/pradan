
<br>
<div class="container-fluid">
  <div class="panel panel-default" >
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left"><?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> to candidate </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <?php 
     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');

     if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <?php //echo "<pre>"; print_r($selectedcandidatedetails); ?>
        <div class="row" style="background-color: #FFFFFF;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white;">
           <table id="Inboxtable" class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
             <tr>
              <th  class="text-center" style ="max-width:50px;" >S. No.</th>
              <th>Category</th>
              <th>Campus Name</th>
              <th> Name</th>
              <th>Gender</th>
              <th> Email Id</th>
              <th> Team </th>
              <?php if ($this->loginData->RoleID !=17) { ?>
                <th> FG </th>
                <th> Batch </th>
              <?php } ?>
              <th class="text-center"> ED Comments</th>
              <th class="text-center"> <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> </th>
              <th style ="max-width:100px;" class="text-center"> <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> </th>
              <!-- <th style ="max-width:100px;" class="text-center">convert To  Executive</th> -->

            </tr> 
          </thead>
          <tbody>
            <?php $i=0; 
              // echo "<pre>";print_r($selectedcandidatedetails);exit();
             foreach ($selectedcandidatedetails as $key => $value) {
              ?>
              <tr>
                <td  class="text-center"><?php echo $i+1; ?></td>
                <td><?php echo $value->categoryname;?> </td>
                <td><?php echo $value->campusname;?></td>
                <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
                <td><?php echo $value->gender;?></td>
                <td ><?php echo $value->emailid;?> </td>
                <td ><?php echo $value->officename;?></td>
                <?php if ($this->loginData->RoleID !=17) { ?>
                  <td ><?php echo $value->name;?></td>
                  <td ><?php echo $value->batch;?></td>
                <?php } ?>
                <td class="text-center"><?php if ($value->status ==0) { ?>
                 <span class="badge badge-pill badge-success">Approve</span>
               <?php   }else{ ?>
                <span class="badge badge-danger">Rejected</span>
                <?php  } ?></td>

                <td class="text-center">
                  <a href="<?php echo site_url("pdf_offerletters").'/'.$value->filename.'.pdf';?>" download  data-toggle="tooltip" title="Download <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?>" style="vertical-align: top;">
                    <i class="fa fa-download fx-2" aria-hidden="true"></i>
                  </a>
                  <?php if(!empty($value->otherfiles)){
                    $otherfiles_array = explode(',', $value->otherfiles);
                    foreach($otherfiles_array as $res){
                      ?>
                      <a href="<?php echo site_url("pdf_offerletters").'/'.$res;?>" download  data-toggle="tooltip" title="Download <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?>" style="vertical-align: top;">
                        <i class="fa fa-download fx-2" aria-hidden="true"></i>
                      </a>
                    <?php } } ?>
                  </td>

                  <?php if ($value->flag=='Send') { ?>
                    <td class="text-center"> <span class="badge badge-success"><?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> Sent</span> </td>
                  <?php } else if($value->status == 1){ ?>
                    <td  class="text-center">  <span class="badge badge-danger">Can`t <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> </span> </td>
                  <?php }else{ ?>
                    <td class="text-center">
                      <!-- href="<?php echo site_url().'Hrddashboard/sendofferlettertocandidate/'.$value->candidateid; ?>" data-toggle="modal" data-target="#myModal" -->
                      <a class="btn btn-primary btn-xs m-t-10 " title="Send to Candidate" onclick="others_document(<?php echo $value->candidateid; ?>);">Send</a>
                    </td>
                    <td class="text-center">
                    </td>
                  <?php } ?>
                </tr>
                <?php $i++; } ?>

                <?php  foreach ($selectedexecutivedetails as $key => $value) {
                  ?>
                  <tr>
                    <td  class="text-center"><?php echo $i+1; ?></td>
                    <td>DA<?php //echo $value->categoryname;?> </td>
                    <td><?php echo $value->campusname;?></td> 
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->gender;?></td>
                    <td ><?php echo $value->emailid;?> </td>
                    <td ><?php echo $value->officename;?></td>
                    <td  class="text-center"><?php if ($value->edstatus == 0) { ?>
                     <span class="badge badge-pill badge-success">Approve</span>
                   <?php   }else{ ?>
                    <span class="badge badge-danger">Rejected</span>
                    <?php  } ?></td>

                    <td class="text-center">
                      <a href="<?php echo site_url("pdf_offerletters").'/'.$value->filename.'.pdf';?>" download  data-toggle="tooltip" title="Download <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?>" style="vertical-align: top;">
                        <i class="fa fa-download fx-2" aria-hidden="true"></i>
                      </a>                
                    </td>


                    <?php if ($value->sendflag == 2) { ?>
                      <td class="text-center"> <span class="badge badge-success"><?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> Sent</span> </td>
                    <?php } else if($value->edstatus == 1){ ?>
                      <td  class="text-center">  <span class="badge badge-danger">Can`t <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> </span> </td>
                    <?php }else{ ?>
                      <td class="text-center">
                        <a class="btn btn-primary btn-xs m-t-10 " title="Send to Candidate" onclick="appiontment_document(<?php echo $value->staffid;?>)">Send</a>
                      </td>

                    <?php } ?>
                    <!-- <td class="text-center">
                      <?php if ($value->sendflag == 2) { ?>
 
                      <a class="btn btn-primary btn-xs m-t-10 " title="Send to Candidate" href="<?php echo site_url().'Hrddashboard/convert_executive/'.$value->staffid; ?>">Click</a>
                    <?php } ?>
                    </td> -->
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>
        <div class="panel thumbnail shadow-depth-2 listcontainer"  style="display:none;">
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Appointment letter to candidate </h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="row" style="background-color: #FFFFFF;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white;">
           <table id="InboxtableAppointment" class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
             <tr>
              <th  class="text-center" style ="max-width:50px;" >S. No.</th>
              <th>Category</th>
              <!-- <th>Campus Name</th> -->
              <th> Name</th>
              <th>Gender</th>
              <th> Email Id</th>
              <th> Team </th>
              <th> ED Comments</th>
              <th> Offer Letter </th>
              <th style ="max-width:100px;" class="text-center"> <?php if ($this->loginData->RoleID !=17) { echo "Offer Letter"; } else { echo "Offer of Appointment";} ?> </th>
              <th style ="max-width:100px;" class="text-center">convert To  Executive</th>
            </tr> 
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div> 
  </div>
</div>
</div>  

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <form method="post" action="<?php echo site_url().'Hrddashboard/sendofferlettertocandidate/'; ?>" enctype="multipart/form-data">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Other Documents</h4>
        </div>
        <div class="modal-body">
          <p>Select Documents.</p>
          <input type="hidden" name="token" id="token" value="">
          <input type="file" name="others_document[]" id="others_document"  accept=".doc, .docx, .txt,.pdf" multiple>
        </div>
        <div class="modal-footer">
          <button type="Submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div> 

<!--- Modal for appointment letter Others document -->
<div class="modal fade" id="myModalAppointment" role="dialog">
  <div class="modal-dialog">
    <form method="post" action="<?php echo site_url().'Hrddashboard/sendappointmentlettertocandidate/'; ?>" enctype="multipart/form-data">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
           <!--  <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">
            <p>Select Documents.</p>
            <input type="hidden" name="staffid" id="staffid" value="">
            <input type="file" name="appointment_others_document[]" id="appointment_others_document"  accept=".doc, .docx, .txt,.pdf" multiple>
          </div>
          <div class="modal-footer">
            <button type="Submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable();   
      $('#InboxtableAppointment').DataTable(); 
    });

    function others_document(candidateid){
      $('#token').val(candidateid);
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
    function appiontment_document(staffid){
      $('#staffid').val(staffid);
      $('#myModalAppointment').removeClass('fade');
      $("#myModalAppointment").show();
    }
  </script>  