<?php //print_r($d_tranfer);

  echo "hello";

?>



<section class="content">
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaverule" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                 <h4 class="header" class="field-wrapper required-field" style="color:green">Add Staff Transfer and Promotion</h4>
                 <div class="col-sm-12">
                   <form method="POST" action="">
                     
                     

                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Staff<span style="color: red;" >*</span></label>
                        <select class="form-control" name="staff" id="staff">
                        <option value="">Select The Staff </option>
                        <?php 
                        foreach($staff as $val)
{


                        
                        ?>



                        
                          <option value="<?php echo $val->staffid;?>"><?php echo $val->name;?></option>
                          <?php
                        }
                        ?>
                           </select>
                      </div>
                     
                    </div>
                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Present Office<span style="color: red;" >*</span></label>
                          <select class="form-control" name="Presentoffice" id="Presentoffice">
                          <option value=""></option>
                           </select>
                    </div>


                    
                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Present Designation<span style="color: red;" >*</span></label>
                       
                        <select class="form-control" name="Presentdesignation" id="Presentdesignation">
                          <option value=""></option>
                           </select>
                      </div>
                     
                    </div>



                     
                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">New Designation<span style="color: red;" >*</span></label>
                       
                        <select class="form-control" name="new_designation" id="new_designation">
                        <option value="" >Select The New Designation</option>
                        <?php foreach($new_designation as $val)
                        {
                          //print_r($val);
                          ?>
                          <option value="<?php echo $val->desid;?>"><?php echo $val->desname;?></option>
                          <?php 
                          } 
                          ?>
                           </select>
                      </div>
                     
                    </div>


                     
                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Transfered To<span style="color: red;" >*</span></label>
                       
                        <select class="form-control" name="transfer_office" id="transfer_office">
                          <option value="">Select Office</option>
                          <?php 
                          foreach($d_tranfer as $val)
                          {
                            ?>
                            <option value="<?php echo $val->officeid;?>" ><?php echo $val->officename;?> </option>
                            <?php
                          }
                          ?>
                           </select>
                      </div>
                     
                    </div>



                       <div class="form-group">
                      <div class="form-line">
                         <label for="StateNameEnglish" class="field-wrapper required-field">Date<span style="color: red;" >*</span></label>

                      <input type="text" class="form-control datepicker " data-toggle="tooltip" 
                              id="fromdate" name="fromdate" placeholder="From Date" style="min-width: 20%;" >
                      </div>
                      
                    </div>
                     <div class="form-group">
                      <div class="form-line">
                         <label for="StateNameEnglish" class="field-wrapper required-field">Remark<span style="color: red;" >*</span></label>

                      <input type="text" class="form-control"  data-toggle="tooltip" 
                              id="remark" name="remark" placeholder="Enter The Remark" style="min-width: 20%;" >
                      </div>
                      
                    </div>

                      <div style="text-align: -webkit-center;"> 
                           
                      <a href="<?php echo site_url("Staffpromotion/history");?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">View History</a> 
                      
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("Staffpromotion");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Go to List</a> 
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>


<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#staff").change(function(){
          var staff_id = $(this).val();
         // var new_designation=
          alert("staff="+staff_id);
          var data;
          data=staff_id;
          
           $.ajax({
          data:{staff_id:staff_id},
          url: '<?php echo site_url(); ?>Ajax/getstaff/',
          type: 'POST'
      
             })
      .done(function(data) {
        var obj=JSON.parse(data);
        console.log("office_name="+obj.office_name+"designation="+obj.designation+"office_id="+obj.office_id+"designation_id="+obj.designation_id);
        var dd=obj.office_name;
       
        var option='';
            option += `<option value="${obj.office_id}">${obj.office_name}</option>`;
            var option1='';
            option1 += `<option value="${obj.designation_id}">${obj.designation}</option>`;

       

$('#Presentoffice').html(option);
$('#Presentdesignation').html(option1);

        
      })

        });
      });


</script>