<?php //print_r($view_history);?>
<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leave" && $row->Action == "index"){ ?>

    <div class="container-fluid">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>


          <?php  //print_r($state_details); ?>
          <!-- Exportable Table -->
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">

               <div class="header">
                <h2>Manage Staff Transfer and Promotion History </h2><br>
            
              </div>
              <div class="body">
                
                <table id="staff_transfer_history" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.NO</th>
                      <th class="text-center">Date</th>
                      <th class="text-center">Present Office</th>
                      <th class="text-center">Staff</th>
                     <th class="text-center">Present Designation</th>
                      <th class="text-center">New Designation</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    
                   // print_r($staffcategory_details);
                  $i=0; foreach($view_history as $row){ 
                  

                    ?>
                    <tr>
                      <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $row->date_of_transfer; ?></td>
                      <td class="text-center"><?php echo $row->present_office; ?></td>
                      <td class="text-center"><?php echo $row->name; ?></td> 
                      <td class="text-center"><?php 
                      if($row->old_designation==$row->office_id)
                      {
                      echo  $row->desname;
                    }
                       ?></td> 
                      <td class="text-center"><?php 
                      if($row->new_designation==$row->office_id)
                      {
                      echo  $row->desname;
                    }
                       ?> </td> 
                               
                      
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>

          


          
        </div>
        <!-- #END# Exportable Table -->
      </div>
    <?php } } ?>
  </section>
  <script>
   $(document).ready(function() {
    $('#staff_transfer_history').DataTable({
      "paging": false,
      "search": false,
    });
  });
   function confirm_delete() {
    
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
      return true;
    } else {
      return false;
    }
    
  }
</script>