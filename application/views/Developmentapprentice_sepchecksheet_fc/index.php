<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
  

</style>

<form role="form" method="post" action="" name="formsepchecksheet" id="formsepchecksheet">
<section class="content" style="background-color: #FFFFFF;" >
<?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

    <br>
    <div class="container-fluid">
       <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="row">
         <div class="col-md-12 text-center">
          <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
        </div>


        <div class="col-md-12 text-center" style="margin-bottom: 20px;">
          <h4>CHECK SHEET</h4>
          <p>(for Processing Cases of Separation from PRADAN)</p>
        </div> 
      </div>
      
     
     

    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1.  Name of graduating Apprentice: <label class="inputborderbelow"> <?php echo $staff_detail->name; ?></label>
      </div>
      <div class="col-md-12">
       2. Apprentice Code : <label class="inputborderbelow"> <?php echo $staff_detail->emp_code; ?></label>
     </div>
     <div class="col-md-12">
      3.  Designation : <label class="inputborderbelow"> <?php echo $staff_detail->joindesig; ?></label>
    </div>
    <div class="col-md-12">
      4.  Date of joining Apprenticeship : <label class="inputborderbelow"> <?php echo $this->gmodel->changedatedbformate($staff_detail->joiningdate); ?></label>
    </div>
    <div class="col-md-12">
      5.  Location  : <label class="inputborderbelow"> <?php echo $staff_detail->address; ?></label>
    </div>
    <div class="col-md-12">
      6.  Date of graduation : <label class="inputborderbelow"> <?php echo $this->gmodel->changedatedbformate($staff_detail->joiningdate); ?></label>
    </div>
    <div class="col-md-12" style="display:none">
     6. Reasons for Separation  : <label type="text" name="reason_for_separation" class="inputborderbelow" value="<?php echo $staff_transaction->trans_status; ?>"  readonly />
   </div>
   <div class="col-md-12" style="font-size: 12px;display:none">
    <i>(Resignation/Retirement/Premature Retirement/Death/Name Struck Off/Dismissal, etc.)</i>
  </div>
  <div class="col-md-12" style="display:none">
   7. Date on Which Resignation Submitted (if applicable): <input type="text" name="date_resignation_submitted_7" id="date_resignation_submitted_7" value="" class="inputborderbelow datepicker"  />
 </div>
 <div class="col-md-12" style="display:none">
   8. Date from which Separation Takes Effect: <input type="text" name="date_from_which_separation_takes_effect_8" id="date_from_which_separation_takes_effect_8" class="inputborderbelow datepicker" value=""  />
 </div>

 <div class="col-md-12">
  7.  Whether any disciplinary action is pending against
the separating Apprentice?:
  <input type="radio" name="whether_under_probation_9" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->whether_under_probation_9 == 1) echo "checked"; } ?>  required> Yes 
  <input type="radio" name="whether_under_probation_9" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->whether_under_probation_9 == 0) echo "checked"; } ?>   required> No
</div>
<div class="col-md-12">
  10. Whether occupying PRADAN's owned/leased/
rented accommodation?:
 <input type="radio" name="if_not_under_probation_10" value="1"  <?php if($tbl_check_sheet){ if($tbl_check_sheet->if_not_under_probation_10 == 1) echo "checked"; } ?>  required> Yes
  <input type="radio" name="if_not_under_probation_10" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->if_not_under_probation_10 == 0) echo "checked"; } ?> required> No
</div>



<div class="col-md-6">
  <div class="card">
    <span class="text-center bg-light">11. Outstanding amounts on account of the following on date of graduation:</span>
    <table class="table" id="amount_recover">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        Vehicle Loan
      </td>
      <td><input class="form-control txtNumeric" name="Vehicle_Loan" id="Vehicle_Loan" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->Vehicle_Loan) {echo $tbl_check_sheet->Vehicle_Loan; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td>Stipend in lieu of excess leave</td>
    <td><input class="form-control txtNumeric" name="stipend_in_lieu_of_excess_leave" id="stipend_in_lieu_of_excess_leave" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->stipend_in_lieu_of_excess_leave) {echo $tbl_check_sheet->stipend_in_lieu_of_excess_leave; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required/></td>
  </tr>
  <tr>
   <td>3.</td>
   <td>Other loans and advances</td>
  <td><input class="form-control txtNumeric" name="other_loans_and_advances" id="other_loans_and_advances" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->other_loans_and_advances) {echo $tbl_check_sheet->other_loans_and_advances; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
</tr>
<tr>
 <td>4.</td>
 <td>Outstanding dues</td>
<td><input class="form-control txtNumeric" name="outstanding_dues" id="outstanding_dues" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->outstanding_dues) {echo $tbl_check_sheet->outstanding_dues; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
</tr>

</tbody>

</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-light">12. Amount due and payable to the separating APPRENTICE on account of: (as on date of release): </span>
   <table class="table" id="amount_dues">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        Stipend
      </td>
      <td><input class="form-control txtNumeric" name="stipend" id="amount7" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->stipend) {echo $tbl_check_sheet->stipend; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td>Personal Claim</td>
     <td><input class="form-control txtNumeric" name="personal_claim" id="amount8" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->personal_claim) {echo $tbl_check_sheet->personal_claim; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>3.</td>
     <td>Any Other</td>
     <td><input class="form-control txtNumeric" name="any_other" id="amount9" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->any_other) {echo $tbl_check_sheet->any_other; } else { echo '';}  ?>"  maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   
   
  
   
 </tbody>
</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-info">13. Summary </span>
   <table class="table">     
    <tbody>
     <tr>
       <td>(i)</td>
       <td> 
        Payments due to the apprentice
      </td>
      <td><input class="form-control txtNumeric" name="payments_due_to_employee" id="payments_due_to_employee" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->payments_due_to_employee) {echo $tbl_check_sheet->payments_due_to_employee; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>(ii)</td>
     <td>Amounts recoverable</td>
     <td><input class="form-control txtNumeric" name="amounts_recoverable" id="amounts_recoverable" value="<?php if($tbl_check_sheet) if($tbl_check_sheet->amounts_recoverable) {echo $tbl_check_sheet->amounts_recoverable; } else { echo '';}  ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>(iii)</td>
     <td>Net payable/recoverable</td>
     <td><input class="form-control txtNumeric" name="net_payable_recoverable" id="net_payable_recoverable" value="<?php if($tbl_check_sheet)  if($tbl_check_sheet->net_payable_recoverable){ echo $tbl_check_sheet->net_payable_recoverable; } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
  
 </tbody>

</table>
</div>
</div>


</div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
  <div class="col-md-12 text-left" >
  <span style="border-bottom:Solid 2px #000; ">Any other information/remarks relevant in this case:</span>
</div>
 <!-- <div class="col-md-4 text-left">
   <strong>Checked (Team Coordinator)</strong> <br>
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?></label> </p>
    <p>Place: _________________________________ </p>

 </div>
 <div class="col-md-4 text-left">
   <strong>Checked (HRD Unit)</strong> <br>
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?> </p>
    <p>Place: _________________________________ </p>

 </div> -->
<?php 
$tcshow = 0;
$fcshow = 0;
if($this->loginData->RoleID==16)
{
  $fcshow =1;
  $tcshow =1;
}
if($this->loginData->RoleID==2)
  $tcshow =1;
if($this->loginData->RoleID==20)
  $fcshow =1; 
  //print_r($curUserDetails);
  
 if($tcshow==1) {?>
    <div class="col-md-4 text-left">
     <strong>Checked (Team Coordinator)</strong> <br>
      <p>Signature:<?php 

if($supervisor_image->encrypted_signature)
{

    $file_path=''; 
                            $file_path=FCPATH.'datafiles/signature/'. $da_image->encrypted_signature;
                            
                            if(file_exists($file_path))
                            {
                          ?>
                     
                         <img src="<?php echo site_url().'datafiles/signature/'. $supervisor_image->encrypted_signature;?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
                         
                         <?php } 
                          else
                        {
                          ?>
                           <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
                           <?php 
                        }
}
else
{
  ?>
  <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
  <?php
}

    ?> </p>
      <p>Name: <label class="inputborderbelow"><?=(isset($tbl_check_sheet->superviser_name) && ($tbl_check_sheet->superviser_name!='') )? $tbl_check_sheet->superviser_name: $this->loginData->UserFirstName.' '.$this->loginData->UserLastName?></label> </p>
      <p>Date: <label class="inputborderbelow"> <?=(isset($tbl_check_sheet->financeapprovaldate) && ($tbl_check_sheet->financeapprovaldate!='0000-00-00') && ($tbl_check_sheet->financeapprovaldate !='') ) ? date('d/m/Y', strtotime($tbl_check_sheet->financeapprovaldate)): date('d/m/Y') ?></label> </p>
      <p>Place: <label class="inputborderbelow"><?=isset($tbl_check_sheet->superviser_office)  ? $tbl_check_sheet->superviser_office : $curUserDetails->location  ?></label> </p>
  
   </div>
  <?php } ?> 
  <?php if($fcshow==1) {?>
    <div class="col-md-4 text-left">
     <strong>Checked (Finance)</strong> <br>
      <p>Signature: <?php 

if($da_image->encrypted_signature)
{

    $file_path=''; 
                            $file_path=FCPATH.'datafiles/signature/'. $da_image->encrypted_signature;
                            
                            if(file_exists($file_path))
                            {
                          ?>
                     
                         <img src="<?php echo site_url().'datafiles/signature/'. $da_image->encrypted_signature;?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
                         
                         <?php } 
                          else
                        {
                          ?>
                           <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
                           <?php 
                        }
}
else
{
  ?>
  <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="50px" hight="140px" title="Profile Image" class="rounded-circle">
  <?php
}

    ?> </p>
      <p>Name: <label class="inputborderbelow"><?=(isset($tbl_check_sheet->finance_name) &&  ($tbl_check_sheet->finance_name!=''))? $tbl_check_sheet->finance_name: $this->loginData->UserFirstName.' '.$this->loginData->UserLastName?></label> </p>
      <p>Date: <label class="inputborderbelow"> <?=(isset($tbl_check_sheet->financeapprovaldate) &&($tbl_check_sheet->financeapprovaldate!='0000-00-00') && ($tbl_check_sheet->financeapprovaldate !='') )? date('d/m/Y', strtotime($tbl_check_sheet->financeapprovaldate)) : date('d/m/Y') ?></label> </p>
      <p>Place: <label class="inputborderbelow"><?=isset($tbl_check_sheet->finance_office) ? $tbl_check_sheet->finance_office: $curUserDetails->location ?></label> </p>
  
   </div>
  <?php }?> 
  <input type="hidden" name="curuserid" value="<?=$this->loginData->staffid?>">
    <input type="hidden" name="approvaldate" value="<?= date('d/m/Y'); ?>">


</div>

<div class="row text-left" style="margin-top: 20px; line-height: 3; display:none">
  <div class="col-md-2 text-right">
    Select HR :
  </div>
  <div class="col-md-4 text-left">
  <input type="hidden" name="hrid" value="<?=$tbl_check_sheet->hrid?>">
    <!-- <select class="form-control" name="hrid" id="hrid">
      <option value="">Select HR</option>
      <?php if($hrlist){
        foreach($hrlist as $res){
      ?>
      <option value="<?php echo $res->staffid; ?>" <?php if($selectedhr) if($selectedhr->receiver == $res->staffid) echo "selected"; ?>><?php echo $res->UserFirstName.' '.$res->UserLastName; ?></option>
      <?php
        }
      } ?>
    </select> -->
  </div>
</div>

</div>
<div class="panel-footer text-right">
<?php
if($this->loginData->RoleID==20 && (!isset($tbl_check_sheet->flag) || ($tbl_check_sheet->flag==0)))
{
?>
  <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
<?php 
}
if(($this->loginData->RoleID==2) && (!($tbl_check_sheet->superviserid > 0)))
{
?>
  <input type="submit" name="saveandsubmit" value="Approve" class="btn btn-success btn-sm">
<?php 
}   
?>
  <!-- <input type="button" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();"> -->
<?php if($this->loginData->RoleID==20){ ?>
 <a href="<?php echo site_url("Proposed_probation_separation_fc/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }elseif($this->loginData->RoleID==16){ ?>
  <a href="<?php echo site_url("Proposed_probation_separation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }elseif($this->loginData->RoleID==2){ ?>
 <a href="<?php echo site_url("Proposed_probation_separation_tc/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } ?>
</div>
</div>
</div>
</section>

</form>
<script type="text/javascript">
  
function acceptancereginationmodal(){
  //alert('dkfhsk');
  var comments = document.getElementById('comments').value;
  var acceept = document.getElementById('status').value;
  var personal_administration = document.getElementById('personal_administration').value;
  if(acceept == ''){
    $('#status').focus();
  }else if(acceept == 9 && personal_administration ==''){
    $('#personal_administration').focus();
  }
  if(comments.trim() == ''){
    $('#comments').focus();
  }


  if(comments.trim() !='' && acceept != '' && personal_administration !=''){
    document.forms['formsepchecksheet'].submit();
  }
}

  $(document).ready(function(){
    $('#amount_recover input[type="text"]').on('change', calculateDues);
    $('#amount_dues input[type="text"]').on('change', calculateDues);

    $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    //maxDate: 'today',
    yearRange: '1920:2030',
    dateFormat : 'dd/mm/yy',
    //defaultDate: new Date(2018, 00, 01)
    });
  });
  function calculateDues()
  {
    var amount_recover = 0;
    var amount_dues = 0;
    var finalpayment = 0;
    $('#amount_recover input[type="text"]').each(function(){
      if($(this).val()!='')
        amount_recover += parseFloat($(this).val());
    })
    $('#amount_dues input[type="text"]').each(function(){
      if($(this).val()!='')
        amount_dues += parseFloat($(this).val());
    })

    $('#payments_due_to_employee').val(amount_dues);
    $('#amounts_recoverable').val(amount_recover);
    finalpayment = amount_dues-amount_recover;
    $('#net_payable_recoverable').val(finalpayment);
    if(finalpayment < 0)
    {
      $('#net_payable_recoverable').css('border', '1px solid red');
    }
    else
    {
      $('#net_payable_recoverable').css('border', '1px solid green');
    }
  }
</script>
<?php  if($tbl_check_sheet){if($tbl_check_sheet->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
    
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  $("form select").prop("disabled", true);
  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");
  });

  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });



</script>
<?php }} ?>