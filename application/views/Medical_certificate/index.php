<section class="content" style="background-color: #FFFFFF;">
  <br/>
  <?php $arraydis = array('0'=>'small_pox',
    '1'=>'spitting_disease',
    '2'=>'fainting_attacks',
    '3'=>'appendicitis',
    '4'=>'lung_disease',
    '5'=>'epilepsy',
    '6'=>'insanity',
    '7'=>'major_surgery',
    '8'=>'any_physical_disability',
    '9'=>'nervousness_depression'
  );?>
  <?php $page='Appointment'; require_once(APPPATH.'views/Employee_particular_form/topbar.php');?>
  
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>  
    <div class="col-md-12">
      <div class="hpanel">
        <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('tr_msg');?>. </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('er_msg');?>. </div>
          </div>
        </div>
      <?php } ?>
      <div class="container-fluid" style="font-family: 'Oxygen' !important;">
        <div class="row text-center" style="padding: 14px;">      
          <div class="panel thumbnail shadow-depth-2 listcontainer">
            <div class="panel-heading">
              <div class="row">
                <h4 class="col-md-12 panel-title pull-left">Appointment For Medical Certificate 
                </h4>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">          
              <div class="row">
                <form method="POST" action="" enctype="multipart/form-data">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
                    <h6>Professional Assistance for Development Action (PRADAN) </h6>
                    <h5>MEDICAL CERTIFICATE FOR APPOINTMENT</h5>  <br/>
                    <h5>Part I - Statement by the Candidate</h5>
                  </div> 

                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                   1. Full Name:&nbsp;<b><?php echo $candidatedetailwithaddress->staff_name ;?>&nbsp;</b>
                   <br><br>2. Date of Birth :&nbsp;<b><?php if(!empty($candidatedetailwithaddress->dob)) echo $this->gmodel->changedatedbformate($candidatedetailwithaddress->dob) ;?>&nbsp;</b><p style="float: right;">Age:
                    <?php
                    if(!empty($candidatedetailwithaddress->dob))
                    {
                     $cur_year = date('Y');
                     $year=$candidatedetailwithaddress->dob;
                     $arr=explode('-', $year);
                     $currentyear=$arr[0];
                     $var=$cur_year-$currentyear;
                     echo '<b>'.$var.'</b>';
                   }
                   ?>
                 years</p>
                 <br><br>
               </div>                   
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left"> 3. Have you ever suffered from (please tick <i class="fa fa-check" aria-hidden="true"></i>
               ):</div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                YES/NO
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pull-right">
              </div>
              <ol type="I">
                <?php $i = 0; foreach($arraydis as $keys=>$val)
                {
                      //echo "key=".$i;
                     // print_r($keys);
                        // print_r($arraydis);
                      //print_r($arraydis->$keys);
                       // print_r($arraydis->{$keys});

                        // die;
                  ?>                
                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left text-left"><li><?php echo $val; ?> :</li></div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                      <input type="checkbox" class="form-check-input"  

                      value="1" name="<?php echo $val; ?>" id="<?php echo $val; ?>" >
                      <label class="form-check-label" for="smallyes1"></label>
                    </div>                  
                  </div>
                  <?php $i++; }
                  ?>
                </ol>
                <br><br>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left" style="margin-top:50px;">
                  If the answer to any of the above is Yes, please give details:
                  <div class="row">
                    <div class="col-sm-12">
                      <textarea style="width: 661px;" id="showthis" name="showthis" placeholder=" Please details here."  class = "form control" value="" cols="5" rows="3" required="required"></textarea></div></div>
                      <br>
                      I hereby declare that:
                      <br><br>
                      (a) All the above answers are true and correct to the best of my knowledge and belief; <br><br>
                      (b) I have not been declared medically unfit for service within the last three years;
                      (c) I know that by wilfully suppressing any information, I may incur the risk of losing my
                      appointment with PRADAN.
                      <br><br>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 text-left pull-right">
                       <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left pull-right">
                        Signature:

                      <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>

                        <!-- <?php if ($signature->encrypted_signature !='') { ?>
                         <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
                       <?php }else{ ?>
                         <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
                       <?php } ?> -->
                     </div>
                   </div>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-left text-left">
                   <p style="float:left;">Date:<input type="text" class="datepicker" 
                    value=""  name="doctorsigneddate" required="required"></p>
                  </div>

                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 text-left pull-right">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left pull-right">
                      Name:&nbsp;<b><?php echo $candidatedetailwithaddress->staff_name;  ?>&nbsp;</b><br/>
                      Date:<input type="text" value="" class="datepicker" name="candidatedate" required="required">
                    </div>
                  </div>


                   <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
                      <h6>Professional Assistance for Development Action (PRADAN) </h6>
                      <h5>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME</h5> 
                      <h5>Part II - Certificate by Examining Doctor</h5>
                    </div> -->

                    <!-- <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">                    
                      <b>1</b>. I hereby certify that I have physically examined Ms./Mr.&nbsp;<b><?php echo $rowsm->candidatefirstname; ?></b>&nbsp;<b><?php echo $candidate_list->candidatelastname; ?> </b>&nbsp;a candidate for employment with PRADAN, as
                      &nbsp;<b><?php echo $candidate_list->desname; ?></b>&nbsp;
                      (designation).
                      I have
                      taken into <br><br>account
                      her/his statements above and her/his Electrocardiogram, Chest X-ray, Blood
                      Examination (Hb%, TLC, DC, ESR), Stool Examination, Urine Analysis,<br><br> 
                      and report about her/his
                      vision (reports enclosed). I cannot identify any disease (communicable or otherwise), or
                      constitutional weakness except
                      <input type="text" name="txtnames" id="txtnames" placeholder=" Please Enter Name" value="<?php echo $fetch_datas->weakness_except; ?>"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" >
                      <br><br>
                      <b>2</b>. I do not/do consider this as a disqualification for employment in PRADAN.
                      <br><br>
                      Signature of Examining Doctor with seal:<br>
                      
                      <input type="hidden" name="docsig" value="<?php echo $fetch_datas->doctor_seal_signature;?>"/>
                    </div>   
                    <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 " style="text-align: justify;">
                      <input type="hidden" name="placename" value="<?php echo $candidatedetailwithaddress->new_office_id;?>">

                      Location:&nbsp;<input type="text" name="placename" style="border-radius: 0px; border: none; border-bottom: 1px solid black;" id="placename" value="<?php echo $candidatedetailwithaddress->officename; ?>" placeholder=" Please Enter Place Name"  required="required" >
                      <br>
                      Date:&nbsp;<input type="text" class="datepicker" style="border-radius: 0px; border: none; border-bottom: 1px solid black;" name="dateofexaminedoc" value="<?php 
                      $timestamp = strtotime($fetch_datas->certificatedate);
                      echo date('d/m/Y', $timestamp);?>" id="dateofexaminedoc" required="required">
                    </div>



                  -->

                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                  <div class="panel-footer  col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                    <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                    <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <style type="text/css">
    input[type=text] {
      background: transparent;
      border: none;
      border-bottom: 1px solid #000000;
    }
    hr{

      border-top: 1px solid black;
    }
  </style>




<!-- <script type="text/javascript">
 $(document).ready(function(){
  $('input[name="showthis"]').hide();

        //show it when the checkbox is clicked
        $('input[type="checkbox"]').on('click', function () {

          if ($(this).prop('checked')) {
            $('input[name="showthis"]').fadeIn();
          } else {
            $('input[name="showthis"]').hide();
          }
        });
      });
    </script> -->

    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  

        $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',

       });

       //$('input[type="checkbox"]').on('change', function() {
       // $(this).siblings('input[type="checkbox"]').not(this).prop('checked', true);
     });


 // $("input[type=checkbox][value=0]").prop("checked",true);​
})


});
$(document).ready(function(){
  $(".ibtn").click(function(){
    $(".ibtn").attr("checked", "checked");
  });
});
</script>  