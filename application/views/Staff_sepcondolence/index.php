f<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
</style>
<form method="POST" action="" name="sep_releaseform" id="sep_releaseform">
 <section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - CONDOLENCE SEPARATING EMPLOYEE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
                     
        </div>
        <div class="col-md-12 text-center">
           Mail should send to family member (mailid): <input type="text" onblur="validateEmail(this);" name="to_name" id="to_name" class="input-block-level" required="required"> 
           <br><br>
        </div>          
      </div>
      
  <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
       <input type="hidden" name="staffid" value="<?php //echo $staffid;?>">
     
      <div class="col-md-12">
                 <div class="">
                  <div class="">
                    
                    <!-- <textarea class="input-block-level" id="summernote" name="content" rows="18"> -->
                      <label for="" class="field-wrapper required-field"><?php echo $content; ?></label>
                      <!-- <textarea class='summernote' id='summernote' name="lettercontent" rows='10' ><?php //echo $content; ?></textarea> --><?php echo form_error("Content");?>

                    </div>
                    
                  </div>
                </div>


<hr/>
 
<?php 
$check = $this->session->userdata('login_data');
if($check->RoleID == 3){
?>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    cc: - Ms./Mr. <label class="inputborderbelow"> <?php echo $staff_detail->name;?></label> (separating employee through  <label class="inputborderbelow"><?php echo $staff_detail->supervisor;?></label> , Supervisor)
- Finance-Personnel-MIS Unit

  </div>
</div>
<?php }?>


</div>
<div class="panel-footer text-right">
  <input type="submit" name="Save" value="Send" class="btn btn-success btn-sm">
  </div>
  </div>
  </div>
 </section>

</form>


<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  // $("form input[type=text]").prop("disabled", true);
  });
</script>

<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
$(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
   });

  });


function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
            alert('Invalid Email Address');
            return false;
        }

        return true;

}
</script>