<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - RELIEVING SEPARATING EMPLOYEE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5>Professional Assistance for Development Action (PRADAN) </h5>           
        </div>
        <div class="col-md-12 text-center">
          <h4> LETTER FOR RELIEVING SEPARATING EMPLOYEE</h4> 
        </div>          
      </div>
      

      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         301-2117/PDR/________________ 
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         May 9, 2018
       </div>
     </div> 
     <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;"> 
        To 
      </div>
    </div>
    <div class="row" style="line-height: 2">
      <div class="col-md-12 text-left text-left"> 
        Name : _______________________ 
      </div>
      <div class="col-md-12 text-left text-left"> 
        Address : _______________________ 
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-left text-center" style="margin-top: 30px;"> 
       Subject  : <b>Release from Services of PRADAN</b>
     </div>
   </div>
   <div class="row">
    <div class="col-md-12 text-left text-left" style="margin-top: 30px;"> 
     Dear,
   </div>
 </div>
 <div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    Please refer to our letter No. _________________ dated _____________ accepting your resignation w.e.f. _______________ (AN). 
  </div> 
</div>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    Having complied with all our requirements regarding submission of ‘Clearance Certificate’ from all concerned, etc, you are being relieved from your post in PRADAN with effect from _____________ (date). All concerned are being informed accordingly.
  </div> 
</div>

<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    Thanking you for your services to PRADAN. 
  </div>
</div>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    Yours sincerely,
  </div>
</div>
<div class="row ">
 <div class="col-md-12 text-left" style="margin-top: 20px;">
   (___________________________)
 </div>
 <div class="col-md-12 text-left" style="margin-top: 20px;">
  Authorised Signatory
</div>
</div>
<hr/>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    cc: - Ms./Mr. ______________ (separating employee through  _____________ , Supervisor)
- Finance-Personnel-MIS Unit
  </div>
</div>

</div>
<div class="panel-footer text-right">
 
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>