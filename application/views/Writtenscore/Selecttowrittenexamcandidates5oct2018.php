<style type="text/css">
   #errmsg
{
color: red;
}
 </style>
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
      <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Written Test score</b></div>
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('trr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. <b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
          
        <div class="row" style="background-color: #FFFFFF;">
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
         <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right">   
         <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
        <div class=" col-md-6 col-lg-6 col-xs-12 col-sm-12" style="background-color: white;">  
          <select name="campusid" id="campusid" class="form-control", required="required">
          <option value="">Select Campus</option>
          <?php foreach ($campusdetails as $key => $value) {
              $expdatefrom = explode('-', $value->fromdate);
              $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
              $expdateto = explode('-', $value->todate);
              $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
               $campval  = $value->campusid.'-'. $value->id; 
              if ($campval == $campusid) {
           ?>
            <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
         <?php  }else{ ?>
          <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
        <?php } } ?>
          
        </select> 
        
      <?php echo form_error("campusid");?>
       </div>
        <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;">
          <button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-warning">Search</button>
        </div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;"><b></b></div>
       </form>
        <br>
        <br>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
       <!--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"><a href="#" class="btn btn-link" id="SendEmail" >Send Email Campus Incharge</a></div> -->
      
      
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
     
      <form name="formwrittenscore" id="formwrittenscore" method="post" action="" >
<input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>">          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="writtenscoreselectecandidate" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width:100%">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Candidate</th>
              <th> Email Id</th>
             
           </tr> 
         </thead>
         <tbody>
          <?php 
             $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
            <tr>
              <td><?php echo $i+1; ?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->emailid;?></td>
           
             <?php $i++; } ?>
            </tr>
          </tbody>
        </table>
       </div>
      
      </div> 
  
     </form>
     </div>
    </div>
  </div>   
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('#writtenscoreselectecandidate').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'print'
        ]
    } );
} );
</script>

<!--  JS Code Start Here  -->
<script type="text/javascript">



  
  //$(document).ready(function(){
    //$('[data-toggle="tooltip"]').tooltip();  
    //$('#tableWrittenScore').DataTable({
      // "bPaginate": true,
       //"bInfo": true,
       //"bFilter": true,
       //"bLengthChange": true
    //}); 
  //}); 


$("#SendEmail").on("click", function(){
    
   var campid =  $("#campusid").val();
     if(campid ==''){   ////Check campus id 

    alert('First !! Please Select campus !!!');

   }else{

 $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: "<?php echo base_url(); ?>Writtenscore/generate_pdf_selected_candidate/" + campid,
      
       async : false,
       cache : false,
       dataType : 'json',
       contentType : false,
       processData : false,
       success: function(data){
        console.log(data);
         if (data) {
          //location.reload();
          //alert("Data saved successfully");
         // $("#btnsubmit").prop('disabled', false);

        }else{
          alert("Data not saved successfully");
        }
      }

    });


   }

});

 

</script>

<!--  JS Code End Here  -->