<style type="text/css">
#errmsg
{
  color: red;
}
</style>
<section class="content" style="background-color: #FFFFFF;">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Writtenscore" && $row->Action == "index"){ ?>
    <br>
    <div class="container-fluid">
     <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Written Test score</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">  

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. <b></div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>

      <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">
        <div class="row" style="background-color: #FFFFFF;">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right">Campus Name</div>
          <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
            <select name="campusid" id="campusid" class="form-control", required="required">
              <option value="">Select Campus</option>
              <?php foreach ($campusdetails as $key => $value) {
                $expdatefrom = explode('-', $value->fromdate);
                $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                $expdateto = explode('-', $value->todate);
                $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                $campval  = $value->campusid.'-'. $value->id; 
                if ($campval == $campusid) {
                 ?>
                 <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
               <?php  }else{ ?>
                <option value="<?php echo $value->campusid.'-'. $value->id;?>" ><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
              <?php } } ?>
            </select> 
            <?php echo form_error("campusid");?>    
          </div>
          <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;">
            <button type="submit" name="search" id="search" value="SearchCampus" data-toggle="tooltip" title="Want to save your changes? Click on me." class="btn btn-dark">Search</button>
          </div>
        </div>
      </form>
    </div>
    <form name="formwrittenscore" id="formwrittenscore" method="post" action="">
     <div class="panel-body">
      <div class="row" style="background-color: #FFFFFF;">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right"><label >Cutoff Marks </label></div>
        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
          <input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>"> 
          <input type="text" name="cutoffmarks" id="cutoffmarks" style="text-align:right;" class="form-control isNumberKey" maxlength="2" minlength="1" placeholder="Marks " required="required" value="<?php if($campuscutoffmarks)echo  $campuscutoffmarks->campus_written_cutoffmarks;?>" >
          <?php echo form_error("cutoffmarks");?>
        </div>
        <div class="col-md-7 col-lg-7 col-xs-12 col-sm-12" style="background-color: white;"></div>
      </div>
      <br>
      <div class="row">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white;">
        <table id="tableWrittenScore" class="table table-bordered dt-responsive table-striped">
          <thead>
           <tr>
            <th class="text-center" style="max-width: 30px;" class="text-center">S. No.</th>
            <th>Category</th>
            <th>Campus Name</th>
            <th>Name</th>
            <th>Gender</th>
            <th> Email Id</th>
            <th>Stream</th>
            <th style="width: 100px;">Written Score</th>
          </tr> 
        </thead>
        <tbody>
          <?php 
          if(count($selectedcandidatedetails)==0){ ?>
            <tr>
              <td colspan="8" class="text-center" style="color: red;"> <b>No Records Found !!!</b></td>
            </tr>
          <?php  }else{
           $i=0; foreach ($selectedcandidatedetails as $key => $value) { 
            ?>
            <tr>
              <td class="text-center"><?php echo $i+1; ?></td>
              <td><?php echo $value->categoryname;?></td>
              <td><?php echo $value->campusname;?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->gender;?></td>
              <td><?php echo $value->emailid;?></td>
              <td><?php echo $value->stream;?></td>
              <td><input type="text" name="writtenscore[<?php echo $value->candidateid;?>]" id="writtenscore"   style="text-align:right;"
               value="<?php if(isset($value->writtenscore)){ echo $value->writtenscore; } ?>" class="form-control isNumberKey" maxlength="2" minlength="1" > </td>
               <?php $i++; } } ?>
             </tr>
           </tbody>
         </table>
       </div>
     </div>



   </div>
   <div class="panel-footer text-right">
     <?php if(count($selectedcandidatedetails) > 0){
       // echo "data=".count($selectedcandidatedetails);
      ?> 
      <button  type="submit" name="save_writtenscore" id="idsave_writtenscore"  class="btn btn-success " data-toggle="tooltip" title="Want to save your changes? Click on me." value="save">Save</button>
    <?php } ?>
  </div>

</form>

</div>
</div>  

<?php } } ?> 
</section>

<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tableWrittenScore').DataTable({
     "bPaginate": true,
     "bInfo": false,
     "bFilter": true,
     "bLengthChange": false
   }); 

    $("#cutoffmarks").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
             (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
             (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
               return;
             }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }
      });

     $(".isNumberKey").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
             (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
             (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
               return;
             }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }
      });




    
  });
</script>