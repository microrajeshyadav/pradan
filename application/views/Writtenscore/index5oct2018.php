<style type="text/css">
   #errmsg
{
color: red;
}
 </style>
 <section class="content" style="background-color: #FFFFFF;">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Writtenscore" && $row->Action == "index"){ ?>
    <br>
    <div class="container-fluid">
      <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Written Test score</b></div>
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. <b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
          <?php //echo "<pre>"; print_r($campusdetails); ?>
        <div class="row" style="background-color: #FFFFFF;">
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
         <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right" style="padding-top: 10px;">   
         <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
        <div class=" col-md-6 col-lg-6 col-xs-12 col-sm-12" style="background-color: white;"> 

      <select name="campusid" id="campusid" class="form-control", required="required">
          <option value="">Select Campus</option>
          <?php foreach ($campusdetails as $key => $value) {
              $expdatefrom = explode('-', $value->fromdate);
              $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
              $expdateto = explode('-', $value->todate);
              $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
               $campval  = $value->campusid.'-'. $value->id; 
              if ($campval == $campusid) {
           ?>
            <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
         <?php  }else{ ?>
        <option value="<?php echo $value->campusid.'-'. $value->id;?>" ><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
         <?php } } ?>
        </select> 
      <?php echo form_error("campusid");?>
       </div>
        <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;">
          <button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-warning">Search</button>
        </div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;"><b></b></div>
       </form>
        <br>
        <br>
       <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
      </div>
        
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
      </div>
      <br>
      <form name="formwrittenscore" id="formwrittenscore" method="post" action="" >

       <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
         <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 text-right" style="background-color: white; padding-top: 10px;"><label >Cutoff Marks </label>
         </div>

         <div class=" col-md-6 col-lg-6 col-xs-12 col-sm-12" style="background-color: white;"> 
         <input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>"> 
         <input type="text" name="cutoffmarks" id="cutoffmarks" class="form-control" placeholder="Enter Cutoff marks " required="required" value="<?php echo $campuscutoffmarks->campus_written_cutoffmarks;?>" >
      <?php echo form_error("cutoffmarks");?>
       </div>
        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12" style="background-color: white;">
        </div>
         <br> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tableWrittenScore" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>S.No.</th>
               <th>CandidateID</th>
              <th>Candidate</th>
              <th> Email Id</th>
              <th>Written Score</th>
           </tr> 
         </thead>
         <tbody>
          <?php 
           if(count($selectedcandidatedetails)==0){ ?>
          <tr>
            <td colspan="4" class="text-center" style="color: red;"> <b>No Records Found !!!</b></td>
          </tr>
           <?php  }else{
             $i=0; foreach ($selectedcandidatedetails as $key => $value) { 
              ?>
            <tr>
              <td><?php echo $i+1; ?></td>
               <td><?php echo $value->candidateid; ?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->emailid;?></td>
            <td><input type="text" name="writtenscore[<?php echo $value->candidateid;?>]" id="writtenscore"  required="required"
             value="<?php if(isset($value->writtenscore)){ echo $value->writtenscore; } ?>" class="form-control isNumberKey"> </td>
             <?php $i++; } } ?>
            </tr>
          </tbody>
        </table>
       </div>
       <br>
       <?php if(count($selectedcandidatedetails) > 0){ ?> 
       <div style="text-align: center;"> 
            <button  type="submit" name="save_writtenscore" id="idsave_writtenscore"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save" value="save">Save</button>
            <a href="<?php echo site_url("writtenscore");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
          </div>
        <?php } ?>
      </div> 
    
     </form>
     </div>
    </div>
  </div>  
  <?php } } ?> 
</section>

<!--  JS Code Start Here  -->
<script type="text/javascript">
  
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tableWrittenScore').DataTable({
       "bPaginate": false,
       "bInfo": false,
       "bFilter": true,
       "bLengthChange": false
    }); 
  });

$(document).ready(function () {
  //called when key is pressed in textbox
  $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});

</script>

<!--  JS Code End Here  -->