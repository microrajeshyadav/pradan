<link rel="stylesheet" type="text/css" href="<?php echo site_url('common/css/jquery.dataTables.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('common/css/buttons.dataTables.min.css');?>">
<script type="text/javascript" src="<?php echo site_url('common/js/jquery-3.3.1.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/dataTables.buttons.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.flash.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/jszip.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/pdfmake.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.html5.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/vfs_fonts.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.html5.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.print.min.js');?>"></script>

<style type="text/css">
#errmsg
{
  color: red;
}
</style>
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <br>
  <div class="container-fluid" style="margin-top: 20px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row clearfix doctoradvice">

        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-8 panel-title pull-left">Eligible candidates for written exam</h4>
             <div class="col-md-4 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
        <div class="panel-body">
          <?php $tr_msg= $this->session->flashdata('trr_msg');
          $er_msg= $this->session->flashdata('er_msg');
          if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
          
              <?php //echo "<pre>"; print_r($selectedcandidatedetails); die; ?>
            <div class="col-md-12 text-left">

               <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">
                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 text-right">   
                 <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
                 <div class=" col-md-3 col-lg-3 col-xs-12 col-sm-12" style="background-color: white;">  
                  <select name="campusid" id="campusid" class="form-control", required="required">
                    <option value="">Select Campus</option>
                    <?php foreach ($campusdetails as $key => $value) {
                      $expdatefrom = explode('-', $value->fromdate);
                      $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                      $expdateto = explode('-', $value->todate);
                      $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                      $campval  = $value->campusid.'-'. $value->id; 
                      if ($campval == $campusid) {
                       ?>
                       <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                       <?php  }else{ ?>
                       <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                       <?php } } ?>

                     </select> 

                     <?php echo form_error("campusid");?>
                   </div>
                   <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right" style="background-color: white;">
                    <button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-dark">Search</button>
                  </div>
                  <?php  

                      $expdata = '';
                     @ $expdata = explode('-', $campusid);
                      if ($expdata[0] !=0) {
                       
                    ?>
                  <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12" style="background-color: white;">
                     <button type="submit" class="btn btn-warning btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Emport & Send" name="btnsend" value="candidate_list" id="btnsend">Send to mail Campus-Incharger</button></div>
                   <?php }  ?>



                </form>
              </div>
            
                    <input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>"> 
                     <input type="hidden" name="campustype" id="" value="<?php if(isset($campustype)) echo $campustype;?>">         
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                      <table id="writtenscoreselectecandidate" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable" style="width:100%">
                        <thead>
                         <tr>
                        <th style ="max-width:30px;" class="text-center">S. No.</th>
                        <th>Category</th>
                        <th>Campus Name</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email Id</th>
                        <th>Mobile </th>
                        <th>Stream</th>
                        </tr> 
                      </thead>
                      <tbody>
                        <?php 
                        // print_r($selectedcandidatedetails); die();
                        $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
                        <tr>
                       <td class="text-center"><?php echo $i+1; ?></td>
                      <td><?php echo $value->categoryname;?></td>
                      <td><?php echo $value->campusname;?></td>
                      <td><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname;?></td>
                      <td><?php echo $value->gender;?></td>
                      <td><?php echo $value->emailid;?> </td>
                      <td><?php echo $value->mobile;?></td>
                      <td><?php echo $value->stream;?></td>
                      </tr>
                      <?php $i++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div> 

                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;">
            
               <!--  <button type="submit" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</button></div> -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              </div> 
    
        
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
$("#btnsend").prop("disabled",false);
  $(document).ready(function(){

 
    $('#writtenscoreselectecandidate').DataTable({
       dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'pdfHtml5',
             'print'
        ],
       "bPaginate": true,
       "bInfo": false,
       "bFilter": true,
       "bLengthChange": false
    }); 

   
    $('[data-toggle="tooltip"]').tooltip();  

      $(".fillaccept").click(function(){
      if ($(".fillaccept").is(':checked')) {
         $("#btnaccept").prop("disabled", false);
      }else{
        $("#btnaccept").prop("disabled", true);
      }
    });

    $(".fillreject").click(function(){
       if ($(".fillreject").is(':checked')) {
         $("#btnreject").prop("disabled", false);
      }else{
        $("#btnreject").prop("disabled", true);
      }
    });


  });
</script>
<!--  JS Code End Here  -->


