<style type="text/css">
  tr.group,
  tr.group:hover {
    background-color: #ddd !important;
  }

</style>

<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if 
    ($row->Controller == "Employeebasicsalary" && $row->Action == "index"){ ?>
      <br>
      <div class="container-fluid">   
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-12 panel-title pull-left">Employee Basic Increment</h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">

          <?php 
          $tr_msg= $this->session->flashdata('tr_msg');
          $er_msg= $this->session->flashdata('er_msg');

          if(!empty($tr_msg)){ ?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-success"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <form action="" method="post" >
                <div class="row bg-light" style="padding: 20px;">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px;">
                    <h5>Search Filters:</h5>
                  </div>
                  <div class="col-md-12">
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                      Office: 
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <select class="form-control" name="officeid" id="officeid" required="required">
                        <option value="0">-- All --</option>
                        <?php 
                        foreach($officelist as $val){?>
                          <option value="<?php echo $val->officeid;?>" <?php if($officeid == $val->officeid) echo "selected"; ?> ><?php echo $val->officename;?> </option>
                        <?php  } ?>
                      </select>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                     Increment Month:
                   </div>
                   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
                    <select id="incrementmonth" name="incrementmonth" class="form-control" required="">
                      <option value="<?php echo date('Y').'-04-01'; ?>" <?php if(date('Y').'-04-01' == $incrementmonth) echo "selected"; ?> > April</option>
                      <option value="<?php echo date('Y').'-10-01'; ?>" <?php if(date('Y').'-10-01' == $incrementmonth) echo "selected"; ?> > October</option>
                    </select>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">
                    <button title ="Click on me, to see the Staff list based on selected office." data-toggle='tooltip' type="submit" class="btn btn-info btn-sm" name="btnShow">
                      <i class="fa fa-search" aria-hidden="true"></i>
                    </button>                  
                  </div>
                </div>
              </div> 
            </form>

            <form id="basicform" action="" method="post" >


              <div class="row" style="background-color: #FFFFFF; margin-top:20px;">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white; ">
                  <input type="hidden" name="incrementmonthdate" id="incrementmonthdate">
                  <?php if($stafflist){ ?>


                    <input  type="checkbox" name="slde_incrmnt[]" id="slide" checked>
                    <label>Slide</label>


                    <input  type="checkbox" name="slde_incrmnt[]" id="increment" checked>
                    <label >Increament</label>
                    
                    <button type="submit" title ="Click here to proceed increments." data-toggle='tooltip' class="btn btn-success btn-sm pull-right ml-2" value="increment" name="btnincrement" id="btnincrement">Increment</button>

                  <?php } ?>
                  <!--  onclick="Increment();" -->
                </div>
                <label id="success_message" class="pull-left" style="color: green; margin:10px;"></label>
                <label style="color: red; margin:10px;" id="errormessage" class="pull-left"></label>
              </div>

              <div class="row" style="background-color: #FFFFFF; margin-top:20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablestaff" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
                    <thead>
                     <tr>
                      <th class="text-left" style="vertical-align: top; ">Stop</th>
                      <th class="text-left" style="vertical-align: top;">#</th>
                      <th class="text-center" style="vertical-align: top;">Code</th>
                      <th class="text-left" style="vertical-align: top;">Staff</th>
                      <th class="text-left" style="vertical-align: top;">Designation (Level)</th>

                      <th class="text-left" style="vertical-align: top;">Office</th>
                      <th class="text-center" style="vertical-align: top;">Date of Joining</th>
                      <th class="text-center" style="vertical-align: top;">Total Experience in PRADAN</th>
                      <th class="text-center" style="vertical-align: top;">LWP</th>
                      <th class="text-center" style="vertical-align: top;">Actual Experience</th>
                      <th class="text-left" style="vertical-align: top;">Old Basic</th>
                      <th class="text-left" style="vertical-align: top;">Slide</th>
                      <th class="text-left" style="vertical-align: top;">Increment</th>
                      <th class="text-left" style="vertical-align: top;">New Basic</th>
                      <!-- <th class="text-left" style="vertical-align: top;">Action -->
                        <!--                       <div id="savesalary"><input type="checkbox" id="checkAll"></div></th> -->

                      </tr> 
                    </thead>
                    <tbody>
                      <?php 
                      $i=0;
                      foreach ($stafflist as $key => $value) {
                       ?>
                       <tr>
                        <?php  if ($value->dlwp > 0 ) {
                         $check = 'checked="checked"';
                       }else{
                        $check = '';
                      } ?>
                      <td class="text-left"><input type="checkbox" name="<?php echo $value->staffid ?>" id="<?php echo  $value->staffid; ?>"  <?php echo $check; ?> disabled ="disabled" ></td>
                      <td class="text-left"><?php echo $i+1;?></td>

                      <td class="text-center"> <input type="hidden" name="txtstaffid[]"  id="txtstaffid_<?php echo $i; ?>" value="<?php echo $value->staffid; ?>"><?php echo $value->emp_code;?></td>
                      <td class="text-left">
                        <!-- <a href="<?php //echo base_url().'Leaveadjustment/adjustment/'.$value->staffid;?>"> -->
                          <a>
                            <?php echo $value->NAME;?> </a>                        
                          </td>
                          <td class="text-left"><?php echo $value->desname;?> <br/>(<?php echo $value->level;?>)</td>

                          <td class="text-left"><?php echo $value->office;?></td>
                          <td class="text-center"><?php echo $this->gmodel->changedatedbformate($value->doj);?>
                          <input type="hidden" name="dateofjoining[<?php echo $value->staffid; ?>]"  id="dateofjoining<?php echo $i; ?>" value="<?php echo $value->doj; ?>">
                        </td>
                        <td class="text-center"><?php echo $value->noofyearexp;?></td>
                        <td class="text-center"><?php echo $value->dlwp;?>

                        <input type="hidden" name="txtdlwp[<?php echo $value->staffid; ?>]"  id="txtdlwp_<?php echo $i; ?>" value="<?php echo $value->dlwp; ?>" >

                      </td>
                       <td class="text-center"><?php echo $value->act_exp;?></td>  
                      <td class="text-left">
                        <input type="text" name="basicsalary[]" id="basicsalary_<?php echo $value->staffid; ?>" class="form-control basicsalary text-right" maxlength="12" onblur="checkvalidation(this);" placeholder="">
                      </td>


                      <input type="hidden" name="netbasicsalary[]" id="netbasicsalary_<?php echo $value->staffid; ?>" class="form-control basicsalary text-right" maxlength="12" onblur="checkvalidation(this);"  placeholder="" readonly>
                      <td>

                        <input type="text" name="slide_[]" id="slide_<?php echo $value->staffid;?>" class="form-control slide text-right" maxlength="12" readonly>
                        <input type="hidden" name="hdnslide_[]" id="hdnslide_<?php echo $value->staffid;?>">

                      </td>  
                      <td>

                        <input type="text" name="increment_[]" id="increment_<?php echo $value->staffid;?>" class="form-control increment text-right" maxlength="12"  readonly>
                        <input type="hidden" name="hdnincrement_[]" id="hdnincrement_<?php echo $value->staffid;?>">

                      </td>
                      <td class="text-left">
                        <input type="text" name="Prebasicsalary[]" id="Prebasicsalary_<?php echo $value->staffid; ?>" class="form-control basicsalary text-right" maxlength="12" onblur="checkvalidation(this);"  placeholder="" readonly>
                      </td>

                    </tr>

                    <?php $i++; } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </form> 
        </div>

      </div>
    </div>   
  <?php } } ?>
</section>

<script>

  $("#slide").click(function () {

    $('.slide:input:text').each(function(){
      if ($(this).val() !='')
      {
        $(this).val('');
        var id = this.id;
         var hdnid = 'hdn'+id; 
         
        var netid = id.replace("slide","Prebasicsalary");
       
        $('#'+netid).val(parseInt($('#'+netid).val().replace(',','')) - parseInt($('#'+hdnid).val().replace(',','')));
      }
      else
      {

        var id = this.id;
        var hdnid = 'hdn'+id;       
        $(this).val($('#'+hdnid).val());
        var netid = id.replace("slide","Prebasicsalary");
       
        $('#'+netid).val(parseInt($('#'+netid).val().replace(',','')) + parseInt($('#'+hdnid).val().replace(',','')));

      }
    })

  });

  $("#increment").click(function (){

    $('.increment:input:text').each(function(){
      if ($(this).val() !='')
      {
        $(this).val('');
        var id = this.id;
         var hdnid = 'hdn'+id; 
         
        var netid = id.replace("increment","Prebasicsalary");
       
        $('#'+netid).val(parseInt($('#'+netid).val().replace(',','')) - parseInt($('#'+hdnid).val().replace(',','')));
      }
      else
      {

        var id = this.id;
        var hdnid = 'hdn'+id;       
        $(this).val($('#'+hdnid).val());
        var netid = id.replace("increment","Prebasicsalary");
       
        $('#'+netid).val(parseInt($('#'+netid).val().replace(',','')) + parseInt($('#'+hdnid).val().replace(',','')));

      }
    })

  });


  $(document).ready(function(){


    <?php 
    $i=0;
    foreach ($stafflist as $key => $value) {
     ?>
     document.getElementById("basicsalary_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->e_basic; ?>");


    // Previous Basic Salary

    document.getElementById("Prebasicsalary_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->pre_e_basic; ?>");

    //  slide 
    document.getElementById("slide_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->slide; ?>");

    document.getElementById("hdnslide_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->slide; ?>");

    // increment
    document.getElementById("increment_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->increment; ?>");
    document.getElementById("hdnincrement_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->slide; ?>");

    // netbasicsalary
    document.getElementById("netbasicsalary_<?php echo $value->staffid; ?>").value = intToNumberBudget("<?php echo $value->basicsalary; ?>");
    <?php $i++; }?>

    var incrementdmonth = $('#incrementmonth').val();

    if (incrementdmonth !='') {
      $("#incrementmonthdate").val(incrementdmonth);
    }

    $("#incrementmonth").change(function(){

      var incrementdmonth = $('#incrementmonth').val();

      if (incrementdmonth !='') {
        $("#incrementmonthdate").val(incrementdmonth);
      }
    });


    $('[data-toggle="tooltip"]').tooltip(

      );  
  });
  
  $(document).ready(function() {
    var groupColumn = 4;
    var table = $('#tablestaff').DataTable({ "paging": false});
    
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
        table.order( [ groupColumn, 'desc' ] ).draw();
      }
      else {
        table.order( [ groupColumn, 'asc' ] ).draw();
      }
    } );
  } );
</script>  

<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>

  function checkvalidation(res){
    var max = parseInt($(res).attr('id'));
    var x=$('#'+max).val();
    x=x.toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
     afterPoint = x.substring(x.indexOf('.'),x.length);
   x = Math.floor(x);
   x=x.toString();
   var lastThree = x.substring(x.length-3);
   var otherNumbers = x.substring(0,x.length-3);
   if(otherNumbers != '')
    lastThree = ',' + lastThree;
  var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
  document.getElementById(max).value = res;
}

// function basicsalaryupdate(staffid){
//   var basicsalary = $("#basicsalary_"+staffid).val();
//   $.ajax({
//     url: '<?php //echo site_url(); ?>Ajax/basicsalaryupdate/',
//     type: 'POST',
//     data:{'staffid':staffid, 'basicsalary':basicsalary},
//   })
//   .done(function(data){
//     document.getElementById('errormessage').innerHTML = '';
//         if(data == 'update'){
//           document.getElementById('success_message').innerHTML = 'Basic salary update successfully' ;
//         }else{
//           document.getElementById('success_message').innerHTML = 'Basic salary insert successfully';
//         }
//     console.log(data);
//    // alert(data);
//  })
// }

function Increment(){
  var incrementmonth = document.getElementById('incrementmonth').value;
  var period = incrementmonth.substring(0, incrementmonth.lastIndexOf("-") );
  $.ajax({
    url: '<?php echo site_url(); ?>Employeebasicsalary/incrementsalaryupdate/',
    type: 'POST',
    data:{'period':period},
  })
  .done(function(data){
    console.log(data);
    alert(data);
  })
}
</script>