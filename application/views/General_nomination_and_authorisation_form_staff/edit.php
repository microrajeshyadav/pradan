
<section class="content" style="background-color: #FFFFFF;">
  <br/>

  <!-- Exportable Table -->
  <?php $page='Nomination'; require_once(APPPATH.'views\candidate\components\Employee_particular_form/topbar.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">

     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">GENERAL  NOMINATION FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6>Professional Assistance for Development Action (PRADAN) </h6>              
          <h6>GENERAL  NOMINATION FORM</h6>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
         <div class="form-group">
          <div class="form-line">
            
           <p><?php echo $office_name->officename;?></p>
           
         </div>
       </div>
       <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
       PRADAN.</label></div>


     </div>
     <div class="form-group">
      <label for="StateNameEnglish" class="field-wrapper required-field">Sir, </label>
    </div>
    <div class="form-group">
      <label class="field-wrapper required-field">Subject: General Nomination and Authorization. </label>
    </div>



    <?php 
   // print_r($candidatedetailwithaddress);
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>    
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>      
      <?php } else if(!empty($er_msg)){?>       
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>       
        <?php } ?>

        <?php //print_r($candidatedetailwithaddress);?>
        <div class="row">                   
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">               
            <label class="field-wrapper required-field" />

            <div class="form-group">
              I,  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->staff_name;?>" required="required">   (name in full) whose particulars are given below, hereby nominate the following person(s) to receive, in the event of my death, all monies payable to me by Professional Assistance for Development Action (PRADAN) or its successors or assignees, other than the monies on those accounts in respect of which specific nominations exist or are required to exist under the rules governing the relevant matters, and want that the monies first aforesaid shall be paid in proportion indicated against the name(s) of the nominee(s).
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <table id="tblForm09" class="table table-responsive" >
            <thead>
             <tr class="bg-light">
              <th colspan="5"> Nominee Details  (You can add upto 4 rows in below table)  </th>
              <th colspan="2" class="text-right">
                <div class="form-inline">
                  Select no. of rows you want to add :  <select name="tablerow" id="tablerow" class="form-control" onchange="hideshow(this.value);">
                    <option value=""> select</option>
                    <option value="1" <?php if(count($nomineedetail)==1) {echo 'selected=selected';} ?>>1</option>
                    <option value="2" <?php if(count($nomineedetail)==2) {echo 'selected=selected';} ?>>2</option>
                    <option value="3" <?php if(count($nomineedetail)==3) {echo 'selected=selected';} ?>> 3</option>
                    <option value="4" <?php if(count($nomineedetail)==4) {echo 'selected=selected';} ?>> 4</option>
                    1
                  </select>


                </div>
              </th>
            </tr>  
            <tr>
             <th>Sr. no</th>
             <th>Name of the nominee in full</th>
             <th>Nominee's Relationship with the Apprentice</th>
             <th style="width: 150px;">Age of nominee</th>
             <th style="width: 250px;">Full address of the nominee</th>
             <th>Portion (Percentage of share of nominee) </th>
             <th style="width: 180px;">If the Nominee is a Minor, Name & Address of the Guardian who may Receive the Amount During the Minority of Nominee
             </th>
           </tr>
         </thead>
         <tbody>


          <?php $i=1; $j=''; $k=1;  /*for ($i=1; $i <=4; $i++) { */



            foreach ($nomineedetail as $key => $val) { if ($i <= count($nomineedetail)){ ?>

            <tr style="" class="<?php for($j=$i; $j<=4; $j++) { echo $j .' ';  } ?>">
              <?php } else  { ?>
              <tr style="display: none;" class="<?php for($j=$i; $j<=4; $j++) { echo $j .' ';  } ?>">

                <?php }?>

                <td class="nominee_detail"><?php echo $i; ?></td>

                <input type="hidden" value="<?php echo $val->nom_id;?>" name="data[<?php echo $i;?>][n_id]">
                <input type="hidden" value="<?php echo $val->nomination_id;?>" name="data[<?php echo $i;?>][nomination_id]">
                <input type="hidden" name="data[<?php echo $i;?>][sr_no]"value="<?php echo $val->sr_no;?>" class="form-control">

                <td><input type="text" name="data[<?php echo $i;?>][full_name_nominee]" id="data[<?php echo $i;?>][full_name_nominee]" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Full Name Nominee !" placeholder="Enter Family Member Name" value="<?php echo $val->nominee_name;?>"></td>
                <td>
                 <?php 
                 $options = array('' => 'Select Relation');
                 foreach($sysrelations as$key => $value) {
                  $options[$value->id] = $value->relationname;
                }
                echo form_dropdown('data['.$i.'][relationship_nominee]', $options,  $val->nominee_relation, 'class="form-control relationship_nominee" data-toggle="tooltip" title="Select Relation With Nominee !"  id="relationship_nominee" ');
                ?>
                <?php echo form_error("relationship_nominee");?>
              </td>

              <td>
                <input type="text" name="data[<?php echo $i;?>][age_nominee]" id="data[<?php echo $i;?>][age_nominee]" data-toggle="tooltip" title="Enter Age Of Nominee !" placeholder = "Enter Age Of Nominee " value="<?php echo $val->nominee_age; ?>" class="form-control age_nominee"  >
              </td>

              <td>
               <textarea name="data[<?php echo $i;?>][address_nominee]" data-toggle="tooltip"  title="Enter Full Address Nominee !" id="data[<?php echo $i;?>][address_nominee]"  placeholder = "Enter Full Address Nominee" class="form-control address_nominee">
                 <?php echo trim($val->nominee_address); ?>
               </textarea>
             </td>


             <td>
              <input type="text" name="data[<?php echo $i;?>][share_nominee]" id="data[<?php echo $i;?>][share_nominee]" data-toggle="tooltip" title="Enter Share Of Nominee !" placeholder = "Enter share Of Nominee " value="<?php echo $val->share_nomine;?>" class="form-control share_nominee"  >
            </td>

            <td>
              <select name="data[<?php echo $i;?>][minior]" id="data[<?php echo $i;?>][minior]" data-toggle="tooltip" placeholder = "Enter Minior" title="Enter Minior !" class="form-control minior"  >
               <option value="">Please select </option>
               <option value="1" <?php if($val->minior==1){?>selected <?php } ?>>Yes </option>
               <option value="0" <?php if($val->minior==0){?>selected <?php } ?>>No </option>
              </select>
            </td>
        </tr>
       <?php $i++; }?>
       <!-- <?php //echo $i; } ?> -->

       <?php $i=''; $j=''; for ($i=count($nomineedetail)+1; $i <=4; $i++) { 

        ?>

        <tr style="display: none;" class="<?php for($j=$i; $j<=4; $j++) { echo $j .' ';  } ?>">
          <td class="nominee_detail"><?php echo $i; ?></td>
                           <input type="hidden" value="<?php echo $val->nomination_id;?>" name="data[<?php echo $i;?>][nomination_id]">
                  <input type="hidden" value="<?php echo $val->pro_id;?>" name="data[<?php echo $i;?>][n_id]">
                   <input type="hidden" name="data[<?php echo $i;?>][sr_no]"value="<?php echo $val->sr_no;?>" class="form-control" data-toggle="tooltip">
                  <td><input type="text" name="data[<?php echo $i;?>][full_name_nominee]" id="full_name_nominee" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Full Name Nominee !" placeholder="Enter Family Member Name" required="required" value="<?php echo $val->name; ?>" ></td>

          <td>

           <?php 

           $options = array('' => 'Select Relation');
           foreach($sysrelations as$key => $value)
           {
            $options[$value->id] = $value->relationname;
          }
          echo form_dropdown('data['.$i.'][relationship_nominee]', $options, set_value('relationship_nominee'), 'class="form-control relation_name" data-toggle="tooltip" title="Select Relation With Nominee !"  id="relationship_nominee"');
          ?>
          <?php echo form_error("relationship_nominee");?>
        </td>

        <td>
          <input type="text" name="data[<?php echo $i;?>][age_nominee]" id="data[<?php echo $i;?>][age_nominee]" data-toggle="tooltip" title="Enter age of nominee !" placeholder = "Enter nominee age" value="" class="form-control age_nominee" >
        </td>

        <td>
         <textarea name="data[<?php echo $i;?>][address_nominee]" data-toggle="tooltip" maxlength="50" title="Enter full address of nominee !" id="data[<?php echo $i;?>][address_nominee]"  placeholder = "Enter nominee full address" class="form-control address_nominee"></textarea>
       </td>

       <td>
        <input type="text" name="data[<?php echo $i;?>][share_nominee]" id="data[<?php echo $i;?>][share_nominee]" data-toggle="tooltip" title="Enter share of nominee !" placeholder = "Enter share of nominee " value="" class="form-control share_nominee" >
      </td>

      <td>
        <select name="data[<?php echo $i;?>][minior]" id="data[<?php echo $i;?>][minior]" data-toggle="tooltip"  title="Select Minior !" class="form-control minior">
         <option value="">Please select </option>
         <option value="1">Yes </option>
         <option value="0">No </option>
       </select>
     </td>

      <!--  <td>
        <button class="btn btn-info btn-xs" type="button" onclick="add_nominee(this);">
          <i class="fa fa-plus"></i>
        </button>
        &nbsp;&nbsp;
        <button class="btn btn-danger btn-xs" type="button" onclick="delete_nominee(this);">
          <i class="fa fa-trash"></i>
        </button>
      </td> -->

    </tr>
    <?php }?>
    <?php $i++;?>


  </tbody>
</table>
</div>
</div>


 <div class="row" style="margin-bottom: 50px;">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <label class="field-wrapper required-field">  This nomination and authorisation shall remain valid unless substituted by a fresh nomination and authorisation.</label>
  </div>
</div>
<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-right"></div>
  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> <strong>Yours sincerely</strong></div>
</div>
<div class="row" style="line-height: 3">        
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >Location :</div>
   <input type="hidden" name="daplace" value="<?php echo $candidatedetailwithaddress->officename;?>">
   <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" > <input type="text" name="daplace" id="daplace" placeholder=" Please Enter Place"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->officename;?>" required="required">  </div>
 </div>
 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
 </div>
 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right">  
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Name: </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"> <b>  <?php echo $candidatedetailwithaddress->staff_name;?>  </b>
  </div>
</div>     
</div>
<div class="row" style="line-height: 3">    
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
  <div class="form-check">
    <label for="Name">Signature<span style="color: red;" >*</span></label>
     <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
   <!--  <?php if ($signature->encrypted_signature !='') { ?>
     <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
   <?php }else{ ?>
     <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
   <?php } ?> -->
 </div>
</div> 
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right">   
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Employee Code: </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <b><?php echo $candidatedetailwithaddress->emp_code;?> </b>
  </div>
</div>    
</div>


<div class="row" style="line-height: 3">    
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Date : </div>
   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text" name="dadate" id="dadate" class="form-control datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php  echo $this->model->changedate($nominee->da_date);?>" required="required">  </div>
 </div>
 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
 </div>
 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right"> 
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Father/Husband Name </div>
   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="father_name" name="father_name" id="dadate" class="form-control datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->fatherfirstname.' '.$candidatedetailwithaddress->fathermiddlename.' '.$candidatedetailwithaddress->fatherlastname;?>" required="required">  </div>   
   
 </div> 
</div>
<div class="row" style="line-height: 3">    
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   
   
 </div>
 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
 </div>
 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right"> 
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Permanent Address : </div>
   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <textarea name="paddress" id="paddress" class="form-control " placeholder=" Please select permanent address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" >  <?php echo $candidatedetailwithaddress->permanentstreet.' '.$candidatedetailwithaddress->permanentcity.' '.$candidatedetailwithaddress->permanentdistrict.' '.$candidatedetailwithaddress->name.', '.$candidatedetailwithaddress->permanentpincode; ?> </textarea></div>   
   
 </div> 
</div>




<!-- 
 <?php 
             if($this->loginData->RoleID==2 || $this->loginData->RoleID==17)
            {?> 

 
 <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: center;">
     <h5>Declaration By Witness<br>

     (Person others than by nominee)
      </h5>
         <div class="form-group">
              The above nomination has been signed by Ms./Mr.  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">   son/daughter/wife of Mr. <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">in our presence.
            </div>
     </div>  


 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Signature : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text"  class="form-control " placeholder=" Please select signature"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">&nbsp;  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right"> Signature</div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <input type="text"  class="form-control " placeholder=" Please select signature"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required"></div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Name : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text"  class="form-control " placeholder=" Please select name"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">&nbsp;  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right"> Name</div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <input type="text"  class="form-control name" placeholder=" Please select name "  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required"></div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Address : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <textarea   class="form-control " placeholder=" Please select address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;"  required="required">&nbsp;</textarea>  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right"> Address</div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <textarea    class="form-control " placeholder=" Please select address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;"  required="required">&nbsp;</textarea></div>
          </div>
          </div>
          


<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify; margin-left:20px;">
               
                cc-  Mr/Mrs  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"> <br>          -Fiance -Personel-Mis Unit <br>
                -Personal Dossier Location
              </div>

              <?php } ?> -->


              <div class="panel-footer text-right" style="width:100%; margin: 15px;">
                <!-- <button  type="submit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save" name="savebtn" id="save" value="senddatasave">Save</button> -->
                <button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>

                
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
  function hideshow(val)
  {

    $('#tblForm09 > tbody > tr').each(function(){
      $(this).hide();
    })
    $('#tblForm09 > tbody > tr > td >input').each(function(){
      $(this).val('');
    })

    $('#tblForm09 > tbody > tr > td > textarea').each(function(){ 
      $(this).html('');
    })

    $('#tblForm09 > tbody > tr > td >select').each(function(){      
      var id = $(this).id;
      $(this).val($("#"+id+" option:first").val());      
    })
    $('#tblForm09').find('.'+val).show();

     $('#tblForm09 > tbody > tr').each(function(){
      // if ($(this).attr('display') != 'none')
      // {
      //   $(this).find('input,select,textarea').each(function(){ 
      //    $(this).attr('required','required'); 
      //  }) 
      // }
    })
    

    
  }
  
</script>



    <script type="text/javascript">

      $(document).ready(function(){
        decimalData();
        $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

   //$('#pradan_selection_process_before_details').prop("disabled",true); 

 });

      $(function () {
        $("#selection_process_befor").click(function () {
          if ($(this).is(":checked")) {
            $("#pradan_selection_process_before_details").removeAttr("disabled");
          } else {
            $("#pradan_selection_process_before_details").prop("disabled", "disabled");
          }
        });
      });
      

      $(document).ready(function(){
        $("#filladdress").on("click", function(){
         if (this.checked) { 
          $("#permanentstreet").val($("#presentstreet").val());
          $("#permanentcity").val($("#presentcity").val());
          $("#permanentstateid").val($("#presentstateid").val()); 
          $("#permanentdistrict").val($("#presentdistrict").val());
          $("#permanentpincode").val($("#presentpincode").val()); 
          $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());
        }
        else {
          $("#permanentstreet").val('');
          $("#permanentcity").val('');
          $("#permanentstateid").val(''); 

          $("#permanentdistrict").val('');
          $("#permanentpincode").val('');
          $("#permanentstateid").val('');          
        }
      });

      });


      function decimalData(){
        $('.txtNumeric').keypress(function(event) {
         var $this = $(this);
         if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
          ((event.which < 48 || event.which > 57) &&
            (event.which != 0 && event.which != 8))) {
          event.preventDefault();
      }

      var text = $(this).val();
      if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
         if ($this.val().substring($this.val().indexOf('.')).length > 3) {
           $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
         }
       }, 1);
     }

     if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
       event.preventDefault();
   }      
 });

        $('.txtNumeric').bind("paste", function(e) {
          var text = e.originalEvent.clipboardData.getData('Text');
          if ($.isNumeric(text)) {
           if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
             e.preventDefault();
             $(this).val(text.substring(0, text.indexOf('.') + 3));
           }
         }
         else {
           e.preventDefault();
         }
       });
      }
    </script>

    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
        $("#btnsubmit").prop('disabled', true);

        $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',

       });


      });
    </script>  

    <script type="text/javascript">
  function hideshow(val)
  {

    $('#tblForm09 > tbody > tr').each(function(){

      $(this).hide();
    })
    $('#tblForm09 > tbody > tr > td >input').each(function(){
      $(this).val('');
    })

    $('#tblForm09 > tbody > tr > td > textarea').each(function(){
     
      $(this).html('');
    })

    $('#tblForm09 > tbody > tr > td >select').each(function(){      
      var id = $(this).id;
      $(this).val($("#"+id+" option:first").val());      
    })
    $('#tblForm09').find('.'+val).show();

     $('#tblForm09 > tbody > tr').each(function(){
      // if ($(this).attr('display') != 'none')
      // {
      //   $(this).find('input,select,textarea').each(function(){ 
      //    $(this).attr('required','required'); 
      //  }) 
      // }
    })
    

    
  }
  
</script>

