
<form method="post" action="" name="form_clearance" id="form_clearance">
  <section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - CLEARANCE CERTIFICATE  
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center" style="margin-bottom: 50px;">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p>
        <p><h4>CLEARANCE CERTIFICATE</h4></p>
        <p>(to be filled in duplicate)</p>
      </div>      

      
    </div>
    <div class="row" style="line-height: 2">
      <div class="col-md-12">
        Name :&nbsp;&nbsp;&nbsp;<input class="inputborderbelow text-center" readonly value="<?php echo $clearance_detail->name;?>" readonly>
      </div>
      <div class="col-md-12">
        Emp Code :&nbsp;&nbsp;&nbsp; <input class="inputborderbelow text-center" readonly value="<?php echo $clearance_detail->emp_code;?>" readonly>
      </div>     
      <div class="col-md-12">
        Designation : &nbsp;&nbsp;&nbsp;<input class="inputborderbelow text-center" readonly value="<?php echo $clearance_detail->desname;?>" readonly>
      </div>
     <!--  <div class="col-md-12">
        Location : &nbsp;&nbsp;&nbsp;<input class="inputborderbelow">
      </div>   --> 
    </div>
    
    
    <div class="row text-left" style="margin-top: 20px; ">
      <div class="col-md-12">
        1.  Date of Separation from PRADAN:&nbsp;&nbsp;&nbsp;<input class="inputborderbelow text-center" readonly value="<?php echo $this->gmodel->changedatedbformate($clearance_detail->seperatedate);?>" readonly>(AN)
      </div>
    </div>
    <div class="row text-left" style="margin-top: 20px; ">
      <div class="col-md-4">
       2. Separation due to:<input type="text" class="inputborderbelow" name="separation_due_to" value="<?php echo $clearance_detail->trans_status;?>" id="separation_due_to" readonly required>
     </div> 
     <div class="col-md-5 text-left" style="font-size: 12px;">
       <i><b>(Resignation/Retirement/Premature Retirement/Death/Name Struck Off/Dismissal/etc.)</b></i>
     </div>
   </div>
   <div class="row text-left" style="margin-top: 20px; ">
    <div class="col-md-12">

      CERTIFIED THAT there is nothing due from Ms./Mr.&nbsp;&nbsp;&nbsp;<input type="text" class="inputborderbelow text-center" name="due_from" value="<?php echo $clearance_detail->name;?>" id="" readonly>as on the date of her/his separation mentioned above, except the items shown in the respective columns below. There is no objection to release the above-named employee after adjusting the cost of items/articles outstanding against her/him and upon her/his completing the requisite formalities:

    </div>
  </div><br>
  


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

               <table id="tblForm10" class="table table-bordered table-striped">
                <thead>
                 <tr class="bg-light">
                  <th colspan="4"> Items

                    <div class="col-lg-6 text-right pull-right">
                      <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                      <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
                    </div>
                  </th>
                </tr> 
                <tr>
                 <th class="text-center" style="vertical-align: top;">Name of Project/Programme</th>
                 <th class="text-center" style="vertical-align: top;">Description/Details of Outstanding Items/Articles and their Value, if Known</th>
              </tr> 
            </thead>
            <tbody id="bodytblForm10">

              <tr id="bodytblForm10">
                <td><input type="text" class="form-control alphabateonly"  data-toggle="tooltip" title="Name of project !" minlength="5" maxlength="50"
                  id="name_of_location"  name="project[]" placeholder="Enter name of project" style="min-width: 20%;"  
                  value="<?php echo set_value('items');?>"  required></td>
                  <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5"   maxlength="150" title=" Description!" 
                    id="description" name="description[]"  placeholder="Enter Description" style="min-width: 20%;" value="" required></td>
                    
                      </tr>
                    </tbody>
                  </table>
                </div>

<div class="row text-left">
  <div class="col-md-12">
   The above outstanding dues have been adjusted and the employee may be released.
 </div>
</div>

<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
  Supervisor Name : <input type="text" readonly class="inputborderbelow" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;?>" name="">

  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    Signature of Supervisor: _____________________

  </div>
</div>
 
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   Name FinanceAdministrator: <input type="text" readonly class="inputborderbelow" value="" name="">

  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    Signature of FinanceAdministrator: _____________________

  </div>
</div>

<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
  cc: - Personal Dossier (PD)  <br>
- Finance-Personnel-MIS Unit <br>
 </div>
</div>
</div>
<div class="panel-footer text-right">
  <input type="button" name="btnsubmit" value="Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>

<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
           <label for="Name" class="pull-left">Notes <span style="color: red;" >*</span></label>
          <button type="button" class="close pull-right" onclick="hidemodal();">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
            <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes"  required> </textarea>
            <?php echo form_error("comments"); ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Status <span style="color: red;" >*</span></label>

                        <select name="status" id="status" class="form-control" required="required"> 
                          <option value="">Select</option>
                        <?php if($this->loginData->RoleID == 2|| $this->loginData->RoleID == 21){ ?>
                          <option value="15">Approved</option>
                          <option value="16">Rejected</option>
                        <?php } ?>
                        </select>
                        <?php echo form_error("status");?>
                      </div>
            <div id="executivedirector_admin" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none;" >
                      <label for="Name">Sent to Finance</label>
                    </div>
                       <!--  <select name="executivedirector_administration" id="executivedirector_administration" class="form-control" >
                          <option value="">Select</option>

                          <option value="<?php //echo $finance_detail->staffid; ?>">
                            <?php //echo $finance_detail->name;?>
                              
                            </option>
                         
                          </select>
                        <?php //echo form_error("personnel_administration");?>
                      </div> -->
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          
          <button type="button" class="btn btn-default btn-sm" onclick="hidemodal();">Close</button>
          <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancereginationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
<script type="text/javascript">
  $('#status').change(function(){
  var statusval = $('#status').val();
    /*alert('sdfsaf');
    alert(statusval);*/

        if(statusval == 15) {
            $('#executivedirector_admin').show(); 
            $('#executivedirector_administration').attr('disabled',false)
            $('#personnel_administration').prop('required', true); 

        } else {
            $('#executivedirector_admin').hide(); 
              $('#executivedirector_administration').attr('disabled',true)
            $('#executivedirector_administration').prop('required', false); 

        } 
    });
  function acceptanceregination(){
    var separation_due_to = document.getElementById('separation_due_to').value;
    if(separation_due_to == ''){
      $('#separation_due_to').focus();
    }else{
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
  function acceptancereginationmodal(){
    var comments = document.getElementById('comments').value;
    var acceept = document.getElementById('status').value;
    // var executivedirector_administration = document.getElementById('executivedirector_administration').value;
    if(acceept == ''){
      $('#status').focus();
    }

    if(acceept != ''){
      document.forms['form_clearance'].submit();
    }
  }
</script>

      <script type="text/javascript">
        $("#btntrainingexposureRemoveRow").click(function() {
          if($('#tblForm10 tr').length-2>1)
            $('#bodytblForm10 tr:last').remove()
        });

        $('#btntrainingexposureAddRow').click(function() {

          rowsEnter1 = parseInt(1);
          if (rowsEnter1 < 1) {
            alert("Row number must be minimum 1.")
            return;
          }
          Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });

        var srNoGlobal=0;
        var inctg = 0;

        function Insaettrainingexposure(count) {
          srNoGlobal = $('#bodytblForm10 tr').length+1;
          var tbody = $('#bodytblForm10');
          var lastRow = $('#bodytblForm10 tr:last');
          var cloneRow = null;

          for (i = 1; i <= count; i++) {
            inctg++
            cloneRow = lastRow.clone();
            var tableData1 = '<tr>'
            + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="50" required="required" id="project" name="project['+inctg+']" placeholder="Enter name of project " style="min-width: 20%;" value="" data-original-title="Name of Location !">'
            + '</td>'
            + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="description['+inctg+']" name="description['+inctg+']" placeholder="Enter Description" style="min-width: 20%;" value="" required="required" data-original-title="Items Description !"></td>'
            + '</tr>';
            $("#bodytblForm10").append(tableData1)

          }

        }

</script>