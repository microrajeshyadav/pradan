<section class="content">
  <?php //foreach ($role_permission as $row) { if ($row->Controller == "Pgeducation" && $row->Action == "index"){ ?>
 <br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


<?php  //print_r($state_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>MANAGE LEAVE HISTORY AND STATUS FOR STAFF</b>
             <div class="pull-right">
               <a href="<?php echo site_url("Applyforleave/applyforleaves/")?>" class="btn btn-primary btn-xs">Apply for leave</a>

                 <a href="<?php echo site_url("Applyforleave/monthlyleavedatafrezzing/")?>" class="btn btn-primary btn-xs">Monthly leave data freezing</a>
             </div>
           </div>
           <div class="panel-body">
             
                     
                <table id="tblleavehostory" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.No.</th>
                      <th >From Date</th>
                       <th >To Date</th>
                      <th >No. of Days</th>
                      <th >Leave Type</th>
                      <th >Reason</th>
                      <th >Date  of Apply</th>
                      <th class="text-center" style="width: 100px;">Status</th>
                      <th class="text-center">Supervisor Remarks</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php

                   // print_r($staffcategory_details);
                     $i=0; foreach($mstleavehostory_details as $row){ ?>
                    <tr>
                      <td class="text-"><?php echo $i+1; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                     <td class="text-center"><?php  if ($row->status==0) {
                      echo '<span class = "label label-success">Active</span>';
                       }else{
                         echo '<span class = "label label-danger">InActive</span>';
                       } ?></td>
                      
                      <td class="text-center">
                        
                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php //} } ?>
  </section>
<script>
  $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip(); 
            $('#tblleavehostory').DataTable({
              "paging": true,
              "search": true,
            });
          });
function confirm_delete() {
  
    var r = confirm("Do you want to delete this Post Graducation");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>