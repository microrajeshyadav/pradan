<section class="content">
  <?php //foreach ($role_permission as $row) { if ($row->Controller == "Pgeducation" && $row->Action == "index"){ ?>
   <br>
   <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>.

              </div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>


      <?php  //print_r($state_details); ?>
      <!-- Exportable Table -->
      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Monthly Leave Data Freezing/Unfreezing  </b>
             <div class="pull-right">
              <!--  <a href="<?php //echo site_url("Pgeducation/add/")?>" class="btn btn-primary btn-xs">Add New Post Graduation </a> -->
            </div>
          </div>
          <div class="panel-body">
           <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
             <div class="form-group">
               <form action="">
                <h5><label for="Office">Office<span style="color: red;" >*</span></label></h5>
                <select class="form-control" data-toggle="dropdown" id="staffid2" name="staffid2" required="">
                 <option value="">--select--</option>
               </select>
             </div>
           </div>

           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <h5><label for="From">Financial Year</label></h5>
              <select class="form-control" data-toggle="dropdown" id="staffid2" name="staffid2" required="">
               <option value="">--select--</option>
             </select>
           </div></div>

           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <h5><label for="To">Month</label></h5>
              <select class="form-control" data-toggle="dropdown" id="staffid2" name="staffid2" required="">
               <option value="">--select--</option>
             </select>
           </div>
         </div>
       </div>


       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
         <div class="row">
           <div class="form-group">
            <label>Freezing Date:<span style="color: red;" >*</span></label>
            <input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
            id="freezingdate" name="freezingdate" placeholder="From Date" style="min-width: 20%;">
          </div></div> </div>

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
          </div>

        </form>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <!--   <div id="table">
           -->

           <i href="" class="fa fa-plus" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Office A
          </i>


          <div class="collapse" id="collapseExample">
            <div class="card card-body">
             <table id="tbltransferprohistory" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
              <thead>


                <tr>

                  <th class="text-center" style="width: 50px;">S.No.</th>
                  <th>Employee</th>

                  <th>Leave Balance Avaliable</th>
                  <th style="text-align: center;" colspan="6">Leave Taken</th>
                  <th>Leave without pay</th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Leave</th>
                  <th>Special Leave</th>
                  <th>Sabibatical Leave </th>
                  <th>Study Leave</th>
                  <th>Paterninty Leave</th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>Table Data</td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>
        <div class="panel-footer text-right"> 

          <button  type="submit"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Freeze">Freeze</button>
          <a href="<?php echo site_url("");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Unfreezing" title="Close">Unfreezing</a> 
          <a href="<?php echo site_url("Applyforleave");?>" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Cancel">Cancel</a> 
        </div>

      </div>
    </div>
  </div>
</div>
<!-- #END# Exportable Table -->
</div>
</div>
<?php //} } ?>
</section>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 


            $('#tbltransferprohistory').DataTable({
              "paging": true,
              "search": true,
            });
          });


</script>





