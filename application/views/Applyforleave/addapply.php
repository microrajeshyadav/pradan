<section class="content">
 <?php //foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "add"){ ?>
 <div class="container-fluid">
	<!-- Exportable Table -->

	<?php 
	$tr_msg= $this->session->flashdata('tr_msg');
	$er_msg= $this->session->flashdata('er_msg');

	if(!empty($tr_msg)){ ?>
	<div class="content animate-panel">
		<div class="row">
			<div class="col-md-12">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			</div>
		</div>
		<?php } else if(!empty($er_msg)){?>
		<div class="content animate-panel">
			<div class="row">
				<div class="col-md-12">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="container-fluid" style="margin-top: 20px;">
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-sm-offset-4"></div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-sm-offset-4">

							
					<form method="POST" action="">
						<div class="panel panel-default" >
							<div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Leave Apply</b> 
							</div>
							<div class="panel-body">
								Leave Available:
								<br>
								 
									<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							 <div class="form-group">
									<h5><label for="To">From Date</label></h5>
									 <input type="text" id="fromdate" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
										 name="fromdate" placeholder="From Date" style="min-width: 20%;">
									</div></div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group">
									<h5><label for="To"> To Date</label></h5>
									 <input type="text"  class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
										id="todate" name="todate" placeholder="To Date" style="min-width: 20%;">
									</div>
										</div>
									</div>
								
							

											 <div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							 <div class="form-group">
									<h5><label for="To">Leave Type</label></h5>
									<!--  <input type="text" class="form-control " data-toggle="tooltip"  
										id="leavetype" name="leavetype" placeholder="Leave Type" style="min-width: 20%;"> -->
										  <select class="form-control" data-toggle="dropdown" id="types" name="types" required="">
				                           <option value="">--select--</option>
				                         <?php foreach($ltype_details as $row){?>

                             <option value="<?php echo $row->Ltypename;?>" <?php echo (trim($this->input->post('staffid')) == $row->staffid?"selected":"");?>><?php echo $row->Ltypename;?></option>

                           <?php } ?>
                         </select>

									</div>
								</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								 
									 <div class="form-group">
									<h5><label class="label-control" for="To">No of Days</label></h5>
									 <input type="text" class="form-control txtNumeric" data-toggle="tooltip"  
										id="Days" name="Days" placeholder="No of days" maxlength="10" style="min-width: 20%;">
									</div>
									</div>
									</div>

								<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									 <div class="form-group">
									<h5><label for="To">Reason</label></h5>
									

									<input type="text" class="form-control " data-toggle="tooltip"  
										id="Reason" name="Reason"  maxlength="50" placeholder="Reason" style="min-width: 20%;">
										</input> </div></div></div></div>



							<div class="panel-footer text-right"> 
							<button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Apply</button>
							<a href="<?php echo site_url("Applyforleave");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Cancel</a> 
					 
						 </div>
					</div>
				</form>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-sm-offset-4"></div>
		</div>
		<!-- #END# Exportable Table -->
	</div>
	<?php// } } ?>
</section>

<script type="text/javascript">
	 $(document).ready(function(){




  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
   }
 }, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});












     $("#fromdate").datepicker({
     changeMonth: true,
     changeYear: true,
     //maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
     // alert(selectedDate);
      jQuery("#todate").datepicker( "option", "minDate", selectedDate );
        // jQuery("#work_experience_todate_2").datepicker( "option", "minDate", selectedDate );
      }
    });

   $("#todate").datepicker({
     changeMonth: true,
     changeYear: true,
     //maxDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      //alert(selectedDate);
      jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
      }
    });

});
</script>