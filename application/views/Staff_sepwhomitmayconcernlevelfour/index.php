<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
  

</style>
<!-- <?php
print_r($level_four);
?> -->
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE FOR (Level 4) 
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5> EXPERIENCE CERTIFICATE FOR (Level 4) </h5> 

        </div>
        <div class="col-md-12 text-center">
          <h4>TO WHOM IT MAY CONCERN</h4> 
        </div>          
      </div>
        <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
     <form method="POST" action="" name="sep_levelfour" id="sep_levelfour">
       <input type="hidden" name="sepdesigid" value="<?php echo $level_four->sepdesigid;?>">

      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         301-<?php echo $level_four->emp_code;?>/PDR/<input type="text" name="sep_no" maxlength="50" value="<?php if($levelfour_detail){ echo $levelfour_detail->sep_no;}?>" class="inputborderbelow" required> 
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         May 9, 2018
       </div>
    </div> 
   
   
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    Ms. <?php echo $level_four->name;?> was employed by this organization as Executive from <label class="inputborderbelow"><?php echo $this->gmodel->changedatedbformate($level_four->joiningdate);?></label> to <label class="inputborderbelow"><?php echo $this->gmodel->changedatedbformate($level_four->seperatedate);?></label> Prior to that she participated in our in-house training programme for Development Apprentices for 12 months in Raigarh, North and South Chhattisgarh Development Cluster. This programme enables young professionals to develop the knowledge, skills and attitude necessary for a career in rural development.
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
    PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has <input type="text" name="total_staff" class="inputborderbelow txtNumeric" value="<?php if($levelfour_detail){ echo $levelfour_detail->total_staff;}?>"> staff, including <input type="text" name="total_professional" value="<?php if($levelfour_detail){ echo $levelfour_detail->total_professional;}?>" class="inputborderbelow txtNumeric"> professionals. It works in seven of the poorest States in India. Ms. <?php echo $level_four->name;?> was part of a team of professionals implementing various development programme in Mohagaon of Mahakausal Development Cluster, Madhya Pradesh.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
   Ms. <?php echo $level_four->name;?> has amply demonstrated her ability to work under difficult conditions in remote villages. She displays empathy and maturity in working with poor people. Ms. <?php echo $level_four->name;?> was able to use her technical knowledge and skills to help poor people improve their livelihoods.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
   I wish her very best in her life and career.
   </div>   
 </div>
 <hr/>
 <div class="row ">
   <div class="col-md-12 text-right" style="margin-top: 20px;">
   (___________________________)
   </div>
   <div class="col-md-12 text-right" style="margin-top: 20px;">
  Executive Director
   </div>

 </div>

</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($levelfour_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
  <?php
    }else{

      if($levelfour_detail->flag == 0){
 ?>
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
  <!-- <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
<?php } ?>
 
<?php } ?>
 <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</form>
</div>
</div>
</section>
<?php 
if ($levelfour_detail){
if($levelfour_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);

  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");
  });
  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
</script>
<?php }} ?>
