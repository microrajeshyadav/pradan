<?php //echo "string";exit(); ?>

<section class="content" style="background-color: #FFFFFF;" >
  <?php  //foreach ($role_permission as $row) { if ($row->Controller == "Hrstaff_approval" && $row->Action == "index"){ ?>
    <br>

    <div class="container-fluid">
     <?php 
     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');

     if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
         <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>


  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Transfer  Approval List</h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
       
              <div class="row" style="background-color: #FFFFFF; overflow-x:auto;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tabletransfer" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>Emp code</th>
                       <th>Name</th>
                       <th>Old Office</th>
                       <th>Proposed Transfer Office</th>
                       <th>Proposed Transfer Date</th>
                       <th>Old Designation</th>
                       <th>New Designation</th>
                       <th>Reason</th>
                       <th>Comments</th>
                       <th>Sender Name</th>
                       <th>Request Status</th>
                       <!-- <th>Request Date</th> -->
                      <!--  <th>Status</th> -->
                       <th style ="max-width:70px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                <?php if (count($gettransferworkflowdetail) ==0) { ?>

               <?php  }else{  ?>


                   <tbody>
                    <?php 
                    $i=0;
                    foreach ($gettransferworkflowdetail as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->emp_code;?></td>
                      <td><?php echo $value->name;?></td>
                      <td><?php echo $value->officename;?></td>
                      <td><?php echo $value->newoffice;?> </td>
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?></td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td><?php echo $value->flag; ?></td>
                      <!-- <td><?php echo $value->Requestdate;?></td>
                      <td><?php echo $value->process_type ;?></td> -->
                      <td class="text-center">
                        <a class="btn btn-success btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."TCRStaff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                      <?php if ($value->status==3 || $value->status==5) { ?>
                        <a class="btn btn-warning btn-sm" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a><br>
                        <a class="btn btn-warning btn-sm" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"> Change Responsibility </a>
                    <?php } ?>
                    <?php if ($value->status==7) { ?>
                      
                     <a title = "Click here to approve this request." class="btn btn-primary btn-sm" onclick="approvemodalshow(<?php echo $value->transid; ?>);"><i class="fas fa-thumbs-up"></i></a>
                    <?php } ?>
                    <?php if ($value->status==7) { ?>
                        <a class="btn btn-warning btn-sm" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> Clearance Certificate on Transfer</a>
                      <?php } ?>
                      <?php if ($value->status==4 || $value->status==6) { ?>
                       <!--href="<?php echo site_url()."Stafftransferpromotion/approve/".$value->staffid.'/'.$value->transid;?>"data-toggle="modal" data-target="#myModal" <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>">Approval</a> -->
                      <?php } ?>
                       
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                <?php } ?>
                </table>
              </div>
            </div>
            </div> 
          </div>
        

        <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Change Responsibility Approval List</h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
        
              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tableCResponsibility" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>Emp code</th>
                       <th>Name</th>
                       <th>Old Office</th>
                       <!-- <th>Proposed Transfer Office</th> -->
                       <th>Proposed Transfer Date</th>
                       <th>Old Designation</th>
                       <th>New Designation</th>
                       <th>Reason</th>
                       <th>Comments</th>
                       <th>Sender Name</th>
                       <th>Request Status</th>
                       <th>Request Date</th>
                       <!-- <th>Status</th> -->
                       <th style ="max-width:50px;" class="text-left">Action</th>
                     </tr> 
                   </thead>
                <?php if (count($getpromotionworkflowdetail) ==0) { ?>

               <?php  }else{  ?>


                   <tbody>
                    <?php 
                    $i=0;
                    foreach ($getpromotionworkflowdetail as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->emp_code;?></td>
                      <td><?php echo $value->name;?></td>
                      <td><?php echo $value->officename;?></td>
                      <!-- <td><?php echo $value->newoffice;?> </td> -->
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?></td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                     <td><?php echo $value->flag; ?></td> 
                      <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                      <!-- <td><?php echo $value->process_type ;?></td> -->
                      <td class="text-left">
                        <a class="btn btn-success btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                            <?php if ($value->status==5) { ?>
                          <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Change_responsibility_encourage/index/".$value->transid;?>">Inter-Office Memo</a>
                      <?php } ?>
                         <?php if ($value->status==3 || $value->status==6) { ?>
                         <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Change_responsibility_encourage/index/".$value->transid;?>">Change Responsibility</a>

                        <?php } ?>
                       
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                <?php } ?>
                </table>
              </div>
            </div>
            </div> 
          </div>

       
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Transfer & Change Responsibility Approval List</h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
       
              <div class="row" style="background-color: #FFFFFF; overflow-x:auto;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tableTCResponsibility" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>Emp code</th>
                       <th>Name</th>
                       <th>Old Office</th>
                       <th>Proposed Transfer Office</th>
                       <th>Proposed Transfer Date</th>
                       <th>Old Designation</th>
                       <th>New Designation</th>
                       <th>Reason</th>
                       <th>Comments</th>
                       <th>Sender Name</th>
                       <th>Request Status</th>
                       <!-- <th>Request Date</th> -->
                      <!--  <th>Status</th> -->
                       <th style ="max-width:70px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                <?php if (count($gettransferworkflowdetail) ==0) { ?>

               <?php  }else{  ?>


                   <tbody>
                    <?php 
                    $i=0;
                    foreach ($gettransferworkflowdetail as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->emp_code;?></td>
                      <td><?php echo $value->name;?></td>
                      <td><?php echo $value->officename;?></td>
                      <td><?php echo $value->newoffice;?> </td>
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?></td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td><?php echo $value->flag; ?></td>
                      <!-- <td><?php echo $value->Requestdate;?></td>
                      <td><?php echo $value->process_type ;?></td> -->
                      <td class="text-center">
                        <a class="btn btn-success btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."TCRStaff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                      <?php if ($value->status==3 || $value->status==5) { ?>
                        <a class="btn btn-warning btn-sm" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a><br>
                        <a class="btn btn-warning btn-sm" title = "Click here to chnage responsibility on submitted." href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"> Change Responsibility </a>
                    <?php } ?>
                    <?php if ($value->status==7) { ?>
                      
                     <a title = "Click here to approve this request submitted." class="btn btn-primary btn-sm" onclick="approvemodalshow(<?php echo $value->transid; ?>);"><i class="fas fa-thumbs-up"></i></a>
                    <?php } ?>
                    <?php if ($value->status==7) { ?>
                        <a class="btn btn-warning btn-sm" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Staff_approval/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> Clearance Certificate on Transfer</a>
                      <?php } ?>
                      <?php if ($value->status==4 || $value->status==6) { ?>
                       <!--href="<?php echo site_url()."Stafftransferpromotion/approve/".$value->staffid.'/'.$value->transid;?>"data-toggle="modal" data-target="#myModal" <a class="btn btn-primary btn-sm" title = "Click here to approve this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>">Approval</a> -->
                      <?php } ?>
                       
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                <?php } ?>
                </table>
              </div>
            </div>
            </div> 
          </div>
            

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-7 panel-title pull-left">Seperation Approval List</h4>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
                <table id="tableSeperation" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.NO</th>
                      <th class="text-center">Seperate Date</th>
                      <th class="text-center">Staff</th>
                      <th class="text-center">Type</th>
                      <th class="text-center">Sender</th>
                      <th class="text-center">Recevier</th>
                      <th class="text-center">Request Date</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Comment</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <?php if (count($staff_seperation)==0) { ?>

                <?php }else{ ?>
                  <tbody>
                    <?php
                    $i=0; foreach($staff_seperation as $grow){ 
                
                      ?>
                      <tr>
                        <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                        <td class="text-center"><?php echo $this->gmodel->changedatedbformate($grow->proposeddate);?></td>
                        <td class="text-center"><?php echo $grow->name; ?></td>
                        <td class="text-center"><?php echo $grow->process_type; ?></td> 
                        <td class="text-center"><?php echo $grow->sendername; ?></td> 
                        <td class="text-center"><?php echo $grow->receivername; ?></td> 
                        <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($grow->Requestdate); ?> </td> 
                        <td class="text-center"><span class="badge badge-pill badge-info" ><?php echo $grow->flag;?> </span></td> 
                         <td class="text-center"><?php echo $grow->scomments;?> </td> 
                        <td class="text-center">
                          <?php if($grow->status == 3){ ?>
                          <a href="<?php echo base_url('Staff_approval/add/'.$grow->staffid.'/'.$grow->transid);?>" class="btn btn-success btn-sm">Approve</a>
                          <?php 
                            }else if($grow->status == 21){
                          echo "<span class='badge badge-pill badge-success'> Approved </span>";
                       }else if($grow->status == 22){
                       echo "<span class='badge badge-pill badge-success'> Rejected </span>";
                       } $i++; } ?>
                    </tbody>
                  <?php  } ?>
                  </table>
                </div>
            </div>

     
</section>
   <!-- Modal -->
  <div class="container">
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <form action="<?php echo site_url(); ?>Staff_approval/approvemodal" id="acceptancetcform" name="acceptancetcform" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <input type="hidden" id="receiverstaffid" name="receiverstaffid" value="">
               <label for="Name">Notes <span style="color: red;" >*</span></label>
               <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments"  required> </textarea>
                <?php echo form_error("comments");?>
              </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetcmodal();">
          </div>
        </div>
       </form> 
      </div>
    </div>
  </div> 
  <!-- Modal -->

   <!-- Modal Transfer Expense CLAIM-->
  <div class="container">
    <div class="modal fade" id="myModaltransfer" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <form action="<?php echo site_url(); ?>Staff_approval/transferapprovemodal" id="acceptancetcform_transfer" name="acceptancetcform_transfer" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" id="receiverstaffid_transfer" name="receiverstaffid_transfer" value="">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments_transfer" name="comments_transfer" placeholder="Enter Comments"  required> </textarea>
              <?php echo form_error("comments_transfer");?>
            </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetransfermodal();">
          </div>
        </div>
       </form> 
      </div>
    </div>
  </div> 
  <!-- Modal -->

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tabletransfer').DataTable();
    $('#tableCResponsibility').DataTable();
    $('#tableTCResponsibility').DataTable();
    $('#tableSeperation ').DataTable(); 
  });
  function approvemodalshow(staffid){
    document.getElementById("receiverstaffid").value = staffid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
  function approvemodalshowtransfer(staffid){
    document.getElementById("receiverstaffid_transfer").value = staffid;
    $('#myModaltransfer').removeClass('fade');
    $("#myModaltransfer").show();
  }
      function acceptancetcmodal(){
        var comments = document.getElementById('comments').value;
        if(comments.trim() == ''){
          $('#comments').focus();
        }
        if(comments.trim() !=''){
          document.forms['acceptancetcform'].submit();
        }
      }

      function acceptancetransfermodal(){
        var comments = document.getElementById('comments_transfer').value;
        if(comments.trim() == ''){
          $('#comments_transfer').focus();
        }
        if(comments.trim() !=''){
          document.forms['acceptancetcform_transfer'].submit();
        }
      }
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>