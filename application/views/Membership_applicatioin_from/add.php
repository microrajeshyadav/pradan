<section class="content" style="background-color: #FFFFFF;">
  <br/>
  <?php $page='Declaration'; require_once(APPPATH.'views/candidate\components\Employee_particular_form/topbar.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">

     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6>Professional Assistance for Development Action (PRADAN) </h6>
          <h6>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME</h6>
          <h6>DECLARATION ABOUT MEMBERS OF FAMILY</h6>
        </div>
      </div>
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
           <div class="form-group">
            <div class="form-line">
              <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
              PRADAN.</label>
              <br>
              <?php echo $officename_list->officename; ?>
              <?php
            // foreach($newfetch as $uu){
                       //  print_r($newfetch);
              ?>
              <?php
             //  foreach($members as $mem){
                        //print_r($members);
              ?>
            </div>
            <?php echo form_error("executive_director");?>
          </div>
        </div>
        <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        </div>
        <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        </div>
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px; ">
         <div class="form-group">
          <label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir / Madam, </label>
        </div>
      </div>
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify; line-height: 2">

       I have enrolled myself as a member of PRADAN’s Employees Contributory Welfare Scheme (ECWS) and hereby declare that the following members of my family* (including myself) shall be the beneficiaries under this Scheme:.

       <br>
       <p>* Family means:
        <br>- employee her/himself<br>- spouse;<br>- minor, natural or adopted child, dependent upon the employee;<br>- child who has no income of her/his own and:<br>(i) is less than 21 years of age<br>(ii) unmarried dependent daughter(s);<br>- child who is infirm by reason of any physical and/or mental abnormality or injury, as certified by a competent medical authority, and is wholly dependent on the employee, as long as the infirmity continues;<br>- dependent parents.
      </p>

      Reimbursement of medical expenses, as above, will be available to the employee her/himself, spouse and the dependent children as defined above. However, if the total number of family members (self, spouse and children) is less than four, the employee will be permitted to add her/his parent(s) as beneficiary(ies) so that the total number of members of  the family (including self) does not exceed four.      
    </div>
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblForm09" class="table" >
          <thead>
           <tr class="bg-light">
            <th colspan="7" ><b> Family Details </b></th>
          </tr> 
          <tr>
           <th>Sr No.</th>
           <th>Name</th>
           <th>Date of Birth</th>
           <th>Age (in years)</th>
           <th>Relationship</th>
           <th>Whether Living With Me(Yes/No)</th>
           <th>Remarks</th>
         </tr>
       </thead>
       <tbody>
        <?php
        $i=0;
        foreach($members as $rowd){ 
          //print_r($rowd);
         ?>
         <tr>
          <td class="text"><?php echo $i+1; ?></td>
          <td class="text"><?php echo $rowd->Familymembername; ?></td>
          <td class="text"><?php echo $this->gmodel->changedatedbformate($rowd->familydob); ?></td>
          <td class="text">
            <?php
            if(!empty($rowd->familydob))
            {
             $cur_year = date('Y');
             $year=$rowd->familydob;
             $arr=explode('-', $year);
             $currentyear=$arr[0];
             $var=$cur_year-$currentyear;
             echo '<b>'.$var.'</b>';
           }
           ?></td>
           <td class="text">
            <?php echo $rowd->relationname;?> 
          </td>
          <td class="text">
           <select name="weather" class="form-control">
            <option value="">Select</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
            <option value="Not Applicable">Not Applicable</option>
          </select> 

        </td>
        
        <td class="text"><input type="text" name="remarks" class="form-control" maxlength="50" minlength="2" required=""></td>
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
</div>
<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;"> 
  I hereby certify that the information provided above is true and correct. I agree to notify any change in the above particulars as laid down in the Scheme.
  <br><br>
  Yours faithfully,
  <br><br>
</div>
<div  class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style="text-align: justify; line-height: 2">
 <input type="hidden" name="placedec" value="<?php echo $candidatedetailwithaddress->new_office_id;?>">
 Location: <b><input type="text" name="placedec" maxlength="50" minlength="2" required="" style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->officename;?>"></b> <br/>
 Date: <b><input type="text" required="required" class="datepicker" name="datess" required="" style="border-radius: 0px; border: none; border-bottom: 1px solid black;">
 </b>

</div>
<div  class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right" style="text-align: justify; line-height: 2;">
  Signature:
   <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
  <!-- <?php if ($signature->encrypted_signature !='') { ?>
   <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
 <?php }else{ ?>
   <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
 <?php } ?> -->
 <br/>
 Name:&nbsp;<b><?php echo $getstaffdetails->name; ?></b> <br/>
 Designation:&nbsp;<b><?php echo $getstaffdetails->desname; ?></b>  <br>
 Employee Code:&nbsp;<b><?php echo $getstaffdetails->emp_code; ?></b>


</div>


</div>
</div>
<div  class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
 <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
 <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
</div>
</form>
</div>
</div>
</div>
</section>
