<section class="content" style="background-color: #FFFFFF;">
  <br/>
  <?php $page='Application'; require_once(APPPATH.'views/Employee_particular_form/topbar.php');?>

  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">

     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">MEMBER APPLICATION FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6>Professional Assistance for Development Action (PRADAN) </h6>              
          <h6>MEMBER APPLICATION FORM</h6>
        </div>
      </div>
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>

        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>      
          <?php } else if(!empty($er_msg)){?>    
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>        
            <?php } ?>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
            
            <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
             <div class="form-group">
              <div class="form-line">
                <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
                PRADAN.</label>
                <br>
                <?php
                
                echo $officename_list->officename;
                
                ?>

              </div>
              
            </div>
          </div>
          
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
           <div class="form-group">
            <label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir / Madam, </label>
          </div>
        </div>
<!-- <?php //foreach($lpo_office as $ro)
{

  ?> -->
<!-- <?php //foreach($emp_code as $row)
{

  ?> -->

  <?php //foreach($data_application as $rows)
 // {
// /print_r($data_application);
  ?>

  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: justify; line-height: 2">
   <div class="form-group">
    <p>
     I, Ms./Mr.&nbsp;<b><?php echo $candidatedetailwithaddress->staff_name;?></b>&nbsp;<b></b>&nbsp;,&nbsp;<b><?php echo $candidatedetailwithaddress->emp_code;?></b>&nbsp;

     , based at&nbsp;<b><?php echo $candidatedetailwithaddress->officename;?></b>&nbsp;  (Location) with a basic pay of Rs <input type="text" minlength="2" maxlength="6" name="rupees" id="rupees" class="number" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" />

     would like to become a member of the Employees’ Contributory Welfare Scheme with effect from  

     <select name="month" required="" required="" style="min-width: 10px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
       <option value="">Select</option>

       <option value="01">01</option>
       <option value="02">02</option>
       <option value="03">03</option>
       <option value="04">04</option>
       <option value="05">05</option>
       <option value="06">06</option>
       <option value="07">07</option>
       <option value="08">08</option>
       <option value="09">09</option>
       <?php 
       for($i=10; $i<=12; $i++)
       {
        ?>
        <option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php
      }
      ?> 
    </select>

    (Month),
    20
    <select name="years" required="" style="min-width: 10px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
      <option value="">Select</option>
      <option value="00">00</option>
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <?php 
      for($i=10; $i<=30; $i++)
      {
        ?>
        <option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php
      }
      ?> 
    </select> 

    (Year). 
  </p>
  <p>I agree to contribute Rs
    <input type="text" minlength="2" maxlength="6" name="basic_pay" id="basic_pay" class="inputborderbelow number text-right"  required="required" />per month as provided for in the Scheme 
    and authorize PRADAN to deduct this amount 
    from my salary every month. <p>

      <p>
        I agree to abide by all the rules and regulations and confirm to the provisions specified in the Scheme.
      </p>
      <p>
      The details about the members of my family to be included under this Scheme are as under:  </p>     
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top: 50px;">
        Yours faithfully,
      </div>
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
        <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
          
          
          
        </div>
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">         
          Location:<b><?php echo $candidatedetailwithaddress->officename;?></b>&nbsp; <br/>
          Employee Code:<b><?php echo $candidatedetailwithaddress->emp_code;?></b>&nbsp; <br/>

          Name:<b><?php echo $candidatedetailwithaddress->staff_name;?></b>&nbsp;<b></b>&nbsp;
          <br/>
          Date:<input type="text" required="required" class="datepicker" style="border-radius: 0px; border: none; border-bottom: 1px solid black;" name="datess"><br/>
          Designation:
          <b><?php echo $candidatedetailwithaddress->desiname;?></b>&nbsp;
          

          <div class="form-check">
            <label for="Name">Signature<span style="color: red;" >*</span></label>
             <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
          <!--   <?php if ($signature->encrypted_signature !='') { ?>
             <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
           <?php }else{ ?>
             <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
           <?php } ?> -->
         </div>
       </div>
     </div>   
     <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top: 50px;">
       cc: - Finance-Personnel-MIS Unit
       <br>
       &emsp;&emsp;- Personal Dossier (Location)
     </div>
     <div class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
       <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
       <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
     </div>
   </div>
 </div>
<?php }}?>
</div>
</div>
</form>
</div>
</div>
</div>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


    $('.number').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });



  });
  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }
</script>  
