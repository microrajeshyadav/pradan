<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - THE EXIT INTERVIEW FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 40px;">
        <h4>THE EXIT INTERVIEW FORM</h4>
        <p>(to be filled by Employees on Resignation)</p>
      </div> 
    </div>
    <div class="row" style="line-height: 2">
      <div class="col-md-6">
        Name of the Employee: ____________________
      </div>
      <div class="col-md-6 text-right">
        Location: __________________
      </div>     
      <div class="col-md-4">
       Designation: _____________________ 
     </div>
     <div class="col-md-4 text-left">
      w.e.f. _______________________
    </div>
    <div class="col-md-4 text-right">
      Employee Code: __________________________________
    </div>
    <div class="col-md-6">
     Date of Joining: _________________________
   </div>
   <div class="col-md-6 text-right">
    Date of Release: _________________________
  </div>  
  <div class="col-md-12 text-left">
   Would you like the contents to be confidential?:  Yes / No
 </div> 

</div>
<div class="row" style="line-height: 2 ; margin-top: 50px;">

  <div class="col-md-12" style="margin-top: 20px;">
    1.  An experience or two in this organization that has left an almost indelible imprint on your mind.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    2.  Any learning opportunities that this organization provided that you think helped you grow both as a person as well as a professional.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    3.  Specifically, two characteristics that you think are the strengths of this organization.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    4.  Two characteristics that you think are the shortcomings of this organization
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    5.  Any experience in the organization in the past six months or so that has left you feeling bitter.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    6.  Any experience in the organization in the past six months or so that has left you feeling happy.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    7.  Any episode or incident in the organization in which you felt discriminated against.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    8.  Now, taking an overview, what could possibly be the factors/issues/circumstances that precipitated your decision to quit this organization?
  </div>
  <div class="col-md-12" style="margin-top: 20px;">______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    9.  What are the factors that attract you to take up your proposed assignment?
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    10. If you have the choice to change the way things are in this organization, what would two such things be that you may like to focus on?
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    11. Anything else that you might like to say before you leave this organization.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    ______________________________________________________________________________________
  </div>

</div>

<div class="row text-left" style="margin-top: 20px; ">
 <div class="col-md-12" style="margin-bottom: 20px; ">
   <strong>(Signature of the Separating Employee) </strong>
 </div>
 <div class="col-md-6 text-left">
   _______________________________
 </div>
 <div class="col-md-6 text-right">
   Date : _______________________________
 </div>

</div>
<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 2">
 <div class="col-md-12" style="margin-bottom: 20px; ">
   <strong>Observations (if any) of the Interviewer Conducting the Exit Interview </strong>
 </div>
 <div class="col-md-6 text-left">
   Signature: _________________________
 </div>
 <div class="col-md-6 text-right">
   Place: _____________________
 </div>
 <div class="col-md-6 text-left">
   Name: _________________________
 </div>
 <div class="col-md-6 text-right">
   Date: _____________________
 </div>
<div class="col-md-6 text-left">
   Designation: ______________________
 </div>
</div>

</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>