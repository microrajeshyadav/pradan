<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;
  
}
textarea{
  width: 600px !important;
}


</style>
<section class="content" style="background-color: #FFFFFF;" >
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <form role="form" method="post" action="">
        <br>
        <div class="container-fluid">
         <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
        <!--  <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - THE EXIT INTERVIEW FORM
             </h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
           <div class="row">
             <div class="col-md-12 text-center">
              <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
            </div>
            <div class="col-md-12 text-center" style="margin-bottom: 40px;">
              <h4>Inter-Office Memo</h4>
              
            </div> 
          </div>
          <div class="row" style="line-height: 2">
            <div class="col-md-9">
              To: <label  name="employee_name"  class="inputborderbelow" > <?php echo $staff_detail->name; ?></label> <br/>
              (Name of the Separating Employee)
            </div>
            <div class="col-md-3 text-left">
              Date: <label  name="date"  class="inputborderbelow" ><?php echo date('d/m/Y')  ?></label>
            </div>
          </div>
          

          <div class="row" style="line-height: 2">
            <div class="col-md-9">
              From: <label  name="employee_name"  class="inputborderbelow" > <?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?></label> <br/>
              (Name of the Person conducting Exit Interview)
            </div>
            <div class="col-md-3 text-left">
              File: Personal Dossier of Employee
            </div>
          </div>
          <div class="row" style="line-height: 2;margin-top: 50px;">
            <div class="col-md-12 text-center">
             Subject  : <strong>The Exit Interview Form </strong>
           </div>
           
         </div>
         <div class="row" style="margin-top: 50px;">
           <div class="col-md-12 text-left">
            <p>As you plan to leave PRADAN soon, I write to request you to fill in the enclosed ‘Exit Interview Form’. We hope, through the information you would provide us in this format, to seek feedback about issues related to the organization and inputs for possible measures to change any aspect of the organization's functioning.

            </p> 
            <p>I would like to reassure you that we would treat the contents of this form as confidential, if you so desire, and use it only for organizational learning.</p>

            <p>I would like to reassure you that we would treat the contents of this form as confidential, if you so desire, and use it only for organizational learning.</p>
            <p>
              We do hope that you make a smooth transition to your new assignment, and sincerely wish you the very best in your life.
            </p>
            <p>
              With regards.
            </p>
            <p style="margin-top: 50px;">
             ( __________________________ )
           </p>
           <p>
            Encl:<strong> As above</strong>
          </p>


        </div>
        
      </div>
 -->

      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 40px;">
        <h4>THE EXIT INTERVIEW FORM</h4>
      </div> 
    </div>

    <?php if($staff_transaction->trans_status != 'Super Annuation')
{ ?>
    <div class="row" style="line-height: 2">
      <div class="col-md-9">
        Name of the Employee: <input type="text" name="employee_name" value="<?php if(!empty($staff_detail->name))echo $staff_detail->name; ?>" class="inputborderbelow" required>
      </div>
      <div class="col-md-3 text-left">
        Location: <input type="text" name="employee_location" class="inputborderbelow" value="<?php if(!empty($join_relive_data->location))echo $join_relive_data->location; ?>" >
      </div>     
      <div class="col-md-5">
       Designation: <input type="text" name="employee_designation" value="<?php if(!empty($staff_detail->desname))echo $staff_detail->desname; ?>" class="inputborderbelow" required> 
     </div>
     <div class="col-md-4 text-left">
      w.e.f. <input type="text" name="employee_wef" class="inputborderbelow" >
    </div>
    <div class="col-md-3 text-left">
      Employee Code: <input type="text" name="employee_code" value="<?php if(!empty($staff_detail->emp_code)) echo $staff_detail->emp_code; ?>" class="inputborderbelow" required>
    </div>
    <div class="col-md-9">
     Date of Joining: <label class="inputborderbelow" > <?php if(!empty($join_relive_data->joniningdate))echo $this->gmodel->changedatedbformate($join_relive_data->joniningdate); ?></label>
   </div>
   <div class="col-md-3 text-left">
    Date of Release: <label class="inputborderbelow" ><?php if(!empty($staff_detail->seperatedate)) echo $this->gmodel->changedatedbformate($staff_detail->seperatedate); ?></label>
  </div>  
  <div class="col-md-12 text-left">
   Would you like the contents to be confidential?:  
   <input type="radio" name="content_confedential" value="1" <?php if(!empty($staff_sepemployeeexitform)) if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->contents_confidential == 1) echo "checked"; } ?> required> Yes <input type="radio" name="content_confedential" value="0" <?php if(!empty($staff_sepemployeeexitform))if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->contents_confidential == 0) echo "checked"; } ?> required> No
 </div> 

</div>
<div class="row" style="line-height: 2 ; margin-top: 50px;">

  <div class="col-md-12" style="margin-top: 20px;">
    1.  An experience or two in this organization that has left an almost indelible imprint on your mind.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" cols="50" name="question1" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->an_experience_1;} ?>
    </textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    2.  Any learning opportunities that this organization provided that you think helped you grow both as a person as well as a professional.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question2" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_learning_2;} ?>
    </textarea>
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    3.  Specifically, two characteristics that you think are the strengths of this organization.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question3" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->specifically_3;} ?>      
    </textarea>
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    4.  Two characteristics that you think are the shortcomings of this organization
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question4" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->two_characteristics_4;} ?>
    </textarea>
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    5.  Any experience in the organization in the past six months or so that has left you feeling bitter.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question5" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_experience_5;} ?>
    </textarea>
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    6.  Any experience in the organization in the past six months or so that has left you feeling happy.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
   <textarea class="inputborderbelow" name="question6" cols="50" maxlength="255" required>
     <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_experience_6;} ?>
   </textarea>
 </div>
 <div class="col-md-12" style="margin-top: 20px;">
  7.  Any episode or incident in the organization in which you felt discriminated against.
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question7" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_episode_7;} ?>
  </textarea>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  8.  Now, taking an overview, what could possibly be the factors/issues/circumstances that precipitated your decision to quit this organization?
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question8" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->taking_an_overview_8;} ?>
  </textarea>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  9.  What are the factors that attract you to take up your proposed assignment?
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question9" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->proposed_assignment_9;} ?>
  </textarea>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  10. If you have the choice to change the way things are in this organization, what would two such things be that you may like to focus on?
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question10" cols="100" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->such_things_be_that_you_may_like_to_focus_on_10;} ?>
  </textarea>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  11. Anything else that you might like to say before you leave this organization.
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question11" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->you_leave_this_organization_11;} ?>
  </textarea>
</div>

</div>

<?php }
else{ ?>

<div class="row" style="line-height: 2 ; margin-top: 50px;">
    <div class="col-md-12" style="margin-top: 20px;">
    
      <!-- <?php //echo $retering_content ?> -->

      <p class="MsoNormal" style="margin-left:30.6pt;text-align:justify;text-indent:
-30.6pt;mso-pagination:none"><b><span lang="EN-US">Note</span></b><span lang="EN-US">: This form is meant to elicit candid
impressions about PRADAN from you with a view to further improve the state of
affairs of PRADAN.<o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<h3><span lang="EN-US" style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-bidi-font-weight:
normal">PART - I<o:p></o:p></span></h3>


<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">1.&nbsp;&nbsp; Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <input type="text" name="employee_name" value="<?php if(!empty($staff_detail->name))echo $staff_detail->name; ?>" class="inputborderbelow" required><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">2.&nbsp;&nbsp; Employee
Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <input type="text" name="employee_code" value="<?php if(!empty($staff_detail->emp_code)) echo $staff_detail->emp_code; ?>" class="inputborderbelow" required><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">3.&nbsp;&nbsp; Location
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
<input type="text" name="employee_location" class="inputborderbelow" value="<?php if(!empty($staff_detail->permanentstreet))echo $staff_detail->permanentstreet; ?>"><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">4.&nbsp;&nbsp; Date
of Joining PRADAN: <label class="inputborderbelow" > <?php if(!empty($staff_detail->doj))echo $this->gmodel->changedatedbformate($staff_detail->doj); ?></label><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">5.&nbsp;&nbsp; Designation
on Joining&nbsp;&nbsp;&nbsp;&nbsp; : <input type="text" name="employee_designation" value="<?php if(!empty($staff_detail->desname))echo $staff_detail->desname; ?>" class="inputborderbelow" required><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">6.&nbsp;&nbsp; Date
of Superannuation&nbsp;&nbsp;&nbsp; : <label class="inputborderbelow" ><?php echo date('d/m/Y') ?></label><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
<p class="MsoNormal"><span lang="EN-US">7.&nbsp;&nbsp; Total
Service in PRADAN: <label class="inputborderbelow" ><?php echo $service_yr ?> years <?php echo $service_mths ?> months</label><o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">8.&nbsp;&nbsp; Designation
at the time of retiring from PRADAN: <input type="text" name="employee_designation" value="<?php if(!empty($staff_detail->desname))echo $staff_detail->desname; ?>" class="inputborderbelow" required><o:p></o:p></span></p>

<span lang="EN-US" style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-ansi-language:
EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA;layout-grid-mode:line"><br clear="all" style="mso-special-character:line-break;page-break-before:always">
</span>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<h3 align="center" style="text-align:center"><span lang="EN-US" style="font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;;mso-bidi-font-weight:normal">PART - II<o:p></o:p></span></h3>

<p class="MsoNormal" align="center" style="text-align:center;mso-pagination:none"><span lang="EN-US">(<i>to be filled by the Retiring
Employee</i>)<o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="text-align:justify;mso-pagination:none;tab-stops:
30.0pt"><span lang="EN-US">(A)&nbsp;&nbsp;&nbsp;&nbsp; Please list <i>three</i> things that you
have liked the most in PRADAN:<o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="most_liked" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->most_liked;} ?>      
    </textarea>
  </div>
<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="margin-left:30.0pt;text-align:justify;text-indent:
-30.0pt;mso-pagination:none"><span lang="EN-US">(B)&nbsp;&nbsp;&nbsp;&nbsp; Please list <i>three</i> things that you
have liked the least in PRADAN:<o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" cols="50" name="least_liked" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->least_liked;} ?>
    </textarea>
  </div>
<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="margin-left:30.0pt;text-align:justify;text-indent:
-30.0pt;mso-pagination:none"><span lang="EN-US">(C)&nbsp;&nbsp;&nbsp;&nbsp; Please give at least <i>three</i>
suggestions for improving the state of affairs in PRADAN:<o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" cols="50" name="suggestions" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->suggestions;} ?>
    </textarea>
  </div>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="margin-left:30.0pt;text-indent:-30.0pt;mso-pagination:
none"><span lang="EN-US">(D)&nbsp;&nbsp;&nbsp;&nbsp; Any other remarks:<o:p></o:p></span></p>

<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="remarks" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->remarks;} ?>
  </textarea>
</div>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="margin-left:30.0pt;text-indent:-30.0pt;line-height:
150%;mso-pagination:none"><span lang="EN-US">(E)&nbsp;&nbsp;&nbsp;&nbsp; Your future contact address:<o:p></o:p></span></p>

<div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" cols="50" name="future_address" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->future_address;} ?>
    </textarea>
  </div>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0">
 <tbody><tr>
  <td width="239" valign="top" style="width:179.4pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Place: ____________________<o:p></o:p></span></p>
  </td>
  <td width="376" valign="top" style="width:282.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Signature:
  _________________________________<o:p></o:p></span></p>
  </td>
 </tr>
 <tr>
  <td width="239" valign="top" style="width:179.4pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Date: <?php echo date('d/m/Y') ?><o:p></o:p></span></p>
  </td>
  <td width="376" valign="top" style="width:282.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $this->loginData->UserFirstName; echo $this->loginData->UserMiddleName; echo $this->loginData->UserLastName ?> <o:p></o:p></span></p>
  </td>
 </tr>
</tbody></table>
<?php if($this->loginData->RoleID == 17) { ?>
<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" align="center" style="text-align:center;mso-pagination:none"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" align="center" style="text-align:center;mso-pagination:none"><b><span lang="EN-US">Observations of the Person
Facilitating the Exit Process<o:p></o:p></span></b></p>

<p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">___________________________________________________________________________<o:p></o:p></span></p>

<p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">___________________________________________________________________________<o:p></o:p></span></p>

<p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">___________________________________________________________________________<o:p></o:p></span></p>

<p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">___________________________________________________________________________<o:p></o:p></span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>

<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0">
 <tbody><tr>
  <td width="239" valign="top" style="width:179.4pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Date: ________________<o:p></o:p></span></p>
  </td>
  <td width="376" valign="top" style="width:282.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Signature:
  _________________________________<o:p></o:p></span></p>
  </td>
 </tr>
 <tr>
  <td width="239" valign="top" style="width:179.4pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US"><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width="376" valign="top" style="width:282.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class="MsoNormal" style="line-height:150%;mso-pagination:none"><span lang="EN-US">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $this->loginData->Name ?><o:p></o:p></span></p>
  </td>
 </tr>
<?php } ?>
</tbody></table>
    
  </div>
  </div>

<?php } ?>

<div class="row text-left" style="margin-top: 20px; ">
 <div class="col-md-12" style="margin-bottom: 20px; ">
   <strong>(Signature of the Separating Employee) </strong>
 </div>
 <div class="col-md-6 text-left">
   <label class="inputborderbelow"> 
 <?php    
if(!empty($staff_detail->encrypted_signature)) {
                              $image_path='';
                              $image_path=FCPATH.'datafiles\signature/'. $staff_detail->encrypted_signature;

                              if (file_exists($image_path)) {




                                ?>
                                <img src="<?php echo site_url().'datafiles\signature/'. $staff_detail->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                              <?php }
                              else
                              {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">

                                <?php
                              } }  ?>


   </label> 
 </div>
 <div class="col-md-6 text-right">
   Date : <input type="text" name="date_below_sign" value="<?php echo date('d/m/Y'); ?>" class="inputborderbelow" required> 
 </div>

</div>
<?php if($this->loginData->RoleID==2 || $this->loginData->RoleID==21) {?>
<hr/>

<div class="row text-left" style="margin-top: 20px; line-height: 2">
 <div class="col-md-12" style="margin-bottom: 20px; ">
   <strong>Observations (if any) of the Interviewer Conducting the Exit Interview </strong>
   
  <div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question11" cols="50" maxlength="255" id="question11"><?php echo $staff_sepemployeeexitform->tc_observation;?></textarea>
   
  </textarea>
</div>
 </div>
 <div class="col-md-6 text-left">
   Signature: <label class="inputborderbelow"></label>
 </div>
 <div class="col-md-6 text-right">
   Place: <input type="text" name="sign_place" class="inputborderbelow" >
 </div>
 <div class="col-md-6 text-left">
   Name: <label class="inputborderbelow" required><?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?></label>
 </div>
 <div class="col-md-6 text-right">
   Date: <input type="text" name="date_below_sign_observer" value="<?php echo date('d/m/Y'); ?>" class="inputborderbelow" required>
 </div>
 <div class="col-md-6 text-left">
   Designation: <input type="text" name="sign_designation" class="inputborderbelow" >
 </div>
</div>

</div>
<?php } ?>
<div class="panel-footer text-right">
  <?php
  // echo $staff_sepemployeeexitform->flag;
  // die;
  if(empty($staff_sepemployeeexitform)){
    ?>
   <!--  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
    <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
    <?php
  } else if($staff_sepemployeeexitform->flag==1 && $this->loginData->RoleID==3){

    ?>
   <!--  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm"> -->
    <a href="<?php echo site_url("Staff_approval_process");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>

    <?php 

  }
  else if($staff_sepemployeeexitform->flag==1 && $this->loginData->RoleID==2){

    ?>
    <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
   

    <?php 

  }
  else if($staff_sepemployeeexitform->flag == 0){
     ?>
     <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
    
     <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
     <?php } 
     if(!empty($staff_sepemployeeexitform->flag))
     {
     if($staff_sepemployeeexitform->flag == 2 && ($this->loginData->RoleID==2 || $this->loginData->RoleID==21) ){
     ?>
<a href="<?php echo site_url("Staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>

   <?php } 
   


 }?>
    
   </div>
 </div>
</div>
</form>
</section>

<?php 
if($staff_sepemployeeexitform){
  if($staff_sepemployeeexitform->flag == 1){ ?>
  <script type="text/javascript">
    $(document).ready(function(){
     
      // $('#saveandsubmit').hide() ;
      // $("form input[type=text]").prop("disabled", true);

      $("form input[type=radio]").prop("disabled", true);
      $("form textarea").prop("disabled", true);
      $("form textarea").css("width", "600px;");
      $("form textarea").attr("style", "width:600px;");
      
       $("#question11").prop("disabled", false);
      $("#question11").css("width", "600px;");
      $("#question11").attr("style", "width:600px;");
    });
  </script>
  <?php } }?>