<section class="content">
  <?php foreach($role_permission as $row) { if($row->Controller == 'Recruitersheadmapping' && $row->Action == 'add') { ?>  
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form name="campus" action="" method="post" > 
              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-8 panel-title pull-left">Anchor Mapping</h4>
                   <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">

                <div class="row">
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <label for="Name" style="vertical-align: top;">Campus<span style="color: red;" >*</span></label>
                  <select name="campus" id="campus" class="form-control" required="required" >
                    <option value="">Select Campus</option>
                    <?php foreach ($campuslist as $key => $value) { ?>
                    <option value="<?php echo  $value->campusid;?>"><?php echo  $value->campusname; ?></option>
                    <?php } ?>
                  </select>
                  <?php echo form_error("campus");?>
                </div>
                <?php //echo print_r($recruitmentlist); ?>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <label for="Name">Anchor <span style="color: red;" >*</span></label>
                  <select name="recruiter" id="recruiter" class="form-control" required="required" >
                    <option value="">Select Anchor</option>
                  </select>
                  <?php echo form_error("recruiter");?>
                </div>


             </div>
           </div>
           <div class="panel-footer text-right">
                 <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Save </button>
                 <a href="<?php echo site_url("Recruitersheadmapping");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
               </div>
         </div>
       </form> 
     </div>
     <!-- #END# Exportable Table -->
   </div>
   <?php } } ?>
 </section>
 <script type="text/javascript">
   $(document).ready(function(){
    $("#campus").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getRecruitersHead/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {

        console.log(data);
        $("#recruiter").html(data);

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    });


  });     

</script>