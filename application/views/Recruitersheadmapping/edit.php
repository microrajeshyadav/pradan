<section class="content">
  <?php foreach($role_permission as $row) { if($row->Controller == 'Recruitersheadmapping' && $row->Action == 'edit') { ?>  
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <?php //print_r($recuiterheadcampusdetail); ?>
          
        <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Recuiters to recuiter head mapping</b></div>
        <div class="panel-body">
           <form name="campus" action="" method="post" > 
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name" style="vertical-align: top;">Campus<span style="color: red;" >*</span></label>
                <select name="campus" id="campus" class="form-control" >
                <option value="">Select Campus</option>
                <?php foreach ($campuslist as $key => $value) {
                  if($value->campusid == $recuiterheadcampusdetail[0]->campusid){
                 ?>
                <option value="<?php echo $value->campusid;?>" SELECTED="SELECTED"><?php echo  $value->campusname; ?></option>
                <?php } else{ ?>
                <option value="<?php echo $value->campusid;?>" ><?php echo  $value->campusname; ?></option>
                <?php } } ?>
                </select>
                  <?php echo form_error("campus");?>
                </div>
               
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Anchor  <span style="color: red;" >*</span></label>
                <select name="recruiters" id="recruiters" class="form-control"  >
                <option value="">Select Anchor</option>
                  <?php foreach ($recruitmentlist as $key => $value) { 
                     if($value->staffid = $recuiterheadcampusdetail[0]->recruiterheadid) { ?>
                <option value="<?php echo $value->staffid;?>" selected="SELECTED"><?php echo $value->name;?></option>
                <?php }else{ ?>
                <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>
                <?php } } ?>
                </select>
                  <?php echo form_error("recruiters");?>
                </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                         <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Save </button>
                          <a href="<?php echo site_url("Campusstaffmapping");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                      </div>  
                </div>
               </div>
                 </form> 
                </div>


              </div><!-- /.panel-->

            </div>

          </div>

        </div>

      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>
   <script type="text/javascript">
        $(document).ready(function(){

            $("#campus").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getRecruitersHead/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   
                    console.log(data);
                    $("#recruiters").html(data);
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     

</script>