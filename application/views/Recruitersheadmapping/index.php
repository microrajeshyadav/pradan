 <section class="content" style="background-color: #FFFFFF;" >
  <?php foreach($role_permission as $row) { if($row->Controller == 'Recruitersheadmapping' && $row->Action == 'index') { ?>  
  <br>
  <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">List Of Recruiters Head Mapping</h4>
      <div class="col-md-2 text-right">
       
         <a ata-toggle="tooltip" data-placement="bottom" title="Want to map Anchor? Click on me." href="<?php echo site_url()."Recruitersheadmapping/add";?>" class="btn btn-sm btn-primary">Anchor Map</a>

      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
            
            <div class="row" style="background-color: #FFFFFF;">
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">
           
                <br>
                <br>
           </div>
       </div>

        
        <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th style="width:50px;" class="text-center">S.No.</th>
              <th class="text-center">Campus</th>
              <th class="text-center"> Anchor </th>
              <th style="width:50px;">Action</th>
           </tr> 
         </thead>
         <tbody>
          <?php  $i=0; foreach ($recuterslist as $key => $value) { ?>
          <tr>
            <td class="text-center"><?php echo $i+1;?></td>
           <td class="text-center"><?php echo $value->campusname;?></td>
           <td class="text-center"><?php echo $value->name;?></td>
            <td><a href="<?php echo site_url('Recruitersheadmapping/edit/'.$value->id);?>" style="padding : 4px;" title="Edit"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a href="<?php echo site_url('Recruitersheadmapping/delete/'.$value->id);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" title="Delete"><i class="fa fa-trash" style="color:red"></i></a></td>
          </tr>
          <?php $i++; } ?>
         
        </tbody>
      </table>
       </div>
    </div> 
    </div>
    </div>
    </div>   

<?php } } ?>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
function yesnoCheck() {
   
    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
    }
  }
</script>
<script>
function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>