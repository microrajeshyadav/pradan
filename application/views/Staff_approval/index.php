
  <style> 
  .table_wrapper{
    display: block;
    overflow-x: auto;
    white-space: nowrap;
  }
</style>
<section class="content" style="background-color: #FFFFFF;" >
  <?php  //foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "index"){ ?>
  <br/>

  <div class="container-fluid">
   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
   <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Transfer Approval List</h4>
         </div>
         <hr class="colorgraph"><br>
       </div>

       <div class="panel-body">

        <div class="row" style="background-color: #FFFFFF;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
            <table id="tablecampus" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
              <thead>
               <tr>
                 <th style ="max-width:50px;" class="text-center">S. No.</th>
                 <th>Emp code</th>
                 <th>Name</th>
                 <th>Old Office</th>
                 <th>Proposed Office</th>
                 <th>Proposed Transfer Date</th>
                 <th>Reason</th>
                 <th>Comments</th>
                 <th>Sender Name</th>
                 <th class="text-center">Stage</th>
                 <th>Request Date</th>
                <!--  <th class="text-center">Status</th> -->
                 <th style ="max-width:50px;" class="text-center">Action</th>
               </tr> 
             </thead>
             <?php 
              
             if (count($getworkflowdetail) == 0) { ?>
             <tbody>

             </tbody>
             <?php } else{ ?>

             <tbody>
              <?php
              $i=0;
             // print_r($getworkflowdetail);
              foreach ($getworkflowdetail as $key => $value) {
                $proposed_date_bdformat = $this->gmodel->changedateFormateExcludeTime($value->proposeddate);
                $request_date = $this->gmodel->changedateFormateExcludeTime($value->Requestdate);

                ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td><?php echo $value->emp_code;?></td>
                  <td><?php echo $value->name;?></td>
                  <td><?php echo $value->officename;?></td>
                  <td><?php echo $value->newoffice;?> </td>
                  <td><?php echo $proposed_date_bdformat;?> </td>
                  <td><?php echo $value->reason;?></td>
                  <td><?php echo $value->scomments;?></td>
                  <td><?php echo $value->sendername; ?></td>
                  <td class="text-center"><span class="badge badge-pill badge-info col-md-12"><?php echo $value->flag; ?></span> </td>
                  <td><?php echo $request_date;?></td>
                 <!--  <td class="text-center"> <span class="badge badge-pill badge-info"><?php echo $value->process_type ;?></span> </td> -->
                  <td class="text-center form inline">
                    <a class="btn btn-dark btn-sm" data-toggle="tooltip" title = "History" href="<?php echo site_url()."TStaff_history/index/".$value->transid;?>"><i class="fas fa-history"></i>
                    </a>
                    <?php if ($value->status==1) { ?>
                    <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>

                    <?php } ?>
                    <?php if ($value->status==5) { ?>
                    <a class="btn btn-info btn-sm" title = "Click here to 
                    inter-office memo (IOM) for transfer." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>
                    <?php } ?>
                    <?php if (($value->status==5 || $value->status==7) && empty($value->tbl_clearance_certificate_id)) {  ?>
                    <a class="btn btn-warning btn-sm" title = "Click here for clearance certificate." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/add_clearance_certificate/".$value->transid;?>"> <i class="fas fa-certificate"></i></a>
                    <?php }else if( $value->status==7 && !empty($value->tbl_clearance_certificate_id)){ ?>
                    <a class="btn btn-warning btn-sm" title = "Click here to clearance certificate on submitted." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> <i class="fas fa-certificate"></i></a>
                    <?php } ?>
                    <?php if ($value->status == 11) { ?>                    
                    <?php } ?>
                    <?php if ($value->status == 13 && $value->reportingto == $this->loginData->staffid) { ?>
                    <a class="btn btn-info btn-sm" title = "Click here to Joining report of newplace posting." data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_newplace_posting/add_transfer/".$value->transid;?>">  <i class="fas fa-file-invoice"></i></a>
                    <?php } ?>
                    <?php if ($value->status == 15 && !empty($value->tbl_joining_report_new_place_id)) { ?>
                    <a class="btn btn-info btn-sm" title = "Click here to Joining report of newplace posting." data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_newplace_posting/view_transfer/".$value->tbl_joining_report_new_place_id;?>"> <i class="fas fa-file-invoice"></i></a>
                    <?php } ?>
                    <?php if ($value->status == 17) { ?>

                    <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                    
                    <a class="btn btn-warning btn-sm" title = "Click here for transfer claim expenses view." data-toggle="tooltip" href="<?php echo site_url()."transfer_claim_expense/transfer/".$value->transid;?>"> <i class="fas fa-dolly-flatbed"></i>  
                    </a>
                     <a class="btn btn-success btn-xs" title = "Click here for 
                     Add iom." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></i>  
                    </a>
                    <a class="btn btn-success btn-xs" title = "Click here for 
                     Clearance Certificate." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> <i class="fas fa-certificate"></i>
                    </a>
                     <a class="btn btn-success btn-xs" title = "Click here for 
                     Handed Over." data-toggle="tooltip" href="<?php echo site_url()."Handed_over_taken_over_transfer/view/".$value->transid.'/'.$value->Handed_id;?>"> <i class="fas fa-arrow-alt-circle-right"> </i> 
                    </a>
                     <a class="btn btn-success btn-xs" title = "Click here for 
                     Taken Over." data-toggle="tooltip" href="<?php echo site_url()."Handed_over_taken_over_transfer/view/".$value->transid.'/'.$value->taken_id;?>"><i class="fas fa-arrow-alt-circle-left"></i> 
                    </a>
                      <a class="btn btn-success btn-xs" title = "Click here for 
                     Joining Report." data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_newplace_posting/view_transfer/".$value->tbl_joining_report_new_place_id;?>"> <i class="fas fa-file-invoice"></i></i>  
                    </a>
                    
                    <?php } ?> 
                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
              <?php } ?>
            </table>
          </div>
        </div> 
      </div>
    </div>

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Change Responsibility Approval List</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
       <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus1" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
            <thead>
             <tr>
               <th style ="max-width:50px;" class="text-center">S. No.</th>
               <th>Emp code</th>
               <th>Name</th>
               <th>Old Office</th>               
               <th>Current Designation</th>
               <th>Proposed  Designation</th>
               <th>Proposed  Date</th>
               <th>Reason</th>
               <th>Remark</th>
               <th>Sender Name</th>
               <th class="text-center">Stage</th>
               <th>Request Date</th>               
               <th style ="max-width:50px;" class="text-center">Action</th>
             </tr> 
           </thead>
           <?php if (count($getpromationworkflowdetail)==0) {?>
           <tbody>

           </tbody>
           <?php  }else{ ?>
           <tbody>
            <?php                     
            $i=0;
            foreach ($getpromationworkflowdetail as $key => $value) {
              $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
              ?>
              <tr>
                <td class="text-center"><?php echo $i+1;?></td>
                <td><?php echo $value->emp_code;?></td>
                <td><?php echo $value->name;?></td>
                <td><?php echo $value->officename;?></td>
                <td><?php echo $value->desnameold;?> </td>
                <td><?php echo $value->desnewname;?> </td>
                <td><?php echo $proposed_date_bdformat;?> </td>
                <td><?php echo $value->reason;?></td>
                <td><?php echo $value->scomments;?></td>
                <td><?php echo $value->sendername; ?></td>
                <td class="text-center"><span class="badge badge-pill badge-info col-md-12"><?php echo $value->flag; ?></span></td>
                <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>                
                <td class="text-center">
                  <a class="btn btn-dark btn-sm" data-toggle="tooltip" title = "History" href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i>
                  </a>
                  <?php if($value->status==5){ ?>
                  <a class="btn btn-info btn-sm" data-toggle="tooltip"  title = "Handed Over" data-toggle="tooltip" href="<?php echo site_url()."Changeresponsibility_handed_over_taken_over_encourage/index/".$value->transid;?>"> <i class="fas fa-arrow-alt-circle-left"></i>  </a>
                  <?php } ?>
                  <?php if ($value->status==1) { ?>
                  | <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip" href="<?php echo site_url()."Staff_approval_changerespons/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>                  
                  <?php } elseif($value->status==7){ ?>
                  <a class="btn btn-info btn-sm" title = "Taken Over" data-toggle="tooltip" href="<?php echo site_url()."Changeresponsibility_taken_over_encourage/index/".$value->transid;?>"> <i class="fas fa-arrow-alt-circle-left"></i> </a>
                  <?php } 
                    if ($value->status==10)
                    {
                  ?>
                    
         <a class="btn btn-info btn-sm" title = "Joining Report" data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_cr/add/".$value->joining_id."/".$value->transid;?>"> <i class="fas fa-arrow-alt-circle-left"></i> </a>
                 <?php } 
                  else if($value->status==11) {
                 ?>
                 <a class="btn btn-info btn-sm" title = "Joining Report" data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_cr/view/".$value->joining_id;?>"> <i class="fas fa-arrow-alt-circle-left"></i> </a>
                  <?php } ?>
                </td>
              </tr>
              <?php $i++; } ?>
            </tbody>
            <?php } ?>
          </table>
        </div>
      </div> 
    </div>
  </div>
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Transfer & Change Responsibility Approval List</h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <div class="panel-body">
     <div class="row" style="background-color: #FFFFFF; ">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"  >
        <table id="tablecampus2" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
          <thead>
           <tr>
             <th style ="max-width:50px;" class="text-center">S. No.</th>
             <th>Emp code</th>
             <th>Name</th>
             <th>Old Office</th>
             <th>Proposed Office</th>
             <th>Current Designation</th>
             <th>Proposed Designation</th>
             <th>Proposed Transfer Date</th>
             <th>Reason</th>
             <th>Remark</th>
             <th>Sender Name</th>
             <th class="text-center">Stage</th>
             <th>Request Date</th>
             <th>Status</th>
             <th style ="max-width:50px;" class="text-center">Action</th>
           </tr> 
         </thead>
         <?php if (count($gettransferpromationworkflowdetail)==0) {?>
         <tbody>

         </tbody>
         <?php  }else{ ?>
         <tbody>
          <?php 
          $i=0;

         
        
          foreach ($gettransferpromationworkflowdetail as $key => $value) {
            $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
            ?>
            <tr>
              <td class="text-center"><?php echo $i+1;?></td>
              <td><?php echo $value->emp_code;?></td>
              <td><?php echo $value->name;?></td>
              <td><?php echo $value->officename;?></td>
              <td><?php echo $value->newoffice;?> </td>
              <td><?php echo $value->desnameold;?> </td>
              <td><?php echo $value->desnewname;?> </td>
              <td><?php echo $proposed_date_bdformat;?> </td>
              <td><?php echo $value->reason;?></td>
              <td><?php echo $value->scomments;?></td>
              <td><?php echo $value->sendername; ?></td>
              <td><span class="badge badge-pill badge-info col-md-12"><?php echo $value->flag; ?></span></td>
              <td><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
              <td><?php echo $value->process_type ;?></td>
              <td class="text-center">
                <a class="btn btn-dark btn-sm" data-toggle="tooltip" title = "History"  href="<?php echo site_url()."TCRStaff_history/index/".$value->transid;?>"> <i class="fas fa-history"></i>
                  </a>               
                <?php if ($value->status==2) { ?>
                <a class="btn btn-warning btn-sm" title = "Click here to chnage responsibility." data-toggle="tooltip" href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"><i class="fas fa-random"></i></a>

                <a class="btn btn-warning btn-sm" title = "Clearance Certificate on Transfer" data-toggle="tooltip" href="<?php echo site_url()."Staff_approval/add_clearance_certificate/".$value->transid;?>"> <i class="fas fa-certificate"></i> </a>

                <a class="btn btn-warning btn-sm" title = "Joining Report" data-toggle="tooltip" href="<?php echo site_url()."Staff_approval/list_joining_report/".$value->transid;?>"><i class="fas fa-file-invoice"></i> </a>

                <a class="btn btn-warning btn-xs" title = "Joining Report" data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_newplace_posting/add/".$value->transid;?>"><i class="fas fa-file-invoice"></i></a>
                <?php } ?>
                <?php if ($value->status==1) { ?>
                <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip"  href="<?php echo site_url()."Stafftransferpromotion/approve/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                <?php }  ?>
                <?php if ($value->status==5) { ?>
                <a class="btn btn-warning btn-sm" title = "Click here to 
                 inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->transid;?>"> <i class="fas fa-exchange-alt"></i></a>
                <a class="btn btn-warning btn-sm" title = "Change Responsibility"  data-toggle="tooltip" href="<?php echo site_url()."Change_responsibility/index/".$value->transid;?>"> <i class="fas fa-random"></i>  
                </a>
                <a class="btn btn-warning btn-sm" title = "Clearance Certificate on Transfer" data-toggle="tooltip" href="<?php echo site_url()."Staff_approval/add_clearance_certificate/".$value->transid;?>"> <i class="fas fa-certificate"></i></a>
                <?php } ?>
                <?php if ($value->status==7) { ?>
                <a class="btn btn-warning btn-sm" title = "Clearance Certificate on Transfer" data-toggle="tooltip" href="<?php echo site_url()."Staff_approval/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> <i class="fas fa-certificate"></i> </a>
                <?php } ?>
                <?php if ($value->status == 11) { ?>
                <a class="btn btn-warning btn-sm" title = "Click here to taking over charge." data-toggle="tooltip" href="<?php echo site_url()."handed_over_taken_over/index/".$value->transid;?>"><i class="fas fa-arrow-alt-circle-right"></i></a>
                <?php } ?>
                <?php if ($value->status == 13) { ?>
                <a class="btn btn-warning btn-sm" title = "JOINING REPORT AT NEW PLACE OF POSTING" data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_newplace_posting/add/".$value->transid;?>"> <i class="fas fa-file-invoice"></i> </a>
                <?php } ?>
                <?php if ($value->status == 15 && !empty($value->tbl_joining_report_new_place_id)) { ?>
                <a class="btn btn-warning btn-xs" title = "JOINING REPORT AT NEW PLACE OF POSTING." href="<?php echo site_url()."Joining_report_of_newplace_posting/view/".$value->tbl_joining_report_new_place_id;?>"> <i class="fas fa-file-invoice"></i></a>
                <?php } ?>

                <?php if ($value->status == 17) { ?>
                
                  <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                    
                    <a class="btn btn-warning btn-sm" title = "Click here for transfer claim expenses view." data-toggle="tooltip" href="<?php echo site_url()."transfer_claim_expense/transfer/".$value->transid;?>"> <i class="fas fa-dolly-flatbed"></i>  
                    </a>
                     <a class="btn btn-success btn-xs" title = "Click here for 
                     Add iom." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/add_iom/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></i>  
                    </a>
                    <a class="btn btn-success btn-xs" title = "Click here for 
                     Clearance Certificate." data-toggle="tooltip" href="<?php echo site_url()."Stafftransfer/view_clearance_certificate/".$value->tbl_clearance_certificate_id;?>"> <i class="fas fa-certificate"></i>
                    </a>
                     <a class="btn btn-success btn-xs" title = "Click here for 
                     Handed Over." data-toggle="tooltip" href="<?php echo site_url()."Handed_over_taken_over_transfer/view/".$value->transid.'/'.$value->Handed_id;?>"> <i class="fas fa-arrow-alt-circle-left"> </i> 
                    </a>
                     <a class="btn btn-success btn-xs" title = "Click here for 
                     Taken Over." data-toggle="tooltip" href="<?php echo site_url()."Handed_over_taken_over_transfer/view/".$value->transid.'/'.$value->Taken_id;?>"><i class="fas fa-arrow-alt-circle-left"></i> 
                    </a>
                      <a class="btn btn-success btn-xs" title = "Click here for 
                     Joining Report." data-toggle="tooltip" href="<?php echo site_url()."Joining_report_of_newplace_posting/view_transfer/".$value->tbl_joining_report_new_place_id;?>"> <i class="fas fa-file-invoice"></i></i>  
                    </a>




                    </a>


                
                <?php } ?>
              </td>
            </tr>
            <?php $i++; } ?>
          </tbody>
          <?php } ?>
        </table>
      </div>
    </div> 
  </div>
</div>



<div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
      <h4 class="col-md-7 panel-title pull-left">Seperation Approval List</h4>
    </div>
    <hr class="colorgraph"><br>
  </div>
  <div class="panel-body">
    <table id="staff_transfer_history" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
      <thead>
        <tr>
          <th class="text-center" style="width: 50px;">S.NO</th>
          <th class="text-center">Seperate Date</th>
          <th class="text-center">Staff</th>
          <th class="text-center">Type</th>
          <th class="text-center">Sender</th>
          <th class="text-center">Recevier</th>
          <th class="text-center">Request Date</th>
          <th class="text-center">Stage</th>
          <th class="text-center">Comment</th>
          <th class="text-center">Action</th>
        </tr>
      </thead>
      <?php if (count($staff_seperation)==0) { ?>
      <tbody>

      </tbody>
      <?php }else{ ?>
      <tbody>
        <?php
      // print_r($staff_seperation);
      // die;
        $i=0; foreach($staff_seperation as $grow){ 
                    //print_r($view_history);
          ?>
          <tr>
            <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
            <td class="text-center"><?php echo $this->gmodel->changedatedbformate($grow->proposeddate);?></td>
            <td class="text-center"><?php echo $grow->name; ?></td>
            <td class="text-center"><?php echo $grow->process_type; ?></td> 
            <td class="text-center"><?php echo $grow->sendername; ?></td> 
            <td class="text-center"><?php echo $grow->receivername; ?></td> 
            <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($grow->Requestdate); ?> </td> 
            <td class="text-center"><span class="badge badge-Pill badge-info col-md-12"><?php echo $grow->flag;?></span> </td> 
            <td class="text-center"><?php echo $grow->scomments;?> </td> 
            <td class="text-center">
              <?php if($grow->status == 1){ ?>
               <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip"  href="<?php echo base_url('Staff_approval/add/'.$grow->staffid.'/'.$grow->transid);?>"><i class="fas fa-thumbs-up"></i></a> 
              <?php 
            }else if($grow->status == 3){ ?>
            <span class="badge badge-Pill badge-success">Approved</span>
            <?php  }else if($grow->status == 4){?>
            <span class="badge badge-Pill badge-error">Rejected</span>

            <?php  }
            if($grow->status == 13 ){
              ?>
              <!-- <a href="<?php //echo base_url('Staff_sepdischargenotice/index/'.$grow->transid);?>" data-toggle="tooltip" title = " DISCHARGE NOTICE" class="btn btn-success btn-sm"><i class="fas fa-eject"></i> </a>
              <a href="<?php //echo base_url('Staff_sepservicecertificate/index/'.$grow->transid);?>" data-toggle="tooltip" title = "SERVICE CERTIFICATE" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i> </a> -->
               <a href="<?php echo base_url('Handed_over_taken_over_seperation/viewtaken/'.$grow->transid.'/'.$grow->tbl_handed_id);?>" data-toggle="tooltip" title = "Staff Taken over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                <a href="<?php echo base_url('Staff_sepemployeeexitform/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Staff Exitfoam" class="btn btn-success btn-sm"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>
              <a href="<?php echo base_url('Staff_sepclearancecertificate/index/'.$grow->transid);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fas fa-certificate"></i></a>
              <?php }if($grow->status == 15){ ?>
             <!--  <a href="<?php //echo base_url('Staff_sepdischargenotice/index/'.$grow->transid);?>" data-toggle="tooltip" title = " DISCHARGE NOTICE" class="btn btn-success btn-sm"><i class="fas fa-eject"></i></a>
              <a href="<?php //echo base_url('Staff_sepservicecertificate/index/'.$grow->transid);?>" data-toggle="tooltip" title = "SERVICE CERTIFICATE" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i></a> -->
              <a href="<?php echo base_url('Staff_sepclearancecertificate/view/'.$grow->tbl_clearance_certificate_id);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fas fa-certificate"></i></a>



            <?php } 

            else if($grow->status >= 19){
                          
                           if($grow->trans_status=='Death')
              {
                ?>
                <a href="<?php echo base_url('Staff_sepchecksheet/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Checksheet" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i></a>
                <a href="<?php echo base_url('Staff_sepclearancecertificate/view/'.$grow->tbl_clearance_certificate_id);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fas fa-certificate"></i></a>
           
              <?php } else if($grow->status == 27 && $this->loginData->RoleID==17)
             
               {
            ?>
            <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip"  href="<?php echo base_url('Staff_finance_approval/add/'.$grow->staffid.'/'.$grow->transid);?>"><i class="fas fa-thumbs-up"></i></a> 
          <?php } else {?>
            <a href="<?php echo base_url('Staff_sepchecksheet/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Checksheet" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i></a>
                <a href="<?php echo base_url('Staff_sepclearancecertificate/view/'.$grow->tbl_clearance_certificate_id);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fas fa-certificate"></i></a>

             <?php  if($grow->status == 27 && $grow->trans_status!='Death') {

                    ?>
                    <a href="<?php echo base_url('Handed_over_taken_over_seperation/viewtaken/'.$grow->transid.'/'.$grow->tbl_handed_id);?>" data-toggle="tooltip" title = "Staff Taken over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                    <?php 
                  }
                  
               


 } 

              ?>
           
                
               
                     <!--  <?php //if($this->loginData->RoleID==3 ||$this->loginData->RoleID==2|| $this->loginData->RoleID==17)  {?> -->
                     <?php  if($grow->trans_status!='Death') {?>

                 <a href="<?php echo base_url('Staff_sepemployeeexitform/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Staff Exitfoam" class="btn btn-success btn-sm"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>
                 <?php 
               }

                 if($grow->status == 25 && $grow->trans_status!='Death')
                 {
                   
                  ?>
                  
                   <a href="<?php echo base_url('Handed_over_taken_over_seperation/taken/'.$grow->transid);?>" data-toggle="tooltip" title = "Staff Taken over" class="btn btn-info btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                  <?php 
                   // }
                 }

                  else if($grow->status == 27 && $grow->trans_status!='Death') {

                    ?>
                    <a href="<?php echo base_url('Handed_over_taken_over_seperation/viewtaken/'.$grow->transid.'/'.$grow->tbl_handed_id);?>" data-toggle="tooltip" title = "Staff Taken over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                    <?php 
                  }
                  

              
          } 

                 else  if($grow->status == 25 && $grow->trans_status!='Death') {
                    ?>
                    <a href="<?php echo base_url('Handed_over_taken_over_seperation/viewtaken/'.$grow->transid.'/'.$grow->tbl_handed_id);?>" data-toggle="tooltip" title = " Taken over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                    <?php 
                  }
                  


          
            else  if($grow->status >= 15){
            ?>
             <a href="<?php echo base_url('Staff_sepchecksheet/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Checksheet" class="btn btn-success btn-sm"><i class="fab fa-servicestack"></i></a>
                <a href="<?php echo base_url('Staff_sepclearancecertificate/view/'.$grow->tbl_clearance_certificate_id);?>" data-toggle="tooltip" title = "CLEARANCE CERTIFICATE" class="btn btn-success btn-sm"><i class="fas fa-certificate"></i>
                </a>
           
              

            <?php 
          }
          ?>
            </td>
          <?php  $i++; } ?>
          </tr>
        </tbody>
        <?php  } ?>
      </table>
    </div>
  </div>

</div>   
<?php //} } ?>
</section>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="<?php echo site_url(); ?>Staff_approval/approvemodal" id="acceptancetcform" name="acceptancetcform" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" id="receiverstaffid" name="receiverstaffid" value="">
              <label for="Name">Notes <span style="color: red;" >*</span></label>
              <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes"> </textarea>
              <?php echo form_error("comments");?>
            </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetcmodal();">
          </div>
        </div>
      </form> 
    </div>
  </div>
</div> 
<!-- Modal -->

<!-- Modal Transfer Expense CLAIM-->
<div class="container">
  <div class="modal fade" id="myModaltransfer" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form action="<?php echo site_url(); ?>Staff_approval/transferapprovemodal" id="acceptancetcform_transfer" name="acceptancetcform_transfer" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" id="receiverstaffid_transfer" name="receiverstaffid_transfer" value="">
              <label for="Name">Notes <span style="color: red;" >*</span></label>
              <textarea class="form-control" data-toggle="" id="comments_transfer" name="comments_transfer" placeholder="Enter Notes"> </textarea>
              <?php echo form_error("comments_transfer");?>
            </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetransfermodal();">
          </div>
        </div>
      </form> 
    </div>
  </div>
</div> 
<!-- Modal -->

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    var groupColumn = 3;
              var table = $('#tablecampus').DataTable({
                "columnDefs": [
                { "visible": false, "targets": groupColumn }
                ],
                "order": [[ groupColumn, 'asc' ]],
                "displayLength": 11,
                "drawCallback": function ( settings ) {
                  var api = this.api();
                  var rows = api.rows( {page:'current'} ).nodes();
                  var last=null;

                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                      $(rows).eq( i ).before(
                        '<tr class="group font-weight-bold text-uppercase"><td colspan="11">'+group+'</td></tr>'
                        );

                      last = group;
                    }
                  } );
                }
              } );

    var groupColumn = 3;
              var table = $('#tablecampus1').DataTable({
                "columnDefs": [
                { "visible": false, "targets": groupColumn }
                ],
                "order": [[ groupColumn, 'asc' ]],
                "displayLength": 13,
                "drawCallback": function ( settings ) {
                  var api = this.api();
                  var rows = api.rows( {page:'current'} ).nodes();
                  var last=null;

                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                      $(rows).eq( i ).before(
                        '<tr class="group font-weight-bold text-uppercase"><td colspan="13">'+group+'</td></tr>'
                        );

                      last = group;
                    }
                  } );
                }
              } );

     var groupColumn = 3;
              var table = $('#tablecampus2').DataTable({
                "columnDefs": [
                { "visible": false, "targets": groupColumn }
                ],
                "order": [[ groupColumn, 'asc' ]],
                "displayLength": 15,
                "drawCallback": function ( settings ) {
                  var api = this.api();
                  var rows = api.rows( {page:'current'} ).nodes();
                  var last=null;

                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                      $(rows).eq( i ).before(
                        '<tr class="group font-weight-bold text-uppercase"><td colspan="15">'+group+'</td></tr>'
                        );

                      last = group;
                    }
                  } );
                }
              } );
 var groupColumn = 3;
              var table = $('#staff_transfer_history').DataTable({
                "columnDefs": [
                { "visible": false, "targets": groupColumn }
                ],
                "order": [[ groupColumn, 'asc' ]],
                "displayLength": 15,
                "drawCallback": function ( settings ) {
                  var api = this.api();
                  var rows = api.rows( {page:'current'} ).nodes();
                  var last=null;

                  api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                      $(rows).eq( i ).before(
                        '<tr class="group font-weight-bold text-uppercase"><td colspan="15">'+group+'</td></tr>'
                        );

                      last = group;
                    }
                  } );
                }
              } );

    


  });
  function approvemodalshow(staffid){
    document.getElementById("receiverstaffid").value = staffid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
  function approvemodalshowtransfer(staffid){
    document.getElementById("receiverstaffid_transfer").value = staffid;
    $('#myModaltransfer').removeClass('fade');
    $("#myModaltransfer").show();
  }
  function acceptancetcmodal(){
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }
    if(comments.trim() !=''){
      document.forms['acceptancetcform'].submit();
    }
  }

  function acceptancetransfermodal(){
    var comments = document.getElementById('comments_transfer').value;
    if(comments.trim() == ''){
      $('#comments_transfer').focus();
    }
    if(comments.trim() !=''){
      document.forms['acceptancetcform_transfer'].submit();
    }
  }
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>