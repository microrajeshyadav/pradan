<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Staffcategory" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
     <div class="container-fluid" style="margin-top: 20px;">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-sm-offset-4">
         <form method="POST" action="">
           <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Edit Staff Category</b></div>

            <div class="panel-body">

              <div class="form-group">
                <div class="form-line">
                  <label for="StateCode" class="field-wrapper required-field">Short Name <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control" id="shortname" minlength="3" maxlength="10" name="shortname" value="<?php echo $staffcategory_details->shortname;?>">
                </div>
                <?php echo form_error("statecode");?>
              </div>
              <div class="form-group">
                <div class="form-line">
                  <label for="StateNameEnglish" class="field-wrapper required-field">Category Name <span style="color: red;" >*</span> </label>
                  <input type="text" class="form-control" maxlength="50" minlength="3" name="categoryname" value="<?php echo $staffcategory_details->categoryname;?>">
                </div>
                <?php echo form_error("categoryname");?>
              </div>

              <div class="form-group">
                <div class="form-line">
                  <label for="SNS" class="field-wrapper required-field">Status <span style="color: red;" >*</span></label>
                  <?php   

                        //$options = array('' => 'Select Status');     
                  $options = array('0' => 'Active', '1' => 'InActive');
                  echo form_dropdown('status', $options, $staffcategory_details->status, 'class="form-control"'); 
                  ?>
                </div>
                <?php echo form_error("status");?>
              </div>

            </div>
            <div class="panel-footer text-right">
              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
              <a href="<?php echo site_url("Staffcategory/index");?>" class="btn btn-success btn-sm m-t-10 waves-effect">Go to List</a>
            </div>

          </div>
        </form>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>