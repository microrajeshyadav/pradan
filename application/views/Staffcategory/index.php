<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Staffcategory" && $row->Action == "index"){ ?>
 <br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <div class="container-fluid">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="row">
            <h4 class="col-md-10 panel-title pull-left"><b>Manage Staff Category</b></h4> 
               <div class="pull-right"> 
                <a href="<?php echo site_url("Staffcategory/add/")?>" class="btn btn-primary btn-xs">Add New Staff Category</a>
              </div>
        </div>
         <hr class="colorgraph"><br>
            <div class="panel-body">
                <table id="tblstaffcategory" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S. No.</th>
                      <th class="text-center">Short Name</th>
                      <th class="text-center">Category Name</th>
                      <th class="text-center" style="width: 50px;">Action</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php

                   // print_r($staffcategory_details);
                    $i=0; foreach($staffcategory_details as $row){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $row->shortname; ?></td>
                      <td class="text-center"><?php echo $row->categoryname;?></td>

                      <td class="text-center">
                        <a title="Click here to edit this Category." href="<?php echo site_url('Staffcategory/edit/'.$row->id);?>" style="padding : 4px;" title="Edit"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> 

                        <a title="Click here to delete this Category." href="<?php echo site_url('Staffcategory/delete/'.$row->id);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" title="Delete"><i class="fa fa-trash" style="color:red"></i></a>
                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>             
            </div>
          </div>
        </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
      <?php } } ?>
    </section>
    <script>

     $(document).ready(function() {
      $('#tblstaffcategory').DataTable({
        "paging": false,
        "search": false,
      });
    });

     function confirm_delete() {

      var r = confirm("Do you want to delete this Category ?");

      if (r == true) {
        return true;
      } else {
        return false;
      }

    }
  </script>