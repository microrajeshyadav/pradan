<br>
<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Da_event_detailing" && $row->Action == "index"){ ?>

    <div class="container-fluid">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

          <!-- Exportable Table -->

              <?php //print_r($daeventdetails_details);  ?>

              <div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row ">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     
     <h4 class="col-md-9 panel-title pull-left">Manage DA Event Detailing</h4>
     <div class="col-md-3 text-right" >
      

       <a data-toggle="tooltip" data-placement="bottom" title="Want to add New  DA Event Detailing? Click on me." href="<?php echo site_url()."Da_event_detailing/add";?>" class="btn btn-primary btn-sm">Add New DA Event Detailing</a>

     </div>
   </div>
   </div>
   <hr class="colorgraph"><br>
 </div>
 <div class="panel-body">


          
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tabledaevent" class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
             <tr>
              <th>S.No.</th>
              <!-- <th>Batch</th>
              <th>Financial Year</th> -->
              <th>Phase-Activities</th>
              <th>DA Details </th>
           </tr> 
         </thead>
         <tbody>

          <?php 
         // print_r($daeventdetails_details); 
            $i=0;
          foreach ($daeventdetails_details as $key => $value) {
         ?>
          <tr>
            <td><?php echo $i+1;?></td>
            <!-- <td><?php //echo $value->batch;?></td>
            <td><?php //echo $value->financialyear;?></td> -->
            <td><?php echo $value->phase_name;?></td>
             <td>
                <table id="tblForm09" class="table table-bordered table-striped" >
              <thead>
              <tr style="background-color: #eee; color: #000;">
                <th>Campus Name</th>
                <th>DA Name</th>
                <th>Gender</th>
                <th>Batch</th>
                <th>Financial Year</th>
                <th>From Date</th>
                <th>To Date</th>
                <th>Action</th>
              </tr>
             </thead>
             <tbody id="tbodyForm09">
              <?php
                $transaction_details = $this->model->getDAEventDetailsTransaction($value->mstphaseid);
                // print_r($transaction_details);
                foreach($transaction_details as $key => $val) {
              ?>
             <tr id="tbodyForm09" >
              <td><?php echo $val->campusname;?></td>
              <td> <?php echo $val->candidatefirstname.' '.$val->candidatemiddlename.' '.$val->candidatelastname;?></td>
              <td><?php echo $val->gender;?></td>
              <td><?php echo $val->batch;?></td>
              <td><?php echo $val->financialyear;?></td>
              <td><?php echo $this->model->changedatedbformate($val->fromdate);?></td>
              <td><?php echo $this->model->changedatedbformate($val->todate);?></td>
              <td>
              
              <a href="<?php echo site_url()."Da_event_detailing/edit/".$value->id;?>" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this DA Event Detailing." ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | 
              <a href="<?php echo site_url()."Da_event_detailing/delete/".$value->id;?>" onclick="return confirm_delete()" data-toggle="tooltip" data-placement="bottom" title = "Click here to Delete this DA Event Detailing."><i class="fa fa-trash" style="color:red"></i></a></td>
            </tr> 
            <?php  } ?>
            </tbody>
          </table>
             </td>
          </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
       </div>
                 
  
</div>
<!-- #END# Exportable Table -->
</div>
</div>

<?php } } ?>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tabledaevent').DataTable(); 
  });
</script>  
<script>

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
  
</script>