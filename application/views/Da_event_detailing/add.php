<br>
<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Da_event_detailing" && $row->Action == "index"){ ?>

    <div class="container-fluid">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

                  
              <form name="DAEventDetailing" id="DAEventDetailing" method="post" action="">
              <div class="body">
              <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                  <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-7 panel-title pull-left">Add DA Event Detailing</h4>
                     <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
                
                <div class="row">
                 <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                   <div class="form-group">
                    <div class="form-line">
                      <label for="TeamName" class="field-wrapper required-field">Batch<span style="color:red;">*</span></label>
                      <?php 
                      $options = array('' => 'Select Batch ');
                      foreach($batch_details as$key => $value) {
                        $options[$value->id] = $value->batch;
                      }
                      echo form_dropdown('batch', $options, set_value('batch'), 'class="form-control" id="batch" required="required"  '  );

                      
                      ?>
                    </div>
                    <?php echo form_error("team");?>
                  </div>
                </div> 
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Financial Year<span style="color:red;">*</span></label>
                    <div id="batchfinancialyear">
                    <input type="text" name="financialyear"  id="financialyear" class="form-control" value=" " required="required" readonly="readonly" >
                  </div>
                                        
                  </div>
                  <?php echo form_error("financialyear");?>
                </div>
              </div>-->

              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Phase-Activity<span style="color:red;">*</span></label>
                  <?php 
                  $options = array('' => 'Select Phase-Activity');
                  foreach($phase_details as$key => $value) {
                    $options[$value->id] = $value->phase_name;
                  }
                  echo form_dropdown('phase', $options, set_value('phase'), 'class="form-control" id="phase" required ' );
                  ?>
                </div>
                <?php echo form_error("phase");?>
              </div>
            </div>
            
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <table id="tblForm09" class="table table-bordered table-striped" >
              <thead>
               <tr>
                <th colspan="4" style="background-color: #eee; color: #000; " class="text-left;"> DA's event detailing</th>
               </tr> 
               <tr>
                 <th>Name</th>
                 <th>Batch</th>
                 <th>From Date</th>
                 <th>To Date</th>
               </tr>
             </thead>
             <tbody id="tbodyForm09" >
              <?php $i=0; foreach($candidate_details as $key => $value) {  ?>
               <tr id="tbodyForm09" >
                <td>
                  <div class="form-check" id="checkid">
                          <input type="checkbox" class="filled-in form-check-input" name="DAname[]" id="DAname_<?php echo $i; ?>" title="Check DA Name!" value="<?php echo $value->candidateid; ?>" onclick="getcandidatedaevent(this.id)" >
                          <label class="form-check-label" for="DAname_<?php echo $i; ?>"><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname .' ['.$value->mobile.' - '. $value->emailid .']'; ?> </label><br>
                        </div>
                </td>
                <td>
                  <input type="hidden" name="batch[<?php echo $value->candidateid; ?>]" id="batch_<?php echo $i; ?>" value="<?php echo $value->batchid; ?>">
                  <input type="hidden" name="financialyear[<?php echo $value->candidateid; ?>]" id="financialyear_<?php echo $i; ?>" value="<?php echo $value->financialyearid; ?>">
                  <?php echo $value->batch; ?>
                </td>
                <td>
                  <input type="text" name="fromdate[<?php echo $value->candidateid; ?>]" id="fromdate_<?php echo $i; ?>" value="" class="form-control datepicker" disable="" data-toggle="tooltip" onclick="changedatepicker(this.id)"  title="Select From Date !" placeholder="Select From Date" disabled="disabled" required="required" >
                </td>
                <td><input type="text" name="todate[<?php echo $value->candidateid; ?>]" id="todate_<?php echo $i; ?>" data-toggle="tooltip" onclick="changedatepicker(this.id)" title="Select To Date !" placeholder="Select To Date" value=""  disabled="disabled" class="form-control datepicker" required="required"  ></td>
              </tr>
            <?php  $i++; } ?>
            </tbody>
          </table>
        </div>
      </div>

       
     <div class="panel-footer text-right">
            <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
            <a href="<?php echo site_url("Da_event_detailing");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list Da_event_detailing.">Go to list</a> 
          </div>
    </form>
  </div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>
<script>

  $(document).ready(function() {
   
  changedatepicker(inc);
     $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
        // maxDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',
        
    });
  
  
});


  

function changedatepicker(id){

    $("#fromdate_"+id).datepicker({
        changeMonth: true,
        changeYear: true,
         minDate: 0,
       // maxDate: 'today',
        dateFormat : 'dd/mm/yy',
        yearRange: '1920:2030',
        onClose: function(selectedDate) {
        jQuery("#todate_"+id).datepicker("option", "minDate", selectedDate);
        }
    });

    $("#todate_"+id).datepicker({
         changeMonth: true,
         changeYear: true,
          minDate: 0,
        // maxDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',
          onClose: function(selectedDate) {
        jQuery("#fromdate_"+id).datepicker("option", "maxDate", selectedDate);
        }
    });
  }


$('#DAEventDetailing').submit(function(e){
     var ck_box = $('input[type="checkbox"]:checked').length;
    if(ck_box == 0){
      alert("Sorry !!! Please check atleast one checkbox!!!");
     return false;
    }

});


    $("#batch").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getfinancialyear/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
        console.log(data);
        $("#batchfinancialyear").html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });
 

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
  



function getcandidatedaevent(id){
   var indno =  id.replace('DAname_','');
   var DAname = $('#DAname_'+indno).val();
   changedatepicker(indno)
   
  if($('#DAname_'+indno).is(':checked')){
       $('#fromdate_'+indno).prop('disabled', false);
        $('#todate_'+indno).prop('disabled', false);
   
  }else{
     $('#fromdate_'+indno).prop('disabled', true);
     $('#todate_'+indno).prop('disabled', true);
     $('#DAname_'+indno).focus();
      return false;
  }
}   
  
</script>