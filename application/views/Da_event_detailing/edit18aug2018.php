<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Da_event_detailing" && $row->Action == "index"){ ?>

    <div class="container-fluid">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php //print_r($singledaeventdetails_details) ?>
          <!-- Exportable Table -->
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">

               <div class="header">
                <h2>Edit DA Event Detailing</h2><br>
              </div>
              <form name="DAEventDetailing" id="DAEventDetailing" method="post" action="">
              <div class="body">
                <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                   <div class="form-group">
                    <div class="form-line">
                      <label for="TeamName" class="field-wrapper required-field">Batch<span style="color:red;">*</span></label>
                      <?php 
                      $options = array('' => 'Select Batch ');
                      $selected_batch = $singledaeventdetails_details->batchid; 
                      foreach($batch_details as$key => $value) {
                        $options[$value->id] = $value->batch;
                      }
                      echo form_dropdown('batch', $options,  $selected_batch, 'class="form-control" id="batch"' );
                      ?>
                    </div>
                    <?php echo form_error("batch");?>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Financial Year<span style="color:red;">*</span></label>
                    <?php 
                    $options = array('' => 'Select Financial Year ');
                    $selected_financial = $singledaeventdetails_details->financial_year; 
                    foreach($batch_details as$key => $value) {
                      $options[$value->id] = $value->financial_year;
                    }
                    echo form_dropdown('financialyear', $options, $selected_financial,  'class="form-control" id="financialyear"' );
                    ?>
                  </div>
                  <?php echo form_error("financialyear");?>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Phase<span style="color:red;">*</span></label>
                  <?php 
                  $options = array('' => 'Select Phase ');
                  $selected_phase = $singledaeventdetails_details->phaseid; 
                  foreach($phase_details as$key => $value) {
                    $options[$value->id] = $value->phase_name;
                  }
                  echo form_dropdown('phase', $options,  $selected_phase, 'class="form-control" id="phase"' );
                  ?>
                </div>
                <?php echo form_error("phase");?>
              </div>
            </div>
            
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <table id="tblForm09" class="table table-bordered table-striped" >
              <thead>
               <tr>
                <th colspan="3" style="background-color: #3CB371; color: #fff;">
                  <div class="col-lg-12">
                    <div class="col-lg-6">DA's event detailing</div>
                    <div class="col-lg-6 text-right ">
                     <button type="button" id="btnRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                     <button type="button" id="btnAddRow" class="btn btn-warning btn-xs">Add</button>
                   </div>
                 </div>  
               </th>
               </tr> 
               <tr>
                 <th>DA Name</th>
                 <th>From Date</th>
                 <th>To Date</th>
               </tr>
             </thead>
             <tbody id="tbodyForm09" >
               <tr id="tbodyForm09" >
                <td>

                  <?php 
                  $options = array('' => 'Select DA Name ');
                   $selected_candidate = $singledaeventdetails_details->candidateid; 
                  foreach($candidate_details as$key => $value) {
                    $options[$value->candidateid] = $value->candidatefirstname;
                  }
                  echo form_dropdown('DAname[]', $options, $selected_candidate, 'class="form-control" data-toggle="tooltip" maxlength="50" title="Enter DA Name!" placeholder="Enter DA Name" required="required"' );
                  ?>
               </td>
                <td>
                  <input type="text" name="fromdate[]" id="fromdate" class="form-control datepicker" data-toggle="tooltip" maxlength="50" title="Enter From Date !" value="<?php echo date('d-m-Y',strtotime($singledaeventdetails_details->fromdate));?>" placeholder="Enter From Date " required="required" >
                </td>
                <td><input type="text" name="todate[]" id="todate" data-toggle="tooltip" title="Enter To Date !" value="<?php echo date('d-m-Y',strtotime($singledaeventdetails_details->todate));?>" class="form-control datepicker" required="required" ></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
       <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
             <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Save </button>
           <a href="<?php echo site_url("DA_event_detailing");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
            </div>
        </div>
    </div>
    </form>
  </div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>
<script>

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
  $("#btnRemoveRow").click(function() {
    if($('#tblForm09 tr').length-1>1)
      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);
    
    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    
  });


var srNoGlobal=0;
var inc = 0;

function insertRows(count) {
  srNoGlobal = $('#tbodyForm09 tr').length+1;
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;

  for (i = 1; i <= count; i++) {
    inc++
    cloneRow = lastRow.clone();
    var tableData = '<tr>'
    + '<td><select class="form-control" name="DAname['+inc+']" required="required">'
    + '<option value="">Select DA Name</option>'
    <?php foreach ($candidate_details as $key => $value): ?>
      + '<option value=<?php echo $value->candidateid;?>><?php echo $value->candidatefirstname;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'
    + '<input type="date" name="fromdate['+inc+']" id="fromdate['+inc+']" value="" class="form-control datepicker" data-toggle="tooltip" maxlength="50" title="Enter From Date!" placeholder="Enter From Date" >'
    + '</td>'
    + '<td><input type="date" class="form-control datepicker"'; 
    +'name="todate['+inc+']" id="todate['+inc+']" value=" " maxlength="100" required="required" />'
    + '</td>'
    + '</tr>';
    $("#tbodyForm09").append(tableData)
  }

}
  //insertRows();
</script>