<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - DISCHARGE NOTICE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h4> DISCHARGE NOTICE</h4> 
        </div>         
      </div>
      <div class="row">
        <div class="col-md-12 text-right" style="margin-bottom: 20px;">
         <h6><b><i>Confidential</i></b></h6>
         
       </div>
     
    </div> 

      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         No.: _______________
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         Date: _______________________
       </div>
    </div> 
    <div class="row" style="line-height: 2">
      <div class="col-md-12">
       To
      </div>
      <div class="col-md-12">
        Name: _____________________________
      </div>
      <div class="col-md-12">
       Employee Code: __________
      </div>
      <div class="col-md-12">
       Designation: ________________________
      </div>
      <div class="col-md-12">
       Location: __________________________
      </div>

    </div>

    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
      Subject : <b>Termination of Service </b>
     </div>
   </div>
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
      Dear _____________________,
     </div>
   </div>
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
     1. As per the terms and conditions of appointment governing employees of PRADAN, your services may be terminated by giving one month’s notice or salary in lieu thereof.
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
     2. The Competent Authority, in exercise of powers delegated to it by the Governing Board of PRADAN, has decided that your services with PRADAN be terminated with effect from the afternoon of ____________ (date).
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
    3.  The Competent Authority has also decided that in lieu of the notice period, you may be paid salary and accordingly you will stand relieved from your post in PRADAN with effect from the afternoon of __________________. A cheque for Rs ________ drawn in your favour on the ________________________________ bank is enclosed.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
   4. Please note that a sum of Rs _________ approximately is outstanding against your name after adjusting the amounts payable to you, as per details given below: <br>
   <table class="table">
     <thead>
       <tr>
         <th>
           #
         </th>
          <th>
           Dues on Account of
         </th>
          <th>
           Amount (In Rs.)
         </th>
       </tr>

     </thead>
     <tbody>
       <tr>
         <td>
           1.
         </td>
          <td>
           <input type="text" name="txtaccount" maxlength="50"  class="form-control" /> 
         </td>
          <td>
           <input type="text" name="" style="max-width: 120px;" class="form-control">
         </td>
          <tr>
         <td>
           2.
         </td>
          <td>
           <input type="text" name="txtaccount" maxlength="50"  class="form-control" /> 
         </td>
          <td>
           <input type="text" name="" style="max-width: 120px;" class="form-control">
         </td>
          <tr>
         <td>
           3.
         </td>
          <td>
           <input type="text" name="txtaccount" maxlength="50"  class="form-control" /> 
         </td>
          <td>
           <input type="text" name="" style="max-width: 120px;" class="form-control">
         </td>
       </tr>
         <tr>
         <td>
           4.
         </td>
          <td>
           <input type="text" name="txtaccount" maxlength="50"  class="form-control" /> 
         </td>
          <td>
           <input type="text" name="" style="max-width: 120px;" class="form-control">
         </td>
       </tr>

     </tbody>
     <tfoot>
        </tr>
         <tr>
         <td>
          
         </td>
          <td>
           <strong>Total</strong>
         </td>
          <td>
          <strong><label id="lbltotal"></label></strong>
         </td>
       </tr>

     </tfoot>
     


   </table>

   </div>
    <div class="col-md-12" style="margin-top: 20px;">
  5. Please also obtain a ‘Clearance Certificate’ (in duplicate) from all concerned in the enclosed prescribed proforma and furnish the same to the Finance-Personnel-MIS Unit as early as possible.
   </div>
    <div class="col-md-12" style="margin-top: 20px;">
   6. For any clarification, you may contact the Finance-Personnel-MIS Unit.
   </div>
   



   <div class="col-md-12" style="margin-top: 20px;">
   (___________________________)
   </div>
   <div class="col-md-12" style="margin-top: 20px;">
   Authorised Signatory
   </div>
 </div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
  <b>Encl.</b>: As above
 </div>
 <div class="col-md-12">
  cc: - Team Coordinator <br>
- Integrator <br>
- Finance-Personnel-MIS Unit<br>
- Executive Director (if not the Signatory of the Letter) <br>

 </div>

 
</div>
</div>
<div class="panel-footer text-right">  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>