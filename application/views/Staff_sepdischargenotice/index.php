<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;

}
textarea{
  width: 600px !important;
}
</style>
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
   <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - DISCHARGE NOTICE
       </h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <form method="POST" action="" name="discharge_notice" id="discharge_notice">
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
       <div class="panel-body">
        <div class="row">
          <div class="col-md-12 text-center">
            <h4> DISCHARGE NOTICE</h4> 
          </div>         
        </div>
        <div class="row">
          <div class="col-md-12 text-right" style="margin-bottom: 20px;">
           <h6><b><i>Confidential</i></b></h6>

         </div>

       </div> 

       <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         No.: <input type="text" class="inputborderbelow" name="sep_discharge_no" id="sep_discharge_no" maxlength="11" value="<?php if($discharge_detail){ echo $discharge_detail->sep_discharge_no;}?>" required>

       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         Date: <label name="todaydate" class="inputborderbelow"> <?php echo date('d/m/Y')  ?></label>
       </div>
     </div> 
     <div class="row" style="line-height: 2">
      <div class="col-md-12">
       To
     </div>
     <div class="col-md-12">
      <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->name; ?></label>
    </div>
    <div class="col-md-12">
     <label name="employeecode" class="inputborderbelow"> <?php echo $staff_detail->emp_code; ?></label>
   </div>
   <div class="col-md-12">
     <label name="designation" class="inputborderbelow"> <?php echo $staff_detail->sepdesig; ?></label>
   </div>
   <div class="col-md-12">
    <label name"location" class="inputborderbelow"><?php echo $staff_detail->newoffice; ?></label>
   </div>
 </div>


</div>

<div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    Subject : <b>Termination of Service </b>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    Dear  <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->name; ?></label>,
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
   1. As per the terms and conditions of appointment governing employees of PRADAN, your services may be terminated by giving one month’s notice or salary in lieu thereof.
 </div>
</div>
<div class="row" style="line-height: 2 ;">
  <div class="col-md-12" style="margin-top: 20px;">
    2. The Competent Authority, in exercise of powers delegated to it by the Governing Board of PRADAN, has decided that your services with PRADAN be terminated with effect from the afternoon of <input type="text" class="inputborderbelow datepicker" name="terminate_date" value="<?php if($discharge_detail){ echo $this->gmodel->changedatedbformate($discharge_detail->terminate_date);}?>" readonly required> (date).
 </div>
 <div class="col-md-12" style="margin-top: 20px;">
  3.  The Competent Authority has also decided that in lieu of the notice period, you may be paid salary and accordingly you will stand relieved from your post in PRADAN with effect from the afternoon of <input type="text" class="inputborderbelow datepicker" name="relieved_date" value="<?php if($discharge_detail){ echo $this->gmodel->changedatedbformate($discharge_detail->relieved_date);}?>" readonly required>. A cheque for Rs <input type="text"  name="cheque_rs" class="inputborderbelow text-right txtNumeric" required value="<?php if($discharge_detail){ echo $discharge_detail->cheque_rs;}?>">drawn in your favour on the <input type="text" name="bank_name" class="inputborderbelow text-left txtOnly" value="<?php if($discharge_detail){ echo $discharge_detail->bank_name;}?>" required>  bank is enclosed.
</div>
<div class="col-md-12" style="margin-top: 20px;">
 4. Please note that a sum of Rs <input type="text" value="<?php if($discharge_detail){ echo $discharge_detail->sum_rs;}?>" name="sum_rs" class="inputborderbelow text-right txtNumeric" required> approximately is outstanding against your name after adjusting the amounts payable to you, as per details given below: <br>
 <?php
//print_r($distransaction_detail->txtdue1); die();
 ?>  
 <table class="table">
   <thead>
     <tr>
       <th>
         #
       </th>
       <th>
         Dues on Account of
       </th>
       <th>
         Amount (In Rs.)
       </th>
     </tr>

   </thead>
   <tbody>
     <tr>

       <td>
         1.
       </td>
       <td>
         <input type="text" name="txtdue1" maxlength="50" value="<?php if($distransaction_detail){ echo $distransaction_detail->txtdue1;}?>" class="form-control inputborderbelow txtOnly" /> 
       </td>
       <td>
         <input type="text" name="dueamount1" style="max-width: 120px;" class="form-control inputborderbelow text-right dueamount txtNumeric" value="<?php if($distransaction_detail){ echo $distransaction_detail->dueamount1;}?>" min="0.00" max="100000.00" step="0.01" />
       </td>
     </tr>

     <tr>

       <td>
         2.
       </td>
       <td>
         <input type="text" name="txtdue2" maxlength="50" value="<?php if($distransaction_detail){ echo $distransaction_detail->txtdue2;}?>" class="form-control inputborderbelow txtOnly" /> 
       </td>
       <td>
         <input type="text" name="dueamount2" value="<?php if($distransaction_detail){ echo $distransaction_detail->dueamount2;}?>" style="max-width: 120px;" class="form-control inputborderbelow text-right dueamount txtNumeric" min="0.00" max="100000.00" step="0.01" />
       </td>

     </tr>

     <tr>

       <td>
         3.
       </td>
       <td>
         <input type="text" name="txtdue3" maxlength="50" value="<?php if($distransaction_detail){ echo $distransaction_detail->txtdue3;}?>" class="form-control inputborderbelow txtOnly" /> 
       </td>
       <td>
         <input type="text" name="dueamount3" style="max-width: 120px;" class="form-control inputborderbelow text-right dueamount txtNumeric" value="<?php if($distransaction_detail){ echo $distransaction_detail->dueamount3;}?>" min="0.00" max="100000.00" step="0.01" />
       </td>
     </tr>

     <tr>

       <td>
         4.
       </td>
       <td>
         <input type="text" name="txtdue4" maxlength="50" value="<?php if($distransaction_detail){ echo $distransaction_detail->txtdue4;}?>" class="form-control inputborderbelow txtOnly" /> 
       </td>
       <td>
         <input type="text" name="dueamount4" style="max-width: 120px;" class="form-control inputborderbelow text-right dueamount txtNumeric" value="<?php if($distransaction_detail){ echo $distransaction_detail->dueamount4;}?>" min="0.00" max="100000.00" step="0.01" />
       </td>

     </tr>

     <tr>

       <td>
         5.
       </td>
       <td>
         <input type="text" name="txtdue5" value="<?php if($distransaction_detail){ echo $distransaction_detail->txtdue5;}?>" maxlength="50"  class="form-control inputborderbelow txtOnly" /> 
       </td>
       <td>
         <input type="text" name="dueamount5" value="<?php if($distransaction_detail){ echo $distransaction_detail->dueamount5;}?>" style="max-width: 120px;" class="form-control inputborderbelow text-right dueamount txtNumeric" min="0.00" max="100000.00" step="0.01" />
       </td>

     </tr>

   </tbody>
   <tfoot>
   <tr>
     <td>
     </td>
     <td>
       <strong>Total</strong>
     </td>
     <td class="text-left">
      <strong><label id="lbltotal"  class="form-control text-right inputborderbelow" style="max-width: 120px;"><span id="lbltotal"></span>></label></strong>
    </td>
  </tr>

</tfoot>
</table>

</div>
<div class="col-md-12" style="margin-top: 20px;">
  5. Please also obtain a ‘Clearance Certificate’ (in duplicate) from all concerned in the enclosed prescribed proforma and furnish the same to the Finance-Personnel-MIS Unit as early as possible.
</div>
<div class="col-md-12" style="margin-top: 20px;">
 6. For any clarification, you may contact the Finance-Personnel-MIS Unit.
</div>




<div class="col-md-12" style="margin-top: 20px;">
 (<?php if(!empty($personnel_detail->name)) echo $personnel_detail->name;?>)
</div>
<div class="col-md-12" style="margin-top: 20px;">
 Authorised Signatory
</div>
</div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
  <b>Encl.</b>: As above
</div>
<div class="col-md-12">
  cc: - Team Coordinator <br>
  - Integrator <br>
  - Finance-Personnel-MIS Unit<br>
  - Executive Director (if not the Signatory of the Letter) <br>

</div>


</div>
</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($distransaction_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save & Submit" class="btn btn-success btn-sm">
  <?php
    }else{

      if($distransaction_detail->flag == 0){
 ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> 
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save & Submit" class="btn btn-success btn-sm">
  
<?php } ?>
 
<?php } ?>
 <a href="<?php echo site_url("Staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</form>
</div>
</div>
</section>
<?php 
if ($distransaction_detail){
if($distransaction_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  });
</script>
<?php }} ?>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
$(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd/mm/yy',
   });

  });

</script>
<script type="text/javascript">
  $(".dueamount").change(function(){
    calculatetotal();
  }); 
  function calculatetotal() {
    var total=0;
    
    $(".dueamount").each(function() {
     total += (isNaN(parseFloat($(this).val()))) ? 0 : parseFloat($(this).val()); 

   });

    $('#lbltotal').text(total.toFixed(2));

    // code
  }

$(document).ready(function(){ 
    var total=0;
    
    $(".dueamount").each(function() {
     total += (isNaN(parseFloat($(this).val()))) ? 0 : parseFloat($(this).val()); 

   });

    $('#lbltotal').text(total.toFixed(2));

    // code


});

$('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });

</script>



