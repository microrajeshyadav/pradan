<section class="content">
  <br>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-8 panel-title pull-left">Send HRD Intimation Details</h4>
                   <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
                  <form name="addIntemation" id="addIntemation" method="post" action="">
                    <div class="panel-body">
                      
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">DA Name <span style="color: red;" >*</span></label>
                        <select name="daname" id="daname"  required="required" class="form-control">
                         <option value="">Select</option>
                         <?php foreach ($getdadetails as $key => $value) {?>
                         <option value="<?php echo $value->staffid;?>"><?php echo $value->NAME;?></option>
                         <?php } ?>
                       </select>  
                     </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Intimation Type <span style="color: red;" >*</span></label>
                         <select name="intemation_type" id="intemation_type"  required="required" class="form-control">
                         <option value="">Select</option>
                         <?php foreach ($getintimation as $key => $value) {?>
                         <option value="<?php echo $value->id;?>"><?php echo $value->intimation;?></option>
                         <?php } ?>
                       </select>  
                     </div>
                   
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="probatextension1" style="display: none;">
                      <label for="Name">Probation Extension Date <span style="color: red;" >*</span></label>
                      <input type="text" name="probation_extension_date" id="probation_extension_date" class="form-control datepicker" >
                    </div>

                 <!--      <div id="hrlistid" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" required="required">
                        <label for="Name">Select HR <span style="color: red;" >*</span></label>
                          <select class="form-control" name="hrid" id="hrid">
                          <option value="">Select HR</option>
                          <?php 
                            foreach($hrlist as $res){ ?>
                          <option value="<?php echo $res->staffid; ?>">
                           <?php echo $res->name; ?></option>
                          <?php } ?>
                        </select> 
                     </div> -->

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Comments <span style="color: red;" >*</span></label>
                      <textarea name="comments" id="comments" class="form-control" required="required"></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right">
                      <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-warning btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save and send your changes? Click on me.">Submit & Send </button>
                      <a href="<?php echo site_url("Hrd_intemation1");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                   </div>
               
               </div>
             </form>               
           </div>
         </div><!-- /.panel-->

       </div>

     </div>

   </div>

 </div>
 <!-- #END# Exportable Table -->
</div>
</section>

<script>
  $(document).ready(function(){

    $(".datepicker").datepicker({
      changeMonth: true,
        changeYear: true,
        minDate: 'today',
        yearRange: '1980:2025',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
    });

    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });

  function getcandidateid(id){
    var string = id;
    $('#candidateid').val(string);
  }


</script>
<script type="text/javascript">
  $('#intemation_type').change(function(){
   
    if($('#intemation_type').val() == 6)
    {

      
      $('#probation_extension_date').prop('required',true);
     $('#probatextension').css('display', 'block');
      $('#probatextension1').css('display', 'block');
     $('#probatextension').css('display', 'block');
   }else if ($('#intemation_type').val() == 4) {
      $('#probatextension').css('display', 'none');
     $('#probatextension').css('display', 'none');
     $('#probatextension1').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
     $('#probatextension').prop('required',false);
     $('#probatextension1').prop('required',false);
     $('#eprobation_extension_date').prop("disabled", true);
     $('#probatextension').prop("disabled", true);
     $('#probatextension1').prop("disabled", true);
     $('#probation_completed').prop("disabled", true);

   }else if ($('#intemation_type').val() == 1) {
     $('#probatextension').css('display', 'none');
     $('#probatextension1').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
     $('#probatextension').prop('required',false);
     $('#probatextension1').prop('required',false);
      $('#eprobation_extension_date').prop("disabled", true);
     $('#probatextension').prop("disabled", true);
     $('#probatextension1').prop("disabled", true);
     $('#probation_completed').prop("disabled", true);
   }else if ($('#intemation_type').val() == 5) {
     $('#probatextension').css('display', 'none');
     $('#probatextension1').css('display', 'none');
     $('#probation_extension_date').prop('required',false);
     $('#probation_extension_date').prop("disabled", true);
     $('#probatextension').prop('required',false);
     $('#probatextension1').prop('required',false);
     
    
   }
  else{

     $('#probatextension').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
   }
 });

 // $('#probation_completed').change(function(){
 //    if($('#probation_completed').val() == 2)
 //    {
 //     $('#probatextension1').css('display', 'block');
 //     $('#probatextension').css('display', 'block');
 //   }else if($('#probation_completed').val() == 1){
 //     $('#probatextension').css('display', 'block');
 //      $('#probatextension1').css('display', 'none');
 //     $('#eprobation_extension_date').prop('required',false);
 //   }
 //  else{
 //      $('#probatextension').css('display', 'none');
 //     $('#probatextension1').css('display', 'none');
 //     $('#eprobation_extension_date').prop('required',false);
 //   }
 // });


</script>