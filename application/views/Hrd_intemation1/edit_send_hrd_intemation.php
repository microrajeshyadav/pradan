<section class="content">
  <br>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-8 panel-title pull-left">Send HRD Intimation Details</h4>
                   <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <?php //print_r($getprobationseparation); ?>
              <div class="panel-body">
                  <form name="addIntemation" id="addIntemation" method="post" action="">
                    <input type="hidden" name="candidateid" value="<?php echo  $getprobationseparation->staffid;?>" >
                    <div class="panel-body">
                      <div class="row"> 
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">DA Name <span style="color: red;" >*</span></label>
                        <select name="daname" id="daname"  required="required" class="form-control">
                         <option value="">Select</option>
                         <?php foreach ($getdadetails as $key => $value) {
                            if ($value->staffid == $getprobationseparation->staffid) { ?>
                         <option value="<?php echo $value->staffid;?>" SELECTED><?php echo $value->name;?></option>
                       <?php }else{ ?>
                         <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>
                         <?php } } ?>
                       </select>
                     </div>
                                           
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Intemation Type <span style="color: red;" >*</span></label>
                         <select name="intemation_type" id="intemation_type"  required="required" class="form-control">
                         <option value="">Select</option>
                         <?php foreach ($getintimation as $key => $value) {
                          if($value->id == $getprobationseparation->intemation_type){?>
                         <option value="<?php echo $value->id;?>" SELECTED><?php echo $value->intimation;?></option>
                         <?php }else{ ?>
                           <option value="<?php echo $value->id;?>"><?php echo $value->intimation;?></option>
                         <?php } } ?>
                       </select>  
                     </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="probatextension1" style="display: none;">
                      <label for="Name">Probation Extension Date <span style="color: red;" >*</span></label>
                      <input type="text" name="probation_extension_date" id="probation_extension_date" class="form-control datepicker" value="<?php if(!empty($getprobationseparation->probation_completed_date) && $getprobationseparation->probation_completed_date !='') echo 
                      $this->gmodel->changedatedbformate($getprobationseparation->probation_completed_date); ?> ">
                    </div>

                       <div id="hrlistid" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" required="required">
                        <label for="Name">Select HR <span style="color: red;" >*</span></label>
                          <select class="form-control" name="hrid" id="hrid">
                          <option value="">Select HR</option>
                          <?php 
                            foreach($hrlist as $res){ 
                              if ($res->staffid == $getprobationseparation->hrid) {
                              ?>
                          <option value="<?php echo $res->staffid; ?>" SELECTED>
                           <?php echo $res->name; ?></option>
                          <?php } else{ ?>
                             <option value="<?php echo $res->staffid; ?>">
                           <?php echo $res->name; ?></option>
                         <?php  } } ?>
                        </select> 
                     </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Comments </label>
                      <textarea name="comments" id="comments" class="form-control" required="required"><?php echo $getprobationseparation->comment; ?></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right"">
                     <button type="reset" value="reset" class="btn btn-warning  btn-sm" data-toggle="tooltip" title="Want to reset your changes? Click on me.">Reset </button>
                      <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-success btn-sm" data-toggle="tooltip" title="Want to save and send your changes? Click on me.">Submit & Send </button>
                      <a href="<?php echo site_url("Hrd_intemation");?>" class="btn btn-dark btn-sm " data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                   </div>
                 </div>
               </div>
             </form>               
           </div>
         </div><!-- /.panel-->

       </div>

     </div>

   </div>

 </div>
 <!-- #END# Exportable Table -->
</div>
</section>

<script>
  $(document).ready(function(){
   var intemationtype = $('#intemation_type').val();
   //alert(intemationtype);

    if(intemationtype == 6)
    {
     $('#probatextension').css('display', 'block');
     $('#probatextension1').css('display', 'block');
     $('#eprobation_extension_date').prop('required',true);
   }else{
     $('#probatextension1').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
   }


  $(".datepicker").datepicker({
      changeMonth: true,
        changeYear: true,
        minDate: 'today',
        yearRange: '1980:2025',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
    });




   
  });

  function getcandidateid(id){
    var string = id;
    $('#candidateid').val(string);
  }


</script>
<script type="text/javascript">



  $('#intemation_type').change(function(){
   // alert($('#intemation_type').val());

    if($('#intemation_type').val() == 4)
    {
     $('#probatextension').css('display', 'block');
      $('#probatextension1').css('display', 'block');
     $('#eprobation_extension_date').prop('required',true);
   }
   else if($('#intemation_type').val() == 4)
   {
     $('probatextension').css('display', 'block');
     $('#probatextension1').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
      $('#probation_completed').prop('required',false);
     $('#eprobation_extension_date').prop('disabled',true);
     
   }
    else if($('#intemation_type').val() == 1)
   {
     $('#probatextension').css('display', 'none');
     $('#probatextension1').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
     $('#probation_completed').prop('required',false)
     $('#probation_completed').prop('disabled',true);
   }
   // else{

   //   $('#probatextension').css('display', 'none');
   //   $('#eprobation_extension_date').prop('required',false);
   // }
 });


  $('#intemation_type').change(function(){
    //alert($('#probation_completed').val());

    if($('#probation_completed').val() == 2)
    {
     $('#probatextension').css('display', 'block');
     $('#probatextension1').css('display', 'block');
     $('#eprobation_extension_date').prop('required',true);
   }else{

     $('#probatextension1').css('display', 'none');
     $('#probation_extension_date').prop('required',false);
     $('#probation_extension_date').prop('disabled',true);
   }
 });

</script>