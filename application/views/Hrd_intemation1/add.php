<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
        <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Campus Details</b></div>

        <div class="panel-body">
           <form name="campus" action="" method="post" > 
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">State <span style="color: red;" >*</span></label>
                <?php 
                  $options = array('' => 'Select State');
                  foreach($statedetails as$key => $value) {
                      $options[$value->id] = $value->name;
                  }
                  echo form_dropdown('stateid', $options, set_value('stateid'), 'class="form-control"');
                  ?>

                  <?php echo form_error("stateid");?>
                </div>
                
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">City <span style="color: red;" >*</span></label>
                <input type="text"  minlength="4" maxlength="50"  data-toggle="" name="city" id="telephone" class="form-control" value="<?php echo set_value("city");?>" placeholder="Enter City Name " >
                <?php echo form_error("city");?>
                </div>
                
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Campus Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" data-toggle="" id="campusname" name="campusname" placeholder="Enter Campus Name " value="<?php echo set_value("campusname");?>" >
                 <?php echo form_error("campusname");?>
                </div>
             
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Campus  Incharge <span style="color: red;" >*</span></label>
                 <input type="text" name="campusincharge" data-toggle="" minlength="10" maxlength="50" class="form-control" value="<?php echo set_value("campusincharge");?>" placeholder="Enter Campus  Incharge" >
                  <?php echo form_error("campusincharge");?>
                </div>
               
                   
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>
                 <input type="email" minlength="10" maxlength="80"  data-toggle="" name="emailid" id="emailid" class="form-control" value="<?php echo set_value("emailid");?>" placeholder="Enter Email Id" >
                  <?php echo form_error("emailid");?>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Telephone No.<!-- (use hyphen in Telephone number) --> <span style="color: red;" >*</span></label>
                   <input type="text"  maxlength="12"  data-toggle="" name="telephone" id="telephone" class="form-control" value="<?php echo set_value("telephone");?>" placeholder="Enter Telephone No " >
                     <?php echo form_error("telephone");?>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Fax No.<!-- (use hyphen in Telephone number) --> <span style="color: red;" >*</span></label>
                   <input type="text"  maxlength="12"  data-toggle="" name="fax" id="fax" class="form-control" value="<?php echo set_value("telephone");?>" placeholder="Enter Fax No " >
                     <?php echo form_error("fax");?>
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Mobile No 1. <span style="color: red;" >*</span></label>
                 <input type="text"  maxlength="10" data-toggle="" name="mobile1" id="mobile1" class="form-control" data-country="India" value="<?php echo set_value("mobile1");?>" placeholder="Enter Mobile No">
                  <?php echo form_error("mobile");?>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Mobile No 2.</label>
                 <input type="text"  maxlength="10" data-toggle="" name="mobile2" id="mobile2" class="form-control" data-country="India" value="<?php echo set_value("mobile2");?>" placeholder="Enter Mobile No">
                </div>

                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Address  <span style="color: red;" >*</span></label>
                 <input type="text" name="address" id="address" class="form-control"  placeholder="Enter Address " value="<?php echo set_value("address");?>"> 
                </div>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                         <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Save </button>
                          <a href="<?php echo site_url("Campus");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                      </div>  
                </div>
               </div>
                 </form> 
                </div>
              </div><!-- /.panel-->

            </div>

          </div>

        </div>

      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>