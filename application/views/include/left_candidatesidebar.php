
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar" >

  <!-- User Info -->
  <div class="user-info">
    <div class="image">
  <img src="<?php echo site_url('common/backend/images/user.png');?>" width="48" height="48" alt="logo" />
</div>
<div class="info-container">
  <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
  <div class="email"></div>
  <div class="btn-group user-helper-dropdown">
    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
    <ul class="dropdown-menu pull-right">
      <!-- <li><a href="#"><i class="material-icons">person</i>Profile</a></li> -->
      <li role="seperator" class="divider"></li>
      <!-- <li><a href="#"><i class="material-icons">person</i>Change Password</a></li> -->
      <li role="seperator" class="divider"></li>
      <li><a href="<?php echo site_url('candidate/login/logout');?>"><i class="material-icons">input</i>Sign Out</a></li>
</ul>
</div>
</div>
</div>
<!-- #User Info -->
<!-- Menu -->

<div class="menu">
  <ul class="list">
    <li class="header" >MAIN NAVIGATION</li>
    <li class="active">
      <a href="javascript:void(0);" class="menu-toggle">
       <i class="material-icons">home</i>
        <span>Home</span>
      </a>
      <ul class="ml-menu">    
   
      <!--  <li>
          <a href="<?php //echo site_url("Candidatedfullinfo");?>">
            <i class="material-icons col-green"></i>
            <span>Candidate Information</span>
          </a>
        </li> -->
    
        </ul>
    </li>
    

   
  </ul>
 
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
  <div class="copyright">
    &copy; 2018 - 2019 <a href="javascript:void(0);">PRADAN</a>.
  </div>
  <div class="version">
    <!-- <b>Version: </b> 1.0.5 -->
  </div>
</div>
<!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->