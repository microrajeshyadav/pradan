<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Pradan | HRMS</title>
    <!-- Favicon-->
    <!--  <link rel="icon" href="favicon.ico" type="image/x-icon"> -->
    <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">

    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script> 
    <script src="<?php echo site_url('common/backend/vendor/jquery/decimal.js');?>"></script>

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
    <script src="<?php echo site_url('common/backend/js/jquery-3.2.1.slim.min');?>" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!--  <script src="<?php echo site_url('common/backend/js/popper.min.js');?>" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script> -->

    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.5/umd/popper.js"></script> -->
    <script src="<?php echo site_url('common/backend/js/cdnjs-popper.js');?>"></script>
    
    <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.js"></script> -->
    <script src="<?php echo site_url('common/backend/js/maxcdn-bootstrap.js');?>"></script>
    
    <!--  <script src="<?php echo site_url('common/backend/vendor/bootstrap-4.1.3-dist/js/bootstrap.js');?>"></script> -->
    <script src="<?php echo site_url('common/backend/jquery-1.12.4.js');?>"></script>
    <script src="<?php echo site_url('common/backend/jquery-ui.js');?>"></script>
    <!-- <script src="<?php //echo site_url('common/backend/js/jquery.dataTables.min.js');?>"></script> -->
    <!-- <script src="<?php //echo site_url('common/backend/js/dataTables.bootstrap4.min.js');?>"></script> -->

    <!-- <link href="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" rel="stylesheet"> -->


<!-- datatable  -->
<!-- js -->
<!-- <script src="<?php //echo site_url('common/DataTable-js');?>"></script> -->
<script src="<?php echo site_url('common/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/js/jquery.dataTables.js');?>"></script>
<script src="<?php echo site_url('common/js/dataTables.responsive.min.js');?>"></script>
<script src="<?php echo site_url('common/js/buttons/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/js/buttons.flash.min.js');?>"></script>
<script src="<?php echo site_url('common/js/buttons/buttons.html5.min.js');?>"></script>

<!-- css -->
<!-- <link href="<?php //echo site_url('common/DataTable-css');?>" rel="stylesheet"> -->
<link href="<?php echo site_url('common/css/responsive.dataTables.min.css');?>" rel="stylesheet">
<link href="<?php echo site_url('common/css/jquery.dataTables.min.css');?>" rel="stylesheet">
<link href="<?php echo site_url('common/css/dataTables.bootstrap4.css');?>" rel="stylesheet">
<link href="<?php echo site_url('common/css/buttons.dataTables.min.css');?>" rel="stylesheet">
<link href="<?php echo site_url('common/css/responsive.bootstrap4.css');?>" rel="stylesheet">

         
    <link href="<?php echo site_url('common/backend/css/jquery-ui.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('common/backend/css/bootstrap.css');?>" rel="stylesheet">
    <!-- <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet"> -->
    <!-- <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"> -->
    <link href="<?php echo site_url('common/backend/css/custom.min.css');?>" rel="stylesheet">


    <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap-4.1.3-dist/css/bootstrap.css');?>" rel="stylesheet">

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />
    <!-- Bootstrap Form Helper Css -->
    <link href="<?php echo site_url('common/backend/dist/css/bootstrap-formhelpers.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('common/backend/dist/css/bootstrap-formhelpers.min.css');?>" rel="stylesheet">

    <link href="<?php echo site_url('common/backend/assets/fontawesome-free-5.6.3-web/css/all.css');?>" rel="stylesheet" />



    <link href="https://fonts.googleapis.com/css?family=Oxygen:400,700" rel="stylesheet">
    <!-- <link href="<?php echo site_url('common/backend/css/google-family');?>" rel="stylesheet"> -->

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('common/backend/monthcalender/css/stylesheet.css');?>">

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('common/backend/monthcalender/css/calendar.css');?>">

    <script type="text/javascript" src="<?php echo site_url('common/backend/monthcalender/js/calendar.min.js');?> "></script>
    <script src="<?php echo site_url('common/backend/js/pair-select.min.js');?>"></script>
    <script src="<?php echo site_url('common/backend/js/main.js');?>"></script>

    <link rel="stylesheet" href="<?php echo site_url('common/dist/summernote-bs4.css');?>">
    <script type="text/javascript" src="<?php echo site_url('common/dist/summernote-bs4.js');?>"></script>
    
    <script type="text/javascript">
       $(document).ready(function() {
         $('.summernote').summernote({
            height: 300,
            tabsize: 2,
            toolbar: [
               // ["style", ["style"]],
               ["font", ["bold", "underline", "clear"]],
               ['fontsize', ['fontsize']],
               //["fontname", ["fontname"]],
                //["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]]
                //["table", ["table"]],
                //["insert", ["link", "picture", "video"]],
                //["view", ["fullscreen", "codeview", "help"]]
                ],
            });
     });

 </script>
 <style type="text/css">
     .note-editor .note-editable {
        padding: 10px 27px !important;
        overflow: auto;
        outline: 0;
    }
    th, td {

        valign: top;
    }
    b, strong

    .form-control {

        font-size: 0.9rem !important;

    }

    .inputborderbelow
    {
        min-width: 5px; 
        max-width:600px; 
        border-radius: 0px; 
        border: none; 
        border-bottom: 1px solid black;
        padding-left: 10px; padding-right: 10px;
        min-width: 100px;
    }
    .checkbox-inline, .radio-inline {
        position: relative;
        display: inline-block;
        padding-left: 20px;
        margin-bottom: 0;
        font-weight: 400;
        vertical-align: middle;
        cursor: pointer;
    }

    .btn-group-xs > .btn, .btn-xs {
        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
    }

    .blue-tooltip + .tooltip > .tooltip-inner {background-color: #0C73CC;}
    .blue-tooltip + .tooltip > .tooltip-arrow { border-bottom-color:#0C73CC; }

    .cnt-block figure{
     width:148px; 
     height:148px; 
     border-radius:100%; 
     display:inline-block;
     margin-bottom: 15px;
 }
 .cnt-block img{ 
     width:148px; 
     height:148px; 
     border-radius:100%; 
 }

 .card-title.calendar-groot{
    position: absolute;
    width: 100%;
    top: 10px;
    right: 0px;
    font-size: 20px;
    text-shadow: 2px 2px 2px rgba(0,0,0,0.5);
    text-align: center;
}

.card-text.calendar-groot{
    margin-top: 25px;
    margin-bottom: 15px;
    font-size: 14px;
}

.calendar-poster{
    position: absolute;
    right: 0px;
    bottom: 0px;
    font-size: 12px;
    padding-right: 8px;
    padding-left: 15px;
    padding-bottom: 1px;
    border-top: 1px solid white;
    border-left: 1px solid white;
    border-top-left-radius: 30px;
    
}

.calendar-groot.green{
    background: rgb(191,210,85);
    background: -moz-linear-gradient(-60deg,  rgba(191,210,85,1) 0%, rgba(142,185,42,1) 50%, rgba(114,170,0,1) 51%, rgba(158,203,45,1) 100%);
    background: -webkit-linear-gradient(-60deg,  rgba(191,210,85,1) 0%,rgba(142,185,42,1) 50%,rgba(114,170,0,1) 51%,rgba(158,203,45,1) 100%);
    background: linear-gradient(150deg,  rgba(191,210,85,1) 0%,rgba(142,185,42,1) 50%,rgba(114,170,0,1) 51%,rgba(158,203,45,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bfd255', endColorstr='#9ecb2d',GradientType=1 );
    color: #fff;
}

.calendar-groot.gray{
    background: rgb(226,226,226);
    background: -moz-linear-gradient(-60deg,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%);
    background: -webkit-linear-gradient(-60deg,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%);
    background: linear-gradient(150deg,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=1 );
}

.calendar-groot.blue{
    background: rgb(183,222,237);
    background: -moz-linear-gradient(-60deg,  rgba(183,222,237,1) 0%, rgba(113,206,239,1) 50%, rgba(33,180,226,1) 51%, rgba(183,222,237,1) 100%);
    background: -webkit-linear-gradient(-60deg,  rgba(183,222,237,1) 0%,rgba(113,206,239,1) 50%,rgba(33,180,226,1) 51%,rgba(183,222,237,1) 100%);
    background: linear-gradient(150deg,  rgba(183,222,237,1) 0%,rgba(113,206,239,1) 50%,rgba(33,180,226,1) 51%,rgba(183,222,237,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b7deed', endColorstr='#b7deed',GradientType=1 );
}

.calendar-groot.red{
    background: rgb(248,80,50);
    background: -moz-linear-gradient(-60deg,  rgba(248,80,50,1) 0%, rgba(241,111,92,1) 50%, rgba(246,41,12,1) 51%, rgba(240,47,23,1) 71%, rgba(231,56,39,1) 100%);
    background: -webkit-linear-gradient(-60deg,  rgba(248,80,50,1) 0%,rgba(241,111,92,1) 50%,rgba(246,41,12,1) 51%,rgba(240,47,23,1) 71%,rgba(231,56,39,1) 100%);
    background: linear-gradient(150deg,  rgba(248,80,50,1) 0%,rgba(241,111,92,1) 50%,rgba(246,41,12,1) 51%,rgba(240,47,23,1) 71%,rgba(231,56,39,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f85032', endColorstr='#e73827',GradientType=1 );
}

.calendar-groot.orange{
    background: rgb(254,204,177);
    background: -moz-linear-gradient(-60deg,  rgba(254,204,177,1) 0%, rgba(241,116,50,1) 50%, rgba(234,85,7,1) 51%, rgba(251,149,94,1) 100%);
    background: -webkit-linear-gradient(-60deg,  rgba(254,204,177,1) 0%,rgba(241,116,50,1) 50%,rgba(234,85,7,1) 51%,rgba(251,149,94,1) 100%);
    background: linear-gradient(150deg,  rgba(254,204,177,1) 0%,rgba(241,116,50,1) 50%,rgba(234,85,7,1) 51%,rgba(251,149,94,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feccb1', endColorstr='#fb955e',GradientType=1 );
}

.calendar-groot.brown{
    background: rgb(243,226,199);
    background: -moz-linear-gradient(-60deg,  rgba(243,226,199,1) 0%, rgba(193,158,103,1) 50%, rgba(182,141,76,1) 51%, rgba(233,212,179,1) 100%);
    background: -webkit-linear-gradient(-60deg,  rgba(243,226,199,1) 0%,rgba(193,158,103,1) 50%,rgba(182,141,76,1) 51%,rgba(233,212,179,1) 100%);
    background: linear-gradient(150deg,  rgba(243,226,199,1) 0%,rgba(193,158,103,1) 50%,rgba(182,141,76,1) 51%,rgba(233,212,179,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3e2c7', endColorstr='#e9d4b3',GradientType=1 );
}

#counter {    
    background-color: #30CAB0;
    color: #fff;
    display: block;
    overflow: hidden;
    text-align: center;
    padding: 30px 0;
}
#counter .count {
    padding: 10px;
    background: rgba(255, 255, 255, 0.1);
    color: #fff;
    text-align: center;
}
.count h4 {
    color: #fff;
    font-size: 16px;
    margin-top: 0;
}
#counter .count .fa {
    font-size: 40px;
    display: block;
    color: #fff;
}
#counter .number {
    font-size: 30px;
    font-weight: 700;
    margin: 0;
}
/*  Pricing Section  */

#pricing {
    background: #f7f7f7;
}
.pricing-items {
    padding-top: 50px;
}
.pricing-item {
    background: #fff;
    position: relative;
    box-shadow: 0 0 9px 0 rgba(130, 121, 121, .2);
    padding: 50px 0px;
    border-top-right-radius: 2em;
    border-bottom-left-radius: 2em;
}
.pricing-item.active {
    top: -50px;
}
.price-list li {
    list-style: none;
    margin-bottom: 15px;
}

.price-list .price {
    font-size: 30px;
    font-weight: bold;
}
.pricing-item .ribbon {
    margin: -50px 0 20px;
    display: block;
    background-color: #ffb400;
    padding: 15px 0 2px;
    opacity: 0.8;
    border-top-right-radius: 2em;
}
.pricing-item:hover .ribbon {
    background-color: #ffb400;
    opacity: 1;
}
.pricing-item.active .ribbon {
    background-color: #ffb400;
    opacity: 1;
}
.pricing-item .ribbon p {
    color: #fff;
    font-size: 22px;
    margin: 0 0 10px;
    font-weight: 600;
}
.pricing-item.active .price-list li, .pricing-item:hover .price-list li {
    color: #848181;;
}
ul.price-list {
    margin-bottom: 30px;
    padding: 0;
}
.btn-price {
    display: inline-block;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    padding: 14px 40px;
    -webkit-border-radius: 26px;
    -moz-border-radius: 26px;
    -o-border-radius: 26px;
    border-radius: 26px;
    border: 1px solid #848181;
    background-color: transparent;
    color: #848181;
}
.pricing-item:hover .btn-price,
.active .btn-price {
    border: 1px solid #ffb400;
    background-color: #ffb400;
    color: #fff;
}
.btn-price:focus {
    background-color: transparent;
    text-decoration: none;
}
@media only screen and (max-width: 767px) {
    .pricing-items {
        padding-top: 0;
    }
    .pricing-item.active {
        top: 20px;
        margin-bottom: 40px;
    }
}
.highlegtedcount
{

  font-size: 20px;
}
.customheading
{

    font-size: 14px;
    font-weight: 700;
    margin: 0;
    
    margin-bottom: 5px;

}
.progress{
    width: 75px;
    height: 75px;
    line-height: 75px;
    background: none;
    margin: 0 auto;
    box-shadow: none;
    position: relative;
}
.progress:after{
    content: "";
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 12px solid #fff;
    position: absolute;
    top: 0;
    left: 0;
}
.progress > span{
    width: 50%;
    height: 100%;
    overflow: hidden;
    position: absolute;
    top: 0;
    z-index: 1;
}
.progress .progress-left{
    left: 0;
}
.progress .progress-bar{
    width: 100%;
    height: 100%;
    background: none;
    border-width: 12px;
    border-style: solid;
    position: absolute;
    top: 0;
}
.progress .progress-left .progress-bar{
    left: 100%;
    border-top-right-radius: 80px;
    border-bottom-right-radius: 80px;
    border-left: 0;
    -webkit-transform-origin: center left;
    transform-origin: center left;
}
.progress .progress-right{
    right: 0;
}
.progress .progress-right .progress-bar{
    left: -100%;
    border-top-left-radius: 80px;
    border-bottom-left-radius: 80px;
    border-right: 0;
    -webkit-transform-origin: center right;
    transform-origin: center right;
    animation: loading-1 1.8s linear forwards;
}
.progress .progress-value{
    width: 90%;
    height: 90%;
    border-radius: 50%;
    background: #44484b;
    font-size: 16px;
    color: #fff;
    line-height: 62px;
    text-align: center;
    position: absolute;
    top: 5%;
    left: 5%;
}
.progress.blue .progress-bar{
    border-color: #049dff;
}
.progress.blue .progress-left .progress-bar{
    animation: loading-2 1.5s linear forwards 1.8s;
}
.progress.yellow .progress-bar{
    border-color: #fdba04;
}
.progress.yellow .progress-left .progress-bar{
    animation: loading-3 1s linear forwards 1.8s;
}
.progress.pink .progress-bar{
    border-color: #ed687c;
}
.progress.pink .progress-left .progress-bar{
    animation: loading-4 0.4s linear forwards 1.8s;
}
.progress.green .progress-bar{
    border-color: #1abc9c;
}
.progress.green .progress-left .progress-bar{
    animation: loading-5 1.2s linear forwards 1.8s;
}
@keyframes loading-1{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
    }
}
@keyframes loading-2{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(144deg);
        transform: rotate(144deg);
    }
}
@keyframes loading-3{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(90deg);
        transform: rotate(90deg);
    }
}
@keyframes loading-4{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(36deg);
        transform: rotate(36deg);
    }
}
@keyframes loading-5{
    0%{
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100%{
        -webkit-transform: rotate(126deg);
        transform: rotate(126deg);
    }
}
@media only screen and (max-width: 990px){
    .progress{ margin-bottom: 20px; }
}
.colorgraph {
  height: 7px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}
.form-signin-heading {
  text-align:center;
  margin-bottom: 30px;
}
.container{ margin-top:15px;}
.thumbnail {

    height: auto;
    border: none;
    border-radius: 0px;
    text-align:left;
    padding: 15px;

}
.thumbnail h3{color: #fff;line-height: 174px;margin: 0px;}

.shadow-depth-1{
  -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
}
.shadow-depth-2{
  -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  -moz-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.shadow-depth-3{
  -webkit-box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);
  -moz-box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19);
  box-shadow: 0 12px 15px 0 rgba(0, 0, 0, 0.24), 0 17px 50px 0 rgba(0, 0, 0, 0.19); 
}
.shadow-depth-4{
 -webkit-box-shadow: 0 16px 28px 0 rgba(0, 0, 0, 0.22), 0 25px 55px 0 rgba(0, 0, 0, 0.21);
 -moz-box-shadow: 0 16px 28px 0 rgba(0, 0, 0, 0.22), 0 25px 55px 0 rgba(0, 0, 0, 0.21);
 box-shadow: 0 16px 28px 0 rgba(0, 0, 0, 0.22), 0 25px 55px 0 rgba(0, 0, 0, 0.21);
}  
h4
{

    font-family: 'Oxygen' !important;
    color: #32703D;
}
.panel-footer {
    margin-top: 10px;
    padding: 10px 15px;
    background-color: #f5f5f5;
    border: 1px solid #ddd;
    
}
.listcontainer
{
  padding-top: 15px !important;
  padding-bottom: 15px !important;
  background-color: white;
  margin: : 15px;
  margin-bottom: 25px;
}

#hider
{
    position:absolute;
    top: 0%;
    left: 0%;
    width:1600px;
    height:2000px;
    margin-top: -800px; /*set to a negative number 1/2 of your height*/
    margin-left: -500px; /*set to a negative number 1/2 of your width*/
        /*
        z- index must be lower than pop up box
        */
        z-index: 99;
        background-color:Black;
        //for transparency
        opacity:0.6;
    }

    #popup_box  
    {

        position:absolute;
        top: 50%;
        left: 50%;
        width:10em;
        height:10em;
        margin-top: -5em; /*set to a negative number 1/2 of your height*/
        margin-left: -5em; /*set to a negative number 1/2 of your width*/
        border: 1px solid #ccc;
        border:  2px solid black;
        z-index:100; 

    }

    body.loading .loadingmodal {
        overflow: hidden;   
    }

/* Anytime the body has the loading class, our
modal element will be visible */
body.loading .loadingmodal {
    display: block;
}



.sticky-container{
    padding:0px;
    margin:0px;
    position:fixed;
    right:-140px;
    top:230px;
    width:210px;
    z-index: 1100;
}
.sticky li{
    list-style-type:none;
    background-color:#fff;
    color:#efefef;
    height:43px;
    padding:0px;
    margin:0px 0px 1px 0px;
    -webkit-transition:all 0.25s ease-in-out;
    -moz-transition:all 0.25s ease-in-out;
    -o-transition:all 0.25s ease-in-out;
    transition:all 0.25s ease-in-out;
    cursor:pointer;
}
.sticky li:hover{
    margin-left:-115px;
}
.sticky li img{
    float:left;
    margin:5px 4px;
    margin-right:5px;
}
.sticky li p{
    padding-top:5px;
    margin:0px;
    line-height:16px;
    font-size:11px;
}
.sticky li p a{
    text-decoration:none;
    color:#2C3539;
}
.sticky li p a:hover{
    text-decoration:underline;
}


</style>  



<script type="text/javascript">
   function hidemodal()
   {

      $('[role=dialog]').addClass('fade');
      $("[role=dialog]").hide();

  }
  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip(); 
      $("th").addClass("align-top");

  });

$(document).ajaxStart(function(){
  $("#overlay").css("display", "block");
});

$(document).ajaxComplete(function(){
  $("#overlay").css("display", "none");
});


$(form).submit(function() {
       $("#overlay").css("display", "block");
       return true;
     });



</script>

<style type="text/css">
    
    #overlay{   
    position: fixed;
    top: 0;
    z-index: 100;
    width: 100%;
    height:100%;
    display: none;
    background: rgba(57, 54, 54);
}
.cv-spinner {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;  
}
.spinner {
    width: 40px;
    height: 40px;
    border: 4px #ddd solid;
    border-top: 4px #2e93e6 solid;
    border-radius: 50%;
    animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
    0% { 
        transform: rotate(0deg); 
    }
    100% { 
        transform: rotate(359deg); 
    }
}
.is-hide{
    display:none;
}
</style>
</head>
<body  style="background-color: #F8F9FA;">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div id="header">
      <div class="container-fluid">
   <!-- <div class="row hidden-xs" style="background-color: #26b99a; color: #FFF; height:40px;">
    <div class="col-md-4" style="text-align: left;" >
     24 July, 2018 | 10:32 PM IST

   </div>
   <div class="col-md-4 text-center" style="font-size: 28px;">
     Pradan HR
   </div>
   <div class="col-md-4" style="text-align: right;">
  
  </div>
</div> -->
<div class="row text-center" style="background-color:#FFF;">
  <!-- Logo -->
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-left" style="font-size:10px; font-family:arial; ;  padding-top: 7px; color: #2E3192;">
    <img src="<?php echo site_url('common/backend/images/pradan_logo1.png'); ?>" alt="Logo" width="300px;"  />
</div>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center" style="color: #fff; font-family:courier ; padding-top: 3px;">

</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <!--  <img src="<?php //echo site_url('common/backend/images/Human-Resources-Management.png'); ?>" alt="Logo" height="120px" width="350px" /> -->


</div>
<br>

</div>


<!-- End Logo -->
</div>
</div>

<!-- Modal for transfer and promotion-->
<div class="container">
    <div class="modal fade" id="errorModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name" class="errormodelmessage"></label>
         </div>
     </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

    </div>
</div>
</div>
</div>
</div> 
<!-- Modal -->








