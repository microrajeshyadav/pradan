  <style type="text/css">
  .navbar{
    background: #17A2B8 !important;
    max-height: 50px;
  }

  .dropdown{
    border-radius:0;

  }
  .dropdown-menu{
    background: #fff;   
    top:84%;
    border-radius:0px !important;
    border:3px solid #17A2B8 !important;
    
    padding: 5px;
    z-index: 9999;
    font-size: 14px;
  }
  .dropdown-item:hover{
    background:#085ca5 !important;
    color:#;
  }
  
  ul > li > ul
  {
    width :auto;
    border:0 !important;
    background: #f3f3f3 !important;
  }
  ul > li > ul > li
  {
    line-height: 1;
    border-bottom: 1px dashed #d4d4d4;
  }

  a {
    white-space: nowrap;
  }
  .pull-right {
    float: right !important;
  }

  .nav>li>ul>li>a:focus, .nav>li>ul>li>a:hover,.nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{

    background: #17A2B8!important;
    color: #fff !important;

  }

  .dropdown-submenu {
    position: relative;
    color: #fff !important;
  }

  

  .dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
  }

  .dropdown-submenu:hover>.dropdown-menu {
    display: block;
  }

  .dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
  }

  .dropdown-submenu:hover>a:after {
    border-left-color: #fff;
  }

  .dropdown-submenu.pull-left {
    float: none;
  }

  .dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
  }

  
</style>


<script type="text/javascript">

  $(document).ready(function () {
    $('.navbar-light .dmenu').hover(function () {
      $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
    }, function () {
      $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
    });
  });

</script>
<?php 



if (!empty($this->loginData->RoleID)) {
  ?>

  <nav class="navbar navbar-expand-sm   navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <?php if ($this->loginData->RoleID == 16) { 
      $workareaurl = site_url().'Hr_report';
    }else{
      $workareaurl = site_url().'staff_dashboard';
    }

    ?>


    <a href="<?php echo $workareaurl;?>" class="navbar-brand" href="#"><i class="fa fa-home" aria-hidden="true">&nbsp; Workarea</i>
    </a>

      <div class="collapse navbar-collapse" id="navbar" class="col-md-9">
      <?php //if (isset($this->loginData->RoleID) && $this->loginData->RoleID ==16 || $this->loginData->RoleID ==10) { 
       ?>
       <ul class="nav navbar-nav ">
        <?php 
          $query= 'select * from role_permissions where RoleID= ? and UserId = ? and parent_menu_id =0 and menuorder != 0 and Is_Home = 1 and Isactive = 1  order by menuorder asc';
         
        $result = $this->db->query($query, [$this->session->userdata("login_data")->RoleID, $this->session->userdata("login_data")->UserID])->result(); 
        
        if (count($result) == 0) {  
           $query= 'select * from role_permissions where RoleID= ? and COALESCE(UserId,0) != ? and parent_menu_id =0 and menuorder != 0 and Is_Home = 1  and Isactive = 1 order by menuorder asc';
          
         $result = $this->db->query($query, [$this->session->userdata("login_data")->RoleID, $this->session->userdata("login_data")->UserID])->result();
        }
        if (count($result) > 0) {  
         foreach ($result as $prow) { 
        //  print_r($result);
           ?>
           <li class="active dropdown">
             <a  href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $prow->menu_name ?>
             <span class="caret"></span></a>
             <?php   $query = "select * from role_permissions where RoleID = ".$this->loginData->RoleID." and parent_menu_id = ".$prow->ID."  and menuorder != 0 and isactive=1 and UserId = ".$this->session->userdata("login_data")->UserID." order by menuorder asc "; 
             $result = $this->db->query($query)->result();
             if (count($result) == 0) {  
               $query = "select * from role_permissions where RoleID = ".$this->loginData->RoleID." and parent_menu_id = ".$prow->ID."  and menuorder != 0 and isactive=1 order by menuorder asc "; 
             $result = $this->db->query($query)->result();
            }
             if (count($result) > 0) { ?>
               <ul class="dropdown-menu">
                <?php foreach ($result as $row) { 
           // if ($row->Action == "index" || $row->Action == "#") { ?>
             <?php 
                $query = "select * from role_permissions where RoleID = ".$this->loginData->RoleID." and parent_menu_id=".$row->ID."  and menuorder != 0 and isactive=1 order by menuorder asc "; 
             $submenuresult = $this->db->query($query)->result();

              // print_r($submenuresult); die;

             if (count($submenuresult) > 0) { 
               ?>

               <li class="dropdown dropdown-submenu"> 
                 <a class="nav-link dropdown-toggle" href="#"><?php  echo $row->menu_name; ?></a>
                 <ul class="dropdown-menu">

                   <?php foreach ($submenuresult as $row) {  

                    ?>
                    <li class="nav-item"><a class="nav-link" href="<?php echo site_url($row->Controller.'/'.$row->Action);?>"><?php echo $row->menu_name?></a></li>
                  <?php  } ?>
                </ul>
              </li>
            <?php }else{

             $expcontroller = explode('/',$row->Controller);
             if (count($expcontroller) > 1) {
              $row->Controller = str_replace('{staffid}',$this->loginData->staffid,$row->Controller);
              $query = "select candidateid from staff where staffid = ".$this->loginData->staffid;
              $result = $this->db->query($query)->row();

              if (!empty($result->candidateid) && $result->candidateid !='' && $result->candidateid !=NULL) {
                $row->Controller = str_replace('{candidateid}',$result->candidateid,$row->Controller);
                ?>
                <li class="nav-item"><a class="nav-link"  href="<?php echo site_url($row->Controller);?>"><?php echo $row->menu_name; ?></a></li>

              <?php }
            }else{
              ?>
              <li class="nav-item"><a class="nav-link"  href="<?php echo site_url($row->Controller.'/'.$row->Action);?>"><?php echo $row->menu_name; ?></a></li>
            <?php } ?>
          <?php }  }  //} ?>
        </ul>
      <?php } ?>
    </li>
  <?php } }//}   ?>
</ul>

<?php 

if ($this->loginData->RoleID ==19) {
  $query = $this->db->query('SELECT * FROM `tbl_general_nomination_and_authorisation_form` WHERE `candidateid`='.$this->loginData->candidateid);
  $result = $query->result()[0];
  $joiningreportquery = $this->db->query('SELECT * FROM `tbl_joining_report` WHERE `candidateid`='.$this->loginData->candidateid);
  $joiningreportresult = $joiningreportquery->result()[0];
  $hrdintimationquery = $this->db->query("SELECT a.*, b.id as trans_id, b.trans_status  FROM `tbl_hr_intemation` as a left JOIN staff_transaction as b on b.staffid = a.staffid WHERE (b.trans_status='Resign' || b.trans_status='Recommended to graduate' || b.trans_status='Facilitate to leave') AND a.staffid=".$this->loginData->staffid);
  $hrdintimationresult = $hrdintimationquery->row();
 /* echo "SELECT a.*, b.id as trans_id, b.trans_status  FROM `tbl_hr_intemation` as a left JOIN staff_transaction as b on b.staffid = a.staffid WHERE (b.trans_status='Resign' || b.trans_status='Recommended to graduate' || b.trans_status='Facilitate to leave') AND a.staffid=".$this->loginData->staffid;*/
  /*echo "<pre>";
  print_r($this->loginData->candidateid);exit();*/
  ?>
  <div class="btn-group btn-group-primary col-md-9" >
    <ul class="nav navbar-nav">
      <li class="active dropdown">
        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home<span class="caret"></span></a>
        <ul class="dropdown-menu">
         <?php 
         
          if(isset($getgeneralform->generalformstatus) && ($getgeneralform->generalformstatus == 1 || $getgeneralform->generalformstatus == 2)) {?>
           <li class="dropdown dropdown"><a href="<?php echo site_url("General_nomination_and_authorisation_form/view/");?>" class="nav-link">General nomination and authorisation form</a></li>

         <?php  } ?>

         <?php  if(isset($getjoiningreport->joinreportstatus) && ($getjoiningreport->joinreportstatus == 1 || $getjoiningreport->joinreportstatus == 2)) {?>
           <li class="dropdown dropdown"><a href="<?php echo site_url("Joining_report_form/view/");?>" class="nav-link">Joining report form </a> </li>
         <?php }?>
         <?php if ($this->loginData->joinstatus==1 && $this->loginData->tc_hrd_document_verfied==2) { ?>
           <li class="dropdown dropdown"> <a href="<?php echo site_url("Daship_components");?>" class="nav-link">DAship components</a>
           </li>
         <?php }  ?>
          <?php
            if($hrdintimationresult){
              // echo $hrdintimationresult->trans_status;exit();
              if (!empty($hrdintimationresult->trans_id) && ($hrdintimationresult->trans_status =='Resign' || $hrdintimationresult->trans_status =='Facilitate to leave' ) ){ ?>
          <li class="dropdown dropdown"> <a href="<?php echo site_url("Developmentapprentice_sepemployeeexitform/index").'/'.$hrdintimationresult->trans_id;?>" class="nav-link">Employee Exit Form</a>
           </li>
         <?php }else if(!empty($hrdintimationresult->trans_id) && $hrdintimationresult->trans_status == 'Recommended to graduate'){ ?>
          <li class="dropdown dropdown"> <a href="<?php echo site_url("Developmentapprentice_graduatingemployeeexitform/index").'/'.$hrdintimationresult->trans_id;?>" class="nav-link">Employee Exit Form</a>
           </li>
         <?php } } ?>
       </ul>
     </li>
   </ul>
 </div>

<?php } ?>
</div>
<div class="pull-right col-md-3">
  <div class="btn-group btn-group-success pull-right" >
    <button class="btn btn-success" type="button"><?php  echo strtoupper($this->loginData->Username); ?></button>
    <button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><span class="caret"></span>
    </button>
    <?php  

    
    $query = $this->db->query('SELECT * FROM `sysaccesslevel` WHERE 
      `Acclevel_Cd`='.$this->loginData->RoleID);
    $result = $query->result()[0];

     $staffquery = $this->db->query('SELECT staffid,flag FROM `staff` WHERE 
       `staffid`='.$this->loginData->staffid);
     $staffresult = $staffquery->result()[0];
    ?>
    <ul class="dropdown-menu bg-light" style="position: absolute;
    right: 0px !important; left: auto;  min-width:270px; border-radius: 5px; top:110%;">
    <div class="panel">
      <div class="panel-body">  
       <div class="col-lg-3" style="color: #fff;">
        <?php $ci =&get_instance(); $ci->load->model('stafffullinfo_model');    $candidatedetails= $ci->stafffullinfo_model->getCandidateDetailsPreview($this->loginData->staffid); //print_r($candidatedetails); die(); ?>

         <?php if(!empty($candidatedetails->encryptedphotoname) && $candidatedetails->encryptedphotoname !='') { ?>
            <img class= "rounded-circle" src="<?php if(!empty($candidatedetails->encryptedphotoname)) echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Profile Picture" style = "height: 40px; width: 40px;">
          <?php }else{ ?>
            <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Profile Picture" style = "height: 40px; width: 40px;">
          <?php } ?>


      

      </div>
      <div class="col-lg-9" style="color: #000;">
        <p class="text-left" style=" font-size: 12px;"><strong>Role : &nbsp; <?php  echo strtoupper($result->Acclevel_Name);?></strong></p>
        <p class="text-left" style=""><strong><?php  echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?></strong></p>
        <p class="text-left small"><?php echo $this->loginData->EmailID; ?></p>
      </div>
    </div>
    <div class="" >
     <div class="col-lg-4" style="color: #000;">
      <?php if ($this->loginData->RoleID==3 &&  $staffresult->flag >=1) {?>
        <a data-toggle="tooltip" data-placement="bottom" title="My Profile" href="<?php echo site_url().'Stafffullinfo/view/'.$this->loginData->staffid;?>" class="btn btn-success btn-block blue-tooltip"><i class="fa fa-user" aria-hidden="true"></i></a>
      <?php }else if($this->loginData->RoleID==3 &&  $staffresult->flag==0){ ?>
       <a data-toggle="tooltip" data-placement="bottom" title="My Profile" href="<?php echo site_url().'Stafffullinfo/edit/'.$this->loginData->staffid;?>" class="btn btn-success btn-block blue-tooltip"><i class="fa fa-user" aria-hidden="true"></i></a>
     
     <?php }else{ ?>
      <a data-toggle="tooltip" data-placement="bottom" title="My Profile" href="<?php echo site_url().'Stafffullinfo/view/'.$this->loginData->staffid;?>" class="btn btn-success btn-block blue-tooltip"><i class="fa fa-user" aria-hidden="true"></i></a>
    <?php } ?>
  </div>  
  <div class="col-lg-4" style="color: #000;">
    <a data-toggle="tooltip" data-placement="bottom"  title="Change Password" href="<?php echo site_url().'Change_password';?>" class="btn btn-warning btn-block blue-tooltip"> <i class="fa fa-key" aria-hidden="true"></i>
    </a>
  </div>
  <div class="col-lg-4" style="color: #000;">
   <a data-toggle="tooltip" data-placement="bottom" title = "Logout" href="<?php echo site_url('login/logout');?>" class="btn btn-danger btn-block blue-tooltip"><i class="fa fa-power-off"></i>
   </a>
 </div>

</div>

</div>
</ul>
</div>
</div>



<!-- </div> -->
</nav>
<?php }else{ 
  if (isset($this->loginData->candidateid)) {

    $query = $this->db->query('SELECT * FROM `tbl_general_nomination_and_authorisation_form` WHERE `candidateid`='.$this->loginData->candidateid);
    $result = $query->row();

    $joiningreportquery = $this->db->query('SELECT * FROM `tbl_joining_report` WHERE `candidateid`='.$this->loginData->candidateid);
    $joiningreportresult = $joiningreportquery->row();
    ?>


    <nav class="navbar navbar-expand-sm navbar-light bg-light" style="height: 40px;"  >
      <div class="collapse navbar-collapse" id="navbar" style="font-size: 16px; color:#fff;" >

       <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home<span class="caret"></span></a>
          <ul class="dropdown-menu">

           <?php 
           if (isset($this->loginData->confirm_attend) && $this->loginData->confirm_attend ==0 && $this->loginData->campustype=='off') { ?>

            <li><a href="<?php echo site_url("candidate/Acceptance_for_interview/index");?>"><span>Interview call for accepted </span></a></li>
          <?php } else{    

            if (isset($this->loginData->BDFFormStatus) && $this->loginData->BDFFormStatus ==1) {    ?>
              <li>
               <a href="
               <?php echo site_url("candidate/Candidatedfullinfo/index");?>">
               <i class="material-icons col-green"></i>
               <span>BDF Info </span>
             </a>
           </li>
         <?php } ?>

         <?php if(isset($getgeneralform->generalformstatus) && ($getgeneralform->generalformstatus == 1 || $getgeneralform->generalformstatus == 2)) { ?>
          <li><a href="
            <?php echo site_url("candidate/General_nomination_and_authorisation_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>

        <?php  }elseif ($getjoiningreport->joinreportstatus == 0 || $getgeneralform->generalformstatus =='') { ?> 

          <li><a href="
            <?php echo site_url("candidate/General_nomination_and_authorisation_form/index");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>
        <?php } ?>

        
        <?php  if(isset($getjoiningreport->joinreportstatus) && ($getjoiningreport->joinreportstatus == 1 || $getjoiningreport->joinreportstatus == 2)) {?>
          <li><a href="
            <?php echo site_url("candidate/Joining_report_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report form</span>
          </a> </li>

        <?php }elseif ($getjoiningreport->joinreportstatus == 0 || $getjoiningreport->joinreportstatus =='') { ?> 
          <li><a href="
            <?php echo site_url("candidate/Joining_report_form/index/");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report form</span></a></li>
          <?php } } ?>

        </ul>
      </ul>
      <ul class="nav navbar-nav navbar-right">
       <li><a href="<?php echo site_url('login/candidatelogout');?>">Logout</a></li>
     </ul> 
   </li>
 </ul>
</div>
</nav>
<?php  } }  ?>
