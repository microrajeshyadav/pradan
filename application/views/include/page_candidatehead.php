<!DOCTYPE html>
<html>

<head>
  <style type="text/css">
  .file {
  visibility: hidden;
  position: absolute;
}
/*
 * Styles for demo only
 */

body {
  background-color: $purple;
  margin: 50px;
}
.container {
  background-color: #fff;
  padding: 40px 80px;
  border-radius: 8px;
}
h1 {
  color: #fff;
  font-size: 4rem;
  font-weight: 900;
  margin: 0 0 5px 0;
  background: -webkit-linear-gradient(#fff, #999);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  text-align: center;
}
h4 {
  color: lighten(#5c3d86,30%);
  font-size: 24px;
  font-weight: 400;
  text-align: center;
  margin: 0 0 35px 0;
}
.btn.btn-primary {
  background-color: $purple;
  border-color: $purple;
  outline: none;
  &:hover {
    background-color: darken($purple, 10%);
    border-color: darken($purple, 10%);
  }
  &:active, &:focus {
    background-color: lighten($purple, 5%);
    border-color: lighten($purple, 5%);
  }
}


</style>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign up | Pradan</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">
  
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

   <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

   <!--  <link href="<?php //echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />
 -->

    <!-- Waves Effect Css -->
    <link href="<?php echo site_url('common/backend/vendor/node-waves/waves.css');?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo site_url('common/backend/css/style.css');?>" rel="stylesheet">
    
    <!-- Jquery Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>


        <!-- Google Fonts -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

      <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />

    <!-- Bootstrap Form Helper Css -->
    <link href="<?php echo site_url('common/backend/dist/css/bootstrap-formhelpers.css');?>" rel="stylesheet">
    
     <link href="<?php echo site_url('common/backend/dist/css/bootstrap-formhelpers.min.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />
    
    <!-- JQuery DataTable Css -->
    <link href="<?php echo site_url('common/backend/vendor/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">
    
    <!-- Custom Css -->
    <link href="<?php echo site_url('common/backend/css/style.css');?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo site_url('common/backend/css/themes/all-themes.css');?>" rel="stylesheet" />
    
    <script type="text/javascript">
      $(document).ready(function(){
    $('.dateinput').datepicker({ format: "dd/mm/yyyy" })  
    });
    </script>

  

</head>