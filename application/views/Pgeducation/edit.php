<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Pgeducation" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-sm-offset-4">
          <form method="POST" action="">
           <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-7 panel-title pull-left">Edit Post Graduation</h4>
               <div class="col-md-5 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>
          <?php //print_r($mstpgeducation_details); die; 
          $grouoparray = array('UG' => 3, 'PG'=> 4);
           ?>
          <div class="panel-body">

            <div class="form-group">
              <div class="form-line">
                <label for="GroupName" class="field-wrapper required-field">Group Name <span style="color: red;" >*</span></label>
              <select id="groupname" name="groupname" class="form-control" required="required">
                <option value="">Select</option>
                <?php foreach($grouoparray as $key => $value) {
                  if ($mstpgeducation_details->groupid == $value ) { ?>
                    <option value="<?php echo $value;?>" SELECTED><?php echo $key;?></option>
                   <?php }else{ ?>
                   <option value="<?php echo $value;?>"><?php echo $key;?></option>
                  <?php } } ?>

              </select>
              </div>
              <?php echo form_error("pgname");?>
            </div>

            <div class="form-group">
              <div class="form-line">
                <label for="StateCode" class="field-wrapper required-field">Education Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" id="pgname" maxlength="50" minlength="2" name="pgname" value="<?php echo $mstpgeducation_details->pgname;?>">
              </div>
              <?php echo form_error("pgname");?>
            </div>
<!-- 
            <div class="form-group">
              <div class="form-line">
                <label for="SNS" class="field-wrapper required-field">Status <span style="color: red;" >*</span></label>
                <?php   

                        //$options = array('' => 'Select Status');     
               // $options = //array('0' => 'Active', '1' => 'InActive');
                //echo form_dropdown('status', $options, $mstpgeducation_details->status, 'class="form-control"'); 
                ?>
              </div>
              <?php// echo form_error("status");?>
            </div> -->

          </div>          
          <div class="panel-footer text-right">
            <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
            <a  href="<?php echo site_url("Pgeducation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
          </div>



        </div>
      </form>
    </div>
  </div>
  <!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>