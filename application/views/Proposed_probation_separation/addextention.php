<style type="text/css">
.checkboxStatus{
  color:red;
}
</style>
<section class="content">
  <br>
  <div class="container-fluid">
     <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <!-- <?php //print_r($getextentiondetails); ?> -->
        <div class="panel thumbnail shadow-depth-2 listcontainer" style="min-height: 600px;" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-8 panel-title pull-left">Proposed Transfer/Extention</h4>
             <div class="col-md-4 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
        <div class="panel-body">                
          <form method="POST" action="" name="separation" id="separation">
            <div class="row " >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-light" style="padding: 10px; ">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                 <div class="form-group">
                   <div class="col-md-4"> 
                    <label class="field-wrapper required-field">DAShip Transactions <span style="color:red;"> *</span></label> 

                  </div>
                  <div class="col-md-8"> 
                   <!--  <select name="dashiptransactions" id="dashiptransactions" class="form-control" required="required" >
                      <option value="">Select</option>
                      <?php  foreach ($getintimation as $key => $value) { 
                        if ($value->id == $getextentiondetails->intemation_type) {

                          ?>
                          <option value="<?php echo $value->id; ?>" SELECTED><?php echo $value->intimation; ?></option>
                        <?php } else{?>

                         <option value="<?php echo $value->id; ?>"><?php echo $value->intimation; ?></option>
                       <?php } } ?>
                     </select> -->
                     <input type="hidden" value="<?php echo $getextentiondetails->intemation_type;?>" name="dashiptransactions">
                     <input type="text" class="form-control" readonly="readonly" value="<?php echo $getextentiondetails->intimation;?>" name="dashiptransactions1">
                   </div>
                   <?php echo form_error("dashiptransactions");?>
                 </div>
               </div>
             </div>


             <!--  start txtdatranfer here -->
             <div id="txtdatranfer" >
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
               <div class="form-group">
                <div class="form-line">
                  <label class="field-wrapper required-field">Old DC <span style="color:red;"> *</span></label>
                <!--   <select name="olddc" id="olddc" class="form-control" readonly="readonly" required="required">
                    <option value="">Select</option>
                    <?php foreach ($getdevelopmentcluster as $key => $value) {
                      if ($value->dc_cd == $getextentiondetails->dc_cd) {
                        ?>
                        <option value="<?php echo $value->dc_cd;?>" SELECTED><?php echo $value->dc_name;?></option>
                      <?php }else{  ?>
                        <option value="<?php echo $value->dc_cd;?>"><?php echo $value->dc_name;?></option>
                      <?php } } ?>
                    </select> -->
                    <input type="hidden" value="<?php echo $getextentiondetails->dc_cd;?>" name="olddc">
                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $getextentiondetails->dc_name;?>" name="olddc1">
                  </div>
                  <?php echo form_error("olddc");?>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
               <div class="form-group">
                <div class="form-line">
                  <label class="field-wrapper required-field">Old Team <span style="color:red;"> *</span></label>
                  <!-- <select name="oldteam" id="oldteam" class="form-control" required="required">
                   <option value="">Select Team</option>
                   <?php foreach ($getofficedetails as $key => $value) {
                    if ($value->officeid == $getextentiondetails->new_office_id) {
                      ?>
                      <option value="<?php echo $value->officeid;?>" SELECTED><?php echo $value->officename;?></option>
                    <?php }else{  ?>
                     <option value="<?php echo $value->officeid;?>"><?php echo $value->officename;?></option>
                   <?php } } ?>
                 </select> -->
                 <input type="hidden" value="<?php echo $getextentiondetails->new_office_id;?>" name="oldteam">
                 <input type="text" class="form-control" readonly="readonly" value="<?php echo $getextentiondetails->officename;?>" name="oldteam1">
               </div>
               <?php echo form_error("oldteam");?>
             </div>
           </div>


           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
             <div class="form-group">
              <div class="form-line">
                <label class="field-wrapper required-field">DA Name <span style="color:red;"> *</span></label>
               <!--  <select name="staff" id="staff" class="form-control" required="required">
                 <option value="">Select Development Apprenticeship </option>
                 <?php foreach ($getdevelopmentapprentice as $key => $value) {
                  if ($value->staffid == $getextentiondetails->staffid) {
                    ?>
                    <option value="<?php echo $value->staffid;?>" SELECTED><?php echo $value->name;?></option>
                  <?php }else{  ?>
                   <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>
                 <?php } } ?>
               </select> --> 
               <input type="hidden" value="<?php echo $getextentiondetails->staffid;?>" name="staff">
               <input type="text" class="form-control" readonly="readonly" value="<?php echo $getextentiondetails->name;?>" name="staff1">
             </div>
             <?php echo form_error("staff");?>
           </div>
         </div>
<?php  
        $weekDay = '';
        $completeddate = '';
        $becoming_executive_date = '';
        $time_original = '';
        $time_add = '';
        $new_date  = '';
        $effective_date = '';

        $completeddate = $this->gmodel->changedatedbformate($getextentiondetails->probation_completed_date);
        
       @ $weekDay = date('l', strtotime($completeddate)); 

       
     
      if (!empty($weekDay) || $weekDay !='Sunday' ) {
         $time_original = strtotime($getextentiondetails->probation_completed_date);
         $time_add      = $time_original + (3600*24); //add seconds of one day
         $new_date      = date("Y-m-d", $time_add);
         $becoming_executive_date = $this->gmodel->changedatedbformate($new_date);
      }else{
        $time_original = strtotime($getextentiondetails->probation_completed_date);
         $time_add      = $time_original + (3600*48); //add seconds of one day
         $new_date      = date("Y-m-d", $time_add);
         $becoming_executive_date =  $this->gmodel->changedatedbformate($new_date);
      }
       
       @ $sql = "SELECT `staff`.doj_team FROM `staff` Where  `staff`.staffid= ".$getextentiondetails->staffid;
          $data = $this->db->query($sql)->row();

        @ $date = $data->doj_team;
          $date1 = strtotime($date);
          $new_date = strtotime('+ 1 year', $date1);
          $probation_completed_date =  date('d/m/Y', $new_date);

                  // $staffid = $this->input->post('staff');
        // $comment = $this->input->post('commenttransfer');
        $dateoftransfer = $probation_completed_date;
        // $becoming_executive_date = $this->input->post('becoming_executive_date');
        


        $months = ceil(abs($effective_date - $dateoftransfer) / 86400);

        $getstaffdetail =  $this->model->getDAExtentionDetails($getextentiondetails->staffid);
        $gethrddetail =  $this->model->gethrDetails($getextentiondetails->staffid);
        $gethrunitdetail =  $this->model->getHRunitDetails();

        // $dateoftransferdb        =  $this->gmodel->changedatedbformate($dateoftransfer);
        // $effective_db_date       = $this->gmodel->changedatedbformate($effective_date);
        // $getcentraleventdetail   =  $this->model->getCentralEventDetails($effective_db_date, $dateoftransferdb);
        // $comeingeventdate = date("F, Y", strtotime($getcentraleventdetail->eventdate));

        $staff = array('$staffname','$tcname','$desname','$effective_date','$months','$becoming_executive_date');
        $staff_replace = array($getstaffdetail->name,$getstaffdetail->reportingtoname,$getstaffdetail->desname,$completeddate,$months,$becoming_executive_date);
      

      // $message='';
      // $subject = "sharing the offer letter and other documents";
      // $appointmentletterfilename = "mailtext/appointmentletter_to_candidate.txt";
      // $fd = fopen("mailtext/appointmentletter_to_candidate.txt","r");  
      // $message .=fread($fd,filesize($appointmentletterfilename));
      // eval ("\$message = \"$message\";");
      // $message =nl2br($message);
            
       
          $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 61 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
          if(!empty($data))
          $post_body = str_replace($staff,$staff_replace , $data->lettercontent); 

 ?>
<div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 resondiv" >
    <div class="form-group">
      <div class="form-line">
        <label class="field-wrapper required-field">Graduation Completion Date :<span style="color:red;"> *</span></label>
        <input type="text" name="dateoftransfer" id="dateoftransfer" class="form-control" required="required" value="<?php echo  $probation_completed_date;  ?>">
      </div>
      <?php echo form_error("dateoftransfer");?>
    </div>
  </div>


   <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 resondiv" >
    <div class="form-group">
      <div class="form-line">
        <label class="field-wrapper required-field">Extention Date :<span style="color:red;"> *</span></label>
        <input type="text" name="effective_date" id="effective_date" class="form-control"  
        value="<?php  if(!empty($completeddate) && $completeddate!='') { echo $completeddate; } ?>" required="required">
      </div>
      <?php echo form_error("effective_date");?>
    </div>
  </div>

 <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 resondiv" >
    <div class="form-group">
      <div class="form-line">
        <label class="field-wrapper required-field">Becoming Executive (with effect) :<span style="color:red;"> *</span></label>
        <input type="text" name="becoming_executive_date" id="becoming_executive_date" class="form-control"  
        value="<?php  if(!empty($becoming_executive_date) && $becoming_executive_date!='00/00/0000') { echo $becoming_executive_date; } ?>" required="required">
      </div>
      <?php echo form_error("becoming_executive_date");?>
    </div>
  </div>

  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="background-color: white;">
    <div class="form-group">
      <div class="form-line">
        <label class="field-wrapper required-field">Comments <span style="color:red;"> *</span></label>
        <textarea name="commenttransfer" id="commenttransfer" class="form-control" maxlength="150" required></textarea>
      </div>
      <?php echo form_error("commenttransfer");?>
    </div>

  </div>

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer text-right" >
   <input class="btn btn-warning" type="reset" value="Reset" data-toggle="tooltip" title="Want to reset your changes? Click on me.">
   <button class="btn btn-success" type="button"  name="savebtntranfer" onclick="others_document();" id="savebtntranfer" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save & Send</button>
 </div>


</div> 
</div>

</div>
</div> 
</div>




  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            
            <h4 class="modal-title">Check Letter.</h4>
          </div>
          <div class="modal-body">
            <textarea class='summernote' id='summernote' name="summernote" rows='10' ><?php echo $post_body ?></textarea>
          </div>
          <div class="modal-footer">
            <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
            <button class="btn btn-success" type="submit" name="savebtntranfer" id="savebtntranfer" data-toggle="tooltip" title="Want to save your changes? Click on me.">Send</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      
    </div>
  </div> 
</form>
</section>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.min.js"></script>


      <script>
        $(document).ready(function(){

          if(required == true)
          {
            $("#savebtntranfer").show();
          }
          $("#savebtntranfer").hide();

          $('[data-toggle="tooltip"]').tooltip();  
          $('#Inboxtable').DataTable();   
          $('#InboxtableAppointment').DataTable(); 
        });

        function others_document(candidateid){
          $('#myModal').removeClass('fade');
          $("#myModal").show();
        }
      </script>  

<script type="text/javascript">
  $('#probation_completed').change(function(){
    if($('#probation_completed').val() == 1)
    {
     $('#probatextension').css('display', 'block');
   }
   else if($('#probation_completed').val() == 2)
   {
     $('proyes').css('display', 'block');
   }
   else{
     $('#probatextension').css('display', 'none');
     $('proyes').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
     $('.Probationdivrt').css('display', 'none');
     
   }
 });


  $(document).ready(function(){
    $("#savebtnsubmit").on("click",function(){

      if (($("input[name*='test']:checked").length)<=0) {
        alert("Please tick the Clearance all document");
      }
      return true;
    });


     $("#effective_date").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: '<?php echo $probation_completed_date; ?>',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
   });


    if($('#probation_completed').val() == 1)
    {
     $('#probatextension').css('display', 'block');
   }
   else 
   {
     $('proyes').css('display', 'block');
   }


    $("#savebtntranfer").change(function(){
    alert(3);
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/extension_letter/'+$("#staff").val()+'/'+$("#commenttransfer").val()+'/'+$("#dateoftransfer").val()+'/'+$("#effective_date").val()+'/'+$("#becoming_executive_date").val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {

      console.log(data);
      $(".note-editable").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



   $("#dc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#team").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

   $("#olddc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#oldteam").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


   $("#newdc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#newteam").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


   $("#oldteam").change(function(){
   // alert('sdfsd');
   $.ajax({
    url: '<?php echo site_url(); ?>ajax/getTeamStaffTransfer/'+$(this).val(),
    type: 'POST',
    dataType: 'text',
  })
   .done(function(data) {

    console.log(data);
    $("#staff").html(data);

  })
   .fail(function() {
    console.log("error");
  })
   .always(function() {
    console.log("complete");
  });

 });


   $("#newteam").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#new_field_guide").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

   $("#probationteam").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getTeamStaff/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#probationstaff").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
   


   $("#probation_completed").change(function(){
    var dac = $('#probationstaff').val(); 
    if (dac == '') {
      alert('Please Select Staff !!!');
      $("#probation_completed").rempveAtt('Selecttd', false);
    }else{

       // alert(dac);

       $.ajax({
        url: '<?php echo site_url(); ?>Ajax/get_DAshipid/'+ dac,
        type: 'POST',
        dataType: 'text',
      })
       .done(function(data) {

        $("#Probatdiv").html(data);

      })
       .fail(function() {
        console.log("error");
      })
       .always(function() {
        console.log("complete");
      });
     } 
   });

 }); 

  $("#probationdc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#probationteam").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



  $("#team").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Apprentice/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#da").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



  $('#type').change(function(){
    var typeid = $(this).val();
    if(typeid == 1)
    {
     $(".Probationdiv").show();
     $(".resondiv").hide();
  // $(".Mandatorytext").prop('required', 'required');

} else if(typeid == 2){
  $(".resondiv").show();

  $(".Probationdivrt").hide();
  $(".probation_extension_date").hide();
  $("#probextedate").hide();

      // $(".Mandatorytext").prop('required', ''); 
    } 
  }); 

  $('#probation_completed').change(function(){
    var probation_completedid = $(this).val();
    if(probation_completedid == 1)
    {
     $(".Probatdiv").show(); 
     $(".probation_extension_date").hide();  
   } else if(probation_completedid == 2){
    $(".probation_extension_date").show();
    $(".Probatdiv").hide();
  } 
});




  $('#dashiptransactions').change(function(){
 // alert('sdsadsad');
 var dashiptransact = $(this).val();

 if(dashiptransact ==1 ) {
  $('#txtseparation').show();
  $('#txtseparation').find('input, textarea, button, select').attr('disabled',false);
 // $("#savebtnsubmit").prop('disabled', 'disabled');
 $('#txtextension').hide();
 $('#txtextension').find('input, textarea, button, select').attr('disabled','disabled');
 $('#txtdatranfer').hide();
 $('#txtdatranfer').find('input, textarea, button, select').attr('disabled','disabled');

} else if(dashiptransact == 2 ) {
  $('#txtseparation').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtseparation').hide();
  $('#txtextension').show();
  $('#txtextension').find('input, textarea, button, select').attr('disabled',false);
  $('#txtdatranfer').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtdatranfer').hide();
}else {

  $('#txtseparation').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtseparation').hide();
  $('#txtextension').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtextension').hide();
  $('#txtdatranfer').show();
  $('#txtdatranfer').find('input, textarea, button, select').attr('disabled',false);
}

});
</script>
