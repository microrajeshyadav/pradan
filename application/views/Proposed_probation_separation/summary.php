
<br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Summary of documents of the Apprentices</h4>
       <div class="col-md-2 text-right">

       </div>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <div class="panel-body">
     <div class="row"  style="margin-top:20px; line-height:2">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
        Name:
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
        <?=$da_summary->candidatefirstname.' '.$da_summary->candidatelastname?>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
        Batch:
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
        <?=$da_summary->batch?>
      </div>
      <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" >

      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
       Placement as DA:
     </div>
     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
       <?=$da_summary->officename?>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
      Previous Placement as DA (if):
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
     ---
   </div>
   <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" >

   </div>

   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
    Supervision as DA:
  </div>
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
    <?=$da_summary->supervisername?>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
   Previous Supervision as DA(if)
 </div>
 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
   ---
 </div>
 <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" >

 </div>
 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
   Field Guide:
 </div>
 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
  <?=$da_summary->fieldguidename?>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
 Previous Field Guide:
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 font-weight-bold" >
 ---
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >

</div>
</div>


<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
      <thead class="bg-light">

        <th width="5%"><div align="center">Degree</div></th>
        <th width="56%"><div align="left">Discipline</div></th>
        <th width="6%"><div align="center">Year</div></th>
        <th width="5%"><div align="center">%  Marks</div></th>
        <th width="10%" class="text-center">
          Mark-sheets <br/> (Y/N)

        </th>
        <th width="9%" > Certificates <br/>  (Y/N) 
        </th>
        <th width="9%" class="text-center"> Signature  <br/>
          (by TC)
        </th>
        <th width="9%"><div align="center">
        Document Download</div>
      </th>
    </thead>
    <tbody> 
      <tr>
        <td class="font-weight-bold">10th</td>
        <td align="left"><?=$da_summary->metricspecialisation?></td>
        <td ><?=$da_summary->metricpassingyear?></td>
        <td><?=$da_summary->metricpercentage?>%</td>
        <td><?=($da_summary->metric_certificate == 'on') ? 'Y' : 'N'?></td>
        <td>--</td>
        <td>
          <?php
          if($da_summary->metric_certificate == 'on')
          {
            echo $da_summary->supervisername;
          }
          ?>

        </td>
        <td>
          <?php
          if($da_summary->encryptmetriccertificate){
            $matricresult = explode('|', $da_summary->encryptmetriccertificate);
        /*echo "<pre>";
        print_r($matricresult);exit();*/
        foreach($matricresult as $res){
          // echo $res;
          ?>
          <a href="<?php if(!empty($res)) echo site_url().'datafiles/educationalcertificate/'.$res;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
          <?php } } ?>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">12th</td>
        <td align="left"><?=$da_summary->hscspecialisation?></td>
        <td><?=$da_summary->hscpassingyear?></td>
        <td><?=$da_summary->hscpercentage?>%</td>
        <td><?=($da_summary->hsc_certificate == 'on') ? 'Y' : 'N'?></td>
        <td>--</td>
        <td>
          <?php
          if($da_summary->hsc_certificate == 'on')
          {
            echo $da_summary->supervisername;
          }
          ?>
        </td>

        <td>
          <?php
          if($da_summary->encrypthsccertificate){
            $hscresult = explode('|', $da_summary->encrypthsccertificate);
        /*echo "<pre>";
        print_r($matricresult);exit();*/
        foreach($hscresult as $res){
          // echo $res;
          ?>
          <a href="<?php if(!empty($res)) echo site_url().'datafiles/educationalcertificate/'.$res;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
          <?php } } ?>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">UG</td>
        <td align="left"> <?=$da_summary->ugspecialisation?></td>
        <td><?=$da_summary->ugpassingyear?></td>
        <td><?=$da_summary->ugpercentage?>%</td>
        <td><?=($da_summary->ug_certificate == 'on') ? 'Y' : 'N'?></td>
        <td><?=($da_summary->ugmigration_certificate == 'on') ? 'Y' : 'N'?></td>
        <td>
          <?php
          if($da_summary->ug_certificate == 'on')
          {
            echo $da_summary->supervisername;
          }
          ?>
        </td>
        <td>
          <?php
          if($da_summary->encryptugcertificate){
            $ugresult = explode('|', $da_summary->encryptugcertificate);
        /*echo "<pre>";
        print_r($matricresult);exit();*/
        foreach($ugresult as $res){
          // echo $res;
          ?>
          <a href="<?php if(!empty($res)) echo site_url().'datafiles/educationalcertificate/'.$res;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
          <?php } } ?>
        </td>
      </tr>
      <?php if($da_summary->pgspecialisation !='') { ?>
      <tr>
        <td class="font-weight-bold">PG</td>
        <td align="left"><?=$da_summary->pgspecialisation?></td>
        <td><?=$da_summary->pgpassingyear?></td>
        <td><?=$da_summary->pgpercentage?>%</td>
        <td><?=($da_summary->pg_certificate == 'on') ? 'Y' : 'N'?></td>
        <td><?=($da_summary->pgmigration_certificate == 'on') ? 'Y' : 'N'?></td>
        <td>
          <?php
          if($da_summary->pg_certificate == 'on')
          {
            echo $da_summary->supervisername;
          }
          ?>
        </td>
        <td>
          <?php
          if($da_summary->encryptpgcertificate){
            $pgresult = explode('|', $da_summary->encryptpgcertificate);
        /*echo "<pre>";
        print_r($matricresult);exit();*/
        foreach($pgresult as $res){
          // echo $res;
          ?>
          <a href="<?php if(!empty($res)) echo site_url().'datafiles/educationalcertificate/'.$res;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
          <?php } } ?>
        </td>
      </tr>
      <?php } ?>
      <?php if($da_summary->otherspecialisation !='') { ?>
      <tr>
        <td>OTHER</td>
        <td align="center"><?=$da_summary->otherspecialisation?></td>
        <td><?=$da_summary->otherpassingyear?></td>
        <td><?=$da_summary->otherpercentage?>%</td>
        <td><?=($da_summary->other_certificate == 'on') ? 'Y' : 'N'?></td>
        <td>--</td>
        <td>
          <?php
          if($da_summary->other_certificate == 'on')
          {
            echo $da_summary->supervisername;
          }
          ?>
        </td>
        <td>
          <?php
          if($da_summary->encryptothercertificate){
            $otherresult = explode('|', $da_summary->encryptothercertificate);
        /*echo "<pre>";
        print_r($matricresult);exit();*/
        foreach($otherresult as $res){
          // echo $res;
          ?>
          <a href="<?php if(!empty($res)) echo site_url().'datafiles/educationalcertificate/'.$res;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
          <?php } } ?>
        </td>
      </tr>
      <?php } ?>
      <?php
      if(!empty($da_gap_summary))
      {
    //print_r($da_gap_summary);
        ?>
        <tr>
          <td>Gap  Year:</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Reasons:</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Document Download</td>
          <td></td>
        </tr>
        <?php   
        foreach($da_gap_summary as $gap)
        {
      /*echo "<pre>";
      print_r($gap);
      exit();*/
      ?>
      <tr>
        <td><?=$this->gmodel->changedatedbformate($gap->fromdate)?>- <?=$this->gmodel->changedatedbformate($gap->todate)?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?=$gap->reason?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
          <?php
          if($gap->encrypteddocumentsname){
            $gapresult = explode('|', $gap->encrypteddocumentsname);
            foreach($gapresult as $res){
        /*echo "<pre>";
        print_r($res);exit();*/
        ?>
        <a href="<?php if(!empty($res)) echo site_url().'datafiles/educationalcertificate/'.$res;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
        <?php } } ?>
      </td>
      <td></td>
    </tr>
    <?php   
  }
}
?>
</tbody>
</table>

</div>
</div>        

<div class="row">
  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered text-center">
      <thead class="bg-light">
        <th>Documents</th>
        <th>BDF</th>
        <th>Offer Letter</th>
        <th>Joining  Report</th>
        <th>General  Nomination form</th>
        <th>Blood  Group Report</th>
        <th>Download Photo</th>
        <th>Work  Experience Certificate</th>
      </thead>
      <tbody>
        <tr>
          <td>Received  (Y/N)</td>
          <td><?=($da_summary->BDFFormStatus)? 'Y' : 'N' ?></td>
          <td>
            <?=($da_summary->offerletter)? 'Y' : 'N' ?>
            <!--  <a href="<?php if(!empty($da_summary->encryptofferlettername)) echo site_url().'datafiles/educationalcertificate/'.$da_summary->encryptofferlettername;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a> -->
          </td>
          <td>
            <?=($da_summary->joinstatus)? 'Y' : 'N' ?>
            <!--    <a href="<?php if(!empty($da_summary->joining_reportfile)) echo site_url().'datafiles/educationalcertificate/'.$da_summary->joining_reportfile;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a> -->
          </td>
          <td>
            <?=($da_summary->general_nomination_form == 'on')? 'Y' : 'N' ?>
            <!--   <a href="<?php if(!empty($da_summary->general_nominationfile)) echo site_url().'datafiles/educationalcertificate/'.$da_summary->general_nominationfile;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a> -->
          </td>
          <td><?=$da_summary->bloodgroup?></td>
          <td>
          <!-- <?php  
            if(  $da_summary->encryptedphotoname != ''){ ?>
              <a href="<?php if(!empty($da_summary->encryptedphotoname)) echo site_url().'datafiles/'.$da_summary->encryptedphotoname;?>" title="ImageName" download>
                <img src="<?php if(!empty($da_summary->encryptedphotoname)) echo site_url().'datafiles/'.$da_summary->encryptedphotoname;?>" width="160px" height="160" data-toggle="tooltip" alt="">
              </a>
            <?php }else{ ?>
              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'; ?>" width="160px" height="160" data-toggle="tooltip" alt="" >
              <?php } ?> -->

              <?php 
              if($da_summary->encryptedphotoname)
              {
                $file_path=''; 
                $file_path=FCPATH.'datafiles/'. $da_summary->encryptedphotoname;

                if(file_exists($file_path))
                {
                  ?>

                  <img src="<?php echo site_url().'datafiles/'. $da_summary->encryptedphotoname;?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">

                  <?php } 
                  else
                  {
                    ?>
                    <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                    <?php 
                  }
                }
                else
                {
                  ?>
                  <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                  <?php 

                }?>
              </td>
              <td>
                <?php
                if(!empty($da_work_experience)){
                  foreach($da_work_experience as $res){
            // print_r($res);exit;
                    ?>
                    <!-- <a href="<?php //if(!empty($da_work_experience)) echo site_url().'Proposed_probation_separation/downloadExperienceCertificate/'.$token;?>" ><i class="fa fa-download" style="font-size:15px;"></i></a> -->
                    <a href="<?php if(!empty($res->encrypteddocumnetname)) echo site_url().'datafiles/workexperience/'.$res->encrypteddocumnetname;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
                    <a href="<?php if(!empty($res->encryptedsalaryslip1)) echo site_url().'datafiles/workexperience/'.$res->encryptedsalaryslip1;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
                    <a href="<?php if(!empty($res->encryptedsalaryslip2)) echo site_url().'datafiles/workexperience/'.$res->encryptedsalaryslip2;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a>
                    <a href="<?php if(!empty($res->encryptedsalaryslip3)) echo site_url().'datafiles/workexperience/'.$res->encryptedsalaryslip3;?>" download><i class="fa fa-download" style="font-size:15px;"></i></a></br>
                    <?php
                  } }
                  ?>
                </td>
              </tr>
            </tbody>
          </table>


        </div>
      </div>  


      <br/>       
      <div class="row">
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">


          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered text-center">
            <thead class="bg-light">
              <tr>
                <th></th>
                <th colspan="3">Feedback  Reports</th>
                <th colspan="4"><div align="center">Assignment  Reports</div></th>
                <th width="13%">&nbsp;</th>
              </tr>
              <tr>
                <th></th>
                <th width="3%">1</th>
                <th width="3%">2</th>
                <th width="5%">3</th>
                <th>7    days report</th>
                <th width="8%">Village Stay</th>
                <th width="11%">Village Study</th>
                <th width="44%">Quarterly Reports (1-3)</th>
                <th>Final Reflective Report</th>
              </tr>
            </thead>
            <tr>
              <td>Received (Y/N)</td>
              <?php
              for($i=0; $i<3; $i++)
              {

                echo '<td>';
                if(array_key_exists($i, $da_feedback_summary))
                {
                  echo 'Y';
                }
                else
                {
                  echo 'N';
                }
                echo '</td>';
              }
              ?>
              <?php
              for($i=0; $i<3; $i++)
              {

                echo '<td>';
                if(array_key_exists($i, $da_reports_summary))
                {
                  echo 'Y';
                }
                else
                {
                  echo 'N';
                }
                echo '</td>';
              }
              ?>

              <td>--</td>
              <td>&nbsp;</td>
            </tr>
          </table>

        </div>
      </div>  



      <br/> 
      <form role="form" method="post" action="" name="formdasummary" id="formdasummary">
        <input type="hidden" value="<?=$da_summary->staffid?>" name="staffid">      
        <input type="hidden" value="<?=$da_summary->candidateid?>" name="candidateid">
        <input type="hidden" value="<?=$token?>" name="transid">
        <input type="hidden" value="<?=isset($da_summary->id_summary) ? $da_summary->id_summary : ''?>" name="id_summary">      
        <div class="row">
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">    
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="11%">Leave  Entitled:</td>
                <td width="16%"> <input type="number" id="leave_entitled" name="leave_entitled" value="<?=$da_summary->leave_entitled?>" class="form-control" required> </td>
                <td width="13%">Leave  Taken:</td>
                <td width="12%"><input type="number" class="form-control" id="leave_taken" name="leave_taken" value="<?=$da_summary->leave_taken?>" required></td>
                <td width="18%">Balance/Excess  leave:</td>
                <td width="30%"><input type="text" class="form-control" id="leave_balance" value="<?=($da_summary->leave_entitled-$da_summary->leave_taken)?>" name="balance_leave" readonly></td>
              </tr>
            </table>    
          </div>
        </div>


        <br/>       
        <div class="row">
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">

            <table width="100%" border ="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="20%">Date of joining apprenticeship:</td>
                <td width="10%"> <strong> <?=$this->gmodel->changedatedbformate($da_summary->doj_team)?> </strong></td>
                <td width="20%">Date of completion of apprenticeship:</td>
                <td width="10%"><input type="text" class="form-control datepicker" autocomplete="off" value="<?=$this->gmodel->changedatedbformate($da_summary->date_apprectice_completion)?>" name="date_apprectice_completion"></td>
                <td width="20%">Date of becoming Executive (Projects):</td>
                <td width="20%"><input type="text" class="form-control datepicker" autocomplete="off" value="<?=$this->gmodel->changedatedbformate($da_summary->date_executive_starting)?>" name="date_executive_starting"></td>
              </tr>
            </table>

          </div>
        </div>

        <br/>       
        <div class="row">
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">

            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="16%">Placement As Executive:</td>
                <td width="11%" class="font-weight-bold"> <?=$da_summary->officename?> </td>
                <td width="13%" Class="text-right">Supervisor:</td>
                <td width="12%" class="font-weight-bold"><?=$da_summary->supervisername?></td>
                <?php if($this->loginData->RoleID=="17" ) { ?>
                <td width="18%" class="text-right" >Basic Pay:</td>
                <td width="18%" class="font-weight-bold "> <input class="form-control text-right" type="inputbasicsalary" name="inputbasicsalary" value="<?=$basic_pay->basicsalary?>" required>  </td>
                <?php } else { ?>
                <td width="18%" class="text-right" ></td>
                <td width="18%" class="font-weight-bold "></td>

                <?php } ?>
              </tr>
            </table>

          </div>
        </div>
        <div class="panel-footer text-right">
          <?php if(!isset($da_summary->flag) || ($da_summary->flag == 0)) { ?>
          <input type="submit" name="btnsubmit" value='Save' class="btn btn-success btn-sm">
          <input type="submit" name="btnsubmit" value='Save and Submit' class="btn btn-success btn-sm">
          <?php } ?>
          <!--  <a href="<?=site_url('Proposed_probation_separation/index')?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> -->

          <input type="button" value="Go to List" onClick="javascript:history.go(-1)" />

        </div>
      </form>

    </div> 
  </div>


</div>



</div>
</div>
</div>  

<Script>
  $(document).ready(function(){

    $('#leave_entitled').on('blur', calculateLeaves);
    $('#leave_taken').on('blur', calculateLeaves);  
    $(".datepicker").datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: 'today',
      dateFormat : 'dd/mm/yy',
      yearRange: '1920:2030',
    });      
  });

  function calculateLeaves()
  {
    var leave_entitled =0;
    var leave_taken = 0;
    leave_entitled = parseInt($('#leave_entitled').val());
    if(isNaN(leave_entitled))
      leave_entitled = 0
    leave_taken = parseInt($('#leave_taken').val());
    if(isNaN(leave_taken))
      leave_taken = 0
    $('#leave_balance').val(leave_entitled - leave_taken);
  }
</script>

