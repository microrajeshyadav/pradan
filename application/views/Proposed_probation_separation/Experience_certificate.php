
  <!-- comment like this in html-->
  <style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  { 

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
</style>

<form role="form" method="post" action="">
<section class="content" style="background-color: #FFFFFF;" >
<?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

    <br>
    <div class="container-fluid">
       <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          
         <hr class="colorgraph"><br>
       </div>
      
     
     

    <div class="row" style="line-height: 3">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="9%">&nbsp;</td>
    <td width="81%" align="center"><h1><strong>To whom it may concern</strong></h1></td>
    <td width="10%">&nbsp;</td>
  </tr>
  <!-- Facilitate to leave -->
  <?php //echo "<pre>"; print_r($staffdetail); //die();
  // DA termination 
  if($staffdetail->trans_status == 0 && $staffdetail->trans_status == 'Facilitate to leave')
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td><p>This is to certify that Mr/Ms. <?php echo $staffdetail->name; ?> worked with Professional Assistance for Development Action (PRADAN) as Assistant from <?php echo $this->gmodel->changedatedbformate($staffdetail->doj_team); ?> to <?php echo $this->gmodel->changedatedbformate($staffdetail->releavingdate); ?> in Dholpur of South Rajasthan Development Cluster.</p></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><p>PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has 493 staff, including 386 professionals. It works in seven of the poorest States in India. </p></td>
    <td>&nbsp;</td>


  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><p>Mr. <?php echo $staffdetail->name; ?>’s major responsibility included maintaining cash and all accounting transactions, preparing indents and other tasks related to supporting the Team.</p></td>
    <td>&nbsp;</td>


  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- <tr>
    <td>&nbsp;</td>
    <td><p>I wish her luck in her future endeavors.</p></td>
    <td>&nbsp;</td>
  </tr> -->
  
  <tr>
    <td>&nbsp;</td>
    <!-- <td><p><?php echo $hrdetail->officename; ?> <?php echo $hrdetail->name; ?><br /> -->
    <td><p>Name of Executive Director<br />
     <p> <?php echo $eddetail->edname; ?></p></td>
    <td>&nbsp;</td>
  </tr>
  <?php } ?>

  <?php
  // Assistant Experience certificate as (In case of termination) 
  if($staffdetail->trans_status == 1 || $staffdetail->trans_status == 2 || $staffdetail->trans_status == 3 && $staffdetail->trans_status == 'Facilitate to leave')
    { ?>
      <tr>
    <td>&nbsp;</td>
    <td><p>This is to certify that Mr/Ms. <?php echo $staffdetail->name; ?> worked with Professional Assistance for Development Action (PRADAN) as Assistant from <?php echo $this->gmodel->changedatedbformate($staffdetail->doj_team); ?> to <?php echo $this->gmodel->changedatedbformate($staffdetail->releavingdate); ?> in Dholpur of South Rajasthan Development Cluster.</p></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><p>PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has 493 staff, including 386 professionals. It works in seven of the poorest States in India. </p></td>
    <td>&nbsp;</td>


  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><p>Mr. <?php echo $staffdetail->name; ?>’s major responsibility included maintaining cash and all accounting transactions, preparing indents and other tasks related to supporting the Team.</p></td>
    <td>&nbsp;</td>


  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- <tr>
    <td>&nbsp;</td>
    <td><p>I wish her luck in her future endeavors.</p></td>
    <td>&nbsp;</td>
  </tr> -->
  
  <tr>
    <td>&nbsp;</td>
    <!-- <td><p><?php echo $hrdetail->officename; ?> <?php echo $hrdetail->name; ?><br /> -->
    <td><p>Name of Executive Director<br />
     <p> <?php echo $eddetail->edname; ?></p></td>
    <td>&nbsp;</td>
  </tr>
<?php } ?>

 <?php
  // Executive Experience certificate as (In case of termination) 
  if($staffdetail->trans_status == 4 && $staffdetail->trans_status == 'Facilitate to leave')
    { ?>
      <tr>
    <td>&nbsp;</td>
    <td><p>Mr/Ms. <?php echo $staffdetail->name; ?> was employed by this organization as Executive from <?php echo $this->gmodel->changedatedbformate($staffdetail->doj_team); ?> to <?php echo $this->gmodel->changedatedbformate($staffdetail->releavingdate); ?>. Prior to that she participated in our in-house training programme for Development Apprentices for 12 months in Raigarh, North and South Chhattisgarh Development Cluster. This programme enables young professionals to develop the knowledge, skills and attitude necessary for a career in rural development.</p></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><p>PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has 493 staff, including 386 professionals. It works in seven of the poorest States in India</p></td>
    <td>&nbsp;</td>


  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><p>Mr. <?php echo $staffdetail->name; ?> was part of a team of professionals implementing various development programme in Mohagaon of Mahakausal Development Cluster, Madhya Pradesh.</p></td>
    <td>&nbsp;</td>


  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- <tr>
    <td>&nbsp;</td>
    <td><p>I wish her luck in her future endeavors.</p></td>
    <td>&nbsp;</td>
  </tr> -->
  
  <tr>
    <td>&nbsp;</td>
    <td><p>Name of Executive Director<br />
     <p> <?php echo $eddetail->edname; ?></p></td>
    <td>&nbsp;</td>
  </tr>
 <?php } ?>

 <!-- Facilitate to leave - end-->

 <!-- Resignation -->

  <?php //echo "<pre>"; print_r($staffdetail); //die();
  // DA Resign 
  if($staffdetail->trans_status == 0 && $staffdetail->trans_status == 'Resign')
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td><p>This is to certify that Mr/Ms. <?php echo $staffdetail->name; ?> worked with Professional Assistance for Development Action (PRADAN) as Assistant from <?php echo $this->gmodel->changedatedbformate($staffdetail->doj_team); ?> to <?php echo $this->gmodel->changedatedbformate($staffdetail->releavingdate); ?> in Dholpur of South Rajasthan Development Cluster.</p></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><p>PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has 493 staff, including 386 professionals. It works in seven of the poorest States in India. </p></td>
    <td>&nbsp;</td>


  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><p>Mr. <?php echo $staffdetail->name; ?>’s major responsibility included maintaining cash and all accounting transactions, preparing indents and other tasks related to supporting the Team.</p></td>
    <td>&nbsp;</td>


  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- <tr>
    <td>&nbsp;</td>
    <td><p>I wish her luck in her future endeavors.</p></td>
    <td>&nbsp;</td>
  </tr> -->
  
  <tr>
    <td>&nbsp;</td>
    <!-- <td><p><?php echo $hrdetail->officename; ?> <?php echo $hrdetail->name; ?><br /> -->
    <td><p>Name of Executive Director<br />
     <p> <?php echo $eddetail->edname; ?></p></td>
    <td>&nbsp;</td>
  </tr>
  <?php } ?>

  <?php
  // Assistant Experience certificate as (In case of Resign) 
  if($staffdetail->trans_status == 1 || $staffdetail->trans_status == 2 || $staffdetail->trans_status == 3 && $staffdetail->trans_status == 'Resign')
    { ?>
      <tr>
    <td>&nbsp;</td>
    <td><p>This is to certify that Mr/Ms. <?php echo $staffdetail->name; ?> worked with Professional Assistance for Development Action (PRADAN) as Assistant from <?php echo $this->gmodel->changedatedbformate($staffdetail->doj_team); ?> to <?php echo $this->gmodel->changedatedbformate($staffdetail->releavingdate); ?> in Dholpur of South Rajasthan Development Cluster.</p></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><p>PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has 493 staff, including 386 professionals. It works in seven of the poorest States in India. </p></td>
    <td>&nbsp;</td>


  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><p>Mr. <?php echo $staffdetail->name; ?>’s major responsibility included maintaining cash and all accounting transactions, preparing indents and other tasks related to supporting the Team.</p></td>
    <td>&nbsp;</td>


  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- <tr>
    <td>&nbsp;</td>
    <td><p>I wish her luck in her future endeavors.</p></td>
    <td>&nbsp;</td>
  </tr> -->
  
  <tr>
    <td>&nbsp;</td>
    <!-- <td><p><?php echo $hrdetail->officename; ?> <?php echo $hrdetail->name; ?><br /> -->
    <td><p>Name of Executive Director<br />
     <p> <?php echo $eddetail->edname; ?></p></td>
    <td>&nbsp;</td>
  </tr>
<?php } ?>

 <?php
  // Executive Experience certificate as (In case of Resign) 
  if($staffdetail->trans_status == 4 && $staffdetail->trans_status == 'Resign')
    { ?>
      <tr>
    <td>&nbsp;</td>
    <td><p>Mr/Ms. <?php echo $staffdetail->name; ?> was employed by this organization as Executive from <?php echo $this->gmodel->changedatedbformate($staffdetail->doj_team); ?> to <?php echo $this->gmodel->changedatedbformate($staffdetail->releavingdate); ?>. Prior to that she participated in our in-house training programme for Development Apprentices for 12 months in Raigarh, North and South Chhattisgarh Development Cluster. This programme enables young professionals to develop the knowledge, skills and attitude necessary for a career in rural development.</p></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><p>PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has 493 staff, including 386 professionals. It works in seven of the poorest States in India</p></td>
    <td>&nbsp;</td>


  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><p>Mr. <?php echo $staffdetail->name; ?> was part of a team of professionals implementing various development programme in Mohagaon of Mahakausal Development Cluster, Madhya Pradesh.</p></td>
    <td>&nbsp;</td>


  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- <tr>
    <td>&nbsp;</td>
    <td><p>I wish her luck in her future endeavors.</p></td>
    <td>&nbsp;</td>
  </tr> -->
  
  <tr>
    <td>&nbsp;</td>
    <td><p>Name of Executive Director<br />
     <p> <?php echo $eddetail->edname; ?></p></td>
    <td>&nbsp;</td>
  </tr>
 <?php } ?>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


</div>

<hr/>

</div>
<div class="panel-footer text-right">
  <?php 
  if($staffdetail->trans_status == 'Facilitate to leave' || $staffdetail->trans_status == 'Resign')
  { ?>
  <button class="btn btn-success" type="submit" name="savebtnpdf" id="savebtnpdf" data-toggle="tooltip" title="Want to send as a PDF? Click on me.">Save as a PDF</button>
  <?php } ?>
  <a href="<?php echo site_url("Proposed_probation_separation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</div>
</div>
</section>

</form>