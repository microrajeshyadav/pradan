<style type="text/css">
.checkboxStatus{
  color:red;
}
</style>
<section class="content">
  <br>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <div class="panel thumbnail shadow-depth-2 listcontainer" style="min-height: 600px;" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-8 panel-title pull-left">Proposed Transfer</h4>
             <div class="col-md-4 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
        <div class="panel-body">                
          <form method="POST" action="" name="separation" id="separation">
            <div class="row " >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-light" style="padding: 10px; ">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                 <div class="form-group">
                   <div class="col-md-4"> 
                    <label class="field-wrapper required-field">DAShip Transactions <span style="color:red;"> *</span></label> 

                  </div>
                  <div class="col-md-8"> 
                   <!--  <select name="dashiptransactions" id="dashiptransactions" class="form-control" required="required" >
                      <option value="">Select</option>
                      <?php  foreach ($getintimation as $key => $value) { 
                        if ($value->id == $gettransferdetails->intemation_type) {

                          ?>
                          <option value="<?php echo $value->id; ?>" SELECTED><?php echo $value->intimation; ?></option>
                        <?php } else{?>

                         <option value="<?php echo $value->id; ?>"><?php echo $value->intimation; ?></option>
                       <?php } } ?>
                     </select> -->
                     <input type="hidden" value="<?php echo $getintimation->id;?>" name="dashiptransactions">
                     <input type="text" readonly="readonly" class="form-control" value="<?php if(!empty($getintimation->intimation)) echo $getintimation->intimation;?>" name="dashiptransactions1">
                   </div>
                   <?php echo form_error("dashiptransactions");?>
                 </div>
               </div>
             </div>


             <!--  start txtdatranfer here -->
             <div id="txtdatranfer" >
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
               <div class="form-group">
                <div class="form-line">
                  <label class="field-wrapper required-field">Old DC <span style="color:red;"> *</span></label>
                  <!-- <select name="olddc" id="olddc" class="form-control" required="required">
                    <option value="">Select</option>
                    <?php foreach ($getdevelopmentcluster as $key => $value) {
                      if ($value->dc_cd == $gettransferdetails->dc_cd) {
                        ?>
                        <option value="<?php echo $value->dc_cd;?>" SELECTED><?php echo $value->dc_name;?></option>
                      <?php }else{  ?>
                        <option value="<?php echo $value->dc_cd;?>"><?php echo $value->dc_name;?></option>
                      <?php } } ?>
                    </select> -->
                    <input type="hidden" value="<?php echo $gettransferdetails->dc_cd;?>" name="olddc">
                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $gettransferdetails->dc_name;?>" name="olddc1">
                  </div>
                  <?php echo form_error("olddc");?>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
               <div class="form-group">
                <div class="form-line">
                  <label class="field-wrapper required-field">Old Team <span style="color:red;"> *</span></label>
                  <!-- <select name="oldteam" id="oldteam" class="form-control" required="required">
                   <option value="">Select Team</option>
                   <?php foreach ($getofficedetails as $key => $value) {
                    if ($value->officeid == $gettransferdetails->new_office_id) {
                      ?>
                      <option value="<?php echo $value->officeid;?>" SELECTED><?php echo $value->officename;?></option>
                    <?php }else{  ?>
                     <option value="<?php echo $value->officeid;?>"><?php echo $value->officename;?></option>
                   <?php } } ?>
                 </select> -->
                 <input type="hidden" value="<?php echo $gettransferdetails->new_office_id;?>" name="oldteam">
                 <input type="text" readonly="readonly" class="form-control" value="<?php echo $gettransferdetails->officename;?>" name="oldteam1">
               </div>
               <?php echo form_error("oldteam");?>
             </div>
           </div>


           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
             <div class="form-group">
              <div class="form-line">
                <label class="field-wrapper required-field">DA Name <span style="color:red;"> *</span></label>
              <!--   <select name="staff" id="staff" class="form-control" required="required">
                 <option value="">Select Development Apprenticeship </option>
                 <?php foreach ($getdevelopmentapprentice as $key => $value) {
                  if ($value->staffid == $gettransferdetails->staffid) {
                    ?>
                    <option value="<?php echo $value->staffid;?>" SELECTED><?php echo $value->name;?></option>
                  <?php }else{  ?>
                   <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>
                 <?php } } ?>
               </select> -->
               <input type="hidden" value="<?php echo $gettransferdetails->staffid;?>" name="staff">
               <input type="text" readonly="readonly" class="form-control" value="<?php echo $gettransferdetails->name;?>" name="staff1">
             </div>
             <?php echo form_error("staff");?>
           </div>
         </div>



         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
           <div class="form-group">
            <div class="form-line">
              <label class="field-wrapper required-field">New DC <span style="color:red;"> *</span></label>
              <select name="newdc" id="newdc" class="form-control" required="required">
                <option value="">Select</option>
                <?php foreach ($getdevelopmentcluster as $key => $value) {
                  ?>
                  <option value="<?php echo $value->dc_cd;?>"><?php echo $value->dc_name;?></option>
                <?php } ?>
              </select>
            </div>
            <?php echo form_error("newdc");?>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
         <div class="form-group">
          <div class="form-line">
            <label class="field-wrapper required-field">New Team <span style="color:red;"> *</span></label>
            <select name="newteam" id="newteam" class="form-control" required="required">
             <option value="">Select Team</option>
           </select>
         </div>
         <?php echo form_error("newteam");?>
       </div>
     </div>

     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
       <div class="form-group">
        <div class="form-line">
          <label class="field-wrapper required-field">New field guide <span style="color:red;"> *</span></label>
          <select name="new_field_guide" id="new_field_guide" required="required" class="form-control">
           <option value="">Select New field guide</option>
         </select>
       </div>
       <?php echo form_error("new_field_guide");?>
     </div>
   </div>

   <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 resondiv" >
    <div class="form-group">
      <div class="form-line">
        <label class="field-wrapper required-field">Transfer date :<span style="color:red;"> *</span></label>
        <input type="text" name="dateoftransfer" id="dateoftransfer" class="form-control datepicker"  value="" required="required">
      </div>
      <?php echo form_error("dateoftransfer");?>
    </div>
  </div>

  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="background-color: white;">
    <div class="form-group">
      <div class="form-line">
        <label class="field-wrapper required-field">Comments <span style="color:red;"> *</span></label>
        <textarea name="commenttransfer" id="commenttransfer" class="form-control" maxlength="150" required="required"></textarea>
      </div>
      <?php echo form_error("commenttransfer");?>
    </div>

  </div>

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer text-right" >
   <input class="btn btn-warning" type="reset" value="Reset" data-toggle="tooltip" title="Want to reset your changes? Click on me.">
   <button class="btn btn-success" type="submit" name="savebtntranfer" id="savebtntranfer" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save & Send</button>
 </div>
</div>
</div> 
</div>
</div>
</div> 
</div>
</section>

<script type="text/javascript">
  $('#probation_completed').change(function(){
    if($('#probation_completed').val() == 1)
    {
     $('#probatextension').css('display', 'block');
   }
   else if($('#probation_completed').val() == 2)
   {
     $('proyes').css('display', 'block');
   }
   else{
     $('#probatextension').css('display', 'none');
     $('proyes').css('display', 'none');
     $('#eprobation_extension_date').prop('required',false);
     $('.Probationdivrt').css('display', 'none');
     
   }
 });


  $(document).ready(function(){
    $("#savebtnsubmit").on("click",function(){

      if (($("input[name*='test']:checked").length)<=0) {
        alert("Please tick the Clearance all document");
      }
      return true;
    });


    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
   });



    if($('#probation_completed').val() == 1)
    {
     $('#probatextension').css('display', 'block');
   }
   else 
   {
     $('proyes').css('display', 'block');
   }

   $("#dc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#team").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

   $("#olddc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#oldteam").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


   $("#newdc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#newteam").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


   $("#oldteam").change(function(){
   // alert('sdfsd');
   $.ajax({
    url: '<?php echo site_url(); ?>ajax/getTeamStaffTransfer/'+$(this).val(),
    type: 'POST',
    dataType: 'text',
  })
   .done(function(data) {

    console.log(data);
    $("#staff").html(data);

  })
   .fail(function() {
    console.log("error");
  })
   .always(function() {
    console.log("complete");
  });

 });


   $("#newteam").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#new_field_guide").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

   $("#probationteam").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getTeamStaff/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#probationstaff").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
   


   $("#probation_completed").change(function(){
    var dac = $('#probationstaff').val(); 
    if (dac == '') {
      alert('Please Select Staff !!!');
      $("#probation_completed").rempveAtt('Selecttd', false);
    }else{

       // alert(dac);

       $.ajax({
        url: '<?php echo site_url(); ?>Ajax/get_DAshipid/'+ dac,
        type: 'POST',
        dataType: 'text',
      })
       .done(function(data) {

        $("#Probatdiv").html(data);

      })
       .fail(function() {
        console.log("error");
      })
       .always(function() {
        console.log("complete");
      });
     } 
   });

 }); 

  $("#probationdc").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Cluster_Team/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#probationteam").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



  $("#team").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Development_Apprentice/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#da").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });



  $('#type').change(function(){
    var typeid = $(this).val();
    if(typeid == 1)
    {
     $(".Probationdiv").show();
     $(".resondiv").hide();
  // $(".Mandatorytext").prop('required', 'required');

} else if(typeid == 2){
  $(".resondiv").show();

  $(".Probationdivrt").hide();
  $(".probation_extension_date").hide();
  $("#probextedate").hide();

      // $(".Mandatorytext").prop('required', ''); 
    } 
  }); 

  $('#probation_completed').change(function(){
    var probation_completedid = $(this).val();
    if(probation_completedid == 1)
    {
     $(".Probatdiv").show(); 
     $(".probation_extension_date").hide();  
   } else if(probation_completedid == 2){
    $(".probation_extension_date").show();
    $(".Probatdiv").hide();
  } 
});




  $('#dashiptransactions').change(function(){
 // alert('sdsadsad');
 var dashiptransact = $(this).val();

 if(dashiptransact ==1 ) {
  $('#txtseparation').show();
  $('#txtseparation').find('input, textarea, button, select').attr('disabled',false);
 // $("#savebtnsubmit").prop('disabled', 'disabled');
 $('#txtextension').hide();
 $('#txtextension').find('input, textarea, button, select').attr('disabled','disabled');
 $('#txtdatranfer').hide();
 $('#txtdatranfer').find('input, textarea, button, select').attr('disabled','disabled');

} else if(dashiptransact == 2 ) {
  $('#txtseparation').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtseparation').hide();
  $('#txtextension').show();
  $('#txtextension').find('input, textarea, button, select').attr('disabled',false);
  $('#txtdatranfer').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtdatranfer').hide();
}else {

  $('#txtseparation').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtseparation').hide();
  $('#txtextension').find('input, textarea, button, select').attr('disabled','disabled');
  $('#txtextension').hide();
  $('#txtdatranfer').show();
  $('#txtdatranfer').find('input, textarea, button, select').attr('disabled',false);
}

});
</script>
