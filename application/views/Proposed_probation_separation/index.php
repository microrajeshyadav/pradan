
<section class="content" style="background-color: #FFFFFF;" >    <br>
  <div class="container-fluid">       
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Separation</h4>
         <div class="col-md-2 text-right">      
         </div>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?> 
          <?php ?>
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
            <div class="col-lg-6 col-md-6"></div>                    
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tableprodationsepration" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                  <th>S.No. </th>
                  <th>DA Code </th>
                  <th>DA Name </th>
                  <th>Intemation Type </th>             
                  <th>Comments</th>
                  <th>Action</th>
                </tr> 
              </thead>
              <tbody>
                <?php  if (count($getprobationdetails) == 0) {
                  ?>         
                  <?php }else{ ?>
                  <?php
                  $count = 1;
                  foreach($getprobationdetails as $row) {             
                    ?>           
                    <tr>
                      <td><?php echo $count; ?></td>          
                      <td><?php echo $row->emp_code; ?></td>
                      <td><?php echo $row->name; ?></td>
                      <td><?php echo $row->intimation; ?></td>            
                      <td><?php echo $row->comment; ?></td>                
                      <td class="text-center">           
                        <?php if($row->status == 1){ ?>
                        <a href="<?php echo site_url('Proposed_probation_separation/intimate/'.$row->id);?>" class="btn btn-info btn-sm" title="Initiate" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        <?php }else{ ?>
                        <span class="badge badge-success">Initiated</span>
                        <?php } ?>
                      </td>     
                    </tr>
                    <?php $count ++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Seperation Approval</h4>
             <div class="col-md-2 text-right">        
             </div>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
            <div class="col-lg-6 col-md-6"></div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tableprodationseprationapproval" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                  <th>S.No.</th>
                  <th>DA Code</th>
                  <th>DA Name</th>
                  <th>Intemation Type</th>             
                  <th>Comments</th>
                  <th>Action</th>
                </tr> 
              </thead>
              <tbody>
                <?php  if (count($getprobationdetailsapproval) == 0) {
                  ?>         
                  <?php }else{ ?>
                  <?php
                  $count = 1;
                  foreach($getprobationdetailsapproval as $row) {              
                    ?>           
                    <tr>
                      <td><?php echo $count; ?></td>          
                      <td><?php echo $row->emp_code; ?></td>
                      <td><?php echo $row->name; ?></td>
                      <td><?php echo $row->process_type; ?></td>
                      <!-- <td><?php echo $row->probation_completed_date; ?></td> -->
                      <td><?php echo $row->scomments; ?></td>                
                      <td class="text-center">
                        <a href="<?php echo site_url('Developmentapprentice_sepemployeeexitform/index/'.$row->transid);?>" class = "btn btn-danger btn-sm" title="Exit Interview Form" data-toggle="tooltip"><i class="fa fa-sign-out-alt" aria-hidden="true"></i>
                        </a>
                        <?php if($row->status >=6 && !empty($row->tbl_da_clearance_certificate_id) && $row->tbl_da_clearance_certificate_flag == 1){ ?>
                        <a href="<?php echo site_url('Developmentapprentice_sepchecksheet/index/'.$row->transid);?>" class = "btn btn-info btn-sm" title="Checksheet Form" data-toggle="tooltip"><i class="fas fa-clipboard-check"></i></a><br><br>
                        <a href="<?php echo site_url('Developmentapprentice_sepclearancecertificate/view/'.$row->tbl_da_clearance_certificate_id); ?>" class = "btn btn-warning btn-sm" title="Clearance Certificate Form" data-toggle="tooltip">
                          <i class="fa fa-certificate" aria-hidden="true"></i></a>
                          <?php } ?>
                          <?php if($row->status == 7 && !empty($row->tbl_da_clearance_certificate_hrid)){ ?>
                          <a href="<?php echo site_url('Proposed_probation_separation/Experience_certificate/'.$row->transid.'/'.$row->staffid);?>" class = "btn btn-success btn-sm" title="Experience Certificate" data-toggle="tooltip"><i class="fas fa-stamp"></i></a>
                          <?php } ?>
                        </td>     
                      </tr>        
                      <?php $count ++; } } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">      
              <div class="row">
               <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to Graduate</h4>
               <div class="col-md-2 text-right">       
               </div>
             </div>
             <hr class="colorgraph"><br>
           </div>
           <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                <form method="post" action="<?=site_url('Proposed_probation_separation/graduateDA')?>">
                  <input type="hidden" name="da_staffid" id="da_staffid" value="">
                  <input type="button" class="btn btn-success btn-sm" name="btn_graduate" id="btn_graduate" value="Graduate">
                </form>
              </div>
            </div>
            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              <div class="col-lg-6 col-md-6"></div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tableprodationgraduate" class="table table-bordered table-striped wrapper">
                  <thead>
                   <tr>
                    <th class="no-sort"><input type="checkbox" name="checkall" value="1"> </th>
                    <th>DA Code</th>
                    <th>DA Name</th>
                    <th>Email ID</th>
                    <th>Gender</th>
                    <th>Comments</th>
                    <th class="text-center">Action</th>
                  </tr> 
                </thead>        
                <?php  if (count($getgraduatedetails) == 0) {?>
                <tbody>
                </tbody>
                <?php }else{ ?>
                <?php
                $count = 1;
                foreach($getgraduatedetails as $row) {             
                 ?>
                 <tbody>
                  <tr>
                    <td>
                      <input type="checkbox" class="chkbox_recommend" name="da_staffid[]" value="<?=$row->staffid?>" <?php if(!($row->tbl_da_exit_interview_form_id && $row->tbl_da_clearance_certificate_id && $row->tbl_da_check_sheet_id && $row->tbl_da_summary)){ echo 'disabled="disabled"'; }?>> 
                    </td>          
                    <td><?php echo $row->emp_code; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->emailid; ?></td>
                    <td><?php echo $row->gender; ?></td>           
                    <td><?php echo $row->comment; ?></td>                
                    <td class="text-center"> 
                    

                      <?php if($row->status==1){?>

                     <!--  <a href="<?php echo site_url('Proposed_probation_separation/intimate_Recommended_to_graduate/'.$row->id);?>"  class = "btn btn-info btn-sm" title="Initiate DA Graduation Process" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                      </a> -->

                      <a href="" data-toggle="modal" onclick="openassignteam(<?php echo $row->id;?>)"  class = "btn btn-info btn-sm" title="Assign Team and Initiate" data-toggle="tooltip" data-toggle="modal" data-target="#modalTeamAssign"><i class="fas fa-user-tag" aria-hidden="true"></i>
                      </a>
                      <?php } else { ?>
                      <?php if($row->tbl_da_exit_interview_form_id){ ?>
                      <a class = "btn btn-danger btn-sm" title="Exit Interview Form" data-toggle="tooltip" href="<?php echo site_url('Developmentapprentice_graduatingemployeeexitform/index/'.$row->tbl_da_exit_interview_form_id);?>"><i class="fa fa-sign-out-alt" aria-hidden="true"></i>

                      </a>
                      <?php } else { ?>
                      <a href="javascript:;" class = "btn btn-dark btn-sm" title="Exit Interview Form" data-toggle="tooltip"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>
                      <?php } ?>

                      <?php if($row->tbl_da_check_sheet_id){ ?>
                      <a href="<?php echo site_url('Developmentapprentice_sepchecksheet_fc/index/'.$row->tbl_da_check_sheet_id);?>" class = "btn btn-info btn-sm" title="Check Sheet Form" data-toggle="tooltip"><i class="fas fa-clipboard-check"></i></a>              
                      <?php } else { ?>
                      <a href="javascript:;" class = "btn btn-dark btn-sm" title="Check Sheet Form" data-toggle="tooltip"><i class="fas fa-clipboard-check"></i>

                      </a>
                      <?php } ?> 

                      <?php if($row->tbl_da_clearance_certificate_id){ ?>
                      <a href="<?php echo site_url('Developmentapprentice_sepclearancecertificate_fc/view/'.$row->tbl_da_clearance_certificate_id);?>"  class = "btn btn-warning btn-sm" title="Clearance Certificate Form" data-toggle="tooltip"><i class="fa fa-certificate" aria-hidden="true"></i></a>
                      <?php } else { ?>
                      <a href="javascript:;" class = "btn btn-dark btn-sm" title="Clearance Certificate Form" data-toggle="tooltip"><i class="fa fa-certificate" aria-hidden="true"></i>
                      </a>
                      <?php } ?>
                      <?php if($row->tbl_da_exit_interview_form_id && $row->tbl_da_clearance_certificate_id && $row->tbl_da_check_sheet_id ){?>
                      <a href="<?php echo site_url('Proposed_probation_separation/summary/'.$row->tbl_da_check_sheet_id);?>" class = "btn btn-success btn-sm" title="Summary" data-toggle="tooltip"><i class="fa fa-id-card" aria-hidden="true"></i></a>
                      <?php } else { ?>
                      <a href="javascript:;" class = "btn btn-dark btn-sm" title="Summary" data-toggle="tooltip"><i class="fa fa-id-card" aria-hidden="true"></i>
                      </a>
                      <?php } } ?>
                    </td>     
                  </tr>
                  <?php if (isset($row->type) && $row->type==2) { ?>
                  <tr>
                   <td colspan="7"><a href=""> Exit Interview Form</a></td>
                 </tr>
                 <tr>
                  <td colspan="7"><a href="">Check Sheet Form</a></td>
                </tr>
                <tr>
                  <td colspan="7"><a href="">Clearance Certificate Form</a></td>
                </tr>
              </tbody>
              <?php }   $count ++; } } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
    <script>
      $(function(){
        $('input[name="checkall"]').click(function(){
      //alert('sdfs');
      if($(this).prop("checked") == true)
      {
        $('input[class="chkbox_recommend"]').each(function(){
          $(this).prop('checked', $(this).is(":enabled"));
        })
      }
      else
      {
        $('input[class="chkbox_recommend"]').each(function(){
          $(this).prop('checked', false);
        })
      }
    }) 

        $('#btn_graduate').click(function(){
          var ids = '';
          $('input[class="chkbox_recommend"]').each(function(){
            if($(this).is(":checked"))
            {
              if(ids == '')
                ids = $(this).val();
              else
                ids += ','+$(this).val();
            }       
          });
          $('#da_staffid').val(ids);
      //alert(ids);
      if(ids!='')
        $(this).parent().submit();
    })   
      })
    </script>

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to Tranfer</h4>
         <div class="col-md-2 text-right">   
         </div>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
        <div class="col-lg-6 col-md-6"></div>               
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tableprodationtransfer" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>DA Code</th>
              <th>DA Name</th>
              <th>Gender</th>
              <th>Email ID</th>
              <th>New Office Name</th>
              <th>Old Office Name</th>
              <th>Comments</th>
              <th>Action</th>
            </tr> 
          </thead>
          <tbody>
            <?php if (count($gettransferdetails)== 0){?>

            <?php }else{ ?>

            <?php
            $count = 1;
            foreach($gettransferdetails as $row) { 
              ?>
              <tr>
                <td><?php echo $count; ?></td>  
                <td><?php echo $row->emp_code; ?></td>        
                <td><?php echo $row->name; ?></td>
                <td><?php echo $row->gender; ?></td>
                <td><?php echo $row->emailid; ?></td>
                <td><?php echo $row->officename; ?></td>
                <td><?php echo $row->oldofficename; ?></td>
                <td><?php echo $row->comment; ?></td>                
                <td class="text-center">
                  <?php if ($row->status==1) { ?>
                  <a href="<?php echo site_url('Proposed_probation_separation/add/'.$row->transid);?>" class= "btn btn-info btn-sm" title="Initiate" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                  <?php   } elseif ($row->status==2) {?>
                  <span class="badge badge-success">  Initiated </span>
                  <?php } ?>
                </td>     
              </tr>
              <?php  $count ++; } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>



  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Proposed Recommended to Extention</h4>
       <div class="col-md-2 text-right">

       </div>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <!-- <?php //print_r($getextentiondetails); ?> -->
   <div class="panel-body">
    <div class="row" style="background-color: #FFFFFF;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
      <div class="col-lg-6 col-md-6"></div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
        <table id="tableprodationExtention" class="table table-bordered table-striped wrapper">
          <thead>
           <tr>
            <th>S.No.</th>
            <th>DA Code</th>
            <th>DA Name</th>
            <th>Gender</th>
            <th>Email ID</th>
            <th>Probation Extension Date</th>
            <th>Comments</th>
            <th>Action</th>
          </tr> 
        </thead>
        <tbody>
          <?php  if (count($getextentiondetails) == 0) {
            ?>

            <?php }else{ ?>

            <?php
            $count = 1;
            foreach($getextentiondetails as $row) { ?>
            <tr>
              <td><?php echo $count; ?></td>          
              <td><?php echo $row->emp_code; ?></td>
              <td><?php echo $row->name; ?></td>
              <td><?php echo $row->gender; ?></td>
              <td><?php echo $row->emailid; ?></td>
              <td><?php echo $this->gmodel->changedatedbformate($row->probation_completed_date); ?></td>
              <td><?php echo $row->comment; ?></td> 

              <td class="text-center">
                <?php if ($row->status==1) { ?>  
                <a href="<?php echo site_url('Proposed_probation_separation/addextention/'.$row->transid);?>" class= "btn btn-info btn-sm" title="Initiate" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                <?php }else{ ?>
                <span class="badge badge-success">  Initiated </span>
                <?php } ?>
              </td>     
            </tr>

            <?php  $count ++; } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</section>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="hidemodal();">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <label for="Name">Seperation Date <span style="color: red;" >*</span></label>
            <input type="text" name="seperation_date" id="seperation_date" class="form-control datepicker" required>
            <input type="hidden" name="intemationid" id="intemationid" value="" class="form-control" required>
          </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hidemodal();">Close</button>
          <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="seperationdatesubmit();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
<!-- Facilitate Leave Modal -->
<div class="container">
  <div class="modal fade" id="modalTeamAssign" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5>Assign Team and Initiate</h5> <button type="button"  class="close" data-dismiss="modal">&times;</button> 
        </div>
        <div class="modal-body">         
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <input id="hdnstaffid" type="hidden" value="">
            <label for="Name">Change Team <span style="color: red;" >*</span></label>
            <select name="team"  id="team" class="form-control">
              <?php foreach($teamsdetails as $row) {
                if($row->officeid == $singleteamedetail->teamid){
                  ?>
                  <option value="<?php echo $row->officeid; ?>" Selected><?php echo $row->officename; ?> </option>
                  <?php }else {?>
                  <option value="<?php echo $row->officeid; ?>" ><?php echo $row->officename; ?> </option>

                  <?php } } ?>
                </select>
              </div>
            </div>
            <div class="modal-footer">
           

               <a id ="BtnInitiate" href=""  class = "btn btn-info btn-sm" title="Initiate DA Graduation Process" data-toggle="tooltip"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                      </a> 


             
            </div>
          </div>

        </div>
      </div>
    </div> 
    <!-- Modal -->
    <script>
      function openassignteam(staffid,siteurl)
      {
        $('#hdnstaffid').val(staffid);
        var teamid = $('#team').val();
        $('#BtnInitiate').attr("href", "intimate_Recommended_to_graduate/"+staffid+"/"+teamid);
      }

      $('#team').on('change', function() {
      
       var teamid = $('#team').val();
      
        var staffid = $('#hdnstaffid').val();
        $('#BtnInitiate').attr("href", "intimate_Recommended_to_graduate/"+staffid+"/"+teamid);
      });


      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tableprodationsepration').DataTable(); 
        $('#tableprodationseprationapproval').DataTable();
        $('#tableprodationgraduate').DataTable(); 
        $('#tableprodationtransfer').DataTable(); 
        $('#tableprodationExtention').DataTable(); 
        $(".datepicker").datepicker({
          changeMonth: true,
          changeYear: true,
          minDate: 'today',
          dateFormat : 'dd-mm-yy',
          yearRange: '1920:2030',
        });

      });

      $('#probation_completed').change(function(){
        if($('#probation_completed').val() == 1)
        {
         $('#probatextension').css('display', 'block');
       }
       else if($('#probation_completed').val() == 2)
       {
         $('probatextension').css('display', 'block');
       }
       else{

         $('#probatextension').css('display', 'none');
         $('#eprobation_extension_date').prop('required',false);
       }
     });

      function confirm_delete() {
        var r = confirm("Do you want to delete this Separation ?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }

      function seperationdatepopup(token){
        if(token){   
          $("#seperation_date").val('');
          $("#intemationid").val(token);
          $('#myModal').removeClass('fade');
          $("#myModal").show();
        }
      }
  /*function seperationdatepopupleave(token){
    if(token){   
      $("#seperationleave_date").val('');
      $("#leaveid").val(token);
      $('#myModalleave').removeClass('fade');
      $("#myModalleave").show();
    }
  }*/
  function seperationdatesubmit(){    
    var seperation_date = $("#seperation_date").val();
    var intemationid = $("#intemationid").val();
    if(seperation_date ==''){
      $('#seperation_date').focus();
    }
    if(seperation_date !='' && intemationid !=''){
      window.location.href = "<?php echo site_url('Proposed_probation_separation/intimate'); ?>"+'/'+intemationid+'/'+seperation_date;
      // alert("<?php //echo site_url('Proposed_probation_separation/intimate'); ?>"+'/'+intemationid+'/'+seperation_date);
    }

  }
  /*function seperationdatesubmitleave(){    
    var seperationleave_date = $("#seperationleave_date").val();
    var leaveid = $("#leaveid").val();
    if(seperationleave_date ==''){
      $('#seperationleave_date').focus();
    }
    if(seperationleave_date !='' && leaveid !=''){
      window.location.href = "<?php //echo site_url('Proposed_probation_separation/intimate'); ?>"+'/'+leaveid+'/'+seperationleave_date;
      // alert("<?php //echo site_url('Proposed_probation_separation/intimate'); ?>"+'/'+intemationid+'/'+seperation_date);
    }

  }*/


</script>  


