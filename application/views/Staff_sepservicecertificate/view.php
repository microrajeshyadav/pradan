<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - SERVICE CERTIFICATE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center" style="margin-bottom: 50px;">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> </div>
        <div class="col-md-6 pull-left"><em>No. ____________________</em></div>
        <div class="col-md-6 pull-right text-right"><em>Date: ________________</em></div>
        <div class="col-md-12 text-center" style="margin-bottom: 20px;">
          <p><h4>SERVICE CERTIFICATE</h4></p>
          <p>(to be filled in duplicate)</p>
        </div>      

        
      </div>
      <div class="row" style="line-height: 2">
        <div class="col-md-12">
          Name of the Employee : ___________________________________
        </div>
        <div class="col-md-12">
          Employee Code : _______________________________
        </div>     
        <div class="col-md-12">
          Date of joining PRADAN : ______________________________
        </div>
        <div class="col-md-12">
          Designation at the time of joining : __________________________________
        </div>
        <div class="col-md-12">
          Date of leaving PRADAN : __________________________________
        </div>
        <div class="col-md-12">
          Designation at the time of leaving PRADAN : __________________________________
        </div>
        <div class="col-md-2">
          Salary details
        </div>
        <div class="col-md-10">
         <p>Basic Pay: _________________</p>
         <p>HRA       : ________________</p>
       </div>
       <div class="col-md-12">
        Reasons for leaving : __________________________________
      </div>
    </div>

    <div class="row text-left" style="margin-top: 20px; ">
      <div class="col-md-6">
        Place: ________________________
      </div>
      <div class="col-md-6">
        Signature    : __________________________
      </div>
    </div>

    <div class="row text-left" style="margin-top: 20px; ">
     <div class="col-md-6">
      Date : ________________
    </div>
    <div class="col-md-6">
      Name          : __________________________
    </div>
  </div>
  <div class="row text-left" style="margin-top: 20px; ">
   <div class="col-md-6">
    
   </div>
   <div class="col-md-6">
    Designation: __________________________
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    cc: - Personal Dossier (PD)
  </div>
</div>
</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>