<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
</style>
<section class="content" style="background-color: #FFFFFF;" >
<!-- <?php print_r($certificate_detail);  ?> -->
  <br>

  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - SERVICE CERTIFICATE
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
     <form method="POST" action="" name="service_certificate" id="service_certificate">
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center" style="margin-bottom: 50px;">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> </div>
        <div class="col-md-6 pull-left"><em>No. <input type="text" class="inputborderbelow" name="separation_no" value="<?php if($certificate_detail){ echo $certificate_detail->separation_no;}?>" required></em></div>
        <div class="col-md-6 pull-right text-right"><em>Date: <input type="text" class="inputborderbelow" name="currentdate" value="<?php echo date('d/m/Y');?>" readonly></em></div>
        <div class="col-md-12 text-center" style="margin-bottom: 20px;" required>
          <h4>SERVICE CERTIFICATE</h4>
          <p>(to be filled in duplicate)</p>
        </div>      

        
      </div>
      <div class="row" style="line-height: 2">
        <div class="col-md-12">
          Name of the Employee : <input type="text" class="inputborderbelow" name="" value="<?php echo $staff_certificate->name;?>" readonly required>
        </div>
        <div class="col-md-12">
          Employee Code : <input type="text" class="inputborderbelow" name="" value="<?php echo $staff_certificate->emp_code;?>" readonly required>
        </div>     
        <div class="col-md-12">
          Date of joining PRADAN : <input type="text" class="inputborderbelow" name="" value="<?php echo $this->gmodel->changedatedbformate($staff_certificate->joniningdate);?>" readonly required>
        </div>
        <div class="col-md-12">
          Designation at the time of joining : <input type="text" class="inputborderbelow" name="" value="<?php echo $staff_certificate->joiningdesignation;?>" readonly required>
        </div>
        <div class="col-md-12">
          Date of leaving PRADAN : <input type="text" class="inputborderbelow" name="" value="<?php echo $staff_certificate->leavingdate;?>" readonly required>
        </div>
        <div class="col-md-12">
          Designation at the time of leaving PRADAN : <input type="text" class="inputborderbelow" name="" value="<?php echo $staff_certificate->leavingdesignation;?>" readonly required>
        </div>
        <div class="col-md-2">
          Salary details
        </div>
        <div class="col-md-10">
         <p>Basic Pay: <input type="text" class="inputborderbelow txtNumeric" name="basic_pay" value="<?php  if($certificate_detail){echo $certificate_detail->basic_pay;}?>" required></p>
         <p>HRA : <input type="text" class="inputborderbelow txtNumeric" name="hra" value="<?php if(!empty($certificate_detail->hra)){echo $certificate_detail->hra;}?>" required></p>
       </div>
       <div class="col-md-12">
        Reasons for leaving : <input type="text" class="inputborderbelow txtOnly" name="" value="<?php echo $staff_certificate->leavingreason;?>" readonly required>
      </div>
    </div>

    <div class="row text-left" style="margin-top: 20px; ">
      <div class="col-md-6">
        Place: <input type="text" class="inputborderbelow txtOnly" name="" value="<?php echo $staff_certificate->officename;?>" readonly>
      </div>
     <!--  <div class="col-md-6">
        Signature    : <input type="text" class="inputborderbelow" name="" value="">
      </div> -->
    </div>

    <div class="row text-left" style="margin-top: 20px; ">
     <div class="col-md-6">
      Date : <input type="text" class="inputborderbelow" name="" value="<?php echo date('d/m/Y');?>" readonly>
    </div>
    <div class="col-md-6">
      Name          : <input type="text" class="inputborderbelow" name="" value="<?php echo $reporting_detail->name;?>" readonly>
    </div>
  </div>
  <div class="row text-left" style="margin-top: 20px; ">
   <div class="col-md-6">
    
   </div>
   <div class="col-md-6">
    Designation: <input type="text" class="inputborderbelow" name="" value="<?php echo $reporting_detail->designation;?>" readonly>
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    cc: - Personal Dossier (PD)
  </div>
</div>
</div>
<hr/>
<div class="row ">
   <div class="col-md-12 text-right" style="margin-top: 20px;">
   (<input type="text" class="inputborderbelow" name="" value="<?php echo $ed_detail->edname;?>">)
   </div>
   <div class="col-md-12 text-right" style="margin-top: 20px;">
  Executive Director
   </div>

 </div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($certificate_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
  <?php
    }else{

      if($certificate_detail->flag == 0){
 ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
<?php } ?>
 
<?php } ?>
 <a href="<?php echo site_url("Staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</form>
</div>
</div>
</section>
<?php 

if($certificate_detail){
if($certificate_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  });
  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
</script>
<?php }} ?>
