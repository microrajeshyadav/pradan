<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
   <?php 
   print_r($this->session->flashdata);
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>

      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <form method="post" action="" name="form_clearance" id="form_clearance">
          <div class="panel-heading">

            <div class="row">
             <div class="col-md-12 text-left" >
               <h4 class="col-md-12 panel-title pull-left">Holiday list</h4>

               <p > (to be filled in duplicate)</p>
             </div>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <table id="tableholiday" class="table table-bordered table-striped">
              <thead>
               <tr>
                <th style ="max-width:50px;" class="text-center">S.No.</th>
                <th>Officename</th>
                <th>CalenderYear</th>
                <th>Holidayname</th>
                <th>HolidayDate</th>
                <th style ="max-width:50px;" class="text-center">Action</th>
              </tr> 
            </thead>
            <tbody>
              <?php if ($Holidaylist != "") {

               $i=0;
               foreach ($Holidaylist as $value) {
                ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td>
                    <input type="hidden" value="<?php echo $value->officeid;?>"  id="office_id_<?php echo $i;?>" >
                    <input type="hidden" value="<?php echo $value->officename;?>"  id="office_name_<?php echo $i;?>" >
                    <?php echo $value->officename;?></td>
                    <td>
                     <input type="hidden" value="<?php echo $value->calender_year;?>"  id="calender_year_<?php echo $i;?>" >
                     <?php echo $value->calender_year;?></td>
                     <td>
                      <input type="hidden" value="<?php echo $value->holiday_name;?>"  id="Holiday_id_<?php echo $i;?>" >
                      <input type="hidden" value="<?php echo $value->Holiday;?>"  id="Holiday_<?php echo $i;?>" >
                      <?php echo $value->Holiday;?></td>
                      <td>
                       <input type="hidden" value="<?php echo $this->gmodel->changedatedbformate($value->holiday_date);?>"  id="holiday_date_<?php echo $i;?>" >
                       <?php echo $this->gmodel->changedatedbformate($value->holiday_date);?></td>

                       <td class="panel-footer text-right">


                        <button type="button" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Holidaylist." onclick="holidayeditfunction(<?php echo $i; ?>);" value="<?php echo $value->h_id;?>" id="edit_holidaylist_<?php echo $i;?>"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></button> 
                        <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Holidaylist." href="<?php echo site_url()."Holidaylist/delete/".$value->h_id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
                        </a></td>
                      </tr> 
                      <?php $i++; } }?>
                    </tbody>
                  </table>
                </div>
                <input type="hidden" id="holiday_id" name="holiday_id" value="">
                <div class="col-md-3 form-line">
                  <div class="row">
                    <div class="col-lg-12 col-md-2 col-xs-2 col-sm-2 " style="margin-bottom: 15px;"> Office :
                      <select name="officeid" class="form-control" id="officeid">

                        <?php foreach ($getlpoffice as $key => $value) { ?>
                          <option value="<?php echo  $value->officeid;?>"><?php echo  $value->officename; ?></option>
                        <?php } ?>



                      </select></div>
                    </div>
                    <div class="row">

                      <div class="col-lg-12 col-md-2 col-xs-2 col-sm-2" style="margin-bottom: 15px;">  Calender Year :<select name="Calender_year"  id="Calender_year" class="form-control">
                        <?php for($i=date('Y');$i<=date('Y')+1;$i++)
                        {
                          echo '<option value='.$i.'>'.$i.'</option>';
                        }
                        ?>

                      </select></div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12 col-md-2 col-xs-2 col-sm-2" style="margin-bottom: 15px;">Holiday name:
                        <select name="holiday_name" class="form-control" id="holiday_name">

                          <?php foreach ($getholiday as  $value) { ?>
                            <option value="<?php echo  $value->ID;?>"><?php echo  $value->Holiday;?></option>
                          <?php } ?>
                        </select></div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-2 col-xs-2 col-sm-2" style="margin-bottom: 15px;"> Holiday date :

                          <input type="text" required="required" data-date-inline-picker="true" name="holiday_date" id="holiday_date" class="form-control datepicker" value="<?php if(!empty($Holidaylist->holiday_date)) echo $Holidaylist->holiday_date;?>" ></div>
                        </div>
                      </div>
                    </div>



                    <div class="panel-footer text-right col-md-12">
                      <div class="col-md-12">
                        <button onclick="addnew();" type="button" id = "btnaddnew " class="btn btn-dark btn-sm"> Add New</button>
                        <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
                        <a href="" class="btn btn-primary btn-sm"> Go Back</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </section>



            <script type="text/javascript">


             function addnew()
             {
              // alert('');
              $('#holiday_id').val('');
              $('#officeid').prop("disabled", false);
              $('#Calender_year').prop("disabled", false);
              $('#holiday_name').prop("disabled", false);
              $('#holiday_date').val('');
              $('#officeid option').eq(0).prop('selected', true);
              $('#Calender_year option').eq(0).prop('selected', true);
              $('#holiday_name option').eq(0).prop('selected', true);


                // $('#holiday_id').val('');
                // $('#holiday_id').val('');
              }

              $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip(); 
                $('#tableholiday').DataTable({
                  "paging": true,
                  "search": true,
                });
              });


              $(document).ready(function(){


                $(".datepicker").datepicker({
                 changeMonth: true,
                 changeYear: true,
    // maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',

  });


              });
              $("#holiday_name").change(function() {

               var holidayname = $("#holiday_name").val();
      // alert(holidayname);

      $.ajax({
        url: '<?php echo site_url(); ?>Holidaylist/getholidate/'+ holidayname,
        type: 'POST',
        dataType: 'text',


      })
      .done(function(data) {
        console.log(data);
     // alert(data.substring(6, 10));
     if (data != '')
     {
      data = data.replace(data.substring(6, 10), $('#Calender_year').val()) ; 
      $("#holiday_date").val('');
      $("#holiday_date").val(data);
    }
    else
    {
      if ($('#holiday_id').val()=='')  {
        $("#holiday_date").val('');
      }
    }

  })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    });

              function holidayeditfunction(id)
              {
   // var hh=document.getel
   var dd=document.getElementById("edit_holidaylist_"+id).value;
    // alert(dd);
    $("#holiday_id").val(dd);
    var office_name=$("#office_name_"+id).val();
    var office_id=$("#office_id_"+id).val();
        //alert("office_name="+office_name);
        var $option = $("<option selected></option>").val(office_id).text(office_name); 
        $('#officeid').append($option).trigger('change'); 
        $('#officeid').attr('disabled','disabled');

        var calender_year=$("#calender_year_"+id).val();

         //alert("calender_year="+calender_year);
         var $option = $("<option selected></option>").val(calender_year).text(calender_year); 
         $('#Calender_year').append($option).trigger('change'); 
         $('#Calender_year').attr('disabled','disabled');
       // $('#Calender_year').attr('selected','selected');
       var Holiday_id=$("#Holiday_id_"+id).val();

       var holiday=$("#Holiday_"+id).val();

       var $option = $("<option selected></option>").val(Holiday_id).text(holiday); 
       $('#holiday_name').append($option).trigger('change'); 
       $('#holiday_name').attr('disabled','disabled');
       var holiday_date=$("#holiday_date_"+id).val();
         // alert(holiday_date);
         $("#holiday_date").val(holiday_date);

       }

       function confirm_delete() {

        var r = confirm("Do you want to delete Holiday list record?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }


    </script>