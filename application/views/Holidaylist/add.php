<?php foreach ($role_permission as $row) { if ($row->Controller == "Batch" && $row->Action == "index"){ ?>
<br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Holiday List</h4>
      <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new H? Click on me." href="<?php echo site_url()."Holidaylist/add";?>" class="btn btn-primary btn-sm">Add New Holiday</a>
      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <!-- <div class="col-md-12"> -->
            <div class="panel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              <!-- </div> -->
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>


            <div class="container-fluid">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="tablecampus" class="table table-bordered table-striped">
                  <thead>
                   <tr>
                    <th style ="max-width:50px;" class="text-center">S. No.</th>
                    <th>Office</th>
                    <th>Calender Year</th>
                    <th>Holiday name</th>
                    <th>Holiday date</th>
                    <th  style ="max-width:70px;" class="text-center"> Status </th>

                    <th style ="max-width:50px;" class="text-center">Action</th>
                  </tr> 
                </thead>
                <tbody>
                  <?php 
                  $i=0;
                  foreach ($Holidaylist as $key => $value) { ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->Office;?></td>
                    <td><?php echo $value->CalenderYear;?></td>
                    <td><?php echo $value->Holidayname;?></td>
                    <td><?php echo $value->Holidaydate;?></td>
                    <td class="text-center"><?php
                    if ($value->status==0) {
                      echo "Active"; 
                    }else{
                      echo "InActive"; 
                    }

                    ?></td>
                    <td class="panel-footer text-right"><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Holidaylist." href="<?php echo site_url()."Holidaylist/edit/".$value->id;?>" ><i class="fa fa-edit" aria-hidden="true" id="ID"></i></a> | <!-- <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Holidaylist." href="<?php echo site_url()."Holidaylist/delete/".$value->id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i> -->
</a></td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>
      </div>
    </div>   
    <?php } } ?>
    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
        var indno =  id.replace('sendtoed_','');
        var candidateid = $('#candidateid_'+indno).val();
//alert(candidateid);

$.ajax({
  url: '<?php echo site_url(); ?>Assigntc/sendofferlettered/'+ candidateid,
  type: 'POST',
  dataType: 'text',
})

.done(function(data) {

  if (data==1) {
   alert('Send Offer Letter To ED');
 }else{
  alert('Already send Offer Letter To ED ');
}

})

});


      function confirm_delete() {
        var r = confirm("Do you want to delete this Batch?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }


    </script>
