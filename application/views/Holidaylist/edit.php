<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <form method="post" action="" name="form_clearance" id="form_clearance">
      <div class="panel-heading">

        <div class="row">
       <div class="col-md-12 text-left" >
       <h4 class="col-md-12 panel-title pull-left">Holiday list</h4>
    
        <p > (to be filled in duplicate)</p>
      </div>
    </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <table id="tablecampus" class="table table-bordered table-striped">
                  <thead>
                   <tr>
                    <th style ="max-width:50px;" class="text-center">S.No.</th>
                    <th>officename</th>
                    <th>CalenderYear</th>
                    <th>Holidayname</th>
                    <th>Holidaydate</th>

                    <th style ="max-width:50px;" class="text-center">Action</th>
                  </tr> 
                </thead>
                <tbody>



                  
                  <?php 
                 $i=0;
                 foreach ($Holidaylist as $value) {

                  //print_r($value);exit();
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->Calender_year;?></td>
                    <td><?php echo $value->Holiday;?></td>
                    <td><?php echo $value->HolidayDate;?></td>
                    
                    <td class="panel-footer text-right"><a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Holidaylist." href="<?php echo site_url()."Holidaylist/edit/".$value->id;?>" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Holidaylist." href="<?php echo site_url()."Holiday/delete/".$value->id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
</a></td>
                  </tr> 
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>

       <div class="col-md-3 form-line">
      <div class="row">
<div class="col-lg-12 col-md-2 col-xs-2 col-sm-2 " style="margin-bottom: 15px;"> Office :
  <select name="officeid" class="form-control">
 
  <?php foreach ($getlpoffice as $key => $value) { ?>
    <option value="<?php echo  $value->officeid;?>"><?php echo  $value->officename; ?></option>
   <?php } ?>

  
 
</select></div>
</div>
      <div class="row">
      
<div class="col-lg-12 col-md-2 col-xs-2 col-sm-2" style="margin-bottom: 15px;">  Calender Year :<select name="Calender_year" class="form-control">
  <?php for($i=date('Y');$i<=date('Y')+1;$i++)
{
    echo '<option value='.$i.'>'.$i.'</option>';
}
?>
 
</select></div>
</div>
<div class="row">
<div class="col-lg-12 col-md-2 col-xs-2 col-sm-2" style="margin-bottom: 15px;">Holiday name:
  <select name="holiday_name" class="form-control" id="holiday_name">

<?php foreach ($getholiday as  $value) { ?>
    <option value="<?php echo  $value->ID;?>"><?php echo  $value->Holiday;?></option>
   <?php } ?>
</select></div>
</div>
<div class="row">
<div class="col-lg-12 col-md-2 col-xs-2 col-sm-2" style="margin-bottom: 15px;"> Holiday date :

<input type="date" data-date-inline-picker="true" name="holiday_date" id="holiday_date" class="form-control" ></div>
</div>

</div>



</div>



<div class="panel-footer text-right col-md-12">
  <div class="col-md-12">
  <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</form>
</div>
</div>
</section>



<script type="text/javascript">

   $("#holiday_name").change(function() {
          
       var holidayname = $("#holiday_name").val();
       alert(holidayname);

    $.ajax({
      url: '<?php echo site_url(); ?>Holidaylist/getholidate/'+ holidayname,
      type: 'POST',
      dataType: 'text',


    })
    .done(function(data) {
      console.log(data);

      $("#holiday_date").val(data);
     
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


    
        

</script>