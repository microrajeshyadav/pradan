<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaverequest" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <form method="post" action="" name="applicationform">
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Credit General Leaves </h4>

           <div class="col-md-2 text-right">
            <a href ="<?php echo site_url('leaverequest/index/'.$this->loginData->staffid)?>" class="btn btn-sm btn-dark" >Back</a>
           <!--  <button type="submit" name="Apply" class="btn btn-sm btn-primary" title="Add a new leave request? Click on me."  data-toggle="tooltip" data-placement="bottom">Apply</button> -->
          <!--   <a href ="<?php //echo site_url('Leaveaccured/accuredApply/'); ?>" class="btn btn-sm btn-primary" >Credit</a> -->
                <button type="submit" name="Credit" class="btn btn-sm btn-primary" title=" Click here to credit leave for the selected period."  data-toggle="tooltip" data-placement="bottom">Credit</button>

            <!--  <a href="<?php //echo site_url()."Leaverequest/index/" .$this->loginData->staffid;?>" class="btn btn-sm btn-primary" title="Add a new leave request? Click on me."  data-toggle="tooltip" data-placement="bottom">Apply</a> -->

          </div>
        </div>
        <hr class="colorgraph"><br>
      </div>


  <div class="panel-body" style="font-family: 'Oxygen'">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

    <div class="row" style="padding: 10px;">
      <div class="col-md-3">
   <h5> Financial Year:  <?php echo $fyear; ?></h5>
 </div> 
   <div class="col-md-9">

   <select name="period" id="period" class="form-control col-md-3">
     <option value="0">Select</option>
     <?php foreach ($creditperiod as $key => $value) { 
      if (date('Y-m-d') <= $value->monthtos && date('Y-m-d') >= $value->monthfroms) {
     
      ?>
        <option value="<?php echo $value->monthfroms ?>" Selected="Selected"><?php echo $this->gmodel->changedatedbformate($value->monthfroms).' to '. $this->gmodel->changedatedbformate($value->monthtos);?></option>
    <?php  } else{?>
      <option value="<?php echo $value->monthfroms ?>"><?php echo $this->gmodel->changedatedbformate($value->monthfroms).' to '. $this->gmodel->changedatedbformate($value->monthtos);?></option>
    <?php } } ?>
    
   </select>
 </div> 
    </div>
  <div class="row" style="background-color: #FFFFFF;">
    <!-- <input type="hidden" name="office_id" id="office_id" value="<?php echo $getreportingdetails->new_office_id;?>">
    
    <hr/> -->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card" style="min-height: 34px;">

          <div class="card-body bg-light">
            <table class="table" id="tableleave" class="table table-bordered table-striped table-hover dataTable js-exportable">
              <thead>
                <tr class="bg-info text-white">
                  <th class="text-center">Sr.No</th>
                   <th class="text-left">Staff Code</th>
                  <th class="text-left">Staff Name</th>
                  <th class="text-left">Designation</th>
                   <th class="text-left">Office</th>
                  <th class="text-left">Leave Type</th>                 
                  <th class="text-right">Leave Credited</th>
                 
                </tr>
              </thead>
              <tbody>
                <?php 
                $i=0;
                foreach ($leavebalance as $key => $value) {
                 ?>
                 <tr>
                  <td class="text-center"><?php echo $i+1;?>. </td>
                  <td class="text-left"><?php echo $value->emp_code;?></td>
                  <td class="text-left"><?php echo $value->Name;?></td>
                  <td class="text-left"><?php echo $value->desname;?></td>
                   <td class="text-left"><?php echo $value->officename;?></td>
                  <td class="text-left">General</td>                 
                  <td class="text-right"><?php echo $value->Accured;?></td>
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div>
   </div>
     </div> 
   </form>
 </div>
</div>
</div>   
<?php } } ?>
</section>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#tableleave').DataTable({
      "paging": true,
      "search": true,
    });
  });
</script>
