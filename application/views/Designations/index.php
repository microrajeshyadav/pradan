
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">  
   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Designation
       </h4>
       <div class="col-md-2 pull-right">
         <a href="<?php echo site_url("Designations/add/")?>" class="btn btn-primary btn-xs">Add New Designations </a>
       </div>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Designations" && $row->Action == "index"){ ?>

   <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php  //print_r($state_details); ?>
        <!-- Exportable Table -->       
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

          <table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th class="text-center" style="width: 60px;">S. No.</th>
                <th>Name</th>
                <th >Short Name</th>
                <th >Level </th>
                <th class="text-center" style="max-width: 50px;">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i=0; foreach($operation_details as $row){ ?>
              <tr>
                <td class="text-center"><?php echo $i+1; ?></td>
                <td ><?php echo $row->desname; ?></td>
                <td ><?php echo $row->shortname; ?></td>
                <td ><?php echo $row->level; ?></td>
                <td class="text-center">
                  <a href="<?php echo site_url('Designations/edit/'.$row->desid);?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to edit this Designation."><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>
                  |
                  <a href="<?php echo site_url('Designations/delete/'.$row->desid);?>" id="statedl" data-toggle="tooltip" onclick="return confirm_delete()" style="padding : 4px;" title="Click here to delete Designations"><i class="fa fa-trash" style="color:red"></i></a>
                </td>
              </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        </div>        
        <!-- #END# Exportable Table -->
      </div>
      <?php } } ?>
    </div>
  </div>
</section>

<script>
  $(document).ready(function() {
   $('[data-toggle="tooltip"]').tooltip(); 
   $('#tbldesignations').DataTable();
 });
  function confirm_delete() {

    var r = confirm("Do you want to delete this Designations");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>