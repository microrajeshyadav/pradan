<style type="text/css">

ul.nav-wizard {
  background-color: #f1f1f1;
  border: 1px solid #d4d4d4;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 3px;
  position: relative;
  overflow: hidden;
}
ul.nav-wizard:before {
  position: absolute;
}
ul.nav-wizard:after {
  display: block;
  position: absolute;
  left: 0px;
  right: 0px;
  top: 138px;
  height: 47px;
  border-top: 1px solid #d4d4d4;
  border-bottom: 1px solid #d4d4d4;
  z-index: 11;
  content: " ";
}
ul.nav-wizard li {
  position: relative;
  float: left;
  height: 46px;
  display: inline-block;
  text-align: center;
  padding: 0 20px 30px;
  margin: 0;
  
  line-height: 36px;
  background-color: #DEE2E6;
}
ul.nav-wizard li a {
  color: #428bca;
  padding: 0;
}
ul.nav-wizard li a:hover {
  background-color: transparent;
}
ul.nav-wizard li:before {
  position: absolute;
  display: block;
  border: 24px solid transparent;
  border-left: 16px solid #d4d4d4;
  border-right: 0;
  top: -1px;
  z-index: 10;
  content: '';
  right: -16px;
}
ul.nav-wizard li:after {
  position: absolute;
  display: block;
  border: 24px solid transparent;
  border-left: 16px solid #DEE2E6;
  border-right: 0;
  top: -1px;
  z-index: 10;
  content: '';
  right: -15px;
}
ul.nav-wizard li.active {
  color: #fff;
  background: #17A2B8;
}
ul.nav-wizard li.active:after {
  border-left: 16px solid #17A2B8;
}
ul.nav-wizard li.active a,
ul.nav-wizard li.active a:active,
ul.nav-wizard li.active a:visited,
ul.nav-wizard li.active a:focus {
  color: #fff;
  background: #17A2B8;
}
ul.nav-wizard .active ~ li {
  color: #999999;
  background: #f9f9f9;
}
ul.nav-wizard .active ~ li:after {
  border-left: 16px solid #f9f9f9;
}
ul.nav-wizard .active ~ li a,
ul.nav-wizard .active ~ li a:active,
ul.nav-wizard .active ~ li a:visited,
ul.nav-wizard .active ~ li a:focus {
  color: #999999;
  background: #f9f9f9;
}
ul.nav-wizard.nav-wizard-backnav li:hover {
  color: #468847;
  background: #f6fbfd;
}
ul.nav-wizard.nav-wizard-backnav li:hover:after {
  border-left: 16px solid #f6fbfd;
}
ul.nav-wizard.nav-wizard-backnav li:hover a,
ul.nav-wizard.nav-wizard-backnav li:hover a:active,
ul.nav-wizard.nav-wizard-backnav li:hover a:visited,
ul.nav-wizard.nav-wizard-backnav li:hover a:focus {
  color: #468847;
  background: #f6fbfd;
}
ul.nav-wizard.nav-wizard-backnav .active ~ li {
  color: #999999;
  background: #ededed;
}
ul.nav-wizard.nav-wizard-backnav .active ~ li:after {
  border-left: 16px solid #ededed;
}
ul.nav-wizard.nav-wizard-backnav .active ~ li a,
ul.nav-wizard.nav-wizard-backnav .active ~ li a:active,
ul.nav-wizard.nav-wizard-backnav .active ~ li a:visited,
ul.nav-wizard.nav-wizard-backnav .active ~ li a:focus {
  color: #999999;
  background: #ededed;
}

</style>

<div class="row" style="padding: 14px;">
  <section>
    <div class="wizard">

      <ul id="menumain" class="nav nav-wizard">
        <li id="li1" class="<?php echo ($this->router->fetch_class() == 'Staff_sepclearancecertificate')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepclearancecertificate/index/'.$token;?>" >CLEARANCE CERTIFICATE</a>
        </li>

        <li id="li1" class="<?php echo ($this->router->fetch_class() == 'Staff_sepservicecertificate')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepservicecertificate/index/'.$token;?>" >SERVICE CERTIFICATE</a>
        </li>

        <li id="li2" class="<?php echo ($this->router->fetch_class() == 'Staff_sepexperiencecertificate')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepexperiencecertificate/index/'.$token;?>" >EXPERIENCE CERTIFICATE</a>
        </li>

        <li id="li3" class="<?php echo ($this->router->fetch_class() == 'Staff_sepemployeeexitform')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepemployeeexitform/index/'.$token;?>" >EXIT INTERVIEW FORM</a>
        </li>

        <li id="li4" class="<?php echo ($this->router->fetch_class() == 'Staff_sepchecksheet')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepchecksheet/index/'.$token;?>">CHECK SHEET</a>
        </li>
        <li id="li5" class="<?php echo ($this->router->fetch_class() == 'Staff_sepcacceptanceofresignation')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepcacceptanceofresignation/index/'.$token;?>" >ACCEPTANCE OF RESIGNATION</a>
        </li>
        <li id="li6" class="<?php echo ($this->router->fetch_class() == 'Staff_sepiomformalities')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepiomformalities/index/'.$token;?>" >SEPARATION FORMALITIES</a>
        </li>

        <li id="li7" class="<?php echo ($this->router->fetch_class() == 'Staff_sepinformtionforsup')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepinformtionforsup/index/'.$token;?>" >SUPERANNUATION</a>
        </li>
        <li id="li8" class="<?php echo ($this->router->fetch_class() == 'Staff_sepdischargenotice')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepdischargenotice/index/'.$token;?>">DISCHARGE NOTICE</a>
        </li>
        <li id="li9" class="<?php echo ($this->router->fetch_class() == 'Staff_sepreleasefromservice')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepreleasefromservice/index/'.$token;?>" >RELEASE FROM SERVICE</a>
        </li>
         <li id="li10" class="<?php echo ($this->router->fetch_class() == 'Staff_sepreleasefromservice')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sepwithdrawalofgratuity/index/'.$token;?>" > WITHDRAWAL OF GRATUITY </a>
        </li>
         <li id="li11" class="<?php echo ($this->router->fetch_class() == 'Staff_sepreleasefromservice')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Leave_encashment/index/'.$token;?>" > LEAVE ENCASHMENT</a>
        </li>
         <li id="li12" class="<?php echo ($this->router->fetch_class() == 'Staff_sepreleasefromservice')? 'active' : 'disabled'; ?>">
          <a href="<?php echo site_url().'Staff_sephanded_over_taken_over/index/'.$token;?>" > HANDING OVER/TAKING OVER CHARGE</a>
        </li>
      </ul>
    </div>
  </section>
</div>
