<section class="content">
  <div class="container">
    <br>
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
         <form name="campus" action="" method="post" >
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
                <h4 class="col-md-10 panel-title pull-left">Change Password</h4>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">
             <div class="row">
              <div class="col-lg-12" role="alert">
                <div class="alert alert-warning" role="alert">
                  It's a good idea to use a strong password that you're not using else where  
                  </div>
              </div>

              <div class="col-lg-12">
               <div class="col-lg-1"> </div>  
               <div class="col-lg-6">
                <label>Username :</label>
                <input type="text" name="username" id="username" value="<?php echo $this->loginData->Username; ?>" class="form-control" placeholder="Enter User Name"  readonly required="required">
              </div>
              <div class="col-lg-5"> </div>  
            </div>


            <div class="col-lg-12">
             <div class="col-lg-1"> </div>  
             <div class="col-lg-6">
              <label> Old Password: </label>
              <input type="password" maxlength="80"  name="oldpassword" id="oldpassword" class="form-control" value="" placeholder="Enter Old Password" required="required">
            </div>
            <div class="col-lg-5"> </div>  
          </div>

          <div class="col-lg-12">
           <div class="col-lg-1"> </div>  
           <div class="col-lg-6">
             <label>New Password:</label>
             <input type="password" maxlength="80"  name="newpassword" id="newpassword" class="form-control" value="" placeholder="Enter Old Password" required="required">
             <span class="npassc"></span>
           </div>
           <div class="col-lg-5"> </div>   
         </div>

         <div class="col-lg-12">
           <div class="col-lg-1"> </div>  
           <div class="col-lg-6">
            <label> Confirm Password </label>
            <input type="password" name="confirmpassword" id="confirmpassword" maxlength="50" class="form-control" value="" placeholder="Enter Confirm Password" required="required">
            <span class="npassc"></span>
          </div>
          <div class="col-lg-5"> </div>  
        </div>


      </div>
    </div>
    <div class="col-lg-12 panel-footer text-right">

      <button type="submit" class="btn btn-success" onclick="return ifanyvalidation();">Save </button>
      <button type="reset" class="btn btn-warning">Reset </button>

    </div>
  </div><!-- /.panel-->
</form> 

</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script>
 function ifanyvalidation(){
  var newpassword = $('#newpassword').val();
  var confirmpassword = $('#confirmpassword').val();
  var oldpassword = $('#oldpassword').val();



  if (oldpassword =='') {
   $('#oldpassword').focus();
   return false;
 }
 if (newpassword =='') {
  $('.npassc').html("Please Enter New password !!!");
  $('#newpassword').focus();
  return false;
}
if (confirmpassword =='') {
  $('.npassc').html("Please Enter confirm password !!!");
  $('#confirmpassword').focus();
  return false;
}

if (newpassword != confirmpassword) {
  $('.npassc').html("New password and confirm password not match please retype !!!");
  $('#newpassword').focus();
  return false;
}



}

</script>