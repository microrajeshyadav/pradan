<section class="content">
  <?php //foreach ($role_permission as $row) { if ($row->Controller == "Tc_daship_components" && $row->Action == "index"){ ?>
<br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
<?php //echo "<pre>"; print_r($dacomponent_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
<div class="panel thumbnail shadow-depth-2 listcontainer" >
                  <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-7 panel-title pull-left">List Of  DAship Components</h4>
                     <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
             

            <div class="body">
              <form name="tc" id="td" action="" method="post">
              <div class="row panel-footer">
                <div class="col-lg-2"></div>
                <div class="col-lg-1">Batch <span style="color: red;">*</span></div>
                <div class="col-lg-3">    
                     <?php 
                      $options = array('' => 'Select Batch ');
                       // $selected_batch = $batch_details->id; 
                      foreach($batch_details as$key => $value) {
                        $options[$value->id] = $value->batch;
                      }
                      echo form_dropdown('batchid', $options, set_value('batchid'), 'class="form-control" id="batchid" required="required"' );
                      ?>
                  </div>
                <div class="col-lg-1">Phase-Activity<span style="color: red;">*</span></div>
                <div class="col-lg-3"> 
                  <?php 
                  $options = array('' => 'Select Phase-Activity');
                       // $selected_batch = $batch_details->id; 
                  foreach($phase_details as$key => $value) {
                    $options[$value->id] = $value->phase_name;
                  }
                  echo form_dropdown('phaseid', $options, set_value('phaseid'), 'class="form-control" id="phaseid" required="required"' );
                  ?></div>
                <div class="col-lg-2">
                   <button  type="submit" name="Search"  id="Search" class="btn btn-round btn-dark btn-sm" data-toggle="tooltip" title="Search" value="Search">Search</button>
                </div>
              </div>
              </form>
              <div class="table-responsive">
                <table id="tablecampus" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center">Id</th>
                    <!--   <th class="text-center">Code</th> -->
                      <th class="text-center">Office Name</th>
                      <th class="text-center">Name</th>
                      <th class="text-center">Gender</th>
                      <th class="text-center">Email id</th>
                      <th class="text-center">Phase-Activities</th>
                      <th class="text-center">From Date </th>
                      <th class="text-center">To Date</th>
                      <th class="text-center">Field guide Name</th>
                       <th class="text-center">Status</th>
                      <th class="text-center">Attachment</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <?php //echo "<pre>"; print_r($dacomponent_details); die(); ?>
                  <tbody>
                    <?php 
                      if (count($dacomponent_details)==0) {?>
                        <tr> 
                          <td colspan="13" style="color: red;  text-center">No Record Found !!!</td>
                        </tr>
                    <?php  }else{
                   $i=0; foreach ($dacomponent_details as $key => $value) {

                   
                    ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <!-- <td class="text-center"><?php echo $value->emp_code; ?></td> -->
                      <td class="text-center"><?php echo $value->officename;?></td>
                      <td class="text-center"><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname; ?></td>
                      <td class="text-center"><?php echo $value->gender;?></td>
                      <td class="text-center"><?php echo $value->emailid; ?></td>
                      <td class="text-center"><?php echo $value->phase_name; ?></td>
                      <td class="text-center"><?php echo $this->model->changedatedbformate($value->fromdate);?></td>
                      <td class="text-center"><?php echo $this->model->changedatedbformate($value->todate); ?></td>
                      <td class="text-center"><?php echo $value->fieldguide; ?></td>
                      <td class="text-center"><?php 
                      if ($value->status==1) {?>
                      <span class="badge badge-Pill badge-warning">Submitted</span>
                    <?php }else if($value->status==2){ ?>
                        <span class="badge badge-Pill badge-success">Accepted</span>
                      <?php  }else if($value->status==3){ ?>
                         <span class="badge badge-Pill badge-danger">Rejected</span>
                      <?php  } ?></td>
                      
                      <td class="text-center"><a href="<?php echo site_url().'datafiles/dashipcomponent/'.$value->encrypted_document_name;?>" download data-toggle="tooltip" title="DAship Components Document"><i class="fa fa-download" aria-hidden="true"></i></td>
                      <td class="text-center">

                      <?php if($value->status==1){ ?>
                        <a href="<?php echo site_url().'Tc_daship_component_comments/add/'.$value->id;?>" target="_blank" class="btn btn-info btn-xs m-t-2 waves-effect" data-toggle="tooltip" title="DAship Components Document">Comments</a>
                      <?php  } ?>
                      </td>
                    </tr>
                  <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php //} //} ?>
  </section>
<script>

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    /*$('#tablecampus').DataTable();  */
});

  
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>