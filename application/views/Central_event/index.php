<?php foreach ($role_permission as $row) { if ($row->Controller == "Central_event" && $row->Action == "index"){ ?>
    <br>
    <div class="container-fluid">
    
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Central Event List</h4>
      <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new central event? Click on me." href="<?php echo site_url()."Central_event/add";?>" class="btn btn-primary btn-sm">Add New Central Event</a>        
      </div>
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
            
        <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
         <table id="tabledaevent" class="table table-bordered table-striped dt-responsive wrapper">
                <thead>
                 <tr>
                  <th class="text-center" style="width: 50px;">S. No.</th>
                  <th>Event Name</th>
                  <th>From Date</th>
                  <th>To Date</th>
                  <th>Place</th>
                  <th>Sent To Supervisior</th>
                  <th style ="width:50px;" class="text-center">Action</th>
               </tr> 
             </thead>
             <tbody>

              <?php 
                $i=0;
              foreach ($centralevent_details as $key => $value) {
             ?>
              <tr>
                <td class="text-center"><?php echo $i+1;?></td>
               <td><?php echo $value->name;?></td>
                <td><?php echo $this->model->changedatedbformate($value->from_date);?></td>
                 <td><?php echo $this->model->changedatedbformate($value->to_date);?></td>
                  <td><?php echo $value->place;?></td>
                  <td><a data-toggle="tooltip" data-placement="bottom" title="Want to add new central event? Click on me." href="<?php echo site_url()."Central_event/send_to_tc/".$value->id;?>" class="btn btn-primary btn-sm">click here</a></td>

                <td><a href="<?php echo site_url()."Central_event/edit/".$value->id;?>" data-toggle="tooltip" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this central event." ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete." href="<?php echo site_url()."Central_event/delete/".$value->id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></a></td>
              </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
       </div>
    </div> 
    </div>
    </div>
    </div>   
<?php } } ?>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tabledaevent').DataTable(); 
  });
</script>  
<script>

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this Central Event?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
  
</script>
