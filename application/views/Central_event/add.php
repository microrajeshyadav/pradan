  <?php foreach ($role_permission as $row) { if ($row->Controller == "Central_event" && $row->Action == "add"){ ?>
    <br>
    <div class="container-fluid">
      <!-- Exportable Table -->

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice"> -->
                 <!-- div class="panel panel-default" >
                  <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Add Central Event</b></div> -->

                  <div class="panel-body">
                    <div class="panel thumbnail shadow-depth-2 listcontainer" >
                      <div class="panel-heading">
                        <div class="row">
                         <h4 class="col-md-8 panel-title pull-left">Add Central Event</h4>
                         <div class="col-md-4 text-right" style="color: red">
                          * Denotes Required Field 
                        </div>
                      </div>
                      <hr class="colorgraph"><br>
                    </div>
                    <!-- <div class="panel-body"> -->
                      
                      <div class="col-sm-12">
                        <form name="centralevent" id="centralevent" method="post" action="" enctype="multipart/form-data">
                          <div class="body">
                            <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                               <div class="form-group">
                                <div class="form-line">
                                  <label for="TeamName" class="field-wrapper required-field">Name <span style="color:red;">*</span></label>
                                  <?php 
                                  $options = array('' => 'Select Event');
                                  foreach($event_details as$key => $value) {
                                    $options[$value->id] = $value->name;
                                  }
                                  echo form_dropdown('central_event_name', $options,  set_value('central_event_name'), 'class="form-control" data-toggle="tooltip" title="Select Event Name !"  id="central_event_name" required="required"');
                                  ?>
                                </div>
                                <?php echo form_error("central_event_name");?>
                              </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                             <div class="form-group">
                              <div class="form-line">
                                <label for="TeamName" class="field-wrapper required-field">From Date<span style="color:red;">*</span></label>
                                <input type="text" name="central_event_from_date" id="central_event_from_date" value="" class="form-control datepicker" required="required">
                              </div>
                              <?php echo form_error("central_event_from_date");?>
                            </div>
                          </div>

                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="form-group">
                            <div class="form-line">
                              <label for="TeamName" class="field-wrapper required-field">To Date<span style="color:red;">*</span></label>
                              <input type="text" name="central_event_to_date" id="central_event_to_date" value="" class="form-control datepicker"required="required">
                            </div>
                            <?php echo form_error("central_event_to_date");?>
                          </div>
                        </div>

                      </div>
                      <?php //print_r($resouceperson_details); ?>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                         
                          <label for="TeamName" class="field-wrapper required-field">Resouce Person <span style="color:red;">*</span></label>
                          <select class="form-control" id="resouce_personer" name="resouce_person[]" multiple="multiple" style="width:100%" required="required">
                            <?php foreach ($resouceperson_details as $row) { ?>
                              <option value="<?php echo $row->id.','.$row->name;?>" ><?php echo $row->name;?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                          <div class="form-group">
                            <div class="form-line">
                              <label for="TeamName" class="field-wrapper required-field">Place <span style="color:red;">*</span></label> 
                              <textarea name="central_event_place" maxlength="50" minlength="2" id="central_event_place" class="form-control" required="required"> </textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-footer text-right">
                       <button type="submit" class="btn btn-primary  btn-sm m-t-10 waves-effect" name="btnsevedata" id="btnsevedata" value="savedata"data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>

                       <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect" name="btnsendsevedata" id="btnsendsevedata"  value="sendsavedata" data-toggle="tooltip" title="Click here to Send Invitation."> Send Invitation</button>
                       <a href="<?php echo site_url("Central_event");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
                     </div>
                   </form>
                 </div> 
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <!-- #END# Exportable Table -->
 </div>
<?php } } ?>

<script>

  function confirm_delete() {

    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
      return true;
    } else {
      return false;
    }

  }



  $(document).ready(function() {

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#central_event_to_date").datepicker( "option", "minDate", selectedDate );
      
    }
    
  });

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#central_event_from_date").datepicker( "option", "maxDate", selectedDate );
    }
  });




  });
</script>