<br/>
<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Edashboard" && $row->Action == "Offercomments"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">Verify Offer Letter</h4>

       </div>
       <hr class="colorgraph"><br>
     </div>

     <?php 
     $tr_msg= $this->session->flashdata('tr_msg');
     $er_msg= $this->session->flashdata('er_msg');
     if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="body">
              <div class="panel panel-default" >


               <form name="campus" action="" method="post" > 
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 Candidate  :  <?php if(!empty($candidate->candidatefirstname)) {echo $candidate->candidatefirstname;} if(!empty($candidate->candidatemiddlename)) {echo " ". $candidate->candidatemiddlename;} if(!empty($candidate->candidatelastname)) {echo " ". $candidate->candidatelastname;}   ?>


               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <label for="Name">Offer Letter </label> 

                 &nbsp; &nbsp; &nbsp; &nbsp; <a href="<?php echo site_url().'pdf_offerletters/'.$getpdf->filename.'.pdf';?>" target="_blank" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Download Appointment letter" style="margin-top:20px;">
                  <i class="fa fa-download" aria-hidden="true"></i></a>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label for="Personnel">Personnel <span style="color: red;" >*</span></label>
                  <select name="personnel" id="personnel" class="form-control" required="required" >   
                    <option value="" >Select</option>
                    <?php if($personnellist){
                      foreach($personnellist as $res){
                        ?>
                        <option value="<?php echo $res->staffid; ?>"><?php echo $res->UserFirstName.' '.$res->UserLastName; ?></option>
                        <?php
                      }
                    } ?>
                  </select>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label for="Name">Status <span style="color: red;" >*</span></label>
                  <select name="status" id="status" class="form-control" required="required" >   
                    <option value="" >Select</option>
                    <option value="0">Approve</option>
                    <option value="1">Reject</option>
                  </select>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label for="Name">Comments  <span style="color: red;" ></span></label>
                  <textarea name="comments" id="comments" maxlength="255" minlength="5" class="form-control" cols="10" rows="5"> <?php echo set_value("comments");?></textarea>
                  <?php echo form_error("comments");?>
                </div>
                <div class="panel-footer text-right" >
                  <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me." style="margin-top: 20px;">Verify</button>
                  <a href="<?php echo site_url("EDashboard");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list." style="margin-top: 20px;">Go To List</a> 
                </div>

              </form> 
            </div>
          </div><!-- /.panel-->
        </div>

      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
<?php } } ?>
</section>

<script type="text/javascript">
 $(document).ready(function(){

  $('[data-toggle="tooltip"]').tooltip();
});
</script>