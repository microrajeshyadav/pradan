<section class="content" style="background-color: #FFFFFF;">
  <br>
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Edashboard" && $row->Action == "index"){ ?>
 <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">Verify Offer Letter</h4>
       
    </div>
    <hr class="colorgraph"><br>
  </div>
     
     <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">

      <div class="row">
        <div class="col-md-12">

          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


         <div class="row" style="background-color: #f8f9fa; color:black;">
          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="row" style="background-color: white;">
              <div class="col-lg-12">
      <form name="offerlettersearch" id="offerlettersearch" method="post" action="" >
       
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right">   
         <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 " style="background-color: white;">  
         <select name="campusid" id="campusid" class="form-control", required="required">
          <option value="">Select Campus</option>
          <?php foreach ($campusalldetails as $key => $value) {
              $expdatefrom = explode('-', $value->fromdate);
              $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
              $expdateto = explode('-', $value->todate);
              $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
               $campval  = $value->campusid.'-'. $value->id; 
              if ($campval == $campusid) {
           ?>
            <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
          <?php }else{ ?>
             <option value="<?php echo $value->campusid.'-'. $value->id;?>" ><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
         <?php } } ?>
        </select> 
      <?php echo form_error("campusid");?>
       </div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-left" style="background-color: white;"><button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-round btn-dark btn-sm">Search</button></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;"></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;"></div>
      
    </form>

              	<form name="sendtohrd" id="sendtohrd" action="" method="POST">

                <!--    <h4 class="col-md-12 panel-title pull-left">Check Offer Letter</h4>
 -->
              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <table id="Inboxtable" class="table table-bordered table-striped table-hover dataTable js-exportable">
          <thead>
           <tr>
            <th style ="max-width:50px;">S. No.</th>
            <th>Category</th>
            <th>Campus Name</th>
            <th>Name</th>
            <th>Gender</th>
            <th> Email Id</th>
            <th> Team </th>
            <th> FG </th>
            <th> Batch </th>
            <th style ="max-width:70px;" class="text-center"> Status </th>
            <th> Offer Letter </th>
          </tr> 
        </thead>
        <tbody>
          <?php 

           
          $i=0; foreach ($selectedcandidatedetails as $key => $value) {
            ?>
            <tr id="rowid_<?php echo $i;?>">
              <td class="text-center"><?php echo $i+1; ?></td>
               <td><?php echo $value->categoryname;?> </td>
               <td><?php echo $value->campusname;?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->gender;?></td>
              <td><?php echo $value->emailid;?> </td>
              <td><?php echo $value->officename;?></td>
              <td><?php echo $value->name;?></td>
              <td><?php echo $value->batch;?></td>
              <td class="text-center"><?php 
                if (isset($value->status) &&  $value->status==0) { ?>
                  <span class="badge badge-pill badge-success">Approved</span>
                 <!--  <a class="btn btn-success btn-sm" href="<?php echo site_url()."Assigntc/edit/".$value->candidateid;?>"  data-toggle="tooltip" data-placement="bottom" title ="Done" ><i class="fas fa-user-tag"></i></a> -->
                
              <?php  }else if(isset($value->status) && $value->status ==1){?>
                    <span class="badge badge-pill badge-danger">Rejected</span>
              <?php   } ?></td>
               <td class="text-center">
                <?php if ($value->flag== 2) {?>
                  <span class="badge badge-pill badge-success">Sent </span>
                 <!--  <a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" title ="Sent" ><i class="fas fa-user-tag"></i></a> -->
                <?php  }else{
                  if ($value->status ==1 ) { ?>
                  <label class="badge badge-pill badge-danger">Rejected</label> 
                 <!--  <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Rejected"><i class="fas fa-user-times"></i>  </a> -->
               <?php   }else if($value->id==1){ 

                ?>

                <!-- <a href="<?php //echo site_url("Edashboard/Offercomments/").'/'.$value->candidateid;?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" id="send" name="send" title="Send to HRD">Send to HRD</a> -->

                <a class="btn btn-info btn-sm" href="<?php echo site_url("Edashboard/Offercomments/").'/'.$value->candidateid."/16";?>" data-toggle="tooltip" id="send" name="send" data-placement="bottom" title="Send to HR"> <i class="fas fa-file-export"> </i></a>
                  <?php }

                    else if($value->id==2 || $value->id==3)
                    {
                      ?>
                        <!-- <a href="<?php //echo site_url("Edashboard/Offercomments/").'/'.$value->candidateid;?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" id="send" name="send" title="Send to personnel">Send to personnel</a> -->


                        <a class="btn btn-info btn-sm" href="<?php echo site_url("Edashboard/Offercomments/").'/'.$value->candidateid."/17";?>" data-toggle="tooltip" id="send" name="send" data-placement="bottom" title="Send to Personnel">
                          
                          <i class="fas fa-file-export"> </i>
                        </a>
                    <?php }
                    } ?>
              </td>
            </tr>
            <?php $i++; } ?>

             <?php  if($appointmentApprovallist){ foreach ($appointmentApprovallist as $key => $value) {
            ?>
            <tr id="rowid_<?php echo $i;?>">
              <td class="text-center"><?php echo $i+1; ?></td>
               <td><?php echo $value->categoryname;?> </td>
               <td><?php echo $value->campusname;?></td>
              <td><?php echo $value->name;?> </td>
              <td><?php echo $value->gender;?></td>
              <td><?php echo $value->emailid;?> </td>
              <td><?php echo $value->officename;?></td>
              <td><?php echo $value->fieldguide_name;?></td>
              <td><?php echo $value->batchid;?></td>
              <td class="text-center"><?php 
                if (isset($value->edstatus) &&  $value->edstatus==0) { ?>
                  <span class="badge badge-pill badge-success">Approved</span>
                
              <?php  }else if(isset($value->edstatus) && $value->edstatus ==1){?>
                    <span class="badge badge-pill badge-danger">Rejected</span>
              <?php   } ?></td>
              <td class="text-center">
                <?php if (isset($value->sendflag) && $value->sendflag== 2) { ?>
                  <span class="badge badge-pill badge-success">Sent </span>

                <?php  }else{
                  if (isset($value->edstatus) && $value->edstatus == 1 ) { ?>
                  <label class="badge badge-pill badge-danger">Rejected</label> 
               <?php   }else if($value->edstatus == NULL && $value->sendflag == 1){ ?>

                <a href="<?php echo site_url("Edashboard/Appointmentcomments/").'/'.$value->transid;?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" id="send" name="send" title="Send to HRD">Send to Personnel</a>
                  <?php }  } ?>
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
              </div>
         </form>
        </div>
      </div>
    </div>
  </div>
 </div>  
 <div class="panel thumbnail shadow-depth-2 listcontainer" style="display:none;" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">Verify Appointment Letter</h4>
       
    </div>
    <hr class="colorgraph"><br>
  </div>
   


         <div class="row" style="background-color: #f8f9fa; color:black;">
          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="row" style="background-color: white;">
              <div class="col-lg-12">
                <form name="sendtohrd" id="sendtohrd" action="" method="POST">

                <!--    <h4 class="col-md-12 panel-title pull-left">Check Offer Letter</h4>
 -->
              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <table id="InboxtableAppointment" class="table table-bordered table-striped table-hover dataTable js-exportable">
          <thead>
           <tr>
            <th style ="max-width:50px;">S. No.</th>
            <th>Category</th>
            <th>Name</th>
            <th>Gender</th>
            <th> Email Id</th>
            <th> Team </th>
            <th style ="max-width:70px;" class="text-center"> Status </th>
            <th> Offer Letter </th>
          </tr> 
        </thead>
        <tbody>
         
          </tbody>
        </table>
              </div>
         </form>
        </div>
      </div>
    </div>
  </div>
 </div>  

<?php } }?>
</section>

<script type="text/javascript">
 $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();  
    $('#Inboxtable').DataTable(); 
    $('#InboxtableAppointment').DataTable(); 

  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
