<section class="content">
	
		<br>
		<div class="container-fluid">
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');

			if(!empty($tr_msg)){ ?>
				<div class="content animate-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						</div>
					</div>
				<?php } else if(!empty($er_msg)){?>
					<div class="content animate-panel">
						<div class="row">
							<div class="col-md-12">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>


					<?php  //print_r($state_details); ?>
					<!-- Exportable Table -->
					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="panel thumbnail shadow-depth-2 listcontainer" >
								<div class="panel-heading">
									<div class="row">
										<h4 class="col-md-10 panel-title pull-left"> Mid-term  Review  </h4>

									</div>
									<hr class="colorgraph"><br>
								</div>
								<div class="panel-body">
									<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
										<thead>
											<tr>
												<th class="text-center" style="width: 45px;">#</th>
												<th>Employ Code</th>
												<th>Name</th>
												<th>Designation </th>
												<th>Level </th> 
												<th>Gender</th>
												<th>Office </th>
												<th>Probation Date</th>
												<th>Status </th>
												<th>Initiate</th>
											</tr>
										</thead>
										<tbody>
											<?php
											// echo "<pre>";
											// print_r($hrd_staff_details);

											$i=0; foreach($hrd_staff_details as $row){ 
												?>
												<tr>
													<td class="text-center"><?php echo $i+1; ?></td>
													<td ><?php echo $row->emp_code; ?></td>
													<td ><?php echo $row->name; ?></td>
													<td ><?php echo $row->desname; ?></td>
													<td ><?php echo $row->levelname; ?></td>
													<td ><?php echo $row->gender; ?></td>
													<td ><?php echo $row->officename; ?></td>
													<td ><?php echo $this->gmodel->changedatedbformate($row->probation_date); ?></td>
													<td class="text-center"><?php echo $row->status; ?></td>
												<td style="text-align: center;">
													<?php 
										 if ($row->levelname =='level-4') { ?>
											<button type="submit" name="initiatedata" id="initiatedata" value="initiatesenddata" class="btn btn-primary xs-12">Mid Term Review </button>
											<?php  }else{ ?>
			<a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Probation_ed_reviewofperformance/hrview/".$row->staffid.'/'.$row->rid;?>" >Send to Personnel</a>
											
											<?php } ?>
									</td>

											</tr>
											<?php $i++; } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			
		</section>
		<script>
			$(document).ready(function() {
				$('[data-toggle="tooltip"]').tooltip(); 
				$('#tbldesignations').DataTable({
					"paging": true,
					"search": true,
				});
			});
			function confirm_delete() {

				var r = confirm("Do you want to delete this Designations");

				if (r == true) {
					return true;
				} else {
					return false;
				}

			}
		</script>]