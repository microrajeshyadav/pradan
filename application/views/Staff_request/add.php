<section class="content">
  <?php //foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "add"){ ?>
   <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-sm-offset-4">
            <form method="POST" action="">
              <div class="panel panel-default" >
                <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Transfer Request by staff</b> 
                </div>
                <div class="panel-body">

                  <div class="form-group">
                    <div class="form-line">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Status<span style="color: red;" >*</span></label>
                        <select class="form-control" name="status" id="status" required="required">
                        <option value="">Select Status</option>
                        
                          <option value="4">Approve</option>
                          <option value="3">Rejected</option>
                         
                       
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="form-line">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Remarks <span style="color: red;" >*</span></label>

                      <textarea class="form-control" name="remarks" id="remarks" style="resize: none;" required="required"></textarea>
                      <?php echo form_error("remarks");?>
                    </div>
                  </div>
               </div>

               <div class="panel-footer text-right"> 
                <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Submit">Submit</button>
                <a href="<?php echo site_url("Staff_request");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Go to List</a> 
              </div>

            </div>
          </form>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
    <?php// } } ?>
  </section>
  <script type="text/javascript">
     $(document).ready(function(){

   $("#date_request").datepicker({
        changeMonth: true,
          changeYear: true,
          minDate : 0,
          yearRange: '1980:2025',
          dateFormat : 'dd/mm/yy',
          //defaultDate: new Date(2018, 00, 01)
      });
 });
  </script>