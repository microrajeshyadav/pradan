

<section class="content">
	<?php foreach ($role_permission as $row) { if ($row->Controller == "Staff_request" && $row->Action == "index"){ ?>
		<br>
		<div class="container-fluid">


			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');

			if(!empty($tr_msg)){ ?>
				<div class="content animate-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						</div>
					</div>
				<?php } else if(!empty($er_msg)){?>
					<div class="content animate-panel">
						<div class="row">
							<div class="col-md-12">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>


					<?php // print_r($staff_details);?>
					<!-- Exportable Table -->
					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default" >
								 <h4 class="col-md-12 panel-title pull-left"><b>Manage Staff Request <h4>
								 	<hr class="colorgraph">
									

								</div>

								<div class="panel-body">

									<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
										<thead>
											<tr>
												<th  class="text-center" style="width: 50px;">S. No.</th>
												<th >Employ Code</th>
												<th >Name</th>
												<th >Office </th>
												<th >Status </th>
												<th class="text-center" style="width: 100px;">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i=0; foreach($staff_details as $row){ 
												?>
												<tr>
													<td class="text-center"><?php echo $i+1; ?></td>
													<td ><?php echo $row->emp_code; ?></td>
													<td ><?php echo $row->staffname; ?></td>
													<td ><?php echo $row->officename; ?></td>
													<td ><?php echo $row->status; ?></td>
													<td class="text-center">
													<a href="<?php echo site_url().'Staff_request/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to view this Staff Profile."><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>	</td>
													</tr>
													<?php $i++; } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!-- #END# Exportable Table -->
						</div>
					<?php } } ?>
				</section>
				<script>
					$(document).ready(function() {
						$('[data-toggle="tooltip"]').tooltip(); 
						$('#tbldesignations').DataTable({
							"paging": true,
							"search": true,
						});
					});
					function confirm_delete() {

						var r = confirm("Do you want to delete this Designations");

						if (r == true) {
							return true;
						} else {
							return false;
						}

					}
				</script>