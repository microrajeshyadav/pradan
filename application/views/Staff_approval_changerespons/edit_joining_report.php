<?php echo "pooja";?>
<div class="container">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <?php print_r($getstaffdetailslist); ?>
          
            <div class="row" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <div class="card" >
                  <div class="body" style="margin-left: 10px;">
                    <div class="row">
                     <h4 class="header" class="field-wrapper required-field" style="text-align: center;"> <label class="field-wrapper required-field">Professional Assistance for Development Action (PRADAN)  <br><br>JOINING REPORT AT NEW PLACE OF POSTING</label></h4>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
                     <form name="joiningreport" id="joiningreport" method="POST" action="">
                      <input type="hidden" name="token" value="<?php echo $token; ?>">
                      
                   <?php //print_r($candidatedetils); echo $candidatedetils->doj; ?>
                    <div class="row">
                     <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label class="field-wrapper required-field">
                       To, <br>
                       The Executive Director  <br>              
                       PRADAN <br>
                       E 1/A, Ground Floor and Basement <br>
                       Kailash Colony <br>
                       New Delhi - 110 048 <br>
                       Tel: 4040 7700 <br>
                     </label>
                   </div>
                   <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" ></div>
                 </div>

                 <div class="row">

                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                   <div class="form-group">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Subject: Joining Report   </label>
                  </div>
                </div>

                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="field-wrapper required-field">Sir, </label>
                </div>
              </div>
              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <label class="field-wrapper required-field">  In response to the Letter of Transfer No. 
                <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $getstaffdetailslist->transferno; ?>"> &nbsp&nbsp  dated &nbsp&nbsp<input type="text" name="offerdate" id="offerdate"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $this->gmodel->changedatedbformate($getstaffdetailslist->letter_date); ?>"  >I hereby report for duty today, forenoon of  <input type="text" maxlength="50" 
                name=" staff_duty_today_date" id="staff_duty_today_date" placeholder=" Please Enter Date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" value="<?php echo date('d/m/Y'); ?>" >(date).<br><br><br>

            </div>

               
        
        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
       <p> Thanking you<p>

<p >Yours sincerely,</p>
</div>
<div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
<table style="border-collapse: collapse;">
<tbody>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="508">
<p style="line-height: 150%; tab-stops: -67.5pt;">Signature: <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value=""></p>
</td>
<td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="text-align: right; line-height: 150%; tab-stops: -67.5pt;">Employee <span style="line-height: 150%;">Code</span>:<input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffdetailslist->emp_code; ?>"></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Name&nbsp;&nbsp;&nbsp;&nbsp; :
 <input type="text" name="offerno" id="offerno" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $getstaffdetailslist->name; ?>"></p>
</td>
<td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="margin-left: 74.9pt; line-height: 150%; tab-stops: -67.5pt;">Place :<input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value=""></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Designation: <input type="text" name="designation" id="designation"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $getstaffdetailslist->desname; ?>"></p>
</td>
<td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="text-align: right; line-height: 150%; tab-stops: -67.5pt;">Date&nbsp; : <input type="text" name="currentdate" id="currentdate" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo date('d/m/Y'); ?>"></p>
</td>
</tr>
</tbody>
</table>
</div>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p style="tab-stops: -1.5in -49.5pt;">--------------------------------------------------------------------------------------------------------------</p>
<p style="text-align: center; tab-stops: -1.5in -49.5pt;">(<strong>For Use of the Integrator/Team Coordinator </strong>)</p>

<p style="text-align: justify; tab-stops: -1.5in -49.5pt;">&nbsp;</p>

<p style="tab-stops: -1.5in -49.5pt;">
  Reported for duty on <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $candidatedetils->offerno; ?>"><span style="font-size: 10.0pt;">(date)</span> forenoon.</p>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<p style="text-align: justify; tab-stops: -1.5in -49.5pt;">S/he has been assigned to<input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $candidatedetils->offerno; ?>">project/programme at <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $candidatedetils->offerno; ?>"> location and will work under the supervision of _____________________&nbsp; (<span style="font-size: 10.0pt;">Name and Designation</span>).</p>
<p style="text-align: justify; tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<table style="border-collapse: collapse;">
<tbody>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Signature: <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $candidatedetils->offerno; ?>"></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :<input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $candidatedetils->offerno; ?>"></p>
</td>
</tr>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
<p style="line-height: 150%; tab-stops: -67.5pt;">Date: <input type="text" name="offerno" id="offerno"
                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                value="<?php echo $candidatedetils->offerno; ?>"></p>
</td>
</tr>
</tbody>
</table>



</div>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
<p style="margin-left: .25in; text-indent: -.25in; tab-stops: -1.5in -49.5pt;">cc: - Employee on Transfer</p>
<p style="margin-left: .25in; tab-stops: -1.5in -49.5pt;">- Person under whose guidance s/he will work</p>
<p style="margin-left: .25in; tab-stops: -1.5in -49.5pt;">- Finance-Personnel-MIS Unit (for Personal Dossier after necessary action)</p>


             <div style="text-align: center;"> 
             
       
<button  type="submit" name="savebtn" id="savebtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button>        
<button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>
         

         
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">

  $(document).ready(function(){
    decimalData();
    // $("#submitbtn").prop("disabled", "disabled");
    $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

   //$('#pradan_selection_process_before_details').prop("disabled",true); 

 });

 
  

  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }

 </script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    //$("#btnsubmit")..prop('disabled', true);

  });
</script>  

<script type="text/javascript">
// $(function(){

 //   $("#joiningreport").submit(function(){
  
 //     var form = $('#joiningreport')[0];
 //     var data = new FormData(form);
    
 //     $.ajax({
 //       type: "POST",
 //       enctype: 'multipart/form-data',
 //       url: "<?php echo base_url(); ?>candidate/Joining_report_form/save",
 //       data : data,
 //       async : false,
 //       cache : false,
 //       dataType : 'json',
 //       contentType : false,
 //       processData : false,
 //       success: function(data){
 //        alert(data);
 //         if (data==1) {
 //           location.reload();
 //          alert("Data saved successfully !!!");
 //        }else if(data==0){
 //          alert("Data not saved successfully !!!");
 //        }else if(data==2){
 //           location.reload();
 //          alert("Joining report Already Sunmitted !!!");
 //        }
 //      }

 //    });

 //         return false;  //stop the actual form post !important!

 //       });
 // });


 
  // var condition = "";
  // $("#sendDataButton").click(function() {
  //   condition = $("#sendDataButton").val();
   
  // })
  // $("#sendNextDataButton").click(function() {
  //   condition = $("#sendNextDataButton").val();
   
  // })
  
  // $("#joiningreport")
  //     .submit(
  //         function() {
  //           var form = $('#joiningreport')[0];
  //           var data = new FormData(form);
  //           if (condition == 'saveSubmit') {
  //             $
  //                 .ajax({
  //                   url : "<?php echo base_url(); ?>candidate/Joining_report_form/save",
  //                   type : 'POST',
  //                   enctype : 'multipart/form-data',
  //                   data : data,
  //                   dataType : 'json',
  //                   async : false,
  //                   cache : false,
  //                   contentType : false,
  //                   processData : false,
  //                   complete : function(data) {
  //                    alert(data);
  //                    if (data==1) {
  //                      location.reload();
  //                     alert("Data saved successfully !!!");
  //                   }else if(data==0){
  //                     alert("Data not saved successfully !!!");
  //                   }else if(data==2){
  //                      location.reload();
  //                     alert("Joining report Already Sunmitted !!!");
  //                   }
  //                     //if (data.responseJSON['status'] == "save") {
  //                     //   alert("Registration data saved successfully");
  //                     //   // window.location.replace("${pageContext.request.contextPath}/user/form09/form09Page");
  //                     // }
  //                   }
  //                 })

  //           } else if (condition == 'saveAndNextSubmit') {
  //             $
  //                 .ajax({
  //                   url : "<?php echo base_url(); ?>candidate/Joining_report_form/edit/",
  //                   type : 'POST',
  //                   enctype : 'multipart/form-data',
  //                   data : data,
  //                   dataType : 'json',
  //                   async : true,
  //                   cache : false,
  //                   contentType : false,
  //                   processData : false,
  //                   success : function(data) {
  //                    alert(data);
  //                     // var str = data['status'].split("?")
  //                     // if (str[0] == "edit") {
  //                     //   // window.location
  //                     //   //     .replace("${pageContext.request.contextPath}/user/firmRegistration/firmRegistrationApproval");
  //                     // }
  //                     // if (data['status'] == "submit") {
  //                     //   // window.location
  //                     //   //     .replace("${pageContext.request.contextPath}/user/odsDashBoard/odsDashBoardPage");
  //                     // }
  //                   }
  //                 })

  //           }
  //           return false;
  //         })
</script>
