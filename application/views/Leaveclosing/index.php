<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Leaveclosing" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <form method="post" action="" name="applicationform">
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-8 panel-title pull-left">Create Opening Balance </h4>
           <div class="col-md-4 text-right">
           <!--  <a href ="<?php //echo site_url('Leaveclosing/index/')?>" class="btn btn-sm btn-dark" >Back</a> -->
           <button class="btn btn-sm btn-primary" value="Nextperiodopningbalance" name="nextob" id="nextob">Generate Opening Balance for Next Period</button>
           <!--  <a href ="<?php //echo site_url('Leaveclosing/NextperiodOpningBalance/')?>" class="btn btn-sm btn-primary" >Generate Opening Balance for Next Year</a> -->
          </div>
        </div>
        <hr class="colorgraph"><br>
      </div>
  <div class="panel-body" style="font-family: 'Oxygen'">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

<?php //print_r($creditperiod); ?>
    <div class="row" style="padding: 10px;">
   <div class="col-md-3">
   <h5> Financial Year:  <?php echo $fyear; ?></h5>
 </div> 
  <div class="col-md-1">
   <label >Period :</label> 
 </div> 
   <div class="col-md-8">

  
   <select name="period" id="period" required class="form-control col-md-3">
     <option value="0">Select</option>
     <?php foreach ($creditperiod as $key => $value) { 
      if (date('Y-m-d') <= $value->monthtos && date('Y-m-d') >= $value->monthfroms) {
      ?>
        <option value="<?php echo $value->monthfroms.'to'.$value->monthtos ?>" Selected="Selected"><?php echo $this->gmodel->changedatedbformate($value->monthfroms).' to '. $this->gmodel->changedatedbformate($value->monthtos);?></option>
    <?php  } else{?>
      <option value="<?php echo $value->monthfroms.'to'.$value->monthtos ?>"><?php echo $this->gmodel->changedatedbformate($value->monthfroms).' to '. $this->gmodel->changedatedbformate($value->monthtos);?></option>
    <?php } } ?>
    
   </select>
 </div> 
    </div>
  <div class="row" style="background-color: #FFFFFF;">
    <!-- <input type="hidden" name="office_id" id="office_id" value="<?php echo $getreportingdetails->new_office_id;?>">
    
    <hr/> -->
    <?php  $leavelapsdate = date('Y').'-03-31';
           $cdate = date('Y-m-d'); ?>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card" style="min-height: 34px;">

          <div class="card-body bg-light">
            <table class="table" id="tableleave" class="table table-bordered table-striped table-hover dataTable js-exportable">
              <thead>
                <tr class="bg-info text-white">
                  <th class="text-center" style="vertical-align: top;">Sr.No</th>
                  <th class="text-left" style="vertical-align: top;">Staff Code</th>
                  <th class="text-left" style="vertical-align: top;">Staff Name</th>
                  <th class="text-left" style="vertical-align: top;">Designation</th>
                   <th class="text-left" style="vertical-align: top;">Office</th>
                  <th class="text-left" style="vertical-align: top;">Date of Team Joining</th>
                  <th class="text-left" style="vertical-align: top;">Total Experience in PRADAN</th>
                  <th class="text-left" style="vertical-align: top;">LWP</th>
                   <th class="text-left" style="vertical-align: top;">Actual Experience in PRADAN</th>
                  <th class="text-right" style="vertical-align: top;">OP Bal (Current Period)</th>
                  <th class="text-right" style="vertical-align: top;">Accured</th>
                  <th class="text-right" style="vertical-align: top;">DLWP</th>
                  <th class="text-right" style="vertical-align: top;">Availed</th>
                  <th class="text-right" style="vertical-align: top;">Leave Laps</th>
                  <th class="text-right" style="vertical-align: top;">Balance</th> 
                  <th class="text-right" style="vertical-align: top;">Opening Balance (Next Period)</th> 
               </tr>
              </thead>
              <tbody>
                <?php 
                $i=0;
            
                $cdate = date('Y-m-d');
                
                foreach ($leavebalance as $key => $value) {
                  $leavelapsdate = date('Y').'-03-31';
                  if ($leavelapsdate==$cdate) {
                   $leavelaps = $value->tavailed-18;
                  if ($leavelaps  < 0) {
                   $leavelapsval = 0;
                  }else{
                    $leavelapsval = $leavelaps;
                  }
                  }else{
                     $leavelapsval = 0;
                  }
                 

               ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?>. </td>
                  <td class="text-left"><?php echo $value->emp_code;?></td>
                  <td class="text-left"><?php echo $value->Name;?></td>
                  <td class="text-left"><?php echo $value->desname;?></td>
                  <td class="text-left"><?php echo $value->officename;?></td>
                  <td class="text-left"><?php echo $this->gmodel->changedatedbformate($value->doj);?></td>
                  <td class="text-right"><?php echo $value->noofyearexp;?></td>
                  <td class="text-right"><?php echo $value->LWP;?></td>

                   <td class="text-right"><?php echo $value->ndays;?></td>
                  <td class="text-right"><?php echo $value->OB;?></td>                  
                  <td class="text-right"><?php echo $value->Accured;?></td>
                  <td class="text-right"><?php echo $this->gmodel->roundvalue($value->DLWP);?></td>
                  <td class="text-right"><?php echo $value->Availed;?></td>
                  <td class="text-right"><?php echo $leavelapsval;?></td>
                  <td class="text-right"><input type="hidden" name="availbalance" id="availbalance" value="<?php echo $value->Balance;?>" ><?php echo $value->Balance;?></td> 
                   <td class="text-right"><input type="hidden" name="availbalance" id="availbalance" value="<?php echo $value->Balance;?>" ><?php echo $value->ClosingBalance;?></td> 

                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div>
   </div>
     </div> 
   </form>
 </div>
</div>
</div>   
<?php } } ?>
</section>
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#tableleave').DataTable({
      "paging": true,
      "search": true,
    });
  });
</script>
