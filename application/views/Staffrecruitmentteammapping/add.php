<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Staffrecruitmentteammapping" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php //print_r($selectrecruiters);?>
       <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form name="campus" action="" method="post" >  
             <div class="panel thumbnail shadow-depth-2 listcontainer" >
               <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-8 panel-title pull-left">Staff to Recruitment Team Mapping</h4>
                     <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>

               

                <div class="panel-body">
                 
                 <div class="row" style="margin-top: 20px;">
                   <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <label for="Name" style="vertical-align: top;">Staff Member </label>
                    <select name="staff" id="MasterSelectBox" multiple="multiple" class="form-control" style="height: 250px;" >

                      <?php 
                      foreach ($staffdetails as $key => $value) {?>
                        <option value="<?php  echo $value->staffid;?>"><?php  echo $value->emp_code."-".$value->name; ?></option>
                      <?php } ?>

                    </select>

                    <?php echo form_error("staff");?>
                  </div>
                  <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-center " style="margin-top: 40px;">
                    <div class="row vcenter"> 
                      <button  data-toggle="tooltip"  Title ="Click here to add selected staff for recruitment team member." class ="btn btn-success" type="button" id="btnAdd">></button>
                    </div>
                    <div class="row" style="margin-top: 20px; "> 
                      <button data-toggle="tooltip"  Title ="Click here to remove staff from recruitment team member." class ="btn btn-danger"  type="button" id="btnRemove"><</button>
                    </div>
                  </div>
                  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
                    <label for="Name">Recruitment Team Members<span style="color: red;" >*</span></label>
                    <select name="recruitmentteam[]" id="PairedSelectBox" multiple="multiple" class="form-control" style="height: 250px;" required="required" >

                      <?php 
                      foreach ($recuitementdetails as $key => $val) {?>
                        <option value="<?php  echo $value->staffid;?>"><?php  echo $val->emp_code."-".$val->name; ?></option>
                      <?php } ?>
                   </select>
                   <?php echo form_error("recruitmentteam");?>
                 </div>

               </div>


             </div>
             <div class="panel-footer text-right">
              <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
              <a href="<?php echo site_url("Staffrecruitmentteammapping");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
            </div>

          </div><!-- /.panel-->
        </form> 
      </div>
      <!-- #END# Exportable Table -->
    </div>
    <?php } } ?>
  </section>

  <style type="text/css">
  .vcenter {
    margin-top: 60%;
  }

</style>