<style type="text/css">
   #errmsg
{
color: red;
}
 </style>
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
      <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>List of eligible candidates</b></div>
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
          <?php //print_r($getcountstatus); ?>
        <div class="row" style="background-color: #FFFFFF;">
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;"></div>
   
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
      <form name="formwrittenscore" id="formwrittenscore" method="post" action="" >
        <input type="hidden" name="campusid" value="<?php echo $this->loginData->campusid;?>">

         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="writtenscoreselectecandidate" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable" style="width:100%">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Candidate</th>
              <th> Email Id</th>
              <th>HRD Status</th>
              <th>Status</th>
           </tr> 
         </thead>
         <tbody>
          <?php 
             $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
            <tr>
              <td><?php echo $i+1; ?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->emailid;?></td>
              <td><?php if (isset($value->hrstatus))  echo ucfirst($value->hrstatus);?></td>
           <td> 
            <select name="status[<?php echo $value->candidateid;?>]" id="status_<?php echo $i;?>" class="form-control">
              <option value="accepted" <?php if (isset($getstatuscandidate[$key]->status) && $getstatuscandidate[$key]->status =='accepted') {
               echo 'selected="selected"';
              }?>>Accepted</option>
             <option value="rejected" <?php if (isset($getstatuscandidate[$key]->status) && $getstatuscandidate[$key]->status =='rejected') {
               echo 'selected="selected"';
              }?>>Rejected</option>
            </select>
               </td>
             <?php $i++; } ?>
            </tr>
          </tbody>
        </table>
       </div>
       <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12" style="background-color: white;"></div> 
       <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 text-center" style="background-color: white;">
        <?php if (count($selectedcandidatedetails)>0) {  ?>
        <button type="submit" class="btn btn-success" name="btnaccept" id="btnaccept" value="AcceptData">Save</button> 
      <?php } ?>
        </div>
       </div> 
       <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12" style="background-color: white;"></div> 
      </div> 
     </form>
     </div>
    </div>
  </div>   
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('#writtenscoreselectecandidate').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel','print'
        ]
    } );
} );
</script>

<!--  JS Code Start Here  -->
<script type="text/javascript">

$(':checkbox.radiocheckbox').click(function() {
    this.checked && $(this).siblings('input[name="' + this.name + '"]:checked.' + this.className).prop('checked', false);
});

  
  //$(document).ready(function(){
    //$('[data-toggle="tooltip"]').tooltip();  
    //$('#tableWrittenScore').DataTable({
      // "bPaginate": true,
       //"bInfo": true,
       //"bFilter": true,
       //"bLengthChange": true
    //}); 
  //}); 


$("#SendEmail").on("click", function(){
    
   var campid =  $("#campusid").val();
     if(campid ==''){   ////Check campus id 

    alert('First !! Please Select campus !!!');

   }else{

 $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: "<?php echo base_url(); ?>Writtenscore/generate_pdf_selected_candidate/" + campid,
      
       async : false,
       cache : false,
       dataType : 'json',
       contentType : false,
       processData : false,
       success: function(data){
        console.log(data);
         if (data) {
          //location.reload();
          //alert("Data saved successfully");
         // $("#btnsubmit").prop('disabled', false);

        }else{
          alert("Data not saved successfully");
        }
      }

    });


   }

});

 

</script>

<!--  JS Code End Here  -->