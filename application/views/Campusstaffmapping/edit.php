<section class="content">
  <?php foreach ($role_permission as $row) { 
    if ($row->Controller == "Campusstaffmapping" && $row->Action == "edit"){ ?>   
    <div class="container-fluid">
      <!-- Exportable Table -->

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
  
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

         <div class="container-fluid" style="margin-top: 20px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
                <div class="body">
                  <?php //print_r($MappedCampusRecruitmentlist); ?>
                  <form name="campus" id="camstaffmapping" action="" method="post" > 
                    <div class="panel thumbnail shadow-depth-2 listcontainer" >
                      <div class="panel-heading">
                        <div class="row">
                         <h4 class="col-md-8 panel-title pull-left">Edit Allocation of recruiters to campus</h4>
                         <div class="col-md-4 text-right" style="color: red">
                          * Denotes Required Field 
                        </div>
                      </div>
                      <hr class="colorgraph"><br>
                    </div>
                    <div class="panel-body">

                      <div class="row">
                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                        <label for="Name" style="vertical-align: top;">Campus<span style="color: red;" >*</span></label>

                        <select name="campus" id="campus" class="form-control", required="required">
                          <option value="">Select Campus</option>
                          <?php foreach ($campuslist as $key => $value) {
                            $expdatefrom = explode('-', $value->fromdate);
                            $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                            $expdateto = explode('-', $value->todate);
                            $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                            $campval  = $value->campusid.'-'. $value->id; 
                            if ($campval==$campusid) {?>
                            <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                            <?php   } else{ ?>
                            <option value="<?php echo $value->campusid.'-'. $value->id;?>" ><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                            <?php } } ?>
                          </select> 
                          <?php echo form_error("campus");?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        </div>
                      </div>

                      <?php //print_r($MappedCampusRecruitmentlist);  ?>
                      <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
                       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                         <table id="tblForm15" class="table table-bordered table-striped">
                          <thead>
                           <tr>
                             <th colspan="2" style="color: #000; font-size: 15px;" >
                              <span style="text-align: left; padding-right: 375px;">Recruiters</span>
                              <span style="text-align: right;"><button type="button" id="btnGapReasonRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                               <button type="button" id="btnGapReasonAddRow" class="btn btn-warning btn-xs">Add</button></span>
                             </th>
                           </tr> 
                         </thead>
                         <tbody id="bodytblForm15" >
                          <input type="hidden" name="RCount" id="RCount" value="<?php echo $countrecruitment->RCount; ?>">
                          <?php if ($countrecruitment->RCount==0) { ?>
                          <tr id="bodytblForm15">
                           <td>
                            <select name="recruiter[]" id="recruiter" class="form-control"  >
                              <option value="">Select Recruiters</option>
                              <?php foreach ($recruitmentlist as $key => $value) { ?>
                              <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>
                              <?php } ?>
                            </select>
                          </td>
                          <td>
                            <div class="form-check" style="padding-top: 7px;">
                              <input type="checkbox" class="filled-in" name="anchor[]" value="1" id="defaultCheck_accept" >
                              <label class="form-check-label" for="defaultCheck_accept">
                               <b>Anchor</b>
                             </label>
                           </div>
                         </td>
                       </tr>
                       <?php }else{  $i=0;

           // print_r($MappedCampusRecruitmentlist);
                        foreach ($MappedCampusRecruitmentlist as $key => $val) { ?>

                        <tr id="bodytblForm15">
                         <td>
                          <select name="recruiter[<?php echo $i; ?>]" id="recruiter_<?php echo $i; ?>" class="form-control"  >
                            <option value="">Select Recruiters</option>
                            <?php foreach ($recruitmentlist as $key => $value) { 
                              if ($value->staffid == $val->recruiterid) {
                                ?>
                                <option value="<?php echo $value->staffid;?>" SELECTED><?php echo $value->name;?></option>
                                <?php }else{ ?>
                                <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>
                                <?php }  } ?>
                              </select>
                            </td>
                            <td>
                              <div class="form-check" style="padding-top: 7px;">
                                <input type="checkbox" class="filled-in" 
                                <?php if ($val->anchor==1) {
                                  echo 'checked="checked"';

                                } ?>
                                name="anchor[<?php echo $i; ?>]" value="1" onclick="checkancherhead(this.id)" id="defaultCheck_accept_<?php echo $i; ?>" onload="checkancherhead(this.id)">
                                <label class="form-check-label" for="defaultCheck_accept_<?php echo $i; ?>">
                                 <b>Anchor</b>
                               </label>
                             </div>
                           </td>
                         </tr>

                         <?php $i++; }  } ?>

                       </tbody>
                     </table>
                   </div>
                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>

                 </div>
               </div>
               <div class="panel-footer text-right">

                 <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
                 <a href="<?php echo site_url("Campusstaffmapping");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 

              
             </div>

           </div><!-- /.panel-->
         </form> 
       </div>

  

   </div>

 </div>
 <!-- #END# Exportable Table -->
</div>
<?php } } ?>
</section>

<script type="text/javascript">
  $(document).ready(function() {  

    function checkancherhead(id){

     var indno =  id.replace('defaultCheck_accept_','');
     var Anchorcheck = $('input[type="checkbox"]:checked');
     if(Anchorcheck.length == 1){
       $('#defaultCheck_accept_'+indno).removeClass('filled-in');
       $('.filled-in').prop('disabled', true);
     }else{ 
       $('#defaultCheck_accept_'+indno).addClass('filled-in');
       $('.filled-in').prop('disabled', false);
       $('.filled-in').prop('checked', false);
     }

   }
 });


  $('#camstaffmapping').submit(function(){
    if(!$('#camstaffmapping input[type="checkbox"]').is(':checked')){
      alert("Please choose one anchor !!!.");
      return false;
    }
  });

  $("#btnGapReasonRemoveRow").click(function() {
    if($('#tblForm15 tr').length-1>1)
      $('#bodytblForm15 tr:last').remove()
  });

  $('#btnGapReasonAddRow').click(function() {
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    InsGapReason(rowsEnter1);
    //addDatePicker();
  });

  var rcount = $('#RCount').val();

  if (rcount ==0) {
    var srNoGlobal=0;
    var inctg = 0;
  }else{
    var srNoGlobal= rcount;
    var inctg = rcount;
  }

  function InsGapReason(count) {
    srNoGlobal = $('#bodytblForm15 tr').length+1;
    var tbody = $('#bodytblForm15');
    var lastRow = $('#bodytblForm15 tr:last');
    var cloneRow = null;
 // alert(count);
 for (i = 1; i <= count; i++) {
  if (rcount ==0) {
   inctg++;
 }
 cloneRow = lastRow.clone();
 var tableData15 = '<tr>'
 + '<td>'
 +'<select name="recruiter['+inctg+']" id="recruiter_'+inctg+'" class="form-control" required="required" >'
 + ' <option value="">Select Recruiters</option>'
 <?php foreach ($recruitmentlist as $key => $value): ?>
 + ' <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?></option>'
 <?php endforeach ?>
 + '</select>'
 + '</td>'
 + '<td><div class="form-check" style="padding-top: 7px;">'
 +   '<input type="checkbox" class="filled-in" name="anchor['+inctg+']" onclick="checkancherhead(this.id)" value="1" id="defaultCheck_accept_'+inctg+'" >'
 +  '<label class="form-check-label" for="defaultCheck_accept_'+inctg+'">'
 +  '<b>Anchor</b>'
 + '</label>'
 + '</div>'
 + '</td>' 
 + '</tr>';
 $("#bodytblForm15").append(tableData15)

}

}



// function checkancherhead(id){

//  var indno =  id.replace('defaultCheck_accept_','');
//  var Anchorcheck = $('input[type="checkbox"]:checked');
//  if(Anchorcheck.length == 1){
//    $('#defaultCheck_accept_'+indno).removeClass('filled-in');
//    $('.filled-in').prop('disabled', true);
//  }else{ 
//    $('#defaultCheck_accept_'+indno).addClass('filled-in');
//    $('.filled-in').prop('disabled', false);
//    $('.filled-in').prop('checked', false);
//  }

// }
function checkancherhead(id){
$('.filled-in').on('change', function() {
    $('.filled-in').not(this).prop('checked', false);  
});
}
</script>