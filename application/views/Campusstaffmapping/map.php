<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
          
        <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Staff to recruitment team mapping</b></div>

        <div class="panel-body">
           <form name="campus" action="" method="post" > 
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name" style="vertical-align: top;">Staff Member <span style="color: red;" >*</span></label>
                <?php 
                  
                  $options = array('' => 'Select Staff Member');
                  foreach($statedetails as$key => $value) {
                      $options[$value->id] = $value->name;
                  }
                  echo form_multiselect('staffmember', $options, set_value('stateid'), 
                    'class="form-control"');
                  ?>

                  <?php echo form_error("stateid");?>
                </div>
                
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label for="Name">Recruitment team <span style="color: red;" >*</span></label>
                <?php 
                  
                  $options = array('' => 'Select Recruitment team');
                  foreach($statedetails as$key => $value) {
                      $options[$value->id] = $value->name;
                  }
                  echo form_multiselect('stateid', $options, set_value('stateid'), 'class="form-control"', 'style="height: 400px;"');
                  ?>

                  <?php echo form_error("stateid");?>
                </div>
                
              
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                         <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Save </button>
                          <a href="<?php echo site_url("Campus");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                      </div>  
                </div>
               </div>
                 </form> 
                </div>


              </div><!-- /.panel-->

            </div>

          </div>

        </div>

      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>