 <section class="content" style="background-color: #FFFFFF;" >
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Campusstaffmapping" && $row->Action == "index"){ ?>   
<br>
   <div class="container-fluid">

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-9 panel-title pull-left">Allocate recruiters to campus</h4>

         <div class="col-md-3 text-right">
        
           <a data-toggle="tooltip" data-placement="bottom" title="Click here to allocate recruiters to campus" href="<?php echo site_url()."Campusstaffmapping/add";?>" class="btn btn-sm btn-primary">Click here to allocate recruiters to campus </a>
        </div>
      </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            
            <div class="row" style="background-color: #FFFFFF;">
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">

              
              <br>
              <br>
            </div>
          </div>
          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped reponsive">
                <thead>
                 <tr>
                   <th  style ="width:50px !important;" class="text-center">S. No.</th>
                   <th>Campus</th>
                   <th>Recruiters</th>
                   <th style ="width:50px !important;" class="text-center">Action</th>
                 </tr> 
               </thead>
               <tbody>
                <?php $i=0; foreach ($recuterslist as $key => $value){?>
                
                 <?php if($value->anchor !=0) {?>
                 <tr>
                 <td class="text-center"><?php echo $i+1;?></td>
                 <td><?php echo $value->campusname;?></td>
                 <td><?php echo $value->name;?>

                   &nbsp;&nbsp;&nbsp; <span class = "label label-success">Campus Incharge</span>

                 </td>
                 <td class="text-center"><a title="Click here to edit this." href="<?php echo site_url('Campusstaffmapping/edit/'.$value->campusintimationid.'/'.$value->campusid);?>" style="padding : 4px;" title="Edit"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a title="Click here to delete this." href="<?php echo site_url('Campusstaffmapping/delete/'.$value->campusintimationid.'/'.$value->campusid);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" title="Delete"><i class="fa fa-trash" style="color:red"></i></a></td>
               </tr>
               <?php } else{  ?>
               <tr>
                 <td class="text-center"><?php echo $i+1;?></td>
                 <td><?php echo $value->campusname;?></td>
                 <td><?php echo $value->name;?>  </td>
                 <td class="text-center"></td>
               </tr>
               <?php  } $i++; }  ?>

             </tbody>
           </table>
         </div>
       </div> 
     </div>
   </div>
 </div>   
 <?php } } ?>
</section>

  


<script type="text/javascript">
 $(document).ready(function(){
  $('#tablecampus').DataTable(); 
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
 
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete ?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>