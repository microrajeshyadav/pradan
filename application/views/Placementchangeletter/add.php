<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
     <?php // print_r($getstaffid); ?>
            <div class="panel-body">             
              <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <form name="formteam" id="formteamid" method="POST" action="">
                   <div class="container-fluid" style="margin-top: 20px;">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-sm-offset-6">
                      <div class="panel thumbnail shadow-depth-2 listcontainer" >
                        <div class="panel panel-default" >
                         <div class="panel-heading">
                          <div class="row">
                           <h4 class="col-md-7 panel-title pull-left">Placement Change letter</h4>
                           <div class="col-md-5 text-right" style="color: red">
                            * Denotes Required Field 
                          </div>
                        </div>
                        <hr class="colorgraph"><br>
                      </div>

                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group" >
                          <div class="form-line">
                            <label for="TeamName" class="field-wrapper required-field">Choose Team <span style="color:red;">*</span></label>
                            <select name="team"  id="team" class="form-control">
                              <?php foreach($teamsdetails as $row) {
                                if($row->officeid == $getstaffid->new_office_id){
                                  ?>
                                  <option value="<?php echo $row->officeid; ?>" Selected><?php echo $row->officename; ?> </option>
                                <?php }else {?>
                                  <option value="<?php echo $row->officeid; ?>" ><?php echo $row->officename; ?> </option>
                                <?php } } ?>
                              </select>
                            </div>
                          </div>
                        </div>

                          <?php  //echo $getstaffid->fgid; ?>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <div class="form-line">
                              <label for="StateNameEnglish" class="field-wrapper required-field">FG <span style="color:red;">*</span></label>
                              <select class="form-control" Name="fieldguide" id="fieldguide">
                               <option value="0">---All ---</option>
                                <?php foreach($teameFGdetail as $row) {
                                if($row->staffid == $getstaffid->fgid){
                                  ?>
                                  <option value="<?php echo $row->staffid; ?>" Selected><?php echo $row->name; ?> </option>
                                <?php }else {?>
                                  <option value="<?php echo $row->staffid; ?>" ><?php echo $row->name; ?> </option>
                                <?php } } ?>
                             </select>
                           </div>
                           <?php echo form_error("fieldguide");?>
                         </div>
                       </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <div class="form-line">
                              <label for="StateNameEnglish" class="field-wrapper required-field">Reportingto <span style="color:red;">*</span></label>
                              <select class="form-control" Name="reportingto" id="reportingto">
                               <option value="0">---Select Reportingto ---</option>
                                 <?php foreach($getreportingto as $row) { ?>
                                  <option value="<?php echo $row->staffid; ?>" Selected><?php echo $row->name; ?> </option>
                                <?php } ?>

                             </select>
                           </div>
                           <?php echo form_error("fieldguide");?>
                         </div>
                       </div>


                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <div class="form-line">
                              <label for="StateNameEnglish" class="field-wrapper required-field">Expected join Date<span style="color:red;">*</span></label>
                            <input type="text" name="effective_date" id="effective_date" class="form-control datepicker" value="" required="required">
                           </div>
                           <?php echo form_error("effective_date");?>
                         </div>
                       </div>

                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right"> 
                        <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                        <a href="<?php echo site_url("Assigntc");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
                      </div>
                    </form>
                  </div>
                </div>
             </div>
            </div>
          </div>
          </div>
          </div>
        </section>


        <script type="text/javascript">
          $(document).ready(function() {

      $("#effective_date").datepicker({
        changeMonth: true,
        changeYear: true,
        minDate: 'today',
        dateFormat : 'dd/mm/yy',
        yearRange: '1920:2030',
      });

            
            $.ajax({
              url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+ $("#team").val(),
              type: 'POST',
              dataType: 'text',
            })
            .done(function(data) {
              console.log(data);

             $("#fieldguide").html(data);


            })

            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });



              $.ajax({
              url: '<?php echo site_url(); ?>ajax/getReportingto/'+$("#team").val(),
              type: 'POST',
              dataType: 'text',
            })
            .done(function(data) {

              console.log(data);
              $("#reportingto").html(data);

            })

             .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });

         
           });

        
           $("#team").change(function(){

              $.ajax({
              url: '<?php echo site_url(); ?>ajax/getReportingto/'+$(this).val(),
              type: 'POST',
              dataType: 'text',
            })
            .done(function(data) {

              console.log(data);
              $("#reportingto").html(data);

            })

             .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });



            $.ajax({
              url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
              type: 'POST',
              dataType: 'text',
            })
            .done(function(data) {

              console.log(data);
              $("#fieldguide").html(data);

            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });

          });


        

       </script>
