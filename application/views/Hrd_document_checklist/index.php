<br/>
   <div class="container-fluid">
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Candidates document verification status</h4>
      <!-- <div class="col-md-2 text-right">
        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new batch? Click on me." href="<?php //echo site_url()."Batch/add";?>" class="btn btn-primary btn-sm">Add New Batch</a>
      </div> -->
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body"> 
        <!-- <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Manage offered candidate verification document list</b></div>
        <div class="panel-body"> -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
    ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <br>
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php  ?>
        <div class="container-fluid">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="tablecampus" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style ="max-width:30px;">S.No.</th>
                      <th class="text-center">Category</th>
                      <th>Campus Name</th>
                      <th class="text-center">Name</th>
                      <th>Gender</th>
                      <th class="text-center">Email id</th>
                      <?php if ($this->loginData->RoleID!=17) { ?>
                      <th class="text-center">Batch</th>
                     <?php  } ?>
                     
                      <th class="text-center">Photo</th>
                      <th  style ="max-width:70px;" class="text-center">Status</th>
                     <th  style ="max-width:30px;" class="text-center">Action</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (count($candidatebdfformsubmit)==0) { ?>
                      <tr>
                      <td colspan="8" style="color:red; text-align: center;">No Record Found !!!</td>
                    </tr>
                 <?php    }else{ ?>
                    
                    <?php 
                    // echo "<pre>";
                    // print_r($candidatebdfformsubmit);
                    // die;

                    $i=0; foreach($candidatebdfformsubmit as $key => $val){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $val->categoryname;?></td>
                      <td><?php echo $val->campusname;?></td>
                      <td class="text-center"><?php echo $val->candidatefirstname.' '.$val->candidatemiddlename.' '.$val->candidatelastname; ?></td>
                      <td><?php echo $val->gender;?></td>
                       <td class="text-center"><?php echo $val->emailid;?></td>
                        <?php if ($this->loginData->RoleID !=17) { ?>
                      <td class="text-center"><?php echo $val->batch;?></td>
                    <?php } ?>
                       <td class="text-center" style="border:solid 2px #e1e1e1; width:105px; height:133px; padding: 3px;">
                         <?php 
                            if($val->encryptedphotoname)
                            {
                            $file_path=''; 
                            $file_path=FCPATH.'datafiles/'. $val->encryptedphotoname;
                            
                            if(file_exists($file_path))
                            {
                          ?>
                     
                         <img src="<?php echo site_url().'datafiles/'. $val->encryptedphotoname;?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                         
                         <?php } 
                          else
                        {
                          ?>
                           <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                           <?php 
                        }
                       }
                       else
                       {
                        ?>
                           <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                           <?php 

                       }



                          ?>
                       </td>
                       <?php if ($val->status==0) { ?>
                       <td class="text-center"> <span class="badge badge-pill badge-danger">Not Approved</span></td>
                     <?php }else{ ?>
                       <td class="text-center">
                        <span class="badge badge-pill badge-success" >
                       Approved</span>
                      </td>
                     <?php } ?>
                      
                      <td class="text-center">
                         <?php if ($val->status==0) { ?>
                        <a data-toggle="tooltip" data-placement="bottom" title = "Click here to verify." href="<?php echo site_url('Hrd_document_checklist/verification/'.$val->candidateid);?>" style="padding : 4px;" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>
                      <?php }else{ ?>

                         <a href="<?php echo site_url('Hrd_document_checklist/view/'.$val->candidateid);?>" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to view ."><i class="fa fa-eye" aria-hidden="true" id="usedbatchid"></i></a>
                      <?php } ?>
                   
                      </td>
                    </tr>
                    <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </div>
<script type="text/javascript">
 $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable();
            $("#candidateid").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
                    type: 'POST',
                    dataType: 'json',
                })
                .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                   $("#batchid").val(data.batch);
                   $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });
      
        });     
  
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>