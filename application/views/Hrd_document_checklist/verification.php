<style type="text/css">
.checkboxStatus{
  color:red;
}
.checkboxStatussuccess{
  color:green;
}
</style>
<section class="content"><br>
  <div class="container-fluid">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ 
      ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php //echo $getworkexpcount; print_r($getcandateverified); ?>
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-8 panel-title pull-left">Offered candidate document verification</h4>
                   <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
                <form name="dadocumentverification" id="dadocumentverification" action="" method="post" enctype="multipart/form-data" >
                  <div class="row">
                    <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
                    <div class="col-lg-4">
                      <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
                      <input type="text" class="form-control" name="candidatename" id="candidatename" value="<?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>" readonly="readonly" >
                    </div>

                    <?php if (!empty($getcandateverified->batch)) { ?>

                    <?php    if ($this->loginData->RoleID !=17) { ?>
                    <div class="col-lg-1 text-right" ><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                    <div class="col-lg-2"><input type="text" name="batch" id="batchid" value="<?php echo $getcandateverified->batch; ?>" class="form-control"  readonly="readonly"></div>
                    <?php }else{ ?>
                    <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field"></label></div>
                    <div class="col-lg-2"></div>
                    <?php } } ?>

                    <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                    <div class="col-lg-1" id="candidateprofilepic" style="padding: 5px;" >

                      <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($getcandateverified->encryptedphotoname)) {
                           $image_path='datafiles/'.$getcandateverified->encryptedphotoname;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                            
                          <?php
                        } }
                        else {
                        ?>
                         <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                        <?php } ?>
                    </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-2"></div>
                      <div class="col-lg-4"></div>
                      <div class="col-lg-4"></div>
                      <div class="col-lg-2"></div>
                    </div>


                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <table id="tblForm09" class="table table-bordered table-striped" >
                        <thead>
                         <tr>
                          <th style="background-color: #eee; ">
                            Documents to be verified 
                          </th>
                          <th class="text-center" style="background-color: #eee;  width: 100px">
                            Certificate
                          </th>
                          <th  style="background-color: #eee; width: 200px;">

                          </th>
                        </tr>
                      </thead> 
                      <tr >
                       <td style="width: 300px;">10th Standard</td>
                       <td style="width: 200px; text-align: center;">

                        

                        <?php 
                        if (!empty($getcandateverified->encryptmetriccertificate)) {
                          

                            $matriccertificateArray = explode("|",$getcandateverified->encryptmetriccertificate);
                            $image_path='';

                            foreach($matriccertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                                 
                              ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"></i></a>
                          <?php } 
                      } 
                        }
                        else 
                        {
                          ?>
                          <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php } ?>
                      </td>
                      <td >
                        <div class="form-check" style="max-width: 500px;">
                          <input type="radio" class="form-check-input verify" id="metric_certificate_verifiedchecked" name="metric_certificate_verified" value="1" onclick="return checkgernalandjoiningform(this.id)" checked>
                          <label class="form-check-label" for="metric_certificate_verifiedchecked" >Verified</label>
                          <!-- Material checked -->
                          <input type="radio" class="form-check-input unverifed" onclick="return checkgernalandjoiningform(this.id)" id="metric_certificate_verified" name="metric_certificate_verified" value="2" style="margin-left: 5px;">
                          <label class="form-check-label" for="metric_certificate_verified" style="margin-left: 30px;">Re-upload</label>
                        </div>
                      </td> 
                    </tr>
                    <tr>
                      <td style="width: 300px;">12th Standard</td>
                      <td style="width: 100px; text-align: center;">
                        <?php if (!empty($getcandateverified->encrypthsccertificate)) 
                        {
                           $hsccertificateArray = explode("|",$getcandateverified->encrypthsccertificate);
                            $image_path='';

                            foreach($hsccertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                                ?>
                               <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>  
                              
                            <?php }
                            else {   
                          ?>

                        <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                        <?php } }}?>
                      </td>
                      <td>

                        <div class="form-check" style="max-width: 500px;">
                          <input type="radio" class="form-check-input verify" onclick="return checkgernalandjoiningform(this.id)" id="hsc_certificate_verifiedchecked" name="hsc_certificate_verified" value="1" checked>
                          <label class="form-check-label"  for="hsc_certificate_verifiedchecked">Verified</label>
                          <!-- Material checked -->
                          <input type="radio" class="form-check-input unverifed" onclick="return checkgernalandjoiningform(this.id)" id="hsc_certificate_verified" name="hsc_certificate_verified" value="2" style="margin-left: 5px;">
                          <label class="form-check-label" for="hsc_certificate_verified" style="margin-left: 30px;">Re-upload</label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 300px;">Graduation
                        <?php 
                        if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) { ?> 
                        <br>Undertaking for final year Marks Sheet <?php 
                        if($getcandateverified->ugmigration_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) { ?> 
                        <br>Unertaking of final Cerificate <?php 
                        if($getcandateverified->ugdoc_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);?>)<?php } } ?>
                     </td>
                     <td style="width: 100px; text-align: center;">
                      <?php if (!empty($getcandateverified->encryptugcertificate)) { 

                         $ugcertificateArray = explode("|",$getcandateverified->encryptugcertificate);
                            $image_path='';

                            foreach($ugcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      
                        Final Year Marksheet :
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugmigration_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);} ?>
                        <br>
                        Undertaking Marksheet 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>




                      <?php if (!empty($getcandateverified->ugdocencryptedImage)) { 

                         $ugdocArray = explode("|",$getcandateverified->ugdocencryptedImage);
                            $image_path='';

                            foreach($ugdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugdoc_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);} ?>
                        <br>
                        Undertaking Certificate 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>
                      </td>

                      <td>

                        <div class="form-check">
                          <input type="radio" class="form-check-input verify" onclick="return checkgernalandjoiningform(this.id)" id="ug_certificate_verifiedchecked" name="ug_certificate_verified" value="1" checked>
                          <label class="form-check-label" for="ug_certificate_verifiedchecked">Verified</label>
                          <!-- Material checked -->
                          <input type="radio" class="form-check-input unverifed" onclick="return checkgernalandjoiningform(this.id)" id="ug_certificate_verified" name="ug_certificate_verified" value="2" style="margin-left: 5px;">
                          <label class="form-check-label" for="ug_certificate_verified" style="margin-left: 30px;">Re-upload</label>
                        </div>
                      </td>
                    </tr>
                    <?php if (!empty($getcandateverified->encryptpgcertificate) || !empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?>  
                    <tr>
                      <td style="width: 300px;">Post Graduation
                        <?php 
                        if (!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?><br> Migration Marks Sheet<?php 
                          if($getcandateverified->pgmigration_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?><br> Migration Certificate<?php 
                          if($getcandateverified->pgdoc_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);?>)<?php } } ?>
                       </td>
                       <td style="width: 100px; text-align: center;">
                        <?php if (!empty($getcandateverified->encryptpgcertificate)) {

                          $pgcertificateArray = explode("|",$getcandateverified->encryptpgcertificate);
                            $image_path='';

                            foreach($pgcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgmigration_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);} ?>
                            <br>
                            Migration Marks Sheet 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>



                        <?php if (!empty($getcandateverified->pgdocencryptedImage)) {

                          $pgdocArray = explode("|",$getcandateverified->pgdocencryptedImage);
                            $image_path='';

                            foreach($pgdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgdoc_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);} ?>
                            <br>
                            Migration Certificate 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>
                          </td>
                          <td >

                            <div class="form-check">
                              <input type="radio" class="form-check-input verify"  onclick="return checkgernalandjoiningform(this.id)" id="pg_certificate_verifiedchecked" name="pg_certificate_verified" value="1" checked>
                              <label class="form-check-label" for="pg_certificate_verifiedchecked">Verified</label>
                              <!-- Material checked -->
                              <input type="radio" class="form-check-input unverifed"  onclick="return checkgernalandjoiningform(this.id)" id="pg_certificate_verified" name="pg_certificate_verified" value="2" style="margin-left: 5px;">
                              <label class="form-check-label" for="pg_certificate_verified" style="margin-left: 30px;">Re-upload</label>
                            </div>
                          </td>
                        </tr>
                        <?php }  ?>


                        <?php if (!empty($getcandateverified->encryptofferlettername)) { ?>  
                        <tr>
                          <td style="width: 300px;">Signed Offer Letter</td>
                          <td style="width: 100px; text-align: center;">

                            <?php  if (!empty($getcandateverified->encryptofferlettername)) {

                               $image_path='pdf_offerletters/'.$getcandateverified->encryptofferlettername;
                               // echo $image_path;
                              if (file_exists($image_path)) {

                              ?>

                            <a href="<?php echo site_url().'pdf_offerletters/'.$getcandateverified->encryptofferlettername;?>" download ><i class="fa fa-download" ></i></a>

                              <?php } else {?>

                              <?php } }?>
                              </td>
                          <td >

                            <div class="form-check">
                              <input type="radio" class="form-check-input verify"  onclick="return checkgernalandjoiningform(this.id)" id="signed_certificate_verifiedchecked" name="signed_certificate_verified" value="1" checked>
                              <label class="form-check-label" for="signed_certificate_verifiedchecked">Verified</label>
                              <!-- Material checked -->
                              <input type="radio" class="form-check-input unverifed"  onclick="return checkgernalandjoiningform(this.id)" id="signed_certificate_verified" name="signed_certificate_verified" value="2" style="margin-left: 5px;">
                              <label class="form-check-label" for="signed_certificate_verified" style="margin-left: 30px;">Re-upload</label>
                            </div>
                          </td>
                        </tr>
                        <?php }  ?>
                        <?php if (!empty($getcandateverified->encryptothercertificate)) {?>  <tr>
                          <td style="width: 300px;">Other</td>
                          <td style="width: 100px; text-align: center;">

                            <input type="hidden" name="other_certificate" id="other_certificate" value="1">
                            <?php 
                              $image_path='';
                                $image_path='datafiles\educationalcertificate/'.$getcandateverified->encryptothercertificate;
                                 if (file_exists($image_path)) {
                            ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->encryptothercertificate;?>" download ><i class="fa fa-download" ></i></a> 
                              <?php } else {?>
                                <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                              <?php } ?>
                          </td>
                            <td style="text-align: left;">

                             <div class="form-check">
                              <input type="radio" class="form-check-input verify" onclick="return checkgernalandjoiningform(this.id)" id="other_certificate_verifiedchecked" name="other_certificate_verified" value="1" checked>
                              <label class="form-check-label" for="other_certificate_verifiedchecked">Verified</label>
                              <!-- Material checked -->
                              <input type="radio" class="form-check-input unverifed" onclick="return checkgernalandjoiningform(this.id)" id="other_certificate_verified" name="other_certificate_verified" value="2" style="margin-left: 5px;" >
                              <label class="form-check-label" for="other_certificate_verified" style="margin-left: 30px;">Re-upload</label>
                            </div>
                          </td>
                        </tr>
                        <?php }  ?>


                        <?php 

                      
                        //echo "<pre>";
                       // print_r($gapyeardetails);
                       // die;
                        if ($gapyearcount > 0) {
                          ?>

                          <input type="hidden" name="gapyear_certificate" id="gapyear_certificate;?>" value="<?php echo $gapyearcount;?>">
                          <?php


                          foreach ($gapyeardetails as $key => $value) {

                            ?>
                            <tr>

                              <td style="width: 300px;">Gap Year Document</td>
                              


                              <td style="width: 100px; text-align: center;">

                                <?php 
                                $image_path='datafiles\educationalcertificate/'.$value->encrypteddocumentsname;
                               
                              if (file_exists($image_path)) {
                                ?>
                                <a href="<?php echo site_url().'datafiles/gapyeardocument/'.$value->encrypteddocumentsname;?>" download ><i class="fa fa-download" ></i></a> 
                                <?php } else {?>
                                <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                                <?php  } ?>
                              </td>


                            </td>
                            <td >




                             <div class="form-check">
                              <input type="radio" class="form-check-input verify"  onclick="return checkgernalandjoiningform(this.id)" id="gapyear_certificate_verifiedchecked_<?php echo $key;?>" name="gapyear_certificate_verified[<?php echo $key;?>]" value="1" checked="checked">
                              <label class="form-check-label" for="gapyear_certificate_verifiedchecked">Verified</label>
                              <!-- Material checked -->
                              <input type="radio" class="form-check-input unverifed"  id="gapyear_certificate_verified" onclick="return checkgernalandjoiningform(this.id)" name="gapyear_certificate_verified[<?php echo $key;?>]" value="2" style="margin-left: 5px;">
                              <label class="form-check-label" for="gapyear_certificate_verified_<?php echo $key;?>" style="margin-left: 30px;">Re-upload</label>
                            </div>





                          </td>
                        </tr>
                        <?php } } ?>

                        <?php if ($getworkexpcount > 0) {

                          foreach ($getworkexpverified as $key => $value) {
                            ?>
                            <tr>
                              <td style="width: 300px;">Work Experience Certificate, Salary Certificate</td>

                              <td style="width: 100px; text-align: center;">

                                <input type="hidden" name="workexp_certificate" id="workexp_certificate_<?php echo $key;?>" value="1">

                                <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download ><i class="fa fa-download"></i></a> |

                                <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip1;?>" download ><i class="fa fa-download"></i></a> |
                                <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip2;?>" download ><i class="fa fa-download"></i></a> |

                                <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip3;?>" download ><i class="fa fa-download"></i></a>
                              </td>
                              <td >
                               <div class="form-check">
                                <input type="radio" onclick="return checkgernalandjoiningform(this.id)" class="form-check-input verify" id="workexp_certificate_verifiedchecked_<?php echo $key;?>" name="workexp_certificate_verified[<?php echo $key;?>]" value="1" checked="checked">
                                <label class="form-check-label" for="workexp_certificate_verifiedchecked_<?php echo $key;?>"> Verified</label>
                                <!-- Material checked -->
                                <input type="radio" onclick="return checkgernalandjoiningform(this.id)" class="form-check-input unverifed" id="workexp_certificate_verified_<?php echo $key;?>" name="workexp_certificate_verified[<?php echo $key;?>]" value="2" style="margin-left: 5px;">
                                <label class="form-check-label" for="workexp_certificate_verified_<?php echo $key;?>" style="margin-left: 30px;">Re-upload</label>
                              </div>

                            </td>
                          </tr>

                          <?php } }  ?>
                        </table>
                      </div>
                    </div>
                    <?php //print_r($getjoiningreportstatus); ?>

                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <table id="tblcomment" class="table table-bordered table-striped" >
                          <thead>
                           <tr>
                             <th style="background-color: #eee;width:20%">Approve </th>
                             <th style="background-color: #eee; width:80%">Comments</th>
                           </tr> 
                         </thead>
                         <tbody >
                          <tr >
                            <td style="max-width: 500px;" class="form-inline">
                             <div class="form-check" style="max-width: 500px; margin: 5px;">
                              <input type="radio" style="padding-left: 50px;" class="form-check-input" id="Approvechecked" name="Approve" value="1" checked>
                              <label class="form-check-label"  for="Approvechecked" >Yes</label>
                              <!-- Material checked -->
                            </div>
                            <div class="form-check" style="max-width: 500px;">
                              <input type="radio" style="padding-left: 50px;" class="form-check-input" id="Approve" name="Approve" value="0" >
                              <label class="form-check-label" for="Approve" style="margin-left: 5px;">No</label>
                            </div>
                          </td>
                          <td><textarea name="comments" id="comments" class="form-control" style="resize: none;" required="required"><?php echo set_value('comments');?></textarea>
                          </td>

                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>




                <div class="panel-footer text-right">
                  <input class="btn btn-success btn-sm md-10"  type="reset" value="Reset" data-toggle="tooltip" title="Want to reset your changes? Click on me.">
                  <!--  <button class="btn btn-primary" type="submit" name="savebtn" value="senddatasave" id="savebtn">Save</button> -->
                  <button class="btn btn-dark btn-sm md-10" type="submit" name="submitbtn" id="submitbtn" value="senddatasubmit" data-toggle="tooltip" title="Want to submit your changes? Click on me.">Submit</button>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>

  <script type="text/javascript">
    function checkgernalandjoiningform(){
      var unverifed = false;
      $('input[type=radio]').each(function(){
        var myclass = this.classList;
        if(myclass[1] == "unverifed" && $(this).is(':checked')==true){
          unverifed = true;
        }
      });

  //alert(unverifed);
  if(unverifed==true){
   $('#Approve').prop('checked', true);
   $('#Approve').prop('disabled', false);
   $('#Approvechecked').prop('disabled', true);
   $('#Approvechecked').prop('checked', false);
 }else{
   $('#Approvechecked').prop('checked', true);
   $('#Approvechecked').prop('disabled', false);
   $('#Approve').prop('disabled', true);
   $('#Approve').prop('checked', false);    
 }

}   

</script>