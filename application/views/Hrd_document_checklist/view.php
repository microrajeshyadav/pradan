
<section class="content">
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-8 panel-title pull-left">Offered candidate verification documents</h4>
               
            </div>
            <hr class="colorgraph"><br>
          </div>
          <div class="panel-body">
      
              <form name="joinDA" action="" method="post">
                <div class="row">
                  <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Candidate Name :</label></div>
                  <div class="col-lg-4">
                    <input type="hidden" name="candidateid" id="candidateid" value="<?php echo $getcandateverified->candidateid;?>">
                   <?php echo $getcandateverified->candidatefirstname.' '.$getcandateverified->candidatemiddlename.' '.$getcandateverified->candidatelastname; ?>
                  </div>

                <?php  if (!empty($getcandateverified->batch)) { ?>
                  <div class="col-lg-1 text-right"><label for="joinDA" class="field-wrapper required-field">Batch :</label></div>
                  <div class="col-lg-2"><?php echo $getcandateverified->batch; ?></div>
              <?php  } ?>
                 
                  <div class="col-lg-1"><label for="joinDA" class="field-wrapper required-field">Photo :</label></div>
                  <div class="col-lg-1" id="candidateprofilepic" style="width:105px; height:133px; padding: 3px;">


                      <?php   
                        $image_path='';
                      if(!empty($getcandateverified->encryptedphotoname)) {
                           $image_path='datafiles/'.$getcandateverified->encryptedphotoname;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$getcandateverified->encryptedphotoname;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
            <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                            
                          <?php
                        } }
                        else {
                        ?>
                         <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                        <?php } ?>


                  </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-2"></div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <table id="tblForm09" class="table table-bordered table-striped" >
                      <thead>
                       <tr>
                        <th class="bg-light">
                          Documents to be verified 
                        </th>
                        <th  class="bg-light text-center" style="max-width: 100px">
                          Certificate
                        </th>
                      <th class="bg-light">
                          Status
                        </th>
                      </tr>
                    </thead> 

                    <tr>
                     <td>10th Standard</td>

                     <td class="text-center" style="max-width: 100px">



                         <?php
                          $matriccertificateArray=array();
                        if (!empty($getcandateverified->encryptmetriccertificate)) {
                          

                            $matriccertificateArray = explode("|",$getcandateverified->encryptmetriccertificate);
                            $image_path='';

                            foreach($matriccertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                                 
                              ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download"></i></a>
                          <?php } 
                      } 
                        }
                        else 
                        {
                          ?>
                          <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php } ?>



                     
                     </td>
                       <td>
                        
                          <label class="form-check-label" for="metric_certificate_verified">verified</label>
                        
                      </td> 
                    </tr>
                    <tr>
                      <td>12th Standard</td>
                      <td class="text-center" style="max-width: 100px">

                           <?php if (!empty($getcandateverified->encrypthsccertificate)) 
                        {
                           $hsccertificateArray = explode("|",$getcandateverified->encrypthsccertificate);
                            $image_path='';

                            foreach($hsccertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                                ?>
                               <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>  
                              
                            <?php }
                            else {   
                          ?>

                        <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                        <?php } }}?>
                       

                      </td>
                      <td >
                       
                          <label class="form-check-label" for="hsc_certificate_verified">verified</label>
                       
                      </td>
                    </tr>
                    <tr>
                      <td>Graduation
                        <?php 
                        if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) { ?> 
                        <br>Migration Marks Sheet <?php 
                        if($getcandateverified->ugmigration_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) { ?> 
                        <br>Migration Cerificate <?php 
                        if($getcandateverified->ugdoc_certificate_date != '0000-00-00')
                         {?>(Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);?>)<?php } } ?>
                     </td>
                     <td style="width: 100px; text-align: center;">
                      <?php if (!empty($getcandateverified->encryptugcertificate)) { 

                         $ugcertificateArray = explode("|",$getcandateverified->encryptugcertificate);
                            $image_path='';

                            foreach($ugcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugmigration_encrypted_certificate_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugmigration_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugmigration_certificate_date);} ?>
                        <br>
                        Migration Marks Sheet 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>




                      <?php if (!empty($getcandateverified->ugdocencryptedImage)) { 

                         $ugdocArray = explode("|",$getcandateverified->ugdocencryptedImage);
                            $image_path='';

                            foreach($ugdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {

                        ?>
                      <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res;?>" download ><i class="fa fa-download" ></i></a>
                      <?php  } 
                        else {
                       ?>

                       <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                       <?php }
                        }}
                        
                        ?>
                         

                      <?php if (!empty($getcandateverified->ugdoc_encrypted_certificate_doc_upload)) {?>
                      <br>
                      <?php if($getcandateverified->ugdoc_certificate_date != '0000-00-00'){
                        ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->ugdoc_certificate_date);} ?>
                        <br>
                        Migration Certificate 

                        <?php 
                          $image_path='';
                      $image_path='datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;
                               
                              if (file_exists($image_path)) {
                        ?>
                        <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->ugdoc_encrypted_certificate_doc_upload;?>" download ><i class="fa fa-download" ></i></a> 
                        <?php }
                          else {
                            ?>
                            <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php 
                          } }
                         ?>
                      </td>
                      <td>
                        
                          <label class="form-check-label" for="ug_certificate_verified">verified</label>
                       
                      </td>

                    </tr>
                   
                      <tr>
                        <td>Post Graduation
                        <?php 
                        if (!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?><br> Migration Marks Sheet<?php 
                          if($getcandateverified->pgmigration_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);?>)<?php } } ?>
                        <?php 
                        if (!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?><br> Migration Certificate<?php 
                          if($getcandateverified->pgdoc_certificate_date != '0000-00-00')
                           {?> (Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);?>)<?php } } ?>
                       </td>
                       <td style="width: 100px; text-align: center;">
                        <?php if (!empty($getcandateverified->encryptpgcertificate)) {

                          $pgcertificateArray = explode("|",$getcandateverified->encryptpgcertificate);
                            $image_path='';

                            foreach($pgcertificateArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgmigration_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgmigration_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgmigration_certificate_date);} ?>
                            <br>
                            Migration Marks Sheet 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgmigration_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>



                        <?php if (!empty($getcandateverified->pgdocencryptedImage)) {

                          $pgdocArray = explode("|",$getcandateverified->pgdocencryptedImage);
                            $image_path='';

                            foreach($pgdocArray as $res)
                            {
                               $image_path='datafiles\educationalcertificate/'.$res;
                               
                              if (file_exists($image_path)) {
                          ?>
                          <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$res?>" download ><i class="fa fa-download" ></i></a>
                          <?php } else { ?>
                           <a href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download"></i></a>
                          <?php }} } ?>
                          <?php if(!empty($getcandateverified->pgdoc_encrypted_certificate_upload)) { ?>
                          <br>
                          <?php if($getcandateverified->pgdoc_certificate_date != '0000-00-00'){
                            ?>Date:<?php echo $this->gmodel->changedatedbformate($getcandateverified->pgdoc_certificate_date);} ?>
                            <br>
                            Migration Certificate 
                              <?php 
                             $image_path='datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;
                               
                              if (file_exists($image_path)) {
                                ?>
                            <a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->pgdoc_encrypted_certificate_upload;?>" download ><i class="fa fa-download" ></i></a>
                            <?php } 


                              else {
                            ?>
                              <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                            <?php }  }?>
                          </td>
                        <td>
                          
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                         
                        </td>
                      </tr>
                   
                    
                   
                   <?php if ($getCandateyapyearexpcount > 0) { 

                      foreach ($getgapyearverified as $key => $value) {
                    ?>  
                      <tr>
                        <td>Gap Year Document's</td>
                        <td class="text-center" style="max-width: 100px"><a href="<?php echo site_url().'datafiles/gapyeardocument/'.$value->encrypteddocumentsname;?>" download ><i class="fa fa-download"></i></a> </td>
                        <td>
                          
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                         
                        </td>
                      </tr>
                    <?php } }?>
                    <?php if (!empty($getcandateverified->originalothercertificate)) {?>  
                      <tr>
                        <td >Post Graduation</td>
                        <td class="text-center"><a href="<?php echo site_url().'datafiles/educationalcertificate/'.$getcandateverified->originalothercertificate;?>" download ><i class="fa fa-download"></i></a> </td>
                        <td>
                          
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                        
                        </td>
                      </tr>
                    <?php }  ?>
                    <?php 
                      if(!empty($getcandateverified->encryptofferlettername)) {
                        if($getcandateverified->categoryid !=2){
                    ?>  
                      <tr>
                        <td>Signed Offer Letter</td>
                        <td class="text-center" style="max-width: 100px">

                              <?php 
                               $image_path='datafiles/educationalcertificate/'.$getcandateverified->encryptofferlettername;
                               
                              if(file_exists($image_path)) {

                              ?>

                            <a href="<?php echo site_url().'pdf_offerletters/'.$getcandateverified->encryptofferlettername;?>" download ><i class="fa fa-download" ></i></a>

                              <?php } else {?>
                                <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" target="_blank" ><i class="fa fa-download" aria-hidden="true"></i> </a>
                              <?php } ?>

                          </td>
                        <td>
                          
                            <label class="form-check-label" for="pg_certificate_verified">verified</label>
                         
                        </td>
                      </tr>
                    <?php } } ?>
                    
                    <?php if ($getworkexpcount > 0) {
                      foreach ($getworkexpverified as $key => $value) {
                      ?>
                    <tr>
                      <td>Work Experience Certificate</td>
                      <td class="text-center">
                        <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encrypteddocumnetname;?>" download ><i class="fa fa-download"></i></a> 
                      </td>
                      <td>
                          <label class="form-check-label" for="workexp_certificate_verified">verified</label>
                      </td>
                    </tr>

                     <tr>
                      <td>Salary Certificate</td>
                      <td class="text-center">
                        <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip1;?>" download ><i class="fa fa-download"></i></a> 
                      </td>
                      <td>
                          <label class="form-check-label" for="workexp_certificate_verified">verified</label>
                      </td>
                    </tr>
                     <tr>
                      <td>Salary Certificate</td>
                      <td class="text-center">
                        <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip2;?>" download ><i class="fa fa-download"></i></a> 
                      </td>
                      <td>
                          <label class="form-check-label" for="workexp_certificate_verified">verified</label>
                      </td>
                    </tr>
                     <tr>
                      <td>Salary Certificate</td>
                      <td class="text-center">
                        <a href="<?php echo site_url().'datafiles/workexperience/'.$value->encryptedsalaryslip3;?>" download ><i class="fa fa-download"></i></a> 
                      </td>
                      <td>
                          <label class="form-check-label" for="workexp_certificate_verified">verified</label>
                      </td>
                    </tr>
                    <?php } }  ?>
                  </table>
                </div>
              </div>
<div class="row">
</div>
          <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <table id="tblcomment" class="table table-bordered table-striped" >
          <thead>
           <tr>
            <th class="bg-light">Status </th>
              <th class="bg-light">Comments </th>
          </tr> 
        </thead>
        <tbody >
          <tr >
            <th>
              <?php if ($getcandateverified->status==1) {?>
              Approved 
              <?php }else{?>
                Rejected 
              <?php } ?>
             
            </th>
            <th><?php echo $getcandateverified->comment;?></th>
          </tr>
        </tbody>
      </table>
    </div>
   </div>
    <div class="panel-footer text-right">
      <a href="<?php echo site_url('Hrd_document_checklist'); ?>" class="btn btn-dark"data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
     
    </div>
</form>

</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
 $(document).ready(function(){
  $("#candidateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getcandidatedetails/'+$(this).val(),
      type: 'POST',
      dataType: 'json',
    })
    .done(function(data) {
                 //alert(data.batch);
                   //alert(data);
                  // alert(data.responseJSON['encryptedphotoname']);
                 // console.log(data);
                 $("#batchid").val(data.batch);
                 $("#candidateprofilepic").html('<img src="<?php echo site_url().'datafiles/' ?>' + data.encryptedphotoname + '" width="50px" hight="50px" title="Default Image" alt="Default Image" boder="2px" />');
               })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>
<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-1>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertIdentityRows(rowsIdentityEnter);
    //addDatePicker();
  });

  // var identcount = $('#identcount').val();

  // if (identcount =='NULL') {
  //   var srNoIdentity=0;
  //   var incr = 0;
  // }else{
  //   var srNoIdentity = identcount;
  //   var incr = identcount;
  // }
  

  function insertIdentityRows(count) {
    alert();
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      incr++
      cloneRow = lastRow.clone();
      var tableDataIdentity = '<tr>'
      + ' <td>'
      + '<select class="form-control" name="identityname['+incr+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysidentity as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
      <?php endforeach ?>
      + '</select>'

      + ' <td><input type="text" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td><td>'

      +' <div class="form-group">'
      +'   <input type="file" name="identityphoto['+incr+']"  id="identityphoto" class="file">'
      +' </div>'
      + '</td>' + '</tr>';
      $("#tbodyForm13").append(tableDataIdentity)
    }

  }
</script>