<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
  

</style>
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE FOR (Level 4) 
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5> EXPERIENCE CERTIFICATE FOR (Level 4)   </h5> 
          <i><h6>(In case of termination)</h6></i>
        </div>
        <div class="col-md-12 text-center">
          <h4>TO WHOM IT MAY CONCERN</h4> 
        </div>          
      </div>
     <!-- <?php
//print_r($levelfourtermination_detail); 
     ?>  -->
<?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
     <form method="POST" action="" name="sep_levelfourtermination" id="sep_levelfourtermination">
       <input type="hidden" name="sepdesigid" value="<?php echo $level_fourtermination->sepdesigid;?>">
      

      <div class="col-md-12">
                 <div class="">
                  <div class="">
                    <label for="" class="field-wrapper required-field"></label>
                      <textarea class='summernote' id='summernote' name="lettercontent" rows='10' ><?php echo $content; ?></textarea>

                    </div>
                    <?php echo form_error("Content");?>
                  </div>
                </div>
                

<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($levelfourtermination_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
  <?php
    }else{

      if($levelfourtermination_detail->flag == 0){
 ?>
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
  <!-- <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
<?php } ?>
 
<?php } ?>
 <?php if($this->loginData->RoleID != 18){ ?>
 <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php }else{ ?>
 <a href="<?php echo site_url("Ed_staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } ?>
</div>

</form>
</div>
</div>
</section>
<?php 
if ($levelfourtermination_detail){
if($levelfourtermination_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);

  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  // $("form textarea").attr("style", "width:600px;");
  });
</script>
<?php }} ?>
