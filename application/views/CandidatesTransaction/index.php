
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
       
        <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>List Of Candidates Transaction</b></div>
        <div class="panel-body">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
            
        <div class="row" style="background-color: #FFFFFF;">
         <div class="col-xs-3 col-sm-12 col-md-3 col-lg-12 text-left" style="background-color: white;">Filter: </div>
         <div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 text-left" style="background-color: white;">
           <select name="filter" id="filter" class="form-control">
             <option value="DA">DA</option>
             <option value="LE">Lateral Entries</option>
           </select>
         </div>

         <div class="col-xs-3 col-sm-12 col-md-3 col-lg-12 text-left" style="background-color: white;">
           Type :
         </div>
    
        <div class="col-xs-3 col-sm-12 col-md-3 col-lg-12 text-left" style="background-color: white;">
           <select name="type" id="type" class="form-control">
             <option value="Campus">Campus</option>
             <option value="Delhi">Delhi</option>
           </select>
        </div>
                
           </div>
           
     <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>

      </div>
        
        <div class="row" style="background-color: #FFFFFF;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Application for</th>
              <th>Candidate Name </th>
             <th> Campus </th>
              <th>City</th>
              <th>Date</th>
            <th>Action</th>
           </tr> 
         </thead>
         <tbody>
        
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

            <td>  <a href="<?php //echo site_url()."CandidatesTransaction/AddTransaction/".$value->id;?>" class="btn btn-sm btn-success" >Add</a></td>
                


          
          </tr>
          
        </tbody>
      </table>
       </div>
    </div> 
    </div>
    </div>
    </div>   

<!---  Start Recruiters Modal Here  -->
<div id="myRecruiters" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        
    <div class="panel panel-default" >
                    <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Recruiters</b></div>
        <div class="panel-body">
           <form name="campus" action="" method="post" >            
           
               <br>
                   <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label style="font-weight: 20px; color: #000;" > Date of selecion process: </label>
                    </div>
                      <div class="col-lg-6">
                           <input type="date" value="" name="date_of_selecion_process" class="datepicker form-control" value="" placeholder="Date of selecion process" required="required">
                      </div>  
                    
                </div>
               </div>
                <br>
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label style="font-weight: 20px; color: #000;"> Recruiter1: </label>
                    </div>
                      <div class="col-lg-6">
                           <input class="form-control" value="" placeholder="Recruiter1" required="required">
                      </div>  
                    
                </div>
               </div>
                <br>
                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label style="font-weight: 20px; color: #000;">Recruiter2:</label>
                    </div>
                      <div class="col-lg-6">
                          <input class="form-control" value="" placeholder="Recruiter2" required="required">
                      </div>  
                    
                </div>
               </div>
                <br>
                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label style="font-weight: 20px; color: #000;"> Recruiter3: </label>
                    </div>
                      <div class="col-lg-6">
                           <input class="form-control" value="" placeholder="Recruiter3" required="required">
                      </div>  
                    
                </div>
               </div>
                <br>
               
              <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6"></div>
                      <div class="col-lg-6 text-right">
                           <button type="submit" class="btn btn-success">Save </button>
                          <button type="reset" class="btn btn-warning">Reset </button>
                      </div>  
                    
                </div>
               </div>
             </form> 
                </div><!-- /.panel-->

     </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>    
 </div>

</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
function yesnoCheck() {
   
    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
    }
  }
</script>
<script>
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>