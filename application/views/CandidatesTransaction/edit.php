<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
        <?php //print_r($campusupdate);  ?>
      <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Edit Batch</b></div>
        
        <div class="panel-body">
           <form name="Batch" action="" method="post" >            
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label> Batch: </label>
                    </div>
                      <div class="col-lg-6">
                           <input type="text" name="batch" id="batch" class="form-control" value="<?php echo $batchupdate[0]->batch;?>" placeholder="Placeholder" required="required">
                      </div>  
                </div>
               </div>
                <br>
                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>Date Of Joining:</label>
                    </div>
                      <div class="col-lg-6">
                          <input type="Date" name="dateofjoining" minlength="10" maxlength="150" class="form-control datepicker" value="<?php echo $batchupdate[0]->dateofjoining;?>" placeholder="Placeholder" required="required">
                      </div>  
                    
                </div>
               </div>
                <br>
               <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>Status:</label>
                    </div>
                      <div class="col-lg-6">
                      <select name="status" class="form-control" required="required">
                      <option value="">Select</option>
                        <option value="0" <?php if ($batchupdate[0]->status =='0') { echo "SELECTED";}else{ echo " ";}  ?> >Active</option>
                      <option value="1" <?php if ($batchupdate[0]->status =='1') { echo "SELECTED";}else{ echo " ";} ?>>InActive</option>
                    </select>
                  </div>  
                </div>
               </div>
                <br>
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6"> </div>
                      <div class="col-lg-6 text-right">
                           <button type="submit" class="btn btn-success">Save </button>
                          <button type="reset" class="btn btn-warning">Reset </button>
                      </div>  
                </div>
               </div>
                 </form> 
                </div><!-- /.panel-->
             </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Exportable Table -->
      </div>
    </section>