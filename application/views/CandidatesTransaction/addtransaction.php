<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
        <?php //print_r($campusupdate);  ?>
      <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Candidates Transation </b></div>
        
        <div class="panel-body">
           <form name="Batch" action="" method="post" >            
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label> Sociometry: </label>
                    </div>
                      <div class="col-lg-6">
                           <input type="text" name="Sociometry" id="Sociometry" class="form-control" value="" placeholder="Enter Score Sociometry" required="required">
                      </div>  
                </div>
               </div>
                <br>
                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>Recruiter :</label>
                    </div>
                      <div class="col-lg-6">
                          <input type="text" name="recruiter" minlength="10" maxlength="50" class="form-control"  value="" placeholder="Placeholder" required="required">
                      </div>  
                    
                </div>
               </div>


                <br>
                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>WT :</label>
                    </div>
                      <div class="col-lg-6">
                          <input type="text"  name="wt" minlength="10" maxlength="50" class="form-control"  value="" placeholder="Enter WT" required="required">
                      </div>  
                    
                </div>
               </div>



                <br>
                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>Choose Team :</label>
                    </div>
                      <div class="col-lg-6">
                        <select name="chooseteam" id="chooseteam" class="form-control">
                          <option></option>
                           <option></option>
                            <option></option>
                             <option></option>
                              <option></option>
                        </select>
                        
                      </div>  
                    
                </div>
               </div>
                <br>

              
                <br>


               <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>FG:</label>
                    </div>
                      <div class="col-lg-6">
                      <select name="fg" class="form-control" required="required">
                      <option value="">Select FG</option>
                         <option></option>
                           <option></option>
                            <option></option>
                             <option></option>
                              <option></option>
                        
                    </select>
                  </div>  
                </div>
               </div>
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                          <label>Batch:</label>
                    </div>
                      <div class="col-lg-6">
                      <select name="fg" class="form-control" required="required">
                      <option value="">Select Batch</option>
                         <option></option>
                           <option></option>
                            <option></option>
                             <option></option>
                              <option></option>
                        
                    </select>
                  </div>  
                </div>
               </div>

                <br>
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-6"> </div>
                      <div class="col-lg-6 text-right">
                           <button type="submit" class="btn btn-success">Save </button>
                          <button type="reset" class="btn btn-warning">Reset </button>
                      </div>  
                </div>
               </div>
                 </form> 
                </div><!-- /.panel-->
             </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Exportable Table -->
      </div>
    </section>