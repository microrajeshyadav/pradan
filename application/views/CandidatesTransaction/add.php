<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">

            
        <div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Add Batch</b></div>

        <div class="panel-body">
           <form name="campus" action="" method="post" >            
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3">
                          <label>Batch:</label>
                    </div>
                      <div class="col-lg-4">
                       <input type="text" name="batch" class="form-control" value="" placeholder="Enter Batch" required="required">
                  </div> 
                  <div class="col-lg-5"></div> 
                </div>
               </div>
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3">
                          <label> Date of Joining: </label>
                    </div>
                      <div class="col-lg-4">
                           <input type="Date"  name="dateofjoining" id="dateofjoining" class="form-control" value="" placeholder="Enter Date of Joining" required="required">
                      </div>  
                        <div class="col-lg-5"></div> 
                </div>
               </div>

                <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3">
                          <label> Status: </label>
                    </div>
                      <div class="col-lg-4">
                           <select name="status" id="status" class="form-control" >
                             <option value="0">Active</option>
                              <option value="1">InActive</option>
                           </select>
                      </div> 
                        <div class="col-lg-5"></div>  
                </div>
               </div>
               
                
               
                 <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3"> </div>
                      <div class="col-lg-4 text-right">
                         <button type="submit" class="btn btn-success">Save </button>
                         <button type="reset" class="btn btn-warning">Reset </button>
                      </div>  
                        <div class="col-lg-5"></div> 
                </div>
               </div>
                 </form> 
                </div>


              </div><!-- /.panel-->

            </div>

          </div>

        </div>

      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>