
<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                 <h4 class="header" class="field-wrapper required-field" style="color:#f44336">Add Recruiters</h4>
                 <div class="col-sm-12">
                   <form method="POST" action="">
                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name <span style="color:red;">*</span></label>

                           <?php 
                          $options = array('' => 'Select Campus');
                          foreach($campusdetails as$key => $value) {
                              $options[$value->campusid] = $value->campusname;
                          }
                          echo form_dropdown('campusname', $options, set_value('campusname'), 'class="form-control"');
                          ?>
                        </div>
                        <?php echo form_error("campusname");?>
                       </div>

                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Date of selection Process <span style="color:red;">*</span></label>
                        <input type="date" class="form-control datepicker" name="dateofselecionprocess" id="dateofselecionprocess" placeholder="Please Enter Selecion Process" value="<?php echo set_value('dateofselecionprocess');   ?>" >
                      </div>
                        <?php echo form_error("dateofselecionprocess");?>
                    </div>

                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Recruiters1 <span style="color:red;">*</span></label>
                        <?php 
                          $options = array('' => 'Select Recruiters');
                          foreach($recruitersdetails as$key => $value) {
                              $options[$value->staffid] = $value->name;
                          }
                          echo form_dropdown('recruiters1', $options, set_value('recruiters1'), 'class="form-control"');
                          ?>
                        </div>
                        <?php echo form_error("recruiters1");?>
                    </div>

                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Recruiters2 <span style="color:red;">*</span></label>
                        <?php 
                          $options = array('' => 'Select Recruiters');
                          foreach($recruitersdetails as$key => $value) {
                              $options[$value->staffid] = $value->name;
                          }
                          echo form_dropdown('recruiters2', $options, set_value('recruiters2'), 'class="form-control"');
                          ?>
                        </div>
                        <?php echo form_error("recruiters2");?>
                    </div>


                      <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Recruiters3 <span style="color:red;">*</span></label>
                        <?php 
                          $options = array('' => 'Select Recruiters');
                          foreach($recruitersdetails as$key => $value) {
                              $options[$value->staffid] = $value->name;
                          }
                          echo form_dropdown('recruiters3', $options, set_value('recruiters3'), 'class="form-control"');
                          ?>
                        </div>
                        <?php echo form_error("recruiters3");?>
                    </div>

                   
                      <div style="text-align: -webkit-center;"> 
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("recruiters");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>



