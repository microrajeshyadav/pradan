
 <section class="content" style="background-color: #FFFFFF;" >
    <br>
    <div class="container-fluid">
      <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Written Test score</b></div>
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        <?php //print_r($recruitercampusdetails);  ?>
      <div class="row" style="background-color: #FFFFFF;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color: white;">
       <!--  <a href="<?php //echo site_url()."Recruiters/add/";?>" class="btn btn-sm btn-success" >Add Recruiter</a> -->
         <br>
         <br>
       </div>
       </div>

        <div class="row" style="background-color: #FFFFFF;">
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">   
         <label for="StateNameEnglish" class="field-wrapper required-field">Campus Name </label> </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="background-color: white;">  
         <?php 
        $options = array('' => 'Select Campus');
        foreach($campusdetails as$key => $value) {
            $options[$value->campusid] = $value->campusname;
        }
        echo form_dropdown('campusname', $options, set_value('campusname'), 'class="form-control"');
        ?>
      <?php echo form_error("campusname");?>
       </div>
        <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white; text-left"></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white; text-left"><b>City</b></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white; text-left"><input type="" class="form-control" name=""></div>
      <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white; text-left"></div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
      </div>
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tablecampus" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th>S.No.</th>
              <th>Candidate</th>
              <th> Email Id</th>
              <th>Written Score</th>
              <th>GD Score</th>
              <th>HR Score</th>
           </tr> 
         </thead>
         <tbody>
            <tr>
              <td>1</td>
              <td>Rajesh</td>
              <td>Rajesh@gmail.com</td>
            <td>
            <div class="col-lg-6"></div>
             <div class="col-lg-6"> <a href="<?php  echo site_url()."Score/AddWrittenScore/";?>" class="btn btn-success btn-sm">Add </a></div>
           </div>
             </td>
             <td>
              <div class="col-lg-6"></div>
             <div class="col-lg-6"><a href="<?php  echo site_url()."Score/AddGDScore/";?>" class="btn btn-success btn-sm">Add </a></div>
              </td>
          <td>
           <div class="col-lg-6"></div>
             <div class="col-lg-6"> <a href="<?php  echo site_url()."Score/AddHRScore/";?>" class="btn btn-success btn-sm">Add </a></div>
           </td>
            </tr>

             <tr>
              <td>2</td>
              <td>Amit</td>
              <td>Amit@gmail.com</td>
             <td>
              <div class="col-lg-6"></div>
             <div class="col-lg-6"> <a href="<?php  echo site_url()."Score/AddWrittenScore/";?>" class="btn btn-success btn-sm" >Add </a></div>
           </div>
             </td>
             <td>
              <div class="col-lg-6"></div>
             <div class="col-lg-6"><a href="<?php  echo site_url()."Score/AddGDScore/";?>" class="btn btn-success btn-sm" >Add </a></div>
              </td>
          <td>
           <div class="col-lg-6"></div>
             <div class="col-lg-6"> <a href="<?php  echo site_url()."Score/AddHRScore/";?>" class="btn btn-success btn-sm" >Add </a></div>
           </td>
            </tr>
            
             <tr>
              <td>3</td>
              <td>Rakesh</td>
              <td>rakesh@gmail.com</td>
           <td>
              <div class="col-lg-6"></div>
             <div class="col-lg-6"> <a href="<?php  echo site_url()."Score/AddWrittenScore/";?>" class="btn btn-success btn-sm">Add </a></div>
           </div>
             </td>
             <td>
              <div class="col-lg-6"></div>
             <div class="col-lg-6"><a href="<?php  echo site_url()."Score/AddGDScore/";?>" class="btn btn-success btn-sm">Add </a></div>
              </td>
          <td>
           <div class="col-lg-6"></div>
             <div class="col-lg-6"> <a href="<?php  echo site_url()."Score/AddHRScore/";?>" class="btn btn-success btn-sm">Add </a></div>
           </td>
            </tr>

          </tbody>
        </table>
       </div>
       <div style="text-align: -webkit-center;"> 
            <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
            <a href="<?php echo site_url("recruiters");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
          </div>
      </div> 
     </div>
    </div>
  </div>   
</section>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });
</script>  


 <script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY', time: false });
});


function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
   
    if (r==true) {
        return true;
    } else {
        return false;
    }

}
</script>