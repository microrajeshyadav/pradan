<br/>
<section class="content">
  <?php //echo $token; //foreach ($role_permission as $row) { if ($row->Controller == "Permissions" && $row->Action == "index"){ ?>

    <div class="container-fluid">

     <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-7 panel-title pull-left">Manage Staff Transfer History </h4>
         
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          
          <!-- Exportable Table -->
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table id="staff_transfer_history" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 50px;">S.NO</th>
                    <th class="text-center">Proposed Date</th>
                    <th class="text-center">Effective Date</th>
                    <th class="text-center">Staff</th>
                    <th class="text-center">Old Office</th>
                    <th class="text-center">New Office</th>
                    <!--  <th class="text-center">New Designation</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php
                  
                    //print_r($gethistory); die;
                  $i=0; foreach($gethistory as $row){ 
                    
                     //print_r($gethistory);  
                    ?>
                    <tr>
                      <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                      <td class="text-center">
                        <?php 
                        $timestamp = strtotime($row->date_of_transfer);
                        echo date('d/m/Y', $timestamp);?>
                     </td>

                     <td class="text-center">
                        <?php 
                        $effective_date = strtotime($row->effective_date);
                        echo date('d/m/Y', $effective_date);?>
                     </td>

                    
                      <td class="text-center"><?php echo $row->name; ?></td> 
                      <td class="text-center"><?php echo $row->oldofffice; ?></td> 
                      <td class="text-center"><?php echo $row->presentoffice; ?></td>
                      
                      
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- #END# Exportable Table -->
          </div>
        </div>
      </div>
      <?php //} } ?>
    </section>
    <script>
     $(document).ready(function() {
      $('#staff_transfer_history').DataTable({
        "paging": true,
        "search": true,
      });
      
      function confirm_delete() {
        
        var r = confirm("Are you sure you want to delete this item?");

        if (r == true) {
          return true;
        } else {
          return false;
        }
        
      }
    });
  </script>