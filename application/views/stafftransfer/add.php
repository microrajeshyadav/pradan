<section class="content">
 <?php //foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "add"){
//echo $token;
  ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <div class="container-fluid" style="margin-top: 20px;">
      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
        <form method="POST" action="">
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-7 panel-title pull-left">Transfer Request</h4>
               <div class="col-md-5 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>
          <div class="panel-body">

            <?php 
            $tr_msg= $this->session->flashdata('tr_msg');
            $er_msg= $this->session->flashdata('er_msg');

            if(!empty($tr_msg)){ ?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('tr_msg');?>. </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('er_msg');?>. </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php } ?>

                <input type="hidden" name="old_designation" value="<?php if(!empty($transfer_promption->new_designation)) echo  $transfer_promption->new_designation;?>">
                <input type="hidden" name="staff" id="staff" value="<?php if(!empty($staff->staffid))  echo $staff->staffid;  ?>" class="form-control" >
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Staff<span style="color: red;" > *</span></label>
                    <input type="text" name="staffname" id="staffname" value="<?php if(!empty($staff->name)) echo $staff->emp_code .' - '.$staff->name .' ['.$staff->desname .']' ;?>" class="form-control" disabled="disabled">
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Old Office<span style="color: red;" >*</span></label>
                    <select class="form-control" name="Presentoffice" id="Presentoffices56">
                      <option value="<?php echo $transfer_promption->new_office_id;?>"  selected> <?php echo $transfer_promption->officename;?></option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">New Office <span style="color: red;" >*</span></label>

                        <select class="form-control" name="transfer_office" id="transfer_office" required="required">
                          <option value="">Select Office</option>
                          <?php foreach($all_office as $val){?>
                          <option value="<?php echo $val->officeid;?>" ><?php echo $val->officename;?> </option>
                          <?php  } ?>
                        </select>
                      </div>

                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Reporting To <span style="color: red;" >*</span></label>

                        <select class="form-control" name="reportingto" id="reportingto" required="required">
                          <option value="">Select Reporting To</option>
                        </select>
                      </div>

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="form-line">
                       <label for="StateNameEnglish" class="field-wrapper required-field">Proposed Transfer Date<span style="color: red;" >*</span></label>

                       <input type="text" class="form-control datepicker " data-toggle="tooltip" 
                       id="fromdate" name="fromdate" placeholder="Proposed Transfer Date" required="required" style="min-width: 20%;" autocomplete="false">
                     </div>
                   </div>
                 </div>

                 <div class="col-md-6">
                   <div class="form-group">
                    <div class="form-line">
                     <label for="StateNameEnglish" class="field-wrapper required-field">Effective Date<span style="color: red;" >*</span></label>

                     <input type="text" class="form-control datepicker " data-toggle="tooltip" 
                     id="effective_date" name="effective_date" placeholder="Effective Date" required="required" style="min-width: 20%;" autocomplete="false" >
                   </div>
                 </div>
               </div></div>
               <!-- <div class="form-check">
                <input class="form-check-input" type="checkbox" name="changereportingto" id = "changereportingto">
                 <label for="StateNameEnglish" class="field-wrapper required-field">Do you want to change reporting for new office staff on this transfer ? </label>
                
               
             </div> -->
               <div class="form-group">
                <div class="form-line">
                 <label for="StateNameEnglish" class="field-wrapper required-field">Reason<span style="color: red;" >*</span></label>
                 (<i>Maximum 250 characters allowed</i>)

                 <textarea class="form-control" data-toggle="tooltip" 
                 name="reason" id="reason" placeholder="Reason" maxlength="250" required="required" style="min-width: 20%;" ></textarea> 
               </div>
             </div>

             <div class="form-group">
              <div class="form-line">
               <label for="StateNameEnglish" class="field-wrapper required-field">Discussion<span style="color: red;" >*</span></label>
               (<i>Maximum 250 characters allowed</i>)

               <textarea class="form-control" data-toggle="tooltip" 
               name="discussion" id="discussion" maxlength="250" placeholder="Discussion" required="required" style="min-width: 20%;" ></textarea> 
             </div>
           </div>
         </div>

         <div class="panel-footer text-right"> 
          <!-- <a href="<?php echo site_url('Stafftransfer/view_history/'.$token);?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on view history.">View History</a> --> 
          <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
          <a href="<?php echo site_url("Staff_list");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
        </div>

      </div>
    </form>
  </div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php //} } ?>
</section>

<script type="text/javascript">
  $(document).ready(function(){

   $("#fromdate").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate : 0,
    yearRange: '1980:2025',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });


   $("#effective_date").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate : 0,
    yearRange: '1980:2025',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });




   var staff=$('#staff').val();

   $.ajax({
     url:'<?php echo base_url(); ?>Stafftransfer/presentoffice',
     method:'POST',
     datatype:'json',
     data:{staff:staff},
     success:function(data)
     {

      var obj=JSON.parse(data);
      $('#Presentoffice').html(obj.office_name);
      $('#Presentoffice').val(obj.office_id);
    }

  });

 });
</script>

<script>

 $(document).ready(function(){
  
  var staffid = $("#staff").val(); 
  $("#transfer_office").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/get_Integrator_ED_OnlyteamTC/'+$(this).val() + '/' + staffid,
      type: 'POST',
      dataType: 'text',
    })

    .done(function(data) {    
      $("#reportingto").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

      //   $("#transfer_office").change(function(){

      //     var transfer_office = $("#transfer_office").val();
      //      var staff_id = $("#staff").val();
      //      alert(transfer_office);

      //      $.ajax({
      //     data:{transfer_office:transfer_office,staff_id:staff_id},
      //     url: '<?php //echo site_url(); ?>Ajax/get_Integrator_ED_OnlyteamTC/',
      //     type: 'POST'
      
      //        })
      // .done(function(data) {
      //   $("$reportingto").html(data);
      
      // })

      //   });
    });

  </script>