<section class="content">
  <?php// foreach ($role_permission as $row) { if ($row->Controller == "Permissions" && $row->Action == "index"){ ?>

    <div class="container-fluid">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>


          <?php  //print_r($state_details); ?>
          <!-- Exportable Table -->
          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">

               <div class="header">
                <h2>Manage Staff Transfer Approval</h2><br>
           
              </div>
              <div class="body">
                
                <table id="tbltrnasfer_approval" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 50px;">S.NO</th>
                      <th class="text-center">Staff</th>
                      <th class="text-center">Old Office</th>
                      <th class="text-center">New Office</th>
                      <th class="text-center">Date of transfer</th>
                      <th class="text-center">Reason</th>
                       <th class="text-center">Approve</th>
                        <th class="text-center">Reject</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    
                   // print_r($staffcategory_details);
                  $i=0; foreach($Leave_details as $row){ ?>
                    <tr>
                    <!--   <td class="text-center" style="width: 50px;"><?php// echo $i+1; ?></td>
                      <td class="text-center"><?php //echo $row->from_month; ?></td>
                      <td class="text-center"><?php //echo $row->to_month; ?></td>
                      <td class="text-center"><?php// echo $row->credit_value; ?></td> -->
                               
                      
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>

          


          
        </div>
        <!-- #END# Exportable Table -->
      </div>
    <?php// } } ?>
  </section>
  <script>
   $(document).ready(function() {
    $('#tbltrnasfer_approval').DataTable({
      "paging": false,
      "search": false,
    });
  });
   function confirm_delete() {
    
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
      return true;
    } else {
      return false;
    }
    
  }
</script>