<style type="">
  input[type='text']:disabled
  {
    background: #fff !important;

  }
textarea:disabled
  {
    background: #fff !important;
  }
  textarea{
    width: 600px !important;
  }
 </style>
<form name="iomtransfer" id="iomtransfer" action="" method="POST">
<section class="content" style="background-color: #FFFFFF;" >
 <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-8 panel-title pull-left"> </h4>

       <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
      </div>

    </div>
    <hr class="colorgraph"><br>
  </div>
<?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?> 



     <input type="hidden" name="staffid" id="staffid" value="<?php echo $staffid; ?>">
     <input type="hidden" name="tarnsid" id="tarnsid" value="<?php echo $tarnsid; ?>">
  <div class="panel-body">
    <div class="row"> 
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
        <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
<p style="text-align: center;"><strong>Inter-Office Memo (IOM) for Transfer</strong></p>

<p style="text-align: center;"><em>I</em><em>nter-Office Memo</em></p>
<p style="text-align: center;">&nbsp;</p>
<table style="width: 996px; height: 131px;">
<tbody>
  <tr>
<td style="width: 532px;">
<p>Transfer No.: <input type="text" name="transferno" id="transferno"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" maxlength="15" value="<?php if($iom_transfer_detail)echo $iom_transfer_detail->transferno; ?>" required="required" ></p>
</td>
<td style="width: 448px;"></td>
</tr>
<tr>
<td style="width: 532px;">
<p>To: <input type="text" name="to" id="to"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if($getstaffdetails)echo $getstaffdetails->Staffname;?>" required="required" readonly></p>
</td>
<td style="width: 448px;">
<p>Ref: (from Personal Dossier)
<input type="hidden" name="personnel_id" id="personnel_id"   value="<?php echo$this->loginData->staffid;?>">
 <input type="text" name="ref" id="ref"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php //echo $personnel_name;?>" required="required"></p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>(Name, Designation of Employee being transferred)</p>
</td>
<td style="width: 448px;">
<p>Date: <input type="text" class="datepicker" name="letter_date" id="letter_date"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo date("d/m/Y");?>" required="required" readonly></p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>From:
<input type="hidden" name="reportingto" id="reportingto" value="<?php echo $getstaffdetails->newtc;?>">

 <input type="text" name="from" id="from"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $eddetail->name;?>" required="required" readonly></p>
</td>
<td style="width: 448px;">
<p>Copy: See list below</p>
</td>
</tr>
<tr>
<td style="width: 532px;">
<p>(Integrator/Executive Director)</p>
</td>
<td style="width: 448px;">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Subject: Your Transfer from 
<input type="hidden" name="transfer_from_id" id="transfer_from_id"  value="<?php echo $getstaffdetails->old_office_id;?>">
  <input type="text" name="transfer_from" id="transfer_from"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->oldoffice;?>" required="required" readonly> to 
<input type="hidden" name="transfer_to_id" id="transfer_to_id"  value="<?php echo $getstaffdetails->new_office_id;?>">
  <input type="text" name="transfer_to" id="transfer_to"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->newoffice;?>" required="required" readonly>&nbsp;</p>
<p></p>
<p></p>
<p style="text-align: justify;">I write to inform you that you have been transferred from <strong><input type="text" name="transferred_from" id="transferred_from"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->oldoffice;?>" required="required" readonly></strong> to <strong><input type="text" name="transferred_to" id="transferred_to"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->newoffice;?>" required="required" readonly> .</strong> .</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Please hand over charge of your responsibilities on <input type="text" name="charge_responsibility_on" id="charge_responsibility_on"  style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  class="datepicker" value="<?php if($iom_transfer_detail)echo $this->gmodel->changedatedbformate($iom_transfer_detail->responsibility_on_date); ?>" required="required"> (date), and report for work at
<input type="hidden" name="report_for_work_at_place_id" id="report_for_work_at_place_id"  value="<?php echo $getstaffdetails->new_office_id;?>" readonly>
<input type="text" name="report_for_work_at_place" id="report_for_work_at_place"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->newoffice;?>" required="required"> 
 (place) on 
<input type="text" class="datepicker" name="restransferno" id="restransferno" value="<?php if($iom_transfer_detail)echo $this->gmodel->changedatedbformate($iom_transfer_detail->report_for_work_place_on_date);?>" style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" > (date).</p>
<p style="text-align: justify;"></p>
<p style="text-align: justify;">Before proceeding to your new place of posting, you would need to hand over charge of your current responsibilities to

<SELECT name="current_responsibilities_to" id="current_responsibilities_to" style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required">
  <option value="" >Select Name</option>
  <?php  foreach ($getstafflistold as $key => $val) {
    if($iom_transfer_detail){
    if ($val->staffid == $iom_transfer_detail->current_responsibility_to) { ?>
     <option value="<?php echo $val->staffid; ?>" SELECTED><?php echo $val->name; ?></option>
    <?php } }else{ ?>
  <option value="<?php echo $val->staffid; ?>"><?php echo $val->name; ?></option>
 <?php  } }  ?>
  </SELECT> 

 . Please also ensure that you get a &lsquo;Clearance Certificate&rsquo; from all concerned (as per the enclosed proforma).</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">On arrival at your new place of posting, please hand in your joining report in the prescribed form to&nbsp;
<input type="hidden" name="joining_report_prescribed_form_to" id="joining_report_prescribed_form_to"   value="<?php echo $getstaffdetails->reportingto;?>" required="required">
  <input type="text" name="joining_report_prescribed_form_to_name" id="joining_report_prescribed_form_to_name"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->reportingtoname;?>" required="required" readonly> 
  <!-- <SELECT name="joining_report_prescribed_form_to" id="joining_report_prescribed_form_to" style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required>
  <option value="" >Select Name</option>
  <?php  foreach ($getstafflist as $key => $val) {
    if ($getstaffdetails->newtc == $val->staffid) { ?>
     <option value="<?php echo $val->staffid; ?>" SELECTED><?php echo $val->name; ?></option>
   <?php }else{ ?>
  
 <?php  } } ?>
  </SELECT> --> . Please also ensure that you send a copy to all concerned.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">If necessary, you may take an advance from our
<input type="hidden" name="cash_advance_from_our_id" id="cash_advance_from_our_id"  value="<?php echo $getstaffdetails->new_office_id;?>">
  <input type="text" name="cash_advance_from_our" id="cash_advance_from_our"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->newoffice;?>" required="required" readonly> 

  office to meet the travel expenses to join at the new place of your posting as per PRADAN&rsquo;s Travel Rules.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">You may avail of journey and joining time as per PRADAN's travel rules. You would be eligible to claim reimbursement of fare and local conveyance for yourself and your family on account of travel to your new place of work, actual expenses incurred to transport your vehicle and personal effects there and <input type="text" name="month" id="month"  style="width: 100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($iom_transfer_detail->month))echo $iom_transfer_detail->month;?>" required="required" > month&rsquo;s basic pay as lump sum transfer allowance.&nbsp; Please claim these by filling the enclosed &ldquo;Travel Expenses Bill&rdquo; and submitting it to&nbsp; 
<input type="hidden" name="travel_expenses_bill_submitting_to_id" id="travel_expenses_bill_submitting_to_id"   value="<?php echo $getstaffdetails->reportingto;?>" required="required">
  <input type="text" name="travel_expenses_bill_submitting_to_name" id="travel_expenses_bill_submitting_to_name"  style="width: 300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffdetails->reporting_name;?>" required="required" readonly>.</em></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><b>List of enclosures:</b></p>
  <ol style="text-align: justify;">

  

  <li>Clearance Certificate</li>
  <li>Joining Report</li>
  <li>Travel Expenses Bill</li>
  <li>Handing Over/Taking over Charge</li>
  <?php if($getstaffdetails->desid == 4 || $getstaffdetails->desid == 16){ ?>
    <li><a href="<?php echo site_url('datafiles\letters').'ProcedureforTeamCoordinator.pdf'; ?>">Procedure for transfer of Team Codordinator</a></li>
  <?php } ?>
  </ol>
<p style="text-align: justify;"><strong>&nbsp;</strong></p>
<p style="text-align: justify;">cc: - Person concerned to whom charge is being given.</p>
<p style="text-align: justify;"> - Team Coordinator/Integrator at the old and new places of posting</p>
<p style="text-align: justify;">- Finance-Personnel-MIS Unit</p>

</div>  
</div>

 <div class="panel-footer text-right">
 <?php 
 if(empty($iom_transfer_detail)){ ?>
  <button  type="submit" name="save" id="save" value="SaveDataSend"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me." >Save</button>
  <button  type="button" value="SendDataSave" name="saveandsubmit" id="saveandsubmit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me." onclick="interofficememotransfer();">Save & Submit</button>
<?php }else{ 
  if($iom_transfer_detail->flag == 0){
  ?>
  <button  type="submit" name="save" id="save" value="SaveDataSend"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me." >Save</button>
   <button  type="button"  name="saveandsubmit" id="saveandsubmit" value="SendDataSave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me." onclick="interofficememotransfer();">Save & Submit</button>
<?php } elseif (empty($iom_transfer_detail->filename)) { ?>
 
  <!-- <a href="<?php echo site_url().'Staff_personnel_approval/genrate_transfer_letter/'.$iom_transfer_detail->id ?>"  value="SendDataSave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to genrate pdf letter? Click on me.">Genrate PDF </a> -->
<?php } } ?>
<?php if($this->loginData->RoleID !=2){ ?>
  <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
<?php }else{ ?>
  <a href="<?php echo site_url("Staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
<?php } ?>
</div>
</div>
</div>
</section>
      <!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->

      <!-- <form method="POST" name="acceptance_regination_modal" id="acceptance_regination_modal"> -->
      <div class="modal-content">
        <div class="modal-header">
          <h4>Approval</h4>
          <button type="button" class="close" onclick="hidemodal();">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
          <input type="hidden" name="approve_command" value="" id="approve_command">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes" style="max-width: 400px;" maxlength="250"  > </textarea>
              <?php echo form_error("comments");?>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hidemodal();">Close</button>
          <input type="button" name="saveandsubmit" id="submit2"value="Submit" class="btn btn-success btn-sm" onclick="iommodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>

<script>
 
  $(document).ready(function(){

   $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate : 0,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });
  
 });

 function interofficememotransfer(){
 
var transferno = document.getElementById('transferno').value;
var ref = document.getElementById('ref').value;
var from = document.getElementById('from').value;
var current_responsibilities_to = document.getElementById('current_responsibilities_to').value;
var charge_responsibility_on = document.getElementById('charge_responsibility_on').value;
var restransferno = document.getElementById('restransferno').value;
var charge_responsibility_on = document.getElementById('charge_responsibility_on').value;
var joining_report_prescribed_form_to = document.getElementById('joining_report_prescribed_form_to').value;

   
    if(joining_report_prescribed_form_to == ''){
      $('#joining_report_prescribed_form_to').focus();
    }
    if(current_responsibilities_to == ''){
      $('#current_responsibilities_to').focus();
    }
    if(restransferno == ''){
      $('#restransferno').focus();
    }
    if(charge_responsibility_on == ''){
      $('#charge_responsibility_on').focus();
    }

     if(from == ''){
      $('#from').focus();
    }
    // if(ref == ''){
    //   $('#ref').focus();
    // }
     if(transferno == ''){
      $('#transferno').focus();
    }

   if(transferno !='' &&  from !=''  && charge_responsibility_on !='' && restransferno !='' && current_responsibilities_to !='' && joining_report_prescribed_form_to !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }

function iommodal(){
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }else{
      // alert('hgdkjgsdh');
      $("#approve_command").val("approve");
      document.forms['iomtransfer'].submit();
     }
  }
 


 

</script>
<?php
  if($iom_transfer_detail){
  if($iom_transfer_detail->flag == 1){
 ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  });
</script>
<?php }} ?> 