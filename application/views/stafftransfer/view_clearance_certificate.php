
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
          <h4 class="col-md-8 panel-title pull-left"></h4>
         <div class="col-md-4 text-right" style="color: red">
          * Denotes Required Field 
        </div>
      </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');
      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel"> 
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
            <?php //echo "<pre>"; print_r($acceptedcandidatedetails); ?>
            <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >

              <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
              <p style="text-align: center;"><strong>Clearance Certificate on Transfer</strong></p>
              <p style="text-align: center;">&nbsp;</p>
              <div class="row">
                <div class="col-xs-12">
                 <table width="600">
                    <tbody>
                      <tr>
                        <td width="200">
                          <p><b>Name : </b></p>
                        </td>
                        <td width="400">
                           <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $getstafflist->name; ?></label>
                        </td>
                      </tr>
                      <tr>
                        <td width="200">
                          <p><b>Employee Code :</b></p>
                        </td>
                        <td width="400">
                           <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $getstafflist->emp_code; ?></label>
                        </td>
                      </tr>
                      <tr>
                        <td width="200">
                          <p><b>Designation :</b></p>
                        </td>
                        <td width="400">
                          <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $getstafflist->desname; ?></label>
                      </tr>
                      <!-- <tr>
                        <td width="200">
                          <p><b>Location :</b></p>
                        </td>
                        <td width="400">
                          <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         </label>
                        </td>
                      </tr> -->

                      <tr>
                        <td width="200">
                          <p><b>Transferred from :</b></p>
                        </td>

                        <td width="400">
                          <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $getstafflist->officename; ?></label>
                        </td>
                      </tr>
                      <tr>
                        <td width="200">
                          <p><b>Transferred to :</b></p>
                        </td>

                        <td width="400">
                           <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $getstafflist->newoffice; ?></label>
                        </td>
                      </tr>
                      <tr>
                        <td width="200">
                          <p><b>Date of Release :</b></p>
                        </td>

                        <td width="400">
                          <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $this->gmodel->changedatedbformate($getstafflist->responsibility_on_date); ?></label>
                        </td>
                      </tr>
                      <tr>
                        <td width="200">
                          <p><b>Letter of Transfer No. :</b></p>
                        </td>

                        <td width="400">
                          <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo $getstafflist->transferno; ?></label>
                        </td>
                          <tr>
                        <td width="200">
                          <p><b>Letter of Transfer Date :</b></p>
                        </td>

                        <td width="400">
                           <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                         <?php  echo  $this->gmodel->changedatedbformate($getstafflist->letter_date); ?></label>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <p>&nbsp;</p>

              <p><strong>No Dues Certificate</strong></p>
              <p>&nbsp;</p>
              <p>This is to certify that  
                <label  style="width: 400px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
                            <?php echo $getstaffcertify_that->certify_that; ?>
                          </label> 
                has cleared all her/his dues as on the date of her/his release, <em>except </em>the items shown in the respective columns. S/he may be released from duty after adjusting the cost of items/articles outstanding against her/him and upon her/his completing the other requisite formalities:</p>
              <p>&nbsp;</p>


              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

               <table id="tblForm10" class="table table-bordered table-striped">
                <thead>
                 <tr class="bg-light">
                  <th colspan="4"> Items

                    <div class="col-lg-6 text-right pull-right">
                    <!--   <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                      <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button> -->
                    </div>
                  </th>
                </tr> 
                <tr>
                 <th class="text-center" style="vertical-align: top;">Name of Location </th>
                 <th class="text-center" style="vertical-align: top;">Description of outstanding Items/Articles and Their Value, Wherever Known.&nbsp; In Other Cases, Please Give Details</th>
                 <th class="text-center" style="vertical-align: top;">Value 
                  (if known)
                </th>
              </tr> 
            </thead>
              <?php if ($getcountstaffitemslist->cccount >0) {
                $i=0;
              foreach ($getstaffitemslist as $key => $value) {  ?>
            <tbody id="bodytblForm10">
              <tr id="bodytblForm10">
                <td> 
                 
                       <?php echo $value->location;?>     
                 
                </td>

                  <td>
                     
                           <?php echo $value->description;?> 
                    
                  </td>
                    <td> 
                       
                           <?php echo $value->item_values;?> 
                     
                    </td>
                      </tr>
                    </tbody>
                      <?php $i++; } } else{?>
                          <tbody id="bodytblForm10">
              <tr id="bodytblForm10">
                <td colspan="3"> 
                
                      Record Not Found !!!   
                </td>

                
                      </tr>
                    </tbody>
                  <?php } ?>
                  </table>
                </div>
               <p>&nbsp;</p>
                <p><strong>Note</strong>: Where the value of the item missing/damaged, etc., is not known, the Executive Director will decide the amount.</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>cc: - Personal Dossier (PD)</p>
                <p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;- Finance-Personnel-MIS Unit</p>


               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer text-right">
                <?php if($this->loginData->RoleID == 2){ ?>
                  <a href="<?php echo site_url().'Staff_approval/'; ?>" class="btn btn-dark" name="btncancel" id="btncancel"data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
                <?php }else{ ?>
                  <a href="<?php echo site_url().'Staff_personnel_approval/'; ?>" class="btn btn-dark" name="btncancel" id="btncancel"data-toggle="tooltip" title="Click here to move on list.">Go to List</a>

                <?php } ?>
                </div>

              </form>
            </div>
          </div>
        </div>   
      </section>

      <script>
        $(document).ready(function(){
         $(".datepicker").datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '1980:2030',
          dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });
         $('[data-toggle="tooltip"]').tooltip();  
         $('#Inboxtable').DataTable(); 
       });


        $("#btntrainingexposureRemoveRow").click(function() {
          if($('#tblForm10 tr').length-2>1)
            $('#bodytblForm10 tr:last').remove()
        });

        $('#btntrainingexposureAddRow').click(function() {

          rowsEnter1 = parseInt(1);
          if (rowsEnter1 < 1) {
            alert("Row number must be minimum 1.")
            return;
          }
          Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });

        var srNoGlobal=0;
        var inctg = 0;

        function Insaettrainingexposure(count) {
          srNoGlobal = $('#bodytblForm10 tr').length+1;
          var tbody = $('#bodytblForm10');
          var lastRow = $('#bodytblForm10 tr:last');
          var cloneRow = null;

          for (i = 1; i <= count; i++) {
            inctg++
            cloneRow = lastRow.clone();
            var tableData1 = '<tr>'
            + '<td> <input type="text"  class="form-control alphabateonly" data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="name_of_location" name="name_of_location['+inctg+']" placeholder="Enter name of location " style="min-width: 20%;" value="" data-original-title="Name of Location !">'
            + '</td>'
            + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="description['+inctg+']" name="description['+inctg+']" placeholder="Enter Description" style="min-width: 20%;" value="" required="required" data-original-title="Items Description !"></td>'


            + '<td class="focused"> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" minlength="5" maxlength="150" title="" id="value['+inctg+']" name="value['+inctg+']" placeholder="Enter value" style="min-width: 20%;" value="" required="required" data-original-title="Value !"></td>'

            + '</tr>';
            $("#bodytblForm10").append(tableData1)

          }

        }

      </script>  


