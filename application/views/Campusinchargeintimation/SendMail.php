 <section class="content" style="background-color: #FFFFFF;" >
   <?php foreach ($role_permission as $row) { if ($row->Controller == "Campusinchargeintimation" && $row->Action == "SendMail"){ ?>
   <br>
   <div class="container-fluid" style="margin-top: 20px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row clearfix doctoradvice">

        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-8 panel-title pull-left">Campus Incharge Intemation</h4>
             <div class="col-md-4 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
        <div class="panel-body">
         <?php 
         $tr_msg= $this->session->flashdata('tr_msg');
         $er_msg= $this->session->flashdata('er_msg');
         if(!empty($tr_msg)){ ?>
         <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <form name="formIntemation" id="formIntemation" action="" method="POST">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                    <label for="Name">Campus Name <span style="color: red;" >*</span></label>
                    <?php 

                    $options = array('' => 'Select Campus');
                    foreach($campusdetails as $key => $value) {
                      $options[$value->campusid] = $value->campusname;
                    }
                    echo form_dropdown('campusid', $options, $campusinchargedet[0]->campusid, 'class="form-control" id="campusid" required="required"');
                    ?>
                    <?php echo form_error("campusid");?>
                  </div>

                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                   <label for="Name">Campus Incharge Name <span style="color: red;" >*</span></label>
                   <span id="campusincharge">
                     <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Campus Incharge Name" name="campusinchargename" id="campusinchargename" class="form-control" value="<?php echo $campusinchargedet[0]->campusincharge;?>" placeholder="Enter First Name"  required="required">
                     <?php echo form_error("campusinchargename");?>
                   </span>
                 </div>
                 <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display: none;"> 
                 <br>
                 <label for="Name">Category Name <span style="color: red;" >*</span></label>
                 <?php 
                 $options = array('' => 'Select Category');
                 $selected = 1;
                 foreach($categorydetails as$key => $value) {
                  $options[$value->id] = $value->categoryname;
                }
                echo form_dropdown('categoryid', $options, $selected, 'class="form-control" id="categoryid" required="required" ');
                ?>
                <?php echo form_error("categoryid");?>
              </div> -->

                 <?php 

                 $schedule_fromdate = $campusinchargedet[0]->fromdate; 
        //$schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);

                 $explodefromdate = explode('-', $schedule_fromdate);
       // print_r($explodefromdate);
                 $schedule_fromdate_display = $explodefromdate[2].'-'.$explodefromdate[1].'-'.$explodefromdate[0]; 

                 $schedule_todate = $campusinchargedet[0]->todate;

                 $explodetodate = explode('-', $schedule_todate);
                 $schedule_todate_display = $explodetodate[2].'-'.$explodetodate[1].'-'.$explodetodate[0];


                 ?>

                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                  <br>
                  <label for="Name">Schedule From Date <span style="color: red;" >*</span></label>
                  <input type="text" data-toggle="tooltip" title="Schedule From Date" name="schedule_fromdate" id="schedule_fromdate" class="form-control datepicker" value="<?php echo $schedule_fromdate_display;?>" placeholder="Schedule From Date"  required="required">
                  <?php echo form_error("schedule_fromdate");?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                 <br>
                 <label for="Name">To <span style="color: red;" >*</span></label>
                 <input type="text" data-toggle="tooltip" title="Schedule To Date" name="schedule_todate" id="schedule_todate" class="form-control datepicker" value="<?php echo $schedule_todate_display;?>" placeholder="Schedule To Date"  required="required">
                 <?php echo form_error("schedule_todate");?>
               </div>

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <br>
                <label for="Name">Message <span style="color: red;" >*</span></label>

                <textarea  name="message" id="message" class="form-control" cols="10" rows="10" required="required"><?php echo $campusinchargedet[0]->messsage;?></textarea>
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <br>
               <br>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">

                <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-warning btn-sm m-t-10 waves-effect"> Save & Send </button>
                <a href="<?php echo site_url("/Campusinchargeintimation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to list</a>
              </div>  
            </div>
            
          </div>
        </div>
      </form>
    </div>
  </div></div></div></div>

  <?php } } ?>  
</section>

<script type="text/javascript">
 $(document).ready(function(){

   adddatepicker();

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: today,
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
   });


   $("#campusid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getCampusInchargeName/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })

    .done(function(data) {
      console.log(data);

      $("#campusincharge").html(data);
      $("#campusintimationid").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });




   $("#schedule_todate").change(function(){
    var campusid = $('#campusid').val();
    var schedule_fromdate = $('#schedule_fromdate').val();
    var schedule_todate = $('#schedule_todate').val();
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getCampusIntimationToDateDetail/'+ campusid +'_'+ schedule_fromdate+'_'+ schedule_todate,
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#message").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
 });     



</script>

<script type="text/javascript">
  function adddatepicker(){
   $("#schedule_fromdate").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: today,
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
     onClose: function(selectedDate) {
      jQuery("#schedule_todate").datepicker( "option", "minDate", selectedDate);
    }
  });

   $("#schedule_todate").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: today,
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
     onClose: function(selectedDate) {
      jQuery("#schedule_fromdate").datepicker( "option", "maxDate", selectedDate);
    }
  });

 }
</script>