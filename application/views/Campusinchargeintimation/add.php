  <section class="content" style="background-color: #FFFFFF;" >
  <br>
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Campusinchargeintimation" && $row->Action == "add"){ ?>
  <div class="container-fluid">
    <form name="formIntemation" id="formIntemation" action="" method="POST">
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-8 panel-title pull-left">Campus Incharge Intimation</h4>
           <div class="col-md-4 text-right" style="color: red">
            * Denotes Required Field 
          </div>
        </div>
        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body">
       <?php 
       $tr_msg= $this->session->flashdata('tr_msg');
       $er_msg= $this->session->flashdata('er_msg');
       if(!empty($tr_msg)){ ?>
       <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>


            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                 <br>
                 <label for="Name">Campus Name <span style="color: red;" >*</span></label>
                 <?php 
                 $options = array('' => 'Select Campus');
                 foreach($campusdetails as$key => $value) {
                  $options[$value->campusid] = $value->campusname;
                }
                echo form_dropdown('campusid', $options, set_value('campusid'), 'class="form-control" id="campusid" required="required"');
                ?>
                <?php echo form_error("campusid");?>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <br>
               <label for="Name">Campus Incharge Name <span style="color: red;" >*</span></label>
               <span id="campusincharge">
                 <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Campus Incharge Name" name="campusinchargename" id="campusinchargename" class="form-control" value="<?php echo set_value("campusinchargename");?>" placeholder="Enter First Name"  required="required" readonly="readonly">
                 <?php echo form_error("campusinchargename");?>
               </span>
             </div>

             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                 <br>
                 <label for="Name">Category Name <span style="color: red;" >*</span></label>
                 <?php 
                 $options = array('' => 'Select Category');
                $selected = 1;
                 foreach($categorydetails as$key => $value) {
                  $options[$value->id] = $value->categoryname;
                }
                echo form_dropdown('categoryid', $options, $selected, 'class="form-control" id="categoryid" required="required" ');
                ?>
                <?php echo form_error("categoryid");?>
              </div>

             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
              <br>
              <label for="Name">Schedule From Date <span style="color: red;" >*</span></label>
              <input type="text" data-toggle="tooltip" title="Schedule From Date" name="schedule_fromdate" id="schedule_fromdate" class="form-control datepicker" required value="<?php echo set_value("fromdate");?>" placeholder="Schedule From Date" onclick="adddatepicker()">
              <?php echo form_error("schedule_fromdate");?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
             <br>
             <label for="Name">To Date <span style="color: red;" >*</span></label>
             <input type="text" data-toggle="tooltip" title="Schedule To Date" name="schedule_todate" id="schedule_todate" required class="form-control datepicker" onclick="adddatepicker()" value="<?php echo set_value("todate");?>" placeholder="Schedule To Date">
             <?php echo form_error("schedule_todate");?>
           </div>

           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br>
            <label for="Name">Message <span style="color: red;" >*</span></label>
            <textarea  name="message" minlength="4" maxlength="500" id="message" class="form-control" cols="10" rows="10" required="required"></textarea>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <br>
           <br>
           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
           
         </div>

       </div>
     </div>
     
   </div>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="panel-footer text-right">

    <button type="submit" name="intemationssave" id="intemationsave" value="save" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
    <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-warning btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save changes and send intimation to Campus Incharge? Click on me.">Save & Send </button>
    <a href="<?php echo site_url("/Campusinchargeintimation/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to list</a> 
  </div>  
</div>
</div>
</form>
</div>
</div>   
<?php } } ?>  
</section>

<script type="text/javascript">
 $(document).ready(function(){

   adddatepicker();

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'todate',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
   });


   $("#campusid").change(function(){
    var campusid = $("#campusid").val(); 
   
    if (campusid == 0)
    {
      $("#categoryid option[value=2]").prop('disabled',false);
       $("#categoryid option[value=3]").prop('disabled',false);
    }
    else

    {
      
       $("#categoryid option[value=2]").prop('disabled',true);
       $("#categoryid option[value=3]").prop('disabled',true);

    }
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getCampusInchargeName/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })

    .done(function(data) {
      console.log(data);

      $("#campusincharge").html(data);
      $("#campusintimationid").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });



  });




   $("#schedule_todate").change(function(){
    var campusid = $('#campusid').val();
    var categoryid = $('#categoryid').val();
    var schedule_fromdate = $('#schedule_fromdate').val();
    var schedule_todate = $('#schedule_todate').val();

    if (campusid=='') {
      alert('Select Campus Name !!!');
      $('#campusid').focus();
      return false;
    }
     if (categoryid =='') {
      alert('Select Category Name !!!');
      $('#categoryid').focus();
      return false;
    }
     if (schedule_fromdate =='') {
      alert('Select From Date !!!');
      $('#schedule_fromdate').focus();
      return false;
    }
     if (schedule_todate =='') {
       alert('Select To Date!!!');
      $('#schedule_todate').focus();
       $('#schedule_todate').val("");
      return false;
    }

    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getCampusIntimationToDateDetail/'+ campusid +'_'+ schedule_fromdate+'_'+ schedule_todate+'_'+ categoryid,
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#message").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
$("#schedule_fromdate").change(function(){
    var campusid = $('#campusid').val();
    var categoryid = $('#categoryid').val();
    var schedule_fromdate = $('#schedule_fromdate').val();
    var schedule_todate = $('#schedule_todate').val();

    if (campusid=='') {
      alert('Select Campus Name !!!');
      $('#campusid').focus();
      return false;
    }
     if (categoryid =='') {
      alert('Select Category Name !!!');
      $('#categoryid').focus();
      return false;
    }
     if (schedule_fromdate =='') {
      alert('Select From Date !!!');
      $('#schedule_fromdate').focus();
      return false;
    }
     if (schedule_todate =='') {
       
      return false;
    }

    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getCampusIntimationToDateDetail/'+ campusid +'_'+ schedule_fromdate+'_'+ schedule_todate+'_'+ categoryid,
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#message").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

 });     



</script>

<script type="text/javascript">
  function adddatepicker(){
   $("#schedule_fromdate").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
     onClose: function(selectedDate) {
      jQuery("#schedule_todate").datepicker( "option", "minDate", selectedDate);
    }
  });

   $("#schedule_todate").datepicker({
     changeMonth: true,
     changeYear: true,
     minDate: 'today',
     yearRange: '1920:2030',
     dateFormat : 'dd-mm-yy',
     onClose: function(selectedDate) {
      jQuery("#schedule_fromdate").datepicker( "option", "maxDate", selectedDate);
    }
  });

 }
</script>







