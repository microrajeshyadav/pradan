 
<section class="content" style="background-color: #FFFFFF;" >
  <?php 
  foreach ($role_permission as $row) { if ($row->Controller == "Campusinchargeintimation" && $row->Action == "index"){ 

    ?>
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-9 panel-title pull-left">Campus Incharge Intimation</h4>
         <div class="col-md-3 text-right">
           <a  data-toggle="tooltip" data-placement="bottom" title="Want to add new batch ? Click on me." href="<?php echo site_url()."Campusinchargeintimation/add";?>" class="btn btn-sm btn-primary">Add Campus Incharge Intimation</a>
         </div>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
       <?php 
       $tr_msg= $this->session->flashdata('tr_msg');
       $er_msg= $this->session->flashdata('er_msg');
       if(!empty($tr_msg)){ ?>
       <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tablecampus" class="table table-bordered table-striped dt-responsive">
                  <thead>
                   <tr>
                    <th class="text-center" style="width: 50px;">S.No.</th>
                    <th class="text-center" style="width: 50px;">Category Name</th>
                    <th class="text-center" style="width: 50px;">Campus Name</th>
                    <th class="text-center">Email Id</th>
                    <th class="text-center">Schedual From Date</th>
                    <th class="text-center">Schedual To Date</th>
                    <th class="text-center">Campus Incharge Name </th>
                    <th class="text-center">Campus status </th>
                    <th class="text-center">Message </th>
                    <th class="text-center">Action</th>
                  </tr> 
                </thead>
                <tbody>
                  <?php $i=0; foreach ($campusinchargedetails as $key => $value) { ?>
                  <tr>
                   <td class="text-center"><?php echo $i+1;?></td>
                   <td><?php echo $value->categoryname;?></td>
                   <td><?php echo $value->campusname;?></td>
                   <td><?php echo $value->emailid;?></td>
                   <td><?php echo $this->model->changedatedbformate($value->fromdate);?></td>
                   <td><?php echo $this->model->changedatedbformate($value->todate);?></td>
                   <td><?php echo $value->campusincharge;?></td>
                   <td>
                    <?php if ($value->status== 0) { ?>
                     <div class="badge badge-pill badge-success text-center">Running</div>
                   <?php }else{ ?>
                     <div class="badge badge-pill badge-success text-center">Closed</div>
                    <?php } ?> 
                   <td>
                    <?php if ($value->status== 0) { ?>
                      <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal_<?php echo $key;?>">Message</button>
                    <?php }else{ ?>
                      <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#CampusClosedModel">Message</button>
                    <?php } ?>
                  </td>
                  <?php if ($value->mailstatus==0) {
                   ?>
                   <td> <a href="<?php echo site_url().'Campusinchargeintimation/SendMail/'.$value->id;?>" class="btn btn-xs btn-primary">  Send </a></td>
                   <?php } else{ ?>
                   <td class="text-center"><i title ="Mesage Sent" class="fa fa-envelope" aria-hidden="true" style="color:green"></i>  <?php if ($value->status==0) { ?> | <a title = "Close" href="<?php echo site_url().'Campusinchargeintimation/delete/'.$value->id;?>" onclick="return confirm_delete();"  ><i class="fa fa-window-close" style="color:red"></i> </a></td>
                   <?php }} ?>
                 </tr>
                 <div id="myModal_<?php echo $key;?>" class="modal fade" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg" role="document">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">

                        <h5 class="modal-title pull-left">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <p><?php echo str_replace($value->cgh, site_url(),  $value->messsage);?></p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>
                <?php $i++; } ?>

                 <div id="CampusClosedModel" class="modal fade" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg" role="document">
                    <!-- Modal content-->
                    <div class="modal-content col-md-6">
                      <div class="modal-header">

                        <h5 class="modal-title pull-left">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <p>Campus Already Closed</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>
              </tbody>
            </table>
          </div>
        </div> 

      </div>
    </div>
  </div>  
  <?php } } ?> 
</section>

<script>
  $(document).ready(function(){
    $('#tablecampus').DataTable(); 
    $("#b1").hover(function () {
      $('#modal1').modal({
        show: true,
        backdrop: false
      })
    });
    // $('[data-toggle="tooltip"]').tooltip();  
    $('[data-toggle="popover"]').popover({
     html: true, 
     content: function() {
      return $('#popover-content').html();
    }
  });   
    
  });
</script>  
<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {
    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>



  function confirm_delete() {
    var r = confirm("Are you sure you want to close this Campus intimation?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>