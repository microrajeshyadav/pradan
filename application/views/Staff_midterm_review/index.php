<section class="content">
	
	<br>
	<div class="container-fluid">
		<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');

		if(!empty($tr_msg)){ ?>
		<div class="content animate-panel">
			<div class="row">
				<div class="col-md-12">
					<div class="hpanel">
						<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('tr_msg');?>. </div>
						</div>
					</div>
				</div>
			</div>
			<?php } else if(!empty($er_msg)){?>
			<div class="content animate-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="hpanel">
							<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('er_msg');?>. </div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>



				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel thumbnail shadow-depth-2 listcontainer" >
							<div class="panel-heading">
								<div class="row">
									<h4 class="col-md-10 panel-title pull-left"> Mid-term  Review  Initiate</h4>

								</div>
								<hr class="colorgraph"><br>
							</div>
							<div class="panel-body">
								<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
									<thead>
										<tr>
											<th class="text-center" style="width: 45px;">#</th>
											<th>Employ Code</th>
											<th>Name</th>
											<th>Designation </th>
											<th>Level </th> 
											<th>Gender</th>
											<th>Office </th>
											<th>Probation Date</th>
											<th>Status </th>
											<th>Initiate</th>
										</tr>
									</thead>
									<tbody>
										<?php
										  // echo "<pre>";
									 	 //  print_r($hrd_staff_details);
									 	 //  die;
										$i=0; foreach($hrd_staff_details as $row){ 

											 ?>
											
											<tr>
												<td class="text-center"><?php echo $i+1; ?></td>
												<td ><?php echo $row->emp_code; ?></td>
												<td ><?php echo $row->name; ?></td>
												<td ><?php echo $row->desname; ?></td>
												<td ><?php echo $row->level; ?></td>
												<td ><?php echo $row->gender; ?></td>
												<td ><?php echo $row->officename; ?></td>
												<td ><?php echo $this->gmodel->changedatedbformate($row->probation_date); ?></td>
												<td class="text-center">
<?php
 if($row->trans_flag==0||$row->trans_flag==1||$row->trans_flag==3||$row->trans_flag==5||$row->trans_flag==7||$row->trans_flag==9) {
           echo  "<span class='badge badge-pill badge-info'>".$row->flag."</span>"; }
?>


													</td>
												<td style="text-align: center;">
													<?php
													 if($row->level !=0 && $row->probation_status==0) {?>
													<a title = "Click here to approvle this request submitted." 
													class="btn btn-primary btn-xs"
													href="<?php echo site_url()."Staff_midterm_review/add/".$row->staffid.'/'.$row->candidateid;?>"
													id="">Initiate</a> 
													<?php } else {?>
													<div class="badge badge-pill badge-info">
													  <strong>Initiated </strong>
													</div>
													<?php } ?>





												</td>
												


											</tr>
											<?php $i++; } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel thumbnail shadow-depth-2 listcontainer" >
							<div class="panel-heading">
								<div class="row">
									<h4 class="col-md-10 panel-title pull-left"> Mid-term  Review  </h4>

								</div>
								<hr class="colorgraph"><br>
							</div>
							<div class="panel-body">
								<table id="tbldesignations1" class="table table-bordered table-striped table-hover dataTable js-exportable">
									<thead>
										<tr>
											<th class="text-center" style="width: 45px;">#</th>
											<th>Employ Code</th>
											<th>Name</th>
											<th>Designation </th>
											<th>Level </th> 
											<th>Gender</th>
											<th>Office </th>
											<th>Probation Date</th>
											<th>Status </th>
											<th>Initiate</th>
										</tr>
									</thead>
									<tbody>
										<?php
										 		
										foreach($hrd_staff_details as $row){ 

											if ($row->trans_flag>=1) { 
												$i=0;
												?>

												<tr>
													<td class="text-center"><?php echo $i+1; ?></td>
													<td ><?php echo $row->emp_code; ?></td>
													<td ><?php echo $row->name; ?></td>
													<td ><?php echo $row->desname; ?></td>
													<td ><?php echo $row->level; ?></td>
													<td ><?php echo $row->gender; ?></td>
													<td ><?php echo $row->officename; ?></td>
													<td ><?php echo $this->gmodel->changedatedbformate($row->probation_date); ?></td>
													<td class="text-center">
														 <?php if($row->trans_flag==1||$row->trans_flag==3||$row->trans_flag==5||$row->trans_flag==7||$row->trans_flag==9) {
           echo  "<span class='badge badge-pill badge-info'>".$row->flag."</span>"; }
           else if($row->trans_flag==4||$row->trans_flag==6||$row->trans_flag==8) {
           echo  "<span class='badge badge-pill badge-danger'>".$row->flag."</span>"; }
           ?>

														</td>
													<td style="text-align: center;">
														<?php 

																
														if($row->trans_flag==7 ||$row->trans_flag==8)  { 
															?>





															
															
															<a title = "Click here to approvle this request submtted." 
														class="btn btn-primary btn-xs" 
														href="<?php echo site_url()."Probation_ed_reviewofperformance/demoview/".$row->staffid.'/'.$row->id;?>" >Review Foam</a>
														<?php } else {?>

															
															<?php } ?>

																


														
													</td>

												</tr>
												<?php  $i++;} } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					
					</div>
  
 


				</section>
				<script>
					$(document).ready(function() {
						$('[data-toggle="tooltip"]').tooltip(); 
						$('#tbldesignations').DataTable({
							"paging": true,
							"search": true,
						});

						$('#tbldesignations1').DataTable({
							"paging": true,
							"search": true,
						});
					});



					function confirm_delete() {

						var r = confirm("Do you want to delete this Designations");

						if (r == true) 
						{
							return true;
						}
						 else {
							return false;
						}

					}
				</script>]