<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <form method="post" action="" name="form_clearance" id="form_clearance">
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-12 panel-title pull-left">
            Monthly input
          </h4>
        </div>
         <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body align-top" style="line-height: 4;">

         <div class="row">
          <div class="col-md-2 align-text-top panel-title">
            <h5>
              <input type="hidden" name="monthyear" value="<?php  echo date('M-Y');?>">
            <?php  echo date('M-Y'); ?> 
          </h5>
          </div>
          <div class="col-md-3">
          
          </div>          
        </div>

        <div class="row">
          <div class="col-md-2 align-text-top">
            Select Head <span style="color: red;">*</span>:
          </div>
          <div class="col-md-3">
           <select id="ddlhead" name="ddlhead" class="form-control" required="required">
            <option value="">Select salary heads</option>
            <?php foreach ($getsalaryhead as $key => $value) {?>
               <option value="<?php echo $value->fieldname;?>"><?php echo $value->fielddesc;?></option>
            <?php } ?>
           </select>
          </div>          
        </div>
         <div class="row">
          <div class="col-md-2 align-text-top">
           Office <span style="color: red;">*</span>:
          </div>
          <div class="col-md-3">
           <select id="ddloffice" name="ddloffice" class="form-control" required="required">
             <option value="">Select office</option>
             <?php foreach ($getofficedest as $key => $val) {?>
               <option value="<?php echo $val->officeid;?>"><?php echo $val->officename;?></option>
            <?php } ?>
           </select>
          </div>          
        </div>
         <div class="row">
          <div class="col-md-2 align-text-top">
           Employee <span style="color: red;">*</span>:
          </div>
          <div class="col-md-3">
           <select id="ddlemployee" name="ddlemployee" class="form-control" required="required">
            <option>Select</option>
           </select>
          </div>          
        </div>

        <div class="row">
          <div class="col-md-2 align-text-top">
           Enter Amount <span style="color: red;">*</span>:
          </div>
          <div class="col-md-3">
            <div id="displytxtamount">
           <input type="text" onblur="intTo(this)" name="txtamount" id="txtamount" class="form-control isNumberKey text-right" maxlength="10" required="required">
         </div>
          </div>          
        </div>

      </div>
      <div class="panel-footer text-right">
        <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
        <a href="" class="btn btn-dark btn-sm"> Go Back</a>
      </div>
    </form>
  </div>
</div>
</section>
<script type="text/javascript">

  //input to comma separated with decimal
  function intTo(nStr) {
    var cid='#'+nStr.id;
    var x1 = $(cid).val(intToNumberBudget(nStr.value));
  }

  $( document ).ready(function(){

    $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
       return false;
     }
   });

   $("#ddloffice").change(function(){
    var ddloffice = $('#ddloffice').val();
   
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getOfficewisestaff/'+ ddloffice ,
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
      console.log(data);
      $("#ddlemployee").html(data);
     
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


   $("#ddlemployee").change(function(){
    var ddloffice    = $('#ddloffice').val();
    var ddlemployee  = $('#ddlemployee').val();

    if (ddloffice=='') {
       alert("Please Select Office!!!");
      return false;
    }

    if (ddlemployee=='') {
       alert("Please Select staff !!!");
      return false;
    }
   
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getOfficewisestaffdetails/',
      type: 'POST',
      data : {'ddloffice':ddloffice,'ddlemployee':ddlemployee},
    })
    .done(function(data) {
      console.log(data);
      $("#displytxtamount").html(data);
     
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });


 });
</script>