<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Pradan</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap-4.1.3-dist/css/bootstrap.css');?>" rel="stylesheet">

    <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />


    <!-- Waves Effect Css -->
    <link href="<?php echo site_url('common/backend/vendor/node-waves/waves.css');?>" rel="stylesheet" />
    <link href="<?php echo site_url('common/backend/assets/fontawesome-free-5.6.3-web/css/all.css');?>" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />
</head>

<body class="bg-dark">
    <section class="testimonial py-5" id="testimonial">
        <div class="container">
            <div class="row">
              <div class="col-md-4 py-5 text-white text-center " style="background-color: #17A2B8">
                <div class=" ">
                    <div class="card-body">
                        <img style="height: 100px; width: 300px;" src="<?php echo site_url('common/backend/images/pradan_logo1.png');?>"  alt="logo" class="text-center" /> 


                        <h2 class="py-3">Sign-In</h2>
                        <p>Welcome to PRADAN HRMS.

                        </p>
                        <p class="text-left">


                            Registered Office: #3, Community Shopping Centre, Niti Bagh, New Delhi - 110049 | A-22, Second Floor, Sector 3, Noida: 201301
                        </p>
                        <p class="text-left">
                            <i class="fa fa-phone" aria-hidden="true"></i> 0120-4800800 <br>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            headoffice@pradan.net

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-8 py-5 pl-5 border" style="background-color: white;">

                <div class="logo text-center">

                </div>
                <form id="sign_in" action="<?php echo site_url().'login/' ?>" method="POST">

                    <?php 
                    $er_msg = $this->session->flashdata('er_msg'); ?>
                    <?php 
                    if ($er_msg != NULL) { ?>
                    <div class="msg"><?php echo $er_msg; ?></div>
                    <?php }else{ ?>
                    <h4 class="pb-4">Please fill with your details</h4>
                    <?php } ?>
                    <div class="form-row">
                       <div class="form-group">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-user text-info"></i></div>
                            </div>
                            <input type="text" class="form-control" name="username" placeholder="User Name" value="<?php echo get_cookie('username');?>" required autofocus>
                        </div>
                    </div>


                </div>
                <div class="form-row">
                    <div class="form-group">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-key text-info"></i></div>
                            </div>
                            <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo get_cookie('password');?>" required>
                        </div>
                    </div>


                </div>
                <?php 
                $sql = "SELECT * FROM `sysaccesslevel` WHERE  Acclevel_Cd NOT IN (5,6,15,25) ORDER BY `Acclevel_Name`";
                $getrolelist = $this->db->query($sql)->result();
                ?>
                <div class="form-row">
                   <div class="form-group">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-id-badge" aria-hidden="true"></i>
</div>
                        </div>

                        <select style="max-width:230px;" name="roleid" id="roleid" required="required" class="form-control">
                            <option value="">Select</option>
                            <?php foreach ($getrolelist as $key => $value){ 
                                if ($value->Acclevel_Cd == get_cookie('roleid')) {
                                    ?>
                                    <option value="<?php echo $value->Acclevel_Cd;?>" SELECTED><?php echo $value->Acclevel_Name;?></option>
                                    <?php }else{ ?>
                                    <option value="<?php echo $value->Acclevel_Cd;?>"><?php echo $value->Acclevel_Name;?></option>
                                    <?php } } ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <?php   ?>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="checkbox" name="rememberme" id="rememberme" value="1" class="filled-in chk-col-success" <?php if (isset($checked) && $checked ==1) {
                             echo 'checked="checked"'; } ?> >
                             <label for="rememberme">Remember Me</label>
                         </div>
                     </div>
                     <div class="form-row">

                        <button class="btn btn-success" type="submit">Login</button>
                        <!-- <a class="btn btn-block bg-pink waves-effect" href="<?php echo site_url('Dashboard');?>">SIGN IN</a> -->

                    </div> 
                    <br/>
                    <div class="form-row">
                        <a href="<?php echo site_url("login/forget");?>">Forgot your password? click here</a>
                    </div>
                </form>

            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="hpanel">
            <div class="alert alert-danger fade in alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/bootstrap/js/bootstrap.js');?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo site_url('common/backend/vendor/node-waves/waves.js');?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery-validation/jquery.validate.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo site_url('common/backend/js/admin.js');?>"></script>
    <script src="<?php echo site_url('common/backend/js/pages/examples/sign-in.js');?>"></script>
</body>

</html>