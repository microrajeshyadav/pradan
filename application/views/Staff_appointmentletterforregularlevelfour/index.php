<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
  <form name="appointmentletter" id="appointmentletter" action="" method="POST">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left"> OFFER OF APPOINTMENT FOR REGULAR POSTS  
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center" style="margin-bottom: 50px;"><h5> (LEVELS 4-6)</h5></div>      

       <div class="col-md-6 pull-left"><em>Ref.: 300/ PER/<input type="text" name="ref_no" id="txtname" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"></em></div>
       <div class="col-md-6 pull-right text-right"><em>Date: <input type="text" name="dadate" id="dadate" class="" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y'); ?>" required="required" readonly> </em></div>
     </div>
     <div class="row" style="line-height: 3">
      <div class="col-md-12">
        Name : <input type="text" name="staff_name" id="staff_name" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">
      </div>
      <div class="col-md-12">
        Addres : <input type="text" name="staff_address" id="
        " placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">
      </div>     

    </div>
    <p></p>
    <p></p>
    <p>Subject: <strong>Offer of Appointment as  <?php 

                 $options = array('' => 'Select Designation');
                 foreach($designation as$key => $value)
                  {
                  $options[$value->desid] = $value->desname;
                  }
                echo form_dropdown('designation', $options, set_value('designation'), ' data-toggle="tooltip" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" title="Select Relation With Nominee !"  id="designation" required="required"');
                ?>
                <?php echo form_error("designation");?> (designation)</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        Dear Ms./Mr.,
      </div>     
    </div>
    <div class="row text-left">
      <div class="col-md-12">
        On behalf of PRADAN, I am happy to appoint you as<input type="text" name="name" id="name" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"> (Designation) in PRADAN with effect from<input type="text" name="fromdate" id="fromdate" class="datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">  (d/m/y) in <?php 

                 $options = array('' => 'Select Location');
                 foreach($office as$key => $value)
                  {
                  $options[$value->officeid] = $value->officename;
                  }
                echo form_dropdown('office', $options, set_value('location'), ' data-toggle="tooltip" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" title="Select office location !"  id="office" required="required"');
                ?>
                <?php echo form_error("designation");?>  (location) in <input type="text" name="name_dc" id="name_dc"  placeholder=""  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">          (Name of DC). The offer is valid till<input type="text" name="valid_date" id="valid_date" class="datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">  (d/m/y).
      </div>
    </div>
    <div class="row text-left" style="margin-top: 20px; ">
      <div class="col-md-12">
       Though your place of posting has been mentioned as <input type="text" name="placeofposting" id="placeofposting" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"> it is subject to change due to organizational needs and in such a case you will be intimated within a period of 15 days before your joining date. The terms and conditions of your appointment are given below. Kindly read those carefully and return us a copy of this letter with your signature as a token of your agreement.
     </div>
   </div>
   <div class="row text-left" style="margin-top: 20px; ">
    <div class="col-md-12">
      1.  Please hand over your joining report as per the enclosed format to <input type="text" name="handover_format" id="handover_format" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required"> (name of Supervisor, designation) and send its duplicates to the persons/units copied on this letter.
    </div>
  </div>
  <div class="row text-left" style="margin-top: 20px; ">
    <div class="col-md-12">
     2. You will be paid a basic salary of Rs. <input type="text" name="basic_salary" id="basic_salary" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"> per month in the scale of pay of <input type="text" name="monthly_salary" id="monthly_salary" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">. In addition to your basic pay, you will be eligible for allowances/benefits detailed in the enclosed Summary of General Service Rules of PRADAN that may be modified/amended from time to time.
   </div>
 </div>
 <div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   3. Your services are subject to the policies and rules of PRADAN as detailed in the Summary of General Service Rules that may be modified/amended from time to time.
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   4. You will be on probation for a period of six months from the date of joining PRADAN. You will be able to continue in service only if your performance (work, conduct and suitability) is found satisfactory during the period of probation. If your performance is not found satisfactory, the period of probation may be extended up to a period of six months. However, you shall continue to be on probation until you are otherwise informed in writing.
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    During the period of probation (including the extended period of probation, if any), your services may be terminated without any notice and/or without assigning any reason whatsoever. Similarly, during the period of probation, you can also leave PRADAN without any notice. However, the formalities of submitting proper letter of resignation and obtaining ‘No-Dues Certificate’ from all concerned shall have to be complied with before your being released from PRADAN.”
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   5. After completion of the period of your probation, your services can be terminated by either side after giving one month’s (30 days’) notice in writing or payment of salary (basic pay) in lieu thereof without assigning any reason:
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    PROVIDED that if any of the information furnished by you through your bio-data/ application form or any other document in connection with your employment with PRADAN is found to be false or incorrect OR it is found at any time that you have not revealed any relevant information, your services may be liable to be terminated forthwith without any compensation whatsoever.
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    6.  As a full-time employee of PRADAN, you cannot accept any other employment on any terms without the prior written approval of the Executive Director of PRADAN. You will also not make yourself directly or indirectly interested in the business of any other person or entity without the prior written approval of the Executive Director of PRADAN.
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   7. You will be responsible for safe-keeping and return in good condition of all the office property, equipment, instruments, tools, books, vehicles, etc., which may be given to you for your use or under your custody/charge. In the event of your failure to account for the aforesaid property, etc., PRADAN shall have the right to deduct/recover the money from your dues or otherwise, and/or take such action as PRADAN may deem fit/proper.
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   8. You will observe due confidentiality with respect to all transactions and activities of PRADAN and shall not, except in performance in good faith of the duties assigned to you, disclose, communicate or part with any confidential, classified or technical information, know-how, details or data, etc., to any other person(s) at any time during your employment in PRADAN. Failure to observe this condition of employment entitles PRADAN to summarily dispense with your services without any notice.
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    9.  Your appointment in PRADAN shall be subject to your medical examination by a registered medical practitioner of the allopathic system of medicine, possessing at least a MBBS qualification, and upon your being found fit as per her/his certificate and the test reports being furnished at the time of your joining PRADAN. Actual expenses will be reimbursed to you by PRADAN upon your filling the prescribed form for claiming reimbursement of medical expenses available at each office. This is besides the reimbursement of medical expenses referred to above.
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    10. Please submit (a) four copies of your latest passport size photograph, (b) your blood group, and (c) your certificates, testimonials, pan card, etc., (one photocopy of each) in support of your date of birth, qualifications and experience to complete your joining formalities.
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   We hope that you will stay with us for long and derive a sense of fulfillment by contributing significantly and meaningfully to the organization and society. In course of time, we are sure you will also help others to learn about the practice of development and initiating action to change things on the ground.
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    We take this opportunity to welcome you on board and look forward to a fruitful and purposive association with you.
  </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   With my best wishes,
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   Yours sincerely,
 </div>
</div>

<div class="row text-left" style="margin-top: 50px; ">
  <div class="col-md-12">
  ( <input type="text" name="name2" id="name2" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"> )
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
  Executive Director
 </div>
</div>
<div class="row text-left" style="margin-top: 50px; ">
  <div class="col-md-12">
  <strong> Encl.:</strong> As above
 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
  cc: - Team Coordinator <br>
- Integrator <br>
- Finance-Personnel-MIS Unit<br>

 </div>
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
  I have carefully read the terms and conditions of this offer of appointment and these are acceptable to me. I shall join duty on <input type="text" name="joining_date" id="joining_date" class="datepicker" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">forenoon
 </div>
</div>

<div class="row text-left" style="margin-top: 50px; ">
  <div class="col-md-6">
 Place: <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">
 </div>
 <div class="col-md-6 text-right">
 Name:       <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">
 </div>
  <div class="col-md-6">
 Date: <input type="text" name="e_date" id="e_date" class="datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required"> 
 </div>
 <div class="col-md-6 text-right">
 Address:       <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">
 </div>

</div>





</div>
<div class="panel-footer text-right">
  <button  type="submit" name="savetbtn" id="savetbtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button>
                 
         <button  type="submit" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>
</div>
</div>
</form>
</div>

</section>
<script type="text/javascript">
$(document).ready(function()
{
   $(".datepicker").datepicker
   ({
         changeMonth: true,
         changeYear: true,
       //  maxDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',
        
    });

   $("#designation").change(function(){
     var des=$(this).val();
      //alert("des"+des);
      var dd=$( "#designation option:selected" ).text();
      $("#name").val(dd);
        
      
     


  });



 });




</script>