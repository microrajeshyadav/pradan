
<style type="text/css">
#errmsg
{
  color: red;
}
</style>
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <br>
  <div class="container-fluid" style="margin-top: 20px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row clearfix doctoradvice">

        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-8 panel-title pull-left">Eligible candidates for written exam</h4>
             <div class="col-md-4 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
        <div class="panel-body">
          <?php $tr_msg= $this->session->flashdata('trr_msg');
          $er_msg= $this->session->flashdata('er_msg');
          if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
          
              <?php //echo "<pre>"; print_r($selectedcandidatedetails); die; ?>
            <div class="col-md-12 text-left">

               <form name="formwrittenscoresearch"  id="formwrittenscoresearch" method="POST" action="">

               <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12 text-left">   
                 <label for="StateNameEnglish" class="field-wrapper required-field">Campus</label> </div>
                 <div class=" col-md-6 col-lg-6 col-xs-12 col-sm-12" style="background-color: white;">  
                  <select name="campusid" id="campusid" class="form-control", required="required">
                    <option value="">Select Campus</option>
                    <?php foreach ($campusdetails as $key => $value) {
                      $expdatefrom = explode('-', $value->fromdate);
                      $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
                      $expdateto = explode('-', $value->todate);
                      $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];
                      $campval  = $value->campusid.'-'. $value->id; 
                      if ($campval == $campusid) {
                       ?>
                       <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                       <?php  }else{ ?>
                       <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
                       <?php } } ?>

                     </select> 

                     <?php echo form_error("campusid");?>
                   </div>
                   <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12" style="background-color: white;">
                    <button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-warning">Search</button>
                  </div>

                </form>
              </div>
             <?php 
             //echo $campusid; 
             $expcampus = '';
             $expcampus[0] = '';
             $expcampus = explode('-', $campusid);
            // echo $expcampus[0]; die;
             ?>
                <form name="formwrittenscore" id="formwrittenscore" method="post" action="" >
                    <input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>"> 
                     <input type="hidden" name="campustype" id="" value="<?php if(isset($campustype)) echo $campustype;?>">         
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                      <table id="writtenscoreselectecandidate" class="table table-bordered table-striped table-hover dataTable js-exportable" style="width:100%">
                        <thead>
                         <tr>
                           
                        <th style ="max-width:30px;" class="text-center">S. No.</th>
                        <?php if ($expcampus[0] !=0) {?>
                         <th class="text-center">Accept </th>
                         <th class="text-center">Reject </th>
                      <?php } ?>
                        <th>Category</th>
                        <th>Campus Name</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email Id</th>
                        <th>Mobile </th>
                        <th>Stream</th>
                        <th>Gap Year</th>
                        <th>10th % </th>
                        <th>12th %</th>
                        <th>UG %</th>
                        <th>PG %</th>
                        <th>State </th>
                        <th>City</th>
                        </tr> 
                      </thead>
                      <tbody>
                        <?php 
                      
                        $i=0; foreach ($selectedcandidatedetails as $key => $value) { ?>
                        <tr>
                           <td class="text-center"><?php echo $i+1; ?></td>
                      <?php if ($expcampus[0] !=0) { ?>
                     
                         <td class="text-center">
                        <div class="form-check">
                        <input class="form-check-input fillaccept text"  type="checkbox" name="accept[]" data-toggle="tooltip" title=" Accept" value="<?php echo $value->candidateid; ?>" id="defaultCheck_accept_<?php echo $i; ?>" >
                        <label class="form-check-label" for="defaultCheck_accept_<?php echo $i; ?>">
                         
                        </label>
                      </div>
                      </td>
                      <td class="text-center">
                        <div class="form-check">
                    <input class="form-check-input fillreject" type="checkbox" name="reject[]" data-toggle="tooltip" title=" Reject" value="<?php echo $value->candidateid; ?>" id="defaultCheck_reject_<?php echo $i; ?>">
                    <label class="form-check-label" for="defaultCheck_reject_<?php echo $i; ?>">
                    </label>
                  </div>
                </td>
              <?php } ?>
                      <td><?php echo $value->categoryname;?></td>
                      <td><?php echo $value->campusname;?></td>
                      <td><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname;?></td>
                      <td><?php echo $value->gender;?></td>
                      <td><?php echo $value->emailid;?> </td>
                      <td><?php echo $value->mobile;?></td>
                      <td><?php echo $value->stream;?></td>
                      <td><?php echo $value->DateDiff;?></td>
                      <td><?php echo $value->metricpercentage;?></td>
                      <td><?php echo $value->hscpercentage;?></td>
                      <td><?php echo $value->ugpercentage;?></td>
                      <td><?php echo $value->pgpercentage;?></td>
                      <td><?php echo $value->statename;?></td>
                      <td><?php echo $value->permanentcity;?></td>
                      </tr>
                      <?php $i++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div> 

                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;">
                    <?php if ($expcampus[0] !=0) {?>
                  <button type="submit" class="btn btn-success" name="btnaccept" id="btnaccept" value="AcceptData" disabled="disabled" >Accept</button> 
                <button type="submit" class="btn btn-warning" disabled="disabled" name="btnreject" id="btnreject" value="RejectData">Reject</button>
              <?php } ?>
             
               <!--  <button type="submit" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</button></div> -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              </div> 
              
            </form>
        
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();  
    $('#writtenscoreselectecandidate').DataTable();
    $('#tableWrittenScore').DataTable({
       dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'pdfHtml5',
             'print'
        ],
       "bPaginate": false,
       "bInfo": false,
       "bFilter": true,
       "bLengthChange": false
    }); 

     $(".fillaccept").click(function(){
      if ($(".fillaccept").is(':checked')) {
         $("#btnaccept").prop("disabled", false);
      }else{
        $("#btnaccept").prop("disabled", true);
      }
    });

    $(".fillreject").click(function(){
       if ($(".fillreject").is(':checked')) {
         $("#btnreject").prop("disabled", false);
      }else{
        $("#btnreject").prop("disabled", true);
      }
    });
  });


 







</script>


<!-- <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js'"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script> -->

<!--  JS Code End Here  -->