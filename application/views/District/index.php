<?php foreach ($role_permission as $row) { if ($row->Controller == "District" && $row->Action == "index"){ ?>
<br>
<div class="container-fluid">
  <style type="text/css">
  .btn1{
    margin-left: 197px;
    margin-top: -93px;
    height: 35px;
  }
</style>
<div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
     <h4 class="col-md-10 panel-title pull-left">District List</h4>
     <div class="col-md-2 text-right">
      

       <a data-toggle="tooltip" data-placement="bottom" title="Want to add new District? Click on me." href="<?php echo site_url()."District/add";?>" class="btn btn-primary btn-sm">Add New District</a>

     </div>
   </div>
   <hr class="colorgraph"><br>
 </div>
 <div class="panel-body">
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        <div class="" style="height: 110px;">
          <div class="col-sm-12"> 
            <form method="GET" class="">
              <div class="row bg-light" style="padding: 10px;">
                <div class="col-md-1">
                  
                  <label for="doctoradvice_date" class="field-wrapper required-field">Select State</label>
                </div>
                <div class="col-md-2">
                  <select style="width: 200px;" name="state_code" id="state_code" class="form-control">
                    <option value="">Select State</option>
                    <?php foreach($state_list as $row){ ?>
                    <option value="<?php echo $row->statecode; ?>" <?php echo ($row->statecode == $this->input->get('state_code') ? "selected" : "");?>><?php echo $row->name; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-9">
                 <button type="submit" name="submit"  class="btn btn-round btn-dark btn-sm">GO</button> </div>
               </div>
             </form>
           </div>
         </div>

         <div class="row" style="background-color: #FFFFFF;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
            <table id="tbldistrict" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
              <thead>
                <tr>
                  <th class="text-center" style="width: 50px;">S. No.</th>
                  <th class="text-center">State Name</th> 
                  <th class="text-center">District Name</th>
                  <!-- <th class="text-center" style="width: 70px;">Status</th> -->
                  <th class="text-center" style="width: 50px;">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0;
                foreach($district_list as $row){ ?>
                <tr>
                  <td class="text-center"><?php echo $i+1; ?></td>
                  <td class="text-center"><?php echo $row->StateName;?></td>
                  <td class="text-center"><?php echo $row->DistrictName;?></td>
                  <!-- <td class="text-center"><?php if($row->status==0)
                  { echo '<span class = "label label-success">Active</span>';} else{
                    echo '<span class = "label label-danger">InActive</span>';  }?></td> -->
                    <td class="text-center">
                      <a href="<?php echo site_url('district/edit/'.$row->districtid);?>" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this District."><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a>|
                      <a data-toggle="tooltip" href="<?php echo site_url('district/delete/'.$row->districtid);?>" style="padding: 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this District." onclick="return confirm_delete()" title="Delete"  data-singleton="True" data-placement="left"><i class="fa fa-trash" style="color:red"></i></a>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>
      </div>
    </div>   
    <?php } } ?>
    <script>
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip(); 
        $('#tbldistrict').DataTable({
          "paging": false,
          "search": false,
        });
      });



      function confirm_delete() {
        var r = confirm("Do you want to delete this District?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }


    </script>
