<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "District" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
          <form method="POST" action="">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-7 panel-title pull-left">Edit District</h4>
                 <div class="col-md-5 text-right" style="color: red">
                  * Denotes Required Field 
                </div>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">  
             <div class="form-group">
              <div class="form-line">
                <label for="doctoradvice_place" class="field-wrapper required-field">State </label>
                <select  name="state_code" id="state_code" class="form-control" required="required">
                  <option value="">Select State</option>
                  <?php foreach($state_list as $row){ ?>
                  <option value="<?php echo $row->statecode; ?>" <?php echo ($row->statecode == 
                    $statecode ? "selected" : "");?>><?php echo $row->name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>


              <div class="form-group">
                <div class="form-line">
                  <label for="doctoradvice_place" class="field-wrapper required-field">District Code</label>
                  <input type="number" class="form-control isNumberKey" maxlength="5" minlength="2" name="DistrictCode" value="<?php echo $districtcode;?>">
                </div>
              </div>
              <input type="hidden" class="form-control" name="state_code" value="<?php echo $district_details->stateid;?>">

              <div class="form-group">
                <div class="form-line">
                  <label for="Name" class="field-wrapper required-field">District Name </label>
                  <input type="text" class="form-control alphabateonly" maxlength="60" minlength="2" name="Name" value="<?php echo $district_details->name;?>">
                </div>
              </div>


              <div class="form-group">
                <div class="form-line">
                  <label for="doctoradvice_place" class="field-wrapper required-field">Status </label>
                  <select  name="status" id="status" class="form-control" required="required">
                    <option value="">Select Status</option>
                    <option value="0" <?php echo ($district_details->isdeleted == 
                      0 ? "selected" : "");?> >Active</option>
                    <option value="1" <?php echo ($district_details->isdeleted == 
                      1 ? "selected" : "");?>>Inactive</option>
                    </select>
                  </div>
                </div>


              </div>
              <div class="panel-footer text-right"> 
                <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>

                <a href="<?php echo site_url("district/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to list</a> 
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
    <?php } } ?>
  </section>

  <script type="text/javascript">
  $(document).ready(function () {
  //called when key is pressed in textbox
  $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
       return false;
     }
   });

  $(".alphabateonly").keypress(function (e){
      var code =e.keyCode || e.which;
      if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46)  
      {
       alert("Only alphabates are allowed");
       return false;
     }
   }); 

});
</script>