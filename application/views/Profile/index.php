<style> 
  /* body {background-color: powderblue;} */
  h3  {color: blue;}
  h3 {font-family: 'Open Sans';}
  h3 {font-style: normal;}
  h3 {font-weight: 400;}

  h4  {color: blue;}
  h4 {font-family: 'Open Sans';}
  h4 {font-style: normal;}
  h4 {font-weight: 400;}

  .Clinical_visit_row:hover
  {
    cursor: pointer;
  }

  .Patient_List:hover{
    cursor: pointer;
  }

  table.field-wrapper {
    display: block;
    width: 100%;
  }
  label.field-wrapper::after { 
    content: "\00a0\00a0 "; /* keeps spacing consistent */
    float: inherit;
  }
  label.field-wrapper input {
    float: inherit;
  }
  label.required-field::after { 
    content: "*";
    color: red;
  }
  .visit_toolbar {
    clear: both;
    vertical-align: bottom;
    width: 100%;
    height: 25px;
  }
  .table
  {
    font-size: 12px;
  }
  body
  {
    font-size: 12px;
  }
</style>
<style>
.overlay{
        z-index : -1;
        position: fixed; 
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.5);
        left: 0; 
        right: 0;
        top: 0;
        bottom: 0; 
        display : none;
        text-align: center;
    }
#loading-image {
  position: fixed;
  top: 300px;
  left: 600px;
  z-index: 100;
}
</style>

<section class="content" style="margin-top: 70px;">
<div class="overlay">
    <img src="<?php echo site_url(); ?>/common/backend/images/loading.gif" alt="loading gif" class="loading_gif" id="loading-image" >
</div>
  <div class="row">
  <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
   <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>  
    <!-- visit details -->
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-center text-gray"> My Profile </h3>
        </div>
        <div class="panel-body">

            <div class="col-sm-12">
                <form method="POST" action="<?php echo site_url("profile/edit");?>">
                      <div class="col-sm-6">
                          <div class="form-line">
                            <?php // echo print_r($user_list); die();?>
                            <label for="doctoradvice_place" class="field-wrapper required-field">User Name</label>
                            <input type="text" class="form-control" name="Username" id="Username" value="<?php echo $user_list[0]->Username;?>">
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-line">
                            <label for="doctoradvice_place" class="field-wrapper required-field">Password</label>
                            <input type="Password" class="form-control" name="Password" id="Password" placeholder="Enter Password" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                       <br>
                          <div class="form-line">
                            <label for="doctoradvice_place" class="field-wrapper required-field">User First Name</label>
                            <input type="text" class="form-control" name="UserFirstName" id="UserFirstName" value="<?php echo $user_list[0]->UserFirstName;?>">
                          </div>
                      </div>
                      <!-- <div class="col-sm-6">
                        <div class="form-group">
                          <div class="form-line">
                            <label for="doctoradvice_place" class="field-wrapper required-field">User Middle Name</label>
                            <input type="text" class="form-control" name="UserMiddleName" id="UserMiddleName" value="<?php //echo $user_list[0]->UserMiddleName;?>">
                          </div>
                        </div>
                      </div> -->
                      <div class="col-sm-6">
                        <br>
                          <div class="form-line">
                            <label for="doctoradvice_place" class="field-wrapper required-field">User Last Name</label>
                            <input type="text" class="form-control" name="UserLastName" id="UserLastName" value="<?php echo $user_list[0]->UserLastName;?>">
                          </div>
                      
                      </div>
                      <div class="col-sm-6">
                        <br>
                          <div class="form-line">
                            <label for="doctoradvice_place" class="field-wrapper required-field">Phone Number</label>
                            <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" value="<?php echo $user_list[0]->PhoneNumber;?>">
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <br>
                          <div class="form-line">
                            <label for="doctoradvice_place" class="field-wrapper required-field">Email ID</label>
                            <input type="email" class="form-control" name="EmailID" id="EmailID" value="<?php echo $user_list[0]->EmailID;?>">
                          </div>
                      </div>
                      <div style="text-align: -webkit-center;">
                        <br>
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="" onclick="goBack()" class="btn btn-danger btn-sm m-t-10 waves-effect">Go Back</a>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
          </section>
<script>
function goBack() {
    window.history.back();
}
</script>