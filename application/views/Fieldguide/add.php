<input type="hidden" name="staff_id" id="staff_id" value="<?php echo $this->uri->segment(3);?>">
<section class="content">
  <div class="container-fluid">



    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      
            <div class="panel-body">             
              <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <form name="formteam" id="formteamid" method="POST" action="">
                   <div class="container-fluid" style="margin-top: 20px;">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-sm-offset-4">
                      <div class="panel thumbnail shadow-depth-2 listcontainer" >
                        <div class="panel panel-default" >
                         <div class="panel-heading">
                          <div class="row">
                           <h4 class="col-md-7 panel-title pull-left">Change Field Guide</h4>
                           <div class="col-md-5 text-right" style="color: red">
                            * Denotes Required Field 
                          </div>
                       </div>
                        <hr class="colorgraph"><br>
                      </div>
                        <div class="form-group">
                          <div class="form-line">
                            <label for="TeamName" class="field-wrapper required-field">Choose Team <span style="color:red;">*</span></label>
                            <?php 
                         
                           
                              $options[$office_id->officeid] = $office_id->officename;
                          
                            echo form_dropdown('team', $options, set_value('team'), 'class="form-control" id="team"'   );
                            ?>
                          </div>
                        
                        </div>  
                      </div>
                     
                  
                           
                          


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        


                        <div class="form-group">
                          <div class="form-line"><label>Field Guide <span style="color:red;">*</span></label>
                            <input type="hidden" name="oldfg" id="oldfg" value="<?php echo $old_Fieldguide->staffid;?>">
                         <?php
                         //$options1[$old_Fieldguide->staffid] = $old_Fieldguide->name;
                          
                           // echo form_dropdown('oldfg', $options1, set_value('oldfg'), 'class="form-control" id="oldfg"');
                            ?>
                            <label class="form-control"><?php echo $old_Fieldguide->name;?></label>
                          </div>
                        
                        </div>  
                      </div>
                     
                           `


                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                              <div class="form-line">
                                <label for="StateNameEnglish" class="field-wrapper required-field">Change Fieldguide <span style="color:red;">*</span></label>
                                <select class="form-control" Name="fieldguide" id="fieldguide">
                                 <option value="0">---All ---</option>
                               </select>
                             </div>
                             <?php echo form_error("fieldguide");?>
                           </div>
                         </div>

                        
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right"> 
                              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                              <a href="<?php echo site_url("Fieldguide");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
                            </div>
               
                </form>
              </div>
            </div>


          </div>
        </div></div></div>
      </div>
    </section>


    <script type="text/javascript">

      $(document).ready(function() {
        //alert("hello");
        var office_id = $("#team option:selected").val();
        var staff_id=$("#staff_id").val();

      //  alert("office_id: "+staff_id);

        $.ajax({
          url: '<?php echo site_url(); ?>ajax/selectedFieldguideteam/',
          type: 'POST',
         
          data:{office_id:office_id,staff_id:staff_id},
          dataType: 'text',
        })
        .done(function(data) {
         //alert(data);

          console.log(data);
        $("#fieldguide").html(data);

        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
         });
        
      });




     
   </script>
