<br>
<section class="content">
    <div class="container-fluid">
     
      <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
   
       <!-- <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-sm-offset-4">
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel panel-default" >
             <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-7 panel-title pull-left">Assign Teams</h4>
                     <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 

                    </div>
                    

                  </div>
                  <hr class="colorgraph"><br>
            </div>
            -->
            <div class="panel-body">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <form name="formteam" id="formteamid" method="POST" action="">


        <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-sm-offset-4">
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel panel-default" >
             <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-7 panel-title pull-left">Assign Teams</h4>
                     <div class="col-md-5 text-right" style="color: red">
                    * Denotes Required Field 

                    </div>
                    

                  </div>
                  <hr class="colorgraph"><br>
            </div>
            <label for="TeamName" class="field-wrapper required-field">Choose Team <span style="color:red;">*</span></label>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <?php  if(empty($teamedetail->teamid)){ ?>
                    <div class="form-group">
                      <div class="form-line">
                        <?php 
                        $options = array('' => 'Select Teams ');
                        $selected_team = $singleteamedetail->teamid; 
                        foreach($teamsdetails as$key => $value) {
                        $options[$value->officeid] = $value->officename;
                        }
                    echo form_dropdown('team', $options, $selected_team, 'class="form-control" id="team"' );
                        ?>
                      </div>
                        <?php echo form_error("team");?>
                    </div>
                    <?php }else{ ?>
                        <div class="form-group" >
                      <div class="form-line">
                        <label for="TeamName" class="field-wrapper required-field">Choose Team <span style="color:red;">*</span></label>
                            <select name="team"  id="team" class="form-control">
                              <?php foreach($teamsdetails as $row) {
                                  if($row->officeid == $singleteamedetail->teamid){
                                ?>
                            <option value="<?php echo $row->officeid; ?>" Selected><?php echo $row->officename; ?> </option>
                            <?php }else {?>
                          <option value="<?php echo $row->officeid; ?>" ><?php echo $row->officename; ?> </option>

                              <?php } } ?>
                            </select>
                          </div>
                        </div>

                      <?php  } ?>
                  </div>
                  <?php if ($getcategoryid->categoryid ==1) { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">FG  <span style="color:red;">*</span></label>
                        <select class="form-control" Name="fieldguide" id="fieldguide">
                   			 <?php foreach($getsingletfieldguide as $row) {
                                ?>
                            <option value="<?php echo $row->staffid; ?>" Selected><?php echo $row->name; ?> </option>
                           <?php } ?>
                   			</select>
                         

                      </div>
                        <?php echo form_error("fieldguide");?>
                    </div>
                  </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                     <?php  if(empty($teamedetail->teamid)){ ?>
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Batch <span style="color:red;">*</span></label>
                       <?php 
                        $options = array('' => 'Select Batch');
                        $selected_batch = $singleteamedetail->batchid; 
                        foreach($batchdetails as $key => $value) {
                        $options[$value->id] = $value->batch;
                        }
                        echo form_dropdown('batch', $options, $selected_batch, 'class="form-control" id="batch"' );
                        ?>
                      </div>
                        <?php echo form_error("batch");?>
                    </div>
                    <?php }else{ ?>
                    <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Batch  <span style="color:red;">*</span></label>

                         <select name="batch"  id="batch" class="form-control" >
                              <?php foreach($batchdetails as $row) {
                                  if($row->id == $teamedetail->batch){
                                ?>
                            <option value="<?php echo $row->id; ?>" Selected><?php echo $row->batch; ?> </option>
                            <?php }else {?>
                          <option value="<?php echo $row->id; ?>" ><?php echo $row->batch; ?> </option>

                              <?php } } ?>
                            </select>

                      </div>
                        <?php echo form_error("batch");?>
                    </div>
                   
                  </div>
                <?php }  }?>
                
                  </div>
                  <div class="panel-footer text-right"> 
              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
              <a href="<?php echo site_url("Assigntc");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
            </div>
                  <!--<div class="row">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" > 

                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("Assigntc");?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> </div>
                    
                       
                    </div>
                    </div>-->
                  </form>
                </div>
             
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </div>
</div>
 </section>


<script type="text/javascript">
 $(document).ready(function(){

   $("#team").load(function(){
 
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   
                    console.log(data);
                    $("#fieldguide").html(data);
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

         $("#team").change(function(){
              
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   
                    console.log(data);
                    $("#fieldguide").html(data);
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        }); 

</script>
