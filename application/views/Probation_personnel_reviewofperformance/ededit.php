
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>

     <form name="staffreviewfrm" id="staffreviewfrm" method="POST" action="">
     <div class="panel-body">
      <div class="text-center">
        <p><h5>Part- I</h5></p>
        <p>(<em>to be filled in by the Finance-Personnel-MIS Unit)</em></p>
      </div>
      <div class="row" style="line-height: 3">
        <div class="col-md-12">
          1. Name :

 <input type="hidden" name="supervisorid" id="supervisorid" value="<?php echo $getstaffprovationreviewperformance->supervisorid; ?>">

           <input type="hidden" name="staffid" id="staffid"   value="<?php echo $getstaffprovationreviewperformance->staffid; ?>"> <input type="text" name="staffname" id="staffname"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly"  value="<?php echo $getstaffprovationreviewperformance->name; ?>">
        </div>
        <div class="col-md-12">
         2. Designation : <input type="text" name="staffdesignation" id="staffdesignation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprovationreviewperformance->desname; ?>" readonly="readonly">
       </div>     
       <div class="col-md-12">
        3. Employee Code :  <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprovationreviewperformance->emp_code; ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        4. Project  :<input type="text" name="staffproject" id="staffproject"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->emp_project; ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        5. Location :<input type="text" name="stafflocation" id="stafflocation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->emp_location; ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        6. Date of Appointment:<input type="text" name="date_of_appointment" id="date_of_appointment" class="datepicker" readonly="readonly" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->date_of_appointment); ?>">
      </div>
      <div class="col-md-12">
        7. Period of Review : From <input type="text" name="period_of_review_from" id="period_of_review_from" class="datepicker"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_from); ?>"  required="required" > (date) To <input type="text" name="period_of_review_to" id="period_of_review_to" class="datepicker" readonly="readonly"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_to); ?>"  required="required" > (date)
      </div>
    </div>
    <p></p>
    <p></p>
    <p><strong>TO BE REVIEWED BY</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        <input type="hidden" name="review_by_id" id="review_by_id"  value="<?php echo $getstaffpersonneldetals->staffid;?>">
        1. Name : <input type="text" name="review_by" id="review_by" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffpersonneldetals->name;?>" readonly="readonly">
      </div>

      <div class="col-md-12">
       2. Designation : <input type="text" name="review_designation" id="review_designation" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffpersonneldetals->desname;?>" readonly="readonly">
     </div>

     <div class="col-md-12">
      3. Employee Code : <input type="text" name="review_emp_code" id="review_emp_code" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffpersonneldetals->emp_code;?>" readonly="readonly">
    </div>

    <div class="col-md-12">
      4. Project  : <input type="text" name="review_project" id="review_project" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->reviewed_project;?>" readonly="readonly">
    </div>

    <div class="col-md-12">
      5. Location : <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprobationreview->reviewed_location;?>" readonly="readonly">
    </div>
  </div>
  <hr/>
  <p><strong>Notes for the Reviewer</strong></p>
  <p></p>
  <ol>
    <li>Please assess the employee&rsquo;s performance during the period of probation mentioned above (Item 6).</li>
  </ol>
  <p></p>
  <ol start="2">
    <li>Please be objective in your assessment. Each factor should be assessed independently, uninfluenced by assessment of the other factor(s).</li>
  </ol>
  <p></p>
  <ol start="3">
    <li>Please return the duly filled in assessment report in a closed cover marked <em>confidential</em> to</li>
  </ol>
  <p>the Finance-Personnel-MIS Unit latest by _______________ (date).</p>
  <p></p>
  <div class="text-center">
    <p><h5>Part-II</h5></p>
    <p><h5>REVIEW</h5></p>
    <p>(<em>to be filled in by the Reviewing Supervisor</em>)</p>
  </div>
  <p>The probation has provided you an opportunity to interact with and observe the probationer in the work setting for six months. This is a significant period of time, for both sides.</p>
  <p></p>
  <p>&ldquo;<strong>How do you see the Probationer fulfilling the primary responsibilities s/he would need to shoulder to be an effective employee of this organization?</strong>&rdquo;</p>
  <p><strong></strong></p>
  <p>Could you please write about <em>500 words</em> addressing the issue (primary responsibilities s/he is expected to handle). Please give anecdotes/examples wherever possible.</p>

 
  <p><strong>Work Habits and Attitudes</strong></p>
  <p></p>
  <p>How would you rate the probationer&rsquo;s interest in and aptitude for work? What is the quality of her/his work? Does s/he demonstrate proaction and take initiative? Is s/he serious about work, and shows a sense of commitment to it? How resourceful is the probationer? Is s/he punctual, disciplined and regular at work? How does s/he fit into PRADAN&rsquo;s work culture? What is her/his attitude to the communities we work with? How does s/he deal with the conditions of work, absence of regular timings, physical hardship and unstructured work situation?</p>
  <textarea name="work_habits_and_attitudes" readonly="readonly" maxlength="500" id="work_habits_and_attitudes" cols="40" rows="10" style="width:1000px;"><?php echo $getstaffprobationreview->work_habits_and_attitudes;?></textarea>

  <p><strong>Conduct and Social Maturity</strong></p>
  <p></p>
  <p>What is the probationer&rsquo;s general behaviour like? Does s/he possess the requisite inter-personal competence? What is her/his demeanour such as&mdash;e.g., is s/he moody, short-tempered, adjusting, reserved, talkative, a gossip, etc.?</p>
   <textarea name="conduct_and_social_maturity" maxlength="500" readonly="readonly" id="conduct_and_social_maturity" cols="40" rows="10" style="width:1000px;"><?php echo $getstaffprobationreview->conduct_and_social_maturity;?></textarea>

 <p>Is the probationer's integrity<em> </em><br>
    <?php if ($getstaffprobationreview->integrity =="questionable") {
     $check = "checked=checked";
    } else if($getstaffprobationreview->integrity =="aboveBoard"){
       $check = "checked=checked";
    } ?>
     <input type="radio" name="questionable" id="questionable" value="questionable" <?php echo $check;?>><em> Questionable</em><strong><em> </em></strong></p>
  <p><strong><em> </em></strong>or</p>
  <p><strong><em>  </em><input type="radio" name="questionable" id="aboveBoard" <?php echo $check;?> value="aboveBoard"></strong><em>Above Board</em><em> <strong> </strong></em></p>


  <p><strong>Any Other Observations</strong></p>
  <p>Please share any other observations you would like to make about the probationer. For instance, does s/he have any areas that s/he could improve upon? Is there anything significant about her/him that has not been captured in the above?</p>
  <textarea name="conduct_and_social_maturity" maxlength="500" readonly="readonly" id="conduct_and_social_maturity" cols="40" rows="10" style="width:1000px;"><?php echo $getstaffprobationreview->any_other_observations;?></textarea>
  <div class="text-center">
    <p><strong><br /> </strong></p>
    <?php  if ($this->loginData->RoleID == 2) {?>
    <p><h5>Part III</h5></p>
    <p><h5>OVERALL REVIEW AND RECOMMENDATIONS</h5></p>
  </div>
  <div class="container-fluid">
    <div class="row text-left">
      1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation indicated above, has been (please tick &uuml;):
    </div>
    <div class="row text-left">
      <div class="col-md-3"> (a)  Satisfactory   </div>
      <div class="col-md-9 text-left"><input type="radio" name="satisfactory" id="satisfactory" value="satisfactory"></div>
      <div class="col-md-12">  OR</div>
      <div class="col-md-3"> (b)  Not satisfactory </div> 
      <div class="col-md-9"><input type="radio" name="satisfactory" id="notsatisfactory" value="notsatisfactory"></div>
    </div>
    <div class="row text-left">
      2. Her/his probation:
    </div>
    <div class="row text-left">
      <div class="col-md-3"> (a)  May be completed </div>
      <div class="col-md-9"> <input type="radio" name="completed" id="completed" value="yes"></div>
      <div class="col-md-12">  OR</div>
      <div class="col-md-12"> (b) May not be completed now as her/his performance needs to be observed further for a period of <input type="text" name="extended_date" id="extended_date"  class="datepicker" style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="" disabled="disabled" > months.<input type="radio" name="completed" id="extended_completed" value="no"></div>
      <div class="col-md-12">  (c)  Not recommended for absorption in PRADAN <input type="radio" name="not_recommended" id="not_recommended" value="not_recommended"></div>
    </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
        Place:  <input type="text" name="place" id="place"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value=""> 
      </div>

      <div class="col-md-6 pull-right">
        Signature of Reviewing Supervisor
      </div>
      <div class="col-md-6 pull-left">
        Date:  <input type="text" name="supervisor_date" id="supervisor_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo date('d/m/Y');?>"> 
      </div>
      <div class="col-md-6 pull-right">
        Name:<input type="text" name="supervisor_name" id="supervisor_name"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getSupervisordetals->supervisorname;?>"> 
      </div>
      <div class="col-md-6 pull-left">

      </div>
      <div class="col-md-6 pull-right">
       Designation: <input type="text" name="supervisor_designation" id="supervisor_designation"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getSupervisordetals->supervisordesignationname;?>"> 
     </div>
   </div>
 </div>
<?php } ?>
 <?php  if ($this->loginData->RoleID == 18) {?>
 <div class="text-center">
  <p><h5>Part - IV</h5></p>
  <p><h5>Executive Director&rsquo;s Note </h5></p>
  <p>(<em>required only in cases where probation is proposed to be extended OR absorption is <u>not </u>recommended by the Reviewing Supervisor</em>)<em>.</em></p> </div>
  <p></p>
  <p>(i) I agree/do not agree with the above recommendations.</p>
  <p></p>
  <p>(ii) The reasons for not agreeing to above recommendations are:</p>
   <p><select name="edstatus" id="edstatus" required="required" style="min-width: 5px; max-width:400px; " >
     <option>Select</option>
     <option value="7">Approved</option>
     <option value="8">Rejected</option>
   </select></p>

 
   <p><textarea name="ed_comments" maxlength="500" id="ed_comments" cols="40" rows="10" style="width:1000px;"></textarea></p>
  </p>


 
 <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
        Date: <input type="text" name="supervisor_date" id="supervisor_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
      </div>
      <div class="col-md-6 pull-right">
       Signature of ED : ________________________
      </div>      
   </div> 

 <?php }  ?>


  <div class="text-center">
    <p><h5>Part - V</h5></p>
    <p>(<em>for use in the Finance-Personnel-MIS Unit)</em></p>
    <p></p>
    <p>Necessary action has been taken and all concerned have been informed accordingly. The Probationer Register has also been completed as above.</p>
    <p></p> </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
       Name:  <input type="text" name="personnel_name" id="personnel_name"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffpersonneldetals->name;?>">
      </div>
      <div class="col-md-6 pull-right">
       Designation: <input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffpersonneldetals->desname;?>">
      </div>
       <div class="col-md-6 pull-left">
       Signature :________________________
      </div>
      <div class="col-md-6 pull-right">
       Date: <input type="text" name="personnel_date" id="personnel_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y'); ?>">
      </div>      
   </div>
  </div>
  <div class="panel-footer text-right">
        <input type="submit" name="submit" value="Submit" value="SaveDataSend" class="btn btn-success btn-sm">
        <a href="<?php  echo site_url().'Staff_list/';?>" class="btn btn-dark btn-sm"> Go Back</a>
      </div>
    </form>
</div>
</div>
</section>
<script type="text/javascript">
  $(".datepicker").datepicker({
      changeMonth: true,
        changeYear: true,
       // maxDate: 'today',
        yearRange: '1980:2025',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
    });

$('#extended_completed').on('click', function() {
 
   if ($(this).val() === 'no') {
      $('#extended_date').removeProp("disabled");
   }
   else {
      $('#extended_date').prop("disabled", "disabled");
   }
});

  
</script>