  <section class="content" style="background-color: #FFFFFF;" >

    <br>
    <div class="container-fluid">
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
         <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - CHECK SHEET
           </h4>
            <div class="col-md-12 text-right">
            <a data-toggle="tooltip" data-placement="bottom" title="Want to add Duplicate Records? Click on me." href="<?php echo site_url()."Batch/add";?>" class="btn btn-primary btn-sm">Duplicate</a>
           </div>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="row">
         <div class="col-md-12 text-center">
          <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
        </div>
        <div class="col-md-12 text-center" style="margin-bottom: 20px;">
          <h4>CHECK SHEET</h4>
          <p>(for Processing Cases of Separation from PRADAN)</p>
        </div> 
      </div>
      
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 10px;">
         <strong>PART - I</strong>
         <p> (to be filled in by Finance-Personnel-MIS Unit)</p>
       </div>
     </div>
     

    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1.  Name of Separating Employee: ______________________________
      </div>
      <div class="col-md-12">
       2. Employee Code : ______________
     </div>
     <div class="col-md-12">
      3.  Designation   : ______________________________
    </div>
    <div class="col-md-12">
      4.  Date of Joining PRADAN  : _______________
    </div>
    <div class="col-md-12">
      5.  Location  : ________________________
    </div>
    <div class="col-md-12">
     6. Reasons for Separation  : ______________________________________________
   </div>
   <div class="col-md-12" style="font-size: 12px;">
    <i>(Resignation/Retirement/Premature Retirement/Death/Name Struck Off/Dismissal, etc.)</i>
  </div>
  <div class="col-md-12">
   7. Date on Which Resignation Submitted (if applicable): _______________
 </div>
 <div class="col-md-12">
   8. Date from which Separation Takes Effect: _____________
 </div>
 <div class="col-md-12">
  9.  Whether under Probation?:           Yes/No
</div>
<div class="col-md-12">
  10. If not under probation, whether due notice of 
  one month for leaving job has been given?:        Yes/No
</div>
<div class="col-md-12">
  11. If No, how much is the short fall in notice period?:  _____ days
</div>
<div class="col-md-12">
  12. Whether the amount paid by separating employee against the               
  short fall of notice period? Yes/No/NA

</div>
<div class="col-md-12">
  13. Whether the employee is under obligation to serve PRADAN?:  Yes/No
</div>
<div class="col-md-12">
 14. If Yes, up to what date?  : ________________ 
</div>
<div class="col-md-12">
 15.  Whether any disciplinary action is pending 
 against the separating employee? Yes/No

</div>
<div class="col-md-12">
 16.  Whether occupying PRADAN’s owned/         
 leased/rented accommodation?: Yes/No

</div>
<div class="col-md-12">
 17   .  Whether PAFS Updated for No Dues Certificate?  Yes/No
</div>
<div class="col-md-12">
 18.  Whether Leave MIS Updated for Leave Encashment? Yes/No/NA
</div>
<div class="col-md-6">
  <div class="card">
    <span class="text-center bg-light">Recoveries involved on account of the following as on date of release:</span>
    <table class="table">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        <select name="" class="form-control">        
          <option value="0"> select          
          </option>
        </select>
      </td>
      <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td><select name="" class="form-control">        
      <option value="0"> select          
      </option>
    </select></td>
    <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
  </tr>
  <tr>
   <td>3.</td>
   <td><select name="" class="form-control">        
    <option value="0"> select          
    </option>
  </select></td>
  <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
<tr>
 <td>4.</td>
 <td><select name="" class="form-control">        
  <option value="0"> select          
  </option>
</select></td>
<td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
<tr>
 <td>5.</td>
 <td><select name="" class="form-control">        
  <option value="0"> select          
  </option>
</select></td>
<td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
<tr>
 <td>6.</td>
 <td><select name="" class="form-control">        
  <option value="0"> select          
  </option>
</select></td>
<td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
</tbody>

</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-light">Amount due and payable to separating employee on account of: (as on date of release): </span>
   <table class="table">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        Pay
      </td>
      <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td>HRA</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
   <tr>
     <td>3.</td>
     <td>Leave Encashment ( ___ days)</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
   <tr>
     <td>4.</td>
     <td>Personal Claim</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
   <tr>
     <td>5.</td>
     <td>Provident Fund</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
   <tr>
     <td>6.</td>
     <td>Gratuity</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
   <tr>
     <td>7.</td>
     <td>Any Other</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
 </tbody>
</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-info">Summary </span>
   <table class="table">     
    <tbody>
     <tr>
       <td>(i)</td>
       <td> 
        Payments due to employee
      </td>
      <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
    </tr>
    <tr>
     <td>(ii)</td>
     <td>Amounts recoverable</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
   <tr>
     <td>(iii)</td>
     <td>Net payable/recoverable</td>
     <td><input class="form-control" maxlength="6" type="text" style="max-width: 120px;" /></td>
   </tr>
  
 </tbody>

</table>
</div>
</div>


</div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
  <div class="col-md-12 text-left" >
  <span style="border-bottom:Solid 2px #000; ">Any other information/remarks relevant in this case:</span>
</div>
 <div class="col-md-4 text-left">
   <strong>Checked (Accountant Location)</strong> <br>
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: _________________________________ </p>
    <p>Place: _________________________________ </p>

 </div>
 <div class="col-md-4 text-left">
   <strong>Checked (Finance-Personnel-MIS Unit)</strong> <br>
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: _________________________________ </p>
    <p>Place: _________________________________ </p>

 </div>
<div class="col-md-4 text-left">
   <strong>Checked (Dealing Person in Finance-Personnel-MIS Unit)</strong> <br>
    
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: _________________________________ </p>
    <p>Place: _________________________________ </p>

 </div>


</div>

</div>
<div class="panel-footer text-right">
 
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>