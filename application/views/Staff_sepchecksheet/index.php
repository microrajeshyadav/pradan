<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 441px !important;
  }
  

</style>

<form role="form" method="post" action="" name="formsepchecksheet" id="formsepchecksheet">
<section class="content" style="background-color: #FFFFFF;" >
<?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>

    <br>
    <div class="container-fluid">
       <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - CHECK SHEET
           </h4> <div class="col-md-12 text-right">
            <?php 
              // echo "<pre>";
              // print_r($tbl_check_sheet);
              // die;
            if(!empty($tbl_check_sheet) && $this->loginData->RoleID==22)
            {
              
            ?>
            <a  id="btnduplicate"data-toggle="tooltip" data-placement="bottom" title="Want to add Duplicate Records? Click on me." href="<?php echo site_url()."Staff_sepchecksheet/check_duplicaterecord/$tbl_check_sheet->transid";?>" class="btn btn-primary btn-sm">Duplicate</a>
          <?php } ?>
           </div>
          
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="row">
         <div class="col-md-12 text-center">
          <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
        </div>
        <div class="col-md-12 text-center" style="margin-bottom: 20px;">
          <h4>CHECK SHEET</h4>
          <p>(for Processing Cases of Separation from PRADAN)</p>
        </div> 
      </div>
      
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 10px;">
         <strong>PART - I</strong>
         <p> (to be filled in by Finance-Personnel-MIS Unit)</p>
       </div>
     </div>
     

    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1.  Name of Separating Employee: <label class="inputborderbelow"> <?php  if(!empty($staff_detail->name))echo $staff_detail->name; ?></label>
      </div>
      <div class="col-md-12">
       2. Employee Code : <label class="inputborderbelow"> <?php if(!empty($staff_detail->emp_code)) echo $staff_detail->emp_code; ?></label>
     </div>
     <div class="col-md-12">
      3.  Designation   : <label class="inputborderbelow"> <?php if(!empty($staff_detail->joindesig)) echo $staff_detail->joindesig; ?></label>
    </div>
    <div class="col-md-12">
      4.  Date of Joining PRADAN  : <label class="inputborderbelow"> <?php if(!empty($staff_detail->joiningdate)) echo $this->gmodel->changedatedbformate($staff_detail->joiningdate); ?></label>
    </div>
    <div class="col-md-12">
      5.  Location  : <label class="inputborderbelow"> <?php if(!empty($staff_detail->address)) echo $staff_detail->address; ?></label>
    </div>
    <div class="col-md-12">
     6. Reasons for Separation  : <input type="text" name="reason_for_separation" class="inputborderbelow" value="<?php echo $staff_transaction->trans_status; ?>"  readonly/>
   </div>
   <div class="col-md-12" style="font-size: 12px;">
    <i>(Resignation/Retirement/Premature Retirement/Death/Name Struck Off/Dismissal, etc.)</i>
  </div>
  <div class="col-md-12">
   7. Date on Which Resignation Submitted (if applicable): <input type="text" name="date_resignation_submitted_7" id="date_resignation_submitted_7" value="<?php if($tbl_check_sheet){ if($tbl_check_sheet->date_resignation_submitted_7) echo $this->gmodel->changedatedbformate($tbl_check_sheet->date_resignation_submitted_7); }else{ echo $this->gmodel->changedateFormateExcludeTime($staff_transaction->datetime); } ?>" class="inputborderbelow " required readonly/>
 </div>
 <div class="col-md-12">
   8. Date from which Separation Takes Effect: <input type="text" name="date_from_which_separation_takes_effect_8" id="date_from_which_separation_takes_effect_8" class="inputborderbelow" value="<?php if($tbl_check_sheet){ if($tbl_check_sheet->date_from_which_separation_takes_effect_8) echo $this->gmodel->changedatedbformate($tbl_check_sheet->date_from_which_separation_takes_effect_8); }else{ echo $this->gmodel->changedatedbformate($staff_detail->seperatedate); } ?>" required readonly/>
 </div>
 <div class="col-md-12">
  <?php //print_r($tbl_check_sheet);?>
  9.  Whether under Probation?:

  <input type="radio" name="whether_under_probation_9" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->whether_under_probation_9 == 1) echo "checked"; } ?> required> Yes 
  <input type="radio" name="whether_under_probation_9" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->whether_under_probation_9 == 0) echo "checked"; } ?>  required> No
</div>
<div class="col-md-12">
  10. If not under probation, whether due notice of 
  one month for leaving job has been given?:
  <input type="radio" name="if_not_under_probation_10" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->if_not_under_probation_10 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="if_not_under_probation_10" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->if_not_under_probation_10 == 0) echo "checked"; } ?>  required> No
</div>
<div class="col-md-12">
  11. If No, how much is the short fall in notice period?:
  <input type="text" name="short_fall_in_notice_period_11" id="short_fall_in_notice_period_11" maxlength="3" value="<?php if($tbl_check_sheet){ echo $tbl_check_sheet->short_fall_in_notice_period_11; } ?>" class="inputborderbelow txtNumeric" required> days
</div>
<div class="col-md-12">
  12. Whether the amount paid by separating employee against the               
  short fall of notice period?
  <input type="radio" name="separating_employee_against_12" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->separating_employee_against_12 == 1) echo "checked"; } ?> required> Yes
  <input type="radio" name="separating_employee_against_12" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->separating_employee_against_12 == 0) echo "checked"; } ?> required> No
  <input type="radio" name="separating_employee_against_12" value="2" <?php if($tbl_check_sheet){ if($tbl_check_sheet->separating_employee_against_12 == 2) echo "checked"; } ?>  required> NA

</div>
<div class="col-md-12">
  13. Whether the employee is under obligation to serve PRADAN?:
  <input type="radio" name="under_obligation_12" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->under_obligation_12 == 1) echo "checked"; } ?> required> Yes
  <input type="radio" name="under_obligation_12" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->under_obligation_12 == 0) echo "checked"; } ?>  required> No
</div>
<div class="col-md-12">
 14. If Yes, up to what date?  : <input type="text" name="what_date_13" id="what_date_13" value="<?php if($tbl_check_sheet){ if($tbl_check_sheet->what_date_13) echo $this->gmodel->changedatedbformate($tbl_check_sheet->what_date_13); } ?>" class="inputborderbelow datepicker" readonly/> 
</div>
<div class="col-md-12">
 15.  Whether any disciplinary action is pending 
 against the separating employee?
 <input type="radio" name="pending_against_the_separating_employee_14" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->pending_against_the_separating_employee_14 == 1) echo "checked"; } ?> required> Yes
  <input type="radio" name="pending_against_the_separating_employee_14" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->pending_against_the_separating_employee_14 == 0) echo "checked"; } ?> checked required> No

</div>
<div class="col-md-12">
 16.  Whether occupying PRADAN’s owned/         
 leased/rented accommodation?:
 <input type="radio" name="occupying_accommo_15dation_15" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->occupying_accommo_15dation_15 == 1) echo "checked"; } ?> required> Yes
  <input type="radio" name="occupying_accommo_15dation_15" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->occupying_accommo_15dation_15 == 0) echo "checked"; } ?>  required> No

</div>
<div class="col-md-12">
 17   .  Whether PAFS Updated for No Dues Certificate?
 <input type="radio" name="no_dues_certificate_16" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->no_dues_certificate_16 == 1) echo "checked"; } ?> required> Yes
  <input type="radio" name="no_dues_certificate_16" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->no_dues_certificate_16 == 0) echo "checked"; } ?>  required> No
</div>
<div class="col-md-12">
 18.  Whether Leave MIS Updated for Leave Encashment?
 <input type="radio" name="leave_encashment_17" value="1" <?php if($tbl_check_sheet){ if($tbl_check_sheet->leave_encashment_17 == 1) echo "checked"; } ?> required> Yes
  <input type="radio" name="leave_encashment_17" value="0" <?php if($tbl_check_sheet){ if($tbl_check_sheet->leave_encashment_17 == 0) echo "checked"; } ?> required> No
  <input type="radio" name="leave_encashment_17" value="2" <?php if($tbl_check_sheet){ if($tbl_check_sheet->leave_encashment_17 == 2) echo "checked"; } ?> required> NA
</div>
<div class="col-md-6">
  <div class="card">
    <span class="text-center bg-light">Recoveries involved on account of the following as on date of release:</span>
    <table class="table">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        <select name="head1" class="form-control">
          <option value="0">Select</option>
        <?php if(!empty($head)) {foreach($head as $key=>$value){ ?>        
          <option value="<?php echo $key; ?>" <?php if($head1)if($head1['headid'] == $key) echo "selected"; ?> > <?php echo $value; ?>
          </option>
        <?php } }?>
        </select>
      </td>
      <td><input class="form-control txtNumeric2" name="amount1" id="amount1" value="<?php if($head1) echo $head1['amount']; ?>"  maxlength="6" type="text" style="max-width: 120px;" /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td><select name="head2" class="form-control">        
      <option value="0">Select</option>
      <?php foreach($head as $key=>$value){ ?>        
          <option value="<?php echo $key; ?>" <?php if($head2)if($head2['headid'] == $key) echo "selected"; ?>> <?php echo $value; ?>          
          </option>
        <?php } ?>
    </select></td>
    <td><input class="form-control txtNumeric2" name="amount2" id="amount2" value="<?php if($head2) echo $head2['amount']; ?>" maxlength="6" type="text" style="max-width: 120px;" /></td>
  </tr>
  <tr>
   <td>3.</td>
   <td><select name="head3" class="form-control"> 
   <option value="0">Select</option>       
    <?php foreach($head as $key=>$value){ ?>        
          <option value="<?php echo $key; ?>" <?php if($head3)if($head3['headid'] == $key) echo "selected"; ?>> <?php echo $value; ?>          
          </option>
        <?php } ?>
  </select></td>
  <td><input class="form-control txtNumeric2" name="amount3" id="amount3" value="<?php if($head3) echo $head3['amount']; ?>" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
<tr>
 <td>4.</td>
 <td><select name="head4" class="form-control">
 <option value="0">Select</option>        
  <?php foreach($head as $key=>$value){ ?>        
          <option value="<?php echo $key; ?>" <?php if($head4)if($head4['headid'] == $key) echo "selected"; ?>> <?php echo $value; ?>          
          </option>
        <?php } ?>
</select></td>
<td><input class="form-control txtNumeric2" name="amount4" id="amount4" value="<?php if($head4) echo $head4['amount']; ?>" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
<tr>
 <td>5.</td>
 <td><select name="head5" class="form-control">
 <option value="0">Select</option>        
  <?php foreach($head as $key=>$value){ ?>        
          <option value="<?php echo $key; ?>" <?php if($head5)if($head5['headid'] == $key) echo "selected"; ?>> <?php echo $value; ?>          
          </option>
        <?php } ?>
</select></td>
<td><input class="form-control txtNumeric2" name="amount5" id="amount5" value="<?php if($head5) echo $head5['amount']; ?>" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
<tr>
 <td>6.</td>
 <td><select name="head6" class="form-control"> 
 <option value="0">Select</option>       
  <?php foreach($head as $key=>$value){ ?>        
          <option value="<?php echo $key; ?>" <?php if($head6)if($head6['headid'] == $key) echo "selected"; ?>> <?php echo $value; ?>          
          </option>
        <?php } ?>
</select></td>
<td><input class="form-control txtNumeric2" name="amount6" id="amount6" value="<?php if($head6) echo $head6['amount']; ?>" maxlength="6" type="text" style="max-width: 120px;" /></td>
</tr>
</tbody>

</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-light">Amount due and payable to separating employee on account of: (as on date of release): </span>
   <table class="table">
     <thead>
       <tr>
        <th>
          #
        </th>
        <th>
          Head
        </th>  
        <th>
          Amount in Rs.
        </th>   
      </tr>
    </thead>
    <tbody>
     <tr>
       <td>1.</td>
       <td> 
        Pay
      </td>
      <?php //print_r($tbl_check_sheet_transaction_right);?>
      <td><input class="form-control txtNumeric1" name="amount7" id="amount7" value="<?php if($tbl_check_sheet_transaction_right){ echo $tbl_check_sheet_transaction_right[0]->amount;  } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>2.</td>
     <td>HRA</td>
     <td><input class="form-control txtNumeric1" name="amount8" id="amount8" value="<?php if($tbl_check_sheet_transaction_right){ echo $tbl_check_sheet_transaction_right[1]->amount;  } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>3.</td>
     <td>Leave Encashment ( ___ days)</td>
     <td><input class="form-control txtNumeric1" name="amount9" id="amount9" value="<?php if($tbl_check_sheet_transaction_right){ echo $tbl_check_sheet_transaction_right[2]->amount;  } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>4.</td>
     <td>Personal Claim</td>
     <td><input class="form-control txtNumeric1" name="amount10" id="amount10" value="<?php if($tbl_check_sheet_transaction_right){ echo $tbl_check_sheet_transaction_right[3]->amount;  } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>5.</td>
     <td>Provident Fund</td>
     <td><input class="form-control txtNumeric1" name="amount11" id="amount11" value="<?php if($tbl_check_sheet_transaction_right){ echo $tbl_check_sheet_transaction_right[4]->amount;  } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>6.</td>
     <td>Gratuity</td>
     <td><input class="form-control txtNumeric1" name="amount12" id="amount12" value="<?php if(!empty($tbl_check_sheet_transaction_right[5]->amount)){ echo $tbl_check_sheet_transaction_right[5]->amount;  } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>7.</td>
     <td>Any Other</td>
     <td><input class="form-control txtNumeric1" name="amount13" id="amount13" value="<?php if(!empty($tbl_check_sheet_transaction_right[6])){ echo $tbl_check_sheet_transaction_right[6]->amount; } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
 </tbody>
</table>
</div>
</div>
<div class="col-md-6">
  <div class="card">
   <span class="text-center bg-info">Summary </span>
   <table class="table">     
    <tbody>
     <tr>
       <td>(i)</td>
       <td> 
        Payments due to employee
      </td>
      <td><input class="form-control txtNumeric" name="payments_due_to_employee" id="payments_due_to_employee" value="<?php if(!empty($tbl_check_sheet)) { echo $tbl_check_sheet->payments_due_to_employee; } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
    </tr>
    <tr>
     <td>(ii)</td>
     <td>Amounts recoverable</td>
     <td><input class="form-control txtNumeric" name="amounts_recoverable" id="amounts_recoverable" value="<?php if(!empty($tbl_check_sheet)){ echo $tbl_check_sheet->amounts_recoverable; } ?>" maxlength="6" type="text" style="max-width: 120px;" required /></td>
   </tr>
   <tr>
     <td>(iii)</td>
     <td>Net payable/recoverable</td>
     <td><input class="form-control txtNumeric" name="net_payable_recoverable" id="net_payable_recoverable" value="<?php if(!empty($tbl_check_sheet)){ echo $tbl_check_sheet->net_payable_recoverable; } ?>" maxlength="6" type="text" style="max-width: 120px;"  required /></td>
   </tr>
  
 </tbody>

</table>
</div>
</div>


</div>
<hr/>
<?php if($this->loginData->RoleID==22){?>
<div class="row">
                    

                      <div class="col-lg-6 col-md-6">
                      <label for="Name">Status <span style="color: red;" >*</span></label>
                    

                        <select name="status" id="status" class="form-control" required="required"> 
                          <option value="">Select</option>
                          

                            
                                   
                                   <option value="18">Approved</option>
                                  <option value="19">Rejected</option>
                                
                          </select>
                        <?php echo form_error("status");?>
                      </div>
                      <div id="personnel_admin" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none;" >
                      <label for="Name">Sent to <?php if($workflow_flag->flag==21){?> Executive Director <?php } else { ?> Personnel Administration<?php } ?></label>
                    </div>
                  </div>
                <?php } ?>


<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
  <div class="col-md-12 text-left" >
  <span style="border-bottom:Solid 2px #000; ">Any other information/remarks relevant in this case:</span>
</div>
 <div class="col-md-4 text-left">
   <strong>Checked (Accountant Location)</strong> <br>
    <p>Signature:  
      <?php 
      if(!empty($workflow_flag->flag))
      {
      if($workflow_flag->flag>=15)
    {
                            if(!empty($finance_detail->encrypted_signature)) {
                              $image_path='';
                              $image_path=FCPATH.'datafiles\signature/'.$finance_detail->encrypted_signature;

                              if (file_exists($image_path)) {




                                ?>
                                <img src="<?php echo site_url().'datafiles\signature/'.$finance_detail->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                              <?php }
                              else
                              {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">

                                <?php
                              } } }  }?>
 </p>
    <p>Name: <label class="inputborderbelow"> <?php 
   //  print_r($finance_detail);
    //die;
    if(!empty($finance_detail->name))echo $finance_detail->name; ?></label> </p> </p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?></label> </p>
    <p>Place:<label class="inputborderbelow"> <?php if(!empty($finance_detail->officename))echo $finance_detail->officename; ?></label> </p>

 </div>
 <div class="col-md-4 text-left">
   <strong>Checked (Finance-Personnel-MIS Unit)</strong> <br>
    <p>Signature:<?php 
    if(!empty($workflow_flag->flag))
    {
      if($workflow_flag->flag>=17)
    
    {
                            if(!empty($ho_detail->encrypted_signature)) {
                              $image_path='';
                              $image_path=FCPATH.'datafiles\signature/'.$ho_detail->encrypted_signature;

                              if (file_exists($image_path)) {




                                ?>
                                <img src="<?php echo site_url().'datafiles\signature/'.$ho_detail->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                              <?php }
                              else
                              {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">

                                <?php
                              } } } }?> </p>
                              <?php //print_r($ho_detail); die;?>
    <p>Name: <label class="inputborderbelow"> <?php if(!empty($ho_detail->name))echo $ho_detail->name; ?></label> </p></p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?> </p>
    <p>Place:<label class="inputborderbelow"> <?php if(!empty($ho_detail->officename))echo $ho_detail->officename; ?></label> </p> </p>

 </div>
<div class="col-md-4 text-left">
   <strong>Checked (Dealing Person in Finance-Personnel-MIS Unit)</strong> <br>
    
    <p>Signature: _________________________________ </p>
    <p>Name: _________________________________ </p>
    <p>Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?> </p>
    <p>Place: _________________________________ </p>

 </div>


</div>

</div>
<div class="panel-footer text-right">
  <?php
  // print_r($tbl_check_sheet);
  // die;
   if(empty($tbl_check_sheet)){
  ?>
  <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm">
  <input type="button" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
  <?php
    }else if($tbl_check_sheet->flag == 0){
 ?>
  <input type="submit" name="Save" id="Save" value="Save" class="btn btn-success btn-sm">
  <input type="button" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
<?php } else if($tbl_check_sheet->flag == 1 && $this->loginData->RoleID==20){ ?>
 <a href="<?php echo site_url("Staff_finance_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } else if($tbl_check_sheet->flag == 1 && $tbl_check_sheet->isduplicate == 1 && $this->loginData->RoleID==22) {?>

<input type="submit" name="d_submit" id="d_submit" value="Submit" class="btn btn-success btn-sm">

  <a href="<?php echo site_url("Staff_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } 

else if($tbl_check_sheet->flag == 1 && $tbl_check_sheet->isduplicate == 1 && $this->loginData->RoleID==22) {?>

<input type="submit" name="d_submit" id="d_submit" value="Submit" class="btn btn-success btn-sm">

 
<?php } 

  else if($this->loginData->RoleID==17 && $tbl_check_sheet->flag == 1){ ?>
  <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
<?php } else if($this->loginData->RoleID==3 && $tbl_check_sheet->flag == 1) {?>
<a href="<?php echo site_url("Staff_approval_process");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
  
<?php } ?>
</div>
</div>
</div>
</section>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="hidemodal();">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments"  > </textarea>
              <?php echo form_error("comments");?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Status <span style="color: red;" >*</span></label>

                        <select name="status" id="status" class="form-control" > 
                          <option value="">Select</option>
                        <?php if($this->loginData->RoleID == 20){ ?>
                          <option value="15">Approved</option>
                          <option value="16">Rejected</option>
                          
                        <?php }
                          else if($this->loginData->RoleID == 22){
                         ?>
                           <option value="17">Approved</option>
                       <?php } ?>
                        </select>
                        <?php echo form_error("status");?>
                      </div>
            <div id="executivedirector_admin" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none;" >
                      <label for="Name"><?php if($this->loginData->RoleID == 22) {?> Head OfficeFiance<?php } else {?> Personal Administator <?php } ?> <span style="color: red;" >*</span></label>

                        <select name="personal_administration" id="personal_administration" class="form-control" >
                          <option value="">Select</option>
                         <!--  <?php //foreach ($ho_detail as $key => $value) { ?> -->
                         
                         <?php if($this->loginData->RoleID == 20) {?>
                          <option value="<?php echo $ho_detail->staffid;?>">
                            <?php echo $ho_detail->name.' - '.'('.$ho_detail->emp_code.')'; 

                            ?>
                              
                            </option>
                          <?php }  else if($this->loginData->RoleID == 22) {?>
                            <option value="<?php echo $personaldetail[0]->staffid;?>">
                              <?php echo $personaldetail[0]->name; 

                            ?>
                            </option>

                            <?php }?>
                          
                          </select>
                        <?php echo form_error("personal_administration");?>
                      </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hidemodal();">Close</button>
          <input type="button" value="Save" class="btn btn-success btn-sm" onclick="acceptancereginationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
<script type="text/javascript">

function sumOfIncome2() {
  var sum = 0;
  $('.txtNumeric2').each(function() {
    sum += parseInt(this.value) || 0;
    
  });
  $('#payments_due_to_employee').val(sum);
}

$('.txtNumeric2').change(function() {
  sumOfIncome2();
})

function sumOfIncome1() {
  var sum = 0;
  $('.txtNumeric1').each(function() {
    sum += parseInt(this.value) || 0;
    var amounts_recoverable=$("#amounts_recoverable").val();
  
  });
  $('#amounts_recoverable').val(sum);
}

$('.txtNumeric1').change(function() {
  sumOfIncome1();
  var amounts_recoverable=$("#amounts_recoverable").val();
  var payments_due_to_employee=$("#payments_due_to_employee").val();
  var sub=parseInt(payments_due_to_employee)-parseInt(amounts_recoverable);
  $("#net_payable_recoverable").val(sub);

})



  $('#status').change(function(){
    var statusval = $('#status').val();
    /*alert('sdfsaf');
    alert(statusval);*/

        if(statusval == 15  || statusval == 17 ) {
            $('#executivedirector_admin').show(); 
            $('#executivedirector_administration').attr('disabled',false)
            $('#personnel_administration').prop('required', true); 

        } else {
            $('#executivedirector_admin').hide(); 
              $('#executivedirector_administration').attr('disabled',true)
            $('#executivedirector_administration').prop('required', false); 

        } 
    });


function acceptanceregination(){
  var date_resignation_submitted_7 = document.getElementById('date_resignation_submitted_7').value;
  var date_from_which_separation_takes_effect_8 = document.getElementById('date_from_which_separation_takes_effect_8').value; 
  var short_fall_in_notice_period_11 = document.getElementById('short_fall_in_notice_period_11').value; 
  var what_date_13 = document.getElementById('what_date_13').value; 
  var amount7 = document.getElementById('amount7').value; 
  var amount8 = document.getElementById('amount8').value; 
  var amount9 = document.getElementById('amount9').value; 
  var amount10 = document.getElementById('amount10').value; 
  var amount11 = document.getElementById('amount11').value; 
  var amount12 = document.getElementById('amount12').value; 
  var amount13 = document.getElementById('amount13').value;
  var payments_due_to_employee = document.getElementById('payments_due_to_employee').value;
  var amounts_recoverable = document.getElementById('amounts_recoverable').value;
  var net_payable_recoverable = document.getElementById('net_payable_recoverable').value;
  
  if(net_payable_recoverable == ''){
    $('#net_payable_recoverable').focus();
  }
  if(amounts_recoverable == ''){
    $('#amounts_recoverable').focus();
  }
  if(payments_due_to_employee == ''){
    $('#payments_due_to_employee').focus();
  }
  if(amount13 == ''){
    $('#amount13').focus();
  }
  if(amount12 == ''){
    $('#amount12').focus();
  }
  if(amount11 == ''){
    $('#amount11').focus();
  }
  if(amount10 == ''){
    $('#amount10').focus();
  }
  if(amount9 == ''){
    $('#amount9').focus();
  }
  if(amount8 == ''){
    $('#amount8').focus();
  }
  if(amount7 == ''){
    $('#amount7').focus();
  }
  if(what_date_13 == ''){
    $('#what_date_13').focus();
  }
  if(short_fall_in_notice_period_11 == ''){
    $('#short_fall_in_notice_period_11').focus();
  }
  if(date_from_which_separation_takes_effect_8 == ''){
    $('#date_from_which_separation_takes_effect_8').focus();
  }
  if(date_resignation_submitted_7 == ''){
    $('#date_resignation_submitted_7').focus();
  }
  
  if(date_resignation_submitted_7 != '' && date_from_which_separation_takes_effect_8 !='' && short_fall_in_notice_period_11 !='' && what_date_13 !='' && amount7 !='' && amount8 !='' && amount9 !='' && amount10 !='' && amount11 !='' && amount12 !='' && amount13 !='' && payments_due_to_employee !='' && amounts_recoverable !='' && net_payable_recoverable !=''){
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
}
function acceptancereginationmodal(){
  //alert('dkfhsk');
  var comments = document.getElementById('comments').value;
  var acceept = document.getElementById('status').value;
  var personal_administration = document.getElementById('personal_administration').value;
  if(acceept == ''){
    $('#status').focus();
  }else if(acceept == 15 && personal_administration ==''){
    $('#personal_administration').focus();
  }
  if(comments.trim() == ''){
    $('#comments').focus();
  }


  if(comments.trim() !='' && acceept != '' && personal_administration !=''){
    document.forms['formsepchecksheet'].submit();
  }
}

  $(document).ready(function(){
    $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    //maxDate: 'today',
    yearRange: '1920:2030',
    dateFormat : 'dd/mm/yy',
    //defaultDate: new Date(2018, 00, 01)
    });
  });
</script>
<?php  
// echo "<pre>";
// print_r($tbl_check_sheet);
// die;
if($tbl_check_sheet){if($tbl_check_sheet->flag == 1 && ($tbl_check_sheet->isduplicate==0 || empty($tbl_check_sheet->isduplicate))){ ?>

<script type="text/javascript">
  $(document).ready(function(){
     $('#btnduplicate').show() ;   
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);
  $("form select").prop("disabled", true);
  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");
  });

  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
</script>
<?php }
else
{
  ?>
  <script type="text/javascript">
   $('#btnduplicate').hide() ; 
   $("form input[type=text]").prop("disabled", false);
  $("form select").prop("disabled", false);
  $("form input[type=radio]").prop("disabled", false);
  $("form textarea").prop("disabled", false);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");

   </script>
  <?php
}


} ?>