<section class="content" style="background-color: #FFFFFF;" >
  <?php // foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "index"){ ?>

   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');
  

   if(!empty($tr_msg)){ ?>
    <div class="col-md-12">
      <div class="hpanel">
        <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
            </div>
          </div>
        <?php } ?>
        <br>
        <div class="container-fluid">
          <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-10 panel-title pull-left">Seperation Approval List</h4>


             </div>
             <hr class="colorgraph"><br>
           </div>
           <div class="panel-body">

            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tablecampus" class="table table-bordered table-striped wrapper">
                  <thead>
                   <tr>
                     <th style ="max-width:50px;" class="text-center">S. No.</th>
                     <th>Emp code</th>
                     <th>Name</th>
                     <th>Sender Name</th>
                     <th class="text-center">Type</th>
                     <th class="text-center">Status</th>
                     <th class="text-center">Request Date</th>
                     <th style ="max-width:50px;" class="text-center">Action</th>
                   </tr> 
                 </thead>
                 <tbody>
                  <?php
                  /*echo "<pre>";
                  print_r($getworkflowdetail); */
                  $i=0;
                  foreach ($getworkflowdetail as $key => $value) {
                     // $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                   ?>
                   <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->sendername; ?></td>
                      <td><?php echo $value->process_type;?></td>
                    <td class="text-center">

                      
                            <?php echo $value->flag; ?> 


                     </td>
                    <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($value->Requestdate);?></td>
                    <td class="text-center">

                       <?php  

                     //  echo $value->status;
                      // die;
                       if($value->status == 7) { ?>
                         <a class="btn btn-success btn-sm" title = "History." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                        <a title = "Click here to approve this request." data-toggle="tooltip" class="btn btn-primary btn-sm" data-toggle="tooltip" href="<?php echo site_url()."Ed_staff_approval/add/".$value->staffid.'/'.$value->r_id; ?>"><i class="fas fa-thumbs-up"></i></a>
                      <?php } 
                      else  if ($value->status == 7 ){
                       // if($value->status == 5) {
                        ?> 
                     
                       
                      <?php } ?>
                      <?php if(($value->status == 5 || $value->status == 7) && $value->trans_status == 'Retirement'){
                        ?>
                        <a class="btn btn-warning btn-sm" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepinformtionforsup/index/".$value->r_id;?>">INFORMATION FOR SUPERANNUATION</a>
                      <?php } 
                      if($value->status >= 28 && $value->level <= 3){ 
                     ?>
            <a class="btn btn-warning btn-sm" title = "Click here to 
             inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_sepwhomitmayconcernlevelthreetermination/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

            <a class="btn btn-warning btn-sm" title = "Click here to 
             inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>">RELEIVING LETTER</a>
         
            <?php }else if($value->level == 4 && $value->status >= 28){ 
              ?>
            
            <a class="btn btn-warning btn-sm" title = "Click here to 
             inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_sepwhomitmayconcernlevelfourtermination/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

            <a class="btn btn-warning btn-sm" title = "Click here to 
             inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>">RELEIVING LETTER</a>
            <?php } 
            else if(($value->level == 5 || $value->level == 6) && ($value->status >= 28)){ ?>
            
            <a class="btn btn-warning btn-sm" title = "Click here to 
             inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_sepwhomitmayconcernlevelsixtermination/index/".$value->transid;?>"><i class="fas fa-exchange-alt"></i></a>

            <a class="btn btn-warning btn-sm" title = "Click here to 
             inter-office memo (IOM) for transfer submitted." data-toggle="tooltip" href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>">RELEIVING LETTER</a>
            <?php }
          //  } 
            if($value->status==28)
            {
            ?>
            <a title = "Click here to approve this request." data-toggle="tooltip" class="btn btn-primary btn-sm" data-toggle="tooltip" href="<?php echo site_url()."Ed_staff_approval/add/".$value->staffid.'/'.$value->r_id; ?>"><i class="fas fa-thumbs-up"></i></a>
          <?php } ?>
                        <!-- <a class="btn btn-warning btn-sm" title = "Click here to 
                        sample inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepexperiencecertificate/index/".$value->transid;?>">EXPERIENCE CERTIFICATE</a>
                        <a class="btn btn-warning btn-sm" title = "Click here to 
                        sample inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Staff_sepreleasefromservice/index/".$value->transid;?>">RELEIVING LETTER</a> -->
                        <?php 
                      //}
                      ?>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-10 panel-title pull-left">Probation</h4>
         </div>
         <hr class="colorgraph"><br>
       </div>
       <div class="panel-body">
        <div class="row" style="background-color: #FFFFFF;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
            <table id="tablecampus1" class="table table-bordered table-striped wrapper">
              <thead>
               <tr>
                 <th style ="max-width:50px;" class="text-center">S. No.</th>
                 <th>Emp code</th>
                 <th>Name</th>
                 <th>Sender Name</th>
                 <th class="text-center">Supervisor Request</th>
                 <th class="text-center">Stage</th>
                 <th class="text-center">Request Date</th>
                 <th style ="max-width:50px;" class="text-center">Action</th>
               </tr> 
             </thead>
             <?php if (count($getworkflowdetailprobation)==0) {?>
                      <tbody>
                        <tr>
                          <td colspan="8" style="color:red;" >Record not found!!!!</td>
                        </tr>
                      </tbody>

                    <?php }else { ?>
             <tbody>
              <?php
              /*echo "<pre>";*/
                   //print_r($getworkflowdetailprobation);
              $i=0;

              foreach ($getworkflowdetailprobation as $key => $value) {


                $date = explode(" ",$value->Requestdate);
                     // $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td><?php echo $value->emp_code;?></td>
                  <td><?php echo $value->name;?></td>
                  <td><?php echo $value->sendername; ?></td>
                  <td class="text-center">
                   <?php if ($value->Request == 'Probation - Complete') { ?>
                          <span data-toggle="tooltip" class="badge badge-pill badge-success col-md-12"  >
                            <?php echo $value->Request; ?>  
                          </span>

                          <?php } elseif ($value->Request == 'Probation - Extend') {?>
                          <span data-toggle="tooltip" class="badge badge-pill badge-info col-md-12" >
                            <?php echo $value->Request; ?>  
                          </span>
                        <?php } elseif ($value->Request == 'Pending') {?>
                              <span data-toggle="tooltip" class="badge badge-pill badge-warning col-md-12"  >
                                <?php echo $row->Request; ?>  
                              </span>
                          <?php } elseif ($value->Request != '') { ?>

                          <span data-toggle="tooltip" class="badge badge-pill badge-danger col-md-12" >
                            <?php echo $value->Request; ?>  
                          </span>
                          
                          <?php } ?>


                   
 </td>
                  <td class="text-center">

                    <?php if($value->status==1 )
                          { ?>
                            <span data-toggle="tooltip" class="badge badge-pill badge-info col-md-12" >
                              <?php echo $value->flag; ?> 
                            </span> 
                            <?php }elseif ($value->status==4 or $value->status==6 or $value->status==8) { ?>  
                            <span data-toggle="tooltip" class="badge badge-pill badge-info col-md-12" >
                              <?php echo $value->flag; ?> 
                            </span> 
                            <?php }elseif ($value->status==3 || $value->status==5 || $value->status==7) { ?>  
                            <span data-toggle="tooltip" class="badge badge-pill badge-info col-md-12"  >
                              <?php echo $value->flag; ?> 
                            </span> 
                            <?php }elseif ($value->status==9) { ?>  
                            <span data-toggle="tooltip" class="badge badge-pill badge-info col-md-12"  >
                              <?php echo $value->flag; ?> 
                            </span> 
                            <?php }?>

                    </td>

                  <td class="text-center"><?php echo $this->gmodel->changedatedbformate($date[0]);?></td>
                  <td class="text-center ">
                    <?php if(($value->status == 4 || $value->status == 3) &&  $value->Request != ''){ ?>
                      <a title = "Click here to approve this request submitted (Review)." class="btn btn-primary btn-sm" href="<?php echo site_url()."Probation_edirector_reviewofperformance/view/".$value->transid;?>" > <i class="fa fa-clipboard-check" aria-hidden="true"></i></a>
                    <?php } elseif($value->status >= 5 &&  $value->Request != '')
                        { ?>
                      <a title = "Click here to approve this request submitted (View)." class="btn btn-primary btn-sm" href="<?php echo site_url()."Probation_edirector_reviewofperformance/view/".$value->transid;?>" ><i class="fa fa-eye" aria-hidden="true" id="usedbatchid"></i></a>
                    <?php }?>



                
                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
              <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
    
  <?php //} } ?>
  <b><?php echo $this->session->flashdata('er_msg');?>.</b>
  <!--  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">MIdterm Review</h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <div class="panel-body">
    <div class="row" style="background-color: #FFFFFF;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
        <table id="tablecampus2" class="table table-bordered table-striped wrapper">
          <thead>
           <tr>
             <th style ="max-width:50px;" class="text-center">S. No.</th>
             <th>Emp code</th>
             <th>Name</th>
             <th>Sender Name</th>
             <th class="text-center">Status</th>
             <th class="text-center">Request Status</th>
             <th class="text-center">Request Date</th>
             <th style ="max-width:50px;" class="text-center">Action</th>
           </tr> 
         </thead>
         <tbody>
          <?php
            // echo "<pre>";
            //   print_r($getworkflowdetailmidtermreview);
            //   die;
         // $i=0;

        //  foreach ($getworkflowdetailmidtermreview as $key => $value) {
                     // $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
           ?>
           <tr>
            <td class="text-center"><?php //echo $i+1;?></td>
            <td><?php //echo $value->emp_code;?></td>
            <td><?php //echo $value->name;?></td>
            <td><?php //echo $value->sendername; ?></td>
            <td class="text-center"><?php 
             
             // if($value->status==1||$value->status==3||$value->status==5||$value->status==7||$value->status==9) {
          // echo  "<span class='badge badge-pill badge-info col-md-12'>".$value->flag."</span>"; }
          // else if($value->status==4||$value->status==6||$value->status==8) {
         //  echo  "<span class='badge badge-pill badge-danger col-md-12'>".$value->flag."</span>"; }
           ?></td>
            <td class="text-center"><?php //echo $value->process_type ;?></td>
            <td class="text-center"><?php //echo $value->Requestdate;?></td>
            <td class="text-center">
              <?php //if($value->status == 3){  ?>
                 <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-sm" href="<?php //echo site_url()."Probation_ed_reviewofperformance/view/".$value->id;?>" >Approval</a> -->
                <!-- <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip" href="<?php //echo site_url()."Probation_ed_reviewofperformance/view/".$value->id;?>"><i class="fas fa-thumbs-up"></i></a> -->
              <?php //} 
              // else if($value->status >= 5 && $value->status<=9 )
              // {
                ?>

     
                <!-- <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-sm" href="<?php //echo site_url()."Probation_ed_reviewofperformance/demoview/".$value->staffid.'/'.$value->id;?>" >Approved</a> -->
               <!--  <a title = "Click here to approve this request." class="btn btn-primary btn-sm"  data-toggle="tooltip" href="<?php //echo site_url()."Probation_ed_reviewofperformance/demoview/".$value->staffid.'/'.$value->id;?>"><i class="fas fa-thumbs-up"></i></a> -->
                <?php

            //  }


 
              ?>
          <!--   </td>
          </tr>
          <?php //$i++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>  -->   --> 
<?php //} } ?>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable({ 
    "paging": true,
    "search": true,
  });

     $('#tablecampus1').DataTable({ 
    "paging": true,
    "search": true,
  });

      $('#tablecampus2').DataTable({ 
    "paging": true,
    "search": true,
  });
  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){

    //   $('[data-toggle="tooltip"]').tooltip();  
    // var groupColumn = 4;
    //           var table = $('#tablecampus').DataTable({
    //             "columnDefs": [
    //             { "visible": false, "targets": groupColumn }
    //             ],
    //             "order": [[ groupColumn, 'asc' ]],
    //             "displayLength": 7,
    //             "drawCallback": function ( settings ) {
    //               var api = this.api();
    //               var rows = api.rows( {page:'current'} ).nodes();
    //               var last=null;

    //               api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
    //                 if ( last !== group ) {
    //                   $(rows).eq( i ).before(
    //                     '<tr class="group font-weight-bold text-uppercase"><td colspan="7">'+group+'</td></tr>'
    //                     );

    //                   last = group;
    //                 }
    //               } );
    //             }
    //           } );

    // var groupColumn = 4;
    //           var table = $('#tablecampus1').DataTable({
    //             "columnDefs": [
    //             { "visible": false, "targets": groupColumn }
    //             ],
    //             "order": [[ groupColumn, 'asc' ]],
    //             "displayLength": 7,
    //             "drawCallback": function ( settings ) {
    //               var api = this.api();
    //               var rows = api.rows( {page:'current'} ).nodes();
    //               var last=null;

    //               api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
    //                 if ( last !== group ) {
    //                   $(rows).eq( i ).before(
    //                     '<tr class="group font-weight-bold text-uppercase"><td colspan="7">'+group+'</td></tr>'
    //                     );

    //                   last = group;
    //                 }
    //               } );
    //             }
    //           } );

    //  var groupColumn = 4;
    //           var table = $('#tablecampus2').DataTable({
    //             "columnDefs": [
    //             { "visible": false, "targets": groupColumn }
    //             ],
    //             "order": [[ groupColumn, 'asc' ]],
    //             "displayLength": 7,
    //             "drawCallback": function ( settings ) {
    //               var api = this.api();
    //               var rows = api.rows( {page:'current'} ).nodes();
    //               var last=null;

    //               api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
    //                 if ( last !== group ) {
    //                   $(rows).eq( i ).before(
    //                     '<tr class="group font-weight-bold text-uppercase"><td colspan="7">'+group+'</td></tr>'
    //                     );

    //                   last = group;
    //                 }
    //               } );
    //             }
    //           } );


  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>