<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;
  
}
textarea{
  width: 600px !important;
}


</style>
<section class="content" style="background-color: #FFFFFF;" >
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <form role="form" method="post" action="">
        <br>
        <div class="container-fluid">
         <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <!-- <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - THE EXIT INTERVIEW FORM
             </h4>
           </div>
           <hr class="colorgraph"><br>
         </div> -->
         <div class="panel-body">
           <!-- <div class="row">
             <div class="col-md-12 text-center">
              <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
            </div>
            <div class="col-md-12 text-center" style="margin-bottom: 40px;">
              <h4>Inter-Office Memo</h4>
              
            </div> 
          </div> -->
          <!-- <div class="row" style="line-height: 2">
            <div class="col-md-9">
              To: <label  name="employee_name"  class="inputborderbelow" > <?php echo $staff_detail->name; ?></label> <br/>
              (Name of the Separating Employee)
            </div>
            <div class="col-md-3 text-left">
              Date: <label  name="date"  class="inputborderbelow" ><?php echo date('d/m/Y')  ?></label>
            </div>
          </div> -->
          

          <!-- <div class="row" style="line-height: 2">
            <div class="col-md-9">
              From: <label  name="employee_name"  class="inputborderbelow" > <?php echo $tcdetail->name; ?></label> <br/>
             (Team Coordinator)
            </div>
            <div class="col-md-3 text-left">
              File: Personal File of Apprentice
            </div>
          </div> -->
          <!-- <div class="row" style="line-height: 2;margin-top: 50px;">
            <div class="col-md-12 text-center">
             Subject  : <strong>The Exit Interview Form </strong>
           </div>
           
         </div> -->
         <!-- <div class="row" style="margin-top: 50px;">
           <div class="col-md-12 text-left">
            <p>As you plan to leave PRADAN soon, I write to request you to fill in the enclosed ‘Exit Interview Form’. We hope, through the information you would provide us in this format, to seek feedback about issues related to the organization and inputs for possible measures to change any aspect of the organization's functioning.

            </p> 
            <p>I would like to reassure you that we would treat the contents of this form as confidential, if you so desire, and use it only for organizational learning.</p>

            <p>
              We do hope that you make a smooth transition to your new assignment, and sincerely wish you the very best in your life.
            </p>
            <p>
              With regards.
            </p>
            <p style="margin-top: 50px;">
             ( __________________________ )
           </p>
           <p>
            Encl:<strong> As above</strong>
          </p>


        </div>
        
      </div> -->


    <hr class="colorgraph"><br>
      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 40px;">
        <h4>THE EXIT INTERVIEW FORM</h4>
        <p>(to be filled by Employees on Resignation)</p>
      </div> 
    </div>
    <div class="row" style="line-height: 2">
      <div class="col-md-9">
       
        Name of the Apprentice: <input type="text" name="employee_name" value="<?php echo $staff_detail->name; ?>" class="inputborderbelow" required>
      </div>
      <div class="col-md-3 text-left">
        Location: <input type="text" name="employee_location" class="inputborderbelow" value="<?php echo $staff_detail->newoffice;?>" >
      </div>     
     
    
    <div class="col-md-11 text-right">
      Date of Separation: <input type="text" name="employee_code" value="<?php echo  $this->gmodel->changedatedbformate($join_relive_data->leavingdate); ?>" class="inputborderbelow datepicker" required>
    </div>
    <div class="col-md-12">
     Date of Joining: <label class="inputborderbelow" > <?php echo $this->gmodel->changedatedbformate($join_relive_data->joniningdate); ?></label>
   </div>
  
  <div class="col-md-12 text-left">
   Would you like the contents to be confidential?:  
   <input type="radio" name="content_confedential" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->contents_confidential == 1) echo "checked"; } ?> required> Yes <input type="radio" name="content_confedential" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->contents_confidential == 0) echo "checked"; } ?> required> No
 </div> 

</div>
<div class="row" style="line-height: 2 ; margin-top: 50px;">

  <div class="col-md-12" style="margin-top: 20px;">
    1.  An experience or two in this organization that has left an almost indelible imprint on your mind.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" cols="50" name="question1" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->an_experience_1;} ?>
    </textarea>
    <!-- <input type="radio" name="question1" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->an_experience_1 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question1" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->an_experience_1 == 0) echo "checked"; } ?> required> No -->
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    2.  Any learning opportunities that this organization provided that you think helped you grow both as a person as well as a professional.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question2" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_learning_2;} ?>
    </textarea>
    <!-- <input type="radio" name="question2" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_learning_2 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question2" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_learning_2 == 0) echo "checked"; } ?> required> No -->
  </div>

  <div class="col-md-12" style="margin-top: 20px;">
    3.  Specifically, two characteristics that you think are the strengths of this organization.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question3" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->specifically_3;} ?>      
    </textarea>
    <!-- <input type="radio" name="question3" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->specifically_3 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question3" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->specifically_3 == 0) echo "checked"; } ?> required> No -->
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    4.  Two characteristics that you think are the shortcomings of this organization
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question4" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->two_characteristics_4;} ?>
    </textarea>
    <!-- <input type="radio" name="question4" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->two_characteristics_4 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question4" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->two_characteristics_4 == 0) echo "checked"; } ?> required> No -->
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    5.  Any experience in the organization in the past six months or so that has left you feeling bitter.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question5" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_experience_5;} ?>
    </textarea>
    <!-- <input type="radio" name="question5" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_experience_5 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question5" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_experience_5 == 0) echo "checked"; } ?> required> No -->
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    6.  Any experience in the organization in the past six months or so that has left you feeling happy.
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
    <textarea class="inputborderbelow" name="question6" cols="50" maxlength="255" required>
      <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_experience_6;} ?>
    </textarea>
   <!-- <input type="radio" name="question6" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_experience_6 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question6" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_experience_6 == 0) echo "checked"; } ?> required> No -->
 </div>
 <div class="col-md-12" style="margin-top: 20px;">
  7.  Any episode or incident in the organization in which you felt discriminated against.
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question7" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->any_episode_7;} ?>
  </textarea>
  <!-- <input type="radio" name="question7" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_episode_7 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question7" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->any_episode_7 == 0) echo "checked"; } ?> required> No -->
</div>
<div class="col-md-12" style="margin-top: 20px;">
  8.  Now, taking an overview, what could possibly be the factors/issues/circumstances that precipitated your decision to quit this organization?
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question8" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->taking_an_overview_8;} ?>
  </textarea>
  <!-- <input type="radio" name="question8" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->taking_an_overview_8 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question8" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->taking_an_overview_8 == 0) echo "checked"; } ?> required> No -->
</div>
<div class="col-md-12" style="margin-top: 20px;">
  9.  What are the factors that attract you to take up your proposed assignment?
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question9" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->proposed_assignment_9;} ?>
  </textarea>
  <!-- <input type="radio" name="question9" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->proposed_assignment_9 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question9" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->proposed_assignment_9 == 0) echo "checked"; } ?> required> No -->
</div>
<div class="col-md-12" style="margin-top: 20px;">
  10. If you have the choice to change the way things are in this organization, what would two such things be that you may like to focus on?
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question10" cols="100" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->such_things_be_that_you_may_like_to_focus_on_10;} ?>
  </textarea>
  <!-- <input type="radio" name="question10" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->such_things_be_that_you_may_like_to_focus_on_10 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question10" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->such_things_be_that_you_may_like_to_focus_on_10 == 0) echo "checked"; } ?> required> No -->
</div>
<div class="col-md-12" style="margin-top: 20px;">
  11. Anything else that you might like to say before you leave this organization.
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <textarea class="inputborderbelow" name="question11" cols="50" maxlength="255" required>
    <?php if($staff_sepemployeeexitform){ echo $staff_sepemployeeexitform->you_leave_this_organization_11;} ?>
  </textarea>
  <!-- <input type="radio" name="question11" value="1" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->you_leave_this_organization_11 == 1) echo "checked"; } ?> required> Yes <input type="radio" name="question11" value="0" <?php if($staff_sepemployeeexitform){ if($staff_sepemployeeexitform->you_leave_this_organization_11 == 0) echo "checked"; } ?> required> No -->
</div>

</div>

<div class="row text-left" style="margin-top: 20px; ">
 <div class="col-md-12" style="margin-bottom: 20px; ">
   <strong>(Signature of the  Development Apprentice) </strong>
 </div>
 <div class="col-md-6 text-left">
    <?php 

      if(!empty($staff_detail->encrypted_signature))
        {

          $image_path=FCPATH.'datafiles/signature/'.$staff_detail->encrypted_signature;
          
          
          
                if (file_exists($image_path)) {

                  ?>
              <img src="<?php echo '/pradan/datafiles/signature/'.$staff_detail->encrypted_signature;?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
            <?php } else{
              ?>
              <img src="<?php echo site_url().'datafiles/40664b7391e7d9b01e313e520b790773.jpg';?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
              <?php 
            }
          }
          
        else 
        {
    ?>
     <img src="<?php echo site_url().'datafiles/40664b7391e7d9b01e313e520b790773.jpg';?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
   <?php } ?>


  
 </div>
 <div class="col-md-6 text-right">
   Date : <input type="text" name="date_below_sign" value="<?php if($staff_sepemployeeexitform){ echo date('d/m/Y', strtotime($staff_sepemployeeexitform->currentdate)); }else { echo date('d/m/Y'); } ?>" class="inputborderbelow" required> 
 </div>
 <div class="col-md-6 text-right">
   Place : <input type="text" name="date_below_sign" value="<?php echo $staff_detail->newoffice; ?>" class="inputborderbelow" required> 
 </div>

</div>
<hr/>
<?php if($this->loginData->RoleID == 2 || !empty($staff_sepemployeeexitform->observer_sign)){ ?>
  <div class="row text-left" style="margin-top: 20px; line-height: 2">
   <div class="col-md-12" style="margin-bottom: 20px; ">
     <strong>Observations (if any) of the Interviewer Conducting the Exit Interview (Team Coordinator/ Field Guide)</strong>
   </div>
   <div class="row text-left" style="margin-top: 20px; line-height: 2">
   <div class="col-md-12" style="margin-bottom: 20px;margin-left: 20px; ">
     <strong>Observation: </strong>
     <textarea class="inputborderbelow" name="observer_observation" cols="50" maxlength="255" required>
     
     <?php
      if($staff_sepemployeeexitform){
       if($staff_sepemployeeexitform->observer_observation){
        echo $staff_sepemployeeexitform->observer_observation;
       }}
     ?>
   </textarea>
     
   </div>
   <div class="col-md-12" style="margin-bottom: 20px;margin-left: 20px; ">
    <p>Recommendation: (If s/he wants to re-join PRADAN)</p>
    <textarea class="inputborderbelow" name="observer_recommendation" cols="50" maxlength="255" required>
     
     <?php
      if($staff_sepemployeeexitform){
       if($staff_sepemployeeexitform->observer_recommendation){
        echo $staff_sepemployeeexitform->observer_recommendation;
       }}
     ?>
   </textarea>
   </div>

   <div class="col-md-6 text-left">
     Signature:<?php 
    
     if(!empty($tcdetail->encrypted_signature))
        {
            
          $image_path='datafiles/signature/'.$tcdetail->encrypted_signature;
          
          
          
                if (file_exists($image_path)) {
                

                  ?>
              <img src="<?php echo site_url().'datafiles/signature/'.$tcdetail->encrypted_signature;?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
            <?php } else{
              ?>
              <img src="<?php echo site_url().'datafiles/40664b7391e7d9b01e313e520b790773.jpg';?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
              <?php 
            }
          }
          
        else 
        {
    ?>
     <img src="<?php echo site_url().'datafiles/40664b7391e7d9b01e313e520b790773.jpg';?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
   <?php } ?>

   </div>
   <div class="col-md-6 text-right">
     Place: <?php echo $tcdetail->officename ?>
   </div>
   <div class="col-md-6 text-left">
     Name: <span class="inputborderbelow"><?php echo $tcdetail->name; ?></span>
     <input type="hidden" name="observer_sign" value="<?php echo $tcdetail->staffid; ?>">
      <!-- <input type="text" name="observer_sign" class="inputborderbelow" value="
     <?php
      if($staff_sepemployeeexitform){
       if($staff_sepemployeeexitform->observer_sign){
        echo $staff_sepemployeeexitform->observer_sign;
       }else{ echo $tcdetail->name; }}else{ echo $tcdetail->name; } ?>" required> -->
   </div>
   </div>
   <div class="col-md-6 text-right">
     Date: <input type="text" name="observer_date" id="observer_date" value="<?php
      if($staff_sepemployeeexitform){
       if($staff_sepemployeeexitform->observer_date){
        echo $staff_sepemployeeexitform->observer_date;
       }else{ echo date('d/m/Y'); }}else{ echo date('d/m/Y'); } ?>" class="inputborderbelow" readonly required>
   </div>
   <div class="col-md-6 text-left">
     Designation: <span class="inputborderbelow"><?php echo $tcdetail->desname; ?></span>
   </div>
  </div>
  </div>
<?php } ?>

<div class="panel-footer text-right">
  <?php
  if(empty($staff_sepemployeeexitform)){
    ?>
    <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
    <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
    <?php
  }else{
    if($staff_sepemployeeexitform->flag == 0){
     ?>
     <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
     <?php } ?>
     <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
     <?php } ?>
     <?php if($this->loginData->RoleID == 2 && empty($staff_sepemployeeexitform->observer_sign)){ ?>
      <input type="submit" name="tcfeedback" id="tcfeedback" value="Submit feedback" class="btn btn-success btn-sm">
     <?php } ?>

     
     <!-- <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a> -->
   </div>
 </div>
</div>
</div>
</form>
</section>

<?php 
if($staff_sepemployeeexitform){
  if($staff_sepemployeeexitform->flag == 1){ ?>
  <script type="text/javascript">
    $(document).ready(function(){
     
      $('#saveandsubmit').hide() ;
      $("form input[type=text]").prop("disabled", true);

      $("form input[type=radio]").prop("disabled", true);
      $("#observer_date").prop("disabled", false);
      /*$("form textarea").prop("disabled", true);
      $("form textarea").css("width", "600px;");
      $("form textarea").attr("style", "width:600px;");*/
    });
  </script>
  <?php } }?>