

  <?php 


  foreach ($role_permission as $row) { if ($row->Controller == "Central_event" && $row->Action == "add"){ ?>
    <br>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="panel thumbnail shadow-depth-2 listcontainer" >
                <div class="panel-heading">
                  <div class="row">
                   <h4 class="col-md-8 panel-title pull-left">Mail to Supervisior</h4>
                   <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                  </div>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
              
             <div class="col-sm-12">
               <form name="centralevent" id="centralevent" method="post" action="">
            <div class="body">


                

            </div>
          
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Content <span style="color:red;">*</span></label>
                  <textarea class="form-control" id="content" name="content" rows="10" cols="900"><?php echo $message;?></textarea> 
                </div>
              </div>
            </div>
                
               
             </div>
                <div class="panel-footer text-right">
                   <button type="submit" class="btn btn-primary  btn-sm m-t-10 waves-effect" name="btnsevedata" id="btnsevedata" value="savedata" data-toggle="tooltip" title="Want to save your changes? Click on me.">Send </button>
                   
               </div>
           </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
<?php } } ?>

<script type="text/javascript">
  $(document).ready(function() {

  $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         minDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',
         onClose: function(selectedDate) {
        jQuery("#central_event_to_date").datepicker( "option", "minDate", selectedDate );
       
        }
         
    });

    $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         minDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',
          onClose: function(selectedDate) {
        jQuery("#central_event_from_date").datepicker( "option", "maxDate", selectedDate );
        }
    });




});
</script>