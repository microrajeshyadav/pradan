<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Event" && $row->Action == "edit"){ ?>
  <br>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

       
        <div class="row clearfix">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-10 panel-title pull-left">Edit Event</h4>
               </div>
               <hr class="colorgraph"><br>
             </div>
             <form method="POST" action="">
              <div class="panel-body">


                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Event</label>

                    <select name="eventname" id="eventname" class="form-control" required="required" >
                    <option value="">Select Event</option>
                    <?php if ($centralevent_details->id =='CENTRAL EVENT I') {?>
                      <option value="CENTRAL EVENT I" selected="selected">CENTRAL EVENT I</option>
                    <option value="MID-TERM DC EVENT" >MID-TERM DC EVENT</option>
                    <option value="CENTRAL EVENT II">CENTRAL EVENT II</option>
                  <?php   }elseif ($centralevent_details->id ==8) {?>
                    <option value="CENTRAL EVENT I">CENTRAL EVENT I</option>
                    <option value="MID-TERM DC EVENT" selected="selected">MID-TERM DC EVENT</option>
                    <option value="CENTRAL EVENT II">CENTRAL EVENT II</option>
                <?php  }elseif ($centralevent_details->id ==9) { ?>
                    <option value="CENTRAL EVENT I">CENTRAL EVENT I</option>
                    <option value="MID-TERM DC EVENT">MID-TERM DC EVENT</option>
                    <option value="CENTRAL EVENT II" selected="selected">CENTRAL EVENT II</option>
                  <?php } ?>
                  </select>
                   <!--  <input type="text" class="form-control" name="eventname" value="<?php //echo $centralevent_details->name;?>"> -->
                  </div>
                  <?php echo form_error("eventname");?>
                </div>

                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Batch</label>
                    <select name="batch" id="batch" class="form-control" >
                          <?php foreach($batchdetails as $row) {
                            if($row->id == $eventbatch_detail->batch){
                              ?>
                              <option value="<?php echo $row->id; ?>" selected><?php echo $row->batch; ?> </option>
                            <?php }else {?>
                              <option value="<?php echo $row->id; ?>" ><?php echo $row->batch; ?> </option>
                            <?php } } ?>
                          </select>
                  </div>
                  <?php echo form_error("batch");?>
                </div>

              </div>


              <div class="panel-footer text-right">
                <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                <a href="<?php echo site_url("Event/index");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>

    <!-- #END# Exportable Table -->

  <?php } } ?>
</section>