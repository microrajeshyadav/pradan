
<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Event" && $row->Action == "index"){ ?>
<br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-10 panel-title pull-left">Manage Events</h4>
                <div class="col-md-2 text-right">
                  <a data-toggle="tooltip" data-placement="bottom" title="Want to add new? Click on me." href="<?php echo site_url("Event/add/")?>" class="btn btn-primary btn-sm">Add Event</a>
                </div>
              </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">
              <div class="table-responsive">
                <?php //print_r($centralevent_details); die(); ?>
                <table id="tablecampus" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center">Id</th>
                      <th class="text-center">Events</th>
                      <th class="text-center">Batch</th>
                      <th class="text-center">Sent To Supervisior</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=0; foreach($centralevent_details as $row){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $row->name;?></td>
                      <td class="text-center"><?php echo $row->batchname;?></td>
                      <td class="text-center">
                        <?php if($row->id==7){?>
                        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new central event? Click on me." href="<?php echo site_url()."event/central_event1/".$row->id;?>" class="btn btn-primary btn-sm">click here</a>
                        <?php } 
                          else if($row->id==8)
                          {
                        ?>
                        <a data-toggle="tooltip" data-placement="bottom" title="Want to add new central event? Click on me." href="<?php echo site_url()."event/mid_term_dc_event/".$row->id;?>" class="btn btn-primary btn-sm">click here</a>
                        <?php } 
                        else if($row->id==9)
                        {
                        ?>
                      <a data-toggle="tooltip" data-placement="bottom" title="Want to add new central event? Click on me." href="<?php echo site_url()."event/central_event2/".$row->id;?>" class="btn btn-primary btn-sm">click here</a>
                      <?php } ?>

                      </td>
                      <td class="text-center"><?php if($row->isdeleted==0) { echo "Active"; }else{ echo "InActive";} ?></td>
                      <td class="text-center">
                        <a href="<?php echo site_url('Event/edit/'.$row->id);?>" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this event."><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> |
                        <a href="<?php echo site_url('Event/delete/'.$row->id);?>" id="eventdelete" onclick="return confirm_delete()" style="padding : 4px;" data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this event."><i class="fa fa-trash" style="color:red"></i></a>
                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>
<script>
   $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable();
      });
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this item?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>