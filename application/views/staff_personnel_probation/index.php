<section class="content">
	<br>
	<div class="container-fluid">
		<!---- Probation Start  -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel thumbnail shadow-depth-2 listcontainer" >
					<div class="panel-heading">
						<div class="row">
							<h4 class="col-md-10 panel-title pull-left"> Probation Review Initiate</h4>

						</div>
						<?php 
						$tr_msg= $this->session->flashdata('tr_msg');
						$er_msg= $this->session->flashdata('er_msg');

						if(!empty($tr_msg)){ ?>
							<div class="content">
								<div class="row">
									<div class="col-md-12">
										<div class="hpanel">
											<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
												<?php echo $this->session->flashdata('tr_msg');?>. </div>
											</div>
										</div>
									</div>
								</div>
							<?php } else if(!empty($er_msg)){?>
								<div class="content animate-panel">
									<div class="row">
										<div class="col-md-12">
											<div class="hpanel">
												<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
													<?php echo $this->session->flashdata('er_msg');?>. </div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								<hr class="colorgraph"><br>
							</div>
							<div class="panel-body">
								<table id="tbldesignations" class="table table-bordered table-striped table-hover dataTable js-exportable">
									<thead>
										<tr>
											<th class="text-center" style="width: 45px;">S.No.</th>
											<th>Employ Code</th>
											<th>Name</th>
											<th>Designation </th>
											<th>Level </th> 
											<th>Gender</th>
											<th>Office </th>
											<th class="text-center">Probation Date</th>
											<th class="text-center">Initiate</th>
										</tr>
									</thead>
									<?php if (count($hrd_staff_details) == 0) { ?>
										<tbody>
											<tr>
												<td colspan="15" style="color: red;">Record not found</td>
											</tr>
										</tbody>
									<?php }else{?>

										<tbody>
											<?php


											$i=0; foreach($hrd_staff_details as $row){ 	?>
												<tr>
													<td class="text-center"><?php echo $i+1; ?></td>
													<td ><?php echo $row->emp_code; ?></td>
													<td ><?php echo $row->name; ?></td>
													<td ><?php echo $row->desname; ?></td>
													<td ><?php echo $row->levelname; ?></td>
													<td ><?php echo $row->gender; ?></td>
													<td ><?php echo $row->officename; ?></td>
													<td class="text-center"><?php echo $this->gmodel->changedatedbformate($row->probation_date); ?></td>
													<td style="text-align: center;">
														<?php 
														if ($row->level !=0 && $row->probation_status==0) { ?>
															<a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Staff_personnel_probation_approval/add/".$row->staffid;?>" >Initiate</a>

															<!-- <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php //echo site_url()."Probation_ed_reviewofperformance/index/".$row->staffid;?>" >Probation Review</a> -->
														<?php  }else{ ?>
															<div class="badge badge-pill badge-success">
																<strong>Initiated </strong>
															</div>

														<?php } ?>
													</td>

												</tr>
												<?php $i++; } ?>
											</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
					</div>

					<!--  Provation End  -->


					<!-- #END# Exportable Table -->
				</div>


				<div class="container-fluid">
					<!---- Probation Start  -->
					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="panel thumbnail shadow-depth-2 listcontainer" >
								<div class="panel-heading">
									<div class="row">
										<h4 class="col-md-10 panel-title pull-left"> Probation Process</h4>

									</div>
									<hr class="colorgraph"><br>
								</div>

								<div class="panel-body">
									<table id="tblProbation" class="table table-bordered table-striped table-hover dataTable js-exportable">
										<thead>
											<tr>
												<th style ="max-width:50px;" class="text-center">S.No.</th>
												<th>Emp code</th>
												<th>Name</th>
												<th>Sender Name</th>
												<th class="text-center">Probation Date</th>
												<th class="text-center">Supervisor Request</th>
												<th class="text-center">Stage</th>


												<th style ="max-width:50px;" class="text-center">Action</th>
											</tr> 
										</thead>
										<?php if (count($getworkflowdetailprobation)==0) {?>
											<tbody>
												<tr>
													<td colspan="8" style="color:red;" >Record not found!!!!</td>
												</tr>
											</tbody>

										<?php }else { ?>
											<tbody>
												<?php
												$i=0;
												foreach ($getworkflowdetailprobation as $key => $value) {

										//$date = explode(" ",$value->Requestdate);
													$date =$value->Requestdate;
													
													?>
													<tr>
														<td class="text-center"><?php echo $i+1;?></td>
														<td><?php echo $value->emp_code;?></td>
														<td><?php echo $value->name;?></td>
														<td><?php echo $value->sendername; ?></td>
														<td class="text-center"><?php echo $this->gmodel->changedatedbformate($date);?></td>
														<td class="text-center">
															<?php if ($value->Request == 'Probation - Complete') { ?>
																<span class="badge badge-pill badge-success col-md-12"  >
																	<?php echo $value->Request; ?>  
																</span>

															<?php } elseif ($value->Request == 'Probation - Extend') {?>
																<span class="badge badge-pill badge-info col-md-12" >
																	<?php echo $value->Request; ?>  
																</span>
															<?php } elseif ($value->Request == 'Pending') {?>
																<span class="badge badge-pill badge-warning col-md-12"  >
																	<?php echo $row->Request; ?>  
																</span>
															<?php } elseif ($value->Request != '') { ?>

																<span class="badge badge-pill badge-danger col-md-12" >
																	<?php echo $value->Request; ?>  
																</span>
																
															<?php } ?>


															
														</td>
														<td class="text-center">

															<?php if($value->status==1 )
															{ ?>
																<span class="badge badge-pill badge-info col-md-12" >
																	<?php echo $value->flag; ?> 
																</span> 
															<?php }elseif ($value->status==4 or $value->status==6 or $value->status==8) { ?>  
																<span class="badge badge-pill badge-danger col-md-12" >
																	<?php echo $value->flag; ?> 
																</span> 
															<?php }elseif ($value->status==3 || $value->status==5 || $value->status==7) { ?>  
																<span class="badge badge-pill badge-primary col-md-12"  >
																	<?php echo $value->flag; ?> 
																</span> 
															<?php }elseif ($value->status==9) { ?>  
																<span class="badge badge-pill badge-success col-md-12"  >
																	<?php echo $value->flag; ?> 
																</span> 
															<?php }?>

														</td>


														<td class="text-center">
															<?php if($value->status==5 || $value->status==3){ ?>
																<a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Probation_personnel_reviewofperformance/view/".$value->r_id;?>" >Approval</a>
															<?php }elseif ($value->status==6) { ?>
																<span class="badge badge-pill badge-success">Rejected BY ED</span>

															<?php }elseif ($value->status==9) {?>
																<div class="badge badge-pill badge-success">
																	<strong>Approved</strong>
																</div>
															<?php	} ?>
														</td>

													</tr>
													<?php $i++; } ?>
												</tbody>
											<?php } ?>
										</table>
									</div>
								</div>
							</div>
						</div>

						<!--  Provation End  -->


						<!-- #END# Exportable Table -->
					</div>

				</section>
				<script>
					$(document).ready(function() {
						$('[data-toggle="tooltip"]').tooltip(); 
						$('#tbldesignations').DataTable({
							"paging": true,
							"search": true,
						});

						$('#tblProbation').DataTable({
							"paging": true,
							"search": true,
						});
					});
					function confirm_delete() {

						var r = confirm("Do you want to delete this Designations");

						if (r == true) {
							return true;
						} else {
							return false;
						}

					}
				</script>]