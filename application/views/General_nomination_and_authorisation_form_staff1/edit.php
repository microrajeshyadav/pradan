
<section class="content" style="background-color: #FFFFFF;">
  <br/>

  <!-- Exportable Table -->
  <?php //$page='Nomination_staff'; require_once(APPPATH.'views/Provident_fund_nomination_form_staff/topbar_staff.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">

     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">GENERAL  NOMINATION FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6>Professional Assistance for Development Action (PRADAN) </h6>              
          <h6>GENERAL  NOMINATION FORM</h6>
        </div>
         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                       <div class="form-group">
                        <div class="form-line">
                          
                           <p><?php echo $office_name->officename;?></p>
                         
                        </div>
                                              </div>
                    <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
                          PRADAN.</label></div>


      </div>
      <div class="form-group">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Sir, </label>
                  </div>
                  <div class="form-group">
                  <label class="field-wrapper required-field">Subject: General Nomination and Authorization. </label>
                </div>



      <?php 
   // print_r($candidatedetailwithaddress);
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>    
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>      
        <?php } else if(!empty($er_msg)){?>       
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>       
          <?php } ?>

          <?php //print_r($candidatedetailwithaddress);?>
          <div class="row">                   
            <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">               
              <label class="field-wrapper required-field" />

              <div class="form-group">
                I,  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->staff_name;?>" required="required">   (name in full) whose particulars are given below, hereby nominate the following person(s) to receive, in the event of my death, all monies payable to me by Professional Assistance for Development Action (PRADAN) or its successors or assignees, other than the monies on those accounts in respect of which specific nominations exist or are required to exist under the rules governing the relevant matters, and want that the monies first aforesaid shall be paid in proportion indicated against the name(s) of the nominee(s).
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table id="tblForm09" class="table" >
                <thead>
                 <tr class="bg-light">
                  <th colspan="7"> Nominee Details </th>
                </tr>  
                <tr>
                 <th>#</th>
                 <th>Name of the nominee in full</th>
                 <th>Nominee's Relationship with the Apprentice</th>
                 <th style="width: 150px;">Age of nominee</th>
                 <th style="width: 250px;">Full address of the nominee</th>
                 <th>Portion (Percentage of share of nominee) </th>
                 <th style="width: 180px;">If the Nominee is a Minor, Name & Address of the Guardian who may Receive the Amount During the Minority of Nominee
                 </th>
               </tr>
             </thead>
             <tbody>
              <?php  $i=0;
  foreach ($nomineedetail as $key => $val) {
    //print_r($val);

    
  ?>
   <tr>
                  <input type="hidden" value="<?php echo $val->nom_id;?>" name="data[<?php echo $i;?>][n_id]">
                   <input type="hidden" value="<?php echo $val->nomination_id;?>" name="data[<?php echo $i;?>][nomination_id]">
                   <td><input type="text" name="data[<?php echo $i;?>][sr_no]"value="<?php echo $val->sr_no;?>" class="form-control"></td>
                  <td><input type="text" name="data[<?php echo $i;?>][full_name_nominee]" id="full_name_nominee" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Full Name Nominee !" placeholder="Enter Family Member Name" required="required" value="<?php echo $val->nominee_name; ?>" ></td>
                  <td>
                 <?php 
                 $options = array('' => 'Select Relation');
                 foreach($sysrelations as$key => $value) {
                  $options[$value->id] = $value->relationname;
                }
                echo form_dropdown('data['.$i.'][relationship_nominee]', $options,  $val->nominee_relation, 'class="form-control" data-toggle="tooltip" title="Select Relation With Nominee !"  id="relationship_nominee" required="required"');
                ?>
                <?php echo form_error("relationship_nominee");?>
                  </td>
                  <td><input type="text" name="data[<?php echo $i;?>][age_nominee]" id="age_nominee" data-toggle="tooltip" title="Enter Age Of Nominee !" placeholder = "Enter Age Of Nominee " value="<?php echo $val->nominee_age; ?>" class="form-control txtNumeric" required="required" ></td>
                  <td>
                   <textarea name="data[<?php echo $i;?>][address_nominee]" data-toggle="tooltip"  title="Enter Full Address Nominee !" id="address_nominee"  placeholder = "Enter Full Address Nominee" class="form-control">
                     <?php echo trim($val->nominee_address); ?>
                   </textarea>
                 </td>
                 <td><input type="text" name="data[<?php echo $i;?>][share_nominee]" id="age_nominee" data-toggle="tooltip" title="Enter Share Of Nominee !" placeholder = "Enter share Of Nominee " value="<?php echo $val->share_nomine;?>" class="form-control txtNumeric" required="required" ></td>
                 <td><select name="data[<?php echo $i;?>][minior]" id="minior" data-toggle="tooltip" placeholder = "Enter Minior" title="Enter Minior !" class="form-control txtNumeric" required="required" >
                 <option value="">Please select </option>
                 <option value="1" <?php if($val->minior==1){?>selected <?php } ?>>Yes </option>
                 <option value="0" <?php if($val->minior==0){?>selected <?php } ?>>No </option>
                 </select>
                 </td>
               </tr>
<?php $i++; }   ?>
           </tbody>
         </table>
       </div>
     </div>
     <div class="row" style="margin-bottom: 50px;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label class="field-wrapper required-field">  This nomination and authorisation shall remain valid unless substituted by a fresh nomination and authorisation.</label>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-right"></div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> <strong>Yours sincerely</strong></div>
    </div>
    <div class="row" style="line-height: 3">        
     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
       <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >Location :</div>
       <input type="hidden" name="daplace" value="<?php echo $candidatedetailwithaddress->new_office_id;?>">
       <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" > <input type="text" name="daplace" id="daplace" placeholder=" Please Enter Place"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->officename;?>" required="required">  </div>
     </div>
     <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
     </div>
     <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right">  
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Name: </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"> <b>  <?php echo $candidatedetailwithaddress->staff_name;?>  </b>
      </div>
    </div>     
  </div>
  <div class="row" style="line-height: 3">    
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >Signature :</div>
     <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" > 
      <input type="hidden" name="candidate_sign" value="<?php echo $nominee->nomination_authorisation_signed;?>">
           <img src="<?php echo base_url().'datafiles\nominee/'.$nominee->nomination_authorisation_signed;?>">
     </div>
   </div> 
   <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
   </div>
   <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right">   
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Employee Code: </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <b><?php echo $candidatedetailwithaddress->emp_code;?> </b>
    </div>
  </div>    
</div>


<div class="row" style="line-height: 3">    
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Date : </div>
   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text" name="dadate" id="dadate" class="form-control datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php  echo $this->model->changedate($nominee->da_date);?>" required="required">  </div>
 </div>
 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
 </div>
 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right"> 
 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">Father/Husband Name </div>
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text" name="father_name" id="dadate" class="form-control datepicker" placeholder=" Please select Date"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->father_name;?>" required="required">  </div>   
   
 </div> 
 <div class="row" style="line-height: 3">    
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   
   
 </div>
 
 <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">  
 </div>
 <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 "> 
 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">Permanent Address : </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">  
    <textarea name="paddress" id="paddress" class="form-control" placeholder=" Please select permanent address"   >  <?php echo $candidatedetailwithaddress->permanenthno.$candidatedetailwithaddress->permanentstreet;?>
 </textarea>
  </div>   
   
 </div> 






 <?php 
             if($this->loginData->RoleID==2 || $this->loginData->RoleID==17)
            {?> 

 
 <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: center;">
     <h5>Declaration By Witness<br>

     (Person others than by nominee)
      </h5>
         <div class="form-group">
              The above nomination has been signed by Ms./Mr.  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">   son/daughter/wife of Mr. <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">in our presence.
            </div>
     </div>  


 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Signature : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text"  class="form-control " placeholder=" Please select signature"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">&nbsp;  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right"> Signature</div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <input type="text"  class="form-control " placeholder=" Please select signature"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required"></div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Name : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text"  class="form-control " placeholder=" Please select name"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">&nbsp;  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right"> Name</div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <input type="text"  class="form-control name" placeholder=" Please select name "  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required"></div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Address : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <textarea   class="form-control " placeholder=" Please select address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;"  required="required">&nbsp;</textarea>  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right"> Address</div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left" > <textarea    class="form-control " placeholder=" Please select address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;"  required="required">&nbsp;</textarea></div>
          </div>
          </div>
          


<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify; margin-left:20px;">
               
                cc-  Mr/Mrs  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name" readonly="readonly" style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required"> <br>          -Fiance -Personel-Mis Unit <br>
                -Personal Dossier Location
              </div>

    <?php } ?>











</div>


</div>

  <div class="panel-footer text-right" style="width:100%; margin: 15px;">
    <button  type="submit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save" name="savebtn" id="save" value="senddatasave">Save</button>
    <button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>

  
</div>
</form>
</div>
</div>
</div>
</section>

<script type="text/javascript">

  $(document).ready(function(){
    decimalData();
    $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

   //$('#pradan_selection_process_before_details').prop("disabled",true); 

 });

  $(function () {
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
  });
  

  $(document).ready(function(){
    $("#filladdress").on("click", function(){
     if (this.checked) { 
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());
    }
    else {
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

  });


  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit").prop('disabled', true);

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>  

