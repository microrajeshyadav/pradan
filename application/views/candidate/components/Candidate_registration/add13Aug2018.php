 <br>
 <style>
		
		#contact_submit{
			padding-left: 100px;
		}
		#contact div{
			margin-top: 1em;
		}
		textarea{
			vertical-align: top;
			height: 5em;
		}
			
		.error{
			display: none;
			margin-left: 10px;
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		
		input.invalid, textarea.invalid{
			border: 2px solid red;
		}
		
		input.valid, textarea.valid{
			border: 2px solid green;
		}
	</style>
  <div class="container" style="background-color: #FFFFFF;"  >
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php //print_r($candidatedetails); die;?>
          <div class="tab-content">    <!-- Start  Candidates Basic Info     -->
            <div id="home" class="tab-pane fade in active">
              <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" name="campusid" id="campusid" value="<?php echo $campusdecodeid; ?>">
                <input type="hidden" name="categoryid" id="categoryid" value="<?php  echo $campuscatid; ?>">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <table id="tbltrainingexposure" class="table table-bordered table-striped">
                    <thead>
                     <tr>
                      <th colspan="3" style="background-color: #3CB371; color: #fff;">Basic Info</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="3">
                        <div class="row">
                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">First Name <span style="color: red;">*</span></label>
                          <?php if ($method=='add') {?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control" value="" placeholder="Enter First Name" required="required">
                          <?php }else{ ?>
                           <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control" value="" placeholder="Enter First Name" required="required">
                         <?php  } ?>
                           <?php echo form_error("candidatefirstname");?>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Middle name </label>
                           <?php if ($method=='add') {?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control" value="" placeholder="Enter Middle Name" >
                          <?php }else{ ?>
                             <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control" value="" placeholder="Enter Middle Name" >
                         <?php  } ?>
                         <?php echo form_error("candidatemiddlename");?>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Last Name <span style="color: red;" >*</span></label>
                          <?php if ($method=='add') {?>
                          <input type="text"  minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="" required="required" >
                        <?php }else{ ?>
                           <input type="text"  minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="" required="required" >
                         <?php } ?>
                          <?php echo form_error("candidatelastname");?>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
                          <?php if ($method=='add') {?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control" value="" placeholder="Enter First Name" required="required" >
                        <?php }else{ ?>
                           <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control" value="" placeholder="Enter First Name" required="required" >
                         <?php } ?>
                          <?php echo form_error("motherfirstname");?>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Middle name </label>
                           <?php if ($method=='add') {?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control" value="" placeholder="Enter Middle Name" >
                        <?php }else{ ?>
                           <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control" value="" placeholder="Enter Middle Name" >
                         <?php } ?>
                          <?php echo form_error("mothermiddlename");?>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Last Name <span style="color: red;" >*</span></label>
                           <?php if ($method=='add') {?>
                          <input type="text" minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="" required="required">
                        <?php }else{ ?>
                           <input type="text" minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="" required="required">
                        <?php } ?>
                        <?php echo form_error("motherlastname");?>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Father's Name <span style="color: red;" >*</span></label>
                          <?php if ($method=='add') {?>
                        <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control" value="" placeholder="Enter First Name" required="required" >
                        <?php } else{?>
                           <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control" value="" placeholder="Enter First Name" required="required" >
                         <?php } ?>
                         <?php echo form_error("motherlastname");?>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Middle name </label>
                         <?php if ($method=='add') {?>
                        <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control" value="" placeholder="Enter Middle Name">
                        <<?php }else{ ?>
                          <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control" value="" placeholder="Enter Middle Name">
                        <?php } ?>
                      <?php echo form_error("fathermiddlename");?>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Last Name <span style="color: red;" >*</span></label>
                        <?php if ($method=='add') {?>
                        <input type="text" class="form-control" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="" required="required">
                      <?php }else{ ?>
                         <input type="text" class="form-control" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="" required="required">
                       <?php } ?>
                       <?php echo form_error("fatherlastname");?>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">Gender <span style="color: red;" >*</span></label>

                      <?php 
                      $options = array("1" => "Male", "2" => "Female");
                      echo form_dropdown('gender', $options, set_value('gender'), 'class="form-control" ');
                      ?>
                      <?php echo form_error("gender");?>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">Nationality  </label>
                      <?php 
                      $options = array("indian" => "Indian", "other" => "Other");
                      echo form_dropdown('nationality', $options,set_value('nationality'), 'class="form-control" ');
                      ?>
                      <?php echo form_error("nationality");?>
                    </div>


                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">Marital Status </label>
                      <?php 
                      $options = array("1" => "Single", "2" => "Married");
                      echo form_dropdown('maritalstatus', $options, set_value('maritalstatus'), 'class="form-control" ');
                      ?>
                      <?php echo form_error("maritalstatus");?>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                    <input type="text" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" name="dateofbirth"  id="dateofbirth" placeholder="Enter Date Of Birth " value="" required="required" >
                    <?php echo form_error("dateofbirth");?>
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Email Id <span style="color: red;" >*</span></label>
                    <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="" required="required" >
                    <?php echo form_error("emailid");?>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                    <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="" placeholder="Enter Mobile No" required="required" >
                    <?php echo form_error("mobile");?>
                  </div>
                </div>
                <br>
               <!--  <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Blood Group<span style="color: red;" >*</span></label>
                  <input type="text"  maxlength="10" data-toggle="tooltip" title="Blood Group !" name="bloodgroup" id="bloodgroup" class="form-control" data-country="India" value="" placeholder="Enter Blod Group " >
                  <?php //echo form_error("bloodgroup");?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left"></div>
                </div>
              </div> -->
            </td>
          </tr>
        </tbody>
      </table>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table id="tbledubackground" class="table table-bordered table-striped" >
        <thead>
         <tr>
          <th colspan="8" style="background-color: #3CB371; color: #fff;"> 
             Communication Address 
          
          </th>
        </tr> 
        <tr>
          <th class="text-center" colspan="4" style="vertical-align: top;">Present Mailing Address</th>
          <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Mailing Address<br>
           
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="filladdress"> 
              <label class="form-check-label" for="filladdress"><b>same as present address</b></label>
            </div>
         
          </th>
        </tr> 
      </thead>
      <tbody>
       <tr>
        <td> <label for="Name">H.No/Street<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="presentstreet" data-toggle="tooltip" title="Enter H.No/Street !" id="presentstreet" class="form-control" 
          value="" placeholder="Enter H.No/Street" required="required" ></td>

          <td> <label for="Name">City<span style="color: red;" >*</span></label> </td>

          <td><input type="text" name="presentcity" id="presentcity" class="form-control" placeholder="Enter City" value="" data-toggle="tooltip" title="Enter  City !"  required="required" ></td>

          <td><label for="Name">H.No/Street<span style="color: red;" >*</span></label></td>
          <td><input type="text" name="permanentstreet" data-toggle="tooltip" title="Enter H.No/Street !"  id="permanentstreet" class="form-control" value=""  placeholder="Enter H.No/Street" required="required" ></td>
          <td><label for="Name">City<span style="color: red;" >*</span></label></td>
          <td><input type="text" name="permanentcity" id="permanentcity" placeholder="Enter City" class="form-control" value="" data-toggle="tooltip" title=" Enter City !" required="required" ></td>
        </tr>
        <tr>
          <td><label for="Name">State<span style="color: red;" >*</span></label></td>
          <td>  <?php 

          $options = array('' => 'Select Present State');
          foreach($statedetails as$key => $value) {
            $options[$value->id] = $value->name;
          }
          echo form_dropdown('presentstateid', $options, set_value('presentstateid'), 'class="form-control" id="presentstateid" data-toggle="tooltip" title=" Select State !" ');
          ?>

          <?php echo form_error("presentstateid");?></td>
          <td><label for="Name">District<span style="color: red;" >*</span></label> </td>
          <td>
            <select name="presentdistrict" id="presentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
             <option value="">Select District </option>
           </select>
         </td>
         <td><label for="Name">State<span style="color: red;" >*</span></label></td>
         <td>  <?php 
         $options = array('' => 'Select Permanent State');
         foreach($statedetails as$key => $value) {
          $options[$value->id] = $value->name;
        }
        echo form_dropdown('permanentstateid', $options, set_value('permanentstateid'), 'class="form-control" id="permanentstateid" data-toggle="tooltip" title=" Select State !" ');
        ?>

        <?php echo form_error("permanentstateid");?></td>
        <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td>
          <select name="permanentdistrict" id="permanentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
            <option value="">Select District </option>
          </select>
        </td>
      </tr>
      <tr>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="presentpincode" id="presentpincode" class="form-control txtNumeric" value="" maxlength="6" data-toggle="tooltip" title=" Enter Pin Code !" required="required" placeholder="Enter PinCode"></td>
        <td> </td>
        <td></td>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentpincode" id="permanentpincode" class="form-control txtNumeric" value="" data-toggle="tooltip" title=" Enter Pin Code !" required="required" maxlength="6" placeholder="Enter PinCode" ></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>  


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">

  <table id="tbledubackground" class="table table-bordered table-striped">
    <thead>
     <tr >
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Educational Background</th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate</th>

      <th class="text-center" style="vertical-align: top;">Year </th>
      <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
      <th class="text-center" style="vertical-align: top;"> Board/ University</th>
      <th class="text-center" style="vertical-align: top;">Specialisation</th>
      <th class="text-center" style="vertical-align: top;">Place</th>
      <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><b>10th</b></td>

      <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
        id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" style="min-width: 20%;"  value="<?php echo set_value("10thpassingyear");?>" >
        <?php echo form_error("10thpassingyear");?></td>
        <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
          id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("10thschoolcollegeinstitute");?>" >
          <?php echo form_error("10thschoolcollegeinstitute");?></td>

          <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
            id="10thboarduniversity" name="10thboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("10thboarduniversity");?>" >
            <?php echo form_error("10thboarduniversity");?></td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
              id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("10thspecialisation");?>" >
              <?php echo form_error("10thspecialisation");?></td>


              <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo set_value("10thplace");?>" >
                <?php echo form_error("10thplace");?></td>

                <td><input type="text" class="form-control txtNumeric"  data-toggle="tooltip" title="Percentage !"
                  id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo set_value("10thpercentage");?>"  >
                  <?php echo form_error("10thpercentage");?></td>
                </tr>
                <tr>
                  <td><b>12th</b> </td>


                  <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                    id="12thpassingyear" name="12thpassingyear" placeholder="Enter Year" value="<?php echo set_value("12thpassingyear");?>" >
                    <?php echo form_error("12thpassingyear");?></td>

                    <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
                      id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("12thschoolcollegeinstitute");?>" >
                      <?php echo form_error("12thschoolcollegeinstitute");?></td>

                      <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                        id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("12thboarduniversity");?>" >
                        <?php echo form_error("12thboarduniversity");?></td>

                        <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                          id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("12thspecialisation");?>" >
                          <?php echo form_error("12thspecialisation");?></td>


                          <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                            id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo set_value("12thplace");?>" >
                            <?php echo form_error("12thplace");?></td>

                            <td><input type="text" class="form-control txtNumeric" 
                              id="12thpercentage" name="12thpercentage" placeholder="Percentage" value="<?php echo set_value("12thpercentage");?>" >
                              <?php echo form_error("12thpercentage");?></td>
                            </tr>


                            <tr>
                              <td><b>UG</b></td>

                              <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                id="ugpassingyear" name="ugpassingyear" placeholder="Enter Year" value="<?php echo set_value("ugpassingyear");?>" >
                                <?php echo form_error("ugpassingyear");?></td>

                                <td>
                                  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
                                  id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute " value="<?php echo set_value("ugschoolcollegeinstitute");?>" >
                                  <?php echo form_error("ugschoolcollegeinstitute");?>
                                </td>

                                <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                                  id="ugboarduniversity" name="ugboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("ugboarduniversity");?>" >
                                  <?php echo form_error("ugboarduniversity");?></td>
                                  <td>
                                   <?php
                                   $options = array('' => 'Select');
                                   foreach($ugeducationdetails as $key => $value) {
                                    $options[$value->ugname] = $value->ugname;
                                  }
                                  echo form_dropdown('ugspecialisation', $options, set_value('ugspecialisation'), 'class="form-control" data-toggle="tooltip" title="Board/University !"');
                                  ?>
                                  <?php echo form_error("ugspecialisation");?></td>


                                  <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                                    id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo set_value("ugplace");?>" >
                                    <?php echo form_error("ugplace");?></td>

                                    <td><input type="text" class="form-control txtNumeric" 
                                      id="ugpercentage" name="ugpercentage" placeholder="Percentage" value="<?php echo set_value("ugpercentage");?>" >
                                      <?php echo form_error("ugpercentage");?></td>
                                    </tr>
                                    <tr>
                                      <td><b>PG</b></td>

                                      <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                        id="pgpassingyear" name="pgpassingyear" placeholder="Enter Year" value="<?php echo set_value("pgpassingyear");?>" >
                                      </td>


                                      <td>  
                                        <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                                        id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>" >
                                        <?php echo form_error("pgschoolcollegeinstitute");?>

                                      </td>

                                      <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                                        id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("pgboarduniversity");?>" >
                                      </td>
                                      <td>

                                        <?php
                                        $options = array('' => 'Select');
                                        foreach($pgeducationdetails as $key => $value) {
                                          $options[$value->pgname] = $value->pgname;
                                        }
                                        echo form_dropdown('pgspecialisation', $options, set_value('pgspecialisation'), 'class="form-control"');
                                        ?>
                                        <?php echo form_error("pgspecialisation");?>   </td>


                                        <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                                          id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo set_value("pgplace");?>" >
                                        </td>

                                        <td><input type="text" class="form-control txtNumeric" 
                                          id="pgpercentage" name="pgpercentage" placeholder="Percentage" value="<?php echo set_value("pgpercentage");?>" >
                                        </td>
                                      </tr>

                                      <tr>
                                        <td><b>If Others,Specify</b></td>

                                        <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                                          id="otherpassingyear" name="otherpassingyear" placeholder="Enter Year" value="<?php echo set_value("otherpassingyear");?>" >
                                        </td>
                                        <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
                                          id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>" >
                                        </td>

                                        <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                                          id="otherboarduniversity" name="otherboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("otherboarduniversity");?>" >
                                        </td>
                                        <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                                          id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("otherspecialisation");?>" >
                                        </td>

                                        <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                                          id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo set_value("otherplace");?>" >
                                        </td>

                                        <td><input type="text" class="form-control txtNumeric" 
                                          id="otherpercentage" name="otherpercentage" placeholder="Percentage" value="<?php echo set_value("otherpercentage");?>" >
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>

                      
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                       <table id="tblForm10" class="table table-bordered table-striped">
                        <thead>
                         <tr>
                          <th colspan="8" style="background-color: #3CB371; color: #fff;">

                           <div class="col-lg-12">
                            <div class="col-lg-6 text-left"> Training  Exposure(if any)</div>
                            <div class="col-lg-6 text-right ">
                             <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                             <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
                           </div>
                         </div>

                       </th>
                     </tr> 
                     <tr>

                      <th class="text-center" style="vertical-align: top;">Nature of Training </th>
                      <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
                      <th class="text-center" style="vertical-align: top;"> From Date</th>
                      <th class="text-center" style="vertical-align: top;">TO Date</th>
                    </tr> 
                  </thead>
                  <tbody id="bodytblForm10">
                    <tr id="bodytblForm10">
                      <td><input type="text" class="form-control" maxlength="150" data-toggle="tooltip" title="Nature of Training !" minlength="5" maxlength="50"
                        id="natureoftraining"  maxlength="150" name="natureoftraining[]" placeholder="Enter Nature of Training" style="min-width: 20%;"  value="<?php echo set_value("natureoftraining");?>"  ></td>
                        <td> <input type="text" class="form-control " data-toggle="tooltip" minlength="5"   maxlength="150" title="Organizing Agency !" 
                          id="organizingagency" name="organizingagency[]"  placeholder="Organizing Agency" style="min-width: 20%;"  value="<?php echo set_value("organizingagency");?>" ></td>
                          <td><input type="text" class="form-control datepicker"   data-toggle="tooltip"  title="From Date !" id="fromdate" name="fromdate[]" placeholder="From Date" style="min-width: 20%;"  value="<?php echo set_value("fromdate");?>"  ></td>
                          <td><input type="text"
                           class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                            id="todate" name="todate[]" placeholder="To Date" style="min-width: 20%;"  value="<?php echo set_value("todate");?>"></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                     <table id="tblForm11" class="table table-bordered table-striped">
                      <thead>
                       <tr>
                        <th colspan="8" style="background-color: #3CB371; color: #fff;"> 
                         <div class="col-lg-12">
                          <div class="col-lg-6 text-left">Language Skill/Proficiency </div>
                          <div class="col-lg-6 text-right ">
                           <button type="button" id="btnproficiencyRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                           <button type="button" id="btnproficiencyAddRow" class="btn btn-warning btn-xs">Add</button>
                         </div>
                       </div>
                     </th>
                   </tr> 
                   <tr>

                    <th class="text-center" style="vertical-align: top;">Language</th>
                    <th class="text-center" style="vertical-align: top;">Speak</th>
                    <th class="text-center" style="vertical-align: top;">Read</th>
                    <th class="text-center" style="vertical-align: top;">Write </th>
                  </tr> 
                </thead>
                <tbody id="bodytblForm11">

                  <tr id="bodytblForm11">
                    <td>  <?php
                    $options = array('' => 'Select');
                    foreach($syslanguage as $key => $value) {
                      $options[$value->lang_cd] = $value->lang_name;
                    }
                    echo form_dropdown('syslanguage[]', $options, set_value('syslanguage'), 'class="form-control" data-toggle="tooltip" required title="Select language !"');
                    ?>
                    <?php echo form_error("syslanguage");?></td>
                    <td>
                     <?php
                     $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                     echo form_dropdown('speak[]',$sysspeak,set_value('speak'),'class ="form-control" data-toggle="tooltip" required title="Select Speak !" ');
                     ?>
                     <?php echo form_error("speak");?>
                   </td>
                   <td><?php
                   $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                   echo form_dropdown('read[]',$sysread,set_value('read'),'class ="form-control" data-toggle="tooltip" required title="Select Read !" ');
                   ?>
                   <?php echo form_error("read");?></td>
                   <td><?php
                   $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                   echo form_dropdown('write[]',$syswrite,set_value('write'),'class ="form-control" data-toggle="tooltip" required title="Select Write !" ');
                   ?>
                   <?php echo form_error("write");?></td>
                 </tr>
               </tbody>
             </table>
           </div>


           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

             <table id="tbltrainingexposure" class="table table-bordered table-striped">
              <thead>
               <tr>
                <th colspan="8" style="background-color: #3CB371; color: #fff;">Any Subject(s) of Interest </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><textarea class="form-control" name="subjectinterest"  maxlength="1500" id="subjectinterest" cols="12" rows="5" data-toggle="tooltip" title="Enter Subject(s) of Interest !" ></textarea></td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

         <table id="tbltrainingexposure" class="table table-bordered table-striped">
          <thead>
           <tr>
            <th colspan="8" style="background-color: #3CB371; color: #fff;">Any Achievement /Awards(if any) </th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td><textarea class="form-control" name="achievementawards" id="achievementawards" cols="12" rows="5"  maxlength="1500" data-toggle="tooltip" title="Enter Achievement /Awards(if any) !" >

            </textarea></td>
          </tr>
        </tbody>
      </table>
    </div>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

     <table id="tblForm12" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="5" style="background-color: #3CB371; color: #fff;">
         <div class="col-lg-12">
          <div class="col-lg-6 text-left">Work Experience(if any) </div>
          <div class="col-lg-6 text-right ">
           <button type="button" id="btnworkexperienceRemoveRow" class="btn btn-danger btn-xs">Remove</button>
           <button type="button" id="btnworkexperienceAddRow" class="btn btn-warning btn-xs">Add</button>
         </div>
       </div>
     </th>
   </tr> 
   <tr>
    <th>Organization Name</th>
    <th>Description of Assignment</th>
    <th>Duration</th>
    <th>Palce of Posting</th>
  </tr> 
  <tr>
    <th colspan="2"></th>
    <th colspan="1">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="2"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">

    <tr id="bodytblForm12"> 
      <td><input type="text" name="orgname[]" maxlengt="100" id="orgname" data-toggle="tooltip" value="" class="form-control" title="Enter Organization Name !"  placeholder="Enter Organization Name ">
      <span class="error">This field is required</span></td>
      <td><textarea  maxlengt="250" name="descriptionofassignment[]" data-toggle="tooltip" id="descriptionofassignment" value="" class="form-control" title="Enter Description of Assignment" placeholder="Enter Description of Assignment " ></textarea></td>
      <td>
        <div class="col-lg-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <input type="text" name="work_experience_fromdate[]" id="work_experience_fromdate" class="form-control datepicker" value=""  placeholder="Enter from date">
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
            <input type="text" name="work_experience_todate[]" id="work_experience_todate" class="form-control datepicker" value="" placeholder="Enter To date">
          </div> 
        </div>
      </td>
      <td><input type="text" maxlengt="50" name="palceofposting[]" data-toggle="tooltip" id="palceofposting" value="" class="form-control" title="Place of Posting[]" placeholder="Enter Place of Posting"></td>

    </tr>
  </tbody>
</table>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
 <table id="tbltrainingexposure" class="table table-bordered table-striped">
  <thead>
   <tr>
    <th colspan="8" style="background-color: #3CB371; color: #fff;">Describe any Assignment(s) of special interest undertaken by you(if any) </th>
  </tr> 

</thead>
<tbody>
  <tr>
   <td><textarea class="form-control" data-toggle="tooltip"  maxlength="1500" name="any_assignment_of_special_interest" id="any_assignment_of_special_interest" cols="12" rows="5" title="Describe any Assignment(s) of special interest undertaken by you(if any) " placeholder="Enter Achievement /Awards" ></textarea></td>
 </tr>
</tbody>
</table>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tbltrainingexposure" class="table table-bordered table-striped">
  <thead>
   <tr>
    <th colspan="8" style="background-color: #3CB371; color: #fff;">Experience of Group and/or Social Activities </th>
  </tr> 
</thead>
<tbody>
  <tr>
    <td><textarea class="form-control" data-toggle="tooltip"  maxlength="1500" name="experience_of_group_social_activities" id="experience_of_group_social_activities" cols="12" rows="5" title="Experience of Group and/or Social Activities " placeholder="Enter Experience of Group and/or Social Activities " ></textarea></td>
  </tr>
</tbody>
</table>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

 <table id="tbltrainingexposure" class="table table-bordered table-striped">
  <thead>
   <tr>
    <td colspan="8" style="background-color: #3CB371; color: #fff;"><b>Have you taken part in PRADAN's selection process before?</b>
      <?php  $checked =''; $val_have_you_taken_part_in_pradan =''; 
      if (isset($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before) && $otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='on' ) {
       echo $checked = "checked=checked";
       $val_have_you_taken_part_in_pradan = $otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_details;

     }  ?>

     <div class="form-check">
      <input type="checkbox"  class="form-check-input" 
      id="selection_process_befor" name="selection_process_befor" <?php echo $checked; ?> >
      <label class="form-check-label" for="selection_process_befor"><b> If yes when and where </b></label>
    </div>
  </td>
</tr> 

</thead>
<tbody>
  <tr>
    <td><textarea class="form-control"  maxlength="250" data-toggle="tooltip" required="required" name="have_you_taken_part_in_pradan_selection_process_before_details" 
      id="pradan_selection_process_before_details" cols="12" rows="5" title="Have you taken part in PRADAN's selection process before? If yes when and where " placeholder="Enter Have you taken part in PRADAN's selection process before? If yes when and where" ></textarea></td>
    </tr>
  </tbody>
</table>
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" style="background-color: #ffffff;">
 <!-- <a class="btn btn-warning  btnPrevious" >Previous</a> -->

 <button type="reset" class="btn btn-warning  btn-sm m-t-10 waves-effect">Reset </button>
 <button type="submit" value="saveWorkExpSubmit" id="sendDataWorkExpButton" class="btn btn-success  btn-sm m-t-10 waves-effect" onclick="return ifanyvalidation()">Save </button>
 <a href="<?php echo site_url();?>" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 

 <!-- <button type="submit" value="saveWorkExpSubmit" id="sendDataWorkExpButton" class="btn btn-primary text-center ">Save</button> -->
 <!--  <button type="button" id="btnsubmit" onclick="return BdfFormSubmit()" class="btn btn"  style="background-color: #3CB371; color: #fff;">Submit</button> -->
 <br><br>
</div>

</form>
</div>
<!-- End  Candidates Basic Info  -->
</div>
</div>
</div>
</section>


<script type="text/javascript">

  function ifanyvalidation(){
    var natureoftraining = $('#natureoftraining').val();
    var organizingagency = $('#organizingagency').val();
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var orgname = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();



    

    if (natureoftraining !='') {
      if (organizingagency =='') {
         // alert('Please Enter Organizing Agency!!!');
         $('#organizingagency').focus();
         $('#organizingagency').css({ "border": "1px solid red"});
          return false;
      }
      if (fromdate =='') {
         $('#fromdate').focus();
         $('#fromdate').css({ "border": "1px solid red"});
          return false;
      }
      if (todate =='') {
         $('#todate').focus();
         $('#todate').css({ "border": "1px solid red"});
          return false;
      }

     }


      if (orgname !='') {
      if (descriptionofassignment =='') {
         $('#descriptionofassignment').focus();
         $('#descriptionofassignment').css({ "border": "1px solid red"});
          return false;
      }
      if (work_experience_fromdate =='') {
        $('#work_experience_fromdate').focus();
         $('#work_experience_fromdate').css({ "border": "1px solid red"});
          return false;
      }
      if (work_experience_todate =='') {
         $('#work_experience_todate').focus();
         $('#work_experience_todate').css({ "border": "1px solid red"});
          return false;
      }
      if (palceofposting =='') {
         $('#palceofposting').focus();
         $('#palceofposting').css({ "border": "1px solid red"});
          return false;
      }

     }


  }
 
 /// Upload Buttone /////////

 $(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
 $(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
 
 var matriccertificate_uploadField = document.getElementById("10thcertificate");

 matriccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 


var hsccertificate_uploadField = document.getElementById("12thcertificate");

hsccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

var ugcertificate_uploadField = document.getElementById("ugcertificate");

ugcertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

</script>

<script type="text/javascript">
  $(document).ready(function(){
    decimalData();
    adddatepicker();
    //addworkdatepicker();
    changedatepicker(inctg);
    workexchangedatepicker(incwe);
  //  ifanyvalidation();
    
    $('[data-toggle="tooltip"]').tooltip(); 
    $("#pradan_selection_process_before_details").prop("disabled", "disabled");
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

    $('#contact_name').on('input', function() {
	var input=$(this);
	var is_name=input.val();
	if(is_name){input.removeClass("invalid").addClass("valid");}
	else{input.removeClass("valid").addClass("invalid");}
  });
				

  });

  $(function () {
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
  });
  

// $("#sendDataWorkExpButton").click(function() {
//     var count_checked = $("[name='selection_process_befor']:checked").length; // count the checked rows
//         if(count_checked == 0) 
//         {
//             alert("Have you taken part in PRADAN's selection process before?.");
//             return false;
//         }
//         return true;
// });


  $(document).ready(function(){
    $("#filladdress").on("click", function(){
     if (this.checked) { 
      if ($("#presentstreet").val().length == 0  && $("#permanentstreet").val().length ==0) {
        alert('Please Fill Present Mailing Address !!!');
        $("#filladdress").removeAttr('checked');
      }else{


      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());
    }
    }
    else {
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

  });


  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

  }

  $("#btnRemoveRow").click(function() {
    if($('#tblForm09 tr').length-2>1)
      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = parseInt(1);

    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    //addDatePicker();
  });



  var srNoGlobal=0;
  var inc = 0;

  function insertRows(count) {
    srNoGlobal = $('#tbodyForm09 tr').length+1;
    var tbody = $('#tbodyForm09');
    var lastRow = $('#tbodyForm09 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inc++;
      cloneRow = lastRow.clone();
      var tableData = '<tr>'
      + ' <td>'
      + '<input type="text" name="familymembername['+inc+']" id="familymembername" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td><td>'
      + '<select class="form-control" name="relationwithenployee['+inc+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysrelations as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>'
      <?php endforeach ?>
      + '</select>'
      + '</td>'
      + '<td><input type="text" class="form-control datepicker" id="familymemberdob'+inc+'" data-toggle="tooltip" title="Date Of Birth!" name="familymemberdob['+inc+']" placeholder="Enter Date Of Birth " value="" required="required" >'

      + '</td>'
      + '</tr>';
      $("#tbodyForm09").append(tableData)

      adddatepicker();
    }

  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });



  var srNoGlobal=0;
  var inctg = 0;

  function Insaettrainingexposure(count) {
    srNoGlobal = $('#bodytblForm10 tr').length+1;
    var tbody = $('#bodytblForm10');
    var lastRow = $('#bodytblForm10 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inctg++
      cloneRow = lastRow.clone();
      var tableData1 = '<tr>'
      + '<td> <input type="text"  class="form-control " data-toggle="tooltip" title="" minlength="5" maxlength="150" required="required" id="natureoftraining" name="natureoftraining['+inctg+']" placeholder="Enter Nature of Training" style="min-width: 20%;" value="" data-original-title="Nature of Training !">'
      + '</td>'
      + '<td class="focused"> <input type="text" class="form-control " data-toggle="tooltip" minlength="5" maxlength="150" title="" id="organizingagency['+inctg+']" name="organizingagency['+inctg+']" placeholder="Organizing Agency" style="min-width: 20%;" value="" required="required" data-original-title="Organizing Agency !"></td>'

      + '<td><input type="text" class="form-control " onclick="return changedatepicker(inctg)" id="fromdate_'+inctg+'" '+
      'name="fromdate['+inctg+']" value=""  required="required" />'
      + '</td><td>'
      + '<input type="text" class="form-control " onclick="return changedatepicker(inctg)" id="todate_'+inctg+'" '+
      'name="todate['+inctg+']" value="" required="required" />'
      + '</td>' + '</tr>';
      $("#bodytblForm10").append(tableData1)
      adddatepicker();
      changedatepicker(inctg);
    }

  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnproficiencyRemoveRow").click(function() {
    if($('#tblForm11 tr').length-2>1)
      $('#bodytblForm11 tr:last').remove()
  });

  $('#btnproficiencyAddRow').click(function() {
    rowsEnter2 = parseInt(1);
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaetproficiency(rowsEnter2);
    //addDatePicker();
  });




  var srNoGlobal=0;
  var inclp = 0;

  function Insaetproficiency(count) {
    srNoGlobal = $('#bodytblForm11 tr').length+1;
    var tbody = $('#bodytblForm11');
    var lastRow = $('#bodytblForm11 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      inclp++
      cloneRow = lastRow.clone();
      var tableData2 = '<tr>'

      + '<td><select class="form-control" id="syslanguage'+inclp+'"   name="syslanguage['+inclp+']" required="required">'
      + '<option value="">Select </option>'
      <?php foreach ($syslanguage as $key => $value): ?>
        + '<option value=<?php echo $value->lang_cd;?>><?php echo $value->lang_name;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'
      + '<select class="form-control" name="speak['+inclp+'] id="speak'+inclp+'"required="required">'
      + '<option value="">Select </option>'
      <?php foreach ($sysspeak as $key => $value): ?>
        + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'

      + '<select class="form-control" id="read'+inclp+'" name="read['+inclp+']" required="required">'
      + '<option value="">Select </option>'
      <?php foreach ($sysread as $key => $value): ?>
        + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td><td>'

      + '<select class="form-control" id="write'+inclp+'" name="write['+inclp+']" required="required">'
      + '<option value="">Select </option>'
      <?php foreach ($syswrite as $key => $value): ?>
        + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
      <?php endforeach ?>
      + '</select> </td>'
      + '</tr>';
      $("#bodytblForm11").append(tableData2)
      adddatepicker();
    }

  }
  //insertRows();
</script>


<script type="text/javascript">
  $("#btnworkexperienceRemoveRow").click(function() {
    if($('#tblForm12 tr').length-3>1)
      $('#bodytblForm12 tr:last').remove()
  });

  $('#btnworkexperienceAddRow').click(function() {
    rowsEnter2 = parseInt(1);
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insertworkexperience(rowsEnter2);
    //addDatePicker();
  });


  var srNoGlobal=0;
  var incwe = 0;
  function Insertworkexperience(count) {
    srNoGlobal = $('#bodytblForm12 tr').length+1;
    var tbody = $('#bodytblForm12');
    var lastRow = $('#bodytblForm12 tr:last');
    var cloneRow = null;
    
    for (i = 1; i <= count; i++) {
      incwe++
      
      cloneRow = lastRow.clone();
      var tableData2 = '<tr>'
      + '<td>'
      + '<input type="text" name="orgname['+incwe+']" id="orgname" data-toggle="tooltip" value="" class="form-control" maxlengt="100" data-original-title="" title="">'
      
      + '</td><td>'
      + '<textarea name="descriptionofassignment['+incwe+']" data-toggle="tooltip" maxlengt="250" id="descriptionofassignment" class="form-control" data-original-title="" required="required" title=""></textarea></td><td>'
     

      + ' <div class="col-lg-12">'
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">'
      + '    <input type="text"  name="work_experience_fromdate['+incwe+']" onclick="return workexchangedatepicker('+incwe+')"  data-toggle="tooltip" id="work_experience_fromdate_'+incwe+'" value="" class="form-control datepicker" value="">'
      + ' </div>' 
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">'
      + ' <input type="text" name="work_experience_todate['+incwe+']" onclick="return workexchangedatepicker('+incwe+')" data-toggle="tooltip" id="work_experience_todate_'+incwe+'" class="form-control datepicker" value=""></div> </div></td><td>'
      + '<input type="text" maxlengt="50" name="palceofposting['+incwe+']" data-toggle="tooltip" id="palceofposting" value="" class="form-control" data-original-title="" title="">'
      + ' </td>'
      + '</tr>';
      $("#bodytblForm12").append(tableData2)
      adddatepicker();
      workexchangedatepicker(incwe);
    }

  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-2>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertIdentityRows(rowsIdentityEnter);
    //addDatePicker();
  });

  
  var srNoIdentity=0;
  var incr = 0;
  
  function insertIdentityRows(count) {
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;

    for (i = 1; i <= count; i++) {
      incr++
      cloneRow = lastRow.clone();
      var tableDataIdentity = '<tr>'
      + ' <td>'
      + '<select class="form-control" id="identityname'+incr+'" name="identityname['+incr+']" required="required">'
      + '<option value="">Select Relation</option>'
      <?php foreach ($sysidentity as $key => $value): ?>
        + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
      <?php endforeach ?>
      + '</select>'

      + ' <td><input type="text" id="identitynumber'+incr+'" name="identitynumber['+incr+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
      + '</td>'
      + '</tr>';
      $("#tbodyForm13").append(tableDataIdentity)
      adddatepicker();
    }

  }
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit")..prop('disabled', true);

  });
</script>  




<script type="text/javascript">
  $(document).ready(function(){
    decimalData();
    $('[data-toggle="tooltip"]').tooltip();   


    $(function () {
      $('#presentcity').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
          e.preventDefault();
        } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
          }
        }
      });
    });
    $(function () {
      $('#permanentcity').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
          e.preventDefault();
        } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
          }
        }
      });
    });

  });

  $(document).ready(function(){
    $("#filladdress").on("click", function(){

     if (this.checked) { 
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());

    }
    else {
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

  });

  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});



    $('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
        e.preventDefault();
        $('.error').show();
        $('.error').text('Please Enter Alphabate');
        return false;
      }
    });
  }



</script>

<script type="text/javascript">
 $(document).ready(function(){
  $("#presentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>

<script type="text/javascript">
  function adddatepicker(){

       $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         yearRange: '1920:2030',
         dateFormat : 'dd/mm/yy',
         onClose: function(selectedDate) {
        jQuery("#todate").datepicker( "option", "minDate", selectedDate );
         jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
        }
    });

    $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         yearRange: '1920:2030',
         dateFormat : 'dd/mm/yy',
          onClose: function(selectedDate) {
        jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
        jQuery("#work_experience_fromdate").datepicker( "option", "maxDate", selectedDate );

        }
    });

}


function changedatepicker(inctg){


     $("#fromdate_"+inctg).datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: 'today',
        yearRange: '1920:2030',
        dateFormat : 'dd/mm/yy',
        onClose: function(selectedDate) {
        jQuery("#todate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
    });

    $("#todate_"+inctg).datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         yearRange: '1920:2030',
         dateFormat : 'dd/mm/yy',
          onClose: function(selectedDate) {
        jQuery("#fromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
        }
    });
  }


  function workexchangedatepicker(incwe){

     $("#work_experience_fromdate_"+incwe).datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: 'today',
        yearRange: '1920:2030',
        dateFormat : 'dd/mm/yy',
        onClose: function(selectedDate) {
        jQuery("#work_experience_todate_"+incwe).datepicker("option", "minDate", selectedDate);
        }
    });

    $("#work_experience_todate_"+incwe).datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         yearRange: '1920:2030',
         dateFormat : 'dd/mm/yy',
          onClose: function(selectedDate) {
        jQuery("#work_experience_fromdate_"+incwe).datepicker("option", "maxDate", selectedDate);
        }
    });

  }
</script>
