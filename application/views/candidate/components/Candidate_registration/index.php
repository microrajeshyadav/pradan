<style>
textarea {
  resize: none;
}
</style>
<?php 
echo $getbdfformstatus->campustype;
echo  $getbdfformstatus->BDFStatusaftergd; 



if ($getbdfformstatus->BDFStatusaftergd =='0' && $getbdfformstatus->campustype =='off') { ?>
  <script type="text/javascript">
   $(document).ready(function(){
     $("#basicinfo").find("input,textarea,select").prop("disabled", true);
     $("#basicinfo").find("input[type=file]").prop("disabled", false);
     $("#basicinfo").find("input[type=hidden]").prop("disabled", false);
      // $("#basicinfo").find("input[type=file]").prop("required", true);
      // $("#familymemberphoto").prop("required", true);
      // $("#identityphoto").prop("required", true);
      // $("#experiencedocument").prop("required", true);

    });
  </script>
<?php } else if($getbdfformstatus->campustype =='off' && $getbdfformstatus->BDFStatusaftergd =='NULL'){  ?>
  <script type="text/javascript">
   $(document).ready(function(){
    $("#basicinfo").find("input,textarea,select").prop("disabled", true);
    $("#basicinfo").find("input[type=file]").prop("disabled", false);
    $("#basicinfo").find("input[type=hidden]").prop("disabled", false);
    $("#basicinfo").find("input[type=file]").prop("required", true);
    $("#familymemberphoto").prop("required", true);
    $("#identityphoto").prop("required", true);
    $("#experiencedocument").prop("required", true);
    $("#pgcertificate").prop("required", false);
    $("#otherscertificate").prop("required", false);
    $("#btnworkexperienceRemoveRow").prop("disabled", true);
    $("#btnworkexperienceAddRow").prop("disabled", true);
    $("#btnproficiencyRemoveRow").prop("disabled", true);
    $("#btnproficiencyAddRow").prop("disabled", true);
    $("#btntrainingexposureRemoveRow").prop("disabled", true);
    $("#btntrainingexposureAddRow").prop("disabled", true);
    $("#bloodgroup").prop("disabled", false);
    $("#photoupload").prop("disabled", false);
    $("#familymembername").prop("disabled", false);
    $("#relationwithenployee").prop("disabled", false);
    $("#familymemberdob").prop("disabled", false);
    $("#identityname").prop("disabled", false);
    $("#identitynumber").prop("disabled", false);
    $("#identityphoto").prop("disabled", false);
  });
</script>
<?php  }else if($getbdfformstatus->BDFStatusaftergd =='1' && $getbdfformstatus->campustype =='off'){ ?>
  <script type="text/javascript">
   $(document).ready(function(){
     $("#basicinfo").find('input,textarea,select,button[type="submit"]').prop("disabled", true);
       //$("#basicinfo").find('input[type="submit"]').prop("disabled", true);

       
     });
   </script>
 <?php }elseif ($getbdfformstatus->BDFStatusaftergd =='1' && $getbdfformstatus->campustype =='on') { ?>
   <script type="text/javascript">
     $(document).ready(function(){
      $("#basicinfo").find('input,textarea,select,button[type="submit"]').prop("disabled", true);
    });
  </script>
<?php }elseif (empty($getbdfformstatus->BDFStatusaftergd) || $getbdfformstatus->BDFStatusaftergd =='99' && $getbdfformstatus->campustype =='on') { ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#candidatefirstname").prop("disabled", true);
      $("#candidatemiddlename").prop("disabled", true);
      $("#candidatelastname").prop("disabled", true);
      $("#motherfirstname").prop("disabled", true);
      $("#mothermiddlename").prop("disabled", true);
      $("#motherlastname").prop("disabled", true);
      $("#fatherfirstname").prop("disabled", true);
      $("#fathermiddlename").prop("disabled", true);
      $("#fatherlastname").prop("disabled", true);
      $("#gender").prop("disabled", true);
      $("#nationality").prop("disabled", true);  
      $("#maritalstatus").prop("disabled", true);
      $("#dateofbirth").prop("disabled", true);
      $("#emailid").prop("disabled", true);
      $("#mobile").prop("disabled", true);
      $("#10thschoolcollegeinstitute").prop("disabled", true);
      $("#10thboarduniversity").prop("disabled", true);
      $("#10thpassingyear").prop("disabled", true);
      $("#10thplace").prop("disabled", true);
      $("#10thspecialisation").prop("disabled", true);  
      $("#10thpercentage").prop("disabled", true);
      $("#12thschoolcollegeinstitute").prop("disabled", true);
      $("#12thboarduniversity").prop("disabled", true);
      $("#12thpassingyear").prop("disabled", true);
      $("#12thplace").prop("disabled", true);
      $("#12thspecialisation").prop("disabled", true);
      $("#12thpercentage").prop("disabled", true);
      $("#ugschoolcollegeinstitute").prop("disabled", true);
      $("#ugboarduniversity").prop("disabled", true);  
      $("#ugpassingyear").prop("disabled", true);
      $("#ugplace").prop("disabled", true);
      $("#ugspecialisation").prop("disabled", true);
      $("#ugpercentage").prop("disabled", true);
      $("#pgschoolcollegeinstitute").prop("disabled", true);
      $("#12thspecialisation").prop("disabled", true);
      $("#12thpercentage").prop("disabled", true);
      $("#ugschoolcollegeinstitute").prop("disabled", true);
      $("#ugboarduniversity").prop("disabled", true);  
      $("#ugpassingyear").prop("disabled", true);
      $("#ugplace").prop("disabled", true);
      
      $("#presenthno").prop("disabled", true);
      $("#permanenthno").prop("disabled", true);
      $("#presentstreet").prop("disabled", true);
      $("#presentcity").prop("disabled", true);
      $("#presentstateid").prop("disabled", true);
      $("#presentdistrict").prop("disabled", true);
      $("#presentpincode").prop("disabled", true);
      $("#permanentstreet").prop("disabled", true);
      $("#permanentcity").prop("disabled", true);  
      $("#permanentstateid").prop("disabled", true);
      $("#permanentdistrict").prop("disabled", true);
      $("#permanentpincode").prop("disabled", true);
      $("#experiencedocument").prop("required", false);
      $("#experiencedocument_1").prop("required", false);

    });
  </script>
<?php }  else if($getbdfformstatus->campustype =='off' && $getbdfformstatus->BDFStatusaftergd =='99'){  ?>
  <script type="text/javascript">
   $(document).ready(function(){
    $("#basicinfo").find("input,textarea,select").prop("disabled", true);
    $("#basicinfo").find("input[type=file]").prop("disabled", false);
    $("#basicinfo").find("input[type=hidden]").prop("disabled", false);
    $("#pgcertificate").prop("required", false);
    $("#otherscertificate").prop("required", false);
    $("#btnworkexperienceRemoveRow").prop("disabled", true);
    $("#btnworkexperienceAddRow").prop("disabled", true);
    $("#btnproficiencyRemoveRow").prop("disabled", true);
    $("#btnproficiencyAddRow").prop("disabled", true);
    $("#btntrainingexposureRemoveRow").prop("disabled", true);
    $("#btntrainingexposureAddRow").prop("disabled", true);
    $("#btnGapReasonRemoveRow").prop("disabled", true);
     $("#btnGapReasonAddRow").prop("disabled", true);


    $("#bloodgroup").prop("disabled", false);
    $("#photoupload").prop("disabled", false);
    $("#familymembername").prop("disabled", false);
    $("#relationwithenployee").prop("disabled", false);
    $("#familymemberdob").prop("disabled", false);
    $("#identitynumber").prop("disabled", false);
    $("#identityphoto").prop("disabled", false);
    $("#familymembername_0").prop("disabled", false);
    $("#relationwithenployee_0").prop("disabled", false);
    $("#familymemberdob_0").prop("disabled", false);

    $("#familymembername_1").prop("disabled", false);
    $("#relationwithenployee_1").prop("disabled", false);
    $("#familymemberdob_1").prop("disabled", false);

    $("#familymembername_2").prop("disabled", false);
    $("#relationwithenployee_2").prop("disabled", false);
    $("#familymemberdob_2").prop("disabled", false);
    $("#identitynumber_0").prop("disabled", false);
    $("#identityname_0").prop("disabled", false);

    

    $("#presentstateid").prop("disabled", true);
    $("#permanentstateid").prop("disabled", true);
    $("#presentdistrict").prop("disabled", true);
    $("#permanentdistrict").prop("disabled", true);
  });
</script>
<?php } elseif (empty($getbdfformstatus->BDFStatusaftergd) || $getbdfformstatus->BDFStatusaftergd == 'NULL' && $getbdfformstatus->campustype =='on') { ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#candidatefirstname").prop("disabled", true);
      $("#candidatemiddlename").prop("disabled", true);
      $("#candidatelastname").prop("disabled", true);
      $("#motherfirstname").prop("disabled", true);
      $("#mothermiddlename").prop("disabled", true);
      $("#motherlastname").prop("disabled", true);
      $("#fatherfirstname").prop("disabled", true);
      $("#fathermiddlename").prop("disabled", true);
      $("#fatherlastname").prop("disabled", true);
      $("#gender").prop("disabled", true);
      $("#nationality").prop("disabled", true);  
      $("#maritalstatus").prop("disabled", true);
      $("#dateofbirth").prop("disabled", true);
      $("#emailid").prop("disabled", true);
      $("#mobile").prop("disabled", true);
      $("#10thschoolcollegeinstitute").prop("disabled", true);
      $("#10thboarduniversity").prop("disabled", true);
      $("#10thpassingyear").prop("disabled", true);
      $("#10thplace").prop("disabled", true);
      $("#10thspecialisation").prop("disabled", true);  
      $("#10thpercentage").prop("disabled", true);
      $("#12thschoolcollegeinstitute").prop("disabled", true);
      $("#12thboarduniversity").prop("disabled", true);
      $("#12thpassingyear").prop("disabled", true);
      $("#12thplace").prop("disabled", true);
      $("#12thspecialisation").prop("disabled", true);
      $("#12thpercentage").prop("disabled", true);
      $("#ugschoolcollegeinstitute").prop("disabled", true);
      $("#ugboarduniversity").prop("disabled", true);  
      $("#ugpassingyear").prop("disabled", true);
      $("#ugplace").prop("disabled", true);
      $("#ugspecialisation").prop("disabled", true);
      $("#ugpercentage").prop("disabled", true);
      $("#pgschoolcollegeinstitute").prop("disabled", true);
      $("#12thspecialisation").prop("disabled", true);
      $("#12thpercentage").prop("disabled", true);
      $("#ugschoolcollegeinstitute").prop("disabled", true);
      $("#ugboarduniversity").prop("disabled", true);  
      $("#ugpassingyear").prop("disabled", true);
      $("#ugplace").prop("disabled", true);
      $("#presentstreet").prop("disabled", true);
      $("#presentcity").prop("disabled", true);
      $("#presentstateid").prop("disabled", true);
      $("#presentdistrict").prop("disabled", true);
      $("#presentpincode").prop("disabled", true);
      $("#permanentstreet").prop("disabled", true);
      $("#permanentcity").prop("disabled", true);  
      $("#permanentstateid").prop("disabled", true);
      $("#permanentdistrict").prop("disabled", true);
      $("#permanentpincode").prop("disabled", true);
    });
  </script>
<?php } ?>
<style type="text/css" media="screen">
.error{
  color: red;
}  
</style>
<br>
<div class="container" style="background-color: #FFFFFF;"  >
  
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php //print_r($getbdfformstatus->BDFFormStatus); //die;?>

          <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
           <input type="hidden" name="campustype" value="<?php echo $this->loginData->campustype;?>">
           <input type="hidden" name="emailid" value="<?php echo $this->loginData->emailid;?>">
           <div class="tab-content">    <!-- Start  Candidates Basic Info     -->
            <div id="home" class="tab-pane fade in active">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
                  <thead>
                   <tr>
                    <th colspan="3" style="background-color: #3CB371; color: #fff;">Basic Info</th>
                  </tr> 
                </thead>
                <tbody>
                  <tr>
                    <td colspan="3">
                      <div class="row">
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Candidate's Name <span style="color: red;" >*</span></label>
                        <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control " value="<?php echo $candidatedetails->candidatefirstname;?>" placeholder="Enter First Name"   required="required">

                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                        <label for="Name"> </label>
                        <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control" value="<?php echo $candidatedetails->candidatemiddlename;?>" placeholder="Enter Middle Name" >

                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                        <label for="Name"><span style="color: red;" >*</span></label>
                        <input type="text"  minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="<?php echo $candidatedetails->candidatelastname;?>" required="required" >
                        <?php echo form_error("candidatelastname");?>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
                        <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control" value="<?php echo $candidatedetails->motherfirstname;?>" placeholder="Enter First Name" required="required" >
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                        <label for="Name"></label>
                        <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control" value="<?php echo $candidatedetails->candidatemiddlename;?>" placeholder="Enter Middle Name" >

                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                        <label for="Name"></label>
                        <input type="text" minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="<?php echo $candidatedetails->candidatelastname;?>" required="required" >
                      </div>
                    </div>
                    <br>
                    <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">Father's Name <span style="color: red;" >*</span></label>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control" value="<?php echo $candidatedetails->fatherfirstname;?>" placeholder="Enter First Name" required="required" >
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                      <label for="Name"></label>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control" value="<?php echo $candidatedetails->fathermiddlename;?>" placeholder="Enter Middle Name" >
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top: 6px;">
                      <label for="Name"> </label>
                      <input type="text" class="form-control" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="<?php echo $candidatedetails->fatherlastname;?>" required="required" readonly="readonly">
                    </div>
                  </div>
                  <br>
                  <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Gender <span style="color: red;" >*</span></label>
                    <?php 
                    $options = array("1" => "Male", "2" => "Female", "3" => "Others");
                    echo form_dropdown('gender', $options, $candidatedetails->gender, 'class="form-control" id="gender" ');
                    ?>
                    <?php echo form_error("gender");?>
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Nationality  </label>
                    <?php 
                    $options = array("indian" => "Indian", "other" => "Other");
                    echo form_dropdown('nationality', $options, $candidatedetails->nationality, 'class="form-control" id="nationality" "');
                    ?>
                    <?php echo form_error("nationality");?>
                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Marital Status </label>
                    <?php 
                    $options = array("1" => "Single", "2" => "Married", "3" => "Divorced", "4" => "Widow", "5" => "Separated");
                    echo form_dropdown('maritalstatus', $options, $candidatedetails->maritalstatus, 'class="form-control" id="maritalstatus" "');
                    ?>
                    <?php echo form_error("maritalstatus");?>
                  </div>
                </div>
                <br>

                <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control " data-toggle="tooltip" title="Date Of Birth!" name="dateofbirth" placeholder="Enter Date Of Birth " value="<?php echo $this->model->changedate($candidatedetails->dateofbirth);?>" required="required" readonly="readonly">
                  <?php echo form_error("dateofbirth");?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Email Id <span style="color: red;" >*</span></label>
                  <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo $candidatedetails->emailid;?>" required="required" readonly="readonly">
                  <?php echo form_error("emailid");?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                  <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="<?php echo $candidatedetails->mobile;?>" placeholder="Enter Mobile No" required="required" readonly="readonly">
                  <?php echo form_error("mobile");?>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Annual income of parents <span style="color: red;" >*</span></label>
                  <select name="annual_income" id="annual_income" required="required" class="form-control">
                    <option value="">Select</option>
                    <option value="1" <?php if ($otherinformationdetails->annual_income==1) {
                     echo "SELECTED";
                   } ?> >Below 50,000</option>
                   <option value="2" <?php   if ($otherinformationdetails->annual_income==2) {
                    echo "SELECTED";
                  }  ?>>50,001-200,000</option>
                  <option value="3" <?php if ($otherinformationdetails->annual_income==3) {
                    echo "SELECTED";
                  } ?> >200,001-500,000</option>
                  <option value="4" <?php if ($otherinformationdetails->annual_income==4) {
                   echo "SELECTED";
                 } ?>>Above 500,000</option>
               </select>
               <?php echo form_error("annual_income");?>
             </div>

             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <label for="Name">No. of male sibling</label>
              <input type="text"  maxlength="2" data-toggle="tooltip" title="Male sibling!" name="no_of_male_sibling" id="no_of_male_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo $otherinformationdetails->male_sibling; ?>" placeholder="Enter sibling" required="required">
              <?php echo form_error("no_of_male_sibling");?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <label for="Name">No. of female sibling</label>
              <input type="text"  maxlength="2" data-toggle="tooltip" title="Female sibling!" name="no_of_female_sibling" id="no_of_female_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo $otherinformationdetails->female_sibling; ?>" placeholder="Enter sibling " required="required">
              <?php echo form_error("no_of_female_sibling");?>
            </div>
          </div>

          <br>
          <div class="row">
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <label for="Name">Blood Group<span style="color: red;" >*</span></label>
            <input type="text"  maxlength="10" data-toggle="tooltip" title="Blood Group !" name="bloodgroup" id="bloodgroup" class="form-control" data-country="India" value="<?php echo $candidatedetails->bloodgroup;?>" placeholder="Enter Blod Group " required="required">
            <?php echo form_error("bloodgroup");?>
          </div>

         
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
              
             
        

        </div>
      </td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php //print_r($getdistrict);  echo $candidatedetails->presentdistrict;?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table table-bordered dt-responsive table-striped" >
      <thead>
       <tr>
        <th colspan="8" style="background-color: #3CB371; color: #fff;"> 
          <b>Communication Address </b>
        </th>
      </tr> 
      <tr>
        <th class="text-center" colspan="4" style="vertical-align: top;">Mailing Address</th>
        <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Address  <div class="form-check">
          <input type="checkbox" class="form-check-input" id="filladdress">
          <label class="form-check-label" for="filladdress"><b>same as the Mailing Address</b></label>
        </div></th>
      </tr> 
    </thead>
    <tbody>
     <tr>
      <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
      <td><input type="text" name="presenthno" id="presenthno" maxlength="50"class="form-control" 
        value="<?php echo $candidatedetails->presenthno;?>" placeholder="Enter H.No" required="required"></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><textarea name="presentstreet" id="presentstreet" maxlength="150" class="form-control" 
          placeholder="Enter H.No/Street" required="required"><?php echo $candidatedetails->presentstreet;?></textarea></td>
          <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
          <td><input type="text" name="permanenthno" id="permanenthno"  maxlength="50" class="form-control" value="<?php echo $candidatedetails->permanenthno;?>"  placeholder="Enter H.No/Street" required="required" ></td>
          <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
          <td><textarea name="permanentstreet" id="permanentstreet" maxlength="150" class="form-control"   placeholder="Enter H.No/Street" required="required"><?php echo $candidatedetails->permanentstreet;?></textarea></td>
        </tr>
        <tr>
          <td><label for="Name">City<span style="color: red;" >*</span></label> </td>
          <td><input type="text" name="presentcity" id="presentcity" maxlength="50" class="form-control txtOnly" placeholder="Enter City" value="<?php echo $candidatedetails->presentcity;;?>"  required="required"></td>

          <td><label for="Name">State<span style="color: red;" >*</span></label> </td>
          <td><?php 
          $options = array('' => 'Select Present State');
          $selected = $candidatedetails->presentstateid;
          foreach($statedetails as$key => $value) {
            $options[$value->statecode] = $value->name;
          }
          echo form_dropdown('presentstateid', $options, $selected, 'class="form-control" id="presentstateid" data-toggle="tooltip" title=" Select State !" ');
          ?>
          <?php echo form_error("presentstateid");?>
        </td>
        <td><label for="Name">City<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentcity" id="permanentcity" maxlength="50" placeholder="Enter City" class="form-control txtOnly" value="<?php echo $candidatedetails->permanentcity;?>" required="required" ></td>
        <td><label for="Name">State<span style="color: red;" >*</span></td>
          <td> <?php 
          $options = array('' => 'Select Permanent State');
          $selected = $candidatedetails->permanentstateid;
          foreach($statedetails as$key => $value) {
            $options[$value->statecode] = $value->name;
          }
          echo form_dropdown('permanentstateid', $options, $selected, 'class="form-control" id="permanentstateid" data-toggle="tooltip" title=" Select State !" ');
          ?>
          <?php echo form_error("permanentstateid");?></td>
        </tr>
        <tr>
          <td><label for="Name">District<span style="color: red;" >*</span></label></td>
          <td>   <select name="presentdistrict" id="presentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
           <option value="">Select District </option>
           <?php foreach ($getdistrict as $key => $value) {
            if ( $value->districtid==$candidatedetails->presentdistrict) {
             ?>
             <option value="<?php echo $value->districtid; ?>" selected><?php echo $value->name; ?></option>
           <?php }else{ ?>
            <option value="<?php echo $value->districtid; ?>"><?php echo $value->name; ?></option>
          <?php } }?>
        </select></td>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="presentpincode" maxlength="6" id="presentpincode" class="form-control txtNumeric" value="<?php echo $candidatedetails->presentpincode; ?>" required="required" placeholder="Enter PinCode"></td>
        <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td><select name="permanentdistrict" id="permanentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
         <?php foreach ($getdistrict as $key => $value) {
          if ($value->districtid==$candidatedetails->permanentdistrict) {
           ?>
           <option value="<?php echo $value->districtid; ?>" selected><?php echo $value->name; ?></option>
         <?php }else{ ?>
          <option value="<?php echo $value->districtid; ?>"><?php echo $value->name; ?></option>
        <?php } }?>
      </select></td>
      <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
      <td><input type="text" name="permanentpincode"  maxlength="6" id="permanentpincode" class="form-control txtNumeric" value="<?php echo $candidatedetails->permanentpincode; ?>" required="required" placeholder="Enter PinCode"></td>
    </tr>
  </tbody>
</table>
</div>  
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

   <table id="tblForm09" class="table table-bordered dt-responsive table-striped" >
    <thead>
     <tr>
      <th colspan="4" style="background-color: #3CB371; color: #fff;">
        <div class="col-lg-12">
          <div class="col-lg-6">Family Members</div>
          <?php if($this->loginData->BDFFormStatus !='1') { ?>
            <div class="col-lg-6 text-right ">
             <button type="button" id="btnRemoveRow" class="btn btn-danger btn-xs">Remove</button>
             <button type="button" id="btnAddRow" class="btn btn-warning btn-xs">Add</button>
           </div>
         <?php } ?>
       </div>  </th>
     </tr> 
     <tr>
       <th>Name</th>
       <th>Relation with Employee</th>
       <th>Date of Birth</th>
      
     </tr>
   </thead>
   <tbody id="tbodyForm09" >
    <input type="hidden" name="familymembercount" id="familymembercount" value="<?php echo $familycount->Fcount;?>">
    <?php if ($familycount->Fcount == 0){ ?> 
      <tr id="tbodyForm09" >

        <td><input type="text" name="familymembername[]" id="familymembername" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" required="required" ></td>
        <td>

          <select class="form-control" name="relationwithenployee[]" id="relationwithenployee" required="required">
            <option value="">Select Relation</option>
            <?php foreach ($sysrelations as $key => $value): ?>
              <option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>
            <?php endforeach ?>
          </select>
          <?php 
                //  $options = array('' => 'Select Relation');
                //  foreach($sysrelations as$key => $value) {
                //   $options[$value->id] = $value->relationname;
                // }
                // echo form_dropdown('relationwithenployee[]', $options, set_value('relationwithenployee'), 'class="form-control" id="relationwithenployee" required="required"');
          ?>
          <?php echo form_error("relationwithenployee");?>
        </td>
        <td><input type="text" name="familymemberdob[]" id="familymemberdob" data-toggle="tooltip" title="Enter Family Member DOB !" value="" class="form-control datepicker" required="required" ></td>
        
    </tr>
  <?php }else{ $i=0;

   foreach ($familymemberdetails as $key => $val) {  ?>
    <tr id="tbodyForm09">
     <td><input type="text" name="familymembername[<?php echo $i; ?>]" id="familymembername_<?php echo $i; ?>" maxlength="50" value="<?php echo $val->Familymembername ?>" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" required="required"  ></td>
     <td>
      <select class="form-control" name="relationwithenployee[<?php echo $i; ?>]"  id="relationwithenployee_<?php echo $i;?>"  required="required">
        <option value="">Select Relation</option>
        <?php foreach ($sysrelations as $key => $value){ 
          if ($value->id==$val->relationwithemployee) {
            ?>
            <option value="<?php echo $value->id;?>" SELECTED><?php echo $value->relationname;?></option>
          <?php }else{ ?>
            <option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>
          <?php } }  ?>
        </select>
        <?php 
              //  $options = array('' => 'Select Relation');
              //  foreach($sysrelations as$key => $value) {
              //   $options[$value->id] = $value->relationname;
              // }
              // echo form_dropdown('relationwithenployee['.$i.']', $options, $val->relationwithemployee, 'class="form-control" data-toggle="tooltip" required="required"  title="Select  Relation With Employee !"  id="relationwithenployee"');
        ?>
        <?php echo form_error("relationwithenployee");?>
      </td>
      <td><input type="text" name="familymemberdob[<?php echo $i; ?>]" id="familymemberdob_<?php echo $i; ?>" data-toggle="tooltip" title="Enter Family Member DOB !" value="<?php if(isset($val->familydob)){ echo $this->model->changedate($val->familydob);}?>" class="form-control datepicker" required="required"  ></td>
      <td></td>
</tr>
<?php $i++;  } } ?>
</tbody>

</table>
</div>
</div>


<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

   <table id="tblForm13" class="table table-bordered dt-responsive table-striped" >
    <thead>
     <tr>
      <th colspan="4" style="background-color: #3CB371; color: #fff;">
        <div class="col-lg-12">
          <div class="col-lg-6">Identity Details</div>
          <?php if($this->loginData->BDFFormStatus !='1') { ?>
            <div class="col-lg-6 text-right ">
             <button type="button" id="btnidentitydetailRemoveRow" class="btn btn-danger btn-xs">Remove</button>
             <button type="button" id="btnidentitydetailAddRow" class="btn btn-warning btn-xs">Add</button>
           </div>
         <?php } ?>
       </div>  </th>
     </tr> 
     <tr>
       <th>Identity</th>
       <th>Number</th>
      
     </tr>
   </thead>
   <tbody id="tbodyForm13" >
    <input type="hidden" name="identcount" id="identcount" value="<?php echo $identitycount->Icount;?>">
    <?php if ($identitycount->Icount==0) { ?>
      <tr id="tbodyForm13" >
        <td>
          <?php 
          $options = array('' => 'Select Identity');
          foreach($sysidentity as$key => $value) {
            $options[$value->id] = $value->name;
          }
          echo form_dropdown('identityname[]', $options, set_value('identityname'), 'class="form-control"  id="identityname_0"  data-toggle="tooltip" title="Select Identity Name!" required="required" ');
          ?>
          <?php echo form_error("identityname");?>
        </td>

        <td><input type="text" name="identitynumber[]" id="identitynumber_0" onchange="return CheckIdentityValue(this.id);" value="" class="form-control" maxlength="30"  data-toggle="tooltip" title="Enter Identity Number!" placeholder="Enter Identity Number " required="required"></td>
        
  </tr>
<?php }else{ $i=0;
  foreach ($identitydetals as $key => $val) {  ?>
    <tr id="tbodyForm13" >
      <td>
        <?php 
        $options = array('' => 'Select Identity');
        foreach($sysidentity as$key => $value) {
          $options[$value->id] = $value->name;
        }
        echo form_dropdown('identityname['.$i.']', $options, $val->identityname, 'class="form-control"  id="identityname_'.$i.'"  data-toggle="tooltip" title="Select Identity Name!" required="required"  ');
        ?>
        <?php echo form_error("identityname");?>
      </td>

      <td><input type="text" name="identitynumber[<?php echo $i; ?>]" id="identitynumber"  maxlength="30" value="<?php echo $val->identitynumber;?>" class="form-control" required="required"   data-toggle="tooltip" title="Enter Identity Number!" placeholder="Enter Identity Number"></td>
      
    </tr>
    <?php $i++; } } ?>
  </tbody>

</table>
</div>
</div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">

    <table id="tbledubackground" class="table table-bordered dt-responsive table-striped">
      <thead>
       <tr>
        <th colspan="8" style="background-color: #3CB371; color: #fff;">Educational Background</th>
      </tr> 
      <tr>
        <th class="text-center" style="vertical-align: top;">Course</th>
        
        <th class="text-center" style="vertical-align: top;">Year </th>
        <th class="text-center" style="vertical-align: top;"> Board/ University</th>
        <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
        <th class="text-center" style="vertical-align: top;">Place</th>
        <th class="text-center" style="vertical-align: top;">Specialisation</th>
        <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
        
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><b>10th</b></td>
        <td><input type="text" class="form-control" data-toggle="tooltip" title="Year !" id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" style="min-width: 5px; max-width: 100px;" value="<?php echo $candidatedetails->metricpassingyear;?>" readonly="readonly" >
          <?php echo form_error("10thpassingyear");?></td>

          <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
            id="10thboarduniversity" name="10thboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->metricboarduniversity;?>"  readonly="readonly" >
            <?php echo form_error("10thboarduniversity");?></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->metricschoolcollege;?>" readonly="readonly" >
              <?php echo form_error("10thschoolcollegeinstitute");?></td>
              <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo $candidatedetails->metricplace;?>" readonly="readonly">
                <?php echo form_error("10thplace");?></td>

                <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                  id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->metricspecialisation;?>" readonly="readonly" >
                  <?php echo form_error("10thspecialisation");?></td>

                  
                  <td><input type="text" class="form-control txtNumeric"  data-toggle="tooltip" title="Percentage !" style="min-width: 5px; max-width: 70px;"
                    id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo $candidatedetails->metricpercentage;?>" readonly="readonly" >
                    <?php echo form_error("10thpercentage");?></td>
                    <td> </td>
            </tr>
            <tr>
              <td>
               <select name="twelthstream" id="twelthstream" class="form-control" required="required" disabled="disabled">
                <option value="1" <?php  if ($candidatedetails->hscstream==1) { echo "SELECTED";
              } ?>>12th</option>
              <option value="2" <?php  if ($candidatedetails->hscstream==2) { echo "SELECTED";
            } ?>>Diploma</option>
          </select> 
        </td>
        <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
          id="12thpassingyear" name="12thpassingyear" style="min-width: 5px; max-width: 100px;" placeholder="Enter Year" value="<?php echo $candidatedetails->hscpassingyear;?>" readonly="readonly">
          <?php echo form_error("12thpassingyear");?></td>


          <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
            id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php  echo $candidatedetails->hscboarduniversity;?>" readonly="readonly">
            <?php echo form_error("12thboarduniversity");?></td>

            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->hscschoolcollege;?>" readonly="readonly">
              <?php echo form_error("12thschoolcollegeinstitute");?></td>

              <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo $candidatedetails->hscplace;?>" readonly="readonly">
                <?php echo form_error("12thplace");?></td>

                <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                  id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->hscspecialisation;?>" readonly="readonly">
                  <?php echo form_error("12thspecialisation");?></td>

                  <td><input type="text" class="form-control txtNumeric" 
                    id="12thpercentage" name="12thpercentage" placeholder="Percentage" style="min-width: 5px; max-width: 70px;" value="<?php echo $candidatedetails->hscpercentage;?>" readonly="readonly" >
                    <?php echo form_error("12thpercentage");?></td>
                   
                 </tr>
                 <tr>
                  <td>
                    <?php
                    $options = array('' => 'Select');
                    $selected = $candidatedetails->ugdegree;
                    foreach($ugeducationdetails as $key => $value) {
                      $options[$value->ugname] = $value->ugname;
                    }
                    echo form_dropdown('ugstream', $options, $candidatedetails->ugdegree, 'class="form-control" id="ugstream" data-toggle="tooltip" title="Board/University !" disabled="disabled"');
                    ?>

                  </td>

                  <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                    id="ugpassingyear" name="ugpassingyear" placeholder="Enter Year" style="min-width: 5px; max-width: 100px;" value="<?php echo $candidatedetails->ugpassingyear; ?>" readonly="readonly">
                    <?php echo form_error("ugpassingyear");?></td>

                    <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                      id="ugboarduniversity" name="ugboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->ugboarduniversity;?>" readonly="readonly">
                      <?php echo form_error("ugboarduniversity");?></td>


                      <td>
                        <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
                        id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute " value="<?php echo $candidatedetails->ugschoolcollege;?>" readonly="readonly">
                        <?php echo form_error("ugschoolcollegeinstitute");?>
                      </td>

                      <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                        id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo $candidatedetails->ugplace;?>" readonly="readonly" >
                        <?php echo form_error("ugplace");?></td>

                        <td>
                          <input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                          id="ugspecialisation" name="ugspecialisation" placeholder="Enter Place" value="<?php echo $candidatedetails->ugspecialisation;?>" readonly="readonly" >

                          <?php echo form_error("ugspecialisation");?></td>
                          <td><input type="text" class="form-control txtNumeric" 
                            id="ugpercentage" name="ugpercentage" placeholder="Percentage" style="min-width: 5px; max-width: 70px;" value="<?php echo $candidatedetails->ugpercentage;?>" readonly="readonly">
                            <?php echo form_error("ugpercentage");?></td>
                           
                       </tr>
                       <tr>
                        <td>
                         <?php
                         $options = array('' => 'Select');

                         foreach($pgeducationdetails as $key => $value) {
                          $options[$value->pgname] = $value->pgname;
                        }
                        echo form_dropdown('pgupstream', $options, $candidatedetails->pgdegree, 'class="form-control"');
                        ?>
                        <?php echo form_error("pgupstream");?> 
                      </td>

                      <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
                        id="pgpassingyear" name="pgpassingyear" placeholder="Enter Year" style="min-width: 5px; max-width: 70px;" value="<?php echo $candidatedetails->pgpassingyear;?>" >
                      </td>
                      <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                        id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->pgboarduniversity;?>" >
                      </td>

                      <td>  
                        <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                        id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->pgschoolcollege;?>" >
                        <?php echo form_error("pgschoolcollegeinstitute");?>

                      </td>

                      <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                        id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo $candidatedetails->pgplace;?>" >
                      </td>

                      <td>
                       <input type="text" class="form-control " 
                       id="pgspecialisation" name="pgspecialisation" placeholder="Enter Specialisation"   value="<?php echo $candidatedetails->pgspecialisation;?>" >

                     </td>
                     <td><input type="text" class="form-control txtNumeric" 
                      id="pgpercentage" name="pgpercentage" placeholder="Percentage"  style="min-width: 5px; max-width: 70px;" value="<?php echo $candidatedetails->pgpercentage;?>" >
                    </td>
                    
                  </tr>
                  <tr>
                    <td>

                      <div class="form-check">
                        <input type="checkbox" class="filled-in" id="other" <?php if ($candidatedetails->otherdegree=='Other') {
                          echo 'checked="checked"';
                        } ;?> value="1" name="other_specify">
                        <label class="form-check-label" for="other"><b>Other (specify)</b></label>
                        <div id="other_other_specify_degree" style="display: none;">
                          <input type="text" name="specify_degree" id="specify_degree"  class="form-control" value="<?php echo $candidatedetails->other_degree_specify;?>">
                        </div>
                      </div>
                    </td>

                    <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !"  style="min-width: 5px; max-width: 100px;"
                      id="otherpassingyear" name="otherpassingyear" placeholder="Enter Year" value="<?php echo $candidatedetails->otherpassingyear;?>" >
                    </td>

                    <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                      id="otherboarduniversity" name="otherboarduniversity" placeholder="Enter Board/ University" value="<?php echo $candidatedetails->otherboarduniversity;?>" >
                    </td>
                    <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
                      id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $candidatedetails->otherschoolcollege;?>" >
                    </td>


                    <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                      id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo $candidatedetails->otherplace;?>" >
                    </td>

                    <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                      id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo $candidatedetails->otherspecialisation;?>" >
                    </td>

                    <td><input type="text" class="form-control txtNumeric" 
                      id="otherpercentage" name="otherpercentage" style="min-width: 5px; max-width: 70px;" placeholder="Percentage" value="<?php echo $candidatedetails->otherpercentage;?>" >
                    </td>
                    
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

           <table id="tblForm15" class="table table-bordered dt-responsive table-striped">
            <thead>
             <tr>
              <th colspan="3" style="background-color: #3CB371; color: #fff;">

                <div class="col-lg-6 text-left"> Gap Year </div>
                <?php if($this->loginData->BDFFormStatus !='1') {  ?>
                  <div class="col-lg-6 text-right ">
                   <button type="button" id="btnGapReasonRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                   <button type="button" id="btnGapReasonAddRow" class="btn btn-warning btn-xs">Add</button>
                 </div>
               <?php } ?>


             </th>
           </tr> 
           <tr>


            <th class="text-center" style="vertical-align: top;"> From Date</th>
            <th class="text-center" style="vertical-align: top;">To Date</th>
            <th>Reason</th>
          </tr> 
        </thead>
        <tbody id="bodytblForm15">
          <input type="hidden" name="TGapreason" id="TGapreason" value="<?php echo $TrainingExpcount->TEcount; ?>">
          <?php
                                      // echo $TrainingExpcount->TEcount;
          if ($GapYearCount->GYcount==0) { ?>
            <tr id="bodytblForm15">

             <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
              id="gapfromdate" name="gapfromdate[]" placeholder="From Date" style="min-width: 20%;"  value=""  ></td>
              <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                id="gaptodate" name="gaptodate[]" placeholder="To Date" style="min-width: 20%;" value=""></td>
                <td>
                  <textarea type="text" value="" rows="2" cols="5" name="gapreason[]" id="gapreason" class="form-control" data-toggle="tooltip"  maxlength="250" title="Gap Reason"   placeholder="Enter gap reason" ></textarea>
                </td>
              </tr>
            <?php } else{ $i=0;
                         // echo "dfgdfgdfg"; 
                         // print_r($trainingexposuredetals);
                         // die;
              foreach ($gapyeardetals as $key => $val) {  ?>
                <tr id="bodytblForm15">


                  <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" id="gapfromdate_<?php echo $i;?>" name="gapfromdate[<?php echo $i;?>]" placeholder="From Date" onclick="return changedatepicker(<?php echo $i;?>)"  style="min-width: 20%;"  value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" required="required" ></td>
                  <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" id="gaptodate_<?php echo $i;?>" name="gaptodate[<?php echo $i;?>]" placeholder="To Date" style="min-width: 20%;" required="required" onclick="return changedatepicker(<?php echo $i;?>)" value="<?php echo $this->model->changedatedbformate($val->todate);?>" ></td>
                  <td><textarea class="form-control" data-toggle="tooltip"  maxlength="250" name="gapreason[<?php echo $i;?>]" id="gapreason_[<?php echo $i;?>]" title="Gap Reason" placeholder="Enter gap reason"  value="" rows="2" cols="5" required="required" ><?php echo $val->reason;?></textarea>

                  </td>
                </tr>
                <?php $i++; } } ?>   
              </tbody>
            </table>
          </div>
        </div>


        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
           <?php //print_r($TrainingExpcount);  ?>
           <table id="tblForm10" class="table table-bordered dt-responsive table-striped">
            <thead>
             <tr>
              <th colspan="8" style="background-color: #3CB371; color: #fff;">

               <div class="col-lg-12">
                <div class="col-lg-6 text-left"> Training  Exposure (if any)</div>
                <?php if($this->loginData->BDFFormStatus !='1') {  ?>
                  <div class="col-lg-6 text-right ">
                   <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                   <!--  <span><a href="javascript:void(0);" class="btn btn-warning btn-xs add" >Add </a></span> -->
                   <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
                 </div>
               <?php } ?>
             </div>

           </th>
         </tr> 
         <tr>

          <th class="text-center" style="vertical-align: top;">Nature of Training </th>
          <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
          <th class="text-center" style="vertical-align: top;"> From Date</th>
          <th class="text-center" style="vertical-align: top;">To Date</th>
        </tr> 
      </thead>
      <tbody id="bodytblForm10">
        <input type="hidden" name="TEcount" id="count_te" value="<?php echo $TrainingExpcount->TEcount; ?>">
        <?php
                                      // echo $TrainingExpcount->TEcount;
        if ($TrainingExpcount->TEcount==0) { ?>
          <tr id="bodytblForm10">

            <td><input type="text" class="form-control " maxlength="150" data-toggle="tooltip" title="Nature of Training !" minlength="5"   maxlength="50"
              id="natureoftraining"  maxlength="150" name="natureoftraining[]" placeholder="Enter Nature of Training" style="min-width: 20%;"  value="" ></td>
              <td> <input type="text" class="form-control " data-toggle="tooltip" minlength="5"   maxlength="150" title="Organizing Agency !" 
                id="organizingagency" name="organizingagency[]"  placeholder="Organizing Agency" style="min-width: 20%;"  value="" ></td>
                <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
                  id="fromdate" name="fromdate[]" placeholder="From Date" style="min-width: 20%;"  value=""  ></td>
                  <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                    id="todate" name="todate[]" placeholder="To Date" style="min-width: 20%;"  value="" ></td>
                  </tr>
                <?php } else{ $i=0;
                         // echo "dfgdfgdfg"; 
                         // print_r($trainingexposuredetals);
                         // die;
                  foreach ($trainingexposuredetals as $key => $val) {  ?>
                    <tr id="bodytblForm10">

                      <td><input type="text" class="form-control inputclass" data-toggle="tooltip" title="Nature of Training !" minlength="5"   maxlength="150" required="required"
                        id="natureoftraining" name="natureoftraining[<?php echo $i;?>]" placeholder="Enter Nature of Training"  maxlength="150"style="min-width: 20%;"  
                        value="<?php echo $val->natureoftraining;?>" ></td>
                        <td> <input type="text" class="form-control" data-toggle="tooltip" minlength="5"   maxlength="50" title="Organizing Agency !" 
                          id="organizingagency" name="organizingagency[<?php echo $i;?>]" placeholder="Organizing Agency" style="min-width: 20%;"  value="<?php echo $val->organizing_agency;?>" required="required" ></td>
                          <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
                            id="fromdate_<?php echo $i;?>" name="fromdate[<?php echo $i;?>]" placeholder="From Date" onclick="return changedatepicker(<?php echo $i;?>)"  style="min-width: 20%;"  
                            value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" required="required" ></td>
                            <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" id="todate_<?php echo $i;?>" name="todate[<?php echo $i;?>]" placeholder="To Date" style="min-width: 20%;" required="required" onclick="return changedatepicker(<?php echo $i;?>)" value="<?php echo $this->model->changedatedbformate($val->todate);?>" ></td>
                          </tr>
                          <?php $i++; } } ?>   
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                     <table id="tblForm11" class="table table-bordered dt-responsive table-striped">
                      <thead>
                       <tr>
                        <th colspan="8" style="background-color: #3CB371; color: #fff;"> 

                         <div class="col-lg-12">
                          <div class="col-lg-6 text-left">Language Skill/Proficiency </div>
                          <?php if($this->loginData->BDFFormStatus !='1') {  ?>
                            <div class="col-lg-6 text-right ">
                             <button type="button" id="btnproficiencyRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                             <button type="button" id="btnproficiencyAddRow" class="btn btn-warning btn-xs">Add</button>
                           </div>
                         <?php } ?>
                       </div>
                     </th>
                   </tr> 
                   <tr>

                    <th class="text-center" style="vertical-align: top;">Language</th>
                    <th class="text-center" style="vertical-align: top;">Speak</th>
                    <th class="text-center" style="vertical-align: top;">Read</th>
                    <th class="text-center" style="vertical-align: top;">Write </th>
                  </tr> 
                </thead>
                <tbody id="bodytblForm11">
                  <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
                  <?php if ($languageproficiency->Lcount==0) { ?>
                    <tr id="bodytblForm11">
                      <td>  <?php
                      $options = array('' => 'Select');
                      foreach($syslanguage as $key => $value) {
                        $options[$value->lang_cd] = $value->lang_name;
                      }
                      echo form_dropdown('syslanguage[]', $options, set_value('syslanguage'), 'class="form-control" data-toggle="tooltip" title="Select language !" required="required" ');
                      ?>
                      <?php echo form_error("syslanguage");?></td>
                      <td>
                       <?php
                       $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                       echo form_dropdown('speak[]',$sysspeak,set_value('speak'),'class ="form-control" data-toggle="tooltip" title="Select Speak !" required="required"  ');
                       ?>
                       <?php echo form_error("speak");?>
                     </td>
                     <td><?php
                     $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                     echo form_dropdown('read[]',$sysread,set_value('read'),'class ="form-control" data-toggle="tooltip" title="Select Read !" required="required"  ');
                     ?>
                     <?php echo form_error("read");?></td>
                     <td><?php
                     $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                     echo form_dropdown('write[]',$syswrite,set_value('write'),'class ="form-control" data-toggle="tooltip" title="Select Write !" required="required"  ');
                     ?>
                     <?php echo form_error("write");?></td>
                   </tr>
                 <?php } else{ $i= 0;

                  foreach ($languagedetals as $key => $val) {
                   ?>
                   <tr id="bodytblForm11">
                    <td>  <?php
                    $options = array('' => 'Select');
                    foreach($syslanguage as $key => $value) {
                      $options[$value->lang_cd] = $value->lang_name;
                    }
                    echo form_dropdown('syslanguage['.$i.']', $options, $val->languageid, 'class="form-control" data-toggle="tooltip" title="Select language !" required="required" ');
                    ?>
                    <?php echo form_error("syslanguage");?></td>
                    <td>
                     <?php
                     $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                     echo form_dropdown('speak['.$i.']',$sysspeak,$val->lang_speak,'class ="form-control" data-toggle="tooltip" title="Select Speak !" required="required" ');
                     ?>
                     <?php echo form_error("speak");?>
                   </td>
                   <td><?php
                   $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                   echo form_dropdown('read['.$i.']',$sysread,$val->lang_read,'class ="form-control" data-toggle="tooltip" title="Select Read !" required="required" ');
                   ?>
                   <?php echo form_error("read");?></td>
                   <td><?php
                   $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                   echo form_dropdown('write['.$i.']',$syswrite,$val->lang_write,'class ="form-control" data-toggle="tooltip" required="required"  title="Select Write !" ');
                   ?>
                   <?php echo form_error("write");?></td>
                 </tr>

                 <?php $i++; } }  ?>
               </tbody>
             </table>
           </div>
         </div>
         <div class="row">



          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

           <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
            <thead>
             <tr>
              <th colspan="8" style="background-color: #3CB371; color: #fff;">Subject(s) of Interest </th>
            </tr> 

          </thead>
          <tbody>
            <tr>
              <td><textarea class="form-control" name="subjectinterest"  maxlength="1500" id="subjectinterest" cols="12" rows="5" data-toggle="tooltip" title="Enter Subject(s) of Interest !" ><?php 
              if(isset($otherinformationdetails->any_subject_of_interest)){
                echo $otherinformationdetails->any_subject_of_interest;
              }?></textarea></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

       <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
        <thead>
         <tr>
          <th colspan="8" style="background-color: #3CB371; color: #fff;">Achievements / Awards (if any) </th>
        </tr> 

      </thead>
      <tbody>
        <tr>

          <td><textarea class="form-control" name="achievementawards" id="achievementawards" cols="12" rows="5"  maxlength="1500" data-toggle="tooltip" title="Enter Achievement /Awards(if any) !" ><?php echo trim($otherinformationdetails->any_achievementa_awards); ?>
          </textarea></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>



<input type="hidden" name="candidatesysid" id="candidatesysid" value="<?php echo $this->loginData->candidateid;?>" >
<div class="row">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm12" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="7" style="background-color: #3CB371; color: #fff;">
       <div class="col-lg-12">
        <div class="col-lg-6 text-left">Work Experience (if any) </div>
        <?php if($this->loginData->BDFFormStatus !='1') {  ?>
          <div class="col-lg-6 text-right ">
           <button type="button" id="btnworkexperienceRemoveRow" class="btn btn-danger btn-xs">Remove</button>
           <button type="button" id="btnworkexperienceAddRow" class="btn btn-warning btn-xs">Add</button>
         </div>
       <?php } ?>
     </div>
   </th>
 </tr> 
 <tr>
  <th>Organization Name</th>
  <th>Designation</th>
  <th>Description of Assignment</th>
  <th>Duration</th>
  <th>Palce of Posting</th>
  <th>Last salary drawn (monthly)</th>
  
</tr> 
<tr>
  <th colspan="3"></th>
  <th colspan="1">
    <div class="col-lg-12">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
    </div></th>
    <th colspan="3"></th>
  </tr>
</thead>
<tbody id="bodytblForm12">
  <input type="hidden" name="workexpcount" id="workexpcount" value="<?php echo $WorkExperience->WEcount;?>">
  <?php if ($WorkExperience->WEcount==0) {?>
    <tr id="bodytblForm12"> 
      <td><input type="text" name="orgname[]" maxlengt="100" id="orgname" data-toggle="tooltip" value="" class="form-control" title="Enter Organization Name !" placeholder="Enter Organization Name " ></td>
      <td><input type="text" name="designation[]" maxlengt="50" id="designation" data-toggle="tooltip" value="" class="form-control" title="Enter Designation !" placeholder="Enter Designation" ></td>
      <td><textarea  maxlengt="250" name="descriptionofassignment[]" data-toggle="tooltip" id="descriptionofassignment" value="" class="form-control" title="Enter Description of Assignment" placeholder="Enter Description of Assignment " ></textarea></td>
      <td>
       <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
          <input type="text" name="work_experience_fromdate[]" id="work_experience_fromdate" value="" class="form-control datepicker" value=""  placeholder="Enter from date">
        </div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
          <input type="text" name="work_experience_todate[]" id="work_experience_todate" class="form-control datepicker" value="" placeholder="Enter To date">
        </div> 
      </div>
    </td>
    <td><input type="text" maxlengt="50" name="palceofposting[]" data-toggle="tooltip" id="palceofposting" value="" class="form-control" title="Place of Posting[]" placeholder="Enter Place of Posting"></td>
    <td><input type="text" maxlengt="50" name="lastsalarydrawn[]" data-toggle="tooltip" id="lastsalarydrawn" value="" class="form-control txtNumeric" title="lastsalarydrawn" placeholder="Enter Last salary drawn"></td>

   
  </tr>
<?php  } else{
  $i=0;
  foreach ($workexperiencedetails as $key => $val) {   ?>
    <tr id="bodytblForm12"> 
      <td><input type="text" maxlengt="100" name="orgname[<?php echo $i;?>]" id="orgname" data-toggle="tooltip" value="<?php echo $val->organizationname;?>" class="form-control" title="Enter Organization Name !" placeholder="Enter Organization Name " required ></td>
      <td><input type="text" name="designation[]" maxlengt="50" id="designation" data-toggle="tooltip" value="<?php echo $val->designation;?>" class="form-control" title="Enter Designation !" placeholder="Enter Designation" ></td>
      <td><textarea maxlengt="250" name="descriptionofassignment[<?php echo $i;?>]" data-toggle="tooltip" id="descriptionofassignment" required value="" class="form-control" title="Enter Description of Assignment" placeholder="Enter Description of Assignment " > <?php echo $val->descriptionofassignment;?></textarea></td>
      <td>
       <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
          <input type="text" name="work_experience_fromdate[<?php echo $i;?>]" id="work_experience_fromdate_<?php echo $i;?>" value="<?php echo $this->model->changedate($val->fromdate);?>" required class="form-control datepicker" value=""  placeholder="Enter from date">
        </div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
          <input type="text" name="work_experience_todate[<?php echo $i;?>]" id="work_experience_todate_<?php echo $i;?>" class="form-control datepicker" value="<?php echo $this->model->changedate($val->todate);?>" required placeholder="Enter To date">
        </div> 
      </div>
    </td>
    <td><input type="text" maxlength="50" name="palceofposting[<?php echo $i;?>]" data-toggle="tooltip" id="palceofposting" 
      value="<?php echo $val->palceofposting;?>" class="form-control " required title="Place of Posting[]" placeholder="Enter Place of Posting"></td>
      <td><input type="text" maxlengt="50" name="lastsalarydrawn[]" data-toggle="tooltip" id="lastsalarydrawn" value="<?php echo $val->lastsalarydrawn;?>" class="form-control txtNumeric" title="lastsalarydrawn" placeholder="Enter Last salary drawn"></td>
      
 </tr>

 <?php $i++; } } ?>
</tbody>
</table>
</div>
</div>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
   <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Describe any assignment(s) of special interest undertaken by you (if any) </th>
    </tr> 

  </thead>
  <tbody>
    <tr>
     <td><textarea class="form-control" data-toggle="tooltip"  maxlength="1500" name="any_assignment_of_special_interest" id="any_assignment_of_special_interest" cols="12" rows="5" title="Describe any Assignment(s) of special interest undertaken by you(if any) " placeholder="Enter Achievement /Awards" ><?php 
     if (isset($otherinformationdetails->any_assignment_of_special_interest)) {
      echo $otherinformationdetails->any_assignment_of_special_interest;
    }
    ?></textarea></td>
  </tr>
</tbody>
</table>
</div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Experience of Group and/or Social Activities </th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><textarea class="form-control" data-toggle="tooltip"  maxlength="1500" name="experience_of_group_social_activities" id="experience_of_group_social_activities" cols="12" rows="5" title="Experience of Group and/or Social Activities " placeholder="Enter Experience of Group and/or Social Activities " ><?php 
      if (isset($otherinformationdetails->experience_of_group_social_activities)) {
        echo $otherinformationdetails->experience_of_group_social_activities;
      }
      ?></textarea></td>
    </tr>
  </tbody>
</table>
</div>
</div>


<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblgap" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <td colspan="8" style="background-color: #3CB371; color: #fff;"><b>Have you taken part in PRADAN's selection process before?</b>
      </td>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
          <div class="form-check">
            <input type="radio" class="form-check-input" id="yes" value="yes" 
            <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='yes') {
              echo 'checked="checked"';
            }
            ?>
            name="pradan_selection_process_before">
            <label class="form-check-label" for="yes"><b>Yes</b></label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" class="form-check-input" id="no" value="no" 
            <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='no') {
              echo 'checked="checked"';
            }else{
              echo 'checked="checked"';
            }
            ?>
            name="pradan_selection_process_before"  >
            <label class="form-check-label" for="no"><b>No</b></label>

          </div>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <input type="text" name="when" id="when" value="<?php echo $this->model->changedate($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when); ?>" class="form-control datepicker">
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
          <select name="where" id="where" class="form-control">
            <option value="">select</option>
            <option value="1" <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==1) {
             echo 'selected="selected"';
           } ?>>ON Campus</option>
           <option value="2" <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==2) {
             echo 'selected="selected"';
           } ?>>Off Campus</option>
         </select>
       </div>

     </td>
   </tr>
 </tbody>
</table>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      var selected = $('#pradan_selection_process_before').val();
      if (selected =='yes') {
        $("#when").removeAttr('disabled','false'); 
        $("#where").removeAttr('disabled','false');
      }else{
        $("#when").prop('disabled','true'); 
        $("#where").prop('disabled','true');
      }

   
    $("input[name$='pradan_selection_process_before']").click(function() {
      var selected = $(this).val();

      if (selected =='yes') {
        $("#when").removeAttr('disabled','false'); 
        $("#where").removeAttr('disabled','false');
      }else{
       $('#when').attr('value', '');  
       $('#where option').slice(1,2).remove(); 
       $("#when").prop('disabled','true'); 
       $("#where").prop('disabled','true');
     }

   });

  });
</script>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <td colspan="8" style="background-color: #3CB371; color: #fff;"><b>From where have you come to know about PRADAN?</b>
      </td>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
          <div class="form-check">
            <input type="radio" class="form-check-input" id="campus_placement_cell" <?php if ($otherinformationdetails->know_about_pradan=='CampusPlacementCell') {
             echo 'checked="checked"';
           }  ?> value="CampusPlacementCell" name="have_you_come_to_know">
           <label class="form-check-label" for="campus_placement_cell"><b>Campus Placement Cell</b></label>
         </div>
       </div>
       <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
         <!-- Material checked -->
         <div class="form-check">
          <input type="radio" class="form-check-input" id="university_professors"  <?php if ($otherinformationdetails->know_about_pradan=='UniversityProfessors') {
           echo 'checked="checked"';
         }  ?> value="UniversityProfessors" name="have_you_come_to_know"  >
         <label class="form-check-label" for="university_professors"><b>University Professors</b></label>
       </div>
     </div>
     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
       <!-- Material checked -->
       <div class="form-check">
        <input type="radio" class="form-check-input" id="campus_alumni"  <?php if ($otherinformationdetails->know_about_pradan=='CampusAlumni') {
         echo 'checked="checked"';
       }  ?> value="CampusAlumni" name="have_you_come_to_know" >
       <label class="form-check-label" for="campus_alumni"><b>Campus Alumni</b></label>
     </div>
   </div>
   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
    <!-- Material checked -->
    <div class="form-check">
      <input type="radio" class="form-check-input" id="friends" <?php if ($otherinformationdetails->know_about_pradan=='friends') {
       echo 'checked="checked"';
     }  ?> value="friends" name="have_you_come_to_know" >
     <label class="form-check-label" for="friends"><b>Friends</b></label>
   </div>
 </div>
 <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2"> 
   <!-- Material checked -->
   <div class="form-check">
    <input type="radio" class="form-check-input" id="other" <?php if ($otherinformationdetails->know_about_pradan=='other') {
     echo 'checked="checked"';
   }  ?>  value="other" name="have_you_come_to_know">
   <label class="form-check-label" for="other"><b>Other (specify)</b></label>
   <div id="other_have_you_come_to_know" style="display: none;">
    <input type="text" name="specify" id="specify" value="<?php echo $otherinformationdetails->know_about_pradan_other_specify ?>" class="form-control">
  </div>
</div>
</div>

</td>
</tr>
</tbody>
</table>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {

   $("input[name$='have_you_come_to_know']").click(function() {
    var test = $(this).val();
    if (test =='other') {
      $("#other_have_you_come_to_know").show();
      $("#specify").prop('required','required');
      $('#specify').removeAttr("disabled");

    }else{
      $("#other_have_you_come_to_know").hide();
      $("#specify").prop('disabled','true');

    }

  });
 });

  $("input[name$='have_you_come_to_know']").load(function() {
    var test = $(this).val();
    if (test =='other') {
      $("#other_have_you_come_to_know").show();
      $("#specify").prop('required','required');
      $('#specify').removeAttr("disabled");

    }else{
      $("#other_have_you_come_to_know").hide();
      $("#specify").prop('disabled','true');

    }

  });
</script>


<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: #ffffff;">
    <!--  <a class="btn btn-warning  btnPrevious" >Previous</a> -->

    <?php if(($getbdfformstatus->BDFStatusaftergd =='NULL' || $getbdfformstatus->BDFStatusaftergd =='99') && $getbdfformstatus->campustype =='off'){ ?>
     <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-primary text-center" onclick="return ifanyvalidation()">Save </button>
   <?php }else if(($getbdfformstatus->BDFStatusaftergd =='NULL' || $getbdfformstatus->BDFStatusaftergd =='99') && $getbdfformstatus->campustype =='on'){  ?>
     <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-primary text-center" onclick="return ifanyvalidation()">Save</button>
   <?php }else if($getbdfformstatus->BDFStatusaftergd =='' && $getbdfformstatus->campustype =='on'){  ?>
     <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-primary text-center" onclick="return ifanyvalidation()">Save</button>
   <?php } ?>
   <br><br>
 </div>

</div>
</div>
<!-- End  Candidates Experience Info -->
</div>
</div>
</form>
</section>
<script type="text/javascript">

  function ifanyvalidation(){
    var natureoftraining = $('#natureoftraining').val();
    var organizingagency = $('#organizingagency').val();
    var fromdate = $('#fromdate').val();
    var todate = $('#todate').val();
    var orgname = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();
    var experiencedocument = $('#experiencedocument').val();

    var gapfromdate = $('#gapfromdate').val();
    var gaptodate = $('#gaptodate').val();
    var gapreason = $('#gapreason').val();

    if (natureoftraining !='') {
      if (organizingagency =='') {

       $('#organizingagency').focus();

       return false;
     }
     if (fromdate =='') {
       $('#fromdate').focus();
       
       return false;
     }
     if (todate =='') {
       $('#todate').focus();

       return false;
     }

   }


   if (gapfromdate !='') {
    if (gaptodate =='') {

     $('#gaptodate').focus();

     return false;
   }
   if (gapreason =='') {
     $('#gapreason').focus();

     return false;
   }

 }

 if (orgname !='') {
  if (descriptionofassignment =='') {
   $('#descriptionofassignment').focus();

   return false;
 }
 if (work_experience_fromdate =='') {
  $('#work_experience_fromdate').focus();

  return false;
}
if (work_experience_todate =='') {
 $('#work_experience_todate').focus();

 return false;
}
if (palceofposting =='') {
 $('#palceofposting').focus();

 return false;
}
      // if (experiencedocument =='') {
      //    $('#experiencedocument').focus();
      //    $('#experiencedocument').css({ "border": "1px solid red"});
      //     return false;
      // }

    }
  }
  
</script>


<script type="text/javascript">


 $(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
 $(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
 
 var matriccertificate_uploadField = document.getElementById("10thcertificate");

 matriccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 


var hsccertificate_uploadField = document.getElementById("12thcertificate");

hsccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

var ugcertificate_uploadField = document.getElementById("ugcertificate");

ugcertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

</script>
<script type="text/javascript">

  $(document).ready(function(){
    decimalData();
    adddatepicker(inc);
    //addworkdatepicker();
    changegapyeardatepicker(inctg)
    changedatepicker(inctg);
    workexchangedatepicker(incwe)
    $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
    if($('#selection_process_befor').prop("checked") == false){
     $("#pradan_selection_process_before_details").prop("disabled", "disabled");
   }

   //$('#pradan_selection_process_before_details').prop("disabled",true); 

 });

  $(function () {
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
  });
  

  $(document).ready(function(){
    $("#filladdress").on("click", function(){
     if (this.checked) { 
      $("#permanenthno").val($("#presenthno").val());
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());;
    }
    else {
       $("#permanenthno").val('');
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');  

           
    }
  });

    if ($("#presentstreet").val() == $("#permanentstreet").val()) {
      $("#filladdress").prop('checked', true);
      $("#filladdress").prop("disabled", true);
    }

  });


  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }

  $("#btnRemoveRow").click(function() {

    //alert($('#tblForm09 tr').length);

    if($('#tblForm09 tr').length-2>1)

      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {
    rowsEnter = $('#tbodyForm09 tr').length;
    
    // if (rowsEnter < 1) {
    //   alert("Row number must be minimum 1.")
    //   return;
    // }
    insertRows(rowsEnter);
    //addDatePicker();
  });


  var familymemcount = $('#familymembercount').val();

  if(familymemcount =='NULL'){
   var srNoGlobal=0;
   var inc = 0;
 }else{
   var srNoGlobal = familymemcount;
   var inc = familymemcount;
 }

 function insertRows(count) {
  srNoGlobal = $('#tbodyForm09 tr').length+1;
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;
 var inc= count;
  // for (i = 0; i < count; i++) {
  //   if(familymemcount =='NULL'){
  //     inc++;
  //   }

    cloneRow = lastRow.clone();
    var tableData = '<tr>'
    + ' <td>'
    + '<input type="text" name="familymembername['+inc+']" id="familymembername_'+inc+'" value="" class="form-control" required="required" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
    + '</td><td>'
    + '<select class="form-control" name="relationwithenployee['+inc+']" id="relationwithenployee_'+inc+'" required="required">'
    + '<option value="">Select Relation</option>'
    <?php foreach ($sysrelations as $key => $value): ?>
      + '<option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>'
    <?php endforeach ?>
    + '</select>'
    + '</td>'
    + '<td><input type="text" class="form-control datepicker"  id="familymemberdob_'+inc+'" '+
    'name="familymemberdob['+inc+']" value=" " maxlength="100" required="required" />'
    + '</td>'
    + '</tr>';
   // adddatepicker(inc);
    $("#tbodyForm09").append(tableData)
    adddatepicker(inc);
  //}

}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
    rowsEnter1 = $('#bodytblForm10 tr').length;
   
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });
  var  counttraining = $("#count_te").val();
  //alert(counttraining);

  if(counttraining==0){
   var srNoGlobal=0;
   var inctg = 0;
 }else{
   var srNoGlobal= counttraining;
   var inctg = counttraining;

 }

 function Insaettrainingexposure(count) {
  srNoGlobal = $('#bodytblForm10 tr').length+1;
  var tbody = $('#bodytblForm10');
  var lastRow = $('#bodytblForm10 tr:last');
  var cloneRow = null;
 var inctg = count


 cloneRow = lastRow.clone();
 var tableData1 = '<tr>'
 + '<td> <input type="text"  class="form-control " data-toggle="tooltip" title="" minlength="5" maxlength="150" id="natureoftraining" name="natureoftraining['+inctg+']" placeholder="Enter Nature of Training" style="min-width: 20%;" value="" data-original-title="Nature of Training !" required="required">'
 + '</td>'
 + '<td class="focused"> <input type="text" class="form-control " data-toggle="tooltip" minlength="5" maxlength="150" title="" id="organizingagency['+inctg+']" name="organizingagency['+inctg+']" placeholder="Organizing Agency" style="min-width: 20%;" value="" data-original-title="Organizing Agency !" required="required"></td>'

 + '<td><input type="text" class="form-control datepicker" id="fromdate_'+inctg+'" '+
 'name="fromdate['+inctg+']" onclick="return changedatepicker(inctg)" value="" required="required"/>'
 + '</td><td>'
 + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="todate_'+inctg+'" '+
 'name="todate['+inctg+']" value="" required="required" />'
 + '</td>' + '</tr>';
 adddatepicker();
 $("#bodytblForm10").append(tableData1)
 changedatepicker(inctg);


}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnproficiencyRemoveRow").click(function() {
    if($('#tblForm11 tr').length-2>1)
      $('#bodytblForm11 tr:last').remove()
  });

  $('#btnproficiencyAddRow').click(function() {
    rowsEnter2 = $('#bodytblForm11 tr').length

   
    Insaetproficiency(rowsEnter2);
    //addDatePicker();
  });



  var lancount = $('#languagecount').val();
  if(lancount =='NULL'){
    var srNoGlobal=0;
    var inclp = 0;
  }else{
   var srNoGlobal=lancount;
   var inclp = lancount;
 }

 function Insaetproficiency(count) {
  srNoGlobal = $('#bodytblForm11 tr').length+1;
  var tbody = $('#bodytblForm11');
  var lastRow = $('#bodytblForm11 tr:last');
  var cloneRow = null;
  var inclp = count;
 
    cloneRow = lastRow.clone();
    var tableData2 = '<tr>'

    + '<td><select class="form-control" name="syslanguage['+inclp+']" required="required">'
    <?php foreach ($syslanguage as $key => $value): ?>
      + '<option value=<?php echo $value->lang_cd;?>><?php echo $value->lang_name;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'
    + '<select class="form-control" name="speak['+inclp+']" required="required">'
    <?php foreach ($sysspeak as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'

    + '<select class="form-control" name="read['+inclp+']" required="required">'
    <?php foreach ($sysread as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
    <?php endforeach ?>
    + '</select> </td><td>'

    + '<select class="form-control" name="write['+inclp+']" required="required">'
    <?php foreach ($syswrite as $key => $value): ?>
      + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
    <?php endforeach ?>
    + '</select> </td>'
    + '</tr>';
    adddatepicker();
    $("#bodytblForm11").append(tableData2)
 

}
  //insertRows();
</script>


<script type="text/javascript">
  $("#btnworkexperienceRemoveRow").click(function() {
    if($('#tblForm12 tr').length-2>1)
      $('#bodytblForm12 tr:last').remove()
  });

  $('#btnworkexperienceAddRow').click(function() {
    rowsEnter2 = $('#bodytblForm12 tr').length;
    
    Insertworkexperience(rowsEnter2);
    //addDatePicker();
  });

  var workexpcount = $('#workexpcount').val();
  //alert(workexpcount);

  if (workexpcount =='NULL') {
    var srNoGlobal=0;
    var incwe = 0;
  }else{

    var srNoGlobal = workexpcount;
    var incwe = workexpcount;

  }


  function Insertworkexperience(count) {
    srNoGlobal = $('#bodytblForm12 tr').length+1;
    var tbody = $('#bodytblForm12');
    var lastRow = $('#bodytblForm12 tr:last');
    var cloneRow = null;
    var incwe = count;
   

      cloneRow = lastRow.clone();
      var tableData2 = '<tr>'
      + '<td>'
      + '<input type="text" name="orgname['+incwe+']" id="orgname" data-toggle="tooltip" value="" class="form-control" maxlengt="100" data-original-title="" required="required" title="">'
      
      + '</td><td>'
      + '<input type="text" name="designation['+incwe+']" id="designation" data-toggle="tooltip" value="" class="form-control" maxlengt="50" data-original-title="" required="required" title="">'

      + '</td><td>'
      + '<textarea name="descriptionofassignment['+incwe+']" data-toggle="tooltip" maxlengt="250"  id="descriptionofassignment" required="required" class="form-control" data-original-title="" title=""> '
      + '</textarea></td><td>'

      + ' <div class="col-lg-12">'
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">'
      + '    <input type="text" required="required" name="work_experience_fromdate['+incwe+']"  data-toggle="tooltip" id="work_experience_fromdate_'+incwe+'" onclick="return workexchangedatepicker(incwe)" value="" class="form-control datepicker" value="">'
      + ' </div>' 
      + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">'
      + ' <input type="text" name="work_experience_todate['+incwe+']" onclick="return workexchangedatepicker(incwe)"  data-toggle="tooltip" id="work_experience_todate_'+incwe+'"  required="required" class="form-control datepicker" value=""></div> </div>'
      + '</td><td>'

      + '<input type="text" maxlengt="50" name="palceofposting['+incwe+']" data-toggle="tooltip" id="palceofposting" required="required" value="" class="form-control" data-original-title=""  title="">'
      + ' </td><td>'
      + '<input type="text" maxlengt="10" name="lastsalarydrawn['+incwe+']" data-toggle="tooltip" id="lastsalarydrawn" required="required" value="" class="form-control txtNumeric" data-original-title=""  title="">'

      + '</td>' + '</tr>';
      $("#bodytblForm12").append(tableData2)
      adddatepicker();
    
  }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-2>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = $('#tbodyForm13 tr').length;
    insertIdentityRows(rowsIdentityEnter);
  
  });

  var identcount = $('#identcount').val();

  if (identcount =='NULL') {
    var srNoIdentity=0;
    var incr = 0;
  }else{
    var srNoIdentity = identcount;
    var incr = identcount;
  }
  

  function insertIdentityRows(count) {
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;
    var incr = count;

    cloneRow = lastRow.clone();
    var tableDataIdentity = '<tr>'
    + ' <td>'
    + '<select class="form-control" name="identityname['+incr+']"  id="identityname_'+incr+'"  required="required">'
    + '<option value="">Select Relation</option>'
    <?php foreach ($sysidentity as $key => $value): ?>
      + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
    <?php endforeach ?>
    + '</select>'

    + ' <td><input type="text" name="identitynumber['+incr+']" id="identitynumber_'+incr+'"  maxlength="30" value="" onchange="return CheckIdentityValue(this.id);" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" required="required">'
    + '</td>'
    
    + '</tr>';
    $("#tbodyForm13").append(tableDataIdentity)
  
}
</script>

<script type="text/javascript">
  $("#btnGapReasonRemoveRow").click(function() {
    if($('#tblForm15 tr').length-2>1)
      $('#bodytblForm15 tr:last').remove()
  });

  $('#btnGapReasonAddRow').click(function() {
    rowsEnter1 = $('#bodytblForm15 tr').length
   
    InsGapReason(rowsEnter1);
    //addDatePicker();
  });
  var  counttraining = $("#count_te").val();
  //alert(counttraining);

  if(counttraining==0){
   var srNoGlobal=0;
   var inctg = 0;
 }else{
   var srNoGlobal= counttraining;
   var inctg = counttraining;

 }

 function InsGapReason(count) {
  srNoGlobal = $('#bodytblForm15 tr').length+1;
  var tbody = $('#bodytblForm15');
  var lastRow = $('#bodytblForm15 tr:last');
  var cloneRow = null;
  var inctg = count
  cloneRow = lastRow.clone();
 var tableData15 = '<tr>'
 + '<td><input type="text" class="form-control datepicker" id="gapfromdate_'+inctg+'" '+
 'name="gapfromdate['+inctg+']" onclick="return changedatepicker(inctg)" value="" required="required"/>'
 + '</td><td>'
 + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="gaptodate_'+inctg+'" name="gaptodate['+inctg+']" value="" required="required" />'
 + '</td><td>' 
 + '<textarea class="form-control"  maxlength="250" name="gapreason['+inctg+']" id="gapreason_'+inctg+'" title="Gap Reason" placeholder="Enter gap reason" required="required" ></textarea>'
 + '</td>'
 + '</tr>';
 adddatepicker();
 $("#bodytblForm15").append(tableData15)
 changegapyeardatepicker(inctg);

}
  //insertRows();
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  //  $("#btnsubmit")..prop('disabled', true);

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){

  $("#presentstateid").load(function(){
    // var presentstaid = $("#presentstateid").val();

    // alert(presentstaid);

    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").load(function(){
     alert($(this).val());
    $.ajax({
      url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     


 $(document).ready(function(){
  $("#presentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     


  function adddatepicker(inc){
   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
      jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
    }
  });

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#work_experience_fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#gapfromdate").datepicker( "option", "maxDate", selectedDate );

    }
  });
 }


 function changedatepicker(inctg){

   $("#fromdate_"+inctg).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#todate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
      });

   $("#todate_"+inctg).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#fromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
    }
  });
 }

 function changegapyeardatepicker(inctg){

   $("#gapfromdate_"+inctg).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#gaptodate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
      });

   $("#gaptodate_"+inctg).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#gapfromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
    }
  });
 }


 function workexchangedatepicker(incwe){

   $("#work_experience_fromdate_"+incwe).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    onClose: function(selectedDate) {
      jQuery("#work_experience_todate_"+incwe).datepicker("option", "minDate", selectedDate);
    }
  });

   $("#work_experience_todate_"+incwe).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      jQuery("#work_experience_fromdate_"+incwe).datepicker("option", "maxDate", selectedDate);
    }
  });

 }

 function CheckIdentityValue(id){  
 
  var indno =  id.replace('identitynumber_','');
 var txtidentityname = $('#identityname_'+indno).val(); 
 var txtidentitynumber = $('#identitynumber_'+indno).val();

if (txtidentityname ==1) {
  var regExp = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/; 
    if (txtidentitynumber.length ==10 ) {

      if (txtidentitynumber.match(regExp)) {
          var textpannumber = txtidentitynumber.toUpperCase();
              txtidentitynumber = textpannumber;
      }else{
        $('#identitynumber_'+indno).val('');
         alert('Not a valid PAN number');
      event.preventDefault(); 
      }
     
    }else{
      $('#identitynumber_'+indno).val('');
         alert('Not a valid PAN number');
      event.preventDefault(); 
      }
  }else if (txtidentityname ==2) {

        var adharcardTwelveDigit = /^\d{12}$/;
        if (txtidentitynumber != '') {
            if (txtidentitynumber.match(adharcardTwelveDigit)) {
                return true;
            }
            else {
                 $('#identitynumber_'+indno).val('');
                alert("Enter valid Aadhar Number");
                return false;
            }
        }

 }else if (txtidentityname == 4) {

        var PRANTwelveDigit = /^\d{12}$/;
      
        if (txtidentitynumber != '') {
            if (txtidentitynumber.match(PRANTwelveDigit)) {
                return true;
            }else{
                    $('#identitynumber_'+indno).val('');
                    alert("Enter valid PRAN !!!");
                   return false;
            }
}
 }else if (txtidentityname ==5) {

        var adharcardTwelveDigit = /^\d{12}$/;
      
        if (txtidentitynumber != '') {
            if (txtidentitynumber.match(adharcardTwelveDigit)) {
                return true;
            }else {
                    $('#identitynumber_'+indno).val('');
                    alert("Enter valid UAN !!!");
                     return false;
            }
  }
 }else if (txtidentityname ==6) {

         var regExp = /[a-zA-z]{1}\d{6}/; 
      
        if (txtidentitynumber != '') {
            if (txtidentitynumber.match(regExp)) {
                return true;
            }else {
                 $('#identitynumber_'+indno).val('');
                alert("Enter valid passport number !!!");
                return false;
            }
 }
}else if (txtidentityname ==7) {

        if (txtidentitynumber == '') {
       $('#identitynumber_'+indno).val('');
          alert("Please enter other(specify) identity number !!!");
          return false;

        }
 
} else{

 alert('Please select identity name');
 $('#identitynumber_'+indno).val('');
 $('#identityname_'+indno).focus();
  event.preventDefault(); 
}


}
</script>
<script type="text/javascript">
  $(document).ready(function() {
   
      if ( $('#other').is(':checked') ) {
        $("#other_other_specify_degree").show();
        $("#specify_degree").prop('required','required');
      } else {
         $("#other_other_specify_degree").hide();
          $("#specify").prop('disabled','true');
      }
  

    $("input[name$='other_specify']").change(function() {
      //var test = $(this).val();
      if ( $(this).is(':checked') ) {
        $("#other_other_specify_degree").show();
        $("#specify_degree").prop('required','required');
      } else {
         $("#other_other_specify_degree").hide();
          $("#specify").prop('disabled','true');
      }
    });
  });
</script>