<br>

<div class="container" id="PrintContainer" style="background-color: #FFFFFF; width: 900px;">
  <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
  <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
        <thead>
         <tr>
          <th  colspan="3" style="background-color: #3CB371; color: #fff; height: 5px;">Basic Info</th>
        </tr> 
      </thead>
      <tbody>
        <tr>
          <td colspan="3">
           <div class="row">
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">First name :</label></div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->candidatefirstname;?></div>

             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Middle name :</label></div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->candidatemiddlename;?></div>

             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Last Name :</label></div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->candidatelastname;?></div>

           </div>

           <div class="row">      
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Mother's name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->motherfirstname;?></div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Middle name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->mothermiddlename;?></div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Last Name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->motherlastname;?></div>

          </div>

          <div class="row">
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <label for="Name">Father's name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <?php echo $candidatedetails->fatherfirstname;?></div>


              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <label for="Name">Middle name :</label></div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <?php echo $candidatedetails->fathermiddlename;?>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <label for="Name">Last name :</label></div>
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <?php echo $candidatedetails->fatherlastname;?> 
                  </div>
                </div>
                <br>
                <div class="row">
                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <label for="Name">Gender </label></div>
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <?php 
                    if ($candidatedetails->gender ==1) {
                      echo "Male";
                    }else{
                     echo "Female";
                   } ?>

                 </div>

                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <label for="Name">Nationality  </label> </div>
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <?php echo  $candidatedetails->nationality;  ?> </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                      <label for="Name">Marital Status </label></div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <?php 
                        if ($candidatedetails->maritalstatus ==1) {
                          echo "Single";
                        }else{
                         echo "Married";
                       }
                       ?>
                     </div>
                   </div>
                   <br>
                   <div class="row">
                     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                      <label for="Name">Date Of Birth </label></div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <?php echo $this->model->changedate($candidatedetails->dateofbirth);?></div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                          <label for="Name">Email Id </label></div>
                          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <?php echo $candidatedetails->emailid;?></div>

                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                              <label for="Name">Mobile No .</label></div>
                              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <?php echo $candidatedetails->mobile;?>
                              </div>
                            </div>
                            <br>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table id="tbledubackground" class="table table-bordered dt-responsive table-striped" >
                      <thead>
                       <tr>
                        <th colspan="8" style="background-color: #3CB371; color: #fff;"> 
                          <div class="form-check">
                            <label class="form-check-label" for="filladdress"><b>Communication Address </b></label>
                          </div>
                        </th>
                      </tr> 
                      <tr>
                        <th class="text-center" colspan="4" style="vertical-align: top;">Present Mailing Address</th>
                        <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Mailing Address<br>
                           <div class="form-check">
                        <input type="checkbox" class="form-check-input" checked="checked" id="filladdress"> 
                        <label class="form-check-label" for="filladdress"><b>same as present address</b></label>
                      </div>
                     </th>
                      </tr> 
                    </thead>
                    <tbody>
                     <tr>
                      <td> <label for="Name">H.No/Street</label></td>
                      <td><?php echo $candidatedetails->presentstreet;?>
                        <input type="hidden" name="presentstreet" id="presentstreet"  
                  value=""<?php echo $candidatedetails->presentstreet;?>  >
                      </td>
                      <td> <label for="Name">City</label> </td>
                      <td><?php echo $candidatedetails->presentcity;?></td>
                      <td><label for="Name">H.No/Street</label></td>
                      <td><?php echo $candidatedetails->permanentstreet;?>
                        
                        <input type="hidden" name="permanentstreet"  id="permanentstreet" value="<?php echo $candidatedetails->permanentstreet;?>">
                      </td>
                      <td><label for="Name">City</label></td>
                      <td><?php echo $candidatedetails->permanentcity;?></td>
                    </tr>
                    <tr>
                      <td><label for="Name">State</label></td>
                      <td>  <?php echo  $candidatedetails->statename; ?>
                    </td>
                    <td><label for="Name">District</label> </td>
                    <td><?php echo $candidatedetails->districtname;?></td>
                    <td><label for="Name">State</label></td>
                    <td>  <?php echo $candidatedetails->statename; ?>
                  </td>
                  <td><label for="Name">District</label></td>
                  <td><?php echo $candidatedetails->districtname;?></td>
                </tr>
                <tr>
                  <td><label for="Name">Pin Code</label></td>
                  <td><?php echo $candidatedetails->presentpincode;?></td>
                  <td> </td>
                  <td></td>
                  <td><label for="Name">Pin Code</label></td>
                  <td><?php echo $candidatedetails->permanentpincode;?></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>  
        </div>
       
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">

  <table id="tbledubackground" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Educational Background</th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Course</th>

      <th class="text-center" style="vertical-align: top;">Year </th>
      <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
      <th class="text-center" style="vertical-align: top;"> Board/ University</th>
      <th class="text-center" style="vertical-align: top;">Specialisation</th>
      <th class="text-center" style="vertical-align: top;">Place</th>
      <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
     
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><b>10th</b></td>
      <td><?php echo $candidatedetails->metricpassingyear;?>
    </td>
    <td>  <?php echo $candidatedetails->metricschoolcollege;?>
  </td>

  <td> <?php echo $candidatedetails->metricboarduniversity;?>
</td>
<td><?php echo $candidatedetails->metricspecialisation;?>
</td>
<td><?php echo $candidatedetails->metricplace;?>
</td>

<td><?php echo $candidatedetails->metricpercentage;?> </td>


</tr>
<tr>
  <td><b>12th</b> </td>
  <td><?php echo $candidatedetails->hscpassingyear;?>
</td>

<td>  <?php echo $candidatedetails->hscschoolcollege;?></td>

<td> <?php  echo $candidatedetails->hscboarduniversity;?></td>

<td><?php echo $candidatedetails->hscspecialisation;?>
</td>


<td><?php echo $candidatedetails->hscplace;?>
</td>

<td><?php echo $candidatedetails->hscpercentage;?></td>

</tr>
<tr>
  <td><b>UG</b></td>

  <td><?php echo $candidatedetails->ugpassingyear; ?>
</td>

<td>
  <?php echo $candidatedetails->ugschoolcollege;?>

</td>

<td> <?php echo $candidatedetails->ugboarduniversity;?></td>


<td>
 <?php echo  $candidatedetails->ugspecialisation;  ?>
</td>
<td><?php echo $candidatedetails->ugplace;?></td>
<td><?php echo $candidatedetails->ugpercentage;?>
</td>

</tr>
<?php if (!empty($candidatedetails->pgpassingyear) && $candidatedetails->pgpassingyear !=0) {?>
  <tr>
    <td><b>PG</b></td>

    <td><?php echo $candidatedetails->pgpassingyear;?>
  </td>
  <td>  
    <?php echo $candidatedetails->pgschoolcollege;?>
  </td>
 <td> <?php echo $candidatedetails->pgboarduniversity;?>
</td>
<td>
  <?php
  echo $candidatedetails->pgspecialisation; 
  ?>
</td>
<td><?php echo $candidatedetails->pgplace;?>
</td>
<td><?php echo $candidatedetails->pgpercentage;?>
</td>
</tr>
<?php } ?>
<?php if (!empty($candidatedetails->otherpassingyear) && $candidatedetails->otherpassingyear !=0) {?>
  <tr>
    <td><b>If Others, Specify</b></td>

    <td>         </td>
    <td> </td>

    <td> </td>
    <td></td>

    <td>  </td>

    <td>  </td>
  
</tr>
<?php } ?>
</tbody>
</table>
</div>
</div>
<?php   if ($TrainingExpcount->TEcount !=0) {   ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm10" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">

       <div class="col-lg-12">
        <div class="col-lg-6 text-left"> Training  Exposure(if any)</div>
        <div class="col-lg-6 text-right ">
        </div>
      </div>
    </th>
  </tr> 
  <tr>

    <th class="text-center" style="vertical-align: top;">Nature of Training </th>
    <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
    <th class="text-center" style="vertical-align: top;">From Date</th>
    <th class="text-center" style="vertical-align: top;">TO Date</th>
  </tr> 
</thead>
<tbody id="bodytblForm10">
 <?php  $i=0;
 foreach ($trainingexposuredetals as $key => $val) { $i++; ?>
  <tr id="bodytblForm10">
    <td><?php echo $val->natureoftraining;?></td>
    <td> <?php echo $val->organizing_agency;?></td>
    <td><?php echo $this->model->changedatedbformate($val->fromdate); ?></td>
    <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
  </tr>
<?php } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>

<?php if ($languageproficiency->Lcount >0) { ?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm11" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;"> 

       <div class="col-lg-12">
        <div class="col-lg-6 text-left">Language Skill/Proficiency </div>
        <div class="col-lg-6 text-right ">
        </div>
      </div>
    </th>
  </tr> 
  <tr>
    <th class="text-center" style="vertical-align: top;">Language</th>
    <th class="text-center" style="vertical-align: top;">Speak</th>
    <th class="text-center" style="vertical-align: top;">Read</th>
    <th class="text-center" style="vertical-align: top;">Write </th>
  </tr> 
</thead>
<tbody id="bodytblForm11">
   <?php $i= 0;
  foreach ($languagedetals as $key => $val) {
    $i++; ?>
    <tr id="bodytblForm11">
      <td>  <?php
      echo $val->lang_name;?> </td>
    <td>
     <?php if($val->lang_speak=='H')
     { echo "High";
   }elseif ($val->lang_speak=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  ?> </td>
   <td><?php if($val->lang_read=='H')
     { echo "High";
   }elseif ($val->lang_read=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  

     ?></td>
 <td><?php if($val->lang_write=='H')
     { echo "High";
   }elseif ($val->lang_write=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  

     ?></td>
</tr>
<?php }   ?>
</tbody>
</table>
</div>
</div>
<?php  } ?>
<?php if ($otherinformationdetails->any_subject_of_interest !='') { ?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Any Subject(s) of Interest </th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_subject_of_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php }  ?> 

<?php if($otherinformationdetails->any_achievementa_awards !='') { ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Any Achievement /Awards(if any) </th>
    </tr> 

  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_achievementa_awards; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if ($WorkExperience->WEcount != 0) {?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm12" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="5" style="background-color: #3CB371; color: #fff;">
       <div class="col-lg-12">
        <div class="col-lg-6 text-left">Work Experience(if any) </div>
        <div class="col-lg-6 text-right ">
        </div>
      </div>
    </th>
  </tr> 
  <tr>
    <th>Organization Name</th>
    <th>Description of Assignment</th>
    <th>Duration</th>
    <th>Palce of Posting</th>
   
  </tr> 
  <tr>
    <th colspan="2"></th>
    <th colspan="1">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="2"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">
    <?php   
    $i=0;
    foreach ($workexperiencedetails as $key => $val) {   ?>
      <tr id="bodytblForm12"> 
        <td><?php echo $val->organizationname;  ?></td>
        <td><?php echo $val->descriptionofassignment;?></td>
        <td>
         <div class="col-lg-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <?php echo $this->model->changedatedbformate($val->fromdate);?>
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
           <?php echo $this->model->changedatedbformate($val->todate);?>
         </div> 
       </div>
     </td>
     <td><?php echo $val->palceofposting;?></td>
   
 </tr>
 <?php $i++; } ?>
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if ($otherinformationdetails->any_assignment_of_special_interest !='') {?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
   <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
    <thead>
     <tr>
      <th colspan="8" style="background-color: #3CB371; color: #fff;">Describe any Assignment(s) of special interest undertaken by you(if any) </th>
    </tr> 
  </thead>
  <tbody>
<tr>
<td><?php echo $otherinformationdetails->any_assignment_of_special_interest; ?></td>
</tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if($otherinformationdetails->experience_of_group_social_activities !='') { ?> 
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
      <thead>
       <tr>
        <th colspan="8" style="background-color: #3CB371; color: #fff;">Experience of Group and/or Social Activities </th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php echo $otherinformationdetails->experience_of_group_social_activities; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

<?php if(isset($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_details) && $otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_details !=''){ ?>
  <div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tbltrainingexposure" class="table table-bordered dt-responsive table-striped">
  <thead>
   <tr>
    <td colspan="8" style="background-color: #3CB371; color: #fff;"><b>Have you taken part in PRADAN's selection process before?</b>
      <?php  $check=''; $val_have_you_taken_part_in_pradan =''; 
      if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='on' ) {
        $check = "checked=checked";
       $val_have_you_taken_part_in_pradan = $otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_details;
     }  ?>
     <div class="form-check">
      <input type="checkbox" class="form-check-input" 
      id="selection_process_befor" name="selection_process_befor" <?php echo $check;?> >
      <label class="form-check-label" for="selection_process_befor"><b> If yes when and where </b></label>
    </div>
  </td>
</tr> 
</thead>
<tbody>
  <tr>
    <td><?php echo $val_have_you_taken_part_in_pradan; ?>
  </td>
</tr>
</tbody>
</table>
</div>
</div>
<?php } ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: #ffffff;">
 <a href="<?php echo site_url().'candidate/Candidate_registration/edit/'.$token; ?>" class="btn btn-primary" >Edit</a>
<a href="<?php echo site_url().'candidate/Candidate_registration/bdfformsubmit/'.$token;?>" class="btn btn-success" >Submit</a>
 <br><br>
</div>
</form>
</div>
<!-- End  Candidates Experience Info -->
</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
 $("#filladdress").load("on", function(){
  var  permanentstreet =  $("#permanentstreet").val();
  var  presentstreet =  $("#presentstreet").val();
   if (permanentstreet == presentstreet) { 
      $("#filladdress").prop('checked', true);
    }
    });
  });
   
</script>