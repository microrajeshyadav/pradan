<!--  <style type="text/css">
 input[type=text] {
  background: transparent;
  border: none;
  border-bottom: 1px solid #000000;
}
hr{

  border-top: 1px solid black;
}
</style>  -->
<section class="content" style="background-color: #FFFFFF;" >
  <br/>
  <?php $page='ID'; require_once(APPPATH.'views\candidate\components\Employee_particular_form\topbar.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">

     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action=""  enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $dataofidentity->id;?>">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">JOINING REPORT ON APPOINTMENT
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>

        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>        
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>            
          <?php } ?>
          <?php //print_r($candidatedetailwithaddress);?>          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
            <h6>Professional Assistance for Development Action (PRADAN) </h6>
            <h5>APPLICATION FORM FOR IDENTITY CARD</h5> </div>
            <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="form-group">
              <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
              PRADAN.</label>
              <br><b> <?php echo $officename_list->officename; ?>  </b>
            </div>
          </div>         
         <?php //foreach($dataofidentity as $rows3)
        // {
                //  print_r($rows3);
//print_r($dataofidentity);
         ?>
         <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="form-group">
            <label for="StateNameEnglish" class="field-wrapper required-field">Dear,&nbsp;</label><?php  echo $dataofidentity->dear ;?>
          </div>
        </div>
        <?php
        //foreach ($data_application as $key) {
                  //print_r($key);
        ?>
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">

         <div class="form-group">
           

           I, Ms./Mr.&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatefirstname)) echo $candidatedetailwithaddress->candidatefirstname; ?></b>&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatelastname)) echo $candidatedetailwithaddress->candidatelastname;?></b>
             &nbsp;<b><!-- <?php echo $data_application->candidatelastname;?></b>&nbsp;
           -->

           ,&nbsp;<b><?php if(!empty($data_application->desname)) echo $data_application->desname;?></b>&nbsp; &nbsp;
           <b><?php echo $data_application->emp_code;?></b>
           , based at &nbsp;<b><?php if(!empty($data_application->officename)) echo $data_application->officename;?></b>&nbsp;
           &nbsp;would like you to issue an 

           Identity card.My blood&nbsp;<b><?php echo $data_application->bloodgroup;?></b>&nbsp; <br><br>group is Enclosed please find a 
           copy of proof of blood group to process my request.
           <br>
       <!--     <div class="row">
            <div class="col-sm-6">
             <p  style="margin-right: 180px;">Passport Photo:&nbsp;
               <?php if(!empty($dataofidentity->photo1)) { ?>
                <a href="<?php echo site_url().'datafiles/staff_identity/'.$dataofidentity->photo1; ?>"><i class="fa fa-download"></i></a>
              <?php } ?>
            </p>
          </div>
        </div> -->
        <div class="row">
          <div class="col-sm-6">
            <p  style="margin-right: 180px;">Blood group (Lab Report):&nbsp;&nbsp;&nbsp;
             <?php if(!empty($dataofidentity->photo2)) { ?>
              <a href="<?php echo site_url().'datafiles/staff_identity/'.$dataofidentity->photo2;?>"><i class="fa fa-download"></i></a>
            <?php } ?>
          </p> 
        </div>
      </div>

    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <label for="Name">Signature<span style="color: red;" >*</span></label>
    <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
   <!-- <?php if ($signature->encrypted_signature !='') { ?>
     <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
   <?php }else{ ?>
     <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
   <?php } ?> -->
   <p class="text-left">Name:&nbsp;<b>&nbsp;<b><?php if(!empty($candidatedetailwithaddress->staff_name)) echo $candidatedetailwithaddress->staff_name;?></b>
     <!-- &nbsp;<b><?php echo $data_application->candidatelastname;?></b>&nbsp;</b></p> -->
     <p style="float: left">Date: &nbsp;&nbsp;&nbsp;&nbsp; 
      <b>
        <?php 
        echo $this->General_nomination_and_authorisation_form_model->changedate($dataofidentity->date);
        ?>
      </b> 
    </p>
  </div> 

  
  
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
  <?php if($this->loginData->RoleID==17 && $id->flag==2){?>

     <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h6 class="bg-light">Approved by team codinator </h6>
      </div> -->
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

          Status For Approve : <b><select name="status" required="required" class="form-control ">
           <option value="">Select The Status</option>
           <option value="4">Approved</option>
           <option value="3">Reject</option>
         </select></b>
       </div>
       <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

        Comments <span style="color: red;"> *</span>: <b><input type="text" name="reject" id="reject" placeholder="Comment" class="form-control" size="20" style="max-width:150px;"></b>
      </div>
    </div>
  <?php }?>
  <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag==1){?>

  <!--   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h6 class="bg-light">Approved by team codinator </h6>
    </div> -->
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

        Status For Approve : <b><select name="status" required="required" class="form-control ">
         <option value="">Select The Status</option>
         <option value="2">Approved</option>
         <option value="3">Reject</option>
       </select></b>
     </div>
     <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

      Comments <span style="color: red;"> *</span>: <b><input type="text" name="reject" id="reject" placeholder="Comment" class="form-control" size="20" style="max-width:150px;"></b>
    </div>
    <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

     Select The Personal: <b> <select name="p_status" class="form-control ">


       <option value="">Select The Personal</option>
       <?php foreach($personal as $value){?>
         <option value="<?php echo $value->staffid;?>"><?php echo $value->Username;?></option>
       <?php } ?>
     </select></b>
   </div>


 </div>
<?php }?>


</div>

<div class="panel-footer text-right" style="float:left;width: 100%;"> 
  <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag == 1) {?>
   <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
 <?php } ?>
 <?php if($this->loginData->RoleID==17 && $id->flag==2) {?>
  <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
<?php } ?>
<?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && ($id->flag == 4 || $id->flag==2)){ ?>
 <h6 class="bg-light text-left">Approved by team codinator </h6>
<?php }?>
<?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag==3){ ?>
 <h6 class="bg-light text-left">Rejected by team codinator </h6>
<?php }?>
<?php if($this->loginData->RoleID==17 && $id->flag == 2){ ?>
 <h6 class="bg-light text-left">Approved by team codinator</h6>
<?php }?>
<?php if($this->loginData->RoleID==17 && $id->flag == 4){ ?>
 <h6 class="bg-light text-left">Approved by Personnel</h6>
<?php }?>
<?php if($this->loginData->RoleID==17 && $id->flag == 3){ ?>
 <h6 class="bg-light text-left">Rejected by Personnel</h6>
<?php }?>
<a href="<?php echo site_url('candidate/Candidatedfullinfo/joininginduction_submit
')?>" class="btn btn-dark btn-sm m-d 10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
</div>



</div>

</div>
</form>
</div>

<!-- #END# Exportable Table -->

</section>


<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>  






