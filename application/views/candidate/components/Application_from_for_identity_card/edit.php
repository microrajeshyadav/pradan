 <style type="text/css">
   input[type=text] {
    background: transparent;
    border: none;
    border-bottom: 1px solid #000000;
  }
  hr{

    border-top: 1px solid black;
  }
</style> 
<section class="content" style="background-color: #FFFFFF;" >
  <br/>
  <?php $page='ID'; require_once(APPPATH.'views/Employee_particular_form/topbar.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">

     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action=""  enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">JOINING REPORT ON APPOINTMENT
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>        
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>            
          <?php } ?>
          <?php //print_r($candidatedetailwithaddress);?>          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
            <h6>Professional Assistance for Development Action (PRADAN) </h6>
            <h5>APPLICATION FORM FOR IDENTITY CARD</h5> </div>
            <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="form-group">
              <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
              PRADAN.</label>
              <br><b> <?php echo $officename_list->officename; ?>  </b>
            </div>
          </div>         
         <?php //foreach($dataofidentity as $rows3)
        // {
                //  print_r($rows3);
//print_r($dataofidentity);
         ?>
         <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="form-group">
            <label for="StateNameEnglish" class="field-wrapper required-field">Dear,&nbsp;</label><input type="text" name="dear" required="" minlength="2" maxlength="7" value="<?php  echo $dataofidentity->dear ;?>"> 
          </div>
        </div>
        <?php
        //foreach ($data_application as $key) {
                  //print_r($key);
        ?>
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">

         <div class="form-group">
           I, Ms./Mr.&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatefirstname)) echo $candidatedetailwithaddress->candidatefirstname; ?></b>&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatelastname)) echo $candidatedetailwithaddress->candidatelastname; ?></b>
           &nbsp;<b></b>&nbsp;


           ,&nbsp;<b><?php if(!empty($data_application->desname)) echo $data_application->desname;?></b>&nbsp; &nbsp;
           <b><?php if(!empty($data_application->emp_code)) echo $data_application->emp_code;?></b>
           , based at &nbsp;<b><?php if(!empty($data_application->officename)) echo $data_application->officename;?></b>&nbsp;
           &nbsp;would like you to issue an 

           Identity card.My blood&nbsp;<b><?php if(!empty($data_application->bloodgroup)) echo $data_application->bloodgroup;?></b>&nbsp; <br><br>group is Enclosed please find a 
           copy of proof of blood group  to process my request.
           <br>

         <!--   <p class="text-center" style="margin-right: 180px;">Passport Photo:
            
            <input type="file" style="margin-left: 430px;" data-toggle="tooltip" title="Only Image file having Max size" name="Photo1" id="Photo1" class="file" accept="image/*"    style="height:50px;width:150px;">
            <img src="<?php echo site_url().'datafiles/staff_identity/'.$dataofidentity->photo1;?>"  style="height:50px;width:150px;">
            <input type="hidden" name="photo111" value="<?php echo $dataofidentity->photo1;?>"/>
            
          </p> -->

          <p class="text-center" style="margin-right: 180px;">Blood group (Lab Report):
            
            <input type="file" style="margin-left: 430px;" data-toggle="tooltip" title="Only Image file having Max size" name="Photo2" id="Photo2" class="file" accept="image/*"    style="height:50px;width:150px;">
            <img src="<?php echo site_url().'datafiles/staff_identity/'.$dataofidentity->photo2;?>"  style="height:50px;width:150px;">
            <input type="hidden" name="photo222" value="<?php echo $dataofidentity->photo2;?>"/>
          </p>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
       <p class="text-left">Name:&nbsp;<b>&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatefirstname)) echo $candidatedetailwithaddress->candidatefirstname; ?></b>&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatelastname)) echo $candidatedetailwithaddress->candidatelastname; ?></b>
         &nbsp;<b></b>&nbsp;</b></p>
         <p style="float: left">Date: &nbsp;&nbsp;&nbsp;&nbsp; 
          <b>
            <input type="text" name="dates" class="datepicker" name="" value="<?php 
            $timestamp = strtotime($dataofidentity->date);
            echo date('d/m/Y', $timestamp);?>">
          </b> 
        </p>
      </div> 

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
        <div class="form-check">
          <label for="Name">Signature<span style="color: red;" >*</span></label>
           <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
         <!--  <?php if ($signature->encrypted_signature !='') { ?>
           <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
         <?php }else{ ?>
           <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
         <?php } ?> -->
       </div>
     </div>

     <?php //}}?> 
   </div>
   <div  class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
    <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
    <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
  </div> 

</form>
</div>
</div>
</div>

<!-- #END# Exportable Table -->

</section>


<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>  






