<section class="content" style="background-color: #FFFFFF;">
  <br/>
  <?php $page='for'; require_once(APPPATH.'views/candidate\components\Employee_particular_form/topbar.php');?>
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');
  if(!empty($tr_msg)){ ?>
  <div class="col-md-12">
    <div class="hpanel">
      <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('tr_msg');?>. </div>
      </div>
    </div>         
    <?php } else if(!empty($er_msg)){?>      
    <div class="col-md-12">
      <div class="hpanel">
        <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('er_msg');?>. </div>
      </div>
    </div>           
      <?php } ?>

      <div class="container-fluid" style="font-family: 'Oxygen' !important;">
        <div class="row text-center" style="padding: 14px;">      
          <div class="panel thumbnail shadow-depth-2 listcontainer">
            <div class="panel-heading">
              <div class="row">
                <h4 class="col-md-12 panel-title pull-left">JOINING REPORT ON APPOINTMENT 
                </h4>
              </div>
              <hr class="colorgraph"><br>
            </div>
            <div class="panel-body">          
              <div class="row">
                <form method="POST" action="" enctype="multipart/form-data">

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
                    <h6>Professional Assistance for Development Action (PRADAN) </h6>
                    <h5>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME</h5> 
                    <h5>Part II - Certificate by Examining Doctor</h5>
                  </div>
                  
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">                    
                      <b>1</b>. I hereby certify that I have physically examined Ms./Mr.&nbsp;<b><?php echo $rowsm->candidatefirstname; ?></b>&nbsp;<b><?php echo $candidate_list->candidatelastname; ?> </b>&nbsp;a candidate for employment with PRADAN, as
                      &nbsp;<b><?php echo $candidate_list->desname; ?></b>&nbsp;
                      (designation).
                      I have
                      taken into <br><br>account
                      her/his statements above and her/his Electrocardiogram, Chest X-ray, Blood
                      Examination (Hb%, TLC, DC, ESR), Stool Examination, Urine Analysis,<br><br> 
                      and report about her/his
                      vision (reports enclosed). I cannot identify any disease (communicable or otherwise), or
                      constitutional weakness except
                      <label   style="border-radius: 0px; border: none; border-bottom: 1px solid black;" ><?php echo $fetch_datas->weakness_except; ?></label>
                      <br><br>
                      <b>2</b>. I do not/do consider this as a disqualification for employment in PRADAN.
                      <br><br>
                      Signature of Examining Doctor with seal:<br>
                      <img src="<?php echo site_url().'datafiles/signature/'.$fetch_datas->doctor_seal_signature;?>"  style="height:50px;width:150px;">
                      <input type="hidden" name="docsig" value="<?php echo $fetch_datas->doctor_seal_signature;?>"/>
                    </div>   
                    <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 " style="text-align: justify;">
                      Location:&nbsp;<label  style="border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo  $candidatedetailwithaddress->officename;; ?></label>
                      <br>
                      Date:&nbsp;<label  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" ><?php 
                      $timestamp = strtotime($fetch_datas->certificatedate);
                      echo date('d/m/Y', $timestamp);?></label>
                    </div>


                    <div class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                      <a href="<?php echo site_url("Medical_certificate");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                    </div>
                    </form>
                  </div>
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <script type="text/javascript">
     $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();
       $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1980:2030',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });
     });
   </script>  






























