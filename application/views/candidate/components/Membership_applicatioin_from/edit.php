<section class="content" style="background-color: #FFFFFF;">
  <br/>
  <?php $page='Enhanced'; require_once(APPPATH.'views/candidate\components\Employee_particular_form/topbar.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">
     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6>Professional Assistance for Development Action (PRADAN) </h6>
          <h6>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME</h6>
          <h6>DECLARATION ABOUT OPTION FOR ENHANCED INSURANCE COVER FOR HOSPITALISATION</h6>
        </div>
      </div>

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>          
        <?php } else if(!empty($er_msg)){?>        
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>            
          <?php } ?>
          
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
            <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
            PRADAN.</label>
            <br>
            <?php
           // foreach($officename_list as $tm){
            echo $officename_list->officename;
          // }
            ?>           
            <?php echo form_error("executive_director");?>         
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">           
            Dear Sir / Madam,          
          </div>


          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;margin-top: 50px;">         
            I would like to get covered under the Enhanced Insurance Cover for Hospitalisation w.e.f.<input type="text" name="txtname" id="txtname"   required="required" class="inputborderbelow datepicker">(date). I would like to opt for:
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style = "margin-top: 30px; margin-bottom: 30px;">
            <h5>Coverage </h5>
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p style="float:left;">(i) Rs 4,00,000/-,
              <div class="check checkbox-inline" >
                <input type="checkbox" class="check-input" value="1" name="coverage1" id="coverage1">

                <label class="check-label" for="coverage1"></label>
              </div>
              but with addition(s) in the number of members of the family
            </p>
          </div>

          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <p style="float:left;">(ii) Raised from  Rs 4 to Rs.7

             <!--  <div class="form-check checkbox-inline" > -->
              <input type="checkbox" class="check-input" value="1" name="coverage21" id="coverage21">

              <label class="check-label" for="coverage21">Rs.10</label>
              <input type="checkbox" class="check-input" value="1" name="coverage22" id="coverage22">
              <label class="check-label" for="coverage21"></label>

              lakh without any change in the number of members of the family
            </p>
          </div>

          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <p style="float:left;">(ii) Raised from  Rs 4 to Rs.7

             <!--  <div class="form-check checkbox-inline" > -->
              <input type="checkbox" class="check-input" value="1" name="coverage31" id="coverage31">

              <label class="check-label" for="coverage31">Rs.10</label>
              <input type="checkbox" class="check-input" value="1" name="coverage32" id="coverage32">

              <label class="check-label" for="coverage32"></label>

              lakh with addition(s) in the number of members of the family
            </p>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <table id="tblForm09" class="table" >
            <thead>
             <tr class="bg-light">
              <th colspan="8"> Family Details </th>
            </tr> 
            <tr>
             <th>Sr No.</th>
             <th>Name</th>
             <th>Date of Birth</th>
             <th>Age (in years)</th>
             <th>Relationship</th>
             <th>Whether Living With Me(Yes/No)</th>
             <th>Remarks</th>

           </tr>
         </thead>
         <tbody>
          <?php
          $i=0;
          foreach($members as $rowd){ 

           ?>
           <tr>
            <td class="text"><?php echo $i+1; ?></td>
            <td class="text"><?php echo $rowd->Familymembername; ?></td>
            <td class="text"><?php echo $this->gmodel->changedatedbformate($rowd->familydob); ?></td>
            <td class="text">
              <?php
              if(!empty($rowd->familydob))
              {
               $cur_year = date('Y');
               $year=$rowd->familydob;
                       //echo '<b>'.$year.'</b>';
               $arr=explode('-', $year);
               $currentyear=$arr[0];
               $var=$cur_year-$currentyear;
               echo '<b>'.$var.'</b>';
             }
             ?> </td>
             <td class="text"><?php echo $rowd->relationname;  ?></td>
             <td class="text">
              <?php echo $rowd->weatherliving;  ?>

            </td>
            <td class="text"><?php echo $rowd->remarks; ?></td>
            <td class="text"></td>
          </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
    </div>
    
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
      I agree to pay the additional contribution as prescribed under the Scheme for which I hereby authorize PRADAN to deduct the required amount from my salary.
      <br><br>
      Yours faithfully,        
    </div>

    <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 " style="text-align: justify; line-height: 2">
      <input type="text" name="placecandidate" maxlength="50" minlength="2" class="form-control inputborderbelow" readonly="readonly" placeholder="Enter Place" value="<?php if(!empty($candidatedetailwithaddress->officename)) echo $candidatedetailwithaddress->officename;?>" />
      <input type="text" name="datecandidate" class="datepicker inputborderbelow form-control" required placeholder="Enter Date" />

    </div>

    <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right pull-right" style="text-align: justify; line-height: 2">
     <div class="input-group">                  
      
     </div>
     Name:<b><?php if(!empty($candidatedetailwithaddress->staff_name)) echo $candidatedetailwithaddress->staff_name;?></b>&nbsp;<b></b> <br/>
     Designation:<b><?php if(!empty($candidatedetailwithaddress->name)) echo $candidatedetailwithaddress->name;?></b> <br/>
     Employee Code:<b><?php if(!empty($candidatedetailwithaddress->emp_code)) echo $candidatedetailwithaddress->emp_code;?></b><br>
     Signature: 
      <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
      <!-- <?php if ($signature->encrypted_signature !='') { ?>
       <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
     <?php }else{ ?>
       <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
     <?php } ?> -->
   </div>

   <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
     cc: - Finance-Personnel-MIS Unit<br>
     &emsp;&emsp;- Personal Dossier (Location)
   </div>
 </div>
 <div class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
   <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
   <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
 </div>
 
</form>
</div>
</div>
</div>
</section>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>
