
<section class="content" style="background-color: #FFFFFF;">
  <br/>
  <?php $page='Enhanced'; require_once(APPPATH.'views\candidate\components\Employee_particular_form\topbar.php');?>
  <div class="container-fluid" style="font-family: 'Oxygen' !important;">
    <div class="row text-center" style="padding: 14px;">
     <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="panel-heading"> <div class="row">
         <h4 class="col-md-12 panel-title pull-left">EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME
         </h4>
         <input type="hidden" id="id" name="id" value="<?php echo $fetch->id;?>">

       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6>Professional Assistance for Development Action (PRADAN) </h6>
          <h6>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME</h6>
          <h6>DECLARATION ABOUT OPTION FOR ENHANCED INSURANCE COVER FOR HOSPITALISATION</h6>
        </div>
      </div>

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>

        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>          
        <?php } else if(!empty($er_msg)){?>        
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>            
          <?php } ?>
          
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
            <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
            PRADAN.</label>
            <br>
            <?php
           // foreach($officename_list as $tm){
            echo $officename_list->officename;
          // }
            ?>           
            <?php echo form_error("executive_director");?>         
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">           
            Dear Sir / Madam,          
          </div>


          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;margin-top: 50px;">         
            I would like to get covered under the Enhanced Insurance Cover for Hospitalisation w.e.f.<label class="inputborderbelow datepicker"><?php 
            echo $this->General_nomination_and_authorisation_form_model->changedate($fetch->wefdate);
            ?></label>(date). I would like to opt for:
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style = "margin-top: 30px; margin-bottom: 30px;">
            <h5>Coverage </h5>
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p style="float:left;">(i) Rs 4,00,000/-,
              <div class="check checkbox-inline" >
                <?php
                if($fetch->coverage1==1)
                {
                  echo ' <input type="checkbox" checked class="check-input" value="1" name="coverage1" id="coverage1">

                  <label class="check-label" for="coverage1"></label>';
                }
                else
                {
                 echo ' <input type="checkbox" checked class="check-input" value="1" name="coverage1" id="coverage1">

                 <label class="check-label" for="coverage1"></label>'; 
               }
               ?>

               <label class="check-label" for="coverage1"></label>
             </div>
             but with addition(s) in the number of members of the family
           </p>
         </div>

         <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <p style="float:left;">(ii) Raised from  Rs 4 to Rs.7

             <!--  <div class="form-check checkbox-inline" > -->
               <?php 
               if($fetch->coverage21==1)
               {
                echo ' <input type="checkbox" checked class="check-input" value="1" name="coverage21" id="coverage21">

                <label class="check-label" for="coverage21">Rs.10</label>';
              }
              else
              {
               echo ' <input type="checkbox" class="check-input" value="1" name="coverage21" id="coverage21">

               <label class="check-label" for="coverage21">Rs.10</label>'; 
             }
             ?>

             <label class="check-label" for="coverage21">Rs.10</label>
             <?php 
             if($fetch->coverage22==1)
             {
              echo '  <input type="checkbox" checked class="check-input" value="1" name="coverage22" id="coverage22">
              <label class="check-label" for="coverage22"></label>';
            }
            else
            {
             echo ' <input type="checkbox" class="check-input" value="1" name="coverage22" id="coverage22">
             <label class="check-label" for="coverage22"></label>'; 
           }
           ?>


           <label class="check-label" for="coverage21"></label>

           lakh without any change in the number of members of the family
         </p>
       </div>

       <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <p style="float:left;">(ii) Raised from  Rs 4 to Rs.7

           <!--  <div class="form-check checkbox-inline" > -->
             <?php 
             if($fetch->coverage31==1)
             {
              echo ' <input type="checkbox" checked class="check-input" value="1" name="coverage31" id="coverage31">

              <label class="check-label" for="coverage31">Rs.10</label> ';
            }
            else
            {
             echo ' <<input type="checkbox" class="check-input" value="1" name="coverage31" id="coverage31">

             <label class="check-label" for="coverage31">Rs.10</label>'; 
           }
           ?>

           <label class="check-label" for="coverage31">Rs.10</label>
           <?php 
           if($fetch->coverage32==1)
           {
            echo '  <input type="checkbox" checked class="check-input" value="1" name="coverage32" id="coverage32">

            <label class="check-label" for="coverage32"></label> ';
          }
          else
          {
           echo ' <input type="checkbox" class="check-input" value="1" name="coverage32" id="coverage32">

           <label class="check-label" for="coverage32"></label>'; 
         }
         ?>

         <label class="check-label" for="coverage32"></label>

         lakh with addition(s) in the number of members of the family
       </p>
     </div>
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <table id="tblForm09" class="table" >
        <thead>
         <tr class="bg-light">
          <th colspan="8"> Family Details </th>
        </tr> 
        <tr>
         <th>Sr No.</th>
         <th>Name</th>
         <th>Date of Birth</th>
         <th>Age (in years)</th>
         <th>Relationship</th>
         <th>Whether Living With Me(Yes/No)</th>
         <th>Remarks</th>

       </tr>
     </thead>
     <tbody>
      <?php
      $i=0;
      foreach($members as $rowd){ 
       ?>
       <tr>
        <td class="text"><?php echo $i+1; ?></td>
        <td class="text"><?php echo $rowd->Familymembername; ?></td>
        <td class="text"><?php echo $rowd->familydob; ?></td>
        <td class="text">
          <?php
          if(!empty($rowd->familydob))
          {
           $cur_year = date('Y');
           $year=$rowd->familydob;
                       //echo '<b>'.$year.'</b>';
           $arr=explode('-', $year);
           $currentyear=$arr[0];
           $var=$cur_year-$currentyear;
           echo '<b>'.$var.'</b>';
         }
         ?> </td>
         <td class="text"><?php echo $rowd->relationname;  ?></td>
         <td class="text">
          <?php echo $rowd->weatherliving;  ?>

        </td>
        <td class="text"><?php echo $rowd->remarks;?></td>
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>

<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
  I agree to pay the additional contribution as prescribed under the Scheme for which I hereby authorize PRADAN to deduct the required amount from my salary.
  <br><br>
  Yours faithfully,        
</div>

<div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 " style="text-align: justify; line-height: 2">
  Place:<label class=" inputborderbelow"><?php if(!empty($candidatedetailwithaddress->officename)) echo $candidatedetailwithaddress->officename;?></label><br>
  Date:<label  class=" inputborderbelow " ><?php 
  $timestamp = strtotime($fetch->date);
  echo date('d/m/Y', $timestamp);?></label>

</div>

<div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right pull-right" style="text-align: justify; line-height: 2">
  
  Name:<b><?php if(!empty($candidatedetailwithaddress->candidatefirstname)) echo $candidatedetailwithaddress->candidatefirstname .' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname  ;?></b>&nbsp;<b></b> <br/>
  Designation:<b><?php if(!empty($getstaffempcodedes->desname)) echo $getstaffempcodedes->desname;?></b> <br/>
  Employee Code:<b><?php if(!empty($getstaffempcodedes->emp_code)) echo $getstaffempcodedes->emp_code;?></b><br>
  Signature:
   <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
   <!--  <?php if ($signature->encrypted_signature !='') { ?>
   <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
 <?php }else{ ?>
   <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
 <?php } ?> -->
</div>

<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
 cc: - Finance-Personnel-MIS Unit<br>
 &emsp;&emsp;- Personal Dossier (Location)
</div>
<?php if($this->loginData->RoleID==17 && $id->flag==2){ ?>
  <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: justify;margin-left:30px;">
    <h5 class="bg-light">Approved by team codinator </h5>
  </div> -->
  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      Status For Approve : <b><select name="status" class="form-control ">
       <option value="">Select The Status</option>
       <option value="4">Approved</option>
       <option value="3">Reject</option>
     </select></b>

   </div>
   <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    Comments <span style="color: red;"> *</span>: <b><input type="text" name="reject" id="reject" placeholder="Reject Comment" class="form-control" size="20" style="max-width:150px;"></b>
  </div>
</div>
<?php } if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag==1) {?>
<!-- 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: justify;margin-left:30px;">
    <h5 class="bg-light">Approved by team codinator </h5>
  </div> -->
  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

      Status For Approve : <b><select name="status" class="form-control " equired ="required">
       <option value="">Select The Status</option>
       <option value="2">Approved</option>
       <option value="3">Reject</option>
     </select></b>

   </div>
   <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

    Comments <span style="color: red;"> *</span>: <b><input type="text" name="reject" id="reject" placeholder="Comment" required ="required" class="form-control" size="20" style="max-width:150px;"></b>
  </div>
  <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

   Select The Personal: <b> <select name="p_status" class="form-control " equired ="required">


     <option value="">Select The Personal</option>
     <?php foreach($personal as $value){?>
       <option value="<?php echo $value->staffid;?>"><?php echo $value->Username;?></option>
     <?php } ?>
   </select></b>
 </div>
</div>
<?php }?>
</div>
<div class="panel-footer text-right" style="float:left;width: 100%;"> 
  <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag == 1) {?>
   <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
 <?php } ?>
 <?php if($this->loginData->RoleID==17 && $id->flag==2) {?>
  <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
<?php } ?>
<?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && ($id->flag == 4 || $id->flag==2)){ ?>
 <h6 class="bg-light text-left">Approved by team codinator/Integerator </h6>
<?php }?>
<?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag==3){ ?>
 <h6 class="bg-light text-left">Rejected by team codinator/Integrator </h6>
<?php }?>
<?php if($this->loginData->RoleID==17 && $id->flag == 2){ ?>
 <h6 class="bg-light text-left">Approved by team codinator/Integrator </h6>
<?php }?>
<?php if($this->loginData->RoleID==17 && $id->flag == 4){ ?>
 <h6 class="bg-light text-left">Approved by Personnel</h6>
<?php }?>
<?php if($this->loginData->RoleID==17 && $id->flag == 3){ ?>
 <h6 class="bg-light text-left">Rejected by Personnel</h6>
<?php }?>
<a href="<?php echo site_url('candidate/Candidatedfullinfo/joininginduction_submit
')?>" class="btn btn-dark btn-sm md 10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
</div>

</form>
</div>
</div>
</div>


</section>
<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>
