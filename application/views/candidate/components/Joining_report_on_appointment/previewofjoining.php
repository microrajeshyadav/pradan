
<style type="text/css">
  /* input[type=text] {
    background: transparent;
    border: none;
    border-bottom: 1px solid #000000;
    }*/
    textarea {
      resize: none;
    }
    hr{
      border-top: 1px solid black;
    }
  </style>
  <section class="content" style="background-color: #FFFFFF;">
    <br/>
    <?php $page='Joining'; require_once(APPPATH.'views\candidate\components\Employee_particular_form/topbar.php');?>
    <?php 

    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>   
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>       
      <?php } else if(!empty($er_msg)){?>    
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>          
        <?php } ?>
        <div class="container-fluid" style="font-family: 'Oxygen' !important;">
          <div class="row text-center" style="padding: 14px;">      
            <div class="panel thumbnail shadow-depth-2 listcontainer">
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-12 panel-title pull-left">JOINING REPORT ON APPOINTMENT 
                  </h4>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">          
                <div class="row">
                 <form class="col-lg-12 col-md-12" method="POST" action="" enctype="multipart/form-data">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
                    <h6>Professional Assistance for Development Action (PRADAN) </h6>
                    <input type="hidden" name="id" id="id" value="<?php echo $dataofidentity->id;?>">
                    <h5>JOINING REPORT ON APPOINTMENT</h5> 
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="ExecutiveDirector" class="field-wrapper required-field">To:<br>The Executive Director,<br>
                      PRADAN.</label>
                      <br>

                    </div>
                  </div> 

                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right">
                   <label for="Name">Address:&nbsp;</label>&nbsp;
                   &nbsp;
                   <?php  echo $dataofidentity->address;?>
                   
                   

                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">       
                  Dear Sir,               
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">
                  <div class="form-group">
                    Subject: <b>Joining Report</b>
                  </div>
                </div>
                

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">

                  Please refer to your offer of appointment no&nbsp;
                  <b><?php if(!empty($fetchresult->offerno)) echo $fetchresult->offerno; ?></b>&nbsp;dated&nbsp;
                  <b><?php if(!empty($fetchresult->doj)) echo $this->gmodel->changedatedbformate($fetchresult->doj); ?></b>&nbsp; offering me appointment as&nbsp;
                  <b><?php if(!empty($fetchresult->desname)) echo $fetchresult->desname; ?></b>&nbsp;at&nbsp;
                  <b><?php if(!empty($fetchresult->officename)) echo $fetchresult->officename; ?></b>&nbsp;
                  I hereby report for duty
                  <br>
                  <br> 
                  in the forenoon of today, the 
                  &nbsp; <input type="text" class="inputborderbelow datepicker" name="dutydate" id="dutydate" value="<?php echo $this->gmodel->changedatedbformate($dataofidentity->duty_date);
                  ?>" required="required" />
                  (date/month/year). I shall inform you of any change in my address, given above, when it occurs.
                  <br>
                  <br>
                  Yours faithfully,
                  <br>
                  <br>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <p class="text-left">
                    <p class="text-left">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="inputborderbelow">
                      <?php if(!empty($candidatedetailwithaddress->staff_name)) echo $candidatedetailwithaddress->staff_name; ?></span></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <p class="text-right">Place:&nbsp;
                      <b> <?php echo $dataofidentity->declaration_place; ?></b>
                    </p>
                    <p class="text-right" >Date:&nbsp;<b><?php 
                    



                    echo $this->gmodel->changedatedbformate($dataofidentity->declaration_date);?>

                  </b></p>

                </div>                                   
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                  <div class="form-check">
                    <label for="Name">Signature<span style="color: red;" >*</span></label>
                     <?php   
                        $image_path='';
                        /*echo "<pre>";
                        print_r($candidatedetails);
                        exit();*/
                      if(!empty($signature->encrypted_signature)) {
                           $image_path='datafiles/'.$signature->encrypted_signature;
                           if (file_exists($image_path)) {

                        ?>
                      <img src="<?php echo site_url().'datafiles/'.$signature->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                        <?php }
                        else
                        {
                          ?>
             
                            
                          <?php
                        } }
                        ?>
                   <!--  <?php if ($signature->encrypted_signature !='') { ?>
                     <img class= "rounded-circle" src="<?php echo site_url().'datafiles/signature/'.$signature->encrypted_signature;?>" alt="Jane" width="100" height="50px">
                   <?php }else{ ?>
                     <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px"  >
                   <?php } ?> -->
                 </div>
               </div>
               
               
             </div>
             
             <hr> 
             <?php if(empty($this->loginData->RoleID) && !empty($id->flag) && $id->flag==4){
              ?>
              <div class="row">
               <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
                 <p class="text-center"><b>Countersigned:</b> (by Employee Responsible for Induction)</p>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
                 

                  
                 <p class="text-left">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="inputborderbelow"> <?php if(!empty($candidatedetailwithaddress->candidatefirstname)) echo $candidatedetailwithaddress->candidatefirstname; ?></b>&nbsp;<b><?php if(!empty($candidatedetailwithaddress->candidatelastname)) echo $candidatedetailwithaddress->candidatelastname; ?></b></p>
               </div>

               <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right" style="margin-bottom: 50px;">
                <input type="text" name="Countersignedplace" required="" maxlength="20" minlength="2" class="form-control inputborderbelow" placeholder="Enter Place" value="<?php if(!empty($dataaa->declaration_place)) echo $dataaa->declaration_place; ?>">  
                <input type="text" class="form-control inputborderbelow datepicker" name="Countersigneddate" required=""  placeholder="Select Date" value="<?php     echo $this->General_nomination_and_authorisation_form_model->changedate($dataofidentity->declaration_date);?>">
              </div>
              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <p style="float: left;margin-left: 5px;">cc: - Supervisor</p>
             </div>
             <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <p style="float: left;"> &emsp;&emsp;- Finance-Personnel-MIS Unit</p>
             </div>
             <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <p style="float: left;"> &emsp;&emsp;- Personal Dossier (Location)</p>
             </div>
           </div>
         </div> 
       <?php }?>

       <?php if($this->loginData->RoleID == 17 || ($this->loginData->RoleID==3  &&  $id->flag==4))
       {?>
         <div  class="row">

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;"> <b>(For use in the Finance-Personnel-MIS Unit)</b></div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

            Ms./Mr. <span class="inputborderbelow"><b><?php if(!empty($candidatedetailwithaddress->staff_name)) echo $candidatedetailwithaddress->staff_name; ?></b></span> has joined at    


            <input type="text" name="newoffice"  class="inputborderbelow" value="<?php if(!empty($candidatedetailwithaddress->officename)) echo $candidatedetailwithaddress->officename;?>">                
           <!--  <select name="newoffice" required="" maxlength="30" minlength="2"  class="inputborderbelow" >
              <option value="">Select The Location</option>
              <?php foreach ($location as $val){

                ?>
                <option value="<?php echo $val->officeid;?>">
                  <?php echo $val->officename;?>
                </option>
              <?php } ?>
            </select>   -->               

            noted in Probation register.
          </div>

          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" readonly="" name="NotedinProbationregister" id="NotedinProbationregister" >
              <label class="form-check-label" for="NotedinProbationregister">(Please<span class="glyphicon glyphicon-ok"></span>)</label>
            </div>
          </div>
          <br><br>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
              <input  class= "form-control inputborderbelow" type="text" name="FinancePersonnelMISUnit" value="<?php echo $this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName; ?>">
            </div>
          </div>

          (Finance-Personnel-MIS Unit) 
        </div>
      <?php }?>


      <!-- <hr> -->

      <?php 
     
      if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag==1)
      {
        ?>
              <!-- <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <h6 class="bg-light">Approved by team codinator </h6>
              </div> -->
              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                  Status For Approve : <b><select name="status" class="form-control ">
                   <option value="">Select The Status</option>
                   <option value="2">Approved</option>
                   <option value="3">Reject</option>
                 </select></b>
               </div>
               <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                Comments <span style="color: red;"> *</span> : <b><input type="text" name="reject" id="reject" placeholder="Comment" required="required" class="form-control" size="20" style="max-width:150px;"></b>
              </div>
              <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

               Select The Personal: <b> <select name="p_status" class="form-control ">


                 <option value="">Select The Personal</option>
                 <?php foreach($personal as $value){?>
                   <option value="<?php echo $value->staffid;?>"><?php echo $value->Username;?></option>
                 <?php } ?>
               </select></b>
             </div>



           </div>
         <?php }
         else  if($this->loginData->RoleID==17 && $id->flag==2)
         {
          ?>
         <!--  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <h6 class="bg-light">Approved by Personnel </h6>
          </div> -->
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

              Status For Approve : <b><select name="status" required="required" class="form-control ">
               <option value="">Select The Status</option>
               <option value="4">Approved</option>
               <option value="3">Reject</option>
             </select></b>
           </div>
           <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

             Comments <span style="color: red;"> *</span>: <b><input type="text" name="reject" id="reject" required="required" placeholder="Comment" class="form-control" size="20" style="max-width:150px;"></b>
           </div>
         </div>
       <?php } ?>

       <div class="panel-footer text-right" style="float:left;width: 100%;"> 
        <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag == 1) {?>
         <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
       <?php } ?>
       <?php if($this->loginData->RoleID==17 && $id->flag==2) {?>
        <button  type="submit" name="status_approved"  value="2" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save status">Save  status</button>
      <?php } ?>
      <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && ($id->flag == 4 || $id->flag==2)){ ?>
       <h6 class="bg-light text-left">Approved by team codinator/Integrator </h6>
     <?php }?>
     <?php if(($this->loginData->RoleID==2 || $this->loginData->RoleID==21) && $id->flag==3){ ?>
       <h6 class="bg-light text-left">Rejected by team codinator/Integrator </h6>
     <?php }?>
     <?php if($this->loginData->RoleID==17 && $id->flag == 2){ ?>
       <h6 class="bg-light text-left">Approved by team codinator/Integrator</h6>
     <?php }?>
     <?php if($this->loginData->RoleID==17 && $id->flag == 4){ ?>
       <h6 class="bg-light text-left">Approved by Personnel</h6>
     <?php }?>
     <?php if($this->loginData->RoleID==17 && $id->flag == 3){ ?>
       <h6 class="bg-light text-left">Rejected by Personnel</h6>
     <?php }?>
     <a href="<?php echo site_url('staff_dashboard')?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
   </div>
 </form>


</div>
</div>
</div>
</div>
</div>
</section>


<script>
  $(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
   $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });

 });
</script>
