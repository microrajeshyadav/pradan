 <style>
 textarea { resize:none; } 
</style>
  <!-- Exportable Table -->

      <div class="container-fluid" style="font-family: 'Oxygen' !important;">
        <div class="row text-center" style="padding: 14px;">

         <div class="col-md-12 panel thumbnail shadow-depth-2 listcontainer">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class=" alert-success alert-dismissable alert"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          
            <div class="panel-heading"> <div class="row">


             <h4 class="col-md-12 panel-title pull-left">Professional Assistance for Development Action (PRADAN)  <br><br>GENERAL NOMINATION AND AUTHORISATION FORM
             </h4>
           </div>
           <hr class="colorgraph"><br>
         </div>

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
           <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
             <div class="form-group">
              <div class="form-line">
                <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
                PRADAN.</label>
                <p> <?php echo $genenominformdetail->executive_director_place; ?></p>
              </div>
              <?php echo form_error("executive_director");?>
            </div>
          </div>
          <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          </div>
          <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          </div>

          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="form-group">
            <label for="StateNameEnglish" class="field-wrapper required-field">Sir, </label>
          </div>
        </div>

        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="form-group">
          <label class="field-wrapper required-field">Subject: General Nomination and Authorization. </label>
        </div>
      </div>
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
       <div class="form-group">
        I,   <span style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;"><?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?> </span> (name in full) whose particulars are given below, hereby nominate the following person(s) to receive, in the event of my death, all monies payable to me by Professional Assistance for Development Action (PRADAN) or its successors or assignees, other than the monies on those accounts in respect of which specific nominations exist or are required to exist under the rules governing the relevant matters, and want that the monies first aforesaid shall be paid in proportion indicated against the name(s) of the nominee(s).
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <table id="tblForm09" class="table table-bordered table-striped" >
      <thead>
       <tr>
        <th colspan="5" style="background-color: #eee;"> Nominee Details </th>
      </tr> 
      <tr>
       <th style="width:50px;">Sr No.</th>
       <th>Name of the nominee in full</th>
       <th>Nominee's Relationship with the Apprentice</th>
       <th>Age of nominee</th>
       <th>Full address of the nominee</th>
     </tr>
   </thead>
   <tbody>
    <?php $i=0; foreach ($nomineedetail as $key => $val) {?>
     <tr>
       <td style="width:50px;"><?php echo $i+1;?></td>
       <td><?php echo $val->nominee_name; ?></td>
       <td><?php echo $val->relationname;?></td>
       <td><?php echo $val->nominee_age; ?></td>
       <td><?php echo trim($val->nominee_address); ?> </td>
     </tr>
     <?php $i++; }    ?>
   </tbody>
 </table>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <label class="field-wrapper required-field">  This nomination and authorisation shall remain valid unless substituted by a fresh nomination and authorisation.</label>
  </div>
</div>

<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-right"></div>
  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> <label for="ExecutiveDirector" class="field-wrapper required-field">Yours sincerely</label></div>
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ><label for="ExecutiveDirector" class="field-wrapper required-field">Place :</label></div>
   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" > <?php echo $genenominformdetail->da_place;?> </div>
 </div>
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
  <!--   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> </div> -->
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right"><label for="ExecutiveDirector" class="field-wrapper required-field" >Name :</label> </div>
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right" style="margin-left:70px;"> <?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>  </div>
</div>
</div>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="ExecutiveDirector" class="field-wrapper required-field">Date :</label> </div>
   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <?php echo /*$this->model->changedate($genenominformdetail->da_date); */ date("d/m/y");?> </div>
 </div>
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
  <!--  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> </div> -->
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right" style="width:250px;"><label for="ExecutiveDirector" class="field-wrapper required-field"> Father's/Husband's Name : </label></div>
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="width:250px;"> <?php echo $candidatedetailwithaddress->fatherfirstname.' '.$candidatedetailwithaddress->fathermiddlename.' '.$candidatedetailwithaddress->fatherlastname;?> </div>
</div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
 <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label for="ExecutiveDirector" class="field-wrapper required-field">Permanent Address : </label></div>  
  <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
   <?php echo $candidatedetailwithaddress->permanentstreet.' '.$candidatedetailwithaddress->permanentcity.' '.$candidatedetailwithaddress->permanentdistrict.' '.$candidatedetailwithaddress->name.', '.$candidatedetailwithaddress->permanentpincode; ?> 
 </div>        
</div>

</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="panel-footer text-right" style="margin: 15px;">
    <!-- <a  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="close" href=" <?php echo site_url('candidate/General_nomination_and_authorisation_form/view') ?>" >Close</a>  -->
 </div>
</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->


<script type="text/javascript">

  $(document).ready(function(){
    decimalData();
    $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

  });

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit").prop('disabled', true);

  });

</script>  

