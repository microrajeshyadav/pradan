  <div class="container">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

            <?php //print_r($candidatedetailwithaddress);?>
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="body">
                    <div class="row clearfix doctoradvice">
                     <h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>GENERAL NOMINATION AND AUTHORISATION FORM </h4>

                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
                     <form method="POST" action="">
                      <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                       <div class="form-group">
                        <div class="form-line">
                          <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
                          PRADAN.</label>
                          <input type="text" class="form-control" name="executive_director_place"  required="required" id="executive_director_place" placeholder="Please Enter Place " value="<?php echo set_value('executive_director'); ?>"  >
                        </div>
                        <?php echo form_error("executive_director");?>
                      </div>
                    </div>
                    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    </div>
                    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  </div>

                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="form-group">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Sir, </label>
                  </div>
                </div>

                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="form-group">
                  <label class="field-wrapper required-field">Subject: General Nomination and Authorization. </label>
                </div>
              </div>
              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
               <div class="form-group">
                I,  <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">   (name in full) whose particulars are given below, hereby nominate the following person(s) to receive, in the event of my death, all monies payable to me by Professional Assistance for Development Action (PRADAN) or its successors or assignees, other than the monies on those accounts in respect of which specific nominations exist or are required to exist under the rules governing the relevant matters, and want that the monies first aforesaid shall be paid in proportion indicated against the name(s) of the nominee(s).
              </div>
            </div>


            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <table id="tblForm09" class="table table-bordered table-striped" >
                <thead>
                 <tr>
                  <th colspan="5" style="background-color: #3CB371; color: #fff;"> Nominee Details </th>
                 </tr> 
                 <tr>
                   <th>Sr No.</th>
                   <th>Name of the nominee in full</th>
                   <th>Nominee's Relationship with the Apprentice</th>
                   <th>Age of nominee</th>
                   <th>Full address of the nominee</th>
                 </tr>
               </thead>
               <tbody>
                <tr>
                   <td>1</td>
                  <td><input type="text" name="full_name_nominee[]" id="full_name_nominee" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Full Name Nominee !" placeholder="Enter Family Member Name" required="required" ></td>
                  <td>
                 <?php 
                 $options = array('' => 'Select Relation');
                 foreach($sysrelations as$key => $value) {
                  $options[$value->id] = $value->relationname;
                }
                echo form_dropdown('relationship_nominee[]', $options, set_value('relationship_nominee'), 'class="form-control" data-toggle="tooltip" title="Select Relation With Nominee !"  id="relationship_nominee" required="required"');
                ?>
                <?php echo form_error("relationship_nominee");?>
                  </td>
                  <td><input type="text" name="age_nominee[]" id="age_nominee" data-toggle="tooltip" title="Enter Age Of Nominee !" placeholder = "Enter Age Of Nominee " value="" class="form-control txtNumeric" required="required" ></td>
                  <td>
                   <textarea name="address_nominee[]" data-toggle="tooltip" maxlength="50" title="Enter Full Address Nominee !" id="address_nominee"  placeholder = "Enter Full Address Nominee" class="form-control" required="required"></textarea>
                 </td>
               </tr>
                <tr id="tbodyForm09" >
                   <td>2</td>
                  <td><input type="text" name="full_name_nominee[]" id="full_name_nominee" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Full Name Nominee !" placeholder="Enter Family Member Name" required="required" ></td>
                  <td>
                     <?php 
                 $options = array('' => 'Select Relation');
                 foreach($sysrelations as$key => $value) {
                  $options[$value->id] = $value->relationname;
                }
                echo form_dropdown('relationship_nominee[]', $options, set_value('relationship_nominee'), 'class="form-control" data-toggle="tooltip" title="Select Relation With Nominee !"  id="relationship_nominee" required="required"');
                ?>
                <?php echo form_error("relationship_nominee");?>
                  </td>
                  <td><input type="text" name="age_nominee[]" id="age_nominee" data-toggle="tooltip" placeholder = "Enter Age Of Nominee" title="Enter Age Of Nominee !" value="" class="form-control txtNumeric" required="required" ></td>
                  <td>
                   <textarea name="address_nominee[]" data-toggle="tooltip" maxlength="50" title="Enter Full Address Nominee !" placeholder = "Enter Full Address Nominee" id="address_nominee" class="form-control" required="required"></textarea>
                 </td>
               </tr>
             </tbody>
           </table>
         </div>
       </div>
       <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label class="field-wrapper required-field">  This nomination and authorisation shall remain valid unless substituted by a fresh nomination and authorisation.</label>
        </div>
      </div>

       <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-right"></div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> Yours sincerely</div>
      </div>

       <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >Place :</div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" > <input type="text" name="daplace" id="daplace" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo set_value('daplace');?>" required="required">  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> </div>
          <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-right">Name </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right" > <input type="text" name="txtname" id="txtname" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">  </div>
          </div>
        </div>
      </div>

       <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">Date : </div>
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  <input type="text" name="dadate" id="dadate" class="form-control datepicker" placeholder=" Please Enter Name"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="" required="required">  </div>
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> </div>
          <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12" > Father's/Husband's Name</div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" > <input type="text" name="txtfathername" id="txtfathername" placeholder=" Please Enter Father Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->fatherfirstname.' '.$candidatedetailwithaddress->fathermiddlename.' '.$candidatedetailwithaddress->fatherlastname;?>" required="required">  </div>
          </div>
        </div>
      </div>
       <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
           <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">Permanent Address : </div>  
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
              <textarea name="permanentaddress" id="permanentaddress" placeholder=" Please Enter Father Name" class="form-control"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"> <?php echo $candidatedetailwithaddress->permanentstreet.' '.$candidatedetailwithaddress->permanentcity.' '.$candidatedetailwithaddress->permanentdistrict.' '.$candidatedetailwithaddress->name.', '.$candidatedetailwithaddress->permanentpincode; ?> </textarea>
            </div>        
           </div>
          <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
          </div>
      </div>

       <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
          <label class="field-wrapper required-field">
          Declaration by Witness</label>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
          <label class="field-wrapper required-field">
         (Persons other than Nominees)</label>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
       <label class="field-wrapper required-field">  The above nomination and authorisation has been signed by Mr. /Ms.<input type="text" name="nomination_authorisation_signed" id="nomination_authorisation_signed" placeholder=" Please Enter Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $candidatedetailwithaddress->candidatefirstname.' '.$candidatedetailwithaddress->candidatemiddlename.' '.$candidatedetailwithaddress->candidatelastname;?>" required="required">  in our presence: </label>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">1 </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right"> Name :</div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
           <select id="firstwitnessname" name="firstwitnessname" class="form-control">
            <option>Select Witness Name</option>
            <?php foreach ($witnessdeclaration as $key => $value) {
              if ($value->staffid == $genenominformdetail->first_witness_name) {
              ?>
            <option value="<?php echo $value->staffid;?>" selected><?php echo $value->name;?> </option>
          <?php } else{?>
           <option value="<?php echo $value->staffid;?>" ><?php echo $value->name;?> </option>
          <?php } } ?>
          </select>

         <!--  <input type="text" name="firstwitnessname" id="firstwitnessname" class="form-control" placeholder=" Please Enter Witness Name"  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php //echo set_value('firstwitnessname');?>" required="required"> -->  </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> </div>
     </div>
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right" >Address :</div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> <textarea type="text" name="firstwitnessaddress" id="firstwitnessaddress" class="form-control" placeholder=" Please Enter Witness Address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"><?php echo set_value('firstwitnessaddress');?></textarea> </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> </div>
     </div>
      <br><br>
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">2</div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">Name :</div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <select id="secondwitnessname" name="secondwitnessname" class="form-control">
          <option>Select Witness Name</option>
          <?php foreach ($witnessdeclaration as $key => $value) {
             if ($value->staffid == $genenominformdetail->second_witness_name) {
              ?>
          <option value="<?php echo $value->staffid;?>" Selected><?php echo $value->name;?> </option>
        <?php }else{ ?>
         <option value="<?php echo $value->staffid;?>"><?php echo $value->name;?> </option>
         <?php } } ?> 
        </select>

  <!-- <input type="text" name="secondwitnessname" id="secondwitnessname" class="form-control" placeholder=" Please Enter Witness Name"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php //echo set_value('secondwitnessname');?>" required="required"> -->  </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> </div>
     </div>
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">Address :</div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><textarea type="text" name="secondwitnessaddress" id="secondwitnessaddress" class="form-control" placeholder=" Please Enter Witness Address"  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"><?php echo set_value('secondwitnessaddress');?></textarea>  </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> </div>
     </div>
   
     <div style="text-align: center;"> 
      <button  type="submit" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save" name="savebtn" id="save" value="senddatasave">Save</button>
         <button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit">Save & Submit</button>
     
      </div>
    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>

<script type="text/javascript">

  $(document).ready(function(){
    decimalData();
    $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

   //$('#pradan_selection_process_before_details').prop("disabled",true); 

 });

  $(function () {
    $("#selection_process_befor").click(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });
  });
  

  $(document).ready(function(){
    $("#filladdress").on("click", function(){
     if (this.checked) { 
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());
    }
    else {
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

  });


  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }
</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    $("#btnsubmit").prop('disabled', true);

     $(".datepicker").datepicker({
         changeMonth: true,
         changeYear: true,
         maxDate: 'today',
         dateFormat : 'dd/mm/yy',
         yearRange: '1920:2030',
        
    });


  });
</script>  

