<?php $date = date("d/m/Y"); ?>
<style>
textarea { resize:none; } 
</style>

 <br/> <br/> <br/>
  <div class="container-flude">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
                    <div class="panel-heading">
                      <div class="row">
                       <h4 class="col-md-12 panel-title pull-left">PRADAN’s APPRENTICESHIP PROGRAMME FOR RURAL DEVELOPMENT <br/>
                         <label class="field-wrapper required-field" style="text-align: center" > JOINING REPORT </label>
                      </h4>
                    </div>
                    <hr class="colorgraph"><br>
                  </div>
                  <div class="panel-body">
          
              <div class="body" style="margin-left: 10px;">
                <div class="row">
              
                 
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
                 <form name="joiningreport" id="joiningreport" method="POST" action="">
                  <div class="row">
                    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                     <div class="form-group">
                      <div class="form-line">
                        <label class="field-wrapper required-field">Address</label>
                        <textarea type="text" class="form-control" name="joining_address" id="joining_address" required="required" readonly="readonly" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" ><?php echo $candidatedetils->permanentstreet.' '.$candidatedetils->permanentcity.' '.$candidatedetils->permanentdistrict.' '.$candidatedetils->name.' '.$candidatedetils->permanentpincode;?> </textarea>
                      </div>

                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" ></div>
                </div>
                <div class="row">
                 <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class="field-wrapper required-field">
                   To, <br>
                   The Executive Director  <br>              
                   PRADAN <br>
                   E 1/A, Ground Floor and Basement <br>
                   Kailash Colony <br>
                   New Delhi - 110 048 <br>
                   Tel: 4040 7700 <br>
                 </label>
               </div>
               <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" ></div>
             </div>

             <div class="row">

              <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
               <div class="form-group">
                <label for="StateNameEnglish" class="field-wrapper required-field">Subject: Joining Report   </label>
              </div>
            </div>

            <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="form-group">
              <label class="field-wrapper required-field">Sir, </label>
            </div>
          </div>
          <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

           <label class="field-wrapper required-field">  Please refer to your offer no. 
            <input type="text" name="offerno" id="offerno"
            style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
            value="<?php echo $candidatedetils->offerno; ?>" readonly="readonly">&nbsp&nbsp  dated &nbsp&nbsp<input type="text" name="offerdate" id="offerdate"   style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $this->gmodel->changedatedbformate($candidatedetils->doj); ?>" readonly="readonly" > &nbsp &nbspoffering me <?php if ($candidatedetils->categoryid == 1){  echo 'apprenticeship';} elseif ( $candidatedetils->categoryid == 2 ) {echo 'executive';
              # code...
            } else{
              echo 'assistant';
            } ?> at  <input type="text" maxlength="50" name=" apprenticeship_at_location" id="  apprenticeship_at_location" placeholder=" Please Enter Location"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" value="<?php echo set_value('  apprenticeship_at_location'); ?>" >.<br><br><br>

            I reported at PRADAN’s office at <input type="text" name="office_at_location" id="office_at_location" required="required" placeholder=" Please Enter Location" maxlength="50"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo set_value('office_at_location'); ?>" >and have joined the programme in the  forenoon of today, the<input type="text" name="join_programme_date" id="join_programme_date" required="required" readonly  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="
            <?php echo $this->gmodel->changedatedbformate(date('Y-m-d')); ?>" >(date/month/year).<br><br><br>
          I shall inform you of any change in my address, given above, when it occurs.</label>
        </div>
        <div row
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
        <div class="panel-footer" style="text-align: right;">
          <button  type="submit" name="submitbtn" id="submit" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 text-right" data-toggle="tooltip" title="savesubmit"> Submit</button> 
      </div>
      </div>
        
      </form>
    </div>
  </div>

</div>
</div>

</div>
<!-- #END# Exportable Table -->
</div>

<script type="text/javascript">

  $(document).ready(function(){
    decimalData();
    // $("#submitbtn").prop("disabled", "disabled");
    $('[data-toggle="tooltip"]').tooltip(); 
    // $('#fromdate').datepicker({ format: "dd/mm/yyyy" });
    // $('#todate').datepicker({ format: "dd/mm/yyyy" }); 

    $("#selection_process_befor").load(function () {
      if ($(this).is(":checked")) {
        $("#pradan_selection_process_before_details").removeAttr("disabled");
      } else {
        $("#pradan_selection_process_before_details").prop("disabled", "disabled");
      }
    });

   //$('#pradan_selection_process_before_details').prop("disabled",true); 

 });

  
  

  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }

</script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    //$("#btnsubmit")..prop('disabled', true);

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     
   });

  });
</script>  

