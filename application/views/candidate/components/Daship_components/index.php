<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <?php //print_r($dashipcomp_details); ?>

        <!-- Exportable Table -->
        <div class="row clearfix">

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

             <div class="header">
              <h2>DAship Components</h2><br>
              <a href="<?php echo site_url("/candidate/Daship_components/add/")?>" class="btn btn-success ">Add New</a> 
            </div>

            <div class="body">
              <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">Name</label>&nbsp;&nbsp;&nbsp;
                    <?php echo $daship_details->candidatefirstname.' '.$daship_details->candidatemiddlename.' '.$daship_details->candidatelastname; ?>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="TeamName" class="field-wrapper required-field">Code</label>&nbsp;&nbsp;&nbsp;
                  <?php echo $daship_details->emp_code; ?>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
             <div class="form-group">
              <div class="form-line">
                <label for="TeamName" class="field-wrapper required-field">Batch</label> &nbsp;&nbsp;&nbsp;
                <?php echo $daship_details->batch; ?>
              </div>
            </div>
          </div>

        </div>
        
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th class="text-center">Id</th>
                <th class="text-center">Phase Name</th>
                <th class="text-center">Document Name</th>
                <th class="text-center">Status </th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if (count($dashipdocumentdetails) == 0) { ?>
               

              <?php 
            }else{
              $i=0; foreach ($dashipdocumentdetails as $key => $value) { ?>
                <tr>
                  <td class="text-center"><?php echo $i+1; ?></td>
                  <td class="text-center"><?php echo $value->phase_name;?></td>
                  <td class="text-center"><a href="<?php echo  site_url().'datafiles/dashipcomponent/'.$value->encrypted_document_name;?>" download ><i class="fa fa-download" style="font-size:20px" aria-hidden="true" 
                    data-toggle="tooltip" title="<?php echo $value->original_document_name;?>" ></i></td>
                    <td class="text-center"><?php if($value->status==1){ ?>
                      <span class="label label-success"> Submitted </span>       
                    <?php  }else if($value->status==2){?>
                      <span class="label label-success">Accepted</span>
                   <?php }else if ($value->status==3) { ?>
                    <span class="label label-danger">Rejected</span>
                <?php   } ?></td>
                   <td class="text-center">
                    <?php if($value->status==3){ ?>
                    <a href="<?php echo site_url().'candidate/Daship_components/edit/'.$value->id ?>"><i class="fa fa-pencil-square-o" style="font-size:15px" aria-hidden="true"></i></a>|
                    <?php } ?>
                    <a href="<?php echo site_url().'candidate/Daship_components/view/'.$value->id ?>"><i class="fa fa-eye" style="font-size:15px" aria-hidden="true"></i></a></td>
                  </tr>
                  <?php $i++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>
      </div>
    </div>
  </div>
</div>

<!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>