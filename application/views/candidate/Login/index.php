<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Pradan</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

    <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />


    <!-- Waves Effect Css -->
    <link href="<?php echo site_url('common/backend/vendor/node-waves/waves.css');?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo site_url('common/backend/css/style.css');?>" rel="stylesheet">
</head>

<body class="login-page">
<?php 
$tr_msg= $this->session->flashdata('tr_msg');
$er_msg= $this->session->flashdata('er_msg11');
if(!empty($tr_msg)){ ?>
<div class="content animate-panel">
    <div class="row">
        <div class="col-md-12">
            <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    
    <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="alert alert-danger fade in alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('er_msg');?>. </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        
    <div class="login-box">
        <div class="card">
            <div class="body">
                <div class="logo text-center">
         <img src="<?php echo site_url('common/backend/images/logo.png');?>"  alt="logo" class="text-center" /> </div>
                <form id="sign_in" method="POST">
                    <?php 
                    $er_msg = $this->session->flashdata('er_msg');
                    if ($er_msg != NULL) { ?>
                    <div class="alert alert-danger fade in alert-dismissable alert1"><?php echo $er_msg; ?></div>
                    <?php }else{ ?>
                    <div class="msg" style="background-color: " >Sign in to start your session</div>
                    <?php } ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="User Name" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-green waves-effect" type="submit">SIGN IN</button>
                            <!-- <a class="btn btn-block bg-pink waves-effect" href="<?php echo site_url('Dashboard');?>">SIGN IN</a> -->
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                          <!--   <a href="#">Register Now!</a> -->
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="<?php echo site_url("login/forget");?>">Forgot Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo site_url('common/backend/vendor/bootstrap/js/bootstrap.js');?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo site_url('common/backend/vendor/node-waves/waves.js');?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo site_url('common/backend/vendor/jquery-validation/jquery.validate.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo site_url('common/backend/js/admin.js');?>"></script>
    <script src="<?php echo site_url('common/backend/js/pages/examples/sign-in.js');?>"></script>
</body>

</html>