<nav class="hornav navbar-inverse bg-primary text-center">
   <!--  <div class="container text-center"> -->
    <div class="collapse navbar-collapse" id="navbar" style="font-size: 16px; color:#fff;">

    <?php 
      $query = $this->db->query('SELECT * FROM `tbl_general_nomination_and_authorisation_form` WHERE `candidateid`='.$this->loginData->candidateid);
      $result = $query->result()[0];

      $joiningreportquery = $this->db->query('SELECT * FROM `tbl_joining_report` WHERE `candidateid`='.$this->loginData->candidateid);
      $joiningreportresult = $joiningreportquery->result()[0];

    //  echo $getgeneralform->generalformstatus;
    //  echo $getjoiningreport->joinreportstatus;

    ?>
      
         <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home<span class="caret"></span></a>
                <ul class="dropdown-menu">
                   
             <?php 
            if (isset($this->loginData->confirm_attend) && $this->loginData->confirm_attend ==0 && $this->loginData->campustype=='off') { ?>

              <li><a href="<?php echo site_url("candidate/Acceptance_for_interview/index");?>"><span>Interview call for accepted </span></a></li>
            <?php } else{    

              if (isset($this->loginData->BDFFormStatus) && $this->loginData->BDFFormStatus ==1) { ?>

            <li>
               <a href="
               <?php echo site_url("candidate/Candidatedfullinfo/index");?>">
               <i class="material-icons col-green"></i>
               <span>BDF Info </span>
             </a>
           </li>
         <?php }else{?>
         <li>
               <a href="
               <?php echo site_url("candidate/Candidatedfullinfo/index");?>">
               <i class="material-icons col-green"></i>
               <span>BDF Info </span>
             </a>
           </li>
        <?php } 

        if(isset($getgeneralform->generalformstatus) && ($getgeneralform->generalformstatus == 1 || $getgeneralform->generalformstatus == 2)) {?>
          <li><a href="
            <?php echo site_url("candidate/General_nomination_and_authorisation_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>

        <?php  }
        elseif (isset($getgeneralform->generalformstatus) && $getgeneralform->generalformstatus ==0) { ?> 

          <li><a href="
            <?php echo site_url("candidate/General_nomination_and_authorisation_form/edit").'/'.$result->id;?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>
        <?php }else{ ?>
  
          <li><a href="
            <?php echo site_url("candidate/General_nomination_and_authorisation_form/index");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>
        <?php }?>
        
        <?php  if(isset($getjoiningreport->joinreportstatus) && ($getjoiningreport->joinreportstatus == 1 || $getjoiningreport->joinreportstatus == 2)) {?>
          <li><a href="
            <?php echo site_url("candidate/Joining_report_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report form</span>
          </a> </li>

           <?php }elseif (isset($getjoiningreport->joinreportstatus) && $getjoiningreport->joinreportstatus == 0) { ?> 

          <li><a href="
            <?php echo site_url("candidate/Joining_report_form/edit").'/'.$joiningreportresult->id;?>">
            <i class="material-icons col-green"></i>
            <span>Joining report form</span></a></li>
        <?php } else{?>
          <li><a href="
            <?php echo site_url("candidate/Joining_report_form/index/");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report form</span>
          </a> </li>
        <?php }?>
        <?php if ($this->loginData->joinstatus==1) { ?>
          <li> <a href="
            <?php echo site_url("candidate/Daship_components");?>">
            <i class="material-icons col-green"></i>
            <span>DAship components</span></li></a>
          </li>
         <?php } } ?>
         
     <li><a href="<?php echo site_url('candidate/login/logout');?>"><i class="fa fa-sign-out fa-3x" aria-hidden="true"></i></a>
   </li>
</ul>
</li>
</ul>
</div>
</nav>

