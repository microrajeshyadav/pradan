
<style type="text/css">
.navbar{
  background: #17A2B8 !important;
  max-height: 50px;
}

.dropdown{
  border-radius:0;

}
.dropdown-menu{
  background: #fff;   
  top:84%;
  border-radius:0px !important;
  border:3px solid #17A2B8 !important;

  padding: 5px;
  z-index: 9999;
  font-size: 14px;
}
.dropdown-item:hover{
  background:#085ca5 !important;
  color:#;
}

ul > li > ul
{
  width :auto;
  border:0 !important;
  background: #f3f3f3 !important;
}
ul > li > ul > li
{
  line-height: 1;
  border-bottom: 1px dashed #d4d4d4;
}

a {
  white-space: nowrap;
}
.pull-right {
  float: right !important;
}

.nav>li>ul>li>a:focus, .nav>li>ul>li>a:hover,.nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{

  background: #17A2B8!important;
  color: #fff !important;

}

.dropdown-submenu {
  position: relative;
  color: #fff !important;
}



.dropdown-submenu>.dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
  -webkit-border-radius: 0 6px 6px 6px;
  -moz-border-radius: 0 6px 6px;
  border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
  display: block;
}

.dropdown-submenu>a:after {
  display: block;
  content: " ";
  float: right;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
  border-width: 5px 0 5px 5px;
  border-left-color: #ccc;
  margin-top: 5px;
  margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
  border-left-color: #fff;
}

.dropdown-submenu.pull-left {
  float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
  left: -100%;
  margin-left: 10px;
  -webkit-border-radius: 6px 0 6px 6px;
  -moz-border-radius: 6px 0 6px 6px;
  border-radius: 6px 0 6px 6px;
}

</style>

<script type="text/javascript">

  $(document).ready(function () {
    $('.navbar-light .dmenu').hover(function () {
      $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
    }, function () {
      $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
    });
  });

</script>
<?php  //echo "<pre>"; print_r($this->loginData); ?>

<nav class="navbar navbar-expand-sm  navbar-light bg-light" style="background-color:#17A2B8 !important; float: left;width: 100%;">
  <div class="collapse navbar-collapse" id="navbar"  style="font-size: 16px; color:#fff;" >
    <?php 
    $query = $this->db->query('SELECT * FROM `tbl_general_nomination_and_authorisation_form` WHERE `candidateid`='.$this->loginData->candidateid);
    $result = $query->row();

    $joiningreportquery = $this->db->query('SELECT * FROM `tbl_joining_report` WHERE `candidateid`='.$this->loginData->candidateid);
    $joiningreportresult = $joiningreportquery->row();

    ?>
    <ul class="nav navbar-nav">
      <li class="dropdown pull-left">
        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home<span class="caret"></span></a>
        <ul class="dropdown-menu">

         <?php 
         if (isset($this->loginData->confirm_attend) && $this->loginData->confirm_attend ==0 && $this->loginData->campustype=='off') { ?>

         <li class="dropdown pull-left"><a href="<?php echo site_url("candidate/Acceptance_for_interview/index");?>" class="nav-link"><span>Interview call for accepted </span></a></li>
         <?php } else{    

          if (isset($this->loginData->BDFFormStatus) && $this->loginData->BDFFormStatus ==1 && $this->loginData->gdemail_status ==1) {    ?>
          <li class="dropdown pull-left">
           <a href="
           <?php echo site_url("candidate/Candidatedfullinfo/index");?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>BDF Info </span>
         </a>
       </li>
       <?php } 

       
       
          # code...
		  
		 if( $this->loginData->categoryid==1)
    
		  {
         if($this->loginData->joinstatus == 2 && count($getgeneralform)== 0) { ?>
          <li class="dropdown pull-left"><a class ="nav-link" href="<?php echo site_url("candidate/General_nomination_and_authorisation_form/index");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>

        <?php  }elseif (isset($getgeneralform->generalformstatus) && ($getgeneralform->generalformstatus == 1 || $getgeneralform->generalformstatus == 2) && $result->status ==1) { ?> 

          <li class="dropdown pull-left"><a class ="nav-link" href="<?php echo site_url("candidate/General_nomination_and_authorisation_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a></li>
		  <?php } }?>
      
        
        <?php 
          # code...
			 if( $this->loginData->categoryid==1)
    
		  {
          if ($this->loginData->joinstatus == 2 && count($getjoiningreport)== 0) { ?> 
          <li class="dropdown pull-left"><a class ="nav-link" href="
            <?php echo site_url("candidate/Joining_report_form/index/");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report form</span></a></li>
		  <?php } } 

            if(isset($this->loginData->BDFFormStatus) && $this->loginData->BDFFormStatus ==1 && $this->loginData->tc_hrd_document_verfied ==2)
           {
            ?>
             <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Employee_particular_form/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Employee Particular Form</span>
         </a> 
         <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Employee_particular_form/addnarrative/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Narrative Self Profile</span>
         </a>
       </li>
       </li>
        <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Joining_report_on_appointment/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span> Joining Report</span>
         </a>
       </li>
       <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Application_from_for_identity_card/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Application for ID Card</span>
         </a>
       </li>
        <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Provident_fund_nomination_form/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>PF Nomination Form</span>
         </a>
       </li>
       <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Gratuity_nomination_form/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Gratutity Nomination Form</span>
         </a>
       </li>
        <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/General_nomination_and_authorisation_form_staff/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>General Nomination Form</span>
         </a>
       </li>
       <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Membership_applicatioin_from/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Membership Application Form</span>
         </a>
       </li>
       <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Membership_applicatioin_from/add/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Declaration About member of family</span>
         </a>
       </li>
        <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Membership_applicatioin_from/edit/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>GMP Enhanced Option</span>
         </a>
       </li>
       <li class="dropdown pull-left">
           <a href="
           <?php echo site_url().'candidate/Medical_certificate/index/'.$this->loginData->staffid.'/'.$this->loginData->candidateid;?>" class="nav-link">
           <i class="material-icons col-green"></i>
           <span>Medical Certificate for Appointment</span>
         </a>
       </li>




            <?php

           }
           
          
      }?>
 
        
      </ul>
    </li>
    
  </ul>
  </div>
  <div class="btn-group btn-group-success pull-right">

    <button class="btn btn-success" type="button"><?php  echo strtoupper($this->loginData->username); ?></button>
    <button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button"><span class="caret"></span>
    </button>
    <ul class="dropdown-menu bg-light" style="position: absolute;
    right: 0px !important; left: auto;  min-width:270px; border-radius: 5px; top:110%;">
    <div class="panel">
      <div class="panel-body">  
       <div class="col-lg-3" style="color: #fff;">
        <img style = "height: 40px; width: 40px;" src="<?php echo site_url('common/backend/images/plogo.jpg'); ?>" class="rounded-circle" alt="Cinque Terre">
      </div>

      <div class="col-lg-9" style="color: #000;">
        <p class="text-left" style=" font-size: 12px;"><strong>Role : Candidate</strong></p>
        <p class="text-left" style=""><strong><?php  echo $this->loginData->candidatefirstname.' '.$this->loginData->candidatemiddlename.' '.$this->loginData->candidatelastname; ?></strong></p>
        <p class="text-left small"><?php echo $this->loginData->emailid; ?></p>
      </div>

    </div>
    <div class="" >
     <div class="col-lg-4" style="color: #000;">

      <a data-toggle="tooltip" data-placement="bottom" title="My Profile" href="<?php echo site_url().'candidate/Candidatedfullinfo/preview/'.$this->loginData->candidateid;?>" class="btn btn-success btn-block blue-tooltip"><i class="fa fa-user" aria-hidden="true"></i></a>

    </div>  
    
    <div class="col-lg-4 offset-md-4" style="color: #000;">
<a data-toggle="tooltip" data-placement="bottom" title = "Logout" href="<?php echo site_url('login/login1');?>" class="btn btn-danger btn-block blue-tooltip"><i class="fa fa-power-off"></i> </a>
    </div>

  </div>
</div>

</ul>
</div>
  </nav>
