
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar" >

  <!-- User Info -->
  <div class="user-info">
    <div class="image">
  <img src="<?php echo site_url('common/backend/images/user.png');?>" width="48" height="48" alt="logo" />
</div>
<div class="info-container">
  <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
  <div class="email"></div>
  <div class="btn-group user-helper-dropdown">
    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
    <ul class="dropdown-menu pull-right">
      <!-- <li><a href="#"><i class="material-icons">person</i>Profile</a></li> -->
    <!--   <li role="seperator" class="divider"></li> -->
      <!-- <li><a href="#"><i class="material-icons">person</i>Change Password</a></li> -->
     <!--  <li role="seperator" class="divider"></li> -->
      <li><a href="<?php echo site_url('login/logout');?>"><i class="material-icons">input</i>Sign Out</a></li>
</ul>
</div>
</div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
  <ul class="list">
    <li class="header" >MAIN NAVIGATION</li>
    <li class="active">
      <a href="javascript:void(0);" class="menu-toggle">
       <i class="material-icons">home</i>
        <span>Home</span>
      </a>
      <ul class="ml-menu">  
  <?php 
  //print_r($this->loginData);
  if ($this->loginData->BDFFormStatus ==1) { ?>
   <li>
          <a href="
          <?php echo site_url("candidate/Candidatedfullinfo/view");?>">
            <i class="material-icons col-green"></i>
            <span>BDF Info </span>
          </a>
        </li> 
  <?php  }else{ ?>

       <li>
          <a href="
          <?php echo site_url("candidate/Candidatedfullinfo");?>">
            <i class="material-icons col-green"></i>
            <span>BDF Info </span>
          </a>
        </li> 
    <?php }

    if(isset($getgeneralform[0]->generalformstatus) && $getgeneralform[0]->generalformstatus == 1 ) {?>
       <li>
          <a href="
          <?php echo site_url("candidate/General_nomination_and_authorisation_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a>
        </li>
      <?php }else{ ?>
       <li>
          <a href="
          <?php echo site_url("candidate/General_nomination_and_authorisation_form");?>">
            <i class="material-icons col-green"></i>
            <span>General nomination and authorisation form
            </span>
          </a>
        </li>

      <?php } ?>
        <div class="clearfix"></div>
          <?php 
         
          if(isset($getjoiningreport[0]->joinreportstatus) && $getjoiningreport[0]->joinreportstatus == 1 ) {?>
        <li>
          <a href="
          <?php echo site_url("candidate/Joining_report_form/view/");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report</span>
          </a> 
        </li>
             
      <?php }else{ ?>

         <li>
          <a href="
          <?php echo site_url("candidate/Joining_report_form");?>">
            <i class="material-icons col-green"></i>
            <span>Joining report</span>
          </a> 
        </li>
          <?php } ?>

          <?php if ($this->loginData->joinstatus==1) { ?>
          
       <li>
          <a href="
          <?php echo site_url("candidate/Daship_components");?>">
            <i class="material-icons col-green"></i>
            <span>Daship components</span>
          </a> 
        </li>
      <?php } ?>
         
        </ul>
    </li>
  </ul>
 
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
  <div class="copyright">
    &copy; 2018 - 2019 <a href="javascript:void(0);">PRADAN</a>.
  </div>
  <div class="version">
    <!-- <b>Version: </b> 1.0.5 -->
  </div>
</div>
<!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->