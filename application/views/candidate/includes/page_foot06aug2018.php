<script>

  $(document).ready(function() {
    adddatepicker();

});


   function adddatepicker(){
       $(".datepicker").datepicker({
         changeMonth: true,
        changeYear: true,
        yearRange: '1920:2030',
        dateFormat : 'dd-mm-yy',
        //defaultDate: new Date(2018, 00, 01)
    });

  }
  
  </script>
<!-- Jquery Core Js -->

<script src="<?php echo site_url('common/backend/jquery-1.12.4.js');?>"></script>
<script src="<?php echo site_url('common/backend/jquery-ui.js');?>"></script>

<!-- Bootstrap Core Js -->
<script src="<?php echo site_url('common/backend/vendor/bootstrap/js/bootstrap.js');?>"></script>

<!-- Select Plugin Js -->
<!-- <script src="<?php echo site_url('common/backend/vendor/bootstrap-select/js/bootstrap-select.js');?>"></script> -->
<!-- Slimscroll Plugin Js -->
<script src="<?php echo site_url('common/backend/vendor/jquery-slimscroll/jquery.slimscroll.js');?>"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo site_url('common/backend/vendor/node-waves/waves.js');?>"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo site_url('common/backend/vendor/jquery-countto/jquery.countTo.js');?>"></script>

 <script src="<?php echo site_url('common/backend/vendor/momentjs/moment.js');?>"></script>

<!-- Morris Plugin Js -->
<script src="<?php echo site_url('common/backend/vendor/raphael/raphael.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/morrisjs/morris.js');?>"></script>

 <script src="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js');?>"></script>
<!-- Custom Js -->
<script src="<?php echo site_url('common/backend/dist/js/bootstrap-formhelpers.js');?>"></script>
<script src="<?php echo site_url('common/backend/dist/js/bootstrap-formhelpers.min.js');?>"></script>
<!-- Custom Js -->
<script src="<?php echo site_url('common/backend/js/admin.js');?>"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
<script src="<?php echo site_url('common/backend/vendor/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

<script src="<?php echo site_url('common/backend/js/pages/tables/jquery-datatable.js');?>"></script>




<!-- <script src="<?php //echo site_url('common/backend/js/pages/forms/basic-form-elements.js');?>"></script> -->

<!-- Demo Js -->
<!-- <script src="<?php //echo site_url('common/backend/js/demo.js');?>"></script>
 --></body>

</html>