
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar" >

  <!-- User Info -->
  <div class="user-info">
    <div class="image">
  <img src="<?php echo site_url('common/backend/images/user.png');?>" width="48" height="48" alt="logo" />
</div>
<div class="info-container">
  <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php   echo $this->session->userdata('login_data')->UserFirstName;?></div>
  <div class="email"><?php echo $this->session->userdata('login_data')->EmailID;?></div>
  <div class="btn-group user-helper-dropdown">
    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
    <ul class="dropdown-menu pull-right">
      <li><a href="#"><i class="material-icons">person</i>Profile</a></li>
      <li role="seperator" class="divider"></li>
      <li><a href="#"><i class="material-icons">person</i>Change Password</a></li>
      <li role="seperator" class="divider"></li>
      <li><a href="<?php echo site_url('login/logout');?>"><i class="material-icons">input</i>Sign Out</a></li>
</ul>
</div>
</div>
</div>
<!-- #User Info -->
<!-- Menu -->

<div class="menu">
  <ul class="list">
    <li class="header" >MAIN NAVIGATION</li>
    <li class="active">
      <a href="javascript:void(0);" class="menu-toggle">
       <i class="material-icons">home</i>
        <span>Home</span>
      </a>
  <?php
  $query = "select * from role_permissions where RoleID = ? and Controller in ('EDashboard', 'EDsummaryeod', 'Campusinchargeintemation', 'HRDDashboard', 'Joining_of_DA','Joining_of_DA_document_verification','Campusinchargeintemation','Writtenscore','Gdscore','Hrscore','Selectedcandidates','Assigntc','Candidates','Hrd_intemation','Proposed_probation_separation','Phase','DA_event_detailing','TC_DAship_Components','DAship_Components') "; 
 $result = $this->db->query($query, [$this->session->userdata("login_data")->RoleID])->result(); 


//print_r($result);
//die;
  if (count($result) > 0) { ?>

      <ul class="ml-menu">    
     <?php foreach ($result as $row) { if ($row->Controller == 'EDashboard' && $row->Action == "index") { ?>
       <li>
          <a href="<?php echo site_url("EDashboard");?>">
            <i class="material-icons col-green"></i>
            <span>EDashboard</span>
          </a>
        </li>
   <?php }else if ($row->Controller == 'EDsummaryeod' && $row->Action == "index") { ?>
           <li>
          <a href="<?php echo site_url("EDsummaryeod");?>">
            <i class="material-icons col-green"></i>
            <span>Ed Summary End of the Day</span>
          </a>
        </li>
   <?php }else if ($row->Controller == 'Campusinchargeintemation' && $row->Action == "index") { ?>
      <li>
          <a href="<?php echo site_url("Campusinchargeintemation");?>">
            <i class="material-icons col-green"></i>
            <span>Campus Incharge Intemation</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'HRDDashboard' && $row->Action == "index") { ?>
     <li>
          <a href="<?php echo site_url("HRDDashboard");?>">
            <i class="material-icons col-green"></i>
            <span>HRD Dashboard</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Joining_of_DA' && $row->Action == "index") { ?>
       <!-- <li>
          <a href="<?php //echo site_url("Writtenscore");?>">
            <i class="material-icons col-green"></i>
            <span>Written Score</span>
          </a>
        </li>
         <li>
          <a href="<?php //echo site_url("Gdscore");?>">
            <i class="material-icons col-green"></i>
            <span>GD Score</span>
          </a>
        </li>
         <li>
          <a href="<?php //echo site_url("Hrscore");?>">
            <i class="material-icons col-green"></i>
            <span>Personal Interview</span>
          </a>
        </li> -->
 
     
           <li>
          <a href="<?php echo site_url("Joining_of_DA");?>">
            <i class="material-icons col-green"></i>
            <span><b>Joining Of DA</b></span>
          </a>
        </li>
        <?php }else if ($row->Controller == 'Joining_of_DA_document_verification' && $row->Action == "index") { ?>
          <li>
          <a href="<?php echo site_url("Joining_of_DA_document_verification");?>">
            <i class="material-icons col-green"></i>
            <span><b>Join DA Document Verification</b></span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Campusinchargeintemation' && $row->Action == "index") { ?>
  
         <li>
          <a href="<?php echo site_url("Campusinchargeintemation");?>">
            <i class="material-icons col-green"></i>
            <span>Campus Incharge Intemation</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Writtenscore' && $row->Action == "Selecttowrittenexamcandidates") { ?>
         <li>
          <a href="<?php echo site_url("Writtenscore/Selecttowrittenexamcandidates");?>">
            <i class="material-icons col-green"></i>
            <span>List of the candidates who eligible for written exam</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Writtenscore' && $row->Action == "index") { ?>
          <li>
          <a href="<?php echo site_url("Writtenscore");?>">
            <i class="material-icons col-green"></i>
            <span>Written Score</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Gdscore' && $row->Action == "Selecttogdscoreexamcandidates") { ?>
        
         <li>
          <a href="<?php echo site_url("Gdscore/Selecttogdscoreexamcandidates");?>">
            <i class="material-icons col-green"></i>
            <span>List of the candidates who eligible for GD</span>
          </a>
        </li>
 <?php }else if ($row->Controller == 'Gdscore' && $row->Action == "index") { ?>       

          <li>
          <a href="<?php echo site_url("Gdscore");?>">
            <i class="material-icons col-green"></i>
            <span>GD Score</span>
          </a>
        </li>
 <?php }else if ($row->Controller == 'Hrscore' && $row->Action == "index") { ?>       
         <li>
          <a href="<?php echo site_url("Hrscore");?>">
            <i class="material-icons col-green"></i>
            <span>Personal Interview</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Selectedcandidates' && $row->Action == "index") { ?>
         <li>
          <a href="<?php echo site_url("Selectedcandidates");?>">
            <i class="material-icons col-green"></i>
            <span>List Of Selected Candidates</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Assigntc' && $row->Action == "index") { ?>
          <li>
          <a href="<?php echo site_url("Assigntc");?>">
            <i class="material-icons col-green"></i>
            <span>Assign TC</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Hrd_intemation' && $row->Action == "index") { ?>
 <li>
          <a href="<?php echo site_url("Hrd_intemation");?>">
            <i class="material-icons col-green"></i>
            <span>HR intemation</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Proposed_probation_separation' && $row->Action == "index") { ?>
<li>
          <a href="<?php echo site_url("Proposed_probation_separation");?>">
            <i class="material-icons col-green"></i>
            <span>Proposed Probation Separation</span>
          </a>
        </li>
  <?php }else if ($row->Controller == 'Phase' && $row->Action == "index") { ?>
<li>
          <a href="<?php echo site_url("Phase");?>">
            <i class="material-icons col-green"></i>
            <span>Phase</span>
          </a>
        </li>

        <?php }else if ($row->Controller == 'TC_DAship_Components' && $row->Action == "index") { ?>
          <li>
          <a href="<?php echo site_url("TC_DAship_Components");?>">
            <i class="material-icons col-green"></i>
            <span>TC DAship Components</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'DA_event_detailing' && $row->Action == "index") { ?>
<li>
          <a href="<?php echo site_url("DA_event_detailing");?>">
            <i class="material-icons col-green"></i>
            <span>DA Event Detailing</span>
          </a>
        </li>
  <?php }else if ($row->Controller == 'EDsummaryeod' && $row->Action == "index") { ?>
           <li>
          <a href="<?php echo site_url("EDsummaryeod");?>">
            <i class="material-icons col-green"></i>
            <span>Ed Summary End of the Day</span>
          </a>
        </li>
<?php }else if ($row->Controller == 'Candidates' && $row->Action == "index") { ?>
        <li>
      <a href="<?php echo site_url("Candidates");?>">
        <i class="material-icons"></i>
        <span>Candidates</span>
      </a>
    </li>
 <?php } } ?> 
      </ul>
    </li>
    
  <?php } ?>
   <!--  <li>
      <a href="<?php //echo site_url("clinical_due_list");?>">
        <i class="material-icons"></i>
        <span>Clinical Due List</span>
      </a>
    </li>
    <li>
      <a href="<?php //echo site_url("patient_due_list");?>">
        <i class="material-icons"></i>
        <span>Patient Due List</span>
      </a>
    </li>
    <li>
      <a href="<?php //echo site_url("referred_due_list");?>">
        <i class="material-icons"></i>
        <span>Referred Due List</span>
      </a>
    </li>
     <li>
      <a href="<?php //echo site_url("e_clinic_referral");?>">
        <i class="material-icons"></i>
        <span>E-Clinic Referral List</span>
      </a>
    </li> -->
    <!--<li>
      <a href="<?php //echo site_url("Permissions");?>">
        <i class="material-icons"></i>
        <span>Permissions Manage</span>
      </a>
    </li>-->
<?php  $query = "select * from role_permissions where RoleID = ? and Controller in ('Staffrecruitmentteammapping', 'Campusstaffmapping','Recruitersheadmapping','Campus','Batch','State') group by Controller";
  $result = $this->db->query($query, [$this->session->userdata("login_data")->RoleID])->result();
  if (count($result) > 0) { ?>
    <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons"></i>
        <span>Masters</span>
      </a>
        
      <ul class="ml-menu">
         <?php foreach ($result as $row) { if ($row->Controller == 'Staffrecruitmentteammapping' && $row->Action == "index") { ?>
         <li>
          <a href="<?php echo site_url("Staffrecruitmentteammapping");?>">
            <i class="material-icons col-red"></i>
            <span>Staff to Recruitment team mapping</span>
          </a>
        </li>
    <?php } else if ($row->Controller == 'Campusstaffmapping' && $row->Action == "index") { ?>

         <li>
          <a href="<?php echo site_url("Campusstaffmapping");?>">
            <i class="material-icons col-red"></i>
            <span>Campus to staff mapping</span>
          </a>
        </li>
   <?php } else if ($row->Controller == 'Recruitersheadmapping' && $row->Action == "index") { ?>
         <li>
          <a href="<?php echo site_url("Recruitersheadmapping");?>">
            <i class="material-icons col-red"></i>
            <span>Recruiters to recruiters head mapping</span>
          </a>
        </li>

        <?php } else if ($row->Controller == 'Campus' && $row->Action == "index") { ?>
        
         <li>
          <a href="<?php echo site_url("Campus");?>">
            <i class="material-icons col-red"></i>
            <span>Campus</span>
          </a>
        </li>
       <?php } else if ($row->Controller == 'Batch' && $row->Action == "index") { ?>
         <li>
          <a href="<?php echo site_url("Batch");?>">
            <i class="material-icons col-red"></i>
            <span>Batch</span>
          </a>
        </li>
         <?php } else if ($row->Controller == 'State' && $row->Action == "index") { ?>
        <li>
          <a href="<?php echo site_url("State");?>">
            <i class="material-icons col-red"></i>
            <span>State</span>
          </a>
        </li>
    <?php } } ?>
      
      </ul>
    </li>
<?php }  ?> 



  <?php  $query = "select * from role_permissions where RoleID = ? and Controller in ('user') group by Controller";
  $result = $this->db->query($query, [$this->session->userdata("login_data")->RoleID])->result();
  if (count($result) > 0) { ?>
    <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons"></i> <span>Manage User</span>
      </a>
      <ul class="ml-menu">
      <?php foreach ($result as $row) { if ($row->Controller == 'user' && $row->Action == "index") { ?>
        <li>
          <a href="<?php echo site_url("user");?>">
            <i class="material-icons col-blue"></i>
            <span>USER Manage</span>
          </a>
        </li>
      <?php } } ?>
      </ul>
    </li>
<?php } ?>

    
<?php  $query = "select * from role_permissions where RoleID = ? and Controller in ('Permissions') group by Controller";
  $result = $this->db->query($query, [$this->session->userdata("login_data")->RoleID])->result();
  if (count($result) > 0) { ?>   
     <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons"></i> <span>Manage Permissions</span>
      </a>
      <ul class="ml-menu">
      <?php foreach ($result as $row) { if ($row->Controller == 'Permissions' && $row->Action == "index") { ?>
        <li>
          <a href="<?php echo site_url("Permissions");?>">
            <i class="material-icons col-blue"></i>
            <span>Permissions Manage</span>
          </a>
        </li>
      <?php } } ?>
      </ul>
    </li>
  <?php } ?>
   
  </ul>

</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
  <div class="copyright">
    &copy; 2018 - 2019 <a href="javascript:void(0);">PRADAN</a>.
  </div>
  <div class="version">
    <!-- <b>Version: </b> 1.0.5 -->
  </div>
</div>
<!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->