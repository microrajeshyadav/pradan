<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Pradan | </title>
    <!-- Favicon-->
   <!--  <link rel="icon" href="favicon.ico" type="image/x-icon"> -->
   <link rel="icon" href="<?php echo site_url('favicon.png');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <script src="<?php echo site_url('common/backend/vendor/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

      <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo site_url('common/backend/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');?>" rel="stylesheet" />

    <!-- Bootstrap Form Helper Css -->
    <link href="<?php echo site_url('common/backend/dist/css/bootstrap-formhelpers.css');?>" rel="stylesheet">
    
     <link href="<?php echo site_url('common/backend/dist/css/bootstrap-formhelpers.min.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo site_url('common/backend/vendor/animate-css/animate.css');?>" rel="stylesheet" />
    
    <!-- JQuery DataTable Css -->
    <link href="<?php echo site_url('common/backend/vendor/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">
    
    <!-- Custom Css -->
    <link href="<?php echo site_url('common/backend/css/style.css');?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo site_url('common/backend/css/themes/all-themes.css');?>" rel="stylesheet" />
    <link href="<?php echo site_url('common/backend/jquery-ui.css');?>" rel="stylesheet" />

    <!-- FONT AWESOME STYLE  -->
    <link href="<?php echo site_url('common/backend/assets/css/font-awesome.css');?>" rel="stylesheet" />

        
</head>
    <body  style="background-color: #fff;">
    <div id="header">
      <div class="container-fluid">
   <!-- <div class="row hidden-xs" style="background-color: #26b99a; color: #FFF; height:40px;">
    <div class="col-md-4" style="text-align: left;" >
     24 July, 2018 | 10:32 PM IST

   </div>
   <div class="col-md-4 text-center" style="font-size: 28px;">
     Pradan HR
   </div>
   <div class="col-md-4" style="text-align: right;">
  
  </div>
</div> -->
<div class="row text-center" style="background-color:#FFF;">
  <!-- Logo -->
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-left" style="font-size:10px; font-family:arial; ;  padding-top: 7px; color: #2E3192;">
    <img src="<?php echo site_url('common/backend/images/plogo.jpg'); ?>" alt="Logo" width="300px;"  />
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  pull-right">
   <img src="<?php echo site_url('common/backend/images/logo_hr.jpg'); ?>" alt="Logo" height="100px" width="100px" />
   <img src="<?php echo site_url('common/backend/images/HR logo3.jpg'); ?>" alt="Logo" height="100px" width="100px" />
</div>
<br>
<span style="font-size: 12px; color:#5cb85c; margin-top: 5px; font-weight: bold;"> </span>
</div>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center" style="color: #fff; font-family:courier ; padding-top: 3px;">
  <!--  <img src="<?php //echo site_url('common_libs/img/ozonecelllogo.png'); ?>" alt="Logo" style="" /> -->
</div>
<!-- End Logo -->
</div>
</div>