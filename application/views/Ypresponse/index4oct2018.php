
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">

    <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>YPR Response List</b></div>
      <div class="panel-body">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. <b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <?php //echo "<pre>"; print_r($acceptedcandidatedetails); ?>
              <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >

             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" > <label for="Name">Choose Letter<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
              <select class="form-control" name="chooseletter" id="chooseletter" required="required">
                <option value="" >Select Letter </option>
              <?php foreach ($getyprresponsetype as $key => $value) {?>
                <option value="<?php echo $value->id;?>"><?php echo $value->ypr_type;?></option>
              <?php } ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            </div>
            <br><br>
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Interview Date<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
           <input type="text" name="interview_date" id="interview_date" value="" class="form-control datepicker">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
             <br>
             <br>
             
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Letter<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
             <textarea class="form-control" name="latter" id="letter" cols="20" rows="10" required=""></textarea>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
                 <br>
                 <br>
                
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">

                   <table id="Inboxtable" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                      <th>Select</th>
                      <th>Candidate Name</th>
                      <th>State </th>
                      <th>City</th>
                      <th>Degree</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php $i=0; foreach ($acceptedcandidatedetails as $key => $value) {
                      ?>
                      <tr>
                       <td>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="accept[]" value="<?php echo $value->candidateid; ?>" id="defaultCheck_accept_<?php echo $i; ?>">
                          <label class="form-check-label" for="defaultCheck_accept_<?php echo $i; ?>">
                          </label>
                        </div>

                      </td>
                      <td><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname;?> </td>
                      <td><?php echo $value->statename;?></td>
                      <td><?php echo $value->permanentcity;?></td>
                      <td><?php echo $value->ugspecialisation;?></td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>

              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;"><button type="submit" class="btn btn-success" name="btnsend" id="checkBtn" value="AcceptData" >Send</button> 
                <a href="<?php echo site_url().'Ypresponse/'; ?>" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</a>

              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
           
          </form>
        </div>
      </div>
    </div>   
  </section>

  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable(); 
    });
  </script>  

  <script type="text/javascript">
    $(document).ready(function () {
      $('#checkBtn').click(function() {
        checked = $("input[type=checkbox]:checked").length;

        if(!checked) {
          alert("You must check at least one checkbox.");
          return false;
        }else{

          if (!confirm('Are you sure?')) {
              return false;
            }
          
        }


      });
    });


     $("#chooseletter").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getYPRResponse/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
        console.log(data);
        $("#letter").html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

  </script>
