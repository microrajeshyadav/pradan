
<section class="content" style="background-color: #FFFFFF;" >

    <?php foreach ($role_permission as $row) { if ($row->Controller == "Ypresponse" && $row->Action == "index"){ ?>
  <br><div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Young Professional Response(YPR) List</h4>
      
    </div>
      <hr class="colorgraph"><br>
    </div>

  
      
      <div class="panel-body">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <?php //echo "<pre>"; print_r($acceptedcandidatedetails); ?>
              <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >

             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" > <label for="Name">Choose Letter<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
              <select class="form-control" name="chooseletter" id="chooseletter" required="required">
                <option value="" >Select Letter </option>
              <?php foreach ($getyprresponsetype as $key => $value) {?>
                <option value="<?php echo $value->id;?>"><?php echo $value->ypr_type;?></option>
              <?php } ?>
              </select>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            </div>
            <br><br>
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Interview Date<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
           <input type="text" name="interview_date" id="interview_date" value="" class="form-control datepicker" required="required" autocomplete="false">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
             <br>
             <br>
             
            <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> -->
<!--             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Interview Time<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
             <input type="text" class="form-control" name="interview_time" id="interview_time" maxlength="1500" minlength="5"  required="" value="" placeholder="hh:mm">
              
            </div> -->
            <br>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Letter<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
              <textarea class="summernote" id="summernote" name="summernote" rows="10"></textarea>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             </div>
                 <br>
                 <br>
                
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">

                   <table id="Inboxtable" class="table table-bordered dt-responsive table-striped wrapper">
                    <thead>
                     <tr>
                      <th style ="max-width:50px;" class="text-center">Select</th>
                      <th>Category</th>
                      <th>Campus Name</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>State </th>
                      <th>City</th>
                      <th>Degree</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php $i=0; foreach ($acceptedcandidatedetails as $key => $value) {
                      ?>
                      <tr>
                       <td class="text-center">
                        <div class="form-check">
                          <input class="form-check-input"  type="checkbox" name="accept[]" value="<?php echo $value->candidateid; ?>" id="defaultCheck_accept_<?php echo $i; ?>">
                          <label class="form-check-label" data-toggle="tooltip"  title="Click here to select candidate for response list" for="defaultCheck_accept_<?php echo $i; ?>">
                          </label>
                        </div>

                      </td>
                      <td><?php echo $value->categoryname;?></td>
                      <td><?php echo $value->campusname;?></td>
                      <td><?php echo $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname;?> </td>
                      <td><?php echo $value->gender;?></td>
                      <td><?php echo $value->statename;?></td>
                      <td><?php echo $value->permanentcity;?></td>
                      <td><?php echo $value->ugspecialisation;?></td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>

              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;"><button type="submit" class="btn btn-success" name="btnsend" id="checkBtn" value="AcceptData" >Send</button> 
                <a href="<?php echo site_url().'Ypresponse/'; ?>" class="btn btn-danger" name="btncancel" id="btncancel">Cancel</a>

              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
           
          </form>
        </div>
      </div>
    </div>   
  </section>
 <?php } } ?>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.min.js"></script> -->

  <script>
    $(document).ready(function(){
       $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
         minDate:'today',
        maxDate: '+3Y',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
    });
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable();
    });
  </script>  

  <script type="text/javascript">

  $(document).ready(function() {
    var summernote = $('#summernote');
    // summernote.summernote({
    //     height: ($(window).height()),
    //     focus: false,
    //     toolbar: [
    //     ['style', ['bold', 'italic', 'underline', 'clear']],
    //             ['font', ['strikethrough']],
    //             ['fontsize', ['fontsize']],
    //             ['para', ['ul', 'ol', 'paragraph']],
    //             ['height', ['height']],
    //   ],
    //     oninit: function() {
            // Add "open" - "save" buttons $('#foo').addClass('connected-sortable droppable-area2');
            /*var noteBtn = '<button id="makeSnote" type="button" class="btn btn-default btn-sm btn-small" title="Identify a music note" data-event="something" tabindex="-1"><i class="fa fa-music"></i></button>';            
            var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
            $(fileGroup).appendTo($('.note-toolbar'));*/
            // Button tooltips
    //         $('#makeSnote').tooltip({container: 'body', placement: 'bottom'});
    //         // Button events
    //         $('#makeSnote').click(function(event) {
    //             var highlight = window.getSelection(),  
    //                 spn = document.createElement('span'),
    //                 range = highlight.getRangeAt(0)
                
    //             spn.innerHTML = highlight;
    //             spn.className = 'snote';  
    //             spn.style.color = 'blue';
            
    //             range.deleteContents();
    //             range.insertNode(spn);
    //         });
    //      },        
    // }); 

});
</script>

  <script type="text/javascript">
    $(document).ready(function () {
      $('#checkBtn').click(function() {
        checked = $("input[type=checkbox]:checked").length;
        if(!checked) {
          alert("You must check at least one checkbox.");
          return false;
        }else{
          if (!confirm('Are you sure?')) {
              return false;
            }
        }
      });
    });


     $("#chooseletter").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getYPRResponse/',
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
        console.log(data);
        
        $(".note-editable").html(data);
        $('[name="summernote"]').html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    });


   // $('#interview_time').timepicker({ 'scrollDefault': 'now' });

  </script>
