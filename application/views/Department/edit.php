<section class="content">
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Department" && $row->Action == "edit"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-sm-offset-4">
          <form method="POST" action="">
           <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Edit Department</b> 
            </div>
            <div class="panel-body">
                         <div class="form-group">
                        <div class="form-line">
                          <label for="StateCode" class="field-wrapper required-field">Department Name <span style="color: red;" >*</span></label>
                          <input type="text" class="form-control" id="depname" maxlength="50" minlength="2" name="depname" value="<?php echo $mstdepartment_details->name;?>">
                        </div>
                         <?php echo form_error("depname");?>
                      </div>

                           <div class="form-group">
                        <div class="form-line">
                          <label for="StateCode" class="field-wrapper required-field">Department Code <span style="color: red;" >*</span></label>
                          <input type="text" class="form-control th1" id="depcode" maxlength="50" minlength="2" name="depcode" value="<?php echo $mstdepartment_details->code;?>">
                        </div>
                         <?php echo form_error("depcode");?>
                      </div>



              <div class="form-group">
                        <div class="form-line">
                          <label for="SNS" class="field-wrapper required-field">Status <span style="color: red;" >*</span></label>
                          <?php   

                        //$options = array('' => 'Select Status');     
                        $options = array('0' => 'Active', '1' => 'InActive');
                       echo form_dropdown('status', $options, $mstdepartment_details->status, 'class="form-control"'); 
                      ?>
                      </div>
                        <?php echo form_error("status");?>
                      </div>

            </div>
            <div class="panel-footer text-right"> 
              <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
              <a href="<?php echo site_url("Department/index");?>" class="btn btn-danger btn-sm m-t-10 waves-effect">Go to List</a>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php } } ?>
</section>


<script type="text/javascript">
  
  $('.th1').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});


</script>