<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="row clearfix doctoradvice">
                  <h4 class="header" class="field-wrapper required-field" style="color:#f44336">ANM Management</h4>
                  <div class="col-sm-12">
                    <form method="POST" action="">
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">Role Id</label>
                          <input type="text" class="form-control" name="RoleID" id="RoleID" value="<?php echo $role_list->RoleID;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="form-line">
                          <label for="doctoradvice_place" class="field-wrapper required-field">Role Name</label>
                          <input type="text" class="form-control" name="RoleName" id="RoleName" value="<?php echo $role_list->RoleName;?>">
                        </div>
                      </div>
                     
                      <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                      <a href="<?php echo site_url("anm/index");?>" class="btn btn-success btn-sm m-t-10 waves-effect">Close</a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Exportable Table -->
      </div>
    </section>