<section class="content">
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="header">
                <div class="title_left">
                  <h2>Role Management
                    <a href="<?php echo site_url("role/add");?>"  class="btn btn-round btn-primary">
                      <i class="fa fa-edit"></i> Add Role
                    </a>
                  </h2>
                </div>

              </div>
              <div class="body">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                      <tr>
                        <td class="text-center">ROLE ID</td>
                        <th class="text-center">ROLE NAME</th>
                        <th class="text-center">Command</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      foreach($role_list as $row){ ?>
                      <tr>
                        <td class="text-center"><?php echo $row->RoleID; ?></td>
                        <td class="text-center"><?php echo $row->RoleName; ?></td>
                        <td class="text-center">
                          <a href="<?php echo site_url('role/Edit/'.$row->ID);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
                          <a href="<?php echo site_url('role/delete/'.$row->ID);?>" style="padding: 4px;" data-toggle="confirmation" data-title="Do you want to delete"  data-singleton="True" data-placement="left"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Exportable Table -->
      </div>
    </section>
