<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" rel="stylesheet">

<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "index"){ ?>
 <br>
  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


<?php  //print_r($state_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Manage Salery Details </b>
             <div class="pull-right">
             
             </div>
           </div>
           <div class="panel-body">
             
              <!-- <form name="frm-salarydetail" id="frm-salarydetail"> -->       
                <table id="tblsalery" class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th></th>
                      <th class="text-center" style="width: 50px;">S.No.</th>
                      <th class="text-center">Staff Code</th>
                      <th class="text-center" >Staff Name</th>
                      <th class="text-center">Designation</th>
                      <th class="text-center">Basic Salery</th>
                      <!-- <th class="text-center">Check Box</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                   // print_r($staffcategory_details);
                     $i=0; foreach($mstdatass_details as $row){
                      // print_r($mstdatass_details);
                      // die();

                      ?>
                    <tr>
                      <td></td>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $row->emp_code; ?></td>
                      <td ><?php echo $row->name; ?></td>
                      <td ><?php echo $row->desname; ?></td>
                      <td class="text-center"><input class="form-control" type="text" name="" value="<?php echo $row->e_basic; ?>"></td>
                      <!-- <td class="form-check text-center">
                        <input type="checkbox" class="form-check-input" id="activeField">
                      </td> -->
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
                <button type="submit" id="frm-salarydetail" class="btn btn-primary">Submit</button>
              <!-- </form> -->
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  <?php } } ?>
  </section>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>  
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
 -->
<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    var table = $('#tblsalery').DataTable({
      "columnDefs": [ {
          orderable: false,
          className: 'select-checkbox',
          targets:   0
      } ],
      "select": {
          style:    'multi',
          selector: 'td:first-child'
      },
      "checkboxes": {
        'selectRow': true
      },
      "order": [[1, 'asc']],
      "paging": true,
      "search": true
    });
    $('#frm-salarydetail').on('click', function(e){
      var form = this;
      //var rows_selected = table.tr.selected();
      /*table.column( 2 ).data().each( function ( value, index ) {
        console.log( 'Data in index: '+index+' is: '+value );
      } );*/

      $("tbody").find("tr").each(function() {
        alert('deepak');
      });

      // Iterate over all selected checkboxes
      /*$.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'id[]')
                .val(rowId)
         );
      });*/
   });            
  });
  

</script>