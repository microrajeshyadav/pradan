<form method="POST" action="" name="sep_formality" id="sep_formality">
 <section class="content" style="background-color: #FFFFFF;" >
  <?php // foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "index"){ ?>

   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
    <div class="col-md-12">
      <div class="hpanel">
        <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
            </div>
          </div>
        <?php } ?>
        <br>
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - IOM SEPARATION FORMALITIES
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
       <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
       <input type="hidden" name="change_flag" id="change_flag" value="">
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h4> IOM SEPARATION FORMALITIES</h4> 
        </div>
        <div class="col-md-12 text-center">
          <h5>Professional Assistance for Development Action (PRADAN)</h5> 
        </div>
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6><i>Inter-Office Memo</i></h6>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         <p>To :  <label name="nameofemployee" class="inputborderbelow"> <?php echo $staff_detail->name; ?></label> </p>
         <p style="font-size: 12px;">Employee/Supervisor/Assistant </p>
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         Date:<label name="todaydate" class="inputborderbelow"> <?php echo date('d/m/Y')  ?></label>
       </div>
       <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         <p>From : Personnel Unit</p>
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
        File  :  301- employee code/PDR/<label name="employee_code" class="inputborderbelow"> <?php echo $staff_detail->emp_code; ?></label>
      </div>

    </div>  

    <div class="row" style="line-height: 2">
      <div class="col-md-12">
        Subject: <b>Separation formalities</b>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
       As per our separation procedure, when someone separates from PRADAN we will deactivate her/his PRADAN email account. We will be deactivating your account on next day of your last working day. Hence request you to save all your important emails, as after deletion of account, emails cannot retrieve. Please inform us after saving your important emails and share your alternate email ID, contact number and complete correspondence address for future communication.
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
      Handover PRADAN ID card to your supervisor. Fill up and send relevant attached applications.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
      <p>Please send us enclosed check sheet, clearance certificate, exit interview form and handover format for further process.</p>
     <p> <strong> The Check sheet and Clearance should be forwarded to Personnel Unit only after the clearance at the level of team and employee and update of PAFS and leave MIS.</strong> </p>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
     Thanks, <br>
     Personnel Unit

   </div>
 </div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
   P.S. Experience Certificate and Reliving letter and Gratuity, leave encashment and PF will be processed on receipt of clearance from Team and Finance Unit of Delhi.
 </div>
 
</div>
</div>
<div class="panel-footer text-right">
  <?php
 // echo $certificate_detail->flag;
   if(empty($formality_detail)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="button" name="saveandsubmit" value="Submit & Send" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">
  <?php
    }else{

      if($formality_detail->flag == 0){
 ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="button" name="saveandsubmit" id="saveandsubmit" value="Save & Submit" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">
<?php } ?>
 
<?php } ?>
 <a href="<?php echo site_url("Staff_personnel_approval");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</div>
</div>
</section>

<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->

      <!-- <form method="POST" name="acceptance_regination_modal" id="acceptance_regination_modal"> -->
      <div class="modal-content">
        <div class="modal-header">
          <label for="Name" class="pull-left">Notes<span style="color: red;" >*</span></label>
          <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
           
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes"  require> </textarea>
              <?php echo form_error("comments");?>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          <input type="button" name="saveandsubmit" value="Save & Submit" class="btn btn-success btn-sm" onclick="acceptancereginationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
<script type="text/javascript">
  function acceptancereginationmodal(){
    var dd=1;
    $("#change_flag").val(dd);
    var comments = document.getElementById('comments').value;

    if(comments.trim() == ''){
      $('#comments').focus();
    }else{
      //document.getElementById("acceptance_regination_modal").submit();
      document.getElementById("sep_formality").submit();
    }
  }
</script>
<?php 
if ($formality_detail){
if($formality_detail->flag == 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
       
  $('#saveandsubmit').hide() ;
  $("form input[type=text]").prop("disabled", true);

  $("form input[type=radio]").prop("disabled", true);
  $("form textarea").prop("disabled", true);
  $("form textarea").css("width", "600px;");
  $("form textarea").attr("style", "width:600px;");
  });
</script>
<?php }} ?>
