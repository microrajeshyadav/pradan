<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - IOM SEPARATION FORMALITIES
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h4> IOM SEPARATION FORMALITIES</h4> 
        </div>
        <div class="col-md-12 text-center">
          <h5>Professional Assistance for Development Action (PRADAN)</h5> 
        </div>
        <div class="col-md-12 text-center" style="margin-bottom: 50px;">
          <h6><i>Inter-Office Memo</i></h6>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         <p>To :  ________________________</p>
         <p style="font-size: 12px;">Employee/Supervisor/Assistant </p>
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         Date: _______________________
       </div>
       <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         <p>From : Personnel Unit</p>
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
        File  :  301- employee code/PDR/____________
      </div>

    </div>  

    <div class="row" style="line-height: 2">
      <div class="col-md-12">
        Subject: <b>Separation formalities</b>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
       As per our separation procedure, when someone separates from PRADAN we will deactivate her/his PRADAN email account. We will be deactivating your account on next day of your last working day. Hence request you to save all your important emails, as after deletion of account, emails cannot retrieve. Please inform us after saving your important emails and share your alternate email ID, contact number and complete correspondence address for future communication.
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
      Handover PRADAN ID card to your supervisor. Fill up and send relevant attached applications.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
      Please send us enclosed check sheet, clearance certificate, exit interview form and handover format for further process.<strong> The Check sheet and Clearance should be forwarded to Personnel Unit only after the clearance at the level of team and employee and update of PAFS and leave MIS.</strong>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
     Thanks, <br>
     Personnel Unit

   </div>
 </div>

<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
   P.S. Experience Certificate and Reliving letter and Gratuity, leave encashment and PF will be processed on receipt of clearance from Team and Finance Unit of Delhi.
 </div>
 
</div>
</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>