<section class="content">


  <div class="container-fluid">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>


<?php  //print_r($state_details); ?>
        <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

             <div class="header">
              <h2>Manage Letter Master</h2><br>
              <a href="<?php echo site_url("Letter_master/add/")?>" class="btn btn-success pull-right">Add Letter Master</a>
            </div>
            <div class="body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                  <thead>
                    <tr>
                      <th class="text-center">Id</th>
                       <th class="text-center">Letter Name</th>
                      <th class="text-center">Letter Path</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=0; foreach($letter_details as $row){ ?>
                    <tr>
                      <td class="text-center"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $row->type; ?></td>
                      <td class="text-center">
                       <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal_<?php echo $row->id;?>">Message</button>
                        </td>
                      <td class="text-center">
                        <a href="<?php echo site_url('Letter_master/edit/'.$row->id);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
                        <a href="<?php echo site_url('Letter_master/delete/'.$row->id);?>" id="statedl" onclick="return confirm_delete()" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>

                      </td>
                    </tr>

                       <div id="myModal_<?php echo $row->id;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Message</h4>
                            </div>
                            <div class="modal-body">
                              <p>
                                <?php   
                              $filepath =  $row->file_path;
                              $fd = fopen($filepath, "r"); 
                              $message =fread($fd,4096);
                              //eval ("\$message = \"$message\";");
                              echo $body = nl2br($message);
                                ?>

                               </p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  
  </section>
<script>
  
function confirm_delete() {
  
    var r = confirm("Are you sure you want to delete this letter?");

    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
</script>