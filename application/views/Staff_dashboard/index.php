<style type="text/css">

  .icon-b {
    color: #FF69B4;
  }
</style>
<section class="content" style="">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="page-header">Workarea</h3>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- update on 9/7/2019 -->
      <?php  if($this->loginData->RoleID == '3') {?>
        <div class="row tile_count bg-light">
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user fa-2x green"></i> Total Staff</span>
            <div class="count green"> <?php echo $dashboardstat->totaltaff;?></div> 
            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>4% </i>From last month</span>             
          </div>

          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-male fa-2x blue"></i> Total Male Staff</span>
            <div class="count blue"> <?php echo  $dashboardstat->malecount;?></div>
            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>2% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-female fa-2x icon-b"></i> Total Female Staff</span>
            <div class="count icon-b"> <?php echo  $dashboardstat->femalecount;?></div>
            <span class="count_bottom"><i class="red"><i class="fas fa-angle-down"></i>1% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fas fa-clock fa-2x"></i> Last Month Average Attendance</span>
            <div class="count green">90.23</div>

            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>13% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-rupee-sign fa-2x""></i> Jan, 2019 Gross Salary</span>
            <div class="count green">26,00,315</div>
            <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>6% </i> From last month</span>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user fa-2x"></i> Total separation in this month</span>
            <div class="count red"> <?php echo  $dashboardstat->totalsep;?></div>
            <span class="count_bottom"><i class="red"><i class="fas fa-angle-down"></i>3% </i> From last month</span>
          </div>
        </div>
      <?php  } ?>
      <!-- update on 9/7/2019 -->
      <!-- <div class="row tile_count bg-light">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Total Employees</span>
          <div class="count green">2500</div> 
          <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>4% </i>From last month</span>             
        </div>
       
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Total Male Employees</span>
          <div class="count green">1,300</div>
          <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>2% </i> From last month</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Total Female Employees</span>
          <div class="count red">1,200</div>
          <span class="count_bottom"><i class="red"><i class="fas fa-angle-down"></i>1% </i> From last month</span>
        </div>
         <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fas fa-clock"></i> Your Last Month Attendance</span>
          <div class="count green">24</div>

          <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>13% </i> From last month</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-rupee-sign"></i> Last Month Gross Salary</span>
          <div class="count green">26,456</div>
          <span class="count_bottom"><i class="green"><i class="fas fa-angle-up"></i>6% </i> From last month</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
          <span class="count_top"><i class="fa fa-user"></i> Staff join in this month</span>
          <div class="count red">25</div>
          <span class="count_bottom"><i class="red"><i class="fas fa-angle-down"></i>3% </i> From last month</span>
        </div>
      </div> -->
      <div class="container-fluid">

       <div id="counter" class="row sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <div class="count"> <p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/leave.jpg'); ?>"> </p>
                <p class="count_top" style="font-weight: 600;">Leave Request</p>
                <P><a href="<?php echo site_url('leaverequest/index/'.$this->loginData->staffid); ?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to make request.">Click</a></P></div>
              </div>
              <div class="col-md-2">
                <div class="count"> <p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/Recommendation_IconsLayer_6-512.png'); ?>"> </p>
                  <p class="count_top" style="font-weight: 600;">Process Alerts</p>
                  <P><a href="<?php echo site_url().'Staff_approval_process/';  ?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here, might be some approval pendings." >Click</a></P></div>
                </div>
                <div class="col-md-2">
                  <div class="count"> <p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/transfer-10-67552.png'); ?>"> </p>
                    <p class="count_top" style="font-weight: 600;">Transfer Request</p>
                    <P><a href="<?php echo site_url().'stafftransfer/add/'.$this->loginData->staffid; ?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to transfer request.">Click</a></P></div>
                  </div>
                  <div class="col-md-2">
                    <div class="count text-center"><div class="progress blue">
                      <span class="progress-left">
                        <span class="progress-bar"></span>
                      </span>
                      <span class="progress-right">
                        <span class="progress-bar"></span>
                      </span>
                      <div class="progress-value">10</div>
                    </div>
                    <p class="count_top" style="font-weight: 600;">Employees on Leave</p>
                    <P><a href="#" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to check employees." >Click</a></P></div>
                  </div>
                  <div class="col-md-2">
                    <div class="count text-center"><p style="margin-left: 35px; height:60px;width: 60px;background-color: #000; color: #fff;  font-size: 40px; font-family: 'Oxygen;'" class="rounded-circle"> <i class="fa fa-history black"> </i> </p>
                      <p class="count_top" style="font-weight: 600;">History</p>
                      <P>
                       <a href="<?php echo site_url().'Staff_personel_records/';?>" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click here to know history.">Click</a>

                     </div>
                   </div>
                   <div class="col-md-2">
                    <div class="count" style="color: #d4d4d4;"><p><img style="height: 60px;width: 60px;" class="rounded-circle" alt= "Leave" src="<?php echo site_url('common/backend/images/terminated-employee-662412.png'); ?>"> </p>
                      <p class="count_top" style="font-weight: 600;">Resignation</p>
                      <P><a data-toggle="tooltip" data-placement="bottom" title="Click here to make request." href="<?php echo site_url().'Staff_seperation/add/'.$this->loginData->staffid;?>" class="btn btn-light btn-sm">Click</a></P></div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="row" style="margin-top: 20px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #FBF859; padding: 20px;">
                 <?php 
                 if($this->loginData->RoleID !=17){  ?>
                  <div class="col-sm-4 py-2">
                    <div class="card text-white" style="background: #2A9FB8">
                      <div class="card-body">
                       <div id="calendar">
                       </div>
                     </div>
                   </div>
                 </div>
               <?php }else{ ?>
                <div class="col-sm-4 py-2">
                  <div class="box box-primary bg-white" style="min-height: 330px;">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="color:#17a2b8 !important; margin-bottom: 15px;" >Super Annuation</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="col-md-12" style="border-bottom: solid 1px #e1e1e1; padding-bottom:   5px; "  >

                        <?php 
                        if($superAnnuation){
                          foreach ($superAnnuation as $value) {
                            ?>
                            <div class="col-md-1">
                              <i class="fa fa-user" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-6 text-left">
                             <label><a href="<?php echo site_url('Staff_sepinformtionforsup/index/').'/'.$value->staffid; ?>"> <?php echo $value->name; ?> </a> </label>
                           </div>
                           <div class="col-md-4 text-right">
                            <span class="badge badge-dark" title="Retirement Date"><?php echo $this->gmodel->changedate($value->retiringdate); ?></span>
                          </div>
                          <div class="col-md-12 text-right">
                            <span class="">
                              Employee Designation - <?php echo $value->desname; ?>
                            </span>
                          </div>
                        <?php } } ?>
                      </div>

                      <!-- /.item -->


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">

                    </div>
                    <!-- /.box-footer -->
                  </div>
                </div>
              <?php }?>


              <div class="col-sm-4 py-2">
                <div class="card text-white bg-dafault" style="min-height: 333px;">
                  <div class="card-body"  style="background-image:url('./common/backend/images/plogo.jpg');  ">
                    <p class="text-center" style="font-size: 22px; font-weight: bold; padding: 10px; font-family: 'Oxygen'; color: #000; background-color: #fff;"><i class="fa fa-bell-o fa-lg " aria-hidden="true"></i> &nbsp;<span style="font-size: 22px; color: #000; text-transform: uppercase;">Policies </span><br>
                    </p>

                    <div>
                     <?php 
                     if($policylist){
                      foreach($policylist as $res){
                       ?>
                       <?php  $file_path=''; 
                       $file_path=FCPATH.'datafiles/policy/'. $res->docpath;

                       if(file_exists($file_path)) { ?>
                         <div class="alert alert-dark">

                          <a href="<?php echo 'datafiles/policy/'.$res->docpath; ?>" download="<?php  echo 'datafiles/policy/'.$res->docpath; ?>" style="margin-top:-5Px" class="btn btn-xs btn-success pull-right btn-sm">Download</a>
                          <strong>Policy:</strong> <?php echo $res->policy_name; ?>
                          
                        </div>
                      <?php }}} ?>

                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4 py-2">
                <div class="card text-white bg-info" style="height: 332px;">
                  <div class="card-body text-center cnt-block">
                   <figure>
                    <?php $ci =&get_instance(); $ci->load->model('stafffullinfo_model');    $candidatedetails= $ci->stafffullinfo_model->getCandidateDetailsPreview($this->loginData->staffid); //print_r($candidatedetails); die(); ?>

                    <?php if(!empty($candidatedetails->encryptedphotoname) && $candidatedetails->encryptedphotoname !='')  { ?>
                      <img class= "rounded-circle" src="<?php if(!empty($candidatedetails->encryptedphotoname)) echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Profile Picture" style="height: 120px; width: 120px;">
                    <?php }else{ ?>
                      <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Profile Picture" style="height: 120px; width: 120px;">
                    <?php } ?>
                  </figure>
                  <br>
                  <h3 style="font-family: 'Oxygen';"><b><?php  if(!empty($candidatedetails->candidatefirstname)) {
                   echo $candidatedetails->candidatefirstname .' '.$candidatedetails->candidatemiddlename.' '.$candidatedetails->candidatelastname;
                 }else{
                  if(!empty($candidatedetails->staff_name)) echo $candidatedetails->staff_name;
                }   ?></b></h3>
                <p class="title"><?php if(!empty($candidatedetails->desname)) echo $candidatedetails->desname;
                ?></p>
                <p><a href="<?php echo site_url().'Stafffullinfo/view/'.$this->loginData->staffid;?>" class="btn btn-light"data-toggle="tooltip" data-placement="bottom" title="Click here to check your complete profile.">Profile</a></p>
                <!--                        <p><a href="talha08.github.io" class="btn btn-light"data-toggle="tooltip" data-placement="bottom" title="Click here to check your complete profile.">Profile</a></p> -->
              </div>
            </div>
          </div>

        </div>
      </div>

      <!-- /.row -->
    </div>

  </div>
  <!-- #END# Exportable Table -->
</div>
</section>
<script type="text/javascript">
  $(document).ready(function(){


    $('#statedashboard').DataTable({
     "responsive": true

   }); 

    $("#financialyear").change(function(){

      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getbatch/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {

        console.log(data);
        $("#batch").html(data);

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    });


  });






</script>


