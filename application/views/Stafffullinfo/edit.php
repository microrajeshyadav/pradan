
<?php// print_r($all_tc);?>
<style>
textarea {
  resize: none;
}
thead>tr:hover
{
  border-bottom: solid 1px red;
}   

.bg-info1
{
  background-color: #F8F9FA !important;
}
.bg-info2
{
  background-color: #F8F9FA !important;
}
.bg-info3
{
  background-color: #F8F9FA !important;
}
.bg-info4
{
  background-color: #F8F9FA !important;
}
.bg-info5
{
  background-color: #F8F9FA !important;
}
.bg-info6
{
  background-color: #F8F9FA !important;
}
.bg-info7
{
  background-color: #F8F9FA !important;
}

label{

  font-weight: bold;
  font-family: 
}
.bg-lightgreen
{
  background-color: #85a4a5;
}
</style>
<script type="text/javascript">
  <?php if($this->loginData->RoleID == 3 ){ ?>
   $(document).ready(function(){
     $("#supervisorname").prop("disabled", true);

   });
   <?php }else {?>
    $(document).ready(function(){
     $("#supervisorname").prop("disabled", false);

   });
    <?php  } ?>
  </script>


  <style type="text/css" media="screen">
  .error{
    color: red;
  }  
</style>

<br>
<div class="container-fluid"  style="font-family: 'Oxygen' !important;" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Staff Edit Profile</h4>
       <div class="col-md-12 text-right" style="color: red">
        * Denotes Required Field 
      </div>

    </div>
    <hr class="colorgraph"><br>
  </div>

  <div class="panel-body">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg',$this->db->error());
    $er_msg= $this->session->flashdata('er_msg',$this->db->error());
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg',$this->db->error());?>. <b></div>
              </div>
            </div>
          </div>
        </div>
        <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg',$this->db->error());?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <?php ?>

          <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data" name="basicinfo">
           <input type="hidden" name="emailid" value="<?php echo $this->loginData->EmailID;?>">

           <div class="tab-content">    <!-- Start  Candidates Basic Info     -->

             <!-- <div id="home" class="tab-pane fade in active">-->

               <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <table id="tbltrainingexposure" class="table table-bordered">
                  <thead>
                   <tr>
                    <th colspan="7" class="bg-lightgreen text-white">Personal Infomation</th>
                  </tr>
                </thead>
                <tbody>

                  <input type="hidden" id="s_id" name="s_id" value="<?php echo $candidatedetails->staffid; ?>">

                  <tr>
                    <td colspan="3">

                      <?php 

                      $Stafffirstname='';
                      $Staffmiddlename='';
                      $Stafflastname='';

                      $arr=explode(" ",$candidatedetails->staff_name);

                      if(count($arr)==2)
                      {
                       $Stafffirstname = $arr[0];
                       $Stafflastname = $arr[1];
                     }else if (count($arr)==3) {
                      $Stafffirstname = $arr[0];
                      $Staffmiddlename = $arr[1];
                      $Stafflastname = $arr[2];
                    }else{
                      $Stafffirstname = $arr[0];

                    }

                    ?>

                    <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">First Name <span style="color: red;" >*</span></label>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control " value="<?php if(isset($Stafffirstname)){echo $Stafffirstname;} else { echo set_value('candidatefirstname');}?>" placeholder="Enter First Name"   required="required">

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">Middle name </label>
                      <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control" value="<?php if(isset($Staffmiddlename)){echo $Staffmiddlename;}else { echo set_value('candidatemiddlename');}?>" placeholder="Enter Middle Name" >

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <label for="Name">Last Name <span style="color: red;" >*</span></label>
                      <input type="text"  minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="<?php if(isset($Stafflastname)){echo $Stafflastname;} else { echo set_value('candidatelastname');}?>" required="required" >
                      <?php echo form_error("candidatelastname");?>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <?php 

                    $motherfirstname='';
                    $mothermiddlename='';
                    $motherlastname='';
                    $arr1=explode(" ",$candidatedetails->mother_name);

                    if(count($arr1)==2)
                    {
                     $motherfirstname = $arr1[0];
                     $motherlastname = $arr1[1];
                   }else if (count($arr1)==3) {
                    $motherfirstname = $arr1[0];
                    $mothermiddlename = $arr1[1];
                    $motherlastname = $arr1[2];
                  }else{
                    $motherfirstname = $arr1[0];

                  }

                  ?>

                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
                    <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control" value="<?php echo $motherfirstname;?>" placeholder="Enter First Name" required="required" >
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Middle name </label>
                    <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control" value="<?php echo $mothermiddlename;?>" placeholder="Enter Middle Name" >

                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label for="Name">Last Name <span style="color: red;" >*</span></label>
                    <input type="text" minlength="2" maxlength="50"  class="form-control" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="<?php echo $motherlastname;?>" required="required" >
                  </div>
                </div>
                <br>
                <div class="row">
                  <?php 
                  $fatherfirstname='';
                  $fathermiddlename='';
                  $fatherlastname='';
                  $arr2=explode(" ",$candidatedetails->father_name);
                  if(count($arr2)==2)
                  {
                   $fatherfirstname = $arr2[0];
                   $fatherlastname = $arr2[1];
                 }else if (count($arr2)==3) {
                  $fatherfirstname = $arr2[0];
                  $fathermiddlename = $arr2[1];
                  $fatherlastname = $arr2[2];
                }else{
                  $fatherfirstname = $arr2[0];

                }
                ?>



                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Father's Name <span style="color: red;" >*</span></label>
                  <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control" value="<?php echo $fatherfirstname;?>" placeholder="Enter First Name" required="required" >
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Middle name </label>
                  <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control" value="<?php echo $fathermiddlename;?>" placeholder="Enter Middle Name" >
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label for="Name">Last Name <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="<?php echo $fatherlastname;?>" required="required" >
                </div>
              </div>
              <br>

              <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Gender <span style="color: red;" >*</span></label>
                <?php 
                $options = array("1" => "Male", "2" => "Female", "3" => "Others");
                echo form_dropdown('gender', $options, $candidatedetails->gender, 'class="form-control" id="gender" ');
                ?>
                <?php echo form_error("gender");?>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Nationality  </label>
                <?php 
                $options = array("1" => "Indian", "2" => "Other");
                echo form_dropdown('nationality', $options, $candidatedetails->nationality, 'class="form-control" id="nationality" "');
                ?>
                <?php echo form_error("nationality");?>
              </div>


              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Marital Status </label>
                <?php 
                $options = array("1" => "Single", "2" => "Married", "3" => "Divorced", "4" => "Widow", "5" => "Separated");
                echo form_dropdown('maritalstatus', $options, $candidatedetails->marital_status, 'class="form-control" id="maritalstatus" "');
                ?>
                <?php echo form_error("maritalstatus");?>
              </div>
            </div>
            <br>

            <div class="row">
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <label for="dateofbirth">Date Of Birth <span style="color: red;" >*</span></label>
              <!--  -->
              <input type="text" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" name="dateofbirth" id="dateofbirth"  placeholder="Enter Date Of Birth " value="<?php echo $this->model->changedate($candidatedetails->dob);?>" required="required" >
              <?php echo form_error("dateofbirth");?>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <label for="Name">Email Id <span style="color: red;" >*</span></label>
              <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo $candidatedetails->emailid;?>" required="required" >
              <?php echo form_error("emailid");?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
              <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="<?php echo $candidatedetails->contact;?>" placeholder="Enter Mobile No" required="required" >
              <?php echo form_error("mobile");?>
            </div>
          </div>
          <br>
          <?php if($candidatedetails->candidateid!=null)
          {
            ?>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Annual income of parents <span style="color: red;" >*</span></label>
                <select name="annual_income" id="annual_income" required="required" class="form-control">
                  <option value="">Select</option>
                  <option value="1" <?php if ($candidatedetails->annual_income==1) {
                   echo "SELECTED";
                 } ?> >Below 50,000</option>
                 <option value="2" <?php   if ($candidatedetails->annual_income==2) {
                  echo "SELECTED";
                }  ?>>50,001-200,000</option>
                <option value="3" <?php if ($candidatedetails->annual_income==3) {
                  echo "SELECTED";
                } ?> >200,001-500,000</option>
                <option value="4" <?php if ($candidatedetails->annual_income==4) {
                 echo "SELECTED";
               } ?>>Above 500,000</option>
             </select>
             <?php echo form_error("annual_income");?>
           </div>

           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <label for="Name">No. of male sibling  <span style="color: red;" >*</span></label>
            <input type="text"  maxlength="1" data-toggle="tooltip" title="Male sibling!" name="no_of_male_sibling" id="no_of_male_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo $candidatedetails->male_sibling; ?>" placeholder="Enter sibling" required="required">
            <?php echo form_error("no_of_male_sibling");?>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <label for="Name">No. of female sibling  <span style="color: red;" >*</span></label>
            <input type="text"  maxlength="1" data-toggle="tooltip" title="Female sibling!" name="no_of_female_sibling" id="no_of_female_sibling" class="form-control txtNumeric" data-country="India" value="<?php echo $candidatedetails->female_sibling; ?>" placeholder="Enter sibling " required="required">
            <?php echo form_error("no_of_female_sibling");?>
          </div>
        </div>
        <?php } ?>
        <br>
        <div class="row">
         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <label for="Name">Blood Group<span style="color: red;" >*</span></label>

          <?php
          $options = array("1" => "O+", "2" => "B+", "3" => "AB+", "4" => "O-", "5" => "B-", "6" => "AB-");
          echo form_dropdown('bloodgroup', $options, $candidatedetails->bloodgroup, 'class="form-control" id="bloodgroup" "');
          ?>
          <?php echo form_error("bloodgroup");?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <input type="hidden" name="supervisorname" value="<?php  echo $candidatedetails->reportingto; ?>">
          <label for="Name">Supervisor Name<span style="color: red;" >*</span></label>
          <select name="new_supervisorname" id="supervisorname"  class="form-control" data-toggle="tooltip" title="Select District">
            <option value="">Select supervisor Name </option>
            <?php 
            foreach ($all_tc as $value) {
              ?>
              <option value="<?php echo $value->staffid; ?>" <?php if($candidatedetails->reportingto==$value->staffid){?> SELECTED <?php } else {echo $value->staffid;}?>><?php echo $value->name; ?></option>
              <?php 
            }?>

          </select>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <label for="Name">Office Name</label> 
          <div class="form-group">

            <div class="input-group col-xs-12">
              <?php
              echo $candidatedetails->officename; ?>
            </div>
          </div> 
        </div>
      </div>
      <br>
      <div class="row bg-light" style="padding: 10px;">
        <?php        
        if ($candidatedetails->encryptedphotoname !='') { ?>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label for="Name">Photo</label>
          <div class="custom-file">
            <div class="input-group col-xs-12">
             <input type="file" name="photoupload" id="photoupload" class="file" accept="image/*" class="file">
           </div>
         </div>
       </div>
       <?php }else{ ?>
       <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label for="Name">Photo </label>
        <div class="form-group">

          <div class="input-group col-xs-12">
            <input type="file" name="photoupload" id="photoupload" accept="image/*" class="file" required="required">
          </div>
          
        </div>
      </div>
      <?php } ?>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">

       <?php if ($candidatedetails->encryptedphotoname !='') { ?>
       <input type="hidden" name="oldphotoupload" id="oldphotoupload" value="<?php echo $candidatedetails->encryptedphotoname;?>" >
       <img src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" class="rounded-circle" width="60" height="60" title="" data-toggle="tooltip" alt="" >
       <?php }  else {?>
       <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" width="60" height="60" title="" data-toggle="tooltip" class="rounded-circle" alt="" >
       <?php } ?>
     </div>
     <?php echo form_error("photoupload");?>

     <?php 

     if ($candidatedetails->encrypted_signature !='') { ?>
     <input type="hidden" name="oldphotoupload" id="oldphotoupload" value="<?php if(!empty($candidatedetails->encryptedphotoname)) echo $candidatedetails->encryptedphotoname;?>" >
     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
      <label for="Name">Signature</label>
      <div class="custom-file">
        <div class="input-group col-xs-12">
         <input type="file" name="signature_upload" id="signature_upload" class="file" accept="image/*" class="file">
       </div>
     </div> </div>
     <?php }else{ ?>
     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
      <label for="Name">Signature </label>
      <div class="form-group">

        <div class="input-group col-xs-12">
          <input type="file" name="signature_upload" id="signature_upload" accept="image/*" class="file" required="required">
        </div>

      </div>
      <?php } ?>
      <?php echo form_error("signature_upload");?>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">

     <?php if ($candidatedetails->encrypted_signature !='') { ?>
     <input type="hidden" name="old_signature_upload" id="old_signature_upload" value="<?php echo $candidatedetails->encrypted_signature ;?>" >
     <img src="<?php echo site_url().'datafiles/signature/'.$candidatedetails->encrypted_signature ;?>" class="" width="100" height="46" title="" data-toggle="tooltip" alt="" >
     <?php }  else {?>
     <img src="<?php echo site_url().'datafiles/signature.png';?>" width="100" height="46" title="" data-toggle="tooltip" class="" alt="" >
     <?php } ?>
   </div>

   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label for="Name">Date of Joining :</label>
          <div class="custom-file">
            <div class="input-group col-xs-12">
             <input type="text" data-toggle="tooltip" title="Date of Joining!"  name="doj" id="doj" class="form-control" value="<?php if(!empty($candidatedetails->doj)) echo $this->model->changedatedbformate($candidatedetails->doj); ?>" placeholder="Enter Date of Joining" readonly>
           </div>
         </div>
       </div>


 </div>

 <div class="row">



  <div class="row">

  </td>
</tr>
</tbody>
</table>
</div>
</div>

<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <table id="tbledubackground" class="table table-bordered" >
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> 
       Communication Address 
     </th>
   </tr> 
   <tr>
    <th class="text-center bg-light" colspan="4" style="background-color: #e1e1e1; vertical-align: top; text-align: center; "> <i class="fa fa-address-card" style="color: #F29005;"></i> &nbsp;Present Mailing Address</th>
    <th class="text-center" colspan="4" style="background-color: #d0d0d0; vertical-align: top; text-align: center;"> <i class="fa fa-address-card" style="color: #17A2B8;"></i> &nbsp;Permanent Mailing Address<br>

      <div>
        <!-- <input type="checkbox" class="form-check-input" id="filladdress"> 
          <label class="form-check-label" for="filladdress"><b>same as Mailing Address</b></label> -->
        </div>
      </th>
    </tr> 
  </thead>
  <tbody>
   <tr>
    <td class="bg-light"><label for="Name">H.No<span style="color: red;" >*</span></label></td>
    <td class="bg-light"><input type="text" name="presenthno" id="presenthno" maxlength="50"class="form-control" 
      value="<?php echo $candidatedetails->presenthno;?>" placeholder="Enter H.No" required="required"></td>
      <td class="bg-light"><label for="Name">Street<span style="color: red;" >*</span></label></td>
      <td class="bg-light"><textarea name="presentstreet" id="presentstreet" maxlength="150" class="form-control" 
        placeholder="Enter H.No/Street" required="required"><?php echo $candidatedetails->presentstreet;?></textarea></td>
        <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanenthno" id="permanenthno"  maxlength="50" class="form-control" value="<?php echo $candidatedetails->permanenthno;?>"  placeholder="Enter H.No/Street" required="required" ></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><textarea name="permanentstreet" id="permanentstreet" maxlength="150" class="form-control"   placeholder="Enter H.No/Street" required="required"><?php echo $candidatedetails->permanentstreet;?></textarea></td>
      </tr>
      <tr>
        <td class="bg-light"><label for="Name">City<span style="color: red;" >*</span></label> </td>
        <td class="bg-light"><input type="text" name="presentcity" id="presentcity" maxlength="50" class="form-control txtOnly" placeholder="Enter City" value="<?php echo $candidatedetails->presentcity;?>"  required="required"></td>

        <td class="bg-light"> <label for="Name">State<span style="color: red;" >*</span></label> </td>
        <td><?php 
        $options = array('' => 'Select Present State');
        $selected = $candidatedetails->presentstateid;
        foreach($statedetails as$key => $value) {
          $options[$value->statecode] = $value->name;
        }
        echo form_dropdown('presentstateid', $options, $selected, 'class="form-control" id="presentstateid" data-toggle="tooltip" title=" Select State !" ');
        ?>
        <?php echo form_error("presentstateid");?>
      </td>
      <td><label for="Name">City<span style="color: red;" >*</span></label></td>
      <td><input type="text" name="permanentcity" id="permanentcity" maxlength="50" placeholder="Enter City" class="form-control txtOnly" value="<?php echo $candidatedetails->permanentcity;?>" required="required" ></td>

      <td ><label for="Name">State<span style="color: red;" >*</span></td>
        <td> <?php 
        $options = array('' => 'Select Permanent State');
        $selected = $candidatedetails->permanentstateid;
        foreach($statedetails as$key => $value) {
          $options[$value->statecode] = $value->name;
        }
        echo form_dropdown('permanentstateid', $options, $selected, 'class="form-control" id="permanentstateid" data-toggle="tooltip" title=" Select State !" ');
        ?>

        <?php echo form_error("permanentstateid");?></td>
      </tr>
      <tr>
        <td class="bg-light"><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td class="bg-light">   <select name="presentdistrict" id="presentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
         <option value="">Select District </option>
         <?php foreach ($getdistrict as $key => $value) {
          if ( $value->districtid==$candidatedetails->presentdistrict) {
           ?>
           <option value="<?php echo $value->districtid; ?>" selected><?php echo $value->name; ?></option>
           <?php }else{ ?>
           <option value="<?php echo $value->districtid; ?>"><?php echo $value->name; ?></option>
           <?php } }?>

         </select></td>
         <td class="bg-light"><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
         <td class="bg-light"><input type="text" name="presentpincode" maxlength="6" id="presentpincode" class="form-control txtNumeric" value="<?php echo $candidatedetails->presentpincode; ?>" required="required" placeholder="Enter PinCode"></td>
         <td><label for="Name">District<span style="color: red;" >*</span></label></td>
         <td><select name="permanentdistrict" id="permanentdistrict" required="required" class="form-control" data-toggle="tooltip" title=" Select District !" >
           <?php foreach ($getdistrict as $key => $value) {
            if ( $value->districtid==$candidatedetails->permanentdistrict) {
             ?>
             <option value="<?php echo $value->districtid; ?>" selected><?php echo $value->name; ?></option>
             <?php }else{ ?>
             <option value="<?php echo $value->districtid; ?>"><?php echo $value->name; ?></option>
             <?php } }?>

           </select></td>

           <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
           <td><input type="text" name="permanentpincode"  maxlength="6" id="permanentpincode" class="form-control txtNumeric" value="<?php echo $candidatedetails->permanentpincode; ?>" required="required" placeholder="Enter PinCode"></td>
         </tr>
       </tbody>
     </table>
   </div>  

   <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">
      <table id="tblForm29" class="table table-bordered">
        <thead>
          <tr class="bg-lightgreen text-white">
            <th colspan="8">
              <div class="col-lg-12">
                <div class="col-lg-6">Educational Background</div>
                <div class="col-lg-6 text-right ">
                 <button type="button" id="btnEducationRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                 <button type="button" id="btnEducationAddRow" class="btn btn-warning btn-xs">Add</button>
               </div>

             </div>  
           </th>
         </tr> 
         <tr>
          <th class="text-center" style="vertical-align: top;">Course <span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;"> Board/ University <span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;">School/ College/ Institute <span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;">Place <span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;">Year <span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;">Specialisation<span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;">Percentage(%)<span style="color: red;">*</span></th>
          <th class="text-center" style="vertical-align: top;">Certificate upload <span style="color: red;">*</span></th>
        </tr> 
      </thead>
      <tbody id="tbodyForm29">
       <input type="hidden" name="education_count" id="education_count" value="<?php echo $count_education->Ecount2;?>">
       <?php if($educationcount->Ecount2==0)
       {
        ?>
        <tr id="tbodyForm29" >
          <input type="hidden" name="sr_no[]" value="0">
          <td>
            <select name="education_level[]" id="" class="form-control" data-toggle="tooltip" title="Select Education Name!" required="required"  >
              <option value=""> Select Cources</option>
              <?php foreach ($edu_level  as $value) { ?>
              <option value="<?php echo $value->edulevel_cd;?>"><?php echo $value->level_name;?></option>
              <?php  } ?>
            </select>
            <?php echo form_error("education_level");?>
          </td>

          <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
            id="boarduniversity"   name="boarduniversity[]"  placeholder="Enter Board/ University" value="<?php echo set_value('boarduniversity');?>"   >
            <?php echo form_error("boarduniversity");?></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="schoolcollegeinstitute"   name="schoolcollegeinstitute[]"  placeholder="Enter School/ College/ Institute" value="<?php echo set_value('schoolcollegeinstitute');?>"  >
              <?php echo form_error("schoolcollegeinstitute");?></td>

              <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                id="place"    name="place[]"     placeholder="Enter Place" value="" >
                <?php echo form_error("place");?></td>
                <td><input type="text" class="form-control" data-toggle="tooltip" title="Year !" id="passingyear"  name="passingyear[]" placeholder="Enter Year" style="min-width: 5px; max-width: 100px;" value="<?php echo set_value('passingyear');?>"  >
                  <?php echo form_error("passingyear");?></td>
                  <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                    id="specialisation"  name="specialisation[]"  placeholder="Enter Specialisation" value="<?php echo set_value('specialisation');?>"  >
                    <?php echo form_error("specialisation");?></td>
                    <td><input type="text" class="form-control txtNumeric"  data-toggle="tooltip" title="Percentage !" style="min-width: 5px; max-width: 70px;"
                      id="percentage"  name="percentage[]" placeholder="Percentage" value=""  >
                      <?php echo form_error("percentage");?></td> 
                      <td>
                        <div class="col-lg-9">
                         <div class="form-group">
                          <input type="file"  class="file"  name="certificate[]" id="certificate"  >

                        </div>
                      </div>
                    </td>
                  </tr>

                  <?php
                }
                else {
                  ?>
                  <?php 
            // echo $educationcount->Ecount2;
            // die;
           // for($i=0;$i<$educationcount->Ecount2;$i++)
                  $i=0;
                  foreach($staffeducationdetail as $val)
                  {
                   ?>
                   <input type="hidden" name="sr_no[]" value="<?php echo $val->id;?>">
                   <tr>

                    <td>

                     <?php 
                     $options = array('' => 'Select Cources');
                     foreach($edu_level  as $value) {
                      $options[$value->edulevel_cd] = $value->level_name;
                    }
                    echo form_dropdown('education_level[]', $options, $val->edulevel_cd, 'class="form-control"  id="education_level_'.$i.'"  data-toggle="tooltip" title="Select Identity Name!" required="required"  ');
                    ?>
                    <?php echo form_error("education_level");?>

                  </td>

                  <td><input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
                    id="boarduniversity"   name="boarduniversity[]"  placeholder="Enter Board/ University" value="<?php echo $val->university;?>"   >
                    <?php echo form_error("boarduniversity");?></td>
                    <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
                      id="schoolcollegeinstitute"   name="schoolcollegeinstitute[]"    placeholder="Enter School/ College/ Institute" value="<?php echo $val->collg_name;?>"  >
                      <?php echo form_error("schoolcollegeinstitute");?></td>
                      <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
                        id="place"    name="place[]"     placeholder="Enter Place" value="<?php echo $val->place;?>" >
                        <?php echo form_error("place");?></td>
                        <td><input type="text" class="form-control" data-toggle="tooltip" title="Year !" id="passingyear"  name="passingyear[]" placeholder="Enter Year" style="min-width: 5px; max-width: 100px;" value="<?php echo $val->year_passing;?>"  >
                          <?php echo form_error("passingyear");?></td>


                          <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
                            id="specialisation"  name="specialisation[]"  placeholder="Enter Specialisation" value="<?php echo $val->stream;?>"  >
                            <?php echo form_error("specialisation");?></td>


                            <td><input type="text" class="form-control txtNumeric"  data-toggle="tooltip" title="Percentage !" style="min-width: 5px; max-width: 70px;"
                              id="percentage"  name="percentage[]" placeholder="Percentage" value="<?php echo $val->percentage;?>"  >
                              <?php echo form_error("percentage");?></td> 
                              <td>
                                <?php if (!empty($staffeducationdetail[$i]->encryptedcertificatename)) { ?>

                                <div class="col-lg-8">
                                  <input type="file" name="certificate[]"  id="certificate_<?php echo $i;?>" class="file" 
                                  >

                                </div>
                                <div class="col-lg-1">
                                  <input type="hidden"  name="oldmatriccertificate[]" id="originalmatriccertificate" value="<?php echo $val->originalcertificate; ?>">
                                  <input type="hidden"  name="oldmatriccertificate[]" id="oldmatriccertificate" value="<?php echo $val->encryptedcertificatename; ?>">
                                  <a  href="<?php echo site_url().'datafiles\educationalcertificate/'.
                                  $val->encryptedcertificatename;?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
                                </div>
                                <?php }else{ ?>

                                <div class="col-lg-8">
                                  <div class="form-group">
                                   <input type="file" name="certificate[]" id="certificate_<?php echo $i; ?>"  class="file" maxlength="15">

                                 </div>
                               </div>
                               <div class="col-lg-1"></div>
                               <?php } ?>
                             </td>
                           </tr>
                           <?php } } ?>
                         </tbody>
                       </table>
                     </div>
                   </div>

                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                     <table id="tblForm09" class="table table-bordered table-striped" >
                      <thead>
                       <tr col-lg-6 text-right >
                        <th colspan="4" class="bg-lightgreen text-white">
                          <div class="col-lg-12">
                            <div class="col-lg-6">Family Members</div>

                            <div class="col-lg-6 text-right ">
                             <button type="button" id="btnRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                             <button type="button" id="btnAddRow" class="btn btn-warning btn-xs">Add</button>
                           </div>

                         </div>  </th>
                       </tr> 
                       <tr>
                         <th>Name<span style="color: red;">*</span></th>
                         <th>Relation with Employee <span style="color: red;">*</span></th>
                         <th>Date of Birth <span style="color: red;">*</span></th>
                         <th>Photo & identity proof document upload <span style="color: red;">*</span></th>
                       </tr>
                     </thead>
                     <tbody id="tbodyForm09" >

                      <input type="hidden" name="familymembercount" id="familymembercount" value="<?php echo $familycount->Fcount;?>">
                      <?php if ($familycount->Fcount == 0){ ?> 
                      <tr id="tbodyForm09" >

                        <td>
                          <input type="text" name="familymembername[]" id="familymembername" value="" class="form-control" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" required="required" ></td>
                          <td>

                            <select class="form-control" name="relationwithenployee[]" id="relationwithenployee" required="required">
                              <option value="">Select Relation</option>
                              <?php foreach ($sysrelations as $key => $value): ?>
                                <option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>
                              <?php endforeach ?>
                            </select>

                            <?php echo form_error("relationwithenployee");?>
                          </td>
                          <td>
                            <input type="text" name="familymemberdob[]" id="familymemberdob" data-toggle="tooltip" title="Enter Family Member DOB !" value="" class="form-control datepicker" required="required" >
                          </td>
                          <td>
                           <div class="col-lg-12">
                            <div class="col-lg-6"><b>Photo : </b> </div>
                            <div class="col-lg-6 text-left"><input type="file" name="familymemberphoto[]" id="familymemberphoto" accept="image/*" required="required"></div>
                          </div>
                          <br>
                          <div class="col-lg-12">
                           <div class="col-lg-6"><b>Identity proof document :</b></div>
                           <div class="col-lg-6 text-left">
                            <br>
                            <input type="file" name="identity_familymemberphoto[]" id="identity_familymemberphoto" accept="image/*" required="required">
                          </div>
                        </div>
                      </td>
                    </tr>
                    <?php }else
                    { 
                      $i=0;

                      foreach ($familymemberdetails as $key => $val) { 
                       ?>
                       <tr id="tbodyForm09">
                         <td><input type="text" name="familymembername[<?php echo $i; ?>]" id="familymembername_<?php echo $i; ?>" maxlength="50" value="<?php echo $val->Familymembername ?>" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Family Member Name" required="required"  ></td>
                         <td>
                          <select class="form-control" id="relationwithenployee_<?php echo $i; ?>" name="relationwithenployee[<?php echo $i; ?>]" required="required">
                            <option value="">Select Relation</option>
                            <?php foreach ($sysrelations as $key => $value){ 
                              if ($value->id==$val->relationwithemployee) {
                                ?>
                                <option value="<?php echo $value->id;?>" SELECTED><?php echo $value->relationname;?></option>
                                <?php }else{ ?>
                                <option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>
                                <?php } }  ?>
                              </select>

                              <?php echo form_error("relationwithenployee");?>
                            </td>
                            <td><input type="text" name="familymemberdob[<?php echo $i; ?>]" id="familymemberdob_<?php echo $i;?>" data-toggle="tooltip" title="Enter Family Member DOB !" value="<?php if(isset($val->familydob)){ echo $this->model->changedate($val->familydob);}?>" class="form-control datepicker" required="required"  ></td>
                            <td>
                             <div class="col-lg-9">
                              <div class="form-group">
                                <div class="input-group col-xs-12">
                                  <div class="col-lg-4"><b>Photo : </b> </div>
                                  <input type="file" name="familymemberphoto[<?php echo $i;?>]"  id="familymemberphoto" class="file" 
                                  accept=" image/*">

                                </div>
                                <div class="input-group col-xs-12">
                                  <div class="col-lg-4"><b>Identity Proof: </b> </div>
                                  <input type="file" name="identity_familymemberphoto[<?php echo $i;?>]"  id="identity_familymemberphoto" class="file" 
                                  accept=" image/*">
                                </div>
                              </div>
                            </div>

                            <div class="col-lg-3">
                              <?php if (!empty($val->encryptedphotoname)) { ?>
                              <div class="form-group">
                               <input type="hidden" name="originalfamilymemberphoto[]" id="originalfamilymemberphoto" value="<?php echo $val->originalphotoname; ?>">
                               <input type="hidden" name="oldfamilymemberphoto[]" id="oldfamilymemberphoto" value="<?php echo $val->encryptedphotoname; ?>">
                               <a  href="<?php echo site_url().'datafiles/familymemberphoto/'.
                               $val->encryptedphotoname; ?>" download class="btn-primary">
                               <i class="fa fa-download" aria-hidden="true"></i>
                             </a>
                           </div>
                           <?php } ?>
                         </div>
                         <div class="col-lg-3">
                          <?php if (!empty($val->encryptedfamilyidentityphotoname)) { ?>
                          <div class="form-group">
                           <input type="hidden" name="originalidentityfamilymemberphoto[]" id="originalidentityfamilymemberphoto" value="<?php echo $val->originalfamilyidentityphotoname; ?>">
                           <input type="hidden" name="oldidentityfamilymemberphoto[]" id="oldidentityfamilymemberphoto" value="<?php echo $val->encryptedfamilyidentityphotoname; ?>">
                           <a  href="<?php echo site_url().'datafiles/familymemberphoto/'.
                           $val->encryptedfamilyidentityphotoname; ?>" download class="btn-primary">
                           <i class="fa fa-download" aria-hidden="true"></i>
                         </a>
                       </div>
                       <?php } ?>
                     </div>
                   </td>
                 </tr>
                 <?php $i++;  } } ?>
               </tbody>
             </table>
           </div>


           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

             <table id="tblForm13" class="table table-bordered" >
              <thead>
               <tr class="bg-lightgreen text-white">
                <th colspan="4">
                  <div class="col-lg-12">
                    <div class="col-lg-6">Identity & Other Detail(PAN Card And Aadhar Card must mandatory)</div>

                    <div class="col-lg-6 text-right ">
                     <button type="button" id="btnidentitydetailRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                     <button type="button" id="btnidentitydetailAddRow" class="btn btn-warning btn-xs">Add</button>
                   </div>

                 </div>  </th>
               </tr> 
               <tr>
                 <th>Identity <span style="color: red;">*</span></th>
                 <th>Number <span style="color: red;">*</span></th>
                 <th>document upload <span style="color: red;">*</span></th>
               </tr>
             </thead>
             <tbody id="tbodyForm13" >
              <input type="hidden" name="identcount" id="identcount" value="<?php echo $identitycount->Icount;?>">
              <?php if ($identitycount->Icount==0) { ?>
              <tr id="tbodyForm13" >
                <td>
                  <select name="identityname[]" class="form-control" data-toggle="tooltip" title="Select Identity Name!" required="required" >
                    <option value="">Select Identity</option>
                    <?php
                    foreach($sysidentity as$key => $value) {
                      ?>
                      <option value="<?php echo $value->id;?>" ><?php echo $value->name; ?></option>
                      <?php } ?>

                    </select>

                  </td>

                  <td><input type="text" name="identitynumber[]" id="identitynumber" value="" class="form-control" maxlength="30"  data-toggle="tooltip" title="Enter Identity Number!" placeholder="Enter Identity Number " required="required"></td>
                  <td>
                   <div class="form-group">
                    <div class="input-group col-xs-12">
                      <input type="file" name="identityphoto[]"  id="identityphoto" class="file" accept="image/*" required="required">

                    </div>
                  </div>

                </td>
              </tr>
              <?php }else{ $i=0;
                foreach ($identitydetals as $key => $val) {   ?>
                <input type="hidden" name="id[]" value="<?php echo $val->id;?>">

                <tr id="tbodyForm13">
                  <td>
                    <?php 
                    $options = array('' => 'Select Identity');
                    foreach($sysidentity as$key => $value) {
                      $options[$value->id] = $value->name;
                    }
                    echo form_dropdown('identityname['.$i.']', $options, $val->identityname, 'class="form-control"  id="identityname_'.$i.'"  data-toggle="tooltip" 
                      title="Select Identity Name!" required="required"');
                      ?>
                      <?php echo form_error("identityname");?>
                    </td>

                    <td><input type="text" name="identitynumber[<?php echo $i; ?>]" id="identitynumber_<?php echo $i; ?>" maxlength="30" value="<?php echo $val->identitynumber;?>" class="form-control" required="required"   data-toggle="tooltip" title="Enter Identity Number!" placeholder="Enter Identity Number"></td>
                    <td>
                      <div class="col-lg-9">
                        <div class="form-group">
                          <div class="input-group col-xs-12">
                            <input type="file" name="identityphoto[<?php echo $i;?>]"  id="identityphoto"  class="file">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <?php if (!empty($val->encryptedphotoname)) { ?>
                        <div class="form-group">
                          <input type="hidden" name="originalidentityphoto[]" id="originalidentityphoto" value="<?php echo $val->originalphotoname;?>">
                          <input type="hidden" name="oldidentityphoto[]" id="oldidentityphoto" value="<?php echo $val->encryptedphotoname;?>">
                          <a href="<?php echo site_url().'datafiles/identitydocuments/'.$val->encryptedphotoname; ?>" download class="btn-primary">
                            <i class="fa fa-download" aria-hidden="true"></i>
                          </a>
                        </div>
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                  <?php $i++; } } ?>
                </tbody>

              </table>
            </div>





            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

             <table id="tblForm15" class="table table-bordered">
              <thead>
               <tr class="bg-lightgreen text-white">
                <th colspan="3" >

                  <div class="col-lg-6 text-left"> Gap Year </div>

                  <div class="col-lg-6 text-right ">
                   <button type="button" id="btnGapReasonRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                   <button type="button" id="btnGapReasonAddRow" class="btn btn-warning btn-xs">Add</button>
                 </div>

               </th>
             </tr> 
             <tr>
              <th class="text-center" style="vertical-align: top;"> From Date</th>
              <th class="text-center" style="vertical-align: top;">To Date</th>
              <th>Reason</th>
            </tr> 
          </thead>
          <tbody id="bodytblForm15">
            <input type="hidden" name="TGapreason" id="TGapreason" value="<?php echo $TrainingExpcount->TEcount; ?>">
            <?php
                                      // echo $TrainingExpcount->TEcount;
            if ($GapYearCount->GYcount==0) { ?>
            <tr id="bodytblForm15">

             <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
              id="gapfromdate" name="gapfromdate[]" placeholder="From Date" style="min-width: 20%;"  value=""  ></td>
              <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                id="gaptodate" name="gaptodate[]" placeholder="To Date" style="min-width: 20%;" value=""></td>
                <td>
                  <textarea type="text" value="" rows="2" cols="5" name="gapreason[]" id="gapreason" class="form-control" data-toggle="tooltip"  maxlength="250" title="Gap Reason"   placeholder="Enter gap reason" ></textarea>
                </td>
              </tr>
              <?php } else{ $i=0;
                foreach ($gapyeardetals as $key => $val) { 
                 ?>
                 <tr id="bodytblForm15">

                   <input type="hidden" name="gap_id[]" value="<?php echo $val->id;?>">  
                   <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" id="gapfromdate_<?php echo $i;?>" name="gapfromdate[<?php echo $i;?>]" placeholder="From Date" onclick="return changedatepicker(<?php echo $i;?>)"  style="min-width: 20%;"  value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" required="required" ></td>
                   <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" id="gaptodate_<?php echo $i;?>" name="gaptodate[<?php echo $i;?>]" placeholder="To Date" style="min-width: 20%;" required="required" onclick="return changedatepicker(<?php echo $i;?>)" value="<?php echo $this->model->changedatedbformate($val->todate);?>" ></td>
                   <td><textarea class="form-control" data-toggle="tooltip"  maxlength="250" name="gapreason[<?php echo $i;?>]" id="gapreason_[<?php echo $i;?>]" title="Gap Reason" placeholder="Enter gap reason"  value="" rows="2" cols="5" required="required" ><?php echo $val->reason;?></textarea>

                   </td>
                 </tr>
                 <?php $i++;
               } 
             } ?>   
           </tbody>
         </table>
       </div>




       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
         <?php //print_r($TrainingExpcount);  ?>
         <table id="tblForm10" class="table table-bordered">
          <thead>
           <tr class="bg-lightgreen text-white">
            <th colspan="8" >

             <div class="col-lg-12">
              <div class="col-lg-6 text-left"> Training  Exposure (if any)</div>

              <div class="col-lg-6 text-right ">
               <button type="button" id="btntrainingexposureRemoveRow" class="btn btn-danger btn-xs">Remove</button>
               <!--  <span><a href="javascript:void(0);" class="btn btn-warning btn-xs add" >Add </a></span> -->
               <button type="button" id="btntrainingexposureAddRow" class="btn btn-warning btn-xs">Add</button>
             </div>

           </div>

         </th>
       </tr> 
       <tr>

        <th class="text-center" style="vertical-align: top;">Nature of Training <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;">Organizing Agency <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;"> From Date <span style="color: red;">*</span></th>
        <th class="text-center" style="vertical-align: top;">To Date <span style="color: red;">*</span></th>
      </tr> 
    </thead>
    <tbody id="bodytblForm10">
      <input type="hidden" name="TEcount" id="count_te" value="<?php echo $TrainingExpcount->TEcount; ?>">
      <?php
                                      // echo $TrainingExpcount->TEcount;
      if ($TrainingExpcount->TEcount==0) { ?>
      <tr id="bodytblForm10">

        <td><input type="text" class="form-control " maxlength="150" data-toggle="tooltip" title="Nature of Training !" minlength="5"   maxlength="50"
          id="natureoftraining"  maxlength="150" name="natureoftraining[]" placeholder="Enter Nature of Training" style="min-width: 20%;"  value="" ></td>
          <td> <input type="text" class="form-control " data-toggle="tooltip" minlength="5"   maxlength="150" title="Organizing Agency !" 
            id="organizingagency" name="organizingagency[]"  placeholder="Organizing Agency" style="min-width: 20%;"  value="" ></td>
            <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
              id="fromdate" name="fromdate[]" placeholder="From Date" style="min-width: 20%;"  value=""  ></td>
              <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" 
                id="todate" name="todate[]" placeholder="To Date" style="min-width: 20%;"  value="" ></td>
              </tr>
              <?php } else{ $i=0;

                foreach ($trainingexposuredetals as $key => $val) {  ?>
                <input type="hidden" name="t_id[]" value="<?php echo $val->id;?>">
                <tr id="bodytblForm10">

                  <td><input type="text" class="form-control inputclass" data-toggle="tooltip" title="Nature of Training !" minlength="5"   maxlength="150" required="required"
                    id="natureoftraining" name="natureoftraining[<?php echo $i;?>]" placeholder="Enter Nature of Training"  maxlength="150"style="min-width: 20%;"  
                    value="<?php echo $val->Training_program;?>" ></td>
                    <td> <input type="text" class="form-control" data-toggle="tooltip" minlength="5"   maxlength="50" title="Organizing Agency !" 
                      id="organizingagency" name="organizingagency[<?php echo $i;?>]" placeholder="Organizing Agency" style="min-width: 20%;"  value="<?php echo $val->organizing_agency;?>" required="required" ></td>
                      <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="From Date !" 
                        id="fromdate_<?php echo $i;?>" name="fromdate[<?php echo $i;?>]" placeholder="From Date" onclick="return changedatepicker(<?php echo $i;?>)"  style="min-width: 20%;"  
                        value="<?php echo $this->model->changedatedbformate($val->fromdate);?>" required="required" ></td>
                        <td><input type="text" class="form-control datepicker" data-toggle="tooltip"  title="To Date !" id="todate_<?php echo $i;?>" name="todate[<?php echo $i;?>]" placeholder="To Date" style="min-width: 20%;" required="required" onclick="return changedatepicker(<?php echo $i;?>)" value="<?php echo $this->model->changedatedbformate($val->todate);?>" ></td>
                      </tr>
                      <?php $i++; } } ?>   
                    </tbody>
                  </table>
                </div>





                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

                 <table id="tblForm11" class="table table-bordered">
                  <thead>
                   <tr class="bg-lightgreen text-white">
                    <th colspan="8" > 

                     <div class="col-lg-12">
                      <div class="col-lg-6 text-left">Language Skill/Proficiency </div>

                      <div class="col-lg-6 text-right ">
                       <button type="button" id="btnproficiencyRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                       <button type="button" id="btnproficiencyAddRow" class="btn btn-warning btn-xs">Add</button>
                     </div>

                   </div>
                 </th>
               </tr> 
               <tr>

                <th class="text-center" style="vertical-align: top;">Language <span style="color: red;">*</span></th>
                <th class="text-center" style="vertical-align: top;">Speak <span style="color: red;">*</span></th>
                <th class="text-center" style="vertical-align: top;">Read <span style="color: red;">*</span></th>
                <th class="text-center" style="vertical-align: top;">Write <span style="color: red;">*</span></th>
              </tr> 
            </thead>
            <tbody id="bodytblForm11">
              <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
              <?php 

              if ($languageproficiency->Lcount==0) { ?>
              <tr id="bodytblForm11">
                <td>  <?php
                $options = array('' => 'Select');
                foreach($syslanguage as $key => $value) {
                  $options[$value->lang_cd] = $value->lang_name;
                }
                echo form_dropdown('syslanguage[]', $options, set_value('syslanguage'), 'class="form-control" data-toggle="tooltip" title="Select language !" required="required" ');
                ?>
                <?php echo form_error("syslanguage");?></td>
                <td>
                 <?php
                 $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                 echo form_dropdown('speak[]',$sysspeak,set_value('speak'),'class ="form-control" data-toggle="tooltip" title="Select Speak !" required="required"  ');
                 ?>
                 <?php echo form_error("speak");?>
               </td>
               <td><?php
               $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
               echo form_dropdown('read[]',$sysread,set_value('read'),'class ="form-control" data-toggle="tooltip" title="Select Read !" required="required"  ');
               ?>
               <?php echo form_error("read");?></td>
               <td><?php
               $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
               echo form_dropdown('write[]',$syswrite,set_value('write'),'class ="form-control" data-toggle="tooltip" title="Select Write !" required="required"  ');
               ?>
               <?php echo form_error("write");?></td>
             </tr>
             <?php } else{ 
              $i= 0;

              foreach ($languagedetals as $key => $val) {
               ?>
               <tr id="bodytblForm11">
                <input type="hidden" name="l_id[]" value="<?php echo $val->id;?>">
                <td>

                  <?php
                  $options = array('' => 'Select');
                  foreach($syslanguage as $key => $value) {
                    $options[$value->lang_cd] = $value->lang_name;
                  }
                  echo form_dropdown('syslanguage['.$i.']', $options, $val->languageid, 'class="form-control" data-toggle="tooltip" title="Select language !" required="required" ');
                  ?>
                  <?php echo form_error("syslanguage");?>
                </td>
                <td>
                 <?php
                 $sysspeak = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                 echo form_dropdown('speak['.$i.']',$sysspeak,$val->lang_speak,'class ="form-control" data-toggle="tooltip" title="Select Speak !" required="required" ');
                 ?>
                 <?php echo form_error("speak");?>
               </td>
               <td>
                 <?php
                 $sysread = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                 echo form_dropdown('read['.$i.']',$sysread,$val->lang_read,'class ="form-control" data-toggle="tooltip" title="Select Read !" required="required" ');
                 ?>
                 <?php echo form_error("read");?></td>
                 <td>
                   <?php
                   $syswrite = array('' => 'Select', 'H' => 'High', 'M'=>'Moderate', 'L'=>'Low');
                   echo form_dropdown('write['.$i.']',$syswrite,$val->lang_write,'class ="form-control" data-toggle="tooltip" required="required"  title="Select Write !" ');
                   ?>
                   <?php echo form_error("write");?></td>
                 </tr>

                 <?php $i++; }   ?>

                 <?php } ?>

               </tbody>
             </table>
           </div>





           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

             <table id="tblForm12" class="table table-bordered">
              <thead>
               <tr class="bg-lightgreen text-white">
                <th colspan="9">
                 <div class="col-lg-12">
                  <div class="col-lg-6 text-left">Work Experience (if any) </div>

                  <div class="col-lg-6 text-right ">
                   <button type="button" id="btnworkexperienceRemoveRow" class="btn btn-danger btn-xs">Remove</button>
                   <button type="button" id="btnworkexperienceAddRow" class="btn btn-warning btn-xs">Add</button>
                 </div>

               </div>
             </th>
           </tr> 
           <tr>
            <th>Organization Name<span style="color: red;">*</span></th>
            <th>Designation<span style="color: red;">*</span></th>
            <th>Description of Assignment<span style="color: red;">*</span></th>
            <th>Duration<span style="color: red;">*</span></th>
            <th>Place of Posting<span style="color: red;">*</span></th>
            <th>Last salary drawn (monthly)<span style="color: red;">*</span></th>
            <th colspan="3">Upload document<span style="color: red;">*</span></th>

          </tr>

          <tr>
            <th colspan="3"></th>
            <th colspan="1">
              <div class="col-lg-12">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
              </div></th>
              <th ></th>
            </tr>
          </thead>
          <tbody id="bodytblForm12">
            <input type="hidden" name="workexpcount" id="workexpcount" value="<?php echo $WorkExperience->WEcount;?>">
            <?php if ($WorkExperience->WEcount==0) {?>
            <tr id="bodytblForm12"> 
              <td><input type="text" name="orgname[]" maxlengt="100" id="orgname" data-toggle="tooltip" value="" class="form-control" title="Enter Organization Name !" placeholder="Enter Organization Name " ></td>
              <td><input type="text" 
                name="designation[]" maxlength="50" id="designation" data-toggle="tooltip" value="" class="form-control" title="Enter Designation !" placeholder="Enter Designation" ></td>
                <td><textarea  maxlength="250" name="descriptionofassignment[]" data-toggle="tooltip" id="descriptionofassignment" value="" class="form-control" title="Enter Description of Assignment" placeholder="Enter Description of Assignment " ></textarea></td>
                <td>
                 <div class="col-lg-12">
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                    <input type="text" name="work_experience_fromdate[]" id="work_experience_fromdate" value="" class="form-control datepicker" value=""  placeholder="Enter from date" size="150">
                  </div> 
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                    <input type="text" name="work_experience_todate[]" id="work_experience_todate" class="form-control datepicker" value="" placeholder="Enter To date" size="150">
                  </div> 
                </div>
              </td>
              <td>
                <input type="text" maxlength="50" name="palceofposting[]" data-toggle="tooltip" id="palceofposting" value="" class="form-control" title="Place of Posting[]" placeholder="Enter Place of Posting"></td>
                <td><input type="text" maxlength="50" name="lastsalarydrawn[]" data-toggle="tooltip" id="lastsalarydrawn" value="" class="form-control txtNumeric" title="lastsalarydrawn" placeholder="Enter Last salary drawn">
                </td>

                <td width="100">

                  <?php if (!empty($workexperiencedetails[0]->organizationname)) { 
                    ?>
                    <input type="file" name="experiencedocument[]"  id="experiencedocument" class="file" accept="application/pdf, application/msword" required >
                    Experience certificate<?php }else{?>
                    <input type="file" name="experiencedocument[]"  id="experiencedocument" class="file" accept="application/pdf, application/msword">Experience certificate
                    <?php } ?>
                    <?php if (!empty($workexperiencedetails[0]->organizationname)) { 
                      ?>
                      <input type="file" name="salary_slip1[]"  id="salary_slip1" class="file" accept="application/pdf, application/msword" required> Salary Slip1
                      <?php }else{?>
                      <input type="file" name="salary_slip1[]"  id="salary_slip1" class="file" accept="application/pdf, application/msword"> Salary Slip1
                      <?php } ?>
                      <?php if (!empty($workexperiencedetails[0]->organizationname)) { 
                        ?>
                        <input type="file" name="salary_slip2[]"  id="salary_slip2" class="file" accept="application/pdf, application/msword" required>Salary Slip2
                        <?php }else{?>
                        <input type="file" name="salary_slip2[]"  id="salary_slip2" class="file" accept="application/pdf, application/msword">Salary Slip2
                        <?php } ?>
                        <?php if (!empty($workexperiencedetails[0]->organizationname)) { 
                          ?>
                          <input type="file" name="salary_slip3[]"  id="salary_slip3" class="file" accept="application/pdf, application/msword" required>Salary Slip3
                          <?php }else{?>
                          <input type="file" name="salary_slip3[]"  id="salary_slip3" class="file" accept="application/pdf, application/msword">Salary Slip3
                          <?php } ?>

                        </td>

                      </tr>

                      <?php  } else{
                        $i=0;
                        foreach ($workexperiencedetails as $key => $val) {  
                          ?>
                          <tr id="bodytblForm12"> 
                            <input type="hidden" name="w_id[]" value="<?php echo $val->id;?>">
                            <td><input type="text" maxlength="100" name="orgname[]" id="orgname" data-toggle="tooltip" value="<?php echo $val->organization;?>" class="form-control" title="Enter Organization Name !" placeholder="Enter Organization Name " required ></td>
                            <td><input type="text" name="designation[]" maxlength="50" id="designation" data-toggle="tooltip" value="<?php echo $val->designation;?>" class="form-control" title="Enter Designation !" placeholder="Enter Designation" ></td>
                            <td>
                              <textarea maxlength="250" name="descriptionofassignment[]" data-toggle="tooltip" id="descriptionofassignment" required value="<?php echo $val->reason_leaving;?>" class="form-control" title="Enter Description of Assignment" placeholder="Enter Description of Assignment " > <?php echo $val->reason_leaving;?></textarea></td>
                              <td>
                               <div class="col-lg-12">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                                  <input type="text" name="work_experience_fromdate[]" id="work_experience_fromdate_<?php echo $i;?>" value="<?php echo $this->model->changedate($val->fromdate);?>" required class="form-control datepicker"   placeholder="Enter from date" size="150">
                                </div> 
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                  <input type="text" name="work_experience_todate[]" id="work_experience_todate_<?php echo $i;?>" class="form-control datepicker" value="<?php echo $this->model->changedate($val->todate);?>" required placeholder="Enter To date" size="150">
                                </div> 
                              </div>
                            </td>
                            <td><input type="text" maxlength="50" name="palceofposting[]" data-toggle="tooltip" id="palceofposting" 
                              value="<?php echo $val->position;?>" class="form-control " required title="Place of Posting[]" placeholder="Enter Place of Posting"></td>
                              <td><input type="text" maxlengt="50" name="lastsalarydrawn[]" data-toggle="tooltip" id="lastsalarydrawn" value="<?php echo $val->lastsalary ;?>" class="form-control txtNumeric" title="lastsalarydrawn" placeholder="Enter Last salary drawn"></td>
                              <td>
                               <div class="col-lg-9">
                                <div class="form-group">
                                  <div class="col-xs-12">
                                    <input type="file" name="experiencedocument[]"  id="experiencedocument" class="file" accept="application/pdf, application/msword">Experience Certificate
                                    <input type="file" name="salary_slip1[]"  id="salary_slip1" class="file" accept="application/pdf, application/msword">Salary Slip1
                                    <input type="file" name="salary_slip2[]"  id="salary_slip2" class="file" accept="application/pdf, application/msword">Salary Slip2
                                    <input type="file" name="salary_slip3[]"  id="salary_slip3" class="file" accept="application/pdf, application/msword">Salary Slip3
                                  </div>
                                </div>
                              </div>
                              <div class="col-lg-3">
                                <?php if(!empty($val->encrypteddocumnetname)) { ?>
                                <div class="form-group">
                                 <input type="hidden" name="originalexperiencedocument[]" id="originalexperiencedocument" value="<?php echo $val->originalexperiencedocument;?>">
                                 <input type="hidden" name="oldexperiencedocument[]" id="oldexperiencedocument" value="<?php echo $val->encrypteddocumnetname;?>">
                                 <a  href="<?php echo site_url().'datafiles/workexperience/'.
                                 $val->encrypteddocumnetname;?>" download >
                                 <i class="fa fa-download" aria-hidden="true"></i>
                               </a>
                             </div>
                             <?php } ?>
                             <?php if(!empty($val->encryptedsalaryslip1)) { ?>
                             <div class="form-group">
                               <input type="hidden" name="originalsalary_slip1[]" id="originalsalary_slip1" value="<?php echo $val->originalsalaryslip1;?>">
                               <input type="hidden" name="oldsalary_slip1[]" id="oldsalary_slip1" value="<?php echo $val->encryptedsalaryslip1;?>">
                               <a  href="<?php echo site_url().'datafiles/workexperience/'.
                               $val->encryptedsalaryslip1;?>" download >
                               <i class="fa fa-download" aria-hidden="true"></i>
                             </a>
                           </div>
                           <?php } ?>
                           <?php if(!empty($val->encryptedsalaryslip2)) { ?>
                           <div class="form-group">
                             <input type="hidden" name="originalsalary_slip2[]" id="originalsalary_slip2" value="<?php echo $val->originalsalaryslip2;?>">
                             <input type="hidden" name="oldsalary_slip2[]" id="oldsalary_slip2" value="<?php echo $val->encryptedsalaryslip2;?>">
                             <a  href="<?php echo site_url().'datafiles/workexperience/'.
                             $val->encryptedsalaryslip2;?>" download >
                             <i class="fa fa-download" aria-hidden="true"></i>
                           </a>
                         </div>
                         <?php } ?>
                         <?php if(!empty($val->encryptedsalaryslip3)) { ?>
                         <div class="form-group">
                           <input type="hidden" name="originalsalary_slip3[]" id="originalsalary_slip3" value="<?php echo $val->originalsalaryslip3;?>">
                           <input type="hidden" name="oldsalary_slip3[]" id="oldexperiencedocument" value="<?php echo $val->encryptedsalaryslip3;?>">
                           <a  href="<?php echo site_url().'datafiles/workexperience/'.
                           $val->encryptedsalaryslip3;?>" download >
                           <i class="fa fa-download" aria-hidden="true"></i>
                         </a>
                       </div>
                       <?php } ?>
                     </div>

                   </td>
                 </tr>

                 <?php $i++; } } ?>
               </tbody>
             </table>
           </div>





           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

             <table id="tbltrainingexposure" class="table table-bordered">
              <thead>
               <tr class="bg-lightgreen text-white">
                <th colspan="8" >Subject(s) of Interest </th>
              </tr> 

            </thead>
            <tbody>
              <tr>
                <td><textarea class="form-control" name="subjectinterest"  maxlength="1500" id="subjectinterest" cols="12" rows="5" data-toggle="tooltip" title="Enter Subject(s) of Interest !" ><?php 
                if(isset($candidatedetails->any_subject_of_interest)){
                  echo $candidatedetails->any_subject_of_interest;
                }?></textarea></td>
              </tr>
            </tbody>
          </table>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

         <table id="tbltrainingexposure" class="table table-bordered">
          <thead>
           <tr class="bg-lightgreen text-white">
            <th colspan="8" >Achievements / Awards (if any) </th>
          </tr> 

        </thead>
        <tbody>
          <tr>

            <td><textarea class="form-control" name="achievementawards" id="achievementawards" cols="12" rows="5"  maxlength="1500" data-toggle="tooltip" title="Enter Achievement /Awards(if any) !" >
              <?php if (isset($candidatedetails->any_achievementa_awards) ) {
                echo ltrim($candidatedetails->any_achievementa_awards);
              } ?>
            </textarea></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
     <table id="tbltrainingexposure" class="table table-bordered">
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8" >Describe any assignment(s) of special interest undertaken by you (if any) </th>
      </tr> 

    </thead>
    <tbody>
      <tr >
       <td><textarea class="form-control" data-toggle="tooltip"  maxlength="1500" name="any_assignment_of_special_interest" id="any_assignment_of_special_interest" cols="12" rows="5" title="Describe any Assignment(s) of special interest undertaken by you(if any) " placeholder="Enter Achievement /Awards" ><?php 
       if (isset($candidatedetails->any_assignment_of_special_interest)) {
        echo $candidatedetails->any_assignment_of_special_interest;
      }
      ?></textarea></td>
    </tr>
  </tbody>
</table>
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tbltrainingexposure" class="table table-bordered">
  <thead>
   <tr class="bg-lightgreen text-white">
    <th colspan="8" >Experience of Group and/or Social Activities </th>
  </tr> 
</thead>
<tbody>
  <tr>
    <td><textarea class="form-control" data-toggle="tooltip"  maxlength="1500" name="experience_of_group_social_activities" id="experience_of_group_social_activities" cols="12" rows="5" title="Experience of Group and/or Social Activities " placeholder="Enter Experience of Group and/or Social Activities " ><?php 
    if (isset($candidatedetails->experience_of_group_social_activities)) {
      echo $candidatedetails->experience_of_group_social_activities;
    }
    ?></textarea></td>
  </tr>
</tbody>
</table>
</div>


<!--  status start-->
<?php if($login_data->RoleID==10)
{
  ?>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tbltrainingexposure" class="table table-bordered">
    <thead>
     <tr class="bg-lightgreen text-white">

      <td colspan="8" ><b>Verification </b></th>
      </tr> 

    </thead>
    <tbody>
      <tr>
        <tr>
          <td>
            <div class="col-lg-12 col-md-12 col-sm-4 col-xs-4">


              <div class="col-lg-12 col-md-3 col-sm-12 col-xs-12">
                <label class="form-check-label" for="campus_placement_cell"><b>status</b></label>

              </div>



              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <select name="status" class="form-control">
                  <option value="">Select The Status</option>
                  <option value="2">Approved</option>
                  <option value="3">Reject</option>
                </select>


              </div>

            </tr>
            <tr>
              <td>
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-4">
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <label class="form-check-label" for="campus_placement_cell"><b>Comments <span style="color: red;">*</span> : </b></label>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <textarea name="comments" id="comments" value="" class="form-control" ></textarea>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php } ?>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

        <div class="row panel-footer ">  
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">


            <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-success text-center" onclick="return ifanyvalidation()" >Save</button>
            <button type="submit" name="SubmitDatasend" value="SubmitData" id="saveDataWorkExpButton" class="btn btn-dark text-center" onclick="return ifanyvalidation()">Save & Submit</button>

          </div>
        </div>

      </div>
    </div>

  </div>
</div>
</div>


</div>
</div>
<!-- End  Candidates Experience Info -->
</div>
</div>
</form>
</section>

<script type="text/javascript">
  $(document).ready(function() {

   $("input[name$='have_you_come_to_know']").click(function() {
    var test = $(this).val();
    if (test =='other') {
      $("#other_have_you_come_to_know").show();
      $("#specify").prop('required','required');
      $('#specify').removeAttr("disabled");

    }else{
      $("#other_have_you_come_to_know").hide();
      $("#specify").prop('disabled','true');

    }

  });
 });

  $("input[name$='have_you_come_to_know']").load(function() {
    var test = $(this).val();
    if (test =='other') {
      $("#other_have_you_come_to_know").show();
      $("#specify").prop('required','required');
      $('#specify').removeAttr("disabled");

    }else{
      $("#other_have_you_come_to_know").hide();
      $("#specify").prop('disabled','true');

    }

  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    if ( $("input[name$='other_specify']").is(':checked') ) {
      $("#other_other_specify_degree").show();
      $("#specify_degree").prop('required','required');
    } else {
     $("#other_other_specify_degree").hide();
     $("#specify").prop('disabled','true');
   }
   $("input[name$='other_specify']").change(function() {
      //var test = $(this).val();
      if ( $(this).is(':checked') ) {
        $("#other_other_specify_degree").show();
        $("#specify_degree").prop('required','required');
      } else {
       $("#other_other_specify_degree").hide();
       $("#specify").prop('disabled','true');
     }
   });

 });

  function ifanyvalidation(){

    var natureoftraining    = $('#natureoftraining').val();
    var organizingagency    = $('#organizingagency').val();
    var fromdate            = $('#fromdate').val();
    var todate              = $('#todate').val();
    var orgname             = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var designation         = $('#designation').val();
    var lastsalarydrawn     = $('#lastsalarydrawn').val();
    var pgupstream          = $('#pgupstream').val();
    var other               = $('#other').val();

    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();
    var experiencedocument = $('#experiencedocument').val();
    var oldexperiencedocument = $('#oldexperiencedocument').val();
    var gapfromdate = $('#gapfromdate').val();
    var gaptodate = $('#gaptodate').val();
    var gapreason = $('#gapreason').val();


    if (pgupstream !='') {
      if (pgcertificate =='') {
       $('#pgcertificate').focus();
       return false;
     }
   }

   if (other !='') {
    if (otherscertificate =='') {
     $('#otherscertificate').focus();
     return false;
   }
 }

 if (natureoftraining !='') {
  if (organizingagency =='') {
   $('#organizingagency').focus();
   return false;
 }
 if (fromdate =='') {
   $('#fromdate').focus();
   return false;
 }
 if (todate =='') {
   $('#todate').focus();
   return false;
 }

}


if (gapfromdate !='') {
  if (gaptodate =='') {
   $('#gaptodate').focus();
   return false;
 }
 if (gapreason =='') {
   $('#gapreason').focus();
   return false;
 }

}

if (orgname !='') {

  if (designation =='') {
   $('#designation').focus();
   return false;
 }

 if (descriptionofassignment =='') {
   $('#descriptionofassignment').focus();
   return false;
 }
 if (work_experience_fromdate =='') {
   $('#work_experience_fromdate').focus();
   return false;
 }

 if (work_experience_todate =='') {
   $('#work_experience_todate').focus();
   return false;
 }
 if (palceofposting =='') {
   $('#palceofposting').focus();
   return false;
 }
 if (lastsalarydrawn =='') {
  $('#lastsalarydrawn').focus();
  return false;
}
if (oldexperiencedocument =='') {
 $("#experiencedocument").focus();
 return false;
}
}
}

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

var matriccertificate_uploadField = document.getElementById("10thcertificate");

matriccertificate_uploadField.onchange = function() {

  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 }
}


var hsccertificate_uploadField = document.getElementById("12thcertificate");

hsccertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

var ugcertificate_uploadField = document.getElementById("ugcertificate");

ugcertificate_uploadField.onchange = function() {
  if(this.files[0].size > 400000){
   alert("File is too big! Please upload less than or equality 50 KB ");
   this.value = "";
 };
}; 

</script>
<script type="text/javascript">


  $(document).ready(function(){
    $("#filladdress").on("click", function(){
     if (this.checked) { 
      $("#permanenthno").val($("#presenthno").val());
      $("#permanentstreet").val($("#presentstreet").val());
      $("#permanentcity").val($("#presentcity").val());
      $("#permanentstateid").val($("#presentstateid").val()); 
      $("#permanentdistrict").val($("#presentdistrict").val());
      $("#permanentpincode").val($("#presentpincode").val()); 
      $("#permanentstateid option:selected").val($("#presentstateid option:selected").val());
    }
    else {
      $("#permanenthno").val('');
      $("#permanentstreet").val('');
      $("#permanentcity").val('');
      $("#permanentstateid").val(''); 

      $("#permanentdistrict").val('');
      $("#permanentpincode").val('');
      $("#permanentstateid").val('');          
    }
  });

    if ($("#presentstreet").val() == $("#permanentstreet").val()) {
      $("#filladdress").prop('checked', true);
        //$("#filladdress").prop("disabled", true);
      }

    });


  function decimalData(){
    $('.txtNumeric').keypress(function(event) {
     var $this = $(this);
     if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
      event.preventDefault();
  }

  var text = $(this).val();
  if ((event.which == 46) && (text.indexOf('.') == -1)) {
   setTimeout(function() {
     if ($this.val().substring($this.val().indexOf('.')).length > 3) {
       $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
     }
   }, 1);
 }

 if ((text.indexOf('.') != -1) &&
   (text.substring(text.indexOf('.')).length > 2) &&
   (event.which != 0 && event.which != 8) &&
   ($(this)[0].selectionStart >= text.length - 2)) {
   event.preventDefault();
}      
});

    $('.txtNumeric').bind("paste", function(e) {
      var text = e.originalEvent.clipboardData.getData('Text');
      if ($.isNumeric(text)) {
       if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
         e.preventDefault();
         $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
     }
     else {
       e.preventDefault();
     }
   });
  }

  $("#btnRemoveRow").click(function() {

    alert($('#tblForm09 tr').length);

    if($('#tblForm09 tr').length-2>1)

      $('#tbodyForm09 tr:last').remove()
  });

  $('#btnAddRow').click(function() {

    rowsEnter = $('#tbodyForm09 tr').length;

    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertfamilyRows(rowsEnter);
  });

  var srNoGlobal=0;
  var inc = 0;
 //  var familymemcount = $('#familymembercount').val();

 //  if(familymemcount ==0){
 //   var srNoGlobal=0;
 //   var inc = 0;
 // }else{
 //   var srNoGlobal = familymemcount;
 //   var inc = familymemcount;
 // }

 function insertfamilyRows(count) {

  srNoGlobal = $('#tbodyForm09 tr').length+1;
  var tbody = $('#tbodyForm09');
  var lastRow = $('#tbodyForm09 tr:last');
  var cloneRow = null;
   //alert(count);
 // for (i = 1; i <= count; i++) {
 //    if(familymemcount ==0){
 //      inc++;
 //    }

 cloneRow = lastRow.clone();
 var tableData = '<tr>'
 + ' <td>'
 + '<input type="text" name="familymembername['+count+']" id="familymembername" value="" class="form-control" required="required" data-toggle="tooltip" maxlength="50" title="Enter Family Member Name !" placeholder="Enter Family Member Name" >'
 + '</td><td>'
 + '<select class="form-control" name="relationwithenployee['+count+']" id="relationwithenployee" required="required">'
 + '<option value="">Select Relation</option>'
 <?php foreach ($sysrelations as $key => $value): ?>
 + '<option value=<?php echo $value->id;?>><?php echo $value->relationname;?></option>'
 <?php endforeach ?>
 + '</select>'
 + '</td>'
 + '<td><input type="text" class="form-control datepicker hasDatepicke"  id="familymemberdob_'+count+'" '+
 'name="familymemberdob['+count+']" value="" maxlength="100" required="required" placeholder="Enter Date of Birth"/>'
 + '</td>'

 +'<td><div class="form-group">'
 +'<div class="col-lg-3"><b>Photo : </b> </div>'
 +' <input type="file" name="familymemberphoto['+count+']" id="familymemberphoto['+count+']" class="file" required="required">'


 +' </div>'
 +'<div class="form-group">'
 +'<div class="col-lg-3"><b>Identity Proof: </b> </div>'
 +' <input type="file" name="identity_familymemberphoto['+count+']" id="identity_familymemberphoto['+count+']" class="file" required="required">'
 +'</div>'
 +'</td>'
 + '</tr>';
 
 $("#tbodyForm09").append(tableData)
 adddatepicker();
 
   // }

 }
  //insertRows();
</script>



<script type="text/javascript">
  $("#btntrainingexposureRemoveRow").click(function() {
    if($('#tblForm10 tr').length-2>1)
      $('#bodytblForm10 tr:last').remove()
  });

  $('#btntrainingexposureAddRow').click(function() {
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaettrainingexposure(rowsEnter1);
    //addDatePicker();
  });
  var  counttraining = $("#count_te").val();
  //alert(counttraining);

 //  if(counttraining==0){
 //   var srNoGlobal=0;
 //   var inctg = 0;
 // }else{
 //   var srNoGlobal= counttraining;
 //   var inctg = counttraining;

 // }

 function Insaettrainingexposure(count) {
  srNoGlobal = $('#bodytblForm10 tr').length+1;
  var tbody = $('#bodytblForm10');
  var lastRow = $('#bodytblForm10 tr:last');
  var cloneRow = null;
 // alert(count);
 // for (i = 0; i < count; i++) {
 //  if(counttraining==0){
 //   inctg++;
 // }
 count=$('#bodytblForm10 tr').length;

 cloneRow = lastRow.clone();
 var tableData1 = '<tr>'
 + '<td> <input type="text"  class="form-control " data-toggle="tooltip" title="" minlength="5" maxlength="150" id="natureoftraining['+count+']" name="natureoftraining['+count+']" placeholder="Enter Nature of Training" style="min-width: 20%;" value="" data-original-title="Nature of Training !" required="required">'
 + '</td>'
 + '<td class="focused"> <input type="text" class="form-control " data-toggle="tooltip" minlength="5" maxlength="150" title="" id="organizingagency['+count+']" name="organizingagency['+count+']" placeholder="Organizing Agency" style="min-width: 20%;" value="" data-original-title="Organizing Agency !" required="required"></td>'

 + '<td><input type="text" class="form-control datepicker" id="fromdate_'+count+'" '+
 'name="fromdate['+count+']" onclick="return changedatepicker(inctg)" value="" required="required"/>'
 + '</td><td>'
 + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="todate_'+count+'" '+
 'name="todate['+count+']" value="" required="required" />'
 + '</td>' + '</tr>';
 adddatepicker();
 $("#bodytblForm10").append(tableData1)
// alert(count);
changedatepicker(count);
 //inctg++; }

}
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnproficiencyRemoveRow").click(function() {
    if($('#tblForm11 tr').length-2>1)
      $('#bodytblForm11 tr:last').remove()
  });

  $('#btnproficiencyAddRow').click(function() {
    rowsEnter2 = parseInt(1);

    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insaetproficiency(rowsEnter2);
    addDatePicker();
  });

  var lancount = $('#languagecount').val();
 //  if(lancount ==0){
 //    var srNoGlobal=0;
 //    var inclp = 0;
 //  }else{
 //   var srNoGlobal=lancount;
 //   var inclp = lancount;
 // }

 function Insaetproficiency(count) {
  srNoGlobal = $('#bodytblForm11 tr').length+1;
  var tbody = $('#bodytblForm11');
  var lastRow = $('#bodytblForm11 tr:last');
  var cloneRow = null;
  count=$('#bodytblForm11 tr').length;

  // for (i = 0; i < count; i++) {
  //   if(lancount ==0){
  //     inclp++;
  //   }
  cloneRow = lastRow.clone();
  var tableData2 = '<tr>'

  + '<td><select class="form-control" name="syslanguage['+count+']" id="syslanguage['+count+']"required="required">'
  + '<option value="">Select </option>'
  <?php foreach ($syslanguage as $key => $value): ?>
  + '<option value=<?php echo $value->lang_cd;?>><?php echo $value->lang_name;?></option>'
  <?php endforeach ?>
  + '</select> </td><td>'
  + '<select class="form-control" name="speak['+count+']" id="speak['+count+']"required="required">'
  + '<option value="">Select </option>'
  <?php foreach ($sysspeak as $key => $value): ?>
  + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
  <?php endforeach ?>
  + '</select> </td><td>'

  + '<select class="form-control" name="read['+count+']" id="read['+count+']"required="required">'
  + '<option value="">Select </option>'
  <?php foreach ($sysread as $key => $value): ?>
  + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
  <?php endforeach ?>
  + '</select> </td><td>'

  + '<select class="form-control" name="write['+count+']" id="write['+count+']"required="required">'
  + '<option value="">Select </option>'
  <?php foreach ($syswrite as $key => $value): ?>
  + '<option value=<?php echo $key;?>><?php echo $value;?></option>'
  <?php endforeach ?>
  + '</select> </td>'
  + '</tr>';
  adddatepicker();
  $("#bodytblForm11").append(tableData2)
//
alert(count);

// inclp++; }`

}
  //insertRows();
</script>


<script type="text/javascript">
  $("#btnworkexperienceRemoveRow").click(function() {

    if($('#tblForm12 tr').length-2>2)
      $('#bodytblForm12 tr:last').remove()
  });

  $('#btnworkexperienceAddRow').click(function() {
    rowsEnter2 = parseInt(1);
    if (rowsEnter2 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    Insertworkexperience(rowsEnter2);
    addDatePicker();
  });

  var workexpcount = $('#workexpcount').val();
  //alert(workexpcount);

  // if (workexpcount ==0) {
  //   var srNoGlobal=0;
  //   var incwe = 0;
  // }else{
  //   var srNoGlobal = workexpcount;
  //   var incwe = workexpcount;
  // }


  function Insertworkexperience(count) {

    srNoGlobal = $('#bodytblForm12 tr').length+1;
    var tbody = $('#bodytblForm12');
    var lastRow = $('#bodytblForm12 tr:last');
    var cloneRow = null;
    count=$('#bodytblForm12 tr').length;
    
    // for (i = 0; i < count; i++) {
    //   if (workexpcount ==0) {
    //     incwe++;
    //   }

    cloneRow = lastRow.clone();
    var tableData2 = '<tr>'
    + '<td>'
    + '<input type="text" name="orgname['+count+']" id="orgname_'+count+'" data-toggle="tooltip" value="" class="form-control" maxlengt="100" data-original-title="" required="required" title="">'
    + '</td><td>'
    + '<input type="text" name="designation['+count+']" id="designation_'+count+'" data-toggle="tooltip" value="" class="form-control" maxlengt="50" data-original-title="" required="required" title="">'

    + '</td><td>'
    + '<textarea name="descriptionofassignment['+count+']" data-toggle="tooltip" maxlengt="250"  id="descriptionofassignment_'+count+'" required="required" class="form-control" data-original-title="" title=""> '
    + '</textarea></td><td>'

    + ' <div class="col-lg-12">'
    + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">'
    + '    <input type="text" name="work_experience_fromdate['+count+']"  data-toggle="tooltip" id="work_experience_fromdate_'+count+'"  value="" class="form-control datepicker" size=150>'
    + ' </div>' 
    + ' <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">'
    + ' <input type="text" name="work_experience_todate['+count+']" "  data-toggle="tooltip" id="work_experience_todate_'+count+'"   class="form-control datepicker" value="" size=150></div> </div>'
    + '</td><td>'

    + '<input type="text" maxlengt="50" name="palceofposting['+count+']" data-toggle="tooltip" id="palceofposting_'+count+'" required="required" value="" class="form-control" data-original-title=""  title="">'
    + ' </td><td>'
    + '<input type="text" maxlengt="10" name="lastsalarydrawn['+count+']" data-toggle="tooltip" id="lastsalarydrawn_'+count+'" required="required" value="" class="form-control txtNumeric" data-original-title=""  title="">'
    + '</td><td>'

    +' <div class="form-group">'
    +'   <input type="file" name="experiencedocument['+count+']"  id="experiencedocument_'+count+'" required class="file" ">'
    +'   <input type="file" name="salary_slip1['+count+']"  id="salary_slip1'+count+'" required class="file" ">'
    +'   <input type="file" name="salary_slip2['+count+']"  id="salary_slip2'+count+'" required class="file" ">'
    +'   <input type="file" name="salary_slip3['+count+']"  id="salary_slip3'+count+'" required class="file" ">'
    +' </div>'
    + '</td></tr>';
    adddatepicker();
    $("#bodytblForm12").append(tableData2)
     // alert(count);
     
     //  incwe++; }
     //changedatepicker(count);

   }
  //insertRows();
</script>

<script type="text/javascript">
  $("#btnidentitydetailRemoveRow").click(function() {
    if($('#tblForm13 tr').length-2>1)
      $('#tbodyForm13 tr:last').remove()
  });

  $('#btnidentitydetailAddRow').click(function() {
    rowsIdentityEnter = parseInt(1);
    if (rowsIdentityEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertIdentityRows(rowsIdentityEnter);
    addDatePicker();
  });

  var identcount = $('#identcount').val();

  // if (identcount ==0) {
  //   var srNoIdentity=0;
  //   var incr = 0;
  // }else{
  //   var srNoIdentity = identcount;
  //   var incr = identcount;
  // }
  

  function insertIdentityRows(count) {
    srNoIdentity = $('#tbodyForm13 tr').length+1;
    var tbody = $('#tbodyForm13');
    var lastRow = $('#tbodyForm13 tr:last');
    var cloneRow = null;
    count=$('#tbodyForm13 tr').length;

    // for (i = 0; i < count; i++) {
    //  if (identcount ==0) {
    //   incr++
    // }
    cloneRow = lastRow.clone();
    var tableDataIdentity = '<tr>'
    + ' <td>'
    + '<select class="form-control" name="identityname['+count+']" required="required">'
    + '<option value="">Select Identity</option>'
    <?php foreach ($sysidentity as $key => $value): ?>
    + '<option value=<?php echo $value->id;?>><?php echo $value->name;?></option>'
    <?php endforeach ?>
    + '</select>'

    + ' <td><input type="text" name="identitynumber['+count+']" id="identitynumber"  maxlength="30" value="" class="form-control" data-toggle="tooltip" title="Enter Family Member Name !" placeholder="Enter Identity Number" required="required">'
    + '</td><td>'

    +' <div class="form-group">'
    +'   <input type="file" name="identityphoto['+count+']"  id="identityphoto" class="file" required="required">'
    +' </div>'
    + '</td>' + '</tr>';
    $("#tbodyForm13").append(tableDataIdentity)
  //alert("count="+count);

  //incr++; }

}
</script>


<script type="text/javascript">
  $("#btnGapReasonRemoveRow").click(function() {
    if($('#tblForm15 tr').length-2>1)
      $('#bodytblForm15 tr:last').remove()
  });

  $('#btnGapReasonAddRow').click(function() {
    rowsEnter1 = parseInt(1);
    if (rowsEnter1 < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    InsGapReason(rowsEnter1);
    addDatePicker();
  });
  var  counttraining = $("#count_te").val();
  //alert(counttraining);

 //  if(counttraining==0){
 //   var srNoGlobal=0;
 //   var inctg = 0;
 // }else{
 //   var srNoGlobal= counttraining;
 //   var inctg = counttraining;
 
 // }

 function InsGapReason(count) {
  srNoGlobal = $('#bodytblForm15 tr').length+1;
  var tbody = $('#bodytblForm15');
  var lastRow = $('#bodytblForm15 tr:last');
  var cloneRow = null;
  count=$('#bodytblForm15 tr').length;
 // alert(count);
 //  for (i = 0; i < count; i++) {
 //    if(counttraining==0){
 //   inctg++;
 // }
 cloneRow = lastRow.clone();
 var tableData15 = '<tr>'
 + '<td><input type="text" class="form-control datepicker" id="gapfromdate_'+count+'" '+
 'name="gapfromdate['+count+']" onclick="return changedatepicker(inctg)" value="" required="required"/>'
 + '</td><td>'
 + '<input type="text" class="form-control datepicker" onclick="return changedatepicker(inctg)" id="gaptodate_'+count+'" name="gaptodate['+count+']" value="" required="required" />'
 + '</td><td>' 
 + '<textarea class="form-control"  maxlength="250" name="gapreason['+count+']" id="gapreason_'+count+'" title="Gap Reason" placeholder="Enter gap reason" required="required" ></textarea>'
 + '</td>'
 + '</tr>';
 adddatepicker();
 $("#bodytblForm15").append(tableData15)
 changegapyeardatepicker(count);
 //inctg++; }
 //alert(count);

}
  //insertRows();


  $("#btnEducationRemoveRow").click(function() {

    //alert(+$('#tblForm29 tr').length);
       //if($('#tblForm15 tr').length-2>1)
       if($('#tblForm29 tr').length-2>1)


        $('#tblForm29 tr:last').remove()
    });

  $('#btnEducationAddRow').click(function() {


    rowsEnter = parseInt(1);
    
    if (rowsEnter < 1) {
      alert("Row number must be minimum 1.")
      return;
    }
    insertRows(rowsEnter);
    //addDatePicker();
  });
  var srNoGlobal=0;
  var inc = 0;
 // var familymemcount = $('#education_count').val();

 //  if(familymemcount ==0){
 //   var srNoGlobal=0;
 //   var inc = 0;
 // }else{
 //   var srNoGlobal = familymemcount;
 //   var inc = familymemcount;
 // }

 function insertRows(count) {
  //alert("count="+count);
  srNoGlobal = $('#tbodyForm29 tr').length+1;

  var tbody = $('#tbodyForm29');
  var lastRow = $('#tbodyForm29 tr:last');
  var cloneRow = null;

  // for (i = 1; i <= count; i++) {
  //   if(familymemcount ==0){
  //     inc++;

  //   }
  count=$('#tbodyForm29 tr').length;
  cloneRow = lastRow.clone();
    //alert("row="+cloneRow);
    var tableData = '<tr>'
    + ' <td>'

    + '<select class="form-control" data-toggle="tooltip"  name="education_level['+count+']" id="education_level_'+count+'" required="required">'
    + '<option value="">Select Education</option>'
    <?php foreach ($edu_level  as $value): ?>
    + '<option value=<?php echo $value->edulevel_cd;?>><?php echo $value->level_name;?></option>'
    <?php endforeach ?>
    + '</select>'

    + '</td><td>'
    
    + '<input type="text" placeholder="Enter Board/ University" title="Board/ University" name="boarduniversity['+count+']" id="boarduniversity_'+count+'" value="" class="form-control" class="form-control" data-toggle="tooltip" title="Year !" id="10thpassingyear['+count+']" required="required" data-toggle="tooltip" maxlength="50" >'
    + '</td><td>'
    + '<input type="text" placeholder="Enter School/ College/ Institute" title="School/ College/ Institute" class="form-control"  id="schoolcollegeinstitute_'+count+'" '+
    ' name="schoolcollegeinstitute['+count+']" value="" maxlength="100" required="required" />'
    + '</td>'
    +' <td>'
    +' <input type="text" class="form-control" placeholder="Enter Place" title="Place"  name="place['+count+']" id="place_'+count+'" class="file" required="required">'

    +' </td><td>'
    + '<input type="text" placeholder="Enter Year" title="Year" name="passingyear['+count+']" id="passingyear_'+count+'" value="" class="form-control" class="form-control" data-toggle="tooltip" title="Year !"  required="required" data-toggle="tooltip" maxlength="50"  >'
    + '</td><td>'
    
    +' <input type="text" class="form-control"  placeholder="Enter Specialisation" title="Specialisation" name="specialisation['+count+']" id="specialisation['+count+']" class="file" required="required">'

    +' </td>'
    +' <td>'
    +' <input type="text" class="form-control"  placeholder="Enter Percentage" title=" Percentage" name="percentage['+count+']" id="percentage_'+count+'" class="file" required="required">'

    +' </td>'
    

    +' <td><div class="form-group">'
    +' <input type="file" name="certificate['+count+']" id="certificate_'+count+'" class="file" required="required" >'

    +' </div></td>'
    + '</tr>';
    
    $("#tbodyForm29").append(tableData)
    adddatepicker(tableData);
  //inc++; }
 // alert($('#tbodyForm29 tr').length);

}

</script>









<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
    //$("#btnsubmit")..prop('disabled', true);

    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',

   });


  });
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $("#presentstateid").load(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").load(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>
<script type="text/javascript">
 $(document).ready(function(){
  $("#presentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#presentdistrict").html(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#permanentstateid").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getDistrict/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#permanentdistrict").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

});     

</script>
<script type="text/javascript">
 function adddatepicker1(){
   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
       jQuery("#todate").datepicker( "option", "minDate", selectedDate );
       jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
       jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
     }
   });

   
 }


 function adddatepicker(){
   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
       jQuery("#todate").datepicker( "option", "minDate", selectedDate );
       jQuery("#work_experience_todate").datepicker( "option", "minDate", selectedDate );
       jQuery("#gaptodate").datepicker( "option", "minDate", selectedDate );
     }
   });

   $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#work_experience_fromdate").datepicker( "option", "maxDate", selectedDate );
      jQuery("#gapfromdate").datepicker( "option", "maxDate", selectedDate );

    }
  });
 }


 function changedatepicker(inctg){

   $("#fromdate_"+inctg).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#todate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
      });

   $("#todate_"+inctg).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#fromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
    }
  });
 }

 function changegapyeardatepicker(inctg){

   $("#gapfromdate_"+inctg).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    yearRange: '1920:2030',
    onClose: function(selectedDate) {
          //alert(selectedDate);
          jQuery("#gaptodate_"+inctg).datepicker("option", "minDate", selectedDate);
        }
      });

   $("#gaptodate_"+inctg).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
     onClose: function(selectedDate) {
      jQuery("#gapfromdate_"+inctg).datepicker("option", "maxDate", selectedDate);
    }
  });
 }


 function workexchangedatepicker(incwe){

   $("#work_experience_fromdate_"+incwe).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: 'today',
    dateFormat : 'dd/mm/yy',
    onClose: function(selectedDate) {
      jQuery("#work_experience_todate_"+incwe).datepicker("option", "minDate", selectedDate);
    }
  });

   $("#work_experience_todate_"+incwe).datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     onClose: function(selectedDate) {
      jQuery("#work_experience_fromdate_"+incwe).datepicker("option", "maxDate", selectedDate);
    }
  });

 }

 $('document').ready(function()
 {
  $('textarea').each(function(){
    $(this).val($(this).val().trim());
  }
  );
});
</script>
