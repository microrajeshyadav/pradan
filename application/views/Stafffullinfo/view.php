<?php 
$arr = '';
if(!empty($candidatedetails->name))
$arr=explode(" ",$candidatedetails->name);
?>
<br>
<style type="text/css">
  thead>tr:hover
  {
    border-bottom: solid 1px red;
  }   

  label{

    font-weight: bold;
    font-family: 
  }
  .bg-lightgreen
  {
    background-color: #85a4a5;
  }
</style>



<div class="container-fluid" id="tblForm09" style="font-family: 'Oxygen' !important;" >
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Staff Profile</h4>
       <?php if ($this->loginData->RoleID==3 && !empty($getresigstatus->trans_flag) && $getresigstatus->trans_flag ==1 ) {?>
         <div class="col-md-2 text-right">
          <a data-toggle="tooltip" title = "Click here to move on staff list." href ="<?php echo base_url().'Staff_resignation_withdrawal/resignation_withdrawal/'.$this->loginData->staffid;?>"  id="sendtolist" class="btn btn-dark text-center">Resignation withdrawal</a>
        </div>
      <?php } ?>
    </div>
    <hr class="colorgraph"><br>
  </div>
   <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <table id="tblForm09" class="table">
        <thead>
         <tr class="bg-light">
          <th colspan="7" style="height: 5px;" class="bg-lightgreen text-white">Personal Infomation</th>
        </tr> 
      </thead>
      <tbody>
        <tr>
         <?php if(!empty($candidatedetails->staff_name)) $arr=explode(" ",$candidatedetails->staff_name);
          $Stafffirstname = '';
          $Staffmiddlename = '';
          $Stafflastname = '';
         
          
        
         if(count($arr)==2)
         {
           $Stafffirstname = $arr[0];
           $Stafflastname = $arr[1];
         }else if (count($arr)==3) {
          $Stafffirstname = $arr[0];
          $Staffmiddlename = $arr[1];
          $Stafflastname = $arr[2];
        }else{
          $Stafffirstname = $arr[0];
        }
        ?>
        <td rowspan ="6" class="text-left bg-light">

          <?php if(!empty($candidatedetails->encryptedphotoname) && $candidatedetails->encryptedphotoname !='') { ?>
            <img class= "rounded-circle" src="<?php if(!empty($candidatedetails->encryptedphotoname)) echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Profile Picture" width="160px" height="160px">
          <?php }else{ ?>
            <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Profile Picture" width="160px" height="160px">
          <?php } ?>

          <h4><?php  if(!empty($candidatedetails->candidatefirstname)) {
           echo $candidatedetails->candidatefirstname .' '.$candidatedetails->candidatemiddlename.' '.$candidatedetails->candidatelastname;
          }else{
            if(!empty($candidatedetails->staff_name)) echo $candidatedetails->staff_name;
          }   ?> 
         </h4>
          <p style= "font-weight: 700"><i class="fa fa-envelope" style="font-size: 14px; color: #17A2B8;"></i> <?php if(!empty($candidatedetails->emailid)) echo $candidatedetails->emailid;?></p>
          <?php if(!empty($candidatedetails->personnel_emaildid))
          { ?>
            <p style= "font-weight: 700"><i class="fa fa-envelope" style="font-size: 14px; color: #17A2B8;"></i> <?php if(!empty($candidatedetails->personnel_emaildid)) echo $candidatedetails->personnel_emaildid;?></p>
          <?php }
          else{ ?>
            <p style= "font-weight: 700"><i class="fa fa-envelope" style="font-size: 14px; color: #000000;"></i> Not Available</p>
          <?php } ?>
          
          <p style= "font-weight: 700"><i class="fa fa-mobile" aria-hidden="true" style="font-size: 14px; color: #17A2B8;"></i>
            <?php if(!empty($candidatedetails->contact)) echo $candidatedetails->contact;?></p>

            <?php $options = array("1" => "O+", "2" => "B+", "3" => "AB+", "4" => "O-", "5" => "B-", "6" => "AB-");  ?>
            <?php if (!empty($candidatedetails->bloodgroup) && $candidatedetails->bloodgroup==1) { ?>
              <p style= "font-weight: 700"><i class="fas fa-tint" style="color: red"></i><?php echo "O+"; ?></p>
            <?php }elseif (!empty($candidatedetails->bloodgroup) && $candidatedetails->bloodgroup==2) { ?>
             <p style= "font-weight: 700"><i class="fas fa-tint" style="color: red"></i><?php echo "B+"; ?></p>
           <?php }  ?>

         </td>
        <td><label for="Name">Staff name :</label></td>
        <td style= "font-weight: 700"><?php if(!empty($Stafffirstname)) echo $Stafffirstname;?></td>
        <td><label for="Name">Middle name :</label></td>
        <td style= "font-weight: 700"><?php if(!empty($Staffmiddlename)) echo $Staffmiddlename;?></td>
        <td><label for="Name">Last Name :</label></td>
        <td style= "font-weight: 700"><?php if(!empty($Stafflastname)) echo $Stafflastname;?></td>
        
       </tr>

       <tr>
        <?php
          $motherfirstname='';
          $mothermiddlename='';
          $motherlastname='';
          $arr2 ='';
          $arr2[0] = '';
          if(!empty($candidatedetails->mother_name))
          {
         $arr2=explode(" ",$candidatedetails->mother_name);
        if(count($arr2)==2)
        {
         $motherfirstname = $arr2[0];
         $motherlastname = $arr2[1];
       }else if (count($arr2)==3) {
        $motherfirstname = $arr2[0];
        $mothermiddlename = $arr2[1];
        $motherlastname = $arr2[2];
      }else{
        $motherfirstname = $arr2[0];

      }
    }
      ?>
      <td><label for="Name">Mother's name :</label></td>
      <td style= "font-weight: 700"><?php if(!empty($motherfirstname)) echo $motherfirstname;?></td>
      <td><label for="Name">Middle name :</label></td>
      <td style= "font-weight: 700"><?php if(!empty($mothermiddlename)) echo $mothermiddlename;?></td>
      <td><label for="Name">Last Name :</label></td>
      <td style= "font-weight: 700"><?php if(!empty($motherlastname)) echo $motherlastname;?></td>
    </tr>
    <tr>
     <?php
      if(!empty($candidatedetails->father_name))
      {
      $arr3=explode(" ",$candidatedetails->father_name);
      $fatherfirstname = '';
      $fathermiddlename = '';
      $fatherlastname = '';
      $arr3 ='';
      $arr3[0]='';
       $arr3[1]='';
       $arr3[2] = '';
     if(count($arr3)==2)
     {
       $fatherfirstname = $arr3[0];
       $fatherlastname = $arr3[1];
     }else if (count($arr3)==3) {
      $fatherfirstname = $arr3[0];
      $fathermiddlename = $arr3[1];
      $fatherlastname = $arr3[2];
    }else{
      $fatherfirstname = $arr3[0];

    }
  }
    ?>
  <td><label for="Name">Father's name :</label></td>
    <td style= "font-weight: 700"><?php if(!empty($fatherfirstname)) echo $fatherfirstname;?></td>
    <td><label for="Name">Middle name :</label></td>
    <td style= "font-weight: 700"><?php if(!empty($fathermiddlename)) echo $fathermiddlename;?></td>
    <td><label for="Name">Last Name :</label></td>
    <td style= "font-weight: 700"><?php if(!empty($fatherlastname)) echo $fatherlastname;?></td>
  </tr>

  <tr>
    <td><label for="Name">Gender :</label></td>
    <td style= "font-weight: 700"><?php 
    $options = array("1" => "Male", "2" => "Female", "3" => "Others");
    if(!empty($candidatedetails->staff_gender) && ($candidatedetails->staff_gender ==1)) {
      echo "Male";
    }elseif ($candidatedetails->staff_gender ==2) {
     echo "Female";
   }elseif ($candidatedetails->staff_gender ==3) {
     echo "Others";
   } ?>
 </td>
 <td><label for="Name">Nationality :</label></td>

 <td style= "font-weight: 700"> 
   <?php 
   $options = array("1" => "Indian", "2" => "Other");
   if(!empty($candidatedetails->nationality))
   if ($candidatedetails->nationality ==1) {
    echo "Indian";
  }else if ($candidatedetails->nationality ==2) {
    echo "Other";
  }
  ?>  </td>
  <td><label for="Name">Marital Status :</label></td>
  <td style= "font-weight: 700"> <?php 
  // $options = array("1" => "Single", "2" => "Married", "3" => "Divorced", "4" => "Widow", "5" => "Separated");
  if(!empty($candidatedetails->marital_status) && ($candidatedetails->marital_status ==1)) {
    echo "Single";
  }else if ($candidatedetails->marital_status ==2) {
    echo "Married";
  }else if ($candidatedetails->marital_status ==3) {
    echo "Divorced";
  }else if ($candidatedetails->marital_status ==4) {
    echo "Widow";
  }else if ($candidatedetails->marital_status ==5) {
    echo "Separated";

  }
  else{
    echo "N.A.";
    
  }
  ?> 
</td>
</tr>
<?php if(!empty($candidatedetails->candidateid) && ($candidatedetails->candidateid!=null))
{
  ?>
  <tr>
    <td><label for="Name">Annual income of parents :</label></td>
    <td style= "font-weight: 700"><?php 

    if ($candidatedetails->annual_income==1) {
     echo "Below 50,000";
   }else if ($candidatedetails->annual_income==2) {
    echo "50,001-200,000";
  }else if ($candidatedetails->annual_income==3) {
    echo "200,001-500,000";
  }else if ($candidatedetails->annual_income==4) {
   echo "Above 500,000";
 }
 else{
    echo "N.A.";
    
  }
 ?>
</td>
<td><label for="Name">No. of male sibling :</label></td>
<td style= "font-weight: 700"><?php if(!empty($candidatedetails->male_sibling)) echo $candidatedetails->male_sibling;?></td>
<td><label for="Name">No. of female sibling :</label></td>
<td style= "font-weight: 700"><?php if(!empty($candidatedetails->female_sibling)) echo $candidatedetails->female_sibling;?></td>
</tr>

<tr>
  <td><label for="Name">Date Of Birth :</label></td>
  <td style= "font-weight: 700"> <?php if(!empty($candidatedetails->dob)) echo $this->model->changedatedbformate($candidatedetails->dob);?></td>
  <td><label for="Name">Email Id :</label></td>
  <td style= "font-weight: 700"> <?php if(!empty($candidatedetails->emailid)) echo $candidatedetails->emailid;?></td>
  <td><label for="Name">Mobile No. :</label></td>
  <td style= "font-weight: 700"><?php if(!empty($candidatedetails->contact)) echo $candidatedetails->contact;?></td>
</tr>
<?php } ?>

<tr>
<td><label for="Name">Supervisor Name :</label></td>
  <td style= "font-weight: 700"><?php if(!empty($select_reporting->name)) echo $select_reporting->name; ?> </td>
  <td><label for="Name">Office  :</label></td>
  <td style= "font-weight: 700"><?php if(!empty($candidatedetails->officename)) echo $candidatedetails->officename; ?> </td>
  <td><label for="Name">Date of Joining:</label></td>
  <td style= "font-weight: 700"><?php if(!empty($candidatedetails->doj)) echo $this->model->changedatedbformate($candidatedetails->doj); ?></td>

  </tr>
</tbody>
</table>
</div>

</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table" >
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8"> 
          <div class="form-check">
            <label class="form-check-label" for="filladdress"><b>Communication Address </b></label>
          </div>
        </th>
      </tr> 
      <?php $checkdata=''; 
      if(!empty($candidatedetails->presenthno) && !empty($candidatedetails->permanenthno)) 
      {
        if($candidatedetails->presenthno==$candidatedetails->permanenthno) {
       $checkdata = 'checked="checked"';
     } 
   }
     ?>  
     <tr>
      <th class="text-center bg-light" colspan="4" style="vertical-align: top; text-align: center; "> <i class="fa fa-address-card" style="color: #F29005;"></i> &nbsp;Present Mailing Address</th>
      <th class="text-center" colspan="4" style="background-color: #d0d0d0; vertical-align: top; text-align: center;"> <i class="fa fa-address-card" style="color: #17A2B8;"></i> &nbsp;Permanent Mailing Address<br>
       <div class="form-check">
        <!-- <input type="checkbox" class="form-check-input"  <?php echo $checkdata;?>  id="filladdress" name="filladdress"> 
        <label class="form-check-label" for="filladdress"><b>same as present address</b></label> -->
      </div></th>
    </tr> 
  </thead>
  <tbody>
   <tr>
    <td class="bg-light"> <label for="Name">H.No</label></td>
    <td class="bg-light" style= "font-weight: 700"><?php if(!empty($candidatedetails->presenthno)) echo $candidatedetails->presenthno;?></td>
    <td class="bg-light"><label for="Name">Street</label></td>
    <td class="bg-light" style= "font-weight: 700; border-right: Solid 1px #e1e1e1;"><?php if(!empty($candidatedetails->presentstreet)) echo $candidatedetails->presentstreet;?></td>

    <td><label for="Name">H.No</label></td>
    <td style= "font-weight: 700"><?php if(!empty($candidatedetails->permanenthno)) echo $candidatedetails->permanenthno;?></td>
    <td><label for="Name">Street</label></td>
    <td style= "font-weight: 700"><?php if(!empty($candidatedetails->permanentstreet)) echo $candidatedetails->permanentstreet;?></td>
  </tr>
  <tr>
    <td class="bg-light"><label for="Name">State</label></td>
    <td class="bg-light" style= "font-weight: 700"><?php if(!empty($candidatedetails->present_state)) echo  $candidatedetails->present_state;?></td>
    <td class="bg-light"> <label for="Name">City</label> </td>
    <td class="bg-light" style= "font-weight: 700; border-right: Solid 1px #e1e1e1;"><?php if(!empty($candidatedetails->presentcity)) echo $candidatedetails->presentcity;?></td>
    <td><label for="Name">State</label></td>
    <td style= "font-weight: 700"><?php if(!empty($candidatedetails->parmanent_state)) echo $candidatedetails->parmanent_state;?></td>
    <td><label for="Name">City</label></td>
    <td style= "font-weight: 700"><?php if(!empty($candidatedetails->permanentcity)) echo $candidatedetails->permanentcity;?></td>
  </tr>
  <tr>
    <td class="bg-light"><label for="Name">District</label> </td>
    <td class="bg-light" style= "font-weight: 700"><?php if(!empty($candidatedetails->present_district)) echo $candidatedetails->present_district;?></td>
    <td class="bg-light"> <label for="Name">Pin Code</label></td>
    <td class="bg-light" style= "font-weight: 700 ; border-right: Solid 1px #e1e1e1;"><?php if(!empty($candidatedetails->presentpincode)) echo $candidatedetails->presentpincode;?></td>
    <td><label for="Name">District</label></td>
    <td style= "font-weight: 700"><?php if(!empty($candidatedetails->permanent_district)) echo $candidatedetails->permanent_district;?></td>
    <td><label for="Name">Pin Code</label></td>
    <td style= "font-weight: 700"><?php if(!empty($candidatedetails->permanentpincode)) echo $candidatedetails->permanentpincode;?></td>

  </tr>
</tbody>
</table>
</div>  
</div>
<?php if($count_education->Ecount2>0) {?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
    <table id="tbledubackground" class="table">
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8">Educational Background</th>
      </tr> 
    </thead>
    <tr>
      <th class="text-center" style="vertical-align: top;">Course</th>
      <th class="text-center" style="vertical-align: top;"> Board/ University</th>
      <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
      <th class="text-center" style="vertical-align: top;">Place</th>
      <th class="text-center" style="vertical-align: top;">Specialisation</th>
      <th class="text-center" style="vertical-align: top;">Year </th>
      <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
      <th class="text-center" style="vertical-align: top;">Document</th>
    </tr> 
  </thead>
  <tbody>
    <?php  //print_r($education_details); ?>
    <?php foreach ($education_details as $key ) {
      ?>
      <tr>
        <td class="text-center"><?php echo $key->level_name;?></td>
        <td class="text-center"><?php echo $key->university;?></td>
        <td class="text-center"><?php echo $key->collg_name;?></td>
        <td class="text-center"> <?php echo $key->place;?></td>
        <td class="text-center"><?php echo $key->stream;?></td>
        <td class="text-center"><?php echo $key->year_passing;?></td>
        <td class="text-center"><?php echo $key->percentage;?></td>
        <td class="text-center">
          <?php  
            if (!empty($key->encryptedcertificatename)) {
              $matriccertificateArray = explode("|", $key->encryptedcertificatename);
              $image_path = '';
              foreach($matriccertificateArray as $res1){
                $image_path='datafiles/educationalcertificate/'.$res1;
                if (file_exists($image_path)) {
          ?>
          <a href="<?php echo site_url().'datafiles\educationalcertificate/'.$res1;?>" download>
            <i class="fa fa-download" aria-hidden="true"></i>
          </a>
        <?php }else{ ?> 
      <a  href="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" download  ><i class="fa fa-download" aria-hidden="true"></i> </a>

   
     <?php } } } ?>
        </td>
      </tr>        
    <?php }?>       
  </tbody>
</table>
</div>
</div>

<?php } ?>

<?php  if($identitycount->Icount>0) {?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

   <table id="tblForm09" class="table" >
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="4">
      Identity Proof Details  </th>
    </tr> 
    <tr>
     <th style="text-align: center;">Identity</th>
     <th style="text-align: center;">Number</th>
     <th style="text-align: center;">document upload</th>


   </tr>
 </thead>
 <tbody id="tbodyForm13" >

  <?php 
//print_r($identitydetals);
  $i=0;
  foreach ($identitydetals as $key => $val) { $i++; ?>
    <tr id="tbodyForm13" style="text-align: center;" >
      <td>
        <?php  echo $val->name; ?>
      </td>
      <td><?php echo $val->identitynumber;?></td>
      
          <td> <a  href="<?php echo site_url().'datafiles/identitydocuments/'.
           $val->encryptedphotoname; ?>" download class="btn-primary">
           <i class="fa fa-download" aria-hidden="true"></i>
         </a></td>
    </tr>
  <?php } ?>
</tbody>

</table>
</div>
</div>
<?php } ?>

<?php if (count($gapyeardetals) > 0) { ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm09" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">
        Gap Year
      </th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top; text-align: center;">Sr.No </th>
      <th class="text-center" style="vertical-align: top; text-align: center;">From</th>
      <th class="text-center" style="vertical-align: top; text-align: center;">To </th>
      <th class="text-center" style="vertical-align: top; text-align: center; ">Reason</th>
    </tr> 
  </thead>
  <tbody id="bodytblForm10">
   <?php  $i=0;
   foreach ($gapyeardetals as $key => $val) {
    $i++; ?>
    <tr id="bodytblForm10" style="text-align: center;">
      <td><?php echo $i;?></td>
      <td> <?php echo $this->model->changedatedbformate($val->fromdate);?></td>
      <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
      <td><?php echo $val->reason; ?></td>
    </tr>
  <?php } ?>   
</tbody>
</table>
</div>
</div>
<?php   } ?>

<?php if (count($workexperiencedetails) > 0) { ?>

<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
  <table id="tblForm09" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8" >
       Work Experience 
     </th>
   </tr> 
   <tr>
    <th style="text-align: center;">Sr.No</th>
    <th style="text-align: center;">Organization Name</th>
    <th style="text-align: center;">Designation </th>
    <th style="text-align: center;">Duration</th>
    <th style="text-align: center;">Description of Assignment</th>
    <th style="text-align: center;">Place of Posting</th>
    <th style="text-align: center;">Last salary drawn (monthly)</th>
    <th style="text-align: center;">document upload</th>
  </tr> 
  <tr>
    <th colspan="4"></th>
    <th colspan="1" style="text-align: center;">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="4"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">
    <?php   
    $i=0;
    foreach ($workexperiencedetails as $key => $val) { 
      $i++;
      ?>
      <tr id="bodytblForm12" style="text-align: center;"> 
        <td><?php echo $i; ?></td>
        <td><?php echo $val->organization;  ?></td>
        <td><?php echo $val->designation;?></td>
        <td>
         <div class="col-lg-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <?php echo $this->model->changedatedbformate($val->fromdate);?>
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
            <?php echo $this->model->changedatedbformate($val->todate);?>
          </div> 
        </div>
      </td>
      <td><?php echo $val->reason_leaving;?></td>
      <td><?php echo $val->position;?></td>
      <td><?php echo $val->lastsalary;?></td>
     
      <td> Experience<a href="<?php echo site_url().'datafiles/workexperience/'.
                               $val->encrypteddocumnetname;?>" download >
                               <i class="fa fa-download  " aria-hidden="true"></i>
                             </a><br>
                            Salaryslip1 <a  href="<?php echo site_url().'datafiles/workexperience/'.
                             $val->encryptedsalaryslip1;?>" download >
                             <i class="fa fa-download  " aria-hidden="true"></i>
                           </a>
                           <br>
                             Salaryslip2<a  href="<?php echo site_url().'datafiles/workexperience/'.
                           $val->encryptedsalaryslip2;?>" download >
                           <i class="fa fa-download  " aria-hidden="true"></i>
                         </a>
                         <br>
                         Salaryslip3<a  href="<?php echo site_url().'datafiles/workexperience/'.
                         $val->encryptedsalaryslip3;?>" download >
                         <i class="fa fa-download  " aria-hidden="true"></i>
                       </a>








                           </td>
    </tr>
    <?php $i++; } ?>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if (count($trainingexposuredetals) > 0) { ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm09" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8" >
        Training  Exposure 
      </th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top; text-align: center;">Sr.No </th>
      <th class="text-center" style="vertical-align: top; text-align: center;">Nature of Training </th>
      <th class="text-center" style="vertical-align: top; text-align: center;">Organizing Agency</th>
      <th class="text-center" style="vertical-align: top; text-align: center;"> From Date</th>
      <th class="text-center" style="vertical-align: top; text-align: center; ">To Date</th>
    </tr> 
  </thead>
  <tbody id="bodytblForm10">
   <?php  $i=0;
   foreach ($trainingexposuredetals as $key => $val) { $i++; ?>
     <tr id="bodytblForm10" style="text-align: center;">
      <td><?php echo $i;?></td>
      <td><?php echo $val->Training_program;?></td>
      <td> <?php echo $val->organizing_agency;?></td>
      <td><?php echo $this->model->changedatedbformate($val->fromdate); ?></td>
      <td><?php echo $this->model->changedatedbformate($val->todate); ?></td>
    </tr>
  <?php } ?>   
</tbody>
</table>
</div>
</div>
<?php  } ?>

<?php if (count($languagedetalsprint) > 0) { ?>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm11" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">                  
        Language Skill/ProficiencyHave                 
      </th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Language</th>
      <th class="text-center" style="vertical-align: top;">Speak</th>
      <th class="text-center" style="vertical-align: top;">Read</th>
      <th class="text-center" style="vertical-align: top;">Write </th>
    </tr> 
  </thead>
  <tbody id="bodytblForm11">
    <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
    <?php $i= 0;
    foreach ($languagedetalsprint as $key => $val) {
      $i++; ?>
      <tr id="bodytblForm11">
        <td class="text-center">  <?php
        echo $val->lang_name;?> </td>
        <td class="text-center">
         <?php if($val->lang_speak=='H')
         { echo "High";
       }elseif ($val->lang_speak=='L') {
        echo "Low";
      }else{
        echo "Moderate";
      }  ?></td>
      <td class="text-center"><?php if($val->lang_read=='H')
      { echo "High";
    }elseif ($val->lang_read=='L') {
      echo "Low";
    }else{
      echo "Moderate";
    }  
    ?></td>
    <td class="text-center"><?php if($val->lang_write=='H')
    { echo "High";
  }elseif ($val->lang_write=='L') {
    echo "Low";
  }else{
    echo "Moderate";
  }  
  ?></td>
</tr>
<?php }  ?>
</tbody>
</table>
</div>
</div>
<?php } 

if(!empty($candidatedetails->any_subject_of_interest))
{
?>
  
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">Subject(s) of Interest </th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><?php if(!empty($candidatedetails->any_subject_of_interest)) echo $candidatedetails->any_subject_of_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } 
if(!empty($candidatedetails->any_achievementa_awards)) {
?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table">
    <thead>
      <tr class="bg-lightgreen text-white">
        <th colspan="8">Achievements /Awards (if any) </th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php if(!empty($candidatedetails->any_achievementa_awards)) echo $candidatedetails->any_achievementa_awards; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>
<?php if (count($dependmember)>0) {?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tblForm09" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">
        List of Dependent
      </th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top; text-align: center;">Sr.No </th>
      <th class="text-center" style="vertical-align: top; text-align: center;">Name</th>
      <th class="text-center" style="vertical-align: top; text-align: center;">Age </th>
      <th class="text-center" style="vertical-align: top; text-align: center; ">Relation With Employee</th>
    </tr> 
  </thead>
  <tbody id="bodytblForm10">
   <?php  $i=0;
   if(!empty($dependmember))
   {
   foreach ($dependmember as $key => $val) { 
    $i++; ?>
    <tr id="bodytblForm10" style="text-align: center;">
      <td><?php echo $i;?></td>
      <td> <?php if(!empty($val->name)) echo $val->name;?></td>
      <td><?php if(!empty($val->age))echo $val->age; ?></td>
      <td><?php if(!empty($val->relationname)) echo $val->relationname; ?></td>
    </tr>
  <?php } } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if(!empty($familymemberdetails) && count($familymemberdetails)>0)
 {?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <table id="tblForm09" class="table" >
    <thead>
     <tr class="bg-light">
      <th colspan="4">Family Members </th>
    </tr> 
    <tr>
      <th style="text-align: center;"> Sr.No</th>
      <th style="text-align: center;"> Name</th>
      <th style="text-align: center;">Relation with Employee</th>
    </tr>
  </thead>
  <tbody id="tbodyForm09" >
    <?php $i=0;
    if(!empty($familymemberdetails))
    {
    foreach ($familymemberdetails as $key => $val)
     {  $i++; ?>
      <tr id="tbodyForm09" style="text-align: center;">
        <td><?php echo $i; ?></td>
        <td><?php if(!empty($val->name)) echo $val->name ?></td>
        <td><?php if(!empty($val->relationname)) echo $val->relationname;?></td>
      </tr>
    <?php } } ?>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if (!empty($candidatedetails->any_assignment_of_special_interest)>0) {?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
   <table id="tblForm09" class="table">
    <thead>
     <tr class="bg-lightgreen text-white">
      <th colspan="8">Describe any assignment(s) of special interest undertaken by you</th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><?php  echo $candidatedetails->any_assignment_of_special_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>


<?php if(!empty($candidatedetails->experience_of_group_social_activities)) {?>

<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tblForm09" class="table">
      <thead>
       <tr class="bg-lightgreen text-white">
        <th colspan="8">Experience of Group and/or Social Activities </th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php  echo $candidatedetails->experience_of_group_social_activities; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

 
<?php if($this->loginData->RoleID==17 &&  $candidatedetails->flag==1)
{
  ?>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <table  class="table table-bordered">
      <thead>
       <tr class="bg-primary">
        <td colspan="8"><b>Verification </b>
        </td>
      </tr> 
    </thead>
    <tbody>
        <tr>
        <td>
          <div class="col-lg-12 col-md-12 col-sm-4 col-xs-4">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <label class="form-check-label" for="campus_placement_cell"><b>Status <span style="color: red;">*</span>: </b></label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <select name="status" id="status" class="form-control" required="required">
                <option value="">Select status</option>
                <option value="3">Approved</option>
                <option value="4">Rejected</option>
              </select>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div class="col-lg-12 col-md-12 col-sm-4 col-xs-4">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <label class="form-check-label" for="campus_placement_cell"><b>Comments <span style="color: red;">*</span> : </b></label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <textarea name="comments" id="comments" value="" class="form-control" ></textarea>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
<?php } ?>

<?php 

  $date = date('Y-m-d');

  if(!empty($candidatedetails->ugmigration_certificate_date)){
    $ugmaigrationdate_foropen = date('Y-m-d', strtotime('-7 day', strtotime($candidatedetails->ugmigration_certificate_date)));
    $ugmaigrationdate_forclose = date('Y-m-d',strtotime($candidatedetails->ugmigration_certificate_date));
  }else{
    $ugmaigrationdate_foropen = '';
    $ugmaigrationdate_forclose = '';
  }

  if(!empty($candidatedetails->pgmigration_certificate_date)){
    $pgmaigrationdate_foropen = date('Y-m-d', strtotime('-7 day', strtotime($candidatedetails->pgmigration_certificate_date)));
    $pgmaigrationdate_forclose = date('Y-m-d',strtotime($candidatedetails->pgmigration_certificate_date));    
  }else{
    $pgmaigrationdate_foropen = '';
    $pgmaigrationdate_forclose = '';
  }


  if(!empty($candidatedetails->ugdoc_certificate_date)){
    $ugdocdate_foropen = date('Y-m-d', strtotime('-7 day', strtotime($candidatedetails->ugdoc_certificate_date)));
    $ugdocdate_forclose = date('Y-m-d',strtotime($candidatedetails->ugdoc_certificate_date));
  }else{
    $ugdocdate_foropen = '';
    $ugdocdate_forclose = '';
  }
  if(!empty($candidatedetails->pgdoc_certificate_date)){
    $pgdocdate_foropen = date('Y-m-d', strtotime('-7 day', strtotime($candidatedetails->pgdoc_certificate_date)));
    $pgdocdate_forclose = date('Y-m-d',strtotime($candidatedetails->pgdoc_certificate_date));    
  }else{
    $pgdocdate_foropen = '';
    $pgdocdate_forclose = '';
  }

?>

<?php 
  if(($ugmaigrationdate_foropen <= $date) && ($ugmaigrationdate_forclose >= $date)){ ?>
    <div class="input-group col-xs-12">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label for="Name">UG Marks Sheet Upload</label>
      </div>
      <input type="file" name="ugmarkssheet" id="ugmarkssheet" class="file" accept="image/*" class="file">
    </div>
<?php
  }
  if(($pgmaigrationdate_foropen <= $date) && ($pgmaigrationdate_forclose >= $date)){?>
    <div class="input-group col-xs-12">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label for="Name">PG Marks Sheet Upload</label>
      </div>
      <input type="file" name="pgmarkssheet" id="pgmarkssheet" class="file" accept="image/*" class="file">
    </div>
<?php
  }
  if(($ugdocdate_foropen <= $date) && ($ugdocdate_forclose >= $date)){?>
    <div class="input-group col-xs-12">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label for="Name">UG Certificate Upload</label>
      </div>
      <input type="file" name="ugcertificate" id="ugcertificate" class="file" accept="image/*" class="file">
    </div>
<?php
  }
  if(($pgdocdate_foropen <= $date) && ($pgdocdate_forclose >= $date)){?>
    <div class="input-group col-xs-12">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label for="Name">PG Certificate Upload</label>
      </div>
      <input type="file" name="pgcertificate" id="pgcertificate" class="file" accept="image/*" class="file">
    </div>
<?php
  
  }

?>
<div class="row" style="padding: 15px;">
  <?php if($this->loginData->RoleID==17 && $candidatedetails->flag==1)
{?>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right panel-footer">
    <button type="submit" name="SubmitDatasend" value="SubmitData" id="saveDataWorkExpButton" class="btn btn-success text-right" onclick="return ifanyvalidation()">Verify</button>    
  
 <a data-toggle="tooltip" title = "Click here to move on staff list." href ="<?php echo base_url('Staff_list');?>"  id="sendtolist" class="btn btn-dark text-right">Go to list</a>
 </div>

<?php }else if($this->loginData->RoleID==3)
{
  ?>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right panel-footer">
  <a data-toggle="tooltip" title = "Click here to move on staff list." href ="<?php echo base_url('staff_dashboard');?>"  id="sendtolist" class="btn btn-dark text-right">Go to list</a>
 </div>
 <?php
}else if((($ugmaigrationdate_foropen <= $date) && ($ugmaigrationdate_forclose >= $date)) || (($pgmaigrationdate_foropen <= $date) && ($pgmaigrationdate_forclose >= $date)) || (($ugdocdate_foropen <= $date) && ($ugdocdate_forclose >= $date)) || (($pgdocdate_foropen <= $date) && ($pgdocdate_forclose >= $date))){?>
  
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right panel-footer">
    <button type="submit" name="SubmitDatasend" value="SubmitDataDocument" id="saveDataWorkExpButton" class="btn btn-success text-right" onclick="return ifanyvalidation()">Save</button> 
    <a data-toggle="tooltip" title = "Click here to move on staff list." href ="<?php echo base_url('staff_dashboard');?>"  id="sendtolist" class="btn btn-dark text-right">Go to list</a>
  </div>
<?php 
  }else { ?>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right panel-footer">
  <a data-toggle="tooltip" title = "Click here to move on staff list." href ="<?php echo base_url('Staff_list');?>"  id="sendtolist" class="btn btn-dark text-right">Go to list</a>
 </div>
<?php } ?>

</div>
</div>
</div>
</div>
</form>
<!-- End  Candidates Experience Info -->
<script type="text/javascript">
   function ifanyvalidation(){

    var natureoftraining    = $('#natureoftraining').val();
    var organizingagency    = $('#organizingagency').val();
    var fromdate            = $('#fromdate').val();
    var todate              = $('#todate').val();
    var orgname             = $('#orgname').val();
    var descriptionofassignment = $('#descriptionofassignment').val();
    var designation         = $('#designation').val();
    var lastsalarydrawn     = $('#lastsalarydrawn').val();
    var pgupstream          = $('#pgupstream').val();
    var other               = $('#other').val();
    var work_experience_fromdate = $('#work_experience_fromdate').val();
    var work_experience_todate = $('#work_experience_todate').val();
    var palceofposting = $('#palceofposting').val();
    var experiencedocument = $('#experiencedocument').val();
    var oldexperiencedocument = $('#oldexperiencedocument').val();
    var gapfromdate = $('#gapfromdate').val();
    var gaptodate = $('#gaptodate').val();
    var gapreason = $('#gapreason').val();

    if (pgupstream !='') {
      if (pgcertificate =='') {
       $('#pgcertificate').focus();
       return false;
     }
   }

   if (other !='') {
    if (otherscertificate =='') {
     $('#otherscertificate').focus();
     return false;
   }
 }

 if (natureoftraining !='') {
  if (organizingagency =='') {
   $('#organizingagency').focus();
   return false;
 }
 if (fromdate =='') {
   $('#fromdate').focus();
   return false;
 }
 if (todate =='') {
   $('#todate').focus();
   return false;
 }

}


if (gapfromdate !='') {
  if (gaptodate =='') {
   $('#gaptodate').focus();
   return false;
 }
 if (gapreason =='') {
   $('#gapreason').focus();
   return false;
 }

}

if (orgname !='') {

  if (designation =='') {
   $('#designation').focus();
   return false;
 }

 if (descriptionofassignment =='') {
   $('#descriptionofassignment').focus();
   return false;
 }
 if (work_experience_fromdate =='') {
   $('#work_experience_fromdate').focus();
   return false;
 }

 if (work_experience_todate =='') {
   $('#work_experience_todate').focus();
   return false;
 }
 if (palceofposting =='') {
   $('#palceofposting').focus();
   return false;
 }
 if (lastsalarydrawn =='') {
  $('#lastsalarydrawn').focus();
  return false;
}
if (oldexperiencedocument =='') {
 $("#experiencedocument").focus();
 return false;
}
}
}
</script>


