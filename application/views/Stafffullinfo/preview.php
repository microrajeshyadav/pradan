<br>
<style type="text/css">
  label{

  font-weight: bold;
  font-family: 
}
.bg-lightgreen
{
  background-color: #85a4a5;
}

</style>
<div class="container" id="PrintContainer" style="background-color: #FFFFFF; width: 900px; "  >
  <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
    <input type="hidden" name="candidateid" value="<?php echo $token;?>">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <table id="tbltrainingexposure" class="table table-bordered table-striped">
        <thead>
         <tr>
          <th  colspan="3" class="bg-lightgreen text-white">Personal Infomation</th>
        </tr> 
      </thead>
      <tbody>
        <tr>
          <td colspan="3">
           <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right">

                                <img src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" width="80px" height="80" title="" data-toggle="tooltip" alt="" >
                              </div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">First name :</label></div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->candidatefirstname;?></div>

             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Middle name :</label></div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->candidatemiddlename;?></div>

             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Last Name :</label></div>
             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->candidatelastname;?></div>

           </div>

           <div class="row">      
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Mother's name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->motherfirstname;?></div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Middle name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->mothermiddlename;?></div>

            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> <label for="Name">Last Name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left"><?php echo $candidatedetails->motherlastname;?></div>
          </div>
          <div class="row">
           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <label for="Name">Father's name :</label></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <?php echo $candidatedetails->fatherfirstname;?></div>


              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <label for="Name">Middle name :</label></div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <?php echo $candidatedetails->fathermiddlename;?>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <label for="Name">Last name :</label></div>
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <?php echo $candidatedetails->fatherlastname;?> 
                  </div>
                </div>
                <br>
                <div class="row">
                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <label for="Name">Gender </label></div>
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <?php 
                    if ($candidatedetails->gender ==1) {
                      echo "Male";
                    }else if ($candidatedetails->gender ==1){
                     echo "Female";
                   }else{
                    echo "Other";
                   } ?>

                 </div>

                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <label for="Name">Nationality  </label> </div>
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <?php echo  $candidatedetails->nationality;  ?> </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                      <label for="Name">Marital Status </label></div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                       <?php 
                        if ($candidatedetails->maritalstatus ==1) {
                          echo "Single";
                        }else if ($candidatedetails->maritalstatus ==2) {
                         echo "Married";
                        }else if ($candidatedetails->maritalstatus ==3) {
                         echo "Divorced";
                        }else if ($candidatedetails->maritalstatus ==4) {
                         echo "Widow";
                        }else if ($candidatedetails->maritalstatus ==5) {
                         echo "Separated";
                        }
                       ?>
                     </div>
                   </div>
                   <br>
                  
                   <div class="row">
                     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                      <label for="Name">Annual income of parents</label></div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <?php 
                          if ($otherinformationdetails->annual_income==1) {
                           echo "Below 50,000";
                          }else if ($otherinformationdetails->annual_income==2) {
                            echo "50,001-200,000";
                          }else if ($otherinformationdetails->annual_income==3) {
                            echo "200,001-500,000";
                          }else if ($otherinformationdetails->annual_income==4) {
                             echo "Above 500,000";
                          }
                        ?></div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                          <label for="Name">No. of male sibling </label></div>
                          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <?php echo $otherinformationdetails->male_sibling;?></div>

                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                              <label for="Name">No. of female sibling </label></div>
                              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <?php echo $otherinformationdetails->female_sibling;?>
                              </div>
                            </div>   
                   <br>
                   <div class="row">
                     <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                      <label for="Name">Date Of Birth </label></div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <?php echo $this->model->changedate($candidatedetails->dateofbirth);?></div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                          <label for="Name">Email Id </label></div>
                          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <?php echo $candidatedetails->emailid;?></div>

                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                              <label for="Name">Mobile No .</label></div>
                              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <?php echo $candidatedetails->mobile;?>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                             <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                              <label for="Name">Blood Group</label></div>
                              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                               <?php echo $candidatedetails->bloodgroup;?></div>

                               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left">

                                <img src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" width="80px" height="80" title="" data-toggle="tooltip" alt="" >
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table id="tbledubackground" class="table table-bordered table-striped" >
                      <thead>
                       <tr>
                        <th colspan="8" class="bg-lightgreen text-white"> 
                          <div class="form-check">
                            <label class="form-check-label" for="filladdress"><b>Communication Address </b></label>
                          </div>
                        </th>
                      </tr> 
                      <tr>
                        <th class="text-center bg-light" colspan="4" style="vertical-align: top;">Present Mailing Address</th>
                        <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Mailing Address<br>
                           <div class="form-check">
                        <!-- <input type="checkbox" class="form-check-input" checked="checked" id="filladdress"> 
                        <label class="form-check-label" for="filladdress"><b>same as present address</b></label> -->
                      </div></th>
                      </tr> 
                    </thead>

                    <tbody>
      <tr>
        <td class="bg-light"><label for="Name">H.No<span style="color: red;" >*</span></label></td>
        <td class="bg-light"><?php echo $candidatedetails->presenthno;?></td>
        <td class="bg-light"><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><?php echo $candidatedetails->presentstreet;?>
          <input type="hidden" name="presentstreet" id="presentstreet"  
                  value=""<?php echo $candidatedetails->presentstreet;?>  ></td>
        <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
        <td><?php echo $candidatedetails->permanenthno;?></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><?php echo $candidatedetails->permanentstreet;?>
                        
                        <input type="hidden" name="permanentstreet"  id="permanentstreet" value="<?php echo $candidatedetails->permanentstreet;?>"></td>
        </tr>
         <tr>
        <td class="bg-light"><label for="Name">City<span style="color: red;" >*</span></label> </td>
        <td><?php echo $candidatedetails->presentcity;?></td>

        <td class="bg-light"><label for="Name">State<span style="color: red;" >*</span></label> </td>
        <td class="bg-light"><?php echo  $candidatedetails->statename; ?> </td>

        
        <td><label for="Name">City<span style="color: red;" >*</span></label></td>
        <td><?php echo $candidatedetails->permanentcity;?></td>

        <td><label for="Name">State<span style="color: red;" >*</span></td>
        <td><?php echo $candidatedetails->statename; ?></td>
        </tr>
         <tr>
          <td class="bg-light"><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td class="bg-light"> <?php echo $candidatedetails->districtname;?></td>
        <td class="bg-light"><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
         <td class="bg-light"><?php echo $candidatedetails->presentpincode;?></td>
        <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td><?php echo $candidatedetails->districtname;?> </td>

        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><?php echo $candidatedetails->permanentpincode;?></td>
        </tr>
  </tbody>
                  
            </table>
          </div>  
        </div>
        <?php if ($familycount->Fcount != 0){ ?> 
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <table id="tblForm09" class="table table-bordered table-striped" >
              <thead>
               <tr>
                <th colspan="4" class="bg-lightgreen text-white">Family Members </th>
              </tr> 
              <tr>
               <th>Name</th>
               <th>Relation with Employee</th>
               <th>Date of Birth</th>
               <th>Photo upload</th>
             </tr>
           </thead>
           <tbody id="tbodyForm09" >
            <?php $i=0;
            foreach ($familymemberdetails as $key => $val) {  $i++; ?>
              <tr id="tbodyForm09">

               <td><?php echo $val->Familymembername ?></td>
               <td>
                 <?php echo $val->relationname;?>

               </td>
               <td><?php echo $this->model->changedate($val->familydob);?></td>
               <td>
                 <div class="form-group">
                  <input type="hidden" name="oldfamilymemberphoto[]" id="oldfamilymemberphoto" value="<?php echo $val->encryptedphotoname; ?>">
                  <a  href="<?php echo site_url().'datafiles/familymemberphoto/'.
                  $val->encryptedphotoname; ?>" download class="btn-primary">
                  <i class="fa fa-download" aria-hidden="true"></i>
                </a>
              </div>
            </td>

          </tr>
        <?php }  ?>
      </tbody>

    </table>
  </div>
</div>
<?php } ?>
<?php if ($identitycount->Icount !=0) { ?>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

     <table id="tblForm13" class="table table-bordered table-striped" >
      <thead>
       <tr>
        <th colspan="4" class="bg-lightgreen text-white">
          <div class="col-lg-12">
            <div class="col-lg-6">Identity Details</div>
            <div class="col-lg-6 text-right ">

            </div>
          </div>  </th>
        </tr> 
        <tr>
         <th>Identity</th>
         <th>Number</th>
         <th>Photo upload</th>
       </tr>
     </thead>
     <tbody id="tbodyForm13" >

      <?php $i=0;
      foreach ($identitydetals as $key => $val) { $i++; ?>
        <tr id="tbodyForm13" >
          <td>
            <?php  echo $val->name; ?>

          </td>

          <td><?php echo $val->identitynumber;?></td>
          <td>

            <div class="form-group">
              <input type="hidden" name="oldidentityphoto[]" id="oldidentityphoto" value="<?php echo $val->encryptedphotoname;?>">
              <a  href="<?php echo site_url().'datafiles/identitydocuments/'.$val->encryptedphotoname; ?>" download class="btn-primary">
                <i class="fa fa-download" aria-hidden="true"></i>
              </a>
            </div>
          </td>
        </tr>
      <?php } ?>
    </tbody>

  </table>
</div>
</div>
<?php }?>
<?php //print_r($candidatedetails); ?>
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;">

  <table id="tbledubackground" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Educational Background</th>
    </tr> 
    <tr>
      <th class="text-center" style="vertical-align: top;">Course</th>

      <th class="text-center" style="vertical-align: top;">Year </th>
        <th class="text-center" style="vertical-align: top;"> Board/ University</th>
      <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
     <th class="text-center" style="vertical-align: top;">Place</th>
      <th class="text-center" style="vertical-align: top;">Specialisation</th>
     
      <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
      <th class="text-center" style="vertical-align: top;">upload</th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><b>10th</b></td>
      <td><?php echo $candidatedetails->metricpassingyear;?>
    </td>
     <td> <?php echo $candidatedetails->metricboarduniversity;?></td>
    <td>  <?php echo $candidatedetails->metricschoolcollege;?></td>

 <td><?php echo $candidatedetails->metricplace;?>
</td>
<td><?php echo $candidatedetails->metricspecialisation;?>
</td>


<td><?php echo $candidatedetails->metricpercentage;?> </td>

<td>
  <?php if (!empty($candidatedetails->encryptmetriccertificate)) { ?>                 
    <div class="form-group">
      <input type="hidden" name="oldmatriccertificate" id="oldmatriccertificate" value="<?php echo $candidatedetails->encryptmetriccertificate; ?>">
      <a  href="<?php echo site_url().'datafiles/educationalcertificate/'.
      $candidatedetails->encryptmetriccertificate; ?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
    </div>
  <?php } ?>

</td>
</tr>
<tr>
  <td><b><?php if ($candidatedetails->hscstream ==1) {
      echo "12th";
  }else{
    echo "Deploma";
  } ?></b> </td>
  <td><?php echo $candidatedetails->hscpassingyear;?>
</td>
<td> <?php  echo $candidatedetails->hscboarduniversity;?></td>

<td>  <?php echo $candidatedetails->hscschoolcollege;?></td>

<td><?php echo $candidatedetails->hscplace;?></td>

<td><?php echo $candidatedetails->hscspecialisation;?></td>
<td><?php echo $candidatedetails->hscpercentage;?></td>
<td> 
 <?php if (!empty($candidatedetails->encrypthsccertificate)) { ?> 
  <a  href="<?php echo site_url().'datafiles/educationalcertificate/'.
  $candidatedetails->encrypthsccertificate; ?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
  <?php }?> </td>
</tr>
<tr>
  <td><b>UG</b></td>

  <td><?php echo $candidatedetails->ugpassingyear; ?>
</td>
<td> <?php echo $candidatedetails->ugboarduniversity;?></td>
<td>
  <?php echo $candidatedetails->ugschoolcollege;?>

</td>
<td><?php echo $candidatedetails->ugplace;?></td>



<td>
 <?php echo  $candidatedetails->ugspecialisation;  ?>
</td>

<td><?php echo $candidatedetails->ugpercentage;?>
</td>
<td>
  <?php if (!empty($candidatedetails->encryptugcertificate)) { ?> 

    <a  href="<?php echo site_url().'datafiles/educationalcertificate/'.$candidatedetails->encryptugcertificate; ?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
  <?php } ?>

</td>
</tr>
<?php if (!empty($candidatedetails->pgpassingyear) && $candidatedetails->pgpassingyear !=0) {?>
  <tr>
    <td><b>PG</b></td>

    <td><?php echo $candidatedetails->pgpassingyear;?>
  </td>

<td> <?php echo $candidatedetails->pgboarduniversity;?></td>
  <td>  
    <?php echo $candidatedetails->pgschoolcollege;?>

  </td>

  <td><?php echo $candidatedetails->pgplace;?></td>
<td>

  <?php  echo $candidatedetails->pgspecialisation;  ?></td>




<td><?php echo $candidatedetails->pgpercentage;?>
</td>
<td>

  <?php if (!empty($candidatedetails->encryptpgcertificate)) { ?>


    <a  href="<?php echo site_url().'datafiles/educationalcertificate/'.
    $candidatedetails->encryptpgcertificate; ?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
  <?php  } ?>


</td>
</tr>
<?php } ?>
<?php if (!empty($candidatedetails->other_degree_specify) && $candidatedetails->other_degree_specify !='') {?>
  <tr>
    <td><b><?php echo $candidatedetails->other_degree_specify; ?></b></td>

    <td> <?php echo $candidatedetails->otherpassingyear; ?> </td>
    <td>  <?php echo $candidatedetails->otherboarduniversity; ?> </td>
    <td> <?php echo $candidatedetails->otherschoolcollege; ?>  </td>

    
    <td><?php echo $candidatedetails->otherplace; ?></td>

    <td> <?php echo $candidatedetails->otherspecialisation; ?> </td>

    <td> <?php echo $candidatedetails->otherpercentage; ?> </td>
    <td>

     <?php if (!empty($candidatedetails->encryptothercertificate)) { ?>


      <a  href="<?php echo site_url().'datafiles/educationalcertificate/'.
      $candidatedetails->encryptothercertificate; ?>" download ><i class="fa fa-download" aria-hidden="true"></i> </a>
    <?php } ?>

  </td>
</tr>
<?php } ?>
</tbody>
</table>
</div>
</div>

<?php if ($GapYearCount->GYcount !=0) {   ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm10" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">

       <div class="col-lg-12">
        <div class="col-lg-6 text-left"> Gap Year</div>
        <div class="col-lg-6 text-right ">
        </div>
      </div>
    </th>
  </tr> 
  <tr>
    <th class="text-center" style="vertical-align: top;">From Date </th>
    <th class="text-center" style="vertical-align: top;">To Date</th>
    <th class="text-center" style="vertical-align: top;"> Reason</th>
  </tr> 
</thead>
<tbody id="bodytblForm10">
 <?php  $i=0;
 foreach ($gapyeardetals as $key => $val) { $i++; ?>
  <tr id="bodytblForm10">
    <td><?php echo $this->model->changedate($val->fromdate); ?></td>
    <td><?php echo $this->model->changedate($val->todate); ?></td>
     <td><?php echo $val->reason;?></td>
  </tr>
<?php } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>

<?php   if ($TrainingExpcount->TEcount !=0) {   ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm10" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">

       <div class="col-lg-12">
        <div class="col-lg-6 text-left"> Training  Exposure (if any)</div>
        <div class="col-lg-6 text-right ">
        </div>
      </div>
    </th>
  </tr> 
  <tr>

    <th class="text-center" style="vertical-align: top;">Nature of Training </th>
    <th class="text-center" style="vertical-align: top;">Organizing Agency</th>
    <th class="text-center" style="vertical-align: top;"> From Date</th>
    <th class="text-center" style="vertical-align: top;">To Date</th>
  </tr> 
</thead>
<tbody id="bodytblForm10">
 <?php  $i=0;
 foreach ($trainingexposuredetals as $key => $val) { $i++; ?>
  <tr id="bodytblForm10">
    <td><?php echo $val->natureoftraining;?></td>
    <td> <?php echo $val->organizing_agency;?></td>
    <td><?php echo $this->model->changedate($val->fromdate); ?></td>
    <td><?php echo $this->model->changedate($val->todate); ?></td>
  </tr>
<?php } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if ($languageproficiency->Lcount !=0) { ?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm11" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> 

       <div class="col-lg-12">
        <div class="col-lg-6 text-left">Language Skill/Proficiency </div>
        <div class="col-lg-6 text-right ">

        </div>
      </div>
    </th>
  </tr> 
  <tr>

    <th class="text-center" style="vertical-align: top;">Language</th>
    <th class="text-center" style="vertical-align: top;">Speak</th>
    <th class="text-center" style="vertical-align: top;">Read</th>
    <th class="text-center" style="vertical-align: top;">Write </th>
  </tr> 
</thead>
<tbody id="bodytblForm11">
  <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">


  <?php $i= 0;

  foreach ($languagedetalsprint as $key => $val) {
    $i++; ?>
    <tr id="bodytblForm11">
      <td>  <?php
      echo $val->lang_name;?> </td>
    <td>
     <?php if($val->lang_speak=='H')
     { echo "High";
   }elseif ($val->lang_speak=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  ?></td>
   <td><?php if($val->lang_read=='H')
     { echo "High";
   }elseif ($val->lang_read=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  
    ?></td>
 <td><?php if($val->lang_write=='H')
     { echo "High";
   }elseif ($val->lang_write=='L') {
      echo "Low";
   }else{
     echo "Moderate";
   }  
   ?></td>
</tr>
<?php }   ?>
</tbody>
</table>
</div>
</div>
<?php  } ?>
<?php if ($otherinformationdetails->any_subject_of_interest !='') { ?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Subject(s) of Interest </th>
    </tr> 
  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_subject_of_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php }  ?> 
<?php if($otherinformationdetails->any_achievementa_awards !='') { ?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Achievements /Awards (if any) </th>
    </tr> 

  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_achievementa_awards; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>

<?php } ?>

<?php if ($WorkExperience->WEcount != 0) {?>
  <div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm12" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="7" class="bg-lightgreen text-white">
       <div class="col-lg-12">
        <div class="col-lg-6 text-left">Work Experience (if any) </div>
        <div class="col-lg-6 text-right ">

        </div>
      </div>
    </th>
  </tr> 
  <tr>
    <th>Organization Name</th>
    <th>Designation </th>
    <th>Description of Assignment</th>
    <th>Duration</th>
    <th>Place of Posting</th>
    <th>Last salary drawn (monthly)</th>
    <th>Upload</th>
  </tr> 
  <tr>
    <th colspan="3"></th>
    <th colspan="1">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="3"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">

    <?php   
    $i=0;
    foreach ($workexperiencedetails as $key => $val) {   ?>
      <tr id="bodytblForm12"> 
        <td><?php echo $val->organizationname;  ?></td>
        <td><?php echo $val->designation;?></td>
        <td><?php echo $val->descriptionofassignment;?></td>
        <td>
         <div class="col-lg-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
            <?php echo $this->model->changedate($val->fromdate);?>
          </div> 
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
           <?php echo $this->model->changedate($val->todate);?>
         </div> 
       </div>
     </td>
     <td><?php echo $val->palceofposting;?></td>
     <td><?php echo $val->lastsalarydrawn;?></td>
     <td>  <a  href="<?php echo site_url().'datafiles/workexperience/'.
     $val->encrypteddocumnetname;?>" download > <i class="fa fa-download" aria-hidden="true"></i> </a>
   </td>

 </tr>

 <?php $i++; } ?>
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if ($otherinformationdetails->any_assignment_of_special_interest !='') {?>
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">Describe any assignment(s) of special interest undertaken by you</th>
    </tr> 

  </thead>
  <tbody>
    <tr>
      <td><?php echo $otherinformationdetails->any_assignment_of_special_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if ($otherinformationdetails->experience_of_group_social_activities !='') { ?>
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tbltrainingexposure" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white">Experience of Group and/or Social Activities </th>
      </tr> 
    </thead>
    <tbody>
      <tr>
        <td><?php echo $otherinformationdetails->experience_of_group_social_activities; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

<?php if (!empty($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before)) { 
  ?> 
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tblgap" class="table table-bordered table-striped">
  <thead>
   <tr>
    <td colspan="8" class="bg-lightgreen text-white"><b>Have you taken part in PRADAN's selection process before?</b>
  </td>
</tr> 
</thead>
<tbody>
  <tr>
    <td>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 

        <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='yes') {
          echo 'Yes';
        }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='no'){
        echo 'No';
        }
       ?>
</div>
    
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <?php echo $this->model->changedate($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when); ?>
        
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
        <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==1) {
           echo 'ON Campus';
          }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==2) {
            echo 'Off Campus';
          } ?>
        
      </div>
        
    </td>
    </tr>
  </tbody>
</table>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      
        $("input[name$='pradan_selection_process_before']").load(function() {
        var selected = $(this).val();
       
        if (selected =='yes') {
          $("#when").removeAttr('disabled','false'); 
          $("#where").removeAttr('disabled','false');
        }else{
          $("#when").prop('disabled','true'); 
          $("#where").prop('disabled','true');
        }
       
    });

      $("input[name$='pradan_selection_process_before']").click(function() {
        var selected = $(this).val();
       
        if (selected =='yes') {
          $("#when").removeAttr('disabled','false'); 
          $("#where").removeAttr('disabled','false');
        }else{
          $("#when").prop('disabled','true'); 
          $("#where").prop('disabled','true');
        }
       
    });

});
</script>
<?php } ?>

<?php if (!empty($otherinformationdetails->know_about_pradan)) {  ?>
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
 <table id="tbltrainingexposure" class="table table-bordered table-striped">
  <thead>
   <tr>
    <td colspan="8" class="bg-lightgreen text-white"><b>From where have you come to know about PRADAN?</b>
    </td>
  </tr> 
</thead>
<tbody>
  <tr>
    <td>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
        <?php 
        if ($otherinformationdetails->know_about_pradan=='CampusPlacementCell') {
         echo 'Campus Placement Cell';
        } else if ($otherinformationdetails->know_about_pradan=='UniversityProfessors') {
          echo 'University Professors';
        }else if ($otherinformationdetails->know_about_pradan=='CampusAlumni') {
          echo 'Campus Alumni';
        }else if ($otherinformationdetails->know_about_pradan=='friends') {
          echo 'Friends';
        }else if ($otherinformationdetails->know_about_pradan=='other') {
          echo $otherinformationdetails->know_about_pradan_other_specify;
        }
      ?> 
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $("input[name$='have_you_come_to_know']").click(function() {
        var test = $(this).val();
        if (test =='Other') {
          $("#other_have_you_come_to_know").show();
          $("#specify").prop('required','required');
           $('#specify').removeAttr("disabled");
         
        }else{
          $("#other_have_you_come_to_know").hide();
          $("#specify").prop('disabled','true');

        }
       
    });

});
</script>
<?php } ?>


<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: #ffffff;">
 <a href="<?php echo site_url().'Stafffullinfo/edit/'.$token; ?>" class="btn btn-primary" >Edit</a>
  <button type="submit" name="SaveDatasend" value="SaveData" id="sendDataWorkExpButton" class="btn btn-success text-center" >Submit</button>
<!-- <a href="<?php //echo site_url().'Stafffullinfo/bdfformsubmit'; ?>" class="btn btn-success" >Submit</a> -->

 <br><br>
</div>
</div>
</form>
</div>
<!-- End  Candidates Experience Info -->
</div>
</div>

