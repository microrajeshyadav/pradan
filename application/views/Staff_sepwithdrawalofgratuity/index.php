<style type="">
input[type='text']:disabled
{

  background: #fff !important;

}
textarea:disabled
{

  background: #fff !important;
  
}
textarea{
  width: 600px !important;
}


</style>
<section class="content" style="background-color: #FFFFFF;" >
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <form role="form" method="post" action="">
        <br>
        <div class="container-fluid">
         <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
         <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - WITHDRAWAL OF GRATUITY
             </h4>
           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">
           <div class="row">
             <div class="col-md-12 text-center">
              <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
            </div>
            <div class="col-md-12 text-center" style="margin-bottom: 40px;">
              <h4>APPLICATION FOR WITHDRAWAL OF GRATUITY </h4>
              
            </div> 
          </div>
          <div class="row" style="line-height: 2">
            <div class="col-md-9">
              To:<br><label  name="employee_name"  class="inputborderbelow" > The Chairperson PRADAN Gratuity Trust PRADAN  <br>
               E-1/A, Kailash Colony <br> New Delhi 110048 </label> <br/>
             
            </div>
            <div class="col-md-3 text-left">
              Date: <label  name="date"  class="inputborderbelow" ><?php echo date('d/m/Y')  ?></label>
            </div>
          </div>
          

      
          <div class="row" style="line-height: 2;margin-top: 50px;">
            <div class="col-md-12 text-left">
             Subject  : <strong>Application for Withdrawal of Gratuity  </strong>
           </div>
           
         </div>
         <div class="row" style="margin-top: 50px;">
           <div class="col-md-12 text-left">
            Dear <input type="text" name="name" required="" maxlength="20" minlength="2" class="inputborderbelow" placeholder="Enter name" value="<?php echo $gratuity_withdraw->sir;?>">, <br>
            <br>
            <p>I, Ms./Mr.<input type="text" name="declarationplace" required="" maxlength="20" minlength="2" class="inputborderbelow"  value="<?php echo $staff_detail->name;?>">(Name),<input type="text" name="empoy_code" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter Place" value="<?php echo $staff_detail->emp_code;?>"> (Employee Code), joined on <input type="text" name="declarationplace" required="" maxlength="20" minlength="2" class="inputborderbelow" placeholder="Enter Place" value="<?php echo $this->gmodel->changedatedbformate($staff_detail->joiningdate);?>"> (Date) had resigned from PRADAN on <input type="text" name="declarationplace" required="" maxlength="20" minlength="2" class="inputborderbelow" placeholder="Enter Place" value="<?php echo $staff_detail->joindesig;?>"> (Date).  I am requesting you to release my Gratuity to the below written bank account. 
 
My Bank Details are as follows: .

            </p> 
            <p>Bank Name: <input type="text" name="bank_name" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter Place" value="<?php echo $gratuity_withdraw->bankname;?>"> <br><br>
Address      : <textarea name="b_address" required=""  class="inputborderbelow" placeholder="Enter Address"><?php echo $gratuity_withdraw->bankaddress;?></textarea> <br><br>
Account Number: <input type="text" name="account_number" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter Account Number" value="<?php echo $gratuity_withdraw->accountnumber;?>"> <br><br>
 
 </p>

            <p>Yours sincerely, </p>
            <p>
             Name: <input type="text" name="staff_name" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter Staff Name" value="<?php echo $staff_detail->name;?>"><br><br>
             Correspondence Address (BLOCK letter):<textarea name="address" required="" maxlength="20" minlength="2" class="inputborderbelow" placeholder="Enter Address"><?php echo $leave_detail->accountnumber; ?><?php echo $staff_detail->address;?></textarea> <br><br> <br><br> <br><br>
             Pin code: <input type="text" name="pincode" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter Pincode" value="<?php echo $staff_detail->permanentpincode;?>"> <br><br>
             Email Address (BLOCK letter): <input type="text" name="emal" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter Email" value="<?php echo $staff_detail->emailid;?>"><br><br>
             Phone No.:<input type="text" name="phone_no" required="" maxlength="20" minlength="2" class="  inputborderbelow" placeholder="Enter contect No" value="<?php echo $staff_detail->contact;?>"> <br><br>
            </p>
           


        </div>
        
      </div>

<div class="panel-footer text-right">
  <?php if(empty($gratuity_withdraw)){
                  ?>
  
                  <button  type="submit" name="Save"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                  <button  type="submit" name="saveandsubmit"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
                  <?php 

                }
                else if($gratuity_withdraw->flag == 0){

                  ?>
                  <button  type="submit" name="saveandsubmit"  id="saveandsubmit" value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
                  <?php 
                }
                else if($gratuity_withdraw->flag == 1)
                {
                  ?>
                  <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
                  <?php } ?>
   </div>
 </div>
</div>
</form>
</section>

<?php 
if($staff_sepemployeeexitform){
  if($staff_sepemployeeexitform->flag == 1){ ?>
  <script type="text/javascript">
    $(document).ready(function(){
     
      $('#saveandsubmit').hide() ;
      $("form input[type=text]").prop("disabled", true);

      $("form input[type=radio]").prop("disabled", true);
      $("form textarea").prop("disabled", true);
      $("form textarea").css("width", "600px;");
      $("form textarea").attr("style", "width:600px;");
    });
  </script>
  <?php } }?>