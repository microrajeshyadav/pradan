
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">      
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
          <h4 class="col-md-10 panel-title pull-left">Recommend to Graduate</h4>
          <div class="col-md-2 text-right"></div>
        </div>
        <hr class="colorgraph"><br>
      </div>
      <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?> 
            <?php //print_r($getprobationdetails); die; ?>
            <?php echo form_open();?>
            <div class="row" style="background-color: #FFFFFF;">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              <div class="col-lg-6 col-md-6"></div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                <table id="tableprodationsepration" class="table table-bordered table-striped dt-responsive wrapper">
                  <thead>
                    <tr>
                      <th>S.No.</th>
                      <!-- <th>Campus Name</th> -->
                      <th>DA Code</th>
                      <th>Name</th>
                      <th>Email ID</th>
                      <th>Gender</th>
                      <th class="text-center">Summary Report</th>
                      <?php  if ($designation->desid != 16) { ?>
                      <th class="text-center">Change Team</th>
                      <?php } ?>
                      <th class="text-center">Offer Letter</th>
                      <th class="text-center">Personnel Administrator</th>
                      <th class="text-center">Send for ED Approval</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php
                    if(isset($recommend_to_graduate_list))  
                    {
                      if (!empty($recommend_to_graduate_list))
                      {
                        $count = 1;
                        // print_r($recommend_to_graduate_list); die;
                        foreach($recommend_to_graduate_list as $row)
                        { 
            //$count = + 1  
                          ?>            
                          <tr>
                            <input type="hidden" name="staffid" value="<?php echo $row->staffid;?>">
                            <input type="hidden" name="id" value="<?php echo $row->id;?>">
                            <td><?php echo $count++; ?></td>
                            <!-- <td><?php echo $row->campusname; ?></td>           -->
                            <td><?php echo $row->emp_code; ?></td>
                            <td > <a class="btn btn-info btn-sm" title="Click here to open staff profile." data-toggle="tooltip" href="<?php echo site_url().'Hrscore/preview/'.$row->candidateid;?>" ><i class="fas fa-address-card"></i>&nbsp;<?php echo $row->name; ?></a>
                            </td>
                            <td><?php echo $row->emailid; ?></td>
                            <td><?php echo $row->gender; ?></td>
                            <td class="text-center"> 
                            <?php if(!empty($row->transid)) { ?>
                              <a class="btn btn-info btn-sm" title="Click here to open summary report." data-toggle="tooltip" href="<?php echo site_url().'Proposed_probation_separation/summary/'.$row->transid;?>"><i class="fas fa-book-reader"></i> </a>
                            <?php } ?>
                            </td>
                             <?php if ($designation->desid != 16) { ?>
                            <td class="text-center"> 
                              <a class="btn btn-success btn-sm" title="Change Team" href="<?php echo site_url().'Assigntc/edit/'.$row->candidateid.'/2';?>" ><i class="fas fa-user-tag"></i>  </a>
                            </td>
                            <?php } ?>
                            

                            <td class="text-center">
                              <?php if ($row->flag == 'Generate' && $designation->desid==16) { ?>  
                              <a class="btn btn-success btn-sm" href="<?php echo site_url().'pdf_offerletters/'.$row->filename.'.pdf';?>"  download data-toggle="tooltip" title="Download offer letter">
                                <i class="fas fa-file-download"></i>  
                              </a>


                              <?php  } if($row->flag!='Generate' && $designation->desid==10){ 
                                 if(!empty($row->transid)) {
                                  ?>
                                  <a href="<?php echo site_url('Recommended_to_graduate/Appointment_letter/'.$row->transid);?>" class="btn btn-danger  btn-sm" data-toggle="tooltip" title="Generate"><i class="fas fa-file-signature"></i></a>
                                <?php } 
                               }else if($row->flag == 'Generate' && $designation->desid==10){ ?>
                              <a class="btn btn-success btn-sm" href="<?php echo site_url().'pdf_offerletters/'.$row->filename.'.pdf';?>"  download data-toggle="tooltip" title="Download">
                                <i class="fas fa-file-download"></i>  
                              </a>                                
                              <?php } ?>
                            </td>

                            <td class="text-left">
                              <?php if ($this->loginData->RoleID == 10 && $row->IntegratorApproval == 0) { ?>
                              <div class="form-check">                                
                                <input type="radio" class="form-check-input" name = "IntegratorApproval[<?php echo $row->staffid; ?>]" id="approve" value="1" >
                                <label class="form-check-label" for="materialUnchecked">Approve</label>
                              </div> 
                              <div class="form-check">
                                <input type="radio" class="form-check-input"  name = "IntegratorApproval[<?php echo $row->staffid; ?>]" id="reject" value="2">
                                <label class="form-check-label" for="materialUnchecked">Reject</label>
                              </div>
                              <?php } else{ ?>
                              <div class="form-check">                                
                                <?php if ($row->IntegratorApproval == "1") {?>
                                <span class="badge badge-pill badge-success col-md-12">Approve </span>
                                <?php } ?>
                              </div> 
                              <div class="form-check">
                                <?php if ($row->IntegratorApproval == "2") {?>
                                <span class="badge badge-pill badge-danger col-md-12">Reject </span>
                                <?php } ?>
                              </div>
                              <div class="form-check">
                                <?php if ($row->IntegratorApproval == "0") {?>
                                <span class="badge badge-pill badge-warning col-md-12">Pending </span>
                                <?php } ?>
                              </div>
                              <?php }?>
                            </td>



                            <td class="text-center">
                              <?php if ($designation->desid == 16 && $row->sendflag== 1 && empty($row->edflag)) { ?>
                             <span class="badge badge-pill badge-danger col-md-12">Sent </span>

                             <?php  }
                             elseif ($designation->desid == 16 && $row->sendflag== 1 && $row->edflag == 2) { ?>
                             <span class="badge badge-pill badge-success col-md-12">Approved </span>

                             <?php }
                             elseif ($this->loginData->RoleID == 17 && $row->edflag == 0) { ?>
                             <span class="badge badge-pill badge-success col-md-12">Approved </span>

                             <?php }
                              elseif ($this->loginData->RoleID == 17 && $row->edflag == 1) { ?>
                                <!-- elseif ($designation->desid == 16 && $row->sendflag== 1 && $row->edflag == 1) -->
                             <span class="badge badge-pill badge-danger col-md-12">Rejected </span>

                               

                             <?php  } 
                             
                             elseif ($designation->desid == 10 && $row->sendflag== 1&& empty($row->edflag) ) { ?>
                              <span class="badge badge-pill badge-danger col-md-12">Sent </span>

                            

                             <?php } elseif ($designation->desid == 10 && $row->sendflag== 1 && $row->edflag == 1) { ?>
                             <span class="badge badge-pill badge-danger col-md-12">Rejected </span>

                              <?php } elseif ($designation->desid == 10 && $row->sendflag== 1 && $row->edflag == 2) { ?>
                              <span class="badge badge-pill badge-success col-md-12">Approved </span>
                             <?php  }  elseif ($designation->desid == 10 && $row->IntegratorApproval == "1") { ?>

                             <button type="button" class="btn btn-info btn-sm" name="sendtord" id="<?php echo $row->transid;  ?>"  onclick="SendTOEDOfferLetter(this.id)" value="SENDTOED" data-toggle="tooltip" title="To ED"><i class="fas fa-file-export"></i></button>

                             <?php } ?> 

                           </td>    
                         </tr>
                         <?php  
                       } $count++;
                     }
                   }
                   ?>
                 </tbody>
               </table>
             </div>
             <?php
             $designationid = '16';

             if (!empty($designation))
             {
              $designationid = $designation->desid;
            }


            if (!empty($recommend_to_graduate_list)  && $designationid == 16)
              { ?>
                <div class = "col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                  <button class="btn btn-primary btn-sm" id="submit" type="submit"  >Submit</button>                           

                </div>
                <?php
              }
              ?>
            </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </section>
    <script type="text/javascript">
      $('#tblstaffleave').DataTable();
      function SendTOEDOfferLetter(id){
        var transid = id;
     // alert(id);

     $.ajax({

      url: '<?php echo site_url(); ?>Recommended_to_graduate/sendofferlettered/'+ transid,
      type: 'POST',
      dataType: 'text'
    })

     .done(function(data) {
      // alert(data);
      if (data=='send') {
        alert('Send Appointment Letter To ED');
      }else if(data=='sent'){
        alert('Already send Appointment Letter To ED ');
      }else if(data=='fail'){
        alert('Please Try Again Email Not send');
      }else if (data=='notgenerate') {
        alert('Please !! First Generate Appointment Letter !!!');
      }

    });

   }

 </script>