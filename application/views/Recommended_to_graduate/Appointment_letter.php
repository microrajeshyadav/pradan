 

<br/>
<section class="content" style="background-color: #FFFFFF;" >
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer col-md-12" >
     <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">Offer Letter generation </h4>
       
     </div>
     <hr class="colorgraph"><br>
   </div>

   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
    <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>

      <form name="formappointment" id="formappointmentid" method="POST" action="">
        <div class="panel-body">
          <div class="body">
            <div class="row clearfix doctoradvice">

<!-- new -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <!-- <h1>Letter Content</h1><br><br> -->
                  <div class="form-line">
            <label for="Name">Message</label>
            <textarea class="summernote" id="summernote" name="summernote" rows="10" ><?php echo $body;?></textarea> 
                  </div>
                </div>
              </div>

              

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <div class="form-group">
                  <div class="form-line">
                    <label for="officeid" class="field-wrapper required-field">Office. <span style="color:red;">*</span></label>
                    <select name="officeid" id="officeid" class="form-control" required>
                      <option value="">Select Office</option>
                      <?php 
                        if($officeList){
                          foreach($officeList as $res){
                      ?>
                        <option value="<?php echo $res->officeid; ?>" <?php if($userDetail) if($userDetail->officeid == $res->officeid) echo "Selected"; ?>><?php echo $res->officename; ?></option>
                    <?php } } ?>
                    </select>
                  </div>
                  <?php echo form_error("officeid");?>
                </div>



                <div class="form-group">
                  <div class="form-line">
                    <label for="TeamName" class="field-wrapper required-field">File No. <span style="color:red;">*</span></label>
                    <input type="text" name="fileno" id="fileno" class="form-control th1"   maxlength="3" minlength="1" value="<?php set_value('fileno');?>" required="required" >
                  </div>
                  <?php echo form_error("fileno");?>
                </div>
                                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Offer No. <span style="color:red;">*</span></label>
                    <input type="text" name="offerno" id="offerno" class="form-control th1 " maxlength="3" minlength="1" value="<?php set_value('offerno');?>" required="required">
                  </div>
                  <?php echo form_error("offerno");?>
                </div>

                                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">DOJ <span style="color:red;">*</span></label>
                    <input type="text" name="doj" id="doj" class="form-control datepicker" value="<?php set_value('doj');?>" required="required">
                  </div>
                  <?php echo form_error("doj");?>

<!--                                   <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Last date of acceptance of docs <span style="color:red;">*</span></label>
                    <input type="text" name="lastdateofacceptanceofdocs" id="lastdateofacceptanceofdocs" class="form-control datepicker" value="<?php //set_value('lastdateofacceptanceofdocs');?>" required="required">
                  </div>
                  <?php //echo form_error("lastdateofacceptanceofdocs");?>
                </div> -->
                </div>
              </div>

<!--               <div class="col-lg-3 col-md-3 col-xs-12">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Offer No. <span style="color:red;">*</span></label>
                    <input type="text" name="offerno" id="offerno" class="form-control th1 " maxlength="3" minlength="1" value="<?php //set_value('offerno');?>" required="required">
                  </div>
                  <?php //echo form_error("offerno");?>
                </div>
              </div>

             <div class="col-lg-3 col-md-3 col-xs-12">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">DOJ <span style="color:red;">*</span></label>
                    <input type="text" name="doj" id="doj" class="form-control datepicker" value="<?php //set_value('doj');?>" required="required">
                  </div>
                  <?php //echo form_error("doj");?>
                </div>
              </div>

                                  <input type="hidden" id="candidate" class="form-control datepicker" value="<?php //set_value('candidate');?>" required="required">

              <div class="col-lg-3 col-md-3 col-xs-12">
                <div class="form-group">
                  <div class="form-line">
                    <label for="StateNameEnglish" class="field-wrapper required-field">Last date of acceptance of docs <span style="color:red;">*</span></label>
                    <input type="text" name="lastdateofacceptanceofdocs" id="lastdateofacceptanceofdocs" class="form-control datepicker" value="<?php //set_value('lastdateofacceptanceofdocs');?>" required="required">
                  </div>
                  <?php //echo form_error("lastdateofacceptanceofdocs");?>
                </div>
              </div> -->






            </div>


          </div>
        </div>
      </div>
      <div class="panel-footer text-right">
        <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save & Download Offer Letter</button>
        <a href="<?php echo site_url("Assigntc");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Close</a> 
      </div>
    </form>
  </div>
  <!-- #END# Exportable Table -->
</div>


</section>

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.6.4/summernote.min.js"></script>

  <script type="text/javascript">
  $(document).ready(function() {
    var editor = $('#summernote');
    editor.summernote({
        height: ($(window).height()),
        focus: false,
        toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
      ],
        oninit: function() {
            // Add "open" - "save" buttons $('#foo').addClass('connected-sortable droppable-area2');
            /*var noteBtn = '<button id="makeSnote" type="button" class="btn btn-default btn-sm btn-small" title="Identify a music note" data-event="something" tabindex="-1"><i class="fa fa-music"></i></button>';            
            var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
            $(fileGroup).appendTo($('.note-toolbar'));*/
            // Button tooltips
            $('#makeSnote').tooltip({container: 'body', placement: 'bottom'});
            // Button events
            $('#makeSnote').click(function(event) {
                var highlight = window.getSelection(),  
                    spn = document.createElement('span'),
                    range = highlight.getRangeAt(0)
                
                spn.innerHTML = highlight;
                spn.className = 'snote';  
                spn.style.color = 'blue';
            
                range.deleteContents();
                range.insertNode(spn);
            });
         },        
    });  
});
</script>



<script type="text/javascript">

  $('.th1').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
      event.preventDefault();
    }
  });


</script>



<script type="text/javascript">

 $(document).ready(function(){

  $("#team").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {

      console.log(data);
      $("#fieldguide").html(data);

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $("#doj").datepicker({
   changeMonth: true,
   changeYear: true,
   minDate: 'today',
   yearRange: '1920:2030',
   dateFormat : 'dd/mm/yy',

 });

  $("#lastdateofacceptanceofdocs").datepicker({
   changeMonth: true,
   changeYear: true,
   minDate: 'today',
   yearRange: '1920:2030',
   dateFormat : 'dd/mm/yy',

 });

});     

</script>
