<?php //print_r($getstaffprovationreviewperformance);?>
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
      <div class="panel-body">
     
      <form name="staffreviewfrm" id="staffreviewfrm" method="POST" action="">
    
      <div class="row" style="line-height: 3">
        <div class="col-md-12">
          1. Name : <input type="text" name="staffname" id="staffname"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly"  value="<?php echo $getstaffprovationreviewperformance->name; ?>">
        </div>
        <div class="col-md-12">
         2. Designation : <input type="text" name="staffdesignation" id="staffdesignation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprovationreviewperformance->desname; ?>" readonly="readonly">
       </div>     
       <div class="col-md-12">
        3. Employee Code : <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprovationreviewperformance->emp_code; ?>" readonly="readonly">
      </div>
    

      <div class="col-md-12">
        5. Location : <input type="text" name="stafflocation" id="stafflocation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getstaffprovationreviewperformance->officename; ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        6. Date of Appointment: <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->date_of_appointment); ?>" readonly="readonly">
      </div>
      <div class="col-md-12">
        7. Period of Review : From <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_from); ?>" readonly="readonly"> (date) To <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $this->gmodel->changedatedbformate($getstaffprobationreview->period_of_review_to); ?>" readonly="readonly"> (date)
      </div>
    </div>
    <p></p>
    <p></p>
    <p><strong>TO BE REVIEWED BY</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        1. Name :<input type="text" name="review_by" id="review_by" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->name;?>" readonly="readonly">
      </div>

      <div class="col-md-12">
       2. Designation :<input type="text" name="review_designation" id="review_designation" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->desname;?>" readonly="readonly">
     </div>

     <div class="col-md-12">
      3. Employee Code : <input type="text" name="review_emp_code" id="review_emp_code" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->emp_code;?>" readonly="readonly">
    </div>

   

    <div class="col-md-12">
      5. Location : <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->officename; ;?>" readonly="readonly">
    </div>
  </div>
  <hr/>
  
<?php if ($this->loginData->RoleID ==2 || $this->loginData->RoleID ==21) { ?>
  <div class="text-center">
    <p><h5> Reviewing By Supervisor</h5></p>
  
  </div>
  

 
  <p><strong>Comment</strong></p>
  

  <textarea name="work_habits_and_attitudes" readonly="readonly" maxlength="500" id="work_habits_and_attitudes" cols="40" rows="10" style="width:1000px;"><?php echo $getstaffprobationreview->work_habits_and_attitudes;?></textarea>

  <div class="container-fluid">
    <div class="row text-left">
      1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation indicated above, has been (please tick &uuml;):
    </div>
    <div class="row text-left">
      <div class="col-md-3"> (a)  Satisfactory</div>
       <div class="col-md-9 text-left"><input type="radio" name="satisfactory" id="satisfactory" value="satisfactory" <?php if ($getstaffprobationreview->satisfactory =="satisfactory") {
     echo "checked=checked";
    }?>></div>
      <div class="col-md-12">  OR</div>
      <div class="col-md-3"> (b)  Not satisfactory</div>
        <div class="col-md-9"><input type="radio" name="satisfactory" id="notsatisfactory" value="notsatisfactory" <?php if ($getstaffprobationreview->satisfactory =="notsatisfactory") {
     echo "checked=checked";
    }?>></div>

    </div>
    
    </div>
    <div class="row" style="line-height: 3">
        <div class="col-md-6 text-right">
          
        </div>
        <div class="col-md-6 text-left">
          
           <img src="<?php if(!empty($reportingto_image->encrypted_signature)) echo site_url().'/datafiles/signature/'.$reportingto_image->encrypted_signature;?>" height="50" width="140">
        </div>
       
      </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
        Place: <input type="text" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $tc_email->officename;?>" readonly="readonly">
      </div>

      <div class="col-md-6 pull-right">
        Signature of Reviewing Supervisor
      </div>
      <div class="col-md-6 pull-left">
        Date:<input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
      </div>
      <div class="col-md-6 pull-right">
         
                Name:  <?php echo $tc_email->name;?>
      </div>
      <div class="col-md-6 pull-left">

      </div>
      <div class="col-md-6 pull-right">
       Designation: <?php echo $tc_email->desname;?>
     </div>
   </div>
 </div>
<?php } ?>
 

     <div class="panel-footer text-right">
    <?php if ($getstaffprobationreview->flag == 4) { ?>
      <input type="submit" name="submit" value="Submit" value="SaveDataSend" class="btn btn-success btn-sm">
  <?php   }  ?>
      
        <a href="<?php echo site_url().'Staff_review/'  ?>" class="btn btn-dark btn-sm"> Go Back</a>
      </div>

  </div>

</div>

</div>

 
    </form>
</div>
</div>
</section>