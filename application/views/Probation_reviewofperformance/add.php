
<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class
    ="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-12 panel-title pull-left">REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <form name="staffreviewfrm" id="staffreviewfrm" method="POST" action="">
     <div class="panel-body">
      
      <div class="row" style="line-height: 3">
        <div class="col-md-12">
          1. Name :
          <input type="hidden" name="supervisorid" id="supervisorid" value="<?php if(!empty($getstaffprovationreviewperformance->supervisorid)) echo $getstaffprovationreviewperformance->supervisorid; ?>">

          <input type="hidden" name="staffid" id="staffid"  value="<?php if(!empty($getstaffprovationreviewperformance->staffid)) echo $getstaffprovationreviewperformance->staffid; ?>"> <input type="text" name="staffname" id="staffname"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly"  value="<?php if(!empty($getstaffprovationreviewperformance->name)) echo $getstaffprovationreviewperformance->name; ?>">
        </div>
        <div class="col-md-12">
         2. Designation : <input type="text" name="staffdesignation" id="staffdesignation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprovationreviewperformance->desname)) echo $getstaffprovationreviewperformance->desname; ?>" readonly="readonly">
       </div>     
       <div class="col-md-12">
        3. Employee Code :  <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprovationreviewperformance->emp_code)) echo $getstaffprovationreviewperformance->emp_code; ?>" readonly="readonly">
      </div>

      <div class="col-md-12">
        4. Location :<input type="text" readonly="readonly" name="stafflocation" id="stafflocation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" maxlength="150" value="<?php if(!empty($getstaffprovationreviewperformance->officename)) echo $getstaffprovationreviewperformance->officename; ?>">
      </div>
      <div class="col-md-12">
        5. Date of Appointment:<input type="text" readonly="readonly" name="date_of_appointment" id="date_of_appointment" class="" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"  value="<?php if(!empty($getstaffprovationreviewperformance->doj)) echo $this->gmodel->changedatedbformate($getstaffprovationreviewperformance->doj); ?>">
      </div>
      <div class="col-md-12">
        6. Period of Review : From <input type="text" readonly="readonly" name="period_of_review_from" id="period_of_review_from"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getstaffprovationreviewperformance->probation_date)) echo $this->gmodel->changedatedbformate($getstaffprovationreviewperformance->probation_date); ?>"  required="required" > (date) To <input type="text" name="period_of_review_to" id="period_of_review_to" class="todate datepicker"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value=""  required="required" > (date)
      </div>
    </div>
    <p></p>
    <p></p>

    <p><strong>TO BE REVIEWED BY</strong></p>
    <div class="row" style="line-height: 3">
      <div class="col-md-12">
        <input type="hidden" name="review_by_id" id="review_by_id"  value="<?php if(!empty($getpersonnaldetals->personnalstaffid)) echo $getpersonnaldetals->personnalstaffid;?>">
        1. Name : <input type="text" name="review_by" id="review_by" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getpersonnaldetals->personnalname)) echo $getpersonnaldetals->personnalname;?>" readonly="readonly">
      </div>

      <div class="col-md-12">
       2. Designation : <input type="text" name="review_designation" id="review_designation" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php  if(!empty($getpersonnaldetals->personnaldesignationname)) echo $getpersonnaldetals->personnaldesignationname;?>" readonly="readonly">
     </div>

     <div class="col-md-12">
      3. Employee Code : <input type="text" name="review_emp_code" id="review_emp_code" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php if(!empty($getpersonnaldetals->personnalempcode)) echo $getpersonnaldetals->personnalempcode;?>" readonly="readonly">
    </div>

    <div class="col-md-12">
      4. Location : <input type="text" readonly="readonly" name="review_location" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" maxlength="150" value="<?php  if(!empty($getpersonnaldetals->officename)) echo $getpersonnaldetals->officename; ?>">
    </div>
  </div>
  <hr/>
 

  <?php if ($this->loginData->RoleID ==2 || $this->loginData->RoleID ==21) { ?>
  <div class="text-center">
   
    <p><h5> Reviewing By Supervisor</h5></p>
   
  </div>
  


  <p><strong>Comment</strong></p>
  

  <textarea name="work_habits_and_attitudes" maxlength="500" id="work_habits_and_attitudes" cols="40" rows="10" style="width:1000px;" required=""></textarea>

 
    <div class="container-fluid">
      <div class="row text-left">
        1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation indicated above, has been (please tick &uuml;):
      </div>
      <div class="row text-left">
        <div class="col-md-3"> (a)  Satisfactory   </div>
        <div class="col-md-9 text-left"><input type="radio" name="satisfactory" id="satisfactory" value="satisfactory"  required="required"></div>
        <div class="col-md-12">  OR</div>
        <div class="col-md-3" id="option1"> (b)  Not satisfactory </div> 
        <div class="col-md-9"><input type="radio" name="satisfactory" id="notsatisfactory" value="notsatisfactory" ></div>
      </div>
      
      <div class="row" style="line-height: 3">
        <div class="col-md-6 text-right">
          
        </div>
        <div class="col-md-6 text-left">
           <img src="<?php if(!empty($reportingto_image->encrypted_signature)) echo site_url().'/datafiles/signature/'.$reportingto_image->encrypted_signature;?>" height="50" width="140">
        </div>
       
      </div>

      <div class="row" style="line-height: 3">
        <div class="col-md-6 pull-left">
          Location:  <input type="text" readonly="readonly" name="place" id="place"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo $getpersonnaldetals->officename;?>" required="required"> 
        </div>

        <div class="col-md-6 pull-right">
          Signature of Reviewing Supervisor
        </div>
        <div class="col-md-6 pull-left">
          Date:  <input type="text" readonly="readonly" name="supervisor_date" id="supervisor_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value="<?php echo date('d/m/Y');?>"> 
        </div>
        <div class="col-md-6 pull-right">
          Name:<input type="text" readonly="readonly" name="supervisor_name" id="supervisor_name"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getpersonnaldetals->personnalname;?>"> 
        </div>
        <div class="col-md-6 pull-left">

        </div>
        <div class="col-md-6 pull-right">
         Designation: <input type="text" readonly="readonly" name="supervisor_designation" id="supervisor_designation"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getpersonnaldetals->personnaldesignationname;?>"> 
       </div>
     </div>
   </div>
   <?php } ?>
   <?php  if ($this->loginData->RoleID == 18) {?>
   <div class="text-center">
    <p><h5>Part - IV</h5></p>
    <p><h5>Executive Director&rsquo;s Note </h5></p>
    <p>(<em>required only in cases where probation is proposed to be extended OR absorption is <u>not </u>recommended by the Reviewing Supervisor</em>)<em>.</em></p> </div>
    <p></p>
    <p>(i) I agree/do not agree with the above recommendations.</p>
    <p></p>
    <p>(ii) The reasons for not agreeing to above recommendations are:</p>
    <p><textarea name="reasons_for_not_above_recommendations" id="reasons_for_not_above_recommendations" class="form-control" cols="230" rows="7"></textarea></p>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
        Date: <input type="text" name="supervisor_date" id="supervisor_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y');?>">
      </div>
      <div class="col-md-6 pull-right">
       Signature of ED : ________________________
     </div>      
   </div> 

   <?php } elseif ($this->loginData->RoleID == 17) {  ?>

   <div class="text-center">
    <p><h5>Part - V</h5></p>
    <p>(<em>for use in the Finance-Personnel-MIS Unit)</em></p>
    <p></p>
    <p>Necessary action has been taken and all concerned have been informed accordingly. The Probationer Register has also been completed as above.</p>
    <p></p> </div>

    <div class="row" style="line-height: 3">
      <div class="col-md-6 pull-left">
       Name:  <input type="text" name="personnel_name" id="personnel_name"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getpersonnaldetals->personnalname;?>">
     </div>
     <div class="col-md-6 pull-right">
       Designation: <input type="text" name="personnel_designation" id="personnel_designation"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getpersonnaldetals->personnaldesignationname;?>">
     </div>
     <div class="col-md-6 pull-left">
      
     </div>
     <div class="col-md-6 pull-right">
       Date: <input type="text" name="personnel_date" id="personnel_date"  style="min-width: 5px; max-width:100px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d/m/Y'); ?>">
     </div>      
   </div>
   <?php } ?>
 </div>
 <div class="panel-footer text-right">
  <input type="submit" name="submit" value="Submit" value="SaveDataSend" class="btn btn-success btn-sm">
  <a href="<?php  echo site_url().'Staff_list/';?>" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</form>
</div>
</div>
</section>
<script type="text/javascript">
  $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate: 'today',
    yearRange: '1920:2030',
    dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });

  $('#extended_completed').on('click', function() {

   if ($(this).val() === 'no') {
    $('#probation_extension_date').removeProp("disabled");
  }
  else {
    $('#probation_extension_date').prop("disabled", "disabled");
  }
});
</script>

<script>
  $(document).ready(function(){

   $('.fromdate').datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: 'today',
    yearRange: '1920:2030',
    changeMonth: true,
    changeYear: true,
  });
   $('.todate').datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: 'today',
    yearRange: '1920:2030',
    changeMonth: true,
    changeYear: true,
  });

   $('.fromdate').datepicker().bind("change", function () {
    var minValue = $(this).val();
    minValue = $.datepicker.parseDate("dd/mm/yy", minValue);
    $('.todate').datepicker("option", "minDate", minValue);
    calculate();
  });
   $('.todate').datepicker().bind("change", function () {
    var maxValue = $(this).val();
    maxValue = $.datepicker.parseDate("dd/mm/yy", maxValue);
    $('.fromdate').datepicker("option", "maxDate", maxValue);
    calculate();
  });

 });
</script>
<script>
$('#satisfactory').on('click', function() {
  if ($(this).val() === 'satisfactory') {
    $('#extended_completed').prop('checked', false);
    $('#not_recommended').prop('checked', false);
    $('#probation_completed').removeProp("disabled");    
    $('#extended_completed').prop("disabled", "disabled");
    $('#not_recommended').prop("disabled", "disabled");
    $('#probation_extension_date').prop("disabled", "disabled");
    $('#probation_extension_date').val("");
      $('#probation_extension_date').prop("required", false);
  }
});

  $('#notsatisfactory').on('change', function() {
    if ($(this).val() === 'notsatisfactory') {
      $('#probation_completed').prop('checked', false);
      $('#probation_completed').prop("disabled", "disabled");
      $('#extended_completed').removeProp("disabled");
      $('#not_recommended').removeProp("disabled");
    }
  });


  $('#extended_completed').on('click', function() {
    if ($(this).val() === 'no') {
      $('#probation_extension_date').removeProp("disabled");
      $('#probation_extension_date').prop("required", true);
    }else {
      $('#probation_extension_date').prop("disabled", "disabled");
    }
  });
  $('#not_recommended').on('click', function() {
    if ($(this).val() === 'not_recommended') {
      $('#not_recommended').removeProp("disabled");
      $('#probation_extension_date').prop("disabled", "disabled");
      $('#probation_extension_date').val("");
      $('#probation_extension_date').prop("required", false);
    }
  });
</script>