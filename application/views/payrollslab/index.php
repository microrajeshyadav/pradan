<section class="content">
  <?php foreach ($role_permission as $row) { if ($row->Controller == "payrollslab" && $row->Action == "index"){ ?>
  <div class="container-fluid">
    <!-- Exportable Table -->

    <?php 
    print_r($this->session->flashdata);
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="container-fluid" style="margin-top: 20px;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <form name="campus" action="" method="post" > 
           <input type="hidden" id="data" name="data" value="0">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-8 panel-title pull-left">Slab Wise Head Breakup</h4>
                 <div class="col-md-4 text-right" style="color: red">
                  * Denotes Required Field 
                </div>
              </div>
              <hr class="colorgraph"><br>
            </div>

            <div class="panel-body" style="line-height: 3">
             <div class="row">
              <div class="col-md-7" style="border-right:solid 1px #d4d4d4; ">
                <table id ="tblslab" class="table table-bordered" style="margin-right:5px;">
                  <thead>
                    <tr>
                      <th style="width:50px;">
                        #
                      </th>
                      <th >
                        Head
                      </th>
                      <th >
                        Financial year
                      </th>
                      <th>
                        Lower
                      </th>
                      <th>
                        Upper
                      </th>
                      <th>
                        Amount
                      </th>
                      <th style="width:60px;">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>

                  <?php 
                   $i=1;
                  foreach($getsalaryheaddetail as $value)
                  {

                  ?>
                   <tr>

                   <td>
                   <?php echo $i;?>
                   </td>
                    <td >
                    <input type="hidden" id="fieldname_<?php echo $i;?>" value="<?php echo $value->fieldname;?>"> 
                   <p> <?php echo $value->fieldname;?></p>
                    </td>
                    <td >
                    <input type="hidden" id="financialyear_<?php echo $i;?>" value="<?php echo $value->financial_year;?>"> 
                   <p> <?php echo $value->financial_year;?></p>
                    </td>
                    <td >
                    <input type="hidden" id="lowerlimit_<?php echo $i;?>" value="<?php echo $value->lowerlimit;?>"> 
                       <label id="lower_<?php echo $i;?>"></label>
                    </td>
                    <td>
                    <input type="hidden" id="upperlimit_<?php echo $i;?>" value="<?php echo $value->upperlimit;?>"> 
                       <label id="upper_<?php echo $i;?>"></label>
                    </td>
                    <td>
                    <input type="hidden" id="amount_<?php echo $i;?>" value="<?php echo $value->amount;?>"> 
                       <label id="amt_<?php echo $i;?>"></label>
                    </td>
                    <td>
                      <button  style="border:0px !important; background:#fff !important; height:0px !important;" type="button" data-toggle="tooltip" data-placement="bottom" title="Click here to edit." onclick="salaryeditfunction(<?php echo $i; ?>);"   id="salary_head_<?php echo $i;?>" value="<?php echo $value->id;?>"><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></button> |
                      <a data-toggle="tooltip" data-placement="bottom" title="Click here to delete." href="<?php echo site_url()."payrollslab/delete/".$value->id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
                    </td>
                    
                  </tr>
                  <script>
                  var cellid= "<?php echo $i; ?>"; var cid="#lower_"+cellid;
                  $(cid).text(intToNumberBudget("<?php echo $value->lowerlimit;?>"));

                  var cellid= "<?php echo $i; ?>"; var cid="#upper_"+cellid;
                  $(cid).text(intToNumberBudget("<?php echo $value->upperlimit;?>"));

                  var cellid= "<?php echo $i; ?>"; var cid="#amt_"+cellid;
                  $(cid).text(intToNumberBudget("<?php echo $value->amount;?>"));
                  </script>
                  <?php 
                  $i++;
                  } ?>

                </tbody>

              </table>
            </div>

            <div class="col-md-5">
            <input type="hidden" id="salary_id" name="salary_id" value="">
             <div class="row">
              <div class="col-md-5 align-text-top">
                Select Head <span style="color:red">*</span>:
              </div>
              <div class="col-md-7">
               <select id="ddlhead" name="ddlhead" class="form-control" required="required">
                 <option>Select salary heads</option>
                 <?php foreach ($getsalaryhead as $key => $value) {?>
                 <option value="<?php echo $value->fieldname;?>"><?php echo $value->fielddesc;?></option>
                 <?php } ?>
               </select>
             </div>          
           </div>
           <div class="row">
           <div class="col-md-5 align-text-top">
           Financial Year <span style="color:red">*</span>:
          </div>
            <div class="col-md-7">
                <select id="financialyear" name ="financialyear" class="form-control">
                 <option value="<?php echo $fyear; ?>"> <?php echo $fyear; ?></option>
                 <?php 
                 $fyearexp = '';
                 $prevyear = '';
                 $nextyear = '';
                 $nextfyear = '';
                 $fyearexp  = explode('-', $fyear); 
                 $prevyear = $fyearexp[0]+1;
                 $nextyear = $fyearexp[1]+1;
                 $nextfyear = $prevyear.'-'.$nextyear;
                 ?>
                 <option value="<?php echo $nextfyear; ?>"> <?php echo $nextfyear; ?></option>
               </select>
             </div>          
           </div>
           <div class="row">
            <div class="col-md-5 align-text-top">
              Basic Salary Lower Limit <span style="color:red">*</span>:
            </div>
            <div class="col-md-7">
              <input type="number" id= "lower" name="lower"  min="0" max="999999" onKeyPress="if(this.value.length==6) return false;"   class="form-control text-right" required="">
            </div>          
          </div>
          <div class="row">
            <div class="col-md-5 align-text-top">
              Basic Salary Upper Limit <span style="color:red">*</span>:
            </div>
            <div class="col-md-7">
             <input type="number" id= "upper" name="upper"  min="1" max="999999" onKeyPress="if(this.value.length==6) return false;"   class="form-control text-right" required="">
           </div>          
         </div>
         <div class="row">
          <div class="col-md-5 align-text-top">
            Amount <span style="color:red">*</span>:
          </div>
          <div class="col-md-7">
           <input type="number" id= "amount" name="amount"  min="1" max="999999" onKeyPress="if(this.value.length==6) return false;"   class="form-control text-right" required="">

         </div>          
       </div>
     </div>
   </div>
 </div>
 <div class="panel-footer text-right">
   <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save </button>
   <a href="<?php echo site_url()."payrollslab";?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
 </div> 

</div>
</form> 

</div>
<!-- #END# Exportable Table -->
</div>
</div>
<?php } } ?>
</section>
<script type="text/javascript">

 $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();  
  $('#tblslab').DataTable(); 
});

$('.navigateTest').click(function(){
 alert("test called");

 
});



 
      function confirm_delete() {

        var r = confirm("Do you want to delete Salary Head?");
        if (r == true) {
          return true;
        } else {
          return false;
        }
      }

      function salaryeditfunction(id){
        var salary_head=$("#salary_head_"+id).val();
        // alert("salary_head_"+salary_head);
        $("#salary_id").val(salary_head);
        
       

        var fieldname=$("#fieldname_"+id).val();
         $("#ddlhead").val(fieldname);
        var fyear=$("#financialyear_"+id).val();
         $("#financialyear").val(fyear);
        var lowerlimit=$("#lowerlimit_"+id).val();
         $("#lower").val(lowerlimit);;
         var upperlimit=$("#upperlimit_"+id).val();
         $("#upper").val(upperlimit);
         var amount=$("#amount_"+id).val();
         $("#amount").val(amount);
        

      }


</script>