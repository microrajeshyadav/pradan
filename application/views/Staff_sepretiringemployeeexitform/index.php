<style type="">
  input[type='text']:disabled
  {

    background: #fff !important;

  }
textarea:disabled
  {

    background: #fff !important;
    
  }
  textarea{
    width: 600px !important;
  }
  

</style>

<section class="content" style="background-color: #FFFFFF;" >
<?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
<form role="form" method="post" action="">
  <br>
  <div class="container-fluid">
     <?php //$page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - THE EXIT INTERVIEW FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <h5>Professional Assistance for Development Action (PRADAN)</h5>
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
        <h4>THE EXIT INTERVIEW FORM</h4>
        <p>(for Retiring Employees only)</p>
      </div> 
    </div>

    <div class="row">
      <div class="col-md-12 text-left" style="margin-bottom: 20px;">
       <strong>Note:</strong>This form is meant to elicit candid impressions about PRADAN from you with a view to further improve the state of affairs of PRADAN.
     </div>
   </div>

   <div class="row">
    <div class="col-md-12 text-center" style="margin-bottom: 10px;">
     <strong>PART - I</strong>
   </div>
 </div>

 <div class="row">
  <div class="col-md-12 text-left text-center" style="margin-bottom: 50px;">
    (to be filled in by the Finance-Personnel-MIS Unit)
  </div>
</div>

<div class="row" style="line-height: 3">
  <div class="col-md-12">
    1.  Name      : <label class="inputborderbelow"> <?php echo $staff_detail->name; ?></label>
  <div class="col-md-12">
   2. Employee Code   : <label class="inputborderbelow"> <?php echo $staff_detail->emp_code; ?></label>
 </div>
 <div class="col-md-12">
  3.  Location      : <label class="inputborderbelow"> <?php echo $staff_detail->address; ?></label>
</div>
<div class="col-md-12">
  4.  Date of Joining PRADAN: <label class="inputborderbelow"> <?php echo $this->gmodel->changedatedbformate($staff_detail->joiningdate); ?></label>
</div>
<div class="col-md-12">
  5.  Designation on Joining  : <label class="inputborderbelow"> <?php echo $staff_detail->joindesig; ?></label>
</div>
<div class="col-md-12">
 6. Date of Superannuation  : <input type="text" class="inputborderbelow datepicker" required />
<div class="col-md-12">
  7.  Total Service in PRADAN: <label class="inputborderbelow"><?php echo $total_servive[0]; ?></label> years <label class="inputborderbelow"><?php echo $total_servive[2]; ?> months
</div>
<div class="col-md-12">
 8. Designation at the time of retiring from PRADAN: <label class="inputborderbelow"> <?php echo $staff_detail->sepdesig; ?></label>
</div>
</div>
<div class="row">
  <div class="col-md-12 text-center" style="margin-top: 30px;">
   <strong>PART - II</strong>
 </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-center" style="margin-bottom: 20px;">
    (to be filled by the Retiring Employee)
  </div>
</div>
<div class="row" style="line-height: 2 ; margin-top: 10px;">

  <div class="col-md-12" style="margin-top: 20px;">
   (A)  Please list three things that you have liked the most in PRADAN:
 </div>
 <div class="col-md-12" style="margin-top: 20px;">
  (i) <input type="text" name="like_pradan_most_comment_1" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->like_pradan_most_comment_1; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (ii) <input type="text" name="like_pradan_most_comment_2" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->like_pradan_most_comment_2; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (iii) <input type="text" name="like_pradan_most_comment_3" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->like_pradan_most_comment_3; } ?>" class="inputborderbelow" required>
</div>

<div class="col-md-12" style="margin-top: 20px;">
 (B)  Please list three things that you have liked the least in PRADAN:
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (i) <input type="text" name="like_pradan_least_comment_1" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->like_pradan_least_comment_1; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (ii) <input type="text" name="like_pradan_least_comment_2" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->like_pradan_least_comment_2; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (iii) <input type="text" name="like_pradan_least_comment_3" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->like_pradan_least_comment_3; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
 (C)  Please give at least three suggestions for improving the state of affairs in PRADAN:
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (i) <input type="text" name="improving_the_state_of_affairs_1" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->improving_the_state_of_affairs_1; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (ii) <input type="text" name="improving_the_state_of_affairs_2" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->improving_the_state_of_affairs_2; } ?>" class="inputborderbelow" required>
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (iii) <input type="text" name="improving_the_state_of_affairs_3" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->improving_the_state_of_affairs_3; } ?>" class="inputborderbelow" required>
</div>

<div class="col-md-12" style="margin-top: 20px;">
 (D)  Any other remarks:
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <input type="text" name="any_other_remarks" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->any_other_remarks; } ?>" class="inputborderbelow" required>
</div>

<div class="col-md-12" style="margin-top: 20px;">
 (E)  Your future contact address: 
</div>
<div class="col-md-12" style="margin-top: 20px;">
  <label class="inputborderbelow"><?php  echo $staff_detail->address;  ?></label>
</div>
</div>

<div class="row text-left" style="margin-top: 20px;line-height: 3 ">
 <div class="col-md-6 text-left">
   Place: <input type="text" name="sign_place" class="inputborderbelow" >
 </div>
 <div class="col-md-6 text-right">
  Signature:  <label class="inputborderbelow"><?php  echo $staff_detail->name;  ?></label>
 </div>
 <div class="col-md-6 text-left">
   Date: <label class="inputborderbelow"><?php echo date('d/m/Y'); ?></label>
 </div>
 <div class="col-md-6 text-right">
   Name      : <label class="inputborderbelow"><?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?></label>
 </div>

</div>
<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
   <strong>Observations of the Person Facilitating the Exit Process </strong>
 </div>
 <div class="col-md-12" style="margin-bottom: 20px; ">
   <input type="text" name="observations_of_the_person_facilitating_the_exit_process" value="<?php if($tbl_the_exit_interview_form){ echo $tbl_the_exit_interview_form->observations_of_the_person_facilitating_the_exit_process; } ?>" class="inputborderbelow" required>
 </div>

 <div class="col-md-6 text-left">
   Date: <label class="inputborderbelow"> <?php echo date('d/m/Y'); ?></label>
 </div>
 <div class="col-md-6 text-right">
   Signature: <label class="inputborderbelow"><?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?> </label>
 </div>
 <div class="col-md-6 text-left">
   
 </div>
 <div class="col-md-6 text-right">
   Name      : <label class="inputborderbelow" required><?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?></label>
 </div>
 
</div>

</div>
</div>
</div>
<div class="panel-footer text-right">
  <?php
   if(empty($tbl_the_exit_interview_form)){
  ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
  <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
  <?php
    }else{
      if($tbl_the_exit_interview_form->flag == 0){
 ?>
  <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
<?php } ?>
  <input type="submit" name="saveandsubmit" id="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm" onclick="acceptanceregination();">
<?php } ?>
 <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
</div>
</div>
</div>
<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments" required="required"> </textarea>
              <?php echo form_error("comments");?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">Status <span style="color: red;" >*</span></label>

                        <select name="status" id="status" class="form-control" > 
                          <option value="">Select</option>
                        <?php if($this->loginData->RoleID == 20){ ?>
                          <option value="9">Approved</option>
                          
                        <?php } ?>
                        </select>
                        <?php echo form_error("status");?>
                      </div>
            <div id="executivedirector_admin" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none;" >
                      <label for="Name">Personal Administrator<span style="color: red;" >*</span></label>

                        <select name="personal_administration" id="personal_administration" class="form-control" >
                          <option value="">Select</option>
                          <?php foreach ($personaldetail as $key => $value) {
                           ?>
                          <option value="<?php echo $value->edstaffid;?>">
                            <?php echo $value->name.' - '.'('.$value->emp_code.')';?>
                              
                            </option>
                          <?php } ?>
                          </select>
                        <?php echo form_error("personal_administration");?>
                      </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="button" value="Save" class="btn btn-success btn-sm" onclick="acceptancereginationmodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
</section>
<script type="text/javascript">
    $('#status').change(function(){
    var statusval = $('#status').val();
    /*alert('sdfsaf');
    alert(statusval);*/

        if(statusval == 9) {
            $('#executivedirector_admin').show(); 
            $('#executivedirector_administration').attr('disabled',false)
            $('#personnel_administration').prop('required', true); 

        } else {
            $('#executivedirector_admin').hide(); 
              $('#executivedirector_administration').attr('disabled',true)
            $('#executivedirector_administration').prop('required', false); 

        } 
    });
function acceptanceregination(){
  var date_resignation_submitted_7 = document.getElementById('date_resignation_submitted_7').value;
  var date_from_which_separation_takes_effect_8 = document.getElementById('date_from_which_separation_takes_effect_8').value; 
  var short_fall_in_notice_period_11 = document.getElementById('short_fall_in_notice_period_11').value; 
  var what_date_13 = document.getElementById('what_date_13').value; 
  var amount7 = document.getElementById('amount7').value; 
  var amount8 = document.getElementById('amount8').value; 
  var amount9 = document.getElementById('amount9').value; 
  var amount10 = document.getElementById('amount10').value; 
  var amount11 = document.getElementById('amount11').value; 
  var amount12 = document.getElementById('amount12').value; 
  var amount13 = document.getElementById('amount13').value;
  var payments_due_to_employee = document.getElementById('payments_due_to_employee').value;
  var amounts_recoverable = document.getElementById('amounts_recoverable').value;
  var net_payable_recoverable = document.getElementById('net_payable_recoverable').value;
  
  if(net_payable_recoverable == ''){
    $('#net_payable_recoverable').focus();
  }
  if(amounts_recoverable == ''){
    $('#amounts_recoverable').focus();
  }
  if(payments_due_to_employee == ''){
    $('#payments_due_to_employee').focus();
  }
  if(amount13 == ''){
    $('#amount13').focus();
  }
  if(amount12 == ''){
    $('#amount12').focus();
  }
  if(amount11 == ''){
    $('#amount11').focus();
  }
  if(amount10 == ''){
    $('#amount10').focus();
  }
  if(amount9 == ''){
    $('#amount9').focus();
  }
  if(amount8 == ''){
    $('#amount8').focus();
  }
  if(amount7 == ''){
    $('#amount7').focus();
  }
  if(what_date_13 == ''){
    $('#what_date_13').focus();
  }
  if(short_fall_in_notice_period_11 == ''){
    $('#short_fall_in_notice_period_11').focus();
  }
  if(date_from_which_separation_takes_effect_8 == ''){
    $('#date_from_which_separation_takes_effect_8').focus();
  }
  if(date_resignation_submitted_7 == ''){
    $('#date_resignation_submitted_7').focus();
  }
  
  if(date_resignation_submitted_7 != '' && date_from_which_separation_takes_effect_8 !='' && short_fall_in_notice_period_11 !='' && what_date_13 !='' && amount7 !='' && amount8 !='' && amount9 !='' && amount10 !='' && amount11 !='' && amount12 !='' && amount13 !='' && payments_due_to_employee !='' && amounts_recoverable !='' && net_payable_recoverable !=''){
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
}
function acceptancereginationmodal(){
  //alert('dkfhsk');
  var comments = document.getElementById('comments').value;
  var acceept = document.getElementById('status').value;
  var personal_administration = document.getElementById('personal_administration').value;
  if(acceept == ''){
    $('#status').focus();
  }else if(acceept == 9 && personal_administration ==''){
    $('#personal_administration').focus();
  }
  if(comments.trim() == ''){
    $('#comments').focus();
  }


  if(comments.trim() !='' && acceept != '' && personal_administration !=''){
    document.forms['formsepchecksheet'].submit();
  }
}
  $(document).ready(function(){
    $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    //maxDate: 'today',
    yearRange: '1920:2030',
    dateFormat : 'dd/mm/yy',
    //defaultDate: new Date(2018, 00, 01)
    });
  });
</script>