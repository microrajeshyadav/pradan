<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - THE EXIT INTERVIEW FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <h5>Professional Assistance for Development Action (PRADAN)</h5>
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
        <h4>THE EXIT INTERVIEW FORM</h4>
        <p>(for Retiring Employees only)</p>
      </div> 
    </div>
    <div class="row">
      <div class="col-md-12 text-left" style="margin-bottom: 20px;">
       <strong>Note:</strong>This form is meant to elicit candid impressions about PRADAN from you with a view to further improve the state of affairs of PRADAN.
     </div>
   </div>
   <div class="row">
    <div class="col-md-12 text-center" style="margin-bottom: 10px;">
     <strong>PART - I</strong>
   </div>
 </div>
 <div class="row">
  <div class="col-md-12 text-left text-center" style="margin-bottom: 50px;">
    (to be filled in by the Finance-Personnel-MIS Unit)
  </div>
</div>

<div class="row" style="line-height: 3">
  <div class="col-md-12">
    1.  Name      : ______________________________
  </div>
  <div class="col-md-12">
   2. Employee Code   : ______________
 </div>
 <div class="col-md-12">
  3.  Location      : ______________________________
</div>
<div class="col-md-12">
  4.  Date of Joining PRADAN: _______________
</div>
<div class="col-md-12">
  5.  Designation on Joining  : ________________________
</div>
<div class="col-md-12">
 6. Date of Superannuation  : ________________________
</div>
<div class="col-md-12">
  7.  Total Service in PRADAN: _____ years _____ months
</div>
<div class="col-md-12">
 8. Designation at the time of retiring from PRADAN: ______________________
</div>
</div>
<div class="row">
  <div class="col-md-12 text-center" style="margin-top: 30px;">
   <strong>PART - II</strong>
 </div>
</div>
<div class="row">
  <div class="col-md-12 text-left text-center" style="margin-bottom: 20px;">
    (to be filled by the Retiring Employee)
  </div>
</div>
<div class="row" style="line-height: 2 ; margin-top: 10px;">

  <div class="col-md-12" style="margin-top: 20px;">
   (A)  Please list three things that you have liked the most in PRADAN:
 </div>
 <div class="col-md-12" style="margin-top: 20px;">
  (i) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (ii) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (iii) __________________________________________________________________________________
</div>

<div class="col-md-12" style="margin-top: 20px;">
 (B)  Please list three things that you have liked the least in PRADAN:
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (i) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (ii) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (iii) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
 (C)  Please give at least three suggestions for improving the state of affairs in PRADAN:
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (i) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (ii) __________________________________________________________________________________
</div>
<div class="col-md-12" style="margin-top: 20px;">
  (iii) __________________________________________________________________________________
</div>

<div class="col-md-12" style="margin-top: 20px;">
 (D)  Any other remarks:
</div>
<div class="col-md-12" style="margin-top: 20px;">
  ______________________________________________________________________________________
</div>

<div class="col-md-12" style="margin-top: 20px;">
 (E)  Your future contact address: 
</div>
<div class="col-md-12" style="margin-top: 20px;">
  ______________________________________________________________________________________
</div>
</div>

<div class="row text-left" style="margin-top: 20px;line-height: 3 ">
 <div class="col-md-6 text-left">
   Place: ____________________
 </div>
 <div class="col-md-6 text-right">
  Signature: _________________________________
 </div>
 <div class="col-md-6 text-left">
   Date: _________________
 </div>
 <div class="col-md-6 text-right">
   Name      : _________________________________
 </div>

</div>
<hr/>
<div class="row text-left" style="margin-top: 20px; line-height: 3">
 <div class="col-md-12">
   <strong>Observations of the Person Facilitating the Exit Process </strong>
 </div>
 <div class="col-md-12" style="margin-bottom: 20px; ">
   ________________________________________________________________________
 </div>

 <div class="col-md-6 text-left">
   Date: _________________________
 </div>
 <div class="col-md-6 text-right">
   Signature: _________________________________
 </div>
 <div class="col-md-6 text-left">
   
 </div>
 <div class="col-md-6 text-right">
   Name      : _________________________________
 </div>
 
</div>

</div>
<div class="panel-footer text-right">
  
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>