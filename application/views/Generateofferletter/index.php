
 <section class="content" style="background-color: #FFFFFF;" >

<div class="container-fluid">
      <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Assign Teams </b></div>
      <div class="panel-body">
      <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
          <?php } else if(!empty($er_msg)){?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>

              <div class="row" style="background-color: #FFFFFF;">
               <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
              
                <form name="gdscore" id="gdscore" method="post" action="" >
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                    <table id="tableWrittenScore" class="table table-bordered table-striped">
                      <thead>
                       <tr>
                        <th>S.No.</th>
                        <th>Candidate Name</th>
                        <th> Email Id</th>
                        <th> Team </th>
                        <th> FG </th>
                        <th> Batch </th>
                        <th> PDF </th>
                        <th> Send </th>
                        <th>Action</th>
                      </tr> 
                    </thead>
                    <tbody>
                      
                        <tr>
                          <td></td>
                          <td> </td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>   </td>
                           <td> </td>

                          <td></td>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div>

                </div>
              </form> 
            </div>
      <!-- #END# Exportable Table -->
    </div>
  </div>
</div>
 </section>


<script type="text/javascript">
 $(document).ready(function(){
            $("#team").change(function(){
                $.ajax({
                    url: '<?php echo site_url(); ?>ajax/chooseFieldguideteam/'+$(this).val(),
                    type: 'POST',
                    dataType: 'text',
                })
                .done(function(data) {
                   
                    console.log(data);
                    $("#fieldguide").html(data);
                   
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            });

       
        });     

</script>
