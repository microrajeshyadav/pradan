<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"></h4>
       <div class="col-md-2 text-right">
       </div>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <?php 
   $tr_msg= $this->session->flashdata('tr_msg');
   $er_msg= $this->session->flashdata('er_msg');

   if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <form method="POST" action="" name="promotion_responsibility" id="promotion_responsibility">
         <input type="hidden" name="staffid" value="<?php echo $staffid;?>">
         <div class="panel-body">
          <p>&nbsp;</p>
          <h1 style="text-align: center;">Inter-Office Memo</h1>
          <p>&nbsp;&nbsp;</p>
          <p><em>To</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; :&nbsp;<label class="inputborderbelow"><?php echo $promotion_getchange->name;?></label>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<em>Date&nbsp;&nbsp;&nbsp; </em>:&nbsp;<label class="inputborderbelow"><?php echo date('d/m/Y');?></label></p>
          <p>&nbsp;</p>
          <p><em>From</em>&nbsp;&nbsp; &nbsp;&nbsp; :<label class="inputborderbelow"><?php echo $getchange_edname->executivedirectorname;?></label>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<em>File</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp; 301<label class="inputborderbelow"><?php echo $promotion_getchange->emp_code;?></label>/PDR/ <input type="text" name="promotion_no" id="" value="<?php if($promotion_detail){ echo $promotion_detail->promotion_no;}?>" class="inputborderbelow"></p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Copies to</em>:&nbsp; See list below&nbsp;</p>
          <p><em>&nbsp;</em></p>
          <p><em>Subject&nbsp;&nbsp; </em>:&nbsp; <u>Change of Responsibility</u></p>
          <p>&nbsp;</p>
          <p>I am happy to assign you a new responsibility as <label class="inputborderbelow"><?php echo $promotion_getchange->changedesig;?></label>( New designation) with effect from <label class="inputborderbelow"><?php echo$this->gmodel->changedatedbformate($promotion_getchange->changedate);?></label>.You will report to <label class="inputborderbelow"><?php echo $promotion_getchange->reportingname;?></label>( Supervisor).</p>
          <p>&nbsp;</p>
          <p>In case of administrative matters pertaining to your travel, leave, etc., please seek approval and authorization from (Supervisor).</p>
          <p>&nbsp;</p>
          <p>Your will continue to be based at <label class="inputborderbelow"><?php echo $promotion_getchange->newoffice;?></label>( name of place of posting).&nbsp; Please hand in your joining report as per the enclosed format to (Supervisor) and give its duplicates to the persons/unit copied on this letter latest by <label class="inputborderbelow"><?php echo$this->gmodel->changedatedbformate($promotion_getchange->changedate);?></label> (seven days after effective date).</p>
          <p>&nbsp;</p>
          <p>Wish you all the best.</p>
          <p>List of enclosures:</p>
          <ol>
            <li>Joining Report</li>
          </ol>
          <p>&nbsp;</p>
          <p><em>Copies to</em>:</p>
          <ol>
            <li>Name of Team Coordinator</li>
            <li>Name of Integrator</li>
            <li>Finance-Personnel-MIS Unit</li>
          </ol>
          <p>&nbsp;&nbsp;</p>
          <p><strong>Nature and Scope of Scope of Job Responsibilities</strong></p>
          <p><em>&nbsp;</em></p>
          <p>Your &nbsp;primary responsibility is to collaborate and support CSOs and work with other stakeholders especially the District and Block Administrations and CSOs in planning, implementation, monitoring and evaluating the project in consultation with lead CSO.</p>
          <p>&nbsp;</p>
          <p>It includes,</p>
          <p>&nbsp;</p>
          <ul>
            <li>Being the Regional Coordinator of the State Project Management Unit</li>
            <li>Preparing the project implementation plan for a cluster of districts</li>
            <li>Developing coordination mechanism between CSOs and SPMU</li>
            <li>Coordinating with District Administration for convergence</li>
            <li>Supporting the SPMU in preparing progress reports</li>
            <li>Day-to-day management and monitoring</li>
            <li>Identifying suitable farm based livelihood interventions for large scale expansion</li>
            <li>Supporting and mentoring CSO partners&mdash;conduct training and capacity building events for CSO partners, extend necessary field-based facilitating and handholding support to CSOs in consultation with lead CSO</li>
            <li>Helping in designing capacity building modules for CSO/PRI/SHG, community based institutional framework and IEC etc.</li>
            <li>Supporting colleagues and building their competencies</li>
            <li>Consolidate experiences from ground, provide feedback to the state to incorporate in programmes and pro-poor policies</li>
          </ul>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
        </div>
        <div class="panel-footer text-right">
          <?php
 // echo $certificate_detail->flag;
          if(empty($promotion_detail)){
            ?>
            <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm">
            <input type="submit" name="saveandsubmit" value="Save And Submit" class="btn btn-success btn-sm">
            <?php
          }else{

            if($promotion_detail->flag == 0){
             ?>
             <input type="submit" name="saveandsubmit" id="saveandsubmit" value="save & submit" class="btn btn-success btn-sm">
             <!-- <input type="submit" name="Save" value="Save" class="btn btn-success btn-sm"> -->
           <?php } ?>

         <?php } ?>
         <a href="<?php echo site_url("Staff_review");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go Back</a>
       </div> 
     </form>
   </div>
 </div>   

