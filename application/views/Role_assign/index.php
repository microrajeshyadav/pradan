
<section class="content" style="background-color: #FFFFFF;" >

    
  <br>
  <div class="container-fluid">

    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-8 panel-title pull-left">Role Assign List</h4>

         <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
        </div>

        <div class="col-md-12 text-right">
            <a data-toggle="tooltip" data-placement="bottom" title="Want to add new role assign? Click on me." href="<?php echo site_url()."Role_assign/add";?>" class="btn btn-primary btn-sm">Add Role Assign </a>
          </div>

      </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">  
            <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel"> 
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>       
              <form name="tc" id="td" action="" method="post">
                <div class="col-xs-10 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <div class="row panel-footer">
                <div class="col-lg-2"></div>
                <div class="col-lg-2">Select Staff <span style="color: red;">*</span></div>
                <div class="col-lg-3"> 
                  <select class="form-control" name="staffid" id="staffid" required="required">
                    <option value="" >Select Staff </option>
                    <?php foreach ($getstafflist as $key => $value) {?>
                    <option value="<?php echo $value->staffid;?>"><?php echo $value->staffid.'-'.$value->name;?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-lg-2">
                   <button  type="submit" name="Search"  id="Search" class="btn btn-round btn-dark btn-sm" data-toggle="tooltip" title="Search" value="Search">Search</button>
                </div>
              </div>
            </div>
              </form>
              <br>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">

                   <table id="Inboxtable" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                      <th style ="max-width:50px;" class="text-center">S No.</th>
                      <th>Role Name</th>
                      <th>Staff Name</th>
                      <th>Action</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php 
                  
                    if (count($roledetail) ==0 ) {
                      # cod $i=1; foreach ($roledetail as $key => $value) {
                      ?>
                      <tr>
                       <td class="text-center" colspan="4">Rcord not found !!!</td>
                      
                    </tr>
                    <?php  
                    }else{ 

 $i=1; foreach ($roledetail as $key => $value) {
                      ?>
                      <tr>
                       <td class="text-center"><?php  echo $i; ?></td>
                      <td><?php echo $value->Acclevel_Name;?></td>
                      <td><?php echo $value->Username;?></td>
                      <td><a href="<?php echo site_url().'Role_assign/delete/'.$value->UserID;?>" id="modalcandidate" onclick="return confirm_delete();" data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this.">
             <i class="fa fa-trash" style="color:red"></i>
            </a></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php }

                  ?>
                  </tbody>
                </table>

              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color: white;">
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
           
          </form>
        </div>
      </div>
    </div>   
  </section>
 
   <script>
    function confirm_delete() {
      var r = confirm("Are you sure you want to delete this item?");
      if (r == true) {
          return true;
      } else {
          return false;
      }
    }
    $(document).ready(function(){
       $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1980:2030',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
    });
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable(); 
    });
  </script>  

  <script type="text/javascript">
    $(document).ready(function () {
      $('#checkBtn').click(function() {
        checked = $("input[type=checkbox]:checked").length;

        if(!checked) {
          alert("You must check at least one checkbox.");
          return false;
        }else{

          if (!confirm('Are you sure?')) {
              return false;
            }
          
        }


      });
    });


     $("#chooseletter").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getYPRResponse/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
        console.log(data);
        $("#letter").html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

  </script>
