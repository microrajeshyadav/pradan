
<section class="content" style="background-color: #FFFFFF;" >

    
  <br>
  <div class="container-fluid">

    <!-- <div class="panel panel-default" >
      <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Role Assign List</b></div>
      <div class="panel-body"> -->
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-8 panel-title pull-left">Assign Role</h4>
         <div class="col-md-4 text-right" style="color: red">
        * Denotes Required Field 
        </div>
      </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
          <div class="content animate-panel"> 
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <?php //echo "<pre>"; print_r($acceptedcandidatedetails); ?>
              <form name="offcampuscandidate" id="offcampuscandidate" action="" method="post" >

             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" > <label for="Name">Choose Role<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
              <select class="form-control" name="chooserole" id="chooserole" required="required">
                <option value="" >Select Role </option>
              <?php foreach ($getrole as $key => $value) {?>
                <option value="<?php echo $value->Acclevel_Cd;?>"><?php echo $value->Acclevel_Name;?></option>
              <?php } ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             <br>
             <br> <br>
             <br>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="txtoffice" style="display: none;">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" > <label for="Name">Office<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
              <select class="form-control" name="officename" id="officename" required="required">
                <option value="">Select Office </option>
              <?php foreach ($getofficelist as $key => $value) {?>
                <option value="<?php echo $value->officeid;?>"><?php echo $value->officename;?></option>
              <?php } ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             <br>
             <br> <br>
             <br>
            </div>

            <br><br>
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label for="Name">Select Staff<span style="color: red;" >*</span></label></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
         <select class="form-control" name="staffid" id="staffid" required="required">
                <option value="" >Select Staff </option>
              <?php foreach ($getstafflist as $key => $value) {?>
                <option value="<?php echo $value->staffid;?>"><?php echo $value->staffid.'-'.$value->name;?></option>
              <?php } ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ></div>
             <br>
             <br> <br>
             <br> <br>
             <br>
             </div>
            
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer text-right">
                <button type="submit" class="btn btn-success" name="btnsend" id="checkBtn" value="AcceptData" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button> 
                <a href="<?php echo site_url().'Role_assign/'; ?>" class="btn btn-dark" name="btncancel" id="btncancel"data-toggle="tooltip" title="Click here to move on list.">Go to List</a>

              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
           
          </form>
        </div>
      </div>
  </div>
          
  </section>
 


  <script>
    $(document).ready(function(){
       $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1980:2030',
        dateFormat : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
    });
      $('[data-toggle="tooltip"]').tooltip();  
      $('#Inboxtable').DataTable(); 


    

  $('#chooserole').change(function(){
   
    if($('#chooserole').val() == 20)
    {
      $('#txtoffice').css('display', 'block');
      $('#officename').removeAttr('disabled',false);
      $('#officename').prop('required',true);

   }else{
     $('#txtoffice').css('display', 'none');
     $('#officename').prop('required',false);
     $('#officename').prop('disabled',true);
   }
 });


 $("#officename").change(function(){
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getStaffList/'+$(this).val(),
      type: 'POST',
      dataType: 'text',
    })

    .done(function(data) {
      console.log(data);
      $("#staffid").html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });





    });
  </script>  

  
