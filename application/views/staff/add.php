   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="<?php echo site_url('common/backend/css/themes/all-themes.css');?>" rel="stylesheet" />
      <link href="<?php echo site_url('common/backend/jquery-ui.css');?>" rel="stylesheet" />
      <br>
  <div class="container">
    <!-- Exportable Table -->
        <div class="row clearfix">
         
           
              <div class="body">
                <div class="panel panel-default" >
                  <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Personal Info</b></div>
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('tr_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('er_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

           <form name="candidatesinfo" id="candidatesinfo" action="" method="post" > 
            <div class="row">

               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"> Candidate's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control alphabateonly" value="<?php echo set_value("candidatefirstname");?>" placeholder="Enter Candidate's First Name"  required="required">
                  <?php echo form_error("candidatefirstname");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"> </label>
                <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control alphabateonly" value="<?php echo set_value("candidatemiddlename");?>" placeholder="Enter Candidate's Middle Name">
                <?php echo form_error("candidatemiddlename");?>
                </div>
                
            
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"></label>
                <input type="text"  minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Candidate's Last Name " value="<?php echo set_value("candidatelastname");?>" required="required">
                 <?php echo form_error("candidatelastname");?>
                </div>
              </div>
               <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control alphabateonly" value="<?php echo set_value("motherfirstname");?>" placeholder="Enter Mother's First Name" required="required">
                  <?php echo form_error("motherfirstname");?>
                </div>
                
               
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"></label>
                <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control alphabateonly" value="<?php echo set_value("candidatemiddlename");?>" placeholder="Enter Mother's Middle Name">
                <?php echo form_error("mothermiddlename");?>
                </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"></label>
                <input type="text" minlength="2" maxlength="50"  class="form-control alphabateonly" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Mother's Last Name " value="<?php echo set_value("candidatelastname");?>" required="required">
                 <?php echo form_error("motherlastname");?>
                </div>
             </div>

              <div class="row">
             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Father's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control alphabateonly" value="<?php echo set_value("fatherfirstname");?>" placeholder="Enter Father's First Name" required="required">
                  <?php echo form_error("fatherfirstname");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"></label>
                <input type="text"  minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control alphabateonly" value="<?php echo set_value("fathermiddlename");?>" placeholder="Enter Father's Middle Name" >
                <?php echo form_error("fathermiddlename");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name"></label>
                <input type="text" class="form-control alphabateonly" minlength="2" maxlength="50"  data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Father's Last Name " value="<?php echo set_value("fatherlastname");?>" required="required">
                 <?php echo form_error("fatherlastname");?>
                </div>
                </div>
                 <div class="row">

                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Gender <span style="color: red;" >*</span></label>
               <?php 
              
               $options = array("" => "Select", "1" => "Male", "2" => "Female", "3" => "Other");
                echo form_dropdown('gender', $options, set_value('gender'), 'class="form-control" required="required"');
                ?>
                  <?php echo form_error("gender");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Nationality</label>
                <?php 
               $options = array("" => "Select", "indian" => "Indian", "other" => "Other");
                echo form_dropdown('nationality', $options, set_value('nationality'), 'class="form-control" required="required"');
                ?>
                <?php echo form_error("nationality");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Marital Status </label>
                <?php 
              
               $options = array("" => "Select", "1" => "Single", "2" => "Married", "3" => "Divorced","4" => "Widow","5" => "Separated");
                echo form_dropdown('maritalstatus', $options, set_value('maritalstatus'), 'class="form-control" required="required"');
                ?>
                <?php echo form_error("maritalstatus");?>
                </div>
                
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                <input type="text" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" id="datepicker" name="dateofbirth" placeholder="Enter Date Of Birth " value="<?php echo set_value("dateofbirth");?>" required="required">
                 <?php echo form_error("dateofbirth");?>
                </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>
                <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo set_value("emailid");?>" required="required" >
                 <?php echo form_error("emailid");?>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                 <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control txtNumeric" data-country="India" value="<?php echo set_value("mobile");?>" placeholder="Enter Mobile No" required="required">
                  <?php echo form_error("mobile");?>
                </div>
                </div> 
                <br>
                <div class="row">

            <table id="tbledubackground" class="table table-bordered table-striped">
              <thead>
             <tr>
              <th colspan="8" style="background-color: #026d0a; color: #fff;">Communication  Address  
              </th>
           </tr> 
             <tr>
              <th class="text-center" colspan="4" style="vertical-align: top;">Mailing Address</th>
              <th class="text-center" colspan="4" style="vertical-align: top;"> Permanent Address <div class="form-check">
              <input type="checkbox" class="form-check-input" id="filladdress">
              <label class="form-check-label" for="filladdress"><b>same as Mailing Address</b></label>
            </div> </th>
             </tr> 
         </thead>
         <tbody>
        <tr>
        <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="presenthno" maxlength="50" id="presenthno" class="form-control" 
          value="<?php echo set_value('presenthno');?>" placeholder="Enter H.No" required="required"></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><textarea name="presentstreet" id="presentstreet" class="form-control" maxlength="150" placeholder="Enter H.No/Street" required="required"><?php echo set_value('presentstreet');?></textarea></td>
        <td><label for="Name">H.No<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanenthno" maxlength="50" id="permanenthno" class="form-control" value="<?php echo set_value('permanenthno');?>"  placeholder="Enter H.No/Street" required="required" ></td>
        <td><label for="Name">Street<span style="color: red;" >*</span></label></td>
        <td><textarea name="permanentstreet" id="permanentstreet" maxlength="150" class="form-control"   placeholder="Enter H.No/Street" required="required"><?php echo set_value('permanentstreet');?></textarea></td>
        </tr>
         <tr>
        <td><label for="Name">City<span style="color: red;" >*</span></label> </td>
        <td><input type="text" name="presentcity" maxlength="50" id="presentcity" class="form-control alphabateonly" placeholder="Enter City" value="<?php echo set_value('presentcity');?>"  required="required"></td>

        <td><label for="Name">State<span style="color: red;" >*</span></label> </td>
        <td><?php 
                  $options = array('' => 'Select State');
                  foreach($statedetails as$key => $value) {
                      $options[$value->statecode] = $value->name;
                  }
                  echo form_dropdown('presentstateid', $options, set_value('presentstateid'), 'class="form-control" id="presentstateid" ');
                  ?>

                  <?php echo form_error("presentstateid");?>
        </td>
        <td><label for="Name">City<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentcity" maxlength="50" id="permanentcity" placeholder="Enter City" class="form-control alphabateonly" value="<?php echo set_value('permanentcity');?>" required="required" ></td>

        <td><label for="Name">State<span style="color: red;" >*</span></td>
        <td> 
          <?php 
          $options = array('' => 'Select State');
          foreach($statedetails as$key => $value) {
              $options[$value->statecode] = $value->name;
          }
          echo form_dropdown('permanentstateid', $options, set_value('permanentstateid'), 'class="form-control" id="permanentstateid"');
          ?>
        <?php echo form_error("permanentstateid");?></td>
        </tr>
         <tr>
          <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td>  
          <!-- <select name="presentdistrict" id="presentdistrict" required="required" class="form-control">
           <option value="">Select District </option>
         </select> -->

        <?php 
          $options = array('' => 'Select District');
          foreach($getdistrict as$key => $value) {
              $options[$value->districtid] = $value->name;
          }
          echo form_dropdown('presentdistrict', $options, set_value('presentdistrict'), 'class="form-control" id="presentdistrict"');
          ?>
        <?php echo form_error("presentdistrict");?>
         
       </td>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
         <td><input type="text" name="presentpincode" id="presentpincode" class="form-control txtNumeric" maxlength="6" value="<?php echo set_value('presentpincode');?>" required="required" placeholder="Enter PinCode"></td>
        <td><label for="Name">District<span style="color: red;" >*</span></label></td>
        <td><!-- <select name="permanentdistrict" id="permanentdistrict" required="required" class="form-control" >
           <option value="">Select District </option>
         </select> -->

           <?php 

          $options = array('' => 'Select District');
          foreach($getdistrict as$key => $value) {
              $options[$value->districtid] = $value->name;
          }
          echo form_dropdown('permanentdistrict', $options, set_value('permanentdistrict'), 'class="form-control" id="permanentdistrict"');
          ?>
        <?php echo form_error("permanentdistrict");?>

          </td>
        <td><label for="Name">Pin Code<span style="color: red;" >*</span></label></td>
        <td><input type="text" name="permanentpincode" id="permanentpincode" class="form-control txtNumeric" maxlength="6" value="<?php echo set_value('permanentpincode');?>" required="required" placeholder="Enter PinCode"></td>
        </tr>
       </tbody>
     </table>

          <table id="tbledubackground" class="table table-bordered table-striped">
            <thead>
             <tr>
              <th colspan="8" style="background-color: #026d0a; color: #fff;">Educational Background</th>
           </tr> 
             <tr>
              <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate</th>
               <th class="text-center" style="vertical-align: top;">Year </th>
               <th class="text-center" style="vertical-align: top;"> Board/ University</th>
               <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
               <th class="text-center" style="vertical-align: top;">Place</th>
               <th class="text-center" style="vertical-align: top;">Specialisation</th>
               <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
           </tr> 
         </thead>
         <tbody>
          <tr>
            <td><b>10th</b></td>
          
             <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" maxlength="4" minlength="2" required="required" style="min-width: 20%;"  value="<?php echo set_value("10thpassingyear");?>" >
                 <?php echo form_error("10thpassingyear");?></td>
            <td> <input type="text" class="form-control alphabateonly"  required="required" data-toggle="tooltip" title="Board/ University !" id="10thboarduniversity" name="10thboarduniversity" placeholder="Enter Board/ University" maxlength="150" minlength="2" value="<?php echo set_value("10thboarduniversity");?>" >
                 <?php echo form_error("10thboarduniversity");?></td>
            <td>  <input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="150" minlength="2" required="required" title="School/ College/ Institute !" 
              id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" required="required" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("10thschoolcollegeinstitute");?>" >
                 <?php echo form_error("10thschoolcollegeinstitute");?></td>

                  <td><input type="text" class="form-control alphabateonly" required="required" maxlength="50" minlength="2" data-toggle="tooltip" title="Place !" 
              id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo set_value("10thplace");?>" >
                 <?php echo form_error("10thplace");?></td>
          
             <td><input type="text" class="form-control alphabateonly" maxlength="150" minlength="2" required="required" data-toggle="tooltip" title="Specialisation !" 
              id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("10thspecialisation");?>" >
                 <?php echo form_error("10thspecialisation");?></td>
           
            <td><input type="text" class="form-control txtNumeric" maxlength="3" minlength="2" required="required" data-toggle="tooltip" title="Percentage !"
              id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo set_value("10thpercentage");?>"  >
                 <?php echo form_error("10thpercentage");?></td>
          </tr>
          <tr>
            <td>
            <select name="twelthstream" id="twelthstream" class="form-control" required="required">
              <option value="1">12th</option>
              <option value="2">Diploma</option>
            </select>
             </td>
            
            <td><input type="text" class="form-control txtNumeric" maxlength="4" minlength="2" data-toggle="tooltip" title="Year !" 
              id="12thpassingyear" name="12thpassingyear" placeholder="Enter Year" value="<?php echo set_value("12thpassingyear");?>" required="required" >
                 <?php echo form_error("12thpassingyear");?></td>

                 <td><input type="text" class="form-control" required="required" maxlength="150" minlength="2" data-toggle="tooltip" title="Board/ University !" id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("12thboarduniversity");?>" >
                 <?php echo form_error("12thboarduniversity");?></td>

            <td><input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="150" minlength="2" title="School/ College/ Institute !" id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" required="required" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("12thschoolcollegeinstitute");?>" >
                 <?php echo form_error("12thschoolcollegeinstitute");?></td>
              <td><input type="text" class="form-control alphabateonly" required="required" maxlength="50" minlength="2" data-toggle="tooltip" title="Place !" id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo set_value("12thplace");?>" >
                 <?php echo form_error("12thplace");?></td>
           
             <td><input type="text" class="form-control alphabateonly" required="required" maxlength="150" minlength="2" data-toggle="tooltip" title="Specialisation !" id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("12thspecialisation");?>" >
                 <?php echo form_error("12thspecialisation");?></td>
           <td><input type="text" class="form-control txtNumeric" 
              id="12thpercentage" name="12thpercentage" required="required" placeholder="Percentage" maxlength="3" minlength="2" value="<?php echo set_value("12thpercentage");?>" >
                 <?php echo form_error("12thpercentage");?></td>
          </tr>


          <tr>
            <td> <?php
              $options = array('' => 'Select');
               foreach($ugeducationdetails as $key => $value) {
                      $options[$value->ugname] = $value->ugname;
                  }
                  echo form_dropdown('ugstream', $options, set_value('ugstream'), 'class="form-control" data-toggle="tooltip" title="Up Stream !" required="required"');
               ?>
             <?php echo form_error("ugstream");?></td>
            
             <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" maxlength="4" minlength="2" title="Year !" 
              id="ugpassingyear" name="ugpassingyear" required="required" placeholder="Enter Year" value="<?php echo set_value("ugpassingyear");?>" >
                 <?php echo form_error("ugpassingyear");?></td>

              <td> <input type="text" class="form-control" alphabateonly data-toggle="tooltip" maxlength="150" minlength="2" required="required" title="Board/ University !" 
              id="ugboarduniversity" name="ugboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("ugboarduniversity");?>" >
                 <?php echo form_error("ugboarduniversity");?></td>

            <td>
            <input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="150" minlength="2" title="School/ College/ Institute !" 
              id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute " value="<?php echo set_value("ugschoolcollegeinstitute");?>" >
                 <?php echo form_error("ugschoolcollegeinstitute");?>
            </td>


              <td><input type="text" class="form-control alphabateonly" maxlength="50" minlength="2" data-toggle="tooltip" required="required" title="Place !" id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo set_value("ugplace");?>" >
                 <?php echo form_error("ugplace");?></td>
         
               <td>
                <input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="150" minlength="2" required="required" title="Specialisation !" 
              id="ugspecialisation" name="ugspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("ugspecialisation");?>" >
              </td>
          
           
            <td><input type="text" class="form-control txtNumeric" maxlength="3" minlength="2" required="required"
              id="ugpercentage" name="ugpercentage" placeholder="Percentage" value="<?php echo set_value("ugpercentage");?>" >
                 <?php echo form_error("ugpercentage");?></td>
          </tr>
          <tr>
            <td>
               <?php
              $options = array('' => 'Select');
                  foreach($pgeducationdetails as $key => $value) {
                      $options[$value->pgname] = $value->pgname;
                  }
                  echo form_dropdown('pgupstream', $options, set_value('pgupstream'), 'class="form-control"');
               ?>
                <?php echo form_error("pgupstream");?> 
            </td>
           
           <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="pgpassingyear" name="pgpassingyear" maxlength="4" minlength="2" placeholder="Enter Year" value="<?php echo set_value("pgpassingyear");?>" >
                 </td>
             <td> <input type="text" class="form-control alphabateonly" maxlength="150" minlength="2" data-toggle="tooltip" title="Board/ University !" id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo set_value("pgboarduniversity");?>" >
              </td>

            <td>  
                  <input type="text" class="form-control alphabateonly" maxlength="150" minlength="2" data-toggle="tooltip" title="Board/ University !" 
              id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("pgschoolcollegeinstitute");?>" >
               <?php echo form_error("pgschoolcollegeinstitute");?>
               </td>
            <td><input type="text" class="form-control alphabateonly" maxlength="50" minlength="2" data-toggle="tooltip" title="Place !" 
              id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo set_value("pgplace");?>" >
                </td>
           
               <td>
                <input type="text" class="form-control alphabateonly" data-toggle="tooltip" maxlength="255" minlength="2" title="Specialisation  !" 
              id="pgspecialisation" name="pgspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("pgspecialisation");?>" >
               </td>

           
            
           
            <td><input type="text" class="form-control txtNumeric" 
              id="pgpercentage" name="pgpercentage" placeholder="Percentage" maxlength="3" minlength="2" value="<?php echo set_value("pgpercentage");?>" >
                </td>
          </tr>

          <tr>
            <td>
                <input type="checkbox" class="filled-in" id="other"  value="Other" name="other_specify">
                                    <label class="form-check-label" for="other"><b>Other (specify)</b></label>
                                    <div id="other_have_you_come_to_know" style="display: none;">
                                      <input type="text" name="specify" id="specify" value="" class="form-control">
                                    </div>
                                  </div>
      <!--     <div class="form-check">
    <input type="radio" class="form-check-input" id="other"  value="Other" name="other_specify">
    <label class="form-check-label" for="other"><b>Other (specify)</b></label>
    <div id="other_have_you_come_to_know" style="display: none;">
      <input type="text" name="specify" id="specify" value="" class="form-control">
    </div>
  </div> -->
            </td>
          
             <td><input type="text" class="form-control txtNumeric" data-toggle="tooltip" title="Year !" 
              id="otherpassingyear" name="otherpassingyear" maxlength="4" minlength="2" placeholder="Enter Year" value="<?php echo set_value("otherpassingyear");?>" >
                </td>

                <td> <input type="text" class="form-control alphabateonly" data-toggle="tooltip" title="Board/ University !" 
              id="otherboarduniversity" name="otherboarduniversity" maxlength="150" minlength="2" placeholder="Enter Board/ University" value="<?php echo set_value("otherboarduniversity");?>" >
                </td>
            <td>  <input type="text" class="form-control alphabateonly" maxlength="150" minlength="2" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo set_value("otherschoolcollegeinstitute");?>" >
                </td>
            
             <td><input type="text" class="form-control alphabateonly" maxlength="30" minlength="2" data-toggle="tooltip" title="Place !" 
              id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo set_value("otherplace");?>" >
                </td>
               <td><input type="text" class="form-control alphabateonly" maxlength="150" minlength="2" data-toggle="tooltip" title="Specialisation !" 
              id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo set_value("otherspecialisation");?>" >
                 </td>
            
           
           
            <td><input type="text" class="form-control txtNumeric"  maxlength="3" minlength="2"
              id="otherpercentage" name="otherpercentage" placeholder="Percentage" value="<?php echo set_value("otherpercentage");?>" >
                 </td>
          </tr>
        </tbody>
      </table>
   
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                <button type="reset" class="btn btn-warning  btn-sm m-t-10 waves-effect">Reset </button>
               <button type="submit" class="btn btn-success  btn-sm m-t-10 waves-effect">Submit </button>
                <a href="http://www.pradan.net/" class="btn btn-danger btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Close">Close</a> 
            </div>  
      </div>
     </div>
      </div>
      </div>
    </div><!-- /.panel-->
  </div>
 </div>
</div>
</form>
</div></div></div></div></div>
<script type="text/javascript">
  $(document).ready(function() {
    $("input[name$='other_specify']").click(function() {
     // var test = $(this).val();
      if ($(this).is(':checked')) {
        $("#other_have_you_come_to_know").show();
        $("#specify").prop('required','required');
        
      }else{
        $("#other_have_you_come_to_know").hide();
        $("#specify").prop('disabled','true');

      }
      
    });

  });
</script>

<script type="text/javascript">
  
$(document).ready(function(){
  decimalData();
    $('[data-toggle="tooltip"]').tooltip();   

    $(function () {
      $('#presentcity').keydown(function (e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
              e.preventDefault();
          } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                  e.preventDefault();
              }
          }
      });
  });
  $(function () {
      $('#permanentcity').keydown(function (e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
              e.preventDefault();
          } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                  e.preventDefault();
              }
          }
      });
  });
  
});

$(document).ready(function(){
$(".alphabateonly").keypress(function (e){
    var code =e.keyCode || e.which;
       if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46)  
      {
       alert("Only alphabates are allowed");
       return false;
      }
    });


    $("#filladdress").on("click", function(){

         if (this.checked) {
           if ($("#presentstreet").val().length == 0  && $("#permanentstreet").val().length ==0) {
      alert('Please Fill Present Mailing Address !!!');
      $("#filladdress").removeAttr('checked');
    }else{

                $("#permanenthno").val($("#presenthno").val());
                $("#permanentstreet").val($("#presentstreet").val());
                $("#permanentcity").val($("#presentcity").val());
                $("#permanentstateid").val($("#presentstateid").val()); 
               
                  $.ajax({
                    url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ $("#presentstateid").val(),
                    type: 'POST',
                    dataType: 'text',
                  })
                  .done(function(data) {

                    //console.log(data);
                    $("#permanentdistrict").html(data);
                    $("#permanentdistrict").val($("#presentdistrict").val());
                   

                  })
                  $("#permanentdistrict").val($("#presentdistrict").val());
                  $("#permanentpincode").val($("#presentpincode").val()); 


               
               // $("#permanentdistrict").val($("#presentdistrict").val());
               // $("#permanentpincode").val($("#presentpincode").val()); 
                
                                 
    } }
    else {
      
        $("#permanenthno").val('');
        $("#permanentstreet").val('');
        $("#permanentcity").val('');
        $("#permanentstateid").val(''); 
        $("#permanentdistrict").val('');
        $("#permanentpincode").val('');
        $("#permanentstateid").val('');          
    }
    });

});

function decimalData(){
$('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
          event.preventDefault();
   }

   var text = $(this).val();
   if ((event.which == 46) && (text.indexOf('.') == -1)) {
       setTimeout(function() {
           if ($this.val().substring($this.val().indexOf('.')).length > 3) {
               $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
           }
       }, 1);
   }

   if ((text.indexOf('.') != -1) &&
       (text.substring(text.indexOf('.')).length > 2) &&
       (event.which != 0 && event.which != 8) &&
       ($(this)[0].selectionStart >= text.length - 2)) {
           event.preventDefault();
   }      
});



$('.txtOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      $('.error').show();
      $('.error').text('Please Enter Alphabate');
      return false;
      }
    });
}


  $(function() {
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '1920:2030',
      dateFormat : 'dd/mm/yy',
      maxDate: 'today',
        //defaultDate: new Date(2018, 00, 01)
    });
  } );
  </script>
<!-- Jquery Core Js -->
<script src="<?php echo site_url('common/backend/jquery-1.12.4.js');?>"></script>
<script src="<?php echo site_url('common/backend/jquery-ui.js');?>"></script>
 <script type="text/javascript">
   $(document).ready(function(){
      var presentstateid =  $("#presentstateid").val();
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ presentstateid,
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
        console.log(data);
        $("#presentdistrict").html(data);
        $("#permanentdistrict").html(data);
      })
     

      var permanentstateid =  $("#permanentstateid").val();
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getDistrict/'+ permanentstateid,
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
       
        console.log(data);
        $("#permanentdistrict").html(data);
        
      })

      
  

    $("#presentstateid").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
       
        console.log(data);
        $("#presentdistrict").html(data);
        $("#permanentdistrict").html(data);
        
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

     $("#permanentstateid").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>Ajax/getDistrict/'+$(this).val(),
        type: 'POST',
        dataType: 'text',
      })
      .done(function(data) {
       
        console.log(data);
        $("#permanentdistrict").html(data);
        
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });
    
  });     

</script>

