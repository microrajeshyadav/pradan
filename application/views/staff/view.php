<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="body">
                <div class="panel panel-default" >
                  <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Personal Info</b></div>

             <?php //print_r($selectedcandidatedetails); ?>     
            <div class="panel-body">
           <form name="campus" action="" method="post" > 
            <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">First Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Candidate First Name!" name="candidatefirstname" id="candidatefirstname" class="form-control" value="<?php 
                   echo $selectedcandidatedetails[0]->candidatefirstname;?>" placeholder="Enter First Name" readonly="readonly"  >
                 
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Middle name </label>
                <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Candidate Middle Name!" name="candidatemiddlename" id="candidatemiddlename" class="form-control" value="<?php echo $selectedcandidatedetails[0]->candidatemiddlename;?>" placeholder="Enter Middle Name" readonly="readonly">
                <?php echo form_error("candidatemiddlename");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Last Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" data-toggle="tooltip" title="Candidate Last Name!"  id="candidatelastname" name="candidatelastname" placeholder="Enter Last Name " value="<?php echo $selectedcandidatedetails[0]->candidatelastname;?>" readonly="readonly">
                 
                </div>


                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mother's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Mother's First Name!" name="motherfirstname" id="motherfirstname" class="form-control" value="<?php echo $selectedcandidatedetails[0]->motherfirstname;?>" placeholder="Enter First Name" readonly="readonly">
                  
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Middle name </label>
                <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Mother's Middle Name!"  name="mothermiddlename" id="mothermiddlename" class="form-control" value="<?php echo $selectedcandidatedetails[0]->candidatemiddlename;?>" placeholder="Enter Middle Name" readonly="readonly">
               
                </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Last Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" data-toggle="tooltip" title="Mother's Last Name!" id="motherlastname" name="motherlastname" placeholder="Enter Last Name " value="<?php echo $selectedcandidatedetails[0]->candidatelastname;?>" readonly="readonly">
                
                </div>
             

             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Father's Name <span style="color: red;" >*</span></label>
               
                   <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Father's First Name!" name="fatherfirstname" id="fatherfirstname" class="form-control" value="<?php echo $selectedcandidatedetails[0]->fatherfirstname;?>" placeholder="Enter First Name" readonly="readonly" >
                 
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Middle name </label>
                <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Father's Middle Name!"  name="fathermiddlename" id="fathermiddlename" class="form-control" value="<?php echo $selectedcandidatedetails[0]->fathermiddlename;?>" placeholder="Enter Middle Name" readonly="readonly" >
               
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Last Name <span style="color: red;" >*</span></label>
                <input type="text" class="form-control" data-toggle="tooltip" title="Father's Last Name!" id="fatherlastname" name="fatherlastname" placeholder="Enter Last Name " value="<?php echo $selectedcandidatedetails[0]->fatherlastname;?>" readonly="readonly">
                
                </div>


                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Gender <span style="color: red;" >*</span></label>
                <select name="gender" id="gender" class="form-control" >
                <?php 
                $options = array("1" => "Male", "2" => "Female");
                 foreach ($options as $row) {  ?>
                <option value="" Selected><?php echo $row; ?></option>  
                <?php } ?>

                </select>
             
                  <?php echo form_error("gender");?>
                </div>
                
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Nationality  </label>
                <?php 
               $options = array("indian" => "Indian", "other" => "Other");
                echo form_dropdown('nationality', $options, set_value('nationality'), 'class="form-control" readonly="readonly"');
                ?>
                <?php echo form_error("nationality");?>
                </div>
                
            
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Marital Status </label>
                <?php 
               $options = array("1" => "Single", "2" => "Married");
                echo form_dropdown('maritalstatus', $options, set_value('maritalstatus'), 'class="form-control" readonly="readonly"');
                ?>
                <?php echo form_error("maritalstatus");?>
                </div>
                
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Date Of Birth <span style="color: red;" >*</span></label>
                <input type="date" class="form-control datepicker" data-toggle="tooltip" title="Date Of Birth!" id="dateofbirth" name="dateofbirth" placeholder="Enter Date Of Birth " value="<?php echo $selectedcandidatedetails[0]->dateofbirth;?>" readonly="readonly">
                
                </div>

              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Email Id <span style="color: red;" >*</span></label>
                <input type="email" class="form-control" data-toggle="tooltip" title="Email id !" id="emailid" name="emailid" placeholder="Enter Email Id " value="<?php echo $selectedcandidatedetails[0]->emailid;?>" readonly="readonly" >
                
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="Name">Mobile No . <span style="color: red;" >*</span></label>
                 <input type="text"  maxlength="10" data-toggle="tooltip" title="Mobile No!" name="mobile" id="mobile" class="form-control" data-country="India" value="<?php echo $selectedcandidatedetails[0]->mobile;?>" placeholder="Enter Mobile No" readonly="readonly">
                 
                </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; overflow-x: auto;"">
                   
             <table id="tbledubackground" class="table table-bordered table-striped">
              <thead>
             <tr>
              <th colspan="7" style="background-color: #e9e9e9;">Educational Background</th>
           </tr> 
             <tr>
              <th class="text-center" style="vertical-align: top;">Degree/ Diploma/Certificate</th>
              <th class="text-center" style="vertical-align: top;">School/ College/ Institute</th>
              <th class="text-center" style="vertical-align: top;"> Board/ University</th>
              <th class="text-center" style="vertical-align: top;">Specialisation</th>
              <th class="text-center" style="vertical-align: top;">Year </th>
              <th class="text-center" style="vertical-align: top;">Place</th>
              <th class="text-center" style="vertical-align: top;">Percentage(%)</th>
           </tr> 
         </thead>
         <tbody>
          <tr>
            <td><b>10th</b></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->metricschoolcollege;?>" 
              id="10thschoolcollegeinstitute" name="10thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $selectedcandidatedetails[0]->metricschoolcollege;?>" readonly="readonly">
                
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->metricboarduniversity;?>" 
              id="10thboarduniversity" name="10thboarduniversity" placeholder="Enter Board/ University" value="<?php echo $selectedcandidatedetails[0]->metricboarduniversity;?>" readonly="readonly"></td>
                
             <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->metricspecialisation;?>" 
              id="10thspecialisation" name="10thspecialisation" placeholder="Enter Specialisation" value="<?php echo $selectedcandidatedetails[0]->metricspecialisation;?>" readonly="readonly">
                 </td>

            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->metricpassingyear;?>" 
              id="10thpassingyear" name="10thpassingyear" placeholder="Enter Year" value="<?php echo $selectedcandidatedetails[0]->metricpassingyear;?>" readonly="readonly">
                 </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->metricplace;?>" 
              id="10thplace" name="10thplace" placeholder="Enter Place" value="<?php echo $selectedcandidatedetails[0]->metricplace;?>" readonly="readonly">
                </td>
           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->metricpercentage;?>"
              id="10thpercentage" name="10thpercentage" placeholder="Percentage" value="<?php echo $selectedcandidatedetails[0]->metricpercentage;?>" readonly="readonly">
                 </td>
          </tr>
          <tr>
            <td><b>12th</b></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->hscschoolcollege;?>" 
              id="12thschoolcollegeinstitute" name="12thschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $selectedcandidatedetails[0]->hscschoolcollege;?>" readonly="readonly">
                 </td>
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->hscboarduniversity;?>" 
              id="12thboarduniversity" name="12thboarduniversity" placeholder="Enter Board/ University" value="<?php echo $selectedcandidatedetails[0]->hscboarduniversity;?>" readonly="readonly">
                </td>

             <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->hscspecialisation;?>" 
              id="12thspecialisation" name="12thspecialisation" placeholder="Enter Specialisation" value="<?php echo $selectedcandidatedetails[0]->hscspecialisation;?>" readonly="readonly">
                 </td>

            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->hscpassingyear;?>" 
              id="12thpassingyear" name="12thpassingyear" placeholder="Enter Year" value="<?php echo $selectedcandidatedetails[0]->hscpassingyear;?>" readonly="readonly">
                 </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->hscplace;?>" 
              id="12thplace" name="12thplace" placeholder="Enter Place" value="<?php echo $selectedcandidatedetails[0]->hscplace;?>" readonly="readonly">
                 </td>
           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->hscpercentage;?>"
              id="12thpercentage" name="12thpercentage" placeholder="Percentage" value="<?php echo $selectedcandidatedetails[0]->hscpercentage;?>" readonly="readonly">
                </td>
          </tr>
          <tr>
            <td><b>UG</b></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->ugschoolcollege;?>" 
              id="ugschoolcollegeinstitute" name="ugschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $selectedcandidatedetails[0]->ugschoolcollege;?>" readonly="readonly">
                 </td>
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->ugboarduniversity;?>" 
              id="ugboarduniversity" name="ugboarduniversity" placeholder="Enter Board/ University" value="<?php echo $selectedcandidatedetails[0]->ugboarduniversity;?>" readonly="readonly">
                </td>
               <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->ugspecialisation;?>" 
              id="ugspecialisation" name="ugspecialisation" placeholder="Enter Specialisation" value="<?php echo $selectedcandidatedetails[0]->ugspecialisation;?>" readonly="readonly">
                </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->ugpassingyear;?>" 
              id="ugpassingyear" name="ugpassingyear" placeholder="Enter Year" value="<?php echo $selectedcandidatedetails[0]->ugpassingyear;?>" readonly="readonly">
                 </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->ugplace;?>" 
              id="ugplace" name="ugplace" placeholder="Enter Place" value="<?php echo $selectedcandidatedetails[0]->ugplace;?>" readonly="readonly">
                </td>
           
            <td><input type="text" class="form-control" data-toggle="tooltip"  title="<?php echo $selectedcandidatedetails[0]->ugpercentage;?>"
              id="ugpercentage" name="ugpercentage" placeholder="Percentage" value="<?php echo $selectedcandidatedetails[0]->ugpercentage;?>" readonly="readonly">
              </td>
          </tr>
          <tr>
            <td><b>PG</b></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->pgschoolcollege;?>" 
              id="pgschoolcollegeinstitute" name="pgschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $selectedcandidatedetails[0]->pgschoolcollege;?>" readonly="readonly" >
               </td>
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->pgboarduniversity;?>" 
              id="pgboarduniversity" name="pgboarduniversity" placeholder="Enter Board/ University" value="<?php echo $selectedcandidatedetails[0]->pgboarduniversity;?>" readonly="readonly">
                 </td>
               <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->pgspecialisation;?>" 
              id="pgspecialisation" name="pgspecialisation" placeholder="Enter Specialisation" value="<?php echo $selectedcandidatedetails[0]->pgspecialisation;?>" readonly="readonly">
                </td>

            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->pgpassingyear;?>" 
              id="pgpassingyear" name="pgpassingyear" placeholder="Enter Year" value="<?php echo $selectedcandidatedetails[0]->pgpassingyear;?>" readonly="readonly" >
                 </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->pgplace;?>" 
              id="pgplace" name="pgplace" placeholder="Enter Place" value="<?php echo $selectedcandidatedetails[0]->pgplace;?>" readonly="readonly">
                </td>
           
            <td><input type="text" class="form-control" data-toggle="tooltip" title="<?php echo $selectedcandidatedetails[0]->pgpercentage;?>"
              id="pgpercentage" name="pgpercentage" placeholder="Percentage" value="<?php echo $selectedcandidatedetails[0]->pgpercentage;?>" readonly="readonly">
                </td>
          </tr>

          <tr>
            <td><b>If Others,Specify</b></td>
            <td>  <input type="text" class="form-control" data-toggle="tooltip" title="School/ College/ Institute !" 
              id="otherschoolcollegeinstitute" name="otherschoolcollegeinstitute" placeholder="Enter School/ College/ Institute" value="<?php echo $selectedcandidatedetails[0]->otherschoolcollege;?>" readonly="readonly">
                </td>
            <td> <input type="text" class="form-control" data-toggle="tooltip" title="Board/ University !" 
              id="otherboarduniversity" name="otherboarduniversity" placeholder="Enter Board/ University" value="<?php echo $selectedcandidatedetails[0]->otherboarduniversity;?>" readonly="readonly">
                </td>
               <td><input type="text" class="form-control" data-toggle="tooltip" title="Specialisation !" 
              id="otherspecialisation" name="otherspecialisation" placeholder="Enter Specialisation" value="<?php echo $selectedcandidatedetails[0]->otherspecialisation;?>" readonly="readonly">
                 </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Year !" 
              id="otherpassingyear" name="otherpassingyear" placeholder="Enter Year" value="<?php echo $selectedcandidatedetails[0]->otherpassingyear;?>" readonly="readonly">
                </td>
            <td><input type="text" class="form-control" data-toggle="tooltip" title="Place !" 
              id="otherplace" name="otherplace" placeholder="Enter Place" value="<?php echo $selectedcandidatedetails[0]->otherplace;?>" readonly="readonly">
                </td>
           
            <td><input type="text" class="form-control" 
              id="otherpercentage" name="otherpercentage" placeholder="Percentage" value="<?php echo $selectedcandidatedetails[0]->otherpercentage;?>" readonly="readonly">
                 </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                <a href="<?php echo site_url("CandidatesInfo");?>" class="btn btn btn-sm m-t-10 waves-effect" data-toggle="tooltip" style="background-color:#800000; color: #fff"  title="Back">Back</a> 
            </div>  
      </div>
     </div>
                 </form> 
                </div>

              </div><!-- /.panel-->
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Exportable Table -->
    </div>
  </section>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  
});
</script>