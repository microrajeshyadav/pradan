
<style type="text/css">
  tr.group,
  tr.group:hover {
    background-color: #ddd !important;
  }

</style>

<section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Generatesalary" && $row->Action == "index"){ ?>
    <br>
    <div class="container-fluid">   
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
        <div class="panel-heading">
          <div class="row">
           <h4 class="col-md-12 panel-title pull-left">Calculate CTC</h4>

         </div>
         <hr class="colorgraph"><br>
       </div>


       <div class="panel-body">

        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>

          <div class="row">
            <div class="col-md-12">

              <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>

              </div>
            </div>

          <?php } else if(!empty($er_msg)){?>

            <div class="row">
              <div class="col-md-12">

                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>

                </div>
              </div>

            <?php } ?>
            <form name="frmgenratesalary" id="frmgenratesalary" action="" method="post">   
              <div class="row bg-light" style="padding: 20px;">

                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                  Office: 
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                  <select class="form-control" name="transfer_office" id="transfer_office" required="required">
                    <option value="0">All</option>
                    <?php foreach($all_office as $val){?>
                      <option value="<?php echo $val->officeid;?>" ><?php echo $val->officename;?> </option>
                    <?php  } ?>
                  </select>
                </div>
                <div class="col-md-1 text-right"><label>Level : </label></div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"> 
                  <select name="levelname" id ="levelname" class="form-control" required="">
                    <option value="1"> 1 </option>
                    <option value="2"> 2 </option>
                    <option value="3"> 3 </option>
                    <option value="4"> 4 </option>
                    <option value="5"> 5 </option>
                    <option value="6"> 6 </option>
                  </select>
                </div>
                <div class="col-md-1 text-right"><label>Experience:</label></div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">  
                  <select name="Experience" id ="Experience" class="form-control" required="">
                    <?php 
                    for ($i=0; $i <= 39; $i++) { ?> 
                     <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                   <?php } ?>
                 </select>
               </div>
               <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                Month: 
              </div>
              <div id="salmonth" class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                <select name="salmonth" class="form-control">
                  <option value="03"> Mar</option>
                  <option value="04"> Apr</option>
                  <option value="05"> May</option>
                  <option value="06"> Jun</option>
                  <option value="07"> Jul</option>
                  <option value="08"> Aug</option>
                  <option value="09"> Sep</option>
                  <option value="10"> Oct</option>
                  <option value="11"> Nov</option>
                  <option value="12"> Dec</option>
                  <option value="01"> Jan</option>
                  <option value="02"> Feb</option>

                </select>
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"><label>Financial Year : </label></div>
              <div class="col-md-2">
                <select id="financialyear" name ="financialyear" class="form-control" required>
                  <option value=""> Select</option>
                 <option value="<?php echo $fyear; ?>"> <?php echo $fyear; ?></option>
                 <?php 
                 $fyearexp = '';
                 $prevyear = '';
                 $nextyear = '';
                 $nextfyear = '';
                 $fyearexp  = explode('-', $fyear); 
                 $prevyear = $fyearexp[0]+1;
                 $nextyear = $fyearexp[1]+1;
                 $nextfyear = $prevyear.'-'.$nextyear;
                 ?>
                 <option value="<?php echo $nextfyear; ?>"> <?php echo $nextfyear; ?></option>
               </select>
             </div>
             <div class="col-md-11">
              <div id="table" name ="table" class="table table-bordered table-striped dataTable no-footer">
              </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-left">
              <input type="button" name="btngeneratesalary" value="Calculate CTC" class="btn btn-sm btn-info" onclick="input();">
            </div>

          </div> 
          <div class="row" style="padding: 20px;">
          </div>

          <div class="row" style="background-color: #FFFFFF; margin-top:20px;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white; overflow: scroll">
              <table id="tablestaff" name ="tablestaff" class="table dt-responsive table-bordered table-striped dataTable no-footer">
               <thead>
                 <tr>
                  <th class="text-left">#</th>
                  <th class="text-left">Office</th>
                  <th class="text-left" >level</th>
                  <th class="text-left">No. of year experince</th>
                  
                  <?php 
                  if (count($getactivehead) >0) { 
                    foreach ($getactivehead as $key => $value) { ?> 
                      <th class="text-center"><?php echo $value->fielddesc; ?></th> 
                    <?php } } ?>              
                    
                  </tr> 
                </thead> 



                <tbody>

                  <?php $i=0; 
                  if ($staffsalary['result'] >0) {

                    foreach ($staffsalary['result'] as $key => $value) {

                      ?>
                      <tr>
                       <td><?php echo $i+1; ?></td>
                       <?php $j=0;  foreach ($value as $row) { 

                        if ($j > 3) {?>
                          <td class="text-right"><label id="otheramt_<?php echo $j; ?>"><?php echo number_format($row); ?></label></td>
                        <?php  }else{ 
                         if($j == 3){ //for basic salary decimal value?> 
                          <td class="text-left"><label id="otheramt_<?php echo $j; ?>"><?php echo number_format($row); ?></label></td>
                        <?php }else{ //static <th> ?>
                          <td class="text-left"> <?php echo $row; ?></td>
                        <?php } ?>

                      <?php } $j++;}  ?>

                    </tr>
                    <?php $i++; } } ?>

                  </tbody>

                </table>
              </div>
            </div> 
          </form>
        </div>
      </div>
    </div>   
  <?php } } ?>
</section>

<button onclick="copy('#tablestaff')" type="button">Click to Copy</button>

<script type="text/javascript">


  //input to comma separated with decimal
  function intTo(nStr) {
    var cid='#'+nStr.id;
    var x1 = $(cid).val(intToNumberBudget(nStr.value));
  }
  

  function selectText(el){
    var doc = document;
    var element = document.getElementById(el);
    if (doc.body.createTextRange) {
      var range = document.body.createTextRange();
      range.moveToElementText(element);
      range.select();
    } else if (window.getSelection) {
      var selection = window.getSelection();        
      var range = document.createRange();
      range.selectNodeContents(element);
      selection.removeAllRanges();
      selection.addRange(range);
    }
  }
  function copy(selector) {
    var $temp = $("<div id='toCopy'>");
    $("body").append($temp);
    $temp.attr("contenteditable", true)
    .html($(selector).html());
    selectText("toCopy");
    document.execCommand("copy");
    $temp.remove();
  }
</script>
<script>


  $(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip(); 
    $('input[type=text]').hide();  


  });





  $('#frmgenratesalary input[type="checkbox"]').change(function() { 
   var resoninputid = $(this).attr("id");
   if ($('#'+resoninputid).is(':checked')) {
     resoninputid = resoninputid.replace('stopsalary','reason');
     $('#'+resoninputid).show();
     $('#'+resoninputid).prop('required', true);

   }else{
     resoninputid = resoninputid.replace('stopsalary','reason');
     $('#'+resoninputid).val('');
     $('#'+resoninputid).prop('required', false);
     $('#'+resoninputid).hide();
   }

 });


  $('#financialyear').change(function() {
    var financialyear = $('option:selected', this).val();
    if (financialyear != '') {
      $.ajax({
       url:'<?php echo site_url(); ?>ajax/getfinicaldetails/'+financialyear,
       method:'POST',
       success:function(data) {
        $('#table').html(data);
      }
    });
    }else{
      alert('Please Select Financial Year');
    }

  });
 
  $(document).ready(function() {


    $('#tablestaff').DataTable();
    $('[data-toggle="tooltip"]').tooltip();    
    var groupColumn = 3;
/*    var table = $('#tablestaff').DataTable({
      "columnDefs": [
      { "visible": false, "targets": groupColumn }
      ],
      "order": [[ groupColumn, 'asc' ]],
      "displayLength": 25,
      "drawCallback": function ( settings ) {
        var api = this.api();
        var rows = api.rows( {page:'current'} ).nodes();
        var last=null;

        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
          if ( last !== group ) {
            $(rows).eq( i ).before(
              '<tr class="group"><td colspan="8">'+group+'</td></tr>'
              );

            last = group;
          }
        } );
      }
    } );*/

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
        table.order( [ groupColumn, 'desc' ] ).draw();
      }
      else {
        table.order( [ groupColumn, 'asc' ] ).draw();
      }
    } );

    // $('#table').DataTable();
    //   $('[data-toggle="tooltip"]').tooltip();    

  } );
</script>  


   <!--  <script type="text/javascript">
     $(document).ready(function(){
      $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
    });
  </script> -->
  <script type="text/javascript">
    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>