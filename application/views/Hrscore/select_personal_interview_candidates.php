 <link rel="stylesheet" type="text/css" href="<?php echo site_url('common/css/jquery.dataTables.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('common/css/buttons.dataTables.min.css');?>">
<script type="text/javascript" src="<?php echo site_url('common/js/jquery-3.3.1.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/dataTables.buttons.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/jszip.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/pdfmake.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.html5.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/ajax/libs/vfs_fonts.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url('common/js/buttons/buttons.print.min.js');?>"></script>

 <section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Hrscore" && $row->Action == "select_personal_interview_candidates"){ ?>

  <div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">List of eligible candidates for personal interview </h4>
      
    </div>
      <hr class="colorgraph"><br>
    </div>
    <div class="panel-body">

    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
    
          <?php //echo $campusid; ?>
           <form name="hrscoresearch" id="hrscore" method="post" action="">
       <div class="row" >
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right" style="text-align: middle;"><label for="StateNameEnglish" class="field-wrapper required-field"> Campus Name : </label> </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-left"> 
          <select name="campusid" id="campusid" class="form-control" required="required">
          <option value="">Select Campus</option>
          <?php foreach ($campusdetails as $key => $value) {
              $expdatefrom = explode('-', $value->fromdate);
              $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
              $expdateto = explode('-', $value->todate);
              $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];

               $campval  = $value->campusid.'-'. $value->id; 

              if ($campval == $campusid) {
           ?>
            <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
          <?php }else{ ?>
             <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
         <?php } } ?>
        </select> 
      <?php echo form_error("campusid");?>
       </div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-left" style="background-color: white;"><button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-round btn-dark btn-sm">Search</button></div>
        <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;"></div>
       </div>
    </form>
        <br>
        <div class="col-xs-10 col-sm-10 col-md-12 col-lg-12" style="background-color: white;">
          <table id="HRScorelist" class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
             <tr>
              <th  style="width: 30px;"  class="text-center">S. No.</th>
              <th>Category</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Email Id</th>
              <th>Stream</th>
           </tr> 
         </thead>
         <tbody>
          <?php
            if (count($selectedcandidatedetails)==0) { ?>
        
           <?php }else{
          $i=0; foreach ($selectedcandidatedetails as $key => $value) {  ?>
            <tr>
              <td class="text-center"><?php echo $i+1; ?></td>
              <td><?php echo $value->categoryname;?></td>
              <td><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></td>
              <td><?php echo $value->gender;?></td>
              <td><?php echo $value->emailid;?></td>
              <td><?php echo $value->stream;?></td>
            </tr>
            
             <?php $i++; } } ?>
           
          </tbody>
        </table>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-footer" style="text-align: right;">
      
       <a href="<?php echo site_url("Hrscore");?>" class="btn btn-round btn-dark btn-sm" data-toggle="tooltip" title="Close">Close</a> 
       </div>
       </div>
</div>
 </div>
</div>

<?php } } ?>
</section>

<!--  JS Code Start Here  -->
<script type="text/javascript">
  
  $(document).ready(function(){
    $('#HRScorelist').DataTable( {
      dom: 'Bfrtip',
      buttons: [
            'excelHtml5',
            'pdfHtml5',
             'print'
      ],
      "bPaginate": true,
       "bInfo": false,
       "bFilter": true,
       "bLengthChange": false
    } );
  });
</script>

<!--  JS Code End Here  -->