 <section class="content" style="background-color: #FFFFFF;" >
  <?php foreach ($role_permission as $row) { if ($row->Controller == "Hrscore" && $row->Action == "index"){ ?>
    <br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Personal Interview</h4>
      
    </div>
      <hr class="colorgraph"><br>
    </div>

    
      <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
            </div>
          </div>
        </div>
      </div>
      <?php } else if(!empty($er_msg)){?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        
         <form name="hrscoresearch" id="hrscore" method="post" action="">
          <div class="row" >
         <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12"></div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-right" style="text-align: middle;"><label for="StateNameEnglish" class="field-wrapper required-field"> Campus Name : </label> </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-left"> 
          <select name="campusid" id="campusid" class="form-control" required="required">
          <option value="">Select Campus</option>
          <?php foreach ($campusdetails as $key => $value) {
              $expdatefrom = explode('-', $value->fromdate);
              $newfromdate = $expdatefrom[2].'/'.$expdatefrom[1].'/'.$expdatefrom[0];
              $expdateto = explode('-', $value->todate);
              $newtodate = $expdateto[2].'/'.$expdateto[1].'/'.$expdateto[0];

               $campval  = $value->campusid.'-'. $value->id; 

              if ($campval == $campusid) {
           ?>
            <option value="<?php echo $value->campusid.'-'. $value->id;?>" SELECTED><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
          <?php }else{ ?>
             <option value="<?php echo $value->campusid.'-'. $value->id;?>"><?php echo $value->campusname .'-('. $newfromdate .'-'. $newtodate .')'; ?></option>
         <?php } } ?>
        </select> 
      <?php echo form_error("campusid");?>
       </div>
        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 text-left" style="background-color: white;"><button type="submit" name="search" id="search" value="SearchCampus" class="btn btn-round btn-dark btn-sm">Search</button></div>
        <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;"></div>
       
    </form>
        <br>
        <br>
       </div>
       </b>
      <form name="gdscore" id="gdscore" method="post" action="" >
      <input type="hidden" name="campusid" id="" value="<?php if(isset($campusid)) echo $campusid;?>">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"></div>
      </div>
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;"><div style="font-size: 15px; font-weight: 700;" ></div><br> 
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
          <table id="tableHrscoreScore" class="table table-bordered dt-responsive table-striped table-hover dataTable js-exportable">
            <thead>
             <tr>
              <th  style="width: 30px;" class="text-center">S. No.</th>
              <th>Category</th>
              <th>Campus Name</th>
              <th>Name</th>
              <th>Gender</th>
              <th> Email Id</th>
              <th>Stream</th>
              <th>HR Score</th>
           </tr> 
         </thead>
        
          <?php 
            if (count($selectedcandidatedetails)==0) { ?>
              <tbody></tbody>
           <?php }else{

          $i=0; foreach ($selectedcandidatedetails as $key => $value) {  

              $selectedcandidatehrscore = $this->model->getSelectCandidHRScore($value->candidateid);
            ?>
             <tbody>
            <tr>
              <td class="text-center"><?php echo $i+1; ?></td>
              <td><?php echo $value->categoryname;?></td>
              <td><?php echo $value->campusname;?></td>
              <td><a href="<?php echo site_url().'Hrscore/preview/'.$value->candidateid;?>" class="btn btn-warning btn-xs" target="_blank" ><?php echo $value->candidatefirstname;?> <?php echo $value->candidatemiddlename;?> <?php echo $value->candidatelastname;?></a></td>
              <td><?php echo $value->gender;?></td>
              <td><?php echo $value->emailid;?></td>
              <td><?php echo $value->stream;?></td>
            <td style="width: 100px;">
              <select name="hrscore[<?php echo $value->candidateid;?>]"  id="hr_<?php echo $i;?>" class="form-control" onchange="setValues(this.id)">
                <?php
                  if($selectedcandidatehrscore){
                  if($selectedcandidatehrscore->hrscore ==2 ){?>
                  <option value="1"> No</option>
                  <option value="2" SELECTED="SELECTED">Yes</option>
                <?php  }else{ ?>
                  <option value="1">No</option>
                  <option value="2">Yes</option>
                <?php } }else{ ?>
                  <option value="1">No</option>
                  <option value="2">Yes</option>  
                <?php } ?>             
              </select>
              <input type="hidden" name="email_[<?php echo $i;?>]" id="email_<?php echo $i;?>">
             </td>
             </tr>
           </tbody>
             <?php $i++; } } ?>


        </table>
       </div>
        <?php 
            if (count($selectedcandidatedetails) > 0) { ?>
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
            <button  type="submit" name="save_hrscore" id="save_hrscore" class="btn btn-success " data-toggle="tooltip" title="Save" value="save" >Save</button>
          </div>
        <?php } ?>
      </div>
      </form> 
     </div>
    </div>
  </div>   
<?php } } ?>
</section>

<script type="text/javascript"> 
  function setValues (id)
  {
var hdnId =  id.replace("hr", "email");
$("#"+hdnId).val($('#'+id).val());
  }
$("#hr").change(function(){
  alert($(this).val());
  var selected = $(this).val();
  // 1 = no and 2 = yes
      if (selected ==1) {
        $('#email').val('no');
      }
      else
      {
        $('#email').val('yes');
      }
      alert($('#email').val());
}); 

$(document).ready(function () {
  $('#tableHrscoreScore').DataTable(); 
  //called when key is pressed in textbox
  $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });


  


 // $('#save_hrscore').click(function() {
        
 //          if (!confirm('Are you sure? after save not modified score')) {
 //              return false;
 //            }
 //      });


});
</script>