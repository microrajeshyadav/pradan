<br>
<style type="text/css">
/*.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
    height: 275px;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
    padding: 2px 16px;
    }*/
/* #tbledubackground  {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#tbledubackground td, #tbledubackground th {
    border: 1px solid #ddd;
    padding: 8px;
    text-align: center;
}

#tbledubackground tr:nth-child(even){background-color: #f2f2f2; text-align: center;}

#tbledubackground tr:hover {background-color: #ddd; }


#tbledubackground th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
   /* background-color: #95A5A6;
   color: white;*/
   /*}*/


/*
 #tblForm09  {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#tblForm09 td, #tblForm09 th {
    border: 1px solid #ddd;
    padding: 8px;
}

#tblForm09 tr:nth-child(even){background-color: #f2f2f2;}

#tblForm09 tr:hover {background-color: #ddd;}

#tblForm09 th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #95A5A6;
    /*color: white;*/
    /*}*/


    label{

      font-weight: bold;
      font-family: 
    }
    .bg-lightgreen
    {
      background-color: #85a4a5;
    }
  </style>
  <div class="container-fluid" id="tblForm09" style="font-family: 'Oxygen' !important;" >
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
     <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Biodata Form</h4>
       
     </div>
     <hr class="colorgraph"><br>
   </div>
   <div class="panel-body">
    <div class="col-lg-12 col-md- col-sm-12 col-xs-12">

      <table id="tblForm09" class="table table-bordered table-striped">
       <thead>
         <tr>
          <th  colspan="7" style=" height: 5px;" class="bg-lightgreen text-white">Personal Infomation</th>
        </tr> 
      </thead>
      <tbody>
        <tr style="background-color: #fff;">
          <td rowspan ="6" class="text-left bg-light">

          <!-- <?php if ($candidatedetails->encryptedphotoname !='') { ?>
          <img class= "rounded-circle" src="<?php echo site_url().'datafiles/'.$candidatedetails->encryptedphotoname;?>" alt="Jane" width="160px" height="160px">
          <?php }else{ ?>
          <img class= "rounded-circle" src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>" alt="Jane" width="160px" height="160px">
          <?php } ?> -->
          <?php 
          if($candidatedetails->encryptedphotoname)
          {
            $file_path=''; 
            $file_path=FCPATH.'datafiles/'. $candidatedetails->encryptedphotoname;

            if(file_exists($file_path))
            {
              ?>

              <img src="<?php echo site_url().'datafiles/'. $candidatedetails->encryptedphotoname;?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">

              <?php } 
              else
              {
                ?>
                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
                <?php 
              }
            }
            else
            {
              ?>
              <img src="<?php echo site_url().'datafiles/imagenotfound.jpg'?>" width="93px" hight="93px" title="Profile Image" class="rounded-circle">
              <?php 

            }?>
            <h4><?php if(!empty($candidatedetails->candidatefirstname))
            echo $candidatedetails->candidatefirstname .' '.$candidatedetails->candidatemiddlename.' '.$candidatedetails->candidatelastname;
            ?> 
          </h4>
          <p style= "font-weight: 700"><i class="fa fa-envelope" style="font-size: 14px; color: #17A2B8;"></i> <?php if(!empty($candidatedetails->emailid)) echo $candidatedetails->emailid;?></p>
          <p style= "font-weight: 700"><i class="fa fa-mobile" aria-hidden="true" style="font-size: 14px; color: #17A2B8;"></i>
            <?php if(!empty($candidatedetails->mobile)) echo $candidatedetails->mobile;?></p>



          </td>
          <td><label for="Name">Candidate Name :</label></td>
          <td><?php if(!empty($candidatedetails->candidatefirstname)) echo $candidatedetails->candidatefirstname;?></td>
          <td><label for="Name">Middle Name :</label></td>
          <td style="width: auto;"><?php if(!empty($candidatedetails->candidatemiddlename)) echo $candidatedetails->candidatemiddlename;?></td>
          <td><label for="Name">Last Name :</label></td>
          <td><?php if(!empty($candidatedetails->candidatelastname)) echo $candidatedetails->candidatelastname;?></td>

        </tr>
        <tr style="background-color: #fff;">
          <td><label for="Name">Mother's Name :</label></td>
          <td><?php if(!empty($candidatedetails->motherfirstname)) echo $candidatedetails->motherfirstname;?></td>
          <td><label for="Name">Middle Name :</label></td>
          <td style="width: auto;"><?php if(!empty($candidatedetails->mothermiddlename)) echo $candidatedetails->mothermiddlename;?></td>
          <td><label for="Name">Last Name :</label></td>
          <td><?php if(!empty($candidatedetails->candidatelastname)) echo $candidatedetails->candidatelastname;?></td>
        </tr>
        <tr style="background-color: #fff;">
          <td><label for="Name">Father's Name :</label></td>
          <td><?php if(!empty($candidatedetails->fatherfirstname)) echo $candidatedetails->fatherfirstname;?></td>
          <td><label for="Name">Middle Name :</label></td>
          <td style="width: auto;"><?php if(!empty($candidatedetails->fathermiddlename)) echo $candidatedetails->fathermiddlename;?></td>
          <td><label for="Name">Last Name :</label></td>
          <td><?php if(!empty($candidatedetails->fatherlastname)) echo $candidatedetails->fatherlastname;?> </td>



        </tr>
        <tr style="background-color: #fff;">
          <td><label for="Name">Gender :</label></td>
          <td><?php 
          $options = array("1" => "Male", "2" => "Female", "3" => "Others");
          if ($candidatedetails->gender ==1) {
            echo "Male";
          }elseif ($candidatedetails->gender ==2) {
           echo "Female";
         }elseif ($candidatedetails->gender ==3) {
           echo "Others";
         } ?>
       </td>
       <td><label for="Name">Nationality :</label></td>
       <td style="width: auto;"> <?php if(!empty($candidatedetails->nationality)) echo  $candidatedetails->nationality;?> </td>
       <td><label for="Name">Marital Status :</label></td>
       <td> <?php 
       $options = array("1" => "Single", "2" => "Married", "3" => "Divorced", "4" => "Widow", "5" => "Separated");
       if ($candidatedetails->maritalstatus ==1) {
        echo "Single";
      }else if ($candidatedetails->maritalstatus ==2) {
        echo "Married";
      }else if ($candidatedetails->maritalstatus ==3) {
        echo "Divorced";
      }else if ($candidatedetails->maritalstatus ==4) {
        echo "Widow";
      }else if ($candidatedetails->maritalstatus ==5) {
        echo "Separated";
      }
      ?> 
    </td>
  </tr>

  <tr style="background-color: #fff;">
    <td><label for="Name">Annual income of parents :</label></td>
    <td><?php 
    if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==1) {
     echo "Below 50,000";
   }else if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==2) {
    echo "50,001-200,000";
  }else if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==3) {
    echo "200,001-500,000";
  }else if (!empty($otherinformationdetails->annual_income) && $otherinformationdetails->annual_income==4) {
   echo "Above 500,000";
 }else{
  echo "NA";
}
?>
</td>
<td><label for="Name">No. of male sibling :</label></td>
<td style="width: auto;"><?php if(!empty($otherinformationdetails->male_sibling)) {echo $otherinformationdetails->male_sibling; } else{ echo "NA"; }
?></td>
<td><label for="Name">No. of female sibling :</label></td>
<td><?php if(!empty($otherinformationdetails->female_sibling)) { echo $otherinformationdetails->female_sibling;} else{ echo "NA"; } ?> </td>
</tr>
<tr style="background-color: #fff;">
  <td><label for="Name">Date Of Birth :</label></td>
  <td><?php if(!empty($candidatedetails->dateofbirth)) echo $this->model->changedatedbformate($candidatedetails->dateofbirth);?></td>
  <td><label for="Name">Email Id :</label></td>
  <td style="width: auto;"> <?php if(!empty($candidatedetails->emailid)) echo $candidatedetails->emailid;?></td>
  <td><label for="Name">Mobile No. :</label></td>
  <td><?php if(!empty($candidatedetails->mobile)) echo $candidatedetails->mobile;?></td>
</tr>

</tbody>
</table>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table id="tbledubackground" class="table table-bordered table-striped" >
      <thead>
       <tr>
         <th colspan="8" class="bg-lightgreen text-white">

          <label class="form-check-label" for="filladdress"><b>Communication Address </b></label>

        </th>
      </tr> 
      <?php  if ($candidatedetails->presenthno==$candidatedetails->permanenthno) {
       $checkdata = 'checked="checked"';
     } ?>  
     <tr >
      <th class="text-center bg-light" colspan="4" style="vertical-align: top; text-align: center; "> Present Mailing Address</th>
      <th class="text-center" colspan="4" style="vertical-align: top; text-align: center;"> Permanent Mailing Address<br>
      </th>
    </tr> 
  </thead>
  <tbody>
   
   <tr style="background-color: #fff;">
     
    <td class="bg-light"> <label for="Name">H.No:</label></td>
    <td class="bg-light"><?php if(!empty($candidatedetails->presenthno)) echo $candidatedetails->presenthno;?></td>
    <td class="bg-light"><label for="Name">Street:</label></td>
    <td class="bg-light"><?php if(!empty($candidatedetails->presentstreet)) echo $candidatedetails->presentstreet;?></td>
    
    <td><label for="Name">H.No :</label></td>
    <td><?php if(!empty($candidatedetails->permanenthno)) echo $candidatedetails->permanenthno;?></td>
    <td><label for="Name">Street :</label></td>
    <td><?php if(!empty($candidatedetails->permanentstreet)) echo $candidatedetails->permanentstreet;?></td>
  </tr>
  <tr style="background-color: #fff;">
    <td class="bg-light"><label for="Name">State:</label></td>
    <td class="bg-light"><?php if(!empty($candidatedetails->presentstatename)) echo  $candidatedetails->presentstatename;?></td>
    <td class="bg-light"> <label for="Name">City:</label> </td>
    <td class="bg-light"><?php if(!empty($candidatedetails->presentcity)) echo $candidatedetails->presentcity;?></td>
    <td ><label for="Name">State :</label></td>
    <td><?php if(!empty($candidatedetails->permanentstatename)) echo $candidatedetails->permanentstatename;?></td>
    <td><label for="Name">City :</label></td>
    <td><?php if(!empty($candidatedetails->permanentcity)) echo $candidatedetails->permanentcity;?></td>
  </tr>
  <tr style="background-color: #fff;">
    <td class="bg-light"><label for="Name">District:</label> </td>
    <td class="bg-light"><?php if(!empty($candidatedetails->presentdistrictname)) echo $candidatedetails->presentdistrictname;?></td>
    <td class="bg-light"><label for="Name">Pin Code:</label></td>
    <td class="bg-light"><?php if(!empty($candidatedetails->presentpincode)) echo $candidatedetails->presentpincode;?></td>
    <td><label for="Name">District :</label></td>
    <td><?php if(!empty($candidatedetails->permanentdistrictname)) echo $candidatedetails->permanentdistrictname;?></td>
    <td><label for="Name">Pin Code :</label></td>
    <td><?php if(!empty($candidatedetails->permanentpincode)) echo $candidatedetails->permanentpincode;?></td>

  </tr>
</tbody>
</table>
</div>  
</div>
<?php if ($familycount->Fcount != 0){ ?> 
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <table id="tblForm09" class="table table-bordered table-striped" >
    <thead>
     <tr>
      <th colspan="4" class="bg-light">Family Members </th>
    </tr> 
    <tr>
     <th style="text-align: center;" class="bg-light"> Name</th>
     <th style="text-align: center;" class="bg-light">Relation with Employee</th>
     <th style="text-align: center;" class="bg-light">Date of Birth</th>
   </tr>
 </thead>
 <tbody id="tbodyForm09" >
  <?php $i=0;
  foreach ($familymemberdetails as $key => $val) {  $i++; ?>
  <tr id="tbodyForm09" style="text-align: center; background-color: #fff;" >
   <td><?php if(!empty($val->Familymembername)) echo $val->Familymembername ?></td>
   <td><?php if(!empty($val->relationname)) echo $val->relationname;?></td>
   <td><?php if(!empty($val->familydob)) echo $this->model->changedatedbformate($val->familydob);?></td>
 </tr>
 <?php }  ?>
</tbody>

</table>
</div>
</div>
<?php } ?>
<?php if ($identitycount->Icount !=0) { ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

   <table id="tblForm09" class="table table-bordered table-striped" >
    <thead>
     <tr>
      <th colspan="5" class="bg-light">
        <div class="col-lg-12">
          <div class="col-lg-6" class="bg-light">Identity Details</div>
          <div class="col-lg-6 text-right ">

          </div>
        </div>  </th>
      </tr> 
      <tr class="bg-light">
       <th style="text-align: center;" class="bg-light">Identity</th>
       <th style="text-align: center;" class="bg-light">Number</th>
       <th style="text-align: center" class="bg-light">Identity Name</th>
       <th style="text-align: center" class="bg-light">Father Name </th>
       <th style="text-align: center" class="bg-light">Date of birth</th>
       
     </tr>
   </thead>
   <tbody id="tbodyForm13" >

    <?php $i=0;
    foreach ($identitydetals as $key => $val) { $i++; ?>
    <tr id="tbodyForm13" style="text-align: center; background-color: #fff;" >
     <td><?php if(!empty($val->name)) echo $val->name; ?></td>
     <td><?php if(!empty($val->identitynumber)) echo $val->identitynumber;?></td>
     <td><?php if(!empty($val->iname)) echo $val->iname;?></td>
     <td><?php if(!empty($val->ifather)) echo $val->ifather;?></td>
     <td><?php if(!empty($val->idateofbirth)) echo $this->model->changedatedbformate($val->idateofbirth);?></td>
   </tr>
   <?php } ?>
 </tbody>

</table>
</div>
</div>

<?php }?>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; ">

    <table id="tbledubackground" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white">Educational Background</th>
      </tr> 
      <tr>
        <th class="text-left" style="vertical-align: top;">Course</th>

        <th class="text-center" style="vertical-align: top;">Year </th>
        <th class="text-left" style="vertical-align: top;"> Board/ University</th>
        <th class="text-left" style="vertical-align: top;">School/ College/ Institute</th>
        <th class="text-left" style="vertical-align: top;">Place</th>
        <th class="text-left" style="vertical-align: top;">Specialisation</th>

        <th class="text-right" style="vertical-align: top;">Percentage(%)</th>

      </tr> 
    </thead>
    <tbody>
      <tr style="">
        <td><b>10th</b></td>
        <td class="text-center"><?php if(!empty($candidatedetails->metricpassingyear)) echo $candidatedetails->metricpassingyear;?>
        </td>
        <td> <?php if(!empty($candidatedetails->metricboarduniversity)) echo $candidatedetails->metricboarduniversity;?></td>
        <td>  <?php if(!empty($candidatedetails->metricschoolcollege)) echo $candidatedetails->metricschoolcollege;?></td>

        <td><?php if(!empty($candidatedetails->metricplace)) echo $candidatedetails->metricplace;?>
        </td>
        <td><?php if(!empty($candidatedetails->metricspecialisation)) echo $candidatedetails->metricspecialisation;?>
        </td>


        <td class="text-right"><?php if(!empty($candidatedetails->metricpercentage)) echo $candidatedetails->metricpercentage.'%';?> </td>


      </tr>
      <tr style="">
        <td><b><?php  if($candidatedetails->hscstream==2)
        { echo "12th"; }
        else { echo "Diploma";}
        ?></b> </td>
        <td class="text-center"><?php if(!empty($candidatedetails->hscpassingyear)) echo $candidatedetails->hscpassingyear;?>
        </td>
        <td> <?php if(!empty($candidatedetails->hscboarduniversity)) echo $candidatedetails->hscboarduniversity;?></td>

        <td>  <?php if(!empty($candidatedetails->hscschoolcollege)) echo $candidatedetails->hscschoolcollege;?></td>

        <td><?php if(!empty($candidatedetails->hscplace)) echo $candidatedetails->hscplace;?></td>

        <td><?php if(!empty($candidatedetails->hscspecialisation)) echo $candidatedetails->hscspecialisation;?></td>
        <td class="text-right"><?php if(!empty($candidatedetails->hscpercentage)) echo $candidatedetails->hscpercentage.'%';?></td>
      </tr>
      <tr style="">
        <td><b><?php if(!empty($candidatedetails->ugdegree)) echo $candidatedetails->ugdegree; ?></b></td>
        <td class="text-center"><?php if(!empty($candidatedetails->ugpassingyear)) echo $candidatedetails->ugpassingyear; ?>
        </td>
        <td> <?php if(!empty($candidatedetails->ugboarduniversity)) echo $candidatedetails->ugboarduniversity;?></td>
        <td>
          <?php if(!empty($candidatedetails->ugschoolcollege)) echo $candidatedetails->ugschoolcollege;?>

        </td>
        <td><?php if(!empty($candidatedetails->ugplace)) echo $candidatedetails->ugplace;?></td>



        <td>
         <?php if(!empty($candidatedetails->ugspecialisation)) echo  $candidatedetails->ugspecialisation;  ?>
       </td>

       <td class="text-right"><?php if(!empty($candidatedetails->ugpercentage)) echo $candidatedetails->ugpercentage.'%';?>
       </td>

     </tr>
     <?php if (!empty($candidatedetails->pgpassingyear) && $candidatedetails->pgpassingyear !=0) {?>
     <tr style="">
      <td><b><?php if(!empty($candidatedetails->pgdegree)) echo $candidatedetails->pgdegree; ?></b></td>

      <td class="text-center"><?php if(!empty($candidatedetails->pgpassingyear)) echo $candidatedetails->pgpassingyear;?>
      </td>

      <td> <?php if(!empty($candidatedetails->pgboarduniversity)) echo $candidatedetails->pgboarduniversity;?></td>
      <td>  
        <?php if(!empty($candidatedetails->pgschoolcollege)) echo $candidatedetails->pgschoolcollege;?>

      </td>

      <td><?php if(!empty($candidatedetails->pgplace)) echo $candidatedetails->pgplace;?></td>
      <td>

        <?php if(!empty($candidatedetails->pgspecialisation)) echo $candidatedetails->pgspecialisation;  ?></td>




        <td class="text-right"><?php if(!empty($candidatedetails->pgpercentage)) echo $candidatedetails->pgpercentage.'%';?>
        </td>

      </tr>
      <?php } ?>
      <?php if (!empty($candidatedetails->other_degree_specify) && $candidatedetails->other_degree_specify !='') {?>
      <tr style="">
        <td><b><?php if(!empty($candidatedetails->other_degree_specify)) echo $candidatedetails->other_degree_specify; ?></b></td>

        <td class="text-center"> <?php if(!empty($candidatedetails->otherpassingyear)) echo $candidatedetails->otherpassingyear; ?> </td>
        <td>  <?php if(!empty($candidatedetails->otherboarduniversity)) echo $candidatedetails->otherboarduniversity; ?> </td>
        <td> <?php if(!empty($candidatedetails->otherschoolcollege)) echo $candidatedetails->otherschoolcollege; ?>  </td>


        <td><?php if(!empty($candidatedetails->otherplace)) echo $candidatedetails->otherplace; ?></td>

        <td> <?php if(!empty($candidatedetails->otherspecialisation)) echo $candidatedetails->otherspecialisation; ?> </td>

        <td class="text-right"> <?php if(!empty($candidatedetails->otherpercentage)) echo $candidatedetails->otherpercentage .'%'; ?> </td>

      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
</div>

<?php if ($GapYearCount->GYcount !=0) {   ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
       <th colspan="8" class="bg-lightgreen text-white">
        Gap Year
      </th>
    </tr> 
    <tr>
      <th class="text-center col-md-2" style="vertical-align: top; text-align: center;">From Date </th>
      <th class="text-center col-md-2" style="vertical-align: top; text-align: center;">To Date</th>
      <th class="text-left col-md-8" style="vertical-align: top; text-align: center;"> Reason</th>
    </tr> 
  </thead>
  <tbody id="bodytblForm10">
   <?php  $i=0;
   foreach ($gapyeardetals as $key => $val) { $i++; ?>
   <tr id="bodytblForm10" style="text-align: center; background-color: #fff;">
    <td class="text-center col-md-2"><?php if(!empty($val->fromdate)) echo $this->model->changedatedbformate($val->fromdate); ?></td>
    <td  class="text-center col-md-2"><?php if(!empty($val->todate)) echo $this->model->changedatedbformate($val->todate); ?></td>
    <td  class="text-left col-md-8"><?php if(!empty($val->reason)) echo $val->reason;?></td>    
  </tr>
  <?php } ?>   
</tbody>
</table>
</div>
</div>
<?php } ?>

<?php   if ($TrainingExpcount->TEcount !=0) {   ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white">
        Training  Exposure (if any)
      </th>
    </tr> 
    <tr>

     <th class="text-left col-md-4" style="vertical-align: top; text-align: center;">Nature of Training </th>
     <th class="text-left col-md-4" style="vertical-align: top; text-align: center;">Organizing Agency</th>
     <th class="text-center col-md-2" style="vertical-align: top; text-align: center;"> From Date</th>
     <th class="text-center col-md-2" style="vertical-align: top; text-align: center; ">To Date</th>
   </tr> 
 </thead>
 <tbody id="bodytblForm10" style="background-color: #fff;">
   <?php  $i=0;
   foreach ($trainingexposuredetals as $key => $val) { $i++; ?>
   <tr id="bodytblForm10" style="text-align: center; background-color: #fff;">
     <td class="text-left col-md-4"><?php if(!empty($val->natureoftraining)) echo $val->natureoftraining;?></td>
     <td class="text-left col-md-4"><?php if(!empty($val->organizing_agency)) echo $val->organizing_agency;?></td>
     <td class="text-center col-md-2"><?php if(!empty($val->fromdate)) echo $this->model->changedatedbformate($val->fromdate); ?></td>
     <td class="text-center col-md-2"><?php if(!empty($val->todate)) echo $this->model->changedatedbformate($val->todate); ?></td>
   </tr>
   <?php } ?>   
 </tbody>
</table>
</div>
</div>
<?php } ?>
<?php if ($languageproficiency->Lcount !=0) { ?>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm11" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> 
        Language Skill/Proficiency
      </th>
    </tr> 
    <tr>

      <th class="bg-light" style="vertical-align: top; text-align: center;">Language</th>
      <th class="bg-light" style="vertical-align: top; text-align: center;">Speak</th>
      <th class="bg-light" style="vertical-align: top; text-align: center;">Read</th>
      <th class="bg-light" style="vertical-align: top; text-align: center;">Write </th>
    </tr> 
  </thead>
  <tbody id="bodytblForm11">
    <input type="hidden" name="languagecount" id="languagecount" value="<?php echo $languageproficiency->Lcount;?>">
    <?php $i= 0;

    foreach ($languagedetalsprint as $key => $val) {
      $i++; ?>
      <tr id="bodytblForm11" style="text-align: center;background-color: #fff; ">
        <td>  <?php
        if(!empty($val->lang_name)) echo $val->lang_name;?> </td>
        <td>
         <?php if($val->lang_speak=='H')
         { echo "High";
       }elseif ($val->lang_speak=='L') {
        echo "Low";
      }else{
       echo "Moderate";
     }  ?></td>
     <td><?php if($val->lang_read=='H')
     { echo "High";
   }elseif ($val->lang_read=='L') {
    echo "Low";
  }else{
   echo "Moderate";
 }  
 ?></td>
 <td><?php if($val->lang_write=='H')
 { echo "High";
}elseif ($val->lang_write=='L') {
  echo "Low";
}else{
 echo "Moderate";
}  
?></td>
</tr>
<?php }   ?>
</tbody>
</table>
</div>
</div>
<?php  } ?>
<?php if (!empty($otherinformationdetails->any_subject_of_interest) && $otherinformationdetails->any_subject_of_interest !='') { ?>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
       <th colspan="8" class="bg-lightgreen text-white"> Subject(s) of Interest </th>
     </tr> 
   </thead>
   <tbody>
    <tr  style="background-color: #fff;">
      <td><?php if(!empty($otherinformationdetails->any_subject_of_interest)) echo $otherinformationdetails->any_subject_of_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php }  ?> 
<?php if(!empty($otherinformationdetails->any_subject_of_interest) && $otherinformationdetails->any_achievementa_awards !='') { ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> Achievements /Awards (if any) </th>
    </tr> 

  </thead>
  <tbody>
    <tr  style="background-color: #fff;">
      <td><?php if(!empty($otherinformationdetails->any_achievementa_awards)) echo $otherinformationdetails->any_achievementa_awards; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>

<?php } ?>

<?php if ($WorkExperience->WEcount != 0) {?>
<div class="row">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">

   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="6" class="bg-lightgreen text-white"> 
       Work Experience (if any) 
     </th>
   </tr> 
   <tr>
    <th style="text-align: center;">Organization Name</th>
    <th style="text-align: center;">Designation </th>
    <th style="text-align: center;">Description of Assignment</th>
    <th style="text-align: center;">Duration</th>
    <th style="text-align: center;">Palce of Posting</th>
    <th style="text-align: center;">Last salary drawn (monthly)</th>
  </tr> 
  <tr>
    <th colspan="3"></th>
    <th colspan="1" style="text-align: center;">
      <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">From Date</div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">To Date </div> 
      </div></th>
      <th colspan="3"></th>
    </tr>
  </thead>
  <tbody id="bodytblForm12">
    <?php   
    $i=0;
    foreach ($workexperiencedetails as $key => $val) {   ?>
    <tr id="bodytblForm12" style="text-align: center; background-color: #fff;"> 
      <td><?php if(!empty($val->organizationname)) echo $val->organizationname;  ?></td>
      <td><?php if(!empty($val->designation)) echo $val->designation;?></td>
      <td><?php if(!empty($val->descriptionofassignment)) echo $val->descriptionofassignment;?></td>
      <td>
       <div class="col-lg-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
          <?php if(!empty($val->fromdate)) echo $this->model->changedatedbformate($val->fromdate);?>
        </div> 
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
         <?php if(!empty($val->todate)) echo $this->model->changedatedbformate($val->todate);?>
       </div> 
     </div>
   </td>
   <td><?php if(!empty($val->palceofposting)) echo $val->palceofposting;?></td>
   <td><?php if(!empty($val->lastsalarydrawn)) echo $val->lastsalarydrawn;?></td>


 </tr>

 <?php $i++; } ?>
</tbody>
</table>
</div>
</div>
<?php } ?>
<?php if (!empty($otherinformationdetails->any_assignment_of_special_interest) &&  $otherinformationdetails->any_assignment_of_special_interest !='') {?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
   <table id="tblForm09" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> Describe any assignment(s) of special interest undertaken by you</th>
    </tr> 

  </thead>
  <tbody>
    <tr  style="background-color: #fff;">
      <td><?php if(!empty($otherinformationdetails->any_assignment_of_special_interest)) echo $otherinformationdetails->any_assignment_of_special_interest; ?></td>
    </tr>
  </tbody>
</table>
</div>
</div>
<?php } ?>

<?php if (!empty($otherinformationdetails->experience_of_group_social_activities) !='') { ?>
<div class="row"> 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;"> 
    <table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white"> Experience of Group and/or Social Activities </th>
      </tr> 
    </thead>
    <tbody>
      <tr  style="background-color: #fff;">
        <td><?php if(!empty($otherinformationdetails->experience_of_group_social_activities)) echo $otherinformationdetails->experience_of_group_social_activities; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<?php } ?>

<?php if (!empty($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before)) { 
  ?> 
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
     <table id="tblForm09" class="table table-bordered table-striped">
      <thead>
       <tr>
        <th colspan="8" class="bg-lightgreen text-white"> <b>Have you taken part in PRADAN's selection process before?</b>
        </td>
      </tr> 
    </thead>
    <tbody>
      <tr  style="background-color: #fff;">
        <td>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 

            <?php 
          // print_r($otherinformationdetails);
          // die;
            if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='yes') {
              echo 'Yes';
            }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before=='no'){
              echo 'No';
            }
            ?>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <?php if(!empty($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when)) echo $this->model->changedatedbformate($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_when); ?>

          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
            <?php if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==1) {
             echo 'ON Campus';
           }else if ($otherinformationdetails->have_you_taken_part_in_pradan_selection_process_before_where==2) {
            echo 'Off Campus';
          } ?>

        </div>
        
      </td>
    </tr>
  </tbody>
</table>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function() {

    $("input[name$='pradan_selection_process_before']").load(function() {
      var selected = $(this).val();

      if (selected =='yes') {
        $("#when").removeAttr('disabled','false'); 
        $("#where").removeAttr('disabled','false');
      }else{
        $("#when").prop('disabled','true'); 
        $("#where").prop('disabled','true');
      }

    });

    $("input[name$='pradan_selection_process_before']").click(function() {
      var selected = $(this).val();

      if (selected =='yes') {
        $("#when").removeAttr('disabled','false'); 
        $("#where").removeAttr('disabled','false');
      }else{
        $("#when").prop('disabled','true'); 
        $("#where").prop('disabled','true');
      }

    });

  });
</script>
<?php } ?>

<?php if (!empty($otherinformationdetails->know_about_pradan)) {  ?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;">
   <table id="tbltrainingexposure" class="table table-bordered table-striped">
    <thead>
     <tr>
      <th colspan="8" class="bg-lightgreen text-white"> <b>From where have you come to know about PRADAN?</b>
      </td>
    </tr> 
  </thead>
  <tbody>
    <tr  style="background-color: #fff;">
      <td>

        <?php 
        if ($otherinformationdetails->know_about_pradan=='CampusPlacementCell') {
         echo 'Campus Placement Cell';
       } else if ($otherinformationdetails->know_about_pradan=='UniversityProfessors') {
        echo 'University Professors';
      }else if ($otherinformationdetails->know_about_pradan=='CampusAlumni') {
        echo 'Campus Alumni';
      }else if ($otherinformationdetails->know_about_pradan=='friends') {
        echo 'Friends';
      }else if ($otherinformationdetails->know_about_pradan=='other1') {
        if(!empty($otherinformationdetails->know_about_pradan_other_specify)) echo $otherinformationdetails->know_about_pradan_other_specify;
      }
      ?> 

    </td>
  </tr>
</tbody>
</table>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("input[name$='have_you_come_to_know']").click(function() {
      var test = $(this).val();
      if (test =='Other') {
        $("#other_have_you_come_to_know").show();
        $("#specify").prop('required','required');
        $('#specify').removeAttr("disabled");

      }else{
        $("#other_have_you_come_to_know").hide();
        $("#specify").prop('disabled','true');

      }

    });

  });
</script>
<?php } ?>


<div class="panel-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" >
  <a href="<?php echo site_url().'Hrscore'; ?>" class="btn btn-round btn-dark btn-sm" >Close</a>

  
</div>

</div>
<!-- End  Candidates Experience Info -->
</div>
</div>
</div>

