<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - EXPERIENCE CERTIFICATE FOR (Level 1 to 3)
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
        <div class="col-md-12 text-center">
          <h5>SAMPLE EXPERIENCE CERTIFICATE FOR (Level 1 to 3)  </h5> 
          <i><h6>(In case of termination)</h6></i>
        </div>
        <div class="col-md-12 text-center">
          <h4>TO WHOM IT MAY CONCERN</h4> 
        </div>          
      </div>
      

      <div class="row">
        <div class="col-md-6 text-left" style="margin-bottom: 20px;">
         301-2117/PDR/________________ 
         
       </div>
       <div class="col-md-6 text-right" style="margin-bottom: 20px;">
         May 9, 2018
       </div>
    </div> 
   
   
    <div class="row">
      <div class="col-md-12 text-left text-left" style="margin-top: 30px;">
    This is to certify that _______________________________ worked with Professional Assistance for Development Action (PRADAN) as Assistant from ___________________ to ___________________ in _____________________________. 
     </div>
   </div>
   <div class="row" style="line-height: 2 ;">
    <div class="col-md-12" style="margin-top: 20px;">
   PRADAN (www.pradan.net) is a voluntary organization registered under the Societies Registration Act (1860), engaged in rural development. Developing scalable models to build self-reliance enhance rural livelihoods, building peoples’ institutions and mainstreaming these for large scale impact is at the core of PRADAN’s work. Founded in 1983, it presently has _____ staff, including _______ professionals. It works in seven of the poorest States in India.
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
   __________________________ major responsibility included _________________________________________. 
    </div>
      
 </div>
 <hr/>
 <div class="row ">
   <div class="col-md-12 text-right" style="margin-top: 20px;">
   (___________________________)
   </div>
   <div class="col-md-12 text-right" style="margin-top: 20px;">
  Executive Director
   </div>

 </div>

</div>
<div class="panel-footer text-right">

  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>