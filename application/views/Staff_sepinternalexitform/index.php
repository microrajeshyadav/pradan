<section class="content" style="background-color: #FFFFFF;" >

  <br>
  <div class="container-fluid">
     <?php $page='employee'; require_once(APPPATH.'views/staff_septab/index.php');?>
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-12 panel-title pull-left">STAFF SEPARATION - THE EXIT INTERVIEW FORM
         </h4>
       </div>
       <hr class="colorgraph"><br>
     </div>
     <div class="panel-body">
      <div class="row">
       <div class="col-md-12 text-center">
        <p><h5>Professional Assistance for Development Action (PRADAN)</h5></p> 
      </div>
      <div class="col-md-12 text-center" style="margin-bottom: 20px;">
         <h6><i>Inter-Office Memo</i></h6>
      </div> 
    </div>
    <div class="row" style="line-height: 2">
      <div class="col-md-6">
        To: 
      </div>
      <div class="col-md-6 text-right">
        Date: _______________________________
      </div>     
      <div class="col-md-6">
        ______________________________
      </div>
      <div class="col-md-6 text-right">
        Employee Code: __________________________________
      </div> 
      <div class="col-md-12 text-left" style="font-size: 12px;">
       <i> (Name of the Separating Employee)</i>
     </div> 
     <div class="col-md-6">
      From: 
    </div>
    <div class="col-md-6 text-right">

    </div>   
    <div class="col-md-6">
      ______________________________
    </div>
    <div class="col-md-6 text-right">
      File: Personal Dossier of Employee
    </div> 
    <div class="col-md-12 text-left" style="font-size: 12px;">
     <i> (Name of the Person conducting Exit Interview)</i>
   </div> 
 </div>
 <div class="row" style="line-height: 2 ; margin-top: 50px;">
  <div class="col-md-12">
    Subject: <strong>The Exit Interview Form</strong>
  </div>
  <div class="col-md-12" style="margin-top: 20px;">
   As you plan to leave PRADAN soon, I write to request you to fill in the enclosed ‘Exit Interview Form’. We hope, through the information you would provide us in this format, to seek feedback about issues related to the organization and inputs for possible measures to change any aspect of the organization's functioning.
 </div>     
 <div class="col-md-12" style="margin-top: 20px;">
  I would like to reassure you that we would treat the contents of this form as confidential, if you so desire, and use it only for organizational learning.
</div> 
<div class="col-md-12" style="margin-top: 20px;">

  We do hope that you make a smooth transition to your new assignment, and sincerely wish you the very best in your life.

</div>
<div class="col-md-12" style="margin-top: 20px;">
  With regards.
</div>
</div>

<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
    ( _________________________________ )
  </div>
  
</div>
<div class="row text-left" style="margin-top: 20px; ">
  <div class="col-md-12">
   Encl: As above
 </div>
</div>
</div>
<div class="panel-footer text-right">
  <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
  <a href="" class="btn btn-dark btn-sm"> Go Back</a>
</div>
</div>
</div>
</section>