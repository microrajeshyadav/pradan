
<?php foreach ($role_permission as $row) { if ($row->Controller == "Policy" && $row->Action == "edit"){ ?>
<div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>

      <div class="container-fluid" style="margin-top: 20px;">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

         <div class="row clearfix doctoradvice">
          <form method="POST" action="" enctype="multipart/form-data">          
           <div class="panel thumbnail shadow-depth-2 listcontainer" >
            <div class="panel-heading">
              <div class="row">
               <h4 class="col-md-8 panel-title pull-left">Edit Policy</h4>
               <div class="col-md-4 text-right" style="color: red">
                * Denotes Required Field 
              </div>
            </div>
            <hr class="colorgraph"><br>
          </div>
          <div class="panel-body">  
            <div class="row">

              <div class="col-md-4">
               <div class="form-group">
                <div class="form-line">
                  <label for="StateNameEnglish" class="field-wrapper required-field">Policy Name <span style="color: red;" >*</span></label>
                  <input type="text" class="form-control " name="name_policy" id="name_policy" placeholder="Please Enter Policy Name " value="<?php echo $edit_policy->policy_name;   ?>" required  >
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
               <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">File Upload <span style="color: red;" >*</span></label>
                <input type="file" class="form-control " name="policy_upload" id="policy_upload"  accept="application/pdf">
              </div></div></div>
              <?php if (!empty($edit_policy->docpath)) { ?>
              <div class="col-md-4">
                <div class="form-group">
                 <input type="hidden" name="edit_policypath" id="edit_policypath" value="<?php echo $edit_policy->docpath; ?>">
                 <div class="form-line" style="padding-top: 23%;">
                   <?php  $file_path=''; 
                   $file_path=FCPATH.'datafiles/policy/'.$edit_policy->docpath;
                   
                   if(file_exists($file_path)) { ?> 
                   <a  href="<?php echo site_url().'datafiles/policy/'.$edit_policy->docpath;?>" width="140"  download >  </a><i class="fa fa-download fa-1x" aria-hidden="true"></i> 

                   <?php } else { ?> 
                   <i title ="File either removed physically or renamed." class="fa fa-window-close fa-1x red" aria-hidden="true"></i> 
                   <?php } ?> 



                 </div>
               </div>
               <?php } ?>
             </div>
           </div>
           <div class="panel-footer text-right">
            <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to Edit your changes? Click on me.">Save</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>



<?php } } ?>
<script type="text/javascript">

  $('.txtNumeric').keypress(function(event) {
   var $this = $(this);
   if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
    ((event.which < 48 || event.which > 57) &&
      (event.which != 0 && event.which != 8))) {
    event.preventDefault();
}

var text = $(this).val();
if ((event.which == 46) && (text.indexOf('.') == -1)) {
 setTimeout(function() {
   if ($this.val().substring($this.val().indexOf('.')).length > 3) {
     $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
   }
 }, 1);
}

if ((text.indexOf('.') != -1) &&
 (text.substring(text.indexOf('.')).length > 2) &&
 (event.which != 0 && event.which != 8) &&
 ($(this)[0].selectionStart >= text.length - 2)) {
 event.preventDefault();
}      
});


  $('.inputelement').keyup(function () {
    if (!this.value.match(/[0-9]/)) {
      this.value = this.value.replace(/[^0-9]/g, '');
    }
  });
</script>