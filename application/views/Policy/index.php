<?php foreach ($role_permission as $row) { if ($row->Controller == "Policy" && $row->Action == "index"){ ?>
<br>
<div class="container-fluid">
  <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">HR Policy Documents</h4>
       <div class="col-md-2 text-right">
        <div class="container">
          <!-- Trigger the modal with a button -->
          <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Add Policy Document</button>

          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">

                  <h4 class="modal-title">Add New Policy</h4>
                </div>
                <div class="modal-body text-left">


                 <form name="basicinfo" id="basicinfo" method="POST" action="" enctype="multipart/form-data">
                  <div class="form-group">
                    <div class="form-line">
                     <label>Policy Name <span style="color:red;"> *</span></label>
                     <input type="text" class="form-control" name="name_policy" id="name_policy" placeholder="Enter policy document name here." value="<?php echo set_value('dateofjoining');   ?>" required  >
                   </div>  
                 </div>            

                 <div class="form-group">
                   <div class="form-line">
                     <label>Upload Policy <span style="color:red;"> *</span></label>
                     <div class="custom-file">
                       <input type="file" class="custom-file-input"  name="policy_upload" id="policy_upload"  accept="application/pdf" required >
                       <label class="custom-file-label" for="customFile"></label>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="modal-footer">
                <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Save</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </form>
      </div>

    </div>

  </div>
</div>
<hr class="colorgraph"><br>
</div>
<div class="panel-body">
  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


      <div class="container-fluid">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <table id="tablecampus" class="table table-bordered dt-responsive table-striped">
            <thead>
             <tr>
              <th style ="max-width:50px;" class="text-center">S. No.</th>
              <th >Policy Document</th>
              <th class="text-center">Download</th>
              <th style ="max-width:50px;" class="text-center">Action</th>
            </tr> 
          </thead>
          <tbody>
            <?php 

            $i=0;
            foreach ($policylist as $key => $value) { ?>
            <tr>
              <td class="text-center"><?php echo $i+1;?></td>
              <td><?php echo $value->policy_name;?></td>
              <td class="text-center">
                <?php  $file_path=''; 
                $file_path=FCPATH.'datafiles/policy/'.$value->docpath;
                
                if(file_exists($file_path)) { ?>
                <a  href="<?php echo site_url().'datafiles/policy/'.$value->docpath;?>" width="140" download><i class="fa fa-download fa-1x" aria-hidden="true"></i> 
                </td>
                <?php } else { ?> 
                <i title ="File either removed physically or renamed." class="fa fa-window-close fa-1x red" aria-hidden="true"></i> 
                <?php } ?> 
                  <!--   <td><?php
                   // if ($value->status==0) {
                   //   echo "Active"; 
                  //  }else{
                   //   echo "InActive"; 
                   // }

                  ?></td> -->
                  <td class="panel-footer text-center">

                    <a data-toggle="tooltip" data-placement="bottom" title = "Click here to edit this Batch." href="<?php echo site_url()."Policy/edit/".$value->policy_id;?>" ><i class="fa fa-edit" aria-hidden="true" id="usedbatchid"></i></a> | <a data-toggle="tooltip" data-placement="bottom" title = "Click here to delete this Batch." href="<?php echo site_url()."Policy/delete/".$value->policy_id;?>" onclick="return confirm_delete()"><i class="fa fa-trash" style="color:red"></i>
                    </a>





                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div> 
      </div>
    </div>
  </div>   
  <?php } } ?>
  <script >
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });
   function confirm_delete() {
    var r = confirm("Do you want to delete selected policy?");
    if (r == true) {
      return true;
    } else {
      return false;
    }
  }


</script>
