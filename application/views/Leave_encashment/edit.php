<style type="text/css">
  /* input[type=text] {
    background: transparent;
    border: none;
    border-bottom: 1px solid #000000;
    }*/
    textarea {
      resize: none;
    }
    hr{
      border-top: 1px solid black;
    }
  </style>
  <section class="content" style="background-color: #FFFFFF;">
    <br/>
    <?php $page='Joining'; require_once(APPPATH.'views/Employee_particular_form/topbar.php');?>
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>   
    <div class="col-md-12">
      <div class="hpanel">
        <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('tr_msg');?>. </div>
        </div>
      </div>       
      <?php } else if(!empty($er_msg)){?>    
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('er_msg');?>. </div>
          </div>
        </div>          
        <?php } ?>
        <div class="container-fluid" style="font-family: 'Oxygen' !important;">
          <div class="row text-center" style="padding: 14px;">      
            <div class="panel thumbnail shadow-depth-2 listcontainer">
              <div class="panel-heading">
                <div class="row">
                  <h4 class="col-md-12 panel-title pull-left">JOINING REPORT ON APPOINTMENT 
                  </h4>
                </div>
                <hr class="colorgraph"><br>
              </div>
              <div class="panel-body">          
                <div class="row">
                  <form method="POST" action="" enctype="multipart/form-data">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
                      <h6>Professional Assistance for Development Action (PRADAN) </h6>
                      <h5>JOINING REPORT ON APPOINTMENT</h5> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="ExecutiveDirector" class="field-wrapper required-field">To:<br>The Executive Director,<br>
                        PRADAN.</label>
                        <br>

                      </div>
                    </div> 

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-right">
                      <label for="Name">Address:&nbsp;</label>&nbsp;

                      <textarea  name="address" id="address"  placeholder=" Enter Address" class="form-control inputborderbelow"  required="required"  ><?php  echo $candidatedetails->permanenthno.$candidatedetails->permanentstreet. $candidatedetails->permanentcity.$candidatedetails->permanentpincode;?></textarea> </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">       
                      Dear Sir,               
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">
                      <div class="form-group">
                        Subject: <b>Joining Report</b>
                      </div>
                    </div>
                   
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">

                        Please refer to your offer of appointment no&nbsp;
                        <b><?php echo $fetchresult->offerno; ?></b>&nbsp;dated&nbsp;
                        <b><?php echo $fetchresult->doj; ?></b>&nbsp; offering me appointment as&nbsp;
                        <b><?php echo $fetchresult->desname; ?></b>&nbsp;at&nbsp;
                        <b><?php echo $fetchresult->officename; ?></b>&nbsp;
                        I hereby report for duty
                        <br>
                        <br> 
                        in the forenoon of today, the 
                        &nbsp; <input type="text" class="inputborderbelow datepicker" name="dutydate" id="dutydate" value="<?php $this->General_nomination_and_authorisation_form_model->changedate($val->duty_date);?>" required="required" />
                        (date/month/year). I shall inform you of any change in my address, given above, when it occurs.
                        <br>
                        <br>
                        Yours faithfully,
                        <br>
                        <br>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
                       <img src="<?php //echo site_url().'datafiles/signature/'.$val->countersigned_signature;?>"  style="height:50px;width:150px;" >

                       <input type="hidden" name="foo" value="<?php echo $val->signature;?>"/>
                       <p class="text-left">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b class="inputborderbelow">
                          <?php echo $value->candidatefirstname; ?>
                        </b>
                      </p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
                      <div class="input-group">                  
                        
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
                      <input type="text" name="declarationplace" required="" maxlength="20" minlength="2" class="form-control inputborderbelow" placeholder="Enter Place" value="<?php echo $val->declaration_place; ?>">                      
                      <input type="text" name="declarationdate" class="form-control inputborderbelow datepicker" required=""  placeholder="Select Date" value="<?php 
                      $this->General_nomination_and_authorisation_form_model->changedate($val->declaration_date);
                      ?>">                        
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" >
                      <div class="panel-footer">
                       <button  type="submit" name="operation"  value="0" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                       <button  type="submit" name="operation"  value="1"  class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save & Submit</button>
                     </div>
                   </div>                 
                   
                 </form>
               </div>
               <?php 

               if($this->loginData->RoleID==2 || $this->loginData->RoleID==17)
            {?> 
            
               <div class="row">
                 <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
                   <p class="text-center"><b>Countersigned:</b> (by Employee Responsible for Induction)</p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
                   <img src="<?php //echo site_url().'datafiles/signature/'.$val->countersigned_signature;?>"  style="height:50px;width:150px;" >

                   <input type="hidden" name="foo" value="<?php echo $val->signature;?>"/>
                   <p class="text-left">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="inputborderbelow"><?php echo $val->countersigned_date; ?></b></p>
                 </div>
                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
                  
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right" style="margin-bottom: 50px;">
                  <input type="text" name="Countersignedplace" required="" maxlength="20" minlength="2" class="form-control inputborderbelow" placeholder="Enter Place" value="<?php echo $val->countersigned_place; ?>">  
                  <input type="text" class="form-control inputborderbelow datepicker" name="Countersigneddate" required=""  placeholder="Select Date" value="<?php echo $val->countersigned_date; ?>">
                </div>
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <p style="float: left;margin-left: 5px;">cc: - Supervisor</p>
               </div>
               <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <p style="float: left;"> &emsp;&emsp;- Finance-Personnel-MIS Unit</p>
               </div>
               <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <p style="float: left;"> &emsp;&emsp;- Personal Dossier (Location)</p>
               </div>
             </div>
           </div>
           <hr>
           <?php } 


               if($this->loginData->RoleID== 17)
            { ?>
          
           <div  class="row">
              <form class="col-lg-12 col-md-12" method="POST" action="" enctype="multipart/form-data">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;"> <b>(For use in the Finance-Personnel-MIS Unit)</b></div>
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                  
                  Ms./Mr. <span class="inputborderbelow"><b><?php echo $roww->candidatefirstname; ?></b>&nbsp;<b><?php echo $roww->candidatelastname; ?></b></span> has joined at                    
                  <input type="text" name="newoffice" required="" maxlength="30" minlength="2" required="" class="inputborderbelow" placeholder="Enter Joined Office Name">                 

                  noted in Probation register.
                </div>
                
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" readonly="" name="NotedinProbationregister" id="NotedinProbationregister" >
                    <label class="form-check-label" for="NotedinProbationregister">(Please<span class="glyphicon glyphicon-ok"></span>)</label>
                  </div>
                </div>
                <br><br>
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                  <div  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                    <input  class= "form-control inputborderbelow" type="text" name="FinancePersonnelMISUnit">
                  </div>
                </div>
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                  (Finance-Personnel-MIS Unit)
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right" style="float;left; width: 100%;">
                  <button  type="submit" disabled  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Save</button>
                </div>
              </form>
              
            </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</section>


 <script>
  $(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
   $(".datepicker").datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:2030',
    dateFormat
     : 'dd/mm/yy',
        //defaultDate: new Date(2018, 00, 01)
      });

 });
</script>
