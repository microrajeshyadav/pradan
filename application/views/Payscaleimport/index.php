<?php foreach ($role_permission as $row) { if ($row->Controller == "Payscaleimport" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <form name="" id="" action="" method="post"> 
          <div class="row">
           <h4 class="col-md-6 panel-title pull-left">Manage - Pay Scale  </h4>

           <div class="col-md-6 text-right">
            <a data-toggle="tooltip" data-placement="bottom" title="Click here to import selected financial year pay scale data." href="<?php echo site_url()."Payscaleimport/add";?>" class="btn btn-primary btn-sm">Add Pay Scale </a>
          </div>
        </div>

        <div class="row bg-light" style="padding: 20px; margin-bottom: 20px;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px;">
            <h5>Filters:</h5>
          </div>     
          <div class="form-inline mr-2">
            <label class="mr-2">Level:</label>
            <select name="levelname" id ="levelname" class="form-control" required>
              <option value="1"> 1 </option>
              <option value="2"> 2 </option>
              <option value="3"> 3 </option>
              <option value="4"> 4 </option>
              <option value="5"> 5 </option>
              <option value="6"> 6 </option>
            </select>
          </div>
          <div class="form-inline mr-2 ">
            <label class="mr-2">Financial Year:</label>   
            <select id="financialyear" name ="financialyear" class="form-control" required>
              <option value="<?php echo $fyear; ?>"> <?php echo $fyear; ?></option>
              <?php 
              $fyearexp = '';
              $prevyear = '';
              $nextyear = '';
              $nextfyear = '';
              $fyearexp  = explode('-', $fyear); 
              $prevyear = $fyearexp[0]+1;
              $nextyear = $fyearexp[1]+1;
              $nextfyear = $prevyear.'-'.$nextyear;
              ?>
              <option value="<?php echo $nextfyear; ?>"> <?php echo $nextfyear; ?></option>
            </select>
          </div>
          <div class="form-inline mr-2">
            <button title ="Click on me, to see the Staff list based on selected office." data-toggle='tooltip' type="submit" value="show" class="btn btn-info btn-sm" name="btnShow">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </div> 
        </div>
    </form>
  </div>
    <hr class="colorgraph"><br>
  
  <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <div class="container-fluid">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table id="tablepayscale" class="table table-centered table-striped dt-responsive">
              <thead>
               <tr>
                <th style ="max-width:50px;" class="text-center">S. No.</th> 
                <th class="text-center"> Financial Year </th>                   
                <th class="text-center">Level</th>
                <th class="text-center">No of year </th>
                <th class="text-center">Last year Basic </th> 
                <th class="text-center">Increment</th> 
                <th class="text-center">Slide</th> 
                <th class="text-center">Current year Basic </th> 
              </tr> 
            </thead>
            <tbody>
              <?php 
              $i=0;
              foreach ($currentyeardata as $key => $value) { ?>
                <tr>
                  <td class="text-center"><?php echo $i+1;?></td>
                  <td class="text-center"><?php echo $value->fyear;?></td>
                  <td class="text-center"><?php echo $value->level;?></td>
                  <td class="text-center"><?php echo $value->noofyear;?></td>
                  <td class="text-right"><label id="basicsalary_<?php echo $i;?>"></label></td>
                  <td class="text-right"><label id="increment_<?php echo $i;?>"><?php echo $value->increment;?></td>
                  <td class="text-right"><label id="slide_<?php echo $i;?>"><?php echo $value->slide;?></td>
                  <td class="text-right"><label id="netsalary_<?php echo $i;?>"></label></td>
                </tr>
                <script>
                  var cellid= "<?php echo $i; ?>"; var cid="#basicsalary_"+cellid;
                  $(cid).text(intToNumberBudget("<?php echo $value->basicsalary; ?>"));
       
                  var cell2id= "<?php echo $i; ?>"; var cid2='#increment_'+cell2id;
                  $(cid2).text(intToNumberBudget("<?php echo $value->increment; ?>"));

                  var cell3id= "<?php echo $i; ?>"; var cid3="#slide_"+cell3id;
                  $(cid3).text(intToNumberBudget("<?php echo $value->slide; ?>"));
       
                  var cell4id= "<?php echo $i; ?>"; var cid4='#netsalary_'+cell4id;
                  $(cid4).text(intToNumberBudget("<?php echo $value->netsalary; ?>"));

                </script>
                <?php $i++; } ?>
              </tbody>
            </table>
          </div>
        </div> 
      </div>
    </div>
  </div>   
<?php } } ?>


<script type="text/javascript">
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablepayscale').DataTable();
  });
</script>
