  <?php foreach ($role_permission as $row) { if ($row->Controller == "Payscaleimport" && $row->Action == "add"){ ?>
    <div class="container-fluid">
      <!-- Exportable Table -->

      <?php 
      $tr_msg= $this->session->flashdata('tr_msg');
      $er_msg= $this->session->flashdata('er_msg');

      if(!empty($tr_msg)){ ?>
        <div class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('tr_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else if(!empty($er_msg)){?>
          <div class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('er_msg'); ?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="container-fluid" style="margin-top: 20px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row">

                <div class="panel thumbnail shadow-depth-2 listcontainer col-md-12">
                  <form method="POST" action="" id="frmCSVImport" name="frmCSVImport" enctype="multipart/form-data">
                    <div class="panel-heading">
                      <div class="row">
                       <h4 class="col-md-8 panel-title pull-left">Manage - Pay Scale </h4>
                       <div class="col-md-4 text-right" style="color: red">
                        * Denotes Required Field 
                      </div>
                    </div>
                    <hr class="colorgraph"><br>
                  </div>
                  <div class="panel-body">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-light" style="padding: 10px;">
                    <div class="col-md-1 text-right"><label>Financial Year : </label></div>
                    <div class="col-md-2">
                      <select id="financialyear" name ="financialyear" class="form-control" required>
                       <option value="<?php echo $fyear; ?>"> <?php echo $fyear; ?></option>
                       <?php 
                       $fyearexp = '';
                       $prevyear = '';
                       $nextyear = '';
                       $nextfyear = '';
                       $fyearexp  = explode('-', $fyear); 
                       $prevyear = $fyearexp[0]+1;
                       $nextyear = $fyearexp[1]+1;
                       $nextfyear = $prevyear.'-'.$nextyear;
                       ?>
                       <option value="<?php echo $nextfyear; ?>"> <?php echo $nextfyear; ?></option>
                     </select>
                   </div>
                   <div class="col-md-1 text-right"><label>Level : </label></div>
                   <div class="col-md-2">  
                    <select name="levelname" id ="levelname" class="form-control" required="">
                      <option value="0"> Select </option>
                      <option value="1"> 1 </option>
                      <option value="2"> 2 </option>
                      <option value="3"> 3 </option>
                      <option value="4"> 4 </option>
                      <option value="5"> 5 </option>
                      <option value="6"> 6 </option>
                    </select>
                  </div>
                  <div class="col-md-1">
                   <div class="form-check">
                    <input type="checkbox" name="copyto" id="copyto" class="form-check-input" >
                    <label class="form-check-label" for="materialUnchecked">Copy To </label>
                  </div>
                </div>
                <div class="col-md-1 text-right">  
                  <label > Level : &nbsp;</label>
                </div>
                <div class="col-md-2">  
                  <select name="Copylevelname" id ="Copylevelname" class="form-control" required="">
                    <option value="0"> Select </option>
                    <option value="1"> 1 </option>
                    <option value="2"> 2 </option>
                    <option value="3"> 3 </option>
                    <option value="4"> 4 </option>
                    <option value="5"> 5 </option>
                    <option value="6"> 6 </option>
                  </select>
                </div>

                <div class="col-md-2">
                 <div class="form-check">
                  <input type="checkbox" name="removeSlide" id="removeSlide" class="form-check-input" >
                  <label class="form-check-label" for="materialUnchecked">Remove Slide </label>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;" >
             <table class="table">
              <thead>
                <tr>
                  <th>Phase</th>
                  <th class="text-center">Basic</th>
                  <th class="text-center">Increment</th>
                  <th class="text-center">No of Years</th>
                </tr> 
              </thead>  
              <tbody id="txtpayscal">
                <tr>
                  <td>Start <span style="color:red;"> * </span> </td>
                  <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;" class="form-control isNumberKey text-right" max="100000"  id="Startphasebasic" name="phasebasic[]" required="required" value="" /> </td>
                  <td><input type="number" min="100"onKeyPress="if(this.value.length==4) return false;" max="9000" class="isNumberKey form-control text-right" id="Startphaseinc" name="phaseinc[]" required="required" value="" /> </td>
                  <td><input type="number" min="0"onKeyPress="if(this.value.length==2) return false;" max="50" class="isNumberKey form-control text-right" id="Startnoofyear" name="noofyear[]" value="" required="required" /></td>
                </tr>

                <tr>
                  <td>First Phase <span style="color:red;"> * </span> </td>
                  <td><input type="number" min="5000"onKeyPress="if(this.value.length==6) return false;" max="100000" class="isNumberKey form-control text-right" id="firstphasebasic" name="phasebasic[]" required="required" value="" /> </td>
                  <td><input type="number" min="100"onKeyPress="if(this.value.length==4) return false;"  max="9000" class="isNumberKey form-control text-right" id="firstphaseinc" name="phaseinc[]" required="required"  value="" /> </td>
                  <td><input type="number" min="0"onKeyPress="if(this.value.length==2) return false;" max="50" class="isNumberKey form-control text-right" id="firstnoofyear" name="noofyear[]" required="required" value="" /></td>
                </tr> 
                <tr>
                 <td>Second Phase <span style="color:red;"> * </span> </td>
                 <td><input type="number" min="5000"onKeyPress="if(this.value.length==6) return false;" max="100000" class="isNumberKey form-control text-right" id="secondphasebasic" name="phasebasic[]" required="required" value="" /> </td>
                 <td><input type="number" min="100"onKeyPress="if(this.value.length==4) return false;" max="9000" class="isNumberKey form-control text-right" id="secondphaseinc" name="phaseinc[]" required="required" value="" /> </td>
                 <td><input type="number" min="0"onKeyPress="if(this.value.length==2) return false;" max="50" class="isNumberKey form-control text-right" id="secondnoofyear" name="noofyear[]" required="required" value="" /></td>
               </tr> 
               <tr>
                <td>Third Phase <span style="color:red;"> * </span> </td>
                <td><input type="number" min="5000"onKeyPress="if(this.value.length==6) return false;" max="100000" class="isNumberKey form-control text-right" id="thirdphasebasic" name="phasebasic[]" required="required" value="" /> </td>
                <td><input type="number" min="100"onKeyPress="if(this.value.length==4) return false;" max="9000" class="isNumberKey form-control text-right" id="thirdphaseinc" name="phaseinc[]" required="required"  value="" /> </td>
                <td><input type="number" min="0"onKeyPress="if(this.value.length==2) return false;" max="50" class="isNumberKey form-control text-right" id="thirdnoofyear" name="noofyear[]" required="required" value="" /></td>
              </tr> 
              <tr>
                <td>Fourth Phase <span style="color:red;"> * </span> </td>
                <td><input type="number" min="5000"onKeyPress="if(this.value.length==6) return false;" max="100000" class="isNumberKey form-control text-right" id="fourthphasebasic" name="phasebasic[]" required="required" value="" /> </td>
                <td><input type="number" min="100"onKeyPress="if(this.value.length==4) return false;" max="9000" class="isNumberKey form-control text-right" id="fourthphaseinc" name="phaseinc[]" required="required" value="" /> </td>
                <td><input type="number" min="0"onKeyPress="if(this.value.length==2) return false;" max="50" class="isNumberKey form-control text-right" id="fourthnoofyear" name="noofyear[]" required="required" value="" /></td>
              </tr> 

              <tr>
                <td>Last Basic <span style="color:red;"> * </span> </td>
                <td><input type="number" min="5000"onKeyPress="if(this.value.length==6) return false;" max="200000" class="isNumberKey form-control text-right" id="lastbasicsal" name="lastbasicsal" required="required" value="" /> </td>
                <td> </td>
                <td></td>
              </tr> 

              <tr>
                <td>Slide<span style="color:red;"> * </span> </td>
                <td><input type="number" min="0"onKeyPress="if(this.value.length==4) return false;" max="5000" class="isNumberKey form-control text-right" id="slide" name="slide" required="required" value="" /> </td>
                <td> </td>
                <td></td>
              </tr> 
              <tr>
                <td>Pay Scale <span style="color:red;"> * </span> </td>
                <td colspan="3"><input type="text" maxlength="100" id="payscale" name="payscale" required="required" class="form-control" /> </td>
              </tr> 
            </tbody> 
          </table>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel-footer text-right">
          <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip"  title="Want to save your changes? Click on me.">Submit</button>
          <a href="<?php echo site_url("Payscaleimport");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
        </div>
      </div>
    </form>
  </div>

</div>

</div>
</div>
<!-- #END# Exportable Table -->
</div>





<?php } } ?>

<script type="text/javascript">

  $('input[type=number]').blur(function () { 
  
generatescale();
  });

  function generatescale()
  {
    var payscaletext = '';

    payscaletext +=  $("#Startphasebasic").val() + "-"+  $("#Startphaseinc").val() + "/"+ (parseInt($("#firstnoofyear").val()) - 1).toString();

    payscaletext +=  "-"+ $("#firstphasebasic").val() + "-"+  $("#firstphaseinc").val() + "/"+ (parseInt($("#secondnoofyear").val()) - parseInt($("#firstnoofyear").val()) -1).toString();

    payscaletext +=  "-"+ $("#secondphasebasic").val() + "-"+  $("#secondphaseinc").val() + "/"+ (parseInt($("#thirdnoofyear").val()) - parseInt($("#secondnoofyear").val()) -1).toString();

     payscaletext +=  "-"+ $("#thirdphasebasic").val() + "-"+  $("#thirdphaseinc").val() + "/"+ (parseInt($("#fourthnoofyear").val()) - parseInt($("#thirdnoofyear").val()) -1).toString();

    payscaletext +=  "-"+ $("#fourthphasebasic").val() + "-"+  $("#fourthphaseinc").val() + "/"+ (39 - parseInt($("#fourthnoofyear").val())).toString();

    payscaletext +=  "-"+ $("#lastbasicsal").val();
   $("#payscale").val(payscaletext);


  }
  $(document).ready(function() {

    $(".isNumberKey").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       // $("#errmsg").html("Digits Only").show().fadeOut("slow");
       return false;
     }
   });

    $('#removeSlide').change(function() {
      if ($(this).is(':checked'))
      {
        var levelname = $('#levelname').val();
        if(levelname == 0){
          $("#removeSlide"). prop("checked", false);
          alert('Please Select Level First');
        }else{
          var Startphasebasic = $('#Startphasebasic').val();
          var firstphasebasic = $('#firstphasebasic').val();
          var secondphasebasic = $('#secondphasebasic').val();
          var thirdphasebasic = $('#thirdphasebasic').val();
          var fourthphasebasic = $('#fourthphasebasic').val();
          var lastbasicsal = $('#lastbasicsal').val();
          var slide = $('#slide').val();
          $('#Startphasebasic').val(($('#Startphasebasic').val()-slide).toFixed(2));
          $('#firstphasebasic').val(($('#firstphasebasic').val()-slide).toFixed(2));
          $('#secondphasebasic').val(($('#secondphasebasic').val()-slide).toFixed(2));
          $('#thirdphasebasic').val(($('#thirdphasebasic').val()-slide).toFixed(2));
          $('#fourthphasebasic').val(($('#fourthphasebasic').val()-slide).toFixed(2));
          $('#lastbasicsal').val(($('#lastbasicsal').val()-slide).toFixed(2));
          $('#slide').val(($('#slide').val()-slide).toFixed(2));
          var payscale = digits($('#Startphasebasic').val())+'-'+$('#Startphaseinc').val()+'/2-'+digits($('#firstphasebasic').val())+'-'+$('#firstphaseinc').val()+'/2-'+digits($('#secondphasebasic').val())+'-'+$('#secondphaseinc').val()+'/5-'+digits($('#thirdphasebasic').val())+'-'+$('#thirdphaseinc').val()+'/8-'+digits($('#fourthphasebasic').val())+'-'+$('#fourthphaseinc').val()+'/18-'+digits($('#lastbasicsal').val());
          $('#payscale').val(payscale);
          // alert(payscale);
        }
      }else{
        //alert('fdghfhg');
        var financialyear = $('#financialyear').val();
        var levelname = $('#levelname').val();
        if (financialyear != '') {
          $.ajax({
            url: '<?php echo site_url(); ?>ajax/getPayscaledetail/'+ levelname+'/'+financialyear , 
            type: 'POST',
            dataType: 'text',
          })
          .done(function(data) {
               // console.log(data);
               $("#txtpayscal").html(data);

             })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });
        }else{
          alert('Please Select Financial Year');
        }
      }
    });

 /* function digits (number) { 
    return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  }*/
  function digits(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = parseInt(val).toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2').toLocaleString('en');
    }
    // alert(val);
    return val;
  }

  $('#copyto').change(function() {
   if ($(this).is(':checked'))
   {
    $("input[type=text]").prop('disabled', true);
    $("input[type=number]").prop('disabled', true);
  }
  else
  {
   $("input[type=text]").prop('disabled', false);
   $("input[type=number]").prop('disabled', false);
 }
});


  $("#levelname").change(function(){
   var financialyear = $('#financialyear').val();
   var levelname = $('#levelname').val();
   if (financialyear != '') {
    $.ajax({
      url: '<?php echo site_url(); ?>ajax/getPayscaledetail/'+ levelname+'/'+financialyear , 
      type: 'POST',
      dataType: 'text',
    })
    .done(function(data) {
       // console.log(data);
       $("#txtpayscal").html(data);
       $('input[type=number]').blur(function () { 
    
generatescale();
  });

     })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }else{
    alert('Please Select Financial Year');
  }
});  
  $("#financialyear").change(function(){
   var financialyear = $('#financialyear').val();
   var levelname = $('#levelname').val();
   if (financialyear != '') {
     $.ajax({
       url: '<?php echo site_url(); ?>ajax/getPayscaledetail/'+ levelname+'/'+financialyear , 
       type: 'POST',
       dataType: 'text',
     })
     .done(function(data) {
	       // console.log(data);
        $("#txtpayscal").html(data);

      })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       console.log("complete");
     });
   }else{
    alert('Please Select Financial Year');
  }
});


  $("#frmCSVImport").on("submit", function () {

    $("#response").attr("class", "");
    $("#response").html("");
    var fileType = ".csv";
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
    if (!regex.test($("#file").val().toLowerCase())) {
      $("#response").addClass("error");
      $("#response").addClass("display-block");
      $("#response").html("Invalid File. Upload : <b>" + fileType + "</b> Files.");
      return false;
    }
    return true;
  });
});
</script>
