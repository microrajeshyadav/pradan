<?php //print_r($view_history);?>
<section class="content">
  <br>
  <?php //echo $token;//foreach ($role_permission as $row) { if ($row->Controller == "Permissions" && $row->Action == "index"){ ?>

    <div class="container-fluid">


      <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-7 panel-title pull-left">Manage Staff Seperation History  </h4>

           </div>
           <hr class="colorgraph"><br>
         </div>
         <div class="panel-body">

          <?php 
          $tr_msg= $this->session->flashdata('tr_msg');
          $er_msg= $this->session->flashdata('er_msg');

          if(!empty($tr_msg)){ ?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('tr_msg');?>. </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } else if(!empty($er_msg)){?>
              <div class="content animate-panel">
                <div class="row">
                  <div class="col-md-12">
                    <div class="hpanel">
                      <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('er_msg');?>. </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>


              <table id="staff_transfer_history" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 50px;">S.NO</th>
                    <th class="text-center">Seperate Date</th>
                    <th class="text-center">Staff</th>
                    <th class="text-center">Type</th>
                    <th class="text-center">Sender</th>
                    <th class="text-center">Recevier</th>
                    <th class="text-center">Request Date</th>
                    <th class="text-center">Status</th>
                    <!-- <th class="text-center">Action</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php

                  // print_r($getseperation); die;
                  $i=0; foreach($getseperation as $row){ 
                  //print_r($view_history);

                    ?>
                    <tr>
                      <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
                      <td class="text-center"><?php echo $this->gmodel->changedatedbformate($row->seperatedate);?></td>
                      <td class="text-center"><?php echo $row->name; ?></td>
                      <td class="text-center"><?php echo $row->process_type; ?></td> 
                      <td class="text-center"><?php echo $row->sendername; ?></td> 
                      <td class="text-center"><?php echo $row->receivername; ?></td> 
                      <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($row->requestdate);?> </td> 
                      <td class="text-center"><?php echo $row->status;?> </td> 
                      <td class="text-center">
                      <!-- <a href="<?php echo base_url('Staff_sepclearancecertificate/index/'.$row->transid);?>" class="btn btn-warning btn-sm">Forms</a> -->
                      </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>

            </div>
        
        <!-- #END# Exportable Table -->
      </div>
      <?php //} } ?>
    </section>
    <script>
     $(document).ready(function() {
      $('#staff_transfer_history').DataTable({
        "paging": true,
        "search": true,
      });
    });
     function confirm_delete() {

      var r = confirm("Are you sure you want to delete this item?");

      if (r == true) {
        return true;
      } else {
        return false;
      }

    }
  </script>