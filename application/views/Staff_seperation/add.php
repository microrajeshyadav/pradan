<section class="content">
  <br>
 <?php  //echo $token;//foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "add"){ ?>
   <div class="container-fluid">
    <!-- Exportable Table -->
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-sm-offset-4">
      <form method="POST" action="">
        <input type="hidden" name="reportingto" value="<?php if(!empty($getStaffReportingto->reportingto)) echo $getStaffReportingto->reportingto;?>">
        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-7 panel-title pull-left"><?php if($login_data->RoleID==3) { echo "Resignation Request"; } else {echo "Seperation Initiate";}?>  </h4>
             <div class="col-md-5 text-right" style="color: red">
              * Denotes Required Field 
            </div>
          </div>
          <hr class="colorgraph"><br>
        </div>
         <?php 
         $tr_msg= $this->session->flashdata('tr_msg');
         $er_msg= $this->session->flashdata('er_msg');
         if(!empty($tr_msg)){ ?>
          <div class="content animate-panel">
            <div class="row">
              <div class="col-md-12">
                <div class="hpanel">
                  <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('tr_msg');?>. </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('er_msg');?>. </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
            <div class="panel-body">
            <div class="form-group">
              <div class="form-line">
                <label for="StateNameEnglish" class="field-wrapper required-field">Staff<span style="color: red;" >*</span></label>
                <select class="form-control" name="staff1" id="staff1"  disabled="true">
                  <option value="">Select The Staff</option>
                  <?php foreach($staff_list as $val)
                  {
                    if($val->staffid==$token)
                    {
                      ?>
                      <option value="<?php echo $val->staffid;?>" SELECTED>
                        <?php echo $val->name;?>
                      </option> 
                    <?php }else {  ?>           
                      <option value="<?php echo $val->staffid;?>"><?php echo $val->name;?></option>
                    <?php  } } ?>
                  </select>

                </div>
                <?php echo form_error("leave");?>
              </div>

              <div class="form-group">
                <div class="form-line">
                  <label for="StateNameEnglish" class="field-wrapper required-field">Present Office<span style="color: red;" >*</span></label>
                  <select class="form-control" name="Presentoffice" id="Presentoffice">
                    <option value="<?php echo $transfer_promption->new_office_id;?>"  selected> <?php echo $transfer_promption->officename;?></option>
                  </select>
                </div>
                <?php echo form_error("leaveaccured");?>
              </div>
              <div class="form-group  <?php if($login_data->RoleID==3) { echo "d-none"; }?>">
                <div class="form-line">
                  <label for="StateNameEnglish" class="field-wrapper required-field">Seperation Type <span style="color: red;" >*</span></label>
                  <select name="trans_status" class="form-control"  required="">
                  
                   <?php if($login_data->RoleID==3) {?>
                     <option value="Resign" selected="selected">Resignation</option>
                      
                   <?php } else if($login_data->RoleID==10 || $login_data->RoleID==16 || $login_data->RoleID==17) {?>
                     <option value="">Select</option>
                    <option value="Termination">Termination</option>
                    <!-- <option value="Termination during Probation">Termination during Probation</option> -->
                    <option value="Death">Death</option>
                    <?php if(!empty($superannuation)){ ?>
                    <option value="Retirement">Retirement</option>
                    <?php } ?>
                    <option value="Discharge simpliciter/ Dismissal">Discharge simpliciter/ Dismissal </option>
                    <option value="Desertion cases">Desertion cases</option>
                    <option value="Super Annuation">Super Annuation</option>
                    
                  <?php }?>

                </select>
              </div>
              <?php echo form_error("leaveaccured");?>
            </div>

            <div class="form-group">
              <div class="form-line">
               <label for="StateNameEnglish" class="field-wrapper required-field">Proposed Seperate Date<span style="color: red;" >*</span></label>
               <input type="text" class="form-control datepicker Datepicker" data-toggle="tooltip"  title="Seperate Date !" 
               id="proposed_seperate_date" name="proposed_seperate_date" required="required" placeholder="Seperate Date" style="min-width: 20%;">
               <?php echo form_error("leaveaccured");?>
             </div> </div>

               <div class="form-group">
                  <div class="form-line">
                   <label for="StateNameEnglish" class="field-wrapper required-field">Reason<span style="color: red;" >*</span></label>
                   (<i>Maximum 250 characters allowed</i>)

                   <textarea class="form-control" data-toggle="tooltip" 
                    name="reason" id="reason" placeholder="Reason" maxlength="250" required="required" style="min-width: 20%;" ></textarea> 
                 </div>
               </div>



             <div class="form-group">
                <div class="form-line">
                 <label for="StateNameEnglish" class="field-wrapper required-field">Discussion<span style="color: red;" >*</span></label>
                  (<i>Maximum 250 characters allowed</i>)
                  
                   <textarea class="form-control" data-toggle="tooltip" 
                    name="remark" id="remark" placeholder="Discussion" maxlength="250" required="required" style="min-width: 20%;" ></textarea> 
               </div>

             </div>
         </div>

          <div class="panel-footer text-right"> 
            <!-- <a href="<?php echo site_url('Staff_sepclearancecertificate/index/'.$this->loginData->staffid);?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to open separation forms.">Seperation Forms</a> --> 
            <!-- <a href="<?php echo site_url('Staff_seperation/viewhistory/'.$token);?>" class="btn btn-primary btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on view history.">View History</a> --> 
            <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save your changes? Click on me.">Submit & Send</button>
            <a href="<?php echo site_url("Staff_list");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
          </div>

        </div>
      </form>
    </div>
    
    <!-- #END# Exportable Table -->
  </div>
  <?php //} } ?>
</section>



<script type="text/javascript">
  $(document).ready(function(){

 $(".datepicker").datepicker({
      changeMonth: true,
        changeYear: true,
        minDate: 0,
        maxDate:'+6M',
        yearRange: '1980:2025',
        dateFormat : 'dd/mm/yy'      
    });

    var staff_id = $('#staff1').val();
    var data;
    data=staff_id;  
    $.ajax({
      data:{staff_id:staff_id},
      url: '<?php echo site_url(); ?>Ajax/getstaff/',
      type: 'POST',
      done:function(data) {   
      var obj=JSON.parse(data);
      console.log("office_name="+obj.office_name+"designation="+obj.designation+"office_id="+obj.office_id+"designation_id="+obj.designation_id);
      var dd=obj.office_name;
      var options=obj.office_name;
      var option='';
      option += `<option value="${obj.office_id}">${obj.office_name}</option>`;
      $('#Presentoffice').html(option);
    }

  });
 });


   </script>