<section class="content">
 <?php //foreach ($role_permission as $row) { if ($row->Controller == "Ugeducation" && $row->Action == "add"){ ?>
 <div class="container-fluid">
  <!-- Exportable Table -->

  <?php 
  $tr_msg= $this->session->flashdata('tr_msg');
  $er_msg= $this->session->flashdata('er_msg');

  if(!empty($tr_msg)){ ?>
  <div class="content animate-panel">
    <div class="row">
      <div class="col-md-12">
        <div class="hpanel">
          <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('tr_msg');?>. </div>
          </div>
        </div>
      </div>
    </div>
    <?php } else if(!empty($er_msg)){?>
    <div class="content animate-panel">
      <div class="row">
        <div class="col-md-12">
          <div class="hpanel">
            <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $this->session->flashdata('er_msg');?>. </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="container-fluid" style="margin-top: 20px;">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-sm-offset-4">
          <form method="POST" action="">
            <div class="panel panel-default" >
              <div class="panel-heading" style="background-color: #026d0a; color: #fff;"><b>Add Staff Resignation</b> 
              </div>
              <div class="panel-body">
    <div class="form-group">
                      <div class="form-line">
                         <label for="StateNameEnglish" class="field-wrapper required-field">Affected Date<span style="color: red;" >*</span></label>
                    <input type="text" class="form-control datepicker Datepicker" data-toggle="tooltip"  title="Affected Date !" 
                              id="affecteddate" name="affecteddate" placeholder="Affected Date" style="min-width: 20%;"  value=>
                       </div>
                       <?php echo form_error("affecteddate");?>
                    </div>     

           <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Notice period<span style="color: red;" >*</span></label>
                        <textarea type="text" class="form-control" name="period" minlength="1" maxlength="3" id="period" placeholder="Please Enter Notice Period" required=""></textarea>
                      </div>
                       <?php echo form_error("reason");?>
                    </div>
                      


                     <div class="form-group">
                      <div class="form-line">
                        <label for="StateNameEnglish" class="field-wrapper required-field">Reason<span style="color: red;" >*</span></label>
                        <textarea type="text" class="form-control" name="reason" minlength="1" maxlength="3" id="reason" placeholder="Please Enter Reason" required=""></textarea>
                      </div>
                       <?php echo form_error("reason");?>
                    </div>



            </div>

            <div class="panel-footer text-right"> 
             <button  type="submit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Save">Submit</button>
            </div>
            
          </div>
        </form>
      </div>
    </div>
    <!-- #END# Exportable Table -->
  </div>
  <?php //} } ?>
</section>