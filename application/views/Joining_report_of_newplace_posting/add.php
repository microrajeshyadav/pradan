 
<form name="joiningreport" id="joiningreport" method="POST" action="">
  <section class="content" style="background-color: #FFFFFF;" >
  <div class="container-fluid">
    <div class="panel thumbnail shadow-depth-2 listcontainer" >
      <div class="panel-heading">
        <div class="row">
         <h4 class="col-md-8 panel-title pull-left"> </h4>
         <div class="col-md-4 text-right" style="color: red">
          * Denotes Required Field 
        </div>

      </div>
      <hr class="colorgraph"><br>
    </div>
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

       
          <input type="hidden" name="token" value="<?php echo $token; ?>">  
          <div class="panel-body">
            <div class="row" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="body" style="margin-left: 10px;">
                    <div class="row" >
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>JOINING REPORT AT NEW PLACE OF POSTING</label></</p>
                    </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>

                    <?php //print_r($candidatedetils); echo $candidatedetils->doj; ?>
                    
                       <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label class="field-wrapper required-field">
                         To, <br>
                         The Executive Director  <br>              
                         PRADAN <br>
                         3, Community Shopping Centre<br> Neeti Bagh,<br> New Delhi – 110049<br>
                       </label>
                     </div>
                     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" ></div>
                    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                     <div class="form-group">
                      <label for="StateNameEnglish" class="field-wrapper required-field">Subject: Joining Report   </label>
                    </div>
                  </div>

                  <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="form-group">
                    <label class="field-wrapper required-field">Sir, </label>
                  </div>
                </div>
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <label class="field-wrapper required-field">  In response to the Letter of Transfer No. 
                  <input type="text" name="transferno" id="transferno"
                  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                  value="<?php echo $getstaffdetailslist->transferno; ?>" readonly> &nbsp&nbsp  dated &nbsp&nbsp<input type="text" name="letter_date" id="letter_date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $this->gmodel->changedatedbformate($getstaffdetailslist->letter_date); ?>" readonly >I hereby report for duty today, forenoon of  <input type="text" maxlength="50" 
                  name=" staff_duty_today_date" id="staff_duty_today_date" placeholder=" Please Enter Date"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required" value="<?php echo date('d/m/Y'); ?>" readonly>(date).<br><br><br>
                </div>

               <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                 <p> Thanking you<p>

                  <p >Yours sincerely,</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">
                  <table style="border-collapse: collapse;">
                    <tbody>
                      <tr>
                        <td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="508">
                          <!-- <p style="line-height: 150%; tab-stops: -67.5pt;">Signature:  -->
                            <!-- <input type="text" name="staff_signature" id="staff_signature"
                            style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                            value="" > -->

                            <p style="line-height: 150%; tab-stops: -67.5pt;">Signature: <?php 
                            if(!empty($getstaffdetailslist->encrypted_signature)) {
                              $image_path='';
                              $image_path=FCPATH.'datafiles\signature/'.$getstaffdetailslist->encrypted_signature;

                              if (file_exists($image_path)) {




                                ?>
                                <img src="<?php echo site_url().'datafiles\signature/'.$getstaffdetailslist->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                              <?php }
                              else
                              {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">

                                <?php
                              } }
                              else {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                                <?php } ?></p>
                              </p>
                            </td>
                            <td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
                              <p style="text-align: right; line-height: 150%; tab-stops: -67.5pt;">Employee <span style="line-height: 150%;">Code</span>:<input type="text" name="emp_code" id="emp_code"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo $getstaffdetailslist->emp_code; ?>" readonly></p>
                            </td>
                          </tr>
                          <tr>
                            <td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
                              <p style="line-height: 150%; tab-stops: -67.5pt;">Name&nbsp;&nbsp;&nbsp;&nbsp; :
                               <input type="text" name="emp_name" id="emp_name" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                               value="<?php echo $getstaffdetailslist->name; ?>" readonly></p>
                             </td>
                             <td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
                            <p style="margin-left: 74.9pt; line-height: 150%; tab-stops: -67.5pt;">Place :<input type="text" name="emp_place" id="emp_place"
                              style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                              value="<?php echo  $getstaffdetailslist->officename;?>" required readonly></p>
                            </td>
                          </tr>
                          <tr>
                            <td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
                              <p style="line-height: 150%; tab-stops: -67.5pt;">Designation: <input type="text" name="designation" id="designation"
                                style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                                value="<?php echo $getstaffdetailslist->desname; ?>" readonly></p>
                              </td>
                              <td style="width: 231.15pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
                                <p style="text-align: right; line-height: 150%; tab-stops: -67.5pt;">Date&nbsp; : <input type="text" name="currentdate" id="currentdate" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
                                  value="<?php echo date('d/m/Y'); ?>" readonly></p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>

<?php if($getstaffdetailslist->STATUS !=0 && $this->loginData->staffid != $getstaffdetailslist->staffid ){ ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        

  <p style="text-align: center; tab-stops: -1.5in -49.5pt;">(<strong>For Use of the Integrator/Team Coordinator </strong>)</p>

  <p style="text-align: justify; tab-stops: -1.5in -49.5pt;">&nbsp;</p>

  <p style="tab-stops: -1.5in -49.5pt;">
    Reported for duty on <input type="text" class="datepicker" name="dutyon" id="dutyon"
    style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
    value="" required><span style="font-size: 10.0pt;">(date)</span> forenoon.</p>
    <p style="tab-stops: -1.5in -49.5pt;">&nbsp;</p>
    <p style="text-align: justify; tab-stops: -1.5in -49.5pt;">S/he has been assigned to<input type="text" name="assignedto" id="assignedto"
      style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
      value="<?php echo $getstaffdetailslist->name; ?>" readonly>project/programme at <input type="text" name="new_office_name" id="new_office_name"
      style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value = "<?php echo  $getstaffdetailslist->newoffice;?>" readonly> location and will work under the supervision of
      <input type="hidden" id="supervision_id" name="supervision_id" value="<?php echo $getstaffsuperiordetailslist->reportingto; ?>">
      <input type="text" name="supervision" id="supervision"
      style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
      value="<?php echo $getstaffsuperiordetailslist->name.'-'.$getstaffsuperiordetailslist->desname;?>" readonly>&nbsp; (<span style="font-size: 10.0pt;">Name and Designation</span>).</p>
     <p style="text-align: justify; tab-stops: -1.5in -49.5pt;">&nbsp;</p>

   </div>


<table style="border-collapse: collapse;">
<tbody>
<tr>
<td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
  <p style="line-height: 150%; tab-stops: -67.5pt;">Signature: <!-- <input type="text" name="tc_signature" id="tc_signature"
    style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
    value=""> -->


    <?php 

    if($workflow_flag->flag>=15){
                            if(!empty($getstaffsuperiordetailslist->encrypted_signature)) {
                              $image_path1='';
                              $image_path1=FCPATH.'datafiles\signature/'.$getstaffsuperiordetailslist->encrypted_signature;

                              
                              if (file_exists($image_path1)) {

                                ?>
                                <img src="<?php echo site_url().'datafiles\signature/'.$getstaffsuperiordetailslist->encrypted_signature;?>"  width="104px" hight="132px" title="Image" alt="Image" boder="2px"  class="rounded-circle">
                              <?php }
                              else
                              {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                                
                                <?php
                              } }
                              else {
                                ?>
                                <img src="<?php echo site_url().'datafiles/imagenotfound.jpg';?>"  width="104px" hight="132px" title="Default Image" alt="Default Image" boder="2px"  class="rounded-circle">
                                <?php } } ?>
  </p>
  </td>
</tr>
<tr>
  <td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
    <p style="line-height: 150%; tab-stops: -67.5pt;">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :<input type="text" name="tc_signature_name" id="tc_signature_name"
      style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"
      value="<?php echo $getstaffsuperiordetailslist->name; ?>" required readonly></p>
    </td>
  </tr>
  <tr>
    <td style="width: 231.1pt; padding: 0in 5.4pt 0in 5.4pt;" width="308">
      <p style="line-height: 150%; tab-stops: -67.5pt;">Date: <input type="text" class="datepicker" name="tc_signature_date" id="tc_signature_date" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" value="<?php echo date('d-m-Y'); ?>" required readonly></p>
      </td>
    </tr>
  </tbody>
</table>
<?php } ?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div style="text-align: right;"> 
 <!-- <button  type="submit" name="savebtn" id="savebtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button> --> 
 <?php if($getstaffdetailslist->STATUS !=0 && $this->loginData->staffid != $getstaffdetailslist->staffid ){ ?>       
 <button  type="button" name="submitbtn" id="submitbtn" value="senddatasubmit"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="savesubmit" onclick="joiningreportnewplace();">Submit</button>
 <?php }else{ ?>
<button  type="submit" name="savebtn" id="savebtn" value="senddatasave"  class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="save">Save</button>
<?php } ?>
</div>
                               </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
             <!-- #END# Exportable Table -->

           </section>

<!-- Modal -->
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Notes"  required> </textarea>
              <?php echo form_error("comments");?>
            </div>
        </div>
        <!-- </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="button" name="btnsubmit" id="btnsubmit" value="Save" class="btn btn-success btn-sm" onclick="joiningreportnewplacemodal();">
        </div>
      </div>
      
    </div>
  </div>
</div> 
<!-- Modal -->
</form>
<script>
  function joiningreportnewplace(){
    var emp_place = document.getElementById('emp_place').value; 
    var dutyon = document.getElementById('dutyon').value;  
    var tc_signature_name = document.getElementById('tc_signature_name').value;   
    var tc_signature_date = document.getElementById('tc_signature_date').value;
    // alert(name_of_location);

    if(tc_signature_date == ''){
      $('#tc_signature_date').focus();
    }

    if(tc_signature_name == ''){
      $('#tc_signature_name').focus();
    }
    
    if(dutyon == ''){
      $('#dutyon').focus();
    }

    if(emp_place == ''){
      $('#emp_place').focus();
    }
    
    if(emp_place != '' && dutyon !='' && tc_signature_name !='' && tc_signature_date !=''){
      $('#myModal').removeClass('fade');
      $("#myModal").show();
    }
  }
  function joiningreportnewplacemodal(){
    var comments = document.getElementById('comments').value;
    if(comments.trim() == ''){
      $('#comments').focus();
    }
    if(comments.trim() !=''){
      document.forms['joiningreport'].submit();
    }
  }
  </script>