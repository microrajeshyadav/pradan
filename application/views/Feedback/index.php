
<section class="content" style="background-color: #FFFFFF;" >
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Feedback" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">
   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">List Of Feedback</h4>
      
    </div>
      <hr class="colorgraph"><br>
    </div>

    <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');
        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">

                <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

         

            <form name="feesbacksearch"  id="feesbacksearch" method="POST" action="">

             <div class="row">
              <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Office Name :<span style="color: red;">*</span></label></div>
              <div class="col-lg-4">
                <select name="team_id" id="team_id" class="form-control" required="required">
                  <option value="">Select Office Name </option>
                  <?php foreach ($office_detail as $key => $value) { ?>
                  <option value="<?php echo $value->officeid;?>" <?php if($tc_detail) if($value->officeid==$tc_detail->officeid){?> selected <?php } else  {
                    echo $value->officeid;
                  }?>><?php echo $value->officename;?></option>
                  <?php } ?>
                </select></div>
                <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;">
            <button type="submit" name="Searchtaemid" id="Searchtaemid" value="Searchtaemid" data-toggle="tooltip" title="Want to save your changes? Click on me." class="btn btn-dark">Search</button>
          </div>
              </div>

              
             <div class="row" style="background-color: #FFFFFF;">
               <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                 <table id="tablecampus" class="table table-bordered table-striped wrapper">
            <thead>
             <tr>
              <th style="max-width: 30px;" class="text-center">S. No.</th>
              <th class="text-center">Employ Code</th>
              <th>Name</th>
              <th>Email id </th>
              <th class="text-center">Batch</th>
              <th>Office</th>
              <th>Field guide Name</th>
              <th>Period</th>
              <th class="text-center">Feedback </th>
              <th class="text-center">Status</th>
           </tr> 
         </thead>
        
          <?php 
          if(count($getfeedbacklist) == 0) {
            ?>
        <tbody>
        
        </tbody>
         <?php } else{
          $i=0; foreach ($getfeedbacklist as $key => $value) { 
          $currentdate = date('Y-m-d');
          $duedate = $value->duedate;
          $enddate = $value->enddate;
          ?>
        <tbody>
         <tr>
           <td class="text-center"><?php echo $i+1; ?></td>
            <td class="text-center"><?php echo $value->emp_code; ?></td>
           <td>
            <?php   
              if ($currentdate >= $duedate && $currentdate <= $enddate  && $value->status==0 ) { ?>
                 <a href="<?php echo site_url().'Feedback/edit/'.$value->id ?>" id="modalcandidate" class="btn btn-warning btn-xs"><?php echo $value->apprenticename; ?></a>
              <?php  }elseif($value->status==2){ ?>
              <a href="<?php echo site_url().'Feedback/view/'.$value->id ?>" id="modalcandidate" class="btn btn-warning btn-xs"><?php echo $value->apprenticename; ?></a>
              <?php }else{                 ?>
              <?php echo $value->apprenticename; ?>
              <?php } ?>
            </td>
             <td><?php echo $value->emailid; ?></td>
           <td class="text-center"><?php echo $value->batch; ?></td>
           <td><?php echo $value->officename; ?></td>
           <td><?php echo $value->fieldguide; ?></td>
           <td><?php echo $this->model->changedatedbformate($value->duedate); ?><br><?php echo $this->model->changedatedbformate($value->enddate); ?></td>
          <td class="text-center" >
            <?php  if (isset($value->feedback_letter)) { ?>
             <a href="<?php echo site_url().'pdf_separationletters/'.$value->feedback_letter.'.pdf'; ?>" id="modalcandidate" data-toggle="tooltip" title="Download feedback PDF" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
          <?php }else{  ?>
          <span class="bedge badge-pill badge-dark"> Not Available</span>
            <?php } ?>
          </td>
             <?php
                if ($currentdate >= $duedate && $currentdate <= $enddate  && $value->status==0) {  ?>

               <td class="text-center"> <span class="bedge badge-pill badge-danger">Pending </span></td>
           <?php  }elseif ($value->status==2) { ?>
               <td class="text-center"><span class="bedge badge-pill badge-success">Submitted</span></td>
        <?php } else{?>
               <td class="text-center"><span class="bedge badge-pill badge-dark">Not Available</span></td>
        <?php } ?>
            </tr>
            </tbody>
          <?php $i++; } }?>
        
      </table>

               </div>
               
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
        <?php } } ?> 
      </form>
      </section>
    <script type="text/javascript">
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
      });
      function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
   
}
    </script>