<section class="content">
  <br>
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
                  <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-8 panel-title pull-left">Feedback</h4>
                     <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
                <div class="panel-body">
                  <form name="addfeedback" id="addfeedback" method="post" action="">
                    <input type="hidden" name="month" id="month" value="<?php   $date = date('Y-m-d');
                     echo $month = date('m-Y', strtotime($date)); ?>" >
                    <div class="panel-body">
                      <div class="row"> 
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Name of the Apprentice <span style="color: red;" >*</span></label>
                        <select name="name_of_apprentice" id="name_of_apprentice"  required="required" class="form-control">
                         <option value="">Select</option>
                         <?php foreach ($getapprentice as $key => $value) {?>
                         <option value="<?php echo $value->staffid;?>"><?php echo $value->name; ?></option>
                         <?php } ?>
                       </select>  
                     </div>
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Month <span style="color: red;" >*</span></label>
                      <?php  
                        $monthArr = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
                      ?>
                        <select name="extmonthdate" id="extmonthdate" class="form-control" required="required" disabled="disabled">
                        <?php foreach ($monthArr as $key => $value) {?>  
                         <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                        <input type="hidden" name="extmonth" id="extmonth"  class="form-control" >
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Batch <span style="color: red;" >*</span></label>
                        <input type="hidden" name="batchid" id="batchid" value="" >
                        <input type="text" name="batch" readonly="readonly" id="batch"  class="form-control" >
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Team <span style="color: red;" >*</span></label>
                        <input type="hidden" name="teamid" id="teamid" value="" >
                      <input type="text" name="team" id="team" class="form-control" readonly="readonly"></div>
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Field Guide  <span style="color: red;" >*</span></label>
                        <input type="hidden" name="field_guideid" id="field_guideid" value="" >
                         <input type="text" name="field_guide" id="field_guide" class="form-control" readonly="readonly" > 
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Team Coordinator (TC) <span style="color: red;" >*</span></label>
                      <input type="hidden" name="tcid" name="tcid"  value="<?php echo $this->loginData->UserID; ?>" >
                     <input type="tcname" name="tcname" class="form-control" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?>" readonly="readonly" > 

                         
                     </div>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Feedback Process:</label>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(i) Aspect(s) you find the Development Apprentice (DA) excels in or is extremely good at.<span style="color: red;" >*</span></label>
                     <textarea name="excels_in_or_extremely_good" id="" maxlength="150" class="form-control" required="required"></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(ii)  Aspect(s) you think that the DA needs to pay attention to. <span style="color: red;" >*</span></label>
                      <textarea name="da_needs_to_pay_attention" id="da_needs_to_pay_attention" maxlength="150" class="form-control" required="required"></textarea>
                    </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(iii) Any concern that you may have seen, necessitating PRADAN to initiate action to ask the DA to leave the organization. <span style="color: red;" >*</span></label>
                      <textarea name="da_to_leave_organization" id="da_to_leave_organization" maxlength="150" class="form-control" required="required"></textarea>
                    </div>
                    <br><br><br>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Date </label>
                        <input type="text" name="feedbackdate" id="feedbackdate" readonly="readonly" class="form-control " value="<?php echo date('d/m/Y'); ?>" >
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Name and TC </label>
                           <input type="text" name="nametc" id="nametc" readonly="readonly" class="form-control" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?>">
                       </div>
                     </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right">
                   <!--   <button type="submit" name="intemationssave" id="intemationsave" value="save" class="btn btn-success btn-sm m-t-10 waves-effect">Save </button> -->
                      <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save and send your changes? Click on me.">Submit & Send </button>
                      <a href="<?php echo site_url("Feedback");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                   </div>
                 </div>
               </div>
             </form>               
           </div>
         </div><!-- /.panel-->

       </div>

     </div>

   </div>

 </div>
 <!-- #END# Exportable Table -->
</div>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
   });

    
  });


$("#name_of_apprentice").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getfeedbackBatch/'+$(this).val(),
        type: 'POST',
        dataType: 'json',
      })
      .done(function(data) {
        $.each(data, function(index) {
            console.log(data);
         
            var fdateSplit          = data[index].doj;
            var dateSplit           = fdateSplit.split('-');
            var dojMonth            =  dateSplit[1];
            var dojextthirdMonth    = parseInt("3");
            var dojextSeventhMonth  = parseInt("7");
            var dojextEleventhMonth = parseInt("11");

           var completeMonth  = parseInt(dojMonth) + parseInt(dojextthirdMonth);
           var completeMonth1 = parseInt(dojMonth) + parseInt(dojextSeventhMonth);
           var completeMonth2 = parseInt(dojMonth) + parseInt(dojextEleventhMonth);

 //console.log(completeMonth);

          if(completeMonth ==1){
            completeMon = 1;
          }else if(completeMonth ==2){
            completeMon = 2;
          }else if(completeMonth ==3){
            completeMon = 3;
          }else if(completeMonth ==4){
            completeMon = 4;
          }else if(completeMonth ==5){
            completeMon = 5;
          }else if(completeMonth ==6){
            completeMon = 6;
          }else if(completeMonth ==7){
            completeMon = 7;
          }else if(completeMonth ==8){
            completeMon = 8;
          }else if(completeMonth ==9){
            completeMon = 9;
          }else if(completeMonth ==10){
            completeMon = 10;
          }else if(completeMonth ==11){
            completeMon = 11;
          }else if(completeMonth ==12){
            completeMon = 12;
          }else if(completeMonth ==13){
            completeMon = 1;
          }else if(completeMonth ==13){
            completeMon = 1;
          }else if(completeMonth ==14){
            completeMon = 2;
          }else if(completeMonth ==15){
            completeMon = 3;
          }else if(completeMonth ==16){
            completeMon = 4;
          }else if(completeMonth ==17){
            completeMon = 5;
          }else if(completeMonth ==18){
            completeMon = 6;
          }else if(completeMonth ==19){
            completeMon = 7;
          }else if(completeMonth ==20){
            completeMon = 8;
          }else if(completeMonth ==21){
            completeMon = 9;
          }else if(completeMonth ==22){
            completeMon = 10;
          }else if(completeMonth ==23){
            completeMon = 11;
          }else if(completeMonth ==24){
            completeMon = 12;
          }

            $("#extmonthdate").val(completeMon);
            $("#extmonth").val(completeMon);
            $("#batchid").val(data[index].batchid);
            $("#batch").val(data[index].batch);
            $("#field_guideid").val(data[index].fgid);
            $("#field_guide").val(data[index].fieldguidename);
            $("#team").val(data[index].officename);   
            $("#teamid").val(data[index].teamid);   
            //$("#extmonth").prop('disabled', 'disabled');       
          })
       })

     .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

 
</script>
