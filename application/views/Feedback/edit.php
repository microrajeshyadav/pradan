<section class="content">
  <div class="container-fluid">
    <!-- Exportable Table -->
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');

    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">
              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('tr_msg');?>. </div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo $this->session->flashdata('er_msg');?>. </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <?php //echo "<pre>"; print_r($feedbacklist); ?>
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
                  <div class="panel-heading">
                    <div class="row">
                     <h4 class="col-md-8 panel-title pull-left">Feedback</h4>
                     <div class="col-md-4 text-right" style="color: red">
                    * Denotes Required Field 
                    </div>
                  </div>
                  <hr class="colorgraph"><br>
                </div>
                <?php //echo "<pre>"; print_r($feedbacklist); ?>
                <div class="panel-body">
                  <form name="addfeedback" id="addfeedback" method="post" action="">
                     <input type="hidden" name="month" id="month" value="<?php echo $feedbacklist->monthyear; ?>" >
                    <div class="panel-body">
                      <div class="row"> 
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Name of the Apprentice <span style="color: red;" >*</span></label>
                        <select name="name_of_apprentice" id="name_of_apprentice"  required="required" class="form-control">
                         <option value="">Select</option>
                         <?php foreach ($getapprentice as $key => $value) {
                          if ($value->staffid==$feedbacklist->apprenticeid) {
                          ?>
                         <option value="<?php echo $value->staffid;?>" SELECTED ><?php echo $value->name; ?></option>
                         <?php } else{ ?>
                          <option value="<?php echo $value->staffid;?>" ><?php echo $value->name; ?></option>
                        <?php } } ?>
                       </select>  
                     </div>

                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Month <span style="color: red;" >*</span></label>
                        <input type="text" name="extmonth" readonly="readonly" id="extmonth"  value="<?php  echo $feedbacklist->monthyear; ?>" class="form-control" >
                     </div>

                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Batch <span style="color: red;" >*</span></label>
                        <input type="hidden" name="batchid" id="batchid" value="<?php  echo $feedbacklist->batchid; ?>" >
                        <input type="text" name="batch" readonly="readonly" id="batch"  value="<?php  echo $feedbacklist->batch; ?>" class="form-control" >
                     </div>
                     
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Team <span style="color: red;" >*</span></label>
                        <input type="hidden" name="teamid" id="teamid" value="<?php  echo $feedbacklist->teamid; ?>" >
                      <input type="text" name="team" id="team" value="<?php  echo $feedbacklist->officename; ?>" class="form-control" readonly="readonly"></div>
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Field Guide  <span style="color: red;" >*</span></label>
                        <input type="hidden" name="field_guideid" id="field_guideid" value="<?php  echo $feedbacklist->field_guideid; ?>" >
                         <input type="text" name="field_guide" value="<?php  echo $feedbacklist->name; ?>" id="field_guide" class="form-control" readonly="readonly" > 
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Team Coordinator (TC) <span style="color: red;" >*</span></label>
                      <input type="hidden" name="tcid" name="tcid"  value="<?php echo $this->loginData->UserID; ?>" >
                     <input type="tcname" name="tcname" class="form-control" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?>" readonly="readonly" > 
                     </div>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Feedback Process:</label>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(i) Aspect(s) you find the Development Apprentice (DA) excels in or is extremely good at.<span style="color: red;" >*</span></label>
                     <textarea name="excels_in_or_extremely_good" id="" maxlength="150" class="form-control" required="required"><?php echo $feedbacklist->da_extremely_good;?></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(ii)  Aspect(s) you think that the DA needs to pay attention to. <span style="color: red;" >*</span></label>
                      <textarea name="da_needs_to_pay_attention" id="da_needs_to_pay_attention" maxlength="150" class="form-control" required="required"><?php echo $feedbacklist->da_needs_to_pay_attention;?></textarea>
                    </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(iii) Any concern that you may have seen, necessitating PRADAN to initiate action to ask the DA to leave the organization. <span style="color: red;" >*</span></label>
                      <textarea name="da_to_leave_organization" id="da_to_leave_organization" maxlength="150" class="form-control" required="required"><?php echo $feedbacklist->da_to_leave_organization;?></textarea>
                    </div>
                    <br><br><br>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Date </label>
                        <input type="text" name="feedbackdate" id="feedbackdate" readonly="readonly" class="form-control " value="<?php echo date('d/m/Y'); ?>" >
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Name and TC </label>
                           <input type="text" name="nametc" id="nametc" readonly="readonly" class="form-control" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?>">
                       </div>
                     </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right">
                    <!--  <button type="submit" name="intemationssave" id="intemationsave" value="save" class="btn btn-success btn-sm m-t-10 waves-effect">Save </button> -->
                      <button type="submit" name="intemationssend" id="intemationssend" value="send" class="btn btn-success btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Want to save and send your changes? Click on me.">Submit & Send </button>
                      <a href="<?php echo site_url("Feedback");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                   </div>
                 </div>
               </div>
             </form>               
           </div>
         </div><!-- /.panel-->

       </div>

     </div>

   </div>

 </div>
 <!-- #END# Exportable Table -->
</div>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
   });

    
  });


$("#name_of_apprentice").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getfeedbackBatch/'+$(this).val(),
        type: 'POST',
        dataType: 'json',
      })
      .done(function(data) {
        $.each(data, function(index) {
            console.log(data);
             var fdateSplit = data[index].doj;
            var dateSplit = fdateSplit.split('-');
            var dojMonth =  dateSplit[1];
            var dojextMonth = parseInt("3");

             var completeMonth = parseInt(dojMonth) + parseInt(dojextMonth);
            $("#extmonth").val(completeMonth);
            $("#batchid").val(data[index].batchid);
            $("#batch").val(data[index].batch);
            $("#field_guideid").val(data[index].fgid);
            $("#field_guide").val(data[index].fieldguidename);
            $("#team").val(data[index].officename);   
            $("#teamid").val(data[index].teamid);          
          })
       })

     .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

 
</script>
