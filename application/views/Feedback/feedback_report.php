<section class="content" style="background-color: #FFFFFF;" >
 <?php foreach ($role_permission as $row) { if ($row->Controller == "Feedback" && $row->Action == "feedback_report"){ ?>
  <br>
  <div class="container-fluid">
   <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Reports</h4>
     </div>
     <hr class="colorgraph"><br>
   </div>
   <div class="panel-body">
    <?php 
    $tr_msg= $this->session->flashdata('tr_msg');
    $er_msg= $this->session->flashdata('er_msg');
    if(!empty($tr_msg)){ ?>
      <div class="content animate-panel">
        <div class="row">
          <div class="col-md-12">
            <div class="hpanel">

              <div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
              </div>
            </div>
          </div>
        </div>
      <?php } else if(!empty($er_msg)){?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
       <form name="feesbacksearch"  id="feesbacksearch" method="POST" action="">
         <div class="row">
          <div class="col-lg-2 text-right"> <label for="joinDA" class="field-wrapper required-field">Office Name :<span style="color: red;">*</span></label></div>
          <div class="col-lg-4">
            <select name="team_id" id="team_id" class="form-control" required="required">
              <option value="">Select Office Name </option>
              <?php foreach ($office_detail as $key => $value) { ?>
                <option value="<?php echo $value->officeid;?>" <?php if($tc_detail) if($value->officeid==$tc_detail->officeid){?> selected <?php } else  {
                  echo $value->officeid;
                }?>><?php echo $value->officename;?></option>
              <?php } ?>
            </select></div>
            <div class="col-md-1 col-lg-1 col-xs-12 col-sm-12" style="background-color: white;">
              <button type="submit" name="Searchtaemid" id="Searchtaemid" value="Searchtaemid" data-toggle="tooltip" title="Want to save your changes? Click on me." class="btn btn-dark">Search</button>
            </div>
          </div>

          <?php //echo "<pre>"; print_r($getfeedbacklist);   ?>
          <div class="row" style="background-color: #FFFFFF;">
           <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
             <table id="tablecampus" class="table table-bordered table-striped wrapper">
              <thead>
               <tr>
                <th style="max-width: 30px;" class="text-center">S. No.</th>
                <th class="text-center">Employ Code</th>
                <th>Name</th>
                <th>Email id </th>
                <th class="text-center">Batch</th>
                <th>Office</th>
                <th>Field guide Name</th>
                <th class="text-center">Feedback Report 1</th>
                <th class="text-center">Feedback Report 2</th>
                <th class="text-center">Feedback Report 3</th>
                <th class="text-center">Reflective Report</th>
              </tr> 
            </thead>

            <?php 
            if(count($getfeedbacklist) == 0) {
              ?>
              <tbody>

              </tbody>
            <?php } else{
              $candidateid = 0;
              $i=0; foreach ($getfeedbacklist as $key => $value) { 
              
                if (!empty($value->feedback1) && !empty($value->feedback2) && !empty($value->feedback3)) {
                   $candidateid = $value->candidateid;
                    $da_feedback_reports = $this->model->getReflective_report($candidateid);
                  ?>
                  <tbody>
                   <tr>
                     <td class="text-center"><?php echo $i+1; ?></td>
                     <td class="text-center"><?php echo $value->emp_code; ?></td>
                     <td><?php echo $value->apprenticename; ?> </td>
                     <td><?php echo $value->emailid; ?></td>
                     <td class="text-center"><?php echo $value->batch; ?></td>
                     <td><?php echo $value->officename; ?></td>
                     <td><?php echo $value->fieldguide; ?></td>
                     <td class="text-center" >
                      <?php  if (isset($value->feedback1)) { ?>
                       <a href="<?php echo site_url().'pdf_separationletters/'.$value->feedback1.'.pdf'; ?>" id="modalcandidate" data-toggle="tooltip" title="Download feedback PDF" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                     <?php }else{  ?>
                      <span class="bedge badge-pill badge-dark"> Not Available</span>
                    <?php } ?>
                  </td>
                  <td class="text-center" >
                    <?php  if (isset($value->feedback2)) { ?>
                     <a href="<?php echo site_url().'pdf_separationletters/'.$value->feedback2.'.pdf'; ?>" id="modalcandidate" data-toggle="tooltip" title="Download feedback PDF" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                   <?php }else{  ?>
                    <span class="bedge badge-pill badge-dark"> Not Available</span>
                  <?php } ?>
                </td>
                <td class="text-center" >
                  <?php  if (isset($value->feedback3)) { ?>
                   <a href="<?php echo site_url().'pdf_separationletters/'.$value->feedback3.'.pdf'; ?>" id="modalcandidate" data-toggle="tooltip" title="Download feedback PDF" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                 <?php }else{  ?>
                  <span class="bedge badge-pill badge-dark"> Not Available</span>
                <?php } ?>
              </td>
              <td class="text-center"> <a href="#" class="btn btn-info btn-sx" data-toggle="modal" data-target="#myModal" onclick="others_document(<?php echo $value->candidateid; ?>);">Reflective report</a>



                 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reflective Reports</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body">
        <?php if (count($da_feedback_reports) > 0) { 
          $i=1;
          foreach ($da_feedback_reports as $key => $value) { ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" ><b><?php echo $value->phase_name; ?> : </b></div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6"><a href="<?php echo site_url().'datafiles/dashipcomponent/'.$value->encrypted_document_name;?>"><i class="fa fa-download"></i></a>   </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6"></div>

      <?php $i++; } }else{ ?>
         <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6">Not Found Data !!!</div>
       

      <?php } ?>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

              </td>
            </tr>
          </tbody>
          <?php $i++; } } } ?>
        </table>
      </div>
    </div>
  </div>


</div>  
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php } } ?> 
</form>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });


   function others_document(candidateid){
          //alert(candidateid)
          $('#myModal').removeClass('fade');
          $("#myModal").show();
        }

  function confirm_delete() {
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>