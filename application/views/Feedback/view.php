<section class="content"><br>
  <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel thumbnail shadow-depth-2 listcontainer" >
              <div class="panel-heading">
                <div class="row">
                 <h4 class="col-md-8 panel-title pull-left">Feedback</h4>
                 <div class="col-md-4 text-right" style="color: red">
                * Denotes Required Field 
                </div>
              </div>
              <hr class="colorgraph"><br>
                </div>
                <div class="panel-body">
                  <form name="addfeedback" id="addfeedback" method="post" action="">
                    <div class="panel-body">
                      <div class="row"> 
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Name of the Apprentice <span style="color: red;" >*</span></label>
                        <select name="name_of_apprentice" id="name_of_apprentice"  required="required" class="form-control" disabled="disable">
                         <option value="">Select</option>
                         <?php foreach ($getapprentice as $key => $value) {
                          if ($value->staffid==$feedbacklist->apprenticeid) {
                          ?>
                         <option value="<?php echo $value->staffid;?>" SELECTED ><?php echo $value->name; ?></option>
                        
                        <?php } } ?>
                       </select>  
                     </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Month <span style="color: red;" >*</span></label>
                      <?php  
                   
                        $monthArr = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
                      ?>
                        <select name="extmonthdate" id="extmonthdate" class="form-control" required="required" disabled="disabled">
                        <?php foreach ($monthArr as $key => $value) { 
                          if ($key==$feedbacklist->monthyear) {
                          ?>  
                         <option value="<?php echo $key; ?>" SELECTED><?php echo $value; ?></option>
                        <?php } } ?>
                        </select>
                        <input type="hidden" name="extmonth"  id="extmonth" value="" >
                     </div>

                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Batch <span style="color: red;" >*</span></label>
                       
                        <input type="text" name="batch"  disabled="disable" id="batch"  value="<?php  echo $feedbacklist->batch; ?>" class="form-control" >
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Team <span style="color: red;" >*</span></label>
                        <input type="hidden" name="teamid" id="teamid" value="<?php  echo $feedbacklist->teamid; ?>" >
                      <input type="text" name="team" id="team" value="<?php  echo $feedbacklist->officename; ?>" class="form-control"  disabled="disable"></div>
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Field Guide  <span style="color: red;" >*</span></label>
                        <input type="hidden" name="field_guideid" id="field_guideid" value="<?php  echo $feedbacklist->field_guideid; ?>" >
                         <input type="text" name="field_guide" value="<?php  echo $feedbacklist->name; ?>" id="field_guide" class="form-control"  disabled="disable" > 
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Team Coordinator (TC) <span style="color: red;" >*</span></label>
                      <input type="hidden" name="tcid" name="tcid"  value="<?php echo $this->loginData->UserID; ?>" >
                     <input type="tcname" name="tcname" class="form-control" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?>"  disabled="disable" > 
                     </div>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="Name">Feedback Process:</label>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(i) Aspect(s) you find the Development Apprentice (DA) excels in or is extremely good at.<span style="color: red;" >*</span></label>
                     <textarea name="excels_in_or_extremely_good" id="" maxlength="150" class="form-control"  disabled="disable"><?php echo $feedbacklist->da_extremely_good;?></textarea>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(ii)  Aspect(s) you think that the DA needs to pay attention to. <span style="color: red;" >*</span></label>
                      <textarea name="da_needs_to_pay_attention" id="da_needs_to_pay_attention" maxlength="150" class="form-control"  disabled="disable"><?php echo $feedbacklist->da_needs_to_pay_attention;?></textarea>
                    </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <label for="Name">(iii) Any concern that you may have seen, necessitating PRADAN to initiate action to ask the DA to leave the organization. <span style="color: red;" >*</span></label>
                      <textarea name="da_to_leave_organization" id="da_to_leave_organization" maxlength="150" class="form-control"  disabled="disable"><?php echo $feedbacklist->da_to_leave_organization;?></textarea>
                    </div>
                    <br><br><br>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label for="Name">Date </label>
                        <input type="text" name="feedbackdate" id="feedbackdate"  disabled="disable" class="form-control " value="<?php echo date('d/m/Y'); ?>" >
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                          <label for="Name">Name and TC </label>
                           <input type="text" name="nametc" id="nametc"  disabled="disable" class="form-control" value="<?php echo $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName; ?>">
                       </div>
                     </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-footer text-right">
                   
                      <a href="<?php echo site_url("Feedback");?>" class="btn btn-dark btn-sm m-t-10 waves-effect" data-toggle="tooltip" title="Click here to move on list.">Go to List</a> 
                   </div>
                 </div>
               </div>
             </form>               
           </div>
         </div><!-- /.panel-->

       </div>

     </div>

   </div>

 </div>
 <!-- #END# Exportable Table -->
</div>
</section>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $(".datepicker").datepicker({
     changeMonth: true,
     changeYear: true,
     maxDate: 'today',
     dateFormat : 'dd/mm/yy',
     yearRange: '1920:2030',
   });

    
  });


$("#name_of_apprentice").change(function(){
      $.ajax({
        url: '<?php echo site_url(); ?>ajax/getfeedbackBatch/'+$(this).val(),
        type: 'POST',
        dataType: 'json',
      })
      .done(function(data) {
        $.each(data, function(index) {
            console.log(data);
            $("#batchid").val(data[index].batchid);
            $("#batch").val(data[index].batch);
            $("#field_guideid").val(data[index].fgid);
            $("#field_guide").val(data[index].fieldguidename);
            $("#team").val(data[index].officename);   
            $("#teamid").val(data[index].teamid);          
          })
       })

     .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

 
</script>
