<section class="content" style="background-color: #FFFFFF;" >
  <br>
  <div class="container-fluid">
    <div class="row clearfix">

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



        <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Transfer Alert's</h4>
           </div>
           <hr class="colorgraph"><br>
         </div>

         <div class="panel-body">

          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Comments</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th>Status</th>
                   <th class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($getworkflowdetail) == 0) { ?>
               <tbody>
                 <tr>
                   <td colspan="13" style="color: red;">
                     Record not found!!!
                   </td>
                 </tr>
               </tbody>
             <?php } else{ ?>

               <tbody>
                <?php
                 $i=0;
                foreach ($getworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername; ?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $value->Requestdate;?></td>
                    <td><?php echo $value->process_type ;?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>

                       <?php if ($value->status==1) { ?>
                        <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>  
                      <?php } ?>
                      <?php if ($value->status==5) {  ?>
                      <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Stafftransfer/add_clearance_certificate/".$value->transid;?>"> <i class="fas fa-certificate"></i></a>
                    <?php } ?>

                    <?php if ($value->status == 9 ) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here for handed over charge." href="<?php echo site_url()."handed_over_taken_over_transfer/index/".$value->transid;?>"> <i class="fas fa-praying-hands"></i></a>
                      <?php } ?>
                    <?php if (($value->status == 11 && $value->staffid == $this->loginData->staffid)) { ?>
                        <a class="btn btn-success btn-xs" title = "Click here for handed over charge." href="<?php echo site_url()."handed_over_taken_over_transfer/view/".$value->transid.'/'.$value->handed_over_id;?>"> <i class="fas fa-praying-hands"></i></a>
                      <?php } ?>
                       <?php if (($value->status == 11) && ($value->staffid != $this->loginData->staffid)) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to taking over charge." href="<?php echo site_url()."handed_over_taken_over_transfer/index/".$value->transid;?>"> <i class="fas fa-hands"></i></a>
                      <?php } ?>
                    <?php if ($value->status == 0) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to Joining report of newplace posting." href="<?php echo site_url()."Joining_report_of_newplace_posting/add_transfer/".$value->transid;?>">  <i class="fas fa-file-invoice"></i></a>
                      <?php } ?>
                      <?php if ($value->status == 17 || $value->status == 15) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to transfer claim expance view." href="<?php echo site_url()."transfer_claim_expense/transfer/".$value->transid;?>"> <i class="fas fa-dolly-flatbed"></i></a>
                        <!-- <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" onclick="approvemodalshowtransfer(<?php //echo $value->transid; ?>);">Approval</a> -->
                        <!-- <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php //echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>">Approval</a> -->
                      <?php } ?> 
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              <?php } ?>
              </table>
            </div>
          </div> 
        </div>
      </div>

        <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left"> Change Responsibility Alerts</h4>
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
        
              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablecampus" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>Name</th>
                       <th>Date</th>
                       <th>Old Designation</th>
                       <th>New Designation</th>
                       <th>Reason</th>
                       <th>Comments</th>
                       <th>Sender Name</th>
                       <th>Status</th>
                       <th style ="max-width:50px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                <?php if (count($getpromotionworkflowdetail) ==0) { ?>
                 <tbody>
                   <tr>
                     <td colspan="13">
                       Record not found!!!
                     </td>
                   </tr>
                 </tbody>
               <?php  }else{  ?>
                   <tbody>
                    <?php 
                    $i=0;
                    foreach ($getpromotionworkflowdetail as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                     
                      <td><?php echo $value->name;?> </td>
                      <td><?php echo $proposed_date_bdformat;?> </td>
                      <td><?php echo $value->olddesid;?></td>
                      <td><?php echo $value->newdesid;?></td>
                      <td><?php echo $value->reason;?> </td>
                      <td><?php echo $value->scomments;?></td>
                      <td><?php echo $value->sendername;?></td>
                      <td><?php echo $value->flag; ?></td>
                      <td class="text-center">
                        <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>
                          <?php if ($value->status==5) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here for handed over charge. 
                        " href="<?php echo site_url()."Changeresponsibility_handed_over_taken_over/index/".$value->transid;?>"><i class="fas fa-praying-hands"></i></a>

                          
                      
                       <?php  }if (($value->status == 7) && ($value->staffid != $this->loginData->staffid)) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to taking over charge." href="<?php echo site_url()."Changeresponsibility_handed_over_taken_over/index/".$value->transid;?>"> <i class="fas fa-hands"></i></a>
                      

                    <?php } if($value->status==9){?>

                         <a class="btn btn-warning btn-xs" title = "Click here to 
                         inter-office memo (IOM) for transfer submitted." href="<?php echo site_url()."Joining_report_of_cr/add/".$value->transid;?>"> <i class="fas fa-file-invoice"></i></a>

                         <?php }if ($value->status==4 || $value->status==6) { ?>
                         |<a class="btn btn-primary btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_personnel_approval/add/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>

                        <?php } ?>
                        
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                <?php } ?>
                </table>
              </div>
            </div>
            </div> 
          </div>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
          <div class="panel-heading">
            <div class="row">
             <h4 class="col-md-10 panel-title pull-left">Transfer & Change Responsibility Alert's</h4>
           </div>
           <hr class="colorgraph"><br>
         </div>

         <div class="panel-body">

          <div class="row" style="background-color: #FFFFFF;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
              <table id="tablecampus" class="table table-bordered table-striped wrapper">
                <thead>
                 <tr>
                   <th style ="max-width:50px;" class="text-center">S. No.</th>
                   <th>Emp code</th>
                   <th>Name</th>
                   <th>Old Office</th>
                   <th>Proposed Transfer Office</th>
                   <th>Proposed Transfer Date</th>
                   <th>Reason</th>
                   <th>Comments</th>
                   <th>Sender Name</th>
                   <th>Request Status</th>
                   <th>Request Date</th>
                   <th>Status</th>
                   <th class="text-center">Action</th>
                 </tr> 
               </thead>
               <?php if (count($gettransferpromationworkflowdetail) == 0) { ?>
               <tbody>
                 <tr>
                   <td colspan="13" style="color: red;">
                     Record not found!!!
                   </td>
                 </tr>
               </tbody>
             <?php } else{ ?>

               <tbody>
                <?php
                 $i=0;
                foreach ($gettransferpromationworkflowdetail as $key => $value) {
                  $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $i+1;?></td>
                    <td><?php echo $value->emp_code;?></td>
                    <td><?php echo $value->name;?></td>
                    <td><?php echo $value->officename;?></td>
                    <td><?php echo $value->newoffice;?> </td>
                    <td><?php echo $proposed_date_bdformat;?> </td>
                    <td><?php echo $value->reason;?></td>
                    <td><?php echo $value->scomments;?></td>
                    <td><?php echo $value->sendername; ?></td>
                    <td><?php echo $value->flag; ?></td>
                    <td><?php echo $value->Requestdate;?></td>
                    <td><?php echo $value->process_type ;?></td>
                    <td class="text-center">
                      <a class="btn btn-success btn-xs" title = "Click here to approvle this request submitted." href="<?php echo site_url()."Staff_history/index/".$value->transid;?>"><i class="fas fa-history"></i></a>

                       <?php if ($value->status==1) { ?>
                        <a title = "Click here to approvle this request submitted." class="btn btn-primary btn-xs" href="<?php echo site_url()."Stafftransfer/approve/".$value->staffid.'/'.$value->transid;?>"><i class="fas fa-thumbs-up"></i></a>
                      <?php } ?>
                      <?php if ($value->status==5) {  ?>
                      <a class="btn btn-warning btn-xs" title = "Click here to clearance certificate on submitted." href="<?php echo site_url()."Stafftransfer/add_clearance_certificate/".$value->transid;?>"> <i class="fas fa-certificate"></i></a>
                    <?php } ?>

                     <?php if ($value->status == 9 ) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here for handed over charge." href="<?php echo site_url()."handed_over_taken_over/index/".$value->transid;?>"> <i class="fas fa-praying-hands"></i></a>
                      <?php } ?>
                    <?php if ($value->status == 11 && $value->staffid == $this->loginData->staffid ) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here for handed over charge." href="<?php echo site_url()."handed_over_taken_over/view/".$value->transid.'/'.$value->handed_over_id;?>"> <i class="fas fa-praying-hands"></i></a>
                      <?php } ?>
                       <?php if ($value->status == 11 && $value->staffid != $this->loginData->staffid) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to taking over charge." href="<?php echo site_url()."handed_over_taken_over/index/".$value->transid;?>"> <i class="fas fa-hands"></i></a>
                      <?php } ?>
                    <?php if ($value->status == 13) { ?>
                        <!-- <a class="btn btn-warning btn-xs" title = "Click here to Joining report of newplace posting." href="<?php echo site_url()."Joining_report_of_newplace_posting/add_transfer/".$value->transid;?>"> JOINING REPORT AT NEW PLACE OF POSTING</a> -->
                      <?php } ?>
                      <?php if ($value->status == 17 || $value->status == 15) { ?>
                        <a class="btn btn-warning btn-xs" title = "Click here to transfer claim expance view." href="<?php echo site_url()."transfer_claim_expense/transfer/".$value->transid;?>"> <i class="fas fa-dolly-flatbed"></i></a>

                      <a class="btn btn-warning btn-xs" title = "Click here to View iom  on submitted." href="<?php echo site_url()."Staff_personnel_approval/add_iom/".$value->transid;?>"> <i class="fas fa-exchange-alt"></i></a>

                       <a class="btn btn-warning btn-xs" title = "Click here to View clearness  on submitted." href="<?php echo site_url()."Staff_approval/view_clearance_certificate/".$value->clearance_id;?>"> <i class="fas fa-certificate"></i></a>
                       
                       <a class="btn btn-warning btn-xs" title = "Click here to View Handed Over by staff  on submitted." href="<?php echo site_url()."Handed_over_taken_over/view/".$value->transid.'/'.$value->handed_over_id;?>"> <i class="fas fa-arrow-alt-circle-left"></i></a>

                       <a class="btn btn-warning btn-xs" title = "Click here to View Taken Over by TC  on submitted." href="<?php echo site_url()."Handed_over_taken_over/view/".$value->transid.'/'.$value->taken_over_id;?>"> <i class="fas fa-arrow-alt-circle-right"></i></a>
                      
                        <a class="btn btn-warning btn-xs" title = "Click here to View Joining Report by TC  on submitted." href="<?php echo site_url()."Joining_report_of_newplace_posting/view/".$value->joining_id;?>"> <i class="fas fa-file-invoice"></i></a>
                      <?php } ?> 

                      <?php if ($value->status == 0) { ?>
                        <a class="btn btn-primary btn-xs" title = "Click here to Joining report of newplace posting." href="<?php echo site_url()."Joining_report_of_newplace_posting/add/".$value->transid;?>"> <i class="fas fa-file-invoice"></i> </a>
                      <?php } ?>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              <?php } ?>
              </table>
            </div>
          </div> 
        </div>
      </div>
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
  <div class="panel-heading">
    <div class="row">
      <h4 class="col-md-7 panel-title pull-left">Seperation Approval List</h4>
    </div>
    <hr class="colorgraph"><br>
  </div>
  <div class="panel-body">
    <table id="staff_transfer_history" class="table table-bordered table-striped table-hover dataTable js-exportable">
      <thead>
        <tr>
          <th class="text-center" style="width: 50px;">S.NO</th>
          <th class="text-center">Seperate Date</th>
          <th class="text-center">Staff</th>
          <th class="text-center">Type</th>
          <th class="text-center">Sender</th>
          <th class="text-center">Recevier</th>
          <th class="text-center">Request Date</th>
          <th class="text-center">Status</th>
          <th class="text-center">Comment</th>
          <th class="text-center">Action</th>
        </tr>
      </thead>
      <?php if (count($staff_seperation)==0) { ?>
        <tbody>
         
        </tbody>
      <?php }else{ ?>
        <tbody>
          <?php
         // print_r($staff_seperation);
          $i=0; foreach($staff_seperation as $grow){ 
      //  print_r($grow);
            ?>
            <tr>
              <td class="text-center" style="width: 50px;"><?php echo $i+1; ?></td>
              <td class="text-center"><?php echo $this->gmodel->changedatedbformate($grow->proposeddate);?></td>
              <td class="text-center"><?php echo $grow->name; ?></td>
              <td class="text-center"><?php echo $grow->process_type; ?></td> 
              <td class="text-center"><?php echo $grow->sendername; ?></td> 
              <td class="text-center"><?php echo $grow->receivername; ?></td> 
              <td class="text-center"><?php echo $this->gmodel->changedateFormateExcludeTime($grow->Requestdate); ?> </td> 
              <td class="text-center"><span class="badge badge-pill badge-info col-md-12"><?php echo $grow->flag; ?> </span></td> 
              <td class="text-center"><?php echo $grow->scomments;?> </td> 
              <td class="text-center">
                
            
              <?php
              if($grow->status==11)
              { 
              ?>
              <a href="<?php echo base_url('Staff_sepemployeeexitform/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Staff Exitfoam" class="btn btn-success btn-sm"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a>
               <a href="<?php echo base_url('Handed_over_taken_over_seperation/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Staff Handed over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                
            <?php } 
             else if($grow->status>11)
              {
                ?>
                 <a href="<?php echo base_url('Staff_sepemployeeexitform/index/'.$grow->transid);?>" data-toggle="tooltip" title = "Staff Exitfoam" class="btn btn-success btn-sm"><i class="fa fa-sign-out-alt" aria-hidden="true"></i></a> 
                <a href="<?php echo base_url('Handed_over_taken_over_seperation/view/'.$grow->transid.'/'.$grow->tbl_handed_id);?>" data-toggle="tooltip" title = "Staff Handed over" class="btn btn-success btn-sm"><i class="fas fa-arrow-alt-circle-left"></i></a>
                  
                <?php 
              }
           

            ?>

                
              </td>
            </tr>
            <?php  $i++; } ?>
          </tbody>
        <?php  } ?>
      </table>
    </div>
  </div>
</div>


       
<?php //} } ?>
</section>
   <!-- Modal -->
  <div class="container">
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <form action="<?php echo site_url(); ?>Staff_approval/approvemodal" id="acceptancetcform" name="acceptancetcform" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <input type="hidden" id="receiverstaffid" name="receiverstaffid" value="">
               <label for="Name">Notes <span style="color: red;" >*</span></label>
               <textarea class="form-control" data-toggle="" id="comments" name="comments" placeholder="Enter Comments"  require> </textarea>
                <?php echo form_error("comments");?>
              </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetcmodal();">
          </div>
        </div>
       </form> 
      </div>
    </div>
  </div> 
  <!-- Modal -->

   <!-- Modal Transfer Expense claim-->
  <div class="container">
    <div class="modal fade" id="myModaltransfer" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <form action="<?php echo site_url(); ?>Staff_approval/transferapprovemodal" id="acceptancetcform_transfer" name="acceptancetcform_transfer" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Modal Header</h4> -->
          </div>
          <div class="modal-body">          <!-- <div class="row"> -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <input type="hidden" id="receiverstaffid_transfer" name="receiverstaffid_transfer" value="">
             <label for="Name">Notes <span style="color: red;" >*</span></label>
             <textarea class="form-control" data-toggle="" id="comments_transfer" name="comments_transfer" placeholder="Enter Comments"  require> </textarea>
              <?php echo form_error("comments_transfer");?>
            </div>
          </div>
          <!-- </div> -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="saveandsubmit" value="Save" class="btn btn-success btn-sm" onclick="acceptancetransfermodal();">
          </div>
        </div>
       </form> 
      </div>
    </div>
  </div> 
  <!-- Modal -->

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();  
    $('#tablecampus').DataTable(); 
  });
  function approvemodalshow(staffid){
    document.getElementById("receiverstaffid").value = staffid;
    $('#myModal').removeClass('fade');
    $("#myModal").show();
  }
  function approvemodalshowtransfer(staffid){
    document.getElementById("receiverstaffid_transfer").value = staffid;
    $('#myModaltransfer').removeClass('fade');
    $("#myModaltransfer").show();
  }
      function acceptancetcmodal(){
        var comments = document.getElementById('comments').value;
        if(comments.trim() == ''){
          $('#comments').focus();
        }
        if(comments.trim() !=''){
          document.forms['acceptancetcform'].submit();
        }
      }

      function acceptancetransfermodal(){
        var comments = document.getElementById('comments_transfer').value;
        if(comments.trim() == ''){
          $('#comments_transfer').focus();
        }
        if(comments.trim() !=''){
          document.forms['acceptancetcform_transfer'].submit();
        }
      }
</script>  


<script type="text/javascript">
 $(document).ready(function(){
  $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
});
</script>
<script type="text/javascript">
  function yesnoCheck() {

    if(document.getElementById('search').value =='Monthly')
    {
      document.getElementById('ifYes').style.display = 'block';
    }
    else
    {
     document.getElementById('ifYes').style.display = 'none';
   }
 }
</script>
<script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>