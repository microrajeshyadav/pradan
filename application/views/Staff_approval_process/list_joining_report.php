<section class="content" style="background-color: #FFFFFF;" >
  <?php  //foreach ($role_permission as $row) { if ($row->Controller == "Campus" && $row->Action == "index"){ ?>
  <br>
  <div class="container-fluid">

   
      <div class="panel thumbnail shadow-depth-2 listcontainer" >
    <div class="panel-heading">
      <div class="row">
       <h4 class="col-md-10 panel-title pull-left">Joining Report form List</h4>

      
    </div>
      <hr class="colorgraph"><br>
    </div>
      <div class="panel-body">
        <?php 
        $tr_msg= $this->session->flashdata('tr_msg');
        $er_msg= $this->session->flashdata('er_msg');

        if(!empty($tr_msg)){ ?>
        <div class="content animate-panel">
          <div class="row">
            <div class="col-md-12">
              <div class="hpanel">
                <div class="alert alert-warning alert-dismissable alert1"> <i class="fa fa-check"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <b> <?php echo $this->session->flashdata('tr_msg');?>. </b></div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else if(!empty($er_msg)){?>
            <div class="content animate-panel">
              <div class="row">
                <div class="col-md-12">
                  <div class="hpanel">
                    <div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <b><?php echo $this->session->flashdata('er_msg');?>.</b> </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <div class="row" style="background-color: #FFFFFF;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-color: white;">
                  <table id="tablecampus" class="table table-bordered table-striped wrapper">
                    <thead>
                     <tr>
                       <th style ="max-width:50px;" class="text-center">S. No.</th>
                       <th>Transfer no</th>
                       <th>Name</th>
                        <th style ="max-width:50px;" class="text-center">Action</th>
                     </tr> 
                   </thead>
                   <tbody>
                    <?php
                    //print_r($getstaffjoiningreport); die; 
                    $i=0;
                    foreach ($getstaffjoiningreport as $key => $value) {
                      $proposed_date_bdformat = $this->gmodel->changedatedbformate($value->proposeddate);
                     ?>
                     <tr>
                      <td class="text-center"><?php echo $i+1;?></td>
                      <td><?php echo $value->transferno;?></td>
                      <td><?php echo $value->name;?></td>
                       <td class="text-center">
                        <a class="btn btn-warning btn-xs" title = "Click here to joining report at New place of posting submitted." href="<?php echo site_url()."Staff_approval/add_joining_report/".$value->joinid;?>">edit</a>
                     </td>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div> 
          </div>
        </div>
      </div>   
      <?php //} } ?>
    </section>

    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        $('#tablecampus').DataTable(); 
      });
    </script>  


    <script type="text/javascript">
     $(document).ready(function(){
      $('.datepicker').bootstrapMaterialDatePicker({ format: 'MM/YYYY', time: false });
    });
  </script>
  <script type="text/javascript">
    function yesnoCheck() {

      if(document.getElementById('search').value =='Monthly')
      {
        document.getElementById('ifYes').style.display = 'block';
      }
      else
      {
       document.getElementById('ifYes').style.display = 'none';
     }
   }
 </script>
 <script>
  function confirm_delete() {
    var r = confirm("Do you want to delete this Campus?");
    if (r == true) {
      return true;
    } else {
      return false;
    }

  }
</script>