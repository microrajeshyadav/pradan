

<section class="content">
	<?php foreach ($role_permission as $row) { if ($row->Controller == "Staff_list" && $row->Action == "index"){ ?>
		<br>
		<div class="container-fluid">
			<?php 
			$tr_msg= $this->session->flashdata('tr_msg');
			$er_msg= $this->session->flashdata('er_msg');

			if(!empty($tr_msg)){ ?>
				<div class="content animate-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						</div>
					</div>
				<?php } else if(!empty($er_msg)){?>
					<div class="content animate-panel">
						<div class="row">
							<div class="col-md-12">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>				
					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="panel thumbnail shadow-depth-2 listcontainer" >
								<div class="panel-heading">
									<div class="row">
										<h4 class="col-md-10 panel-title pull-left">Staff List</h4>
									</div>
									<hr class="colorgraph"><br>
								</div>
								<div class="panel-body">
									<form action="" method="post">
										<div class="row bg-light" style="padding: 20px; margin-bottom: 20px;">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px;">
												<h5>Search Filters:</h5>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
												Status: 
											</div>	
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">

												<?php 

												$Sarray =  array( 1 => 'Active' , 0 => 'Inactive'  ); 

												$options = array('' => 'Select Status');

												foreach($Sarray as $key => $value) {
													$options[$key] = $value;
												}

												echo form_dropdown('ddstatus', $options, set_value('ddstatus'), 'class="form-control" id="ddstatus" data-toggle="tooltip" title=" Select Status !" required');
												?>


											</div>
											<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
												Office: 
											</div>
											<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
												<select class="form-control" name="officeid" id="officeid" required="required">
													<option value="0">-- All --</option>
													<?php foreach($officelist as $val){
														if($officeid == $val->officeid){
															?>
															<option value="<?php echo $val->officeid;?>" SELECTED><?php echo $val->officename;?> </option>
														<?php  }else{ ?>
															<option value="<?php echo $val->officeid;?>" ><?php echo $val->officename;?> </option>

														<?php } } ?>	
													</select>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
													<button type="submit" class="btn btn-info btn-sm">
														<i class="fa fa-search" aria-hidden="true"></i>


													</button>
												</div>
											</div>
										</form>

										
										<table id="table" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable no-footer dtr-inline collapsed" role="grid">
											<thead>
												<tr>
													<th  class="text-center" style="">#</th>

													<th style="width:50px;">Staff</th>
													<th >Designation </th>
													<th> Official Email Id  </th>
													<th> Gender</th>
													<th> Office </th>
													<th> Date of Joining</th>
													<th>Active </th>
													<th>Profile Status </th>
													<th>C.E.A.</th>
													<?php  if ($this->loginData->RoleID==16 || $this->loginData->RoleID==10 || $this->loginData->RoleID==17) {
														?>
														<th id="hraction" class="text-left" style="width: 200px;">Initiate</th>
													<?php }  ?>
													<?php if ($this->loginData->RoleID==17 || $this->loginData->RoleID==10) {
														?>
														<th id="peraction" class="text-center" style="width: 90px;">Action</th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
												<?php
												$i=0; foreach($staff_details as $row){ 
													?>

													<tr>
														
														<td class="text-center"><input type="hidden" id="staff_id" value="<?php echo $row->staffid;?>" name="staff_id"><?php echo $i+1; ?></td>
														<td ><?php echo $row->name ." - ". $row->emp_code; ?></td>
														<td ><?php echo $row->desname; ?></td>
														<td >
															<?php if(!empty($row->emailid))
															{ ?>
																<?php echo $row->emailid; ?>
															<?php } 
															else { ?>
																<a title = "Click here to assign official emailid."  data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm text-white" onclick="officialid(<?php echo $row->staffid; ?>);"><?php echo "Not Available"; ?>  </a>
															<?php	}?>

														</td>		    
														<td ><?php echo $row->gender; ?></td>
														<td ><?php echo $row->officename; ?></td>
														<td ><?php echo $this->gmodel->changedatedbformate($row->doj); ?></td>
														<td class="text-center"><?php  if ($row->status==1) {
															echo '<span class = "badge badge-success badge-pill">Yes</span>';
														}else{
															echo '<span class = "badge badge-danger badge-pill">NO</span>';
														} ?></td>
														<td>
															<?php 
															if ($this->loginData->RoleID == 10 && $row->flag=="Approved") { ?>
																<button type="button" name="approved" value="<?php echo $row->staffid; ?>" id="approved" class="btn btn-success text-center btn-xs" >Unlocked</button>
															<?php } elseif($row->flag=="Approved"){
																echo '<span class = "badge badge-success badge-pill" style="width:60px;">'.$row->flag.'</span>';	
															}
															elseif($row->flag=="Save"){
																echo '<span class = "badge badge-dark badge-pill" style="width:60px;">'.$row->flag.'</span>';	
															}
															elseif($row->flag=="Submitted"){
																echo '<span class = "badge badge-info badge-pill" style="width:60px;">'.$row->flag.'</span>';	
															}
															elseif($row->flag=="Rejected"){
																echo '<span class = "badge badge-danger badge-pill" style="width:60px;">'.$row->flag.'</span>';	
															}
															else{
																echo $row->flag;
															}
															?>
														</td>
														<td>
															<?php 
															if(!empty($row->children_allowance))
															{ ?>
																<input class="form-control" type="text" name="" disabled value="<?php echo $row->children_allowance; ?>">

															<?php }
															else{ ?>
																<a title = "Click here for changes of Children Education Allowance"  data-toggle="modal" data-target="#ModalCEA" class="btn btn-primary btn-sm text-white" onclick="childallowance(<?php echo $row->staffid; ?>)"><i class="fas fa-rupee-sign"></i></a>
															<?php } ?>
															
														</td>
														<td class="text-center">

															<?php 

															if ($row->designation ==13 && $this->loginData->RoleID==16) { ?>
																<a href="<?php echo base_url().'Placementchangeletter/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff placement change letter.">PC</a>|
																<a href="<?php echo base_url().'Fieldguide/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff field guide change letter.">FGC</a>



															<?php 	}

															if ($row->status==1 && $row->seperation_status !=1  && $row->transCRstatus !=1 && $row->transstatus !=1&& $row->PromotionStatus !=1 && $row->designation !=13 and $row->transid !=null) {

																if ($this->loginData->RoleID==16 || $this->loginData->RoleID==10 || $this->loginData->RoleID==17  ) { 

																	$staffprobationdetails = $this->Staff_list_model->getProbationStatus($row->staffid);
																	?>

																	<a href="<?php echo base_url().'stafftransfer/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff transfer.">T</a>|

																	<a href="<?php echo base_url().'Staffpromotion/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff promotion.">CR
																	</a>|

																	<a href="<?php echo base_url().'Stafftransferpromotion/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff transfer & promotion.">T.CR</a>|
																	<?php if($row->seperation_status == 0){ ?>
																		<a href="<?php echo base_url().'Staff_seperation/add/'.$row->staffid;?>" style="padding : 4px;" data-toggle="tooltip" title="Click here to initiate staff sepration.">S </a>
																	<?php } } } elseif ($row->designation !=13 && $this->loginData->RoleID!=16) { ?>
																		<span class="badge badge-pill btn-danger"> One of operation in process.
																		</span>

																	<?php }   ?>

																</td>


																<?php if ($this->loginData->RoleID==17 || $this->loginData->RoleID==10) { ?>

																	<td class="text-left">
																		<?php if ($this->loginData->RoleID==10) { ?>
																			<a href="<?php echo site_url().'Stafffullinfo/edit/'.$row->staffid;?>"  Class="btn btn-info btn-sm" data-toggle="tooltip" title="Click here to view this Staff Profile."><i class="fa fa-edit" aria-hidden="true" title="Click here to edit Staff Profile." data-toggle="tooltip"></i></a>
																		<?php }  ?>

																		<?php if ($row->flag=='Approved') { ?>
																			<a href="<?php echo site_url().'Stafffullinfo/view/'.$row->staffid;?>" Class="btn btn-info btn-sm" data-toggle="tooltip" title="Click here to view this Staff Profile."><i class="fa fa-eye" aria-hidden="true" title="Click here to verify this Staff Profile." data-toggle="tooltip"></i></a>
																		<?php } else if ($row->flag=='Submitted') { ?>
																			<a href="<?php echo site_url().'Stafffullinfo/view/'.$row->staffid;?>" Class="btn btn-info btn-sm" data-toggle="tooltip" title="Click here to verify this Staff Profile."><i class="fa fa-eye" aria-hidden="true" title="Click here to verify this Staff Profile." data-toggle="tooltip"></i></a>
																		<?php }else{ ?>

																			<a href="<?php echo site_url().'Stafffullinfo/view/'.$row->staffid;?>" Class="btn btn-info btn-sm" data-toggle="tooltip" title="Click here to view this Staff Profile."><i class="fa fa-eye" aria-hidden="true" id="usedbatchid"></i></a>											


																		<?php } ?>
																		<?php 
																		if($row->joiningandinduction==NULL)
																		{

																		}

																		else if($row->joiningandinduction!=NULL && $row->candidateid!=NULL && $row->status==1)
																		{
																			?>
																			<a Class="btn btn-info btn-sm" href="<?php echo site_url('Employee_particular_form/index/'.$row->staffid.'/'.$row->candidateid)?>" id="statedl" data-toggle="tooltip"  title="Click here to view Staff Joining  induction Forms"><i class="fa fa-universal-access" aria-hidden="true"></i>

																			</a>


																		<?php }?>
																	<?php } ?>

																</tr>

																<?php $i++; } ?>
															</tbody>
														</table>


														<div class="container">
															<div id="ModalCEA" class="modal fade" role="dialog">
																<div class="modal-dialog">

																	<form name="callowancee" id="callowancee" action="" method="post">
																		<div class="modal-content">
																			<div class="modal-header">
																				<h4><label for="Name">Children Education Allowance</label>  </h4><button type="button" class="close" data-dismiss="modal">&times;</button>
																			</div>
																			<div class="modal-body"> 
																				<div class="row p-2">
																					
																					<div class="form-inline"> 
																						<input type="hidden" class="form-control" id="staff2" name="staff2" value="">
																						<input type="checkbox" name="chkCEA" id="chkCEA" value="1" class="form-control mr-2">  
																						<strog><label>Applicable for Childern Education Allowance</label></strong>
																						</div>
																					</div>
																					<div class="row p-2">

																						<div class="form-group"> 
																							<label>No. Of Children</label>
																							<input type="Number" name="txtNoofchildren" id="txtNoofchildren" value="" class="form-control" min="0" max="9" maxlength="1"> 
																						</div>

																					</div>
																					<div class="row p-2">
																						<div class="form-group"> 
																							<label>Children Education Allowance</label>
																							<input type="text" name="children_allowance" id="children_allowance" value="" class="form-control text-right"> 
																						</div>
																					</div>
																					<div class="row p-2">
																						<div class="form-group"> 
																							<label>Effective From</label>
																							<input type="text" name="dateofeffective" id="dateofeffective" value="" class="form-control datepicker"> 
																						</div>
																					</div>
																				</div>
																				<div class="modal-footer">
																					<input type="hidden" name="children" id="btnsubmit2" value="allowance" class="btn btn-success btn-sm">
																					<input type="button" name="btnsubmit1" id="btnsubmit1" value="Save" class="btn btn-success btn-sm" onclick="callowance();">

																				</div>
																			</div> 

																		</form>
																	</div>
																</div>

																<div id="myModal" class="modal fade" role="dialog">
																	<div class="modal-dialog">

																		<form name="officestaffid" id="officestaffid" action="" method="post">
																			<div class="modal-content">
																				<div class="modal-header">
																					<label for="Name">Enter official email id <span style="color: red;" > *</span></label> <button type="button" class="close" data-dismiss="modal">&times;</button>
																				</div>
																				<div class="modal-body"> 
																					<div class="row p-2">
																						<input type="hidden" class="form-control" id="staff1" name="staff1" value="">
																						<div class="form-group">


																							<input type="text" class="form-control" placeholder="Enter official email id" id="emailid" name="emailid" required="required" maxlength="40" >
																						</div>

																					</div>
																				</div> 
																				<div class="modal-footer">

																					<input type="submit" name="btnsubmit" id="btnsubmit" value="Save" class="btn btn-success btn-sm"  onclick="officialemailid();">

																				</div>
																			</div>
																		</form>
																	</div>
																</div>
															</div>


														</div>
													</div>
												</div>
											</div>

										</div>

									<?php } } ?>
								</section>

								<script type="text/javascript">
									$('.datepicker').datepicker({
											dateFormat: 'dd/mm/yy',
											minDate: '-3m',
											changeMonth: true,
											changeYear: true,
										});

									function officialid(staffid){
										$('#staff1').val(staffid);
										$('#myModal').removeClass('fade');									
										$("#myModal").show();
									}
									function officialemailid(){
										var emailid = document.getElementById('emailid').value;
										if(emailid == ''){
											$('#emailid').focus();
										}
										if(emailid !=''){
											document.forms['officestaffid'].submit();
										}
									}


									function childallowance(staffid){
										$('#staff2').val(staffid);
										$('#myModal').removeClass('fade');									
										$("#ModalCEA").show();
									}
									function callowance(){

										var chkCEA = document.getElementById('chkCEA').value;
										var txtNoofchildren = document.getElementById('txtNoofchildren').value;
										var children_allowance = document.getElementById('children_allowance').value;
										var dateofeffective = document.getElementById('dateofeffective').value;

										if(dateofeffective == ''){
											$('#dateofeffective').focus();
										}
		
										if(children_allowance == ''){
											$('#children_allowance').focus();
										}
										
										if(txtNoofchildren == ''){
											$('#txtNoofchildren').focus();
										}

										if(txtNoofchildren !='' && children_allowance !='' && dateofeffective !=''){
											document.forms['callowancee'].submit();
										}
									}



									$(document).ready(function() {

										$('[data-toggle="tooltip"]').tooltip(); 

										var table = $('#table').DataTable();


										$('#example tbody').on( 'click', 'tr.group', function () {
											var currentOrder = table.order()[0];
											if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
												table.order( [ groupColumn, 'desc' ] ).draw();
											}
											else {
												table.order( [ groupColumn, 'asc' ] ).draw();
											}
										} );

										$("#approved").on('click', function(){

											var staff_id = $("#approved").val();

											var data;
											data=staff_id;

											$.ajax({
												data:{staff_id:staff_id},
												url: '<?php echo site_url(); ?>Ajax/staff_approved/',
												type: 'POST'

											})
											.done(function(data) {
												alert("Your Account is unlocked");
												window.location = "<?php echo current_url();?>";

											})

										});
									});


								</script>