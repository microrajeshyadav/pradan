<?php 

/**
* Dashboard Model
*/
class Change_responsibilitypart_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

	public function campusList()
	{
		$sql = "SELECT * FROM `mstcampus`";
		$res = $this->db->query($sql)->result();

		return $res;
	}



    public function getBatch()
    {
    
        try{

            $sql = "SELECT a.*,b.financialyear FROM `mstbatch` as a
				   inner join `mstfinancialyear` as b ON b.id = a.financial_year
                  WHERE a.`isdeleted`='0'";

            $result = $this->db->query($sql)->result();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }


    

    /**
   * Method get_staff_sep_detail() get the staff detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */



    public function get_staff_changeresponsibility_detail($token)
   {
      try
      {

      $sql="SELECT
    c.name,
    a.date_of_transfer AS changedate,
    a.staffid,
    b.desname AS changedesig,
    z.name AS reportingname,
    c.emp_code,
    st.date_of_transfer AS joiningdate,
    a.trans_flag,
    a.reason,
    a.createdon,
    lp.officename AS newoffice
FROM
    staff_transaction AS a
INNER JOIN `staff` AS c
ON
    a.staffid = c.staffid
INNER JOIN staff_transaction AS st
ON
    st.staffid = c.staffid AND st.trans_status = 'Promotion'
INNER JOIN `msdesignation` AS b
ON
    a.new_designation = b.desid

INNER JOIN `lpooffice` AS lp
ON
    a.new_office_id = lp.officeid
INNER JOIN `staff` AS z
ON
    c.reportingto = z.staffid

           WHERE  a.id = $token"; 
 
         //$query=$this->db->get();
         return  $this->db->query($sql)->row();

    }

        catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}


    /**
   * Method get ED Name () get the staff Ed Name.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getEDName()
    {
    
        try{

            $sql = "SELECT name as executivedirectorname FROM `Staff` WHERE  `staffid` = 15";

            $result = $this->db->query($sql)->row();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }




	
}