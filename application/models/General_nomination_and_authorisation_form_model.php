<?php 

/**
* General Nnomination And Authorisation Form Model Class
*/
class General_nomination_and_authorisation_form_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
if ($date=='//') {
  $date = NULL;
 }
  return $date;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);

 if ($date=='//') {
  $date = NULL;
 }

  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


	/**
   * Method getSelectedCandidate() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSelectedCandidate()
  {
    
    try{

         $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";
        
        $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateWithAddressDetails() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateWithAddressDetails($token)
  {
    
    try{

           $sql = 'SELECT `st`.name as staff_name,`st`.dob,`st`.emp_code,`lp`.officename,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname, `tbl_candidate_communication_address`.permanentstreet, `tbl_candidate_communication_address`.permanentcity, `tbl_candidate_communication_address`.permanentstateid, `tbl_candidate_communication_address`.permanentdistrict, `tbl_candidate_communication_address`.permanentpincode, b.`name` as permanentstatename, `dis`.name FROM `tbl_candidate_registration` 
         left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid

          left join `state` as b ON `tbl_candidate_communication_address`.permanentstateid = b.`statecode` 
     left join `district` as dis ON `tbl_candidate_communication_address`.permanentdistrict = dis .districtid
     left JOIN `lpooffice` as lp on `tbl_candidate_registration`.teamid = lp.`officeid`
     left JOIN `staff` as st on `tbl_candidate_registration`.candidateid = st.`candidateid`

                Where `tbl_candidate_registration`.candidateid ='.$token.'';  
               // die();

                // echo $sql; die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneralnominationform() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralnominationform($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_general_nomination_and_authorisation_form`         
                Where `tbl_general_nomination_and_authorisation_form`.id ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWitnessDeclaration()
  {

    try{
             $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name 
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  )  
  AND staff_transaction.`new_designation` IN(4) 
ORDER BY 
  `staff`.name ";

  //  -- AND staff_transaction.`new_office_id` = '$id'
            
            $result = $this->db->query($sql)->result();

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getViewGeneralnominationform() get View General Nomination  Authorisation Form Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewGeneralnominationform($token)
  {
    
    try{

           $sql = 'SELECT * FROM `tbl_general_nomination_and_authorisation_form`         
                Where `tbl_general_nomination_and_authorisation_form`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Count Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountNominee($token)
  {
    
    try{
        
        $this->db->where('general_nomination_id', $token);
        $result = $this->db->count_all_results('tbl_nominee_details');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Select Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT  tbl_nominee_details.general_nomination_id,tbl_nominee_details.nominee_name,tbl_nominee_details.nominee_relation,tbl_nominee_details.nominee_age,tbl_nominee_details.nominee_address,sysrelation.relationname,sysrelation.status,sysrelation.id 
          FROM `tbl_nominee_details`  
                 left join sysrelation on   `tbl_nominee_details`.nominee_relation =  `sysrelation`.id     
                Where `tbl_nominee_details`.general_nomination_id ='.$token.'';  
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



 /**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $data  = array('Mother' => 'Mother','Father' => 'Father', 'Spouse' => 'Spouse', 'Child' => 'Child');
      $this->db->select('*');

      $this->db->from('sysrelation');
      $this->db->where('isdeleted', 0);
      $this->db->where('status', 0);
      $this->db->where_in('relationname', $data);
      //$this->db->get();
     // echo $this->db->last_query();
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }




/**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
     $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->row();
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

       $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." "; 
       $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->result()[0];
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }

   public function office_name()
  {
     try{

      $sql = "select * from tbl_table_office";
       $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->row();
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


public function tc_data($teamid)
{
  try{

      $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emailid,
   `staff`.`designation`
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '$teamid' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
ORDER BY
  `staff_transaction`.staffid  DESC  LIMIT 0,1
 ";
      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

}