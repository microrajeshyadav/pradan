<?php 

/**
* Master  Model
*/
class Staffleaveapplied_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{

	}




public function staffleavedetailsforMonthlyClosing($laeve_array)
  {
    //print_r($laeve_array);
    
    try{
      // print_r($laeve_array); die;
            extract($laeve_array);            
if ($From_date!='' && $To_date!='')                
 { 

   $sql= "SELECT s.new_office_id, lt.`From_date`, lt.`To_date`, lt.`Noofdays`, case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2  then 'Rejected' when lt.`status` = 3 then 'Approved' when lt.`status` = 4 then 'Reverse' end status,  DATE_FORMAT(lt.`appliedon`,'%d/%m/%Y') appliedon, lt.`reason`, l.`Ltypename`, lt.status as stage, CONCAT(s.emp_code,'-',s.name) as employee,msdesignation.desname, s.emp_code, mlc.stage as mlcstage, mlc.mlcid FROM `Staff` AS s LEFT JOIN `trnleave_ledger` AS lt
ON lt.`Emp_code` = s.`emp_code` AND (lt.`status` = 3 OR lt.`status` = 1 ) AND lt.`Leave_transactiontype` = 'DR'  AND s.status = ". $status." AND lt.`From_date` <= '".$To_date."' AND lt.`To_date` >= '".$From_date."'
LEFT JOIN `msleavetype` AS l ON lt.`Leave_type` = l.`Ltypeid`
INNER JOIN `lpooffice` AS lp ON s.new_office_id = lp.officeid
INNER JOIN msdesignation ON s.designation = msdesignation.desid
left JOIN monthly_leave_closing AS mlc
ON
    mlc.emp_code = s.emp_code AND mlc.fromdate<='".$To_date."' AND mlc.todate >='".$From_date."'
WHERE
    lp.officeid = CASE WHEN ".$officeid." = 0 THEN lp.officeid ELSE ".$officeid." END and desid != 13";

// echo $sql; die;

}elseif ($From_date=='' || $To_date=='')
{ 


 $sql = "SELECT lt.`From_date`, lt.`To_date`,
lt.`Noofdays`, case when lt.`status` =
1 then 'Request Submitted' when lt.`status` = 2  then 'Rejected' when lt.`status` = 3 then 'Approved' when
lt.`status` = 4 then 'Reverse' end status, DATE_FORMAT(lt.`appliedon`,'%d/%m/%Y') appliedon, lt.`reason`,
 l.`Ltypename`, lt.status as stage, CONCAT(s.emp_code,'-',s.name)
as employee, msdesignation.desname,s.emp_code, mlc.stage as mlcstage  FROM `trnleave_ledger` as lt LEFT JOIN
`msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` INNER JOIN `Staff` as s on
lt.`Emp_code` = s.`emp_code` INNER JOIN `lpooffice` AS lp ON s.new_office_id =
lp.officeid  INNER JOIN msdesignation ON s.designation = msdesignation.desid

LEFT JOIN monthly_leave_closing as mlc on mlc.emp_code=s.emp_code
WHERE (lt.`status` = 3 or lt.`status` = 1) AND lt.`Leave_transactiontype` ='DR' AND s.status = ". $status ." AND
lp.officeid = case when ".$officeid." =0 then lp.officeid else ".$officeid."
end  ORDER BY appliedon DESC";                   
}
// echo $sql;
// die;
        
               return $res = $this->db->query($sql)->result();

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

	public function staffleavedetails($laeve_array)
	{
		//print_r($laeve_array);
		
		try{
			 // print_r($laeve_array); die;
            extract($laeve_array);            
if ($From_date!='' && $To_date!='')                
 { 

   $sql= "SELECT lt.`From_date`, lt.`To_date`, lt.`Noofdays`, case when lt.`status` =
1 then 'Request Submitted' when lt.`status` = 2  then 'Rejected' when lt.`status` = 3 then 'Approved' when
lt.`status` = 4 then 'Reverse' end status,  DATE_FORMAT(lt.`appliedon`,'%d/%m/%Y') appliedon, lt.`reason`,
l.`Ltypename`, lt.status as stage, CONCAT(s.emp_code,'-',s.name) as employee,msdesignation.desname, s.emp_code, mlc.filename ,mlc.stage as mlcstage , mlc.mlcid
FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` =
l.`Ltypeid` INNER JOIN `Staff` as s on lt.`Emp_code` = s.`emp_code` INNER JOIN
`lpooffice` AS lp ON s.new_office_id = lp.officeid  INNER JOIN msdesignation
ON s.designation = msdesignation.desid 
left JOIN monthly_leave_closing as mlc on mlc.emp_code=s.emp_code  
WHERE (lt.`status` = 3 AND lt.`Leave_transactiontype` ='DR' ) 
AND s.status = ". $status ." AND lp.officeid= case when ".$officeid." =0 then
lp.officeid else ".$officeid." end and lt.`From_date`<='".$To_date."' AND
lt.`To_date`>='".$From_date."' GROUP BY lt.id ORDER BY appliedon DESC ";

  //echo "data2".$sql; die;

}elseif ($From_date=='' || $To_date=='')
{ 


 $sql = "SELECT lt.`From_date`, lt.`To_date`,
lt.`Noofdays`, case when lt.`status` =
1 then 'Request Submitted' when lt.`status` = 2  then 'Rejected' when lt.`status` = 3 then 'Approved' when
lt.`status` = 4 then 'Reverse' end status, DATE_FORMAT(lt.`appliedon`,'%d/%m/%Y') appliedon, lt.`reason`,
 l.`Ltypename`, lt.status as stage, CONCAT(s.emp_code,'-',s.name)
as employee, msdesignation.desname,s.emp_code, mlc.stage as mlcstage  FROM `trnleave_ledger` as lt LEFT JOIN
`msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` INNER JOIN `Staff` as s on
lt.`Emp_code` = s.`emp_code` INNER JOIN `lpooffice` AS lp ON s.new_office_id =
lp.officeid  INNER JOIN msdesignation ON s.designation = msdesignation.desid

LEFT JOIN monthly_leave_closing as mlc on mlc.emp_code=s.emp_code
WHERE (lt.`status` = 3 AND lt.`Leave_transactiontype` ='DR' ) AND s.status = ". $status ." AND
lp.officeid = case when ".$officeid." =0 then lp.officeid else ".$officeid."
end  ORDER BY appliedon DESC";    
        
}
				
//echo  "data1".$sql; die();               
			return $res = $this->db->query($sql)->result();

		}catch(Exception $e){
			print_r($e->getMessage());die();
		}
	}



	public function staffemp_code($staff)
	{
		
		
		try{
			
			$sql = "SELECT emp_code,new_office_id from staff where staffid=$staff"; 
			return $res = $this->db->query($sql)->row();
			}catch(Exception $e){
			print_r($e->getMessage());die();
		}
	}

  // public function monthly_details($fromdate,$todate)
  // {
    
    
  //   try{
      
  //     $sql= "select * from monthly_leave_closing where fromdate>='$fromdate' AND todate<=$todate";
  //  echo $sql;
  //  die; 
  //     return $res = $this->db->query($sql)->row();
  //     }catch(Exception $e){
  //     print_r($e->getMessage());die();
  //   }
  // }
	public function getoffice()
	{
		try{
			

			$sql = "SELECT * FROM `lpooffice` WHERE  `closed` ='No' ORDER BY officetype, officename"; 
			return $res = $this->db->query($sql)->result();

		}catch(Exception $e){
			print_r($e->getMessage());die();
		}
	}


	public function getrequestedleave($token)
  {
    try{     
$sql="SELECT
  lt.`From_date`,
  lt.`id`,
  lt.`To_date`,
  lt.`Noofdays`,
  -- md.desname AS sender_des,
  SUBSTRING(st.name, 1, LOCATE(' ',st.name)) AS name,
  st.staffid,
  lp.officename,
  d.name as districtname,
  s.name as statename,
  st.gender,
  st.emailid,
  st.marital_status,
  lt.`Emp_code`,
  lt.`reason`,
  l.`Ltypename`,
  case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2   
then 'Rejected' when lt.`status` = 3 then 'Approved' when lt.`status` = 4 then 'Reverse' end status,
lt.status as stage
FROM
  `trnleave_ledger` AS lt
INNER JOIN
  `msleavetype` AS l ON lt.`Leave_type` = l.`Ltypeid` 
INNER JOIN
  staff AS st ON lt.`Emp_code` = st.emp_code
-- INNER JOIN
--   tbl_workflowdetail AS tw ON st.staffid = tw.staffid and lt.id=tw.r_id and lt.status = tw.flag
 INNER JOIN
  lpooffice as lp on st.new_office_id = lp.officeid
-- INNER JOIN
--   msdesignation AS md ON st.designation = md.desid
LEFT JOIN 
state as s on  lp.state = s.statecode 
LEFT JOIN 
District as d on  lp.district = d.districtid
WHERE
st.`emp_code` = $token AND lt.status = 3
ORDER BY
  lt.appliedon DESC";

          return $res = $this->db->query($sql)->row();

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }


  public function getEDid()
{

  try{

    $sql = "SELECT mstuser.staffid as edid,mstuser.UserFirstName,mstuser.UserMiddleName,mstuser.UserLastName  FROM mstuser LEFT JOIN staff as s on mstuser.staffid=s.staffid  
    WHERE s.designation = 2 AND mstuser.RoleID= 18 AND mstuser.IsDeleted = 0 AND s.status =1";
    // echo $sql; die;

    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}

public function personal_email()
  {
    
    try{

        $sql = "SELECT  staffid FROM  mstuser WHERE  RoleID=17 and IsDeleted='0'";
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->row();
        // print_r($result); die;
        return $result;


     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}