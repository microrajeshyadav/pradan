<?php 

/**
* Staff history Model
*/
class Staff_history_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


 /**
   * Method getStaff_Transfer_History() get single staff history.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaff_Transfer_History($rid)
  {
    
    try{
    
  $sql = "SELECT 
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  CONCAT(k.`name` , ' (', md.`desname` , ')') as sendername, 
  CONCAT(g.`name` , ' (', mdd.`desname` , ')') as recivername,
  a.`id` as transid,
  e.`officename`,
  a.`trans_flag` as status,
  a.`reason`,
  a.`date_of_transfer` as proposeddate,
  b.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.`flag` = 1 THEN 'Process Initiate'  
      WHEN b.`flag` = 3 THEN 'Accepted By TC/Intergrator'
      WHEN b.`flag` = 4 THEN 'Rejected By TC/Intergrator' 
      WHEN b.`flag` = 5 THEN 'Accepted By Personnel Unit' 
      WHEN b.`flag` = 6 THEN 'Rejected By Personnel Unit' 
      WHEN b.`flag` = 7 THEN 'Handed By staff'
      WHEN b.`flag` = 8 THEN 'Rejected Clearance Certificated By TC/Intergrator' 
      WHEN b.`flag` = 9 THEN 'Taken By TC/Intergrator'
      WHEN b.`flag` = 10 THEN 'Rejected Clearance Certificated By Personnel Unit' 
      WHEN b.`flag` = 11 THEN 'Request submited' 
      WHEN b.`flag` = 12 THEN 'Request not submited' 
      WHEN b.`flag` = 13 THEN 'Taken Over Accepted By TC/Intergrator' 
      WHEN b.`flag` = 14 THEN 'Taken Over Rejected By TC/Intergrato' 
      WHEN b.`flag` = 15 THEN 'PLACE OF POSTING Accepted By TC/Intergrator' 
      WHEN b.`flag` = 16 THEN 'PLACE OF POSTING Rejected By TC/Intergrato' 
      WHEN b.`flag` = 17 THEN 'TRANSFER EXPENSES Request submited' 
      WHEN b.`flag` = 18 THEN 'TRANSFER EXPENSES Request Not submited' 
      WHEN b.`flag` = 19 THEN 'TRANSFER EXPENSES Accepted By TC/Intergrator' 
      WHEN b.`flag` = 20 THEN 'TRANSFER EXPENSES Rejected By TC/Intergrator' 
      WHEN b.`flag` = 21 THEN 'Accepted By Personnel Unit' 
      WHEN b.`flag` = 22 THEN 'Rejected By Personnel Unit'
      ELSE 'Not status here'
  END
  ) AS flag
  FROM `staff_transaction` AS a
  LEFT JOIN
  `tbl_workflowdetail` AS b ON a.`id` = b.`r_id`
  LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
  LEFT JOIN
  `mst_workflow_process` AS d ON b.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON b.`sender` = k.`staffid`
   LEFT JOIN
  `staff` AS g ON b.`receiver` = g.`staffid`
  LEFT JOIN
  `lpooffice` AS e ON a.`old_office_id` = e.`officeid`
  LEFT JOIN
  `lpooffice` AS f ON a.`new_office_id` = f.`officeid`
  LEFT JOIN
  `msdesignation` AS md ON k.`designation` = md.`desid`
  LEFT JOIN
  `msdesignation` AS mdd ON g.`designation` = mdd.`desid`


  Where b.`r_id` =".$rid." Order BY b.createdon ASC"; 
  // echo $sql;
  // die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function getStaff_cr_History($rid)
  {
    
    try{
    
  $sql = "SELECT 
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  CONCAT(k.`name` , ' (', md.`desname` , ')') as sendername, 
  CONCAT(g.`name` , ' (', mdd.`desname` , ')') as recivername,
  a.`id` as transid,
  e.`officename`,
  a.`trans_flag` as status,
  a.`reason`,
  a.`date_of_transfer` as proposeddate,
  b.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.`flag` = 1 THEN 'Process Initiate'  
      WHEN b.`flag` = 3 THEN 'Accepted By TC/Intergrator'
      WHEN b.`flag` = 4 THEN 'Rejected By TC/Intergrator' 
      WHEN b.`flag` = 5 THEN 'Accepted By Personnel Unit' 
      WHEN b.`flag` = 6 THEN 'Rejected By Personnel Unit' 
      WHEN b.`flag` = 7 THEN 'Handed Over By staff'
      WHEN b.`flag` = 8 THEN 'Taken Over Certificated By TC/Intergrator' 
      WHEN b.`flag` = 9 THEN 'Approv By Personnel Unit'
      WHEN b.`flag` = 10 THEN 'Fill joining Report By Personnel Unit' 
      WHEN b.`flag` = 11 THEN 'Approved by new TC/Intergrator' 
     
      ELSE 'Not status here'
  END
  ) AS flag
  FROM `staff_transaction` AS a
  LEFT JOIN
  `tbl_workflowdetail` AS b ON a.`id` = b.`r_id`
  LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
  LEFT JOIN
  `mst_workflow_process` AS d ON b.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON b.`sender` = k.`staffid`
   LEFT JOIN
  `staff` AS g ON b.`receiver` = g.`staffid`
  LEFT JOIN
  `lpooffice` AS e ON a.`old_office_id` = e.`officeid`
  LEFT JOIN
  `lpooffice` AS f ON a.`new_office_id` = f.`officeid`
  LEFT JOIN
  `msdesignation` AS md ON k.`designation` = md.`desid`
  LEFT JOIN
  `msdesignation` AS mdd ON g.`designation` = mdd.`desid`


  Where b.`r_id` =".$rid." Order BY b.createdon ASC"; 
   // echo $sql;
   // die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  public function getStaff_sep_History($rid)
  {
    
    try{
    
  $sql = "SELECT 
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  CONCAT(k.`name` , ' (', md.`desname` , ')') as sendername, 
  CONCAT(g.`name` , ' (', mdd.`desname` , ')') as recivername,
  a.`id` as transid,
  e.`officename`,
  a.`trans_flag` as status,
  a.`reason`,
  a.`date_of_transfer` as proposeddate,
  b.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE 
    WHEN b.flag = 1 THEN 'Process Initiate' 
    WHEN b.flag = 3 THEN 'Accepted By TC/Intergrator' 
     WHEN b.flag = 4 THEN 'Rejected By TC/Intergrator' 
    WHEN b.flag = 5 THEN 'Accepted By Personnel Unit' 
   
    WHEN b.`flag` = 6 THEN 'Rejected By Personnel Unit' 
    WHEN b.`flag` = 7 THEN 'Approve By ED' 
    WHEN b.`flag` = 8 THEN 'Rejected By ED'
    WHEN b.`flag` = 11 THEN 'Approve By Personnel Unit' 
    WHEN b.`flag` = 12 THEN 'Rejected By Personnel Unit' 
    WHEN b.`flag` = 13 THEN 'Approve By TC/Intergrator' 
    WHEN b.`flag` = 14 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`flag` = 15 THEN 'Approve By Finance' 
    WHEN b.`flag` = 16 THEN 'Rejected By Finance' 
    
    WHEN b.`flag` = 17 THEN 'Approve By Head Office Finance' 
    WHEN b.`flag` = 18 THEN 'Rejected By Head Office Finance' 
     WHEN b.`flag` = 19 THEN 'Approve By Personnel Unit' 
    WHEN b.`flag` = 20 THEN 'Rejected By Personnel Unit' 
    WHEN b.`flag` = 21 THEN 'Approve By HR Unit' 
    WHEN b.`flag` = 22 THEN 'Rejected By HR Unit'
     WHEN b.`flag` = 25 THEN 'Handed Over  By Staff'
     WHEN b.`flag` = 27 THEN 'Taken Over  By TC/Intergrator'
    WHEN b.`flag` = 28 THEN 'Approve By Personnel Unit' 
     WHEN b.`flag` = 29 THEN 'Approve By ED' 
     
      ELSE 'Not status here'


  END
) AS flag
  FROM `staff_transaction` AS a
  LEFT JOIN
  `tbl_workflowdetail` AS b ON a.`id` = b.`r_id`
  LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
  LEFT JOIN
  `mst_workflow_process` AS d ON b.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON b.`sender` = k.`staffid`
   LEFT JOIN
  `staff` AS g ON b.`receiver` = g.`staffid`
  LEFT JOIN
  `lpooffice` AS e ON a.`old_office_id` = e.`officeid`
  LEFT JOIN
  `lpooffice` AS f ON a.`new_office_id` = f.`officeid`
  LEFT JOIN
  `msdesignation` AS md ON k.`designation` = md.`desid`
  LEFT JOIN
  `msdesignation` AS mdd ON g.`designation` = mdd.`desid`


  Where b.`r_id` =".$rid." Order BY b.createdon ASC"; 
   // echo $sql;
   // die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}