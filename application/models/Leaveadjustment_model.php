<?php 

/**
* Master  Model
*/
class Leaveadjustment_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{

	}
	public function getStaffList()
	{
		try{

			$sql = "select staff.staffid,name,doj,emp_code,designation,msdesignation.desid,msdesignation.desname ,lp.officename as office from staff  inner join msdesignation on staff.designation= msdesignation.desid  LEFT JOIN `lpooffice` as lp on staff.new_office_id = lp.officeid where status =1 order by name"; 

			return $res = $this->db->query($sql)->result();



		}catch(Exception $e){
			print_r($e->getMessage());die();
		}
	}

	public function getrequestedleave($emp_code)
	{
		try{

			$sql = "SELECT lt.`From_date`, lt.`To_date`, lt.`Noofdays`, case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2  or lt.`status` = 4  or lt.`status` = 6 then 'Rejected' when lt.`status` = 7 then 'Approved' when lt.`status` = 9 then 'Reverse' end status, lt.`appliedon`, lt.`reason`, l.`Ltypename`, lt.status as stage FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` WHERE  lt.`Leave_transactiontype` ='DR' AND  lt.`Emp_code`=".$emp_code." ORDER BY appliedon DESC"; 

			return $res = $this->db->query($sql)->result();



		}catch(Exception $e){
			print_r($e->getMessage());die();
		}
	}


	public function get_empid($token)
	{
		try{

			$sql = "SELECT emp_code FROM staff WHERE staffid =".$token; 

			$res = $this->db->query($sql)->row();
			return $res;

		}catch(Exception $e){
			print_r($e->getMessage());die();
		}

	}
	public function getleavedetails($emp_code)
	{
		try{

      $sql = "SELECT l.`Ltypename` FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` WHERE  lt.`Leave_transactiontype` ='DR' AND  lt.`Emp_code`=".$emp_code;  

      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  public function getreportingtodetails($emp_code)
  {
    try{

     $sql = "SELECT
     a.emp_code,
     a.name  as staffname,
     a.new_office_id,
     msd.desname as staffdesignation,
     b.name AS reportingname,
     b.emailid as reportingtoemailid,
     lpo.officename as staffofficename,
     b.permanenthno,
     b.permanentstreet,
     b.permanentcity,
     b.permanentpincode,
     s.name AS statename,
     d.name AS districtname,
     b.contact
     FROM
     staff AS a
     INNER JOIN `staff` AS b
     ON
     a.reportingto = b.staffid
     LEFT JOIN `state` AS s
     ON
     a.permanentstateid = s.statecode
     LEFT JOIN `district` AS d
     ON
     a.permanentdistrict = d.districtid
     LEFT JOIN `msdesignation` as msd
     on 
     a.designation = msd.desid
     LEFT JOIN `lpooffice` as lpo
     on 
     a.new_office_id = lpo.officeid
     WHERE
     a.emp_code = ".$emp_code; 

     $res = $this->db->query($sql)->row();

     return $res;

   }catch(Exception $e){
    print_r($e->getMessage());
  }
}

// find leave types. created on 16/12/19 rajat
public function getlongleave($leavetype)
  {
      try{

        $LoginData = $this->session->userdata('login_data');

   $query="SELECT b.gender FROM mstuser a inner JOIN staff b ON a.staffid=b.staffid where a.staffid=? GROUP by b.gender";
   $gender_details = $this->db->query($query,[$LoginData->staffid])->row();

        if ($gender_details->gender == 1) {
    $sql = "select  longleavedays from msleavetype where Ltypeid BETWEEN 5 AND 11 and isdeleted = 0";
    $res = $this->db->query($sql)->result();
  }else if ($gender_details->gender == 2) {
    $sql = "select  longleavedays from msleavetype where Ltypeid BETWEEN 3 AND 10 and isdeleted = 0";
    $res = $this->db->query($sql)->result();
            // print_r($res); die();
  }else if ($gender_details->gender == 3) {
   $sql = "select  longleavedays from msleavetype where Ltypeid BETWEEN 3 AND 11 and isdeleted = 0";
   $res = $this->db->query($sql)->result();

 }

      // $sql = "SELECT longleavedays FROM `msleavetype`  WHERE  Ltypeid =$leavetype ";  
      // $res = $this->db->query($sql)->row();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


public function getleavectegory()
{

 $LoginData = $this->session->userdata('login_data');

 $query="SELECT b.gender FROM mstuser a inner JOIN staff b ON a.staffid=b.staffid where a.staffid=? GROUP by b.gender";
 $gender_details = $this->db->query($query,[$LoginData->staffid])->row();
        // print_r($gender_details); die;

 if ($gender_details->gender == 1) {
  $sql = "select 	Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 5 AND 11";
  $res = $this->db->query($sql)->result();
}else if ($gender_details->gender == 2) {
  $sql = "select  Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 3 AND 10";
  $res = $this->db->query($sql)->result();
            // print_r($res); die();
}else if ($gender_details->gender == 3) {
 $sql = "select  Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 3 AND 11";
 $res = $this->db->query($sql)->result();

}
return $res;


}



public function getleavedetail_balance($emp_code)
{
  try{

      $preyear = date('Y')-1;

   if (date('m')>=1 && date('m') <=3) {

     
      $fromdate = $preyear.'-04-01';
      $todate = date('Y').'-03-31';

    }else{
       $nextyear = date('Y')+1;
       $fromdate  = $preyear.'-04-01';
       $todate    = $nextyear.'-03-31';
    }

   $sql = "SELECT staff.emp_code, 'General' as Ltypename, staff.Name, desname, lpo.officename , IFNULL(trnleave_ledger.OB,0) OB, IFNULL(trnleave_ledger.Accured,0) Accured,IFNULL(trnleave_ledger.Availed,0) Availed, (IFNULL(trnleave_ledger.OB,0)+ IFNULL(trnleave_ledger.Accured,0) - IFNULL(trnleave_ledger.Availed,0)) Balance FROM  (SELECT emp_code, sum(IFNULL(case when Leave_transactiontype = 'OB' AND Leave_type= 0 and From_date = '".$fromdate."' then Noofdays else 0 end,0)) OB , sum(IFNULL(case when Leave_transactiontype = 'CR' AND Leave_type= 0 AND Description ='Credit leaves posting' then Noofdays else 0 end,0)) Accured, sum(IFNULL(case when Leave_transactiontype = 'DR' and Leave_type = 9 and status =3 then Noofdays else 0 end,0)) Availed FROM `trnleave_ledger` WHERE From_date >= '".$fromdate."' and to_date <= '".$todate."' GROUP BY Emp_code) as trnleave_ledger INNER JOIN staff on trnleave_ledger.Emp_code= staff.emp_code LEFT JOIN `msdesignation` as msd on staff.designation = msd.desid INNER JOIN `lpooffice` as lpo on staff.new_office_id = lpo.officeid where staff.status = 1 and trnleave_ledger.emp_code =". $emp_code . " GROUP BY staff.emp_code, staff.Name ORDER BY staff.emp_code";
    //echo  $sql; die;


   $res = $this->db->query($sql)->result();

   return $res;

 }catch(Exception $e){
  print_r($e->getMessage());die();
}
}


	


}