<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;

/**
* Dompdf wrapper class
* @author: Sandeep Kumar
*/
class Dompdf_model extends Ci_model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function generatePDF($html,$filename)
	{
		try{
		//to replace all un-used tags
		$html = trim($html);
		// echo $html; die();
		// echo strpos($html, "Sample Offer"); die();

		// da and (da to executive)
		if (strpos($html, "APTR") > 0 && strpos($html, "APTR") == 217) {
		 	$html = substr($html,171,strlen($html)- 167);
		 	$html = str_replace("<table  width", "<table width", $html);
		$html = str_replace("</table >", "</table>", $html);
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
		 } 
		 elseif (strpos($html, "Sample Offer") > 0 && strpos($html, "Sample Offer") == 652) {
		 	$html = substr($html,238,strlen($html)- 236);
		 	$html = str_replace("<table  width", "<table width", $html);
		$html = str_replace("</table >", "</table>", $html);
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
		 }

			// echo $html; die();
		// instantiate and use the dompdf class
			// $dompdf = new Dompdf();
			$options = new Options();
			$options->set('isRemoteEnabled', true);
			$dompdf = new Dompdf($options);
			$dompdf->loadHtml($html);
			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

		//$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render();

			$pdf = $dompdf->output();
			$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
			$path = FCPATH . "pdf_offerletters/$filename";
 		// echo $path;  die;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);

			return true;

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
			}catch (Exception $e) {
		print_r($e->getMessage());die;
			}
		}


			public function generatePDFAssistant($html,$filename)
	{
		try{
		//to replace all un-used tags
		$html = trim($html);

		 if (strpos($html, "Sample Offer") > 0 && strpos($html, "Sample Offer") == 573) {
		 	$html = substr($html,82,strlen($html)- 80);
		 }  


		
		$html = str_replace("<table  width", "<table width", $html);
		$html = str_replace("</table >", "</table>", $html);
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
			// echo $html; die();
		// instantiate and use the dompdf class
			// $dompdf = new Dompdf();
			$options = new Options();
			$options->set('isRemoteEnabled', true);
			$dompdf = new Dompdf($options);
			$dompdf->loadHtml($html);
			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

		//$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render();

			$pdf = $dompdf->output();
			$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
			$path = FCPATH . "pdf_offerletters/$filename";
 		// echo $path;  die;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);

			return true;

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
			}catch (Exception $e) {
		print_r($e->getMessage());die;
		}
		}



			public function generatePDFExecutive($html,$filename)
	{
		try{
		//to replace all un-used tags
		$html = trim($html);

		 if (strpos($html, "Sample Offer") > 0 && strpos($html, "Sample Offer") == 537) {
		 	$html = substr($html,82,strlen($html)- 80);
		 }  


		
		$html = str_replace("<table  width", "<table width", $html);
		$html = str_replace("</table >", "</table>", $html);
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
			// echo $html; die();
		// instantiate and use the dompdf class
			// $dompdf = new Dompdf();
			$options = new Options();
			$options->set('isRemoteEnabled', true);
			$dompdf = new Dompdf($options);
			$dompdf->loadHtml($html);

			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

		//$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render();

			$pdf = $dompdf->output();
			$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
			$path = FCPATH . "pdf_offerletters/$filename";
 		// echo $path;  die;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);

			return true;

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
			}catch (Exception $e) {
		print_r($e->getMessage());die;
			}
		}

//createdon 16/09/2019 for edsignature on pdf 
//created by rajat
		public function generatePDFed($html,$filename)
	{
		try{
		//to replace all un-used tags
		$html = trim($html);
		 // strpos($html, "src"); 

		// da and (da to executive)
		// if (strpos($html, "src") > 0 && strpos($html, "src") == 10299) {
		 	// echo $html = substr($html,10024,10025); die();
		 // } 
		// die(1);

		if (strpos($html, "APTR") > 0 && strpos($html, "APTR") == 217) {
		 	$html = substr($html,171,strlen($html)- 167);
		 }
		 elseif (strpos($html, "Sample Offer") > 0 && strpos($html, "Sample Offer") == 573) {
		 	$html = substr($html,82,strlen($html)- 80);
		 }
		 elseif (strpos($html, "Sample Offer") > 0 && strpos($html, "Sample Offer") == 537) {
		 	$html = substr($html,82,strlen($html)- 80);
		 }  

			$options = new Options();
			$options->set('isRemoteEnabled', true);
			$dompdf = new Dompdf($options);
			$dompdf->loadHtml($html);
			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

		//$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render();

			$pdf = $dompdf->output();
			$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
			$path = FCPATH . "pdf_offerletters/$filename";
 		// echo $path;  die;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);

			return true;

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
			}catch (Exception $e) {
		print_r($e->getMessage());die;
		}
		}

		public function GeneralNominationformPDF($html,$filename)
		{
		try{
		//to replace all un-used tags
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
			//echo $html; die;

		// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml($html);
			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render(); 

			$pdf = $dompdf->output(); 

			$pdfPath = file_put_contents(FCPATH . "pdf_generalnominationform/$filename", $pdf);

			$path = FCPATH . "pdf_generalnominationform/$filename";

			$dompdf->stream($path, array('Attachment' =>0));
		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
			}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
		}



		public function joiningreportfordaPDF($html,$filename)
		{
		try{
		//to replace all un-used tags
		$html = trim($html);
		// echo $html;
	    // echo strpos($html, "JOINING REPORT"); die();
		 if (strpos($html, "JOINING REPORT") > 0 && strpos($html, "JOINING REPORT") == 1199) {
		 	$html = substr($html,83,strlen($html)- 81);
		 } 
		 // echo $html; die();

		// $html = str_replace("<table  width=''100%''></table >", "", $html);

		// $html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		// $html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		// $html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		// $html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		// $html = str_replace("<div></div>", "", $html);
		// $html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		// $html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
		
		// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml($html);
			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render();
			
			$pdf = $dompdf->output();
			$pdfPath = file_put_contents(FCPATH . "pdf_joiningreportforda/$filename", $pdf);
			$path = FCPATH . "pdf_joiningreportforda/$filename";
			// echo $path; die();
		// $dompdf->stream($path, array('Attachment' =>0));

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");

		 //$dompdf->stream($path);

			return true;
			}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
		}

		public function generatePDFMedicalcertificate($html,$filename)
		{
			try{
		//to replace all un-used tags
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);
			
			
		// instantiate and use the dompdf class
			$dompdf = new Dompdf();

			$dompdf->loadHtml($html);
			
		// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

		//$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
			$dompdf->render();

			$pdf = $dompdf->output();
			$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
			$path = FCPATH . "pdf_offerletters/$filename.pdf";


			$dompdf->stream($path, array('Attachment' =>0));

			$dompdf->stream($path);

			return true;

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
			}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
		}



/**
   * Method generate_transfer_letter_PDF() genrate transfer letter PDF in personnel system .
   * @access  public
   * @param Null
   * @return  Array
   */



public function generate_personnel_transfer_letter_PDF($html,$filename)
{

	try{
		$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);
		
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		//$dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
		$dompdf->render();

		$pdf = $dompdf->output();
		$pdfPath = file_put_contents(FCPATH . "datafiles/letters/transfer/$filename", $pdf);
		$path = FCPATH . "datafiles/letters/transfer/$filename.pdf";
 		//echo $path;  die;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);
		return true;

		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



public function genrate_transfer_letter_pdf($html,$filename)
{
	try{
	//to replace all un-used tags
	$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);

		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
	$path = FCPATH . "pdf_offerletters/$filename.pdf";
	
 		//echo $path;  die;

		//$dompdf->stream($path, array('Attachment' =>0));

	$dompdf->stream($path);

	
		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}






public function feedback_letter_PDF($html,$filename)
{
	try{
//to replace all un-used tags
	
	$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);

		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "pdf_separationletters/$filename.pdf", $pdf);
	$path = FCPATH . "pdf_separationletters/$filename.pdf";
 		//echo $path;  die;

	return true;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);



		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}



public function separation_letter_PDF($html,$filename)
{
	try{
//to replace all un-used tags
	$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);


		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "pdf_separationletters/$filename.pdf", $pdf);
	$path = FCPATH . "pdf_separationletters/$filename.pdf";
 		//echo $path;  die;

	return true;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);



		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function probation_letter_PDF($html,$filename)
{
	try{
	//to replace all un-used tags
	$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);


		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
	$path = FCPATH . "pdf_offerletters/$filename.pdf";
 		//echo $path;  die;

	return true;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);



		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

public function midtem_letter_PDF($html,$filename)
{
	try{
	//to replace all un-used tags
	$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);


		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "midtem_offerletter/$filename.pdf", $pdf);
	$path = FCPATH . "midtem_offerletter/$filename.pdf";
 		// echo $path;  die;

	return true;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);



		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

public function leave_letter_PDF($html,$filename)
{
	try{
	//to replace all un-used tags
	// $html = str_replace("<table  width=''100%''></table >", "", $html);

	// 	$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
	// 	$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
	// 	$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
	// 	$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
	// 	$html = str_replace("<div></div>", "", $html);
	// 	$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
	// 	$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);

		// echo $html; die();
		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
	$path = FCPATH . "pdf_offerletters/$filename.pdf";
 		//echo $path;  die;

	$dompdf->stream($path);

	return true;
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}



public function transfer_letter_PDF($html,$filename)
{
	try{
	//to replace all un-used tags
	$html = str_replace("<table  width=''100%''></table >", "", $html);

		$html = str_replace("<table ", "<table style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<td", "<td style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<th", "<th style='border:1px Solid #e1e1e1; border-collapse:collpase;'", $html);
		$html = str_replace("<span style=''font-size: 14.4px;''></span>", "", $html);
		$html = str_replace("<div></div>", "", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 12pt;'='' style='font-size: 14.4px;'>$</span>", "$", $html);
		$html = str_replace("<span times='' new='' roman',='' serif;='' font-size:='' 16px;'='' style='font-size: 14.4px; text-align: left;'>effective_date</span>", "effective_date", $html);



		// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($html);
	
		// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
		// $dompdf->setPaper('A4', 'landscape');

		// Render the HTML as PDF
	$dompdf->render();

	
	$pdf = $dompdf->output();
	$pdfPath = file_put_contents(FCPATH . "pdf_separationletters/$filename.pdf", $pdf);
	$path = FCPATH . "pdf_separationletters/$filename.pdf";
 		//echo $path;  die;

	return true;

		//$dompdf->stream($path, array('Attachment' =>0));

		//$dompdf->stream($path);



		//$handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}