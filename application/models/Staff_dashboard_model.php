<?php 

/**
* State Model
*/
class Staff_dashboard_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



	public function getstaffStatus($staffids)
{

    try{
     
      $sql ="SELECT flag FROM staff WHERE staff.staffid= ". $staffids;

     $result = $this->db->query($sql)->row();

     return $result;

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }

}

/**
   * Method superannuationlist() Find the Super Annuation listing.
   * @access  public
   * @param Null
   * @return  Array
   */


   public function superannuationlist()
   {

    try{

     $sql = "SELECT st.staffid, st.name, st.emailid, st.dob, DATE_ADD(st.dob, INTERVAL 60 YEAR) retiringdate, IFNULL(msd.desname, 'NA') desname FROM `staff` as st LEFT JOIN msdesignation msd ON st.designation = msd.desid WHERE st.dob IS NOT NULL AND st.dob !='0000-00-00' AND st.`dateofleaving` IS NULL AND st.status = 1 AND ( DATE_ADD(st.dob, INTERVAL 60 YEAR) <= DATE_ADD( CURDATE(), INTERVAL 4 MONTH ) ) AND st.super_annuation = 0 ORDER BY st.dob DESC"; 

     $result = $this->db->query($sql)->result();

     return $result;

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

// public function getstaffStatus($staffids)
// {

//     try{
     
//       $sql ="SELECT flag FROM staff WHERE staff.staffid= ". $staffids;

//      $result = $this->db->query($sql)->result()[0];

//      return $result;

//    }catch (Exception $e) {
//      print_r($e->getMessage());die;
//    }

// }

public function getpolicylist()
 {

    try{

     $query = $this->db->get('policy');
     if($query->num_rows())
     {
        return $query->result();
    }else{
        return 0;
    }

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


/**
   * Method getdashboardstat() get dasboard statistics.
   * @access  public
   * @param Null
   * @return  Array
   * created by rajat updated on 9/7/2019
   */
    public function getdashboardstat()
    {
    
        try{

            $sql = "select count(*) totaltaff, sum(case when gender = 1 then 1 else 0 end) malecount, sum(case when gender = 2 then 1 else 0 end) femalecount, case when month(dateofleaving) =  month(NOW()) and  year(dateofleaving) =  year(NOW()) then 1 else 0 end totalsep  from staff where status = 1";

            $result = $this->db->query($sql)->row();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }



}