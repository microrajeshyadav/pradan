<?php 
/**
* Feedback Model
*/
class Feedback_model extends Ci_model
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {

  }

 /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function changedatedbformate($Date)
 {
  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
   //print_r($date); 
 if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
 else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 return $date;
 }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method getApprentice() get Apprentice List .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getApprentice($teamid = null)
  {

    try{

     $sql = "SELECT DISTINCT a.teamid,b.staffid,b.name  FROM tbl_feedback as a 
     left join `tbl_da_personal_info` as b ON a.`apprenticeid` = b.`staffid`  Where 1=1 ";

     if (!empty($teamid)) {
       $sql .= " AND a.teamid =".$teamid;
     }

     $result = $this->db->query($sql)->result();

     return $result;

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



 /**
   * Method getfeedbackStaffdetail() get Team Coordinator.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getfeedbackStaffdetail($id)
 {

  try{

   $sql = "SELECT 
   a.`teamid`,
   a.`batchid`,
   a.`fgid`,
   SUBSTRING(a.`name`, 1, LOCATE(' ',a.`name`)) as name,
   b.`batch`,
   c.`officename`,
   d.`name` as fieldguidename
   FROM 
   `tbl_da_personal_info` as a 
   LEFT JOIN `mstbatch` as b ON b.`id` = a.`batchid`
   LEFT JOIN `lpooffice` as c ON c.`officeid` = a.`teamid`
   LEFT JOIN `staff` as d ON d.`staffid` = a.`fgid` 
   WHERE a.`staffid` = $id"; 


   $result = $this->db->query($sql)->row();
   return $result;
   // echo json_encode($result);


 }catch(Exception $e){
  print_r($e->getMessage());die();
}

} 


/**
   * Method officedetail() get office name.
   * @access  public
   * @param Null
   * @return  Array
   */

public function officedetail()
{

  try{

    $sql = "SELECT officeid,officename FROM `lpooffice`";

    $result = $this->db->query($sql)->result();

    return $result;


  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

} 


/**
   * Method tc_details() get office name.
   * @access  public
   * @param Null
   * @return  Array
   */

public function tc_details($teamid)
{

  try{

    $sql = "SELECT officeid,officename FROM `lpooffice` where officeid='$teamid'";

    $result = $this->db->query($sql)->row();

    return $result;


  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

} 




/**
   * Method getFeedbackdetail() get Team Coordinator.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getFeedbackdetail($id )
{

  try{

    $sql = "SELECT * FROM `tbl_feedback` as f 
    LEFT JOIN `tbl_da_personal_info` as a ON a.`staffid` = f.`apprenticeid`
    LEFT JOIN `mstbatch` as b ON b.`id` = f.`batchid`
    LEFT JOIN `lpooffice` as c ON c.`officeid` = f.`teamid`
    LEFT JOIN `staff` as d ON d.`staffid` = f.`field_guideid`
    WHERE f.`id` = $id "; 


    $result = $this->db->query($sql)->row();

    return $result;


  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

} 


/**
   * Method getStaffName() get Team Coordinator.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffDetails($id )
{

  try{

    $sql = "SELECT emailid FROM `tbl_da_personal_info` WHERE staffid=".$id; 

    $result = $this->db->query($sql)->result()[0];

    return $result;


  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

} 


/**
   * Method getFeedbackList() get Team Coordinator.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getFeedbackList($teamid)
{

  try{

   $sql = "SELECT 
   f.id,
   f.status,
   a.name as apprenticename,
   f.feedback_letter,
   f.duedate,
   f.enddate,
   b.batch,
   c.officename,
   a.emailid,
   rt.emp_code,
   f.teamid,
   f.status,
   d.name as fieldguide
   FROM `tbl_feedback` as f 
   LEFT JOIN `tbl_da_personal_info` as a ON a.`staffid` = f.`apprenticeid`
   LEFT JOIN `mstbatch` as b ON b.`id` = f.`batchid`
   LEFT JOIN `lpooffice` as c ON c.`officeid` = f.`teamid`
   LEFT JOIN `staff` as d ON d.`staffid` = f.`field_guideid` 
   LEFT JOIN `staff` as rt ON rt.`staffid` = a.`staffid` 
   Where a.`teamid`=". $teamid ."    ORDER BY f.id ASC "; 

   $result = $this->db->query($sql)->result();

   return $result;


 }catch(Exception $e){
  print_r($e->getMessage());die();
}

} 


/**
   * Method getFeedback_status() get Team Coordinator.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getFeedback_status($teamid)
{

  try{

   $sql = "SELECT 
   f.id,
   f.status,
   a.name as apprenticename,
   f.feedback_letter,
   f.duedate,
   f.enddate,
   b.batch,
   c.officename,
   a.emailid,
   rt.emp_code,
   f.teamid,
   f.status,
   d.name as fieldguide
   FROM `tbl_feedback` as f 
   LEFT JOIN `tbl_da_personal_info` as a ON a.`staffid` = f.`apprenticeid`
   LEFT JOIN `mstbatch` as b ON b.`id` = f.`batchid`
   LEFT JOIN `lpooffice` as c ON c.`officeid` = f.`teamid`
   LEFT JOIN `staff` as d ON d.`staffid` = f.`field_guideid` 
   LEFT JOIN `staff` as rt ON rt.`staffid` = a.`staffid` 
   Where a.`teamid`=". $teamid ."  AND f.status=2  ORDER BY f.id ASC "; 

   $result = $this->db->query($sql)->result();

   return $result;


 }catch(Exception $e){
  print_r($e->getMessage());die();
}

} 

/**
   * Method getFeedback_report() get DA feedback report.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getFeedback_report($teamid)
{

  try{

    $sql = "SELECT    
    a.staffid,
    a.candidateid,
    a.name AS apprenticename,
    b.batch,
    a.candidateid,
    c.officename,
    a.emailid,
    rt.emp_code,
    d.name AS fieldguide,
    returnfeedback(a.staffid,1) as feedback1,
    returnfeedback(a.staffid,2) as feedback2,
    returnfeedback(a.staffid,3) as feedback3     
    FROM
    `tbl_da_personal_info` AS a
    LEFT JOIN `mstbatch` AS b
    ON
    b.`id` = a.`batchid`
    LEFT JOIN `lpooffice` AS c
    ON
    c.`officeid` = a.`teamid`
    LEFT JOIN `staff` AS d
    ON
    d.`staffid` = a.`fgid`
    LEFT JOIN `staff` AS rt
    ON
    rt.`staffid` = a.`staffid`

    WHERE
    a.`teamid` =".$teamid;

    $result = $this->db->query($sql)->result();

    return $result;


  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

} 


/**
   * Method getReflective_report() get list of DA Reflective Report.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getReflective_report($candidateid)
{

  try{
   $sql = "SELECT `tbl_daship_component`.candidateid,`tbl_daship_component`.encrypted_document_name,mstphase.phase_name FROM `tbl_daship_component` INNER JOIN mstphase  on mstphase.id=`tbl_daship_component`.phaseid WHERE candidateid =".$candidateid;
   $res = $this->db->query($sql)->result();

   return $res;  

 }catch(Exception $e){
  print_r($e->getMessage());die();
}

}




}