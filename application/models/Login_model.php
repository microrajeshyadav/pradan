<?php 

/**
* Login Class
*/
class Login_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function verifylogin()
	{
		try{
		//print_r($_POST); die; 
	    $username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$roleid   = $this->security->xss_clean($this->input->post('roleid'));
		$checked  = $this->security->xss_clean($this->input->post('rememberme'));

		

		if ($username == NULL || trim($username) == '') {
			$this->session->set_flashdata('er_msg', 'UserName is required for loggin in');
			return false;
		}

		if ($password == NULL || trim($password) == '') {
			$this->session->set_flashdata('er_msg', 'Password is required for loggin in');
			return false;
		}

		$this->db->where('Username', $username);
		$this->db->where('password', md5($password));
		$this->db->where('RoleID', $roleid);
		$this->db->where('IsDeleted', 0);
		$result = $this->db->get('mstuser')->row();
		//echo $this->db->last_query();
		
		// echo count($result);  die;
		//echo $this->db->last_query(); die;
	
       // print_r($result); 
       // die();
		if (count($result) == 1) {

			

			// $cookie_time = 60 * 60 * 24 * 30; ///30 days
			// $cookie_time_onset = $cookie_time + time();

			//  if ($rememberme) {
			//  	// find user id of cookie_user stored in application database
			//  	set_cookie("username", $username, $cookie_time_onset);
			//  	set_cookie("password", $password, $cookie_time_onset);
			//  	set_cookie("roleid", $roleid, $cookie_time_onset);
			//  	set_cookie("checked", $checked, $cookie_time_onset);
			//  }else {
			//  	$cookie_time_onset = $cookie_time; 
			//  	set_cookie("username", '', $cookie_time_onset);
			//  	set_cookie("password", '', $cookie_time_onset);
			//  	set_cookie("roleid", '', $cookie_time_onset);
			//  	set_cookie("checked", '', $cookie_time_onset);
			//  }
			      
		if ($result->RoleID==19) {

	    	$username = $result->Username;
	    	$EmailID = $result->EmailID;
	    	$RoleID = $result->RoleID;
	    	$PhoneNumber = $result->PhoneNumber;

			$UserFirstName = $result->UserFirstName;
			$UserMiddleName = $result->UserMiddleName;
			$UserLastName = $result->UserLastName;
			$Candidateid = $result->Candidateid;
			$staffid = $result->staffid;
			$userexp = explode('_', $username);
			$UserID = $userexp[1];
			$getcandidate 		= $this->GetCandidate($Candidateid);
			$BDFFormStatus 		= $getcandidate->BDFFormStatus;
			$teamid 			= $getcandidate->teamid;
			$joinstatus 		= $getcandidate->joinstatus;
			$campustype 		= $getcandidate->campustype;
			$tc_hrd_document_verfied 		= $getcandidate->tc_hrd_document_verfied;

			  $sessArr = (object) array(
	          'UserID'  		=> $UserID,
	          'Username'  		=> $username,
	          'UserFirstName'  	=> $UserFirstName,
	          'UserMiddleName' 	=> $UserMiddleName,
	          'UserLastName'  	=> $UserLastName,
	          'PhoneNumber'  	=> $PhoneNumber,
	          'EmailID'  		=> $EmailID,
	          'RoleID' 			=> $RoleID,
	          'candidateid'		=> $Candidateid,
	          'BDFFormStatus'   => $BDFFormStatus,
	          'joinstatus'      => $joinstatus,
	          'campustype'      => $campustype,
	          'emailid'         => $EmailID,
	          'staffid'         => $staffid,
	          'officeid'        => $teamid,
	          'tc_hrd_document_verfied' => $tc_hrd_document_verfied,
	          );

		//print_r($sessArr); die;
			$this->session->set_userdata('login_data',$sessArr);

		}else if ($result->RoleID==3) {

	    	$username = $result->Username;
	    	$UserID   = $result->UserID;
	    	$EmailID = $result->EmailID;
	    	$RoleID = $result->RoleID;
	    	$PhoneNumber = $result->PhoneNumber;
			$UserFirstName = $result->UserFirstName;
			$UserMiddleName = $result->UserMiddleName;
			$UserLastName = $result->UserLastName;
			$Candidateid = $result->Candidateid;
			$userexp = explode('_', $username);
			$staffid = $userexp[1];
			$getteam = $this->GetTeam($staffid);
			$teamid = $getteam->new_office_id; 
		
			$sessArr = (object) array(
	          'UserID'  		=> $UserID,
	          'Username'  		=> $username,
	          'UserFirstName'  	=> $UserFirstName,
	          'UserMiddleName' 	=> $UserMiddleName,
	          'UserLastName'  	=> $UserLastName,
	          'PhoneNumber'  	=> $PhoneNumber,
	          'EmailID'  		=> $EmailID,
	          'RoleID' 			=> $RoleID,
	          'candidateid'		=> $Candidateid,
	          'staffid'		    => $staffid,
	          'teamid'          => $teamid
	          );

		//print_r($sessArr); die;
			$this->session->set_userdata('login_data',$sessArr);

		}else if ($result->RoleID==30) {

	    	$username = $result->Username;
	    	$EmailID = $result->EmailID;
	    	$RoleID = $result->RoleID;
	    	$PhoneNumber = $result->PhoneNumber;

			$UserFirstName = $result->UserFirstName;
			$UserMiddleName = $result->UserMiddleName;
			$UserLastName = $result->UserLastName;
			$userexp = explode('_', $username);
			$UserID = $userexp[1];
			$staffid= $result->staffid;
	
			//$this->session->set_userdata('login_data', $result[0]);

		  $sessRecruiterArr = (object) array(
          'UserID'  		=> $UserID,
          'Username'  		=> $username,
          'UserFirstName'  	=> $UserFirstName,
          'UserMiddleName' 	=> $UserMiddleName,
          'UserLastName'  	=> $UserLastName,
          'PhoneNumber'  	=> $PhoneNumber,
          'EmailID'  		=> $EmailID,
          'RoleID' 			=> $RoleID,
          'emailid'         => $EmailID,
          'staffid'			=> $staffid,
          );

		//print_r($sessArr); die;
			$this->session->set_userdata('login_data',$sessRecruiterArr);

		}else if ($result->RoleID==2 || $result->RoleID==21) {


	    	$username = $result->Username;
	    	$EmailID = $result->EmailID;
	    	$RoleID = $result->RoleID;
	    	$PhoneNumber = $result->PhoneNumber;

			$UserFirstName = $result->UserFirstName;
			$UserMiddleName = $result->UserMiddleName;
			$UserLastName = $result->UserLastName;
			$staffid =   $result->staffid;
			$getteam = $this->GetTeam($staffid);
			$teamid = $getteam->new_office_id; 

			 $sessRecruiterArr = (object) array(
	          'UserID'  		=> $staffid,
	          'Username'  		=> $username,
	          'UserFirstName'  	=> $UserFirstName,
	          'UserMiddleName' 	=> $UserMiddleName,
	          'UserLastName'  	=> $UserLastName,
	          'PhoneNumber'  	=> $PhoneNumber,
	          'EmailID'  		=> $EmailID,
	          'RoleID' 			=> $RoleID,
	          'emailid'         => $EmailID,
	          'teamid'          => $teamid,
	          'staffid'         => $staffid,
	          );

		//print_r($sessArr); die;
		$this->session->set_userdata('login_data',$sessRecruiterArr);

		}else if ($result->RoleID == 10) {

			$UserID = $result->UserID;
	    	$username = $result->Username;
	    	$EmailID = $result->EmailID;
	    	$RoleID = $result->RoleID;
	    	$PhoneNumber = $result->PhoneNumber;

			$UserFirstName = $result->UserFirstName;
			$UserMiddleName = $result->UserMiddleName;
			$UserLastName = $result->UserLastName;
			$staffid = $result->staffid;
			$getteam = $this->GetTeam($staffid);
			 $teamid = $getteam->new_office_id; 

			
			
		  $sessRecruiterArr = (object) array(
          'UserID'  		=> $UserID,
          'Username'  		=> $username,
          'UserFirstName'  	=> $UserFirstName,
          'UserMiddleName' 	=> $UserMiddleName,
          'UserLastName'  	=> $UserLastName,
          'PhoneNumber'  	=> $PhoneNumber,
          'EmailID'  		=> $EmailID,
          'RoleID' 			=> $RoleID,
          'emailid'         => $EmailID,
          'teamid'          => $teamid,
          'staffid'         => $staffid,
          );
//print_r($sessRecruiterArr); die;
		//print_r($sessArr); die;
		$this->session->set_userdata('login_data',$sessRecruiterArr);

		}else if ($result->RoleID==17) {
	
    	$username = $result->Username;
    	$EmailID = $result->EmailID;
    	$RoleID = $result->RoleID;
    	$PhoneNumber = $result->PhoneNumber;

		$UserFirstName = $result->UserFirstName;
		$UserMiddleName = $result->UserMiddleName;
		$UserLastName = $result->UserLastName;
		$staffid = $result->staffid;
		$getteam = $this->GetTeam($staffid);
		 $teamid = $getteam->new_office_id; 

		

	  	$sessRecruiterArr = (object) array(
          'UserID'  		=> $staffid,
          'Username'  		=> $username,
          'UserFirstName'  	=> $UserFirstName,
          'UserMiddleName' 	=> $UserMiddleName,
          'UserLastName'  	=> $UserLastName,
          'PhoneNumber'  	=> $PhoneNumber,
          'EmailID'  		=> $EmailID,
          'RoleID' 			=> $RoleID,
          'emailid'         => $EmailID,
          'teamid'          => $teamid,
          'staffid'  		=> $staffid,
          );
		$this->session->set_userdata('login_data',$sessRecruiterArr);
		}else{

			//print_r($result);die;
		$this->session->set_userdata('login_data', $result);
		}

		return true;
		}else{
			$this->session->set_flashdata('er_msg', 'Incorrect Email ID / password please try again');
			return false;	
		}
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}




public function GetTeam($staffid)
	{

		try{

	 	$sql = "SELECT 
	staff_transaction.date_of_transfer, 
	staff_transaction.new_office_id,
	staff_transaction.new_office_id 
FROM 
	staff_transaction 
	INNER JOIN(
		SELECT 
			
			MAX(
				staff_transaction.date_of_transfer
			) AS MaxDate 
		FROM 
			staff_transaction  where 
	`staff_transaction`.`staffid` = ".$staffid."
		
	) AS TMax ON `staff_transaction`.date_of_transfer = TMax.MaxDate
    where `staff_transaction`.`staffid` = ".$staffid; 

 
 			$result = $this->db->query($sql)->result()[0];
		    return $result;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

public function GetCandidate($Candidateid)
	{

		try{
			$this->db->select('*');
			$this->db->where('tbl_candidate_registration.candidateid', $Candidateid);
			$this->db->from('tbl_candidate_registration');
			$result = $this->db->get()->result(); 
			return $result;
		
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



public function GetCampusdetail($emailid)
	{

		try{
			$this->db->select('campusid');
			$this->db->where('emailid', $emailid);
			$this->db->from('mstcampus');
			$result = $this->db->get()->result()[0]; 
			return $result;
		
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




	public function GetData($userid)
	{

		try{
			$this->db->select('*');
			$this->db->from('mstuser');
			$this->db->where('mstuser.UserID', $userid);
			return $this->db->get()->row(); //echo $this->db->last_query(); die;
		
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


public function getRole()
{
	 try{

         $sql = "SELECT * FROM `sysaccesslevel` ORDER BY `Acclevel_Name` ";
         $res = $this->db->query($sql)->result();
         return $res;

    }catch(Exception $e){
        print_r($e->getMessage());die();
    }
}


/**
   * Method getBdfFormStatus() get Candidate registration status .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getBdfFormStatus($candiid)
{

  try{

      $sql = "SELECT `tblcr`.BDFFormStatus FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid.""; //die;

          $res = $this->db->query($sql)->result()[0];

    return $res;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}




/**
   * Method getOfferletterStatus() get Candidate registration status .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getOfferletterStatus($candiid)
{

  try{

      $sql = "SELECT `tblcr`.flag FROM `tbl_sendofferletter` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid.""; //die;
      	$query = $this->db->query($sql);
        $res = $this->db->query($sql)->row();
      return $res;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

}