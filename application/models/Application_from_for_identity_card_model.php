<?php 

/**
* State Model
*/
class Application_from_for_identity_card_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


  public function changedate($Date)
  {
    try{

//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
 if ($date=='//') {
      $date = NULL;
   }
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}



  public function do_uploads($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
  
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/signature/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );
   
  $this->load->library('upload',$config);

  if(!$this->upload->do_upload('signature'))
  {
    $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}

}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}


public function getCandidateWithAddressDetails($staff_id)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getCandidateWith($reportingto)
  {
    
    try{

        $sql = "SELECT
    staff.name as reprotingname
    
FROM
    staff WHERE   staff.staffid='$reportingto'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




  public function do_uploads2($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/staff_identity/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );

  $this->load->library('upload',$config);
  
  if(!$this->upload->do_upload('Photo2'))
  {
     $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
       $fileData=$this->upload->data();  

    //die();
return $fileData;
}

}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}


public function do_upload($element_name = 'userfile', $allowed_types = ['jpg','png','pdf','xls','xlsx','docx','doc']) 
{

  try{
  $config = array(
    'allowed_types'    => implode('|', $allowed_types),
    'upload_path'      => FCPATH . "datafiles/staff_identity/",
    'max_size'         => 10000,
    'encrypt_name'     => TRUE,
    'file_ext_tolower' => TRUE,
  );

  $this->load->library('upload', $config);
  $this->upload->initialize($config);
  $retval = $this->upload->do_upload($element_name);
  if ($retval === FALSE) {
    $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
    redirect(current_url());
  }

  $file_data = $this->upload->data();
  return $file_data;
  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}

  public function do_uploads3($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
  
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/staff_identity/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );

  $this->load->library('upload',$config);
  
  if(!$this->upload->do_upload('Photo1'))
  {
     $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}

}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}



  public function do_uploads4($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/signature/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );

  $this->load->library('upload',$config);
  
  if(!$this->upload->do_upload('supervisorsignature'))
  {
    $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}

}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}

public function fetchdata($staffids)
{

$qry="Select * from tblidentitycard where staffid=$staffids";
$qru11 = $this->db->query($qry)->result();

return $qru11;
} 


public function fetchdatas($candidateid)
{

  try{

    $sql = "SELECT a.candidatefirstname, a.candidatelastname, d.desname,
     f.officename, c.emp_code, a.bloodgroup FROM tbl_candidate_registration a 
     LEFT JOIN staff as c on c.candidateid=a.candidateid 
     LEFT JOIN msdesignation as d on c.designation=d.desid 
     LEFT JOIN lpooffice as f on f.staffid=c.staffid 
     where a.candidateid=$candidateid";
    //echo $sql;

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function pdfdata($candidateid)
{

  try{

    $sql = "SELECT a.candidatefirstname, a.candidatelastname, b.offerno, b.doj, d.desname, f.officename, c.emp_code, a.bloodgroup, q.* FROM tbl_candidate_registration a INNER JOIN tbl_generate_offer_letter_details AS b ON b.candidateid = a.candidateid LEFT JOIN staff AS c ON c.candidateid = b.candidateid LEFT JOIN msdesignation AS d ON c.designation = d.desid LEFT JOIN lpooffice AS f ON f.staffid = c.staffid INNER JOIN tblidentitycard as q on q.candidate_id=a.candidateid WHERE a.candidateid=$candidateid";

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method get max eworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_applicationworkflowid($staff_id)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=12 and staff.staffid=$staff_id ORDER BY workflowid DESC LIMIT 1";
    
// echo $sql;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


}