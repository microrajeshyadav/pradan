<?php 

/**
* Campus Incharge Intimation Model
*/
class Campusinchargeintimation_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}


  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try
    {
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "-" )
    $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try
    {
//echo strlen($Date);
    $len = (strlen($Date)-5); 
  // echo substr($Date,$len,-4);
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
      $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}







public function getCategory()
  {
    try{

        $sql = "SELECT * FROM `mstcategory` Where isdeleted = 0 ";
        $result = $this->db->query($sql)->result();
        return $result;

        }catch (Exception $e) {
                print_r($e->getMessage());die;
      }
  }


	public function getCampus()
	{
		try{

        $sql = "SELECT * FROM `mstcampus` where isdeleted=0";
		$result = $this->db->query($sql)->result();

		return $result;

        }catch (Exception $e) {
                print_r($e->getMessage());die;
            }
	}



    public function getCampusEmailid($token)
    {
       try{

        $sql = "SELECT campusincharge,campusname,emailid,city FROM `mstcampus` WHERE `campusid` =$token";
        $result = $this->db->query($sql)->result();

        return $result;
        }catch (Exception $e) {
            print_r($e->getMessage());die;
       }
    }


   public function getCampusInchargeDetail($token=null)
    {
       try{
             
           $sql = "SELECT `tbl_campus_intimation`.fromdate, `tbl_campus_intimation`.todate, `mstcampus`.campusname,`mstcampus`.emailid,`mstcampus`.campusincharge,`tbl_campus_intimation`.messsage, `tbl_campus_intimation`.mailstatus, `tbl_campus_intimation`.id, `tbl_campus_intimation`.campusid,`tbl_campus_intimation`.categoryid,`mstcategory`.`categoryname`, `tbl_campus_intimation`.campus_status as status, SUBSTR(`tbl_campus_intimation`.messsage,  LOCATE('http://',`tbl_campus_intimation`.messsage),case when LOCATE('CandidatesInfo/',`tbl_campus_intimation`.messsage) = 0 then LOCATE('Candidate_registration/',`tbl_campus_intimation`.messsage) else LOCATE('CandidatesInfo/',`tbl_campus_intimation`.messsage) end - LOCATE('http://',`tbl_campus_intimation`.messsage)) cgh FROM `tbl_campus_intimation` Inner join  `mstcampus` ON `tbl_campus_intimation`.campusid=  `mstcampus`.campusid Inner join  `mstcategory` ON `tbl_campus_intimation`.categoryid=  `mstcategory`.id "; 
        
          if($token !=''){
                 $sql .= "  AND `tbl_campus_intimation`.id=".$token;
             }
           
           $sql .= " ORDER BY `tbl_campus_intimation`.createdon DESC";


           //echo $sql; die;
              
        $result = $this->db->query($sql)->result();

        return $result;

        }catch (Exception $e) {
            print_r($e->getMessage());die;
       }
    }



public function getCampusInchargeIntemationDetail($token)
    {
       try{

           $sql = 'SELECT * FROM `tbl_campus_intimation` WHERE 1=1 '; 

           if($token !=''){
            $sql .= '  AND `tbl_campus_intimation`.id='.$token;
           }

            $sql .= ' ORDER BY `tbl_campus_intimation`.id ASC';
              
        $result = $this->db->query($sql)->result();

        return $result;

        }catch (Exception $e) {
            print_r($e->getMessage());die;
       }
    }


 public function getInchargeDetail($campusid=null)
    {
       try{
             
           $sql = 'SELECT * FROM `mstcampus` WHERE isdeleted = 0'; 
        
          if($campusid !=''){
                 $sql .= '  AND `mstcampus`.campusid='.$campusid;
             }
                      
        $result = $this->db->query($sql)->result()[0];

        return $result;

        }catch (Exception $e) {
            print_r($e->getMessage());die;
       }
    }



     

    

}