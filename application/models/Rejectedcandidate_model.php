<?php 

/**
* Dashboard Model
*/
class Rejectedcandidate_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}


  public function getTeamlist()
  {
    try{
     
      $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No' ORDER BY `officename`";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


  public function getBatchlist()
  {
    try{
     
      $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }



  public function getCampus()
  {
    try{
     
      $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }



  public function getSelectedCandidate()
  {
    
    try{

     $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.campustype,`mstcampus`.`campusname`,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`mstcategory`.`categoryname`, `lpooffice`.officename, `mstbatch`.batch,`staff`.name, `edcomments`.status,`edcomments`.comments,`tbl_generate_offer_letter_details`.`filename`,tbl_offers_othersdocument.staffid, 
     `tbl_sendofferletter`.`flag`,(CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender, GROUP_CONCAT(tbl_offers_othersdocument.encrypteddocumentname) as otherfiles FROM `tbl_candidate_registration`

     left join `tbl_candidate_hrscore` ON `tbl_candidate_hrscore`.candidateid =`tbl_candidate_registration`.candidateid
     left join `mstcategory` ON `mstcategory`.id = `tbl_candidate_registration`.categoryid
     left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid 
     LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
     left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid 
     left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid 
     left join `edcomments` ON  `tbl_candidate_registration`.candidateid  = `edcomments`.candidateid
     left join `tbl_generate_offer_letter_details` ON `tbl_candidate_registration`.candidateid = `tbl_generate_offer_letter_details`.candidateid
     left join `tbl_sendofferletter` ON `tbl_candidate_registration`.candidateid = `tbl_sendofferletter`.candidateid 
     left JOIN tbl_offers_othersdocument ON tbl_candidate_registration.candidateid = tbl_offers_othersdocument.candidateid 
     Where `tbl_candidate_registration`.teamid !=''  
     AND `tbl_candidate_hrscore`.hrscore > 1
     AND `tbl_candidate_registration`.joinstatus <> 1
     AND  `tbl_generate_offer_letter_details`.flag='Generate' 
     AND `edcomments`.flag = 2 
     AND `edcomments`.send_hrd_mail_status = 1 
     And `tbl_candidate_registration`.rejected = 0
     "; 

     if ($this->loginData->RoleID==17) {
       $sql .= " AND `tbl_candidate_registration`.categoryid IN(2,3) "; 
     }else if ($this->loginData->RoleID==16) {
       $sql .= " AND `tbl_candidate_registration`.categoryid =1 "; 
     }else{
      $sql .= " AND `tbl_candidate_registration`.categoryid IN(1,2,3)"; 
    }

    $sql .=" and tbl_candidate_registration.rejected = 0 ";
    $sql .= "GROUP BY tbl_candidate_registration.candidateid ORDER BY `tbl_candidate_registration`.createdon DESC ";
    // echo $sql; die;
    $res = $this->db->query($sql)->result();

    return $res;
    
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function getSelectedExecutive(){
  try{
    $sql = "SELECT staff.staffid, staff.name, staff.emailid, lpooffice.officename, a.edstatus, a.transid, a.sendflag, a.filename, (CASE WHEN staff.gender =1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' END) as gender FROM tbl_offer_of_appointment a left join staff on staff.staffid=a.staffid left join lpooffice on lpooffice.officeid=a.proposed_officeid WHERE a.edstatus=0 AND a.personnelId=".$this->loginData->staffid;
    $res = $this->db->query($sql)->result();
    return $res;
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function getRecruitersList()
{
  
  try{

    $sql = "SELECT  `staff`.staffid, `staff`.name FROM `mapping_campus_recruiters` Inner join staff on `mapping_campus_recruiters`.`recruiterid`= `staff`.staffid ";
    $res = $this->db->query($sql)->result();
    return $res;
    
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getComments()
{
  
  try{

    $sql = "SELECT  `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `lpooffice`.officename, `mstbatch`.batch,`staff`.name,
    `edcomments`.comments FROM `tbl_candidate_registration`
    left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid
    left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid
    left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid 
    left join `edcomments` ON `edcomments`.candidateid=`tbl_candidate_registration`.candidateid 
    

    ";
    

    $res = $this->db->query($sql)->result();
    return $res;
    
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getCandidateDetails($token)
{
  
  try{

   $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid, `tbl_candidate_registration`.mobile,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_generate_offer_letter_details`.filename,`tbl_generate_offer_letter_details`.doj,lpooffice.officename,COALESCE(NULLIF(tbl_candidate_registration.otherspecialisation,''), NULLIF(tbl_candidate_registration.ugspecialisation,''), NULLIF(tbl_candidate_registration.pgspecialisation,'') ) stream
   FROM `tbl_candidate_registration` LEFT join `tbl_generate_offer_letter_details` ON `tbl_candidate_registration`.candidateid  = `tbl_generate_offer_letter_details`.candidateid
   left join lpooffice on tbl_candidate_registration.teamid=lpooffice.officeid

   Where `tbl_candidate_registration`.candidateid = '".$token."' "; 

   $result = $this->db->query($sql)->row();

   return $result;
   
 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


     /**
   * Method get_Category() display the category of candidate
   * @access  public
   * @param Null
   * @return  row
   */

     public function get_Category($token)
     {
       try
       {

        $sql="SELECT
        tbl_candidate_registration.name
        FROM
        tbl_candidate_registration
        where candidateid = $token";
  // echo $sql;die;

      //$query=$this->db->get();
        return  $this->db->query($sql)->row();

      }

      catch (Exception $e) {
       print_r($e->getMessage());die;
     }
     
   }



   /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

   public function get_staffDetails($token)
   {
     try
     {

      $sql="SELECT
      staff.name,
      staff.emailid,
      staff.emp_code,
      staff.candidateid,
      staff.contact,
      staff_transaction.staffid,
      staff_transaction.trans_status,
      staff_transaction.date_of_transfer,
      staff_transaction.new_office_id,
      staff_transaction.new_designation,
      staff.reportingto,
      staff.doj_team,
      lpooffice.officename,
      lp_old.officename as oldofficename,
      msdesignation.desname,
      staff_transaction.date_of_transfer,
      tbl_offer_of_appointment.doj as joiningdate,
      tbl_offer_of_appointment.filename,
      staff_transaction.effective_date as releavingdate

      FROM
      staff_transaction
      INNER JOIN
      (
      SELECT
      staff_transaction.staffid,
      staff_transaction.date_of_transfer,
      MAX(
      staff_transaction.date_of_transfer
      ) AS MaxDate
      FROM
      staff_transaction
      GROUP BY
      staffid
    ) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
    left join lpooffice as lp_old on staff_transaction.old_office_id=lp_old.officeid
    left join msdesignation on staff_transaction.new_designation=msdesignation.desid
    left join tbl_offer_of_appointment on staff_transaction.staffid = `tbl_offer_of_appointment`.`staffid`
    left join lpooffice on  tbl_offer_of_appointment.proposed_officeid=lpooffice.officeid
    INNER JOIN
    `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
    `staff`.staffid =$token
    ORDER BY
    `staff`.name ASC
    ";
  // echo $sql;die;

      //$query=$this->db->get();
    return  $this->db->query($sql)->row();

  }

  catch (Exception $e) {
   print_r($e->getMessage());die;
 }
 
}

}