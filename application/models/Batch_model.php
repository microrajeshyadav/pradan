<?php 

/**
* Dashboard Model
*/
class Batch_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	/**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try
    {
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}


	public function index()
	{

	}

	public function campusList()
	{
    try
    {
		$sql = "SELECT * FROM `mstcampus`";
		$res = $this->db->query($sql)->result();

		return $res;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}



    public function getBatch()
    {
    
        try{

            $sql = "SELECT a.*,b.financialyear, case when stage = 1 then 'Will Join' when stage = 2 then 'Ongoing' when stage = 3  then 'Gratuated'  else 'Will Join' end batchstage FROM `mstbatch` as a
				   inner join `mstfinancialyear` as b ON b.id = a.financial_year
                  WHERE a.`isdeleted`='0' ORDER BY createdon DESC";

            $result = $this->db->query($sql)->result();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }


    public function getfinancialyear()
    {
    
        try{

            $sql = "SELECT * FROM `mstfinancialyear` WHERE `isdeleted`='0'";

            $result = $this->db->query($sql)->result();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }

	
}