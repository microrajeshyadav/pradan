<?php

/**
 * Api model for healthrise
 */
class Api_model extends CI_Model
{
	private $user;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
	}

	public function authenticateApiUser()
	{	
		$requested_method = $this->input->server('REQUEST_METHOD');
		if($requested_method != "POST")
		{
			return "ERROR! Request method not supported. Please use HTTP POST method";
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		if($username == "" || $password == "" )
		{
			return "Please specify username and password in the request";
		}

		$this->db->where('Username',$username);
		$this->db->where('Password', md5($password));
		// $this->db->where('user_role', $user_role);
		$query = $this->db->get('mstuser');


		// Error executing query
		if(!$query)
		{
			$errObj   = $this->db->error();
			$errNo = $errObj->code;
			$errMess = $errObj->message;
			return "ERROR: Login error: ".$errNo .' : '.$errMess;
		}

		// Check if corresponding user founds
		if($query->num_rows() != 1)
		{
			return "ERROR: Incorrect username/password. Please try again.";
		}
		
		$this->user = $query->result()[0];
		return $query->result()[0];
	}

	public function get_masters($user)
	{

		$this->user = $user;
		$content = array();
		$this->db->trans_start();

		/**
		 * ASHA
		 */
		$this->db->where('UserID', $this->user->UserID);
		$content['mappingashauser'] = $this->db->get('mappingashauser')->result();

		$query = "select c.* from mstuser a 
		inner join mappingashauser b 
		on a.UserID = b.UserID
		inner join mappingashavillage c 
		on b.AshaID = c.AshaID
		where a.UserID = ?";
		$content['mappingashavillage'] = $this->db->query($query, [$this->user->UserID])->result();

		$query = "select c.* from mstuser a 
		inner join mappingashauser b 
		on a.UserID = b.UserID
		inner join mstasha c 
		on b.AshaID = c.AshaID
		where a.UserID = ?";
		$content['mstasha'] = $this->db->query($query, [$this->user->UserID])->result();

		$dateFields = $this->get_date_fields('mstasha');

		foreach ($content['mstasha'] as $row) {
			foreach($dateFields as $date_field)
			{
				$first_date = new DateTime("1900-01-01");
				$second_date = new DateTime($row->$date_field);

				$difference = $first_date->diff($second_date);
				$row->$date_field = $difference->days;
			}
		}

	//	print_r($content['mstasha']); die();
		/**
		 * ORW
		 */

		$this->db->where('UserID', $this->user->UserID);
		$content['mappingorwuser'] = $this->db->get('mappingorwuser')->result();

		$query = "select c.* from mstuser a
		inner join mappingorwuser b 
		on a.UserID = b.UserID
		inner join mappingorwvillage c 
		on b.ORWID = c.ORWID
		where a.UserID = ?";
		$content['mappingorwvillage'] = $this->db->query($query, [$this->user->UserID])->result();

		$query = "select c.* from mstuser a
		inner join mappingorwuser b 
		on a.UserID = b.UserID
		inner join mstorw c 
		on b.ORWID = c.ORWID
		where a.UserID = ?";
		$content['mstorw'] = $this->db->query($query, [$this->user->UserID])->result();

		/**
		 * Common tables
		 */
		$content['mstanm'] = $this->db->get('mstanm')->result();
		$content['mstadherence'] = $this->db->get('mstadherence')->result();
		$content['mstadvice'] = $this->db->get('mstadvice')->result();
		$content['mstclinicaltest'] = $this->db->get('mstclinicaltest')->result();
		$content['mstcommon'] = $this->db->get('mstcommon')->result();
		$content['mstcounselling'] = $this->db->get('mstcounselling')->result();
		$content['mstdmtest'] = $this->db->get('mstdmtest')->result();
		$content['mstdiagnosis'] = $this->db->get('mstdiagnosis')->result();
		$content['mstevidence'] = $this->db->get('mstevidence')->result();
		$content['mstfacilitytype'] = $this->db->get('mstfacilitytype')->result();
		$content['mstnotsatisfiedreason'] = $this->db->get('mstnotsatisfiedreason')->result();
		$content['mstpatientstatus'] = $this->db->get('mstpatientstatus')->result();
		$content['mstradiogroupvalues'] = $this->db->get('mstradiogroupvalues')->result();
		$content['mstreason'] = $this->db->get('mstreason')->result();
		foreach($content['mstreason'] as $row)
		{
			$row->Reason = str_replace("'", "`",  $row->Reason);
		}

		$content['mstscreeningstatus'] = $this->db->get('mstscreeningstatus')->result();
		$content['msttesttype']	= $this->db->get('msttesttype')->result();
		$content['msttreatmentstatus'] = $this->db->get('msttreatmentstatus')->result();
		$content['mstusestatus'] = $this->db->get('mstusestatus')->result();
		$content['mstcenter'] = $this->db->get('mstcenter')->result();
		$content['mstcenterlevel'] = $this->db->get('mstcenterlevel')->result();
		

		/**
		 * Tables that depend upon village Code
		 */

		if ($this->user->RoleID == 10) {

			/**
			 * Case of ASHA user
			 */

			$query = "select d.* from mstuser a 
			inner join mappingashauser b 
			on a.UserID = b.UserID
			inner join mappingashavillage c 
			on b.AshaID = c.AshaID
			inner join mappingcentervillage d 
			on c.VillageCode = d.VillageCode
			where a.UserID = ?";
			$content['mappingcentervillage'] = $this->db->query($query, [$this->user->UserID])->result();

		 	$query = "select e.* from mstuser a 
			inner join mappingashauser b on a.UserID = b.UserID
			inner join mappingashavillage c on b.AshaID = c.AshaID
			inner join mappingcentervillage d on c.VillageCode = d.VillageCode
			inner join mstcenter e on d.CenterID = e.CenterID
			where a.UserID = ?";
			$content['mstcenter'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select d.VillageUID,d.StateCode,d.DistrictCode,d.BlockCode,d.PanchayatCode,d.VillageCode,d.Name,d.Name_H,d.Name_SN,d.Age15_29M,d.Age15_29F,d.Age30_70M,d.Age30_70F,d.IsDeleted from mstuser a 
			inner join mappingashauser b 
			on a.UserID = b.UserID
			inner join mappingashavillage c 
			on b.AshaID = c.AshaID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			where a.UserID = ?";
			$content['mstvillage'] = $this->db->query($query, [$this->user->UserID])->result();	

			$query = "select e.PanchayatUID,e.StateCode,e.DistrictCode,e.BlockCode,e.PanchayatCode,e.Name,e.Name_H,e.Name_SN,e.IsDeleted from mstuser a 
			inner join mappingashauser b 
			on a.UserID = b.UserID
			inner join mappingashavillage c 
			on b.AshaID = c.AshaID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mstpanchayat e 
			on d.PanchayatCode = e.PanchayatCode
			where a.UserID = ?";
			$content['mstpanchayat'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select e.BlockUID,e.StateCode,e.DistrictCode,e.BlockCode,e.Name,e.Name_H,e.Name_SN,e.IsDeleted from mstuser a 
			inner join mappingashauser b on a.UserID = b.UserID
			inner join mappingashavillage c on b.AshaID = c.AshaID
			inner join mstvillage d on c.VillageCode = d.VillageCode
			inner join mstblock e on d.BlockCode = e.BlockCode
			where a.UserID = ?";
			$content['mstblock'] = $this->db->query($query, [$this->user->UserID])->result();

			
			$query = "select e.* from mstuser a 
			inner join mappingashauser b 
			on a.UserID = b.UserID
			inner join mappingashavillage c 
			on b.AshaID = c.AshaID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mstdistrict e 
			on d.DistrictCode = e.DistrictCode
			where a.UserID = ?";
			$content['mstdistrict'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select e.* from mstuser a 
			inner join mappingashauser b 
			on a.UserID = b.UserID
			inner join mappingashavillage c 
			on b.AshaID = c.AshaID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mststate e 
			on d.StateCode = e.StateCode
			where a.UserID = ?";
			$content['mststate'] = $this->db->query($query, [$this->user->UserID])->result();

		}else if ($this->user->RoleID == 3) {

			/**
			 * Case of ORW user
			 */

			$query = "select d.* from mstuser a
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mappingcentervillage d 
			on c.VillageCode = d.VillageCode
			where a.UserID = ?";
			$content['mappingcentervillage'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select e.* from mstuser a
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mappingcentervillage d 
			on c.VillageCode = d.VillageCode
			inner join mstcenter e 
			on d.CenterID = e.CenterID
			where a.UserID = ?";
			$content['mstcenter'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select d.VillageUID,d.StateCode,d.DistrictCode,d.BlockCode,d.PanchayatCode,d.VillageCode,d.Name,d.Name_H,d.Name_SN,d.Age15_29M,d.Age15_29F,d.Age30_70M,d.Age30_70F,d.IsDeleted from mstuser a 
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			where a.UserID = ?";
			$content['mstvillage'] = $this->db->query($query, [$this->user->UserID])->result();	

			$query = "select e.PanchayatUID,e.StateCode,e.DistrictCode,e.BlockCode,e.PanchayatCode,e.Name,e.Name_H,e.Name_SN,e.IsDeleted from mstuser a 
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mstpanchayat e 
			on d.PanchayatCode = e.PanchayatCode
			where a.UserID = ?";
			$content['mstpanchayat'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select  e.BlockUID,e.StateCode,e.DistrictCode,e.BlockCode,e.Name,e.Name_H,e.Name_SN,e.IsDeleted from mstuser a 
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mstblock e 
			on d.BlockCode = e.BlockCode
			where a.UserID = ?";
			$content['mstblock'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select  e.BlockUID,e.StateCode,e.DistrictCode,e.BlockCode,e.Name,e.Name_H,e.Name_SN,e.IsDeleted from mstuser a 
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mstdistrict e 
			on d.DistrictCode = e.DistrictCode
			where a.UserID = ?";
			$content['mstblock'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select e.* from mstuser a 
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mstdistrict e 
			on d.DistrictCode = e.DistrictCode
			where a.UserID = ?";
			$content['mstdistrict'] = $this->db->query($query, [$this->user->UserID])->result();

			$query = "select e.* from mstuser a 
			inner join mappingorwuser b 
			on a.UserID = b.UserID
			inner join mappingorwvillage c 
			on b.ORWID = c.ORWID
			inner join mstvillage d 
			on c.VillageCode = d.VillageCode
			inner join mststate e 
			on d.StateCode = e.StateCode
			where a.UserID = ?";
			$content['mststate'] = $this->db->query($query, [$this->user->UserID])->result();

			
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return "ERROR: Error in transaction";					
		}

		return $content;

	}

	public function uploadData($user)
	{
		$this->user = $user;

		$data = $this->input->post('data');

		if($data == null || $data == "")
		{
			return "ERROR: Please send data with request";	
		}

		$data_array = json_decode($data);

		if($data_array == null)
		{
			return "ERROR: Please send properly formatted json";
		}

		$this->db->trans_start();

		foreach ($data_array as $tableName => $tableData) {

			switch(strtolower($tableName))
			{
				case "tblpatientclinicaltestdetails":
				$this->tblpatientclinicaltestdetails($tableData);
				break;

				case "tblpatientfollowup":
				$this->tblpatientfollowup($tableData);
				break;

				case "tblpatientreffrredfollowup":
				$this->tblpatientreffrredfollowup($tableData);
				break;

				case "tblpatientregistrationdetails":
				$this->tblpatientregistrationdetails($tableData);
				break;

				case "tbl_eclinic":
				$this->tbl_eclinic($tableData);
				break;

				case "tbl_eclinic_suggestion_advice":
				$this->tbl_eclinic_suggestion_advice($tableData);
				break;

				default:
				$this->db->trans_complete();
				return "ERROR: Unknown table $tableName in data.".__LINE__;
				
			}
		}


		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			return "ERROR:error in transaction";
		}

		return array("status"=>"success");

	}

	private function tblpatientclinicaltestdetails($data)
	{
		$dateFields = $this->get_date_fields(__FUNCTION__);

		foreach ($data as $row) {
			
			$row = $this->clean_row($row, $dateFields);
			
			$row->IsUploaded = 1;
			$row->UploadedBy = $this->user->UserID;
			$row->UploadedOn = date("Y-m-d");
			
			$this->db->where('PatientClinicalTestGUID', $row->PatientClinicalTestGUID);
			$result = $this->db->get('tblpatientclinicaltestdetails')->result();
			if (count($result) < 1) {
				// insert
				unset($row->PatientClinicalTestUID);
				$this->db->insert('tblpatientclinicaltestdetails', $row);
			}else{
				// update
			
				$this->db->where('PatientClinicalTestGUID', $row->PatientClinicalTestGUID);
				$this->db->update('tblpatientclinicaltestdetails', $row);
			}

		}
	}

	private function tblpatientfollowup($data)
	{
		$dateFields = $this->get_date_fields(__FUNCTION__);

		foreach ($data as $row) {

			$row = $this->clean_row($row, $dateFields);

			$row->UploadedBy = $this->user->UserID;
			$row->UploadedOn = date("Y-m-d");

			$row->IsEdited = 1;
			$row->IsUploaded = 1;
			$row->IsOnline = 0;
			$row->IsDeleted = 0;

			
			$this->db->where('PatientFollowUpGUID', $row->PatientFollowUpGUID);
			$result = $this->db->get('tblpatientfollowup')->result(); 
			if (count($result) < 1) {
				// insert
				unset($row->PatientFollowUpUID);
				$this->db->insert('tblpatientfollowup', $row);
			}else{
				// update
				//echo "Amit"; die;
				unset($row->PatientFollowUpUID);
				$this->db->where('PatientFollowUpGUID', $row->PatientFollowUpGUID);
				$this->db->update('tblpatientfollowup', $row);
			}

		}
	}

	private function tblpatientreffrredfollowup($data)
	{
		$dateFields = $this->get_date_fields(__FUNCTION__);

		foreach ($data as $row) {

			$row = $this->clean_row($row, $dateFields);

			$row->UploadedBy = $this->user->UserID;
			$row->UploadedOn = date("Y-m-d");

			$row->IsOnline = 0;
			$row->IsDeleted = 0;

			$this->db->where('PatientReferredFollowUpGUID', $row->PatientReferredFollowUpGUID);
			$result = $this->db->get('tblpatientreffrredfollowup')->result();
			if (count($result) < 1) {
				// insert
				unset($row->PatientReferredFollowUpUID);
				$this->db->insert('tblpatientreffrredfollowup', $row);
			}else{
				// update
				unset($row->PatientReferredFollowUpUID);
				$this->db->where('PatientReferredFollowUpGUID', $row->PatientReferredFollowUpGUID);
				$this->db->update('tblpatientreffrredfollowup', $row);
			}

		}
	}

	private function tblpatientregistrationdetails($data)
	{
		$dateFields = $this->get_date_fields(__FUNCTION__);

		foreach ($data as $row) {

			$row = $this->clean_row($row, $dateFields);

			$row->UploadedBy = $this->user->UserID;
			$row->UploadedOn = date("Y-m-d");

			$this->db->where('PatientGUID', $row->PatientGUID);
			$result = $this->db->get('tblpatientregistrationdetails')->result();
			if (count($result) < 1) {
				// insert
				unset($row->PatientUID);
				$this->db->insert('tblpatientregistrationdetails', $row);
			}else{
				// update
				$this->db->where('PatientGUID', $row->PatientGUID);
				$this->db->update('tblpatientregistrationdetails', $row);
			}

		}
	}

	private function clean_row($row = array(), $dateFields = array())
	{
		$cleanRow = new stdclass;
		foreach ($row as $key => $value) 
		{

			if (trim($value) == "") {
				$cleanRow->{$key} = NULL;
				continue;
			}

			if (in_array(strtolower($key), array_map('strtolower',$dateFields))) {
				$cleanRow->{$key} = date('Y-m-d', strtotime("1900-01-01 + $value days"));
			}else{
				$cleanRow->{$key} = $value;
			}

		}
		
		return $cleanRow;
	}

	private function null_to_empty_string($table_name)
	{
		$tableFields = $this->db->list_fields($table_name);
		$dateFields = $this->get_date_fields($table_name);
// print_r($dateFields); die();
		
		$returnArray = array();
		foreach ($tableFields as $field) {
			
			$field_alias = $field;

			if (in_array(strtolower($field), $dateFields)) {
				$field = "TIMESTAMPDIFF(DAY, '1900-01-01', `$field`)";
				$returnArray[] = "IFNULL($field,'') as `$field_alias`";
			}else{
				$returnArray[] = "IFNULL(`$field`,'') as `$field`";
			}

		}
		return implode(' , ', $returnArray);

	}

	public function get_table_data($user, $table_name, $created_by=NULL)
	{
		$this->user = $user;

		$content = array();

		$this->db->trans_start();

		if ($created_by == NULL) {
			$created_by = "CreatedBy";
		}

		$query = "select ".$this->null_to_empty_string($table_name)." from $table_name where $created_by = " . $this->user->UserID;
		// echo $query; die();
		$resultSet = $this->db->query($query)->result();

		$content[$table_name] = $resultSet;

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			return "ERROR:error in transaction";
		}

		return $content;
	}

	private function get_date_fields($table_name)
	{

		$dateFields = array();

		$fields = $this->db->field_data($table_name);
		foreach ($fields as $field) {
			if ($field->type == "date") {
				// $dateFields[] = strtolower($field->name);
				$dateFields[] = strtolower($field->name);
			}
		}

		//print_r($dateFields);
		return $dateFields;
	}

	private function tbl_eclinic_suggestion_advice($data)
	{
		$dateFields = $this->get_date_fields(__FUNCTION__);

		foreach ($data as $row) {

			$row = $this->clean_row($row, $dateFields);

			$row->UploadedBy = $this->user->UserID;
			$row->UploadedOn = date("Y-m-d H:i:s");
			$row->CreatedBy  = $this->user->UserID;
			$row->CreatedOn  = date("Y-m-d H:i:s");

			$this->db->where('eclinic_suggestion_guid', $row->eclinic_suggestion_guid);
			$result = $this->db->get('tbl_eclinic_suggestion_advice')->result();
			// print_r($result); die();
			if (count($result) < 1) {
				// insert
				unset($row->eclinic_suggestion_uid);
				$this->db->insert('tbl_eclinic_suggestion_advice', $row);
			}else{
				// update
				unset($row->eclinic_suggestion_uid);
				$this->db->where('eclinic_suggestion_guid', $row->eclinic_suggestion_guid);
				$this->db->update('tbl_eclinic_suggestion_advice', $row);
			}

		}
	}

	private function tbl_eclinic($data)
	{
		$dateFields = $this->get_date_fields(__FUNCTION__);

		foreach ($data as $row) {

			$row = $this->clean_row($row, $dateFields);

			$row->UploadedBy = $this->user->UserID;
			$row->UploadedOn = date("Y-m-d H:i:s");
			$row->CreatedBy  = $this->user->UserID;
			$row->CreatedOn  = date("Y-m-d H:i:s");

			$this->db->where('eclinic_patient_guid', $row->eclinic_patient_guid);
			$result = $this->db->get('tbl_eclinic')->result();
			if (count($result) < 1) {
				// insert
				// unset($row->PatientUID);
				$this->db->insert('tbl_eclinic', $row);
			}else{
				// update
				$this->db->where('eclinic_patient_guid', $row->eclinic_patient_guid);
				$this->db->update('tbl_eclinic', $row);
			}

		}
	}

}
