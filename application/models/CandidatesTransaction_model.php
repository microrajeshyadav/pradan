<?php 

/**
* Dashboard Model
*/
class CandidatesTransaction_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Countclinicaldetails()
	{
		try{
			$this->db->select('count(Name) as clinical');
			$this->db->from('tblpatientregistrationdetails as prd ');
			$this->db->join('tblpatientclinicaltestdetails as pc', 'prd.PatientGUID = pc.PatientGUID');
			$this->db->where('prd.IsDeleted', 0);
			return $this->db->count_all_results();

		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
		
	}

	public function index()
	{

	}

	public function campusList()
	{
        try{
		$sql = "SELECT * FROM `mstcampus`";
		$res = $this->db->query($sql)->result();

		return $res;

        }catch (Exception $e) {
     print_r($e->getMessage());die;
        }
	}

	public function patients_first_followup_done()
	{
        try{
		$sql = "SELECT * FROM `tblpatientregistrationdetails` a inner join ( select * from tblpatientreffrredfollowup where VisitNo = 1 group by PatientGUID) b on b.PatientGUID = a.patientguid where a.Referred = 1 and b.VisitNo = 1";
		$res['first_followup_done'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `tblpatientregistrationdetails` WHERE Referred = 1";
		$res['referred_patients'] = $this->db->query($sql)->result();

		return $res;

        }catch (Exception $e) {
        print_r($e->getMessage());die;
        }
	}

	public function referred_patients_who_went_to_the_clinic()
	{
        try{
		$sql = "SELECT * FROM `tblpatientregistrationdetails` a inner join ( select * from tblpatientreffrredfollowup where VisitNo = 1 group by PatientGUID) b on b.PatientGUID = a.patientguid where a.Referred = 1 and b.VisitNo = 1";
		$res['first_followup_done'] = $this->db->query($sql)->result();

		$sql = "SELECT
    distinct PatientGUID
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)";
		$res['patients_who_went_to_the_clinic'] = $this->db->query($sql)->result();

		return $res;
	}

	public function referred_patients_who_resulted_positive()
	{
		$sql = "SELECT
    DISTINCT PatientGUID
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)

and DC_Daignosis > 0";
		$res['patients_positive_result'] = $this->db->query($sql)->result();

		$sql = "SELECT
    distinct PatientGUID
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)";
		$res['patients_who_went_to_the_clinic'] = $this->db->query($sql)->result();

		return $res;
	}

	public function hypertension_diabetes_both()
	{
		$sql = "SELECT
    DC_Daignosis, count(*) as count
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)
and DC_Daignosis > 0
group by DC_Daignosis";

$res = $this->db->query($sql)->result();

return $res;
	}

	public function referred_patients_who_did_not_go_to_the_clinic()
	{
		$sql = "SELECT * FROM `tblpatientregistrationdetails` a inner join ( select * from tblpatientreffrredfollowup where VisitNo = 1 group by PatientGUID) b on b.PatientGUID = a.patientguid where a.Referred = 1 and b.VisitNo = 1";
		$res['first_followup_done'] = $this->db->query($sql)->result();

		$sql = "SELECT
        count(*) as count
    FROM
        `tblpatientregistrationdetails` a
    left JOIN
    tblpatientclinicaltestdetails b on
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.patientguid is null";
		$res['patients_who_did_not_go_to_the_clinic'] = $this->db->query($sql)->result();

		return $res;
	}

}catch (Exception $e) {
     print_r($e->getMessage());die;
   }

}