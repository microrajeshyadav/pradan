<?php 

/**
* Joining_of_DA Model
*/
class Transfer_claim_expense_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}





/**
   * Method getCandidate_BDFFormSubmit() get BDF Form Submit Details.
   * @access  public
   * @param Null
   * @return  Array
   *  @return Status 2 - Team Join, 1- Make complete Staff
   */

  public function getCandidate_BDFFormSubmit($teamid=null)
  {
    
    try{
           $sql = "SELECT `tblvd`.comment,`tblvd`.status,`tblvd`.approvedby,`tblvd`.approvedon, `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid
            left join `tbl_verification_document` as `tblvd` ON `tblvd`.candidateid =`tbl_candidate_registration`.candidateid
            Where `tbl_candidate_registration`.BDFFormStatus=1  AND `tbl_candidate_registration`.joinstatus=2 ";  //die;

           if (isset($teamid) && $teamid !='') {
            $sql .= " AND `tbl_candidate_registration`.teamid =".$teamid;
           }
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandateVerified() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandateVerified($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch,`tbl_candidate_registration`.encryptmetriccertificate,`tbl_candidate_registration`.encrypthsccertificate,`tbl_candidate_registration`.encryptugcertificate,`tbl_candidate_registration`.encryptpgcertificate,`tbl_candidate_registration`.encryptothercertificate FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=2 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getexpense_claimdetail() get single staff exp  Details.
   * @access  public
   * @param Null
   * @return  row
   */

 public function getexpense_claimdetail($token)
  {
    
    try{
           $sql = "SELECT
    `id`,
    `travel_fare`,
    `local_conveyance`,
    `conveyance_of_personal_train`,
    `conveyance_of_personal_truck`,
    `conveyance_of_personal_other`,
    `conveyance_of_vehicle_car`,
    `conveyance_of_vehicle_motor`,
    `conveyance_of_vehicle_bicycle`,
    `total_vehicle_con`,
    `lumpsum_transfer_allowance`,
    `less_amount_of_transfer_ta_advance`,
    `grand_total`,`net_amount_payable`,
    `tdate`
    
FROM
    `tbl_transfer_expenses_claim` where `staffid`=$token";  // die;
        
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /* 
     * Method get_staff_finance_detail() get single finance  Details.
   * @access  public
   * @param office argument
   * @return  row
  */

public function get_staff_finance_detail($officeid)
   {
      try
    {

      $sql="SELECT c.staffid, c.name, c.emailid,c.encrypted_signature FROM mstuser as a LEFT JOIN `staff` as c on a.staffid = c.staffid WHERE c.new_office_id = $officeid AND a.RoleID = 20";

       // echo $sql; die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

/* 
     * Method get_staff_newsupervisor_detail() get single finance  Details.
   * @access  public
   * @param office argument
   * @return  row
  */

public function get_newsupervisor_detail($staffid)
   {
      try
    {

      $sql="SELECT c.staffid, c.name, c.emailid,c.encrypted_signature FROM mstuser as a LEFT JOIN `staff` as c on a.staffid = c.staffid WHERE c.staffid = $staffid";

       // echo $sql; die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}



/**
   * Method getSingleCandidateDetails() get single candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSingleCandidateDetails($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateTeam() get single candidate team id.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandidateTeam($token)
  {
    
    try{
           $sql = "SELECT * FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=2 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method getDevelopmentApprenticeship() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getDevelopmentApprenticeship($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail($token)
  {
    
    try{

       $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=1 And `tblgnau`.status =1";  //die;
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail1($token)
  {
    
    try{

          $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where  `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=2 And `tblgnau`.status =2 AND `tblcanreg`.candidateid=".$token."";  
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method getNomieeDetail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomieeDetail($id)
  {
    
    try{

        $sql = "SELECT `tblnd`.general_nomination_id,`tblnd`.nominee_name,`sysr`.relationname,`tblnd`.nominee_age,`tblnd`.nominee_address  FROM `tbl_nominee_details` as `tblnd`  
       left join `sysrelation` as `sysr` ON `sysr`.id = `tblnd`.nominee_relation Where `tblnd`.general_nomination_id = $id";  
        //die;
        $result = $this->db->query($sql)->result();

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSubmittedBy($token)
  {
    
    try{
     $sql = 'SELECT * FROM `tbl_candidate_registration` Where `tbl_candidate_registration`.candidateid ='.$token.'';         
      $result = $this->db->query($sql)->result();
      return $result[0];
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateDetails($token)
  {
    
    try{
             $sql = 'SELECT `tbl_candidate_communication_address` .permanentstreet,`tbl_candidate_communication_address` .permanentcity,`tbl_candidate_communication_address` .permanentstateid,`tbl_candidate_communication_address` .permanentdistrict,`tbl_candidate_communication_address` .permanentpincode, tbl_generate_offer_letter_details.*,`state`.name FROM `tbl_candidate_communication_address` 
            left join `tbl_generate_offer_letter_details` ON `tbl_generate_offer_letter_details`.candidateid = `tbl_candidate_communication_address`.candidateid
            left join `state` ON `state`.id=`tbl_candidate_communication_address`.permanentstateid
                Where `tbl_candidate_communication_address`.candidateid ='.$token.'';  
          //echo $sql;die();
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  
/**
   * Method getJoiningReportDetail() get Joining Report Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportDetail($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_joining_report` Where candidateid = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandateWorkExperincecount($token)
  {
    
    try{

          $this->db->where('candidateid', $token);
         $res = $this->db->count_all_results('tbl_work_experience');

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceCandateVerified($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceVerifiedValue($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience_verified` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getVerifiedDetailes() get Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getVerifiedDetailes($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getverifiedstatus() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getverifiedstatus($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneralNominationFormPDF() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationFormPDF($token)
  {
    
    try{

         $sql = "SELECT status, filename FROM `tbl_general_nomination_and_authorisation_form` Where `candidateid` = $token"; //die;
       $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateWithAddressDetails() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateWithAddressDetails($token)
  {
    
    try{

          $sql = 'SELECT `tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname, `tbl_candidate_communication_address`.permanentstreet, `tbl_candidate_communication_address`.permanentcity, `tbl_candidate_communication_address`.permanentstateid, `tbl_candidate_communication_address`.permanentdistrict, `tbl_candidate_communication_address`.permanentpincode, `state`.name FROM `tbl_candidate_registration` 
         left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
          left join `state` ON `state`.id = `tbl_candidate_communication_address`.permanentstateid
                Where `tbl_candidate_registration`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getViewGeneralnominationform() get View General Nomination  Authorisation Form Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewGeneralnominationform($token)
  {
    
    try{

         $sql = 'SELECT * FROM `tbl_general_nomination_and_authorisation_form`         
                Where `tbl_general_nomination_and_authorisation_form`.candidateid ='.$token.'';
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getCountOtherDocument() get Count Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountOtherDocument($token)
  {
    
    try{
        
        $this->db->where('candidateid', $token);
        $result = $this->db->count_all_results('tbl_other_documents');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getOtherDocumentDetails() get Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocumentDetails($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_other_documents`         
                Where candidateid ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getNomineedetail() get Count Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountNominee($token)
  {
    
    try{
        
        $this->db->where('general_nomination_id', $token);
        $result = $this->db->count_all_results('tbl_nominee_details');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Select Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_nominee_details`         
                Where `tbl_nominee_details`.general_nomination_id ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $this->db->select('*');
      $this->db->from('sysrelation');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getOtherDocuments() get Document Name With Type List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocuments()
  {

    try{
      $this->db->select('*');
      $this->db->from('mstdocuments');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }



/**
   * Method getJoiningReportStatus() get Joining Report Status List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportStatus($token)
  {

    try{
      $this->db->select('status, filename');
      $this->db->from('tbl_joining_report');
      //$this->db->where('status', 1);
      $this->db->where('candidateid', $token);
      return $this->db->get()->result()[0];

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getSingleNameWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSingleNameWitnessDeclaration($id)
  {

    try{
           $sql = "SELECT `staffid`,`name` FROM `staff` as a WHERE `a`.staffid=".$id; 
            
            $result = $this->db->query($sql)->result()[0];

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getWitnessDeclaration($id)
  {

    try{
        $sql = "SELECT staff_transaction.staffid, staff_transaction.date_of_transfer, staff_transaction.new_office_id, staff_transaction.new_designation, staff.name FROM staff_transaction 
    INNER JOIN  staff ON staff_transaction.staffid = staff.staffid  
    INNER JOIN (SELECT staff_transaction.staffid,Max(staff_transaction.date_of_transfer) as MaxDate FROM staff_transaction GROUP BY staffid) as TMax ON staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
      AND staff_transaction.trans_status NOT IN('Resign','Termination','Retirement','Death') AND staff_transaction.`new_office_id`='".$id."' AND  staff_transaction.`new_designation` IN(4,10) ORDER BY staff_transaction.staffid ";  
            ///AND staff_transaction.`new_office_id`='".$id."';
            $result = $this->db->query($sql)->result();

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


  /**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



  /**
   * Method getGeneralNominationWithWitnessformFormUploadStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationWithWitnessformFormUploadStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.filename as filename FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



  /**
   * Method getJoiningReportFormPdfStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportFormPdfStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.filename as filename FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }

  public function transfer_claim_expense_detail($token)
  {
    
    try{
      $sql = "SELECT * FROM tbl_transfer_expenses_claim WHERE staffid=".$token;
      //echo $sql;die;
        $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}