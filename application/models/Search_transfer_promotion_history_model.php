<?php 

/**
* State Model
*/
class Search_transfer_promotion_history_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

		public function index()
	{
		
	}
	
	 public function staff_transfer_list()
    {
        try{

             $sql = "SELECT staffid,name, emp_code FROM staff  where status = 1 ORDER BY `emp_code` ASC";

            
            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
}
public function getOfficeList($p_office)
  {
     try
   {
    $this->db->select("officeid,officename");
      $this->db->from('lpooffice');
      $query=$this->db->get();
     // echo $this->db->last_query();
      //die();
      return $query->result();
    }
     catch (Exception $e) 
     {
       print_r($e->getMessage());die;
     }

  }

public function fetch_table($staff_id,$types,$froms,$tos)
{
 try{

 	$stff=$this->input->post('staff_id');
    $types=$this->input->post('types');
    $froms=$this->input->post('froms');
    $tos=$this->input->post('tos');


	$newDatefrom = $this->changedatedbformate($froms);
 	$newDateto = $this->changedatedbformate($tos);



if($this->input->post('staff_id')!='0')
{
  
          $paremeters = [];
    $sql="SELECT a.date_of_transfer,b.dateofleaving,a.trans_status, a.staffid, b.emp_code, b.name, c.officename as old_office_name, d.officename as new_office_name, f.desname as New_Designation,h.desname as Present_Designation FROM staff_transaction a left join staff b on a.staffid = b.staffid left join lpooffice c on a.old_office_id = c.officeid left join lpooffice d on a.new_office_id = d.officeid left join msdesignation f on a.old_designation= f.desid left join msdesignation h on a.new_designation=h.desid where 1=1 ";
    if ($this->input->post('staff_id') != null) 
    {
      $sql .= " and a.staffid = $stff";
      $parameteres[] = $stff;                   //$this->input->post('staff_id');
    
    }

    if ($this->input->post('froms') != null && $this->input->post('tos')) 
    {
      $sql .= " and a.date_of_transfer between '$newDatefrom' and '$newDateto' ";
      $parameteres[] =$newDatefrom;
      $parameteres[] =$newDateto ;
     
          }

    if ($this->input->post('types') != null && $this->input->post('types') != 'Sepertion') 
    {
      $sql .= " and a.trans_status = '$types' ";
      $parameteres[] =$types;  
                //$this->input->post('groupOfMaterialRadios');
 			//print_r($sql);
        	
    }


      if ($this->input->post('types') =='Sepertion') 
    {
      $sql .= " and a.trans_status  IN('Termination','Resign','Retirement','Death')";
      $parameteres[] =$types;  
                //$this->input->post('groupOfMaterialRadios');
     
    }



   
      $res2= $this->db->query($sql)->result();
   
      return $res2;
}


else
{

	  $sql1="SELECT a.date_of_transfer,b.dateofleaving,a.trans_status, a.staffid, b.emp_code, b.name, c.officename as old_office_name, d.officename as new_office_name, f.desname as New_Designation,h.desname as Present_Designation FROM staff_transaction a left join staff b on a.staffid = b.staffid left join lpooffice c on a.old_office_id = c.officeid left join lpooffice d on a.new_office_id = d.officeid left join msdesignation f on a.old_designation= f.desid left join msdesignation h on a.new_designation=h.desid where 1=1";


	  if ($this->input->post('froms') != null && $this->input->post('tos')) 
   		 {
		      $sql1 .= " and a.date_of_transfer between '$newDatefrom' and '$newDateto' ";
		      $parameteres[] =$newDatefrom;
		      $parameteres[] =$newDateto ;
          }
			
			if ($this->input->post('types') != null && $this->input->post('types') != 'Sepertion') 
			    {
			      $sql1 .= " and a.trans_status = '$types' ";
			      $parameteres[] =$types;            //$this->input->post('groupOfMaterialRadios');
			
			    }
   if ($this->input->post('types') =='Sepertion') 
    {
      $sql1 .= " and a.trans_status  IN('Termination','Resign','Retirement','Death')";
      $parameteres[] =$types;  
                //$this->input->post('groupOfMaterialRadios');
     
    }
     //print_r($sql1);die();
	  	$res3= $this->db->query($sql1)->result();

	    return $res3;
	
}
  }catch(Exception $e){
            print_r($e->getMessage());die();
        }
}




  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
  // echo substr($Date,$len,-4);
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
      $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



}