<?php 

/**
* State Model
*/
class Staff_midterm_review_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    public function get_candidaateid($staff_id)
    {
        try{
        $sql="select * from staff where staffid='$staff_id'";
        
        return  $this->db->query($sql)->row();
        }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
    }

/*
  getstaffname       
*/
	public function getstaffname()
	{
    try{

    $currentdate = date('Y-m-d');


		 $sql = "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,

    staff.gender,
    staff.emp_code,
    staff.candidateid,
     staff_transaction.id,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    staff.probation_status,
    msdesignation.level AS level,
    staff.joiningandinduction,
    staff.gender,

    (
        CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
    END
) AS gender,
staff_transaction.trans_flag,
(
    CASE
    WHEN staff_transaction.trans_flag = 0 THEN 'Save' 
    WHEN staff_transaction.trans_flag = 1 THEN 'Submitted By HR' 
    WHEN staff_transaction.trans_flag = 3 THEN 'Approved By TC ' 
    WHEN staff_transaction.trans_flag = 4 THEN 'Rejected by TC' 
    WHEN staff_transaction.trans_flag = 5 THEN 'Approved by ED' 
    WHEN staff_transaction.trans_flag = 6 THEN 'Rejected by ED' 
    WHEN staff_transaction.trans_flag = 7 THEN 'Approved by Personnal' 
    WHEN staff_transaction.trans_flag = 8 THEN 'Rejected by Personnal' 
    ELSE 'Proceess Initiate'
END
) AS flag
FROM
    staff_transaction
left JOIN(
    SELECT staff_transaction.staffid,
        staff_transaction.date_of_transfer,
        MAX(
            staff_transaction.date_of_transfer
        ) AS MaxDate
    FROM
        staff_transaction
    GROUP BY
        staffid
) AS TMax
ON
    `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
LEFT JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`
LEFT JOIN lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
LEFT JOIN msdesignation ON staff.`designation` = msdesignation.`desid`
WHERE 
staff_transaction.trans_status = CASE WHEN staff_transaction.trans_status = 'Midtermreview' then 'Midtermreview' else 'JOIN' END AND 
    msdesignation.`level` = 4 AND '".$currentdate."' <= CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date END  AND staff.status=1"; 

    $sql .=' ORDER BY `staff_transaction`.datetime desc';
    
// echo $sql; die;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}
  public function getstaffname1()
  {
    try{

    $currentdate = date('Y-m-d');

     $sql = "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,

    staff.gender,
    staff.emp_code,
    staff.candidateid,
     staff_transaction.id,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    staff.probation_status,
    msdesignation.level AS level,
    staff.joiningandinduction,
    staff.gender,

    (
        CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
    END
) AS gender,
staff_transaction.trans_flag,
(
    CASE
    WHEN staff_transaction.trans_flag = 0 THEN 'Save' 
    WHEN staff_transaction.trans_flag = 1 THEN 'Submitted By HR' 
    WHEN staff_transaction.trans_flag = 3 THEN 'Approved By TC ' 
    WHEN staff_transaction.trans_flag = 4 THEN 'Rejected by TC' 
    WHEN staff_transaction.trans_flag = 5 THEN 'Approved by ED' 
    WHEN staff_transaction.trans_flag = 6 THEN 'Rejected by ED' 
    WHEN staff_transaction.trans_flag = 7 THEN 'Approved by Personnal' 
    WHEN staff_transaction.trans_flag = 8 THEN 'Rejected by Personnal' 
    ELSE 'Not status here'
END
) AS flag
FROM
    staff_transaction
left JOIN(
    SELECT staff_transaction.staffid,
        staff_transaction.date_of_transfer,
        MAX(
            staff_transaction.date_of_transfer
        ) AS MaxDate
    FROM
        staff_transaction
    GROUP BY
        staffid
) AS TMax
ON
    `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`
INNER JOIN lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
INNER JOIN msdesignation ON staff.`designation` = msdesignation.`desid`
WHERE 
staff_transaction.trans_status = CASE WHEN staff_transaction.trans_status = 'Midtermreview' then 'Midtermreview' else 'JOIN' END AND 
    msdesignation.`level` = 4 AND '".$currentdate."' <= CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date END  AND staff.status=1"; 
  //staff_transaction.trans_status = CASE WHEN staff_transaction.trans_status = 'Midtermreview' then 'Midtermreview' else 'JOIN' END AND  
   // msdesignation.`level` = 4 AND '".$currentdate."' <= CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date
//END
    // if ($this->loginData->RoleID == 2) {
    //   $sql .= "WHERE staff.`reportingto` =".$this->loginData->staffid;  
    // }
 // $sql .= " GROUP BY `staff_transaction`.staffid  order by cast(staff.emp_code as UNSIGNED)";  
  //echo $sql; die;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}
public function getstaff($token)
  {
    try{

    $currentdate = date('Y-m-d');


     $sql = "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.emp_code,
      staff.emailid,
    staff.candidateid,
    staff_transaction.id,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff_transaction.reason,
    staff_transaction.old_office_id,
    staff_transaction.old_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    msdesignation.level AS levelname,
    staff.joiningandinduction FROM staff_transaction
INNER JOIN staff ON staff_transaction.staffid = `staff`.`staffid`
INNER JOIN lpooffice ON staff.new_office_id = lpooffice.`officeid`
INNER JOIN msdesignation ON staff.designation = msdesignation.`desid`
WHERE
    `staff`.`staffid` = $token 
"; 
//echo $sql;
  //staff_transaction.trans_status = CASE WHEN staff_transaction.trans_status = 'Midtermreview' then 'Midtermreview' else 'JOIN' END AND  
   // msdesignation.`level` = 4 AND '".$currentdate."' <= CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date
//END
    // if ($this->loginData->RoleID == 2) {
    //   $sql .= "WHERE staff.`reportingto` =".$this->loginData->staffid;  
    // }
 // $sql .= " GROUP BY `staff_transaction`.staffid  order by cast(staff.emp_code as UNSIGNED)";  
  //  echo $sql; die;
      return  $this->db->query($sql)->row();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  

    public function fetchdatas($token)
    { 
      try{

        $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
        $result = $this->db->query($sql);
        $rows = $result->num_rows();
        $tm='-1';
         if($rows==0)
         {
           return $tm;
         }
         else
         {
             
           $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
        
            $query=$this->db->query($sql);
         }
         }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

     //  

    }

 /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSupervisername($staffid)
  {
    
    try{
   $sql = "SELECT staff.reportingto FROM
    staff_transaction
INNER JOIN(
    SELECT staff_transaction.staffid,
        staff_transaction.date_of_transfer,
        MAX(
            staff_transaction.date_of_transfer
        ) AS MaxDate
    FROM
        staff_transaction
    GROUP BY
        staffid
) AS TMax
ON
    `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate
INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`


WHERE staff.staffid=".$staffid;
//echo $sql;

         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    /**
   * Method getSupervisername() get supervisername email id.
   * @access  public
   * @param Null
   * @return  row
   */
    

  public function superviser_email($staffid)
  {

    
    try{
   $sql = "SELECT staff.staffid,staff.name,staff.designation,staff.new_office_id,staff.emp_code,staff.emailid,lp.officename,msd.desname FROM `staff` 
   
   inner join lpooffice  as lp on staff.new_office_id=lp.officeid
   inner join msdesignation as msd on staff.designation=msd.desid

   
   where staff.`staffid`='$staffid'" ;

    // echo $sql;
    // die;
         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



    /**
   * Method getHrd administrator() get hrdadmonistrator email id.
   * @access  public
   * @param Null
   * @return  row
   */
    

  public function hrd_email()
  {

    
    try{
   $sql = "SELECT staff.emailid FROM `mstuser`  inner join staff on staff.staffid=mstuser.staffid WHERE `RoleID` = 16
 and IsDeleted=0";


         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    /**
   * Method get personal_email() get personnal email id.
   * @access  public
   * @param Null
   * @return  row
   */
    

  public function personal_email()
  {

    
    try{
   $sql = "SELECT *,staff.emailid as staff_email FROM `mstuser`  inner join staff on mstuser.staffid=staff.staffid WHERE `RoleID` = 17 and IsDeleted=0";


         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    /**
   * Method get ed_email() get ed email id.
   * @access  public
   * @param Null
   * @return  row
   */
    

  public function ed_email()
  {

    
    try{
   $sql = "SELECT * FROM `mstuser`  inner join staff on mstuser.staffid=staff.staffid  WHERE `RoleID` = 18
 and IsDeleted=0";
 // echo $sql;
 // die;


         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    /**
   * Method loginstaff() get Personnal User List.
   * @access  public
   * @param Null
   * @return  row
   */


  public function loginstaff($staffid)
  {
   
    
    try{
   $sql = "SELECT staff.staffid,staff.name,msdesignation.desname,staff.emailid FROM `staff` 
   inner join msdesignation on staff.designation=msdesignation.desid 
   where `staffid`=
".$staffid;


         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

    /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getProbationStatus($staffid)
  {
    
    try{
   $sql = "SELECT flag FROM `tbl_probation_review_performance` where `staffid`=".$staffid;

         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
      INNER join staff as st ON u.staffid = st.staffid
      WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0";

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function getExecutiveDirectorList()
  {
    
    try{
   $sql = "SELECT st.staffid as edstaffid,st.name,emp_code FROM `mstuser` as u 
      INNER join staff as st ON u.staffid = st.staffid
      WHERE u.`RoleID` = ".$transfer_to_id." AND u.`IsDeleted`=0";

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function get_staff_midtermreview_detail($token)
   {
      try
    {

      $sql="SELECT * FROM `staff_transaction` as a INNER JOIN(
      SELECT
      `staff_transaction`.staffid,
      MAX(
      `staff_transaction`.date_of_transfer
      ) AS MaxDate
      FROM
      staff_transaction
      GROUP BY
      staffid
    ) AS TMax
    ON
    `a`.staffid = TMax.staffid AND `a`.date_of_transfer = TMax.MaxDate  WHERE a.staffid=".$token;
       
    //echo $sql; die;

       //$query=$this->db->get();
       return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}



public function get_providentworkflowid($token)
{

  try{



$sql = "SELECT * FROM `tbl_workflowdetail`
    

     WHERE   tbl_workflowdetail.r_id=$token";
    
// echo $sql;


    $result = $this->db->query($sql)->result();

    //print_r($result);
    return $result;




  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function getmidtermmax($tablename,$incval){


    try{

        // echo $tablename;
        // echo $incval;  die;
        ////'staff_transaction',@a; 

      $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }

   

}