<?php 

/**
* State Model
*/
class Staff_list_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



  public function get_candidaateid($staff_id)
  {
    
    $sql="select * from staff where staffid='$staff_id'";
    
    return  $this->db->query($sql)->row();
  }


  public function getstaffname($isactive =null,$officeid =null)
  {
    $sql = '';
    $sql .= "SELECT
    staff.staffid,
    staff.name,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.children_allowance,
    staff.emp_code,
    staff.candidateid,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff.designation,
    staff.emailid,
    lpooffice.officename,
    msdesignation.desname,
    staff.joiningandinduction,
    sum(sep.seperation_status) as seperation_status,
    sum(sep.transstatus) as transstatus,
    sum(sep.transCRstatus) as transCRstatus,
    sum(sep.PromotionStatus) as PromotionStatus,
    staff.gender,
    (CASE
    WHEN staff.gender = 1 THEN 'Male'
    WHEN staff.gender = 2 THEN 'Female'
    WHEN staff.gender = 3 THEN 'Other'
    ELSE 'Not gender here'
    END) as gender,
    staff.status,
    (CASE
    WHEN flag = 0 THEN 'Save'
    WHEN flag = 1 THEN 'Submitted'
    WHEN flag = 3 THEN 'Approved'
    WHEN flag = 4 THEN 'Rejected'
    ELSE 'Not status here'
    END) as flag,
    `staff_transaction`.id as transid
    FROM
    staff 
    left JOIN `staff_transaction` ON `staff_transaction`.staffid = `staff`.`staffid`
    INNER JOIN
    (
    SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
    staff_transaction.date_of_transfer
    ) AS MaxDate
    FROM
    staff_transaction
    GROUP BY
    staffid
  ) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate  
  left JOIN
  (select staffid, CASE when `sep`.trans_status  = 'Retirement' OR `sep`.trans_status = 'Termination during Probation' OR `sep`.trans_status  = 'Termination' OR `sep`.trans_status  = 'Super Annuation' OR `sep`.trans_status  = 'Death' OR `sep`.trans_status  = 'Discharge simpliciter/ Dismiss' OR `sep`.trans_status  = 'Desertion cases' OR `sep`.trans_status  = 'Resignation' OR `sep`.trans_status  = 'Premature retirement' then ( case when  sep.trans_flag =3 OR sep.trans_flag =1 OR sep.trans_flag =5 OR sep.trans_flag =7 OR sep.trans_flag =9 OR sep.trans_flag =11 OR sep.trans_flag =13 OR sep.trans_flag =15 OR sep.trans_flag =17 OR sep.trans_flag =19 OR sep.trans_flag =21 then 1 else 0 END) else 0 end as seperation_status, case when  trans_status = 'Both' and trans_flag in(1,3,5,7,9,11,13,15,17,19) then 1 else 0 end transCRstatus ,   case when  trans_status = 'Transfer' and trans_flag in(1,3,5,7,9,11,13,15,17,19) then 1 else 0 end transstatus, case when  trans_status = 'Promotion' and trans_flag in(1,3,5,7) then 1 else 0 end PromotionStatus FROM staff_transaction as sep) as sep on staff.staffid = sep.staffid
  
  
  left join lpooffice on staff.`new_office_id` = lpooffice.`officeid`
  left join msdesignation on staff.`designation` = msdesignation.`desid` 
  where staff.status = IFNULL(".$isactive.",staff.status)";
  if($officeid){
    $sql .=" AND lpooffice.officeid= case when ".$officeid." =0 then lpooffice.officeid else ".$officeid." end";
  }
  if ($this->loginData->RoleID == 2) {
    $sql .= "AND staff.`reportingto` =".$this->loginData->staffid;  
  }
  $sql .= " GROUP BY `staff_transaction`.staffid  order by cast(staff.emp_code as UNSIGNED)";  
  //echo $sql;die;
  return  $this->db->query($sql)->result();
}

public function getOfficeList($p_office=null)
{
 try
 {
    //echo "office id=".$p_office;
   // die;
  $this->db->select("officeid,officename");
  

  $this->db->from('lpooffice');
  
  
  $query=$this->db->get();
     // echo $this->db->last_query();
      //die();
  return $query->result();
}


catch (Exception $e) 
{
 print_r($e->getMessage());die;
}

}

public function fetchdatas($token=null)
{ 

  try{
  $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
  $result = $this->db->query($sql);
  $rows = $result->num_rows();
  $tm='-1';
  if($rows==0)
  {
   return $tm;
 }
 else
 {
   
   $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
   
   $query=$this->db->query($sql);
 }
 }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



    /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

    public function getProbationStatus($staffid)
    {
      
      try{
       $sql = "SELECT flag FROM `tbl_probation_review_performance` where `staffid`=".$staffid;

       $res = $this->db->query($sql)->row();
       return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }



 }