<?php 

/**
* Tc_verify_documents Model
*/
class Tc_verify_documents_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}





/**
   * Method getCandidate_BDFFormSubmit() get BDF Form Submit Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidate_BDFFormSubmit()
  {
   
    try{
          $sql = "SELECT tbl_workflowdetail.type,`tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.teamid,`tbl_candidate_registration`.categoryid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`mstcampus`.`campusname`,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch, `mstcategory`.categoryname,`tbl_verification_document_hrd`.status,(CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender FROM `tbl_candidate_registration` 
            left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
     LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
                left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid
                left join `staff` ON `staff`.candidateid =`tbl_candidate_registration`.candidateid
                left join `tbl_workflowdetail` ON `tbl_workflowdetail`.staffid =`staff`.staffid
           left join `tbl_verification_document_hrd` ON `tbl_candidate_registration`.candidateid = `tbl_verification_document_hrd`.candidateid
           Where `tbl_candidate_registration`.BDFFormStatus=1 AND (`tbl_candidate_registration`.joinstatus=2 or `tbl_candidate_registration`.joinstatus=1 ) AND `tbl_candidate_registration`.tc_hrd_document_verfied NOT IN(3,0) "; 

         if ($this->loginData->RoleID ==17) {
          $sql .= "  AND `tbl_candidate_registration`.categoryid IN(2,3)";
         }elseif ($this->loginData->RoleID ==16){
            $sql .= "  AND `tbl_candidate_registration`.categoryid IN(1)";
         }else{
           $sql .= "  AND `tbl_candidate_registration`.categoryid IN(1,2,3)";
         }

        $sql .= ' GROUP BY tbl_candidate_registration.candidateid ORDER BY tbl_candidate_registration.candidateid DESC  ';

         // echo $sql; die;
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandateVerified() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandateVerified($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.categoryid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch,`tbl_candidate_registration`.encryptmetriccertificate, tbl_gap_year.encrypteddocumentsname as gapyeardocument,`tbl_candidate_registration`.encrypthsccertificate,`tbl_candidate_registration`.encryptugcertificate,`tbl_candidate_registration`.encryptpgcertificate,`tbl_candidate_registration`.encryptothercertificate, `tbl_candidate_registration`.ugmigration_encrypted_certificate_upload, `tbl_candidate_registration`.pgmigration_encrypted_certificate_upload, `tbl_candidate_registration`.ugmigration_certificate_date, `tbl_candidate_registration`.pgmigration_certificate_date,
           `tbl_candidate_registration`.encryptofferlettername,
           `tbl_hrd_verification_document`.status,
           `tbl_hrd_verification_document`.comment,
           `tbl_candidate_registration`.ugdoc_certificate,
           `tbl_candidate_registration`.ugdoc_certificate_date,
           `tbl_candidate_registration`.ugdoc_encrypted_certificate_doc_upload,
           `tbl_candidate_registration`.pgdoc_certificate,
           `tbl_candidate_registration`.pgdoc_certificate_date,
           `tbl_candidate_registration`.pgdoc_encrypted_certificate_upload,
           `tbl_candidate_registration`.ugdocencryptedImage,
           `tbl_candidate_registration`.pgdocencryptedImage 
           FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid 
           left JOIN tbl_gap_year ON tbl_candidate_registration.candidateid = tbl_gap_year.candidateid
            left join `tbl_hrd_verification_document` ON `tbl_candidate_registration`.candidateid = `tbl_hrd_verification_document`.candidateid
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.complete_inprocess=1 ";  // die;
        
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getSingleCandidateDetails() get single candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSingleCandidateDetails($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateTeam() get single candidate team id.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandidateTeam($token)
  {
    
    try{
           $sql = "SELECT * FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method getDevelopmentApprenticeship() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getDevelopmentApprenticeship($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.teamid FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1";  
        
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail($token)
  {
    
    try{

       $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=1 And `tblgnau`.status =1";  //die;
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail1($token)
  {
    
    try{

          $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where  `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=1 And `tblgnau`.status =2 AND `tblcanreg`.candidateid=".$token."";  
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method getNomieeDetail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomieeDetail($id)
  {
    
    try{

        $sql = "SELECT `tblnd`.general_nomination_id,`tblnd`.nominee_name,`sysr`.relationname,`tblnd`.nominee_age,`tblnd`.nominee_address  FROM `tbl_nominee_details` as `tblnd`  
       left join `sysrelation` as `sysr` ON `sysr`.id = `tblnd`.nominee_relation Where `tblnd`.general_nomination_id = $id";  
        //die;
        $result = $this->db->query($sql)->result();

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSubmittedBy($token)
  {
    
    try{
     $sql = 'SELECT * FROM `tbl_candidate_registration` Where `tbl_candidate_registration`.candidateid ='.$token.'';         
      $result = $this->db->query($sql)->result();
      return $result[0];
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateDetails($token)
  {
    
    try{
             $sql = 'SELECT `tbl_candidate_communication_address` .permanentstreet,`tbl_candidate_communication_address` .permanentcity,`tbl_candidate_communication_address` .permanentstateid,`tbl_candidate_communication_address` .permanentdistrict,`tbl_candidate_communication_address` .permanentpincode, tbl_generate_offer_letter_details.*,`state`.name FROM `tbl_candidate_communication_address` 
            left join `tbl_generate_offer_letter_details` ON `tbl_generate_offer_letter_details`.candidateid = `tbl_candidate_communication_address`.candidateid
            left join `state` ON `state`.id=`tbl_candidate_communication_address`.permanentstateid
                Where `tbl_candidate_communication_address`.candidateid ='.$token.'';  //die;
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  
/**
   * Method getJoiningReportDetail() get Joining Report Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportDetail($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_joining_report` Where candidateid = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandateWorkExperincecount($token)
  {
    
    try{

          $this->db->where('candidateid', $token);
         $res = $this->db->count_all_results('tbl_work_experience');

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceCandateVerified($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceVerifiedValue($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience_verified` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getVerifiedDetailes() get Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getVerifiedDetailes($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getverifiedstatus() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getverifiedstatus($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneralNominationFormPDF() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationFormPDF($token)
  {
    
    try{

         $sql = "SELECT status, filename FROM `tbl_general_nomination_and_authorisation_form` Where `candidateid` = $token"; //die;
       $res = $this->db->query($sql)->row();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateWithAddressDetails() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateWithAddressDetails($token)
  {
    
    try{

          $sql = 'SELECT `tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname, `tbl_candidate_communication_address`.permanentstreet, `tbl_candidate_communication_address`.permanentcity, `tbl_candidate_communication_address`.permanentstateid, `tbl_candidate_communication_address`.permanentdistrict, `tbl_candidate_communication_address`.permanentpincode, `state`.name FROM `tbl_candidate_registration` 
         left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
          left join `state` ON `state`.id = `tbl_candidate_communication_address`.permanentstateid
                Where `tbl_candidate_registration`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getViewGeneralnominationform() get View General Nomination  Authorisation Form Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewGeneralnominationform($token)
  {
    
    try{

         $sql = 'SELECT * FROM `tbl_general_nomination_and_authorisation_form`         
                Where `tbl_general_nomination_and_authorisation_form`.candidateid ='.$token.'';
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method ggetCandategapyearcount() get candate gap year Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandategapyearcount($token)
  {
    
    try{

          $this->db->where('candidateid', $token);
         $res = $this->db->count_all_results('tbl_gap_year');

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getgapyeareCandateVerified() get candate gapyear  Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getgapyeareCandateVerified($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_gap_year` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCountOtherDocument() get Count Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountOtherDocument($token)
  {
    
    try{
        
        $this->db->where('candidateid', $token);
        $result = $this->db->count_all_results('tbl_other_documents');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getOtherDocumentDetails() get Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocumentDetails($token)
  {
    
    try{

           $sql = 'SELECT `tblod`.id,`tblod`.documentupload,`mstdoc`.document_name,`mstdoc`.document_type FROM `tbl_other_documents` as tblod INNER JOIN `mstdocuments` as mstdoc ON `tblod`.documentname = `mstdoc`.id  Where `tblod`.candidateid ='.$token.' AND `mstdoc`.status = 0'; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getNomineedetail() get Count Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountNominee($token)
  {
    
    try{
        
        $this->db->where('general_nomination_id', $token);
        $result = $this->db->count_all_results('tbl_nominee_details');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Select Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_nominee_details`         
                Where `tbl_nominee_details`.general_nomination_id ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $this->db->select('*');
      $this->db->from('sysrelation');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getOtherDocuments() get Document Name With Type List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocuments()
  {

    try{
      $this->db->select('*');
      $this->db->from('mstdocuments');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }



/**
   * Method getJoiningReportStatus() get Joining Report Status List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportStatus($token)
  {

    try{
      $this->db->select('status, filename');
      $this->db->from('tbl_joining_report');
      //$this->db->where('status', 1);
      $this->db->where('candidateid', $token);
      return $this->db->get()->row();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSingleNameWitnessDeclaration($id)
  {

    try{
           $sql = "SELECT `staffid`,`name` FROM `staff` as a WHERE `a`.staffid=".$id; 
            
            $result = $this->db->query($sql)->result()[0];

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getWitnessDeclaration($id)
  {

    try{
         $sql = "SELECT staff_transaction.staffid, staff_transaction.date_of_transfer, staff_transaction.new_office_id, staff_transaction.new_designation, staff.name FROM staff_transaction 
    INNER JOIN  staff ON staff_transaction.staffid = staff.staffid  
    INNER JOIN (SELECT staff_transaction.staffid,Max(staff_transaction.date_of_transfer) as MaxDate FROM staff_transaction GROUP BY staffid) as TMax ON staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
      AND staff_transaction.trans_status NOT IN('Resign','Termination','Retirement','Death') AND staff_transaction.`new_office_id`='".$id."' AND  staff_transaction.`new_designation` IN(4,10) ORDER BY staff_transaction.staffid "; 
            ///AND staff_transaction.`new_office_id`='".$id."';
            $result = $this->db->query($sql)->result();

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


  /**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



  /**
   * Method getGeneralNominationWithWitnessformFormUploadStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationWithWitnessformFormUploadStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.filename as filename FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



  /**
   * Method getJoiningReportFormPdfStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportFormPdfStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.filename as filename FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }






/**
   * Method getReportingTo() get reporting office name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getReportingTo($id)
  {
    
    try{

  $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name 
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  ) 
  AND staff_transaction.`new_office_id` = '$id' 
  AND staff_transaction.`new_designation` IN(4,16) 

ORDER BY staff_transaction.`new_designation` asc limit 1
   ";   
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  
  
  /**
   * Method getStaffId() get incrementel staff id.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffId()
  {
    
    try{
          

      $sql = "SELECT max(staffid) as staffid FROM staff"; 
      $query = $this->db->query($sql);
      $numrows =  $query->num_rows();
      $result = $query->row();
    
      if($numrows > 0 ){
       $newstaffid = $result->staffid + 1;
      }
     return $newstaffid;
   }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





 /**
   * Method getStaffEmployeeCode() get incrementel staff emp code.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffEmployeeCode()
  {
    
    try{
          

      $sql = "SELECT max(emp_code) as emp_code FROM staff"; 
      $query = $this->db->query($sql);
      $numrows =  $query->num_rows();
      $result = $query->row();
    
      if($numrows > 0 ){
       $newstaffempcode = $result->emp_code + 1;
      }
     
     return $newstaffempcode;
   }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





  public function getStaffTransactioninc($tablename,$incval)
{
    try{

        // echo $tablename;
        // echo $incval;  die;
        //'staff_transaction',@a; 

        $sql = "select get_maxvalue('$tablename',$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }



/**
   * Method getCountWorkExperience() no of work experience .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountWorkExperience($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_work_experience` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getWorkExperience() list of candidate work experience .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperience($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_work_experience` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getCountFamilymember() no of Family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountgapyear($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_gap_year` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getFamilyMember() list of candidate family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGapYear($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_gap_year` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

/**
   * Method getCountFamilymember() no of Family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountFamilymember($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_family_members` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getFamilyMember() list of candidate family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getFamilyMember($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_family_members` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getCountidentitydetails() no of Family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountidentitydetails($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_identity_details` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




/**
   * Method getFamilyMember() list of candidate family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getIdentityDetails($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_identity_details` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getCountlanguageproficiency() no of Family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountlanguageproficiency($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_language_proficiency` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

/**
   * Method getLanguageProficiency() list of candidate family member .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getLanguageProficiency($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_language_proficiency` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




/**
   * Method getCountTrainingExposure() no of training exposure .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountTrainingExposure($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_training_exposure` as `tblte` 
      WHERE `tblte`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


/**
   * Method getTrainingExposure() list of candidate Training Exposure .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTrainingExposure($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_training_exposure` as `tblte` 
      WHERE `tblte`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


/**
   * Method getTrainingExposure() list of candidate Training Exposure .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getEducationDetails($candiid)
  {

    try{

      $sql = "SELECT metricschoolcollege,metricboarduniversity,metricpassingyear,metricplace,metricspecialisation,metricpercentage,hscschoolcollege,hscboarduniversity,hscpassingyear,hscplace,hscspecialisation,hscpercentage,hscstream,ugschoolcollege,ugboarduniversity,ugpassingyear,ugplace,ugspecialisation,ugpercentage,ugdegree,pgschoolcollege,pgboarduniversity,pgpassingyear,pgplace,pgspecialisation,pgpercentage,pgdegree,otherschoolcollege,otherboarduniversity,otherpassingyear,otherplace,otherspecialisation,otherpercentage,otherdegree,other_degree_specify FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function tc_data($teamid)
{
  try{

      $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emailid,
   `staff`.`designation`
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '$teamid' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
ORDER BY
  `staff_transaction`.staffid  DESC  LIMIT 0,1
 ";
 //echo $sql;

      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


}