<?php 
class Common_Model extends MY_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->file_path = realpath(APPPATH . '../datafiles');
		$this->banner_path = realpath(APPPATH . '../banners');
		$this->gallery_path_url = base_url().'datafiles/';
	}

	public function insert_data($table,$data1)
	{   
		try{
		$this->db->insert($table,$data1);
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}	
	
	public function update_data($table,$data1,$col,$val)
	{   
		try{
		$this->db->where($col, $val);
		$this->db->update($table,$data1);
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}	
	
	
	public function delete_row($table,$col,$val)
	{   
		try{
		$this->db->where($col, $val);
		$this->db->delete($table);
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}			



	public function exist_data($table,$compare)
	{   
		try{
		$query = $this->db->get_where($table, $compare);

        $count = $query->num_rows(); //counting result from query

        return $count;
        }catch (Exception $e) {
		print_r($e->getMessage());die;
		}
      }

      public function query_data($sql){
      	try{
      	$query = $this->db->query($sql);
      	return $query->result();
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
		}
      }

      public function query_data_array($sql){
      	try{
      	$query = $this->db->query($sql);
      	return $query->result_array();
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
		}
      }

      public function get_data($table,$fld = "*",$col=null,$val=null){
      	try{
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}		
      	$query = $this->db->get($table);
      	$row  = $query->row();
      	return $row;
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
      }

      // get all records created on 16/12/19 rajat
      public function get_all_data($table,$fld = "*"){
      	try{
      	$this->db->select($fld);		
      	$query = $this->db->get($table);
      	$result  = $query->result();
      	return $result;
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
      }

      public function get_data_multiple_where($table,$fld = "*",$where){
      	$this->db->select($fld);
      	$this->db->where($where); 
      	$query = $this->db->get($table);
      	$row  = $query->result();
      	return $row;
      }

      public function get_data_array($table,$fld = "*",$col=null,$val=null){
      	try{
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->where($col,$val); 
      	}
      	$query = $this->db->get($table);
      	$row  = $query->result_array();
      	return $row;
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
      }

      public function get_data_like($table,$fld = "*",$col=null,$val=null){
      	try{
      	$this->db->select($fld);

      	if($col != NULL){ 
      		$this->db->like($col,$val,'both'); 
      	}		
      	$query = $this->db->get($table);
      	$row  = $query->result();
      	return $row;
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
      }	

      function do_upload($new_name) {
      	try{
		//$this->load->helper(array('form', 'url'));

      	$config = array(
      		'allowed_types' => 'png|jpeg|jpg',
      		'upload_path' => $this->file_path,
      		'max_size' => 20000,
      		'file_name' => $new_name
      		);

      	$this->load->library('upload', $config);
      	$this->upload->do_upload();
      	$file_data = $this->upload->data();


      	return $file_data;
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
		}
      }

      	function staff_signature($token)
      	{
      		if($token==null || $token==''|| empty($token))
      		{
      			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
      		}
      		else
      		{
      		 try{

      		 	$image="SELECT encrypted_signature FROM staff WHERE staffid  = '$token'";
      		 	
      		 	

      		 	 $res = $this->db->query($image)->row();
      		 	 $record='';
      		 	 $path='';
      		 	 
      		 	  $record= count($res);
      		 	
      		 	  if($record>0)
      		 	  {
      		 	 $ima=$res->encrypted_signature;
      		  $path=site_url().'/datafiles/signature/'.$res->encrypted_signature;
      		 
      	
      		 	$image_path='datafiles/signature/'.$res->encrypted_signature;
      		
      		
            		if (file_exists($image_path)) {

   						return $res;
					}

					 else {

						$this->session->set_flashdata('er_msg', ' File not found on specific location.');
    					
					}
				}
	  			
	  			else
	  			{
	  				$this->session->set_flashdata('er_msg', 'File not found on specific location.');
	  			}

              }

              catch (Exception $e) {
       print_r($e->getMessage());die;
     }
 }
 
      	}

      function do_upload_banner() {
      	try{
		//$this->load->helper(array('form', 'url'));

      	$config = array(
      		'allowed_types' => 'png|jpeg|jpg',
      		'upload_path' => $this->banner_path,
      		'max_size' => 200000
      		);

      	$this->load->library('upload', $config);
      	$this->upload->do_upload();
      	$file_data = $this->upload->data();


      	return $file_data;
      	}catch (Exception $e) {
		print_r($e->getMessage());die;
		}
      }


      function do_upload_thumbnail()
      {
      	try{
		// Get configuartion data (we fill up 2 arrays - $config and $conf)

      	$config['img_path']			= $this->config->item('img_path');
      	$config['allow_resize']		= $this->config->item('allow_resize');

      	$config['allowed_types']	= $this->config->item('allowed_types');
      	$config['max_size']			= $this->config->item('max_size');
      	$config['encrypt_name']		= $this->config->item('encrypt_name');
      	$config['overwrite']		= $this->config->item('overwrite');
      	$config['upload_path']		= $this->config->item('upload_path');

		// Load uploader
      	$this->load->library('upload', $config);

		if ($this->upload->do_upload()) // Success
		{
			// General result data
			$result = $this->upload->data();

			$config['max_width']	= $this->config->item('max_width');
			$config['max_height']	= $this->config->item('max_height');
			

			if ($config['allow_resize'] and $config['max_width'] > 0 and $config['max_height'] > 0 and (($result['image_width'] > $config['max_width']) or ($result['image_height'] > $config['max_height'])))
			{			
				// Resizing parameters
				$resizeParams = array
				(
					'source_image'	=> $result['full_path'],
					'new_image'		=> $result['full_path'],
					'width'			=> $config['max_width'],
					'height'		=> $config['max_height']
					);
				
				// Load resize library
				$this->load->library('image_lib', $resizeParams);
				
				// Do resize
				if ( ! $this->image_lib->resize())
				{
					echo "errors in resize";
					echo $this->image_lib->display_errors();
				}
			}
			
			// Add our stuff
			$result['result']		= "file_uploaded";
			$result['resultcode']	= 'ok';
			$result['file_name']	= $config['img_path'] . '/' . $result['file_name'];
		}
		else // Failure
		{
			// Compile data for output
			$result['result']		= $this->upload->display_errors(' ', ' ');
			$result['resultcode']	= 'failure';
		}
		return $result;
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}
	


	public function get_language_phrase($key){
		try{
		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == ''){
			$languageId = 1;
		}

		$query = "select * from mstlanguage 
		where LanguageId =	$languageId 
		and Lang_Key = '".trim($key)."'";
		// echo $query; die();
		$langRow = $this->Common_Model->query_data($query);
		if(count($langRow) < 1){
			return '';
		}else{
			return $langRow[0]->Value;
		}
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}

	public function get_indicator_heading_phrase($headingId=null)
	{
		try{
		$query 	=	 "select * from tblmstindicatorheading where HeadingID	=	$headingId";
		$headingDetails	=	$this->Common_Model->query_data($query);
		if(count($headingDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == ''){
			$languageId = 1;
		}

		if($languageId == 1) return $headingDetails[0]->HeadingName;
		else if($languageId == 2) return $headingDetails[0]->HeadingNameTamil;
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}

	public function get_indicator_phrase($indicatorId=null)
	{
		try{
		$query = "select * from tblmstindicators where indicatorID	=	$indicatorId";
		$indicatorDetails	=	$this->Common_Model->query_data($query);

		if(count($indicatorDetails) <1) return '';
		
		$loginData	=	$this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == ''){
			$languageId = 1;
		}

		if($languageId == 1) return $indicatorDetails[0]->IndicatorName;
		else if($languageId == 2) return $indicatorDetails[0]->IndicatorNameTamil;
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	}

	public function get_graph_phrase($id=null)
	{
		$query 	=	 "select * from tblmstgraphs where Id	=	$id";
		$graphDetails	=	$this->Common_Model->query_data($query);

		if(count($graphDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == '')
		{
			$languageId = 1;
		}
		if($languageId == 1) return $graphDetails[0]->GraphName;
		else if($languageId == 2) return $graphDetails[0]->GraphNameTamil;
	}

	public function get_subheading_phrase($indicatorID=null)
	{
		$query =	"select * from tblmstindicators where indicatorID	=	$indicatorID";
		$subheadingDetails	=	$this->Common_Model->query_data($query);

		if(count($subheadingDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == '')
		{
			$languageId = 1;
		}
		if($languageId == 1) return $subheadingDetails[0]->SubHeading;
		else if($languageId == 2) return $subheadingDetails[0]->SubHeadingTamil;
	}

	public function get_graph_heading_phrase($headingId=null)
	{
		$query =	"select * from tblmstgraphgroup where GroupId	=	$headingId";
		$subheadingDetails	=	$this->Common_Model->query_data($query);

		if(count($subheadingDetails) <1) return '';

		$loginData = $this->session->userdata('loginData');
		$languageId = $loginData["Language_Id"];
		if($languageId == null || trim($languageId) == '')
		{
			$languageId = 1;
		}
		if($languageId == 1) return $subheadingDetails[0]->Name;
		else if($languageId == 2) return $subheadingDetails[0]->NameTamil;
	}

	public function send_email($subject, $body, $to_email= null, $to_name=null, $recipients=null, $attachments=array())
	{
		try {

					 //remove all special char 
					 $body = utf8_decode($body);
   					 $body = str_replace("&nbsp;", "", $body);
   				 	 $body = preg_replace("/\s+/", " ", $body);
    				 $body = trim($body);
			 $this->load->library('phpmailer','phpmailer');

		//load SMTP details
	   $loginDetails = $this->Common_Model->get_data('settings');		

		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		// date_default_timezone_set('Etc/UTC');
		date_default_timezone_set('Asia/Calcutta');

		//Create a new PHPMailer instance
		$mail = new Phpmailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host = $loginDetails->smtp_hostname;
		// use
		// $mail->Host = gethostbyname('smtp.gmail.com');
		// if your network does not support SMTP over IPv6

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = $loginDetails->smtp_port;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = $loginDetails->smtp_username;

		//Password to use for SMTP authentication
		$mail->Password = $loginDetails->smtp_password;

		//Set who the message is to be sent from
		$mail->setFrom($loginDetails->smtp_password, $loginDetails->company_name);

		//Set an alternative reply-to address
		// $mail->addReplyTo('replyto@example.com', 'First Last');

		//Set who the message is to be sent to
		 //$mail->addAddress('amit.kum2008@gmail.com', 'test');

		if ($to_email == NULL) {
			$to_email = $loginDetails->email_s_id;

			//$to_email = 'amit.kum2008@gmail.com';
		}

		$mail->addAddress($to_email, '');

		//CC and BCC
		if($recipients){
			foreach($recipients as $email => $name)
			{
			   $mail->addCC($email, $name);
			}
		}
		//$mail->addCC($to_candidate, '');

		$mail->addBCC($to_name, '');

		//Set the subject line
		$mail->Subject = $loginDetails->company_name . ' - ' . $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($body);

		//Replace the plain text body with one created manually
		// $mail->AltBody = 'This is a plain-text message body';
		//print_r($attachments); die;
		//Attach an image file
		// echo count($attachments); die;
		// $attachments = array('name.pdf','1062c61bf92629c404593623cdaceb14.pdf');
		// echo count($attachments); die;
		if(count($attachments)>0){
			foreach($attachments as $attachment){

				if(file_exists(FCPATH  . 'pdf_offerletters/' . $attachment)){
					$mail->addAttachment(FCPATH  . 'pdf_offerletters/' . $attachment);
				}else{
					return "ERROR: attachment file ". FCPATH  . 'pdf_offerletters/' . $attachment . " does not exists";
				}
			}
		}
		
		//send the message, check for errors
		if (!$mail->send()) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		} else {
			return 1;
		}
		} catch (Exception $e) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		}


		
	}


public function transfer_letter_send_email($subject, $body, $to_email= null, $to_name=null, $attachments=array())
	{
					 //remove all special char 
					 $body = utf8_decode($body);
   					 $body = str_replace("&nbsp;", "", $body);
   				 	 $body = preg_replace("/\s+/", " ", $body);
    				 $body = trim($body);

		 $this->load->library('phpmailer','phpmailer');

		//load SMTP details
	   $loginDetails = $this->Common_Model->get_data('settings');	

	 	
		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		// date_default_timezone_set('Etc/UTC');
		date_default_timezone_set('Asia/Calcutta');

		//Create a new PHPMailer instance
		$mail = new Phpmailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host = $loginDetails->smtp_hostname;
		// use
		// $mail->Host = gethostbyname('smtp.gmail.com');
		// if your network does not support SMTP over IPv6

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = $loginDetails->smtp_port;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = $loginDetails->smtp_username;

		//Password to use for SMTP authentication
		$mail->Password = $loginDetails->smtp_password;

		//Set who the message is to be sent from
		$mail->setFrom($loginDetails->smtp_password, $loginDetails->company_name);

		//Set an alternative reply-to address
		// $mail->addReplyTo('replyto@example.com', 'First Last');

		//Set who the message is to be sent to
		// $mail->addAddress('amit.kum2008@gmail.com', 'test');

		if ($to_email == NULL) {
			$to_email = $loginDetails->email_s_id;
		}

		$mail->addAddress($to_email, '');

				//CC and BCC
		//$mail->addCC($to_candidate, '');

		$mail->addBCC($to_name, '');

		//Set the subject line
		$mail->Subject = $loginDetails->company_name . ' - ' . $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($body);

		//Replace the plain text body with one created manually
		// $mail->AltBody = 'This is a plain-text message body';
		//print_r($attachments); die;
		//Attach an image file
		//echo count($attachments); die;
		// $attachments = array('user.jpg','88d11c5452e538df95e0dc920d2ed8f6.pdf');
		//echo count($attachments); 

		if(count($attachments)>0){
			foreach($attachments as $attachment){
				if(file_exists(FCPATH  . 'datafiles/letters/transfer/' . $attachment)){
					$mail->addAttachment(FCPATH  . 'datafiles/letters/transfer/' . $attachment);
				}else{
					return "ERROR: attachment file ". FCPATH  . 'datafiles/letters/transfer/' . $attachment . " does not exists";
				}
			}
		}
		

		//print_r($mail); die;
		//send the message, check for errors
		if (!$mail->send()) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Send Mail";

		}
	}
		public function midemreview_send_email($subject, $body, $to_email= null, $to_name=null,$recipenent=null, $attachments=array())
	{
		// print_r($attachments);
		// print_r($recipenent);
		// die;

					 //remove all special char 
					 $body = utf8_decode($body);
   					 $body = str_replace("&nbsp;", "", $body);
   				 	 $body = preg_replace("/\s+/", " ", $body);
    				 $body = trim($body);

		$this->load->library('phpmailer','phpmailer');

		//load SMTP details
		$loginDetails = $this->Common_Model->get_data('settings');	


		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		// date_default_timezone_set('Etc/UTC');
		date_default_timezone_set('Asia/Calcutta');

		//Create a new PHPMailer instance
		$mail = new Phpmailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host = $loginDetails->smtp_hostname;
		// use
		// $mail->Host = gethostbyname('smtp.gmail.com');
		// if your network does not support SMTP over IPv6

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = $loginDetails->smtp_port;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = $loginDetails->smtp_username;

		//Password to use for SMTP authentication
		$mail->Password = $loginDetails->smtp_password;

		//Set who the message is to be sent from
		$mail->setFrom($loginDetails->smtp_password, $loginDetails->company_name);

		//Set an alternative reply-to address
		// $mail->addReplyTo('replyto@example.com', 'First Last');

		//Set who the message is to be sent to
		// $mail->addAddress('amit.kum2008@gmail.com', 'test');

		if ($to_email == NULL) {
			$to_email = $loginDetails->email_s_id;
		}
		$recipients='';
		if($recipients){
			foreach($recipients as $email => $name)
			{
				$mail->addCC($email, $name);
			}
		}

		$mail->addAddress($to_email, '');

				//CC and BCC
		//$mail->addCC($to_candidate, '');

		$mail->addBCC($to_name, '');

		//Set the subject line
		$mail->Subject = $loginDetails->company_name . ' - ' . $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($body);

		//Replace the plain text body with one created manually
		// $mail->AltBody = 'This is a plain-text message body';
		//print_r($attachments); die;
		//Attach an image file
		//echo count($attachments); die;
		// $attachments = array('user.jpg','88d11c5452e538df95e0dc920d2ed8f6.pdf');
		//echo count($attachments); 

		if(count($attachments)>0){
			foreach($attachments as $attachment){
				if(file_exists(FCPATH  . 'midtem_offerletter/' . $attachment)){
					$mail->addAttachment(FCPATH  . 'midtem_offerletter/' . $attachment);
				}else{
					return "ERROR: attachment file ". FCPATH  . 'midtem_offerletter/' . $attachment . " does not exists";
				}
			}
		}
		

		//print_r($mail); die;
		//send the message, check for errors
		if (!$mail->send()) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Send Mail";

		}
	}




public function send_email_separation($subject, $body, $to_email= null, $to_name=null, $recipenent=null, $attachments=array())
	{

					 //remove all special char 
					 $body = utf8_decode($body);
   					 $body = str_replace("&nbsp;", "", $body);
   				 	 $body = preg_replace("/\s+/", " ", $body);
    				 $body = trim($body);

    	/*echo "<pre>";
    	print_r($attachments);exit();*/
		$this->load->library('phpmailer','phpmailer');

		//load SMTP details
	   $loginDetails = $this->Common_Model->get_data('settings');		

// 	   echo $subject;

// echo $body;

// echo $to_email;
// echo $to_name;

// print_r($attachments); die;
		
		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		// date_default_timezone_set('Etc/UTC');
		date_default_timezone_set('Asia/Calcutta');

		//Create a new PHPMailer instance
		$mail = new Phpmailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host = $loginDetails->smtp_hostname;
		// use
		// $mail->Host = gethostbyname('smtp.gmail.com');
		// if your network does not support SMTP over IPv6

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = $loginDetails->smtp_port;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = $loginDetails->smtp_username;

		//Password to use for SMTP authentication
		$mail->Password = $loginDetails->smtp_password;

		//Set who the message is to be sent from
		$mail->setFrom($loginDetails->smtp_password, $loginDetails->company_name);

		//Set an alternative reply-to address
		// $mail->addReplyTo('replyto@example.com', 'First Last');

		//Set who the message is to be sent to
		// $mail->addAddress('amit.kum2008@gmail.com', 'test');

		if ($to_email == NULL) {
			$to_email = $loginDetails->email_s_id;
		}
		// $recipients='';
		if($recipients){
			foreach($recipients as $email => $name)
			{
				$mail->addCC($email, $name);
			}
		}

		$mail->addAddress($to_email, '');

				//CC and BCC
		//$mail->addCC($to_candidate, '');

		$mail->addBCC($to_name, '');

		//Set the subject line
		$mail->Subject = $loginDetails->company_name . ' - ' . $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($body);

		//Replace the plain text body with one created manually
		// $mail->AltBody = 'This is a plain-text message body';
		//print_r($attachments); die;
		//Attach an image file
		//echo count($attachments); die;
		// $attachments = array('user.jpg','88d11c5452e538df95e0dc920d2ed8f6.pdf');
		//echo count($attachments); die;
		if(count($attachments)>0){
			foreach($attachments as $attachment){
				if(file_exists(FCPATH  . 'pdf_separationletters/' . $attachment)){
					$mail->addAttachment(FCPATH  . 'pdf_separationletters/' . $attachment);
				}else{
					return "ERROR: attachment file ". FCPATH  . 'pdf_separationletters/' . $attachment . " does not exists";
				}
			}
		}
		
		//send the message, check for errors
		if (!$mail->send()) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		} else {
			return 1;
		}
	}



	public function send_email_prejoining_document($subject, $body, $to_email= null, $to_name=null, $recipients=null, $attachments=array())
	{

					 //remove all special char 
					 $body = utf8_decode($body);
   					 $body = str_replace("&nbsp;", "", $body);
   				 	 $body = preg_replace("/\s+/", " ", $body);
    				 $body = trim($body);

		///print_r($attachments); die;

		$this->load->library('phpmailer','phpmailer');

		//load SMTP details
	   $loginDetails = $this->Common_Model->get_data('settings');		

// 	   echo $subject;

// echo $body;

// echo $to_email;
// echo $to_name;

// print_r($attachments); die;
		
		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		// date_default_timezone_set('Etc/UTC');
		date_default_timezone_set('Asia/Calcutta');

		//Create a new PHPMailer instance
		$mail = new Phpmailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		if($recipients){
			foreach($recipients as $email => $name)
			{
			   $mail->addCC($email, $name);
			}
		}

		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';

		//Set the hostname of the mail server
		$mail->Host = $loginDetails->smtp_hostname;
		// use
		// $mail->Host = gethostbyname('smtp.gmail.com');
		// if your network does not support SMTP over IPv6

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = $loginDetails->smtp_port;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = $loginDetails->smtp_username;

		//Password to use for SMTP authentication
		$mail->Password = $loginDetails->smtp_password;

		//Set who the message is to be sent from
		$mail->setFrom($loginDetails->smtp_password, $loginDetails->company_name);

		//Set an alternative reply-to address
		// $mail->addReplyTo('replyto@example.com', 'First Last');

		//Set who the message is to be sent to
		// $mail->addAddress('amit.kum2008@gmail.com', 'test');

		if ($to_email == NULL) {
			$to_email = $loginDetails->email_s_id;
		}

		$mail->addAddress($to_email, '');

				//CC and BCC
		//$mail->addCC($to_candidate, '');

		$mail->addBCC($to_name, '');

		//Set the subject line
		$mail->Subject = $loginDetails->company_name . ' - ' . $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($body);

		//Replace the plain text body with one created manually
		// $mail->AltBody = 'This is a plain-text message body';
		// echo FCPATH; 
		// print_r($attachments); die;
		//Attach an image file
		//echo count($attachments); die;
		// $attachments = array('user.jpg','88d11c5452e538df95e0dc920d2ed8f6.pdf');
		//print_r($attachments);
		//echo count($attachments); die;
		if(count($attachments)>0){
			foreach($attachments as $attachment){
				if(file_exists(FCPATH  . 'datafiles/prejoiningdocument/' . $attachment)){
					$mail->addAttachment(FCPATH  . 'datafiles/prejoiningdocument/' . $attachment);
				}else{
					return "ERROR: attachment file ". FCPATH  . 'datafiles/prejoiningdocument/' . $attachment . " does not exists";
				}
			}
		}
		
		//send the message, check for errors
		if (!$mail->send()) {
			return "ERROR: Mailer Error: " . $mail->ErrorInfo;
		} else {
			return 1;
		}
	}


  public function get_staff_finance_detail($officeid, $roleid)
   {
      try
    {

      $sql="SELECT c.staffid, 
      c.name,c.encrypted_signature, 
      c.emailid,lp.officename FROM mstuser as a 
      LEFT JOIN `staff` as c 
      on a.staffid = c.staffid 
      left join lpooffice as lp
      on c.new_office_id=lp.officeid
      WHERE c.new_office_id = $officeid AND a.RoleID = $roleid and c.status = 1 limit 1";
      return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}
public function get_ho_finance_detail($officeid, $roleid)
   {
      try
    {

      $sql="SELECT c.staffid, c.emp_code,
      c.name,c.encrypted_signature, 
      c.emailid,lp.officename FROM mstuser as a 
      LEFT JOIN `staff` as c 
      on a.staffid = c.staffid 
      left join lpooffice as lp
      on c.new_office_id=lp.officeid
      WHERE c.new_office_id = $officeid AND a.RoleID = $roleid and c.status = 1 limit 1";
      // echo $sql;
      // die;

      return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}


public function get_staffReportingto($token)
    {
       
       try
       {

          $sql=" SELECT s1.staffid, s1.reportingto, msdesignation.desname as designation, s.emailid, s.name,s.encrypted_signature FROM staff as s1 LEFT JOIN staff as s on s1.reportingto = s.staffid LEFT JOIN msdesignation on msdesignation.desid=s1.designation 
          inner JOIN mstuser as u  on u.staffid=s.staffid
          where s1.staffid = $token and s.status=1 and (u.RoleID=2  OR u.RoleID=21) order by u.RoleID limit 1";
         
          return  $this->db->query($sql)->row();

     } catch (Exception $e) {
       print_r($e->getMessage());die;
     }

   }

/**
   * Method get_staff_sep_detail() get the staff detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */



	public function get_staff_sep_detail($token=NULL)
   {
      try
	  {

	    $sql="SELECT SUBSTRING(c.name, 1, LOCATE(' ',c.name)) as name,c.new_office_id,c.permanentpincode,c.contact,c.emailid,a.date_of_transfer as seperatedate,  a.staffid,a.reportingto, b.desname as sepdesig, a.new_designation as sepdesigid, c.emp_code, CONCAT(IFNULL(c.permanenthno,''), IFNULL(c.permanentstreet,''), IFNULL(c.permanentcity,''),IFNULL(c.permanentdistrict,'') , IFNULL(s.name,''),'-',IFNULL(c.permanentpincode,'')) as address, st.date_of_transfer as joiningdate, d.desname as joindesig, a.trans_flag, a.reason, a.createdon,a.trans_status as process_status,a.date_of_transfer, lp.officename as newoffice, st.trans_status, dc.dc_cd, msdc.dc_name  FROM staff_transaction as a
	       INNER JOIN `staff` as c on a.staffid = c.staffid and (a.`trans_status` ='Termination' OR a.`trans_status` ='Resign' OR a.`trans_status` ='Termination during Probation' OR a.`trans_status` ='Death' OR a.`trans_status` ='Retirement' OR a.`trans_status` ='Discharge simpliciter/ Dismiss' OR a.`trans_status` ='Desertion cases' OR a.`trans_status` ='Premature retirement' OR a.`trans_status` ='Super Annuation')
	       left join `state` as s ON s.id = c.permanentstateid
	       inner join staff_transaction as st
	        on st.staffid = c.staffid and st.trans_status = 'JOIN'
	       left JOIN `msdesignation` as b on a.new_designation = b.desid
	       left JOIN `msdesignation` as d on st.new_designation = d.desid
	       LEFT JOIN `lpooffice` as lp on a.new_office_id = lp.officeid 
	       LEFT JOIN `dc_team_mapping` as dc on c.new_office_id = dc.teamid 
    LEFT JOIN  `msdc_details` as msdc ON dc.dc_cd = msdc.dc_cd
	     WHERE a.id = $token";
	     
 		// echo $sql; die;

	     //$query=$this->db->get();
	     return  $this->db->query($sql)->row();

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}


 /**
   * Method get ED Name () get the staff Ed Name.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getEDName()
    {
    
        try{

            $sql = "SELECT name as executivedirectorname FROM `Staff` WHERE  `staffid` = 15";

            $result = $this->db->query($sql)->row();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }
	
/**
   * Method get_Personnal_Staff_List() get personnal staff detail all mpdule.
   * @access  public
   * @param Null
   * @return  Array
 */
  
  public function get_Personnal_Staff_List()
   {
      try
	  {

	     $sql="SELECT * FROM `mstuser` 
	     inner join staff on mstuser.staffid=staff.staffid
	     WHERE mstuser.RoleID = 17 AND mstuser.IsDeleted=0 And staff.status=1 limit 1";
	     // echo $sql;
	     // die;
	     
	     $result =  $this->db->query($sql)->result();

	      return $result;

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }
   public function get_hrdetails()
   {
      try
	  {

	     $sql="SELECT * FROM `mstuser` 
	     inner join staff on mstuser.staffid=staff.staffid
	     WHERE mstuser.RoleID = 16 AND mstuser.IsDeleted=0 And staff.status=1 limit 1";
	     // echo $sql;
	     // die;
	     
	     $result =  $this->db->query($sql)->result();

	      return $result;

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }


   /**
   * Method get_Personnal_Staff_List() get personnal staff detail all mpdule.
   * @access  public
   * @param Null
   * @return  Array
 */
  
  public function get_hr_Staff_List()
   {
      try
	  {

	     $sql="SELECT * FROM `mstuser` WHERE RoleID = 16 AND IsDeleted=0";
	     
	     $result =  $this->db->query($sql)->result();

	      return $result;

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   /**
   * Method get_Personnal_Staff_List() get personnal staff detail all mpdule.
   * @access  public
   * @param Null
   * @return  Array
 */
  
  public function get_finance_list()
   {
      try
	  {

	     $sql="SELECT * FROM `mstuser` WHERE RoleID = 20 AND IsDeleted=0";
	     
	     $result =  $this->db->query($sql)->result();

	      return $result;

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   	public function get_Staff_detail($token){
		try
		{
			$sql="select staffid, name, doj, emp_code, designation, emailid, msdesignation.desid, permanentstreet, msdesignation.desname from staff inner join msdesignation on staff.designation= msdesignation.desid where staffid=".$token;
			$result =  $this->db->query($sql)->row();
			return $result;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
   	}





}
