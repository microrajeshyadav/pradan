<?php 

/**
* TC_DAship_Components_model Model
*/
class Tc_daship_component_comments_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


/**
	 * Method getBatch() get all Batch .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getBatch()
{
	
	try{
		
		$sql = "SELECT * FROM `mstbatch` Where status=0 AND isdeleted=0";

		$res = $this->db->query($sql)->result();

		return $res;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	
}



	/**
	 * Method getDAComponent() get all DA Component .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDAComponent($token)
	{
		
		try{
			
	$sql = "SELECT DISTINCT
    `tbldc`.candidateid,
    `tblcr`.teamid,
    `tblcr`.candidatefirstname,
    `tblcr`.candidatemiddlename,
    `tblcr`.candidatelastname,
    `tblcr`.emailid,
    `tblcr`.candidatelastname,
    `tbldsed`.phaseid,
    `tbldsed`.batchid,
    `tbldc`.original_document_name,
    `tbldc`.encrypted_document_name,
    `tbldaedt`.fromdate,
    `tbldaedt`.todate,
    `mstph`.phase_name,
    `tbldc`.id,
    `tbldc`.status,
    `lpo`.officename
FROM
    `tbl_da_event_detailing` AS `tbldsed`,
    `tbl_da_event_detailing_transaction` AS `tbldaedt`,
    `tbl_daship_component` AS `tbldc`,
    `tbl_candidate_registration` AS `tblcr`,
    `lpooffice` AS `lpo`,
    `mstphase` AS `mstph`
    
WHERE
    `tbldsed`.`id` = `tbldaedt`.da_event_detailing AND `tbldaedt`.`candidateid` = `tbldc`.`candidateid` AND `tbldsed`.phaseid = `tbldc`.phaseid 
    AND `tblcr`.candidateid = `tbldc`.candidateid 
     AND `tblcr`.teamid = `lpo`.officeid 
    AND `tbldc`.phaseid =`mstph`.id 

     AND `tbldc`.status=1 AND  `tbldc`.id = $token "; 

    // echo $sql; die;

			$res = $this->db->query($sql)->result();

			return $res;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


	/**
   * Method getReportingTo() get reporting office name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getReportingTo($id)
  {
    
    try{

  $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name,`staff`.emailid
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  ) 
  AND staff_transaction.`new_office_id` = '$id' 
  AND staff_transaction.`new_designation` IN(4,16) 
ORDER BY 
  `staff_transaction`.staffid ";   
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }  


}
