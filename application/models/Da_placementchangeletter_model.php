<?php 

/**
* Dashboard Model
*/
class Da_placementchangeletter_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{

	}



	public function getTeamlist()
	{
		try{

      $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No'  ORDER BY `officename`";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }




public function getstafftransdetail($staffid)
  {
    try{

 $sql = "SELECT
  
  `tbl_candidate_registration`.candidatefirstname,
 `tbl_candidate_registration`.candidatemiddlename,
 `tbl_candidate_registration`.candidatelastname,
 `tbl_candidate_registration`.emailid,

  
 
  
  `tbl_candidate_registration`.teamid,
  tbl_candidate_registration.fgid,
  
  `tbl_candidate_communication_address`.permanenthno,
  `tbl_candidate_communication_address`.permanentstreet,
  `tbl_candidate_communication_address`.permanentcity,
  `tbl_candidate_communication_address`.permanentpincode,
  `tbl_candidate_communication_address`.permanentstateid,
  `tbl_candidate_communication_address`.permanentdistrict,
  state.name as state_name,
  district.name as district_name,
  tbl_generate_offer_letter_details.doj,
  olddc.dc_name as olddcname,
  lpooffice.officename as old_office

FROM
  tbl_candidate_registration
   left join tbl_candidate_communication_address on tbl_candidate_registration.candidateid=tbl_candidate_communication_address.candidateid
  left JOIN state ON `state`.statecode = tbl_candidate_communication_address.permanentstateid
  left JOIN district ON `district`.districtid = tbl_candidate_communication_address.permanentstateid
  left join tbl_generate_offer_letter_details on tbl_candidate_registration.candidateid=tbl_generate_offer_letter_details.candidateid

 LEFT JOIN dc_team_mapping as olddctm on  olddctm.teamid = tbl_candidate_registration.teamid 
LEFT JOIN msdc_details as olddc on olddctm.dc_cd = olddc.dc_cd
left join lpooffice on tbl_candidate_registration.teamid=lpooffice.officeid

  WHERE `tbl_candidate_registration`.candidateid = $staffid 
  group BY `tbl_candidate_registration`.candidateid ";  

  
   $res = $this->db->query($sql)->row();

    return $res; 

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }



public function getCampus($id=null)
{
  try{

//print_r($this->loginData);

     //$sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
   // ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0"; 

    if (!empty($id) AND $this->loginData->RoleID==30) {
      
       $sql = "SELECT * FROM `mstcampus` as a 
       Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
       inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
     WHERE a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND b.`campus_status`=0  AND b.`recruiterid`= ". $id; 
       
     } else{

      $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND b.`mailstatus`= 1  AND b.`campus_status`=0 ";

     }

   // echo $sql; die;

    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



public function getdachangeplacementdetail($office)
  {
    try{

   $sql = "SELECT
    staff.name,
    staff.designation,
    `lpooffice`.street,
    `lpooffice`.state,
    `lpooffice`.district,
    `state`.name AS statename,
      district.name as district_name,
    `lpooffice`.officename AS newofficename,
    `newdc`.dc_name AS newdcname
FROM
    staff
LEFT JOIN lpooffice ON lpooffice.staffid = staff.staffid
LEFT JOIN dc_team_mapping AS newdctm
ON
    newdctm.teamid = staff.new_office_id
LEFT JOIN msdc_details AS newdc
ON
    newdc.dc_cd = newdctm.dc_cd
LEFT JOIN msdesignation AS des
ON
    des.desid = staff.designation
LEFT JOIN state AS officest
ON
    `officest`.statecode = lpooffice.state

LEFT JOIN state ON `state`.statecode = lpooffice.state
left join district on district.districtid=lpooffice.district

WHERE
    `lpooffice`.`officeid` = '$office' and staff.designation=4
"; 
   
   $res = $this->db->query($sql)->row();

    return $res; 

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }




  public function getRoleEdDetails()
  {
    try{

      $sql = "SELECT staff.staffid,staff.name,staff.emailid FROM `staff` LEFT join  
        mstuser as u on u.staffid = staff.staffid 
        where staff.designation = 4 and staff.status = 1 and u.IsDeleted=0 GROUP BY staffid";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  public function getTeamFGDetail($officeid)
  {
    try{

      $sql = "SELECT a.staffid, a.name FROM `staff` as a
        left join tbl_candidate_registration on a.candidateid=tbl_candidate_registration.candidateid
       WHERE a.new_office_id =".$officeid;  
       
      $result = $this->db->query($sql)->result();

      return $result;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

public function getSingleTeamDetail($token)
  {
    try{

       $sql = "SELECT `teamid`,`fgid`,`batchid` FROM `tbl_candidate_registration`  WHERE `tbl_candidate_registration`.candidateid = $token";  
      $result = $this->db->query($sql)->result();

      return $result[0];

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


  public function getSelectedCandidate($campusid, $campusintimationid)
  {

    try{

      //,`tbl_generate_offer_letter_details`.filename
     
      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.campustype,`tbl_candidate_registration`.emailid,`mstcampus`.`campusname`,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`mstcategory`.`categoryname`,`tbl_candidate_registration`.candidatelastname, `lpooffice`.officename, `mstbatch`.batch,`staff`.name,  `tbl_generate_offer_letter_details`.sendflag,`tbl_generate_offer_letter_details`.id as letterid, `tbl_generate_offer_letter_details`.flag,
      (CASE WHEN tbl_candidate_registration.encryptofferlettername IS NULL THEN tbl_generate_offer_letter_details.filename WHEN tbl_candidate_registration.encryptofferlettername IS NOT NULL THEN tbl_candidate_registration.encryptofferlettername END) as filename, 
      (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender,COALESCE(
     nullif(otherspecialisation, ''),
     nullif(pgspecialisation, ''),
     nullif(ugspecialisation, '')
  ) AS stream
      FROM `tbl_candidate_registration`
     left join `tbl_candidate_hrscore` ON `tbl_candidate_hrscore`.candidateid =`tbl_candidate_registration`.candidateid
      left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid
     LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
      left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid
      left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid
      left join `mstcategory` ON `mstcategory`.id = `tbl_candidate_registration`.categoryid
      left join `tbl_generate_offer_letter_details` ON `tbl_generate_offer_letter_details`.candidateid=`tbl_candidate_registration`.candidateid
      Where `tbl_candidate_hrscore`.hrscore > 1 ";

      if($campusid == 0 && $campusid !='NULL'){
       $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
        AND  `tbl_candidate_registration`.`campusintimationid`= $campusintimationid 
       AND `tbl_candidate_registration`.`campustype` = 'off' 
       AND `tbl_candidate_registration`.`inprocess` = 'closed' 
       AND `tbl_candidate_registration`.`yprststus` = 'yes'
       AND (`tbl_candidate_registration`.`complete_inprocess`= 0 OR `tbl_candidate_registration`.`complete_inprocess`= 1)
      AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
     }elseif(empty($campusid)){

      $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' 
       AND  `tbl_candidate_registration`.`campusintimationid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`complete_inprocess`= 0";

    } else{

      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid  
       AND  `tbl_candidate_registration`.`campusintimationid`= $campusintimationid 
AND `tbl_candidate_registration`.`campustype` ='on' 
AND `tbl_candidate_registration`.`inprocess` ='closed' 
AND (`tbl_candidate_registration`.`complete_inprocess`= 0 OR `tbl_candidate_registration`.`complete_inprocess`= 1) ";

    }

   if ($this->loginData->RoleID == 16) {
     // $sql .= " AND `tbl_candidate_registration`.`categoryid`NOT IN(2,3) "; 
    $sql .= " AND `tbl_candidate_registration`.`categoryid` IN(1) ";
   }

   if ($this->loginData->RoleID == 17) {
      $sql .= " AND `tbl_candidate_registration`.`categoryid` IN(2,3) "; 
   }

    $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid DESC';
    // echo $sql;
    // die;
    
     
    $res = $this->db->query($sql)->result();


    return $res;
 

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function getReportingto($teamid)
  {
    try{

  $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emp_code,
   `staff`.`designation`,
   `msdesignation`.desname
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN msdesignation ON `msdesignation`.desid = staff.designation
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '".$teamid."' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
    ORDER BY `staff_transaction`.staffid  DESC ";  
     $res = $this->db->query($sql)->result();

      return  $res;

 

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


public function getSingelRecruitersCampus($id){

  try{

       $sql = "SELECT *  FROM `tbl_generate_offer_letter_details` Where `id` = $id ";

       $res = $this->db->query($sql)->result();


    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


public function getflag($candidateid)
{

  try{
    $this->db->where('candidateid',$candidateid);
    $query = $this->db->get('tbl_generate_offer_letter_details');
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getEdcommentStatus($token)
{

  try{

    $sql = "SELECT *  FROM `edcomments` Where `flag` !='NULL'   AND `candidateid` = $token ";
    $query = $this->db->query($sql);

   // echo $query->num_rows(); 

    if ($query->num_rows() >0) {
     $res = $this->db->query($sql)->result()[0];

      return $res;
    }else{

      return -1;
      }

    
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


 public function getCategoryid($token)
  {
    try{

      $sql = "SELECT categoryid FROM `tbl_candidate_registration`  WHERE candidateid =".$token;  
      $res = $this->db->query($sql)->row();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }




public function getStaffDetails($token)
  {
    try{

      $sql = "SELECT staff.teamid,staff.fgid  FROM `staff_transaction` 
               INNER JOIN `staff`  on staff.staffid = staff_transaction.staffid 
              INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  )  WHERE   staff_transaction.staffid =".$token;  
      $res = $this->db->query($sql)->row();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

public function getstatedetail($statecode)
  {
    try{

      $sql = " SELECT name FROM `state` as st WHERE  st.statecode =".$statecode;  
      $res = $this->db->query($sql)->row();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


public function getdistrictdetail($districtcode)
  {
    try{

       $sql = " SELECT name FROM `district` as st WHERE  st.districtid ='".$districtcode."'";  
      $res = $this->db->query($sql)->row();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }



  public function getStaffTransactioninc($tablename,$incval)
{
    try{

      $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }


public function getHRunitDetails($staff)
{
    try{
       $roleid=$this->loginData->RoleID;

  $sql = "SELECT staff.name,msdesignation.desname,staff.emailid,sysaccesslevel.Acclevel_Name FROM staff

       
INNER JOIN msdesignation on msdesignation.desid = staff.designation 
inner join mstuser on staff.staffid=mstuser.staffid
inner join sysaccesslevel on mstuser.RoleID=sysaccesslevel.Acclevel_Cd
where staff.staffid='$staff' and sysaccesslevel.Acclevel_Cd='$roleid'";


      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }



public function changefieldguide($staff)
{
    try{

      $sql = "SELECT staff.name,msdesignation.desname,staff.emailid FROM `staff` 
INNER JOIN msdesignation on msdesignation.desid = staff.designation 
inner join mstuser on staff.staffid=mstuser.staffid

where staff.staffid='$staff'";
// echo $sql;
// die;

      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }


}