<?php 

/**
* Dashboard Model
*/
class Candidatesinfo_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  

  public function index()
  {

  }


   /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {

    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
   //echo substr($Date,$len,-4); die;
    if(substr($Date,$len,-4)=="/")
      $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
      $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}





  public function getSelectedCandidate()
  {
    
    try{

          $sql = "SELECT * FROM `tbl_candidate_registration` 
                  Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  AND `metricpercentage` >= 60 AND 
                  `hscpercentage` >= 60 AND 
                  `ugpercentage` >= 60 OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60 )"; 

        $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 public function getUgEducation()
  {
    
    try{

        // $sql = "SELECT id,ugname FROM `mstugeducation` Where isdeleted=0";
        $sql = "SELECT id,pgname FROM `mstpgeducation` Where groupid = 3  AND isdeleted=0";
        $res = $this->db->query($sql)->result();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


public function getPgEducation()
  {
    
    try{

        // $sql = "SELECT id,pgname FROM `mstpgeducation` Where isdeleted=0";
        $sql = "SELECT id,pgname FROM `mstpgeducation` Where groupid = 4 AND isdeleted=0";  
        $res = $this->db->query($sql)->result();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

	public function getViewSelectedCandidate($token)
	  {
	    
	    try{

	        $sql = "SELECT * FROM `tbl_candidate_registration` 
                  Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  AND `metricpercentage` >= 60 AND 
                  `hscpercentage` >= 60 AND 
                  `ugpercentage` >= 60 OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60 ) ";

	         if (!empty($token)) {
	          $sql .=" AND candidateid=$token";
	        }

	        $res = $this->db->query($sql)->result();

	        return $res;
	     }catch (Exception $e) {
	       print_r($e->getMessage());die;
	     }
	}



  public function getState()
   {
        
        try{

            $sql = "SELECT * FROM state where isdeleted='0' ORDER BY name";

            $res = $this->db->query($sql)->result();

            return $res;

         }catch (Exception $e) {
           print_r($e->getMessage());die;
         }
   }

  

  public function getCampus()
  {
    try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
        $res = $this->db->query($sql)->result();

        return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }


/**
   * Method getdistrict() get district list according to state.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getdistrict()
{
  try{

    $sql = "SELECT * FROM `district`";  
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

  



   function mail_exists($key)
  {
      $this->db->where('emailid',$key);
      $query = $this->db->get('tbl_candidate_registration');
      if ($query->num_rows() > 0){
          return true;
      }
      else{
          return false;
      }
  }


}