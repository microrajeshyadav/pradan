<?php 

/**
* Staff Personnel Approval Model
*/
class Staff_personnel_approval_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



/**
   * Method getAllOffice() get all office details with id.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getAllOffice()
  {
    
    try{
     
    $sql = "SELECT officeid, officename FROM `lpooffice` WHERE `closed`='No' ORDER BY officename ASC ";
 
    $res = $this->db->query($sql)->result();
     return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/** updated by rajat
   * Method getPersonalUserList() get all personnel details with staffid.
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
      INNER join staff as st ON u.staffid = st.staffid
      WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0"; 

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getStaffList() get all staff with id.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffList($officeid, $staffid)
  {
    try{
     
    //$sql = "SELECT staff.staffid, CONCAT(' (', `emp_code` , ') ', `name`) name FROM `staff` LEFT JOIN staff_transaction as st on st.staffid=staff.staffid AND st.trans_status NOT IN ('Transfer', 'BOTH', 'Termination', 'Termination during Probation', 'Death', 'Retirement', 'Discharge simpliciter/ Dismissal', 'Desertion cases') WHERE staff.`status`= 1  AND staff.new_office_id = IFNULL(".$officeid.",staff.new_office_id) ORDER BY name ASC";
    $sql = "SELECT staffid, CONCAT(' (', `emp_code` , ') ', `name`) name FROM `staff` WHERE `status`= 1  AND staff.new_office_id = IFNULL(".$officeid.",new_office_id) AND staffid NOT IN ($staffid) ORDER BY name ASC";
 
    $res = $this->db->query($sql)->result();
     return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

 /**
   * Method getTransferStaffDetail() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffiomtransferDetail($token)
  {
    
    try{
        $sql = "SELECT * FROM  `tbl_iom_transfer` as a Where  a.`transid` =".$token;
      
       $res = $this->db->query($sql)->row();
        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getstaff_iom_transfer() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaff_iom_transfer($token)
  {
    
    try{

       $sql = "SELECT * FROM `staff_transaction` AS a  Where  a.`id` =".$token;
       $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

 /**
   * Method getSingleTransferStaffDetail() get transfer staff details With name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getMailSingleTransferStaffDetail($token)
  {
    
    try{

 $sql = "SELECT
  a.*,
  b.`officename`,
   CONCAT(d.`name` , ' (', md.`desname` , ')') as Staffname, 
   CONCAT(f.`name` , ' (', mdd.`desname` , ')') as reportingname,
  c.`name` AS padmin,
  d.`name` as staffname2,
  d.`emailid` as staffemailid,
  lp.`officename` AS officename,
  lpold.`officename` as oldofficename,
  lpnew.`officename` as newofficename,
  g.`name` as currentresponsibilityto,
  h.`name` as joiningreportprescribedformto,
 j.`officename` as cashadvancefromouroffice
  
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `lpooffice` AS b ON a.`report_for_work_place` = b.`officeid`
LEFT JOIN
  `staff` AS c ON a.`personnel_staffid` = c.`staffid`
LEFT JOIN
  `staff` AS d ON a.`staffid` = d.`staffid`
LEFT JOIN
  `staff` AS f ON a.`integrator_ed` = f.`staffid`
LEFT JOIN
  `staff_transaction` AS stt ON a.`transid` = stt.`id`
LEFT JOIN
  `lpooffice` AS lp ON a.`report_for_work_place` = lp.`officeid`
LEFT JOIN
  `lpooffice` AS lpold ON stt.`old_office_id` = lpold.`officeid`
LEFT JOIN
  `lpooffice` AS lpnew ON stt.`new_office_id` = lpnew.`officeid`
LEFT JOIN
  `msdesignation` AS md ON d.`designation` = md.`desid`
LEFT JOIN
  `msdesignation` AS mdd ON f.`designation` = mdd.`desid`
LEFT JOIN
  `staff` AS g ON a.`current_responsibility_to` = g.`staffid`
LEFT JOIN
  `staff` AS h ON a.`joining_report_prescribed_form_to` = h.`staffid`
LEFT JOIN
  `lpooffice` AS j ON a.`cash_advance_from_our_office` = j.`officeid`
WHERE a.`id` =".$token;

   $res = $this->db->query($sql)->result()[0];
    return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


   /**
   * Method getSingleTransferStaffDetail() get transfer staff details With name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSingleTransferStaffDetail($token)
  {
    
    try{

   
  $sql = "SELECT
  a.*,
  b.`officename`,
   CONCAT(d.`name` , ' (', md.`desname` , ')') as Staffname, 
   CONCAT(f.`name` , ' (', mdd.`desname` , ')') as reportingname,
  c.`name` AS padmin,
  d.`name` as staffname2,
  d.`emailid` as staffemailid,
  lp.`officename` AS officename,
  lpold.`officename` as oldofficename,
  lpnew.`officename` as newofficename,
  g.`name` as currentresponsibilityto,
  h.`name` as joiningreportprescribedformto,
 j.`officename` as cashadvancefromouroffice
  
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `lpooffice` AS b ON a.`report_for_work_place` = b.`officeid`
LEFT JOIN
  `staff` AS c ON a.`personnel_staffid` = c.`staffid`
LEFT JOIN
  `staff` AS d ON a.`staffid` = d.`staffid`
LEFT JOIN
  `staff` AS f ON a.`integrator_ed` = f.`staffid`
LEFT JOIN
  `staff_transaction` AS stt ON a.`transid` = stt.`id`
LEFT JOIN
  `lpooffice` AS lp ON a.`report_for_work_place` = lp.`officeid`
LEFT JOIN
  `lpooffice` AS lpold ON stt.`old_office_id` = lpold.`officeid`
LEFT JOIN
  `lpooffice` AS lpnew ON stt.`new_office_id` = lpnew.`officeid`
LEFT JOIN
  `msdesignation` AS md ON d.`designation` = md.`desid`
LEFT JOIN
  `msdesignation` AS mdd ON f.`designation` = mdd.`desid`
  LEFT JOIN
  `staff` AS g ON a.`current_responsibility_to` = g.`staffid`
    LEFT JOIN
  `staff` AS h ON a.`joining_report_prescribed_form_to` = h.`staffid`
  LEFT JOIN
  `lpooffice` AS j ON a.`cash_advance_from_our_office` = j.`officeid`
 WHERE
  a.`id` =".$token; 

   $res = $this->db->query($sql)->result()[0];
    return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



 /**
   * Method getStaffDetail() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffDetail($transid)
  {
    
    try{
     
   $sql = "SELECT
  c.`emp_code`,
  c.`reportingto`,
  d.desid,
  a.reportingto  as newtc,
  CONCAT(SUBSTRING(c.`name`, 1, LOCATE(' ',c.`name`)) , ' (', md.`desname` , ')') as Staffname, 
  CONCAT(k.`name` , ' (', mdd.`desname` , ')') as reportingtoname,
  k.name as reporting_name,
  e.`officename` as oldoffice,
  f.`officename` AS newoffice,
  a.old_office_id,
  a.new_office_id
    
FROM
  (select * from `staff_transaction` where  `id` =".$transid.") AS a
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid` 
  LEFT JOIN
  `staff` AS k ON a.`reportingto` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON a.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON a.`new_office_id` = f.`officeid`
   LEFT JOIN
  `msdesignation` AS d ON c.`designation` = d.`desid`
  
  LEFT JOIN
  `msdesignation` AS md ON c.`designation` = md.`desid`
  LEFT JOIN
  `msdesignation` AS mdd ON k.`designation` = mdd.`desid`";
  
  
  //echo $sql; die;
  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



    /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getworkflowdetaillist()
  {
    
    try{

    	//print_r($this->loginData); die;
    	
   $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  ms.Acclevel_Name as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  b.`trans_flag` as status,
  g.id AS tbl_clearance_certificate_id,
  (
     CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate' 
     WHEN b.`trans_flag` = 3 THEN 'Accepted By TC/Intergrator' 
     WHEN b.`trans_flag` = 5 THEN 'Accepted By Personnel Unit' 
     WHEN b.`trans_flag` = 4 THEN 'Rejected By TC/Intergrator' 
     WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 7 THEN 'Accepted Clearance Certificated By TC/Intergrator'
     WHEN b.`trans_flag` = 8 THEN 'Rejected Clearance Certificated By TC/Intergrator'
      WHEN b.`trans_flag` = 9 THEN 'Accepted Clearance Certificated By Personnel Unit' 
       WHEN b.`trans_flag` = 10 THEN 'Rejected Clearance Certificated By Personnel Unit'
       WHEN b.`trans_flag` = 11 THEN 'Accepted Clearance Certificated By TC/Intergrator'
     WHEN b.`trans_flag` = 12 THEN 'Rejected Clearance Certificated By TC/Intergrator'
      WHEN b.`trans_flag` = 13 THEN 'Accepted By TC/Intergrator' 
       WHEN b.`trans_flag` = 14 THEN 'Rejected By TC/Intergrator'
       WHEN b.`trans_flag` = 15 THEN 'Accepted By TC/Intergrator'
     WHEN b.`trans_flag` = 16 THEN 'Rejected TC/Intergrator'
      WHEN b.`trans_flag` = 17 THEN 'Transfer Expense By Staff Unit' 
       WHEN b.`trans_flag` = 18 THEN 'Rejected Clearance Certificated By Personnel Unit'
      WHEN b.`trans_flag` = 19 THEN 'Accepted TC/Intergrator' 
       WHEN b.`trans_flag` = 20 THEN 'Rejected TC/Intergrator'
      WHEN b.`trans_flag` = 21 THEN 'Accepted Finance Unit' 
       WHEN b.`trans_flag` = 22 THEN 'Rejected Finance Unit'

     ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a 
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=2)
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid` AND a.`receiver` =".$this->loginData->staffid."  
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`

LEFT JOIN
  `tbl_clearance_certificate` AS g ON g.`transid` = b.`id`

  inner join mstuser m on a.sender=m.staffid 
   inner join mst_workflow_process_stages as mp on m.RoleID=mp.sender_roleid And a.flag=mp.stage
  inner join sysaccesslevel ms on m.RoleID=ms.Acclevel_Cd
  Where b.`trans_flag`>= 3  AND b.`trans_status` = 'Transfer' GROUP BY c.emp_code";

 // echo $sql; die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





    /**
   * Method getseprationworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getseprationworkflowdetaillist()
  {
    
    try{

      //print_r($this->loginData); die;
      
   $sql = "SELECT
  c.`name`,
  tc.id as clearnes_id,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
   h.id as tbl_handed_id,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  a.receiver,
  a.sender,
  l.level,
  l.desid,
  l.desname,
  s.relieved_document,
  b.trans_status,
  b.`trans_flag` as status,
  (
     CASE 
    
      WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit' 
    WHEN b.trans_flag = 4 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' 
      WHEN b.trans_flag = 7 THEN 'Accepted by ED' 
    WHEN b.trans_flag = 8 THEN 'Rejected By ED' 
    WHEN b.`trans_flag` = 11 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 12 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 13 THEN 'Approve By TC/Intergrator' 
    WHEN b.`trans_flag` = 14 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 9 THEN 'Approve By Finance' 
    WHEN b.`trans_flag` = 10 THEN 'Rejected By Finance' 
    WHEN b.`trans_flag` = 15 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 16 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 17 THEN 'Approve By Finance' 
    WHEN b.`trans_flag` = 18 THEN 'Rejected By Finance' 
     WHEN b.`trans_flag` = 19 THEN 'Approve By Head Office Finance' 
    WHEN b.`trans_flag` = 20 THEN 'Rejected By Head Office Finance' 
    WHEN b.`trans_flag` = 21 THEN 'Approve By HR Unit' 
    WHEN b.`trans_flag` = 22 THEN 'Rejected By HR Unit'
     WHEN b.`trans_flag` = 25 THEN 'Handed Over  By Staff'
     WHEN b.`trans_flag` = 27 THEN 'Taken Over  By TC/Intergrator'
    WHEN b.`trans_flag` = 28 THEN 'Approve By Personnel Unit' 
     WHEN b.`trans_flag` = 29 THEN 'Approve By ED' 
     ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
  LEFT JOIN
  `msdesignation` AS l ON c.`designation` = l.`desid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  left join 
  tbl_clearance_certificate   AS tc on b.id=tc.transid
  left Join 
  tbl_hand_over_taken_over_charge as h on b.id=h.transid


  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
  left join 
  tbl_sep_releaseform AS s on s.transid=b.id
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where b.`trans_flag`>= 3  AND (b.`trans_status` ='Termination' OR b.`trans_status` ='Resign' OR b.`trans_status` ='Termination during Probation' OR b.`trans_status` ='Super Annuation' OR b.`trans_status` ='Death' OR b.`trans_status` ='Retirement' OR b.`trans_status` ='Discharge simpliciter/ Dismiss' OR b.`trans_status` ='Desertion cases' OR b.`trans_status` ='Premature retirement') AND a.`receiver` =".$this->loginData->staffid." GROUP BY c.emp_code";

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
  // echo $sql;die();
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

   public function get_transfer_promotion_workflowdetail()
  {
    
    try{

      //print_r($this->loginData); die;
      
   $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  b.`trans_flag` as status,
  z.`desname` as newdesid,
  y.`desname` as olddesid,
  h.id as tbl_clearance_certificate_id,
  (
     CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate' WHEN b.`trans_flag` = 3 THEN 'Accepted By TC/Intergrator' WHEN b.`trans_flag` = 5 THEN 'Accepted By Personnel Unit' WHEN b.`trans_flag` = 4 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' WHEN b.trans_flag = 7 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 8 THEN 'Rejected By TC/Intergrator' WHEN b.trans_flag = 9 THEN 'Accepted By Personnel Unit' WHEN b.trans_flag = 10 THEN 'Rejected By Personnel Unit'  WHEN b.trans_flag = 11 THEN 'HANDING OVER CHARGE BY Staff Unit' WHEN b.trans_flag = 12 THEN 'HANDING OVER CHARGE by staff Unit'   WHEN b.trans_flag = 13 THEN 'TAKING OVER CHARGE Staff Unit' WHEN b.trans_flag = 14 THEN 'TAKING OVER CHARGE Staff Unit'   WHEN b.trans_flag = 15 THEN 'Accepted By TC/Intergrator Unit' WHEN b.trans_flag = 16 THEN 'Rejected By TC/Intergrator Unit'   WHEN b.trans_flag = 17 THEN 'TRANSFER EXPENSES CLAIM By Staff Unit' WHEN b.trans_flag = 18 THEN 'TRANSFER EXPENSES CLAIM By Staff Unit'   WHEN b.trans_flag = 19 THEN 'Accepted By TC/Intergrator Unit' WHEN b.trans_flag = 20 THEN 'Rejected By TC/Intergrator Unit'   WHEN b.trans_flag = 21 THEN 'Accepted By Finance Unit' WHEN b.trans_flag = 22 THEN 'Rejected By Finance Unit' ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=6)
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  LEFT JOIN 
  `msdesignation` AS z ON b.`new_designation` = z.`desid`
  LEFT JOIN
  `msdesignation` AS y ON b.`old_designation` = y.`desid`
  LEFT JOIN
  `tbl_clearance_certificate` AS h ON h.`transid` = b.`id`
  Where  b.`trans_status` = 'BOTH'
  AND a.`receiver` =".$this->loginData->staffid." GROUP BY c.emp_code";

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function get_promotion_workflowdetail()
  {
    
    try{

      //print_r($this->loginData); die;
      
  $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  b.`trans_flag` as status,
  z.`desname` as newdesid,
  y.`desname` as olddesid,
  (
     CASE 
  WHEN b.trans_flag =1 THEN 'Process Initiate' 
  WHEN b.trans_flag=3 THEN 'Accepted By TC/Intergrator'
  WHEN b.trans_flag=5 THEN 'Accepted By Personnel Unit'WHEN b.trans_flag = 4 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 7 THEN 'Fill Handed over By Staff'
    WHEN b.`trans_flag` = 8 THEN 'Rejected By Staff' WHEN b.`trans_flag` = 9 THEN 'Approved By TC' WHEN b.`trans_flag` = 10 THEN 'Fill Joining Report  By Staff' 
    WHEN b.`trans_flag` = 11 THEN 'Approved Joining Report  By New Tc'
    WHEN b.`trans_flag` = 12 THEN 'Rejected Joining Report  By New Tc'
    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  LEFT JOIN 
  `msdesignation` AS z ON b.`new_designation` = z.`desid`
  LEFT JOIN
  `msdesignation` AS y ON b.`old_designation` = y.`desid`
  Where b.`trans_flag`>=3  AND b.`trans_status` = 'Promotion'
  AND a.`receiver` =".$this->loginData->staffid." GROUP BY c.emp_code"; 
  // echo $sql;
  // die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getTransferStaffDetails() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTransferStaffDetails($transid)
  {
    
    try{
        $sql = "SELECT * FROM `staff_transaction` WHERE `id` =".$transid;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method gettbltransferflag() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function gettbltransferflag($transid)
  {
    
    try{
         $sql = "SELECT flag,filename FROM `tbl_iom_transfer` WHERE `transid` =".$transid; 

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function geteddetail(){
  $query = "SELECT name, staffid FROM staff WHERE designation=2";
  return $this->db->query($query)->row();
}


}