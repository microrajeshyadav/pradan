<?php 

/**
* State Model
*/
class Gratuity_nomination_form_staff_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}
  public function getCandidateWithAddressDetails($token)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    msdesignation.desname AS desiname,
    staff.father_name,
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.reportingto
    
    
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid

LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid
WHERE
    staff.staffid='$token'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


public  function do_flag($staff_id) {
  //die("hello");

  $sql= "SELECT
   
    pr.status as provident_flag,
    gd.isupdate as graduity_flag,
    n.status as nomination_flag
    
FROM  staff as h
left join tbl_general_nomination as n ON
h.staffid=n.staff_id
left join tbl_graduitynomination as gd ON
h.staffid=gd.staff_id
left join provident_fund_nomination as pr ON
h.staffid=pr.staff_id


WHERE
    h.staffid =$staff_id";
 //echo $sql;
  //die("pooja");
  $tmmt= $this->db->query($sql)->row();

  return $tmmt;
}

  /**
   * Method get max get_gratuityworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  row
   */

public function get_gratuityworkflowid($token)
{

  try{

    $sql = "SELECT workflowid as workflow_id, tbl_workflowdetail.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=24 and tbl_workflowdetail.staffid=$token ORDER BY workflowid DESC limit 1";
    
//echo $sql;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
	public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT  tbl_graduitynomination_detail.id as pro_id,tbl_graduitynomination_detail.share_nominee,tbl_graduitynomination_detail.sr_no,tbl_graduitynomination_detail.minior,tbl_graduitynomination_detail.graduity_id,tbl_graduitynomination_detail.name,tbl_graduitynomination_detail.relation_id,tbl_graduitynomination_detail.age,tbl_graduitynomination_detail.address,sysrelation.relationname,sysrelation.status,sysrelation.id FROM `tbl_graduitynomination_detail`  
                 left join sysrelation on   `tbl_graduitynomination_detail`.relation_id =  `sysrelation`.id  
                 left join   tbl_graduitynomination on  tbl_graduitynomination_detail.graduity_id = tbl_graduitynomination.graduity_id
                Where tbl_graduitynomination.graduity_id ='.$token.''; 
               //echo $sql;
                 

        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function get_pfinformation($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_graduitynomination`  

                Where `tbl_graduitynomination`.graduity_id ='.$token.''; 
                //echo $sql;
               // die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

}