<?php 

/**
* Dashboard Model
*/
class Joining_induction_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

	  public function Approve($token,$role)
	  {
	  	try
	  	{


            $sql = "SELECT
    *
FROM
    (SELECT
    cnt.cnt,
    b.staffid,
    b.doj,
    b.candidateid,
    b.name,
    b.dob,

    b.emailid,
    a.flag,
     CASE WHEN cnt.cnt >= 11 THEN a.type ELSE 0
END type,
CASE WHEN cnt.cnt >= 11 THEN replace(replace(c.process_type,'_',' '),'Joining induction','') ELSE CONCAT(
    cnt,
    ' joining induction forms submitted out of 11'
)
END process_type,

     tbl_graduitynomination.graduity_id
FROM
    `tbl_workflowdetail` AS a
INNER JOIN staff AS b
ON
    a.staffid = b.staffid
INNER JOIN mst_workflow_process AS c
ON
    a.type = c.id
LEFT JOIN tbl_graduitynomination ON tbl_graduitynomination.staff_id = a.staffid
LEFT JOIN(
    SELECT
        staffid,
        COUNT(TYPE) cnt
    FROM
        tbl_workflowdetail
    WHERE
        receiver = '$token' AND TYPE IN(
            8,
            9,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            23,
            24,
            25
        ) AND flag = 1
   
GROUP BY
    staffid
) cnt
ON
    a.staffid = cnt.staffid 
WHERE
    a.type IN(
        8,
        9,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        23,
        24,
        25
    ) AND a.flag = 1 AND a.receiver = '$token' ) a
GROUP BY
    cnt,
    staffid,
    candidateid,
    NAME,
    dob,
    emailid,
    flag,
    process_type,
    graduity_id" ;

             // echo $sql;
             // die;
				
             $result = $this->db->query($sql)->result();
//              echo "<pre>";
// print_r($result); die();
           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }

	 public function Approveprocess($token,$role)
	  {
	  	try
	  	{

            $sql = "SELECT b.staffid,b.candidateid,b.name,b.dob,b.emailid,a.flag,a.type,c.process_type 
				FROM `tbl_workflowdetail` as a 
				inner join staff as b on a.staffid=b.staffid 
				INNER join mst_workflow_process as c ON a.type = c.id
				WHERE a.type and `type`BETWEEN 8 and 20 and a.flag = 4 and 
				a.receiver='$token'";
             $result = $this->db->query($sql)->result();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }
	
}