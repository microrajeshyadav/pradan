<?php 

/**
* Master  Model
*/
class Developmentapprentice_sepchecksheet_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}  



/**
   * Method get_staff_sep_detail() get the staff detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */



	public function get_staff_sep_detail($token)
   {
      try
	  {

	    $sql="SELECT c.name,c.permanentpincode, c.candidateid, c.contact,c.emailid,a.date_of_transfer as seperatedate,  a.staffid, b.desname as sepdesig, a.new_designation as sepdesigid, c.emp_code, CONCAT(IFNULL(c.permanenthno,''), IFNULL(c.permanentstreet,''), IFNULL(c.permanentcity,''),IFNULL(c.permanentdistrict,'') , IFNULL(s.name,''),'-',IFNULL(c.permanentpincode,'')) as address, st.date_of_transfer as joiningdate, d.desname as joindesig, a.trans_flag, a.reason, a.createdon, lp.officename as newoffice, st.trans_status, dc.dc_cd, msdc.dc_name  FROM staff_transaction as a
	       INNER JOIN `staff` as c on a.staffid = c.staffid 
	       left join `state` as s ON s.id = c.permanentstateid
	       inner join staff_transaction as st
	        on st.staffid = c.staffid and st.trans_status = 'JOIN'
	       left JOIN `msdesignation` as b on a.new_designation = b.desid
	       left JOIN `msdesignation` as d on st.new_designation = d.desid
	       LEFT JOIN `lpooffice` as lp on a.new_office_id = lp.officeid 
	       LEFT JOIN `dc_team_mapping` as dc on c.new_office_id = dc.teamid 
    LEFT JOIN  `msdc_details` as msdc ON dc.dc_cd = msdc.dc_cd
	     WHERE a.id = $token";
	     
 		 //echo $sql; die;

	     //$query=$this->db->get();
	     return  $this->db->query($sql)->row();

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}



 /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
       try
   {

    	$sql="SELECT
		staff.name,
		staff.emailid,
		staff.emp_code,
		staff_transaction.staffid,
		staff_transaction.trans_status,
		staff_transaction.date_of_transfer,
		staff_transaction.new_office_id,
		staff_transaction.new_designation,
		staff.reportingto,
		lpooffice.officename,
		lp_old.officename as oldofficename,
		msdesignation.desname,
		staff_transaction.date_of_transfer

		FROM
		staff_transaction
		INNER JOIN
		(
		SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
left join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
left join lpooffice as lp_old on staff_transaction.old_office_id=lp_old.officeid
left join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";
  // echo $sql;die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


}