<?php 


class Provident_fund_nomination_form_staff_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

public  function do_flag($staff_id) {
  

  $sql= "SELECT
   
    pr.status as provident_flag,
    gd.isupdate as graduity_flag,
    n.status as nomination_flag
    
FROM  staff as h
left join tbl_general_nomination as n ON
h.staffid=n.staff_id
left join tbl_graduitynomination as gd ON
h.staffid=gd.staff_id
left join provident_fund_nomination as pr ON
h.staffid=pr.staff_id


WHERE
    h.staffid =$staff_id";
 //echo $sql;
  //die("pooja");
  $tmmt= $this->db->query($sql)->row();

  return $tmmt;
}

	public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT  provident_fund_nomination_details.id as pro_id,provident_fund_nomination_details.share_nominee,provident_fund_nomination_details.sr_no,provident_fund_nomination_details.minior,provident_fund_nomination_details.provident_id,provident_fund_nomination_details.name,provident_fund_nomination_details.relation_id,provident_fund_nomination_details.age,provident_fund_nomination_details.address,sysrelation.relationname,sysrelation.status,sysrelation.id FROM `provident_fund_nomination_details`  
                 left join sysrelation on   `provident_fund_nomination_details`.relation_id =  `sysrelation`.id  
                    left join provident_fund_nomination on   `provident_fund_nomination`.id =  `provident_fund_nomination_details`.provident_id
                Where `provident_fund_nomination`.id ='.$token.''; 
               // echo $sql;
                 

        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method get_pfinformation()details of provident fund nomination.
   * @access  public
   * @param Null
   * @return  row
   */

  public function get_pfinformation($token)
  {
    
    try{

          $sql = 'SELECT * FROM `provident_fund_nomination`         
                Where `provident_fund_nomination`.id ='.$token.''; 
                //echo $sql;

                //die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method count_pfinformation(()count records of provident fund nomination.
   * @access  public
   * @param Null
   * @return  row
   */

  public function count_pfinformation($token)
  {
    
    try{

          $sql = 'SELECT count(*) as count_nomination FROM `provident_fund_nomination`         
                Where `provident_fund_nomination`.id ='.$token.''; 
               // die();
                //echo $sql;    
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  /**
   * Method staffName() get Staff Name.
   * @access  public
   * @param Null
   * @return  Array
   */


public function staffName($staff_id,$login_staff)
  {
   
    try{
     
     
           $sql = "SELECT * FROM staff where staffid !=$staff_id and staff.staffid!=$login_staff order by staff.name asc";  
           

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}

/**
   * Method get max get_providentworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_providentworkflowid($token)
{

  try{

    $sql = "SELECT max(`workflowid`) as workflow_id FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=23 and staff.staffid=$token";
    
//echo $sql;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function getCandidateWithAddressDetails($token)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    staff.father_name,
    msdesignation.desname AS desiname,
    
    state.name,
    lpooffice.officename,
    staff.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
    
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid

LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid=".$token;
                          
        
        $result = $this->db->query($sql)->row();
        // print_r($result);
        // die;

        return $result;

     }
     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  public function tc_email($token)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$token'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}