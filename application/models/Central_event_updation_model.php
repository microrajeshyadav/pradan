<?php 

/**
* Central event updation model Model
*/
class Central_event_updation_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}
/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
  	try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

/**
	 * Method getResoucePerson() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getResoucePerson()
	{
		
		try{

		$sql = "SELECT * FROM  `mst_resource_person` WHERE `isdeleted`=0";
		

		$result = $this->db->query($sql)->result();
		
		return $result;

	}catch (Exception $e) {
			print_r($e->getMessage());die;
	}
		
	}

/**
	 * Method getCentralEventList() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getCentralEventList()
	{

		try{

			 $sql = "SELECT 
	`tbl_central_event`.id as centraleventid, 
	`tbl_central_event`.name as eventid, 
	`tbl_central_event`.from_date, 
	`tbl_central_event`.to_date, 
	`tbl_central_event`.place, 
	`mstevents`.name,
    `tbl_central_event_updation_metrial`.original_document_upload,
    `tbl_central_event_updation_metrial`.encrypted_document_upload
FROM 
	`tbl_central_event` 
	inner join `mstevents` on `tbl_central_event`.name = `mstevents`.id 
    inner join `tbl_central_event_updation_metrial` on `tbl_central_event_updation_metrial`.central_event_id = `tbl_central_event`.id 
    WHERE `tbl_central_event`.isdeleted = 0
ORDER BY `tbl_central_event`.id ASC "; 

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCentralEvent() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getCentralEvent()
	{

		try{

			 $sql = "SELECT 
			`tbl_central_event`.id as centraleventid, 
			`tbl_central_event`.name as eventid, 
			`mstevents`.name			
    		FROM `tbl_central_event` 
			inner join `mstevents` on `tbl_central_event`.name = `mstevents`.id 
			WHERE `tbl_central_event`.isdeleted = 0 AND `tbl_central_event`.id NOT IN (SELECT `central_event_id` FROM `tbl_central_event_updation_metrial`)
			ORDER BY `tbl_central_event`.id ASC ";
			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




	/**
	 * Method getCentralEventEdit() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getCentralEventEdit()
	{

		try{

		  $sql = "SELECT 
			`tbl_central_event`.id as centraleventid, 
			`tbl_central_event`.name as eventid, 
			`mstevents`.name			
    		FROM `tbl_central_event` 
			inner join `mstevents` on `tbl_central_event`.name = `mstevents`.id 
			WHERE `tbl_central_event`.isdeleted = 0
			ORDER BY `tbl_central_event`.id ASC ";
			$result = $this->db->query($sql)->result(); 

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	/**
	 * Method getResoucePerson() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getPaticipants($token)
	{

		try{

		$sql = "SELECT * FROM `tbl_participant` WHERE `central_event_id`=$token AND `central_event_join_status` = 1 ";

		 $result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


   /**
	 * Method getSingleCentralEvent() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getSingleCentralEvent($token=NULL)
	{
		
		try{

			$sql = " SELECT * FROM `tbl_central_event` WHERE `id`=$token ";

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}



  /**
	 * Method getEventDocument() get all Document .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getEventDocument($token=NULL)
	{
		
		try{

			 $sql = " SELECT * FROM `tbl_central_event_updation_metrial` WHERE `central_event_id`=$token ";

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	/**
	 * Method getSingleResoucePerson() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getSingleResoucePerson($token=NULL)
	{
		
		try{

			$sql = " SELECT `resouce_person_id` FROM `tbl_central_event_transaction` WHERE `central_event_id`=$token ";

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


/**
	 * Method getEvent() get all Event List .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getEvent()
	{

		
		try{

			$sql = " SELECT * FROM `mstevents` Where `isdeleted`= 0 ORDER BY id DESC";

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}




/**
	 * Method CurrentFinancialYear() get Current Financial Year  .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */


public function CurrentFinancialYear()
{

	try{

		$today = @date("Y-m-d");
		$curyear=@date("Y");
		$nextyear=@date("Y")+1;
		$curfin=$curyear.'-'.'03'.'-'.'31';
		$nextfin=$nextyear.'-'.'04'.'-'.'01'; 
		if($today > $curfin && $today < $nextfin) 
			$rowyear= $curyear.'-'.$nextyear; 
		if($today <= $curfin)
			$rowyear=($curyear-1).'-'.$curyear;
		return $rowyear;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


	/**
	 * Method getDevelopmentShip() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getDevelopmentShip($financialyear)
	{
		
		try{

			  $sql = "SELECT 
	`tblcr`.candidateid, 
	`tblcr`.emailid, 
	`tblcr`.candidatefirstname, 
	`tblcr`.candidatemiddlename, 
	`tblcr`.candidatelastname 
FROM 
	`mstbatch` AS `mstb` 
	INNER JOIN `tbl_candidate_registration` AS `tblcr` ON `tblcr`.batchid = `mstb`.id 
    INNER JOIN `mstfinancialyear` AS `fy` ON `fy`.id = `mstb`.financial_year 
Where 
	`mstb`.status = 0 
	AND `tblcr`.joinstatus = 1 
	AND `fy`.financialyear = '".$financialyear."' "; 

			$result = $this->db->query($sql)->result();
			//print_r($result); die;

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


}