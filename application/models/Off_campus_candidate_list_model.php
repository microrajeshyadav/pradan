<?php 

/**
* Off_campus_candidate_list Model
*/
class Off_campus_candidate_list_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "-" )
    $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
  // echo substr($Date,$len,-4);
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
      $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


    
  public function getSelectedCandidate()
  {
    
    try{

           $sql = "SELECT floor(datediff(`tbl_gap_year`.todate,`tbl_gap_year`.fromdate)/365) AS DateDiff,`tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`mstcampus`.`campusname`,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,
         `tbl_candidate_registration`.frstatus,
        `tbl_candidate_registration`.emailid,
        `tbl_candidate_registration`.mobile,
        `tbl_candidate_registration`.metricpercentage,
        `tbl_candidate_registration`.hscpercentage,
        `tbl_candidate_registration`.ugpercentage,
        `tbl_candidate_registration`.pgpercentage,
        COALESCE(NULLIF(otherspecialisation,''), NULLIF(ugspecialisation,''), NULLIF(pgspecialisation,'') ) stream,
        `tbl_candidate_communication_address`.permanentcity,
        `state`.name as statename,
        `mstcategory`.categoryname,
        (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender
         FROM `tbl_candidate_registration` 
         inner join `tbl_candidate_communication_address` on 
         `tbl_candidate_registration`.candidateid =
         `tbl_candidate_communication_address`.candidateid
      left join `tbl_gap_year` on `tbl_gap_year`.candidateid=`tbl_candidate_registration`.candidateid
     LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
         inner join `tbl_campus_intimation` on 
         `tbl_campus_intimation`.id =
         `tbl_candidate_registration`.campusintimationid

         left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
         inner JOIN `state` ON
         `tbl_candidate_communication_address`.permanentstateid = `state`.statecode
            Where `tbl_campus_intimation`.mailstatus = 1 

            and CASE when trim(pgdegree) ='' or trim(pgdegree) is null then 1 else CASE WHEN ((trim(pgdegree) ='M.A.' OR trim(pgdegree) ='M.A' OR trim(pgdegree) ='MA' OR trim(pgdegree) ='Masters in Art') && pgpercentage >= 55) THEN '1' WHEN ((trim(pgdegree) !='M.A.' OR trim(pgdegree) !='M.A' OR trim(pgdegree) !='MA' OR trim(pgdegree) !='Masters in Art') && pgpercentage >= 60) THEN 1 ELSE 0 END END = 1 and CASE WHEN (ugpercentage >= 50 && ugpercentage < 60) THEN '1' WHEN ugpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (metricpercentage >= 50 && metricpercentage < 60) THEN '1' WHEN metricpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (hscpercentage >= 50 && hscpercentage < 60) THEN '1' WHEN hscpercentage >= 60 THEN 2 ELSE 0 END >= 5 
 AND TIMESTAMPDIFF(YEAR,dateofbirth, CONCAT(YEAR(CURDATE()),'-10-01')) <= CASE WHEN `tbl_candidate_registration`.categoryid = 1 then 28 ELSE 30 end  
           AND `tbl_candidate_registration`.`campusid`= 0 AND  
           `tbl_candidate_registration`.`inprocess`= 'open'
           AND (`tbl_candidate_registration`.`frstatus` IS NULL
           OR  `tbl_candidate_registration`.`frstatus` IS NULL) ";

         $sql .= " ORDER BY `tbl_candidate_registration`.createdon DESC";

        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  /**
   * Method getHrdemailid() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getHrdemailid()
{
 try{

      $sql = "SELECT
    staff.staffid,staff.name,staff.emailid
FROM
    `mstuser`
INNER JOIN staff ON mstuser.staffid = staff.staffid
inner join role_permissions on  role_permissions.RoleID=mstuser.RoleID
WHERE

    role_permissions.Controller = 'Campusinchargeintimation' and role_permissions.RoleID=16 and role_permissions.Action='index' ORDER BY
  `staff`.staffid  DESC  LIMIT 0,1
 ";
 //echo $sql;

      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

  /*
     function  gap_year_details is used details of gap year 
  */

public function gap_year_details()
  {
    
    try{

         $sql = "SELECT  * from tbl_gap_year inner join tbl_candidate_registration on tbl_gap_year.candidateid=tbl_candidate_registration.candidateid"; 

        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
public function getSelectedAccptedCandidate()
  {
    
    try{

         $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,
         `tbl_candidate_registration`.frstatus,
        `tbl_candidate_registration`.emailid,
        `tbl_candidate_registration`.mobile,
        `tbl_candidate_registration`.metricpercentage,
        `tbl_candidate_registration`.hscpercentage,
        `tbl_candidate_registration`.ugpercentage,
        `tbl_candidate_registration`.ugspecialisation,
        `tbl_candidate_communication_address`.permanentcity,
        `state`.name as statename,`mstcategory`.categoryname
         FROM `tbl_candidate_registration` 
         inner join `tbl_candidate_communication_address` on 
         `tbl_candidate_registration`.candidateid =
         `tbl_candidate_communication_address`.candidateid
          left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
         inner JOIN `state` ON
         `tbl_candidate_communication_address`.permanentstateid = `state`.statecode
       
       Where CASE when trim(pgdegree) ='' or trim(pgdegree) is null then 1 else CASE WHEN ((trim(pgdegree) ='M.A.' OR trim(pgdegree) ='M.A' OR trim(pgdegree) ='MA' OR trim(pgdegree) ='Masters in Art') && pgpercentage >= 55) THEN '1' WHEN ((trim(pgdegree) !='M.A.' OR trim(pgdegree) !='M.A' OR trim(pgdegree) !='MA' OR trim(pgdegree) !='Masters in Art') && pgpercentage >= 60) THEN 1 ELSE 0 END END = 1 and CASE WHEN (ugpercentage >= 50 && ugpercentage < 60) THEN '1' WHEN ugpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (metricpercentage >= 50 && metricpercentage < 60) THEN '1' WHEN metricpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (hscpercentage >= 50 && hscpercentage < 60) THEN '1' WHEN hscpercentage >= 60 THEN 2 ELSE 0 END >= 5 
      AND TIMESTAMPDIFF(YEAR,dateofbirth, CONCAT(YEAR(CURDATE()),'-10-01')) <= CASE WHEN `tbl_candidate_registration`.categoryid = 1 then 28 ELSE 30 end 

      AND `tbl_candidate_registration`.`campusid`= 0 AND  `tbl_candidate_registration`.`categoryid`= 1 
      AND `tbl_candidate_registration`.`frstatus` ='accepted' AND `tbl_candidate_registration`.`inprocess` ='open'  "; // die;


        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  
 	

public function getRejectedCandidate()
  {
    
    try{

         $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,
         `tbl_candidate_registration`.frstatus,
        `tbl_candidate_registration`.emailid,
        `tbl_candidate_registration`.mobile,
        `tbl_candidate_registration`.metricpercentage,
        `tbl_candidate_registration`.hscpercentage,
        `tbl_candidate_registration`.ugpercentage,
        `tbl_candidate_registration`.ugspecialisation,
        `tbl_candidate_communication_address`.permanentcity,
        `state`.name as statename, `mstcategory`.categoryname
         FROM `tbl_candidate_registration` 
         inner join `tbl_candidate_communication_address` on 
         `tbl_candidate_registration`.candidateid =
         `tbl_candidate_communication_address`.candidateid
         left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
         inner JOIN `state` ON
         `tbl_candidate_communication_address`.permanentstateid = `state`.statecode
       
       Where CASE when trim(pgdegree) ='' or trim(pgdegree) is null then 1 else CASE WHEN ((trim(pgdegree) ='M.A.' OR trim(pgdegree) ='M.A' OR trim(pgdegree) ='MA' OR trim(pgdegree) ='Masters in Art') && pgpercentage >= 55) THEN '1' WHEN ((trim(pgdegree) !='M.A.' OR trim(pgdegree) !='M.A' OR trim(pgdegree) !='MA' OR trim(pgdegree) !='Masters in Art') && pgpercentage >= 60) THEN 1 ELSE 0 END END = 1 and CASE WHEN (ugpercentage >= 50 && ugpercentage < 60) THEN '1' WHEN ugpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (metricpercentage >= 50 && metricpercentage < 60) THEN '1' WHEN metricpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (hscpercentage >= 50 && hscpercentage < 60) THEN '1' WHEN hscpercentage >= 60 THEN 2 ELSE 0 END >= 5 
 AND TIMESTAMPDIFF(YEAR,dateofbirth, CONCAT(YEAR(CURDATE()),'-10-01')) <= CASE WHEN `tbl_candidate_registration`.categoryid = 1 then 28 ELSE 30 end
 
           AND `tbl_candidate_registration`.`campusid`= 0 AND  `tbl_candidate_registration`.`categoryid`= 1 
           AND `tbl_candidate_registration`.`frstatus` ='rejected' AND `tbl_candidate_registration`.`inprocess` ='open'  "; //die;

        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }	





  /**
   * Method getCandidateDetailsPreview() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateDetailsPreview($token)
{

  try{

   $sql = 'SELECT `tbl_candidate_registration`.*, `tbl_candidate_communication_address`.*,a.`name` as presentstatename, ad.`name` as presentdistrictname,b.`name` as permanentstatename, bd.`name` as permanentdistrictname FROM `tbl_candidate_registration` 
    left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid 
     left join `state` as a ON `tbl_candidate_communication_address`.presentstateid = a.statecode 
     left join `district` as ad ON `tbl_candidate_communication_address`.presentdistrict = ad.districtid 
     left join `state` as b ON `tbl_candidate_communication_address`.permanentstateid = b.statecode 
     left join `district` as bd ON `tbl_candidate_communication_address`.permanentdistrict = bd .districtid Where `tbl_candidate_registration`.candidateid ='.$token.''; 


   $result = $this->db->query($sql)->result();

   return $result[0];

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}




 /**
   * Method getCountTrainingExposure() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountTrainingExposure($token)
  {
    
    try{
          $sql = 'SELECT count(*) as `TEcount` FROM `tbl_training_exposure`      
                Where `tbl_training_exposure`.candidateid ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateTrainingExposureDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateTrainingExposureDetails($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_training_exposure` Where `tbl_training_exposure`.candidateid ='.$token.''; 
       
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCountWorkExprience() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountWorkExprience($token)
  {
    
    try{
          $sql = 'SELECT count(*) as `WEcount` FROM `tbl_work_experience`      
                Where `tbl_work_experience`.candidateid ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateWorkExperienceDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateWorkExperienceDetails($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_work_experience` Where `tbl_work_experience`.candidateid ='.$token.''; 
       
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


   /**
   * Method getCountLanguage() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountLanguage($token)
  {
    
    try{
          $sql = 'SELECT count(*) as `Lcount` FROM `tbl_language_proficiency`      
                Where `tbl_language_proficiency`.candidateid ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateLanguageDetails($token)
  {
    
    try{
          $sql = 'SELECT * FROM `tbl_language_proficiency`
          INNER JOIN `syslanguage` on `syslanguage`.lang_cd= `tbl_language_proficiency`.languageid  Where `tbl_language_proficiency`.candidateid ='.$token.''; 
       
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateOtherInformationDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateOtherInformationDetails($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_other_information` Where `tbl_other_information`.candidateid ='.$token.''; 
       
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getUgEducation() get Post Under Graduation Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function getUgEducation()
  {
    
    try{

        $sql = "SELECT id,ugname FROM `mstugeducation` Where isdeleted=0";

        $res = $this->db->query($sql)->result();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getPgEducation() get Post Graduation Education List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getPgEducation()
  {
    
    try{

        $sql = "SELECT id,pgname FROM `mstpgeducation` Where isdeleted=0";
        
        $res = $this->db->query($sql)->result();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  /**
   * Method getViewSelectedCandidate() get Selected Candidates Listing.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewSelectedCandidate($token)
    {
      
      try{

          $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";

           if (!empty($token)) {
            $sql .=" AND candidateid=$token";
          }

          $res = $this->db->query($sql)->result();

          return $res;
       }catch (Exception $e) {
         print_r($e->getMessage());die;
       }
  }

  /**
   * Method getState() get State name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getState()
   {
        
        try{

            $sql = "SELECT * FROM state where isdeleted='0' ORDER BY name";

            $res = $this->db->query($sql)->result();

            return $res;

         }catch (Exception $e) {
           print_r($e->getMessage());die;
         }
   }

   /**
   * Method getCampus() get Campus Name.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function getCampus()
  {
    try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
        $res = $this->db->query($sql)->result();

        return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }

  /**
   * Method mail_exists() get Email Id Exist OR Not.
   * @access  public
   * @param Null
   * @return  Array
   */

   function mail_exists($key)
  {
    try{
      $this->db->where('emailid',$key);
      $query = $this->db->get('tbl_candidate_registration');
      if ($query->num_rows() > 0){
          return true;
      }
      else{
          return false;
      }
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
  }

 /**
   * Method getSysLanguage() get Language Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysLanguage()
  {

    try{
      $this->db->select('*');
      $this->db->from('syslanguage');
      return $this->db->get()->result(); //echo $this->db->last_query(); die;
    
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


 /**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $this->db->select('*');
      $this->db->from('sysrelation');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getSysIdentity() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysIdentity()
  {

    try{
      $this->db->select('*');
      $this->db->from('sysidentity');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }

 public function getDistrict()
  {
    try{

     $sql = "SELECT * FROM `district`"; 
     $result = $this->db->query($sql)->result();
     return $result;
    
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  /**
   * Method getCountGapYear() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCountGapYear($token)
 {

  try{
    $sql = 'SELECT count(*) as `GYcount` FROM `tbl_gap_year`      
    Where `tbl_gap_year`.candidateid ='.$token.''; 
    $result = $this->db->query($sql)->result();

    return $result[0];

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateGapYearDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateGapYearDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_gap_year` Where `tbl_gap_year`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


}