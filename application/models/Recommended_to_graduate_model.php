<?php 

/**
* Master  Model
*/
class Recommended_to_graduate_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

    }  
    public function getRecommendedDA()
    {
      try{
        $sql = "SELECT
                    s.emp_code,
                    s.name,
                    s.staffid,
                    s.emailid,
                    s.IntegratorApproval,
                    mstcampus.campusname,
                    tbl_da_summary.transid,
                    ta.flag,
                    ta.sendflag,
                    ta.id,
                    ta.filename,
                    s.candidateid,
                    edcomments.status as edflag,
                    
                    (
                        CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
                    END
                    ) AS gender

                    FROM
                    `staff`  s
                     inner JOIN
                    `tbl_da_personal_info` AS `b` ON `b`.staffid = `s`.staffid
                    inner JOIN
                    `tbl_candidate_registration` AS tb ON `tb`.candidateid = `s`.candidateid
                    inner JOIN
                    `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid` 
                    left join 
                    `edcomments` ON `edcomments`.candidateid = tb.`candidateid` 
                    left JOIN
                    `tbl_da_summary` ON `tb`.candidateid = `tbl_da_summary`.`candidateid` 
                    left JOIN
                    `tbl_offer_of_appointment` AS ta ON `ta`.transid = `tbl_da_summary`.`transid`  
                    where s.recommend_to_graduate=1";
        $res = [];
        $res = $this->db->query($sql)->result();
        return $res;
        }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
    }


 public function  getOfferDetails($token)
{

  try{


    if ($this->loginData->RoleID ==17) {
      $sql = "SELECT `tbl_candidate_registration`.categoryid,
      `mstcategory`.categoryname, `staff`.name,
      `msdesignation`.`desname`,
      `district`.name as districtname,
      `msdc_details`.dc_name,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.`candidatemiddlename`, `tbl_candidate_registration`.`candidatelastname`,`tbl_candidate_registration`.`emailid`,`lpooffice`.`officename`,`lpooffice`.`street`,`lpooffice`.`street`,`lpooffice`.`district`,`lpooffice`.`phone1`,`lpooffice`.`email`, `tbl_candidate_communication_address`.presentstreet, `tbl_candidate_communication_address`.presentcity, `tbl_candidate_communication_address`.presentstateid, `tbl_candidate_communication_address`.presentdistrict,`tbl_candidate_communication_address`.presentpincode,`state`.name as statename,`tbl_da_summary`.basic FROM `tbl_candidate_registration` 
      INNER JOIN `mstcategory` ON `tbl_candidate_registration`.`categoryid` = `mstcategory`.`id`
      Inner join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
      Inner join `lpooffice` ON `tbl_candidate_registration`.teamid = `lpooffice`.officeid
      Inner join `state` ON `tbl_candidate_communication_address`.presentstateid = `state`.id
      Inner join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid
      INNER JOIN
      `staff` ON  `lpooffice`.`staffid` = `staff`.`staffid`
      INNER JOIN
      `msdesignation` ON  `staff`.`designation` = `msdesignation`.`desid`
      LEFT JOIN
      `dc_team_mapping` ON `tbl_candidate_registration`.teamid = `dc_team_mapping`.teamid
      LEFT JOIN
      `msdc_details` ON `dc_team_mapping`.`dc_cd` = `msdc_details`.dc_cd
      LEFT JOIN
      `tbl_da_summary` ON `tbl_da_summary`.`candidateid` = $token

      Where `tbl_candidate_registration`.candidateid =$token"; 
      $query = $this->db->query($sql);
      $res = $this->db->query($sql)->result();
    }else{
      $sql = "SELECT `tbl_candidate_registration`.categoryid,
      `mstcategory`.categoryname, st.name,
      `msdesignation`.`desname`,
      `district`.name as districtname,
      `msdc_details`.dc_name,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.`candidatemiddlename`, `tbl_candidate_registration`.`candidatelastname`,`tbl_candidate_registration`.`emailid`,`mstbatch`.batch,`lpooffice`.`officename`,`lpooffice`.`street`,`lpooffice`.`street`,`lpooffice`.`district`,`lpooffice`.`phone1`,`lpooffice`.`email`,st.`name` as fieldguide, `tbl_candidate_communication_address`.presentstreet, `tbl_candidate_communication_address`.presentcity, `tbl_candidate_communication_address`.presentstateid, `tbl_candidate_communication_address`.presentdistrict,`tbl_candidate_communication_address`.presentpincode,`state`.name,`tbl_da_summary`.basic as statename
      FROM `tbl_candidate_registration` 
      INNER JOIN `mstcategory` ON `tbl_candidate_registration`.`categoryid` = `mstcategory`.`id`
      Inner join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
      Inner join `lpooffice` ON `tbl_candidate_registration`.teamid = `lpooffice`.officeid
      Inner join `staff` as st ON `tbl_candidate_registration`.fgid = st.staffid
      Inner join `mstbatch` ON `tbl_candidate_registration`.batchid = `mstbatch`.id
      Inner join `state` ON `tbl_candidate_communication_address`.presentstateid = `state`.id
      Inner join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid
      INNER JOIN
      `staff` as ts ON  `lpooffice`.`staffid` = ts.`staffid`
      INNER JOIN
      `msdesignation` ON  st.`designation` = `msdesignation`.`desid`
      LEFT JOIN
      `dc_team_mapping` ON `tbl_candidate_registration`.teamid = `dc_team_mapping`.teamid
      LEFT JOIN
      `msdc_details` ON `dc_team_mapping`.`dc_cd` = `msdc_details`.dc_cd
      LEFT JOIN
      `tbl_da_summary` ON `tbl_da_summary`.`candidateid` = $token
      
      Where `tbl_candidate_registration`.candidateid =$token"; 
      $query = $this->db->query($sql);
      $res = $this->db->query($sql)->result();
    }

    //echo $sql; die;

    $row = $query->num_rows(); 
    if($row > 0){
      return $res;
    }else{
      return -1;
    }

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


public function getpayscale($categoryid){

  try{

    if(date('m') >= 04) {
      $d = date('Y-m-d', strtotime('+1 years'));
      $currentyear =  date('Y') .'-'.date('Y', strtotime($d)); 
    } else { 
      $d = date('Y-m-d', strtotime('-1 years'));
      $currentyear = date('Y', strtotime($d)).'-'.date('Y');
    }


   $sql = "SELECT `mstcategory`.categoryname,`msdesignation`.level,`msdesignation`.payscale,`tblsalary_increment_slide`.basicsalary FROM `mstcategory` 
    inner join msdesignation on `mstcategory`.`categoryname`= msdesignation.desname
    inner join tblsalary_increment_slide on msdesignation.level = tblsalary_increment_slide.level WHERE `mstcategory`.id='".$categoryid."' AND fyear = '".$currentyear."' AND noofyear = 0 GROUP BY LEVEL";

    $query = $this->db->query($sql);
    $result = $this->db->query($sql)->row();
    //$row = $query->num_rows();
    // echo $result;
    return $result;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}   


  public function getRoleEdDetails()
  {
    try{

      $sql = "SELECT staff.staffid,staff.name,staff.emailid FROM `staff` LEFT join  
        mstuser as u on u.staffid = staff.staffid 
        where staff.designation = 2 and staff.status = 1 and u.IsDeleted=0 GROUP BY staffid";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

   public function get_staffDetails($token)
    {
             try
   {

 $sql="SELECT 
  c.emailid, 
  c.`name`, 
  c.`emp_code`, 
  c.`candidateid`, 
  b.staffid,
  /*a.`staffid`,*/ 
  e.`officename`, 
  f.officename as oldofice, 
  /*a.transferno, */
  c.name as staff_name, 
  g.name as currentresponsibilityto, 
  /*a.`current_responsibility_to` as current_responsibility_to_id, */
  g.emp_code as empcode, 
  ut.desname as currentresponsibilitytodesignation, 
  k.desname, 
  c.reportingto, 
  e.`officeid` AS officeid, 
  sd.desname as staffdesignation 
FROM 
  `staff_transaction` AS b 
  LEFT JOIN `staff` AS c ON b.`staffid` = c.`staffid` 
  /*LEFT JOIN `tbl_iom_transfer` AS a ON a.`transid` = b.`id` */
  LEFT JOIN `staff` AS g ON b.staffid = g.`staffid` 
  /*LEFT JOIN `staff_transaction` AS pk ON a.`current_responsibility_to` = pk.`staffid` */
  LEFT JOIN `msdesignation` AS sd ON c.designation = sd.`desid` 
  LEFT JOIN `lpooffice` AS e ON b.`new_office_id` = e.`officeid` 
  LEFT JOIN `lpooffice` AS f ON b.`old_office_id` = f.`officeid` 
  LEFT JOIN `msdesignation` AS k ON b.`new_designation` = k.`desid` 
  LEFT JOIN `msdesignation` AS ut ON g.`designation` = ut.`desid` 
where 
  b.id= $token 
  GROUP BY emp_code"; 

/*echo $sql;
die();*/
      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }
     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

  public function getReportingto($teamid){
    try{
      $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emp_code,
   `staff`.`designation`,
   `msdesignation`.desname
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN msdesignation ON `msdesignation`.desid = staff.designation
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '".$teamid."' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
    ORDER BY `staff_transaction`.staffid  DESC";
    return  $this->db->query($sql)->row();
    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
}