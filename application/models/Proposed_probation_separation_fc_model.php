<?php 
/**
* Proposed_probation_separation  Model
*/
class Proposed_probation_separation_fc_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}



  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


// getpersonnel detail for email.

public function getpersonneldetail(){
  try{
  $sql = "SELECT * FROM mstuser where RoleID=17 AND staffid != 0";
  return $this->db->query($sql)->row();
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


 /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
       try
   {

      $sql="SELECT
      staff.name,
      staff.emailid,
      staff.emp_code,
  staff_transaction.staffid,
  staff_transaction.trans_status,
  staff_transaction.date_of_transfer,
  staff_transaction.new_office_id,
  staff_transaction.new_designation,
  staff.reportingto,
  lpooffice.officename,
  lp_old.officename as oldofficename,
  msdesignation.desname,
  staff_transaction.date_of_transfer

FROM
  staff_transaction
INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
left join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
left join lpooffice as lp_old on staff_transaction.old_office_id=lp_old.officeid
left join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";
  // echo $sql;die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

    public function stateList()
    {
        try{

             $sql = "SELECT id,statecode,name FROM `state` WHERE `isdeleted`='0' ORDER BY `name` ";
             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

	      /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function get_Probation_Separation()
  {
    
    try{

    $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  b.trans_status as da_status,
  b.`trans_flag` as status,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  a.`createdon` AS Requestdate,
  g.id AS tbl_da_exit_interview_form_id,
  h.id AS tbl_da_clearance_certificate_id,
  h.flag AS tbl_da_clearance_certificate_flag,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 4 THEN 'Accepted By Personnel Unit' 
    WHEN b.trans_flag = 5 THEN 'Accepted By TC/Intergrator' 
    ELSE 'Not status here'
  END
) AS flag
FROM 
  `staff_transaction` AS b 

LEFT JOIN `tbl_workflowdetail` AS a
   ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=27)
  
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `tbl_da_exit_interview_form` AS g ON g.`transid` = b.`id`
  LEFT JOIN
  `tbl_da_clearance_certificate` AS h ON h.`transid` = b.`id`
  Where 1=1 AND (b.`trans_status` ='Resign' || b.`trans_status` ='Facilitate To Leave') ";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
    //$sql .= " AND a.receiver =".$this->loginData->staffid;
  }

 // echo $sql;die();
   
$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function get_Recommended_to_graduate()
  {
    
    try{
     
      $sql = "SELECT a.*,b.trans_status as intimation, b.reason as comment, c.* 
              FROM `tbl_da_exit_interview_graduating_form` AS a
              INNER JOIN `staff_transaction` b on a.transid=b.id
              INNER JOIN `staff` c on b.staffid=c.staffid
              Where `a`.flag = 1  
              AND a.staffid in 
              (select a.staffid from staff a 
              inner join tbl_candidate_registration b on a.candidateid=b.candidateid
              inner join mstuser c on c.staffid=a.staffid
              where new_office_id = 
                ( SELECT new_office_id FROM `staff` where staffid = ".$this->loginData->staffid." ) 
              and c.RoleID=19)";
          
        $res = $this->db->query($sql)->result();
        // echo $this->db->last_query(); 
          
          return $res;
          
       }catch (Exception $e) {
         print_r($e->getMessage());die;
       }
  }



 public function get_Recommended_to_transfer()
  {
    
    try{
     
    $sql = "SELECT b.emp_code, a.id, intm.intimation, a.comment,b.name,a.probation_completed,a.probation_completed_date, a.intemation_type, mstcampus.campusname, (CASE WHEN tb.gender =1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female' END) as gender FROM `tbl_hr_intemation` as a 
      Left join `staff` as st on st.staffid = a.staffid
      Left JOIN mst_intimation as intm on intm.id = a.intemation_type
      left join `tbl_da_personal_info` as `b` ON `b`.staffid =`a`.staffid
      LEFT JOIN `tbl_candidate_registration` as tb ON `b`.candidateid = `tb`.candidateid
      LEFT JOIN `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`
      Where `a`.intemation_type = 4  and `a`.status = 1 AND `a`.Isdeleted = 0";
        
        $res = $this->db->query($sql)->result();
      // print_r($res); die();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





  public function get_Recommended_to_Extention()
  {
    
    try{
     
    $sql = "SELECT b.emp_code, a.id, intm.intimation, a.comment,b.name,a.probation_completed,a.probation_completed_date, a.intemation_type, mstcampus.campusname, (CASE WHEN tb.gender =1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female' END) as gender FROM `tbl_hr_intemation` as a 
      Left join `staff` as st on st.staffid = a.staffid
      Left JOIN mst_intimation as intm on intm.id = a.intemation_type
      left join `tbl_da_personal_info` as `b` ON `b`.staffid =`a`.staffid
      LEFT JOIN `tbl_candidate_registration` as tb ON `b`.candidateid = `tb`.candidateid
      LEFT JOIN `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`
      Where `a`.intemation_type = 6  and `a`.status = 1 AND `a`.Isdeleted = 0";
        
        $res = $this->db->query($sql)->result();
      // print_r($res); die();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


	 /**
   * Method getStaffDetails() get Staff details .
   * @access  public
   * @param Null
   * @return  Array
   */

   public function getstaffDetails($token=NULL){


    try{

      $sql = "SELECT .`sttr`.designation,`sttr`.reportingto,`sttr`.name,`sttr`.emailid,`lpo`.officename FROM `staff_transaction` as `sttf` 
      LEFT JOIN `staff` as `sttr` ON `sttf`.staffid  = `sttr`.staffid
      LEFT JOIN `lpooffice` as `lpo` ON sttf.`new_office_id` = `lpo`.officeid 
          Where `sttf`.staffid = $token ";
      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }


public function get_Prob_Separation($token)
  {
    
    try{
            $sql = "SELECT  * FROM `tbl_probation_separation`  as `tblps`
            Where `tblps`.id =$token ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
   


public function get_Development_Cluster()
  {
    try{
        $sql = "SELECT  `msdc_details`.dc_cd,`msdc_details`.dc_name FROM `msdc_details`  order By dc_name ASC  ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function get_Development_Apprentice()
  {
    
    try{
            $sql = "SELECT * FROM `tbl_candidate_registration`  Where `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 AND `tbl_candidate_registration`.`inprocess`='closed' AND `tbl_candidate_registration`.`categoryid`=1 ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
   

/**
   * Method getIntimation() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getIntimation()
  {
    
    try{

      $sql = "SELECT * FROM `mst_intimation` Where Isdeleted=0";

       $result = $this->db->query($sql)->result();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getstaffid() get staff email Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getstaffid($staffid)
  {
    
    try{

       $sql = "SELECT emailid FROM `staff` Where staffid=$staffid "; 

       $result = $this->db->query($sql)->row();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateteamfg() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandidateteamfg($id)
  {
    
    try{

      $sql = "SELECT teamid,fgid FROM `tbl_candidate_registration` 
              Where candidateid = '$id' AND joinstatus=1";

       $result = $this->db->query($sql)->result();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateSeparationDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getStaffSeparationDetails($id)
  {
    
    try{

       $sql = "SELECT `b`.name,`b`.emailid,`a`.generate_letter_name FROM `tbl_probation_separation` as a
      Inner Join `staff` as b on a.staffid = b.staffid  Where b.staffid = '$id' ";

       $result = $this->db->query($sql)->result()[0];

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getStaffTransferDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getStaffTransferDetails($id)
  {
    
    try{

      $sql = "SELECT b.name,b.emailid,a.generate_letter_name FROM `tbl_transfer_da` as a
      Inner Join `staff` as b on a.staffid = b.staffid  Where b.staffid = '$id' ";

       $result = $this->db->query($sql)->result()[0];

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





public function getStaffTransactioninc($tablename,$incval)
{
    try{

        // echo $tablename;
        // echo $incval;  die;
        //'staff_transaction',@a; 

        $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }


}