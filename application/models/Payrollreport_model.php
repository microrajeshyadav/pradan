<?php 

/**
* Master  Model
*/
class Payrollreport_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

		public function index()
	{

	}
  public function get_alloffice($p_office=null)
  {
     try
   {
     $this->db->select("officeid,officename");
     $this->db->from('lpooffice');
     $query=$this->db->get();
     return $query->result();
    }catch (Exception $e) 
    {
       print_r($e->getMessage());die;
    }

  }


/**
  * Method getActivesalaryheads() get head list.
  * @access  public
  * @param Null
  * @return  Array
  */


 public function getActivesalaryheads($period)
  {
     try
   {

     $sql = "select sh.fieldname,sh.fielddesc  from (SELECT `COLUMN_NAME`,`COLUMN_COMMENT` FROM `INFORMATION_SCHEMA`.`COLUMNS` 
        WHERE `TABLE_SCHEMA`='pradan' AND `TABLE_NAME`='tblmstemployeesalary' AND `COLUMN_NAME` !='deletedemployee' ) as a
        INNER join pradan.salaryheadshistory as sh on  sh.fieldname = a.`COLUMN_NAME` WHERE sh.activefield = 1 and sh.officeid is null AND sh.period ='".$period."' GROUP by  sh.fieldname,sh.fielddesc,sh.formulacolumn,sh.c,sh.dw,sh.roundtohigher,sh.monthlyinput,sh.mT,sh.ct ORDER BY cast(a.COLUMN_COMMENT AS UNSIGNED)"; 
   
     $result = $this->db->query($sql)->result();

     //print_r($result); die;
     return  $result;
    

     }catch (Exception $e){
       print_r($e->getMessage());die;
    }

  }



/**
  * Method getActivesalaryheads() get head list.
  * @access  public
  * @param Null
  * @return  Array
  */


 public function getstaffsalary($period, $salmonth, $salyear,$officeid)
  {
     try
   {

      $sql = "select sh.fieldname from (SELECT `COLUMN_NAME`,`COLUMN_COMMENT` FROM `INFORMATION_SCHEMA`.`COLUMNS` 
        WHERE `TABLE_SCHEMA`='pradan' AND `TABLE_NAME`='tblmstemployeesalary' AND `COLUMN_NAME` !='deletedemployee' ) as a
        INNER join pradan.salaryheadshistory as sh on  sh.fieldname = a.`COLUMN_NAME` WHERE sh.activefield = 1 and sh.officeid is null AND sh.period ='".$period."' GROUP by sh.fieldname,sh.fielddesc,sh.formulacolumn,sh.c,sh.dw,sh.roundtohigher,sh.monthlyinput,sh.mT,sh.ct ORDER BY cast(a.COLUMN_COMMENT AS UNSIGNED)"; 

     $result = $this->db->query($sql)->result();

if(!empty($result))
{
  $impdata= '';
$i=6;
     foreach ($result as $key => $value) {


      $impdata = $impdata.$value->fieldname.',';
      // echo $value->fieldname; die();
      $i++;
     }
     $impdata = substr($impdata,0,strlen($impdata)-1);
    
  $salarysql = "select lp.officename,tblfinalmonthlysalary.designationcode as level  ,tblfinalmonthlysalary.recordtype as exp ,". $impdata." from tblfinalmonthlysalary  
  inner JOIN `lpooffice` as lp on tblfinalmonthlysalary.officeid = lp.officeid 
  where salmonth= $salmonth and salyear= $salyear"; 

  if($officeid > 0)
    {
      $salarysql .=" AND lp.officeid= case when ".$officeid." = 0 then lp.officeid else ".$officeid." end";
    }
    
  $salarresult = $this->db->query($salarysql)->result();
  // echo "<pre>";
  // print_r($salarresult);
  $arrayres = array('countres'=> $i, 'result'=> $salarresult);
 
     return $arrayres;
}
// die();
return false;

     }catch (Exception $e){
       print_r($e->getMessage());die;
    }

  }






}