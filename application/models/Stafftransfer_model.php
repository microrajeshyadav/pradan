<?php 

/**
* State Model
*/
class Stafftransfer_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


   /**
   * Method getStaffDetails() get Staff details .
   * @access  public
   * @param Null
   * @return  Array
   */

	 public function getstaffDetails($token){


	 	try{

	 		$sql = "SELECT .`sttr`.designation,`sttr`.reportingto,`sttr`.name,`sttr`.emailid,`lpo`.officename FROM `staff_transaction` as `sttf` 
	 		LEFT JOIN `staff` as `sttr` ON `sttf`.staffid  = `sttr`.staffid
	 		LEFT JOIN `lpooffice` as `lpo` ON sttf.`new_office_id` = `lpo`.officeid 
      		Where `sttf`.staffid = $token ";
	 		$result = $this->db->query($sql)->result()[0];

	 		return $result;

	 	}catch (Exception $e) {
	 		print_r($e->getMessage());die;
	 	}
	 }


/**
   * Method getStaffDetails() get Staff details .
   * @access  public
   * @param Null
   * @return  Array
   */

	 public function getNewOfficeName($token){


	 	try{

	 		$sql = "SELECT `lpo`.officename FROM `staff_transaction` as `sttf` 
	 			    LEFT JOIN `lpooffice` as `lpo` ON sttf.`new_office_id` = `lpo`.officeid 
      		       Where `sttf`.staffid = $token AND 'trans_status' = 'Transfer'";
	 		$result = $this->db->query($sql)->result();

	 		return $result;

	 	}catch (Exception $e) {
	 		print_r($e->getMessage());die;
	 	}
	 }



/**
   * Method getStaffTransactioninc() get Staff transaction incremental id .
   * @access  public
   * @param Null
   * @return  Array
   */

   public function getStaffTransactioninc($tablename,$incval){


    try{

      //  echo $tablename;
      //  echo $incval;  

        ////'staff_transaction',@a; 

       $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->row();

     // print_r($result); die;

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }



	 public function get_staffname()
	 {

	 	$this->db->select("lpooffice.officeid,lpooffice.officename,staff_transaction.old_office_id,staff_transaction.new_office_id");
	 	$this->db->from("staff_transaction");
	 	$this->db->join('staff', 'staff_transaction.staffid = staff`.staffid ', 'inner');
	 	$this->db->join('lpooffice', 'lpooffice.officeid = staff_transaction.old_office_id', 'inner');

	 	$query=$this->db->get();
	 	return $query->result();
			  //echo $this->db->last_query();
	 }


	 public function getdata($token)
	 {
    try{
	 	$sql="SELECT a.staffid, 
    b.name,
     c.officename as oldofffice, 
     d.officename as presentoffice, 
     a.date_of_transfer,
    s.Acclevel_Name as sender,
     a.effective_date FROM staff_transaction AS a 

     INNER JOIN staff AS b ON a.staffid = b.staffid 
    inner join tbl_workflowdetail w on a.staffid=w.staffid 
    
    INNER join mstuser m on w.sender=m.staffid
    inner join sysaccesslevel s on m.RoleID=s.Acclevel_Cd
    INNER JOIN lpooffice as c on c.officeid=a.old_office_id INNER JOIN lpooffice as d on d.officeid=a.new_office_id WHERE  a.trans_status='Transfer' and a.staffid=$token GROUP by b.staffid";
		//echo $sql;die();
	 	$result = $this->db->query($sql)->result();
    // print_r($result);
    // die;

	 	return $result;
    }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
	 }



    /**
   * Method get_staffDetails() display the details of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
       try
   {

      $sql="SELECT
  staff_transaction.staffid,
  staff_transaction.date_of_transfer,
  staff_transaction.new_office_id,
  staff_transaction.new_designation,
  lpooffice.officename,
  msdesignation.desname
FROM
  staff_transaction
INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
inner join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
inner join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }




  /**
   * Method get_alloffice() get all office location
   * @access  public
   * @param Null
   * @return  array
   */
	public function get_alloffice($p_office)
	{
     try
   {
    //echo "office id=".$p_office;
   // die;
		$this->db->select("officeid,officename");
    

      $this->db->from('lpooffice');
      $this->db->where_not_in('officeid',$p_office);
     
      $query=$this->db->get();
     // echo $this->db->last_query();
      //die();
      return $query->result();
    }
     

     catch (Exception $e) 
     {
       print_r($e->getMessage());die;
     }

	}



/**
   * Method get_Integrator_ED_OnlyteamTC() get all integrator ED And only team TC 
   * @access  public
   * @param Null
   * @return  array
   */
  public function get_Integrator_ED_OnlyteamTC($p_office)
  {
     try
   {
  
   $sql = 'SELECT staffid, CONCAT(name, " (", b.desname,")") as name From (

SELECT Staffid, name, designation, 1 as sortorder FROM Staff where new_office_id =". $p_office ."  AND designation = 4 AND status = 1
UNION  
SELECT Staffid, name, designation, 10 as sortorder FROM staff WHERE designation = 16 OR designation = 2  AND status = 1 ) as a
INNER JOIN msdesignation as b ON b.desid = a.designation ORDER by sortorder ASC'; die;
     
     return $this->db->query($sql)->result();
    
    }catch (Exception $e) 
     {
       print_r($e->getMessage());die;
     }

  }

 /**
   * Method get_alloffice() get all designation 
   * @access  public
   * @param Null
   * @return  array
   */


	public function get_alldesignation($p_designation)
	{
     try
   {
		$this->db->select("desid,desname");
      $this->db->from('msdesignation');
      $this->db->where_not_in('desid',$p_designation);

     
      $query=$this->db->get();
     // echo $this->db->last_query();
      // die();
      return $query->result();
       }
     

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
}




 public function getstaffname($token)
  {
    try{
   $sql = "SELECT
   staff.emp_code,
   staff.name,
   msdesignation.desname,
   staff.reportingto,
   staff_transaction.staffid,
   staff_transaction.date_of_transfer,
   staff_transaction.new_office_id,
   staff_transaction.new_designation
   FROM
   staff_transaction
   INNER JOIN(
   SELECT
   staff_transaction.staffid,
   MAX(
   staff_transaction.date_of_transfer
   ) AS MaxDate
   FROM
   staff_transaction
   GROUP BY
   staffid
   ) AS TMax
   ON
   `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
   'Resign',
   'Termination',
   'Retirement',
   'Death'
   )

   INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`
   INNER JOIN `msdesignation` on `msdesignation`.desid = `staff`.`designation`
   where 1=1 and `staff`.staffid NOT IN (1) AND staff_transaction.`staffid` 
   =".$token; 
   $sql .= " GROUP by `staff_transaction`.staffid ORDER BY  `staff`.name ASC  "; 
   // echo $sql;exit();
   return  $this->db->query($sql)->row();
   }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
 }
	

	}