<?php 

/**
* General Nnomination And Authorisation Form Model Class
*/
class General_nomination_and_authorisation_form_staff_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

  public function staffName($staff_id,$login_staff)
  {
   
    try{
     
     
           $sql = "SELECT * FROM staff where staffid !=$staff_id and staff.staffid!=$login_staff AND new_office_id = ".$this->loginData->teamid." order by staff.name asc";  
           

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}

  public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getCandidateWithAddress($staff_id)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



public function getCandidateWith($reportingto)
  {
    
    try{

        $sql = "SELECT
    staff.name as reprotingname
    
FROM
    staff WHERE   staff.staffid='$reportingto'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method get max get_providentworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_generalworkflowid($staff_id)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=15 and staff.staffid=$staff_id ORDER BY workflowid DESC LIMIT 1";
    
//echo $sql;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
if ($date=='//') {
  $date = NULL;
 }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);

 if ($date=='//') {
  $date = NULL;
 }

  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

 public function get_witness_detail($staff_id)
  {
    
    try{

        $sql = 'SELECT   a.address1,a.address2,b.name as name1 , c.name as name2 from tbl_graduitynomination a
                  inner join `staff` b on b.staffid = a.witness1
                  inner join `staff` c on c.staffid = a.witness2
                Where `a`.staff_id ='.$staff_id.''; 
               //  echo $sql;
               // die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



	/**
   * Method getSelectedCandidate() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSelectedCandidate()
  {
    
    try{

         $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";
        
        $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateWithAddressDetails() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */
public function do_uploadd($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
   try{
   
  $config = array(
       'allowed_types'    =>    'jpg|jpeg|png|gif',
       'upload_path'      =>    FCPATH . "datafiles/nominee/",
       'max_size'         =>    10000,
       'encrypt_name'     =>    TRUE,
       'file_ext_tolower' =>    TRUE
   );


 $this->load->library('upload',$config);
 if(!$this->upload->do_upload('signatureplace'))
 {
   echo $this->upload->display_errors();

 }

 else   //if upload is successfully
 {
   $fileData=$this->upload->data();

   //die();
return $fileData;
}
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}
  public function getCandidateWithAddressDetails($token)
  {
    
    try{

         $sql = 'SELECT staff.emp_code,staff.name as staff_name,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname, `tbl_candidate_communication_address`.permanentstreet, `tbl_candidate_communication_address`.permanentcity, `tbl_candidate_communication_address`.permanentstateid, `tbl_candidate_communication_address`.permanentdistrict, `tbl_candidate_communication_address`.permanentpincode, `state`.name,lpooffice.officename,staff.new_office_id FROM `tbl_candidate_registration` 
         left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
          left join `state` ON `state`.id = `tbl_candidate_communication_address`.permanentstateid
          left join staff on tbl_candidate_registration.candidateid=staff.candidateid left join lpooffice on staff.new_office_id=lpooffice.officeid
                Where `tbl_candidate_registration`.candidateid ='.$token.''; 
               // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneralnominationform() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralnominationform($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_general_nomination`         
                Where `tbl_general_nomination`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWitnessDeclaration()
  {

    try{
            $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name 
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  ) 
  AND staff_transaction.`new_designation` IN(4) 
ORDER BY 
  `staff`.name ";
            
            $result = $this->db->query($sql)->result();

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getViewGeneralnominationform() get View General Nomination  Authorisation Form Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewGeneralnominationform($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_general_nomination`         
                Where `tbl_general_nomination`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Count Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountNominee($token)
  {
    
    try{
        
        $this->db->where('nomination_id', $token);
        $result = $this->db->count_all_results('tbl_nominee_detail_joininginduction');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Select Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT  tbl_nominee_detail_joininginduction.id as nom_id,tbl_nominee_detail_joininginduction.share_nomine,tbl_nominee_detail_joininginduction.sr_no,tbl_nominee_detail_joininginduction.minior,tbl_nominee_detail_joininginduction.nomination_id,tbl_nominee_detail_joininginduction.nominee_name,tbl_nominee_detail_joininginduction.nominee_relation,tbl_nominee_detail_joininginduction.nominee_age,tbl_nominee_detail_joininginduction.nominee_address,sysrelation.relationname,sysrelation.status,sysrelation.id FROM `tbl_nominee_detail_joininginduction`  
                 left join sysrelation on   `tbl_nominee_detail_joininginduction`.nominee_relation =  `sysrelation`.id  
                 left join   tbl_general_nomination on  tbl_nominee_detail_joininginduction.nomination_id=tbl_general_nomination.id
                Where `tbl_general_nomination`.candidateid ='.$token.''; 
                //echo $sql;
                 

        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



 /**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $this->db->select('*');

      $this->db->from('sysrelation');
      


      $this->db->where('isdeleted', 0);
      //$this->db->get();
     // echo $this->db->last_query();
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }




/**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
     $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->result()[0];
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
       $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->result()[0];
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }

  public function office_name()
  {
     try{

      $sql = "select * from tbl_table_office";
       $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->row();
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

  public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

}