<?php 

/**
* Handed Over Taken Over  Model
*/
class Handed_over_taken_over_seperation_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	

	public function index()
	{

	}

/**
   * Method getRole() list of roles .
   * @access  public
   * @param Null
   * @return  Array
   */
 	
public function getRole()
  {
    
    try{

       $sql = "SELECT * FROM `sysaccesslevel` Order by Acclevel_Name ASC  ";

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  /*
     * Method getTransid() list of details .
   * @access  public
   * @param Null
   * @return  row
   */
  
public function getTransid($staff_id)
  {
    
    try{

       $sql = "SELECT
           staff.name,
           staff_transaction.staffid,
           staff_transaction.id
          
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`
       
       where staff_transaction.staffid='$staff_id'

        ORDER BY  `staff`.staffid ASC";

       // echo $sql;die();

       $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

   public function get_staffDetails($token)
    {
             try
   {

 $sql="SELECT 
  c.emailid, 
  c.`name`, 
  c.`emp_code`, 
  b.staffid,
  /*a.`staffid`,*/ 
  e.`officename`, 
  f.officename as oldofice, 
  /*a.transferno, */
  c.name as staff_name, 
  g.name as currentresponsibilityto, 
  /*a.`current_responsibility_to` as current_responsibility_to_id, */
  g.emp_code as empcode, 
  ut.desname as currentresponsibilitytodesignation, 
  k.desname, 
  c.reportingto, 
  e.`officeid` AS officeid, 
  sd.desname as staffdesignation 
FROM 
  `staff_transaction` AS b 
  LEFT JOIN `staff` AS c ON b.`staffid` = c.`staffid` 
  /*LEFT JOIN `tbl_iom_transfer` AS a ON a.`transid` = b.`id` */
  LEFT JOIN `staff` AS g ON b.staffid = g.`staffid` 
  /*LEFT JOIN `staff_transaction` AS pk ON a.`current_responsibility_to` = pk.`staffid` */
  LEFT JOIN `msdesignation` AS sd ON c.designation = sd.`desid` 
  LEFT JOIN `lpooffice` AS e ON b.`new_office_id` = e.`officeid` 
  LEFT JOIN `lpooffice` AS f ON b.`old_office_id` = f.`officeid` 
  LEFT JOIN `msdesignation` AS k ON b.`new_designation` = k.`desid` 
  LEFT JOIN `msdesignation` AS ut ON g.`designation` = ut.`desid` 
where 
  b.id= $token 
  GROUP BY emp_code"; 

/*echo $sql;
die();*/
      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }
     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }
    
/**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

   public function get_takenstaffDetails($token)
    {
             try
   {

 $sql="SELECT 
  c.emailid, 
  c.`name`, 
  c.`emp_code`, 
  b.staffid,
  /*a.`staffid`,*/ 
  e.`officename`, 
  f.officename as oldofice, 
  /*a.transferno, */
  c.name as staff_name, 
  g.name as currentresponsibilityto, 
  /*a.`current_responsibility_to` as current_responsibility_to_id, */
  g.emp_code as empcode, 
  ut.desname as currentresponsibilitytodesignation, 
  k.desname, 
  c.reportingto, 
  e.`officeid` AS officeid, 
  sd.desname as staffdesignation 
FROM 
  `staff_transaction` AS b 
  LEFT JOIN `staff` AS c ON b.`staffid` = c.`staffid` 
  /*LEFT JOIN `tbl_iom_transfer` AS a ON a.`transid` = b.`id` */
  LEFT JOIN `staff` AS g ON b.staffid = g.`staffid` 
  /*LEFT JOIN `staff_transaction` AS pk ON a.`current_responsibility_to` = pk.`staffid` */
  LEFT JOIN `msdesignation` AS sd ON c.designation = sd.`desid` 
  LEFT JOIN `lpooffice` AS e ON b.`new_office_id` = e.`officeid` 
  LEFT JOIN `lpooffice` AS f ON b.`old_office_id` = f.`officeid` 
  LEFT JOIN `msdesignation` AS k ON b.`new_designation` = k.`desid` 
  LEFT JOIN `msdesignation` AS ut ON g.`designation` = ut.`desid` 
where 
  c.staffid= $token 
  GROUP BY emp_code"; 

/*echo $sql;
die();*/
      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }
     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }
    


  /**
   * Method getStaffList() list of Staff with staffid .
   * @access  public
   * @param Null
   * @return  Array
   */
  






public function getStaffList()
  {
    
    try{

       $sql = "SELECT
           staff.name,
           staff_transaction.staffid,
           staff_transaction.date_of_transfer,
           staff_transaction.new_office_id,
           staff_transaction.new_designation
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

        ORDER BY  `staff`.staffid ASC ";

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  /**
   * Method getStaffList() list of Staff with staffid on given Office Id .
   * @access  public
   * @param Null
   * @return  Array
   */
  






public function getStaffListByOfficeId($officeid)
  {
    
    try{

       $sql = "SELECT
           staff.name,
           staff_transaction.staffid,
           staff_transaction.date_of_transfer,
           staff_transaction.new_office_id,
           staff_transaction.new_designation
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Termination during Probation',
       'Discharge simpliciter/ Dismissal',
       'Desertion cases',
       'Retirement',
       'Death',
       'Super Annuation'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid` where staff.new_office_id = ".$officeid."

       ORDER BY  `staff`.staffid ASC ";

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method staffName() get Staff Name.
   * @access  public
   * @param Null
   * @return  Array
   */


public function staffName($staff_id)
  {
   
    try{
    
           $sql = "SELECT
           staffid,name FROM staff where staffid !='$staff_id' ";  
            // echo $sql;

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}

/**
   * Method supervisiorName() get supervisior Name.
   * @access  public
   * @param Null
   * @return  Array
   */


public function supervisiorName($staff_id)
  {
   
    try{
    
           $sql = "SELECT
           staffid,name FROM staff where staffid ='$staff_id' ";  
            //echo $sql;

       $res = $this->db->query($sql)->row();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}



// *
//    * Method count_expeness() get Staff expense.
//    * @access  public
//    * @param Null
//    * @return  row
//    */


 public function count_handedchrges($token)
   {
    //echo "staff=".$staff_id;
    //die;
   
    try{
    
            $sql = "SELECT
            count(*)as Cexp FROM tbl_hand_over_taken_over_charge where transid ='$token'";
            //echo $sql;
            //die();  

       $res = $this->db->query($sql)->row();
      return $res;
    }catch (Exception $e) {
     print_r($e->getMessage());die;
 }

 }

// *
//    * Method count_expeness() get Staff expense.
//    * @access  public
//    * @param Null
//    * @return  row
//    */


 public function expeness_details($token)
   {
    //echo "staff=".$staff_id;
    //die;
   
    try{
    
            $sql = "SELECT
            * FROM tbl_handed_taken_over_charge_transac where handedtaken_id ='$token'";
           // echo $sql;
            //die();  

       $res = $this->db->query($sql)->result();
      return $res;
    }catch (Exception $e) {
     print_r($e->getMessage());die;
 }

 }
 // *
//    * Method handed over charge() of  Staff expense.
//    * @access  public
//    * @param Null
//    * @return  array
//    */


 public function handed_over_charge($token)
   {
    //echo "staff=".$staff_id;
    //die;
   
    try{
    
            $sql = "SELECT
            * FROM tbl_hand_over_taken_over_charge where transid ='$token'";
           // echo $sql;
           //  die();  

       $res = $this->db->query($sql)->row();
      return $res;
    }catch (Exception $e) {
     print_r($e->getMessage());die;
 }

 }


 // *
//    * Method handed over charge() of  Staff expense.
//    * @access  public
//    * @param Null
//    * @return  array
//    */


 public function handed_over_charge_date($token)
   {
    //echo "staff=".$staff_id;
    //die;
   
    try{
    
            $sql = "SELECT
            * FROM tbl_hand_over_taken_over_charge 

            where staffid ='$token' AND type=1";
           /*echo $sql;
            die(); */ 

       $res = $this->db->query($sql)->row();
      return $res;
    }catch (Exception $e) {
     print_r($e->getMessage());die;
 }

 }
 // *
//    * Method taken_over_charge_date() of  Staff expense.
//    * @access  public
//    * @param Null
//    * @return  array
//    */


 public function taken_over_charge_date($token)
   {
    //echo "staff=".$staff_id;
    //die;
   
    try{
    
            $sql = "SELECT
            * FROM tbl_hand_over_taken_over_charge where staffid ='$token' AND type=2";
           /*echo $sql;
            die(); */ 

       $res = $this->db->query($sql)->row();
      return $res;
    }catch (Exception $e) {
     print_r($e->getMessage());die;
 }

 }



// */

 /**
   * Method staffDetailList() get Staff Detail.
   * @access  public
   * @param Null
   * @return  Array
   */


public function staffDetailList($staffid)
  {
    try{
    
           $sql = "SELECT
           staff.name,
           staff.contact,
           staff.emailid,
           staff.staffid
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

       where 1=1 and `staff`.staffid = $staffid  

       ORDER BY  `staff`.name ASC ";  

       $res = $this->db->query($sql)->result()[0];
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}



/**
   * Method getSingleRole() get role name .
   * @access  public
   * @param Null
   * @return  Array
   */
  
public function getSingleRole($roleid)
  {
    
    try{

       $sql = "SELECT * FROM `sysaccesslevel` WHERE Acclevel_Cd =".$roleid;

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}