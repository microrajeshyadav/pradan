<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Daship components Model Class
*/
class Daship_components_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	/**
   * Method getPhase() get Phase List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getPhase()
  {

    try{

     $sql = "SELECT * FROM `mstphase` order by id ASC";

     $res = $this->db->query($sql)->result()[0];

     return $res;
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



/**
   * Method getReportingTo() get reporting office name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getReportingTo($id)
  {
    
    try{

  $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name,`staff`.emailid
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  ) 
  AND staff_transaction.`new_office_id` = '$id' 
  AND staff_transaction.`new_designation` IN(4,16) 
ORDER BY 
  `staff_transaction`.staffid ";   
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }  




  /**
   * Method getJoiningReportview() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportview($token)
  {

    try{

     $sql = "SELECT * FROM `tbl_joining_report` Where candidateid = $token";

     $res = $this->db->query($sql)->result()[0];

     return $res;
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


/**
   * Method getJoiningReportview() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getPhaseWithtransaction($candidateid)
  {

    try{

      $sql = "SELECT `a`.phaseid,`b`.fromdate,`b`.todate,`mstp`.phase_name FROM `tbl_da_event_detailing` as a INNER JOIN `tbl_da_event_detailing_transaction` as b on `a`.id = `b`.da_event_detailing
    INNER JOIN mstphase as mstp on `a`.phaseid = `mstp`.id 
    WHERE `b`.candidateid = $candidateid "; 
    
    $res = $this->db->query($sql)->result();

     return $res;
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method getDashipComponentDocument() get Document List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getDashipComponentDocument($id)
{

  try{


   $sql = "SELECT
   `tbl_daship_component`.original_document_name, `tbl_daship_component`.encrypted_document_name, `tbl_daship_component`.status, `tbl_daship_component`.id, `mstphase`.phase_name
   FROM
   `tbl_daship_component`
   INNER JOIN `mstphase` ON `tbl_daship_component`.phaseid = `mstphase`.id
   WHERE `tbl_daship_component`.candidateid=".$this->loginData->candidateid."
   ORDER BY
   `tbl_daship_component`.id
   DESC"; 

   $res = $this->db->query($sql)->result();

   return $res;
 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getSindleDashipComponentDocument() get Document List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSindleDashipComponentDocument($token)
{

  try{


    $sql = "SELECT  `tbl_daship_component`.phaseid,
   `tbl_daship_component`.original_document_name, `tbl_daship_component`.encrypted_document_name, `tbl_daship_component`.status, `tbl_daship_component`.id, `mstphase`.phase_name
   FROM
   `tbl_daship_component` INNER JOIN `mstphase` ON `tbl_daship_component`.phaseid = `mstphase`.id
   Where `tbl_daship_component`.id=$token  ";  

   $res = $this->db->query($sql)->result()[0];

   return $res;
 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}





/**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
     $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->result()[0];
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
       $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->result()[0];
      }
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



}