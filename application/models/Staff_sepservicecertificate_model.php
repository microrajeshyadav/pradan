<?php 

/**
* Master  Model
*/
class Staff_sepservicecertificate_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}    

	/**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_seperation_certificate($staffid)
    {
       try
   {

   	// echo $token; die;
       $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation,g.desname as leavingdesignation,k.reason as leavingreason, h.officename FROM staff_transaction as a 
          INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid 
          INNER JOIN `staff` as c on a.staffid = c.staffid
          left JOIN `msdesignation` as b on a.new_designation = b.desid
          left JOIN `msdesignation` as g on k.new_designation = g.desid
          INNER JOIN `lpooffice` as h on c.new_office_id = h.officeid
          WHERE  (k.`trans_status` ='Termination' OR k.`trans_status` ='Resign' OR k.`trans_status` ='Termination during Probation' OR k.`trans_status` ='Death' OR k.`trans_status` ='Super Annuation' OR k.`trans_status` ='Retirement' OR k.`trans_status` ='Discharge simpliciter/ Dismiss' OR k.`trans_status` ='Desertion cases' OR k.`trans_status` ='Premature retirement') and  a.trans_status = 'JOIN' and a.staffid =".$staffid;

     //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
   }



     /**
   * Method get_seperation_staffid() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_seperation_staffid($token)
    {
       try
   {

   	// echo $token; die;
      $sql="SELECT a.staffid FROM staff_transaction as a WHERE a.id = $token";

     //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
   }


}