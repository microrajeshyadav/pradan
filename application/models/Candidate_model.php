<?php 

/**
* Dashboard Model
*/
class Candidate_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Countclinicaldetails()
	{
		try{
			$this->db->select('count(Name) as clinical');
			$this->db->from('tblpatientregistrationdetails as prd ');
			$this->db->join('tblpatientclinicaltestdetails as pc', 'prd.PatientGUID = pc.PatientGUID');
			$this->db->where('prd.IsDeleted', 0);
			return $this->db->count_all_results();

		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
		
	}

	public function index()
	{

	}

	public function campusList()
	{
		try{
		$sql = "SELECT * FROM `mstcampus`";
		$res = $this->db->query($sql)->result();

		return $res;
		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}


}