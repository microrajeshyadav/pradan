<?php 

/**
* TC_DAship_Components_model Model
*/
class Tc_daship_components_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


/**
	 * Method getBatch() get all Batch .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getBatch()
{
	
	try{
		
		$sql = "SELECT * FROM `mstbatch` Where status=0 AND isdeleted=0";

		$res = $this->db->query($sql)->result();

		return $res;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	
}



	/**
	 * Method getDAComponent() get all DA Component .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDAComponent($batchid =null,$phaseid =null, $teamid =null)
	{
		
		try{
			
			
	 $sql = "SELECT DISTINCT
    `tbldc`.candidateid,
    `tblcr`.candidatefirstname,
    `tblcr`.candidatemiddlename,
    `tblcr`.candidatelastname,
    `tblcr`.emailid,
    `tbldsed`.phaseid,
    `tbldsed`.batchid,
    `tbldc`.original_document_name,
    `tbldc`.encrypted_document_name,
    `tbldaedt`.fromdate,
    `tbldaedt`.todate,
    `mstph`.phase_name,
    `tbldc`.id,
    `tbldc`.status,
    `st`.name as fieldguide,
    `lpo`.officename,
    `ms`.campusname,
    (case when tblcr.gender=1 then 'Male' when tblcr.gender=2 then 'Female' end) as gender
FROM
    `tbl_da_event_detailing` AS `tbldsed`,
    `tbl_da_event_detailing_transaction` AS `tbldaedt`,
    `tbl_daship_component` AS `tbldc`,
    `tbl_candidate_registration` AS `tblcr`,
    `staff` as st,
    `lpooffice` as `lpo`,
    `mstcampus` as ms,
    `mstphase` AS `mstph`
WHERE
    `tbldsed`.`id` = `tbldaedt`.da_event_detailing AND `tbldaedt`.`candidateid` = `tbldc`.`candidateid` AND `tbldsed`.phaseid = `tbldc`.phaseid AND `tblcr`.candidateid = `tbldc`.candidateid AND `tbldc`.phaseid =`mstph`.id
     and `ms`.`campusid` = `tblcr`.`campusid` AND `tblcr`.fgid = st.staffid AND `lpo`.officeid = `tblcr`.`teamid`";

    if ($batchid !='') {
     	$sql .= " AND `tbldsed`.batchid=".$batchid."";
     }else{
     	$sql .= " AND `tbldsed`.batchid=0";
     } 

     if ($teamid !='') {
     	$sql .= " AND `tblcr`.teamid =".$teamid."";
     }

      if ($phaseid !='') {
     	$sql .= " AND `tbldsed`.phaseid=".$phaseid."";
     }
     else{
     	$sql .= " AND `tbldsed`.phaseid=0";
     } 
    // $sql .= " Group BY rt.emp_code ";
     
    // echo $sql; die;

	$res = $this->db->query($sql)->result();

			return $res;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
   if ($date=='//') {
      $date = NULL;
   }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


}