<?php 

/**
* Master  Model
*/
class Staff_appointmentletterforregularlevelfour_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}  
	
/**
   * Method alldesignation() get designation List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function alldesignation()
{
  //echo "token".$token;
  try{

    


      $this->db->select('desid,desname');
   
      $this->db->from("msdesignation");
   
    
   

  $query=$this->db->get($sql);
              return $query->result();
        

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
} 
/**
   * Method alloffice() get office List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function alloffice()
{
  //echo "token".$token;
  try{

    


      $this->db->select('officeid,officename');
   
      $this->db->from("lpooffice");
   
    
   

  $query=$this->db->get($sql);
              return $query->result();
        

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}   
    /**
   * Method details_appointmentletterforregular() get details of appointmentletterforregular.
   * @access  public
   * @param Null
   * @return  Array
   */

public function details_appointmentletterforregular($token)
{
  
  try{

    


      $this->db->select('*');

   
      $this->db->from("appointmentletterforregular a");
          $this->db->join('lpooffice l', 'l.officeid=a.location', 'left');
    $this->db->join('msdesignation d', 'd.desid=a.designation', 'left');

      
      $this->db->where('appointment_id',$token);
      
   
    
   

  $query=$this->db->get();
  
              return $query->row();
        

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}     
}