<?php 

/**
* Dashboard Model
*/
class Score_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}


	public function getRecruiters()
	{
		try{
           
            $sql = "SELECT staffid,name FROM `staff`";  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}


	public function getCampus()
	{
		try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}


    public function getRecruitersCampus()
    {
        try{

            $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, cam.`campusincharge`,cam.`campusname`,cam.`city`,st1.`name` as name1, st2.`name` as name2, st3.`name` as name3 from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.IsDeleted = 0"; 

            
            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }




    public function getSingelRecruitersCampus($token)
    {
        try{

            $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, rec.`recruitersname1`, rec.`recruitersname2`,rec.`recruitersname3`, cam.`campusincharge`,cam.`campusid`,cam.`city` from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.recruiterid='".$token."' and  rec.IsDeleted = 0"; 

            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }













}