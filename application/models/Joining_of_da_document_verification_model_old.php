<?php 

/**
* Joining_of_DA Model
*/
class Joining_of_da_document_verification_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

/**
   * Method getCandidate_BDFFormSubmit() get BDF Form Submit Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidate_BDFFormSubmit()
  {
    
    try{
           $sql = "SELECT `tblvd`.comment,`tblvd`.status,`tblvd`.approvedby,`tblvd`.approvedon, `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch, `tbldapi`.emp_code FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid
            left join `tbl_verification_document` as `tblvd` ON `tblvd`.candidateid =`tbl_candidate_registration`.candidateid

            left join `tbl_da_personal_info` as `tbldapi` ON `tbldapi`.candidateid =`tbl_candidate_registration`.candidateid

            Where `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandateVerified() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandateVerified($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch,`tbl_candidate_registration`.encryptmetriccertificate,`tbl_candidate_registration`.encrypthsccertificate,`tbl_candidate_registration`.encryptugcertificate,`tbl_candidate_registration`.encryptpgcertificate,`tbl_candidate_registration`.encryptothercertificate FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail($token)
  {
    
    try{

       $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=1 And `tblgnau`.status =1";  //die;
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getNomieeDetail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomieeDetail($id)
  {
    
    try{

       $sql = "SELECT `tblnd`.general_nomination_id,`tblnd`.nominee_name,`sysr`.relationname,`tblnd`.nominee_age,`tblnd`.nominee_address  FROM `tbl_nominee_details` as `tblnd`  
       left join `sysrelation` as `sysr` ON `sysr`.id = `tblnd`.nominee_relation Where `tblnd`.general_nomination_id = $id";  // die;
        $result = $this->db->query($sql)->result();

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateDetails($token)
  {
    
    try{
            $sql = 'SELECT `tbl_candidate_communication_address` .permanentstreet,`tbl_candidate_communication_address` .permanentcity,`tbl_candidate_communication_address` .permanentstateid,`tbl_candidate_communication_address` .permanentdistrict,`tbl_candidate_communication_address` .permanentpincode, tbl_generate_offer_letter_details.*,`state`.name FROM `tbl_candidate_communication_address` 
            left join `tbl_generate_offer_letter_details` ON `tbl_generate_offer_letter_details`.candidateid = `tbl_candidate_communication_address`.candidateid
            left join `state` ON `state`.id=`tbl_candidate_communication_address`.permanentstateid
                Where `tbl_candidate_communication_address`.candidateid ='.$token.'';  //die;
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  
/**
   * Method getJoiningReportDetail() get Joining Report Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportDetail($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_joining_report` Where candidateid = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandateWorkExperincecount($token)
  {
    
    try{

          $this->db->where('candidateid', $token);
         $res = $this->db->count_all_results('tbl_work_experience');

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceCandateVerified($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceVerifiedValue($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience_verified` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getVerifiedDetailes() get Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getVerifiedDetailes($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getverifiedstatus() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getverifiedstatus($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneralNominationFormPDF() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationFormPDF($token)
  {
    
    try{

       $sql = "SELECT status, filename FROM `tbl_general_nomination_and_authorisation_form` Where  status= 2  AND `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateWithAddressDetails() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateWithAddressDetails($token)
  {
    
    try{

          $sql = 'SELECT `tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname, `tbl_candidate_communication_address`.permanentstreet, `tbl_candidate_communication_address`.permanentcity, `tbl_candidate_communication_address`.permanentstateid, `tbl_candidate_communication_address`.permanentdistrict, `tbl_candidate_communication_address`.permanentpincode, `state`.name FROM `tbl_candidate_registration` 
         left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
          left join `state` ON `state`.id = `tbl_candidate_communication_address`.permanentstateid
                Where `tbl_candidate_registration`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getViewGeneralnominationform() get View General Nomination  Authorisation Form Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewGeneralnominationform($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_general_nomination_and_authorisation_form`         
                Where `tbl_general_nomination_and_authorisation_form`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getCountOtherDocument() get Count Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountOtherDocument($token)
  {
    
    try{
        
        $this->db->where('candidateid', $token);
        $result = $this->db->count_all_results('tbl_other_documents');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getOtherDocumentDetails() get Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocumentDetails($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_other_documents`         
                Where candidateid ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getNomineedetail() get Count Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountNominee($token)
  {
    
    try{
        
        $this->db->where('general_nomination_id', $token);
        $result = $this->db->count_all_results('tbl_nominee_details');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Select Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_nominee_details`         
                Where `tbl_nominee_details`.general_nomination_id ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $this->db->select('*');
      $this->db->from('sysrelation');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getOtherDocuments() get Document Name With Type List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocuments()
  {

    try{
      $this->db->select('*');
      $this->db->from('mstdocuments');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }



/**
   * Method getJoiningReportStatus() get Joining Report Status List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportStatus()
  {

    try{
      $this->db->select('status, filename');
      $this->db->from('tbl_joining_report');
      $this->db->where('status', 1);
      return $this->db->get()->result()[0];

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWitnessDeclaration()
  {

    try{
            $sql = "SELECT `staffid`,`name` FROM `staff` ORDER BY `name` ";
            
            $result = $this->db->query($sql)->result();

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


}