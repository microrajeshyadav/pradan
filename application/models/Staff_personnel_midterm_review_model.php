<?php 

/**
* State Model
*/
class Staff_personnel_midterm_review_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    public function get_candidaateid($staff_id)
    {
          try{
        $sql="select * from staff where staffid='$staff_id'";
        
        return  $this->db->query($sql)->row();
        }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
    }
     public function get_ed()
    {
      // echo "hello";
      // die;
        try{
        $sql="select * from mstuser where RoleID=18";
        // echo $sql;
        
        return  $this->db->query($sql)->result();
        }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
    }


	public function getmidtermreviewworkflowList()
	{
    try{

    $currentdate = date('Y-m-d');

    $sql="SELECT
  c.`name`,
  c.`emp_code`,
  msdesignation.desname,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  c.`probation_date` ,
  a.`scomments`,
  a.sender as send,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  msdesignation.level_name AS levelname,
  c.gender,
   (
     CASE WHEN c.gender = 1 THEN 'Male' WHEN c.gender = 2 THEN 'Female' WHEN c.gender = 3 THEN 'Other' ELSE 'Not gender here'
   END
 ) AS gender,

  b.`trans_flag` as status,
  (
     CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate' 
      WHEN b.`trans_flag`     = 3 THEN 'Satisfactory By TC/Intergrator'
     
      WHEN b.`trans_flag`     = 4 THEN 'Not Satisfactory By TC/Intergrator' 
     
      
      ELSE 'Not status here'
  END
) AS flag

FROM
  `tbl_workflowdetail` AS a 
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`    
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  INNER JOIN
  tbl_probation_review_performance AS probre ON c.`staffid` = probre.`staffid`
  INNER JOIN
   msdesignation ON c.`designation` = msdesignation.`desid`

  Where b.`trans_status` = 'Midtermreview'  GROUP BY c.staffid,a.`r_id` asc";
  //echo $sql;
// 		$sql = "SELECT staff.name,
//   staff.staffid,
//   staff.probation_date,
//   staff.doj,
//   staff.gender,
//   staff.emp_code,
//   staff.candidateid,
//   staff.designation,
//   lpooffice.officename,
//   msdesignation.desname,
//   w.`r_id` as rid,
//   msdesignation.level_name AS levelname,
//   staff.gender,
//   (
//     CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
//   END
// ) AS gender,

// (
//   CASE WHEN w.flag = 1 THEN 'Submitted By HRD' 
//   WHEN w.flag = 3 THEN 'Approve By Team Coordinator' 
//   WHEN w.flag = 4 THEN 'Reject By Team Coordinator' 
//   WHEN w.flag = 5 THEN 'Approve By Personnal'
//    WHEN w.flag = 6 THEN 'Reject By Personnal' 
//    WHEN w.flag = 7 THEN 'Approve By ED'  
//    WHEN w.flag = 8 THEN 'Reject By ED' 
//     ELSE 'Not status here'
// END) AS status
//     FROM staff
  
// INNER JOIN
//   lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
// INNER JOIN
//   msdesignation ON staff.`designation` = msdesignation.`desid`
// INNER JOIN
//   tbl_workflowdetail as w ON staff.`staffid` = w.`staffid`
// INNER JOIN
//   tbl_probation_review_performance AS probre ON staff.`staffid` = probre.`staffid`

//     where msdesignation.`level`=4 AND `staff`.flag = 0 ";  

//     if ($this->loginData->RoleID == 17) {
//       $sql .= " AND w.`receiver` =".$this->loginData->staffid;  
//     }
//     $sql .= "   order by staff.staffid ASC ";  
//     //echo $sql;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



    
    /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getProbationStatus($staffid)
  {
    
    try{
   $sql = "SELECT flag FROM `tbl_probation_review_performance` where `staffid`=".$staffid;

         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}