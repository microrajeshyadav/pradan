<?php 

/**
* State Model
*/
class Staff_request_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    public function get_candidaateid($staff_id)
    {
        try{
        $sql="select * from staff where staffid='$staff_id'";
        
        return  $this->db->query($sql)->row();
        }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
    }


  public function getstaffRequest()
  { 
    try{
     $sql = "Select a.status, a.staffid, b.emp_code, a.newofficeid, c.officename, b.name as staffname FROM stafftransferrequest as a 
      Left JOIN staff as b on a.staffid = b.staffid
      left join lpooffice as c on  a.newofficeid = c.officeid";  
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
       
  }





public function get_staffrequestdetails($token)
  { 
    try{
     $sql = "Select a.status,a.requestDate, a.staffid, b.emp_code, a.newofficeid, b.name as staffname FROM stafftransferrequest as a 
      Left JOIN staff as b on a.staffid = b.staffid WHERE a.staffid=".$token;  
      return  $this->db->query($sql)->result()[0];

       }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
  }



public function get_staffrequestcountdetails($token)
  { 
    try{
     $sql = "Select count(requested) as rcount FROM stafftransferrequest  WHERE staffid=".$token;  
      return  $this->db->query($sql)->result()[0];
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
       
  }

  public function get_staffrequestid($token)
  { 
    try{
     $sql = "Select requested FROM stafftransferrequest  WHERE staffid=".$token;  
      return  $this->db->query($sql)->result()[0];
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
       
  }

public function get_staffname()
  {
    try{
            $this->db->select("lpooffice.officeid,lpooffice.officename,staff_transaction.old_office_id,staff_transaction.new_office_id");
            $this->db->from("staff_transaction");
            $this->db->join('staff', 'staff_transaction.staffid = staff`.staffid ', 'inner');
              $this->db->join('lpooffice', 'staff_transaction.old_office_id = lpooffice.officeid', 'inner');
              $this->db->group_by('lpooffice.officeid');

                  
              $query=$this->db->get();
              return $query->result();
              //echo $this->db->last_query();

              }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

  }

	public function getstaffname()
	{
    try{
		 $sql = "SELECT
    staff.name,
    staff.doj,
    staff.gender,
    staff.emp_code,
    staff.candidateid,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    staff.joiningandinduction,
    staff.gender,
    staff.status,
   (CASE
    WHEN flag = 0 THEN 'Save'
    WHEN flag = 1 THEN 'Submitted'
    WHEN flag = 4 THEN 'Approved'
    WHEN flag = 3 THEN 'Rejected'
    ELSE 'Not status here'
END) as flag
    FROM
        staff_transaction
     INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 

    INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`
    inner join lpooffice on staff_transaction.new_office_id=lpooffice.officeid
    inner join msdesignation on staff.designation=msdesignation.desid
    GROUP BY
        `staff_transaction`.staffid
        order by staff.emp_code";  
      // echo $sql;
      return  $this->db->query($sql)->result();

      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }  
	}



    public function fetchdatas($token)
    { 

      try{
        $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
        $result = $this->db->query($sql);
        $rows = $result->num_rows();
        $tm='-1';
         if($rows==0)
         {  
           return $tm;
         }
         else
         {
             
           $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
        
            $query=$this->db->query($sql);
         }

         }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
     //  

    }



}