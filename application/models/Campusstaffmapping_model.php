<?php 

/**
* Master  Model
*/
class Campusstaffmapping_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	  // /**
    //  * Method getstaff() staff detail.
    //  * @access  public
    //  * @param   $token
    //  * @return  string.
    //  */

    public function getRecuters()
    {
        try{

              $sql = "SELECT mcr.`id_mapping_campus_recruiter`,mcr.`anchor`,mcr.`campusid`,cam.`campusname`, mcr.`campusintimationid`, st.`name` FROM `mapping_campus_recruiters` AS mcr 
                  INNER JOIN mstcampus AS cam ON mcr.`campusid` = cam.`campusid`
                  Inner JOIN staff as st ON  st.`staffid` = mcr.`recruiterid` WHERE mcr.`isdeleted`='0' ORDER BY mcr.`createdon` DESC";  
             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

	public function index()
	{

	}


    // /**
    //  * Method getstaff() staff detail.
    //  * @access  public
    //  * @param   $token
    //  * @return  string.
    //  */

    public function getRecruitment()
    {
        try{

            
              $sql = "SELECT  staff.`staffid`, staff.`name` FROM mapping_staff_recruitment  
              INNER JOIN  `staff` ON mapping_staff_recruitment.`recruitmentteamid` = staff.`staffid` WHERE `isdeleted`='0' ";  

             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

    /**
     * Method getCampus() staff detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

	public function getCampus()
{
  try{

      $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND b.`mailstatus`= 1 AND b.`campus_status`=0 ";
  
    //echo $sql; die;

    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


    /**
     * Method getRecruitmentDetails() Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function getCountCampusRecruitment($campusid)
    {
        try{
                
                $sql = "SELECT count(id_mapping_campus_recruiter) as RCount FROM mapping_campus_recruiters Where isdeleted !=1 AND campusid =".$campusid;
                $res = $this->db->query($sql)->result()[0];

                return  $res;
               
        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }  


 /**
     * Method getRecruitmentDetails() Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function getMappedCampusRecruitment($campusid,$campusintimationid)
    {
        try{
                $this->db->select('id_mapping_campus_recruiter,campusid,recruiterid,anchor');
                $this->db->where('campusid', $campusid);
                $this->db->where('campusintimationid', $campusintimationid);
                $this->db->where('isdeleted', 0);

                
                return  $this->db->get('mapping_campus_recruiters')->result(); 
                //echo $this->db->last_query(); die;
               
        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }  

    /**
     * Method delete() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */
    public function delete($token)
    {
        try {

                $deleteArray = array(
                        'isdeleted'    => 1,
                      );

                    $this->db->where("campusid",$token);
                  return ($this->db->update('mapping_campus_recruiters', $deleteArray)) ? 1 : -1; //$this->db->last_query(); die;
        }
        catch (Exception $e) {
            print_r($e->getMessage());die;
        }
    }

	


}