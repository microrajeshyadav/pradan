<?php 

/**
* Ed_staff_approval Model
*/
class Ed_staff_approval_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getworkflowdetaillist()
  {
    
    try{

      $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  c.`doj_team` as date_of_appointment,
  c.`probation_date`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  a.receiver,
  a.sender,
  b.`trans_flag` as status,
  (
     CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate By Personnel' 
    
   WHEN b.trans_flag = 3 and t.probation_extension_date is not null THEN 'Review Submitted by TC' 
  WHEN b.trans_flag = 3 and t.satisfactory = 'satisfactory'  THEN 'Review Submitted by TC'
  WHEN b.trans_flag = 3 and t.satisfactory = 'notsatisfactory'  THEN 'Review Submitted by TC'
  WHEN b.`trans_flag` = 4 THEN 'Disagreed By TC/Intergrator' 
     WHEN b.`trans_flag` = 5 THEN 'Agreed By ED' 
     
     WHEN b.`trans_flag` = 6 THEN 'Rejected By ED' 
     WHEN b.`trans_flag`= 7 THEN 'Agreed by Personnel'
      WHEN b.`trans_flag`= 8 THEN 'Disagreed by Personnel' 
     
      WHEN b.trans_flag = 9 and t.probation_extension_date is not null THEN 'Letter Sent to Staff' 
  WHEN b.trans_flag = 9 and t.satisfactory = 'satisfactory'  THEN 'Letter Sent to Staff'
  WHEN b.trans_flag = 9 and t.satisfactory = 'notsatisfactory'  THEN 'Letter Sent to Staff'
     ELSE 'Stage N.A.'
  END
) AS flag,
case when t.probation_extension_date is not null THEN 'Probation - Extend' 
  WHEN t.satisfactory = 'satisfactory'  THEN 'Probation - Complete'
  WHEN t.satisfactory = 'notsatisfactory'  THEN 'Probation - Separation'  WHEN b.`trans_flag` = 1  THEN 'Pending' end
 Request
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
  LEFT JOIN `tbl_probation_review_performance` AS t 
  ON a.`r_id` = t.`transid`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where b.`trans_flag` >= 1  AND b.`trans_status` = 'Probation' 
  AND a.`receiver` =".$this->loginData->staffid." ";

      //print_r($this->loginData); die;
      
//   $sql = "SELECT 
//     st.`name`,    st.`emp_code`,    a.`process_type`,    st.`staffid`,     a.r_id id,    a.`name` AS sendername,    a.flag AS status  ,    a.`scomments`,    a.r_id,    a.`createdon` AS Requestdate,
//     (
//         CASE WHEN a.flag = 1 THEN 'Process Initiate By Personnel' WHEN a.flag = 3 THEN 'Agreed By TC/Intergrator' WHEN a.flag = 5 THEN 'Agreed By ED' WHEN a.flag = 4 THEN 'Disagreed By TC/Intergrator' WHEN a.flag = 6 THEN 'Disagreed By ED' WHEN a.flag = 7 THEN 'Agreed by Personnel' ELSE 'Not status here'
//     END
// ) AS flag


// FROM
// staff st LEFT join 
// staff_transaction t on t.staffid=st.staffid left join 
// (Select workflowid,r_id,sender,w.flag,scomments,w.type,s.name,w.createdon,s.staffid,t.trans_status,d.process_type from staff as s LEFT JOIN 
// tbl_workflowdetail as w
// on w.sender = s.staffid
// left join staff_transaction t on t.id=w.r_id
// left join tbl_probation_review_performance b on t.`id` = b.transid 
// LEFT JOIN `mst_workflow_process` AS d
// ON  w.`type` = d.`id`
// where r_id in (Select Max(id) id  from staff_transaction group by staffid)  and workflowid  in (Select Max(workflowid) id from tbl_workflowdetail group by r_id)) a on a.r_id=t.id

// WHERE
//       a.trans_status = 'Probation'   order by a.workflowid";
      // echo $sql; 
         $res = $this->db->query($sql)->result();
         // echo "<pre>";
         // print_r($res);
         // die;
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

    /**
   * Method getworkflowdmidtermreviewetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getworkflowdmidtermreviewetaillist()
  {
    
    try{

    

$sql="SELECT 
    st.`name`,    st.`emp_code`,    d.`process_type`,    st.`staffid`,    t.id,    st.`name` AS sendername,    t.trans_flag AS status  ,    a.`scomments`,    t.id as r_id,    a.`createdon` AS Requestdate,
    (
        CASE WHEN t.trans_flag = 1 THEN 'Process Initiate' WHEN t.trans_flag = 3 THEN 'Accepted By TC/Intergrator' WHEN t.trans_flag = 5 THEN 'Accepted By ED' WHEN t.trans_flag = 4 THEN 'Rejected By TC/Intergrator' WHEN t.trans_flag = 6 THEN 'Rejected By ED' WHEN t.trans_flag = 7 THEN 'Accepted By Personnel Unit' WHEN t.trans_flag = 8 THEN 'Rejected By Personnel Unit' ELSE 'Not status here'
    END
) AS flag


FROM
staff st LEFT join 
staff_transaction t  on st.staffid = t.staffid

LEFT JOIN `tbl_probation_review_performance` AS b
ON
    t.`id` = b.transid AND st.staffid = b.staffid

left JOIN (Select * from tbl_workflowdetail where r_id  in (Select id from  (Select Max(id) id ,staffid from staff_transaction group by staffid) as a) group by r_id order by workflowid desc) a on t.id = a.r_id   and  a.`staffid` = st.`staffid`
LEFT JOIN `mst_workflow_process` AS d
ON
    a.`type` = d.`id`

WHERE
      t.trans_status = 'Midtermreview'  order by a.workflowid";
       if ($this->loginData->RoleID == 18) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }


 //die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function getworkflowdetailseperationlist()
  {

    
    try{

    // Only Approve and Rejection will be enable to personnel for stage 5 and 6
    //    Generate acceptance of regignation in below stage of personnel
      //print_r($this->loginData); die;
      $receiver=$this->loginData->staffid;
      
  $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  concat(d.process_type , ' - ' , b.trans_status) as process_type,
  a.`staffid`,
  k.`name` as sendername, 
  b.`trans_flag` as status,
  b.trans_status, 
  b.`id` as transid,
  l.level,

  a.`r_id`,
  a.`createdon` AS Requestdate,
  (
    CASE 
    WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 4 THEN 'Rejected By TC/Intergrator'
    WHEN b.`trans_flag` = 21 THEN 'Approve By HR Unit' 
    WHEN b.`trans_flag` = 22 THEN 'Rejected By HR Unit'

    WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit'     
    WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' 
  
    WHEN b.trans_flag = 7 THEN 'Accepted by ED' 
    WHEN b.trans_flag = 8 THEN 'Rejected By ED' 
  
    WHEN b.`trans_flag` = 11 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 12 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 13 THEN 'Approve By TC/Intergrator' 
    WHEN b.`trans_flag` = 14 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 9 THEN 'Approve By Team Accoutant' 
    WHEN b.`trans_flag` = 10 THEN 'Rejected By Team Accoutant'    
    WHEN b.`trans_flag` = 15 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 16 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 17 THEN 'Approve By Team Accoutant' 
    WHEN b.`trans_flag` = 18 THEN 'Rejected By Team Accoutant' 
    WHEN b.`trans_flag` = 19 THEN 'Approve By Head Office Finance' 
    WHEN b.`trans_flag` = 20 THEN 'Rejected By Head Office Finance'   
    WHEN b.`trans_flag` = 25 THEN 'Handed Over  By Staff'
    WHEN b.`trans_flag` = 27 THEN 'Taken Over  By TC/Intergrator'
    WHEN b.`trans_flag` = 28 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 29 THEN 'Approve By ED'
    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b 
ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
  LEFT JOIN
  `msdesignation` AS l ON c.`designation` = l.`desid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
  Where 1=1 And b.trans_status IN('Termination', 'Termination during Probation', 'Death', 'Discharge simpliciter/ Dismiss', 'Desertion cases', 'Retirement', 'Resign','Super Annuation')";
  //echo $sql;exit();

  if ($this->loginData->RoleID == 18) {
  $sql .= " AND a.receiver =$receiver OR b.trans_flag >= 1";
  }

  $sql .=" group by c.emp_code ORDER BY a.`workflowid` DESC ";


  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
  // echo $sql; die;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
			INNER join staff as st ON u.staffid = st.staffid
			WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0";

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getTransferStaffDetails() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTransferStaffDetails($transid)
  {
    
    try{
        $sql = "SELECT * FROM `staff_transaction` WHERE `id` =".$transid;

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist($token)
  {
    
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
WHERE
  a.`transid`= ".$token;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSinglestaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSinglestaffdetailslist($transid)
  {
    
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
WHERE
  a.`transid`= ".$transid;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffclerancelist($token)
  {
    
    try{
        $sql = "SELECT * FROM `tbl_clearance_certificate` WHERE `id` =".$token;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getcountstaffitmsclerancelist($token)
  {
    
    try{

         $sql = "SELECT count(clearance_certificate_id) as cccount FROM `tbl_clearance_certificate_transaction` WHERE `clearance_certificate_id` =".$token;
         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffitmsclerancelist($token)
  {
    
    try{
        $sql = "SELECT * FROM `tbl_clearance_certificate_transaction` WHERE `clearance_certificate_id` =".$token;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist1($token)
  {
    
    try{

        $sql .= "SELECT
              a.`letter_date`,
              a.`transferno`,
              des.`desname`,
              a.`responsibility_on_date`,
              c.`name`,
              c.`emp_code`,
              b.`staffid`,
              b.`id` AS transid,
              e.`officename`,
              b.`trans_flag` AS STATUS,
              b.`reason`,
              b.`date_of_transfer` AS proposeddate,
              f.`officename` AS newoffice
            FROM
              `tbl_iom_transfer` AS a
            LEFT JOIN
              `staff_transaction` AS b ON a.`transid` = b.`id`
            LEFT JOIN
              `staff` AS c ON a.`staffid` = c.`staffid`

            LEFT JOIN
              `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
            LEFT JOIN
              `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
            LEFT JOIN
              `msdesignation` AS des ON c.`designation` = des.`desid`
            WHERE  a.integrator_ed = ". $this->loginData->staffid ;
  
        $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffjoiningreport($token)
  {
    
    try{
   $sql = "SELECT
  jrnp.`id` as joinid,
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
LEFT JOIN
  `tbl_joining_report_new_place` AS jrnp ON a.`transid` = jrnp.`transid`
WHERE jrnp.`flag` = 1 AND  a.`transid` =".$token;

 
        $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method getStaffjoningplace() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffjoningplace($token)
  {
    
    try{
       $sql = "SELECT * FROM `tbl_joining_report_new_place` WHERE  flag = 1 AND `id` =".$token;  

        $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}