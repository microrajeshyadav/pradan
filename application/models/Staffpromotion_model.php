<?php 

/**
* State Model
*/
class Staffpromotion_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();


	}


 public function get_staffDetails($token)
 {
   try
   {

    $sql="SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    lpooffice.officename,
    msdesignation.desname
    FROM
    staff_transaction
    INNER JOIN
    (
    SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
    staff_transaction.date_of_transfer
    ) AS MaxDate
    FROM
    staff_transaction
    GROUP BY
    staffid
  ) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
  inner join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
  inner join msdesignation on staff_transaction.new_designation=msdesignation.desid
  INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
  `staff`.staffid =$token
  ORDER BY
  `staff`.name ASC
  ";


      //$query=$this->db->get();
  return  $this->db->query($sql)->row();

}

catch (Exception $e) {
 print_r($e->getMessage());die;
}

}

   /**
   * Method getview_history() display the details .of staff
   * @access  public
   * @param Null
   * @return  Array
   */
   public function getview_history($token)
   {
//echo $token;
   //  $this->db->select("staff.name,staff_transaction.old_office_id,staff_transaction.date_of_transfer,staff_transaction.new_office_id,lpooffice.officename as 'present_office', staff_transaction.old_designation, staff_transaction.new_designation,msdesignation.desname,msdesignation.desid  as 'office_id'");
   //  $this->db->from('staff_transaction');
   //  $this->db->join('staff', 'staff_transaction.staffid = staff`.staffid ', 'inner');
   //  $this->db->join('lpooffice', 'lpooffice.officeid = staff_transaction.old_office_id', 'inner');
   //  $this->db->join('msdesignation', 'msdesignation.desid = staff_transaction`.`old_designation', 'lnner');
   //  $this->db->where('staff_transaction.staffid',$token);
   // $this->db->where('staff_transaction.trans_status','Promotion');
   //  $query=$this->db->get();
    //echo "<pre>";
    //print_r($query);die();

    $sql="SELECT a.staffid, b.name, c.officename AS oldofffice, a.date_of_transfer, e.desname as new_designation, f.desname as old_designation FROM staff_transaction AS a INNER JOIN staff AS b ON a.staffid = b.staffid INNER JOIN lpooffice AS c ON c.officeid = a.old_office_id  INNER JOIN msdesignation as e on e.desid=a.new_designation INNER JOIN msdesignation as f on f.desid=a.old_designation WHERE a.staffid = $token and a.trans_status = 'Promotion'";

    $result = $this->db->query($sql)->result();

    return $result;




    return $query->result();
     // echo $this->db->last_query();




  }

  public function index()
  {

  }

  public function getstaffname($token)
  {
   $sql = "SELECT
   staff.emp_code,
   staff.name,
   msdesignation.desname,
   staff.reportingto,
   staff_transaction.staffid,
   staff_transaction.date_of_transfer,
   staff_transaction.new_office_id,
   staff_transaction.new_designation
   FROM
   staff_transaction
   INNER JOIN(
   SELECT
   staff_transaction.staffid,
   MAX(
   staff_transaction.date_of_transfer
   ) AS MaxDate
   FROM
   staff_transaction
   GROUP BY
   staffid
   ) AS TMax
   ON
   `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
   'Resign',
   'Termination',
   'Retirement',
   'Death'
   )
   INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`
     INNER JOIN `msdesignation` on `msdesignation`.desid = `staff`.`designation`
   where 1=1 and `staff`.staffid NOT IN (1)  and staff_transaction.staffid =".$token."
   group by `staff_transaction`.staffid ORDER BY  `staff`.name ASC ";  

   // echo $sql; die;

   return  $this->db->query($sql)->row();
 }

 public function get_alloffice($officeid)
 {
  // echo $officeid; die;
   try
   {
   // echo "office id=".$p_office;
   // die;
    $this->db->select("officeid,officename");
    

    $this->db->from('lpooffice');
    $this->db->where('officeid',$officeid);

    $query=$this->db->get();
      // echo $this->db->last_query();
      // die();
    return $query->result();
  }


  catch (Exception $e) 
  {
   print_r($e->getMessage());die;
 }

}
public function get_alldesignation($p_designation=null)
{
 try
 {
  $this->db->select("desid,desname");
  $this->db->from('msdesignation');
  $this->db->where_not_in('desid',$p_designation);


  $query=$this->db->get();
      // echo $this->db->last_query();
      // die();
  return $query->result();
}


catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


public function fetchpresent($staffidd)
{
  $sql = "
  SELECT c.officename,c.officeid FROM staff AS a 
  INNER JOIN staff_transaction AS b ON a.staffid = b.staffid 
  INNER JOIN lpooffice AS c ON b.new_office_id = c.officeid WHERE a.staffid=$staffidd";  
  return  $this->db->query($sql)->row();

}

public function getStaffTransactioninc($tablename,$incval){


    try{

        // echo $tablename;
        // echo $incval;  die;
        ////'staff_transaction',@a; 

      $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }



}