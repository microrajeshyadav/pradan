<?php 

/**
* Biodata Full Information Model Class
*/
class Biodata_form_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try
    {
    //echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
 if ($date=='//') {
      $date = NULL;
   }
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}





/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function changedatedbformate($Date)
  {
    try
    {
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
   if ($date=='//') {
      $date = NULL;
   }
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}

	/**
   * Method getSelectedCandidate() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSelectedCandidate()
  {

    try{

     $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";

     $res = $this->db->query($sql)->result();

     return $res;
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateDetails($token)
{

  try{

   $sql = 'SELECT * FROM `tbl_candidate_registration` 
   left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
   left join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid
   Where `tbl_candidate_registration`.candidateid ='.$token.''; 


   $result = $this->db->query($sql)->result();

   return $result[0];

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getHrdemailid() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getHrdemailid()
{
 try{

      $sql = "SELECT
    staff.staffid,staff.name,staff.emailid
FROM
    `mstuser`
INNER JOIN staff ON mstuser.staffid = staff.staffid
inner join role_permissions on  role_permissions.RoleID=mstuser.RoleID
WHERE

    role_permissions.Controller = 'Campusinchargeintimation' and role_permissions.RoleID=16 and role_permissions.Action='index' ORDER BY
  `staff`.staffid  DESC  LIMIT 0,1
 ";
 //echo $sql;

      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateDetailsPrint($token)
{

  try{

      $sql = 'SELECT `tbl_candidate_registration`.*, `tbl_candidate_communication_address`.*,a.`name` as presentstatename, ad.`name` as presentdistrictname,b.`name` as permanentstatename, bd.`name` as permanentdistrictname FROM `tbl_candidate_registration` 
    left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid 
     left join `state` as a ON `tbl_candidate_communication_address`.presentstateid = a.statecode 
     left join `district` as ad ON `tbl_candidate_communication_address`.presentdistrict = ad.districtid 
     left join `state` as b ON `tbl_candidate_communication_address`.permanentstateid = b.statecode 
     left join `district` as bd ON `tbl_candidate_communication_address`.permanentdistrict = bd .districtid Where `tbl_candidate_registration`.candidateid ='.$token.'';


   $result = $this->db->query($sql)->result();

   return $result[0];

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateFamilyMemberDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_family_members` Where `tbl_family_members`.candidateid ='.$token.''; 


    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateFamilyMemberDetailsPrint($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_family_members`
    LEFT JOIN  `sysrelation` on `tbl_family_members`.relationwithemployee =  `sysrelation`.id Where `tbl_family_members`.candidateid ='.$token.''; 


    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


  /**
   * Method getCountFamilyMember() get No Of family Member !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountFamilyMember($token)
  {

    try{
      $sql = 'SELECT count(*) as `Fcount` FROM `tbl_family_members`      
      Where `tbl_family_members`.candidateid ='.$token.''; 
      $result = $this->db->query($sql)->result();

      return $result[0];

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }






  /**
   * Method getCountIdentityNumber() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountIdentityNumber($token)
  {

    try{
      $sql = 'SELECT count(*) as `Icount` FROM `tbl_identity_details`      
      Where `tbl_identity_details`.candidateid ='.$token.''; 
      $result = $this->db->query($sql)->result();

      return $result[0];

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



/**
   * Method getCandidateIdentityDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateIdentityDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_identity_details` Where `tbl_identity_details`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateIdentityDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateIdentityDetailsPrint($candidateid)
{

  try{

     $sql = 'SELECT * FROM `tbl_identity_details`
    Left JOIN `sysidentity` ON `tbl_identity_details`.identityname =`sysidentity`.id  Where `tbl_identity_details`.candidateid ='.$candidateid.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


 /**
   * Method getCountTrainingExposure() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCountTrainingExposure($token)
 {

  try{
    $sql = 'SELECT count(*) as `TEcount` FROM `tbl_training_exposure`      
    Where `tbl_training_exposure`.candidateid ='.$token.''; 
    $result = $this->db->query($sql)->result();

    return $result[0];

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateTrainingExposureDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateTrainingExposureDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_training_exposure` Where `tbl_training_exposure`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCountGapYear() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCountGapYear($token)
 {

  try{
    $sql = 'SELECT count(*) as `GYcount` FROM `tbl_gap_year`      
    Where `tbl_gap_year`.candidateid ='.$token.''; 
    $result = $this->db->query($sql)->result();

    return $result[0];

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateGapYearDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateGapYearDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_gap_year` Where `tbl_gap_year`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getCountWorkExprience() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCountWorkExprience($token)
{

  try{
    $sql = 'SELECT count(*) as `WEcount` FROM `tbl_work_experience`      
    Where `tbl_work_experience`.candidateid ='.$token.''; 
    $result = $this->db->query($sql)->result();

    return $result[0];

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateWorkExperienceDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateWorkExperienceDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_work_experience` Where `tbl_work_experience`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


   /**
   * Method getCountLanguage() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

   public function getCountLanguage($token)
   {

    try{
      $sql = 'SELECT count(*) as `Lcount` FROM `tbl_language_proficiency`      
      Where `tbl_language_proficiency`.candidateid ='.$token.''; 
      $result = $this->db->query($sql)->result();

      return $result[0];

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateLanguageDetails($token)
{

  try{
    $sql = 'SELECT * FROM `tbl_language_proficiency` 
          left join syslanguage on `tbl_language_proficiency`.languageid = `syslanguage`.lang_cd
    Where `tbl_language_proficiency`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateLanguageDetailsPrint($candidateid)
{

  try{
     $sql = 'SELECT * FROM `tbl_language_proficiency` left join `syslanguage`  on `syslanguage`.lang_cd = `tbl_language_proficiency`.languageid Where `tbl_language_proficiency`.candidateid ='.$candidateid.''; 

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getCandidateOtherInformationDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateOtherInformationDetails($token)
{

  try{

    $sql = 'SELECT * FROM `tbl_other_information`  Where `tbl_other_information`.candidateid ='.$token.''; 

    $result = $this->db->query($sql)->result();

    return $result[0];

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getUgEducation() get Post Under Graduation Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getUgEducation()
{

  try{

   $sql = "SELECT id,pgname FROM `mstpgeducation` Where groupid = 3  AND isdeleted=0";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getPgEducation() get Post Graduation Education List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getPgEducation()
{

  try{
      $sql = "SELECT id,pgname FROM `mstpgeducation` Where groupid = 4 AND isdeleted=0";
   
       
    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

  /**
   * Method getViewSelectedCandidate() get Selected Candidates Listing.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewSelectedCandidate($token)
  {

   try{

     $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";

     if (!empty($token)) {
       $sql .=" AND candidateid=$token";
     }

     $res = $this->db->query($sql)->result();

     return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method getState() get State name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getState()
  {

    try{

      $sql = "SELECT * FROM state where isdeleted='0' ORDER BY name";

      $res = $this->db->query($sql)->result();

      return $res;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

   /**
   * Method getCampus() get Campus Name.
   * @access  public
   * @param Null
   * @return  Array
   */


   public function getCampus()
   {
    try{

      $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  /**
   * Method mail_exists() get Email Id Exist OR Not.
   * @access  public
   * @param Null
   * @return  Array
   */

  function mail_exists($key)
  {
    try
    {
    $this->db->where('emailid',$key);
    $query = $this->db->get('tbl_candidate_registration');
    if ($query->num_rows() > 0){
      return true;
    }
    else{
      return false;
    }

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  }

 /**
   * Method getSysLanguage() get Language Name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSysLanguage()
 {

  try{
    $this->db->select('*');
    $this->db->where('isdeleted', 0);
    $this->db->from('syslanguage');
      return $this->db->get()->result(); //echo $this->db->last_query(); die;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


 /**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSysRelations()
  {

    try{
      $data  = array('Mother' => 'Mother','Father' => 'Father', 'Spouse' => 'Spouse' , 'Child' => 'Child');
      $this->db->select('*');

      $this->db->from('sysrelation');
      $this->db->where('isdeleted', 0);
      $this->db->where('status', 0);
      $this->db->where_in('relationname', $data);
      //$this->db->get();
     // echo $this->db->last_query();
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


/**
   * Method getSysIdentity() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSysIdentity()
{

  try{
    $this->db->select('*');
    $this->db->from('sysidentity');
    $this->db->where('isdeleted', 0);
    return $this->db->get()->result();

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


/**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneralFormStatus($candiid)
{

  try{

     $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf`
        WHERE `gnaf`.`candidateid` = ".$candiid."";   
    
    $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->row();
      }

  

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getJoiningReport($candiid)
{

  try{

     $sql = "SELECT `gnaf`.status as joinreportstatus 
          FROM `tbl_joining_report` as `gnaf` 
          WHERE `gnaf`.`candidateid` = ".$candiid." ";

           $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->result()[0];
      }

         // $res = $this->db->query($sql)->result();

   // return $res;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


/**
   * Method getCandidateJoiningStatus() get Candidate Joning status .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateJoiningStatus($candiid)
{

  try{

      $sql = "SELECT `tblcr`.joinstatus  FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid." "; //die;
    $res = $this->db->query($sql)->result()[0];

    return $res;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


/**
   * Method getBdfFormStatus() get Candidate registration status .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getBdfFormStatus($candiid)
{

  try{

      $sql = "SELECT `tblcr`.BDFStatusaftergd, `tblcr`.campustype  FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid." "; 
  

    $res = $this->db->query($sql)->result()[0];

    return $res;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


public function getDistrict($id = NULL)
{
  try{
    //$sql = " SELECT * FROM `district` WHERE `districtid`= $id ";

    $sql = " SELECT * FROM `district` ORDER BY `districtid` ASC ";

    $result = $this->db->query($sql)->result();

    return $result;


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

}