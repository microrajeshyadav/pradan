<?php 

/**
* Event Model
*/
class Event_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function getBatchlist()
  {
    try{

      $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  public function getBatchEventDetail($token)
  {
    try{

      $sql = "SELECT `batch` FROM `mstevents` where id= $token";  
      $result = $this->db->query($sql)->row();

      return $result;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

    public function getBatchEvent($token)
  {
    try{

      $sql = "SELECT e.`batch`,b.batch as batchname FROM `mstevents` as e
       left join mstbatch as b on b.id = $token
      where b.isdeleted= 0";  
      $result = $this->db->query($sql)->row();

      return $result;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  /*
    function getAllSuprvsn() is get All Supervisors email id 
    result row;
    created by rajat
*/
  public function getAllSuprvsn()
  {
    
    try{

  $sql = "SELECT name,emailid,staffid,designation FROM staff WHERE STATUS = 1 AND designation IN (4,16) ORDER BY
  `staff`.staffid";   
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getreportint_to($staffid)
  {
    
    try{

  $sql = "SELECT name,staffid FROM staff WHERE reportingto='$staffid'";   
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

}