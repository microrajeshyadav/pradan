<?php 

/**
* State Model
*/
class Staff_resignation_withdrawal_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

public function getStaffTransactioninc($tablename,$incval){


   try{

       // echo $tablename;
       // echo $incval;  die;
       // //'staff_transaction',@a;

     $sql = "select get_maxvalue($tablename,$incval) as maxincval";

     $result = $this->db->query($sql)->result()[0];

     return $result;

   }
   catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  }


  /**
   * Method get_staffReportingto() get staff reportingto. 
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffReportingto($token)
    {
       
       try
       {

          $sql=" SELECT staffid, reportingto,designation FROM staff where staffid = ".$token;
         
          return  $this->db->query($sql)->result()[0];

     } catch (Exception $e) {
       print_r($e->getMessage());die;
     }

   }

 /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
       try
   {

      $sql="SELECT
  staff_transaction.staffid,
  staff_transaction.date_of_transfer,
  staff_transaction.new_office_id,
  staff_transaction.new_designation,
  lpooffice.officename,
  msdesignation.desname
FROM
  staff_transaction
INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
inner join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
inner join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

	public function getdata()
	{
    try{

      $sql = "SELECT
          staff.name,
          staff_transaction.staffid,
          staff_transaction.date_of_transfer,
          staff_transaction.new_office_id,
          staff_transaction.new_designation,
          staff.
          FROM
          staff_transaction
          INNER JOIN(
          SELECT
          staff_transaction.staffid,
          MAX(
          staff_transaction.date_of_transfer
          ) AS MaxDate
          FROM
          staff_transaction
          GROUP BY
          staffid
      ) AS TMax
      ON
      `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
      )

      INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

      where 1=1 and `staff`.staffid NOT IN (1)

      ORDER BY  `staff`.name ASC ";
      return $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
	}

  public function getdataview()
  {
      $sql = "";
      return $this->db->query($sql)->result();
  }

  public function fetchdatas($token)
    {
      try{
      // $this->db->select("staff.name,staff_transaction.old_office_id,staff_transaction.date_of_transfer,staff_transaction.new_office_id,lpooffice.officename as 'present_office', staff_transaction.old_designation, staff_transaction.new_designation,msdesignation.desname,msdesignation.desid  as 'office_id'");
      // $this->db->from('staff_transaction');
      // $this->db->join('staff', 'staff_transaction.staffid = staff`.staffid ', 'inner');
      // $this->db->join('lpooffice', 'lpooffice.officeid = staff_transaction.old_office_id', 'inner');
      // $this->db->join('msdesignation', 'msdesignation.desid = staff_transaction`.`old_designation', 'lnner');
      // $this->db->where('staff_transaction.staffid',$token);
      // $this->db->where('staff_transaction.trans_status','Promotion','Transfer');
      $sql="SELECT a.staffid, b.name, c.officename AS oldofffice, a.date_of_transfer, e.desname AS new_designation, f.desname AS old_designation, z.officename AS newoffice, a.trans_status, j.separationtype, j.dateofleaving FROM staff_transaction AS a INNER JOIN staff AS b ON a.staffid = b.staffid LEFT JOIN lpooffice AS c ON c.officeid = a.old_office_id LEFT JOIN lpooffice AS z ON z.officeid = a.new_office_id LEFT JOIN msdesignation AS e ON e.desid = a.new_designation LEFT JOIN msdesignation AS f ON f.desid = a.old_designation INNER JOIN staff as j on j.staffid=a.staffid WHERE j.staffid= $token AND a.trans_status = 'JOIN'";


      //$query=$this->db->get();
      return  $this->db->query($sql)->result();
     // echo $this->db->last_query();
      
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_seperation_detail($token)
    {
       try
   {

      $sql="SELECT c.name, k.name as sendername, g.name as receivername,b.senddate as requestdate,b.type, l.process_type,a.date_of_transfer as seperatedate, a.staffid, a.id as transid,

      (
      CASE a.trans_flag WHEN 1 THEN 'Request submitted' WHEN 4 THEN 'Request Approved' WHEN 3 THEN 'Request Rejected' ELSE 'Other' END
      ) AS status

        FROM staff_transaction as a 
        INNER JOIN `tbl_workflowdetail` as b on a.id = b.r_id
        INNER JOIN `staff` as c on a.staffid = c.staffid
        INNER JOIN `staff` as k on b.sender = k.staffid
        INNER JOIN `staff` as g on b.receiver = g.staffid
        INNER JOIN `mst_workflow_process` as l on b.type = l.id

      WHERE a.trans_status='Resign' AND  a.staffid = $token  ORDER BY c.name ASC";


      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

}