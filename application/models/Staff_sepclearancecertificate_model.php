<?php 

/**
* Master  Model
*/
class Staff_sepclearancecertificate_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}   

	  /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_clearance_detail($token)
    {
       try
   {

      $sql="SELECT c.name,a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code, a.trans_status

        FROM staff_transaction as a 
        INNER JOIN `staff` as c on a.staffid = c.staffid
        left JOIN `msdesignation` as b on a.new_designation = b.desid

      WHERE a.id = $token";
      // echo $sql;
      // die;


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_detail($token)
    {
       try
   {

      $sql="SELECT a.id, a.transid, a.separation_due_to, a.flag,
       (
      CASE a.flag WHEN 2 THEN 'Transfer' WHEN 3 THEN 'Seperation' END
      ) AS type

        FROM  tbl_clearance_certificate as a 
      WHERE a.id = $token";

      // echo $sql;
      // die;


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_transaction($token = NULL)
    {
       try
   {

      $sql="SELECT a.description, a.project

        FROM  tbl_clearance_certificate_transaction as a 
      WHERE a.clearance_certificate_id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


     /**
   * Method get_staff_finance_detail() display the details .of finance
   * @access  public
   * @param Null
   * @return  row
   */

     

     /**
   * Method get_staff_sepervisior_detail() display the details .of staff supervisor
   * @access  public
   * @param Null
   * @return  row
   */

public function get_staff_sepervisior_detail($token)
   {
      try
    {

      $sql="SELECT a.name,a.reportingto,r.name,r.encrypted_signature  FROM staff as a
          left join staff as r on a.reportingto=r.staffid

         WHERE a.staffid = $token";
       
    // echo $sql; die;

       //$query=$this->db->get();
       return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}


public function get_finance_detail($token)
   {
      try
    {

      $sql="SELECT a.name,a.encrypted_signature  FROM staff as a
          

         WHERE a.staffid = $token";
       
    // echo $sql; die;

       //$query=$this->db->get();
       return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}


public function get_staff_sep_detail($token)
   {
      try
    {

      $sql="SELECT c.name,c.permanentpincode,c.contact,c.emailid,a.date_of_transfer as seperatedate,  a.staffid, b.desname as sepdesig, a.new_designation as sepdesigid, c.emp_code, CONCAT(IFNULL(c.permanenthno,''), IFNULL(c.permanentstreet,''), IFNULL(c.permanentcity,''),IFNULL(c.permanentdistrict,'') , IFNULL(s.name,''),'-',IFNULL(c.permanentpincode,'')) as address, st.date_of_transfer as joiningdate, d.desname as joindesig, a.trans_flag, a.reason, a.createdon,a.trans_status as process_status,a.date_of_transfer, lp.officename as newoffice, st.trans_status, dc.dc_cd, msdc.dc_name  FROM staff_transaction as a
         INNER JOIN `staff` as c on a.staffid = c.staffid and (a.`trans_status` ='Termination' OR a.`trans_status` ='Resign' OR a.`trans_status` ='Termination during Probation' OR a.`trans_status` ='Death' OR a.`trans_status` ='Retirement' OR a.`trans_status` ='Discharge simpliciter/ Dismiss' OR a.`trans_status` ='Desertion cases' OR a.`trans_status` ='Premature retirement' OR a.`trans_status` ='Super Annuation')
         left join `state` as s ON s.id = c.permanentstateid
         inner join staff_transaction as st
          on st.staffid = c.staffid and st.trans_status = 'JOIN'
         left JOIN `msdesignation` as b on a.new_designation = b.desid
         left JOIN `msdesignation` as d on st.new_designation = d.desid
         LEFT JOIN `lpooffice` as lp on a.new_office_id = lp.officeid 
         LEFT JOIN `dc_team_mapping` as dc on c.new_office_id = dc.teamid 
    LEFT JOIN  `msdc_details` as msdc ON dc.dc_cd = msdc.dc_cd
       WHERE a.id = $token";
       
    // echo $sql; die;

       //$query=$this->db->get();
       return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}



 
}