<?php 

/**
* Staff review Model
*/
class Staff_leave_approval_personnel_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    
  }


  public function get_empid($token)
  {
      try{

       $sql = "SELECT emp_code FROM staff  inner join tbl_workflowdetail on staff.staffid=tbl_workflowdetail.staffid WHERE tbl_workflowdetail.receiver ='$token' and type='5'"; 
     

        $res = $this->db->query($sql)->row();
        return $res;

         }catch(Exception $e){
            print_r($e->getMessage());die();
        }

  }


public function getrequestedleave($emp_code)
  {
    try{           
      
$sql="SELECT
  lt.`From_date`,
  lt.`id`,
  lt.`To_date`,
  lt.`Noofdays`,
  md.desname AS sender_des,
  SUBSTRING(st.name, 1, LOCATE(' ',st.name)) AS name,
  st.staffid,
  lp.officename,
  d.name as districtname,
  s.name as statename,
  st.gender,
  st.marital_status,
  lt.`Emp_code`,
  lt.`reason`,
  l.`Ltypename`,
  case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2   
then 'Rejected' when lt.`status` = 3 then 'Approved' when lt.`status` = 4 then 'Reverse' end status,
lt.status as stage
FROM
  `trnleave_ledger` AS lt
INNER JOIN
  `msleavetype` AS l ON lt.`Leave_type` = l.`Ltypeid` 
INNER JOIN
  staff AS st ON lt.`Emp_code` = st.emp_code
INNER JOIN
  tbl_workflowdetail AS tw ON st.staffid = tw.staffid and lt.id=tw.r_id and lt.status = tw.flag
 INNER JOIN
  lpooffice as lp on st.new_office_id = lp.officeid
INNER JOIN
  msdesignation AS md ON st.designation = md.desid
LEFT JOIN 
state as s on  lp.state = s.statecode 
LEFT JOIN 
District as d on  lp.district = d.districtid
WHERE
  tw.`receiver` = ".$this->loginData->staffid." AND tw.type = 5
ORDER BY
  lt.appliedon DESC";

// echo $sql; die;
          return $res = $this->db->query($sql)->result();

        

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }



  public function getstaffname()
  {
    try{

    $currentdate = date('Y-m-d');


     $sql = "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.emp_code,
    staff.candidateid,
    staff_transaction.staffid,
    staff_transaction.id,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    msdesignation.level_name AS levelname,
    staff.joiningandinduction,
    staff.gender,
    (
        CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
    END
) AS gender,
staff.status,
(
    CASE WHEN flag = 0 THEN 'Save' WHEN flag = 1 THEN 'Submitted' WHEN flag = 4 THEN 'Approved' WHEN flag = 3 THEN 'Rejected' ELSE 'Not status here'
END
) AS flag
FROM
    staff_transaction
INNER JOIN(
    SELECT staff_transaction.staffid,
        staff_transaction.date_of_transfer,
        MAX(
            staff_transaction.date_of_transfer
        ) AS MaxDate
    FROM
        staff_transaction
    GROUP BY
        staffid
) AS TMax
ON
    `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate
INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`
INNER JOIN lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
INNER JOIN msdesignation ON staff.`designation` = msdesignation.`desid`
WHERE 
staff_transaction.trans_status = CASE WHEN staff_transaction.trans_status = 'Midtermreview' then 'Midtermreview' else 'JOIN' END AND  trans_flag=1 AND 
    msdesignation.`level_name` = 'level-4' AND '".$currentdate."' <= CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date END";  //die;

    // if ($this->loginData->RoleID == 2) {
    //   $sql .= "WHERE staff.`reportingto` =".$this->loginData->staffid;  
    // }
 // $sql .= " GROUP BY `staff_transaction`.staffid  order by cast(staff.emp_code as UNSIGNED)";  
     // echo $sql;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

public function getEDid()
{

  try{

    $sql = "SELECT mstuser.staffid as edid,mstuser.UserFirstName,mstuser.UserMiddleName,mstuser.UserLastName  FROM mstuser LEFT JOIN staff as s on mstuser.staffid=s.staffid  
    WHERE s.designation = 2 AND mstuser.RoleID= 18 AND mstuser.IsDeleted = 0 AND s.status =1";

    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}


public function requestedleave()
  {
    try{
           
           $sql = "SELECT lt.`From_date`, lt.`To_date`, lt.`Noofdays`, case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2  or lt.`status` = 4  then 'Rejected' when lt.`status` = 3 or  lt.`status` = 5 then 'Approved' when lt.`status` = 6 then 'Reverse' end status, lt.`appliedon`, lt.`reason`, l.`Ltypename`, lt.status as stage FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` WHERE  lt.`Leave_transactiontype` ='DR' AND  lt.`Emp_code`=".$emp_code." ORDER BY appliedon DESC"; 

          return $res = $this->db->query($sql)->row();

        

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }




    public function fetchdatas($token)
    { 

      try{
        $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
        $result = $this->db->query($sql);
        $rows = $result->num_rows();
        $tm='-1';
         if($rows==0)
         {
           return $tm;
         }
         else
         {
             
           $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
        
            $query=$this->db->query($sql);
         }

         }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
     //  

    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_seperation_detail()
    {
       try
   {

      $sql="SELECT c.name, k.name as sendername, g.name as receivername,b.senddate as requestdate,b.type, l.process_type,a.date_of_transfer as seperatedate, a.staffid, a.id as transid, b.flag,

      (
      CASE a.trans_flag WHEN 1 THEN 'Request submitted' WHEN 4 THEN 'Request Approved' WHEN 3 THEN 'Request Rejected' ELSE 'Other' END
      ) AS status

        FROM staff_transaction as a 
        INNER JOIN `tbl_workflowdetail` as b on a.id = b.r_id
        INNER JOIN `staff` as c on a.staffid = c.staffid
        INNER JOIN `staff` as k on b.sender = k.staffid
        INNER JOIN `staff` as g on b.receiver = g.staffid
        INNER JOIN `mst_workflow_process` as l on b.type = l.id
        WHERE b.receiver = ".$this->loginData->staffid."  ORDER BY c.name ASC";

        //echo $sql; die;
      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }



public function get_providentworkflowid($staffid)
{

  try{

    $sql = "SELECT max(`workflowid`) as workflow_id FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=5 and staff.staffid=$staffid";
    
// echo $sql; die;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function personal_email()
  {
    
    try{

        $sql = "SELECT  * FROM  mstuser WHERE  RoleID=17 and IsDeleted='0'";
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}