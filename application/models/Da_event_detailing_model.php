<?php 

/**
* Da_event_detailing_model Model
*/
class Da_event_detailing_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}
 /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

/**
	 * Method getBatch() get all Batch .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getBatch()
	{
		
		try{
			

			$sql = "SELECT * FROM `mstbatch` Where status=0 AND isdeleted=0";

	        $res = $this->db->query($sql)->result();

	        return $res;

			// $this->db->select('*');
			// $this->db->where('isdeleted','0');
			// $this->db->order_by('id','asc');
			// $result =  $this->db->get('mstbatch')->result(); 
			// // echo $this->db->last_query(); die;
			//  return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

/**
	 * Method getCountDatrancation() get all Batch .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCountDatrancation($id)
	{
		
		try{
			

			$sql = "select count(id) as dacount from tbl_da_event_detailing_transaction WHERE da_event_detailing =$id ";

	        $res = $this->db->query($sql)->result()[0];

	        return $res;

			

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}



/**
	 * Method getCountDatrancation() get all Batch .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDaTransaction($id)
	{
		
		try{
			

			$sql = "SELECT a.`id`,a.`da_event_detailing`,a.`candidateid`,a.`fromdate`,a.`todate` FROM `tbl_da_event_detailing_transaction` as a
                 WHERE a.da_event_detailing =$id ";

	        $res = $this->db->query($sql)->result();

	        return $res;

			

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

/**
	 * Method getDAEventDetails() get all Da Event details .
	 * @access	public
	 * @param	
	 * @return	array
	 */

function getDAEventDetails(){

  try{
     $sql = "SELECT 
  `tbl_da_event_detailing`.id, 
  `mstbatch`.id as mstbatchid,
  `mstbatch`.batch, 
  `mstbatch`.financial_year,
  `mstphase`.id as mstphaseid,  
  `mstphase`.phase_name, 
  `mstfinancialyear`.financialyear 
   
FROM 
  `tbl_da_event_detailing` 
  LEFT join `mstbatch` on `tbl_da_event_detailing`.batchid = `mstbatch`.id 
  LEFT join `mstphase` on `tbl_da_event_detailing`.phaseid = `mstphase`.id 
  LEFT join `mstfinancialyear` on `tbl_da_event_detailing`.financial_year = `mstfinancialyear`.id 
WHERE 
  `tbl_da_event_detailing`.`isdeleted` = '0' 
 GROUP BY `mstphase`.phase_name"; //die;
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}

/**
	 * Method getDAEventDetailsTransaction() get all Da Event transation details .
	 * @access	public
	 * @param	
	 * @return	array
	 */

function getDAEventDetailsTransaction($id){

  try{
    $sql = "SELECT 
    `tbl_candidate_registration`.`candidatefirstname`,
    `tbl_candidate_registration`.`candidatemiddlename`,
    `tbl_candidate_registration`.`candidatelastname`,
    `tbl_candidate_registration`.`emailid`,
    `tbl_da_event_detailing_transaction`.`fromdate`,
    `tbl_da_event_detailing_transaction`.`todate`,
    mstbatch.batch,
    mstfinancialyear.financialyear,
    `mstcampus`.`campusname`,
    (case when `tbl_candidate_registration`.gender = 1 then 'Male' when `tbl_candidate_registration`.gender = 2 then 'Female' end) as gender
    FROM 
    tbl_da_event_detailing_transaction
    LEFT join `tbl_candidate_registration` on `tbl_candidate_registration`.candidateid = `tbl_da_event_detailing_transaction`.candidateid 
    LEFT join `mstcampus` on `tbl_candidate_registration`.campusid = `mstcampus`.campusid
    INNER join tbl_da_event_detailing on tbl_da_event_detailing.id=tbl_da_event_detailing_transaction.da_event_detailing
    LEFT join `mstbatch` on `tbl_da_event_detailing`.batchid = `mstbatch`.id 
    LEFT join `mstfinancialyear` on `tbl_da_event_detailing`.financial_year = `mstfinancialyear`.id
    WHERE `tbl_da_event_detailing`.phaseid = ".$id." AND 
    `tbl_da_event_detailing_transaction`.`isdeleted` = '0' "; //die;
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}


/**
	 * Method getDADetails() get all Da details .
	 * @access	public
	 * @param	
	 * @return	array
	 */

function getDADetails($id){

  try{
      $sql = "SELECT 
      candidatefirstname,
    candidatemiddlename,
    candidatelastname,
    emailid
FROM 
  tbl_candidate_registration
WHERE tbl_candidate_registration.candidateid = ".$id." "; //die;
    $res = $this->db->query($sql)->row();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}
	



}