<?php 

/**
* Master  Model
*/
class Leaveclosing_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{

	}

	public function getleavedetails($emp_code)
	{
		try{

      $sql = "SELECT l.`Ltypename` FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` WHERE  lt.`Leave_transactiontype` ='DR' AND  lt.`Emp_code`=".$emp_code;  

      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

/*
    function getleavectegory is used to get ed id
    result row;
    created by pooja
*/

 public function getleavectegory()
{

 $LoginData = $this->session->userdata('login_data');

 $query="SELECT b.gender FROM mstuser a inner JOIN staff b ON a.staffid=b.staffid where a.staffid=? GROUP by b.gender";
 $gender_details = $this->db->query($query,[$LoginData->staffid])->row();
       //  print_r($gender_details); die;

 if ($gender_details->gender == 1) {
  $sql = "select 	Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 5 AND 11";
  $res = $this->db->query($sql)->result();
}else if ($gender_details->gender == 2) {
  $sql = "select  Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 3 AND 10";
  $res = $this->db->query($sql)->result();
            // print_r($res); die();
}else if ($gender_details->gender == 3) {
 $sql = "select  Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 3 AND 11";
 $res = $this->db->query($sql)->result();

}
return $res;


}


/*
    function getleavedetail_balance() is get leave Opning Balance, Accured, Avalied, Balance 
    result row;
    created by pooja
*/


public function getleavedetail_balance($fromdate, $todate)
{
  try{

    $expdate  = explode('-', $todate);
     //print_r($expdate);
     $Y = date($expdate[0])-1;
     $lapfromdate = $Y.'-04-01';
   
    $sql = " SELECT staff.emp_code, 'General' AS Ltypename, staff.Name, EXP.Date_of_transfer doj, desname, lpo.officename, CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE CAST( DATEDIFF('".$todate."', EXP.Date_of_transfer) / 365 as decimal(5,1)) END AS noofyearexp,CAST(( CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE  DATEDIFF('".$todate."', EXP.Date_of_transfer) END - IFNULL(lwp.LWP, 0))/ 365  as decimal(5,1)) ndays,  IFNULL(lwp.LWP, 0) LWP, IFNULL(totalavailed.tavailed, 0) tavailed, IFNULL(trnleave_ledger.OB, 0) OB, IFNULL(trnleave_ledger.Accured, 0) Accured, IFNULL(trnleave_ledger.DLWP, 0) DLWP, IFNULL(trnleave_ledger.Availed, 0) Availed, ( IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) ) Balance, CASE WHEN CAST(( CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE  DATEDIFF('".$todate."', EXP.Date_of_transfer) END - IFNULL(lwp.LWP, 0))/ 365  as decimal(5,1)) > 10 AND( IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) - IFNULL(trnleave_ledger.DLWP, 0) ) > 348 THEN 348 WHEN CAST(( CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE  DATEDIFF('".$todate."', EXP.Date_of_transfer) END - IFNULL(lwp.LWP, 0))/ 365  as decimal(5,1)) <= 10 AND (IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) - IFNULL(trnleave_ledger.DLWP, 0) ) > 300 THEN 300 ELSE( IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) - IFNULL(trnleave_ledger.DLWP, 0) ) END AS ClosingBalance FROM ( SELECT `trnleave_ledger`.`Emp_code`, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'OB' AND Leave_type = 0 AND From_date = '".$fromdate."' THEN Noofdays ELSE 0 END, 0 ) ) OB, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'CR' AND Leave_type = 0 THEN Noofdays ELSE 0 END, 0 ) ) Accured, SUM( IFNULL( tl.nofdays, 0 ) ) DLWP, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 9 AND STATUS = 3 THEN Noofdays ELSE 0 END, 0 ) ) Availed FROM `trnleave_ledger` LEFT JOIN (SELECT SUM(Noofdays) as nofdays,Emp_code,status as sid FROM `trnleave_ledger` WHERE Leave_transactiontype = 'CR' AND Leave_type = 100 AND Description = 'DLWP' GROUP BY Emp_code, status ) as tl on `trnleave_ledger`.`Emp_code` = tl.`Emp_code` And `trnleave_ledger`.id = tl.sid And `trnleave_ledger`.status = 3 WHERE From_date >= '".$fromdate."' AND to_date <= '".$todate."' GROUP BY `trnleave_ledger`.Emp_code ) AS trnleave_ledger INNER JOIN staff ON trnleave_ledger.Emp_code = staff.emp_code 
      LEFT JOIN (SELECT  Emp_code, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 5 AND STATUS = 3 THEN Noofdays ELSE 0 END, 0 ) ) LWP from `trnleave_ledger` where  to_date <= '".$todate."' GROUP BY `trnleave_ledger`.Emp_code) lwp on lwp.Emp_code = staff.emp_code
       LEFT JOIN (SELECT  Emp_code, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 9 AND STATUS = 3 THEN Noofdays ELSE 0 END, 0 ) ) tavailed from `trnleave_ledger` where  From_date >= '".$lapfromdate."' AND to_date <= '".$todate."' GROUP BY `trnleave_ledger`.Emp_code) totalavailed on totalavailed.Emp_code = staff.emp_code

      LEFT JOIN `msdesignation` AS msd ON staff.designation = msd.desid INNER JOIN `lpooffice` AS lpo ON staff.new_office_id = lpo.officeid LEFT JOIN ( SELECT staff.Staffid, case when st1.Date_of_transfer is NULL then st.Date_of_transfer else st1.Date_of_transfer end Date_of_transfer FROM `staff` INNER JOIN staff_transaction st ON staff.staffid = st.staffid AND st.new_designation != 13 AND st.trans_status = 'JOIN' lEFT JOIN staff_transaction_old as st1 on st.staffid = st1.staffid AND st1.new_designation != 13 AND st1.trans_status = 'JOIN' ) EXP ON staff.staffid = EXP.staffid WHERE staff.status = 1 GROUP BY staff.emp_code, staff.Name ORDER BY staff.emp_code ";

  //   $sql = "SELECT
  // staff.emp_code,
  // 'General' AS Ltypename,
  // staff.Name,
  // EXP.Date_of_transfer doj,
  // desname,
  // lpo.officename,
  // CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE FLOOR(
  //   DATEDIFF(CURDATE(),
  //   EXP.Date_of_transfer) / 365) END AS noofyearexp,
  //   IFNULL(trnleave_ledger.OB,
  //   0) OB,
  //   IFNULL(trnleave_ledger.Accured,
  //   0) Accured,
  //  IFNULL(trnleave_ledger.DLWP,
  //   0) DLWP,
  //   IFNULL(trnleave_ledger.Availed,
  //   0) Availed,
  //   (
  //     IFNULL(trnleave_ledger.OB,
  //     0) + IFNULL(trnleave_ledger.Accured,
  //     0) - IFNULL(trnleave_ledger.Availed,
  //     0)
  //   ) Balance,
  //   CASE WHEN FLOOR(
  //     DATEDIFF(CURDATE(),
  //     EXP.Date_of_transfer) / 365) > 10 AND(
  //       IFNULL(trnleave_ledger.OB,
  //       0) + IFNULL(trnleave_ledger.Accured,
  //       0) - IFNULL(trnleave_ledger.Availed,
  //       0) - IFNULL(trnleave_ledger.DLWP,
  //       0)
  //     ) > 348 THEN 348 WHEN FLOOR(
  //       DATEDIFF(CURDATE(),
  //       EXP.Date_of_transfer) / 365) <= 10 AND(
  //         IFNULL(trnleave_ledger.OB,
  //         0) + IFNULL(trnleave_ledger.Accured,
  //         0) - IFNULL(trnleave_ledger.Availed,
  //         0) - IFNULL(trnleave_ledger.DLWP,
  //         0)
  //       ) > 300 THEN 300 ELSE(
  //         IFNULL(trnleave_ledger.OB,
  //         0) + IFNULL(trnleave_ledger.Accured,
  //         0) - IFNULL(trnleave_ledger.Availed,
  //         0) - IFNULL(trnleave_ledger.DLWP,
  //         0)
  //       ) END AS ClosingBalance
  //     FROM
  //       (
  //       SELECT
  //        `trnleave_ledger`.`Emp_code`,
  //         SUM(
  //           IFNULL(
  //             CASE WHEN Leave_transactiontype = 'OB' AND Leave_type = 0 AND From_date = '".$fromdate."' THEN Noofdays ELSE 0 END,
  //             0
  //           )
  //         ) OB,
  //         SUM(
  //           IFNULL(
  //             CASE WHEN Leave_transactiontype = 'CR' AND Leave_type = 0 THEN Noofdays ELSE 0 END,
  //             0
  //           )
  //         ) Accured,
  //         SUM(
  //           IFNULL(
  //            tl.nofdays, 
  //             0
  //           )
  //         ) DLWP,
  //         SUM(
  //           IFNULL(
  //             CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 9 AND STATUS = 3 THEN Noofdays ELSE 0 END,
  //             0
  //           )
  //         ) Availed,
  //          SUM(
  //           IFNULL(
  //             CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 5 AND STATUS = 3 THEN Noofdays ELSE 0 END,
  //             0
  //           )
  //         ) LWP
  //       FROM
  //         `trnleave_ledger`
  //           LEFT JOIN (SELECT SUM(Noofdays) as nofdays,Emp_code,status as sid FROM  `trnleave_ledger` WHERE Leave_transactiontype = 'CR' AND Leave_type = 100 AND Description = 'DLWP'  GROUP BY Emp_code, status ) as tl on `trnleave_ledger`.`Emp_code` = tl.`Emp_code` And `trnleave_ledger`.id = tl.sid And `trnleave_ledger`.status = 3
  //       WHERE
  //         From_date >= '".$fromdate."' AND to_date <= '".$todate."'
  //       GROUP BY
  //         `trnleave_ledger`.Emp_code
  //     ) AS trnleave_ledger
  //   INNER JOIN
  //     staff ON trnleave_ledger.Emp_code = staff.emp_code
  //   LEFT JOIN
  //     `msdesignation` AS msd ON staff.designation = msd.desid
  //   INNER JOIN
  //     `lpooffice` AS lpo ON staff.new_office_id = lpo.officeid
  //   LEFT JOIN
  //     (
  //     SELECT
  //       staff.Staffid,
  //       case when st1.Date_of_transfer is NULL then st.Date_of_transfer else st1.Date_of_transfer end Date_of_transfer
  //     FROM
  //       `staff`
  //     INNER JOIN
  //       staff_transaction st ON staff.staffid = st.staffid AND st.new_designation != 13 AND st.trans_status = 'JOIN'
  //       lEFT JOIN staff_transaction_old as st1 on st.staffid = st1.staffid AND st1.new_designation != 13 AND st1.trans_status = 'JOIN' 
  //   ) EXP ON staff.staffid = EXP.staffid
  // WHERE
  //   staff.status = 1
  // GROUP BY
  //   staff.emp_code,
  //   staff.Name
  // ORDER BY
  //   staff.emp_code";

 //echo  $sql; die;
   $res = $this->db->query($sql)->result();

   return $res;

 }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


/*
    function getleavedetail_balance() is get leave Opning Balance, Accured, Avalied, Balance 
    result row;
    created by pooja
*/

public function checkNextperiodopningbalance($currentyear){
  try{
    //  if (date('m')>=1 && date('m') <=3) {

    //   $preyear = date('Y')+1;
    //   echo $fromdate = $preyear.'-04-01';
    //  echo  $todate = date('Y').'-03-31';

    // }else{
    //    $nextyear = date('Y')+1;
    //    $fromdate  = $preyear.'-04-01';
    //    $todate    = $nextyear.'-03-31';
    // }

    



    foreach ($userdata as $value) {

        if ($value->noofyearexp > 10) {
          //pass max leave 340,

         
        }elseif ($value->noofyearexp <= 10) {
          //pass max leave 300,


        }


     $sql = "select Noofdays, id from trnleave_ledger where From_date='".$From_date."' AND  Emp_code=".$value->emp_code." AND Leave_type = 0 AND Leave_transactiontype ='CR' AND Description='Credit leaves posting' " ;
      $res = $this->db->query($sql)->row();
     // print_r($res); die;
      if(count($res) > 0){  
       $transtd =  $res->id;
        $updatearray = array(
          'Noofdays'=> $Accuredvalue
        );
        $this->db->where('id',$transtd);
        $this->db->update('trnleave_ledger', $updatearray);
      }else{
        $insertarray = array(
          'Emp_code'=> $value->emp_code,
          'From_date'=> $From_date,
          'To_date'=> $TO_date,
          'Leave_type'=> 0,
          'Leave_transactiontype'=> 'CR',
          'Description' => 'Credit leaves posting',
          'Noofdays'=> $Accuredvalue,
          'appliedon'=> date('Y-m-d')
        );
      
      $this->db->insert('trnleave_ledger', $insertarray);

      }
    }
   
    return true;
    }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
  
}




public function getCurrentfyearperiod($fyear){

try{

 $sql = "SELECT id,`monthfroms`,`monthtos` FROM `msleavecrperiod` WHERE `CYear`='".$fyear."' ORDER BY `from_month` ASC"; 
 return $res = $this->db->query($sql)->result();

 }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

}