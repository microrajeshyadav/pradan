<?php 

/**
* Clinical Due List Model
*/
class Referred_due_list_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
		$this->loggedIn = $this->session->userdata('USERID');
		$this->loggedDate   = date("Y-m-d H:i:s"); 
	}

	public function index()
	{
		
	}

	public function edit($token)
	{
		try{
		 
		$RequestMethod = $this->input->server("REQUEST_METHOD");
	
		if($RequestMethod == 'POST'){

          $PatientReferredFollowUpGUID = $this->input->post('PatientReferredFollowUpGUID');

			$this->db->where('PatientReferredFollowUpGUID', $PatientReferredFollowUpGUID);
			$result = $this->db->get('tblpatientreffrredfollowup')->result();
			
			if (count($result) == 1) {

			$updateArray = array(		
				"VisitDate"     				=>  (trim($this->input->post('VisitDate'))),
				"UserID"		 		  		=>  (trim($this->input->post('CT_Done'))==""?NULL:$this->input->post('UserID')),
				"ORWID"				   			=>  (trim($this->input->post('ORWID'))==""?NULL:$this->input->post('ORWID')),
				"AshaID"			  			=>	(trim($this->input->post('AshaID'))==""?NULL:$this->input->post('AshaID')),	
				"VillageCode"					=>  (trim($this->input->post('VillageCode'))==""?NULL:$this->input->post('VillageCode')),
				"Available" 					=>  (trim($this->input->post('Available'))),
				"CT_Done"				    	=>	(trim($this->input->post('CT_Done'))==""?NULL:$this->input->post('CT_Done')),
				"CT_Date"   		  			=>  (trim($this->input->post('CT_Date'))==""?NULL:$this->input->post('CT_Date')),
				"CT_Place" 						=>  (trim($this->input->post('CT_Place'))==""?NULL:$this->input->post('CT_Place')),
				"CT_Type"		  				=>  (trim($this->input->post('CT_Type'))==""?NULL:$this->input->post('CT_Type')),
				"DC" 							=>  (trim($this->input->post('DC'))),
				"DC_Date"			   			=>	(trim($this->input->post('DC_Date'))==""?NULL:$this->input->post('DC_Date')),
				"DC_Place"     					=>  (trim($this->input->post('DC_Place'))==""?NULL:$this->input->post('DC_Place')),
				"DC_Daignosis"		 		  	=>  (trim($this->input->post('DC_Daignosis'))==""?NULL:$this->input->post('DC_Daignosis')),
				"DC_Advice"				  		=>  (trim($this->input->post('DC_Advice'))==""?NULL:$this->input->post('DC_Advice')),
				"NextVisitDate"			   		=>  (trim($this->input->post('NextVisitDate'))==""?NULL:$this->input->post('NextVisitDate')),	
				"CounsellingID"		 			=>  (trim($this->input->post('CounsellingID'))==""?NULL:$this->input->post('CounsellingID')),
				"facilityName" 					=>  (trim($this->input->post('facilityName'))==""?NULL:$this->input->post('facilityName')),
				"RelativeAvailable"				=>  (trim($this->input->post('RelativeAvailable'))),
				"InfoConfirmatoryTest"   		=>  (trim($this->input->post('InfoConfirmatoryTest'))),
				"CheckedDocument" 				=>  (trim($this->input->post('CheckedDocument'))==""?NULL:$this->input->post('CheckedDocument')),
				"TypeofTest"		  			=>  (trim($this->input->post('TypeofTest'))==""?NULL:$this->input->post('TypeofTest')),
				"EnterClinicalValue"			=>  (trim($this->input->post('EnterClinicalValue'))),
				"DiabetesValue"					=>	(trim($this->input->post('DiabetesValue'))==""?NULL:$this->input->post('DiabetesValue')),
				"BPSBPValue"     				=>  (trim($this->input->post('BPSBPValue'))==""?NULL:$this->input->post('BPSBPValue')),
				"BPDBPValue"		 		  	=>  (trim($this->input->post('BPDBPValue'))==""?NULL:$this->input->post('BPDBPValue')),
				"TimetoVisit"				   	=>  (trim($this->input->post('TimetoVisit'))==""?NULL:$this->input->post('TimetoVisit')),
				"AvgTimeforTest"			   	=>	(trim($this->input->post('AvgTimeforTest'))==""?NULL:$this->input->post('AvgTimeforTest')),	
				"TimeWithDr"		 			=>  (trim($this->input->post('TimeWithDr'))==""?NULL:$this->input->post('TimeWithDr')),
				"Satisfied" 					=>  (trim($this->input->post('Satisfied'))),
				"Recommend"				    	=>  (trim($this->input->post('Recommend'))),
				"Suggestion"   		  			=>  (trim($this->input->post('Suggestion'))),
				"NotSatisfied_Reason" 			=>  (trim($this->input->post('NotSatisfied_Reason'))==""?NULL:$this->input->post('NotSatisfied_Reason')),
				"SpecifyReason"		  			=>  (trim($this->input->post('SpecifyReason'))==""?NULL:$this->input->post('<SpecifyReason></SpecifyReason>')),
				"ORW_NextVisitDate" 			=>  (trim($this->input->post('ORW_NextVisitDate'))),
				"Test_NotDone"					=>	(trim($this->input->post('Test_NotDone'))==""?NULL:$this->input->post('Test_NotDone')),
				"Test_NotDone_Reason"			=>	(trim($this->input->post('Test_NotDone_Reason'))==""?NULL:$this->input->post('Test_NotDone_Reason')),
				);
   //print_r($updateArray); //die();
			$this->db->where("PatientReferredFollowUpGUID",$PatientReferredFollowUpGUID);
			$udaterefrred =	$this->db->update('tblpatientreffrredfollowup', $updateArray);

				if($udaterefrred ==true){
					return 1;
					$this->session->set_flashdata('tr_msg', 'Successfully Modified Referred Test Details');
				}else{
					return -1;
					$this->session->set_flashdata('tr_msg', 'Not Referred Test Details');
				}

			}else{

				$InsertArray = array(		
				"VisitDate"     				=>  (trim($this->input->post('VisitDate'))),
				"UserID"		 		  		=>  (trim($this->input->post('CT_Done'))==""?NULL:$this->input->post('UserID')),
				"ORWID"				   			=>  (trim($this->input->post('ORWID'))==""?NULL:$this->input->post('ORWID')),
				"AshaID"			  			=>	(trim($this->input->post('AshaID'))==""?NULL:$this->input->post('AshaID')),	
				"VillageCode"					=>  (trim($this->input->post('VillageCode'))==""?NULL:$this->input->post('VillageCode')),
				"Available" 					=>  (trim($this->input->post('Available'))),
				"CT_Done"				    	=>	(trim($this->input->post('CT_Done'))==""?NULL:$this->input->post('CT_Done')),
				"CT_Date"   		  			=>  (trim($this->input->post('CT_Date'))==""?NULL:$this->input->post('CT_Date')),
				"CT_Place" 						=>  (trim($this->input->post('CT_Place'))==""?NULL:$this->input->post('CT_Place')),
				"CT_Type"		  				=>  (trim($this->input->post('CT_Type'))==""?NULL:$this->input->post('CT_Type')),
				"DC" 							=>  (trim($this->input->post('DC'))),
				"DC_Date"			   			=>	(trim($this->input->post('DC_Date'))==""?NULL:$this->input->post('DC_Date')),
				"DC_Place"     					=>  (trim($this->input->post('DC_Place'))==""?NULL:$this->input->post('DC_Place')),
				"DC_Daignosis"		 		  	=>  (trim($this->input->post('DC_Daignosis'))==""?NULL:$this->input->post('DC_Daignosis')),
				"DC_Advice"				  		=>  (trim($this->input->post('DC_Advice'))==""?NULL:$this->input->post('DC_Advice')),
				"NextVisitDate"			   		=>  (trim($this->input->post('NextVisitDate'))==""?NULL:$this->input->post('NextVisitDate')),	
				"CounsellingID"		 			=>  (trim($this->input->post('CounsellingID'))==""?NULL:$this->input->post('CounsellingID')),
				"facilityName" 					=>  (trim($this->input->post('facilityName'))==""?NULL:$this->input->post('facilityName')),
				"RelativeAvailable"				=>  (trim($this->input->post('RelativeAvailable'))),
				"InfoConfirmatoryTest"   		=>  (trim($this->input->post('InfoConfirmatoryTest'))),
				"CheckedDocument" 				=>  (trim($this->input->post('CheckedDocument'))==""?NULL:$this->input->post('CheckedDocument')),
				"TypeofTest"		  			=>  (trim($this->input->post('TypeofTest'))==""?NULL:$this->input->post('TypeofTest')),
				"EnterClinicalValue"			=>  (trim($this->input->post('EnterClinicalValue'))),
				"DiabetesValue"					=>	(trim($this->input->post('DiabetesValue'))==""?NULL:$this->input->post('DiabetesValue')),
				"BPSBPValue"     				=>  (trim($this->input->post('BPSBPValue'))==""?NULL:$this->input->post('BPSBPValue')),
				"BPDBPValue"		 		  	=>  (trim($this->input->post('BPDBPValue'))==""?NULL:$this->input->post('BPDBPValue')),
				"TimetoVisit"				   	=>  (trim($this->input->post('TimetoVisit'))==""?NULL:$this->input->post('TimetoVisit')),
				"AvgTimeforTest"			   	=>	(trim($this->input->post('AvgTimeforTest'))==""?NULL:$this->input->post('AvgTimeforTest')),	
				"TimeWithDr"		 			=>  (trim($this->input->post('TimeWithDr'))==""?NULL:$this->input->post('TimeWithDr')),
				"Satisfied" 					=>  (trim($this->input->post('Satisfied'))),
				"Recommend"				    	=>  (trim($this->input->post('Recommend'))),
				"Suggestion"   		  			=>  (trim($this->input->post('Suggestion'))),
				"NotSatisfied_Reason" 			=>  (trim($this->input->post('NotSatisfied_Reason'))==""?NULL:$this->input->post('NotSatisfied_Reason')),
				"SpecifyReason"		  			=>  (trim($this->input->post('SpecifyReason'))==""?NULL:$this->input->post('<SpecifyReason></SpecifyReason>')),
				"ORW_NextVisitDate" 			=>  (trim($this->input->post('ORW_NextVisitDate'))),
				"Test_NotDone"					=>	(trim($this->input->post('Test_NotDone'))==""?NULL:$this->input->post('Test_NotDone')),
				"Test_NotDone_Reason"			=>	(trim($this->input->post('Test_NotDone_Reason'))==""?NULL:$this->input->post('Test_NotDone_Reason')),

				);
   //print_r($updateArray); //die();
			//$this->db->where("PatientReferredFollowUpGUID",$token);
				$Insertrefrred =	$this->db->insert('tblpatientreffrredfollowup', $InsertArray);
				if($Insertrefrred ==true){
					return 1;
					$this->session->set_flashdata('tr_msg', 'Successfully Modified Referred Test Details');
				}else{
					return -1;
					$this->session->set_flashdata('tr_msg', 'Not Referred Test Details');
				}
			}

				//echo $this->db->last_query(); die();
			
		}
		}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
		
	}

	/**
	 * Method getPatientRegistrationDetails() get all Patient Registration Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	// public function getPatientRegistrationDetails()
	// {
		
	// 	try{
	// 		$query = "select pc.VisitNo,prd.Name,prd.Age,prd.Gender,prd.PatientGUID,pc.PatientReferredFollowUpGUID from tblpatientregistrationdetails prd left join tblpatientreffrredfollowup pc on prd.PatientGUID = pc.PatientGUID and prd.IsDeleted =0 GROUP BY pc.PatientGUID";
	// 		$result = $this->db->query($query)->result();
	// 		return $result;
		
	// 	}catch (Exception $e) {
	// 		print_r($e->getMessage());die;
	// 	}
		
	// }


// /**
// 	 * Method getReferredDueList() get all Patient Registration Details .
// 	 * @access	public
// 	 * @param	
// 	 * @return	array
// 	 */
// 	public function getReferredDueList($token=NULL)
// 	{
// 		try{
// 		$query = " SELECT prgr.Name, prgr.ReferralSlipNo,ptrf.*  FROM tblpatientreffrredfollowup as ptrf 
// INNER JOIN tblpatientregistrationdetails as prgr ON prgr.PatientGUID = ptrf.PatientGUID WHERE ptrf.`PatientReferredFollowUpGUID` = '".$token."'";
// 		$result = $this->db->query($query)->result()[0];
// 		return $result;
// 		}catch (Exception $e) {
// 			print_r($e->getMessage());die;
// 		}
		
// 	}


	/**
	 * Method getDiagnosis() get all Diagnosis .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDiagnosis()
	{
		
		try{

			$this->db->select('*');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('DiagnosisID','asc');
			return $this->db->get('mstdiagnosis')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


	/**
	 * Method getAdvice() get all Advice .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getAdvice()
	{
		try{
			
			$this->db->select('*');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('AdviceID','asc');
			return $this->db->get('mstadvice')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
		
	/**
	 * Method getCounselling() get all Advice .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCounselling()
	{
		try{
			
			$this->db->select('CounsellingID,Counselling');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('CounsellingID','asc');
			return $this->db->get('mstcounselling')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getCenter() get all Center .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getCenter()
	{
		try{
			
			$this->db->select('CenterID,Name');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('CenterID','asc');
			return $this->db->get('mstcenter')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	/**
	 * Method getReason() get all Center .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getReason()
	{
		try{
			
			$this->db->select('CenterUID,Reason');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('CenterUID','asc');
			return $this->db->get('mstreason')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	/**
	 * Method getDMTest() get all DM Test .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getDMTest()
	{
		try{
			
			$this->db->select('DMTestID,DMTest');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('DMTestID','asc');
			return $this->db->get('mstdmtest')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getClinicalTest() get all Clinical Test .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getClinicalTest()
	{
		try{
			
			$this->db->select('ClinicalTestID,ClinicalTest');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('ClinicalTestID','asc');
			return $this->db->get('mstclinicaltest')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getEvidence() get all Evidence Test .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getEvidence()
	{
		try{
			
			$this->db->select('CenterUID,Evidence');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('CenterUID','asc');
			return $this->db->get('mstevidence')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	/**
	 * Method getTestType() get all Type Test .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getTestType()
	{
		try{
			
			$this->db->select('TestTypeUID,TestType');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('TestTypeUID','asc');
			return $this->db->get('msttesttype')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	/**
	 * Method getNotSatisfieDreason() get all Type Test .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getNotSatisfieDreason()
	{
		try{
			
			$this->db->select('NotSatisfiedUID,NotSatisfied');
			$this->db->where('IsDeleted','0');
			$this->db->order_by('NotSatisfiedUID','asc');
			return $this->db->get('mstnotsatisfiedreason')->result();

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



}