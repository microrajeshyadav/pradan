<?php 
/**
* Proposed_probation_separation  Model
*/
class Proposed_probation_separation_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}



  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

public function getTeamlist()
  {
    try{

      $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No' ORDER BY `officename`";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


// getpersonnel detail for email.

public function getpersonneldetail(){
  $sql = "SELECT * FROM mstuser where RoleID=17 AND staffid != 0";
  return $this->db->query($sql)->row();
}


 /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
       try
   {

      $sql="SELECT
      staff.name,
      staff.emailid,
      staff.emp_code,
  staff_transaction.staffid,
  staff_transaction.trans_status,
  staff_transaction.date_of_transfer,
  staff_transaction.new_office_id,
  staff_transaction.new_designation,
  staff.reportingto,
  staff.candidateid,
  staff.doj_team,
  lpooffice.officename,
  lp_old.officename as oldofficename,
  msdesignation.desname,
  msdesignation.level,
  staff_transaction.date_of_transfer,
  staff_transaction.effective_date as releavingdate

FROM
  staff_transaction
INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
left join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
left join lpooffice as lp_old on staff_transaction.old_office_id=lp_old.officeid
left join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";
  // echo $sql;die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

    public function stateList()
    {
        try{

             $sql = "SELECT id,statecode,name FROM `state` WHERE `isdeleted`='0' ORDER BY `name` ";
             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

	public function get_Probation_Separation()
  {
    
    try{
     
    $sql = "SELECT st.emp_code, a.id, intm.intimation, a.comment,st.name,a.probation_completed,a.probation_completed_date, a.intemation_type, a.status, mstcampus.campusname, (CASE WHEN tb.gender =1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female' END) as gender FROM `tbl_hr_intemation` as a 
      Left join `staff` as st on st.staffid = a.staffid
      Left JOIN mst_intimation as intm on intm.id = a.intemation_type
      left join `tbl_da_personal_info` as `b` ON `b`.staffid =`a`.staffid
      LEFT JOIN `tbl_candidate_registration` as tb ON `b`.candidateid = `tb`.candidateid
      LEFT JOIN `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`
      Where `a`.intemation_type in(1,5) AND `a`.Isdeleted = 0";
        
        $res = $this->db->query($sql)->result();
      // print_r($res); die();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  public function get_Probation_Separation_approval()
  {
    
   try{

    $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  b.`trans_flag` as status,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  a.`createdon` AS Requestdate,
  g.id AS tbl_da_exit_interview_form_id,
  h.id AS tbl_da_clearance_certificate_id,
  h.hrid AS tbl_da_clearance_certificate_hrid,
  h.flag AS tbl_da_clearance_certificate_flag,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 4 THEN 'Accepted By Personnel Unit' 
    WHEN b.trans_flag = 5 THEN 'Accepted By TC/Intergrator' 
    ELSE 'Not status here'
  END
) AS flag
FROM 
  `staff_transaction` AS b 

LEFT JOIN `tbl_workflowdetail` AS a
   ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=27)
  
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `tbl_da_exit_interview_form` AS g ON g.`transid` = b.`id`
  LEFT JOIN
  `tbl_da_clearance_certificate` AS h ON h.`transid` = b.`id`
  Where 1=1 AND (b.`trans_status` ='Resign' || b.`trans_status` ='Facilitate To Leave') ";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
    //$sql .= " AND a.receiver =".$this->loginData->staffid;
  }

 // echo $sql;die();
   
$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function get_Probation_Separation_leave_approval()
  {
    
   try{

    $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  b.`trans_flag` as status,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  a.`createdon` AS Requestdate,
  g.id AS tbl_da_exit_interview_form_id,
  h.id AS tbl_da_clearance_certificate_id,
  h.flag AS tbl_da_clearance_certificate_flag,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 4 THEN 'Accepted By Personnel Unit' 
    WHEN b.trans_flag = 5 THEN 'Accepted By TC/Intergrator' 
    ELSE 'Not status here'
  END
) AS flag
FROM 
  `staff_transaction` AS b 

LEFT JOIN `tbl_workflowdetail` AS a
   ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=27)
  
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `tbl_da_exit_interview_form` AS g ON g.`transid` = b.`id`
  LEFT JOIN
  `tbl_da_clearance_certificate` AS h ON h.`transid` = b.`id`
  Where 1=1 AND b.`trans_status` ='Facilitate To Leave' ";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
    //$sql .= " AND a.receiver =".$this->loginData->staffid;
  }

 // echo $sql;die();
   
$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 /**
   * Method gettcemailid() to get reportingto TC of DA staffid and emailid 
   * @access  public
   * @param Null
   * @return  row
   created by rajat
   */

    public function gettcemailid($dastaffid)
    {
        try{
            // get reportingto 
             $sql = "SELECT reportingto FROM `staff` WHERE staffid=".$dastaffid;
             $res = $this->db->query($sql)->row();

             // reportingto detail
            if(!empty($res))
             $sql1 = "SELECT name,emailid FROM `staff` WHERE staffid=".$res->reportingto;
             $res1 = $this->db->query($sql1)->row();
            
             return $res1;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

  public function get_Recommended_to_graduate()
  {
    
    try{
     
    $sql = "SELECT a.*,s.name, s.emailid,s.emp_code, 
  g.transid AS tbl_da_exit_interview_form_id,
  h.id AS tbl_da_clearance_certificate_id,
  i.transid AS tbl_da_check_sheet_id,
  j.transid AS tbl_da_summary,
  `tb`.candidateid,
  (
    CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
  END
) AS gender
  
FROM
  `tbl_hr_intemation` AS a
LEFT join staff as s on a.staffid = s.staffid
LEFT join (select * from staff_transaction where trans_status = 'Recommended to graduate') as st on st.staffid=s.staffid

LEFT JOIN
  `tbl_da_exit_interview_graduating_form` AS g ON g.`transid` = st.`id`
LEFT JOIN
  `tbl_da_clearance_certificate` AS h ON h.`transid` = st.`id`
LEFT JOIN
  `tbl_da_check_sheet` AS i ON i.`transid` = st.`id`
LEFT JOIN
  (select * from tbl_da_summary where flag=1) j ON i.`transid` = st.`id`
  LEFT JOIN
  `tbl_candidate_registration` AS tb ON `tb`.candidateid = `s`.candidateid


Where `a`.intemation_type = 2  
AND `a`.status in (1,2)
AND `a`.Isdeleted = 0 and s.recommend_to_graduate=0 GROUP BY a.staffid";
        
        $res = $this->db->query($sql)->result();
       // echo $this->db->last_query();
      // print_r($res); die();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getPhaseActivities()
{
  $query = "SELECT * FROM `mstphase` where isdeleted=0 ORDER BY sorting_order";
  return $this->db->query($query)->result();
}


  public function getDASummary($transid)
  {
    try
    {
      /* Get candidated details */
      $sql = "select a.staffid, a.reportingto, b.doj_team, c.*, d.officename, 
              e.name as supervisername, f.batch, g.*,
              coalesce(h.id, 0) as offerletter, 
              i.id_summary, i.leave_entitled, i.leave_taken, i.date_apprectice_completion, i.date_executive_starting, i.flag, k.filename as general_nominationfile, l.filename as joining_reportfile, 
              j.name as fieldguidename, a.new_office_id
              FROM `staff_transaction` AS a
              inner join staff b on a.staffid=b.staffid
              inner join tbl_candidate_registration c on c.candidateid=b.candidateid
              inner join lpooffice d on d.officeid = a.new_office_id
              inner join staff e on a.reportingto = e.staffid
              left join mstbatch f on c.batchid= f.id
              left join tbl_verification_document g on g.candidateid=b.candidateid
              left join tbl_sendofferletter h on h.candidateid=b.candidateid
              left join tbl_da_summary i on i.candidateid=b.candidateid
              left join tbl_general_nomination_and_authorisation_form k on k.candidateid=b.candidateid
              left join tbl_joining_report l on l.candidateid=b.candidateid
              left join staff j on c.fgid=j.staffid
              where a.id=".$transid;   
              // echo $sql;  die();         
      $res = $this->db->query($sql)->result();

      if(!empty($res))
        $result['da_summary'] = $res[0];
      

      /* Get gap year details */

      $sql = "select d.*
            FROM `staff_transaction` AS a
            inner join staff b on a.staffid=b.staffid
            inner join tbl_candidate_registration c on c.candidateid=b.candidateid
            inner join tbl_gap_year d on d.candidateid=b.candidateid
            where a.id=".$transid.'';    

      $res = $this->db->query($sql)->result();
      
      if(!empty($res))
        $result['da_gap_summary'] = $res;
      else
      $result['da_gap_summary'] = ''; 



      /* Get Feeedbacks */
      $sql = "select b.*
            FROM `staff_transaction` AS a
            left join tbl_feedback b on a.staffid=b.apprenticeid
            where b.status=2 and a.id=".$transid. ' limit 0,3';    

      $res = $this->db->query($sql)->result();
      
      if(!empty($res))
        $result['da_feedback_summary'] = $res;
      else
        $result['da_feedback_summary'] = []; 
      
      
      /* Get Phase Reports */
      $sql = "select b.*,c.phaseid 
              FROM `staff_transaction` AS a
              inner join staff b on a.staffid=b.staffid
              left join tbl_daship_component c on c.candidateid=b.candidateid
            where c.status=2 and a.id=".$transid. ' order by c.phaseid ';    

      $res = $this->db->query($sql)->result();
      
      if(!empty($res))
        $result['da_reports_summary'] = $res;
      else
        $result['da_reports_summary'] = [];
      /* Get Work Experience Reports */
      $sql = "select c.*
              FROM `staff_transaction` AS a
              inner join staff b on a.staffid=b.staffid
              inner join tbl_work_experience c on c.candidateid=b.candidateid
            where a.id=".$transid;    

      $res = $this->db->query($sql)->result();
      
      if(!empty($res))
        $result['da_work_experience'] = $res;
      else
        $result['da_work_experience'] = [];


      /* echo "<pre>";
      print_r($result['da_feedback_summary']);
      echo "</pre>"; */
      
      if(!empty($result))
        return $result;

      return [];

    }
    catch (Exception $e)
    {
      print_r($e->getMessage());die;
    }
  }



 public function get_Recommended_to_transfer()
  {
    
    try{
     
     $sql = "SELECT
  st.emp_code,
  st.emailid,
  a.id,
  a.status,
  intm.intimation,
  a.comment,
  st.name,
  `lop`.officename,
  `lpo`.officename as oldofficename,
  a.probation_completed,
  a.probation_completed_date,
  a.intemation_type,
  stra.id as transid,
  a.intemation_type,
  mstcampus.campusname,
  (
    CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
  END
) AS gender
FROM staff_transaction as stra
 INNER join  `tbl_hr_intemation` AS a on stra.staffid=a.staffid
LEFT JOIN
  `staff` AS st ON st.staffid = a.staffid
LEFT JOIN
  mst_intimation AS intm ON intm.id = a.intemation_type
LEFT JOIN
  `tbl_da_personal_info` AS `b` ON `b`.staffid = `a`.staffid
LEFT JOIN
  `tbl_candidate_registration` AS tb ON `b`.candidateid = `tb`.candidateid

LEFT JOIN
  `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`

  LEFT JOIN
  `lpooffice` AS `lop` ON `lop`.officeid = stra.new_office_id
   LEFT JOIN
  `lpooffice` AS `lpo` ON `lpo`.officeid = stra.old_office_id

WHERE  stra.trans_status='Transfer' AND 
  `a`.intemation_type = 4  
  AND `a`.status in (0,1,2)
  AND `a`.Isdeleted = 0";
        
        $res = $this->db->query($sql)->result();
      // print_r($res); die();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





  public function get_Recommended_to_Extention()
  {
    
    try{
     
    $sql = "SELECT
  st.emp_code,
  st.emailid,

  a.id,
  a.status,
  intm.intimation,
  a.comment,
  st.name,
  a.probation_completed,
  a.probation_completed_date,
  a.intemation_type,
  stra.id as transid,
  a.intemation_type,
  mstcampus.campusname,
  (
    CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
  END
) AS gender
FROM staff_transaction as stra
 INNER join  `tbl_hr_intemation` AS a on stra.staffid=a.staffid
LEFT JOIN
  `staff` AS st ON st.staffid = a.staffid
LEFT JOIN
  mst_intimation AS intm ON intm.id = a.intemation_type
LEFT JOIN
  `tbl_da_personal_info` AS `b` ON `b`.staffid = `a`.staffid
LEFT JOIN
  `tbl_candidate_registration` AS tb ON `b`.candidateid = `tb`.candidateid

LEFT JOIN
  `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`

WHERE  stra.trans_status='Extention' AND 
  `a`.intemation_type = 6  
  AND `a`.status in (0,1,2)
  AND `a`.Isdeleted = 0";
        
        $res = $this->db->query($sql)->result();
      // print_r($res); die();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method get_Transferdetails() get Staff details .
   * @access  public
   * @param Null
   * @return  Array
   */

 public function get_Transferdetails($transid)
 {

  try{

   $sql = "SELECT
   b.emp_code,
   a.id,
   intm.intimation,
   a.comment,
   b.name,
   a.probation_completed,
   a.probation_completed_date,
   a.intemation_type,
   stra.id AS transid,
   stra.new_office_id,
   stra.staffid,
   st.emailid as da_mail,
   a.intemation_type,
   mstcampus.campusname,
   msdc_details.dc_name,
   lpooffice.officename,
   s.emailid as tc_mail,
   s.name as tc_name,
   f.name as fg_name,
   f.emailid as fg_email, 
   (
   CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
   END
 ) AS gender,
 `dc_team_mapping`.dc_cd
 FROM
 staff_transaction AS stra
 INNER JOIN
 `tbl_hr_intemation` AS a ON stra.staffid = a.staffid
 LEFT JOIN
 `staff` AS st ON st.staffid = a.staffid
 LEFT JOIN
 mst_intimation AS intm ON intm.id = a.intemation_type
 LEFT JOIN
 `tbl_da_personal_info` AS `b` ON `b`.staffid = `a`.staffid
 LEFT JOIN
 `tbl_candidate_registration` AS tb ON `b`.candidateid = `tb`.candidateid
 LEFT JOIN
 `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`
 LEFT JOIN
 `dc_team_mapping` ON dc_team_mapping.teamid = `st`.new_office_id
 LEFT JOIN 
 `msdc_details` on dc_team_mapping.dc_cd = `msdc_details`.dc_cd
 LEFT JOIN 
 `lpooffice` on st.new_office_id = lpooffice.officeid
 LEFT JOIN
 `staff` s on st.reportingto = s.staffid
 LEFT JOIN
 `staff` f on st.fgid = f.staffid
 WHERE
 stra.trans_status = 'Transfer' AND `a`.intemation_type = 4 AND `stra`.trans_flag = 1 AND `a`.Isdeleted = 0 AND stra.id = $transid"; 
// echo $sql; die;

 return $res = $this->db->query($sql)->row();

}catch (Exception $e) {
  print_r($e->getMessage());die;
}

}

public function get_Extentiondetails($transid)
  {
    
    try{
     
  $sql = "SELECT
  st.emp_code,
  a.id,
  intm.intimation,
  a.comment,
  st.name,
  a.probation_completed,
  a.probation_completed_date,
  a.intemation_type,
  stra.id AS transid,
  stra.new_office_id,
  stra.staffid,
  a.intemation_type,
  mstcampus.campusname,
  (
    CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
  END
) AS gender,
`msdc_details`.dc_name,
`lpooffice`.officename,
`msdc_details`.dc_cd

FROM
  staff_transaction AS stra
INNER JOIN
  `tbl_hr_intemation` AS a ON stra.staffid = a.staffid
LEFT JOIN
  `staff` AS st ON st.staffid = a.staffid
LEFT JOIN
  mst_intimation AS intm ON intm.id = a.intemation_type
LEFT JOIN
  `tbl_da_personal_info` AS `b` ON `b`.staffid = `a`.staffid
LEFT JOIN
  `tbl_candidate_registration` AS tb ON `b`.candidateid = `tb`.candidateid
LEFT JOIN
  `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`
  LEFT JOIN
  `dc_team_mapping` ON dc_team_mapping.teamid = `st`.new_office_id
  LEFT JOIN 
  `lpooffice` on lpooffice.officeid = stra.new_office_id
  LEFT JOIN 
  `msdc_details` on dc_team_mapping.dc_cd = msdc_details.dc_cd
  
WHERE
  stra.trans_status = 'Extention' AND `a`.intemation_type = 6 AND `stra`.trans_flag = 1 AND `a`.Isdeleted = 0 AND stra.id = $transid"; 

  // echo $sql; die;

   return $res = $this->db->query($sql)->row();
   
     }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
    
  }


public function newguide($new_field_guide)
{
 try{

  $sql = "SELECT 
  emailid ,name as newguidename,contact
  FROM staff 
  where staffid = '$new_field_guide'";
  return $res = $this->db->query($sql)->row();

} catch (Exception $e){
  print_r($e->getMessage());die;
}
}

/**
   * Method get_Office_Details() get Development Cluster Team.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_Office_Details()
{
  try{

         $sql = "SELECT `lpooffice`.`officeid`, `lpooffice`.`officename` FROM `lpooffice` ";  
        return $result = $this->db->query($sql)->result();    
      
        
      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }


   /**
   * Method getDAExtentionDetails() get DAs Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getDAExtentionDetails($staffid)
  {
    // echo $staffid; die;  
    try{

      if (empty($staffid) ||  $staffid == '' ) {
      
      $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
      redirect('/Proposed_probation_separation/index');
      
    } else {
     


      $sql = "SELECT a.name, a.emailid, a.permanenthno, a.permanentstreet,b.name as statename,c.name as districtname,a.permanentpincode,d.emailid as tcemail, d.name as reportingtoname,e.desname FROM `staff` as a
      LEFT JOIN state as b on a.permanentstateid = b.statecode
      LEFT JOIN district as c on a.permanentdistrict = c.districtid
      LEFT JOIN staff as d on a.reportingto = d.staffid
      LEFT JOIN msdesignation as e on d.designation = e.desid
      Where a.staffid =". $staffid; 
      $result = $this->db->query($sql)->row();

        return $result;
}
    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getDAExtentionFile() get DAs Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getDAExtentionFile($staffid)
  {
    
    try{

 if (empty($staffid) ||  $staffid == '' ) {
      
      $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
      redirect('/Proposed_probation_separation/index');
      
    } else {

       $sql = "SELECT a.generate_letter_name FROM `tbl_probation_separation` as a
      Where a.staffid =". $staffid; 
      $result = $this->db->query($sql)->row();

        return $result;
}
    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }






/**
   * Method gethrDetails() get HRD Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function gethrDetails($staffid)
  {
    
    try{

      if (empty($staffid) ||  $staffid == '' ) {
      
      $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
      redirect('/Proposed_probation_separation/index');
      
    } else {


       $sql = "SELECT staff.staffid,staff.name,staff.emailid,msdesignation.desname FROM `mstuser`
        INNER JOIN staff ON mstuser.staffid = staff.staffid
        INNER JOIN msdesignation ON msdesignation.desid = staff.designation
        inner join role_permissions on  role_permissions.RoleID=mstuser.RoleID
        WHERE role_permissions.Controller = 'Proposed_probation_separation' and role_permissions.RoleID=16 and role_permissions.Action='index' ORDER BY
         `staff`.staffid  DESC  LIMIT 0,1 "; 

     $res = $this->db->query($sql)->row();
     return $res;
}
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }

  }
	 /**
   * Method getStaffDetails() get Staff details .
   * @access  public
   * @param Null
   * @return  Array
   */

   public function getstaffDetails($token=NULL){


    try{

      $sql = "SELECT `sttr`.designation,`sttr`.reportingto,`sttf`.fgid,`sttr`.name,`sttr`.emailid,`lpo`.officename FROM `staff_transaction` as `sttf` 
      LEFT JOIN `staff` as `sttr` ON `sttf`.staffid  = `sttr`.staffid
      LEFT JOIN `lpooffice` as `lpo` ON sttf.`new_office_id` = `lpo`.officeid 
          Where `sttf`.staffid = $token ";
      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }


public function get_Prob_Separation($token)
  {
    
    try{
            $sql = "SELECT  * FROM `tbl_probation_separation`  as `tblps`
            Where `tblps`.id =$token ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
   


public function get_Development_Cluster()
  {
    try{
        $sql = "SELECT  `msdc_details`.dc_cd,`msdc_details`.dc_name FROM `msdc_details`  order By dc_name ASC  ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function get_Development_Apprentice()
  {
    
    try{
            $sql = "SELECT staff.staffid, staff.name FROM `staff`  Where  staff.designation =13 AND staff.status = 1 ";  // die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
   

/**
   * Method getIntimation() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getIntimation()
  {
    
    try{

      $sql = "SELECT * FROM `mst_intimation` Where id in(4,6) AND Isdeleted=0";

       $result = $this->db->query($sql)->row();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 public function getIntimation1()
  {
    
    try{

      $sql = "SELECT * FROM `mst_intimation` Where id in(4) AND Isdeleted=0";

       $result = $this->db->query($sql)->row();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffid() get staff email Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getstaffid($staffid)
  {
    
    try{

       $sql = "SELECT emailid FROM `staff` Where staffid=$staffid "; 

       $result = $this->db->query($sql)->row();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateteamfg() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandidateteamfg($id)
  {
    
    try{

      $sql = "SELECT teamid,fgid FROM `tbl_candidate_registration` 
              Where candidateid = '$id' AND joinstatus=1";

       $result = $this->db->query($sql)->result();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateSeparationDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getStaffSeparationDetails($id)
  {
    
    try{

       $sql = "SELECT `b`.name,`b`.emailid,`a`.generate_letter_name FROM `tbl_probation_separation` as a
      Inner Join `staff` as b on a.staffid = b.staffid  Where b.staffid = '$id' ";

       $result = $this->db->query($sql)->result()[0];

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method getDATransferDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getDATransferDetails($staffid)
  {
    
    try{


     $sql = "SELECT staff.name, staff.emailid,msdesignation.desname FROM `staff`
            LEFT JOIN
            `msdesignation` on staff.designation = msdesignation.desid
      Where staff.staffid =". $staffid;
      // echo $sql; die;
       $result = $this->db->query($sql)->row();

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getStaffTransferDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getStaffTransferDetails($id)
  {
    
    try{

      $sql = "SELECT b.name,b.emailid,a.generate_letter_name FROM `tbl_transfer_da` as a
      Inner Join `staff` as b on a.staffid = b.staffid  Where b.staffid = '$id' ";

       $result = $this->db->query($sql)->result()[0];

        return $result;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



public function getStaffTransactioninc($tablename,$incval)
{
    try{

        // echo $tablename;
        // echo $incval;  die;
        //'staff_transaction',@a; 

        $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }




public function getHRunitDetails()
{
    try{

      $sql = "SELECT staff.name,msdesignation.desname,CASE WHEN sysaccesslevel.Acclevel_Cd = 16 then 'HRD Unit' WHEN sysaccesslevel.Acclevel_Cd = 17 then 'PA Unit' WHEN sysaccesslevel.Acclevel_Cd = 20 then 'Finance Unit' ELSE 'ROle' END as rolename FROM `staff` INNER JOIN mstuser on mstuser.staffid = staff.staffid
INNER JOIN msdesignation on msdesignation.desid = staff.designation
INNER Join sysaccesslevel ON sysaccesslevel.Acclevel_Cd = mstuser.RoleID
WHERE staff.designation = 4 AND mstuser.RoleID = 16";
      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }



public function getCentralEventDetails($effective_date, $dateoftransfer)
{
    try{

       $sql = "SELECT mste.name, tbl_central_event.from_date as eventdate FROM tbl_central_event INNER JOIN mstevents as mste on mste.id= tbl_central_event.name  WHERE from_date BETWEEN '".$dateoftransfer."'  AND '".$effective_date."' AND tbl_central_event.isdeleted = 0 AND mste.isdeleted = 0  LIMIT 0,1";
   // echo $sql; die;
      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
}