<?php 


class Provident_fund_nomination_form_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}





  public function getCandidateWithAddressDetails($staff_id)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getCandidateWith($reportingto)
  {
    
    try{

        $sql = "SELECT
    staff.name as reprotingname
    
FROM
    staff WHERE   staff.staffid='$reportingto'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


	public function getNomineedetail($candidateid)
  {
    
    try{

          $sql = 'SELECT provident_fund_nomination_details.id as pro_id,provident_fund_nomination_details.share_nominee,provident_fund_nomination_details.sr_no,provident_fund_nomination_details.minior,provident_fund_nomination_details.provident_id,provident_fund_nomination_details.name,provident_fund_nomination_details.relation_id,provident_fund_nomination_details.age,provident_fund_nomination_details.address,sysrelation.relationname,sysrelation.status,sysrelation.id FROM `provident_fund_nomination_details`  
                 left join sysrelation on   `provident_fund_nomination_details`.relation_id =  `sysrelation`.id  
                    left join provident_fund_nomination on   `provident_fund_nomination`.id =  `provident_fund_nomination_details`.provident_id
                Where `provident_fund_nomination`.candidate_id ='.$candidateid.''; 
               // echo $sql; die;
                 

        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method get_pfinformation()details of provident fund nomination.
   * @access  public
   * @param Null
   * @return  row
   */

  public function get_pfinformation($token)
  {
    
    try{

          $sql = 'SELECT * FROM `provident_fund_nomination`         
                Where `provident_fund_nomination`.candidate_id ='.$token.''; 
              // echo $sql;  die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method count_pfinformation(()count records of provident fund nomination.
   * @access  public
   * @param Null
   * @return  row
   */

  public function count_pfinformation($token)
  {
    
    try{

          $sql = 'SELECT count(*) as count_nomination FROM `provident_fund_nomination`         
                Where `provident_fund_nomination`.id ='.$token.''; 
               // die();
                //echo $sql;    
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  /**
   * Method staffName() get Staff Name.
   * @access  public
   * @param Null
   * @return  Array
   */


public function staffName($staff_id,$login_staff)
  {
   
    try{
     
           $sql = "SELECT * FROM staff where staffid !=$staff_id and staff.staffid!=$login_staff
           AND new_office_id = ".$this->loginData->teamid." order by staff.name asc";  
           

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}

/**
   * Method get max get_providentworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_providentworkflowid($staff_id)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=13 and staff.staffid=$staff_id ORDER BY workflowid DESC LIMIT 1";
    
//echo $sql;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

  public function get_witness_detail($staff_id)
  {
    
    try{

        $sql = 'SELECT   a.address1,a.address2,b.name as name1 , c.name as name2 from provident_fund_nomination a
                  inner join `staff` b on b.staffid = a.witness1
                  inner join `staff` c on c.staffid = a.witness2
                Where `a`.staff_id ='.$staff_id.''; 
                //echo $sql;
               // die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}