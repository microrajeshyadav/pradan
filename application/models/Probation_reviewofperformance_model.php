<?php 

/**
* Master  Model
*/
class Probation_reviewofperformance_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

/**
	 * Method get_providentworkflowid() get workflow id .
	 * @access	public
	 * @param	
	 * @return	array
	 */

public function get_providentworkflowid($token)
{

  if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Probation_reviewofperformance/index');
				
			} else {


 try{

$sql = "SELECT
  tbl_workflowdetail.workflowid as workflowid,tbl_workflowdetail.flag
FROM
    `tbl_workflowdetail`
INNER JOIN staff ON staff.staffid = tbl_workflowdetail.staffid
WHERE
    tbl_workflowdetail.type = 22 AND tbl_workflowdetail.r_id = $token
     ORDER BY tbl_workflowdetail.workflowid DESC LIMIT 1";
             $res = $this->db->query($sql)->row();
             return $res;
              }
              catch (Exception $e) {
       print_r($e->getMessage());die;
     }
 }

//echo $sql;

}
/**
	 * Method loginstaff() get login staff  .
	 * @access	public
	 * @param	
	 * @return	array
	 */

	public function loginstaff($staffid)
  {

    
    try{
   $sql = "SELECT staff.staffid,staff.name,msdesignation.desname FROM `staff` 
   inner join msdesignation on staff.designation=msdesignation.desid 
   where `staffid`=
".$staffid;


         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



public function getstaff($token)
  {

    
  		if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Probation_reviewofperformance/index');
				
			} else 
			{
    
			  try{

     $sql = "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.emp_code,
    staff.candidateid,
    staff_transaction.id,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff_transaction.reason,
    staff_transaction.old_office_id,
    staff_transaction.old_designation,
    staff.designation,
    staff.emailid,
    lpooffice.officename,
    msdesignation.desname,
    msdesignation.level AS levelname,
    staff.joiningandinduction FROM staff_transaction
INNER JOIN staff ON staff_transaction.staffid = `staff`.`staffid`
INNER JOIN lpooffice ON staff.new_office_id = lpooffice.`officeid`
INNER JOIN msdesignation ON staff.designation = msdesignation.`desid`
WHERE
    staff_transaction.id = $token and staff_transaction.trans_status='Midtermreview' 
"; 

      return  $this->db->query($sql)->row();
       }
       catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
}






/**
	 * Method getstaffid() get staff ID .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffid($token)
{
	if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Probation_reviewofperformance/index');
				
			} 
			else 
			{


	try{


		 $sql = "
		SELECT
		`staff`.name,
		`staff`.emp_code,
		`staff`.staffid,
		`staff`.reportingto AS supervisorid,
		`msdesignation`.desname,
		`lpooffice`.officename
		FROM
		staff_transaction
		INNER JOIN staff ON `staff`.staffid = `staff_transaction`.staffid
		INNER JOIN lpooffice ON `staff_transaction`.new_office_id = `lpooffice`.officeid
		INNER JOIN msdesignation ON `staff_transaction`.new_designation = `msdesignation`.desid
		WHERE
		`staff_transaction`.id = $token
		ORDER BY
		`staff_transaction`.staffid "; 

		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');

	return	$result = $this->db->query($sql)->row();

		// //print_r($result); die;

		// return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
		}

}


/**
	 * Method getStaffProbationReviewofPerformance() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getStaffProbationReviewofPerformance($staffid)
{
		if (empty($staffid) || $staffid == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $staffid is either blank or empty.');
				redirect('/Probation_reviewofperformance/index');
				
			} 
			else {

	try{

		$sql = "SELECT
		`staff`.name,`staff`.emp_code,`staff`.staffid,`staff`.reportingto as supervisorid,`msdesignation`.desname,`lpooffice`.officename,`staff`.doj,`staff`.probation_date
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join lpooffice on `staff_transaction`.new_office_id = `lpooffice`.officeid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		WHERE `staff_transaction`.id = $staffid
		ORDER BY
		`staff_transaction`.staffid ";

		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');
		 // echo $sql;
		 // die;

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}



/**
	 * Method getstaffprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffprobationdetals($token)
{
		if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Probation_reviewofperformance/index');
				
			} 
			else {
	try{

		$sql = "SELECT prp.* FROM tbl_probation_review_performance as prp
		WHERE prp.`transid` =".$token;
		// echo $sql;
		// die;
		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');

		$result = $this->db->query($sql)->result()[0];

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}



/**
	 * Method getstaffpeersonnelprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffpeersonnelprobationdetals()
{
	
	try{

		$sql = "SELECT prp.*,prp.createdby, stf.name,stf.emp_code,stf.staffid,msdes.desname  FROM tbl_probation_review_performance as prp
		inner join staff as stf ON prp.createdby = stf.staffid
		inner join msdesignation as msdes ON stf.designation = msdes.desid
		 			";// die;
		 			

		 			$result = $this->db->query($sql)->result()[0];

		 			return $result;

		 		}catch (Exception $e) {
		 			print_r($e->getMessage());die;
		 		}

		 	}





/**
	 * Method getstaffpersonnaldetails() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffpersonnaldetails()
{


	try{

		$sql = "SELECT
		`staff`.name as personnalname,`staff`.emp_code as personnalempcode,`staff`.staffid as personnalstaffid,`msdesignation`.desname as personnaldesignationname,
		lpooffice.officename as officename
		FROM
		staff_transaction
		left JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		inner join lpooffice on  `staff_transaction`.new_office_id = `lpooffice`.officeid
		inner join mstuser on `staff_transaction`.staffid = `mstuser`.staffid
		WHERE  `staff`.staffid =". $this->loginData->staffid ." ORDER BY `staff_transaction`.staffid ";
		$result = $this->db->query($sql)->row();
		//print_r($result);

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getstaffpersonnaldetails() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffPnndetails()
{


	try{

		$sql = "SELECT
		`staff`.name as personnalname,`staff`.emp_code as personnalempcode,`staff`.staffid as personnalstaffid,`msdesignation`.desname as personnaldesignationname
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		inner join mstuser on `staff_transaction`.staffid = `mstuser`.staffid
		WHERE  `mstuser`.RoleID = 17 ORDER BY `staff_transaction`.staffid ";
		//echo $sql;

		$result = $this->db->query($sql)->row();


		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


/**
	 * Method getSupervisor() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getSupervisor()
{


	try{

		$sql = "SELECT
		`staff`.name as supervisorname,`staff`.emp_code as supervisorempcode,`staff`.staffid as supervisorstaffid,`msdesignation`.desname as supervisordesignationname
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)

		inner join staff on `staff`.staffid = `staff_transaction`.staffid

		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid

		WHERE  `staff`.staffid =". $this->loginData->staffid ."  AND `staff`.designation   IN(4,16)  ORDER BY `staff_transaction`.staffid ";


		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getEDName() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getEDName($edid)
{
	if (empty($edid) || $edid == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $edid is either blank or empty.');
				redirect('/Probation_reviewofperformance/index');
				
			} else {

	try{

		$sql = "SELECT staff.name FROM staff WHERE `staff`.staffid = ".$edid;

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}
/**
	 * Method getEDName() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getEd_id()
{

	try{

		$sql = "SELECT staff.staffid from mstuser inner join staff on mstuser.staffid=staff.staffid where mstuser.RoleID=18 and staff.designation=2 and IsDeleted=0";

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

}