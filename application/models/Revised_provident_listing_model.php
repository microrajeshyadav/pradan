<?php 

/**
* State Model
*/
class Revised_provident_listing_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    
  }



    public function get_candidaateid($staff_id)
    {
        
        $sql="select * from staff where staffid='$staff_id'";
        
        return  $this->db->query($sql)->row();
    }


  public function getstaffname()
  {
    $sql = '';
     $sql .= "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.emp_code,
    staff.candidateid,
   
    staff.designation,
    staff.staffid,
   
    staff.joiningandinduction,
    provident_fund_nomination.place,
    provident_fund_nomination.date,

    staff.gender,
    (CASE
    WHEN staff.gender = 1 THEN 'Male'
    WHEN staff.gender = 2 THEN 'Female'
    WHEN staff.gender = 3 THEN 'Other'
    ELSE 'Not gender here'
    END) as gender,
    provident_fund_nomination.status,
   (CASE
    WHEN provident_fund_nomination.status = 0 THEN 'Save'
    WHEN provident_fund_nomination.status = 1 THEN 'Submitted'
    WHEN provident_fund_nomination.status = 3 THEN 'Approved'
    WHEN provident_fund_nomination.status = 4 THEN 'Rejected'
    ELSE 'Not status here'
END) as flag
    FROM
        provident_fund_nomination 
         left JOIN `staff` ON `staff`.staffid = `provident_fund_nomination`.`staff_id`
          where provident_fund_nomination.type='m'
   
    ";  
       // echo $sql;
       // die;
      return  $this->db->query($sql)->result();
}

public function getOfficeList($p_office)
  {
     try
   {
    //echo "office id=".$p_office;
   // die;
    $this->db->select("officeid,officename");
    

      $this->db->from('lpooffice');
      
     
      $query=$this->db->get();
     // echo $this->db->last_query();
      //die();
      return $query->result();
    }
     

     catch (Exception $e) 
     {
       print_r($e->getMessage());die;
     }

  }

    public function fetchdatas($token)
    { 


        $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
        $result = $this->db->query($sql);
        $rows = $result->num_rows();
        $tm='-1';
         if($rows==0)
         {
           return $tm;
         }
         else
         {
             
           $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
        
            $query=$this->db->query($sql);
         }
   }



    /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getProbationStatus($staffid)
  {
    
    try{
   $sql = "SELECT flag FROM `tbl_probation_review_performance` where `staffid`=".$staffid;

         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}