<?php 

/**
* Master  Model
*/
class Recruitersheadmapping_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	  // /**
    //  * Method getRecuters() staff detail.
    //  * @access  public
    //  * @param   $token
    //  * @return  string.
    //  */

    public function getRecuters()
    {
        try{

              $sql = "SELECT mcr.`id_mapping_campus_recruiter`,cam.`campusname`, st.`name` FROM `mapping_campus_recruiters` AS mcr 
                  INNER JOIN mstcampus AS cam ON mcr.`campusid` = cam.`campusid`
                  Inner JOIN staff as st ON  st.`staffid` = mcr.`recruiterid` WHERE mcr.`isdeleted`='0'";  
             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

	public function index()
	{

	}


    // /**
    //  * Method getRecruitment() staff detail.
    //  * @access  public
    //  * @param   $token
    //  * @return  string.
    //  */

    public function getRecruitment()
    {
        try{

            
              $sql = "SELECT  staff.`staffid`, staff.`name` FROM mapping_staff_recruitment  
              INNER JOIN  `staff` ON mapping_staff_recruitment.`recruitmentteamid` = staff.`staffid` WHERE `isdeleted`='0' ";  

             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }


     // /**
    //  * Method getRecruitersHead() staff detail.
    //  * @access  public
    //  * @param   $token
    //  * @return  string.
    //  */

    public function getRecruitersHead()
    {
        try{
           
             $sql = "SELECT `mapping_campus_recruiters_head`.id, `mstcampus`.campusname, `staff`.name FROM mapping_campus_recruiters_head 
              INNER JOIN `mstcampus` ON mapping_campus_recruiters_head.`campusid` = `mstcampus`.`campusid`
              INNER JOIN `staff` ON mapping_campus_recruiters_head.`recruiterheadid` = staff.`staffid`
               Where  `mapping_campus_recruiters_head`.`isdeleted`='0'";   
                
             $result = $this->db->query($sql)->result();
            
             return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }



    /**
     * Method getCampus() staff detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

	public function getCampus($impcamp)
	{
		try{
          
            if (empty($impcamp)) {
                $impcamp = 0;
            }
           
             $sql = "SELECT cam.`campusincharge`,cam.`campusname`,cam.`emailid`,cam.`city`,cam.`mobile`,cam.`telephone`,cam.`campusid` FROM `mstcampus` as cam  WHERE `cam`.campusid NOT IN ($impcamp) AND cam.`IsDeleted`='0'"; //die;  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}


     /**
     * Method getCampus() staff detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function getRecruitersWithCampus()
    {
        try{
           
            $sql = "SELECT `mcrh`.campusid, `mcrh`.recruiterheadid FROM `mapping_campus_recruiters_head` as mcrh  WHERE mcrh.`isdeleted`='0'";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }

 /**
     * Method getRecruitmentDetails() Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function getMappedCampusRecruitment($token)
    {
        try{
                $this->db->select('id,campusid,recruiterheadid');
                $this->db->where('id',$token);
                return  $this->db->get('mapping_campus_recruiters_head')->result()[0]; //echo $this->db->last_query();//die;
               
        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }  
 /**
     * Method getCampus() staff detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function getRecuiterHeadCampusDetail($token)
    {
        try{
           
             $sql = "SELECT * FROM `mapping_campus_recruiters_head` as crh WHERE crh.id='".$token."' AND `IsDeleted`=0";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }

    /**
     * Method delete() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */
    public function delete($token)
    {
        try {

                $deleteArray = array(
                        'isdeleted'    => 1,
                      );

                    $this->db->where(" id",$token);
                  return ($this->db->update('mapping_campus_recruiters_head', $deleteArray)) ? 1 : -1; //$this->db->last_query(); die;
        }
        catch (Exception $e) {
            print_r($e->getMessage());die;
        }
    }

	


}