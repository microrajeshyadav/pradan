<?php 

/**
* Dashboard Model
*/
class Letterformat_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	/**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


	public function index()
	{

	}
  public function getletterformat()
  {
    try{
      $sql = "SELECT m.id, m.processid, case when type = 1 then 'Letter' else 'Email' end as type, p.type as typeid, p.process, Case when m.isactive=1 then 'Active' else 'De Active' end as status from tbl_letter_master m inner join tbl_letter_process p on m.processid = p.processid";
      $result = $this->db->query($sql)->result();
      return $result;
    }
    catch(Exception $e){
        print_r($e->getMessage());die();
    }
  }
  public function getletterformatdetail($processid)
  {
    try{
      $sql = "SELECT a.id, a.processid, a.lettercontent, b.type  FROM tbl_letter_master as a LEFT JOIN tbl_letter_process as b ON b.processid = a.processid where a.id = ".$processid;
      $result = $this->db->query($sql)->row();
      return $result;
    }
    catch(Exception $e){
        print_r($e->getMessage());die();
    }
  }
  public function getattributeslist($processid){
    try{
      $sql = "SELECT * FROM tbl_letter_process_attributes where processid = ".$processid;
      $result = $this->db->query($sql)->result();
      return $result;
    }
    catch(Exception $e){
        print_r($e->getMessage());die();
    }
  }
  public function getprocesslist($typeid){
    try{
      $sql = "SELECT * FROM tbl_letter_process where type = ".$typeid;
      $result = $this->db->query($sql)->result();
      return $result;
    }
    catch(Exception $e){
        print_r($e->getMessage());die();
    }
  }


	
}