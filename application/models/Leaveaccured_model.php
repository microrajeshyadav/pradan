<?php 

/**
* Master  Model
*/
class Leaveaccured_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{

	}

	public function getleavedetails($emp_code)
	{
		try{

      $sql = "SELECT l.`Ltypename` FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` WHERE  lt.`Leave_transactiontype` ='DR' AND  lt.`Emp_code`=".$emp_code;  

      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

/*
    function geted_id is used to get ed id
    result row;
    created by pooja
*/
  public function geted_id()
  {
    try{

      $sql = "SELECT staffid FROM `staff` where `status`=1 && `designation`=2
";  

      $res = $this->db->query($sql)->row();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  public function getreportingtodetails($emp_code)
  {
    try{

     $sql = "SELECT
     a.emp_code  as empcode,
     a.name  as staffname,
     msd.desname as staffdesignation,
     b.name AS reportingname,
     b.emailid as reportingtoemailid,
     lpo.officename as staffofficename,
     b.permanenthno,
     b.permanentstreet,
     b.permanentcity,
     b.permanentpincode,
     s.name AS statename,
     d.name AS districtname,
     b.contact,
     a.new_office_id,
     a.reportingto as reportingstaffid
     FROM
     staff AS a
     INNER JOIN `staff` AS b
     ON
     a.reportingto = b.staffid
     LEFT JOIN `state` AS s
     ON
     a.permanentstateid = s.statecode
     LEFT JOIN `district` AS d
     ON
     a.permanentdistrict = d.districtid
     LEFT JOIN `msdesignation` as msd
     on 
     a.designation = msd.desid
     LEFT JOIN `lpooffice` as lpo
     on 
     a.new_office_id = lpo.officeid
     WHERE
     a.emp_code = ".$emp_code; 

     $res = $this->db->query($sql)->row();

     return $res;

   }catch(Exception $e){
    print_r($e->getMessage());
  }
}

public function getleavectegory()
{
try{
 $LoginData = $this->session->userdata('login_data');

 $query="SELECT b.gender FROM mstuser a inner JOIN staff b ON a.staffid=b.staffid where a.staffid=? GROUP by b.gender";
 $gender_details = $this->db->query($query,[$LoginData->staffid])->row();
       //  print_r($gender_details); die;

 if ($gender_details->gender == 1) {
  $sql = "select 	Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 5 AND 11";
  $res = $this->db->query($sql)->result();
}else if ($gender_details->gender == 2) {
  $sql = "select  Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 3 AND 10";
  $res = $this->db->query($sql)->result();
            // print_r($res); die();
}else if ($gender_details->gender == 3) {
 $sql = "select  Ltypeid, Ltypename from msleavetype where Ltypeid BETWEEN 3 AND 11";
 $res = $this->db->query($sql)->result();

}
return $res;

}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



public function get_empid($token)
{
 try{

   $sql = "SELECT emp_code FROM staff WHERE staffid =".$token; 

  $res = $this->db->query($sql)->row();
  return $res;

}catch(Exception $e){
  print_r($e->getMessage());die();
}

}

public function getleavedetail_balance($fromdate, $todate)
{
  try{

    //  if (date('m')>=1 && date('m') <=3) {

    //   $preyear = date('Y')-1;
    //   $fromdate = $preyear.'-04-01';
    //   $todate = date('Y').'-03-31';

    // }else{
    //    $nextyear = date('Y')+1;
    //    $fromdate  = $preyear.'-04-01';
    //    $todate    = $nextyear.'-03-31';
    // }




    $sql =  "SELECT staff.emp_code, staff.Name, desname, lpo.officename , IFNULL(trnleave_ledger.Accured,0) Accured FROM  (SELECT emp_code,  sum(IFNULL(case when Leave_transactiontype = 'CR' AND Leave_type= 0 AND Description ='Credit leaves posting' then Noofdays else 0 end,0)) Accured FROM `trnleave_ledger` WHERE From_date >= '".$fromdate."' and to_date <= '".$todate."' GROUP BY Emp_code) as trnleave_ledger INNER JOIN staff on trnleave_ledger.Emp_code= staff.emp_code LEFT JOIN `msdesignation` as msd on staff.designation = msd.desid INNER JOIN `lpooffice` as lpo on staff.new_office_id = lpo.officeid where staff.status = 1 GROUP BY staff.emp_code, staff.Name ORDER BY staff.emp_code";
   //echo  $sql; die;
   $res = $this->db->query($sql)->result();

   return $res;

 }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

public function getCurrentfyearperiod($fyear){

try{

 $sql = "SELECT id,`monthfroms`,`monthtos` FROM `msleavecrperiod` WHERE `CYear`='".$fyear."' ORDER BY `from_month` ASC"; 
 return $res = $this->db->query($sql)->result();

 }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



public function checkaccuredavailaibility($currentyear,$fromdate){

   //   if (date('m')>=4 && date('m') <=9) {

   //    $preyear = date('Y');
   //    $fromdate = $preyear.'-04-01';
   //  }else{
   //     $nextyear = date('Y')-1;
   //     $fromdate  = $nextyear.'-10-01';
   // }


  $sql = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms = '".$fromdate."' AND isprocess=0"; 
  $res = $this->db->query($sql)->row();

  if(count($res) > 0){  
    $Accuredvalue = $res->credit_value;
    $From_date = $res->monthfroms;
    $TO_date = $res->monthtos;
    
    $sql = "select emp_code, designation, Date_of_transfer from staff
     left join(SELECT staff.Staffid, Date_of_transfer FROM `staff` inner join staff_transaction st on staff.staffid = st.staffid and st.new_designation != 13 and trans_status  = 'JOIN' ) exp on staff.staffid = exp.staffid
     where staff.status=1 AND staff.designation!= 13 ORDER BY emp_code";
    $userdata = $this->db->query($sql)->result();
   
    foreach ($userdata as $value) {
     $sql = "select Noofdays, id from trnleave_ledger where From_date='".$From_date."' AND  Emp_code=".$value->emp_code." AND Leave_type = 0 AND Leave_transactiontype ='CR' AND Description='Credit leaves posting'  " ;
      $res = $this->db->query($sql)->row();
     // print_r($res); die;

      // if ($value->designation == 13) {
      //    $Accuredvalue = 0;
      // }

      if(count($res) > 0){  
       // $transtd =  $res->id;
       //  $updatearray = array(
       //    'Noofdays'=> $Accuredvalue
       //  );
       //  $this->db->where('id',$transtd);
       //  $this->db->update('trnleave_ledger', $updatearray);
        
      
      }else{
        $insertarray = array(
          'Emp_code'=> $value->emp_code,
          'From_date'=> $From_date,
          'To_date'=> $TO_date,
          'Leave_type'=> 0,
          'Leave_transactiontype'=> 'CR',
          'Description' => 'Credit leaves posting',
          'Noofdays'=> $Accuredvalue,
          'status' =>  0,
          'appliedon'=> date('Y-m-d')
        );
      
      $this->db->insert('trnleave_ledger', $insertarray);


      }
    }

    $updatesql = "update msleavecrperiod set isprocess=1 where CYear='".$currentyear."' AND status=1 and  monthfroms = '".$fromdate."' AND isprocess=0 " ;
      $res = $this->db->query($updatesql);
      
    return 1;
  }else{
    return 3;
  }
}

}