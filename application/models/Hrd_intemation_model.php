<?php 

/**
* Hrd_intemation  Model
*/
class Hrd_intemation_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}



/**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */

public function changedatedbformate($Date)
{
  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
   //print_r($date); 
 if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
 else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 return $date;

 }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

   /**
   * Method get_Probation_Separation() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

   public function get_Probation_Separation()
   {
    
    try{
     
      $sql = "SELECT
  st.emp_code,
  a.id,
  a.comment,
  st.name,
  a.probation_completed,
  a.probation_completed_date,
  a.intemation_type,
  mstcampus.campusname,
  (
    CASE WHEN tb.gender = 1 THEN 'Male' WHEN tb.gender = 2 THEN 'Female'
  END
) AS gender

FROM
  `tbl_hr_intemation` AS a
LEFT JOIN
  `staff` AS st ON st.staffid = a.staffid
LEFT JOIN
  `tbl_da_personal_info` AS `b` ON `b`.staffid = `a`.staffid
LEFT JOIN
  `tbl_candidate_registration` AS tb ON `tb`.candidateid = `st`.candidateid
LEFT JOIN
  `mstcampus` ON `tb`.campusid = `mstcampus`.`campusid`
WHERE
  `a`.status in (1,2) AND `a`.Isdeleted = 0 and a.intemation_type <> 4"; 
      // echo $sql; die;
      $res = $this->db->query($sql)->result();

      return $res;
      
    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method getIntimation() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getIntimation()
{
  
  try{

    $sql = "SELECT * FROM `mst_intimation` Where Isdeleted=0 and id <> 4";

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

 /**
   * Method get_DA_Details() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function get_DA_Details()
 {
  
  try{

    $sql = "SELECT staffid, name FROM staff WHERE designation=13 AND candidateid !='' AND staffid NOT IN (SELECT staffid FROM `tbl_hr_intemation`)";
     // SELECT a.candidateid,a.candidatefirstname,a.candidatemiddlename,a.candidatelastname,b.status FROM `tbl_candidate_registration` as a inner join 
     // tbl_verification_document as b ON a.candidateid = b.candidateid Where b.status=1";

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



 /**
   * Method get_Personnal_Staff_List() get personnal staff detail all mpdule.
   * @access  public
   * @param Null
   * @return  Array
 */
  
  public function get_hr_Staff_List()
   {
      try
    {

       $sql="SELECT `mstuser`.staffid, `staff`.name FROM `mstuser` 
       inner JOIN staff on staff.staffid = mstuser.staffid 
       inner JOIN role_permissions on mstuser.UserID = role_permissions.UserId 
       WHERE role_permissions.Controller='Proposed_probation_separation' AND `mstuser`.RoleID = 16 AND `mstuser`.IsDeleted=0 AND staff.status=1";
       // echo $sql;exit();
       $result =  $this->db->query($sql)->result();
        return $result;
  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

  /**
   * Method get_DA_EDIT_Details() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function get_DA_EDIT_Details()
  {
    
    try{

      $sql = "SELECT staffid, name FROM staff WHERE designation=13 AND candidateid !='' ";
     // SELECT a.candidateid,a.candidatefirstname,a.candidatemiddlename,a.candidatelastname,b.status FROM `tbl_candidate_registration` as a inner join 
     // tbl_verification_document as b ON a.candidateid = b.candidateid Where b.status=1";

      $result = $this->db->query($sql)->result();

      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method get_DA_Single_Details() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_DA_Single_Details()
{
  
  try{

    // $sql = "SELECT staffid, name FROM staff WHERE designation=13 AND candidateid !='' AND staffid NOT IN (SELECT staffid FROM `tbl_hr_intemation`) and probation_date between CURDATE() and  DATE_ADD(CURDATE(), INTERVAL 30 DAY)  ";

    $sql="SELECT
    staffid,
    name
FROM
    staff
    inner join tbl_candidate_registration on staff.candidateid=tbl_candidate_registration.candidateid
WHERE
    designation=13 And(tbl_candidate_registration.joinstatus=1|| staff.recommend_to_graduate=1) || staff.candidateid != '' AND staffid NOT IN(
    SELECT
        staffid
    FROM
        `tbl_hr_intemation`
) AND probation_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY)";

   // $sql = "SELECT staffid, name FROM staff WHERE designation=13 AND candidateid !='' AND staffid NOT IN (SELECT staffid FROM `tbl_hr_intemation`) and DATE_ADD(doj, INTERVAL 365 DAY) between CURDATE() and DATE_ADD(CURDATE(), INTERVAL 365 DAY)"";



   $result = $this->db->query($sql)->result();
   // print_r($result);
   // die;
   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method get_DA_PersonalDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_DA_PersonalDetails($id)
{
  
  try{

   $sql = "SELECT 
   b.name,
   b.emailid,
   a.comment,
   c.intimation
   FROM 
   `tbl_hr_intemation` as a 
   LEFT join `tbl_da_personal_info` as b ON a.staffid = b.staffid 
   LEFT join `mst_intimation` as c ON a.intemation_type = c.id WHERE a.staffid='$id'"; 

   $result = $this->db->query($sql)->result()[0];

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method get_DA_Staff_Details() get DA staff Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_DA_Staff_Details($id)
{
  
  try{

    $sql = "SELECT sta.staffid, sta.name, sta.emailid, sta.reportingto FROM staff as sta 
           WHERE sta.designation=13 AND sta.staffid =$id";
          // echo $sql;
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method get_HRDDetails() get DA staf HRD  Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_HRDDetails($hrdid)
{
  
  try{

    $sql = "SELECT sta.staffid, sta.name, sta.emailid FROM staff as sta 
           WHERE sta.staffid =$hrdid";
        
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method get_Single_DA_Details() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_edit_Probation_Separation($token)
{
  
  try{

    $sql = "SELECT * FROM `tbl_hr_intemation` WHERE `tbl_hr_intemation`.id = $token";

    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
     * Method getHrdintimationDetails() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

public function getHrdintimationDetails($token)
{
  try{

    $sql = "SELECT * FROM `tbl_hr_intemation` WHERE id=".$token."";
    $res = $this->db->query($sql)->result();
    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



 /**
     * Method delete() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */
 public function delete($token)
 {
  try {

          //cho $token;

    $deleteArray = array(
      'Isdeleted'    => 1,
    );

    $this->db->where("id",$token);
                  return ($this->db->update('tbl_hr_intemation', $deleteArray)) ? 1 : -1; //$this->db->last_query(); die;


            //$this->db->update(DISTRICT,$form); //echo $this->db->last_query(); die;
           // return ($this->db->update(state,$form)) ? 1 : -1; $this->db->last_query(); die;
                }
                catch (Exception $e) {
                  print_r($e->getMessage());die;
                }
              }


            }