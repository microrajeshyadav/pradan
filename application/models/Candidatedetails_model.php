<?php 

/**
* Login Class
*/
class Candidatedetails_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	
  public function getSelectedCandidate()
  {
    
    try{

         $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";
        
        $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 public function getUgEducation()
  {
    
    try{

         $sql = "SELECT id,ugname FROM `mstpgeducation` Where groupid = 3  AND isdeleted=0";

        

        $res = $this->db->query($sql)->result();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


public function getPgEducation()
  {
    
    try{
        
        $sql = "SELECT id,pgname FROM `mstpgeducation` Where groupid = 4 AND isdeleted=0";
        
        
        $res = $this->db->query($sql)->result();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

	public function getViewSelectedCandidate($token)
	  {
	    
	    try{

	        $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";

	         if (!empty($token)) {
	          $sql .=" AND candidateid=$token";
	        }

	        $res = $this->db->query($sql)->result();

	        return $res;
	     }catch (Exception $e) {
	       print_r($e->getMessage());die;
	     }
	}



  public function getState()
   {
        
        try{

            $sql = "SELECT * FROM state where isdeleted='0' ORDER BY name";

            $res = $this->db->query($sql)->result();

            return $res;

         }catch (Exception $e) {
           print_r($e->getMessage());die;
         }
   }

  

  public function getCampus()
  {
    try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
        $res = $this->db->query($sql)->result();

        return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }

   function mail_exists($key)
  {
      $this->db->where('emailid',$key);
      $query = $this->db->get('tbl_candidate_registration');
      if ($query->num_rows() > 0){
          return true;
      }
      else{
          return false;
      }
  }


	public function GetData($userid)
	{

		try{
			$this->db->select('*');
			$this->db->from('mstuser');
			$this->db->where('mstuser.UserID', $userid);
			return $this->db->get()->row(); //echo $this->db->last_query(); die;
		
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

}