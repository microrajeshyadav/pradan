<?php 

/**
* Master  Model
*/
class Staffrecruitmentteammapping_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function staffList($recruitid)
	{
		try{
    
           $sql = "SELECT
           staff.name,
           staff_transaction.staffid,
           staff_transaction.date_of_transfer,
           staff_transaction.new_office_id,
           staff_transaction.new_designation, staff.emp_code
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

       where 1=1 and `staff`.staffid NOT IN ($recruitid)  

       ORDER BY  `staff`.name ASC ";  

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}


 public function staffDetailList($recruitid=null)
  {
    try{
    
      // echo $recruitid; die;
           $sql = "SELECT
           staff.name,
           staff.contact,
           staff.emailid,
           staff.staffid
         
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

       where 1=1 and `staff`.staffid = $recruitid  

       ORDER BY  `staff`.name ASC ";  
       $query = $this->db->query($sql);
       $res = $this->db->query($sql)->row();
       if ($query->num_rows() > 0 ) {
         return $res;
       } else {
         return 1;
       }
       

       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}


 /**
     * Method get_Listed_Recruiter() Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */


     public function get_Listed_Recruiter($currentfinancialyear)
     {
        try{

          $sql = "SELECT `mapping_staff_recruitment`.recruitmentteamid, `staff`.name FROM `mapping_staff_recruitment`
           INNER join `staff` on  `mapping_staff_recruitment`.recruitmentteamid = `staff`.staffid WHERE  `year` = '".$currentfinancialyear ."' AND `isdeleted`='0' "; 
           $res = $this->db->query($sql)->result();

           return $res;

       }catch(Exception $e){
        print_r($e->getMessage());die();
    }

}  

     /**
     * Method getRecruitment() Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */


     public function getRecruitment()
     {
        try{

         $sql = "SELECT
  mapping_staff_recruitment.id_mapping_staff_recruitment,
  staff.name,
  CASE WHEN ISNULL(c.officename) THEN '' ELSE c.officename END officename,
  CASE WHEN ISNULL(f.desname) THEN '' ELSE f.desname END desname,
  staff.emp_code,
  dc.dc_name
FROM
  `mapping_staff_recruitment`
INNER JOIN
  staff ON mapping_staff_recruitment.recruitmentteamid = staff.staffid
LEFT JOIN
  lpooffice c ON staff.new_office_id = c.officeid
LEFT JOIN
  msdesignation f ON staff.designation = f.desid
  LEFT JOIN
  dc_team_mapping d ON c.officeid = d.teamid
   LEFT JOIN
  msdc_details dc ON d.dc_cd = dc.dc_cd
WHERE
  mapping_staff_recruitment.`isdeleted` = '0'
ORDER BY
  staff.`name` DESC";

          // echo $sql; 

           $res = $this->db->query($sql)->result();

           return $res;

       }catch(Exception $e){
        print_r($e->getMessage());die();
    }

}  


    /**
     * Method getRecruitmentDetails() Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function getRecruitmentDetails()
    {
        try{
            $this->db->select('recruitmentteamid');
                $query =  $this->db->get('mapping_staff_recruitment')->result(); //echo $this->db->last_query();//die;

                $res = array();

                foreach ($query as $row)
                {
                 $res[] = $row->recruitmentteamid;
             }

             return $res;

         }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }  


     /**
     * Method getRecruitmentDeletes($token) Recruitment detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

     public function getRecruitmentDeletes($token)
     {
        try{
            $this->db->select('recruitmentteamid');
            $this->db->where('id_mapping_staff_recruitment', $token);
                $res =  $this->db->get('mapping_staff_recruitment')->result(); //echo $this->db->last_query();//die;
                return $res;


            }catch(Exception $e){
                print_r($e->getMessage());die();
            }

        }  


    /**
     * Method edit() edit detail.
     * @access  public
     * @param   $token
     * @return  string.
     */

    public function edit(){

        try{

            $RequestMethod = $this->input->server('REQUEST_METHOD');
            
            if($RequestMethod == 'POST'){

                $this->db->trans_start();
                $count = count($this->input->post('recruitmentteam'));

                $this->db->delete('mapping_staff_recruitment');

                // echo $this->db->last_query(); die;

                for ($i=0; $i < $count; $i++) { 


                    $updateArray = array(
                        'recruitmentteamid '    => $this->input->post('recruitmentteam')[$i],
                        'createdon'             => date('Y-m-d H:i:s'),    // Current Date and time
                        'createdby'             => $this->loginData->UserID, // login user id
                        'isdeleted'             => 0, 
                    );
                //print_r($updateArray); die;
                    
                    $this->db->insert('mapping_staff_recruitment', $updateArray);
                }
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE){

                    $this->session->set_flashdata('er_msg', 'Error update Recruitment');    
                }else{
                    $this->session->set_flashdata('tr_msg', 'Successfully update Recruitment');         
                }
                redirect('/Staffrecruitmentteammapping/index');

            }


            $content['staffdetails'] = $this->model->staffList();
            $content['recruitmentupdate'] = $this->model->getRecruitmentDetails();
            $content['title'] = 'add';
            $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
            $this->load->view('_main_layout', $content);

        }catch (Exception $e) {
            print_r($e->getMessage());die;
        }

    }
    



    /**
     * Method delete() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */
    public function delete($token)
    {
        try {

            $deleteArray = array(
                'isdeleted'    => 1,
            );

            $this->db->where("id_mapping_staff_recruitment",$token);
                  return ($this->db->update('mapping_staff_recruitment', $deleteArray)) ? 1 : -1; //$this->db->last_query(); die;
              }
              catch (Exception $e) {
                print_r($e->getMessage());die;
            }
        }




    }