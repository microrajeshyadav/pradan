<?php 

/**
* Joining_of_DA Model
*/
class Joining_report_of_cr_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
    $this->loginData = $this->session->userdata('login_data');
    //print_r($this->loginData);exit();
	}

	public function index()
	{

	}

/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist($token)
  {
    // echo $token;die;
    try{
      $sql = "SELECT
   des.`desname`,

    
    c.`name`,
     h.transfernno,
      h.trans_date,
    c.emailid,
    c.encrypted_signature,
    c.`emp_code`,
    b.old_office_id,
    b.new_office_id,
    b.`new_designation` AS new_designation_id,
    b.`staffid`,
    b.`id` AS transid,
    e.`officename`,
    b.`trans_flag` AS
STATUS
    ,
    b.`reason`,
    b.reportingto,
    b.`date_of_transfer` AS proposeddate,
    f.`officename` AS newoffice

FROM
  `staff_transaction` AS b
LEFT JOIN
  `staff` AS c ON b.`staffid` = c.`staffid`
left join tbl_hand_over_taken_over_charge as h on 
    b.id=h.staffid



LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON b.`new_designation` = des.`desid`
WHERE 1=1 ";
  
 //echo $this->loginData->RoleID;die();
  if ($this->loginData->RoleID ==3) {
    $sql .= " AND  b.staffid = ". $this->loginData->staffid ." AND b.`id`= ".$token; 
  }else{
    // $sql .= " AND a.integrator_ed = ". $this->loginData->staffid ." AND a.`transid`= ".$token; 
     $sql .= " AND  b.`id`= ".$token; 
  }
     //echo $sql; die();
    $res = $this->db->query($sql)->row();
    return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

 /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */
 public function get_staffDetails($token)
    {
       try
   {

      $sql="SELECT
      staff.name,
      staff.emailid,
      staff.emp_code,
  staff_transaction.staffid,
  staff_transaction.trans_status,
  staff_transaction.date_of_transfer,
  staff_transaction.new_office_id,
  staff_transaction.new_designation,
  staff_transaction.old_designation,
  staff_transaction.reportingto,
  lpooffice.officename,
  lp_old.officename as oldofficename,
  msdesignation.desname,
  staff_transaction.date_of_transfer

FROM
  staff_transaction
INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
left join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
left join lpooffice as lp_old on staff_transaction.old_office_id=lp_old.officeid
left join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";
   // echo $sql;die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffSuperior($token)
  {
    
    try{
      $sql = "SELECT b.officename, st.name,st.encrypted_signature,c.desname, a.reportingto FROM `staff_transaction`  as a
      Inner Join `staff` as st on a.reportingto = st.staffid
      Inner JOIN `lpooffice` as b on st.new_office_id = b.officeid 
      Inner JOIN `msdesignation` as c on st.designation = c.desid 
       WHERE a.id=".$token;
       // echo $sql;
       // die;
  
        $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffjoiningreport($token)
  {
    
    try{
      $sql = "SELECT * FROM `tbl_joining_report_new_place`  WHERE id=".$token;
      //echo $sql;
  
        $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
      INNER join staff as st ON u.staffid = st.staffid
      WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0"; 
      // echo $sql;
      // die;

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }







}