<?php 

/**
* Acceptance for interview  Model
*/
class Acceptance_for_interview_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}




	
  public function getOffCandidateDetails($candidateid)
  {

    try{

      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid FROM `tbl_candidate_registration` WHERE candidateid =".$candidateid;
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


 public function getApproveStatus ($candidateid)
 {

  try{

    $sql = "SELECT `confirm_attend` FROM `tbl_candidate_registration` WHERE `campustype` ='Off' AND candidateid =".$candidateid;
    $res = $this->db->query($sql)->result()[0];
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

 /*
    function getHrdemailid() is get Hrd email id 
    result row;
    created by rajat
*/
public function getHrdemailid()
{

  try{

    $sql = "SELECT
   staff.staffid,staff.name,staff.emailid
FROM
   `mstuser`
INNER JOIN staff ON mstuser.staffid = staff.staffid
inner join role_permissions on  role_permissions.RoleID=mstuser.RoleID
WHERE
   role_permissions.Controller = 'Campusinchargeintimation' and role_permissions.RoleID=16 and role_permissions.Action='index' ORDER BY
 `staff`.staffid  DESC  LIMIT 0,1";
    $res = $this->db->query($sql)->row();
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

}