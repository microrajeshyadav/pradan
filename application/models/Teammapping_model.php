<?php 

/**
* Master  Model
*/
class Campus_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Countclinicaldetails()
	{
		try{
			$this->db->select('count(Name) as clinical');
			$this->db->from('tblpatientregistrationdetails as prd ');
			$this->db->join('tblpatientclinicaltestdetails as pc', 'prd.PatientGUID = pc.PatientGUID');
			$this->db->where('prd.IsDeleted', 0);
			return $this->db->count_all_results();

		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
		
	}

	public function index()
	{

	}



    public function stateList()
    {
        try{

             $sql = "SELECT id,statecode,name FROM `state` WHERE `isdeleted`='0' ORDER BY `name` ";
             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

	public function getCampus()
	{
		try{
           
            $sql = "SELECT cam.`campusincharge`,cam.`campusname`,cam.`emailid`,cam.`city`,cam.`mobile`,cam.`telephone`,cam.`campusid`, st.`name` FROM `mstcampus` as cam INNER JOIN `state` as st 
            on cam.`stateid`=st.`id` WHERE cam.`IsDeleted`='0'";  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}


    public function getCampusDetails($token)
    {
        try{

            $sql = "SELECT * FROM `mstcampus` WHERE campusid=".$token."";
            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }

	public function patients_first_followup_done()
	{
		$sql = "SELECT * FROM `tblpatientregistrationdetails` a inner join ( select * from tblpatientreffrredfollowup where VisitNo = 1 group by PatientGUID) b on b.PatientGUID = a.patientguid where a.Referred = 1 and b.VisitNo = 1";
		$res['first_followup_done'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `tblpatientregistrationdetails` WHERE Referred = 1";
		$res['referred_patients'] = $this->db->query($sql)->result();

		return $res;
	}

	public function referred_patients_who_went_to_the_clinic()
	{
		$sql = "SELECT * FROM `tblpatientregistrationdetails` a inner join ( select * from tblpatientreffrredfollowup where VisitNo = 1 group by PatientGUID) b on b.PatientGUID = a.patientguid where a.Referred = 1 and b.VisitNo = 1";
		$res['first_followup_done'] = $this->db->query($sql)->result();

		$sql = "SELECT
    distinct PatientGUID
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)";
		$res['patients_who_went_to_the_clinic'] = $this->db->query($sql)->result();

		return $res;
	}

	public function referred_patients_who_resulted_positive()
	{
		$sql = "SELECT
    DISTINCT PatientGUID
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)

and DC_Daignosis > 0";
		$res['patients_positive_result'] = $this->db->query($sql)->result();

		$sql = "SELECT
    distinct PatientGUID
FROM
    tblpatientclinicaltestdetails
WHERE
    PatientGUID IN(
    SELECT
        a.PatientGUID
    FROM
        `tblpatientregistrationdetails` a
    INNER JOIN(
        SELECT
            *
        FROM
            tblpatientreffrredfollowup
        WHERE
            VisitNo = 1
        GROUP BY
            PatientGUID
    ) b
ON
    b.PatientGUID = a.patientguid
WHERE
    a.Referred = 1 AND b.VisitNo = 1
)";
		$res['patients_who_went_to_the_clinic'] = $this->db->query($sql)->result();

		return $res;
	}

	


    /**
     * Method delete() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */
    public function delete($token)
    {
        try {

                $deleteArray = array(
                        'IsDeleted'    => 1,
                      );

                    $this->db->where("campusid",$token);
                  return ($this->db->update('mstcampus', $deleteArray)) ? 1 : -1; //$this->db->last_query(); die;


            //$this->db->update(DISTRICT,$form); //echo $this->db->last_query(); die;
           // return ($this->db->update(state,$form)) ? 1 : -1; $this->db->last_query(); die;
        }
        catch (Exception $e) {
            print_r($e->getMessage());die;
        }
    }

	


}