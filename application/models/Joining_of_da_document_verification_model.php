<?php 

/**
* Joining_of_DA Model
*/
class Joining_of_da_document_verification_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}





/**
   * Method getCandidate_BDFFormSubmit() get BDF Form Submit Details.
   * @access  public
   * @param Null
   * @return  Array
   *  @return Status 2 - Team Join, 1- Make complete Staff
   */

  public function getCandidate_BDFFormSubmit($teamid=null)
  {
    
    try{
           $sql = "SELECT `tblvd`.comment,`tblvd`.status,`tblvd`.approvedby,`tblvd`.approvedon, `tbl_candidate_registration`.candidateid,`mstcampus`.`campusname`,`tbl_candidate_registration`.emailid,`mstcategory`.categoryname,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`staff`.name, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch,(CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid
           LEFT JOIN `staff` ON `tbl_candidate_registration`.fgid = `staff`.staffid
           LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
           left join `mstcategory` ON `mstcategory`.id =`tbl_candidate_registration`.categoryid
           left join `tbl_verification_document` as `tblvd` ON `tblvd`.candidateid =`tbl_candidate_registration`.candidateid
            Where `tbl_candidate_registration`.BDFFormStatus=1  
            AND `tbl_candidate_registration`.joinstatus=2";  //die;

           if (isset($teamid) && $teamid !='') {
            $sql .= " AND `tbl_candidate_registration`.teamid =".$teamid;
           }
           $sql .= ' ORDER BY `tbl_candidate_registration`.createdon Desc';
          // echo $sql; die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandateVerified() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandateVerified($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,
           `tbl_candidate_registration`.emailid,
           `tbl_candidate_registration`.candidatefirstname,
           `tbl_candidate_registration`.candidatemiddlename,
           `tbl_candidate_registration`.candidatelastname,
           `tbl_candidate_registration`.encryptedphotoname,
           `mstbatch`.batch,
           `tbl_candidate_registration`.categoryid,
           `tbl_candidate_registration`.encryptmetriccertificate,
           `tbl_candidate_registration`.encrypthsccertificate, 
           tbl_gap_year.encrypteddocumentsname as gapyeardocument,
           `tbl_candidate_registration`.encryptugcertificate,
           `tbl_candidate_registration`.encryptpgcertificate, 
           `tbl_candidate_registration`.encryptothercertificate, 
           `tbl_candidate_registration`.encryptofferlettername, 
           `tbl_candidate_registration`.ugmigration_encrypted_certificate_upload, `tbl_candidate_registration`.pgmigration_encrypted_certificate_upload, `tbl_candidate_registration`.ugmigration_certificate_date, 
           `tbl_candidate_registration`.pgmigration_certificate_date,
           `tbl_candidate_registration`.teamid,
           `tbl_candidate_registration`.ugdoc_certificate,
           `tbl_candidate_registration`.ugdoc_certificate_date,
           `tbl_candidate_registration`.ugdoc_encrypted_certificate_doc_upload,
           `tbl_candidate_registration`.pgdoc_certificate,
           `tbl_candidate_registration`.pgdoc_certificate_date,
           `tbl_candidate_registration`.pgdoc_encrypted_certificate_upload,
           `tbl_candidate_registration`.ugdocencryptedImage ,
           `tbl_candidate_registration`.pgdocencryptedImage 
           FROM `tbl_candidate_registration` 
           left JOIN tbl_gap_year ON tbl_candidate_registration.candidateid = tbl_gap_year.candidateid
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=2 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getSingleCandidateDetails() get single candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSingleCandidateDetails($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token;  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getCandidateTeam() get single candidate team id.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandidateTeam($token)
  {
    
    try{
           $sql = "SELECT * FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=2 ";  // die;
        
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method getDevelopmentApprenticeship() get Candate Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getDevelopmentApprenticeship($token)
  {
    
    try{
           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname FROM `tbl_candidate_registration` 
           Where `tbl_candidate_registration`.candidateid=".$token." AND `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.joinstatus=1 ";  // die;
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail($token)
  {
    
    try{

       $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=1 And `tblgnau`.status =1";  //die;
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneral_Nomination_Form_Detail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneral_Nomination_Form_Detail1($token )
  {
    
    try{

          $sql = "SELECT `tblcanreg`.candidatefirstname,`tblcanreg`.candidatemiddlename,`tblcanreg`.candidatelastname, `tblgnau`.executive_director_place,`tblgnau`.id, `tblgnau`.da_place, `tblgnau`.nomination_authorisation_signed, `tblgnau`.first_witness_name,`tblgnau`.first_witness_address, `tblgnau`.second_witness_name, `tblgnau`.second_witness_address, `tblgnau`.da_date FROM `tbl_candidate_registration`  as `tblcanreg`
            left join `tbl_general_nomination_and_authorisation_form` as `tblgnau` ON `tblgnau`.candidateid =`tblcanreg`.candidateid
          Where  `tblcanreg`.BDFFormStatus=1 AND `tblcanreg`.joinstatus=2 And `tblgnau`.status =2 AND `tblcanreg`.candidateid=".$token."";  
        
        $result = $this->db->query($sql)->result()[0];

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method getNomieeDetail() get Nomiee Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomieeDetail($id)
  {
    
    try{

        $sql = "SELECT `tblnd`.general_nomination_id,`tblnd`.nominee_name,`sysr`.relationname,`tblnd`.nominee_age,`tblnd`.nominee_address  FROM `tbl_nominee_details` as `tblnd`  
       left join `sysrelation` as `sysr` ON `sysr`.id = `tblnd`.nominee_relation Where `tblnd`.general_nomination_id = $id";  
        //die;
        $result = $this->db->query($sql)->result();

        return $result;

      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getSubmittedBy() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSubmittedBy($token)
  {
    
    try{
     $sql = 'SELECT * FROM `tbl_candidate_registration` Where `tbl_candidate_registration`.candidateid ='.$token.'';         
      $result = $this->db->query($sql)->result();
      return $result[0];
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateDetails($token)
  {
    
    try{
             $sql = 'SELECT `tbl_candidate_communication_address` .permanentstreet,`tbl_candidate_communication_address` .permanentcity,`tbl_candidate_communication_address` .permanentstateid,`tbl_candidate_communication_address` .permanentdistrict,`tbl_candidate_communication_address` .permanentpincode,
              
              b.`name` as permanentstatename,
               `dis`.*, 

            (CASE WHEN a.fileno is NULL THEN c.fileno else a.fileno END)as fileno,
    (CASE WHEN a.offerno is NULL THEN c.offerno else a.offerno END)as offerno,
    (CASE WHEN a.doj is NULL THEN c.doj else a.doj END)as doj,
    (CASE WHEN a.lastdateofacceptanceofdocs is NULL THEN c.lastdateofacceptanceofdocs else a.lastdateofacceptanceofdocs END)as lastdateofacceptanceofdocs,
    (CASE WHEN a.flag is NULL THEN c.flag else a.flag END)as flag,
    (CASE WHEN a.filename is NULL THEN c.filename else a.filename END)as filename,
    (CASE WHEN a.sendflag is NULL THEN c.sendflag else a.sendflag END)as sendflag,
    (CASE WHEN a.payscale is NULL THEN c.payscale else a.payscale END)as payscale,
    (CASE WHEN a.basicsalary is NULL THEN c.basicsalary else a.basicsalary END)as basicsalary, 
    c.transid,
    c.staffid,
    c.proposed_officeid,
    c.edcomments,
    c.edstatus,
    c.personnelId,
    b.`name` AS permanentstatename,
    `dis`.*
FROM
    `tbl_candidate_communication_address`
    
LEFT JOIN `tbl_generate_offer_letter_details` as a ON `a`.candidateid = `tbl_candidate_communication_address`.candidateid
    
LEFT JOIN `tbl_offer_of_appointment` as c ON `c`.candidateid = `tbl_candidate_communication_address`.candidateid
            left join `state` as b ON `tbl_candidate_communication_address`.permanentstateid = b.`statecode` 
     left join `district` as dis ON `tbl_candidate_communication_address`.permanentdistrict = dis .districtid
                


                Where `tbl_candidate_communication_address`.candidateid ='.$token.'';  //die;
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  
/**
   * Method getJoiningReportDetail() get Joinindieg Report Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportDetail($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_joining_report` Where candidateid = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandateWorkExperincecount($token)
  {
    
    try{

          $this->db->where('candidateid', $token);
         $res = $this->db->count_all_results('tbl_work_experience');

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method ggetCandategapyearcount() get candate gap year Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandategapyearcount($token)
  {
    
    try{

          $this->db->where('candidateid', $token);
         $res = $this->db->count_all_results('tbl_gap_year');

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceCandateVerified($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getgapyeareCandateVerified() get candate gapyear  Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getgapyeareCandateVerified($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_gap_year` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getWorkExperinceCandateVerified() get candate Work Experince Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperinceVerifiedValue($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_work_experience_verified` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result();

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getVerifiedDetailes() get Verified Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getVerifiedDetailes($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getverifiedstatus() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getverifiedstatus($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_verification_document` Where `candidateid` = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getGeneralNominationFormPDF() get Verified Status.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationFormPDF($token)
  {
    
    try{

        $sql = "SELECT status, filename FROM `tbl_general_nomination_and_authorisation_form` Where `candidateid` = $token"; //die;
       $res = $this->db->query($sql)->row();

        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getCandidateWithAddressDetails() get Select Candidates With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateWithAddressDetails($token)
  {
    
    try{

          $sql = 'SELECT `tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname, `tbl_candidate_communication_address`.permanentstreet, 
          `tbl_candidate_communication_address`.permanentcity,
           `tbl_candidate_communication_address`.permanentstateid, 
           `tbl_candidate_communication_address`.permanentdistrict, 
           `tbl_candidate_communication_address`.permanentpincode,
            b.`name` as permanentstatename, 
            `dis`.* FROM `tbl_candidate_registration` 
         left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
           left join `state` as b ON `tbl_candidate_communication_address`.permanentstateid = b.`statecode` 
     left join `district` as dis ON `tbl_candidate_communication_address`.permanentdistrict = dis .districtid
                
                Where `tbl_candidate_registration`.candidateid ='.$token.''; 
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getViewGeneralnominationform() get View General Nomination  Authorisation Form Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewGeneralnominationform($token)
  {
    
    try{

       $sql = 'SELECT * FROM `tbl_general_nomination_and_authorisation_form`         
                Where `tbl_general_nomination_and_authorisation_form`.candidateid ='.$token.''; 
          $query   = $this->db->query($sql);
          $numrows = $query->num_rows();
          $result  = $this->db->query($sql)->result();

          if ($numrows > 0 ) {
           return $result[0];
          }else{
             return 1;
          }

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getCountOtherDocument() get Count Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountOtherDocument($token)
  {
    
    try{
        
        $this->db->where('candidateid', $token);
        $result = $this->db->count_all_results('tbl_other_documents');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getOtherDocumentDetails() get Other Document Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocumentDetails($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_other_documents`         
                Where candidateid ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getNomineedetail() get Count Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountNominee($token)
  {
    
    try{
        
        $this->db->where('general_nomination_id', $token);
        $result = $this->db->count_all_results('tbl_nominee_details');

        //echo $result; die;
        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getNomineedetail() get Select Nominee With Address Details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getNomineedetail($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_nominee_details`         
                Where `tbl_nominee_details`.general_nomination_id ='.$token.''; 
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSysRelations()
  {

    try{
      $this->db->select('*');
      $this->db->from('sysrelation');
      $this->db->where('status', 0);
      $this->db->where('isdeleted', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getOtherDocuments() get Document Name With Type List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getOtherDocuments()
  {

    try{
      $this->db->select('*');
      $this->db->from('mstdocuments');
      $this->db->where('status', 0);
      return $this->db->get()->result();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }



/**
   * Method getJoiningReportStatus() get Joining Report Status List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportStatus($token)
  {

    try{
      $this->db->select('status, filename');
      $this->db->from('tbl_joining_report');
      //$this->db->where('status', 1);
      $this->db->where('candidateid', $token);

      return $this->db->get()->row();

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getSingleNameWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSingleNameWitnessDeclaration($id)
  {

    try{
           $sql = "SELECT `staffid`,`name` FROM `staff` as a WHERE `a`.staffid=".$id; 
            
            $result = $this->db->query($sql)->result()[0];

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

/**
   * Method getWitnessDeclaration() get Staff List.
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getWitnessDeclaration($id)
  {

    try{
        $sql = "SELECT staff_transaction.staffid, staff_transaction.date_of_transfer, staff_transaction.new_office_id, staff_transaction.new_designation, staff.name FROM staff_transaction 
    INNER JOIN  staff ON staff_transaction.staffid = staff.staffid  
    INNER JOIN (SELECT staff_transaction.staffid,Max(staff_transaction.date_of_transfer) as MaxDate FROM staff_transaction GROUP BY staffid) as TMax ON staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
      AND staff_transaction.trans_status NOT IN('Resign','Termination','Retirement','Death') AND staff_transaction.`new_office_id`='".$id."' AND  staff_transaction.`new_designation` IN(4,10) ORDER BY staff_transaction.staffid ";  
            ///AND staff_transaction.`new_office_id`='".$id."';
            $result = $this->db->query($sql)->result();

            return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }


  /**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      // echo $sql;
       $res = $this->db->query($sql)->row();
      // print_r($res);
      // die;
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->row();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



  /**
   * Method getGeneralNominationWithWitnessformFormUploadStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralNominationWithWitnessformFormUploadStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.filename as filename FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



  /**
   * Method getJoiningReportFormPdfStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportFormPdfStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.filename as filename FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



public function tc_data($teamid)
{
  try{

      $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emailid,
   `staff`.`designation`
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '$teamid' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
ORDER BY
  `staff_transaction`.staffid  DESC  LIMIT 0,1
 ";
 //echo $sql;

      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


public function hr_data()
{
  try{

      $sql = "SELECT
    staff.staffid,staff.name,staff.emailid
FROM
    `mstuser`
INNER JOIN staff ON mstuser.staffid = staff.staffid
inner join role_permissions on  role_permissions.RoleID=mstuser.RoleID
WHERE

    role_permissions.Controller = 'Campusinchargeintimation' and role_permissions.RoleID=16 and role_permissions.Action='index' ORDER BY
  `staff`.staffid  DESC  LIMIT 0,1
 ";
 //echo $sql;

      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }






}