<?php 

/**
* Master  Model
*/
class Employeebasicsalary_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }
  public function index()
  {

  }
  public function getStaffList($officeid =null, $incrementmonth =null)
  {
    try{

//       $sql1="SELECT tbl_work_experience.candidateid,staff.staffid,tbl_work_experience.fromdate,tbl_work_experience.todate FROM `tbl_work_experience`  
// inner join staff on staff.candidateid=tbl_work_experience.candidateid
// inner join lpooffice on staff.staffid=lpooffice.staffid
// inner join msdesignation on staff.designation=msdesignation.desid
// left join trnleave_ledger  as lt on staff.Emp_code=lt.Emp_code 
//    WHERE staff.STATUS = 1 and staff.designation <> 13";
//    if($officeid){
//         $sql1 .=" AND lpooffice.officeid= case when ".$officeid." =0 then lpooffice.officeid else ".$officeid." end";
//       }

//       $sql1 .=" group by tbl_work_experience.fromdate"; 
//       $res1 = $this->db->query($sql1)->result();
//       $exp=0;
//       if(!empty($res1))
//       {
//         for($i=0;$i<count($res1);$i++)
//         {
//         $todate= date("Y-m-d", strtotime($res1[$i]->todate)); 

//         $fromdate= date("Y-m-d", strtotime($res1[$i]->fromdate)); 
//           $diff = abs(strtotime($todate) - strtotime($fromdate));
//          $years = floor($diff / (365*60*60*24));
//          $exp=$exp+$years;
         
        
         
//         }
        
//       }
//       else {
//         $exp=0;
//       }
//       echo $exp;
    




      $fyear = $this->gmodel->getcurrentfyear();
      
      $sql = "SELECT inc.* ,twe.exp1 as e,CASE when floor(twe.exp1+(CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE CAST(
DATEDIFF(
'".$incrementmonth."', EXP.Date_of_transfer
) / 365 as decimal(5, 1)
) END)) IS NULL THEN 0 else floor(twe.exp1+(CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE CAST(
DATEDIFF(
'".$incrementmonth."', EXP.Date_of_transfer
) / 365 as decimal(5, 1)
) END)) END as act_exp,staff.staffid, NAME, doj, staff.emp_code, staff.designation, ifnull(inc.basicsalary,0.00) AS e_basic, ifnull(inc.basicsalary,0.00)  + inc.increment + inc.Slide AS pre_e_basic, msdesignation.desid, msdesignation.desname, lp.officename AS office, CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE CAST(
    DATEDIFF('".$incrementmonth."',
    EXP.Date_of_transfer) / 365  as decimal(5,1)) END AS noofyearexp, LWPD.LWP,DLWP.lwp as dlwp, CAST(( CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE  DATEDIFF('".$incrementmonth."', EXP.Date_of_transfer) END - IFNULL(LWPD.LWP, 0))/ 365  as decimal(5,1)) ndays, msdesignation.level FROM staff 
   left JOIN(
   SELECT DATEDIFF(t1.todate,t1.fromdate)/365 as exp1, t1.candidateid  FROM 
    (SELECT
    min(fromdate) as fromdate ,max(todate) as todate,
       candidateid
    FROM
        tbl_work_experience GROUP by candidateid)t1
) AS twe
ON
    staff.candidateid = twe.candidateid

    LEFT JOIN
      (
      SELECT
        staff.Staffid,
        case when st1.Date_of_transfer is NULL then st.Date_of_transfer else st1.Date_of_transfer end Date_of_transfer
      FROM
        `staff`

      INNER JOIN
        staff_transaction st ON staff.staffid = st.staffid AND st.new_designation != 13 AND st.trans_status = 'JOIN'
        lEFT JOIN staff_transaction_old as st1 on st.staffid = st1.staffid AND st1.new_designation != 13 AND st1.trans_status = 'JOIN' 
    ) EXP ON staff.staffid = EXP.staffid

      INNER JOIN msdesignation ON staff.designation = msdesignation.desid 
     LEFT JOIN (SELECT
      `trnleave_ledger`.`Emp_code`,SUM(
  IFNULL(
    CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 5 AND
  STATUS = 3  THEN Noofdays ELSE 0
  END,
  0)) LWP FROM
  `trnleave_ledger` GROUP BY  `trnleave_ledger`.`Emp_code`) as LWPD ON LWPD.Emp_code = staff.emp_code
  LEFT JOIN (SELECT
    `trnleave_ledger`.`Emp_code`,
    SUM(
      IFNULL(
        CASE WHEN Leave_transactiontype = 'CR' AND Leave_type = 100 AND Description = 'DLWP'
       THEN Noofdays ELSE 0
      END,
      0
    )
) LWP
FROM
  `trnleave_ledger` WHERE From_date BETWEEN DATE_SUB('".$incrementmonth."', INTERVAL 365 DAY) AND '".$incrementmonth."'
GROUP BY
  `trnleave_ledger`.`Emp_code`) dlwp
  ON dlwp.Emp_code = staff.emp_code

 LEFT JOIN tblsalary_increment_slide as inc ON inc.level = msdesignation.level AND CASE when floor(twe.exp1+(CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE CAST(
DATEDIFF(
'".$incrementmonth."', EXP.Date_of_transfer
) / 365 as decimal(5, 1)
) END)) IS NULL THEN 0 else floor(twe.exp1+(CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE CAST(
DATEDIFF(
'".$incrementmonth."', EXP.Date_of_transfer
) / 365 as decimal(5, 1)
) END)) END  = inc.noofyear and fyear = '". $fyear ."'
      left JOIN `lpooffice` AS lp ON staff.new_office_id = lp.officeid 
      LEFT JOIN tblmstemployeesalary empsl ON staff.staffid = empsl.staffid

      left join (select payscale, level from msdesignation group BY payscale, level) d on inc.level = d.level left join (select max(netsalary) maxnetsalary , level from tblsalary_increment_slide group by level) maxi on inc.level = maxi.level


       WHERE staff.STATUS = 1 and staff.designation <> 13";

      // echo  $sql; die();
      // if(date("m",strtotime($incrementmonth)) == 04){        
      //   $sql .= " AND MONTH(doj) in(4,5,6,7,8,9)";
      // }elseif(date("m",strtotime($incrementmonth)) == 10){
      //   $sql .= " AND MONTH(doj) in(10,11,12,1,2,3)";
      // }

       if(date("m",strtotime($incrementmonth)) == 04){        
        $sql .= " AND MONTH(doj) in(4,5,6,7,8,9)";
      }elseif(date("m",strtotime($incrementmonth)) == 10){
        $sql .= " AND MONTH(doj) in(10,11,12,1,2,3)";
      }

     
      if($officeid){
        $sql .=" AND lp.officeid= case when ".$officeid." =0 then lp.officeid else ".$officeid." end";
      }

      $sql .=" order by name"; 
// echo $sql; 
 $res = $this->db->query($sql)->result();
 // echo "<pre>";
// print_r($res); die();
  return $res;
  // die();

      



    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }
  public function getOfficeList($p_office=null)
  {
     try
   {
    //echo "office id=".$p_office;
   // die;
    $this->db->select("officeid,officename");
    $this->db->from('lpooffice');
    $query=$this->db->get();
     // echo $this->db->last_query();
      //die();
      return $query->result();
    }
     

     catch (Exception $e) 
     {
       print_r($e->getMessage());die;
     }

  }

  public function CreateHistory($period, $wherecondition){
    $this->db->query("delete FROM tblhistoryemployeesalary where period=".$period);

    
    $query = "INSERT INTO `tblhistoryemployeesalary`( `staffid`, `e_basic`, `e_01`, `e_02`, `e_03`, `e_04`, `e_05`, `e_06`, `e_07`, `e_08`, `e_09`, `e_10`, `e_11`, `e_12`, `e_13`, `e_14`, `e_15`, `d_pf`, `d_vpf`, `d_01`, `d_02`, `d_03`, `d_04`, `d_05`, `d_06`, `d_07`, `d_08`, `d_09`, `d_10`, `d_11`, `d_12`, `d_13`, `d_14`, `d_15`, `bankcode`, `bankacno`, `deletedemployee`, `issalgenrated`, `c_totearn`, `c_netsal`, `c_grosssalary`, `period` ) 
    Select `staffid`, `e_basic`, `e_01`, `e_02`, `e_03`, `e_04`, `e_05`, `e_06`, `e_07`, `e_08`, `e_09`, `e_10`, `e_11`, `e_12`, `e_13`, `e_14`, `e_15`, `d_pf`, `d_vpf`, `d_01`, `d_02`, `d_03`, `d_04`, `d_05`, `d_06`, `d_07`, `d_08`, `d_09`, `d_10`, `d_11`, `d_12`, `d_13`, `d_14`, `d_15`, `bankcode`, `bankacno`, `deletedemployee`, `issalgenrated`, `c_totearn`, `c_netsal`, `c_grosssalary`, ".$period." `period` FROM tblmstemployeesalary ".$wherecondition;
  }


}