<?php 
class Global_Model extends MY_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->file_path = realpath(APPPATH . '../datafiles');
		$this->banner_path = realpath(APPPATH . '../banners');
		$this->gallery_path_url = base_url().'datafiles/';
	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


public function changedate($Date)
{

  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
  // print_r($date); die;
 if($pattern == "-" )
  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
else
  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
if ($date=='//') {
  $date= NULL;
}
return $date;
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
//echo strlen($Date);
    $originalDate = $Date;
    $Date = explode(" ",$Date);
    $onlydate = $Date[0];
    if (count($Date) ==1 ) {
      $len = (strlen($onlydate)-5); 

      if(substr($onlydate,$len,-4)=="/")
       $pattern = "/";
     else
       $pattern = "-";

     $date = explode($pattern,$onlydate);
   //print_r($date); die;
 //  echo $pattern;
     if($pattern == "/" )
      $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
    else
      @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
    if ($date=='//') {
      $date= NULL;
    }
    return $date;
  }
  else
  {
    $len = (strlen($onlydate)-5);
    
    if(substr($onlydate,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";
   $date = explode($pattern,$onlydate);

   if($pattern == "/" )
    $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  if ($date=='//') {
    $date= NULL;
  } 
  $date = $date ." ".  $Date[1];
  return $date;
}


}



/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


public function changedateFormateExcludeTime($Date)
{

  if (trim($Date) == '' || substr($Date,0,10) == '0000-00-00') {
    return '';
  }else{
   $date =  date("d/m/Y", strtotime($Date)); 
 }

 return $date;
}


/*
    function roundvalue() is get round leves  
    result row;
    created by pooja
*/

    public function roundvalue($val)
    {
      $decimal = strstr($val,'.');
      if($decimal == '')
      {
        return $val;
      }
      else
      {
        $pos = strpos($val,'.');
        $firstdecimal = substr($val,$pos+1,1);
        $startval = substr($val,0,$pos);
        $seconddecimal = substr($val,$pos+2,2);
        if($firstdecimal == '5')
        {
          return $startval.'.'.$firstdecimal;
        }
        else
        {
          if($seconddecimal != '')
          {
            $decimalvalue = '0.'.$firstdecimal.$seconddecimal;  
          }
          else
          {
            $decimalvalue = '0.'.$firstdecimal.'0';   
          }
          if($decimalvalue <= '0.24')
          {
            return $startval;
          }
          elseif($decimalvalue > '0.24' and $decimalvalue <= '0.74')
          {
            return $startval.'.5';
          }
          elseif($decimalvalue > '0.74' and $decimalvalue <= '0.9999')
          {
            return $startval+1;
          }
        }
      }
    }


    /**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   * created by rajat
   */

public function getCandidateDetails($id)
{

  try{

   $sql = 'SELECT * FROM `tbl_candidate_registration` Where `tbl_candidate_registration`.candidateid =?'; 


   $result = $this->db->query($sql,$id)->result();

   return $result[0];

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


    /**
   * Method getReportingTo() get reporting office name.
   * @access  public
   * @param Null
   * @return  Array
   * created by rajat
   */

 public function getReportingTo($id)
  {
    
    try{

  $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name, 
   `staff`.emailid
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  ) 
  AND staff_transaction.`new_office_id` = '$id' 
  AND staff_transaction.`new_designation` IN(4,16) 
ORDER BY 
  `staff_transaction`.staffid ";   
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/*
    function getcurrentfyear() is get current financial year 
    result row;
    created by pooja
*/

    public function getcurrentfyear(){

      if(date('m') >= 04) {
        $d = date('Y-m-d', strtotime('+1 years'));
        $currentyear =  date('Y') .'-'.date('Y', strtotime($d)); 
      } else { 
        $d = date('Y-m-d', strtotime('-1 years'));
        $currentyear = date('Y', strtotime($d)).'-'.date('Y');
      }

      return $currentyear;

    }


/*
    function getHRDEmailid() is get HR Administrator email id 
    result row;
    created by pooja
*/

    public function getHRDEmailid(){

      try{

        $sql = "SELECT `mstuser`.EmailID as hrdemailid, `staff`.`name`, `staff`.`designation`, des.desname  FROM mstuser 
        INNER JOIN `staff` on `mstuser`.staffid = `staff`.staffid
INNER JOIn `msdesignation` des on staff.designation = des.desid
        WHERE mstuser.RoleID = 16 AND mstuser.IsDeleted = 0 AND staff.status=1 and  `staff`.`designation` = 4";
        $result = $this->db->query($sql)->row(); 
        return $result;

      }catch (Exception $e) {
        print_r($e->getMessage());die;
      }
    }

/*
    function getPAEmailid() is get Personnel Administrator Email id 
    result row;
    created by pooja
*/

    public function getPersonnelAdministratorEmailid(){

      try{

       $sql = "SELECT `mstuser`.EmailID as personnelemailid,`staff`.name as name,staff.staffid as staff_id FROM mstuser 
       INNER JOIN `staff` on `mstuser`.staffid = `staff`.staffid
       WHERE mstuser.RoleID = 17 AND mstuser.IsDeleted = 0 AND staff.status=1";
       // echo $sql;
       // die;
       $result = $this->db->query($sql)->row();
       return $result;

     }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

  /*
    function getExecutiveDirectorEmailid() is get Executive Director email id
    result row;
    created by pooja
*/

    public function getExecutiveDirectorEmailid(){

      try{

       $sql = "SELECT `mstuser`.EmailID as edmailid,`staff`.encrypted_signature as sign, `staff`.name as edname FROM mstuser 
       INNER JOIN `staff` on `mstuser`.staffid = `staff`.staffid
       WHERE mstuser.RoleID = 18 AND mstuser.IsDeleted = 0 AND staff.status=1";
       $result = $this->db->query($sql)->row();
       return $result;

     }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }



   /*
    function getFinanceAdministratorEmailid() is get Finance Administrator email id
    result row
    created by pooja
*/

    public function getFinanceAdministratorEmailid(){

      try{

       $sql = "SELECT staff.staffid,staff.name,`mstuser`.EmailID as financeemailid FROM mstuser 
       INNER JOIN `staff` on `mstuser`.staffid = `staff`.staffid
       WHERE mstuser.RoleID = 20 AND mstuser.IsDeleted = 0 AND staff.status=1";
       $result = $this->db->query($sql)->row();
       return $result;

     }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }
   public function getpersonalAdministrator_details(){

      try{

       $sql = "SELECT staff.staffid ,staff.name,`mstuser`.EmailID as financeemailid, msdesignation.desname,lpooffice.officename,staff.emp_code FROM mstuser 
       INNER JOIN `staff` on `mstuser`.staffid = `staff`.staffid
       inner join msdesignation on staff.designation=msdesignation.desid
       inner join lpooffice on staff.new_office_id=lpooffice.officeid
       WHERE mstuser.RoleID = 17 AND mstuser.IsDeleted = 0 AND staff.status=1";
       $result = $this->db->query($sql)->row();
       return $result;

     }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

  }

  public function do_uploadd($element_name = 'userfile', $allowed_types = ['jpg','png'])
  {
   echo "hello"   ;
   
   $config = array(
     'allowed_types'    =>    'jpg|jpeg|png|gif',
     'upload_path'      =>    FCPATH . "datafiles/nominee/",
     'max_size'         =>    10000,
     'encrypt_name'     =>    TRUE,
     'file_ext_tolower' =>    TRUE
   );


   $this->load->library('upload',$config);
   if(!$this->upload->do_upload('signatureplace'))
   {
     echo $this->upload->display_errors();

   }

 else   //if upload is successfully
 {
   $fileData=$this->upload->data();

   //die();
   return $fileData;
 }
}





// public function get

}
