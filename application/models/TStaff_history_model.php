<?php 

/**
* Staff history Model
*/
class TStaff_history_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


 /**
   * Method getStaff_Transfer_History() get single staff history.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaff_Transfer_History($rid)
  {
    
    try{
    
  $sql = "SELECT 
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  mdd.desname,
  ms.Acclevel_Name as sendername, 
  ms1.Acclevel_Name as recivername,
  a.`id` as transid,
  e.`officename`,
  a.`trans_flag` as status,
  a.`reason`,
  a.`date_of_transfer` as proposeddate,
  b.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.`flag` = 1 THEN 'Process Initiate' 
    WHEN b.`flag` = 4 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`flag` = 5 THEN 'Accepted By Personnel Unit' 
    WHEN b.`flag` = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.`flag` = 6 THEN 'Rejected By Personnel Unit' 
     WHEN b.`flag` = 7 THEN 'Accepted Clearance Certificated By TC/Intergrator'
     WHEN b.`flag` = 8 THEN 'Rejected Clearance Certificated By TC/Intergrator' 
      WHEN b.`flag` = 9 THEN 'Accepted Clearance Certificated By Personnel Unit'
     WHEN b.`flag` = 10 THEN 'Rejected Clearance Certificated By Personnel Unit' 
       WHEN b.`flag` = 11 THEN 'Handing Over Charge By Staff' 
    WHEN b.`flag` = 12 THEN 'Handing Over Charge Request not submited' 
     WHEN b.`flag` = 13 THEN 'Taken Over Accepted By Staff' 
    WHEN b.`flag` = 14 THEN 'Taken Over Rejected By Staff' 
    WHEN b.`flag` = 15 THEN 'PLACE OF POSTING Accepted By TC/Intergrator' 
    WHEN b.`flag` = 16 THEN 'PLACE OF POSTING Rejected By TC/Intergrato' 
    WHEN b.`flag` = 17 THEN 'TRANSFER EXPENSES Request submited' 
    WHEN b.`flag` = 18 THEN 'TRANSFER EXPENSES Request Not submited' 
     WHEN b.`flag` = 19 THEN 'TRANSFER EXPENSES Accepted By TC/Intergrator' 
    WHEN b.`flag` = 20 THEN 'TRANSFER EXPENSES Rejected By TC/Intergrator' 
     WHEN b.`flag` = 21 THEN 'Accepted By Finance Unit' 
    WHEN b.`flag` = 22 THEN 'Rejected By Finance Unit' 

    ELSE 'Not status here'
  END
) AS flag
 FROM `staff_transaction` AS a
LEFT JOIN
  `tbl_workflowdetail` AS b ON a.`id` = b.`r_id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
  LEFT JOIN
 `mst_workflow_process` AS d ON b.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON b.`sender` = k.`staffid`
   LEFT JOIN
  `staff` AS g ON b.`receiver` = g.`staffid`
LEFT JOIN
  `lpooffice` AS e ON a.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON a.`new_office_id` = f.`officeid`
  LEFT JOIN
  `msdesignation` AS md ON k.`designation` = md.`desid`
  LEFT JOIN
  `msdesignation` AS mdd ON g.`designation` = mdd.`desid`


  inner join mstuser m on b.sender=m.staffid 
   inner join mst_workflow_process_stages as mp on m.RoleID=mp.sender_roleid And b.flag=mp.stage
  inner join sysaccesslevel ms on m.RoleID=ms.Acclevel_Cd

  
  inner join mstuser m1 on b.receiver=m1.staffid
  inner join mst_workflow_process_stages as ms2 on m1.RoleID=ms2.receiver_roleid And ms2.stage=b.flag
  inner join sysaccesslevel ms1 on m1.RoleID=ms1.Acclevel_Cd
  Where b.`r_id` =".$rid."   group by b.createdon Order BY b.createdon ASC"; 
  // echo $sql;
  // die;        
  

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}