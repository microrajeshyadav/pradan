<?php 

/**
* Team_coordinator_dashboard Dashboard Model
*/

class Team_coordinator_dashboard_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}


    public function getTeamlist()
    {
        try{
           
            $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No' ORDER BY `officename`";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }


    public function getBatchlist()
    {
        try{
           
            $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }



    public function getCampus()
    {
        try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }



  public function getSelectedCandidate()
  {
    
    try{

           $sql = "SELECT *  FROM `tbl_daship_component` INNER JOIN 
           `tbl_candidate_registration` ON `tbl_daship_component`.candidateid = `tbl_candidate_registration`.candidateid  
           Where `tbl_daship_component`.candidateid !='' ";   //die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function getRecruitersList()
  {
    
    try{

        $sql = "SELECT  `staff`.staffid, `staff`.name FROM `mapping_campus_recruiters` Inner join staff on `mapping_campus_recruiters`.`recruiterid`= `staff`.staffid ";
        $res = $this->db->query($sql)->result();
        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function getComments()
  {
    
    try{

        $sql = "SELECT  `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `lpooffice`.officename, `mstbatch`.batch,`staff`.name,
        `edcomments`.comments 
                FROM `tbl_candidate_registration`
            left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid
            left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid
            left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid 
             left join `edcomments` ON `edcomments`.candidateid=`tbl_candidate_registration`.candidateid ";
       
        $res = $this->db->query($sql)->result();
        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  
 public function getCandidateDetails($token)
  {
    
    try{

         $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_generate_offer_letter_details`.filename 
          FROM `tbl_candidate_registration` LEFT join `tbl_generate_offer_letter_details` ON `tbl_candidate_registration`.candidateid  = `tbl_generate_offer_letter_details`.candidateid
            Where `tbl_candidate_registration`.candidateid = '".$token."' "; 

        $result = $this->db->query($sql)->result();

        return $result;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
	

	

	


}