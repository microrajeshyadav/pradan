<?php 

/**
* State Model
*/
class Staff_hr_midterm_review_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    public function get_candidaateid($staff_id)
    {
        try{
        $sql="select * from staff where staffid='$staff_id'";
        
        return  $this->db->query($sql)->row();
        }catch (Exception $e) {
    print_r($e->getMessage());die;
    }
    }


	public function getmidtermreviewworkflowList()
	{
    try{

    $currentdate = date('Y-m-d');


		$sql = "SELECT staff.name,
  staff.staffid,
  staff.probation_date,
  staff.doj,
  staff.gender,
  staff.emp_code,
  staff.candidateid,
  staff.designation,
  lpooffice.officename,
  msdesignation.desname,
  w.`r_id` as rid,
  msdesignation.level_name AS levelname,
  staff.gender,
  (
    CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
  END
) AS gender,

(
  CASE WHEN w.flag = 1 THEN 'Submitted By HRD' WHEN w.flag = 3 
  THEN 'Approve By Team Coordinator' WHEN w.flag = 4 THEN 'Reject By Team Coordinator' WHEN w.flag = 5 THEN 'Approve By Personnal' WHEN w.flag = 6 THEN 'Reject By Personnal' WHEN w.flag = 7 THEN 'Approve By ED'  WHEN w.flag = 8 THEN 'Reject By ED' WHEN w.flag = 25 THEN 'Approve By HRD' WHEN w.flag = 26 THEN 'Reject By HRD' ELSE 'Not status here'
END) AS status
    FROM staff
  
INNER JOIN
  lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
INNER JOIN
  msdesignation ON staff.`designation` = msdesignation.`desid`
INNER JOIN
  tbl_workflowdetail as w ON staff.`staffid` = w.`staffid`
INNER JOIN
  tbl_probation_review_performance AS probre ON staff.`staffid` = probre.`staffid`

    where msdesignation.`level_name`='level-4' AND `staff`.flag = 0 ";  

    if ($this->loginData->RoleID == 16) {
      $sql .= " AND w.`receiver` =".$this->loginData->staffid;  
    }
    $sql .= " GROUP BY `staff`.staffid  order by staff.staffid ASC ";  
   // echo $sql;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



    
    /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getProbationStatus($staffid)
  {
    
    try{
   $sql = "SELECT flag FROM `tbl_probation_review_performance` where `staffid`=".$staffid;

         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}