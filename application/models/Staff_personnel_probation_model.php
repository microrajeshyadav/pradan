<?php 

/**
* State Model
*/
class Staff_personnel_probation_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    public function get_candidaateid($staff_id)
    {
        try{
        $sql="select * from staff where staffid='$staff_id'";
        
        return  $this->db->query($sql)->row();
        }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
    }


	public function getmidtermreviewworkflowList()
	{
      try{

  $currentdate = date('Y-m-d');

	$sql = "SELECT staff.name,
  staff.staffid,
  staff.probation_date,
  staff.doj,
  staff.gender,
  staff.emp_code,
  staff.candidateid,
  staff.designation,
  lpooffice.officename,
  msdesignation.desname,
  w.`r_id` as rid,
  msdesignation.level_name AS levelname,
  staff.gender,
  (
    CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
  END
) AS gender,

(
  CASE WHEN w.flag = 1 THEN 'Submitted By HRD' WHEN w.flag = 3 
  THEN 'Approve By Team Coordinator' WHEN w.flag = 4 THEN 'Reject By Team Coordinator' WHEN w.flag = 5 THEN 'Approve By Personnal' WHEN w.flag = 6 THEN 'Reject By Personnal' WHEN w.flag = 7 THEN 'Approve By ED'  WHEN w.flag = 8 THEN 'Reject By ED' WHEN w.flag = 25 THEN 'Approve By HRD' WHEN w.flag = 26 THEN 'Reject By HRD' ELSE 'Not status here'
END) AS status
    FROM staff
  
INNER JOIN
  lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
INNER JOIN
  msdesignation ON staff.`designation` = msdesignation.`desid`
INNER JOIN
  tbl_workflowdetail as w ON staff.`staffid` = w.`staffid`
INNER JOIN
  tbl_probation_review_performance AS probre ON staff.`staffid` = probre.`staffid`
where msdesignation.`level` = 4 AND `staff`.flag = 0 ";  

  if ($this->loginData->RoleID == 17) {
      $sql .= " AND w.`receiver` =".$this->loginData->staffid;  
    }

    $sql .= " GROUP BY `staff`.staffid  order by staff.staffid ASC ";  
    //echo $sql;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

 public function getworkflowdetaillist()
  {
    
    try{

      //print_r($this->loginData); die;
      
  $sql = "SELECT c.`name`, c.`emp_code`, d.`process_type`, a.`staffid`, t.id , k.`name` as sendername, a.`flag` as status, a.`scomments`, a.`r_id`, a.`createdon` AS Requestdate, ( CASE 
  WHEN a.flag = 1 THEN 'Process Initiate By Personnel'
 
  WHEN a.flag = 3 and b.probation_extension_date is not null THEN 'Review Submitted by TC' 
  WHEN a.flag = 3 and b.satisfactory = 'satisfactory'  THEN 'Review Submitted by TC'
  WHEN a.flag = 3 and b.satisfactory = 'notsatisfactory'  THEN 'Review Submitted by TC'
  WHEN a.flag= 4 THEN 'Disagreed By TC/Intergrator' 
  WHEN a.flag = 5 THEN 'Agreed By ED' 
  WHEN a.flag = 6 THEN 'Disagreed By ED' 
  WHEN a.flag = 7 THEN 'Agreed by Personnel' 
  WHEN a.flag = 8 THEN 'Disagreed by Personnel'
  WHEN a.flag = 9 and b.probation_extension_date is not null THEN 'Letter Sent to Staff' 
  WHEN a.flag = 9 and b.satisfactory = 'satisfactory'  THEN 'Letter Sent to Staff'
  WHEN a.flag = 9 and b.satisfactory = 'notsatisfactory'  THEN 'Letter Sent to Staff'
  ELSE 'Not status here' 
  END ) AS flag,
case when b.probation_extension_date is not null THEN 'Probation - Extend' 
  WHEN b.satisfactory = 'satisfactory'  THEN 'Probation - Complete'
  WHEN b.satisfactory = 'notsatisfactory'  THEN 'Probation - Separation' WHEN t.`trans_flag` = 1  THEN 'Pending' end
 
 Request
   FROM `tbl_workflowdetail` AS a LEFT JOIN `tbl_probation_review_performance` AS b ON a.`r_id` = b.`transid` and a.workflowid in (Select Max(workflowid) workflowid from tbl_workflowdetail group by staffid,r_id ) and a.staffid =b.staffid  LEFT JOIN `staff` AS c ON a.`staffid` = c.`staffid` 
LEFT JOIN `staff_transaction` AS t ON a.`staffid` = t.`staffid` AND a.flag = t.trans_flag and a.r_id=t.id
LEFT JOIN `mst_workflow_process` AS d ON a.`type` = d.`id`  LEFT JOIN `staff` AS k ON a.`receiver` = k.`staffid`  Where 1=1 AND d.process_type='Probation' and 1 = (case when a.flag =9 and date_add(CURRENT_DATE() , INTERVAL - 10 DAY)  <= t.updatedon  then 1 when  a.flag !=9  then 1 else 0 end) "; 
  
  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }
   $sql.= " GROUP BY staffid, a.r_id asc";

  //echo $sql; die;

  // Where a.sender = ".$this->loginData->staffid." and a.createdby =".$this->loginData->staffid;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




public function getprobationdetails()
  {
    try{

  $currentdate = date("Y-m-d", strtotime("-1 month")); 

  $sql = "SELECT
  staff.name,
  staff.staffid,
  staff.probation_date,
  staff.probation_status,
  staff.probation_completed,
  staff.doj,
  staff.gender,
  staff.emp_code,
  staff.candidateid,
  staff.designation,
  staff.probation_date,
  staff.probation_completed,
  lpooffice.officename,
  msdesignation.level,
  msdesignation.desname,
  msdesignation.level_name AS levelname,
  staff.gender,
  (
    CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
  END
) AS gender
FROM
  staff
INNER JOIN
  lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
INNER JOIN
  msdesignation ON staff.`designation` = msdesignation.`desid`
WHERE
  (msdesignation.`level` = 4 or  msdesignation.`level` = 2 )
  AND  CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date END between CURRENT_DATE() and date_add(CURRENT_DATE() , INTERVAL 30 DAY)  and staff.probation_completed = 0";  

//$sql .= " GROUP BY `staff`.staffid  order by staff.staffid ASC ";  
     //echo $sql; 
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

    
    /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getProbationStatus($staffid)
  {
    
    try{
   $sql = "SELECT flag FROM `tbl_probation_review_performance` where `staffid`=".$staffid;

         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}