<?php 

/**
* Master  Model
*/
class Office_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
      
       
	}   

	  /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_clearance_detail($token)
    {
       try
   {

      $sql="SELECT c.name,a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code

        FROM staff_transaction as a 
        INNER JOIN `staff` as c on a.staffid = c.staffid
        INNER JOIN `msdesignation` as b on a.new_designation = b.desid

      WHERE a.id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_detail($token)
    {
       try
   {

      $sql="SELECT a.id, a.transid, a.separation_due_to, a.flag,
       (
      CASE a.flag WHEN 2 THEN 'Transfer' WHEN 3 THEN 'Seperation' END
      ) AS type

        FROM  tbl_clearance_certificate as a 
      WHERE a.id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_transaction($token)
    {
       try
   {

      $sql="SELECT a.description, a.project

        FROM  tbl_clearance_certificate_transaction as a 
      WHERE a.clearance_certificate_id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


 
}