<?php 

/**
* State Model
*/
class Staff_approval_changerespons_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getworkflowdetaillist()
  {
    
    try{

    $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  e.`officename`,
  b.`trans_flag` as status,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' WHEN b.trans_flag = 4 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit' WHEN b.trans_flag = 3 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where 1=1 AND b.`trans_status` ='Transfer' ";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
  	//$sql .= " AND a.receiver =".$this->loginData->staffid;
  }


  //echo $sql; die;

$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 /**
   * Method get_transfer_promation_workflow_detail_list() get transfer promation workflow list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_transfer_promation_workflow_detail_list()
  {
    
    try{

$sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  e.`officename`,
  b.`trans_flag` as status,
  b.`reason`,
  s.`desname` as desnameold,
  g.`desname` as desnewname,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' WHEN b.trans_flag = 4 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit' WHEN b.trans_flag = 3 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
   LEFT JOIN
  `msdesignation` AS g ON b.`new_designation` = g.`desid`
  LEFT JOIN
  `msdesignation` AS s ON b.`old_designation` = s.`desid`
  Where 1=1 AND b.`trans_status` ='BOTH'";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =" .$this->loginData->staffid;
    //$sql .= " AND a.receiver =".$this->loginData->staffid;
  }
// echo $sql;  die;
$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  /**
   * Method get_transfer_promation_workflow_detail_list() get transfer promation workflow list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_promation_workflow_detail_list()
  {
    
    try{

 $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  e.`officename`,
  b.`trans_flag` as status,
  b.`reason`,
  s.`desname` as desnameold,
  g.`desname` as desnewname,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' WHEN b.trans_flag = 4 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit' WHEN b.trans_flag = 3 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
   LEFT JOIN
  `msdesignation` AS g ON b.`new_designation` = g.`desid`
  LEFT JOIN
  `msdesignation` AS s ON b.`old_designation` = s.`desid`
  Where 1=1 AND b.`trans_status` ='Promotion'"; 

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =" .$this->loginData->staffid;
    //$sql .= " AND a.receiver =".$this->loginData->staffid;
  }
// echo $sql;  die;

$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




  /**
   * Method getprobationworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getprobationworkflowdetaillist()
  {
    
    try{

   $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  e.`officename`,
  b.`trans_flag` as status,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate' WHEN b.`trans_flag` = 4 THEN 'Accepted By TC/Intergrator' WHEN b.`trans_flag` = 5 THEN 'Accepted By Personnel Unit' WHEN b.`trans_flag` = 3 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit'
     WHEN b.`trans_flag` = 7 THEN 'Approved By ED'  WHEN b.`trans_flag` = 8 THEN 'Rejected By ED' 

    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where 1=1 AND b.`trans_status`='Promation'";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
  }

  $res = $this->db->query($sql)->result();
  return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

 /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
			INNER join staff as st ON u.staffid = st.staffid
			WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0";

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getTransferStaffDetails() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTransferStaffDetails($transid)
  {
    
    try{
        $sql = "SELECT * FROM `staff_transaction` WHERE `id` =".$transid;

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist($token)
  {
    
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
WHERE
  a.`transid`= ".$token;

  //echo $sql; die;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSinglestaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSinglestaffdetailslist($transid)
  {
    
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
WHERE
  a.`transid`= ".$transid;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method get_staff_sep_detail() get the staff detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */



  public function get_staff_transfer_promation_detail($token)
   {
      try
    {

      $sql="SELECT c.name,a.date_of_transfer as dateoftransfer,  a.staffid, b.desname as sepdesig, a.new_designation as sepdesigid, c.emp_code, st.date_of_transfer as joiningdate, d.desname as joindesig, a.trans_flag, a.reason, a.createdon, lp.officename as newoffice  
      FROM staff_transaction as a
        INNER JOIN `staff` as c on a.staffid = c.staffid or (a.trans_status = 'Termination' or a.trans_status = 'Retirement' or a.trans_status = 'Resign' or a.trans_status = 'Death' )
         left join `state` as s ON s.id = c.permanentstateid
         inner join staff_transaction as st
          on st.staffid = c.staffid and st.trans_status = 'JOIN'
         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
         INNER JOIN `msdesignation` as d on st.new_designation = d.desid
         INNER JOIN `lpooffice` as lp on a.new_office_id = lp.officeid
       WHERE a.id = $token";
 
       //$query=$this->db->get();
       return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffclerancelist($token)
  {
    
    try{
        $sql = "SELECT * FROM `tbl_clearance_certificate` WHERE `id` =".$token;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getcountstaffitmsclerancelist($token)
  {
    
    try{

         $sql = "SELECT count(clearance_certificate_id) as cccount FROM `tbl_clearance_certificate_transaction` WHERE `clearance_certificate_id` =".$token;
         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffitmsclerancelist($token)
  {
    
    try{
        $sql = "SELECT * FROM `tbl_clearance_certificate_transaction` WHERE `clearance_certificate_id` =".$token;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist1($token)
  {
    
    try{

        $sql .= "SELECT
              a.`letter_date`,
              a.`transferno`,
              des.`desname`,
              a.`responsibility_on_date`,
              c.`name`,
              c.`emp_code`,
              b.`staffid`,
              b.`id` AS transid,
              e.`officename`,
              b.`trans_flag` AS STATUS,
              b.`reason`,
              b.`date_of_transfer` AS proposeddate,
              f.`officename` AS newoffice
            FROM
              `tbl_iom_transfer` AS a
            LEFT JOIN
              `staff_transaction` AS b ON a.`transid` = b.`id`
            LEFT JOIN
              `staff` AS c ON a.`staffid` = c.`staffid`

            LEFT JOIN
              `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
            LEFT JOIN
              `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
            LEFT JOIN
              `msdesignation` AS des ON c.`designation` = des.`desid`
            WHERE  a.integrator_ed = ". $this->loginData->staffid ;
  
        $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffjoiningreport($token)
  {
    
    try{
   $sql = "SELECT
  jrnp.`id` as joinid,
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
LEFT JOIN
  `tbl_joining_report_new_place` AS jrnp ON a.`transid` = jrnp.`transid`
WHERE jrnp.`flag` = 1 AND  a.`transid` =".$token;

 
        $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }





/**
   * Method getStaffjoningplace() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffjoningplace($token)
  {
    
    try{
       $sql = "SELECT * FROM `tbl_joining_report_new_place` WHERE  flag = 1 AND `id` =".$token;  

        $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}