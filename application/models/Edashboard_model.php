<?php 

/**
* Dashboard Model
*/
class Edashboard_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}


	public function index()
	{

	}



    public function getTeamlist()
    {
        try{

           $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No' ORDER BY `officename`";  
           $res = $this->db->query($sql)->result();

           return $res;

       }catch(Exception $e){
        print_r($e->getMessage());die();
    }
}


public function getBatchlist()
{
    try{

        $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
        $res = $this->db->query($sql)->result();

        return $res;

    }catch(Exception $e){
        print_r($e->getMessage());die();
    }
}



public function getCampus()
{
    try{

        $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
        $res = $this->db->query($sql)->result();

        return $res;

    }catch(Exception $e){
        print_r($e->getMessage());die();
    }
}


public function getRoleHRDDetails()
{
    try{

               $sql = "SELECT
   staff.staffid,staff.name,staff.emailid, role_permissions.UserId
FROM
   `mstuser`
INNER JOIN staff ON mstuser.staffid = staff.staffid
inner join role_permissions on  role_permissions.roleid=mstuser.roleid
WHERE
   trim(role_permissions.menu_name) in('DAship Activities','Placement/offer process') and staff.staffid = 784 and role_permissions.RoleID=16  ORDER BY
 `staff`.staffid  DESC
";
//echo $sql;

     $res = $this->db->query($sql)->row();
     return $res;

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }


}



public function getRolePersonalDetails()
{
    try{

               $sql = "SELECT
   staff.staffid,staff.name,staff.emailid
FROM
   `mstuser`
INNER JOIN staff ON mstuser.staffid = staff.staffid
inner join role_permissions on  role_permissions.RoleID=mstuser.RoleID
WHERE

   role_permissions.RoleID=17  ORDER BY
 `staff`.staffid  DESC  LIMIT 0,1
";
//echo $sql;

     $res = $this->db->query($sql)->row();
     return $res;

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }


}


public function getSelectedCandidate($campusid,$campusintimationid)
{

    try{

       $sql = "SELECT `tbl_candidate_registration`.candidateid,`mstcategory`.categoryname,`mstcategory`.id,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.campusintimationid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.campusid ,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `lpooffice`.officename, `mstbatch`.batch,`staff`.name,`edcomments`.flag,`edcomments`.status,`mstcategory`.categoryname, `mstcampus`.campusname,
    (CASE WHEN `tbl_candidate_registration`.gender = 1 THEN 'Male' WHEN `tbl_candidate_registration`.gender = 2 THEN 'Female' END) as gender FROM `tbl_candidate_registration` 
       LEFT join `tbl_candidate_writtenscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_writtenscore`.candidateid
       LEFT join `tbl_candidate_gdscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_gdscore`.candidateid
       LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
       LEFT join `tbl_candidate_hrscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_hrscore`.candidateid
       left join `mstcategory` ON `mstcategory`.id =`tbl_candidate_registration`.categoryid 
       left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid 
       left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid 
       left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid 
       left join `edcomments` ON  `tbl_candidate_registration`.candidateid  = `edcomments`.candidateid
       left join `tbl_generate_offer_letter_details` ON  `tbl_candidate_registration`.candidateid  = `tbl_generate_offer_letter_details`.candidateid

       Where `tbl_candidate_registration`.`teamid` !=''
       AND `tbl_candidate_hrscore`.hrscore > 1
       AND `tbl_generate_offer_letter_details`.flag='Generate' 
       AND `tbl_generate_offer_letter_details`.sendflag = 1
       AND `tbl_candidate_registration`.`campusid` = '".$campusid."'
       AND `tbl_candidate_registration`.`campusintimationid` ='".$campusintimationid."'
       order by `tbl_candidate_registration`.candidateid DESC ";   //die;
//die;`tbl_generate_offer_letter_details`.
       //AND `tbl_generate_offer_letter_details`.candidateid not in(SELECT candidateid FROM `edcomments` WHERE `send_hrd_mail_status` = 1)
             // echo $sql; die;

             $res = $this->db->query($sql)->result();

             return $res;

         }catch (Exception $e) {
           print_r($e->getMessage());die;
       }
   }

    public function Candidate_details($token)
{

    try{

       $sql = "SELECT  tbl_candidate_registration.categoryid from tbl_candidate_registration where candidateid='$token'
    
      
       ";   

             $res = $this->db->query($sql)->row();

             return $res;

         }catch (Exception $e) {
           print_r($e->getMessage());die;
       }
   }
 ///AND  `edcomments`.flag = 's';
   public function getRecruitersList()
   {

    try{

        $sql = "SELECT  `staff`.staffid, `staff`.name FROM `mapping_campus_recruiters` Inner join staff on `mapping_campus_recruiters`.`recruiterid`= `staff`.staffid ";

        $res = $this->db->query($sql)->result();

        return $res;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
   }
}


public function getSelectedCandidateForAppointment(){
  try{
    $sql = "SELECT mstcategory.categoryname,tbl_candidate_registration.batchid,tbl_candidate_registration.fgid, fg.name as fieldguide_name,mstcampus.campusname,staff.staffid, staff.name, staff.emailid, lpooffice.officename, a.edstatus, a.transid, a.sendflag, (CASE WHEN staff.gender =1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' END) as gender FROM tbl_offer_of_appointment a 
    left join staff on staff.staffid=a.staffid left join lpooffice on lpooffice.officeid=a.proposed_officeid 
    left join tbl_candidate_registration on staff.candidateid=tbl_candidate_registration.candidateid
    left join staff as fg on tbl_candidate_registration.fgid=fg.staffid
    left join mstcampus on tbl_candidate_registration.campusid=mstcampus.campusid 
    left join mstcategory on tbl_candidate_registration.categoryid=mstcategory.id
    " 
    ;
    $res = $this->db->query($sql)->result();
    return $res;
  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


public function getAllCampus()
{
  try{
    // echo $id;
    // die();

//print_r($this->loginData);

     //$sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
   // ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0"; 

    // if (!empty($id)) {
      
    //    $sql = "SELECT * FROM `mstcampus` as a 
    //    Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
    //    inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
    //  WHERE a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND c.`campus_status`=0"; 
       
    //  } else{

      $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND b.`mailstatus`= 1 AND b.`campus_status`=0 ";

     // }

    // echo $sql; die;

    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}




public function getComments()
{

    try{

        $sql = "SELECT  `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `lpooffice`.officename, `mstbatch`.batch,`staff`.name,
        `edcomments`.comments 
        FROM `tbl_candidate_registration`
        left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid
        left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid
        left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid 
        left join `edcomments` ON `edcomments`.candidateid=`tbl_candidate_registration`.candidateid ";

        $res = $this->db->query($sql)->result();

        return $res;

    }catch (Exception $e) {
       print_r($e->getMessage());die;
   }
}



public function getOfferletterPDF($token)
{

    try{

        $sql = "SELECT `tbl_generate_offer_letter_details`.filename, `tbl_generate_offer_letter_details`.flag FROM `tbl_candidate_registration` 
        left join  `tbl_generate_offer_letter_details` ON  `tbl_generate_offer_letter_details`.candidateid = `tbl_candidate_registration` .candidateid  Where `tbl_candidate_registration` .candidateid ='".$token."' AND `tbl_generate_offer_letter_details`.flag ='Generate'";

        $result = $this->db->query($sql)->result();

        return  $result;
        
    }catch (Exception $e) {
       print_r($e->getMessage());die;
   }
}
public function getAppointmentletterPDF($token)
{
  try{

    $sql = "SELECT `tbl_offer_of_appointment`.filename, `tbl_offer_of_appointment`.flag FROM `tbl_offer_of_appointment` 
      Where `tbl_offer_of_appointment` .transid ='".$token."' AND `tbl_offer_of_appointment`.flag ='Generate'";
    $result = $this->db->query($sql)->row();
    return  $result;
      
  }catch (Exception $e) {
     print_r($e->getMessage());die;
  }
}



public function  getOfferDetails($token)
{

    try{

      $sql = "SELECT `tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.`candidatemiddlename`, `tbl_candidate_registration`.`candidatelastname`,`tbl_candidate_registration`.`emailid`,`mstbatch`.batch,`lpooffice`.`officename`,`lpooffice`.`street`,`lpooffice`.`street`,`lpooffice`.`district`,`lpooffice`.`phone1`,`lpooffice`.`email`,`staff`.`name` as fieldguide, `tbl_candidate_communication_address`.presentstreet, `tbl_candidate_communication_address`.presentcity, `tbl_candidate_communication_address`.presentstateid, `tbl_candidate_communication_address`.presentdistrict,`tbl_candidate_communication_address`.presentpincode,`state`.name as statename,
      `tbl_generate_offer_letter_details`.fileno,`tbl_generate_offer_letter_details`.offerno,
      `tbl_generate_offer_letter_details`.doj,`tbl_generate_offer_letter_details`.lastdateofacceptanceofdocs
      FROM `tbl_candidate_registration` 
      Inner join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
      Inner join `lpooffice` ON `tbl_candidate_registration`.teamid = `lpooffice`.officeid
      Inner join `staff` ON `tbl_candidate_registration`.fgid = `staff`.staffid
      Inner join `mstbatch` ON `tbl_candidate_registration`.batchid = `mstbatch`.id
      Inner join `state` ON `tbl_candidate_communication_address`.presentstateid = `state`.id
      Inner join `tbl_generate_offer_letter_details` ON `tbl_candidate_registration`.candidateid = `tbl_generate_offer_letter_details`.candidateid
                Where `tbl_candidate_registration`.candidateid =$token"; //die;


                $result = $this->db->query($sql)->result();

                return $result;

            }catch (Exception $e) {
                print_r($e->getMessage());die;
            }


        }



    }