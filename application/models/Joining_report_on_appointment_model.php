
<?php 


/**
* State Model
*/
class Joining_report_on_appointment_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

  public function getworkflowid($staff_id)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=11 and staff.staffid=$staff_id ORDER BY workflowid DESC LIMIT 1";
   // echo $sql; die;
    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



   public function getCandidateDetailsPreview($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
   if ($date=='//') {
      $date = NULL;
   }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

public function fetchdata($candidateid)
{
  try{
$qry="Select * from tblstaffjoiningreport where candidate_id=$candidateid";
//echo $qry;

$qru11 = $this->db->query($qry)->row();

return $qru11;
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
} 

	

 public function do_uploads($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/signature/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );


  $this->load->library('upload',$config);
  if(!$this->upload->do_upload('photoupload1'))
  {
    $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



 public function do_uploadsss($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/signature/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );


  $this->load->library('upload',$config);
  if(!$this->upload->do_upload('countersignedsignature'))
  {
    $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

public function fetchdatas($candidateid)
{


  try{

     $sql = "SELECT a.candidatefirstname, a.candidatelastname, b.offerno, b.doj, d.desname, f.officename FROM tbl_candidate_registration a 
    INNER JOIN tbl_generate_offer_letter_details as b on b.candidateid=a.candidateid 
    LEFT JOIN staff as c on c.candidateid=b.candidateid 
    LEFT JOIN msdesignation as d on c.designation=d.desid 
    LEFT JOIN lpooffice as f on f.staffid=c.staffid where 
    a.candidateid=$candidateid"; 

    // echo $sql; die;

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
/*
all_office get all office
*/
 public function all_office()
{


  try{

    $this->db->select("officeid,officename");
    $this->db->from("lpooffice");
    $query=$this->db->get();
     return $query->result();

    

  }
  catch (Exception $e) 
  {
   print_r($e->getMessage());die;
  }


}

public function all_officename($staff_id)
{


  try{

  $this->db->select("officeid,officename");
    $this->db->from("lpooffice");
    $this->db->where("staffid",$staff_id);

    $query=$this->db->get();
     // echo  $this->db->last_query();
     return $query->row();

    

  }
  catch (Exception $e) 
  {
   print_r($e->getMessage());die;
  }


}


public function getCandidateWithAddressDetails($staff_id)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getCandidateWith($reportingto)
  {
    
    try{

        $sql = "SELECT
    staff.name as reprotingname
    
FROM
    staff WHERE   staff.staffid='$reportingto'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


public function fetchsssssss($candidateid)
{

  try{

    $sql = "SELECT a.candidatefirstname, a.candidatelastname, b.offerno, b.doj, d.desname, f.officename,c.encrypted_signature, z.* FROM tbl_candidate_registration a INNER JOIN tbl_generate_offer_letter_details AS b ON b.candidateid = a.candidateid LEFT JOIN staff AS c ON c.candidateid = b.candidateid LEFT JOIN msdesignation AS d ON c.designation = d.desid LEFT JOIN lpooffice AS f ON f.staffid = c.staffid LEFT JOIN tblstaffjoiningreport as z on z.candidate_id=a.candidateid WHERE a.candidateid=$candidateid";
    

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function getCandidateDetailsPreview1($token)
  {
  //echo "token".$token;
    try{

     $this->db->select('*,staff.gender as staff_gender,staff.name as staff_name,s.name as present_state,sp.name as parmanent_state,d.name as present_district,dp.name as permanent_district');

     $this->db->from("staff");
     $this->db->join('tbl_candidate_communication_address ' , 'staff.candidateid=tbl_candidate_communication_address.candidateid ', 'left');
     $this->db->join('tbl_candidate_registration', 'staff.candidateid=tbl_candidate_registration.candidateid ', 'left');
     $this->db->join('state s', 'tbl_candidate_communication_address.presentstateid=s.statecode', 'left');
     $this->db->join('state sp', 'tbl_candidate_communication_address.permanentstateid=sp.statecode', 'left');
     $this->db->join('district d', 'tbl_candidate_communication_address.presentdistrict=d.districtid', 'left');
     $this->db->join('district dp', 'tbl_candidate_communication_address.permanentdistrict=dp.districtid', 'left');

     $this->db->where('staff.staffid',$token);
     $query=$this->db->get();
 // echo "query=".$this->db->last_query();
 //   die;
     return $query->row();


   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }
public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


}