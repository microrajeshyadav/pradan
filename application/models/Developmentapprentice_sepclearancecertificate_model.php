<?php 

/**
* Master  Model
*/
class Developmentapprentice_sepclearancecertificate_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}   

	  /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_clearance_detail($token)
    {
       try
   {

      $sql="SELECT c.name, c.emailid, a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code, a.trans_status, d.id as tbl_da_exit_interview_form_id

        FROM staff_transaction as a 
        INNER JOIN `staff` as c on a.staffid = c.staffid
        left JOIN `msdesignation` as b on a.new_designation = b.desid
        left JOIN tbl_da_clearance_certificate as d on d.transid = a.id

      WHERE a.id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_detail($token)
    {
       try
   {

      $sql="SELECT a.id, a.transid, a.separation_due_to,  a.financename, a.approvaldate, a.superviserapprovaldate, a.superviserid, a.hrid, a.hrapprovaldate, a.place, a.flag,
       (
      CASE a.flag WHEN 2 THEN 'Transfer' WHEN 3 THEN 'Seperation' END
      ) AS type

        FROM  tbl_da_clearance_certificate as a 
      WHERE a.id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_transaction($token)
    {
       try
   {

      $sql="SELECT a.description, a.project,a.id

        FROM  tbl_da_clearance_certificate_transaction as a 
      WHERE a.clearance_certificate_id = $token";

// echo $sql;
// die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


 /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
       try
   {

      $sql="SELECT
      staff.name,
      staff.emailid,
      staff.emp_code,
  staff_transaction.staffid,
  staff_transaction.trans_status,
  staff_transaction.date_of_transfer,
  staff_transaction.new_office_id,
  staff_transaction.new_designation,
  staff.reportingto,
  lpooffice.officename,
  lp_old.officename as oldofficename,
  msdesignation.desname,
  staff_transaction.date_of_transfer

FROM
  staff_transaction
INNER JOIN
  (
  SELECT
    staff_transaction.staffid,
    staff_transaction.date_of_transfer,
    MAX(
      staff_transaction.date_of_transfer
    ) AS MaxDate
  FROM
    staff_transaction
  GROUP BY
    staffid
) AS TMax ON `staff_transaction`.staffid = TMax.staffid  AND `staff_transaction`.date_of_transfer = TMax.MaxDate
left join lpooffice on  staff_transaction.new_office_id=lpooffice.officeid
left join lpooffice as lp_old on staff_transaction.old_office_id=lp_old.officeid
left join msdesignation on staff_transaction.new_designation=msdesignation.desid
INNER JOIN
  `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE
   `staff`.staffid =$token
ORDER BY
  `staff`.name ASC
";
  // echo $sql;die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


/**
   * Method get_staff_finance_detail() get the Finance detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */
 
}