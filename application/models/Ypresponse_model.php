<?php 

/**
* Ypresponse Model
*/
class Ypresponse_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}


    
  public function getSelectedCandidate($token)
  {
    
    try{

          $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,
         `tbl_candidate_registration`.frstatus,
        `tbl_candidate_registration`.emailid,
        `tbl_candidate_registration`.mobile,
        `tbl_candidate_registration`.metricpercentage,
        `tbl_candidate_registration`.hscpercentage,
        `tbl_candidate_registration`.ugpercentage,
        `tbl_candidate_registration`.ugspecialisation,
        `mstcategory`.categoryname
         FROM `tbl_candidate_registration` 
         left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id       
        Where `tbl_candidate_registration`.candidateid=".$token; // die;
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  

public function getSelectedAccptedCandidate()
  {
    
    try{

         $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,
         `tbl_candidate_registration`.frstatus,
        `tbl_candidate_registration`.emailid,
        `tbl_candidate_registration`.mobile,
        `tbl_candidate_registration`.metricpercentage,
        `tbl_candidate_registration`.hscpercentage,
        `tbl_candidate_registration`.ugpercentage,
        `tbl_candidate_registration`.ugspecialisation,
        `tbl_candidate_communication_address`.permanentcity,
        `mstcampus`.campusname,
        `state`.name as statename, `mstcategory`.categoryname,
        (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender
         FROM `tbl_candidate_registration`
         LEFT JOIN `mstcampus` on `tbl_candidate_registration`.`campusid` = `mstcampus`.`campusid` 
         inner join `tbl_candidate_communication_address` on 
         `tbl_candidate_registration`.candidateid =
         `tbl_candidate_communication_address`.candidateid
         left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id    
         inner JOIN `state` ON
         `tbl_candidate_communication_address`.permanentstateid = `state`.statecode
       
       Where tbl_candidate_registration.`inprocess` = 'closed' AND tbl_candidate_registration.`frstatus` = 'accepted' AND tbl_candidate_registration.campusid = 0  AND  (`tbl_candidate_registration`.`yprststus` IS NULL OR  `tbl_candidate_registration`.`yprsent` IS NULL) "; 

	// echo $sql; die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  
 	
public function getYPRResponseType()
  {
    
    try{

       $sql = "SELECT * FROM `tbl_ypr_response_master` Where `isdeleted` = 0 ";

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  



}