<?php 
class Generateofferletter_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


public function changedatedbformate($Date)
{
	try{
//echo strlen($Date);
	$len = (strlen($Date)-5); 
	if(substr($Date,$len,-4)=="/")
		$pattern = "/";
	else
		$pattern = "-";

	$date = explode($pattern,$Date);
   //print_r($date); 
	if($pattern == "/" )
		@ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
	else
		@ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
	if ($date=='//') {
		$date = NULL;
	}
	return $date;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function  getOfferDetails($token)
{

	try{


		if ($this->loginData->RoleID ==17) {
			$sql = "SELECT `tbl_candidate_registration`.categoryid,
			`mstcategory`.categoryname, `staff`.name,
			`msdesignation`.`desname`,
			`district`.name as districtname,
			`msdc_details`.dc_name,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.`candidatemiddlename`, `tbl_candidate_registration`.`candidatelastname`,`tbl_candidate_registration`.`emailid`,`lpooffice`.`officename`,`lpooffice`.`street`,`lpooffice`.`street`,`lpooffice`.`district`,`lpooffice`.`phone1`,`lpooffice`.`email`, `tbl_candidate_communication_address`.presentstreet, `tbl_candidate_communication_address`.presentcity, `tbl_candidate_communication_address`.presentstateid, `tbl_candidate_communication_address`.presentdistrict,`tbl_candidate_communication_address`.presentpincode,`state`.name as statename FROM `tbl_candidate_registration` 
			INNER JOIN `mstcategory` ON `tbl_candidate_registration`.`categoryid` = `mstcategory`.`id`
			Inner join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
			Inner join `lpooffice` ON `tbl_candidate_registration`.teamid = `lpooffice`.officeid
			Inner join `state` ON `tbl_candidate_communication_address`.presentstateid = `state`.id
			Inner join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid
			INNER JOIN
			`staff` ON  `lpooffice`.`staffid` = `staff`.`staffid`
			INNER JOIN
			`msdesignation` ON  `staff`.`designation` = `msdesignation`.`desid`
			LEFT JOIN
			`dc_team_mapping` ON `tbl_candidate_registration`.teamid = `dc_team_mapping`.teamid
			LEFT JOIN
			`msdc_details` ON `dc_team_mapping`.`dc_cd` = `msdc_details`.dc_cd

			Where `tbl_candidate_registration`.candidateid =$token"; 
			$query = $this->db->query($sql);
			$res = $this->db->query($sql)->result();
		}else{
			$sql = "SELECT `tbl_candidate_registration`.categoryid,
			`mstcategory`.categoryname, st.name,
			`msdesignation`.`desname`,
			`district`.name as districtname,
			`msdc_details`.dc_name,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.`candidatemiddlename`, `tbl_candidate_registration`.`candidatelastname`,`tbl_candidate_registration`.`emailid`,`mstbatch`.batch,`lpooffice`.`officename`,`lpooffice`.`street`,`lpooffice`.`street`,`lpooffice`.`district`,`lpooffice`.`phone1`,`lpooffice`.`email`,st.`name` as fieldguide, `tbl_candidate_communication_address`.presentstreet, `tbl_candidate_communication_address`.presentcity, `tbl_candidate_communication_address`.presentstateid, `tbl_candidate_communication_address`.presentdistrict,`tbl_candidate_communication_address`.presentpincode,`state`.name as statename
			FROM `tbl_candidate_registration` 
			INNER JOIN `mstcategory` ON `tbl_candidate_registration`.`categoryid` = `mstcategory`.`id`
			Inner join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
			Inner join `lpooffice` ON `tbl_candidate_registration`.teamid = `lpooffice`.officeid
			Inner join `staff` as st ON `tbl_candidate_registration`.fgid = st.staffid
			Inner join `mstbatch` ON `tbl_candidate_registration`.batchid = `mstbatch`.id
			Inner join `state` ON `tbl_candidate_communication_address`.presentstateid = `state`.id
			Inner join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid
			INNER JOIN
			`staff` as ts ON  `lpooffice`.`staffid` = ts.`staffid`
			INNER JOIN
			`msdesignation` ON  st.`designation` = `msdesignation`.`desid`
			LEFT JOIN
			`dc_team_mapping` ON `tbl_candidate_registration`.teamid = `dc_team_mapping`.teamid
			LEFT JOIN
			`msdc_details` ON `dc_team_mapping`.`dc_cd` = `msdc_details`.dc_cd
			Where `tbl_candidate_registration`.candidateid =$token"; 
			$query = $this->db->query($sql);
			$res = $this->db->query($sql)->result();
		}

		//echo $sql; die;

		$row = $query->num_rows(); 
		if($row > 0){
			return $res;
		}else{
			return -1;
		}

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}


}



public function getpayscale($categoryid){

	try{

		if(date('m') >= 04) {
			$d = date('Y-m-d', strtotime('+1 years'));
			$currentyear =  date('Y') .'-'.date('Y', strtotime($d)); 
		} else { 
			$d = date('Y-m-d', strtotime('-1 years'));
			$currentyear = date('Y', strtotime($d)).'-'.date('Y');
		}


	 $sql = "SELECT `mstcategory`.categoryname,`msdesignation`.level,`msdesignation`.payscale,`tblsalary_increment_slide`.basicsalary FROM `mstcategory` 
		inner join msdesignation on `mstcategory`.`categoryname`= msdesignation.desname
		inner join tblsalary_increment_slide on msdesignation.level = tblsalary_increment_slide.level WHERE `mstcategory`.id='".$categoryid."' AND fyear = '".$currentyear."' AND noofyear = 0 GROUP BY LEVEL";

		$query = $this->db->query($sql);
		$result = $this->db->query($sql)->row();
		//$row = $query->num_rows();
		// echo $result;
		return $result;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}




function generateofferletter(){
	try{
	$query = "SELECT count(*) as Offernumber FROM `tbl_candidate_registration` ";
	$CountRegNumber = $this->db->query($query)->result();	
	//print_r($CountRegNumber);
		//$CountRegNumber = $this->db->query($query);

	$registratNumber1 = $CountRegNumber[0]->Offernumber + 1;
	$newStr = "PAD ".$i."/.".$i."/".$i."/(". $registratNumber1.')/'.date('Y');
		//die;
	return $newStr;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}