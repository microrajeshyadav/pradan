<?php 

/**
* Staff Full Information Model Class
*/
class Stafffullinfo_model extends CI_model
{

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {

  }



/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


public function changedate($Date)
{
  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
  // print_r($date); die;
 if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
if ($date=='//') {
  $date = NULL;
}
return $date;
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

 /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function changedatedbformate($Date)
 {
  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
   //print_r($date); 
 if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
 else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 if ($date=='//') {
  $date = NULL;
}
return $date;
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}




/**
   * Method get max eworkflowid() get Select staff.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getworkflowid($token)
{
         if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

    $sql = "SELECT max(`workflowid`) as workflowid FROM `tbl_workflowdetail`
    WHERE  `tbl_workflowdetail`.type=1 and staffid=$token";
    $result = $this->db->query($sql)->row();
   return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}




/**
   * Method getCandidateDetailsPreview() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateDetailsPreview($token)
{
   if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{

   $this->db->select('*,staff.gender as staff_gender,staff.name as staff_name, staff.father_name, staff.mother_name,staff.encryptedphotoname,staff.nationality,staff.male_sibling,staff.female_sibling,staff.personnel_emailid,staff.emailid,staff.contact, lpooffice.officename,staff.reportingto,s.name as present_state,sp.name as parmanent_state,d.name as present_district,dp.name as permanent_district, des.desname');
   $this->db->from("staff");
   $this->db->join('msdesignation des' , 'staff.designation =des.desid ', 'left');
   $this->db->join('lpooffice', 'staff.new_office_id=lpooffice.officeid', 'left');
   $this->db->join('state s', 'staff.presentstateid=s.statecode', 'left');
   $this->db->join('state sp', 'staff.permanentstateid=sp.statecode', 'left');
   $this->db->join('district d', 'staff.presentdistrict=d.districtid', 'left');
   $this->db->join('district dp', 'staff.permanentdistrict=dp.districtid', 'left');
   $this->db->where('staff.staffid',$token);
   $query=$this->db->get();
 // echo "query=".$this->db->last_query();
 //   die;
   return $query->row();


 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}



 /**
   * Method getResignationstatus() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function getResignationstatus($staff_id)
 {
   if(empty($staff_id) || $staff_id == ''||$staff_id==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{
    $sql  = " SELECT staff_transaction.staffid,staff_transaction.trans_flag FROM staff_transaction INNER JOIN( SELECT staff_transaction.staffid, staff_transaction.date_of_transfer, MAX( staff_transaction.date_of_transfer ) AS MaxDate FROM staff_transaction GROUP BY staffid ) AS TMax ON `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid` WHERE staff_transaction.staffid = '".$staff_id."'  ";

    return $result = $this->db->query($sql)->row(); 

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}
  /**
   * Method getdependmember() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function getdependmember($tokenL)
  {
     if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  //echo "token".$token;
  //die;
    try{

    /*$sql = 'SELECT `tbl_candidate_registration`.*, `tbl_candidate_communication_address`.*,`state`.`name` as statename, `district`.`name` as districtname FROM `tbl_candidate_registration` 
   left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
   left join `state` ON `tbl_candidate_communication_address`.presentstateid = `state`.statecode 
   left join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid
   Where `tbl_candidate_registration`.candidateid ='.$token.'';  */
   //$sql = 'SELECT * from staff  Where `staff`.staffid ='.$token.'';  


//SELECT * FROM `staff` left join tbl_candidate_registration on staff.candidateid=tbl_candidate_registration.candidateid where staff.candidateid=189


   $this->db->select('*');
   //$query = $this->db->get('staff');
   $this->db->from("mstaffdependents");
   $this->db->join('sysrelation', 'mstaffdependents.relationship=sysrelation.id ', 'inner');
    //$this->db->join('msdesignation', 'staff.designation=msdesignatu
 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}


public function getStaffDetail($candidateid)
{
   if(empty($candidateid) || $candidateid == ''||$candidateid==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{

   $sql = "SELECT * FROM pradan.staff as a, pradan.msstaffdetails as b, pradan.staffmail as c  Where a.staffid = b.staffid AND b.staffid = c.staffid And a.staffid = ".$candidateid; 

   $res = $this->db->query($sql)->result()[0];

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}



/**
   * Method getStaffEducationDetail() get Staff Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getStaffEducationDetail($candidateid)
{
  if(empty($candidateid) || $candidateid == ''||$candidateid==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{

   $sql = "SELECT a.*,b.level_name FROM pradan.msstaffeducation_details as a 
   inner join pradan.syseducation_level as b ON a.edulevel_cd = b.edulevel_cd WHERE a.staffid = ".$candidateid; 

   $res = $this->db->query($sql)->result();

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}


/**
   * Method getStaffExpDetail() get Staff Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getStaffExpDetail($candidateid)
{
  if(empty($candidateid) || $candidateid == ''||$candidateid==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{

   $sql = "SELECT * FROM pradan.msstaffexp_details as a WHERE a.staffid = ".$candidateid; 

   $res = $this->db->query($sql)->result();

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}



/**
   * Method getcountStaffExpDetail() get count Staff Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getcountStaffExpDetail($candidateid)
{


  try{

   $sql = "SELECT Count(staffid) as WECount FROM pradan.msstaffexp_details as a WHERE a.staffid = ".$candidateid; 

   $res = $this->db->query($sql)->result()[0];

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getcountStaffTrainingDetail() get count Staff Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getcountStaffTrainingDetail($candidateid)
{
if(empty($candidateid) || $candidateid == ''||$candidateid==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

   $sql = "SELECT Count(*) as TEcount FROM pradan.msstafftraining_programs_attend as a WHERE a.staffid = ".$candidateid; 
    // echo $sql;
     //die();

   $res = $this->db->query($sql)->row();


   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}

/**
   * Method getStaffExpDetail() get Staff Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getStaffTrainingDetail($candidateid)
{
    if(empty($candidateid) || $candidateid == ''||$candidateid==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

   $sql = "SELECT * FROM pradan.msstafftraining_programs_attend as a WHERE a.staffid = ".$candidateid; 

   $res = $this->db->query($sql)->result();

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}



/**
   * Method getSelectedCandidate() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSelectedCandidate()
{

  try{

   $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";

   $res = $this->db->query($sql)->result();

   return $res;
 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateDetails($token)
{
   if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{

   $sql = 'SELECT * FROM `tbl_candidate_registration` 
   left join `tbl_candidate_communication_address` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_communication_address`.candidateid
   left join `district` ON `tbl_candidate_communication_address`.presentdistrict = `district`.districtid

   Where `tbl_candidate_registration`.candidateid ='.$token.''; 


   $result = $this->db->query($sql)->result();

   return $result[0];

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}


/**
   * Method superviser_list() get all superwiser.
   * @access  public
   * @param Null
   * @return  Array
   */

public function superviser_list($token)
{

   if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

   // $sql = 'SELECT * FROM `mstuser`
   // WHERE mstuser.IsDeleted=0 and (mstuser.RoleID=2 or mstuser.RoleID=21) and mstuser.`staffid`!='.$token.''; 

     $sql = 'SELECT DISTINCT(a.staffid), CONCAT(name, " (", b.desname,")") as name, case when u.staffid is null then 0 else 1 end user_status From (

   SELECT staff.staffid, CONCAT(emp_code,"-",name) as name, designation, 1 as sortorder FROM Staff  where  (designation = 4 or designation = 16 and designation = 2) AND status = 1 ) as a
   INNER JOIN msdesignation as b ON b.desid = a.designation  LEFT JOIN mstuser as u ON u.staffid = a.staffid AND u.IsDeleted=0 ORDER by sortorder ASC ';
    
   // echo $sql;
   // die;

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}

public function staffsupervisor($token)
{

   if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

   // $sql = 'SELECT * FROM `mstuser`
   // WHERE mstuser.IsDeleted=0 and (mstuser.RoleID=2 or mstuser.RoleID=21) and mstuser.`staffid`!='.$token.''; 

     $sql = 'SELECT DISTINCT(a.staffid), CONCAT(name, " (", b.desname,")") as name, case when u.staffid is null then 0 else 1 end user_status From (

   SELECT DISTINCT(staff.Staffid), CONCAT(emp_code,"-",name) as name, designation, 1 as sortorder FROM Staff  where staff.Staffid = '.$token.'  ) as a
   INNER JOIN msdesignation as b ON b.desid = a.designation  LEFT JOIN mstuser as u ON u.staffid = a.staffid AND u.IsDeleted=0 ORDER by sortorder ASC ';
    


   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}



/**
   * Method selectedsuperviser() get  superwiser of select staff.
   * @access  public
   * @param Null
   * @return  row
   */

public function selectedsuperviser($token)
{
  if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
 
      }
      else
      {
       
     
  try{

   $sql = 'SELECT staffid,CONCAT(emp_code,"-",name) as name FROM `staff` where `staffid`='.$token.''; 

  // echo $sql;
   $result = $this->db->query($sql);
    if($result->num_rows()>0)
      return $result->row();

        else

        return "empty";

  //return $result;


 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}
}

/**
   * Method getSatff education Details() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffEducationDetails($token)
{
    if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

    $sql ="SELECT * FROM msstaffeducation_details as a 
    INNER JOIN syseducation_level as b on b.edulevel_cd=a.edulevel_cd 
    WHERE a.staffid = $token ORDER BY a.edulevel_cd ASC";
    //echo "data".$sql;
    //die();



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}
/**
   * Method getSatff education level.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffEducation_level()
{

  try{

    $sql ="SELECT * FROM syseducation_level";
    //echo "data".$sql;
    //die();



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
/**
   * Method count Satff education Details() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function countStaffEducationDetails($token)
{
   if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {

  try{

    $sql = "SELECT
    count(*) as Ecount2 
    FROM msstaffeducation_details


    WHERE
    msstaffeducation_details.staffid= $token";
    //echo "data".$sql;
    //die();



    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}


/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateFamilyMemberDetails($token)
{
         if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

    $sql = "SELECT a.id as family_id,
    a.`staffid`,a.`relationwithemployee`,a.`familydob`,a.originalphotoname,a.encryptedphotoname,a.originalfamilyidentityphotoname,a.encryptedfamilyidentityphotoname,b.staffid,d.id,d.relationname,a.Familymembername
    FROM
    msstaff_family_members a
    LEFT JOIN staff b ON
    a.staffid = b.staffid

    LEFT JOIN sysrelation d ON
    a.relationwithemployee = d.id
    WHERE
    a.staffid= $token";
    //echo $sql;
    //die($sql);



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}

/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateFamilyMemberDetailsPrint($token)
{
     if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{


    $sql = 'SELECT * FROM `tbl_family_members`
    LEFT JOIN  `sysrelation` on `tbl_family_members`.relationwithemployee =  `sysrelation`.id Where `tbl_family_members`.candidateid ='.$token.''; 


    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}


  /**
   * Method getCountFamilyMember() get No Of family Member !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountFamilyMember($token)
  {
           if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
    try{
      $sql = "SELECT
      count(*) as Fcount
      FROM
      msstaff_family_members a
      LEFT JOIN staff b ON
      a.staffid = b.staffid

      LEFT JOIN sysrelation d ON
      a.relationwithemployee = d.id
      WHERE
      a.staffid ='$token'"; 
     // echo $sql;
      //die;
      $result = $this->db->query($sql)->row();
      //print_r($result);
      //die;
      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }
 }

 public function getStaffCountFamilyMember($token)
 {
     if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{
    $sql = "SELECT count(*) as `Fcount` FROM `trnstaffrelatives` inner join sysrelation on  trnstaffrelatives.Relation_Cd= sysrelation.id  
    Where `trnstaffrelatives`.staffid ='$token'"; 
      //echo $sql;
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
}


public function getcount_dependmember($token=NULL)
{
     if(empty($token) || $token == ''||$token==NULL) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
  
      }
      else
      {
  try{

    $sql = "SELECT count(*) as `dcount` FROM `mstaffdependents`      
    Where `mstaffdependents`.staffid ='$token'"; 
      //echo $sql;
    $result = $this->db->query($sql)->result();

    return $result[0];


     /* $this->db->select('*');
   
      $this->db->from("staffdependent");
   
    
   $this->db->where('staffdependent.staffid',$token);

  $query=$this->db->get();
  //$query->num_rows()
  return $this->db->count_all_results();*/



}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}
}


  /**
   * Method getCountIdentityNumber() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountIdentityNumber($token)
  {

    try{
      $sql = 'SELECT count(*) as `Icount` FROM `msstaff_identity_details`      
      Where `msstaff_identity_details`.staffid ='.$token.''; 
      $result = $this->db->query($sql)->row();
      //print_r($result);

      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



/**
   * Method getCandidateIdentityDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateIdentityDetails($token)
{

  try{

    $sql = 'SELECT * FROM `msstaff_identity_details` Where `msstaff_identity_details`.staffid ='.$token.''; 
    //echo $sql;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateIdentityDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateIdentityDetailsPrint($candidateid)
{

  try{

   $sql = 'SELECT * FROM `tbl_Identity_details`
   Left JOIN `sysIdentity` ON `tbl_Identity_details`.identityname =`sysIdentity`.id  Where `tbl_Identity_details`.candidateid ='.$candidateid.''; 

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


 /**
   * Method getCountTrainingExposure() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCountTrainingExposure($token)
 {

  try{
    $sql = 'SELECT count(*) as `TEcount` FROM `msstafftraining_programs_attend`      
    Where `msstafftraining_programs_attend`.staffid ='.$token.''; 
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateTrainingExposureDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateTrainingExposureDetails($token)
{

  try{

    $sql = 'SELECT * FROM `msstafftraining_programs_attend` Where `msstafftraining_programs_attend`.staffid ='.$token.''; 
   // echo $sql;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCountGapYear() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCountGapYear($token)
{

  try{
    $sql = 'SELECT count(*) as `GYcount` FROM `msstaff_gap_year`      
    Where `msstaff_gap_year`.staffid ='.$token.''; 
   // echo $sql;
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateGapYearDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateGapYearDetails($token)
{

  try{

    $sql = 'SELECT * FROM `msstaff_gap_year` Where `msstaff_gap_year`.staffid ='.$token.''; 
   // echo $sql;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getCountWorkExprience() get No Identity Number !!!.
   * @access  public
   * @param NULL
   * @return  Array
   */

public function getCountWorkExprience($token)
{

  try{
    $sql = 'SELECT count(*) as `WEcount` FROM `msstaffexp_details`      
    Where `msstaffexp_details`.staffid ='.$token.''; 
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateWorkExperienceDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateWorkExperienceDetails($token)
{

  try{


    $sql="SELECT * FROM  msstaffexp_details   where `staffid`=$token order by id";

   /* $sql="SELECT
    *
FROM
    staff a
LEFT JOIN tbl_candidate_registration b ON
    a.candidateid = b.candidateid
LEFT JOIN msstaffexp_details c ON
    a.staffid = c.staffid

WHERE
a.staffid = $token";*/




    //$sql = 'SELECT * FROM `msstaffexp_details` Where `msstaffexp_details`.staffid ='.$token.''; 

$result = $this->db->query($sql)->result();

return $result;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


   /**
   * Method getCountLanguage() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

   public function getCountLanguage($token)
   {
      //echo "token=".$token;
    try{
      $sql = 'SELECT count(*) as `Lcount` FROM `msstaff_language_proficiency`      
      Where `msstaff_language_proficiency`.staffid ='.$token.''; 
     // echo $sql;
     // die();
      $result = $this->db->query($sql)->row();
      //print_r($result);

      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateLanguageDetails($token)
{

  try{
    $sql = 'SELECT * FROM `msstaff_language_proficiency` Where `msstaff_language_proficiency`.staffid ='.$token.''; 
    //echo $sql;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateLanguageDetailsPrint($candidateid)
{

  try{
   $sql = 'SELECT * FROM `tbl_language_proficiency` left join `syslanguage`  on `syslanguage`.lang_cd = `tbl_language_proficiency`.languageid Where `tbl_language_proficiency`.candidateid ='.$candidateid.''; 

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getCandidateOtherInformationDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateOtherInformationDetails($token)
{

  try{
     // echo "staff=".$token;
    $sql = "SELECT * FROM `msstaff_other_information`  Where `msstaff_other_information`.staffid  ='$token'"; 
    //echo $sql;
    //die;

    $result = $this->db->query($sql)->row();
   // print_r($result);

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getUgEducation() get Post Under Graduation Education List.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getUgEducation()
{

  try{

    $sql = "SELECT id,ugname FROM `mstugeducation` Where isdeleted=0";

    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getPgEducation() get Post Graduation Education List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getPgEducation()
{

  try{

    $sql = "SELECT id,pgname FROM `mstpgeducation` Where isdeleted=0";

    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

  /**
   * Method getViewSelectedCandidate() get Selected Candidates Listing.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getViewSelectedCandidate($token)
  {

   try{

     $sql = "SELECT * FROM `tbl_candidate_registration` Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 ";

     if (!empty($token)) {
       $sql .=" AND candidateid=$token";
     }

     $res = $this->db->query($sql)->result();

     return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method getState() get State name.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getState()
  {

    try{

      $sql = "SELECT * FROM state where isdeleted='0' ORDER BY name";

      $res = $this->db->query($sql)->result();

      return $res;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

   /**
   * Method getCampus() get Campus Name.
   * @access  public
   * @param Null
   * @return  Array
   */


   public function getCampus()
   {
    try{

      $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  /**
   * Method mail_exists() get Email Id Exist OR Not.
   * @access  public
   * @param Null
   * @return  Array
   */

  function mail_exists($key)
  {
    $this->db->where('emailid',$key);
    $query = $this->db->get('tbl_candidate_registration');
    if ($query->num_rows() > 0){
      return true;
    }
    else{
      return false;
    }
  }

 /**
   * Method getSysLanguage() get Language Name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSysLanguage()
 {

  try{
    $this->db->select('*');
    $this->db->from('syslanguage');
    $this->db->where('isdeleted','0');

      return $this->db->get()->result(); //echo $this->db->last_query(); die;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


 /**
   * Method getSysRelations() get Relation Ship Name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSysRelations()
 {

  try{
    $this->db->select('*');
    $this->db->from('sysrelation');
    $this->db->where('isdeleted', 0);
    return $this->db->get()->result();

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


/**
   * Method getSysIdentity() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getSysIdentity()
{

  try{
    $this->db->select('*');
    $this->db->from('sysIdentity');
    $this->db->where('isdeleted', 0);
    return $this->db->get()->result();

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


/**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGeneralFormStatus($candiid)
{

  try{

   $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf`
   WHERE `gnaf`.`candidateid` = ".$candiid."";   

   $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
   if ($query->num_rows() > 0 ) {
    return $res = $query->result()[0];
  }

  

}catch (Exception $e) {
  print_r($e->getMessage());die;
}


}


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getJoiningReport($candiid)
{

  try{

   $sql = "SELECT `gnaf`.status as joinreportstatus 
   FROM `tbl_joining_report` as `gnaf` 
   WHERE `gnaf`.`candidateid` = ".$candiid." ";

   $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
   if ($query->num_rows() > 0 ) {
    return $res = $query->result()[0];
  }

         // $res = $this->db->query($sql)->result();

   // return $res;

}catch (Exception $e) {
  print_r($e->getMessage());die;
}


}


/**
   * Method getCandidateJoiningStatus() get Candidate Joning status .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateJoiningStatus($candiid)
{

  try{

      $sql = "SELECT `tblcr`.joinstatus  FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid." "; //die;
      $res = $this->db->query($sql)->result()[0];

      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


/**
   * Method getBdfFormStatus() get Candidate registration status .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getBdfFormStatus($candiid)
{

  try{

      $sql = "SELECT `tblcr`.BDFFormStatus FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid.""; //die;

      $res = $this->db->query($sql)->result()[0];

      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }




  public function getDistrict()
  {
    try{
      $sql = " SELECT * FROM `district` where isdeleted='0' ";

      $result = $this->db->query($sql)->result();

      return $result;


    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


 public function fetchdataofstaff($token)
 {

  try{

    $sql ="SELECT a.*, a.staffid,a.gender AS staff_gender, a.name AS staff_name,a.status, s.name AS present_state, sp.name AS parmanent_state, d.name AS present_district, dp.name AS permanent_district FROM staff as a 
    LEFT JOIN state s ON a.presentstateid = s.statecode 
    LEFT JOIN state sp ON a.permanentstateid = sp.statecode 
    LEFT JOIN district d ON a.presentdistrict = d.districtid 
    LEFT JOIN district dp ON a.permanentdistrict = dp.districtid 
    WHERE a.staffid  =$token";



    $result = $this->db->query($sql)->row();
    return $result;
   //print_r($result);die();
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}




public function fetcheducaitondata($token)
{

  try{

   $sql = "SELECT * FROM msstaffeducation_details WHERE msstaffeducation_details.staffid= $token"; 

   $res = $this->db->query($sql)->result();

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function fetchgapyear($token)
{

  try{

   $sql = "SELECT * FROM msstaffeducation_details WHERE msstaffeducation_details.staffid= $token"; 

   $res = $this->db->query($sql)->result();

   return $res;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function fetchgapyearcount($token)
{

  try{
    $sql = 'SELECT count(*) as `GYcount` FROM `msstaff_gap_year`      
    Where `msstaff_gap_year`.staffid ='.$token.''; 
    //echo $sql;
    $result = $this->db->query($sql)->result();

    return $result[0];

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function fetchgapyeardata($token)
{

  try{

    $sql = 'SELECT * FROM `msstaff_gap_year` Where `msstaff_gap_year`.staffid ='.$token.''; 
   // echo $sql;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function languagecount($token)
{
     //echo "token=".$token;
 try{
   $sql = 'SELECT count(*) as `Lcount` FROM `msstaff_language_proficiency`
   Where `msstaff_language_proficiency`.staffid ='.$token.'';
   $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
  print_r($e->getMessage());die;
}
}

/**
  * Method getCandidateLanguageDetails() get Select Candidates List.
  * @access  public
  * @param Null
  * @return  Array
  */

public function getlanguageDetails($token)
{

 try{
   $sql = 'SELECT * FROM `msstaff_language_proficiency` Where `msstaff_language_proficiency`.staffid ='.$token.'';

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
  print_r($e->getMessage());die;
}
}


/**
  * Method getoffice_details() get Select Candidate office.
  * @access  public
  * @param Null
  * @return  Array
  */
public function getoffice_details($token)
{
  //echo "token".$token;
  try{

    $this->db->select('staff.staffid,staff.doj_team,staff.doj,staff.designation,staff.reportingto,staff.emp_code,staff.candidateid,lpooffice.staffid,lpooffice.officename,msdesignation.desid,msdesignation.desname,tbl_candidate_registration.candidateid,tbl_candidate_registration.candidatefirstname,tbl_candidate_registration.candidatemiddlename,tbl_candidate_registration.candidatelastname');
   //$query = $this->db->get('staff');
    $this->db->from("staff");
    $this->db->join('lpooffice', 'staff.staffid=lpooffice.staffid ', 'left');
    $this->db->join('msdesignation', 'staff.designation=msdesignation.desid', 'left');
    $this->db->join('tbl_candidate_registration', 'staff.candidateid=tbl_candidate_registration.candidateid', 'left');
    
    $this->db->where('staff.staffid',$token);

    $query=$this->db->get();
    return $query->row();


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

  /**
   * Method getCountIdentityNumber() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountIdnumber($token)
  {

    try{
      $sql = 'SELECT count(*) as `Icount` FROM `msstaff_identity_details`      
      Where `msstaff_identity_details`.staffid ='.$token.''; 
      $result = $this->db->query($sql)->result();

      return $result[0];

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


/**
   * Method getCandidateIdentityDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getcandidateiddata($token)
{

  try{

   $sql = 'SELECT * FROM `msstaff_identity_details`
   Left JOIN `sysIdentity` ON `msstaff_identity_details`.identityname =`sysIdentity`.id  Where `msstaff_identity_details`.staffid ='.$token.''; 

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


  /**
   * Method delete_familyrecord() delete record Of family Member !!!.
   * @access  public
   * @param Null
   * @return  row
   */

  public function delete_familyrecord($id)
  {

    try{

      $this->db->where('id', $id);
      $sql=$this->db->delete('msstaff_family_members');   
      return $sql;




    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


  /**
   * Method getCountFamilyMember() get No Of family Member !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountfamilty($token)
  {

    try{
      $sql = "SELECT count(*) as `Fcount` FROM `trnstaffrelatives` inner join sysrelation on  trnstaffrelatives.Relation_Cd= sysrelation.id  
      Where `trnstaffrelatives`.staffid ='$token'"; 
      
      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getfamilydata($token)
{

  try{

    $sql = "SELECT * FROM staff a 
    LEFT JOIN trnstaffrelatives c ON a.staffid = c.RelatedStaffid
    LEFT JOIN sysrelation d ON c.Relation_Cd = d.id
    WHERE c.staffid = $token";
   // die($sql);a.candidateid=189



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * //@access  public
   */ ///@param Null
  // *// @return  Array
  // *///

public function get($token)
{
//echo $token;die();
  try{
   $sql = 'SELECT * FROM `msstaff_language_proficiency` left join `syslanguage`  on `syslanguage`.lang_cd = `msstaff_language_proficiency`.languageid Where `msstaff_language_proficiency`.staffid ='.$token.''; 

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function fetcheducationdata($token)
{
//echo $token;die();
  try{
   $sql ="SELECT * FROM msstaffeducation_details as a
   INNER JOIN syseducation_level as b ON a.`edulevel_cd` = b.`edulevel_cd`
   WHERE `a`.staffid=".$token." ORDER BY a.edulevel_cd ASC" ; 

   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


}