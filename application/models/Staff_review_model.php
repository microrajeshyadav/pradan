<?php 

/**
* Staff review Model
*/
class Staff_review_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    
  }



  public function getstaffname()
  {
    
    try{
    $currentdate = date('Y-m-d');

$sql = "SELECT
  c.`name`,
  c.`emp_code`,
  c.`doj_team` as date_of_appointment,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  a.receiver,
  a.sender,
   ds.level as levelname,
   b.trans_flag,
  b.`trans_flag` as status,

  (
     CASE WHEN b.`trans_flag` = 1 THEN 'Accepted By HR Administator' 
     WHEN b.`trans_flag` = 3 THEN 'Satisfactory By TC/Intergrator' 
     WHEN b.`trans_flag` = 5 THEN 'Accepted By ED' 
     WHEN b.`trans_flag` = 4 THEN 'Not Satisfactory By TC/Intergrator' 
     WHEN b.`trans_flag` = 6 THEN 'Rejected By ED' 
     WHEN b.`trans_flag` = 7 THEN 'Accepted By Personnel Unit' 
      WHEN b.`trans_flag` = 8 THEN 'Rejected By Personnel Unit' 
     ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  LEFT JOIN
  `msdesignation` AS ds ON c.`designation` = ds.`desid`
  Where b.`trans_flag`>= 1  AND b.`trans_status` = 'Midtermreview' 
  AND a.`receiver` =".$this->loginData->staffid." GROUP BY c.emp_code, a.`r_id` asc";
 //echo $sql;
       return  $this->db->query($sql)->result();
       }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}




 public function getstaffprobationname()
  {
    
    try{
    $currentdate = date('Y-m-d');

$sql = "SELECT
  c.`name`,
  c.`emp_code`,
  c.`doj_team` as date_of_appointment,
  c.`probation_date`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername,
  b.`id` as transid,
  e.`officename`,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  a.receiver,
  a.sender,
  b.`trans_flag` as status,
  (
     CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate By Personnel' 
    
  WHEN b.trans_flag = 3 and t.probation_extension_date is not null THEN 'Review Submitted by TC' 
  WHEN b.trans_flag = 3 and t.satisfactory = 'satisfactory'  THEN 'Review Submitted by TC'
  WHEN b.trans_flag = 3 and t.satisfactory = 'notsatisfactory'  THEN 'Review Submitted by TC'
     WHEN b.`trans_flag` = 5 THEN 'Agreed By ED' 
     WHEN b.`trans_flag` = 4 THEN 'Disagreed By TC/Intergrator' 
     WHEN b.`trans_flag` = 6 THEN 'Rejected By ED' 
     WHEN b.`trans_flag`= 7 THEN 'Agreed by Personnel'
      WHEN b.`trans_flag`= 8 THEN 'Disagreed by Personnel' 
     
     WHEN b.trans_flag = 9 and t.probation_extension_date is not null THEN 'Letter Sent to Staff' 
  WHEN b.trans_flag = 9 and t.satisfactory = 'satisfactory'  THEN 'Letter Sent to Staff'
  WHEN b.trans_flag = 9 and t.satisfactory = 'notsatisfactory'  THEN 'Letter Sent to Staff'
     ELSE 'Stage N.A.'
  END
) AS flag,
case when t.probation_extension_date is not null THEN 'Probation - Extend' 
  WHEN t.satisfactory = 'satisfactory'  THEN 'Probation - Complete'
  WHEN t.satisfactory = 'notsatisfactory'  THEN 'Probation - Separation' WHEN b.`trans_flag` = 1  THEN 'Pending' end
 Request
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
  LEFT JOIN `tbl_probation_review_performance` AS t 
  ON a.`r_id` = t.`transid`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where b.`trans_flag` >= 1  AND b.`trans_status` = 'Probation' 
  AND a.`receiver` =".$this->loginData->staffid." ";
 // echo $sql; die;
       return  $this->db->query($sql)->result();
       }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}






    public function fetchdatas($token)
    { 
      try{

        $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
        $result = $this->db->query($sql);
        $rows = $result->num_rows();
        $tm='-1';
         if($rows==0)
         {
           return $tm;
         }
         else
         {
             
           $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
        
            $query=$this->db->query($sql);
         }
         }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

     //  

    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_seperation_detail()
    {
       try
   {

      $sql="SELECT c.name, k.name as sendername, g.name as receivername,b.senddate as requestdate,b.type, l.process_type,a.date_of_transfer as seperatedate, a.staffid, a.id as transid, b.flag,

      (
      CASE a.trans_flag WHEN 1 THEN 'Request submitted' WHEN 4 THEN 'Request Approved' WHEN 3 THEN 'Request Rejected' ELSE 'Other' END
      ) AS status

        FROM staff_transaction as a 
        INNER JOIN `tbl_workflowdetail` as b on a.id = b.r_id
        INNER JOIN `staff` as c on a.staffid = c.staffid
        INNER JOIN `staff` as k on b.sender = k.staffid
        INNER JOIN `staff` as g on b.receiver = g.staffid
        INNER JOIN `mst_workflow_process` as l on b.type = l.id
        WHERE b.receiver = ".$this->loginData->staffid."  ORDER BY c.name ASC";

        //echo $sql; die;
      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }



}