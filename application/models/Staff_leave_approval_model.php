<?php 

/**
* Staff review Model
*/
class Staff_leave_approval_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    
  }


  public function get_empid($token)
  {
      try{

      $sql = "SELECT emp_code FROM staff  inner join tbl_workflowdetail on staff.staffid=tbl_workflowdetail.staffid WHERE tbl_workflowdetail.receiver ='$token' and type='5'"; 
     

        $res = $this->db->query($sql)->row();
        return $res;

         }catch(Exception $e){
            print_r($e->getMessage());die();
        }

  }

  public function getdetails($id)
  {
      try{

      $sql = "SELECT * FROM `trnleave_ledger`  WHERE  id =$id";  
      // echo $sql;
      // die;

      $res = $this->db->query($sql)->row();
      // print_r($res);
      // die;
      
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    } 
  }

  public function getreportingtodetails($emp_code)
  {
    try{

     $sql = "SELECT
     a.emp_code,
     a.name  as staffname,
     a.new_office_id,
     msd.desname as staffdesignation,
     b.name AS reportingname,
     b.emailid as reportingtoemailid,
     lpo.officename as staffofficename,
     b.permanenthno,
     b.permanentstreet,
     b.permanentcity,
     b.permanentpincode,
     s.name AS statename,
     d.name AS districtname,
     b.contact
     FROM
     staff AS a
     INNER JOIN `staff` AS b
     ON
     a.reportingto = b.staffid
     LEFT JOIN `state` AS s
     ON
     a.permanentstateid = s.statecode
     LEFT JOIN `district` AS d
     ON
     a.permanentdistrict = d.districtid
     LEFT JOIN `msdesignation` as msd
     on 
     a.designation = msd.desid
     LEFT JOIN `lpooffice` as lpo
     on 
     a.new_office_id = lpo.officeid
     WHERE
     a.emp_code = ".$emp_code; 

     $res = $this->db->query($sql)->row();

     return $res;

   }catch(Exception $e){
    print_r($e->getMessage());
  }
}

public function getrequestedleave()
  {
    try{           
      
$sql="SELECT
  lt.`maturnityduedate`,
  lt.`From_date`,
  lt.`id`,
  lt.`To_date`,
  lt.`Noofdays`,
  lt.`longleave`,
  lt.Emp_code,
  lt.filename,
 


  case when 
  lt.supervisiorverify = 1 then 'Yes' when  lt.supervisiorverify = 0 then 'No' else '' end supervisiorverify,
  case when 
  lt.personalverify = 1 then 'Yes'  when 
  lt.personalverify = '0' then 'No' else '' end personalverify,
  case when 
  lt.hrverify = 1 then 'Yes' when 
  lt.hrverify = 0 then 'No' else '' end hrverify,

  lt.`supervisiorview`, 
  lt.`hrview`,
  lt.`personalview`,
  
  md.desname AS sender_des,
  st.name AS name,
  st.staffid,
  lt.`Emp_code`,
  tw.`flag`,
  lt.`reason`,
  l.`Ltypename`,
  case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2   
then 'Rejected' when lt.`status` = 3 then 'Approved' when lt.`status` = 4 then 'Reverse' end status,
lt.status as stage
FROM
  `trnleave_ledger` AS lt
INNER JOIN
  `msleavetype` AS l ON lt.`Leave_type` = l.`Ltypeid` 
INNER JOIN
  staff AS st ON lt.`Emp_code` = st.emp_code
INNER JOIN
  tbl_workflowdetail AS tw ON st.staffid = tw.staffid and lt.id=tw.r_id and lt.status = tw.flag
LEFT JOIN
  msdesignation AS md ON st.designation = md.desid

WHERE
  (tw.`receiver` = ".$this->loginData->staffid."  AND tw.type = 5) OR  (tw.type = 5 and lt.longleave=1 and lt.status = 1 AND tw.type = 5)
ORDER BY
  lt.appliedon DESC";
  // echo $sql;
  // die;

 //echo $sql; die;
          return $res = $this->db->query($sql)->result();

        

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }



  public function getstaffname()
  {
    
    try{

    $currentdate = date('Y-m-d');


     $sql = "SELECT
    staff.name,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.emp_code,
    staff.candidateid,
    staff_transaction.staffid,
    staff_transaction.id,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    msdesignation.level_name AS levelname,
    staff.joiningandinduction,
    staff.gender,
    mlc.mlcid
    (
        CASE WHEN staff.gender = 1 THEN 'Male' WHEN staff.gender = 2 THEN 'Female' WHEN staff.gender = 3 THEN 'Other' ELSE 'Not gender here'
    END
) AS gender,
staff.status,
(
    CASE WHEN flag = 0 THEN 'Save' WHEN flag = 1 THEN 'Submitted' WHEN flag = 4 THEN 'Approved' WHEN flag = 3 THEN 'Rejected' ELSE 'Not status here'
END
) AS flag
FROM
    staff_transaction
INNER JOIN(
    SELECT staff_transaction.staffid,
        staff_transaction.date_of_transfer,
        MAX(
            staff_transaction.date_of_transfer
        ) AS MaxDate
    FROM
        staff_transaction
    GROUP BY
        staffid
) AS TMax
ON
    `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate
INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`
INNER JOIN lpooffice ON staff.`new_office_id` = lpooffice.`officeid`
INNER JOIN msdesignation ON staff.`designation` = msdesignation.`desid`
WHERE 
staff_transaction.trans_status = CASE WHEN staff_transaction.trans_status = 'Midtermreview' then 'Midtermreview' else 'JOIN' END AND  trans_flag=1 AND 
    msdesignation.`level_name` = 'level-4' AND '".$currentdate."' <= CASE WHEN probation_extension_date IS NOT NULL THEN probation_extension_date ELSE staff.probation_date END";  //die;

    // if ($this->loginData->RoleID == 2) {
    //   $sql .= "WHERE staff.`reportingto` =".$this->loginData->staffid;  
    // }
 // $sql .= " GROUP BY `staff_transaction`.staffid  order by cast(staff.emp_code as UNSIGNED)";  
     // echo $sql;
      return  $this->db->query($sql)->result();
      }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


public function requestedleave($emp_code)
  {
    try{
           
           $sql = "SELECT lt.`From_date`, lt.`To_date`, lt.`Noofdays`, case when lt.`status` = 1 then 'Request Submitted' when lt.`status` = 2  or lt.`status` = 4  then 'Rejected' when lt.`status` = 3 or  lt.`status` = 5 then 'Approved' when lt.`status` = 6 then 'Reverse' end status, lt.`appliedon`, lt.`reason`, l.`Ltypename`, lt.status as stage FROM `trnleave_ledger` as lt LEFT JOIN `msleavetype` as l on lt.`Leave_type` = l.`Ltypeid` WHERE  lt.`Leave_transactiontype` ='DR' AND  lt.`Emp_code`=".$emp_code." ORDER BY appliedon DESC"; 

          return $res = $this->db->query($sql)->row();

        

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
  }



    public function fetchdatas($token)
    { 

      try{
        $sql="SELECT * FROM mstuser WHERE SUBSTRING( Username, LOCATE('_', Username)+1)=$token";
        $result = $this->db->query($sql);
        $rows = $result->num_rows();
        $tm='-1';
         if($rows==0)
         {
           return $tm;
         }
         else
         {
             
           $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$token";
        
            $query=$this->db->query($sql);
         }

         }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
     //  

    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_seperation_detail()
    {
       try
   {

      $sql="SELECT c.name, k.name as sendername, g.name as receivername,b.senddate as requestdate,b.type, l.process_type,a.date_of_transfer as seperatedate, a.staffid, a.id as transid, b.flag,

      (
      CASE a.trans_flag WHEN 1 THEN 'Request submitted' WHEN 4 THEN 'Request Approved' WHEN 3 THEN 'Request Rejected' ELSE 'Other' END
      ) AS status

        FROM staff_transaction as a 
        INNER JOIN `tbl_workflowdetail` as b on a.id = b.r_id
        INNER JOIN `staff` as c on a.staffid = c.staffid
        INNER JOIN `staff` as k on b.sender = k.staffid
        INNER JOIN `staff` as g on b.receiver = g.staffid
        INNER JOIN `mst_workflow_process` as l on b.type = l.id
        WHERE b.receiver = ".$this->loginData->staffid."  ORDER BY c.name ASC";

        //echo $sql; die;
      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }



public function get_providentworkflowid($token)
{

  try{

    $sql = "SELECT max(`workflowid`) as workflow_id FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=5 and staff.staffid=$token";
    
//echo $sql;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function personal_email()
  {
    
    try{

        $sql = "SELECT  * FROM  mstuser WHERE  RoleID=17 and IsDeleted='0'";
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}