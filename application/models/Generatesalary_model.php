<?php 

/**
* Master  Model
*/
class Generatesalary_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }
  public function index()
  {

  }

  public function getStaffList($officeid=null,$salmonth,$salyear)
  {
    try{

      $date = new DateTime($salyear."-".$salmonth."-01");
      $date->modify('last day of this month');
      $lastday = $date->format('Y-m-d'); 


      $sql = "select staff.staffid,name,doj,staff.emp_code,designation,msdesignation.desid,
      msdesignation.desname,lp.officename as office, e_basic as basicsalary, trnld.lwp from staff  
      inner join msdesignation on staff.designation= msdesignation.desid  
      LEFT JOIN `lpooffice` as lp on staff.new_office_id = lp.officeid 
      LEFT JOIN `tblmstemployeesalary` as sal on staff.staffid = sal.staffid 
      LEFT JOIN (select emp_code, sum(noofdays) lwp from `trnleave_ledger` where leave_type = 5 And From_date BETWEEN '".$salyear."-".$salmonth."-01' and '".$lastday."' group by emp_code)  as trnld ON staff.emp_code = trnld.Emp_code  WHERE staff.status =1 ";

      if (!empty($officeid) && $officeid !='') {
        $sql .= " AND staff.new_office_id = ".$officeid; 
      }

      $sql .= " order by name"; 
      //echo $sql; die;

      return $res = $this->db->query($sql)->result();



    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }




  public function get_alloffice()
  {
     try
   {
     $this->db->select("officeid,officename");
     $this->db->from('lpooffice');
     $query=$this->db->get();
     return $query->result();
    }catch (Exception $e) 
    {
       print_r($e->getMessage());die;
    }

  }

}