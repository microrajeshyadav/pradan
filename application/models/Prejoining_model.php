<?php 

/**
* Pre joining Model
*/
class Prejoining_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{

	}

 /**
   * Method getBatchlist() get batch list .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getBatchlist()
  {
    try{

      $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


/**
   * Method getCampus() get campus list .
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getCampus()
  {
    try{

      $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
      $res = $this->db->query($sql)->result();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


/**
   * Method getRoleEdDetails() get campus list .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getRoleEdDetails()
  {
    try{

      $sql = "SELECT * FROM `mstuser`  WHERE RoleID=18";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

 
public function getSinglePrejoiningList($id=NULL)
  {

    try{
     
      $sql = "SELECT b.`candidateid`, a.`id`, a.`batch`,a.`prejoining_letter`,b.`candidatefirstname`,b.`candidatemiddlename`,b.`candidatelastname` FROM tbl_prejoining_letter as a
        inner join tbl_candidate_registration as b on a.`candidateid` = b.`candidateid`
      Where a.`isdeleted`= 0 AND a.`id` = '".$id."' ORDER BY a.`id` DESC ";

    $res = $this->db->query($sql)->result()[0];

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



  public function getPrejoiningList()
  {

    try{
     
      $sql = "SELECT
    a.`id`,
    a.`batch`,
    a.`prejoining_letter`,
    `mstcampus`.`campusname`,
    b.`candidatefirstname`,
    b.`candidatemiddlename`,
    b.`candidatelastname`,
    b.teamid,
    b.`emailid`,
    b.`mobile`,
    staff.`name`,
    b.`ugdegree`,
    `mstcategory`.categoryname,
    (
        CASE WHEN b.gender = 1 THEN 'Male' WHEN b.gender = 2 THEN 'Female'
    END
) AS gender,COALESCE(
     nullif(otherspecialisation, ''),
     nullif(pgspecialisation, ''),
     nullif(ugspecialisation, '')
  ) AS specialization
FROM
    tbl_prejoining_letter AS a
INNER JOIN tbl_candidate_registration AS b
ON
    a.`candidateid` = b.`candidateid`
INNER JOIN `mstcampus` ON `b`.campusid = `mstcampus`.campusid
INNER JOIN `mstbatch` ON `a`.batch = `mstbatch`.batch and stage = 1
LEFT JOIN `mstcategory` ON b.categoryid = `mstcategory`.id
LEFT JOIN `staff` ON b.fgid = `staff`.`staffid`
WHERE
    a.`isdeleted` = 0 AND b.categoryid NOT IN(2, 3)
ORDER BY
    a.`id`
DESC";

      // echo $sql; die;
    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


  public function prejoing_supervisior($batch)
  {

    try{
     
      $sql = "SELECT
    a.`id`,
    a.`batch`,
    `mstcampus`.`campusname`,
    b.`candidatefirstname`,
    b.`candidatemiddlename`,
    b.`candidatelastname`,
    b.teamid,
    b.`emailid`,
    b.`mobile`,
    -- staff.emailid as supervisior_email,
    -- staff.name as supervisior_name,
    b.`ugdegree`,
    `mstcategory`.categoryname,
    (
        CASE WHEN b.gender = 1 THEN 'Male' WHEN b.gender = 2 THEN 'Female'
    END
) AS gender,COALESCE(
     nullif(otherspecialisation, ''),
     nullif(pgspecialisation, ''),
     nullif(ugspecialisation, '')
  ) AS specialization
FROM
    tbl_prejoining_letter AS a
INNER JOIN tbl_candidate_registration AS b
ON
    a.`candidateid` = b.`candidateid`
INNER JOIN `mstcampus` ON `b`.campusid = `mstcampus`.campusid
LEFT JOIN `mstcategory` ON b.categoryid = `mstcategory`.id
left join lpooffice on b.teamid=lpooffice.officeid
-- LEFT JOIN `staff` ON lpooffice.staffid = `staff`.`staffid`

WHERE
    a.`isdeleted` = 0 AND b.categoryid NOT IN(2, 3) and a.`batch` = '$batch'
    group by b.teamid
";

      // echo $sql; die;
    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

 public function supervisior_staffid($teamid)
  {

    try{
     
      $sql = "SELECT staff.name, staff.staffid,staff.emailid, staff.designation FROM staff WHERE staff.new_office_id = '$teamid' and (designation =4 or designation=16)  ORDER BY staff.staffid DESC LIMIT 0,1";

      //echo $sql; die;
    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

 public function candidate_batch($batch)
  {

    try{
     
      $sql = "SELECT a.batch, a.teamid from tbl_candidate_registration as a left join mstbatch as b on a.batchid = b.batchid left join staff as s  where a.isdeleted='0' AND a.stage = 1";

      //echo $sql; die;
    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getHRunitDetails()
{
    try{

      $sql = "SELECT staff.name,msdesignation.desname,CASE WHEN sysaccesslevel.Acclevel_Cd = 16 then 'HRD Unit' WHEN sysaccesslevel.Acclevel_Cd = 17 then 'PA Unit' WHEN sysaccesslevel.Acclevel_Cd = 20 then 'Finance Unit' ELSE 'ROle' END as rolename FROM `staff` INNER JOIN mstuser on mstuser.staffid = staff.staffid
INNER JOIN msdesignation on msdesignation.desid = staff.designation
INNER Join sysaccesslevel ON sysaccesslevel.Acclevel_Cd = mstuser.RoleID
WHERE staff.designation = 4 AND mstuser.RoleID = 16";
      $result = $this->db->query($sql)->row();

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }

   


public function getCandidateDetails($candidateid)
  {

    try{
     
      $sql = "SELECT a.`prejoining_letter`,b.`candidatefirstname`,b.`candidatemiddlename`,b.`candidatelastname`, b.`emailid` FROM tbl_prejoining_letter as a
        inner join tbl_candidate_registration as b on a.`candidateid` = b.`candidateid`
      Where a.`isdeleted`= 0 AND b.`candidateid`='".$candidateid."' ORDER BY a.`id` DESC ";
    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}





public function getPrejoiningDocumentDetails($id)
  {

    try{
     
      $sql = "SELECT * FROM tbl_prejoining_document as a
       Where a.`prejoining_letterid`=".$id;
      $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}




public function getSingelRecruitersCampus($id){

  try{


    $sql = "SELECT *  FROM `tbl_generate_offer_letter_details` Where `id` = $id ";

      // echo  $sql; die;   

    $res = $this->db->query($sql)->result();


    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}








public function getflag($candidateid)
{

  try{
    $this->db->where('candidateid',$candidateid);
    $query = $this->db->get('tbl_generate_offer_letter_details');
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getEdcommentStatus($token)
{

  try{

    $sql = "SELECT *  FROM `edcomments` Where `flag` !='NULL'   AND `candidateid` = $token ";
    $query = $this->db->query($sql);

   // echo $query->num_rows(); 

    if ($query->num_rows() > 0) {
     $res = $this->db->query($sql)->result()[0];

      return $res;
    }else{

      return -1;
      }


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}




 public function getcheckdocumentcandidatelist()
  {

    try{
     
      $sql = "SELECT 
            `tbl_candidate_registration`.candidateid, 
            `tbl_candidate_registration`.candidatefirstname, 
            `tbl_candidate_registration`.candidatemiddlename, 
            `tbl_candidate_registration`.candidatelastname, 
            `mstbatch`.batch 
          FROM 
            `tbl_candidate_registration` 
            left join `mstbatch` ON `mstbatch`.id = `tbl_candidate_registration`.batchid 


          Where 
             `tbl_candidate_registration`.`complete_inprocess` = 1 
               AND 
               `tbl_candidate_registration`.`hrddocumentcheckstatus` = 1 
               AND 
               `tbl_candidate_registration`.`categoryid` Not In(2,3) 
               AND 
               `tbl_candidate_registration`.`candidateid` Not in (Select candidateid FROM tbl_prejoining_letter)
               AND 
               `tbl_candidate_registration`.`rejected` = 0
          ORDER BY 
            `tbl_candidate_registration`.candidateid DESC";
            
   
    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getallcheckdocumentcandidatelist()
  {

    try{
     
      $sql = "SELECT 
            `tbl_candidate_registration`.candidateid, 
            `tbl_candidate_registration`.candidatefirstname, 
            `tbl_candidate_registration`.candidatemiddlename, 
            `tbl_candidate_registration`.candidatelastname, 
            `mstbatch`.batch 
          FROM 
            `tbl_candidate_registration` 
            left join `mstbatch` ON `mstbatch`.id = `tbl_candidate_registration`.batchid 


          Where 
             `tbl_candidate_registration`.`complete_inprocess` = 1 
               AND 
               `tbl_candidate_registration`.`hrddocumentcheckstatus` = 1 
               AND 
               `tbl_candidate_registration`.`categoryid` Not In(2,3) 
          ORDER BY 
            `tbl_candidate_registration`.candidateid DESC";
            
   
    $res = $this->db->query($sql)->result();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


  /*
    function getAllSuprvsn() is get All Supervisors email id 
    result row;
    created by rajat
*/
  public function getAllSuprvsn()
  {
    
    try{

  $sql = "SELECT name,emailid,staffid,designation FROM staff WHERE STATUS = 1 AND designation IN (4,16) ORDER BY
  `staff`.staffid";   
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getreportint_to($staffid)
  {
    
    try{

  $sql = "SELECT name,staffid FROM staff WHERE reportingto='$staffid'";   
        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



}