<?php 

/**
* State Model
*/
class Staff_approval_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}



    /**
   * Method getworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   21 stage is pario to stage 3 beacause 21 stage has discussed  in latter stage and did not want to change define stages
   by poojja dhamija */

  public function getworkflowdetaillist()
  {
    
    try{

    $sql = "SELECT
  c.`name`,
  th.id as Handed_id,
  tk.id as taken_id,

  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  ms.Acclevel_Name as sendername, 
  b.`id` as transid,
  e.`officename`,
  b.`trans_flag` as status,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  a.receiver,
  b.reportingto,
  g.id AS tbl_clearance_certificate_id,
  h.id AS tbl_joining_report_new_place_id,
  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Agreeed By TC/Intergrator' 
     WHEN b.trans_flag = 4 THEN 'Disagreed By TC/Intergrator' 
    WHEN b.trans_flag = 5 THEN 'Accepted By ED' 
     WHEN b.trans_flag = 6 THEN 'Rejected By ED' 
    WHEN b.trans_flag = 7 THEN 'Agreeed by Pesonnel' 
    WHEN b.trans_flag = 8 THEN 'Disagreed By Pesonnel' 
     WHEN b.trans_flag = 9 THEN ' Accepted by TC/Personnel Unit' 
    WHEN b.trans_flag = 10 THEN 'Rejected By TC/Personnel Unit' 
      WHEN b.trans_flag = 11 THEN 'Accepted by Personnel Unit' 
    WHEN b.trans_flag = 12 THEN 'Rejected By Personnel Unit' 
     WHEN b.trans_flag = 13 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 14 THEN 'Rejected By TC/Intergrator' 
    WHEN b.trans_flag = 15 THEN 'Accepted By Team Accountant' 
    WHEN b.trans_flag = 16 THEN 'Rejected By Team Accountant' 
    WHEN b.trans_flag = 17 THEN 'Accepted By  Fiance Administrator' 
    WHEN b.trans_flag = 18 THEN 'Rejected By Fiance Administrator' 
     WHEN b.trans_flag = 19 THEN 'Accepted by Personnel Unit' 
    WHEN b.trans_flag = 20 THEN 'Rejected By TC/Personnel Unit' 
     WHEN b.trans_flag = 21 THEN 'Agreeed By HR Unit' 
    WHEN b.trans_flag = 22 THEN 'Disagreed By HR Unit' 
     WHEN b.trans_flag = 23 THEN 'Accepted By Staff'
      WHEN b.trans_flag = 24 THEN 'Rejected By staff'
       WHEN b.trans_flag = 25 THEN 'Accepted By TC/Intergrator'
       WHEN b.trans_flag = 26 THEN 'Rejected By TC/Intergrator'
         WHEN b.`trans_flag` = 27 THEN 'Taken Over  By TC/Intergrator'
         WHEN b.`trans_flag` = 28 THEN 'Approve By Personnel Unit' 
         WHEN b.`trans_flag` = 29 THEN 'Approve By ED'

    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=2)
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `tbl_clearance_certificate` AS g ON g.`transid` = b.`id`
LEFT JOIN
  `tbl_joining_report_new_place` AS h ON h.`transid` = b.`id`
  
  LEFT JOIN
  `tbl_hand_over_taken_over_charge` AS th ON th.`transid` = b.`id` AND th.type=1
  LEFT JOIN
  `tbl_hand_over_taken_over_charge` AS tk ON tk.`transid` = b.`id` AND tk.type=2

LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`

  inner join mstuser m on a.sender=m.staffid 
   inner join mst_workflow_process_stages as mp on m.RoleID=mp.sender_roleid And a.flag=mp.stage
  inner join sysaccesslevel ms on m.RoleID=ms.Acclevel_Cd
  Where 1=1 AND b.`trans_status` ='Transfer' group by m.staffid ";

  if ($this->loginData->RoleID == 17) {
    $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
  	//$sql .= " AND a.receiver =".$this->loginData->staffid;
  }

// echo $sql;die();
   
$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 /**
   * Method get_transfer_promation_workflow_detail_list() get transfer promation workflow list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_transfer_promation_workflow_detail_list()
  {
    
    try{

$sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  e.`officename`,
  b.`trans_flag` as status,
  b.`reason`,
  s.`desname` as desnameold,
  g.`desname` as desnewname,
  h.id as clearness_id,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  Handed.id  as Handed_id,
  Taken.id AS Taken_id,
  h.id as tbl_clearance_certificate_id,
  i.id as tbl_joining_report_new_place_id,

  (
    CASE WHEN b.trans_flag = 1 THEN 'Process Initiate' WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit' WHEN b.trans_flag = 4 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' WHEN b.trans_flag = 7 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 8 THEN 'Rejected By TC/Intergrator' WHEN b.trans_flag = 11 THEN 'Handed Over BY Staff' WHEN b.trans_flag = 12 THEN 'Not Handed Over BY Staff' WHEN b.trans_flag = 13 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 14 THEN 'Rejected By TC/Intergrator' WHEN b.trans_flag = 15 THEN 'Accepted By TC/Intergrator' WHEN b.trans_flag = 16 THEN 'Rejected By TC/Intergrator'  ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
   LEFT JOIN
  `msdesignation` AS g ON b.`new_designation` = g.`desid`
  LEFT JOIN
  `msdesignation` AS s ON b.`old_designation` = s.`desid`
  LEFT JOIN
  `tbl_clearance_certificate` AS h ON h.`transid` = b.`id`
 
  LEFT JOIN 
  tbl_hand_over_taken_over_charge AS Handed ON Handed.staffid=b.staffid AND Handed.type=1
   LEFT JOIN 
  tbl_hand_over_taken_over_charge AS Taken ON Taken.staffid=b.staffid AND Taken.type=2

  LEFT JOIN
  `tbl_joining_report_new_place` AS i ON i.`transid` = b.`id`
  Where 1=1 AND b.`trans_status` ='BOTH' ";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =" .$this->loginData->staffid;
    //$sql .= " AND a.receiver =".$this->loginData->staffid;
  }
  $sql.= " ORDER BY a.workflowid DESC LIMIT 1";
//   echo $sql;
// die;

$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method getprobationworkflowdetaillist() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getprobationworkflowdetaillist()
  {
    
    try{

   $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  e.`officename`,
  b.`trans_flag` as status,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE WHEN b.`trans_flag` = 1 THEN 'Process Initiate' WHEN b.`trans_flag` = 4 THEN 'Accepted By TC/Intergrator' WHEN b.`trans_flag` = 5 THEN 'Accepted By Personnel Unit' WHEN b.`trans_flag` = 3 THEN 'Rejected By TC/Intergrator' WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit'
     WHEN b.`trans_flag` = 7 THEN 'Approved By ED'  WHEN b.`trans_flag` = 8 THEN 'Rejected By ED' 

    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where 1=1 AND b.`trans_status`='Promation'";

  if ($this->loginData->RoleID == 17) {
  $sql .= " AND a.receiver =".$this->loginData->staffid;
  }else{
    $sql .= " AND a.receiver =".$this->loginData->staffid;
  }
    // echo $sql;
    // die;
  $res = $this->db->query($sql)->result();
  return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

 /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
      INNER join staff as st ON u.staffid = st.staffid
      WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0"; 
      // echo $sql;
      // die;

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function getExecutiveDirectorList($transfer_to_id)
  {
    
    try{
     if (empty($transfer_to_id) || $transfer_to_id == '' ) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
        redirect('/Staff_approval/index');
        
      } else {

   $sql = "SELECT st.staffid as edstaffid,st.name,st.emailid,emp_code FROM `mstuser` as u 
			INNER join staff as st ON u.staffid = st.staffid
			WHERE u.`RoleID` = ".$transfer_to_id." AND u.`IsDeleted`=0 and designation = case when ".$transfer_to_id." = '16' then 4 else designation end ";

         $res = $this->db->query($sql)->result();
        return $res;

      }

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getTransferStaffDetails() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTransferStaffDetails($transid)
  {
    
    try{
        $sql = "SELECT a.*, staff.name, staff.emp_code FROM `staff_transaction` as a LEFT JOIN staff ON a.staffid = Staff.staffid Where a.`id` =".$transid;
        // echo $sql;
         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }




/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist($token)
  {
    
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
WHERE
  a.`transid`= ".$token;

  // echo $sql; die;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getSinglestaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getSinglestaffdetailslist($transid)
  {
    
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
WHERE
  a.`transid`= ".$transid;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method get_staff_sep_detail() get the staff detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */



  public function get_staff_transfer_promation_detail($token)
   {
      try
    {

      $sql="SELECT c.name,a.date_of_transfer as dateoftransfer,  a.staffid, b.desname as sepdesig, a.new_designation as sepdesigid, c.emp_code, st.date_of_transfer as joiningdate, d.desname as joindesig, a.trans_flag, a.reason, a.createdon, lp.officename as newoffice,lp_old.officename as oldoffice  
      FROM staff_transaction as a
        INNER JOIN `staff` as c on a.staffid = c.staffid or (a.trans_status = 'Termination' or a.trans_status = 'Transfer' or a.trans_status = 'Retirement' or a.trans_status = 'Resign' or a.trans_status = 'Death' )
         left join `state` as s ON s.id = c.permanentstateid
         inner join staff_transaction as st
          on st.staffid = c.staffid and st.trans_status = 'JOIN'
         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
         INNER JOIN `msdesignation` as d on st.new_designation = d.desid
         INNER JOIN `lpooffice` as lp on a.new_office_id = lp.officeid
         INNER JOIN `lpooffice` as lp_old on a.old_office_id = lp_old.officeid
       WHERE a.id = $token";
 
     /* echo $sql;
     die;*/
       //$query=$this->db->get();
       return  $this->db->query($sql)->row();

  }

      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffclerancelist($token)
  {
    
    try{
        $sql = "SELECT * FROM `tbl_clearance_certificate` WHERE `id` =".$token;

         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getcountstaffitmsclerancelist($token)
  {
    
    try{

         $sql = "SELECT count(clearance_certificate_id) as cccount FROM `tbl_clearance_certificate_transaction` WHERE `clearance_certificate_id` =".$token;
         $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffclerancelist() get transfer staff details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffitmsclerancelist($token)
  {
    
    try{
        $sql = "SELECT * FROM `tbl_clearance_certificate_transaction` WHERE `clearance_certificate_id` =".$token;
         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist1($token)
  {
    
    try{

        $sql .= "SELECT
              a.`letter_date`,
              a.`transferno`,
              des.`desname`,
              a.`responsibility_on_date`,
              c.`name`,
              c.`emp_code`,
              b.`staffid`,
              b.`id` AS transid,
              e.`officename`,
              b.`trans_flag` AS STATUS,
              b.`reason`,
              b.`date_of_transfer` AS proposeddate,
              f.`officename` AS newoffice
            FROM
              `tbl_iom_transfer` AS a
            LEFT JOIN
              `staff_transaction` AS b ON a.`transid` = b.`id`
            LEFT JOIN
              `staff` AS c ON a.`staffid` = c.`staffid`

            LEFT JOIN
              `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
            LEFT JOIN
              `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
            LEFT JOIN
              `msdesignation` AS des ON c.`designation` = des.`desid`
            WHERE  a.integrator_ed = ". $this->loginData->staffid ;
  
        $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffjoiningreport($token)
  {
    
    try{
   $sql = "SELECT
  jrnp.`id` as joinid,
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`emp_code`,
  b.`staffid`,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `tbl_iom_transfer` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON c.`designation` = des.`desid`
LEFT JOIN
  `tbl_joining_report_new_place` AS jrnp ON a.`transid` = jrnp.`transid`
WHERE jrnp.`flag` = 1 AND  a.`transid` =".$token;

 
        $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

   /**
   * Method get_transfer_promation_workflow_detail_list() get transfer promation workflow list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_promation_workflow_detail_list()
  {
    
    try{

  $sql = "SELECT
  c.`name`,
  c.`emp_code`,
  d.`process_type`,
  b.`staffid`,
  k.`name` as sendername, 
  b.`id` as transid,
  j.id as joining_id,
  e.`officename`,
  b.`trans_flag` as status,
  b.`reason`,
  s.`desname` as desnameold,
  g.`desname` as desnewname,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  b.`trans_flag`,
  (
    CASE 
  WHEN b.trans_flag =1 THEN 'Process Initiate' 
  WHEN b.trans_flag=3 THEN 'Accepted By TC/Intergrator'
  WHEN b.trans_flag=5 THEN 'Accepted By Personnel Unit'WHEN b.trans_flag = 4 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 7 THEN 'Approved By Staff'WHEN b.`trans_flag` = 8 THEN 'Rejected By Staff' WHEN b.`trans_flag` = 9 THEN 'Approved By TC' WHEN b.`trans_flag` = 10 THEN 'Fill Joining Report  By Staff' 
    WHEN b.`trans_flag` = 11 THEN 'Approved Joining Report  By New Tc'
    WHEN b.`trans_flag` = 12 THEN 'Rejected Joining Report  By New Tc'
    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
   LEFT JOIN
  `msdesignation` AS g ON b.`new_designation` = g.`desid`
  LEFT JOIN
  `msdesignation` AS s ON b.`old_designation` = s.`desid` 
   left join 
   tbl_joining_report_new_place As j on b.id=j.transid
  Where  b.`trans_status` ='Promotion' "; 
   $sql .= " AND a.receiver =".$this->loginData->staffid;


  // if ($this->loginData->RoleID == 17) {
  // $sql .= " AND a.receiver =".$this->loginData->staffid;
  // }elseif($this->loginData->RoleID == 2){
  //   $sql .= " AND a.receiver =" .$this->loginData->staffid;
   
  // }
  $sql .= " group by a.staffid";
 // echo $sql; 
 //  die; 
$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }






/**
   * Method getStaffjoningplace() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffjoningplace($token)
  {
    
    try{
       $sql = "SELECT * FROM `tbl_joining_report_new_place` WHERE  flag = 1 AND `id` =".$token;  

        $res = $this->db->query($sql)->result()[0];
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function staff_seperation(){
    try{

    $sql = "SELECT
    g.id as tbl_clearance_certificate_id,
    h.id as tbl_handed_id,
  c.`name`,
  c.`emp_code`,
  concat(d.process_type , ' - ' , b.trans_status) as process_type,
  b.`staffid`,
  k.`name` as sendername,
  l.`name` as receivername, 
  b.`id` as transid,
  e.`officename`,
  b.`trans_flag` as status,
  b.trans_status,
  b.`reason`,
  b.`date_of_transfer` as proposeddate,
  a.`scomments`,
  f.`officename` AS newoffice,
  a.`createdon` AS Requestdate,
  (
    CASE 
    WHEN b.trans_flag = 1 THEN 'Process Initiate' 
    WHEN b.trans_flag = 3 THEN 'Accepted By TC/Intergrator' 
    WHEN b.trans_flag = 5 THEN 'Accepted By Personnel Unit' 
    WHEN b.trans_flag = 4 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 6 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 7 THEN 'Approve By ED' 
    WHEN b.`trans_flag` = 8 THEN 'Rejected By ED'
    WHEN b.`trans_flag` = 11 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 12 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 13 THEN 'Approve By TC/Intergrator' 
    WHEN b.`trans_flag` = 14 THEN 'Rejected By TC/Intergrator' 
    WHEN b.`trans_flag` = 15 THEN 'Approve By Finance' 
    WHEN b.`trans_flag` = 16 THEN 'Rejected By Finance' 
    
    WHEN b.`trans_flag` = 17 THEN 'Approve By Head Office Finance' 
    WHEN b.`trans_flag` = 18 THEN 'Rejected By Head Office Finance' 
     WHEN b.`trans_flag` = 19 THEN 'Approve By Personnel Unit' 
    WHEN b.`trans_flag` = 20 THEN 'Rejected By Personnel Unit' 
    WHEN b.`trans_flag` = 21 THEN 'Approve By HR Unit' 
    WHEN b.`trans_flag` = 22 THEN 'Rejected By HR Unit'
     WHEN b.`trans_flag` = 25 THEN 'Handed Over  By Staff'
     WHEN b.`trans_flag` = 27 THEN 'Taken Over  By TC/Intergrator'
    WHEN b.`trans_flag` = 28 THEN 'Approve By Personnel Unit' 
     WHEN b.`trans_flag` = 29 THEN 'Approve By ED' 
    ELSE 'Not status here'
  END
) AS flag
FROM
  `tbl_workflowdetail` AS a
LEFT JOIN
  `staff_transaction` AS b ON a.`r_id` = b.`id` AND  a.flag = (SELECT MAX(flag) as a FROM tbl_workflowdetail WHERE r_id = a.r_id AND receiver = ".$this->loginData->staffid." AND type=3)
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`
LEFT JOIN
  `mst_workflow_process` AS d ON a.`type` = d.`id`
  LEFT JOIN
  `staff` AS k ON a.`sender` = k.`staffid`
  LEFT JOIN
  `staff` AS l ON a.`receiver` = l.`staffid`
LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `tbl_clearance_certificate` AS g ON b.`id` = g.`transid`
  left Join 
  tbl_hand_over_taken_over_charge as h on b.id=h.transid
  
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
  Where 1=1 AND (b.`trans_status` ='Termination' OR b.`trans_status` ='Resign' OR b.`trans_status` ='Termination during Probation' OR b.`trans_status` ='Death' OR b.`trans_status` ='Retirement' OR b.`trans_status` ='Discharge simpliciter/ Dismiss' OR b.`trans_status` ='Desertion cases' OR b.`trans_status` ='Premature retirement' OR b.`trans_status` ='Super Annuation') ";

  // if ($this->loginData->RoleID == 17) {
  // $sql .= " AND a.receiver =".$this->loginData->staffid;
  // }else{
  //   $sql .= " AND a.receiver =".$this->loginData->staffid;
  //   //$sql .= " AND a.receiver =".$this->loginData->staffid;
  // }
  //$sql.= " ORDER BY a.workflowid DESC LIMIT 1";

   //echo $sql; die;

$res = $this->db->query($sql)->result();
return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}