<?php 

/**
* State Model
*/
class Staff_personnel_probation_approval_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


/**
   * Method getStaffDetails() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStaffDetails($token )
  {
    
    try{
        
         $sql = "SELECT *  FROM `staff` a  WHERE a.`staffid` = ".$token; 
         // echo $sql; die;
         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function getReportingemail($token)
  {
    
    try{
        
         $sql = " SELECT s.emailid,s.name  FROM `staff` a LEFT JOIN `staff` as s ON `a`.`reportingto` = `s`.staffid WHERE  a.`staffid` = ".$token; 
         // echo $sql; die;
         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getStaffTransactioninc() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffTransactioninc($tablename,$incval){


    try{

        // echo $tablename;
        // echo $incval;  die;
        ////'staff_transaction',@a; 

      $sql = "select get_maxvalue($tablename,$incval) as maxincval";

      $result = $this->db->query($sql)->result()[0];

      return $result;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }

 

 /**
   * Method getPersonalUserList() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getPersonalUserList()
  {
    
    try{
   $sql = "SELECT st.staffid as personnelstaffid,st.name,emp_code FROM `mstuser` as u 
      INNER join staff as st ON u.staffid = st.staffid
      WHERE u.`RoleID` = 17 AND u.`IsDeleted`=0"; 

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  public function getExecutiveDirectorList($transfer_to_id)
  {
    
    try{
   $sql = "SELECT st.staffid as edstaffid,st.name,emp_code FROM `mstuser` as u 
			INNER join staff as st ON u.staffid = st.staffid
			WHERE u.`RoleID` = ".$transfer_to_id." AND u.`IsDeleted`=0";

         $res = $this->db->query($sql)->result();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }






}