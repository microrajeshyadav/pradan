<?php 

/**
* Joining Report Form Model Class
*/
class Joining_report_form_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
if ($date=='//') {
  $date = NULL;
 }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 if ($date=='//') {
  $date = NULL;
 }

  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

	/**
   * Method getJoiningReportDetail() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportDetail($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_joining_report` Where id = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  /**
   * Method getJoiningReportview() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReportview($token)
  {
    
    try{

       $sql = "SELECT * FROM `tbl_joining_report` Where candidateid = $token";
        
      $res = $this->db->query($sql)->result()[0];

        return $res;
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCandidateDetails($token)
  {
    
    try{
            $sql = 'SELECT
    `tbl_candidate_communication_address`.permanentstreet,
    `tbl_candidate_communication_address`.permanentcity,
    `tbl_candidate_communication_address`.permanentstateid,
    `tbl_candidate_communication_address`.permanentdistrict,
    `tbl_candidate_communication_address`.permanentpincode,
    `tbl_candidate_registration`.categoryid,
    (CASE WHEN a.fileno is NULL THEN c.fileno else a.fileno END)as fileno,
    (CASE WHEN a.offerno is NULL THEN c.offerno else a.offerno END)as offerno,
    (CASE WHEN a.doj is NULL THEN c.doj else a.doj END)as doj,
    (CASE WHEN a.lastdateofacceptanceofdocs is NULL THEN c.lastdateofacceptanceofdocs else a.lastdateofacceptanceofdocs END)as lastdateofacceptanceofdocs,
    (CASE WHEN a.flag is NULL THEN c.flag else a.flag END)as flag,
    (CASE WHEN a.filename is NULL THEN c.filename else a.filename END)as filename,
    (CASE WHEN a.sendflag is NULL THEN c.sendflag else a.sendflag END)as sendflag,
    (CASE WHEN a.payscale is NULL THEN c.payscale else a.payscale END)as payscale,
    (CASE WHEN a.basicsalary is NULL THEN c.basicsalary else a.basicsalary END)as basicsalary, 
    c.transid,
    c.staffid,
    c.proposed_officeid,
    c.edcomments,
    c.edstatus,
    c.personnelId,
    b.`name` AS permanentstatename,
    `dis`.*
FROM
    `tbl_candidate_communication_address`
    
LEFT JOIN `tbl_generate_offer_letter_details` as a ON `a`.candidateid = `tbl_candidate_communication_address`.candidateid
    
LEFT JOIN `tbl_offer_of_appointment` as c ON `c`.candidateid = `tbl_candidate_communication_address`.candidateid

LEFT JOIN `tbl_candidate_registration` ON `tbl_candidate_communication_address`.candidateid = `tbl_candidate_registration`.candidateid
LEFT JOIN `state` AS b
ON
    `tbl_candidate_communication_address`.permanentstateid = b.`statecode`
LEFT JOIN `district` AS dis
ON
    `tbl_candidate_communication_address`.permanentdistrict = dis.districtid
                Where `tbl_candidate_communication_address`.candidateid ='.$token.'';  //die;
               //  echo $sql;
               // die();
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method getStatus() get status Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getStatus($token)
  {
    
    try{
     
       $sql = "SELECT `status` FROM `tbl_joining_report` Where candidateid = $token";
        
      $res = $this->db->query($sql)->result();
     
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";

       $query = $this->db->query($sql); 
    // echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->row();
      }else{
        return $res = 0;
      }
    

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

     $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." "; 
       $query = $this->db->query($sql); 
     //echo $query->num_rows(); die;
      if ($query->num_rows() > 0 ) {
        return $res = $query->row();
      }else{
        $res = 0;
      }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


public function tc_data($teamid)
{
  try{

      $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emailid,
   `staff`.`designation`
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '$teamid' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
ORDER BY
  `staff_transaction`.staffid  DESC  LIMIT 0,1
 ";
 //echo $sql;

      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }




}