<?php 

/**
* Joining_of_DA Model
*/
class Joining_report_of_newplace_posting_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
    $this->loginData = $this->session->userdata('login_data');
    //print_r($this->loginData);exit();
	}

	public function index()
	{

	}

/**
   * Method getstaffdetailslist() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffdetailslist($token)
  {
    // echo $token;die;
    try{
      $sql = "SELECT
  a.`letter_date`,
  a.`transferno`,
  des.`desname`,
  a.`responsibility_on_date`,
  c.`name`,
  c.`encrypted_signature`,
  c.emailid,
  c.`emp_code`,
  b.old_office_id,
  b.new_office_id,
  b.`new_designation` as new_designation_id,
  b.`staffid`,
  c.encrypted_signature,
  b.`id` AS transid,
  e.`officename`,
  b.`trans_flag` AS STATUS,
  b.`reason`,
  b.reportingto,
  b.`date_of_transfer` AS proposeddate,
  f.`officename` AS newoffice
FROM
  `staff_transaction` AS b
LEFT JOIN
  `tbl_iom_transfer` AS a ON a.`transid` = b.`id`
LEFT JOIN
  `staff` AS c ON a.`staffid` = c.`staffid`

LEFT JOIN
  `lpooffice` AS e ON b.`old_office_id` = e.`officeid`
LEFT JOIN
  `lpooffice` AS f ON b.`new_office_id` = f.`officeid`
LEFT JOIN
  `msdesignation` AS des ON b.`new_designation` = des.`desid`
WHERE 1=1 ";
  
 //echo $this->loginData->RoleID;die();
  if ($this->loginData->RoleID ==3) {
    $sql .= " AND  a.staffid = ". $this->loginData->staffid ." AND a.`transid`= ".$token; 
  }else{
    // $sql .= " AND a.integrator_ed = ". $this->loginData->staffid ." AND a.`transid`= ".$token; 
     $sql .= " AND  a.`transid`= ".$token; 
  }
     //echo $sql; die();
    $res = $this->db->query($sql)->result()[0];
    return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffSuperior($token)
  {
    
    try{
      $sql = "SELECT b.officename, st.name,st.encrypted_signature,c.desname, a.reportingto FROM `staff_transaction`  as a
      Inner Join `staff` as st on a.reportingto = st.staffid
      Inner JOIN `lpooffice` as b on st.new_office_id = b.officeid 
      Inner JOIN `msdesignation` as c on st.designation = c.desid 
       WHERE a.id=".$token;
  
        $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getstaffjoiningreport() get Personnal User List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getstaffjoiningreport($token)
  {
    
    try{
      $sql = "SELECT * FROM `tbl_joining_report_new_place`  WHERE id=".$token;
      //echo $sql;
  
        $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }








}