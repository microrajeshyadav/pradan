<?php 

/**
* Master  Model
*/
class Developmentapprentice_sepclearancecertificate_fc_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}   

	  /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_clearance_detail($token)
    {
       try
   {

      $sql="SELECT c.name,a.date_of_transfer as seperatedate, a.staffid,a.reportingto, b.desname,c.emp_code, a.trans_status, d.id as tbl_da_exit_interview_form_id, d.flag

        FROM staff_transaction as a 
        INNER JOIN `staff` as c on a.staffid = c.staffid
        left JOIN `msdesignation` as b on a.new_designation = b.desid
        left JOIN tbl_da_clearance_certificate as d on d.transid = a.id

      WHERE a.id = $token";


      //echo $sql;
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_detail($token)
    {
       try
   {

      $sql="SELECT a.*, b.name as superviser_name, b.reportingto,c.officename,
       (
      CASE a.flag WHEN 2 THEN 'Transfer' WHEN 3 THEN 'Seperation' END
      ) AS type

        FROM  tbl_da_clearance_certificate as a 
        left join staff b on a.superviserid= b.staffid
        left join lpooffice c on b.new_office_id = c.officeid
      WHERE a.id = $token";


      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }

     /**
   * Method get_staff_seperation_detail() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staff_sepration_clearance_transaction($token)
    {
       try
   {

      $sql="SELECT a.description, a.project,a.id

        FROM  tbl_da_clearance_certificate_transaction as a 
      WHERE a.clearance_certificate_id = $token";

// echo $sql;
// die;

      //$query=$this->db->get();
      return  $this->db->query($sql)->result();

 }

     catch (Exception $e) {
       print_r($e->getMessage());die;
     }
      
    }


 
}