<?php 

/**
* Campus incharge Model
*/
class Campusincharge_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}


public function getElihibalCandidateListForWrittenExam($campusid = NULL){
                                                         
try{
     // echo $campusid; die;

      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,`tbl_candidate_writtenscore`.writtenscore,
      `tbl_accept_campusinchargetocandidate`.hrstatus,`tbl_accept_campusinchargetocandidate`.freezed FROM `tbl_candidate_registration` 
      left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
       inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid =`tbl_accept_campusinchargetocandidate`.candidateid 
      Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` AND  `tbl_candidate_registration`.`metricpercentage` >= 60 AND ((`tbl_candidate_registration`.`hscpercentage` >= 60 And `tbl_candidate_registration`.`ugpercentage` >= 60) OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) "; 
         
      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND
      `tbl_accept_campusinchargetocandidate`.freezed = 0 AND  
      `tbl_candidate_registration`.`wstatus` Is NULL ";
      $sql .= " ORDER BY `tbl_candidate_registration`.`candidateid` ASC ";

     // echo $sql; die;

    $res = $this->db->query($sql)->result();
 //print_r($res); die; 
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



public function getAcceptedcandidate($campusid = NULL){
                                                         
try{
     // echo $campusid; die;

      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_accept_campusinchargetocandidate`.status FROM `tbl_candidate_registration` 
      inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid =`tbl_accept_campusinchargetocandidate`.candidateid 
      Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` AND  `tbl_candidate_registration`.`metricpercentage` >= 60 AND ((`tbl_candidate_registration`.`hscpercentage` >= 60 And `tbl_candidate_registration`.`ugpercentage` >= 60) OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) "; 
      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND 
      `tbl_accept_campusinchargetocandidate`.freezed = 0 AND  
        `tbl_candidate_registration`.`wstatus` Is NULL ";
      $sql .= " ORDER BY `tbl_candidate_registration`.`candidateid` ASC ";

     // echo $sql; die;

    $res = $this->db->query($sql)->result();
 //print_r($res); die; 
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



public function getShortlistCandidate($campusid = NULL){
                                                         
try{
     // echo $campusid; die;

      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,
      `tbl_accept_campusinchargetocandidate`.hrstatus, `tbl_accept_campusinchargetocandidate`.status,`tbl_accept_campusinchargetocandidate`.freezed FROM `tbl_candidate_registration` 
      inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid =`tbl_accept_campusinchargetocandidate`.candidateid 
      Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` AND  `tbl_candidate_registration`.`metricpercentage` >= 60 AND ((`tbl_candidate_registration`.`hscpercentage` >= 60 And `tbl_candidate_registration`.`ugpercentage` >= 60) OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) "; 
      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND 
      `tbl_accept_campusinchargetocandidate`.freezed =1 AND  
        `tbl_candidate_registration`.`wstatus` Is NULL ";
      $sql .= " ORDER BY `tbl_candidate_registration`.`candidateid` ASC ";

    // echo $sql; die;

    $res = $this->db->query($sql)->result();
 //print_r($res); die; 
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}





public function getSelectedCandidateWrittenScore($campusid = NULL)
{

  try{

    $sql = 'SELECT `tbl_candidate_registration`.`candidateid`, `tbl_candidate_registration`.`candidatefirstname`,`tbl_candidate_registration`.`candidatemiddlename`,`tbl_candidate_registration`.`candidatelastname`, `tbl_candidate_registration`.`emailid` FROM `tbl_candidate_registration`
    left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
    Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`hscpercentage`+4) <= `ugpassingyear` AND  metricpercentage >= 60 AND ((hscpercentage >= 60 And ugpercentage >= 60) OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60))  And `tbl_candidate_writtenscore`.writtenscore > 0';
    
    if($campusid == 0 && $campusid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed' 
     AND `tbl_candidate_registration`.`complete_inprocess`= 0
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`wstatus` Is NULL
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid)){

    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on'  AND `tbl_candidate_registration`.`complete_inprocess`= 0,";

  } else{

    $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='closed' AND `complete_inprocess`=0 AND  `tbl_candidate_registration`.`wstatus` Is NULL";
  }

  $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid ASC';

//echo $sql; die;

  $res = $this->db->query($sql)->result();
  return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


public function getcampusinchargestatus($campusid=NULL)
{
    try{

         $sql = " SELECT count(id) as countid FROM `tbl_accept_campusinchargetocandidate` WHERE freezed = 1 AND `campusid`=".$campusid;
         $res = $this->db->query($sql)->result()[0];
         return $res;

      }catch(Exception $e){
        print_r($e->getMessage());die();
      }
}


public function getgenratepdf($token=NULL)
{
    try{

         $sql = " SELECT name,flag FROM `genratepdflist` WHERE flag = 0 ";
         $res = $this->db->query($sql)->result()[0];
         return $res;

      }catch(Exception $e){
        print_r($e->getMessage());die();
      }
}

public function getcampusInchargeEmailid($token=NULL)
{
  try{

       $sql = " SELECT emailid,campusincharge FROM `mstcampus` WHERE campusid =".$token;
       $res = $this->db->query($sql)->result()[0];
       return $res;

      }catch(Exception $e){
        print_r($e->getMessage());die();
      }
}



public function getUpdateStatus($candidateid=NULL)
{
  try{
    $this->db->trans_start();

    $updateArr = array(
      'inprocess'  => 'closed',
    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_candidate_registration', $updateArr);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){
     return -1;  

   }else{
    return 1;
  }

}catch(Exception $e){
  print_r($e->getMessage());die();
}
}

public function getCampus($id=NULL)
{
  try{

//print_r($this->loginData);

     //$sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
   // ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0"; 

    if (!empty($id) AND $this->loginData->RoleID==30) {
      
       $sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
    ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND  b.`recruiterid`= ". $id; 
       
     } else{
      $sql = "SELECT * FROM `mstcampus` WHERE IsDeleted=0";
     }

    //echo $sql; die;

    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


}