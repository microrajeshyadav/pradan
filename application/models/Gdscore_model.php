<?php 

/**
* Dashboard Model
*/
class Gdscore_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}

 public function getSelectedCandidate($campusid =null, $campusintimationid=null)
 {

  try{
       // echo $campusid; die;

    $sql = "SELECT `tbl_candidate_registration`.*, (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender, COALESCE(NULLIF(otherspecialisation,''), NULLIF(pgspecialisation,''), NULLIF(ugspecialisation,'') ) stream,`mstcampus`.`campusname`, `mstcategory`.categoryname,
    `mstcampus`.campusname,
    (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender 
    FROM `tbl_candidate_registration` 
    left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
    LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
    left join `tbl_candidate_writtenscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_writtenscore`.candidateid 
    Where `tbl_candidate_writtenscore`.writtenscore >= `tbl_candidate_writtenscore`.cutoffmarks ";

    if($campusid == 0 && $campusid !='NULL' && $campusintimationid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
     AND `tbl_candidate_registration`.`campusintimationid`= $campusintimationid 
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed'
     AND `tbl_candidate_registration`.`complete_inprocess`= 0 
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid) && $campusid =='NULL'){

    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND 
    `tbl_candidate_registration`.`campusintimationid`= 'NULL' AND `tbl_candidate_registration`.`complete_inprocess`= 0";

  } else{

    $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND 
    `tbl_candidate_registration`.`campusintimationid`= $campusintimationid AND 
    `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`complete_inprocess`= 0";
  }

  $sql .=' ORDER BY `tbl_candidate_registration`.`createdon` desc';

    // echo $sql; 

  $res = $this->db->query($sql)->result();

  return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


public function getSelectedCandidateGDScore($campusid, $campusintimationid)
{

  try{

   $sql = "SELECT `tbl_candidate_gdscore`.gdscore,`tbl_candidate_gdscore`.rsscore,`tbl_candidate_gdscore`.ssscore,`tbl_candidate_gdscore`.`candidateid`, `tbl_candidate_gdscore`.`cutoffmarks`,COALESCE(NULLIF(otherspecialisation,''), NULLIF(pgspecialisation,''), NULLIF(ugspecialisation,'') ) stream,`mstcategory`.categoryname,
    `tbl_candidate_registration`.`gdemail_status` 

   FROM `tbl_candidate_gdscore`  
   join `tbl_candidate_registration` on `tbl_candidate_gdscore`.candidateid =`tbl_candidate_registration`.candidateid 

   left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
   WHERE `tbl_candidate_gdscore`.gdscore >= `tbl_candidate_gdscore`.cutoffmarks "; 
   if($campusid == 0 && $campusid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid  AND
     `tbl_candidate_registration`.`campusintimationid`= $campusintimationid
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed'
     AND `tbl_candidate_registration`.`complete_inprocess`= 0 
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid) && $campusid =='NULL'){
    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND 
    `tbl_candidate_registration`.`campusintimationid`= 'NULL' AND`tbl_candidate_registration`.`complete_inprocess`= 0";
  } else{
    $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND 
    `tbl_candidate_registration`.`campusintimationid`= $campusintimationid AND
    `tbl_candidate_registration`.`campustype` ='on' AND 

    `tbl_candidate_registration`.`complete_inprocess`= 0";
  }

  $sql .=' ORDER BY `tbl_candidate_registration`.candidateid ASC';

  // echo $sql; die;

  $res = $this->db->query($sql)->result();
  

  return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}



public function getCandidateDetails($token)
{

  try{

   $sql = "SELECT
   `tbl_candidate_registration`.candidateid,
   `tbl_candidate_registration`.emailid,
   `tbl_candidate_registration`.candidatefirstname,
   `tbl_candidate_registration`.candidatemiddlename,
   `tbl_candidate_registration`.candidatelastname,
   `tbl_candidate_gdscore`.`cutoffmarks`,
   `tbl_candidate_gdscore`.`gdscore`,
   CASE WHEN `tbl_candidate_gdscore`.gdscore >= `tbl_candidate_gdscore`.`cutoffmarks` THEN '1' ELSE 0 END as gdsendmail
   FROM
   `tbl_candidate_registration`
   inner JOIN
   `tbl_candidate_gdscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_gdscore`.candidateid 

   Where `tbl_candidate_registration`.candidateid = '".$token."'"; 
            // echo $sql;
            // die;

   // SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_generate_offer_letter_details`.filename 
   //        FROM `tbl_candidate_registration` LEFT join `tbl_generate_offer_letter_details` ON `tbl_candidate_registration`.candidateid  = `tbl_generate_offer_letter_details`.candidateid
   //          Where `tbl_candidate_registration`.candidateid = '".$token."' "


   $result = $this->db->query($sql)->result();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getSelectCandidGDScore($candidateid)
{

  try{


   $sql = "SELECT `tbl_candidate_gdscore`.gdscore,`tbl_candidate_gdscore`.rsscore,`tbl_candidate_gdscore`.ssscore,`tbl_candidate_gdscore`.`candidateid`,`tbl_candidate_gdscore`.cutoffmarks,`tbl_candidate_gdscore`.`gdpercentage` FROM `tbl_candidate_gdscore`  
   join `tbl_candidate_registration` on `tbl_candidate_gdscore`.candidateid =`tbl_candidate_registration`.candidateid 
   WHERE `tbl_candidate_gdscore`.gdscore > 0  AND `tbl_candidate_registration`.`candidateid`= $candidateid ORDER BY `tbl_candidate_registration`.candidateid ASC"; 

   $res = $this->db->query($sql)->row();



   return  $res;
 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getCampus($id)
{
  try{

    // $sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
    // ON a.`campusid` = b.`campusid` WHERE b.`recruiterid`= $id AND  a.`IsDeleted`=0";  

   if (!empty($id) AND $this->loginData->RoleID==30) {

     $sql = "SELECT * FROM `mstcampus` as a 
     Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
     inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
     WHERE a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND  c.`campus_status`=0  AND  b.`recruiterid`= ". $id; 

   } else{
    $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a 
    inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` 
    WHERE a.`IsDeleted`=0 AND b.`mailstatus`= 1 AND b.`campus_status`=0 ";
  }

  $res = $this->db->query($sql)->result();
  return $res;

}catch(Exception $e){
  print_r($e->getMessage());die();
}
}




public function getCampusCutOffMarks($campusid, $id)
{
  try{

   $sql = "SELECT * FROM tbl_campus_intimation as a Where `a`.`campusid`='".$campusid."' AND `a`.`id`='".$id."'   AND a.campus_status =0 "; 
   $res = $this->db->query($sql)->row();
   return $res;

 }catch(Exception $e){
  print_r($e->getMessage());die();
}
}



public function getgdmailstatus($campusid, $id)
{
  try{

   $sql = "SELECT * FROM tbl_campus_intimation as a Where `a`.`campusid`='".$campusid."' AND `a`.`id`='".$id."'   AND a.campus_status =0 "; 
   $res = $this->db->query($sql)->result()[0];
   return $res;

 }catch(Exception $e){
  print_r($e->getMessage());die();
}
}

}