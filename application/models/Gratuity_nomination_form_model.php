<?php 

/**
* State Model
*/
class Gratuity_nomination_form_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


  public function staffName($staff_id,$login_staff)
  {
   
    try{
     
     
           $sql = "SELECT * FROM staff where staffid !=$staff_id and staff.staffid!=$login_staff 
           AND new_office_id = ".$this->loginData->teamid." order by staff.name asc";  
           

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}

public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


  public function getCandidateWithAddressDetails($staff_id)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getCandidateWith($reportingto)
  {
    
    try{

        $sql = "SELECT
    staff.name as reprotingname
    
FROM
    staff WHERE   staff.staffid='$reportingto'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method get max get_gratuityworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  row
   */

public function get_gratuityworkflowid($token)
{

  try{

  $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=14 and staff.staffid=$token ORDER BY workflowid DESC LIMIT 1";
     // echo $sql; die;

    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
	public function getNomineedetail($candidateid)
  {
    
    try{

          $sql = 'SELECT  tbl_graduitynomination_detail.id as pro_id,tbl_graduitynomination_detail.share_nominee,tbl_graduitynomination_detail.sr_no,tbl_graduitynomination_detail.minior,tbl_graduitynomination_detail.graduity_id,tbl_graduitynomination_detail.name,tbl_graduitynomination_detail.relation_id,tbl_graduitynomination_detail.age,tbl_graduitynomination_detail.address,sysrelation.relationname,sysrelation.status,sysrelation.id FROM `tbl_graduitynomination_detail`  
                 left join sysrelation on   `tbl_graduitynomination_detail`.relation_id =  `sysrelation`.id  
                 left join   tbl_graduitynomination on  tbl_graduitynomination_detail.graduity_id = tbl_graduitynomination.graduity_id
                Where tbl_graduitynomination.candidate_id ='.$candidateid.''; 
               // echo $sql; die;
                 

        $result = $this->db->query($sql)->result();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function get_pfinformation($token)
  {
    
    try{

          $sql = 'SELECT * FROM `tbl_graduitynomination`  

                Where `tbl_graduitynomination`.candidate_id ='.$token.''; 
               //  echo $sql;
               // die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

   public function get_witness_detail($staff_id)
  {
    
    try{

        $sql = 'SELECT   a.address1,a.address2,b.name as name1 , c.name as name2 from tbl_graduitynomination a
                  inner join `staff` b on b.staffid = a.witness1
                  inner join `staff` c on c.staffid = a.witness2
                Where `a`.staff_id ='.$staff_id.''; 
                //echo $sql;
               // die();
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

  public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

}