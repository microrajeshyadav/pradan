
<?php 

/**
* Employee_particular_form_model Model
*/
class Employee_particular_form_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		
	}

  public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


	public function getCandidateDetailsPreview($token)
  {
  //echo "token".$token;
    try{

     $this->db->select('*,staff.gender as staff_gender,staff.name as staff_name,s.name as present_state,sp.name as parmanent_state,d.name as present_district,dp.name as permanent_district');

     $this->db->from("staff");
     $this->db->join('tbl_candidate_communication_address ' , 'staff.candidateid=tbl_candidate_communication_address.candidateid ', 'left');
     $this->db->join('tbl_candidate_registration', 'staff.candidateid=tbl_candidate_registration.candidateid ', 'left');
     $this->db->join('state s', 'tbl_candidate_communication_address.presentstateid=s.statecode', 'left');
     $this->db->join('state sp', 'tbl_candidate_communication_address.permanentstateid=sp.statecode', 'left');
     $this->db->join('district d', 'tbl_candidate_communication_address.presentdistrict=d.districtid', 'left');
     $this->db->join('district dp', 'tbl_candidate_communication_address.permanentdistrict=dp.districtid', 'left');

     $this->db->where('staff.staffid',$token);
     $query=$this->db->get();
 // echo "query=".$this->db->last_query();
 //   die;
     return $query->row();


   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidatePradanMemberDetails($staff_id)
{

  try{

    $sql = "SELECT
    *
    FROM
    staff a
    LEFT JOIN tbl_candidate_registration b ON
    a.candidateid = b.candidateid
    LEFT JOIN trnstaffrelatives c ON
    a.staffid = c.RelatedStaffid
    left join msdesignation d on 
    a.designation=d.desid
    left join lpooffice e on 
    a.staffid=e.staffid
    left join sysrelation s on
    c.Relation_Cd=s.id

    WHERE
    c.staffid = $staff_id"; 
    // die($sql);



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method get employ particular Details() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getEmployDetails($token)
{

  try{

   $sql = "SELECT
    * from tbl_staffemployee_particularsform WHERE candidate_id = $token";
    // die;



    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getSysLanguage() get Language Name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getSysLanguage()
 {

  try{
    $this->db->select('*');
    $this->db->from('syslanguage');
    $this->db->where('isdeleted','0');

      return $this->db->get()->result(); //echo $this->db->last_query(); die;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

/**
   * Method getCountLanguage() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

   public function getCountLanguage($staffid)
   {
      //echo "token=".$token;
    try{
      $sql = 'SELECT count(*) as `Lcount` FROM `msstaff_language_proficiency`      
      Where `msstaff_language_proficiency`.staffid ='.$staffid.''; 
     // echo $sql;
      // die();
      $result = $this->db->query($sql)->row();
      //print_r($result);

      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method getCandidateLanguageDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateLanguageDetails($candidateid)
{

  try{
    $sql = 'SELECT mslp.*,aepf.understand FROM `tbl_language_proficiency` as mslp
    left join tbl_staffemployee_particularsform as aepf on  mslp.candidateid =aepf.candidate_id  Where mslp.candidateid ='.$candidateid.''; 
    //echo $sql; die;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



  /**
   * Method getCountFamilyMember() get No Of family Member !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountFamilyMember($token)
  {

    try{
      $sql = "SELECT
      count(*) as Fcount
      FROM
      msstaff_family_members a
      LEFT JOIN staff b ON
      a.staffid = b.staffid
      LEFT JOIN sysrelation d ON
      a.relationwithemployee = d.id
      WHERE
      a.staffid ='$token'"; 
     // echo $sql;
      //die;
      $result = $this->db->query($sql)->row();
      //print_r($result);
      //die;
      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }




/**
   * Method getCandidateFamilyMemberDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateFamilyMemberDetails($token)
{

  try{

    $sql = "SELECT a.id as family_id,
    a.`staffid`,a.`relationwithemployee`,a.`familydob`,a.originalphotoname,a.encryptedphotoname,a.originalfamilyidentityphotoname,a.encryptedfamilyidentityphotoname,b.staffid,d.id,d.relationname,a.Familymembername
    FROM
    msstaff_family_members a
    LEFT JOIN staff b ON
    a.staffid = b.staffid

    LEFT JOIN sysrelation d ON
    a.relationwithemployee = d.id
    WHERE
    a.staffid= $token";
    //echo $sql;
   // die($sql);



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidateOtherInformationDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateOtherInformationDetails($token)
{

  try{
     // echo "staff=".$token;
    $sql = "SELECT * FROM `msstaff_other_information`  Where `msstaff_other_information`.staffid  ='$token'"; 
    //echo $sql;
    //die;

    $result = $this->db->query($sql)->row();
   // print_r($result);

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
/**
   * Method getCountWorkExprience() get No Identity Number !!!.
   * @access  public
   * @param NULL
   * @return  Array
   */

public function getCountWorkExprience($candidateid)
{

  try{
    $sql = 'SELECT count(*) as `WEcount` FROM `tbl_work_experience`      
    Where `tbl_work_experience`.candidateid ='.$candidateid.''; 
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getCandidateWorkExperienceDetails($candidateid)
{

  try{


    $sql="SELECT * FROM  tbl_work_experience where `candidateid`=$candidateid order by id";

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getCountTrainingExposure() get No Identity Number !!!.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCountTrainingExposure($candidateid)
 {

  try{
     $sql = 'SELECT count(*) as `TEcount` FROM `tbl_training_exposure`      
    Where `tbl_training_exposure`.candidateid ='.$candidateid.''; 
    $result = $this->db->query($sql)->row();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getCandidateTrainingExposureDetails() get Select Candidates List.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCandidateTrainingExposureDetails($candidateid)
{

  try{

    $sql = 'SELECT * FROM `tbl_training_exposure` Where `tbl_training_exposure`.candidateid ='.$candidateid.''; 
   // echo $sql; die;

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method get max eworkflowid() get Select staff.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getworkflowid($staff_id)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=8 and staff.staffid=$staff_id ORDER BY workflowid DESC LIMIT 1";
   // echo $sql; die;
    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method get max eworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_narrativeworkflowid($token)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=9 and staff.staffid=$token ORDER BY workflowid DESC LIMIT 1";
    
// echo $sql; die;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



/**
   * Method getCandidatedesignation() get Select Candidates List.
   * @access  public
   * @param staff id
   * @return  Array
   */
public function getCandidatedesignation($token)
{

  try{

    $sql = "SELECT
    *
    FROM
    staff a
    LEFT JOIN tbl_candidate_registration b ON
    a.candidateid = b.candidateid

    left join msdesignation d on 
    a.designation=d.desid

    WHERE
    a.staffid = $token";
    //die($sql);



    $result = $this->db->query($sql)->result();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function changedate($Date)
{
  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
  // print_r($date); die;
 if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
if ($date=='//') {
  $date = NULL;
}
return $date;
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



public function fetch($candidateid)
{
  try{

    $sql = "SELECT c.name, a.candidatefirstname,a.candidatelastname, d.desname, f.officename, c.emp_code FROM tbl_candidate_registration a  
    LEFT JOIN staff AS c ON c.candidateid = a.candidateid 
    LEFT JOIN msdesignation AS d ON c.designation = d.desid 
    LEFT JOIN lpooffice AS f ON f.staffid = c.staffid 
    WHERE a.candidateid=$candidateid  ";
   // echo $sql;
   
    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}

public  function table_flag($candidateid) {
  //die("hello");
  try{
  $sql= "SELECT
    a.flag AS narrative_flage,
    b.flag AS joining_report_flag,
    c.flag AS identity_flag,
    d.flag AS medical_certifacte_flag,
    e.flag AS ehnaced_flag,
    f.flag AS declarte_flag,
    g.flag AS membership_flag,
    em.status as employ_flag,
    pr.status as provident_flag,
    gd.isupdate as graduity_flag,
    n.status as nomination_flag
    
FROM  tbl_candidate_registration as h
left join tbl_general_nomination as n ON
h.candidateid=n.candidateid
left join tbl_graduitynomination as gd ON
h.candidateid=gd.candidate_id
left join provident_fund_nomination as pr ON
h.candidateid=pr.candidate_id
left join tbl_staffemployee_particularsform as em ON
h.candidateid=em.candidate_id
left join tblstaffnarrativeselfprofile as a on
h.candidateid=a.candidateid
    
LEFT JOIN tblstaffjoiningreport AS b
ON
    h.candidateid =b.candidate_id 
LEFT JOIN tblidentitycard AS c
ON
    h.candidateid = c.candidate_id
LEFT JOIN tbl_medical_certificate AS d
ON
    h.candidateid = d.candidate_id
LEFT JOIN tbl_for_enhanced AS e
ON
    h.candidateid = e.candidate_id
LEFT JOIN tbldeclartion_about_family AS f
ON
  h.candidateid = f.candidate_id
LEFT JOIN membership_application_form AS g
ON
    h.candidateid = g.candidate_id

WHERE
    h.candidateid =$candidateid";
 
  $tmmt= $this->db->query($sql)->row();

  return $tmmt;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


public function getpdf($token)
{

  try{

   $sql = "SELECT a.candidatefirstname, a.candidatelastname, b.offerno, b.doj, d.desname, f.officename, c.emp_code, g.narrative, g.flag FROM tbl_candidate_registration a INNER JOIN tbl_generate_offer_letter_details AS b ON b.candidateid = a.candidateid LEFT JOIN staff AS c ON c.candidateid = b.candidateid LEFT JOIN msdesignation AS d ON c.designation = d.desid LEFT JOIN lpooffice AS f ON f.staffid = c.staffid INNER JOIN tblstaffnarrativeselfprofile as g on g.candidateid=a.candidateid WHERE a.candidateid=$token"; 


   $res = $this->db->query($sql)->row();

   return $res;

 }catch (Exception $e) {
  print_r($e->getMessage());die;
}


}


public function getCandidateWithAddressDetails($staff_id)
  {
    
    try{

        $sql = "SELECT
    staff.emp_code,
    staff.name AS staff_name,
    staff.Joining_basicpay AS Joining_basicpay,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";

    // echo $sql;
    // die;
                //                // die();
                 
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function getCandidateWith($reportingto)
  {
    
    try{

        $sql = "SELECT
    staff.name as reprotingname
    
FROM
    staff WHERE   staff.staffid='$reportingto'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


public function generatePDF($dom,$filename)
{
  try{
    // instantiate and use the dompdf class
  
 // echo $dom;die();
  //echo 'sdsdd';die();
  $dompdf = new Dompdf();
  $dompdf->loadHtml($dom); 
    // (Optional) Setup the paper size and orientation
  $dompdf->setPaper('A4', 'portrait');
    // $dompdf->setPaper('A4', 'landscape');

    // Render the HTML as PDF
  $dompdf->render();

  
  $pdf = $dompdf->output();
  //$pdfPath = file_put_contents(FCPATH . "pdf_offerletters/$filename.pdf", $pdf);
  //$path = FCPATH . "pdf_offerletters/$filename.pdf";
    //echo $path;  die;

  $dompdf->stream($path, array('Attachment' =>0));

  //$dompdf->stream($path);

  //return $dompdf;

  //  $handle = fopen(FCPATH."pdf_offerletters/$filename.pdf", "r");
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}



 public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }









}