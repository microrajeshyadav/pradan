<?php 

/**
* IOM To Transfer Model
*/
class Iom_to_transfer_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

/**
   * Method getRole() list of roles .
   * @access  public
   * @param Null
   * @return  Array
   */
 	
public function getRole()
  {
    
    try{

       $sql = "SELECT * FROM `sysaccesslevel` Order by Acclevel_Name ASC  ";

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


  /**
   * Method getStaffList() list of Staff with staffid .
   * @access  public
   * @param Null
   * @return  Array
   */
  
public function getStaffList()
  {
    
    try{

       $sql = "SELECT
           staff.name,
           staff_transaction.staffid,
           staff_transaction.date_of_transfer,
           staff_transaction.new_office_id,
           staff_transaction.new_designation
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

        ORDER BY  `staff`.staffid ASC ";

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



 /**
   * Method staffDetailList() get Staff Detail.
   * @access  public
   * @param Null
   * @return  Array
   */


public function staffDetailList($staffid)
  {
    try{
    
           $sql = "SELECT
           staff.name,
           staff.contact,
           staff.emailid,
           staff.staffid
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

       where 1=1 and `staff`.staffid = $staffid  

       ORDER BY  `staff`.name ASC ";  

       $res = $this->db->query($sql)->result()[0];
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}



/**
   * Method getSingleRole() get role name .
   * @access  public
   * @param Null
   * @return  Array
   */
  
public function getSingleRole($roleid)
  {
    
    try{

       $sql = "SELECT * FROM `sysaccesslevel` WHERE Acclevel_Cd =".$roleid;

       $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


}