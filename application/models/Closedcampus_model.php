<?php 

/**
* Closed campus Model
*/
class Closedcampus_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}


  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "-" )
    $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
  // echo substr($Date,$len,-4);
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
      $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


	public function getCampus($id=null)
{
  try{

    // $sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
    // ON a.`campusid` = b.`campusid` WHERE b.`recruiterid`= $id AND  a.`IsDeleted`=0";  

     if (!empty($id) AND $this->loginData->RoleID==30) {
      
       $sql = "SELECT * FROM `mstcampus` as a 
       Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
       inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
     WHERE a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND  b.`campus_status`=0  AND  b.`recruiterid`= ". $id; 
       
     } else{
      $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND b.`mailstatus`= 1 AND b.`campus_status`=0 ";
     }

    $res = $this->db->query($sql)->result();
    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}




    public function getCampusEmailid($token)
    {
       try{

        $sql = "SELECT campusincharge,campusname,emailid,city FROM `mstcampus` WHERE `campusid` =$token";
        $result = $this->db->query($sql)->result();

        return $result;
        }catch (Exception $e) {
            print_r($e->getMessage());die;
       }
    }

   public function getCampusInchargeDetail($token)
    {
       try{
             
           $sql = 'SELECT `tbl_campus_intimation`.fromdate, `tbl_campus_intimation`.todate, `mstcampus`.campusname,`mstcampus`.emailid,`mstcampus`.campusincharge,`tbl_campus_intimation`.messsage, `tbl_campus_intimation`.mailstatus, `tbl_campus_intimation`.id, `tbl_campus_intimation`.campusid,`tbl_campus_intimation`.campus_status FROM `tbl_campus_intimation` Inner join  `mstcampus` ON `tbl_campus_intimation`.campusid=  `mstcampus`.campusid WHERE 
           `tbl_campus_intimation`.campus_status =0 AND `tbl_campus_intimation`.mailstatus= 1 '; 
           $sql .= ' ORDER BY `tbl_campus_intimation`.id DESC';

        $result = $this->db->query($sql)->result();

        return $result;

        }catch (Exception $e) {
            print_r($e->getMessage());die;
       }
    }





 

}