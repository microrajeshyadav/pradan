

<?php 

/**
* Master  Model
*/
class Probation_ed_reviewofperformance_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

	public function get_providentworkflowid($token)
{

  
//echo "hello";

 try{

$sql = "SELECT
  tbl_workflowdetail.workflowid as workflowid,tbl_workflowdetail.flag
FROM
    `tbl_workflowdetail`
INNER JOIN staff ON staff.staffid = tbl_workflowdetail.staffid
WHERE
    tbl_workflowdetail.type = 22 AND tbl_workflowdetail.r_id = $token
     ORDER BY tbl_workflowdetail.workflowid DESC LIMIT 1";
             $res = $this->db->query($sql)->row();
             return $res;
              }
              catch (Exception $e) {
       print_r($e->getMessage());die;
     }

//echo $sql;

}

/*
	 * Method getStaffHRDList() get staff Details .
	 * @access	public


	 * @param	
	 * @return	array
	 */


		
public function getstaff($token)
  {
    

    try{


     $sql = "SELECT
    SUBSTRING(staff.name, 1, LOCATE(' ',staff.name)) as name,
    staff.staffid,
    staff.probation_date,
    staff.doj,
    staff.gender,
    staff.emailid,
    staff.reportingto,
    staff.emp_code,
    staff.doj_team,
    staff.candidateid,
    staff_transaction.id,
    staff_transaction.staffid,
    staff_transaction.trans_status,
    staff_transaction.date_of_transfer,
    staff_transaction.new_office_id,
    staff_transaction.new_designation,
    staff_transaction.reason,
    staff_transaction.old_office_id,
    staff_transaction.old_designation,
    staff.designation,
    lpooffice.officename,
    msdesignation.desname,
    offerdetail.offerno,
    msdesignation.level AS levelname,
    staff.joiningandinduction FROM staff_transaction
INNER JOIN staff ON staff_transaction.staffid = `staff`.`staffid`
INNER JOIN lpooffice ON staff.new_office_id = lpooffice.`officeid`
INNER JOIN msdesignation ON staff.designation = msdesignation.`desid`
left JOIN tbl_generate_offer_letter_details as offerdetail ON staff.candidateid = offerdetail.candidateid
WHERE
    staff_transaction.id = $token and staff_transaction.trans_status='Midtermreview' 
"; 
//echo $sql;
//die;

      return  $this->db->query($sql)->row();
      }catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


	/**
	 * Method loginstaff() get login staff  .
	 * @access	public
	 * @param	
	 * @return	array
	 */

	public function loginstaff($staffid)
  {

    
    try{
   $sql = "SELECT staff.staffid,staff.name,msdesignation.desname FROM `staff` 
   inner join msdesignation on staff.designation=msdesignation.desid 
   where `staffid`=
".$staffid;


         $res = $this->db->query($sql)->row();
        return $res;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



	public function getStaffHRDList()
	{
		
		try{

		 $sql = "SELECT *,staff.emailid as staff_email FROM mstuser   
		 inner join staff on mstuser.staffid=staff.staffid  
		 WHERE RoleID = 16  AND `IsDeleted` = 0";
		
     	$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


/**
	 * Method getStaffHRDList() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStaffPersonnalList()
	{
		

		
		try{

		 $sql = "SELECT staff.name,msdesignation.desname,staff.staffid FROM mstuser 

		 	inner join staff on staff.staffid=mstuser.staffid
		 	inner join msdesignation on staff.designation=msdesignation.desid
		 WHERE mstuser.RoleID = 17  AND mstuser.IsDeleted = 0 ORDER BY mstuser.staffid ASC";
		// echo $sql;

     	$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



/**
	 * Method getStaffProbationReviewofPerformance() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStaffProbationReviewofPerformance($token)
	{

		
		try{

		 	$sql =  "SELECT
			 `staff`.name,`staff`.emp_code,`staff`.staffid,`staff`.reportingto as supervisorid,`msdesignation`.desname,`lpooffice`.officename
			FROM
			staff_transaction
		left JOIN(
			SELECT
			`staff_transaction`.staffid,
			MAX(
			`staff_transaction`.date_of_transfer
			) AS MaxDate
			FROM
			staff_transaction
			GROUP BY
			staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join lpooffice on `staff_transaction`.new_office_id = `lpooffice`.officeid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid

		WHERE `staff_transaction`.id = $token
		ORDER BY
		`staff_transaction`.staffid ";

		

		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');
	
		$result = $this->db->query($sql)->row();

			
			
		return $result;


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



/**
	 * Method getstaffprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getstaffprobationdetals($token)
	{

		
		try{

		  	$sql = "SELECT
		  	prp.*,
		  	 s.name,
    s.emp_code,
    d.desname,
    lp.officename,

			`staff_transaction`.staffid,
			
			MAX(
			`staff_transaction`.trans_flag
			) AS trans_flag,

			MAX(
			`staff_transaction`.id
			) AS transid
			FROM
			staff_transaction 
            INNER JOIN tbl_probation_review_performance as prp ON staff_transaction.staffid = prp.staffid

             inner join staff as s on 
    s.staffid=staff_transaction.staffid
    inner JOIN msdesignation as d on 
    d.desid=s.designation
    inner join lpooffice as lp ON
    lp.officeid=s.new_office_id

			WHERE
			staff_transaction.id =".$token." AND prp.transid = ".$token; 
			//echo $sql;
			//die;

		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



/**
	 * Method getstaffpeersonnelprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getstaffpeersonnelprobationdetals($token)
	{
	
		try{

			$sql = "SELECT prp.*,prp.createdby, stf.name,stf.emp_code,stf.staffid,msdes.desname,lpo.officename as newofficename,stf.doj_team,	(month(stf.probation_extension_date) - month(stf.probation_date)) as month FROM tbl_probation_review_performance as prp
		 			inner join staff as stf ON prp.createdby = stf.staffid
		 			inner join msdesignation as msdes ON stf.designation = msdes.desid
		 			inner join lpooffice as lpo ON stf.new_office_id = lpo.officeid 
		 			inner join staff_transaction as tt 
    on prp.staffid=tt.staffid
    where tt.id='$token'
		 			";// die;
		 			

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getstaffpeersonnelprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getspeersonneldetails()
	{
	
		try{

			$sql = "select * from mstuser inner join staff on mstuser.staffid=staff.staffid where mstuser.RoleID=17 && IsDeleted=0
		 			";// die;
		 			

		$result = $this->db->query($sql)->result();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}







/**
	 * Method getstaffpersonnaldetails() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getstaffpersonnaldetails()
	{
			
			
		
		try{

		  	  $sql = "SELECT
			 `staff`.name as personnalname,`staff`.emp_code as personnalempcode,`staff`.staffid as personnalstaffid,`msdesignation`.desname as personnaldesignationname,
			 lpooffice.officename as officename
			FROM
			staff_transaction
			INNER JOIN(
			SELECT
			`staff_transaction`.staffid,
			MAX(
			`staff_transaction`.date_of_transfer
			) AS MaxDate
			FROM
			staff_transaction
			GROUP BY
			staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		inner join lpooffice on  `staff_transaction`.new_office_id = `lpooffice`.officeid
		inner join mstuser on `staff_transaction`.staffid = `mstuser`.staffid
		WHERE  `mstuser`.RoleID = 17 ORDER BY `staff_transaction`.staffid ";

		

		$result = $this->db->query($sql)->result()[0];

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}
/**
	 * Method getpersonnalName() get person Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */

public function getpersonnalName()
	{
		//echo "hello";
		//die;
		
		try{

		  	$sql = "SELECT staff.staffid,staff.name from staff inner join mstuser on staff.staffid=mstuser.staffid inner join msdesignation on staff.designation=msdesignation.desid where mstuser.RoleID=17 and mstuser.IsDeleted=0";
		  		
		     $result = $this->db->query($sql)->result();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getstaffpersonnaldetails() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getstaffPnndetails()
	{

		
		try{

		  	echo   $sql = "SELECT
			 `staff`.name as personnalname,`staff`.emp_code as personnalempcode,`staff`.staffid as personnalstaffid,`msdesignation`.desname as personnaldesignationname
			FROM
			staff_transaction
			INNER JOIN(
			SELECT
			`staff_transaction`.staffid,
			MAX(
			`staff_transaction`.date_of_transfer
			) AS MaxDate
			FROM
			staff_transaction
			GROUP BY
			staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		inner join mstuser on `staff_transaction`.staffid = `mstuser`.staffid
		WHERE  `staff`.staffid =". $this->loginData->staffid ." AND `mstuser`.RoleID = 17 ORDER BY `staff_transaction`.staffid "; //die;

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


/**
	 * Method getSupervisor() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
		 
public function getSupervisor($token)
{


	try{

		$sql = "SELECT
		`staff`.name as supervisorname,`staff`.emp_code as supervisorempcode,`staff`.staffid as supervisorstaffid,`msdesignation`.desname as supervisordesignationname,`lpooffice`.officename AS edofficename 
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)

		inner join staff on `staff`.staffid = `staff_transaction`.staffid

		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		INNER JOIN lpooffice ON `staff_transaction`.new_office_id =`lpooffice`.`officeid`

		WHERE  `staff`.staffid =".$token."  AND `staff`.designation   IN(4,16)  ORDER BY `staff_transaction`.staffid ";

		// echo $sql; die;
		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getEDName() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getEDName()
	{
		
		try{

		  	$sql = "SELECT staff.staffid ,staff.name from staff inner join mstuser on staff.staffid=mstuser.staffid inner join msdesignation on staff.designation=msdesignation.desid where mstuser.RoleID=18 and mstuser.IsDeleted=0";
		  		
		     $result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

    
}