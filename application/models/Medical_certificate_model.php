<?php 

/**
* State Modelssq
*/

require_once APPPATH . 'vendor/autoload.php';
use Dompdf\Dompdf;
class Medical_certificate_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}


  public function getCandidateWithAddressDetails($staff_id)
  {
    
    try{
        $sql = "SELECT
    staff.emp_code,
    SUBSTRING(staff.name, 1, LOCATE(' ',staff.name)) AS staff_name,
    msdesignation.desname AS desiname,
    
    `state`.name,
    `lpooffice`.officename,
    `staff`.new_office_id,
    staff.permanenthno,
    staff.permanentstreet,
    staff.presentdistrict,
    staff.presentcity,
    staff.emailid,
    staff.reportingto,
    staff.dob
    
FROM
    staff

LEFT JOIN `state` ON `state`.id =staff.permanentstateid
LEFT JOIN `staff` as st ON `staff`.staffid =st.reportingto
LEFT JOIN `msdesignation` ON `staff`.designation = `msdesignation`.desid
LEFT JOIN `lpooffice` ON `staff`.new_office_id = `lpooffice`.officeid

WHERE
    staff.staffid='$staff_id'";
                //                // die();
                // echo $sql;
                // die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



 /**
   * Method get max get_medicalworkflowid() get workflow id .
   * @access  public
   * @param Null
   * @return  row
   */

public function get_medicalworkflowid($staff_id)
{

  try{

    $sql = "SELECT (`workflowid`) as workflow_id,`tbl_workflowdetail`.flag FROM `tbl_workflowdetail`
    inner join  staff on staff.staffid=tbl_workflowdetail.staffid

     WHERE  tbl_workflowdetail.type=19 and staff.staffid=$staff_id ORDER BY workflowid DESC LIMIT 1";
    
// echo $sql; die;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * staff_reportingto() getStaff repotion to
   * @access  public
   * @param Null
   * @return  row
   */


public function staff_reportingto($staff_id)
{
 // echo "staff=".$staff_id;
  //die();

  try{

     $sql = "select reportingto from staff where  staffid = '$staff_id'"; 
    // echo $sql; die;/

    $res = $this->db->query($sql)->row();

    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



  public function do_uploads($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif|pdf',
        'upload_path'      =>    FCPATH . "datafiles/medicl_certificate/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );


  $this->load->library('upload',$config);
  if(!$this->upload->do_upload('pdf_gen'))
  {
    echo $this->upload->display_errors();

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

   public function do_uploadss($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/signature/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );


  $this->load->library('upload',$config);
  if(!$this->upload->do_upload('doctorssignature'))
  {
   $this->session->set_flashdata("er_msg", $this->upload->display_errors('',''));
        redirect(current_url());

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


 public function do_uploadd($element_name = 'userfile', $allowed_types = ['jpg','png'])
{
  try{
   $config = array(
        'allowed_types'    =>    'jpg|jpeg|png|gif',
        'upload_path'      =>    FCPATH . "datafiles/signature/",
        'max_size'         =>    10000,
        'encrypt_name'     =>    TRUE,
        'file_ext_tolower' =>    TRUE
    );


  $this->load->library('upload',$config);
  if(!$this->upload->do_upload('signatureofcandidate'))
  {
    echo $this->upload->display_errors();

  }

  else   //if upload is successfully
  {
    $fileData=$this->upload->data();  

    //die();
return $fileData;
}
}catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

public function fetchdatas($candidateid)
{

  try{

    $sql = "SELECT a.candidatefirstname, a.candidatelastname, b.offerno, b.doj, a.dateofbirth, d.desname, f.officename FROM tbl_candidate_registration a INNER JOIN tbl_generate_offer_letter_details as b on b.candidateid=a.candidateid LEFT JOIN staff as c on c.candidateid=b.candidateid LEFT JOIN msdesignation as d on c.designation=d.desid LEFT JOIN lpooffice as f on f.staffid=c.staffid where a.candidateid =$candidateid";
    // echo $sql; die;

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



  public function fetchpdfdata($candidateid)
{

  try{

    $sql = "SELECT a.candidatefirstname, a.candidatelastname, b.offerno, b.doj, a.dateofbirth, d.desname, f.officename, z.* FROM tbl_candidate_registration a INNER JOIN tbl_generate_offer_letter_details AS b ON b.candidateid = a.candidateid LEFT JOIN staff AS c ON c.candidateid = b.candidateid LEFT JOIN msdesignation AS d ON c.designation = d.desid LEFT JOIN lpooffice AS f ON f.staffid = c.staffid INNER JOIN tbl_medical_certificate as z ON z.candidate_id=a.candidateid WHERE a.candidateid =$candidateid";

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}










public function fetchtblcertificate($candidateid)
{

  try{

     $sql = "Select * from  tbl_medical_certificate where candidate_id=$candidateid";
     // echo $sql; die;

    $result = $this->db->query($sql)->row();

   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}






   public function getCandidateDetailsPreview($Date)
  {
//echo strlen($Date);
    try{
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
   if ($date=='//') {
      $date = NULL;
   }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

public function tc_email($reportingto)
  {
    
    try{

        $sql = "SELECT
    
    staff.emailid
    
    
    
    
FROM
    staff



WHERE
    staff.staffid='$reportingto'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->result();

        return $result[0];

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
    public function personal_email()
  {
    
    try{

        $sql = "SELECT
    
    *
    
    
    
    
FROM
    mstuser



WHERE
    RoleID=17 and IsDeleted='0'";
                               // die();
                //echo $sql;
                //die;
        
        $result = $this->db->query($sql)->row();

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
	
}