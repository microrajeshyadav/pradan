<?php 

/**
* Dashboard Model
*/
class Assigntc_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{

	}

	public function getTeamlist()
	{
		try{

      $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No' ORDER BY `officename`";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


  public function getBatchlist()
  {
    try{

      $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }



public function getCampus($id=NULL)
{
  try{

//print_r($this->loginData);

     //$sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
   // ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0"; 

    if (!empty($id) AND $this->loginData->RoleID==30) {
       if ($this->loginData->RoleID==17) {
          $sql = "SELECT * FROM `mstcampus` as a 
       Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
       inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
     WHERE a.campusid =0 and a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND b.`campus_status`=0  AND b.`recruiterid`= ". $id; 
        }
        else{
      $sql = "SELECT * FROM `mstcampus` as a 
       Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
       inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
     WHERE a.campusid !=0 and a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND b.`campus_status`=0  AND b.`recruiterid`= ". $id; 

        } 
     
       
     } else{
      if ($this->loginData->RoleID==17) {$sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.campusid =0 and a.`IsDeleted`=0 AND b.`mailstatus`= 1  AND b.`campus_status`=0 ";
      }
      elseif ($this->loginData->RoleID==10 || $this->loginData->RoleID==21) 
      {
        $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE  a.`IsDeleted`=0 AND b.`mailstatus`= 1  AND b.`campus_status`=0 ";

      }
      else
      {
        $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.campusid !=0 and a.`IsDeleted`=0 AND b.`mailstatus`= 1  AND b.`campus_status`=0 ";

      }
      

     }

   // echo $sql; die;

    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}





  public function getRoleEdDetails()
  {
    try{

      $sql = "SELECT staff.staffid,staff.name,staff.emailid FROM `staff` LEFT join  
        mstuser as u on u.staffid = staff.staffid 
        where staff.designation = 2 and staff.status = 1 and u.IsDeleted=0 GROUP BY staffid";  
      $res = $this->db->query($sql)->result();

      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

  public function getTeamDetail()
  {
    try{

      $sql = "SELECT tbl_candidate_registration.`teamid`,tbl_candidate_registration.`fgid`,tbl_candidate_registration.`batchid`, staff.staffid FROM `tbl_candidate_registration` left join staff on staff.candidateid=tbl_candidate_registration.candidateid";  
      $result = $this->db->query($sql)->result();

      return $result;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }
  public function getGraduateDetail($staffid)
  {
    try{

      $sql = "SELECT recommend_to_graduate from staff Where staffid=".$staffid;  
      $result = $this->db->query($sql)->row();

      /*echo "<pre>";
      print_r($sql);exit();*/
      if($result){

        return $result;
      }else{
        $result['recommend_to_graduate'] = 0;
        return $result;
      }

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }

public function getSingleTeamDetail($token=NULL)
  {
    try{

       $sql = "SELECT tbl_candidate_registration.`teamid`,tbl_candidate_registration.`fgid`,tbl_candidate_registration.`batchid`, staff.staffid FROM `tbl_candidate_registration` left join staff on staff.candidateid=tbl_candidate_registration.candidateid WHERE `tbl_candidate_registration`.candidateid = $token";  
      $result = $this->db->query($sql)->row();

      return $result;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


  public function getSelectedCandidate($campusid, $campusintimationid)
  {

    try{

      //,`tbl_generate_offer_letter_details`.filename
     
      $sql = "SELECT `tbl_candidate_registration`.AdminApproval,`tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.campustype,`tbl_candidate_registration`.emailid,`mstcampus`.`campusname`,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`mstcategory`.`categoryname`,`tbl_candidate_registration`.candidatelastname, `lpooffice`.officename, `mstbatch`.batch,`staff`.name,  `tbl_generate_offer_letter_details`.sendflag,`tbl_generate_offer_letter_details`.id as letterid, `tbl_generate_offer_letter_details`.flag,
      (CASE WHEN tbl_candidate_registration.encryptofferlettername IS NULL THEN tbl_generate_offer_letter_details.filename WHEN tbl_candidate_registration.encryptofferlettername IS NOT NULL THEN tbl_candidate_registration.encryptofferlettername END) as filename, 
      (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender,COALESCE(
     nullif(otherspecialisation, ''),
     nullif(pgspecialisation, ''),
     nullif(ugspecialisation, '')
  ) AS stream
      FROM `tbl_candidate_registration`
     left join `tbl_candidate_hrscore` ON `tbl_candidate_hrscore`.candidateid =`tbl_candidate_registration`.candidateid
      left join `lpooffice` ON `lpooffice`.officeid=`tbl_candidate_registration`.teamid
     LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
      left join `mstbatch` ON `mstbatch`.id=`tbl_candidate_registration`.batchid
      left join `staff` ON `staff`.staffid=`tbl_candidate_registration`.fgid
      left join `mstcategory` ON `mstcategory`.id = `tbl_candidate_registration`.categoryid
      left join `tbl_generate_offer_letter_details` ON `tbl_generate_offer_letter_details`.candidateid=`tbl_candidate_registration`.candidateid";
      if ($this->loginData->RoleID == 17) {
        $sql .= " left join `tbl_hrd_verification_document` ON `tbl_hrd_verification_document`.candidateid = `tbl_candidate_registration`.candidateid";
      }

      $sql .= " Where `tbl_candidate_hrscore`.hrscore > 1 and (recommend_to_graduate = 0  or recommend_to_graduate is null)";

      if($campusid == 0 && $campusid != 'NULL'){
       $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
        AND  `tbl_candidate_registration`.`campusintimationid`= $campusintimationid 
       AND `tbl_candidate_registration`.`campustype` = 'off' 
       AND `tbl_candidate_registration`.`inprocess` = 'closed' 
       AND `tbl_candidate_registration`.`yprststus` = 'yes'
       AND `tbl_candidate_registration`.`BDFStatusaftergd` = 1
       AND (`tbl_candidate_registration`.`complete_inprocess`= 0 OR `tbl_candidate_registration`.`complete_inprocess`= 1)
      AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
     }elseif(empty($campusid)){

      $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' 
       AND  `tbl_candidate_registration`.`campusintimationid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`complete_inprocess`= 0";

    } else{

      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid  
       AND  `tbl_candidate_registration`.`campusintimationid`= $campusintimationid 
    AND `tbl_candidate_registration`.`campustype` ='on' 
    AND `tbl_candidate_registration`.`BDFStatusaftergd` = 1
    AND `tbl_candidate_registration`.`inprocess` ='closed' 
    AND (`tbl_candidate_registration`.`complete_inprocess`= 0 OR `tbl_candidate_registration`.`complete_inprocess`= 1)  ";

    }

   if ($this->loginData->RoleID == 16) {
     // $sql .= " AND `tbl_candidate_registration`.`categoryid`NOT IN(2,3) "; 
    $sql .= " AND `tbl_candidate_registration`.`categoryid` IN(1) and (`tbl_candidate_registration`.old_candidate_id is null or `tbl_candidate_registration`.old_candidate_id  =0 )";
   }

   if ($this->loginData->RoleID == 17) {
      $sql .= " AND `tbl_candidate_registration`.`categoryid` IN(2,3) AND tbl_hrd_verification_document.status=1 and (`tbl_candidate_registration`.old_candidate_id is null or `tbl_candidate_registration`.old_candidate_id  =0 )"; 
   }

    $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid DESC';
     // echo $sql;
     // die;
    
     
    $res = $this->db->query($sql)->result();


    return $res;
 

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



public function getSingelRecruitersCampus($id){

  try{

       $sql = "SELECT *  FROM `tbl_generate_offer_letter_details` Where `id` = $id ";

       $res = $this->db->query($sql)->result();


    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


public function getflag($candidateid)
{

  try{
    $this->db->where('candidateid',$candidateid);
    $query = $this->db->get('tbl_generate_offer_letter_details');
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


public function getEdcommentStatus($token=NULL)
{

  try{

    $sql = "SELECT *  FROM `edcomments` Where `flag` !='NULL'   AND `candidateid` = $token ";
    $query = $this->db->query($sql);

   // echo $query->num_rows(); 

    if ($query->num_rows() >0) {
     $res = $this->db->query($sql)->result()[0];

      return $res;
    }else{

      return -1;
      }

    
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


 public function getCategoryid($token)
  {
    try{

      $sql = "SELECT categoryid FROM `tbl_candidate_registration`  WHERE candidateid =".$token;  
      $res = $this->db->query($sql)->row();
      return $res;

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }
  }


}