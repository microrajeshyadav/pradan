<?php 

/**
* State Model
*/
class Stafftransferpromotion_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();


	}

  /**
   * Method getview_history() display the details .of staff
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getview_history($token)
  {
    try{
      // $this->db->select("staff.name,staff_transaction.old_office_id,staff_transaction.date_of_transfer,staff_transaction.new_office_id,lpooffice.officename as 'present_office', staff_transaction.old_designation, staff_transaction.new_designation,msdesignation.desname,msdesignation.desid  as 'office_id'");
      // $this->db->from('staff_transaction');
      // $this->db->join('staff', 'staff_transaction.staffid = staff`.staffid ', 'inner');
      // $this->db->join('lpooffice', 'lpooffice.officeid = staff_transaction.old_office_id', 'inner');
      // $this->db->join('msdesignation', 'msdesignation.desid = staff_transaction`.`old_designation', 'lnner');
      // $this->db->where('staff_transaction.staffid',$token);
      // $this->db->where('staff_transaction.trans_status','Promotion','Transfer');
    $sql="SELECT a.staffid, b.name, c.officename AS oldofffice, a.date_of_transfer, e.desname AS new_designation, f.desname AS old_designation, z.officename as newoffice, REPLACE(a.trans_status, 'BOTH', 'TCR') as trans_status FROM staff_transaction AS a 
    INNER JOIN staff AS b ON a.staffid = b.staffid 
    left JOIN lpooffice AS c ON c.officeid = a.old_office_id 
    left JOIN lpooffice as z on z.officeid=a.new_office_id 
    left JOIN msdesignation AS e ON e.desid = a.new_designation 
    left JOIN msdesignation AS f ON f.desid = a.old_designation 
    WHERE a.staffid = $token AND a.trans_status IN('Both')";


      //$query=$this->db->get();
    return  $this->db->query($sql)->result();
     // echo $this->db->last_query();
    }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
  }


/**
   * Method getStaffDetails() get Staff details .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getstaffDetails($token){


  try{

    $sql = "SELECT .`sttr`.designation,`sttr`.reportingto,`sttr`.name,`sttr`.emailid,`lpo`.officename FROM `staff_transaction` as `sttf` 
    LEFT JOIN `staff` as `sttr` ON `sttf`.staffid  = `sttr`.staffid
    LEFT JOIN `lpooffice` as `lpo` ON sttf.`new_office_id` = `lpo`.officeid 
    Where `sttf`.staffid = $token ";
    $result = $this->db->query($sql)->result()[0];

    return $result;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


    /**
   * Method get_staffDetails() display the details .of staff location and designation
   * @access  public
   * @param Null
   * @return  row
   */

    public function get_staffDetails($token)
    {
     try
     {

      $sql="SELECT
      staff.emp_code,
      staff.name,
      msdesignation.desname,
      staff.emailid,
      staff_transaction.staffid,
      staff_transaction.reportingto,
      staff_transaction.date_of_transfer,
      staff_transaction.new_office_id,
      staff_transaction.new_designation,
      staff.name AS 'staff_name',
      staff.emp_code,
      staff.designation,
      staff.new_office_id,
      staff.reportingto,
      staff.encrypted_signature,
      lp.officename as old_name,
      lp1.officename as new_name,
      msdesignation.desname
      FROM
      staff_transaction
      INNER JOIN(
      SELECT staff_transaction.staffid,
      staff_transaction.date_of_transfer,
      MAX(
      staff_transaction.date_of_transfer
      ) AS MaxDate
      FROM
      staff_transaction
      GROUP BY
      staffid
      ) AS TMax
      ON
      staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
      INNER JOIN `staff` ON `staff_transaction`.staffid = `staff`.`staffid`

      LEFT JOIN lpooffice AS lp
      ON
      staff_transaction.new_office_id = lp.officeid
      LEFT JOIN lpooffice AS lp1
      ON
      staff_transaction.old_office_id = lp1.officeid
      INNER JOIN msdesignation ON staff.designation = msdesignation.desid
      WHERE
      `staff`.staffid = $token
      ORDER BY
      `staff`.name ASC
      ";

// echo $sql;die();
      //$query=$this->db->get();
      return  $this->db->query($sql)->row();

    }

    catch (Exception $e) {
     print_r($e->getMessage());die;
   }

 }

 public function index()
 {

 }


    /**
   * Method getstaffname() get all staff name
   * @access  public
   * @param Null
   * @return  array
   */

    public function getstaffname($token)
    {
      try{
     $sql = "SELECT
     staff.emp_code,
     staff.name,
     msdesignation.desname,
     staff_transaction.staffid,
     staff_transaction.date_of_transfer,
     staff_transaction.new_office_id,
     staff_transaction.new_designation
     FROM
     staff_transaction
     INNER JOIN(
     SELECT
     staff_transaction.staffid,
     MAX(
     staff_transaction.date_of_transfer
     ) AS MaxDate
     FROM
     staff_transaction
     GROUP BY
     staffid
     ) AS TMax
     ON
     `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
     'Resign',
     'Termination',
     'Retirement',
     'Death'
     )

     INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`
        INNER JOIN `msdesignation` on `msdesignation`.desid = `staff`.`designation`
     where 1=1 and `staff`.staffid NOT IN (1)  and staff_transaction.staffid =".$token." 

     group by `staff_transaction`.staffid ORDER BY  `staff`.name ASC ";  
     return  $this->db->query($sql)->row();

     }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
   }
  /**
   * Method get_alloffice() get all office location
   * @access  public
   * @param Null
   * @return  array
   */
  public function get_alloffice($p_office=null)
  {
   try
   {
    //echo "office id=".$p_office;
   // die;
    $this->db->select("officeid,officename");
    

    $this->db->from('lpooffice');
    $this->db->where_not_in('officeid',$p_office);

    $query=$this->db->get();
     // echo $this->db->last_query();
      //die();
    return $query->result();
  }


  catch (Exception $e) 
  {
   print_r($e->getMessage());die;
 }

}


 /**
   * Method get_alloffice() get all designation 
   * @access  public
   * @param Null
   * @return  array
   */


 public function get_alldesignation($p_designation=null)
 {
   try
   {
    $this->db->select("desid,desname");
    $this->db->from('msdesignation');
    $this->db->where_not_in('desid',$p_designation);


    $query=$this->db->get();
      // echo $this->db->last_query();
      // die();
    return $query->result();
  }


  catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function getStaffTransactioninc($tablename,$incval){


  try{

        // echo $tablename;
        // echo $incval;  die;
        ////'staff_transaction',@a; 

    $sql = "select get_maxvalue($tablename,$incval) as maxincval";

    $result = $this->db->query($sql)->result()[0];

    return $result;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method get_staffReportingto() get staff reportingto. 
   * @access  public
   * @param Null
   * @return  row
   */

  public function get_staffReportingto($token)
  {

   try
   {

    $sql=" SELECT s1.staffid, s1.reportingto, s1.designation, s.emailid, s.name FROM staff as s1 LEFT JOIN staff as s on s1.reportingto = s.staffid  where s1.staffid = ".$token;
         //echo $sql;exit;
    return  $this->db->query($sql)->result()[0];

  } catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}




}