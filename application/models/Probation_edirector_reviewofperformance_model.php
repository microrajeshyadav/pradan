<?php 

/**
* Master  Model
*/
class Probation_edirector_reviewofperformance_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

	public function get_providentworkflowid($staffid)
{

  try{

    $sql = "SELECT
  tbl_workflowdetail.workflowid as workflowid,tbl_workflowdetail.flag,tbl_workflowdetail.receiver
FROM
    `tbl_workflowdetail`
INNER JOIN staff ON staff.staffid = tbl_workflowdetail.staffid
WHERE
    tbl_workflowdetail.type = 7 AND tbl_workflowdetail.staffid = $staffid
     ORDER BY tbl_workflowdetail.workflowid DESC LIMIT 1";
    
// echo $sql; die;


    $result = $this->db->query($sql)->row();


    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
	 * Method getstaffid() get staff ID .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffid($token)
{


	try{


		 $sql = "
		SELECT
		SUBSTRING(`staff`.name, 1, LOCATE(' ',`staff`.name)) as name,
		`staff`.emp_code,
		`staff`.staffid,
		`staff`.emailid,
		`staff`.reportingto AS supervisorid,
		`msdesignation`.desname,
		`lpooffice`.officename
		FROM
		staff_transaction
		INNER JOIN staff ON `staff`.staffid = `staff_transaction`.staffid
		INNER JOIN lpooffice ON `staff_transaction`.new_office_id = `lpooffice`.officeid
		INNER JOIN msdesignation ON `staff_transaction`.new_designation = `msdesignation`.desid
		WHERE
		`staff_transaction`.id = $token
		ORDER BY
		`staff_transaction`.staffid "; 
		//echo $sql; die;
		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');

	return	$result = $this->db->query($sql)->row();

		// //print_r($result); die;

		// return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

public function getStaffPersonnalList()
	{

		
		try{

		 $sql = "SELECT * FROM mstuser WHERE RoleID = 17  AND `IsDeleted` = 0 ORDER BY staffid ASC";
		 // echo $sql; die;
     	$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


/**
	 * Method getStaffProbationReviewofPerformance() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getStaffProbationReviewofPerformance($token)
{


	try{

		$sql = "SELECT
		SUBSTRING(`staff`.name, 1, LOCATE(' ',`staff`.name)) as name,`staff`.emp_code,`staff`.staffid,`staff`.reportingto as supervisorid,`msdesignation`.desname,`lpooffice`.officename
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join lpooffice on `staff_transaction`.new_office_id = `lpooffice`.officeid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		WHERE `staff_transaction`.id = $token
		ORDER BY
		`staff_transaction`.staffid ";

		///AND `staff_transaction`.trans_status NOT IN('Resign','Termination','Retirement','Death');

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



/**
	 * Method getstaffprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffprobationdetals($token)
{


	try{

		$sql = "SELECT prp.* FROM tbl_probation_review_performance as prp
		WHERE prp.`transid` =".$token;
		// echo $sql; die;
		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



/**
	 * Method getstaffpeersonnelprobationdetals() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffpeersonnelprobationdetals($token)
{
	
	try{

		$sql = "SELECT prp.*,prp.createdby, stf.name,stf.emp_code,stf.staffid,msdes.desname,lpooffice.officename as officename FROM tbl_probation_review_performance as prp
		inner join staff as stf ON prp.createdby = stf.staffid
		inner join msdesignation as msdes ON stf.designation = msdes.desid
		inner join lpooffice on  stf.new_office_id = `lpooffice`.officeid
		Where prp.transid= $token";// die;
		$result = $this->db->query($sql)->result()[0];

		 return $result;

		 		}catch (Exception $e) {
		 			print_r($e->getMessage());die;
		 		}

		 	}





/**
	 * Method getstaffpersonnaldetails() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffpersonnaldetails()
{


	try{

		$sql = "SELECT
		`staff`.name as personnalname,`staff`.emp_code as personnalempcode,`staff`.staffid as personnalstaffid,`msdesignation`.desname as personnaldesignationname,
		`lpooffice`.officename as officename
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		inner join lpooffice on  `staff_transaction`.new_office_id = `lpooffice`.officeid
		inner join mstuser on `staff_transaction`.staffid = `mstuser`.staffid
		WHERE  `staff`.staffid =". $this->loginData->staffid ." AND `mstuser`.RoleID = 2 ORDER BY `staff_transaction`.staffid ";

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getstaffpersonnaldetails() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getstaffPnndetails()
{


	try{

		$sql = "SELECT
		`staff`.name as personnalname,`staff`.emp_code as personnalempcode,`staff`.staffid as personnalstaffid,`msdesignation`.desname as personnaldesignationname
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid
		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		inner join mstuser on `staff_transaction`.staffid = `mstuser`.staffid
		WHERE  `staff`.staffid =". $this->loginData->staffid ." AND `mstuser`.RoleID = 17 ORDER BY `staff_transaction`.staffid ";

		$result = $this->db->query($sql)->result()[0];

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


/**
	 * Method getSupervisor() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getSupervisor($token)
{


	try{

		$sql = "SELECT
		`staff`.name as supervisorname,`staff`.emp_code as supervisorempcode,`staff`.staffid as supervisorstaffid,`msdesignation`.desname as supervisordesignationname,`lpooffice`.officename AS edofficename 
		FROM
		staff_transaction
		INNER JOIN(
		SELECT
		`staff_transaction`.staffid,
		MAX(
		`staff_transaction`.date_of_transfer
		) AS MaxDate
		FROM
		staff_transaction
		GROUP BY
		staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)

		inner join staff on `staff`.staffid = `staff_transaction`.staffid

		inner join msdesignation on `staff_transaction`.new_designation = `msdesignation`.desid
		INNER JOIN lpooffice ON `staff_transaction`.new_office_id =`lpooffice`.`officeid`

		WHERE  `staff`.staffid =".$token."  AND `staff`.designation   IN(4,16)  ORDER BY `staff_transaction`.staffid ";

		// echo $sql; die;
		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

/**
	 * Method getEDName() get staff Details .
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getEDName($edid)
{

	try{

		$sql = "SELECT staff.name FROM staff WHERE `staff`.staffid = ".$edid;

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

public function getpname($token)
{

	try{

		$sql = "SELECT staff.name,staff.staffid FROM staff WHERE `staff`.staffid = ".$token;
		// echo $sql; die;
		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


public function getEDid()
{

	try{

		$sql = "SELECT mstuser.staffid as edid FROM mstuser WHERE RoleID= 18";

		$result = $this->db->query($sql)->row();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


}