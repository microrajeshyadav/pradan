<?php 

/**
* Joining_of_DA Model
*/
class Joining_of_da_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}


  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



/**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


/**
   * Method getTeamlist() List of office name and id .
   * @access  public
   * @param Null
   * @return  Array
   */
    public function getTeamlist()
    {
        try{
           
            $sql = "SELECT officename, officeid FROM lpooffice WHERE closed='No' ORDER BY `officename`";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }

/**
   * Method getBatchlist() List of batch .
   * @access  public
   * @param Null
   * @return  Array
   */
    public function getBatchlist()
    {
        try{
           
            $sql = "SELECT id, batch FROM mstbatch WHERE IsDeleted='0' AND status='0'";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }


/**
   * Method staffList() List of Staff id .
   * @access  public
   * @param Null
   * @return  Array
   */

 public function staffList($id)
  {
    try{
    
           $sql = "SELECT
           staff.name,
           staff_transaction.staffid,
           staff_transaction.date_of_transfer,
           staff_transaction.new_office_id,
           staff_transaction.new_designation
           FROM
           staff_transaction
           INNER JOIN(
           SELECT
           staff_transaction.staffid,
           MAX(
           staff_transaction.date_of_transfer
           ) AS MaxDate
           FROM
           staff_transaction
           GROUP BY
           staffid
       ) AS TMax
       ON
       `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
       'Resign',
       'Termination',
       'Retirement',
       'Death'
       )

       INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid`

       where 1=1 and `staff`.`designation` = 4  

       ORDER BY `staff`.name ASC ";  

       $res = $this->db->query($sql)->result();
       return $res;
   }catch (Exception $e) {
    print_r($e->getMessage());die;
}

}

/**
   * Method getCandidate_BDFFormSubmit() get detail with BDF submit team wise .
   * @access  public
   * @param Null
   * @return  Array
   */
  public function getCandidate_BDFFormSubmit($teamid)
  {
    
    try{


    
      $sql = "SELECT DISTINCT
  `tbl_candidate_registration`.candidateid,
  `tbl_candidate_registration`.emailid,
 CONCAT(`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname) as name,
  `tbl_candidate_registration`.encryptedphotoname,
  `tbl_candidate_registration`.fatherfirstname,
  `tbl_candidate_registration`.fathermiddlename,
  `tbl_candidate_registration`.fatherlastname,
  `tbl_candidate_registration`.motherfirstname,
  `tbl_candidate_registration`.mothermiddlename,
  `tbl_candidate_registration`.motherlastname,
  `tbl_candidate_registration`.mobile,
  `tbl_candidate_registration`.emailid,
  `tbl_candidate_registration`.dateofbirth,
  `tbl_candidate_registration`.maritalstatus,
  `mstbatch`.batch,
  `staff`.name as fieldguidename,
  `msdc_details`.dc_name,
  `lpooffice`.officename,

  `mstcampus`.campusname,
  (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender,

  `mstbatch`.batch
FROM
  `tbl_candidate_registration`
  LEFT JOIN
  `mstcampus` ON `mstcampus`.campusid = `tbl_candidate_registration`.campusid
LEFT JOIN
  `mstbatch` ON `mstbatch`.id = `tbl_candidate_registration`.batchid
  LEFT JOIN
  `lpooffice` ON `lpooffice`.officeid = `tbl_candidate_registration`.teamid
   LEFT JOIN
  `staff` ON `staff`.staffid = `tbl_candidate_registration`.fgid
   LEFT JOIN
  `dc_team_mapping` ON `dc_team_mapping`.teamid = `tbl_candidate_registration`.teamid
  LEFT JOIN
  `msdc_details` ON `msdc_details`.dc_cd = `dc_team_mapping`.dc_cd
  LEFT JOIN 
  `staff` st  ON st.candidateid = `tbl_candidate_registration`.candidateid
  left join tbl_hr_intemation on staff.staffid=tbl_hr_intemation.staffid
  INNER JOIN
  `tbl_hrd_verification_document` ON `tbl_hrd_verification_document`.candidateid = `tbl_candidate_registration`.candidateid and  `tbl_hrd_verification_document`.`status` = 1
LEFT JOIN
  `tbl_prejoining_letter` ON `tbl_candidate_registration`.candidateid = `tbl_prejoining_letter`.candidateid
WHERE   
  `tbl_candidate_registration`.BDFFormStatus = 1 AND `tbl_candidate_registration`.`complete_inprocess` = 1 AND `tbl_candidate_registration`.`joinstatus` != 2 AND CASE WHEN `tbl_candidate_registration`.categoryid = 1 THEN `tbl_prejoining_letter`.status = 1 ELSE 1 = 1 END   ";  
             
            if (!empty($this->loginData->teamid)) {
                // $teamid = $this->loginData->teamid;
                 $sql .= " And `tbl_candidate_registration`.teamid = ".$this->loginData->teamid;
            }
             $sql .= " ORDER BY  `tbl_candidate_registration`.createdon  desc ";

             //AND `tbl_prejoining_letter`.status =1  

         // echo $sql;  die;
        $res = $this->db->query($sql)->result(); 

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

/**
   * Method getCandidateDetails() get candidate detail list .
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getCandidateDetails($candidateid)
  {
    
    try{

           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.username,`tbl_candidate_registration`.password   FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid        
             Where `tbl_candidate_registration`.BDFFormStatus = 1 AND `tbl_candidate_registration`.candidateid=".$candidateid;   
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


public function selectedCandidateDetails($candidateid)
  {

    
    try{

           $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,CONCAT(`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname) as name,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.username,`tbl_candidate_registration`.password,`tbl_candidate_registration`.teamid  FROM `tbl_candidate_registration` 
           left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid        
             Where `tbl_candidate_registration`.BDFFormStatus = 1 AND `tbl_candidate_registration`.candidateid=".$candidateid;   
             

        $res = $this->db->query($sql)->row();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
  /**
   * Method getGeneralFormStatus() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getGeneralFormStatus($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as generalformstatus FROM `tbl_general_nomination_and_authorisation_form` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid."";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }


/**
   * Method getJoiningReport() get Identity Name List.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getJoiningReport($candiid)
  {

    try{

      $sql = "SELECT `gnaf`.status as joinreportstatus FROM `tbl_joining_report` as `gnaf` WHERE `gnaf`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
     
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }

/**
   * Method getReportingTo() get reporting office name.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getReportingTo($id)
  {
    
    try{

           $sql = "SELECT 
  `staff_transaction`.staffid, 
   `staff`.name 
FROM 
  staff_transaction 
  INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
  INNER JOIN (
    SELECT 
      `staff_transaction`.staffid, 
      Max(
        `staff_transaction`.date_of_transfer
      ) as MaxDate 
    FROM 
      staff_transaction 
    GROUP BY 
      staffid
  ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
  AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
  AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
  ) 
  AND staff_transaction.`new_office_id` = '$id' 
  AND staff_transaction.`new_designation` IN(4) 
ORDER BY 
  `staff_transaction`.staffid ";   
        $res = $this->db->query($sql)->result()[0];

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


/**
   * Method getStaffId() get incrementel staff id.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffId()
  {
    
    try{
          

      $sql = "SELECT max(staffid) as staffid  FROM `staff`"; 
      $query = $this->db->query($sql);
      $numrows =  $query->num_rows();
      $result = $query->result()[0];
    
      if($numrows > 0 ){
       $newstaffid = $result->staffid + 1;
      }
     
     // echo $newstaffid; die;
      return $newstaffid;

      
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }






/**
   * Method getCountWorkExperience() no of work experience .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountWorkExperience($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_work_experience` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



/**
   * Method getWorkExperience() list of candidate work experience .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getWorkExperience($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_work_experience` as `tblwe` 
      WHERE `tblwe`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

/**
   * Method getCountTrainingExposure() no of training exposure .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getCountTrainingExposure($candiid)
  {

    try{

      $query = $this->db->query("SELECT * FROM `tbl_training_exposure` as `tblte` 
      WHERE `tblte`.`candidateid` = ".$candiid." ");
      $count = $query->num_rows();
      return $count;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


/**
   * Method getTrainingExposure() list of candidate Training Exposure .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTrainingExposure($candiid)
  {

    try{

      $sql = "SELECT * FROM `tbl_training_exposure` as `tblte` 
      WHERE `tblte`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


/**
   * Method getTrainingExposure() list of candidate Training Exposure .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getEducationDetails($candiid)
  {

    try{

      $sql = "SELECT metricschoolcollege,metricboarduniversity,metricpassingyear,metricplace,metricspecialisation,metricpercentage,hscschoolcollege,hscboarduniversity,hscpassingyear,hscplace,hscspecialisation,hscpercentage,hscstream,ugschoolcollege,ugboarduniversity,ugpassingyear,ugplace,ugspecialisation,ugpercentage,ugdegree,pgschoolcollege,pgboarduniversity,pgpassingyear,pgplace,pgspecialisation,pgpercentage,pgdegree,otherschoolcollege,otherboarduniversity,otherpassingyear,otherplace,otherspecialisation,otherpercentage,otherdegree,other_degree_specify FROM `tbl_candidate_registration` as `tblcr` WHERE `tblcr`.`candidateid` = ".$candiid." ";
      $res = $this->db->query($sql)->result();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function tc_data($teamid)
{
  try{
 $sql = "SELECT
  `staff_transaction`.staffid,
  `staff`.name,
  `staff`.emailid,
   `staff`.`designation`
FROM
  staff_transaction
INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
INNER JOIN(
  SELECT `staff_transaction`.staffid,
      MAX(
          `staff_transaction`.date_of_transfer
      ) AS MaxDate
  FROM
      staff_transaction
  GROUP BY
      staffid
) AS TMax
ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
      'Resign',
      'Termination',
      'Retirement',
      'Death'
  ) AND staff_transaction.`new_office_id` = '$teamid' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
ORDER BY
  `staff_transaction`.staffid  DESC  LIMIT 0,1
 ";
      $res = $this->db->query($sql)->row();
      return $res;

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


}



}