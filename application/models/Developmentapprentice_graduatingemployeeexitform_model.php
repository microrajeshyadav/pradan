<?php 

/**
* Master  Model
*/
class Developmentapprentice_graduatingemployeeexitform_model extends Ci_model
{	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}

	
/**
   * Method get_staff_sep_detail() get the staff detail in separation mpdule.
   * @access  public
   * @param Null
   * @return  Array
   */



	public function get_staff_sep_detail($token)
   {
      try
	  {

	  	if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Developmentapprentice_graduatingemployeeexitform/index');
				
			} else {

	    $sql="SELECT c.name,c.permanentpincode,c.contact,c.encrypted_signature,c.reportingto,c.emailid,a.date_of_transfer as seperatedate,  a.staffid, b.desname as sepdesig, a.new_designation as sepdesigid, c.emp_code, CONCAT(IFNULL(c.permanenthno,''), IFNULL(c.permanentstreet,''), IFNULL(c.permanentcity,''),IFNULL(c.permanentdistrict,'') , IFNULL(s.name,''),'-',IFNULL(c.permanentpincode,'')) as address, st.date_of_transfer as joiningdate, d.desname as joindesig, a.trans_flag, a.reason, a.createdon, lp.officename as newoffice, st.trans_status, dc.dc_cd, msdc.dc_name, IFNULL(sp.name,'') as superwiser  FROM staff_transaction as a
	       INNER JOIN `staff` as c on a.staffid = c.staffid 
	       INNER JOIN `staff` as sp on a.reportingto = sp.staffid
	       left join `state` as s ON s.id = c.permanentstateid
	       inner join staff_transaction as st
	        on st.staffid = c.staffid and st.trans_status = 'JOIN'
	       left JOIN `msdesignation` as b on a.new_designation = b.desid
	       left JOIN `msdesignation` as d on st.new_designation = d.desid
	       LEFT JOIN `lpooffice` as lp on a.new_office_id = lp.officeid 
	       LEFT JOIN `dc_team_mapping` as dc on c.new_office_id = dc.teamid 
           LEFT JOIN  `msdc_details` as msdc ON dc.dc_cd = msdc.dc_cd
	     WHERE a.id = $token";
	     
 		//echo $sql; die;

	     //$query=$this->db->get();
	     return  $this->db->query($sql)->row();
	 }

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

 public function get_staff_seperation_certificate($staffid)
   {
      try
	  {


	  	if (empty($staffid) && $staffid =='') {
	  		
	  		$this->session->set_flashdata('er_msg', 'Required parameter $staffid is either blank or empty.');
				redirect('/Developmentapprentice_graduatingemployeeexitform/index');
	  	}else{


	        $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation, g.desname as leavingdesignation, k.reason as leavingreason
	         FROM staff_transaction as a
	         INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid
	         INNER JOIN `staff` as c on a.staffid = c.staffid
	         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
	         INNER JOIN `msdesignation` as g on k.new_designation = g.desid
	         WHERE    a.trans_status = 'JOIN' and a.staffid =".$staffid;
       

	     return  $this->db->query($sql)->row();
	 }

		}catch (Exception $e) {
	      print_r($e->getMessage());die;
	    }

  }


    
}