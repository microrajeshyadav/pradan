<?php 

/**
* Master  Model
*/
class Campus_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Countclinicaldetails()
	{
		try{
			$this->db->select('count(Name) as clinical');
			$this->db->from('tblpatientregistrationdetails as prd ');
			$this->db->join('tblpatientclinicaltestdetails as pc', 'prd.PatientGUID = pc.PatientGUID');
			$this->db->where('prd.IsDeleted', 0);
			return $this->db->count_all_results();

		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
		
	}

	public function index()
	{

	}

    public function stateList()
    {
        try{

             $sql = "SELECT statecode,name FROM `state` WHERE `isdeleted`='0' ORDER BY `name` ";
             $res = $this->db->query($sql)->result();
            
             return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
       
    }

	public function getCampus()
	{
		try{
           
            $sql = "SELECT cam.`campusincharge`,`cam`.`campusincharge2`,cam.`campusname`,cam.`emailid`,cam.`city`,cam.`mobile`,cam.`telephone`,cam.`campusid`, cam.`fax`, cam.`address`, st.`name` FROM `mstcampus` as cam 
            INNER JOIN `state` as st on cam.`stateid`=st.`statecode` WHERE cam.`IsDeleted`='0'  ORDER BY createdon DESC";  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}



    public function getCampusDetails($token)
    {
        try{

            $sql = "SELECT * FROM `mstcampus` WHERE campusid=".$token."";
            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }

	

    /**
     * Method delete() delete detail.
     * @access  public
     * @param   $token
     * @return  string.
     */
    public function delete($token)
    {
        try {

                $deleteArray = array(
                        'IsDeleted'    => 1,
                      );

                    $this->db->where("campusid",$token);
                  return ($this->db->update('mstcampus', $deleteArray)) ? 1 : -1; //$this->db->last_query(); die;


            //$this->db->update(DISTRICT,$form); //echo $this->db->last_query(); die;
           // return ($this->db->update(state,$form)) ? 1 : -1; $this->db->last_query(); die;
        }
        catch (Exception $e) {
            print_r($e->getMessage());die;
        }
    }

	


}