<?php 

/**
* Dashboard Model
*/
class Writtenscore_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}



  /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedate($Date)
  {
    try{
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
  // print_r($date); die;
   if($pattern == "-" )
    $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}

  /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
    try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
  // echo substr($Date,$len,-4);
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
    $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
  if ($date=='//') {
    $date= NULL;
  }
  return $date;
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}



public function getElihibalCandidateListForWrittenExam($campusid , $campusintimationid){

  try{
      //echo $campusid; //die;

    $sql = "SELECT `tbl_gap_year`.DateDiff,`tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,
    `tbl_candidate_registration`.campustype,
    `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,`tbl_candidate_writtenscore`.writtenscore, `mstcategory`.categoryname, COALESCE(NULLIF(otherspecialisation,''), NULLIF(pgspecialisation,''), NULLIF(ugspecialisation,''), NULLIF(pgspecialisation,'') ) stream,
    `mstcampus`.campusname,
    `tbl_candidate_registration`.emailid,
    `tbl_candidate_registration`.mobile,
    `tbl_candidate_registration`.metricpercentage,
    `tbl_candidate_registration`.hscpercentage,
    `tbl_candidate_registration`.ugpercentage,
    `tbl_candidate_registration`.ugspecialisation,
    `tbl_candidate_registration`.pgspecialisation,
    `tbl_candidate_registration`.pgpercentage,
    `tbl_candidate_communication_address`.permanentcity,
    `state`.name as statename,
    (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender


    FROM `tbl_candidate_registration` 
    inner join `tbl_candidate_communication_address` on 
    `tbl_candidate_registration`.candidateid =
    `tbl_candidate_communication_address`.candidateid
     left join (select Candidateid, FLOOR(sum(datediff(`tbl_gap_year`.todate,`tbl_gap_year`.fromdate)/365)) AS DateDiff from `tbl_gap_year` group by Candidateid) tbl_gap_year on `tbl_gap_year`.candidateid=`tbl_candidate_registration`.candidateid
    left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
    LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
    inner JOIN `state` ON
    `tbl_candidate_communication_address`.permanentstateid = `state`.statecode
    left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid  Where CASE when trim(pgdegree) ='' or trim(pgdegree) is null then 1 else CASE WHEN ((trim(pgdegree) ='M.A.' OR trim(pgdegree) ='M.A' OR trim(pgdegree) ='MA' OR trim(pgdegree) ='Masters in Art') && pgpercentage >= 55) THEN '1' WHEN ((trim(pgdegree) !='M.A.' OR trim(pgdegree) !='M.A' OR trim(pgdegree) !='MA' OR trim(pgdegree) !='Masters in Art') && pgpercentage >= 60) THEN 1 ELSE 0 END END = 1 and CASE WHEN (ugpercentage >= 50 && ugpercentage < 60) THEN '1' WHEN ugpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (metricpercentage >= 50 && metricpercentage < 60) THEN '1' WHEN metricpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (hscpercentage >= 50 && hscpercentage < 60) THEN '1' WHEN hscpercentage >= 60 THEN 2 ELSE 0 END >= 5 
    AND TIMESTAMPDIFF(YEAR,dateofbirth, CONCAT(YEAR(CURDATE()),'-10-01')) <= CASE WHEN categoryid = 1 then 28 ELSE 30 end "; 

    if($campusid == 0 && $campusid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
     AND `tbl_candidate_registration`.`campusintimationid`= $campusintimationid
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed' 
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`complete_inprocess`= 0 
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid)){
    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND
    `tbl_candidate_registration`.`campusintimationid`= 'NULL'  
    `tbl_candidate_registration`.`campustype` ='on'";
  } else{
   $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND 
   `tbl_candidate_registration`.`campusintimationid`= $campusintimationid AND `tbl_candidate_registration`.`campustype` ='on' AND 
   `tbl_candidate_registration`.`inprocess`= 'closed'
   AND `tbl_candidate_registration`.`frstatus` ='accepted'
   AND  `tbl_candidate_registration`.`complete_inprocess`= 0 ";
 }
 $sql .= " ORDER BY `tbl_candidate_registration`.`candidateid` DESC ";

 $res = $this->db->query($sql)->result();
 //print_r($res); die; 
 return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


/*
    function getCampusEmail() is get Campus in-changer email id 
    result row;
    created by rajat
*/
    public function getCampusEmail($token)
    {

      try{

       $sql = "SELECT a.*,b.fromdate as fromdate,b.todate as todate FROM `mstcampus` as a 
       left join `tbl_campus_intimation` as b ON a.campusid = b.campusid 
       Where a.campusid = '".$token."'"; 

       $result = $this->db->query($sql)->result();

       return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }



   public function getSelectedCandidate($campusid  , $campusintimationid )
   {

    try{
     // echo $campusid; die;

       $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`mstcampus`.`campusname`,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,`tbl_candidate_writtenscore`.writtenscore, `mstcategory`.categoryname, (CASE WHEN tbl_candidate_registration.gender =1 THEN 'Male' WHEN tbl_candidate_registration.gender = 2 THEN 'Female' END)as gender, COALESCE(NULLIF(otherspecialisation,''), NULLIF(pgspecialisation,''), NULLIF(ugspecialisation,'') ) stream  FROM `tbl_candidate_registration` 
     left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
     left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid
     LEFT JOIN `mstcampus` ON `tbl_candidate_registration`.campusid = `mstcampus`.campusid
     inner join `tbl_campus_intimation` on 
     `tbl_campus_intimation`.id =
     `tbl_candidate_registration`.campusintimationid 

     Where CASE when trim(pgdegree) ='' or trim(pgdegree) is null then 1 else CASE WHEN ((trim(pgdegree) ='M.A.' OR trim(pgdegree) ='M.A' OR trim(pgdegree) ='MA' OR trim(pgdegree) ='Masters in Art') && pgpercentage >= 55) THEN '1' WHEN ((trim(pgdegree) !='M.A.' OR trim(pgdegree) !='M.A' OR trim(pgdegree) !='MA' OR trim(pgdegree) !='Masters in Art') && pgpercentage >= 60) THEN 1 ELSE 0 END END = 1 and CASE WHEN (ugpercentage >= 50 && ugpercentage < 60) THEN '1' WHEN ugpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (metricpercentage >= 50 && metricpercentage < 60) THEN '1' WHEN metricpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (hscpercentage >= 50 && hscpercentage < 60) THEN '1' WHEN hscpercentage >= 60 THEN 2 ELSE 0 END >= 5 
     AND TIMESTAMPDIFF(YEAR,dateofbirth, CONCAT(YEAR(CURDATE()),'-10-01')) <= CASE WHEN `tbl_candidate_registration`.categoryid = 1 then 28 ELSE 30 end "; 

     if($campusid == 0 && $campusid !='NULL'){
       $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid AND `tbl_candidate_registration`.`campusintimationid`= $campusintimationid
       AND `tbl_candidate_registration`.`campustype` = 'off' 
       AND `tbl_candidate_registration`.`inprocess` = 'closed' 
       AND `tbl_candidate_registration`.`yprststus` = 'yes' 
       AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
     }elseif(empty($campusid)){

      $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on'";

    } else{

      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND 
      `tbl_candidate_registration`.`campusintimationid`= $campusintimationid AND
      `tbl_candidate_registration`.`campustype` ='on' 
      AND `tbl_candidate_registration`.`inprocess`= 'closed'
      AND `tbl_candidate_registration`.`frstatus` ='accepted'
      AND `tbl_candidate_registration`.`complete_inprocess`= 0";
    }

    $sql .= " ORDER BY `tbl_candidate_registration`.`createdon` desc ";


    // echo $sql; die;

    $res = $this->db->query($sql)->result();
 //print_r($res); die; 
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



public function getSelectedCandidateWrittenScore($campusid )
{

  try{

    $sql = 'SELECT `tbl_candidate_registration`.`candidateid`, `tbl_candidate_registration`.`candidatefirstname`,`tbl_candidate_registration`.`candidatemiddlename`,`tbl_candidate_registration`.`candidatelastname`, `tbl_candidate_registration`.`emailid`, 
    `mstcategory`.categoryname, COALESCE(otherspecialisation,pgspecialisation,ugspecialisation) AS stream  FROM `tbl_candidate_registration`
    left join `mstcategory` on `tbl_candidate_registration`.categoryid =`mstcategory`.id
    left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
    
    Where CASE when trim(pgdegree) =" " or trim(pgdegree) is null then 1 else CASE WHEN ((trim(pgdegree) ="M.A." OR trim(pgdegree) ="M.A" OR trim(pgdegree) ="MA" OR trim(pgdegree) ="Masters in Art") && pgpercentage >= 55) THEN "1" WHEN ((trim(pgdegree) !="M.A." OR trim(pgdegree) !="M.A" OR trim(pgdegree) !="MA" OR trim(pgdegree) !="Masters in Art") && pgpercentage >= 60) THEN 1 ELSE 0 END END = 1 and CASE WHEN (ugpercentage >= 50 && ugpercentage < 60) THEN "1" WHEN ugpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (metricpercentage >= 50 && metricpercentage < 60) THEN "1" WHEN metricpercentage >= 60 THEN 2 ELSE 0 END + CASE WHEN (hscpercentage >= 50 && hscpercentage < 60) THEN "1" WHEN hscpercentage >= 60 THEN 2 ELSE 0 END >= 5 
    AND TIMESTAMPDIFF(YEAR,dateofbirth, CONCAT(YEAR(CURDATE()),"-10-01")) <= CASE WHEN `tbl_candidate_registration`.categoryid = 1 then 28 ELSE 30 end 
    And `tbl_candidate_writtenscore`.writtenscore > 0';
    
    if($campusid == 0 && $campusid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed' 
     AND `tbl_candidate_registration`.`complete_inprocess`= 0
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid)){

    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on'  AND `tbl_candidate_registration`.`complete_inprocess`= 0,";

  } else{

    $sql .= "
     AND `tbl_candidate_registration`.`campusid`= $campusid 
     AND `tbl_candidate_registration`.`campustype` = 'on' 
     AND `complete_inprocess`=0 
     AND `tbl_candidate_registration`.`inprocess` = 'closed'
     AND `tbl_candidate_registration`.`frstatus`  = 'accepted'
     AND `tbl_candidate_registration`.`wstatus` Is NULL";
  }

  $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid ASC';

//echo $sql; die;

  $res = $this->db->query($sql)->result();
  return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}




public function getUpdateStatus($candidateid)
{
  try{
    $this->db->trans_start();

    $updateArr = array(
      'inprocess'  => 'closed',
    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_candidate_registration', $updateArr);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){
     return -1;  

   }else{
    return 1;
  }

}catch(Exception $e){
  print_r($e->getMessage());die();
}
}



public function getRecruiters()
{
  try{

    $sql = "SELECT staffid,name FROM `staff`";  
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


public function getCampus($id)
{
  try{

//print_r($this->loginData);

     //$sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
   // ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0"; 

    if (!empty($id) AND $this->loginData->RoleID==30) {

     $sql = "SELECT * FROM `mstcampus` as a 
     Inner join mapping_campus_recruiters as b ON a.`campusid` = b.`campusid` 
     inner JOIN `tbl_campus_intimation` as c ON a.`campusid` = c.`campusid`
     WHERE a.`IsDeleted`=0 AND c.`mailstatus`= 1 AND c.`campus_status`=0 AND  b.`recruiterid`= ". $id; 

   } else{

    $sql = "SELECT a.`campusid`,a.`campusname`, b.`id`,b.`fromdate`,b.`todate` FROM `mstcampus` as a inner JOIN `tbl_campus_intimation` as b ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND b.`mailstatus`= 1 AND b.`campus_status`=0 ";

  }

    //echo $sql; die;

  $res = $this->db->query($sql)->result();

  return $res;

}catch(Exception $e){
  print_r($e->getMessage());die();
}
}


public function getRecruitersCampus()
{
  try{

    $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, cam.`campusincharge`,cam.`campusname`,cam.`city`,st1.`name` as name1, st2.`name` as name2, st3.`name` as name3 from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.IsDeleted = 0"; 

    $res = $this->db->query($sql)->result();
    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}




public function getSingelRecruitersCampus($token)
{
  try{

    $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, rec.`recruitersname1`, rec.`recruitersname2`,rec.`recruitersname3`, cam.`campusincharge`,cam.`campusid`,cam.`city` from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.recruiterid='".$token."' and  rec.IsDeleted = 0"; 

    $res = $this->db->query($sql)->result();
    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



public  function get_quick_list($campusid)  
{  
  $this->db->select('*');    
  $this->db->from('tbl_candidate_registration');  
  $this->db->where("TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 AND campusid LIKE '$distct'");
  $query=$this->db->get()->result_array(); 
  return $query;
}



public function getCampusCutOffMarks($campusid, $id)
{
  try{

   $sql = "SELECT * FROM tbl_campus_intimation as a Where `a`.`campusid`='".$campusid."' AND `a`.`id`='".$id."'   AND a.campus_status =0 "; 
   $res = $this->db->query($sql)->row();
   return $res;

 }catch(Exception $e){
  print_r($e->getMessage());die();
}
}


}