<?php 
/**
* State Model
*/
class Permissions_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    
  }

  public function update_permissions()
  {
    try{
    $RoleID = $this->input->post('RoleID');
    $selected = $this->input->post('selected');
    // empty the role_permissions table 
    $this->db->where('RoleID', $RoleID);
    $this->db->delete('role_permissions');
    foreach ($selected as $controller => $row) 
    {
      foreach ($row as $action => $value) 
      {
        $insertArr = array(
          'RoleID'    =>  $RoleID,
          'Controller'  =>  $controller, 
          'Action'    =>  $action,
          );
        $this->db->insert('role_permissions', $insertArr);
        $this->session->set_flashdata('tr_msg', 'Successfully added Controller');
        
      }
    }
    }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
  }

}