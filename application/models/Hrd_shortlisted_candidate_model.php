<?php 

/**
* Hrd_shortlisted_candidate  Model
*/
class Hrd_shortlisted_candidate_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}


public function getElihibalCandidateListForWrittenExam($campusid){
                                                         
try{
     // echo $campusid; die;

      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,`tbl_candidate_writtenscore`.writtenscore FROM `tbl_candidate_registration` 
      left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
       inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid =`tbl_accept_campusinchargetocandidate`.candidateid ";
   
     $sql .=  " Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` AND  `tbl_candidate_registration`.`metricpercentage` >= 60 AND ((`tbl_candidate_registration`.`hscpercentage` >= 60 And `tbl_candidate_registration`.`ugpercentage` >= 60) OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) "; 
      $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND 
       `tbl_accept_campusinchargetocandidate`.freezed = 0 AND  `tbl_candidate_registration`.`wstatus` Is NULL ";

    $sql .= " ORDER BY `tbl_candidate_registration`.`candidateid` ASC ";


     // echo $sql; die;

    $res = $this->db->query($sql)->result();
 //print_r($res); die; 
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

  public function getSelectedCandidate($campusid)
  {

    try{
     // echo $campusid; die;

      $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,`tbl_candidate_writtenscore`.writtenscore FROM `tbl_candidate_registration` 
      left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
       inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid =`tbl_accept_campusinchargetocandidate`.candidateid 
      Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` AND  `tbl_candidate_registration`.`metricpercentage` >= 60 AND ((`tbl_candidate_registration`.`hscpercentage` >= 60 And `tbl_candidate_registration`.`ugpercentage` >= 60) OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) "; 
    $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND 
     `tbl_accept_campusinchargetocandidate`.freezed = 0 AND (`tbl_candidate_registration`.`wstatus`Is NULL OR `tbl_candidate_registration`.`wstatus`= 0) ";

   

    $sql .= " ORDER BY `tbl_candidate_registration`.`candidateid` ASC ";


    //echo $sql; die;

    $res = $this->db->query($sql)->result();
 //print_r($res); die; 
    return $res;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



public function getSelectedCandidateWrittenScore($campusid)
{

  try{

    $sql = 'SELECT `tbl_candidate_registration`.`candidateid`, `tbl_candidate_registration`.`candidatefirstname`,`tbl_candidate_registration`.`candidatemiddlename`,`tbl_candidate_registration`.`candidatelastname`, `tbl_candidate_registration`.`emailid` FROM `tbl_candidate_registration`
    left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
    inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid =`tbl_accept_campusinchargetocandidate`.candidateid 
    Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`hscpercentage`+4) <= `ugpassingyear` AND  metricpercentage >= 60 AND ((hscpercentage >= 60 And ugpercentage >= 60) OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60))  And `tbl_candidate_writtenscore`.writtenscore > 0';
    
    
    $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='closed' AND
     `tbl_accept_campusinchargetocandidate`.freezed = 0
    AND `complete_inprocess`=0 
    AND  `tbl_candidate_registration`.`wstatus` Is NULL";
 

  $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid ASC';

//echo $sql; die;

  $res = $this->db->query($sql)->result();
  return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}

}


public function getShortlistcandidate($campusid=null)
{

  try{

    $sql = 'SELECT `tbl_candidate_registration`.`candidateid`';
     if($campusid !=0 && $campusid !='NULL'){
   $sql.=',`tbl_accept_campusinchargetocandidate`.status, `tbl_accept_campusinchargetocandidate`.hrstatus '; 
}
    $sql.='FROM `tbl_candidate_registration`';

    if($campusid !=0 && $campusid !='NULL'){
   $sql.='inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid = `tbl_accept_campusinchargetocandidate`.candidateid '; 
 }

    $sql.=' Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`hscpercentage`+4) <= `ugpassingyear` AND  metricpercentage >= 60 AND ((hscpercentage >= 60 And ugpercentage >= 60) OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60))';

    if($campusid == 0 && $campusid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed' 
     AND `tbl_candidate_registration`.`complete_inprocess`= 0
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`wstatus` Is NULL
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid)){

    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on'  AND `tbl_candidate_registration`.`complete_inprocess`= 0,";

  } else{

   $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND
     `tbl_accept_campusinchargetocandidate`.freezed = 0
      AND `complete_inprocess`=0 
      AND `tbl_candidate_registration`.`wstatus` Is NULL";
  }

  $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid ASC';

//echo $sql; die;

  $res = $this->db->query($sql)->result();
  return $res;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}

}



public function getShortlistcandidatestatus($campusid =null)
{
  try{

     $sql = 'SELECT `tbl_candidate_registration`.`candidateid`';
     if($campusid !=0 && $campusid !='NULL'){
   $sql.=',`tbl_accept_campusinchargetocandidate`.status, `tbl_accept_campusinchargetocandidate`.hrstatus '; 
}
    $sql.='FROM `tbl_candidate_registration`';

    if($campusid !=0 && $campusid !='NULL'){
   $sql.='inner join `tbl_accept_campusinchargetocandidate` on `tbl_candidate_registration`.candidateid = `tbl_accept_campusinchargetocandidate`.candidateid '; 
 }

    $sql.=' Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`hscpercentage`+4) <= `ugpassingyear` AND  metricpercentage >= 60 AND ((hscpercentage >= 60 And ugpercentage >= 60) OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60))';

    if($campusid == 0 && $campusid !='NULL'){
     $sql .= "  AND `tbl_candidate_registration`.`campusid` = $campusid 
     AND `tbl_candidate_registration`.`campustype` = 'off' 
     AND `tbl_candidate_registration`.`inprocess` = 'closed' 
     AND `tbl_candidate_registration`.`complete_inprocess`= 0
     AND `tbl_candidate_registration`.`yprststus` = 'yes'
     AND `tbl_candidate_registration`.`wstatus` Is NULL
     AND `tbl_candidate_registration`.`confirm_attend` = 1 ";
   }elseif(empty($campusid)){

    $sql .= " AND `tbl_candidate_registration`.`campusid`= 'NULL' AND `tbl_candidate_registration`.`campustype` ='on'  AND `tbl_candidate_registration`.`complete_inprocess`= 0,";

  } else{

   $sql .= " AND `tbl_candidate_registration`.`campusid`= $campusid AND `tbl_candidate_registration`.`campustype` ='on' AND `tbl_candidate_registration`.`inprocess` ='open' AND
     `tbl_accept_campusinchargetocandidate`.freezed = 0
      AND `complete_inprocess`=0 
      AND `tbl_candidate_registration`.`wstatus` Is NULL";
  }

  $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid ASC';


    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

public function getgenratepdf($token)
{
    try{

         $sql = " SELECT name,flag FROM `genratepdflist` WHERE flag = 0 ";
         $res = $this->db->query($sql)->result()[0];
         return $res;

      }catch(Exception $e){
        print_r($e->getMessage());die();
      }
}

public function getcampusInchargeEmailid($token)
{
  try{

       $sql = " SELECT emailid,campusincharge FROM `mstcampus` WHERE campusid =".$token;
       $res = $this->db->query($sql)->result()[0];
       return $res;

      }catch(Exception $e){
        print_r($e->getMessage());die();
      }
}



public function getUpdateStatus($candidateid)
{
  try{
    $this->db->trans_start();

    $updateArr = array(
      'inprocess'  => 'closed',
    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_candidate_registration', $updateArr);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){
     return -1;  

   }else{
    return 1;
  }

}catch(Exception $e){
  print_r($e->getMessage());die();
}
}



public function getRecruiters()
{
  try{

    $sql = "SELECT staffid,name FROM `staff`";  
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


public function getCampus($id=null)
{
  try{

//print_r($this->loginData);

     //$sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
   // ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0"; 

    if (!empty($id) AND $this->loginData->RoleID==30) {
      
       $sql = "SELECT * FROM `mstcampus` as a Inner join mapping_campus_recruiters as b 
    ON a.`campusid` = b.`campusid` WHERE a.`IsDeleted`=0 AND  b.`recruiterid`= ". $id; 
       
     } else{
      $sql = "SELECT * FROM `mstcampus` WHERE IsDeleted=0";
     }

    //echo $sql; die;

    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}


public function getRecruitersCampus()
{
  try{

    $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, cam.`campusincharge`,cam.`campusname`,cam.`city`,st1.`name` as name1, st2.`name` as name2, st3.`name` as name3 from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.IsDeleted = 0"; 

    
    $res = $this->db->query($sql)->result();
    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}




public function getSingelRecruitersCampus($token)
{
  try{

    $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, rec.`recruitersname1`, rec.`recruitersname2`,rec.`recruitersname3`, cam.`campusincharge`,cam.`campusid`,cam.`city` from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.recruiterid='".$token."' and  rec.IsDeleted = 0"; 

    $res = $this->db->query($sql)->result();
    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



public  function get_quick_list($campusid)  
{  
  try{
  $this->db->select('*');    
  $this->db->from('tbl_candidate_registration');  
  $this->db->where("TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 AND campusid LIKE '$distct'");
  $query=$this->db->get()->result_array(); 
  return $query;
  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



}