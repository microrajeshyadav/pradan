<?php 

/**
* Dashboard Model
*/
class Hr_report_model extends Ci_model
{
  
  function __construct()
  {
    parent::__construct();
  }

  
    /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


    public function changedate($Date)
    {
      try{
    //echo strlen($Date);
      $len = (strlen($Date)-5); 
      if(substr($Date,$len,-4)=="/")
       $pattern = "/";
     else
       $pattern = "-";

     $date = explode($pattern,$Date);
      // print_r($date); die;
     if($pattern == "/" )
      @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
    else
      @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
     // echo $date; die;
    if ($date=='//') {
      $date = NULL;
    }
    return $date;

    }catch(Exception $e){
    print_r($e->getMessage());die();
  }
  }




  public function index()
  {

  }

    /**
   * Method getbatch() Find the batch listing.
   * @access  public
   * @param Null
   * @return  Array
   */


    public function getbatch()
    {

      try{

       $sql = "SELECT *  FROM `mstbatch` WHERE `status` !=1 ORDER BY id asc"; 

       $result = $this->db->query($sql)->result();

       return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }
   


/**
   * Method getfinancialyear() get current financial year.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getfinancialyear()
{

  try{

    $sql = "SELECT * FROM `mstfinancialyear` WHERE `isdeleted`='0'";

    $result = $this->db->query($sql)->result();

    return $result;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}

public function getdashboardstat()
{

  try{

    $sql = "select count(*) totaltaff, sum(case when gender = 1 then 1 else 0 end) malecount, sum(case when gender = 2 then 1 else 0 end) femalecount, case when month(dateofleaving) =  month(NOW()) and  year(dateofleaving) =  year(NOW()) then 1 else 0 end totalsep  from staff inner join msdesignation des on des.desid = staff.designation inner join lpooffice off on off.officeid = staff.new_office_id where status = 1";

    $result = $this->db->query($sql)->row();

    return $result;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}


    /**
   * Method getCountRegisteredUser() get count register user.
   * @access  public
   * @param Null
   * @return  Array
   */

    public function getCountRegisteredUser($token)
    {

      try{

       $query = $this->db->get('tbl_candidate_registration');
       if($query->num_rows())
       {
        return $query->num_rows();
      }else{
        return 0;
      }

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method getpolicylist() get policy list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getpolicylist()
{

  try{

   $query = $this->db->get('policy');
   if($query->num_rows())
   {
    return $query->result();
  }else{
    return 0;
  }

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


 /**
   * Method getState_Institute_Offered() get count state wise institute offered candidate.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getState_Institute_Offered($batchid=null)
 {

  try{

    $sql = "SELECT 
    `b`.campusname as statewisecampusename,
    `b`.campusid,
    `c`.name as statename,
    `c`.statecode, 
    `d`.`fromdate`,
    `d`.`todate`,
    count(`a`.candidateid) as offeredcandidate";

    if ($batchid != ' ') {

      $sql .= ", `a`.batchid as batchid ";

    }

    $sql .= " FROM `tbl_candidate_registration` as a 
    LEFT JOIN `mstcampus` as b on `a`.campusid = `b`.campusid 
    LEFT JOIN `state` as c on `b`.stateid = `c`.statecode 
    LEFT JOIN `tbl_campus_intimation` as d on `a`.campusintimationid = `d`.id 
    WHERE a.`complete_inprocess` = 1 ";

    if ($batchid != ' ') {
      $sql .= " AND a.`batchid` = ".$batchid;
    }

    $sql.= " GROUP BY `b`.campusname,`b`.campusid,`d`.`fromdate`,
    `d`.`todate`, `c`.name,`c`.statecode";

    if ($batchid != ' ') {
      $sql.= ", `a`.batchid ";
    }   

    $query = $this->db->query($sql);
     //$numrow = $query->num_rows(); 

    // echo $sql;

    $result =$query->result();

    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

 /**
   * Method getState_Institute_Offered_Accepted() get count state wise institute offered accepted candidate.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function getState_Institute_Offered_Accepted($batchid,$campusid)
 {

  try{

    $sql = "SELECT count(`a`.candidateid) as offeredacceptrdcandidate FROM 
    `tbl_candidate_registration` as a 
    LEFT JOIN `mstcampus` as b on `a`.campusid = `b`.campusid 
    LEFT JOIN `state` as c on `b`.stateid= `c`.statecode 
    WHERE  `a`.campusid = $campusid 
    AND `a`.`BDFFormStatus` = 1 ";
    if ($batchid !='') {
      $sql .=" AND `a`.`batchid` = $batchid";
    }
    $sql .= " ORDER BY `a`.`candidateid` ASC"; 

    $query = $this->db->query($sql);
    $result =$query->result()[0];
    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getState_Institute_Offered_Accepted_List() get list state wise institute offered accepted candidate list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getState_Institute_Offered_Accepted_List($batchid, $campusid)
{

  try{

   $sql = "SELECT `a`.candidatefirstname,`a`.candidatemiddlename,`a`.candidatelastname,`a`.emailid,`b`.writtenscore, `c`.rsscore,`c`.ssscore,`c`.gdscore FROM 
   `tbl_candidate_registration` as a 
   Inner Join `tbl_candidate_writtenscore` as b 
   on `a`.candidateid = `b`.candidateid
   Inner JOIN `tbl_candidate_gdscore` as c ON 
   `a`.candidateid = `c`.candidateid
   WHERE `a`.`BDFFormStatus` = 1 ";

   if ($campusid == 0) {
     $sql .= " AND a.`campustype` = 'off' AND `a`.`campusid` = $campusid "; 

   }else{

     $sql .= " AND `a`.`campusid` = $campusid "; 
   }

   if ($batchid !=0) {
    $sql .= " AND  `a`.`batchid` = $batchid ";
  }    

  $sql .= "ORDER BY `a`.`candidateid` ASC"; 

  $query = $this->db->query($sql);
     //echo $numrow = $query->num_rows(); 


  $result =$query->result();
  return $result;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}

/**
   * Method getJoin_Candidate_List() get join candidates list.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getJoin_Candidate_List($batchid=null,$teamid=null)
{

  try{

   $sql = "SELECT `a`.candidatefirstname,`a`.candidatemiddlename,`a`.candidatelastname,`a`.emailid,`b`.emp_code,`b`.createdon FROM 
   `tbl_candidate_registration` as a 
   Inner Join `tbl_da_personal_info` as b 
   on `a`.candidateid = `b`.candidateid
   WHERE 1=1 ";

   if ($batchid !=0 ) {
    $sql .= " AND a.`batchid`= ".$batchid;
  }

  if (!empty($teamid)) {
    $sql .= " AND a.`teamid`= ".$teamid;
  }

       $sql .= " ORDER BY `a`.`candidateid` ASC";  //die;

       $query = $this->db->query($sql);
     //echo $numrow = $query->num_rows(); 


       $result =$query->result();
       return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }


/**
   * Method getDC() get list of Development Cluster.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getDC()
{

  try{

   $sql = "SELECT `a`.dc_name, `a`.dc_cd FROM `msdc_details` as a
   LEFT Join `dc_team_mapping` as b on `a`.dc_cd = `b`.dc_cd          
   group by `a`.dc_name, `a`.dc_cd";  
   $query = $this->db->query($sql);
     //echo $numrow = $query->num_rows(); 


   $result =$query->result();
   return $result;

 }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getTeam_CountCandidate() get team wise count candidates .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getTeam_CountCandidate($dcid,$batchid)
{

  try{

    $sql = "SELECT 
    COUNT(a.`candidateid`) as countcandiadte,
    a.`teamid`, 
    b.`officename`
    FROM 
    `tbl_candidate_registration` as a 
    LEFT Join `lpooffice` as b on b.officeid = a.teamid 
    LEFT Join `dc_team_mapping` as c on c.teamid = a.teamid 
    WHERE  `joinstatus` = 1 AND c.dc_cd = ". $dcid;
    if ($batchid == 0 ) {
     $sql .= " AND `batchid` >= 0  "; 
   }else{
     $sql .= " AND `batchid`= ".$batchid;
   }
      $sql .= " group by a.teamid";  //die;

      $query = $this->db->query($sql);
     //echo $numrow = $query->num_rows(); 


      $result =$query->result();
      return $result;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



// Candidate details for reminder of documents

 public function candidate_details()
 {

  try{
    $current_date = date('Y-m-d');

    $sql = "SELECT
    DATEDIFF(CURRENT_DATE, `a`.ugmigration_certificate_date) AS DAYS,
    concat(a.candidatefirstname,' ',a.candidatemiddlename,' ',a.candidatelastname ) FullName,
    `a`.candidateid,
    `a`.emailid,
    `a`.encryptedphotoname,
    `mstbatch`.batch,
    `a`.encryptmetriccertificate,
    `a`.encrypthsccertificate,
    `a`.campustype,
    `a`.ugdegree,
    `a`.pgdegree,
    `a`.encryptugcertificate,
    `a`.encryptpgcertificate,
    `a`.mobile,
    `a`.encryptothercertificate,
    `a`.ugmigration_encrypted_certificate_upload,
    `a`.pgmigration_encrypted_certificate_upload,
    `a`.encryptofferlettername,
    `a`.ugmigration_certificate_date,
    `a`.pgmigration_certificate_date,
    `a`.pgmigration_encrypted_certificate_upload,
    tbl_gap_year.encrypteddocumentsname AS gapyeardocument,
    `tbl_hrd_verification_document`.status,
    `tbl_hrd_verification_document`.comment,
    (case WHEN a.pgmigration_certificate_date >=1 THEN 'PG' WHEN a.ugmigration_certificate_date >=1 THEN 'UG' END) as degree,
     (case WHEN a.pgmigration_certificate_date >=1 THEN a.pgmigration_certificate_date WHEN a.ugmigration_certificate_date >=1 THEN a.ugmigration_certificate_date END) as dta,
    (case WHEN a.gender = 1 THEN 'Male' WHEN a.gender = 2 THEN 'Female' END) as gender 
    FROM
    `tbl_candidate_registration` a
    LEFT JOIN `mstbatch` ON `mstbatch`.id = `a`.batchid
    LEFT JOIN `tbl_hrd_verification_document` ON `a`.candidateid = `tbl_hrd_verification_document`.candidateid
    LEFT JOIN tbl_gap_year ON a.candidateid = tbl_gap_year.candidateid
    WHERE
    1 = 1 AND (DATEDIFF(CURRENT_DATE, `a`.ugmigration_certificate_date) <=20 or DATEDIFF(CURRENT_DATE, `a`.ugdoc_certificate_date) <=20) 
    GROUP BY  concat(a.candidatefirstname,' ',a.candidatemiddlename,' ',a.candidatelastname ),a.emailid,a.candidateid";  
    // echo $sql;
    $query = $this->db->query($sql);
    
    $result =$query->result();
    return $result;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}




}