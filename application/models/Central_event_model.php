<?php 

/**
* Central Event Model
*/
class Central_event_model extends CI_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
  	try{
//echo strlen($Date);
    $len = (strlen($Date)-5); 
    if(substr($Date,$len,-4)=="/")
     $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); 
   if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
  return $date;

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}
/**
	 * Method getResoucePerson() get all Resouce Person List.
	 * @access	public
	 * @param	
	 * @return	array
	 */
public function getResoucePerson()
{


	try{

		$sql = "SELECT * FROM  `mst_resource_person` WHERE `isdeleted`=0";
		$result = $this->db->query($sql)->result();
		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}




	/**
	 * Method getStaffDetails() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 */
	public function getStaffDetails($id)
	{

		
		try{

			$sql = "SELECT `staff`.name,`staff`.emailid
			FROM
			staff_transaction
			INNER JOIN(
			SELECT
			`staff_transaction`.staffid,
			MAX(
			`staff_transaction`.date_of_transfer
			) AS MaxDate
			FROM
			staff_transaction
			GROUP BY
			staffid
		) AS TMax
		ON
		`staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
		'Resign',
		'Termination',
		'Retirement',
		'Death'
		)
		inner join staff on `staff`.staffid = `staff_transaction`.staffid

		WHERE `staff`.staffid = $id
		ORDER BY
		`staff_transaction`.staffid ";

		$result = $this->db->query($sql)->result();

		return $result;



	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



/**
	 * Method getResoucePerson() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
public function getCentralEvent()
{


	try{

		$sql = " SELECT `tbl_central_event`.id, `tbl_central_event`.name, `tbl_central_event`.from_date, `tbl_central_event`.to_date, `tbl_central_event`.place,`mstevents`.name  FROM `tbl_central_event` inner join `mstevents`  on `tbl_central_event`.name = `mstevents`.id  

			WHERE `tbl_central_event`.isdeleted =0 ORDER BY `tbl_central_event`.id DESC";

		$result = $this->db->query($sql)->result();

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



	/**
	 * Method getEvent() get all Event List .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getEvent()
	{

		
		try{

			$sql = "SELECT * FROM `mstevents` Where `mstevents`.id NOT IN 
			(select name From tbl_central_event) AND `mstevents`.`isdeleted`= 0 ORDER BY `mstevents`.id DESC";

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}




	/**
	 * Method getEventEdit() get all Event List .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getEventEdit()
	{

		
		try{

			$sql = "SELECT * FROM `mstevents` Where `mstevents`.`isdeleted`= 0 ORDER BY `mstevents`.id DESC";

			$result = $this->db->query($sql)->result();

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


   /**
	 * Method getSingleCentralEvent() get Single Central Event .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
   public function getSingleCentralEvent($token)
   {

   	try{

   		$sql = "SELECT `tbl_central_event`.id,`tbl_central_event`.name as eventid,`tbl_central_event`.from_date,`tbl_central_event`.to_date,`tbl_central_event`.place, `mstevents`.name FROM `tbl_central_event` 
   		    inner join `mstevents` on `tbl_central_event`.name = `mstevents`.id  WHERE `tbl_central_event`.`id`=$token"; 

   		$result = $this->db->query($sql)->result();

   		return $result;

   	}catch (Exception $e) {
   		print_r($e->getMessage());die;
   	}

   }


	/**
	 * Method getSingleResoucePerson() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getSingleResoucePerson($token)
	{
		
		try{

			$sql = " SELECT `resouce_person_id` FROM `tbl_central_event_transaction` WHERE `central_event_id`=$token ";

			$result = $this->db->query($sql)->result();
			//print_r($result); die;

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


/**
	 * Method CurrentFinancialYear() get Current Financial Year  .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */


public function CurrentFinancialYear()
{

	try{

		$today = @date("Y-m-d");
		$curyear=@date("Y");
		$nextyear=@date("Y")+1;
		$curfin=$curyear.'-'.'03'.'-'.'31';
		$nextfin=$nextyear.'-'.'04'.'-'.'01'; 
		if($today > $curfin && $today < $nextfin) 
			$rowyear= $curyear.'-'.$nextyear; 
		if($today <= $curfin)
			$rowyear=($curyear-1).'-'.$curyear;
		return $rowyear;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


	/**
	 * Method getDevelopmentShip() get all staff .
	 * @access	public
	 * @param	
	 * @return	array
	 *
	 */
	public function getDevelopmentShip($financialyear)
	{
		
		try{

			 $sql = "SELECT
			`tblcr`.candidateid,
			`tblcr`.emailid,
			`tblcr`.candidatefirstname,
			`tblcr`.candidatemiddlename,
			`tblcr`.candidatelastname
			FROM `mstbatch` AS `mstb`
			INNER  JOIN  `tbl_candidate_registration` AS `tblcr`
			ON `tblcr`.batchid = `mstb`.id
			Where `mstb`.status =0 AND `tblcr`.joinstatus = 1"; 

			$result = $this->db->query($sql)->result();
			//print_r($result); die;

			return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}



}