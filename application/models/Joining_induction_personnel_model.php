<?php 

/**
* Dashboard Model
*/
class Joining_induction_personnel_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	

	public function index()
	{

	}

	  public function Approve($token)
	  {
	  	try
	  	{
            // $sql = "SELECT b.staffid,b.candidateid,b.name,b.dob,b.emailid,a.flag,a.type,c.process_type, tbl_graduitynomination.graduity_id FROM `tbl_workflowdetail` as a inner join staff as b on a.staffid=b.staffid INNER join mst_workflow_process as c ON a.type = c.id left join tbl_graduitynomination on tbl_graduitynomination.staff_id=a.staffid WHERE a.type IN (8,9,11,12,13,14,15,16,17,18,19,23,24,25) and a.flag= 2 and a.receiver='$token' "; 
            $sql="SELECT
    *
FROM
    (SELECT
    cnt.cnt,
    b.staffid,
    b.candidateid,
    b.name,
    b.dob,
    b.doj,
   
    b.emailid,
    a.flag,
     CASE WHEN cnt.cnt >= 11 THEN a.type ELSE 0
END TYPE,
CASE WHEN cnt.cnt >= 11 THEN replace(replace(c.process_type,'_',' '),'Joining induction','') ELSE CONCAT(
    cnt,
    ' joining induction forms submitted out of 11'
)
END process_type,

     tbl_graduitynomination.graduity_id
FROM
    `tbl_workflowdetail` AS a
INNER JOIN staff AS b
ON
    a.staffid = b.staffid
INNER JOIN mst_workflow_process AS c
ON
    a.type = c.id
LEFT JOIN tbl_graduitynomination ON tbl_graduitynomination.staff_id = a.staffid
LEFT JOIN(
    SELECT
        staffid,
        COUNT(TYPE) cnt
    FROM
        tbl_workflowdetail
    WHERE
        receiver = '$token' AND type IN(
            8,
            9,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            23,
            24,
            25
        ) AND flag = 2
   
GROUP BY
    staffid
) cnt
ON
    a.staffid = cnt.staffid 
WHERE
    a.type IN(
        8,
        9,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        23,
        24,
        25
    ) AND a.flag = 2 AND a.receiver = '$token' ) a
GROUP BY
    cnt,
    staffid,
    candidateid,
    NAME,
    dob,
    emailid,
    flag,
    process_type,
    graduity_id";
            
           

             $result = $this->db->query($sql)->result();

           return $result;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }

    }

	 
}