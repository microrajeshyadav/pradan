<?php 
class Closedcampus extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model('Closedcampus_model', 'model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
		
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token)
	{
		try{
		
		 // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

          
	
		$content['campusinchargedetails'] = $this->model->getCampusInchargeDetail($token);
		$content['title'] = 'index';
		 //echo __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__; die;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


public function add($token)
	{
		try{
		 // start permission 
		
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

          
             $RequestMethod = $this->input->server('REQUEST_METHOD');			
			if($RequestMethod == 'POST'){

				$this->db->trans_start();

					$updateArr1 = array(
						'campus_status'  => $this->input->post('status'),
					  );
					$this->db->where('id',$token);
					$this->db->update('tbl_campus_intimation', $updateArr1);
                 
					$this->db->trans_complete();

					if ($this->db->trans_status() === true){
						$this->session->set_flashdata('tr_msg', 'Successfully closed campus !!!');
					}else{
						$this->session->set_flashdata('er_msg', 'Error closed campus !!!');
					}

					redirect('Closedcampus/index');
		
			}

			$content['campusid'] =  $token;
	
		$content['campusinchargedetails'] = $this->model->getCampusInchargeDetail($token);
		$content['campusdetails'] = $this->model->getCampus();
		$content['title'] = 'add';
		 //echo __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__; die;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	

	function delete($token)
	{
		try{
		$this->Common_Model->delete_row('mstcampus','campusid', $token); 
		$this->session->set_flashdata('tr_msg' ,"Campus Deleted Successfully");
		redirect('/Campus/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


}