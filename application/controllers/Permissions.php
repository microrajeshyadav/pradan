<?php 
/**
* Permissions class for managing permissions
*/  
class Permissions extends Ci_controller
{
  public $loginData;
  function __construct()
  {
    parent::__construct();
    $this->load->model("Permissions_model");
    $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {

    // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 

    $sql = "Select * from sysaccesslevel WHERE Acclevel_Cd NOT IN('25','17','15','5','6')";
    $content['role_list'] = $this->db->query($sql)->result();
    //$content['role_list'] = $this->db->get('sysaccesslevel')->result(); 

    $RoleID = $this->input->post('RoleID');




    $flag = $this->input->post('flag');
    if ($flag == "update") {
      //echo "<pre>";
     // print_r($this->input->post()); die;

      $this->Permissions_model->update_permissions();


    }

    if ($RoleID != NULL) {
      $this->db->where('RoleID', $RoleID);
      
      $content['permissions_list'] = $this->db->get('role_permissions')->result();
    }else{
      $content['permissions_list'] = [];
    }

    
    $content['title'] = 'Permissions';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }


}