<?php 
class Dashboard extends CI_Controller
{

		
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Dashboard_model');
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index()
	{
  

			
	$content['title'] = 'Dashboard';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
		
	}


}