<?php 
/**
* Feedback List
*/
class Feedback extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model','model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');
   // print_r($this->loginData);

   
 }

 public function index()
 {
  try{
   // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');
  $content['tc_detail']=array();
  $content['getfeedbacklist']=array();
  if($RequestMethod == 'POST'){

   $teamid=$this->input->post('team_id');

   $content['tc_detail'] = $this->model->tc_details($teamid);
   $content['getfeedbacklist'] = $this->model->getFeedback_status($teamid);
          //  print_r( $content['getfeedbacklist']);

   }

   if($this->loginData->RoleID==2)
   {
     $content['getfeedbacklist'] = $this->model->getFeedbackList($this->loginData->teamid);
     $content['tc_detail'] = $this->model->tc_details($this->loginData->teamid);
   }

 $content['office_detail'] = $this->model->officedetail();
 $content['title'] = 'Feedback';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
 
 }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




 public function feedback_report()
 {
   // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
  $content['tc_detail']=array();

  $content['getfeedbacklist']=array();

  $RequestMethod = $this->input->server('REQUEST_METHOD');
  if($RequestMethod == 'POST'){

   $teamid=$this->input->post('team_id');

   $content['tc_detail'] = $this->model->tc_details($teamid);
   $content['getfeedbacklist'] = $this->model->getFeedback_report($teamid);
   //print_r($content['getfeedbacklist']);


   // $content['da_feedback_reports'] = $this->model->getReflective_report($content['getfeedbacklist']->candidateid);

  }
  
 

 $content['office_detail'] = $this->model->officedetail();

 $content['title'] = 'Feedback';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
 
}


public function edit($token=null)
{
   // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){
  //   echo "<pre>";
  // print_r($this->input->post());die;

    if ($this->input->post('intemationssave')=='save') {

     $feedbackdate = $this->input->post("feedbackdate");
     $feedbackdatedb = $this->model->changedatedbformate($feedbackdate);

     $this->db->trans_start();

     $apprenticeid = $this->input->post('name_of_apprentice');

     $staffdetails =  $this->model->getfeedbackStaffdetail($apprenticeid);

     $html = '<table  width="100%" >
     <tr>
     <td colspan="3" style="text-align: center;"><b>Feedback Report</b></td>
     </tr>
     <tr>
     <td colspan="3" style="text-align: center;">(To be filled by the Team Coordinator)</td>
     </tr>
     <tr>
     <td><b>Name of the Apprentice : </b></td>
     <td>'. $staffdetails->name .' </td> 
     <td><b>Team : </b></td>
     <td> '. $this->input->post('team') .'</td>
     </tr>
     <tr>
     <td><b>Team Coordinator (TC) : </b></td>
     <td>'. $this->input->post('tcname') .' </td>
     <td><b>Batch : </b></td>
     <td>'. $this->input->post('batch') .' </td>
     </tr>
     <tr>
     <td><b>Field Guide  :</b> </td>
     <td>'. $this->input->post('field_guide') .' </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td><b>Feedback Process : </b></td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>

     <tr>
     <td colspan="4" align="justify" ><b>(i) &nbsp;  Aspect(s) you find the Development Apprentice (DA) excels in or is extremely good at.</b></td>
     </tr>

     <tr>
     <td colspan="4" align="justify" >'. $this->input->post('excels_in_or_extremely_good') .'</td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td colspan="4" align="justify" ><b>(ii) &nbsp; Aspect(s) you think that the DA needs to pay attention to.</b></td>
     </tr>
     <tr>
     <td colspan="4" align="justify" >'. $this->input->post('da_needs_to_pay_attention') .'</td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td colspan="4" align="justify" ><b>(iii) &nbsp; Any concern that you may have seen, necessitating PRADAN to initiate action to ask the DA to leave the organization.</b></td>
     </tr>
     <tr>
     <td colspan="4" align="justify" >'. $this->input->post('da_to_leave_organization') .'</td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td><b>Date : </b></td>
     <td>'. $feedbackdate .' </td>
     <td> </td>
     <td> </td>
     </tr>

     <tr>
     <td><b>Name and Signature of TC:</b> </td>
     <td> '. $this->input->post('tcname') .'</td>
     <td> </td>
     <td> </td>
     </tr>
     </table>';
     $filename = 'feedback_'.md5(time());

     $this->load->model('Dompdf_model');
     $generate =   $this->Dompdf_model->feedback_letter_PDF($html, $filename);

     if ($generate==1) {

      $insertArr = array(
        'apprenticeid'  =>  $this->input->post('name_of_apprentice'),
        'teamid'  => $this->input->post('teamid'),
        'tcid'  => $this->input->post("tcid"),
        'batchid' => $this->input->post("batchid"),
        'field_guideid'  =>  $this->input->post('field_guideid'),
        'da_extremely_good'  => $this->input->post('excels_in_or_extremely_good'),
        'da_needs_to_pay_attention'  => $this->input->post("da_needs_to_pay_attention"),
        'da_to_leave_organization' => $this->input->post("da_to_leave_organization"),
        'monthyear' => $this->input->post("month"),
        'feedbackdate' => $feedbackdatedb,
        'status'  =>  0,
        'feedback_letter' => $filename,
        'createdon' => date('Y-m-d H:i:s'),
          'createdby' => $this->loginData->UserID, // login user id
        );
      $this->db->where('id',$token);
      $this->db->update('tbl_feedback', $insertArr);
   //$insertid = $this->db->insert_id(); 
      $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Error adding Feedback'); 
      }else{
       $this->session->set_flashdata('tr_msg', 'Successfully added Feedback'); 
       redirect('Feedback/edit/'.$token);    }
     }

   }

   if ($this->input->post('intemationssend')=='send') {

     $feedbackdate = $this->input->post("feedbackdate");
     $feedbackdatedb = $this->model->changedatedbformate($feedbackdate);

     $this->db->trans_start();

     $apprenticeid = $this->input->post('name_of_apprentice');

     $staffdetails =  $this->model->getfeedbackStaffdetail($apprenticeid);

     $html = '<table  width="100%" >
     <tr>
     <td colspan="3" style="text-align: center;"><b>Feedback Report</b></td>
     </tr>
     <tr>
     <td colspan="3" style="text-align: center;">(To be filled by the Team Coordinator)</td>
     </tr>
     <tr>
     <td><b>Name of the Apprentice : </b></td>
     <td>'. $staffdetails->name .' </td> 
     <td><b>Team : </b></td>
     <td> '. $this->input->post('team') .'</td>
     </tr>
     <tr>
     <td><b>Team Coordinator (TC) : </b></td>
     <td>'. $this->input->post('tcname') .' </td>
     <td><b>Batch : </b></td>
     <td>'. $this->input->post('batch') .' </td>
     </tr>
     <tr>
     <td><b>Field Guide  :</b> </td>
     <td>'. $this->input->post('field_guide') .' </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td><b>Feedback Process : </b></td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>

     <tr>
     <td colspan="4" align="justify" ><b>(i) &nbsp;  Aspect(s) you find the Development Apprentice (DA) excels in or is extremely good at.</b></td>
     </tr>

     <tr>
     <td colspan="4" align="justify" >'. $this->input->post('excels_in_or_extremely_good') .'</td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td colspan="4" align="justify" ><b>(ii) &nbsp; Aspect(s) you think that the DA needs to pay attention to.</b></td>
     </tr>
     <tr>
     <td colspan="4" align="justify" >'. $this->input->post('da_needs_to_pay_attention') .'</td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td colspan="4" align="justify" ><b>(iii) &nbsp; Any concern that you may have seen, necessitating PRADAN to initiate action to ask the DA to leave the organization.</b></td>
     </tr>
     <tr>
     <td colspan="4" align="justify" >'. $this->input->post('da_to_leave_organization') .'</td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td>&nbsp; </td>
     <td> </td>
     <td> </td>
     <td> </td>
     </tr>
     <tr>
     <td><b>Date : </b></td>
     <td>'. $feedbackdate .' </td>
     <td> </td>
     <td> </td>
     </tr>

     <tr>
     <td><b>Name and Signature of TC:</b> </td>
     <td> '. $this->input->post('tcname') .'</td>
     <td> </td>
     <td> </td>
     </tr>
     </table>';
     $filename = 'feedback_'.md5(time());

     $this->load->model('Dompdf_model');
     $generate =   $this->Dompdf_model->feedback_letter_PDF($html, $filename);


     if ($generate==1) {


       $staffid = $this->input->post('name_of_apprentice');

       $insertArr = array(
        'apprenticeid'  =>  $this->input->post('name_of_apprentice'),
        'teamid'  => $this->input->post('teamid'),
        'tcid'  => $this->input->post("tcid"),
        'batchid' => $this->input->post("batchid"),
        'field_guideid'  =>  $this->input->post('field_guideid'),
        'da_extremely_good'  => $this->input->post('excels_in_or_extremely_good'),
        'da_needs_to_pay_attention'  => $this->input->post("da_needs_to_pay_attention"),
        'da_to_leave_organization' => $this->input->post("da_to_leave_organization"),
        'monthyear' => $this->input->post("month"),
        'feedbackdate' => $feedbackdatedb,
        'status'  =>  1,
        'feedback_letter' => $filename,
        'createdon' => date('Y-m-d H:i:s'),
          'createdby' => $this->loginData->UserID, // login user id
        );
       $this->db->where('id',$token);     
       $this->db->update('tbl_feedback', $insertArr);
   //$insertid = $this->db->insert_id(); 
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Error adding Feedback'); 
      }else{

        $getstaffdetail = '';
        $getHRDEmailid = '';

        $getstaffdetail =  $this->model->getStaffDetails($staffid);
        $staffemail = $getstaffdetail->emailid;

        $hrdemail = $this->loginData->EmailID;
        $getHRDEmailid = $this->gmodel->getHRDEmailid();

        $to_name = $getHRDEmailid->hrdemailid;
    //  $filename = $getstaffdetail->generate_letter_name;

        $attachments = array($filename.'.pdf');

       //

        print_r($attachments);  

        $html = 'Dear Sir, <br><br> 

        Feedback letter . <br>
        Regards <br>
        Pradan Team';


// public function send_email_separation($subject, $body, $to_email= null, $to_name=null, $recipients=null, $attachments=array())





       $sendmail = $this->Common_Model->send_email_separation($subject = 'Feedback Letter ', $message = $html, $to_name, $staffemail, $staffemail,$attachments);  //// Send Mail candidates With Offer Letter ////
           // die($sendmail);
       if ($sendmail) {
        $insertArr = array(
          'status'  => 2,
        );
        $this->db->where('id',$token);
        $this->db->update('tbl_feedback', $insertArr);
        $this->session->set_flashdata('tr_msg', 'Successfully added Feedback'); 
        redirect('Feedback/');
      }
    }

  }


}
}

$valapprentice = $this->model->getApprentice($this->loginData->teamid);

$valappren = $this->model->getfeedbackStaffdetail($token);
    //print_r($valappren); die;


$content['feedbacklist'] = $this->model->getFeedbackdetail($token);

$content['getapprentice'] = $valapprentice;
$content['title'] = 'Feedback';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}



public function view($token=null)
{
  try {
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  $valapprentice = $this->model->getApprentice($this->loginData->teamid);
  $valappren = $this->model->getfeedbackStaffdetail($token);
  $content['feedbacklist'] = $this->model->getFeedbackdetail($token);
  $content['getapprentice'] = $valapprentice;
  $content['title'] = 'Feedback';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
    
  } catch (Exception $e) {

    print_r($e->getMessage());die;
    
  }
  
}


}