<?php 
class Payrollreport extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Payrollreport_model');
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		try{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission    



		$content['getactivehead'] = NULL;
		$content['staffsalary'] = NULL;

		$content['all_office'] = $this->Payrollreport_model->get_alloffice();
		

		
		

		$officeid = $this->input->post('transfer_office');
		$salmonth = $this->input->post('salmonth');
		$salyear = $this->input->post('salyear');
		if(!empty($salmonth)) {
			$salmonth = date('m');
			$salyear = date('Y');
		}

		$period = $salyear.$salmonth;

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){



			$content['getactivehead'] = $this->Payrollreport_model->getActivesalaryheads($period);
			$content['staffsalary'] = $this->Payrollreport_model->getstaffsalary($period,$salmonth,$salyear);

		}

		
		//print_r($staffsalary);
		$content['title'] = 'Payroll CTC Report';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	
	

}