<?php 

/**
* State Review
*/
class Staff_review extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_review_model","Staff_review_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
    try{
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

   $this->load->model('Staff_review_model');

   //$content['staff_seperation'] = $this->Staff_review_model->get_staff_seperation_detail();
   /*echo "<pre>";
   print_r($content['staff_seperation']);exit();*/
  
   $content['staff_details'] = $this->Staff_review_model->getstaffname();
   // print_r($content['staff_details']);
   // die;


   
   $content['staff_probation_details'] = $this->Staff_review_model->getstaffprobationname();


  // print_r($content['staff_probation_details']); 




   $content['title'] = 'Staff_review';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }

 public function stfflistdectivated($token)
 {
  try{

  $this->load->model('Staff_review_model');
  $fetch=$this->Staff_list_model->fetchdatas($token);


  if($fetch==-1)
  {
    $this->session->set_flashdata('er_msg', 'Sorry');

  }
  else
  {

    $updateArrs = array(
      'status'    => 0,
    );
    $this->db->where('staffid', $token);
    $this->db->update('staff', $updateArrs);
    $this->session->set_flashdata('tr_msg', 'Staff is deactivated');
  }
  

  
  redirect('/Staff_review/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  
} 


}