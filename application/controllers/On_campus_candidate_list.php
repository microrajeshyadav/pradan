<?php 
class On_campus_candidate_list extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model("Writtenscore_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');



	}

	public function index()
	{
		 // start permission 
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." "; 
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{
			if($this->input->post('campusid')){
				$campusdata = $this->input->post('campusid');
				$campexp = explode('-', $campusdata);
				$campusid = $campexp[0];
				$campusintimationid = $campexp[1];

			}else {
				$campusid = 'NULL';
				$campusintimationid = 'NULL';
				$campusdata = '';
			}

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			if($RequestMethod == 'POST'){
				$this->db->trans_start();
				if ($this->input->post('btnaccept')=='Submit') {
					// print_r($this->input->post()); die();
					$countaccept = count($this->input->post('accept'));
					if ($countaccept > 0)
					{
						for ($i=0; $i < $countaccept ; $i++) {

							$candidateid = $this->input->post('accept')[$i]; 

							if ($candidateid !='') {
								$checkaccept = "accepted";
							}else{
								$checkaccept = 'Null';
							}
							$updateAcceptReject	 = array(

								'frstatus'      => $checkaccept,
								'frdate'        => date('Y-m-d H:i:s'),
								'frcreatedby'	=> $this->loginData->UserID,
								'inprocess'     => 'closed',
							);
							$this->db->where('candidateid',$candidateid);
							$this->db->update('tbl_candidate_registration', $updateAcceptReject);

						}

						$this->db->trans_complete();
						if ($this->db->trans_status() === FALSE){

							$this->session->set_flashdata('er_msg', 'Error Candidate not accept ');	
							redirect(current_url());	
						}else{


							// $this->session->set_flashdata('tr_msg', 'Successfully Candidate accept');
							// redirect('On_campus_candidate_list/index');
						}
					}
					$countreject = count($this->input->post('reject'));

					if ($countreject > 0)
					{
						for ($i=0; $i < $countreject ; $i++) { 

							$Candidateid = $this->input->post('reject')[$i];

							if ($Candidateid !='') {
								$checkaccept = 'rejected';
							}else{
								$checkaccept = 0;
							}

							$updateAcceptReject	 = array(
								'frstatus'     => $checkaccept,
								'frdate'       => date('Y-m-d H:i:s'),
								'frcreatedby'  => $this->loginData->UserID,
								'inprocess'     => 'closed',
							);
							$this->db->where('candidateid',$Candidateid);
							$this->db->update('tbl_candidate_registration', $updateAcceptReject);
						}


						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							
							$this->session->set_flashdata('er_msg', 'Error Candidate not reject');	
							redirect(current_url());	
						}else{

				// 			$countreject = count($this->input->post('reject'));

				// 			for ($i=0; $i < $countreject ; $i++) { 

				// 	$Candidateid = $this->input->post('reject')[$i];
				// 	$candidatedetails = $this->model->getCandidateDetailsPreview($Candidateid);
				// 	$candidate = array('$candidatefirstname','$candidatemiddlename','$candidatelastname');
				// 	$candidate_replace = array($candidatedetails->candidatefirstname,$candidatedetails->candidatemiddlename,$candidatedetails->candidatelastname);
				// 	$candidatefirstname = $candidatedetails->candidatefirstname;
				// 	$candidatemiddlename = $candidatedetails->candidatemiddlename;
				// 	$candidatelastname = $candidatedetails->candidatelastname;
				// 				$message='';
				// 				$subject = "From PRADAN"; 
		 	// //print_r($candidatedetails); die;
				// 				// $fd = fopen("mailtext/HRDRejection_Off_campus.txt", "r"); 
				// 				// $message .=fread($fd,4096);
				// 				// eval ("\$message = \"$message\";");
				// 				// $message =nl2br($message);
				// 				// $body = $message;

				// 		//update from db
				// 		$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 9 AND `isactive` = '1' AND id = 7";
   	// 	    			$data = $this->db->query($sql)->row();
   	// 	    			if(!empty($data))
   	// 	    			{
   	// 	   				$body = str_replace($candidate,$candidate_replace ,$data->lettercontent);

				// 				$candidateemail  = $candidatedetails->emailid;
				// 				$hrdemailid = $this->model->getHrdemailid();
			 // 	$to_email    = $candidateemail; //// Candidate Mail Id ////
			 // 	$to_name     = $hrdemailid; /// HRD Mail Id ////
			 // 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);
			 // }
			 // }
						//echo $this->db->last_query(); die;
			
			}

		}
		 $this->session->set_flashdata('tr_msg', 'Data submitted successfully.');
			 redirect('On_campus_candidate_list/index');
	}




}


$content['campusid'] = $campusdata;

$content['selectedcandidatedetails']  = $this->model->getElihibalCandidateListForWrittenExam($campusid,$campusintimationid);
			 //print_r($content['selectedcandidatedetails']);
			// die;

$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
$content['campuscutoffmarks'] = $this->model->getCampusCutOffMarks($campusid, $campusintimationid);

$content['selectedcandidatewrittenscore'] 	 = $this->model->getSelectedCandidateWrittenScore($campusid);
			// print_r($content['selectedcandidatewrittenscore']);
			// die;

$content['method']  = $this->router->fetch_method();
$content['title']  = 'Writtenscore';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
	print_r($e->getMessage());die;
}

}

public function Selecttowrittenexamcandidates() 
{
				 // start permission 
	$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
	$content['role_permission'] = $this->db->query($query)->result();
// end permission 
	try{

			//print_r($this->loginData); 

		if($this->input->post('campusid') !=NULL && $this->input->post('campusid') !=''){
			$campusdata = $this->input->post('campusid');
			$campexp = explode('-', $campusdata);
			$campusid = $campexp[0];
			$campusintimationid = $campexp[1];

		}else {
			$campusdata =NULL;
			$campusid ='NULL';
			$campusintimationid = 'NULL';

		}


		$content['campusid'] = $campusdata;
			//$content['campustype'] = $campusdata;
		$selected_candidate_list = $this->model->getElihibalCandidateListForWrittenExam($campusid, $campusintimationid);

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
			$this->db->trans_start();
			if ($this->input->post('btnaccept')=='AcceptData') {

				$countaccept = count($this->input->post('accept'));

				for ($i=0; $i < $countaccept ; $i++) {

					$candidateid = $this->input->post('accept')[$i]; 

					if ($candidateid !='') {
						$checkaccept = "accepted";
					}else{
						$checkaccept = 'Null';
					}

					$updateAcceptReject	 = array(

						'frstatus'      => $checkaccept,
						'frdate'        => date('Y-m-d H:i:s'),
						'frcreatedby'	=> $this->loginData->UserID,
						'inprocess'     => 'closed',
					);
					$this->db->where('candidateid',$candidateid);
					$this->db->update('tbl_candidate_registration', $updateAcceptReject);

				}

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error Candidate not accept ');	
					redirect(current_url());	
				}else{

					$this->session->set_flashdata('tr_msg', 'Successfully Candidate accept');
					redirect('Writtenscore/Selecttowrittenexamcandidates');
				}

			}


			if ($this->input->post('btnreject')=='RejectData') {

				$countreject = count($this->input->post('reject'));


				for ($i=0; $i < $countreject ; $i++) { 

					$Candidateid = $this->input->post('reject')[$i];

					if ($Candidateid !='') {
						$checkaccept = 'rejected';
					}else{
						$checkaccept = 0;
					}

					$updateAcceptReject	 = array(
						'frstatus'     => $checkaccept,
						'frdate'       => date('Y-m-d H:i:s'),
						'frcreatedby'  => $this->loginData->UserID,
						'inprocess'     => 'closed',
					);
					$this->db->where('candidateid',$Candidateid);
					$this->db->update('tbl_candidate_registration', $updateAcceptReject);
				}


				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error Candidate not reject');	
					redirect(current_url());	
				}else{
						//echo $this->db->last_query(); die;
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate reject');
					redirect('Writtenscore/Selecttowrittenexamcandidates');
				}

			}

		}

		$content['selectedcandidatedetails'] = $selected_candidate_list;
		$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
		$content['title']    = 'Writtenscore';
		$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function generate_pdf_selected_candidate($token=NULL) 
{

	if (empty($token) || $token == '' ) {
		$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
		redirect('/Writtenscore/index');

	} else { 

				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			//echo $token; die;

			
			$selectedcandidatedetails = $this->model->getSelectedCandidateWrittenScore($token);
			//print_r($selectedcandidatedetails); die;


			echo $table = ' <table>';
			$table .= '<tr>';
			$table .= '<td>S.No.</td>';
			$table .= '<td>Candidate</td>';
			$table .= '<td> Email Id</td>';
			$table .= '</tr>'; 
			$i=0;
			foreach ($selectedcandidatedetails as $key => $value) { 

				$table .=  '<tr>';
				$table .=  ' <td>'.$i.'</td>';
				$table .=  '<td>'.$value->candidatefirstname.'</td>';
				$table .=  '<td>'.$value->emailid.'</td>';
				$table .=  '</tr> ';
			}
			$table .=  '</table> ';
			die;
			
			$content['campusdetails'] = $this->model->getCampus();
			$content['title']    = 'Writtenscore';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
}

}