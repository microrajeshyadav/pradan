<?php 

/**
* Candidate details controller
*/
class Candidatedetails extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		$this->load->model("Candidatedetails_model");
		$this->load->model("Common_model","Common_Model");


		// $this->load->Libraries('form_validation');
		// $this->load->libraries('Phpmailer');
		$this->load->model(__CLASS__ . '_model', 'model');	
	}


	public function index()
	{
		try{
     // echo  $this->session;
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == "POST")
		{
			//print_r($this->model->verifylogin());
			$data = $this->model->verifylogin();
			// echo $data; 
			//  die; 

			//die;
			if($data == 1){
				//echo "czx"; //die;
				$this->loginData = $this->session->userdata('login_data');

			   // print_r($this->loginData); die;

				if(isset($this->loginData->RoleID)  &&  $this->loginData->RoleID ==6 && $this->loginData->UserID==4) {

					redirect('EDashboard');

				}elseif (isset($this->loginData->RoleID)  && $this->loginData->RoleID ==4) {
					 
					 redirect('HRDDashboard');

				}else{
					redirect('campus');
				}


				//$this->session->set_userdata('login_data', $result[0]);
				

			}else{
				//echo "amit"; die;
				redirect('Candidatedetails');
			}
		}

	    $content['statedetails']        = $this->model->getState();
		$content['ugeducationdetails']  = $this->model->getUgEducation();
		$content['pgeducationdetails']  = $this->model->getPgEducation();
		$content['campusdetails']       = $this->model->getCampus();
		$content['title'] = 'Candidatedetails';

		//$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		//$this->load->view('_main_layout', $content);

		$this->load->view(__CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__, $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}
	
	

}