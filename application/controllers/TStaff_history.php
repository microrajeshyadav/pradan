<?php 

/**
* Staff History */
class TStaff_history extends CI_controller
{

  function __construct()
  {

    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');

 }

 public function index()
 {

   try{
    
    $rid = $this->uri->segment(3);

    $this->load->model("TStaff_history_model");
    // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // echo "<pre>";
    // print_r($content['role_permission']);
    // die;
    // end permission 


    $content['getworkflowdetail'] = $this->TStaff_history_model->getStaff_Transfer_History($rid);
    

    // echo "<pre>";
    // print_r($content['getworkflowdetail']);
   // echo $content['getworkflowdetail']->recivername;
    //die;


    $content['title'] = 'TStaff_history';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }


 }
 

}