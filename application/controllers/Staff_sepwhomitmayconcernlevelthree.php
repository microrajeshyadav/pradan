<?php 
class Staff_sepwhomitmayconcernlevelthree extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepwhomitmayconcernlevelthree_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");


		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$query ="SELECT * FROM staff_transaction WHERE id =".$token; 
			$content['seplevel_three'] = $this->db->query($query)->row();

			$content['level_three'] = $this->Common_Model->get_staff_sep_detail($token);
			$content['staffid'] = $content['seplevel_three']->staffid;
			
			// print_r($content['level_three']); die;

			$query = "SELECT * FROM tbl_levelwisetermination WHERE type = 1 and  transid = ".$token;
			$content['levelthree_detail'] = $this->db->query($query)->row();



			// print_r($content['levelfour_detail']); die;

			$content['staffid'] = $content['seplevel_three']->staffid;

			$RequestMethod = $this->input->server("REQUEST_METHOD");


			if($RequestMethod == 'POST')
			{

				
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'save & submit'){

					$db_flag = 1;
				}

				$insertArray = array(

					'desid'              => $this->input->post('sepdesigid'),
					'staffid'            => $content['seplevel_three']->staffid,
					'transid'            => $token,
					'sep_no'             => $this->input->post('sep_no'),
					'total_staff'        => $this->input->post('total_staff'),
					'total_professional' => $this->input->post('total_professional'),
					'flag'               => $db_flag,
					'type'               => 1,
					'createdby'          => $this->loginData->staffid,
				);

				if($content['levelthree_detail'])
				{
					$updateArray = array(
					'desid'              => $this->input->post('sepdesigid'),	
					'staffid'            => $content['seplevel_three']->staffid,
					'transid'            => $token,
					'sep_no'             => $this->input->post('sep_no'),
					'total_staff'        => $this->input->post('total_staff'),
					'total_professional' => $this->input->post('total_professional'),
					'flag'               => $db_flag,
					'type'               => 1,
					'updatedby'          => $this->loginData->staffid,
					'updatedon'          => date("Y-m-d H:i:s"),
					);
					$this->db->where('id', $content['levelthree_detail']->id);
					$flag = $this->db->update('tbl_levelwisetermination', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_levelwisetermination',$insertArray);
				}
				//echo $this->db->last_query(); die;

				if($flag) {
				  $subject = "Your Request Submited Successfully";
				  $body = 'Dear,<br><br>';
				  $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
				  $body .= 'This is to certify that Mr. '.$content['level_three']->name.'<br> has issued experience certificate.<br><br>';
				  $body .= 'Thanks<br>';
				  $body .= 'Administrator<br>';
				  $body .= 'PRADAN<br><br>';

				  $body .= 'Disclaimer : <br>';
				  $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

				  $to_email = $content['level_three']->emailid;
				  $to_name = $content['level_three']->name;
				  $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name);
				  if (substr($email_result, 0, 5) == "ERROR") {
				  	$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
				  }
					$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
					redirect(current_url());
				} 
				else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
					redirect(current_url());
				}


	}

	$content['title'] = 'Staff_sepwhomitmayconcernlevelthree';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}