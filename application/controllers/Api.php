<?php 

/**
 * Api controller for healthrise
 */
class Api extends Ci_controller
{
	private $user;

	public function __construct()
	{
		
		parent::__construct();
		$this->load->model('Api_model');
		$auth_res = $this->Api_model->authenticateApiUser();

		if(is_string($auth_res))
		{
			die($auth_res);
		}
		$this->user = $auth_res;
	}

	public function login()
	{
		header("Content-Type: application/json");

		$content = $this->Api_model->get_masters($this->user);
		
		$users[0] = $this->user;
		$content["mstuser"] = $users;

		die(json_encode($content));
		
	}

	
	public function index()
	{

	}

	public function getMasterData()
	{

		$sFlag = strtolower($this->input->post('sFlag'));
		// echo $sFlag; die();
		switch ($sFlag) {
			case 'master':
			$content = $this->Api_model->get_masters($this->user);
			break;
			case "tblpatientclinicaltestdetails":
			case "tblpatientfollowup":
			case "tblpatientreffrredfollowup":
			case "tblpatientregistrationdetails":
			case "tbl_eclinic":
			case "tbl_eclinic_suggestion_advice":
			$content = $this->Api_model->get_table_data($this->user, $sFlag);
			break;
			default:
			die("ERROR: the sFlag $sFlag is not supported");
			break;
		}

		header("Content-Type: application/json");
		die(json_encode($content));
	}

	public function uploadData()
	{

		$content = $this->Api_model->uploadData($this->user);

		$data = $this->input->post('data');

		if (is_array($content)) {
			// insert success log
			$insertArr = array(
				'UserID' => $this->user->UserID, 
				'JSON'=> $data,
				'ImportedOn'	=>	date("Y-m-d H:i:s"),
				'Status'=>'Success',
				'Message'=>json_encode($content),
				'ErrorNumber'=>NULL,
				'ErrorMessage'=>NULL,
				);
			$this->db->insert('tablet_dataimport', $insertArr);

		}else{


			// insert error log
			$db_error = $this->db->error();

			$insertArr = array(
				'UserID' => $this->user->UserID,
				'JSON'=> $data,
				'ImportedOn'	=>	date("Y-m-d H:i:s"),
				'Status'=>'Error',
				'Message'=>json_encode($content),
				'ErrorNumber'=>$db_error['code'],
				'ErrorMessage'=>(trim($db_error['message']) == ""?NULL:trim($db_error['message'])),
				);
			$e = $this->db->insert('tablet_dataimport', $insertArr);
			if ($e == FALSE) {
				echo "error in executing " . print_r($this->db->error(), TRUE);
			}
		}

		header("Content-Type: application/json");
		die(json_encode($content));
	}

	public function make1900Null($table_name)
	{
		$tableFields = $this->db->list_fields($table_name);
		
		foreach ($tableFields as $field) {
			$query = "update $table_name set `$field`= NULL where `$field` = '1900-01-01'";
			$this->db->query($query);
		}

	}

	public function PutImage()
	{
		
		$result = $this->Api_model->PutImage();
		die($result);
	}

	public function DownloadFile()
	{
		$this->Api_model->DownloadFile();
	}

	
}