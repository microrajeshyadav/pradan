<?php 
class Prejoining extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->model("Assigntc_model");
		//$this->load->model("Dompdf_model");
		$this->load->model("Common_model","Common_Model");
		

		$this->load->model(__CLASS__ . '_model','model');
		//$mod = $this->router->class.'_model'; 
	        //$this->load->model($mod,'',TRUE);
		//$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{

		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		try{

			$campusid = $this->input->post('campusid'); 

			if ($campusid =='') {
				$campusid ='NULL';
			}

			$content['prejoiningdetails']  = $this->model->getPrejoiningList();
			$content['campusdetails'] 	   = $this->model->getCampus();

			$content['title'] 		= 'Reassigntc';
			$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function add(){

 // start permission 

		try{


			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				$this->db->trans_start();
				/*echo "<pre>";
				print_r($_POST);exit();*/

				$this->form_validation->set_rules('daname','DA Name ','trim|required');
				$this->form_validation->set_rules('batch','Batch Name ','trim|required');
			//$this->form_validation->set_rules('latter','Latter ','trim|required');


				if($this->form_validation->run() == FALSE){
					$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
						'</div>');

					$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

					$hasValidationErrors    =    true;
					goto prepareview;

				}

				$candidateid = $this->input->post('daname');

				$insertArr = array(
					'candidateid'    	=> $this->input->post('daname'),
					'batch'     		=> $this->input->post('batch'),
					'prejoining_letter' => $this->input->post('summernote'),
					'createdon'      	=> date('Y-m-d H:i:s'),
				    'createdby'      	=> $this->loginData->UserID, // login user id
				    'isdeleted'      	=> 0, 
			);

				$this->db->insert('tbl_prejoining_letter', $insertArr);

				$insertid = $this->db->insert_id();


				$countupload_material = count($_FILES['prejoiningdocument']['name']);

				for ($i=0; $i < $countupload_material ; $i++) { 

					if ($_FILES['prejoiningdocument']['name'][$i] != NULL) {

       // echo "xczxc"; die;
						@  $ext = end((explode(".", $_FILES['prejoiningdocument']['name'][$i])));
						$OriginalUploadMaterialPhotoName = $_FILES['prejoiningdocument']['name'][$i];
						$encryptedName = $insertid.'_'. md5(date("Y-m-d H:i:s").$i) . "." .$ext;

						$tempFile = $_FILES['prejoiningdocument']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/prejoiningdocument/";
						$targetFile = $targetPath . $encryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedPloadMaterialNameImage = $encryptedName;

						}else{

							// die("Error uploading Photo file");
							$this->session->set_flashdata("er_msg", "Error uploading event document");
							redirect(current_url());
						}
					}

					$insertArr_matrial = array(
						'prejoining_letterid'     => $insertid,
						'encrypted_prejoining_document_name' => $encryptedPloadMaterialNameImage,
						'original_prejoining_document_name' => $OriginalUploadMaterialPhotoName,
					);

					$this->db->insert('tbl_prejoining_document', $insertArr_matrial);

   // echo $this->db->last_query(); die;
				}

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){
					$this->session->set_flashdata('er_msg', 'Error Letter !!!!');	
				}else{

					$getcandidateDetails = $this->model->getCandidateDetails($candidateid);
				// print_r($getcandidateDetails);

					$hrname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;

					$hr1 = array('$candidatefirstname','$hrname');
					$hr_replace1 = array($getcandidateDetails->candidatefirstname,$hrname);

					$candidateemail = $getcandidateDetails->emailid;
					$hrdemail = $this->loginData->EmailID;

					$getprejoiningdocument = $this->model->getPrejoiningDocumentDetails($insertid);

				//print_r($getprejoiningdocument);
					$filename = array();

					foreach ($getprejoiningdocument as $key => $value) {
						$filename[] = $value->encrypted_prejoining_document_name;
					}

					$attachments = $filename;
					$message='';


					$subject ="Pre-joining formalities (Induction kit)";
				  // $body = 	'Dear Sir, <br><br> ';
				  // $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
					// $fd = fopen("mailtext/prejoining.txt","r");	
					// $message .=fread($fd,4096);
					// eval ("\$message = \"$message\";");
					// $message =nl2br($message);

					$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 16 AND `isactive` = '1'";
   		    			$data = $this->db->query($sql)->row();
   		    			 if(!empty($data))
					$message= str_replace($hr1, $hr_replace1, $data->lettercontent);

				//print_r($attachments); die;

					$html = $getcandidateDetails->prejoining_letter;


					$sendmail = $this->Common_Model->send_email_prejoining_document($subject, $message, $candidateemail, $hrdemail,
			 $recipients = null, $attachments);  //// Send Mail ED 


					if ($sendmail) {

						$insertArrstatus = array(
							'status'     => 1,

						);
						$this->db->where('id', $insertid);
						$this->db->update('tbl_prejoining_letter', $insertArrstatus);

					}
					$this->session->set_flashdata('tr_msg', 'Successfully send Letter ');			
				}

				redirect('/Prejoining/index');
			}

			prepareview:

			$content['batchdetails']   = $this->model->getBatchlist();
			$content['dadetails']      = $this->model->getcheckdocumentcandidatelist();

			$content['method']         = $this->router->fetch_method();
			$content['title']          = 'add';
			$content['subview']        = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function edit($token){


		try{


	   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		   $RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				$this->db->trans_start();

				$candidateid = $this->input->post('daname');

				$insertArr = array(
					'candidateid'    	=> $this->input->post('daname'),
					'batch'     		=> $this->input->post('batch'),
					'prejoining_letter' => $this->input->post('summernote'),
					'updatedon'      	=> date('Y-m-d H:i:s'),
				    'updatedby'      	=> $this->loginData->UserID, // login user id
				    'isdeleted'      	=> 0, 
			);
				$this->db->where('id',$token);
				$this->db->update('tbl_prejoining_letter', $insertArr);
	

				$countupload_material = count($_FILES['prejoiningdocument']['name']);

				$this->db->where('prejoining_letterid', $token);
				$this->db->delete('tbl_prejoining_document'); 

				for ($i=0; $i < $countupload_material ; $i++) { 

					if ($_FILES['prejoiningdocument']['name'][$i] != NULL) {

       // echo "xczxc"; die;
						@  $ext = end((explode(".", $_FILES['prejoiningdocument']['name'][$i])));
						$OriginalUploadMaterialPhotoName = $_FILES['prejoiningdocument']['name'][$i];
          // $encryptedName = md5(date("Y-m-d H:i:s").$i) . "." . $ext;
						$encryptedName = $token.'_'. md5(date("Y-m-d H:i:s").$i) . "." . $ext;

						$tempFile = $_FILES['prejoiningdocument']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/prejoiningdocument/";
						$targetFile = $targetPath . $encryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedPloadMaterialNameImage = $encryptedName;

						}else{

							// die("Error uploading Photo file");
							$this->session->set_flashdata("er_msg", "Error uploading event document");
							redirect(current_url());
						}
					}else{
						$encryptedPloadMaterialNameImage = $this->input->post('encryptedprejoiningdocumentold')[$i];
						$OriginalUploadMaterialPhotoName = $this->input->post('original_prejoining_document_name')[$i];

					}

					$insertArr_matrial = array(
						'prejoining_letterid'     => $token,
						'encrypted_prejoining_document_name' => $encryptedPloadMaterialNameImage,
						'original_prejoining_document_name' => $OriginalUploadMaterialPhotoName,
					);

					$this->db->insert('tbl_prejoining_document', $insertArr_matrial);

    // echo $this->db->last_query(); die;
				}

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){
					$this->session->set_flashdata('er_msg', 'Error Letter !!!!');	
				}else{

					$getcandidateDetails = $this->model->getCandidateDetails($candidateid);
				//print_r($getEdDetails);

					$candidateemail = $getcandidateDetails->emailid;
					$hrdemail = $this->loginData->EmailID;

					$getprejoiningdocument = $this->model->getPrejoiningDocumentDetails($token);

				//print_r($getprejoiningdocument);

					$filename = array();

					foreach ($getprejoiningdocument as $key => $value) {
						$filename[] = $value->encrypted_prejoining_document_name;
					}

					$attachments = $filename;

				// print_r($attachments); die;


					$html = $getcandidateDetails->prejoining_letter;
					$recipients = array($hrdemail => 'hr' );

					$sendmail = $this->Common_Model->send_email_prejoining_document($subject = 'Prejoining letter by Pradan', $message = $html, $candidateemail, $hrdemail,
			 $recipients, $attachments);  //// Send Mail ED 
			// echo $sendmail; die();
					if ($sendmail) {

						$insertArrstatus = array(
							'status'     => 1,

						);
						$this->db->where('id', $insertid);
						$this->db->update('tbl_prejoining_letter', $insertArrstatus);

					}
					$this->session->set_flashdata('tr_msg', 'Successfully send Letter ');			
				}

				redirect('/Prejoining/index');
			}


			$content['presinglejoiningdetails']  = $this->model->getSinglePrejoiningList($token);
			$content['daeditdetails']   = $this->model->getallcheckdocumentcandidatelist();
			$content['getprejoiningattachments'] =  $this->model->getPrejoiningDocumentDetails($token);

			$content['title'] = 'edit';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	function delete($token = null)
	{
		try{


		if (empty($token) || $token == '' ) {

			redirect('/Campusinchargeintimation/index');
			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');

		} 

		else
		{ 

			if (!empty($token)) {
				$updateArrstatus = array(
					'isdeleted'     => 1,

				);
				$this->db->where('id', $token);
				$this->db->update('tbl_prejoining_letter', $updateArrstatus);
				$this->session->set_flashdata('tr_msg' ,"Prejoining Letter Deleted Successfully");
				redirect('/Prejoining/index');
			}else{
				$this->session->set_flashdata('er_msg' ,"Sorry !!! Error Prejoining Letter Deleted ");
				redirect(current_url());
			}	



			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	public function sendmail()
	{
		try{
			// start permission 
		
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 


		// print_r($this->loginData); die();
		$message ='';
		$subject ='';
		$hrunitdetails = $this->model->getHRunitDetails();
		// print_r($hrunitdetails); die();
		$hrname ='';
		$hrrole ='';
		$hrdesignation ='';
		$hr = array('$hrname','$hrrole','$hrdesignation');
		$hr_replace = array($hrunitdetails->name,$hrunitdetails->rolename,$hrunitdetails->desname);

		// $prejoiningdetail = $this->model->prejoing_supervisior();
		// echo "<pre>";
		// print_r($prejoiningdetail); die();
		// $prejoiningbatch = $prejoiningdetail[0]->batch; 
		$subject = "Intimation of offers for the ** batch of Development Apprenticeship programme";
		// $fd = fopen("mailtext/joining_intimation.txt", "r");
		// $message .=fread($fd,4096);

		// echo $message; die();
		// $message .=fread($fd,4096);
		// eval ("\$message = \"$message\";");
		// $body = nl2br($message);
		// $body = preg_replace("/<br\W*?\/>/", "\n", $body);

		$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 7 AND `isactive` = '1'";
   		$data = $this->db->query($sql)->row();
   		// $hrname = '';

   		// echo $data->lettercontent; die();

   		if(!empty($data))
   	    $body = str_replace($hr,$hr_replace , $data->lettercontent);

	 $RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){
			// print_r($this->input->post()); die();
			$message = $this->input->post('lettercontent');
			$subject = $this->input->post('subject');
			$batch = $this->input->post('batch');
			
			$this->form_validation->set_rules('subject','Subject','trim|required');
			$this->form_validation->set_rules('lettercontent','Message','trim|required');

			if($this->form_validation->run() == FALSE){
				$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
					'</div>');

				$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors    =    true;
				goto prepareview;

			}


			// $hrname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;

			// 	$hrdemail = $this->loginData->EmailID;

			$supervisior = $this->model->getAllSuprvsn();
			// print_r($supervisior); die();
			$recipients1 = array();

			$content['prejoiningdetails']  = $this->model->prejoing_supervisior($batch);
 
			foreach($content['prejoiningdetails'] as $val)
			{
				// $teamid=$val->teamid;

				// $batch=$val->batch;
				// $candidatefirstname=$val->candidatefirstname;

				$supervisior_staffid=$this->model->supervisior_staffid($val->teamid);
				$supervisior_name=$supervisior_staffid->name;
				$supervisior_emailid=$supervisior_staffid->emailid;
				$supervisior_staffid=$supervisior_staffid->staffid;


				$recipients2 = array(
				$supervisior_emailid => $supervisior_name);
				$recipients1 = array_merge($recipients1,$recipients2);

			}
			// echo $subject; 
			// echo $message =nl2br($message); die();

			$sendmail = $this->Common_Model->send_email($subject, $message, $to_email = null,$to_name = null,$recipients1);

			if($sendmail==1){

				$this->session->set_flashdata('tr_msg', 'Successfully Email Sent');
				redirect('Prejoining/index');

			}else{

				$this->session->set_flashdata('er_msg', 'Error !! Email Not Send !!!');
				redirect(current_url());
			}

		}

		prepareview:
		$query = "select batch from mstbatch where status =0 AND isdeleted='0' AND stage = 1";
        $content['batch_details'] = $this->Common_Model->query_data($query);
		$content['title'] = 'Phase';
		$content['campusdetails'] 	   = $this->model->getCampus();
		$content['body'] = $body;
		$content['subject'] = "Intimation of offers for the ** batch of Development Apprenticeship programme"; 
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

}