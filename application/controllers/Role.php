<?php 
class Role extends Ci_controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
	}

	public function index()
	{
		$content['role_list'] = $this->db->get('mstrole')->result();
		$content['title'] = 'role';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}

	public function Add()
	{
		try{

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
			$insertArr = array(
				'RoleID'        => $this->input->post('RoleID'),
				'RoleName'      => $this->input->post('RoleName'),
				'IsDeleted'     => 0,
				);
			$this->Common_Model->insert_data('mstrole', $insertArr);
			$this->session->set_flashdata('tr_msg', 'Successfully added Role');
			redirect('/role/');
		}
		$content['title'] = 'role';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}

	public function edit($ID)
	{
		try{
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
			$updateArr = array(
				'RoleID'        => $this->input->post('RoleID'),
				'RoleName'      => $this->input->post('RoleName'),
				'IsDeleted'     => 0,
				);

			$this->Common_Model->update_data('mstrole', $updateArr,'ID',$ID);
			$this->session->set_flashdata('tr_msg', 'Successfully Updated ANM');
			redirect('/role/');
		}
		$content['title'] = 'role';
		$role_list = $this->Common_Model->get_data('mstrole', '*', 'ID',$ID);
		$content['role_list'] = $role_list;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }

	}

	function delete($ID)
	{
		try{
		$this->Common_Model->delete_row('mstrole','ID', $ID); 
		$this->session->set_flashdata('tr_msg' ,"ROLE Deleted Successfully");
		redirect('/role/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}

}