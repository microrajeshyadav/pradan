<?php 

/**
* State List
*/
class Provident_fund_nomination_form_staff extends CI_controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    $this->load->model("General_nomination_and_authorisation_form_model");
    $this->load->model("Provident_fund_nomination_form_model");
    $this->load->model("Employee_particular_form_model");
    $this->load->model('Medical_certificate_model');

    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

  
 public function index($staff=null)
 {

  $this->session->set_userdata('staff', $staff);

  $staff_id=$this->loginData->staffid;


  if(isset($staff_id))
  {

    $staff_id=$staff_id;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;


 }
 $content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Provident_fund_nomination_form_staff_model->personal_email();
      // print_r($content['personnal_mail']);
 $personal_email=$content['personnal_mail']->EmailID;
      // die;


 $reportingto = $content['candidatedetailwithaddress']->reportingto;
 $content['tc_email'] = $this->Provident_fund_nomination_form_staff_model->tc_email($reportingto);

     // print_r($content['tc_email']); die;



 $content['report']=$this->Medical_certificate_model->staff_reportingto($staff_id);

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 

 if($RequestMethod == "POST"){

print_r($this->input->post()); die();


  $Sendsavebtn = $this->input->post('savebtn');

  if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

   $dadate = $this->input->post('date');

   $dadate = $this->gmodel->changedatedbformate($dadate);

   $m_date = date("Y-m-d");
   $insertarraydata = array(

     'candidate_id'        => 0,
     'staff_id'           =>$staff_id,
     'place'              => $this->input->post('daplace'),
     'date'               => $dadate,
     'status'             => 0,
     'type'               =>'m',
     'm_date'             => $m_date

   );

   $this->db->insert('provident_fund_nomination', $insertarraydata);
   $insertid = $this->db->insert_id();
   $data1=$this->input->post('data');

   foreach($data1 as $key => $value)
   {
    if ($value['name'] != '') {
    $m_date = date("Y-m-d");
    $arr=array('sr_no'=>$key,
      'name'=>$value['name'],
      'provident_id'=>$insertid,
      'relation_id'=>$value['relationship_nominee'],
      'age'=>$value['age_nominee'],
      'address'=>$value['address_nominee'],
      'share_nominee'=>$value['share_nominee'],
      'minior'=>$value['minior'],
      'type'=>'m',
      'm_date'=>$m_date

    );

    $this->db->insert('provident_fund_nomination_details', $arr);
  }
}

  $insert_data =array(
    'type'=>23,
    'r_id'=> $insertid,
    'sender'=> $staff_id,
    'receiver'=>$content['report']->reportingto,
    'senddate'=>date('Y-m-d'),
    'createdon'=>date('Y-m-d H:i:s'),
    'createdby'=>$this->loginData->UserID,
    'flag'=>4,
    'staffid'=>$staff_id
  );
  $this->db->insert('tbl_workflowdetail', $insert_data);









  $this->db->trans_complete();

  if ($this->db->trans_status() === FALSE){

    $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

  }else{

       // print_r($content['candidatedetailwithaddress']);





    $subject = ': Provident Fund Nomination';
    $body = '<h4>'.$content['candidatedetailwithaddress']->staff_name.' submited Provident fund Nomination Form </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidatedetailwithaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidatedetailwithaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidatedetailwithaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidatedetailwithaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidatedetailwithaddress']->staff_name ."<br>";
    $body .= " ". $content['candidatedetailwithaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidatedetailwithaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;

    $to_name=$content['candidatedetailwithaddress']->staff_name;
    $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email =>'personal'
    );

         // );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);


    $this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form'); 

    redirect('Provident_fund_nomination_form_staff/edit/'.$staff_id.'/'.$insertid);    
  }

}

$submitdatasend = $this->input->post('submitbtn');

if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {


  $dadate = $this->input->post('dadate');

  $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);

  $m_date = date("Y-m-d");

  $insertarraydata = array(
   'candidate_id'      => 0,
   'staff_id'          =>$staff_id,
   'place'              => $this->input->post('daplace'),
   'date'               => $dadate,
   'status'             => 1,
   'type'               =>'m',
   'm_date'             =>$m_date

 );

  $this->db->insert('provident_fund_nomination', $insertarraydata);
  $insertid = $this->db->insert_id();
  $data1=$this->input->post('data');

  foreach($data1 as $key => $value)
   {
    if ($value['name'] != '') {
   $m_date = date("Y-m-d");
   $arr=array('sr_no'=>$key,
    'name'=>$value['name'],
    'provident_id'=>$insertid,
    'relation_id'=>$value['relationship_nominee'],
    'age'=>$value['age_nominee'],
    'address'=>$value['address_nominee'],
    'share_nominee'=>$value['share_nominee'],
    'minior'=>$value['minior'],
    'type'=>'m',
    'm_date'=>$m_date

  );

   $this->db->insert('provident_fund_nomination_details', $arr);
 }
}


 $insertid = $this->db->insert_id();

 $data1=$this->input->post('data');
 $insert_data =array(
  'type'=>23,
  'r_id'=> $insertid,
  'sender'=> $staff_id,
  'receiver'=>$content['report']->reportingto,
  'senddate'=>date('Y-m-d'),
  'createdon'=>date('Y-m-d H:i:s'),
  'createdby'=>$this->loginData->UserID,


  'flag'=>4,
  'staffid'=>$staff_id
);
 $this->db->insert('tbl_workflowdetail', $insert_data);






 $this->db->trans_complete();

 if ($this->db->trans_status() === FALSE){

  $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

}else{

 $subject = ': Provident Fund Nomination';
 $body = '<h4>'.$content['candidatedetailwithaddress']->staff_name.' submited Provident fund Nomination Form </h4><br />';
 $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
 <tr>
 <td width="96">Name </td>
 <td width="404">'.$content['candidatedetailwithaddress']->staff_name.'</td>
 </tr>
 <tr>
 <td>Employ Code</td>
 <td> '.$content['candidatedetailwithaddress']->emp_code.'</td>
 </tr>
 <tr>
 <td>Designation</td>
 <td>' .$content['candidatedetailwithaddress']->desiname.'</td>
 </tr>
 <tr>
 <td>Office</td>
 <td>'.$content['candidatedetailwithaddress']->officename.'</td>
 </tr>
 </table>';
 $body .= "<br /> <br />";
 $body .= "Regards <br />";
 $body .= " ". $content['candidatedetailwithaddress']->staff_name ."<br>";
 $body .= " ". $content['candidatedetailwithaddress']->desiname."<br>";
 $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
 $to_useremail = $content['candidatedetailwithaddress']->emailid;
 $tcemailid=$content['tc_email']->emailid;


 $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
 $arr=array($tcemailid=>'tc',
  $personal_email=>'personal');

         // );

 $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
         // die("hello");

 $this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form'); 

 redirect('Provident_fund_nomination_form_staff/view/'.$staff_id.'/'.$insertid);    
}
}

}

// $content['topbar'] = $this->Provident_fund_nomination_form_staff_model->do_flag($staff_id);


// $var=$content['topbar']->provident_flag;

// if ($var==null)
//   {
//     goto preview;

//   }


//   if($var==0)
//   {
//     redirect('/Provident_fund_nomination_form/edit/'.$staff.'/'.$candidateid);
//   }
//   elseif($var==1)
//   {
//     redirect('/Provident_fund_nomination_form/view/'.$staff.'/'.$candidateid);
//   }


preview:
$content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();


if(isset($content['candidatedetailwithaddress']))
{
$content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);

}
else 
{
  $this->session->set_flashdata('er_msg', 'Error !!! Candidate communication Address are not found');
}
//print_r($content['candidatedetailwithaddress']);
//die;

 // print_r($content['candidatedetailwithaddress']);


 // print_r($content['candidatedetailwithaddress']);

    //$content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
    //$content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);

   // $content['witnessdeclaration'] = $this->General_nomination_and_authorisation_form_model->getWitnessDeclaration($candidateid);
$content['office_name'] = $this->General_nomination_and_authorisation_form_model->office_name();



$content['title'] = 'Provident_fund_nomination_form_staff';

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('_main_layout', $content);

 
}
public function edit($staff=null,$insertid=null)
{
 $this->session->set_userdata('staff', $staff);

 $staff_id=$this->loginData->staffid;


 if(isset($staff_id))
 {

  $staff_id=$staff_id;


}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;

        // echo "candidateid".$candidateid;

}




$RequestMethod = $this->input->server('REQUEST_METHOD');

if($RequestMethod == "POST"){

  $savesenddata = $this->input->post('savebtn');

  if (!empty($savesenddata) && $savesenddata =='senddatasave') {

    $dadate = $this->input->post('date3');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



$updatearraydata='';
    $updatearraydata = array(

     'candidate_id'   => 0,
     'staff_id'=>$staff_id,

     'place' => $this->input->post('daplace'),


     'date'               => $dadate,
     'status'                => 0


   );

   // print_r($updatearraydata);
   // die;
    $this->db->where('id', $insertid);
    $this->db->update('provident_fund_nomination', $updatearraydata);
    //echo $this->db->last_query();
    //die;
      //$countnominee = count($this->input->post('full_name_nominee'));


    $data1=$this->input->post('data');
    // echo "<pre>";
    // print_r($data1);
    //die;
    foreach($data1 as $key => $value)
   {
    $id =  $value['provident_id']; 
    $this->db->where('provident_id', $id);
      $this->db->delete('provident_fund_nomination_details');
   }

  
    foreach($data1 as $key => $value)
    {
      if ($value['full_name_nominee'] != '') {
        //echo "hhh";
      $arr=array(
        'sr_no'=>$key,
        'name'=>$value['full_name_nominee'],
        'provident_id'=>$value['provident_id'],
        'relation_id'=>$value['relationship_nominee'],
        'age'=>$value['age_nominee'],
        'address'=>$value['address_nominee'],
        'share_nominee'=>$value['share_nominee'],
        'minior'=>$value['minior']

      );
      //  echo "<pre>";
      //   print_r($arr);  
       // extract($arr);
      // echo $id;
      
     
     // $this->db->update('provident_fund_nomination_details', $arr);
     // $this->db->where('id',$p_id);

      // $this->db->where('id',$id);
            $this->db->insert('provident_fund_nomination_details',$arr);
    // echo $this->db->last_query();


    }
  }
    // echo "<pre>";
    // print_r($arr);
      //   die;
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

    }else{

      $this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form'); 

    }
    redirect('Provident_fund_nomination_form_staff/edit/'.$staff_id.'/'.$insertid);   
  }


  $submitsenddata = $this->input->post('submitbtn');

  if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

// print_r($this->input->post()); die();


    $dadate = $this->input->post('date3');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);


     $updatearraydata='';

    $updatearraydata = array(

     'candidate_id'             => 0,
     'staff_id'=>$staff_id,

     'place'              => $this->input->post('daplace'),
     'signature'   => '',

     'date'               => $dadate,
     'status'                => 1


   );
    $this->db->where('id', $insertid);
    $this->db->update('provident_fund_nomination', $updatearraydata);
     // echo $this->db->last_query();
      //die;
      //$countnominee = count($this->input->post('full_name_nominee'));


    $data1=$this->input->post('data');
    //print_r($data1);
    //die;
    foreach($data1 as $key => $value)
   {
    $id =  $value['provident_id']; 
    $this->db->where('provident_id', $id);
      $this->db->delete('provident_fund_nomination_details');
   }

    foreach($data1 as $key => $value)
    {
      if ($value['full_name_nominee'] != '') {

        $arr=array(
        'sr_no'         => $key,
        'name'=>$value['full_name_nominee'],
        'provident_id'=>$value['provident_id'],
        'relation_id'=>$value['relationship_nominee'],
        'age'=>$value['age_nominee'],
        'address'=>$value['address_nominee'],
        'share_nominee'=>$value['share_nominee'],
        'minior'=>$value['minior']

      );
      //  echo "<pre>";
      //   print_r($arr);  
       // extract($arr);
     // echo $id;
      
     
     // $this->db->update('provident_fund_nomination_details', $arr);
     // $this->db->where('id',$p_id);

      // $this->db->where('id',$id);
            $this->db->insert('provident_fund_nomination_details',$arr);
          }
    }
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

    }

      /*else{

        // $hrdemailid    = 'poonamadlekha@pradan.net';
         $hrdemailid    = 'amit.kum2008@gmail.com';
         $tcemailid     = $this->loginData->EmailID;

         $subject = "Submit Gereral Nomination And Authorisation Form ";
          $body =   'Dear Sir, <br><br> ';
          $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
         //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
         $to_email = $hrdemailid;
         $to_name = $tcemailid;
        
         $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/

         $this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form'); 

         redirect('Provident_fund_nomination_form_staff/view/'.$staff_id.'/'.$insertid);    



       }


     }
     $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();
     $content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);
    //print_r($content['candidatedetailwithaddress']);

     $content['genenominformdetail'] = $this->General_nomination_and_authorisation_form_model->getGeneralnominationform($staff_id);


     $content['nominee'] =$this->Provident_fund_nomination_form_staff_model->get_pfinformation($insertid);
   

     $content['count_nominee'] =$this->Provident_fund_nomination_form_staff_model->count_pfinformation($insertid);
   

     $content['nomineedetail'] = $this->Provident_fund_nomination_form_staff_model->getNomineedetail($insertid);



     $content['title'] = 'Provident_fund_nomination_form_staff/edit';

     $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

     $this->load->view('_main_layout', $content);
   }



   public function view($staff=null,$inseted=null)
   {
        $staff = $this->uri->segment(3);
                       // $joining_staff=$staff;

    $staff_id=$this->loginData->staffid;
    $login_staff=$this->loginData->staffid;
    $candidateid=$this->loginData->candidateid;



    if(isset($staff_id))
    {

      $staff_id=$staff_id;


    }
    else
    {
     $staff_id=$staff;
         //echo "staff".$staff_id;

       //echo "candidateid".$candidateid;

   }

  

     $content['nominee'] =$this->Provident_fund_nomination_form_staff_model->get_pfinformation($inseted);
     //print_r($content['nominee']);
   

     $content['count_nominee'] =$this->Provident_fund_nomination_form_staff_model->count_pfinformation($inseted);
   

     $content['nomineedetail'] = $this->Provident_fund_nomination_form_staff_model->getNomineedetail($inseted);
   // print_r($content['nomineedetail']);
   $content['id'] = $this->Provident_fund_nomination_form_model->get_providentworkflowid($login_staff);


   $content['staff_details'] = $this->Provident_fund_nomination_form_model->staffName($staff_id,$login_staff);

   $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();

   $content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);
   //print_r($content['candidatedetailwithaddress']);
   
   
   
    // $content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
    // $content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);
   $content['personal']=$this->Common_Model->get_Personnal_Staff_List();

   if($this->loginData->RoleID==2)
   {
     $RequestMethod = $this->input->server('REQUEST_METHOD'); 


     if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
      $status=$this->input->post('status');
      $reject=$this->input->post('reject');
      $p_status=$this->input->post('p_status');
      $wdtaff1=$this->input->post('wdtaff1');
      $wdtaff2=$this->input->post('wdtaff2');
      $r_id=$this->input->post('id');

      $check = $this->session->userdata('insert_id');



      $insert_data =array(
        'type'=>23,
        'r_id'=> $r_id,
        'sender'=> $login_staff,
        'receiver'=>$p_status,
        'senddate'=>date('Y-m-d'),
        'createdon'=>date('Y-m-d H:i:s'),
        'createdby'=>$login_staff,
        'scomments'=>$reject,
        'forwarded_workflowid'=>$content['id']->workflow_id,
        'flag'=> $status,
        'staff_id'=>$staff_id
      );

      $arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 



      $this->db->insert('tbl_workflowdetail', $insert_data);

      $this->db->where('id',$r_id);
      $this->db->update('provident_fund_nomination',$arr_data);



    }


  }

  else if($this->loginData->RoleID==17)
  {
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
    $status=$this->input->post('status');
    $reject=$this->input->post('reject');
    $p_status=$this->input->post('p_status');
    $wdtaff1=$this->input->post('wdtaff1');
    $wdtaff2=$this->input->post('wdtaff2');
    $r_id=$this->input->post('id');

    $check = $this->session->userdata('insert_id');



    $insert_data =array(
      'type'=>23,
      'r_id'=> $r_id,
      'sender'=> $login_staff,
      'receiver'=>$staff_id,
      'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
      'createdby'=>$login_staff,
      'scomments'=>$reject,
      'forwarded_workflowid'=>$content['id']->workflow_id,
      'flag'=> $status,
      'staff_id'=>$staff_id
    );

    $arr_data=array(
      'personal_date'=>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('personal_date')),
      'personal_place'=>$this->input->post('personal_place'),
      'personal_name'=>$login_staff,

    ); 



    $this->db->insert('tbl_workflowdetail', $insert_data);

    $this->db->where('id',$r_id);
    $this->db->update('provident_fund_nomination',$arr_data);


  }


}






$content['title'] = 'Provident_fund_nomination_form_staff/view';

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('_main_layout', $content);
}





public function Add($staff=null,$candidate_id=null)
{


  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;

   $candidateid=$candidate_id;


 }


        // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){

  $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]');
  $this->form_validation->set_rules('status','Status','trim|required');

  if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }


  $insertArr = array(
    'pgname'      => $this->input->post('pgname'),
    'status'      => $this->input->post('status')
  );

  $this->db->insert('mstpgeducation', $insertArr);

  $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
  $this->session->set_flashdata('er_msg', $this->db->error());
  redirect('/Employee_particular_form/index/');
}

prepareview:


$content['subview'] = 'Employee_particular_form/add';
$this->load->view('_main_layout', $content);
}

}