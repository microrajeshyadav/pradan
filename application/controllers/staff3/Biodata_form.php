<?php 

/**
* Candidate Full Information controller
*/
class Biodata_form extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		$this->load->model("Biodata_form_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	

		$check = $this->session->userdata('loginData');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('loginData');

	}

	public function index()
	{

		try{
			//print_r($this->loginData);

			$this->load->model('Biodata_form_model','model');

			$RequestMethod = $this->input->server('REQUEST_METHOD');  

			if($RequestMethod == 'POST'){

				$CampusType = $this->input->post('campustype');

				if(isset($CampusType) && $CampusType=='off' && $this->input->post('SaveDatasend')=='SaveData'){
				

					$this->db->trans_start();
					$this->db->trans_strict(FALSE);

					// echo "<pre>";
					// print_r($this->input->post()); die;

					
					$updateArrCandidate = array(
						'bloodgroup'        => $this->input->post('bloodgroup'),
						'updatedon'      	=> date('Y-m-d H:i:s'),
						'updatedby'      	=> $this->loginData->candidateid, // login user id
						'isdeleted'      	=> 0, 
					);

				//print_r($updateArrCandidate); die;

					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateArrCandidate);
				//echo $this->db->last_query(); die;
					//echo  $countfamilymember = count($_FILES['familymemberphoto']['name']); 

					
				

					$this->db->delete('tbl_family_members',array('candidateid'=> $this->loginData->candidateid));



						// $countfamilymember = count($this->input->post('familymembername')); 

	 $countfamilymember = count($this->input->post('familymembername'));

	for ($i=0; $i < $countfamilymember; $i++) { 

		$familydob = $this->input->post('familymemberdob')[$i];
		$familydobformat = $this->model->changedatedbformate($familydob);

	
		

		$insertFamilyMember	 = array(

			'candidateid'          => $this->loginData->candidateid,
			'Familymembername'     => $this->input->post('familymembername')[$i],
			'relationwithemployee'  => $this->input->post('relationwithenployee')[$i],
			'familydob'             => $familydobformat,
			'createdon'      	    => date('Y-m-d H:i:s'),
			'createdby'      	    => $this->loginData->candidateid, // 
			'isdeleted'      	    => 0, 
			);

         
		$this->db->insert('tbl_family_members', $insertFamilyMember);
    
	}



    // echo $this->db->last_query();


				

					//echo $countidentityname;

		 	//$this->db->delete('tbl_identity_details',array('candidateid'=> $this->loginData->candidateid));

$this->db->delete('tbl_identity_details',array('candidateid'=> $this->loginData->candidateid));

$countidentityname = count($this->input->post('identityname'));

for ($j=0; $j < $countidentityname; $j++) { 

		

$insertIdentityDetails	 = array(
	'candidateid'         =>$this->loginData->candidateid,
	'identityname'        =>$this->input->post('identityname')[$j],
	'identitynumber'      =>$this->input->post('identitynumber')[$j],
	'createdon'           => date('Y-m-d H:i:s'),
    'createdby'           => $this->loginData->candidateid, // 
    'isdeleted'           => 0, 

    );
		$this->db->insert('tbl_identity_details', $insertIdentityDetails);

}


	 	/// End Step I ///////


		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 


		  

		 
		   $this->db->trans_complete();

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   }else{

		   	$updateStatusInfo	 = array(

		   		'BDFFormStatus'  => '99',
		   	);

		   	$this->db->where('candidateid',$this->loginData->candidateid);
		   	$this->db->update('tbl_candidate_registration', $updateStatusInfo);

		   	//echo $this->db->last_query(); die;

		   	$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	

		   	redirect('candidate/Biodata_form/preview');
		   	exit;

		   }

		}
elseif(isset($CampusType) && $CampusType=='on' && $this->input->post('SaveDatasend')=='SaveData'){//// If END //// 
 			// echo "<pre>";

 			//print_r($this->input->post());
 			// print_r($_FILES);

 			// die;

			   	//// Step I ////////////////
	$OriginalFamilyMemberPhotoName='';
	$OriginalName = '';
	$this->db->trans_off();
	$this->db->trans_start();
	$this->db->trans_strict(FALSE);

	

	$updateArrCandidate = array(
		'bloodgroup'				=> $this->input->post('bloodgroup'),
		
		'updatedon'      	  => date('Y-m-d H:i:s'),
		'updatedby'      	  => $this->loginData->candidateid, // login user id
		'isdeleted'      	  => 0, 
			);

				//print_r($updateArrCandidate); die;

	$this->db->where('candidateid', $this->loginData->candidateid);

	$this->db->update('tbl_candidate_registration', $updateArrCandidate);
			//echo $this->db->last_query(); die;


	$countfamilymember = count($this->input->post('familymembername'));


	$this->db->delete('tbl_family_members',array('candidateid'=> $this->loginData->candidateid));

	for ($i=0; $i < $countfamilymember ; $i++) { 


		//print_r($encryptedFamilyMemberNameImage);


		$familymemberdob     = $this->input->post('familymemberdob')[$i];
		$familymemberdobnew  = $this->model->changedatedbformate($familymemberdob);


		$insertFamilyMember	 = array(

			'candidateid'           => $this->loginData->candidateid,
			'Familymembername'      => $this->input->post('familymembername')[$i],
			'relationwithemployee'  => $this->input->post("relationwithenployee")[$i],
			'familydob'             => $familymemberdobnew,
			
			'updateon'      	      => date('Y-m-d H:i:s'),
					'updateby'      	      => $this->loginData->candidateid, // 
					'isdeleted'      	      => 0, 

				);

				// print_r($insertFamilyMember); die;

		$this->db->insert('tbl_family_members', $insertFamilyMember);
		 		//echo $this->db->last_query(); 

	}


	$countidentityname = count($this->input->post('identityname'));

	$this->db->delete('tbl_identity_details',array('candidateid'=> $this->loginData->candidateid));

	for ($i=0; $i < $countidentityname; $i++) { 

		$insertIdentityDetails	 = array(
			'candidateid'        => $this->loginData->candidateid,
			'identityname'       => $this->input->post('identityname')[$i],
			'identitynumber'     => $this->input->post("identitynumber")[$i],
			
			'updateon'      	  => date('Y-m-d H:i:s'),
		    'updatedby'      	  => $this->loginData->candidateid, // login user id
		    'isdeleted'          => 0, 

				);
		$this->db->insert('tbl_identity_details', $insertIdentityDetails);
		 	//echo $this->db->last_query();
	}


		 	/// End Step I ///////

	

///////// Gap Year Save Start Here ///////////// 


	if (!empty($this->input->post('gapfromdate')[0])) {

		$countgapfromdate = count($this->input->post('gapfromdate'));

		$this->db->delete('tbl_gap_year',array('candidateid'=> $this->loginData->candidateid));

		for ($j=0; $j < $countgapfromdate; $j++) { 
			$gapfromdate    = $this->input->post('gapfromdate')[$j];
			$gapfromdate1   = $this->model->changedatedbformate($gapfromdate);
			$gaptodate      = $this->input->post('gaptodate')[$j];
			$gaptodate1     = $this->model->changedatedbformate($gaptodate);

			$insertGapyear	 = array(
				'candidateid'      => $this->loginData->candidateid,
				'fromdate'         => $gapfromdate1,
				'todate'           => $gaptodate1,
				'reason'           => $this->input->post('gapreason')[$j],
				'createdon'        => date('Y-m-d H:i:s'),
				'createdby'        => $this->loginData->candidateid,
				'isdeleted'        => 0, 
			);

			  //print_r($insertGapyear); die;

			$this->db->insert('tbl_gap_year', $insertGapyear);
		//echo $this->db->last_query(); //die; 	

		}
	}

	///////// Gap Year Save End Here ///////////// 


///////// Training Exposure Save Start Here ///////////// 

 			//print_r($this->input->post('natureoftraining'));

	if (!empty($this->input->post('natureoftraining')[0])) {

 			//echo sizeof($this->input->post('natureoftraining')); 
		$countnatureoftraining = count($this->input->post('natureoftraining'));
//die;
//print_r($this->input->post('natureoftraining')); die;
 			// echo "dsfdsfds";
    //echo  $countnatureoftraining; die;



		$this->db->delete('tbl_training_exposure',array('candidateid'=> $this->loginData->candidateid));

		for ($j=0; $j < $countnatureoftraining; $j++) { 
		 		//echo $j; die;
		 		//$this->input->post('natureoftraining')[$j];

			$fromdate     = $this->input->post('fromdate')[$j];
			$fromdate1  = $this->model->changedatedbformate($fromdate);
			$todate     = $this->input->post('todate')[$j];
			$todate1  = $this->model->changedatedbformate($todate);

			$insertTrainingExposure	 = array(
				'candidateid'      => $this->loginData->candidateid,
				'natureoftraining' => $this->input->post('natureoftraining')[$j],
				'organizing_agency'=> $this->input->post('organizingagency')[$j],
				'fromdate'         => $fromdate1,
				'todate'           => $todate1,
				'updatedon'        => date('Y-m-d H:i:s'),
					'updatedby'        => $this->loginData->candidateid, // 
					'isdeleted'        => 0, 
				);

			  //print_r($insertTrainingExposure); die;

			$this->db->insert('tbl_training_exposure', $insertTrainingExposure);
		//echo $this->db->last_query(); //die; 	

		}
	}

	///////// Training Exposure Save End Here ///////////// 

	///////// Language Skill/Proficiency Save start here ///////////// 

	$countsyslanguage = count($this->input->post('syslanguage')); 


	$this->db->delete('tbl_language_proficiency',array('candidateid'=> $this->loginData->candidateid));

	for ($i=0; $i < $countsyslanguage; $i++) { 

		$insertLanguageProficiency = array(
			'candidateid'  => $this->loginData->candidateid,
			'languageid'   => $this->input->post("syslanguage")[$i],
			'lang_speak'   => $this->input->post("speak")[$i],
			'lang_read'    => $this->input->post("read")[$i],
			'lang_write'   => $this->input->post("write")[$i],
			'updatedon'    => date('Y-m-d H:i:s'),
				    'updatedby'    => $this->loginData->candidateid, // 
				    'isdeleted'    => 0, 
				);

			  //print_r($insertLanguageProficiency); 

		$this->db->insert('tbl_language_proficiency', $insertLanguageProficiency);
				//echo $this->db->last_query(); 

	}
				///////// Language Skill/Proficiency Save End here  ///////////// 

	$this->db->where('candidateid', $this->loginData->candidateid);
	$result = $this->db->get('tbl_other_information');

	$sql = 'SELECT * FROM `tbl_other_information` Where `tbl_other_information`.candidateid ='.$this->loginData->candidateid.''; 
	$result1 = $this->db->query($sql)->result();

	if ($result->num_rows() > 0){

		$otherid = $result1[0]->id;
		$updateOtherInfo	 = array(

			'candidateid'             => $this->loginData->candidateid,
			'any_subject_of_interest' => trim($this->input->post('subjectinterest')),
			'any_achievementa_awards' => trim($this->input->post("achievementawards")),
			'createdon'               => date('Y-m-d H:i:s'),
				    'createdby'               => $this->loginData->candidateid, // 
				    'isdeleted'               => 0, 
				);

		$this->db->where('id', $otherid);
		$this->db->update('tbl_other_information', $updateOtherInfo);
		//echo $this->db->last_query();
	}
	else{

		$insertOtherInfo	 = array(

			'candidateid'             => $this->loginData->candidateid,
			'any_subject_of_interest' => trim($this->input->post('subjectinterest')),
			'any_achievementa_awards' => trim($this->input->post("achievementawards")),
			'createdon'               => date('Y-m-d H:i:s'),
				  'createdby'               => $this->loginData->candidateid, // 
				  'isdeleted'               => 0, 
				);

		$this->db->insert('tbl_other_information', $insertOtherInfo);
		//echo $this->db->last_query();
	}	


		 	/// End Step II //////////

		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 

	if (!empty($this->input->post('orgname')[0])) {

		   $count_orgname = count($this->input->post('orgname'));  // die;



		   $this->db->delete('tbl_work_experience',array('candidateid'=> $this->loginData->candidateid));

		   for ($i=0; $i < $count_orgname; $i++) { 
					//echo $i; die;
	  

		   	$workfromdate     = $this->input->post('work_experience_fromdate')[$i];
		   	$workexpfromdate  = $this->model->changedatedbformate($workfromdate);
		   	$worktodate       = $this->input->post('work_experience_todate')[$i];	
		   	$workexptodate    = $this->model->changedatedbformate($worktodate);

		   	$insertWorkExperience1 = array(
		   		'candidateid'      => $this->loginData->candidateid,
		   		'organizationname' => $this->input->post('orgname')[$i],
		   		'descriptionofassignment'  => trim($this->input->post("descriptionofassignment")[$i]),
		   		'fromdate'                 => $workexpfromdate,
		   		'todate'                   => $workexptodate,
		   		'palceofposting'          => $this->input->post("palceofposting")[$i],
		   		'designation'	          => $this->input->post("designation")[$i],
		   		'lastsalarydrawn'         => $this->input->post("lastsalarydrawn")[$i],
		   		
		   		'updateon'                => date('Y-m-d H:i:s'),
			    'updatedby'               => $this->loginData->candidateid, // 
			);

		   	$this->db->insert('tbl_work_experience', $insertWorkExperience1);
			 // echo 	$this->db->last_query(); die;
		   //	echo $this->db->last_query();

		   }

		}

				///////// Work Experience Save End Here ///////////// 

		$this->db->where('candidateid', $this->loginData->candidateid);
		$result = $this->db->get('tbl_other_information');

		$sql = 'SELECT * FROM `tbl_other_information` Where `tbl_other_information`.candidateid ='.$this->loginData->candidateid.''; 
		$result1 = $this->db->query($sql)->result();

		if ($this->input->post("where") !='') {
			$where = $this->input->post("where");
		}else{
			$where = NULL;
		}

		if ($result->num_rows() > 0){

			$otherid = $result1[0]->id;

			$whendate     = $this->input->post('when');
			$dbformatwhendate  = $this->model->changedatedbformate($whendate);

			$updateOtherInfo	 = array(

				'candidateid'             => $this->loginData->candidateid,
				'any_assignment_of_special_interest' => $this->input->post('any_assignment_of_special_interest'),
				'experience_of_group_social_activities' => $this->input->post("experience_of_group_social_activities"),
				'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("pradan_selection_process_before"),

				'have_you_taken_part_in_pradan_selection_process_before_when'=> $dbformatwhendate,
				'have_you_taken_part_in_pradan_selection_process_before_where'=> $where,

				'annual_income'          => $this->input->post("annual_income"),
				'male_sibling'           => $this->input->post("no_of_male_sibling"),
				'female_sibling'         => $this->input->post("no_of_female_sibling"),
				'know_about_pradan'      => $this->input->post("have_you_come_to_know"),
				'know_about_pradan_other_specify'  => $this->input->post("specify"),
				'createdon'               => date('Y-m-d H:i:s'),
				  'createdby'               => $this->loginData->candidateid, // 
				  'isdeleted'               => 0, 

				);
			$this->db->where('id', $otherid);
			$this->db->update('tbl_other_information', $updateOtherInfo);
		   //	echo $this->db->last_query();

		}
		else{


			$whendate     = $this->input->post('when');
			$dbformatwhendate  = $this->model->changedatedbformate($whendate);

			$insertOtherInfo	 = array(

				'candidateid'             => $this->loginData->candidateid,
				'any_assignment_of_special_interest' => $this->input->post('any_assignment_of_special_interest'),
				'experience_of_group_social_activities' => $this->input->post("experience_of_group_social_activities"),
				'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("pradan_selection_process_before"),

				'have_you_taken_part_in_pradan_selection_process_before_when'=> $dbformatwhendate,
				'have_you_taken_part_in_pradan_selection_process_before_where'=> $where,

				'annual_income'          => $this->input->post("annual_income"),
				'male_sibling'           => $this->input->post("no_of_male_sibling"),
				'female_sibling'         => $this->input->post("no_of_female_sibling"),
				'know_about_pradan'      => $this->input->post("have_you_come_to_know"),
				'know_about_pradan_other_specify'  => $this->input->post("specify"),
				'createdon'              => date('Y-m-d H:i:s'),
				'createdby'              => $this->loginData->candidateid, // 
				'isdeleted'              => 0, 

			);

			$this->db->insert('tbl_other_information', $insertOtherInfo);
		   //	echo $this->db->last_query();
		}	

        //die;

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){

			$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		}else{

			$updateStatusInfo	 = array(

				'BDFStatusaftergd'  => '99',
			);

			$this->db->where('candidateid',$this->loginData->candidateid);
			$this->db->update('tbl_candidate_registration', $updateStatusInfo);
		   	//die;
			$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');
			redirect('candidate/Biodata_form/preview');

		}



		}elseif(isset($CampusType) && $CampusType=='on' && $this->input->post('SubmitDatasend')=='SubmitData'){  	//// Step I ////////////////

				//echo "<pre>";
				//print_r($this->input->post());
				//die;
			$OriginalFamilyMemberPhotoName='';
			$OriginalName = '';

			$this->db->trans_start();

			if ($_FILES['photoupload']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 


				$OriginalName = $_FILES['photoupload']['name']; 
				$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['photoupload']['tmp_name'];
				$targetPath = FCPATH . "datafiles/";
				$targetFile = $targetPath . $encryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$encryptedImage = $encryptedName;
				}else{

					die("Error uploading Profile Photo ");
					$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
					redirect(current_url());
				}

			}else{

				$encryptedImage = $this->input->post('oldphotoupload');

			}

			$updateArrCandidate = array(
				'bloodgroup'				=> $this->input->post('bloodgroup'),
				'originalphotoname'	=> $OriginalName,
				'encryptedphotoname'=> $encryptedImage,
				'updatedon'      	  => date('Y-m-d H:i:s'),
				'updatedby'      	  => $this->loginData->candidateid, // login user id
				'isdeleted'      	  => 0, 
			);

				//print_r($updateArrCandidate); die;

			$this->db->where('candidateid', $this->loginData->candidateid);

			$this->db->update('tbl_candidate_registration', $updateArrCandidate);
				//echo $this->db->last_query(); //die;
			$countfamilymember = count($this->input->post('familymembername'));


			$this->db->delete('tbl_family_members',array('candidateid'=> $this->loginData->candidateid));

			for ($i=0; $i < $countfamilymember ; $i++) { 


				if ($_FILES['familymemberphoto']['name'][$i] != NULL) {


					@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

					$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
					$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
					$targetPath = FCPATH . "datafiles/familymemberphoto/";
					$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$encryptedFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;

					}else{

						die("Error uploading Photo file");
						$this->session->set_flashdata("er_msg", "Error uploading registration image");
						redirect(current_url());
					}
				}
				else{
					$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
					$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
				}

	//print_r($encryptedFamilyMemberNameImage);


				$familymemberdob     = $this->input->post('familymemberdob')[$i];
				$familymemberdobnew  = date('Y-m-d', strtotime($familymemberdob));


				$insertFamilyMember	 = array(

					'candidateid'           => $this->loginData->candidateid,
					'Familymembername'      => $this->input->post('familymembername')[$i],
					'relationwithemployee'  => $this->input->post("relationwithenployee")[$i],
					'familydob'             => $familymemberdobnew,
					'originalphotoname'     => $OriginalFamilyMemberPhotoName,
					'encryptedphotoname'    => $encryptedFamilyMemberNameImage,
					'updateon'      	      => date('Y-m-d H:i:s'),
				  'updateby'      	      => $this->loginData->candidateid, // 
				  'isdeleted'      	      => 0, 

				);

				// print_r($insertFamilyMember); die;

				$this->db->insert('tbl_family_members', $insertFamilyMember);
		 		//echo $this->db->last_query();

			}


			$countidentityname = count($this->input->post('identityname'));

			$this->db->delete('tbl_identity_details',array('candidateid'=> $this->loginData->candidateid));

			for ($i=0; $i < $countidentityname; $i++) { 


				if ($_FILES['identityphoto']['name'][$i] != NULL) {


					@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


					$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
					$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
					$targetPath = FCPATH . "datafiles/identitydocuments/";
					$targetFile = $targetPath . $encryptedIdentityName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$encryptedIdentitydocument = $encryptedIdentityName;
					}else{

						die("Error uploading Identity Document !");
						$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
						redirect(current_url());
					}

				}else{
					$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
					$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
				}


				$insertIdentityDetails	 = array(

					'candidateid'        => $this->loginData->candidateid,
					'identityname'       => $this->input->post('identityname')[$i],
					'identitynumber'     => $this->input->post("identitynumber")[$i],
					'encryptedphotoname' => $encryptedIdentitydocument,
					'originalphotoname'  => $OriginalIdentityName,
					'updateon'      	  => date('Y-m-d H:i:s'),
				    'updatedby'      	  => $this->loginData->candidateid, // login user id
				    'isdeleted'          => 0, 

				);
				$this->db->insert('tbl_identity_details', $insertIdentityDetails);
		 		//echo $this->db->last_query();
			}


		 	/// End Step I ///////

		 	//// Step II ////////
			$pgencryptedImage ='';
			$Otherscertificateencryptedimage ='';


			if ($_FILES['10thcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['10thcertificate']['name']))); 


				$matricoriginalName = $_FILES['10thcertificate']['name']; 
				$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['10thcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $matricencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$matricencryptedImage = $matricencryptedName;
				}else{

					die("Error uploading 10thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$matricoriginalName = $this->input->post('originalmatriccertificate');
				$matricencryptedImage = $this->input->post('oldmatriccertificate');

			}




			if ($_FILES['12thcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['12thcertificate']['name']))); 

				$hscoriginalName = $_FILES['12thcertificate']['name']; 
				$hscencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['12thcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $hscencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$hscencryptedImage = $hscencryptedName;
				}else{

					die("Error uploading 12thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$hscoriginalName = $this->input->post('originalhsccertificate');
				$hscencryptedImage = $this->input->post('oldhsccertificate');

			}


			if ($_FILES['ugcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['ugcertificate']['name']))); 

				$ugoriginalName = $_FILES['ugcertificate']['name']; 
				$ugencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['ugcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $ugencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$ugencryptedImage = $ugencryptedName;
				}else{

					die("Error uploading ugcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
					redirect(current_url());
				}

			}
			else{

				$ugoriginalName = $this->input->post('originalugcertificate');
				$ugencryptedImage = $this->input->post('oldugcertificate');

			}


			if ($_FILES['pgcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['pgcertificate']['name']))); 
				

				$pgriginalName = $_FILES['pgcertificate']['name']; 
				$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['pgcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $pgencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$pgencryptedImage = $pgencryptedName;
				}else{

					die("Error uploading pgcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$pgriginalName = $this->input->post('originalpgcertificate');
				$pgencryptedImage = $this->input->post('oldpgcertificate');

			}



			if ($_FILES['otherscertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


				$OtheroriginalName = $_FILES['otherscertificate']['name']; 
				$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['otherscertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $otherencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$Otherscertificateencryptedimage = $otherencryptedName;
				}else{

					die("Error uploading otherscertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
					redirect(current_url());
				}

			}
			else{
				$OtheroriginalName = $this->input->post('originalotherscertificate');
				$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
			}


			$updateeducationalcert	 = array(
				'encryptmetriccertificate'  => $matricencryptedImage,
				'encrypthsccertificate'     => $hscencryptedImage,
				'encryptugcertificate'      => $ugencryptedImage,
				'encryptpgcertificate'      => $pgencryptedImage,
				'encryptothercertificate'   => $Otherscertificateencryptedimage,
				'originalmetriccertificate'  => $matricencryptedImage,
				'originalhsccertificate'     => $hscencryptedImage,
				'originalugcertificate'      => $ugencryptedImage,
				'originalpgcertificate'      => $pgencryptedImage,
				'originalothercertificate'   => $Otherscertificateencryptedimage,

			);

			// print_r($updateeducationalcert);die;
			$this->db->where('candidateid', $this->loginData->candidateid);

			$this->db->update('tbl_candidate_registration', $updateeducationalcert);

			//echo $this->db->last_query(); //die;


///////// Gap Year Save Start Here ///////////// 


			if (!empty($this->input->post('gapfromdate')[0])) {

				$countgapfromdate = count($this->input->post('gapfromdate'));

				$this->db->delete('tbl_gap_year',array('candidateid'=> $this->loginData->candidateid));

				for ($j=0; $j < $countgapfromdate; $j++) { 
					$gapfromdate    = $this->input->post('gapfromdate')[$j];
					$gapfromdate1   = $this->model->changedatedbformate($gapfromdate);
					$gaptodate      = $this->input->post('gaptodate')[$j];
					$gaptodate1     = $this->model->changedatedbformate($gaptodate);

					$insertGapyear	 = array(
						'candidateid'      => $this->loginData->candidateid,
						'fromdate'         => $gapfromdate1,
						'todate'           => $gaptodate1,
						'reason'           => $this->input->post('gapreason')[$j],
						'createdon'        => date('Y-m-d H:i:s'),
						'createdby'        => $this->loginData->candidateid,
						'isdeleted'        => 0, 
					);

			  //print_r($insertGapyear); die;

					$this->db->insert('tbl_gap_year', $insertGapyear);
		//echo $this->db->last_query(); //die; 	

				}
			}

	///////// Gap Year Save End Here ///////////// 

///////// Training Exposure Save Start Here ///////////// 
			$countnatureoftraining = count($this->input->post('natureoftraining'));
//die;
//print_r($this->input->post('natureoftraining')); die;

			$this->db->delete('tbl_training_exposure',array('candidateid'=> $this->loginData->candidateid));

			for ($j=0; $j < $countnatureoftraining; $j++) { 
		 		//echo $j; die;
		 		//$this->input->post('natureoftraining')[$j];

				$fromdate     = $this->input->post('fromdate')[$j];
				$fromdate1  = date('Y-m-d', strtotime($fromdate));
				$todate     = $this->input->post('todate')[$j];
				$todate1  = date('Y-m-d', strtotime($todate));

				$insertTrainingExposure	 = array(
					'candidateid'      => $this->loginData->candidateid,
					'natureoftraining' => $this->input->post('natureoftraining')[$j],
					'organizing_agency'=> $this->input->post('organizingagency')[$j],
					'fromdate'         => $fromdate1,
					'todate'           => $todate1,
					'updatedon'        => date('Y-m-d H:i:s'),
		 'updatedby'        => $this->loginData->candidateid, // 
		 'isdeleted'        => 0, 
		);

			  //print_r($insertTrainingExposure); die;

				$this->db->insert('tbl_training_exposure', $insertTrainingExposure);
		//echo $this->db->last_query(); //die; 	

			}

	///////// Training Exposure Save End Here ///////////// 

	///////// Language Skill/Proficiency Save start here ///////////// 

			$countsyslanguage = count($this->input->post('syslanguage')); 
			

			$this->db->delete('tbl_language_proficiency',array('candidateid'=> $this->loginData->candidateid));

			for ($i=0; $i < $countsyslanguage; $i++) { 

				$insertLanguageProficiency = array(
					'candidateid'  => $this->loginData->candidateid,
					'languageid'   => $this->input->post("syslanguage")[$i],
					'lang_speak'   => $this->input->post("speak")[$i],
					'lang_read'    => $this->input->post("read")[$i],
					'lang_write'   => $this->input->post("write")[$i],
					'updatedon'    => date('Y-m-d H:i:s'),
				    'updatedby'    => $this->loginData->candidateid, // 
				    'isdeleted'    => 0, 
				);

			  //print_r($insertLanguageProficiency); 

				$this->db->insert('tbl_language_proficiency', $insertLanguageProficiency);
				//echo $this->db->last_query(); 

			}
				///////// Language Skill/Proficiency Save End here  ///////////// 

			$this->db->where('candidateid', $this->loginData->candidateid);
			$result = $this->db->get('tbl_other_information');

			$sql = 'SELECT * FROM `tbl_other_information` Where `tbl_other_information`.candidateid ='.$this->loginData->candidateid.''; 
			$result1 = $this->db->query($sql)->result();

			if ($result->num_rows() > 0){

				$otherid = $result1[0]->id;
				$updateOtherInfo	 = array(

					'candidateid'             => $this->loginData->candidateid,
					'any_subject_of_interest' => trim($this->input->post('subjectinterest')),
					'any_achievementa_awards' => trim($this->input->post("achievementawards")),
					'createdon'               => date('Y-m-d H:i:s'),
				  'createdby'               => $this->loginData->candidateid, // 
				  'isdeleted'               => 0, 
				);

				$this->db->where('id', $otherid);
				$this->db->update('tbl_other_information', $updateOtherInfo);
		 		//echo $this->db->last_query();
			}
			else{

				$insertOtherInfo	 = array(

					'candidateid'             => $this->loginData->candidateid,
					'any_subject_of_interest' => trim($this->input->post('subjectinterest')),
					'any_achievementa_awards' => trim($this->input->post("achievementawards")),
					'createdon'               => date('Y-m-d H:i:s'),
				  'createdby'               => $this->loginData->candidateid, // 
				  'isdeleted'               => 0, 
				);

				$this->db->insert('tbl_other_information', $insertOtherInfo);
		 		//echo $this->db->last_query();
			}	

		 	/// End Step II //////////

		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 


		   $count_orgname = count($this->input->post('orgname'));  // die;

		   $this->db->delete('tbl_work_experience',array('candidateid'=> $this->loginData->candidateid));

		   for ($i=0; $i < $count_orgname; $i++) { 
					//echo $i; die;
	   
		   	$workfromdate     = $this->input->post('work_experience_fromdate')[$i];
		   	$workexpfromdate  = date('Y-m-d', strtotime($workfromdate));
		   	$worktodate       = $this->input->post('work_experience_todate')[$i];	
		   	$workexptodate    = date('Y-m-d', strtotime($worktodate));

		   	$insertWorkExperience1 = array(
		   		'candidateid'      => $this->loginData->candidateid,
		   		'organizationname' => $this->input->post('orgname')[$i],
		   		'descriptionofassignment'=> trim($this->input->post("descriptionofassignment")[$i]),
		   		'fromdate'         => $workexpfromdate,
		   		'todate'           => $workexptodate,
		   		'palceofposting'   => $this->input->post("palceofposting")[$i],
		   		'designation'	  => $this->input->post("designation")[$i],
		   		'lastsalarydrawn' => $this->input->post("lastsalarydrawn")[$i],
		   		'originaldocumentname' => $OriginalexperiencedocumentName,
		   		'encrypteddocumnetname' => $encrypteddocument,
		   		'updateon'              => date('Y-m-d H:i:s'),
			    'updatedby'             => $this->loginData->candidateid, // 
			);

		   	$this->db->insert('tbl_work_experience', $insertWorkExperience1);
			 // echo 	$this->db->last_query(); die;
		   //	echo $this->db->last_query();

		   }


				///////// Work Experience Save End Here ///////////// 

		   $this->db->where('candidateid', $this->loginData->candidateid);
		   $result = $this->db->get('tbl_other_information');

		   $sql = 'SELECT * FROM `tbl_other_information` Where `tbl_other_information`.candidateid ='.$this->loginData->candidateid.''; 
		   $result1 = $this->db->query($sql)->result();

		   if ($result->num_rows() > 0){

		   	$otherid = $result1[0]->id;
		   	$updateOtherInfo	 = array(

		   		'candidateid'             => $this->loginData->candidateid,
		   		'any_assignment_of_special_interest' => $this->input->post('any_assignment_of_special_interest'),
		   		'experience_of_group_social_activities' => $this->input->post("experience_of_group_social_activities"),
		   		'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("pradan_selection_process_before"),

		   		'have_you_taken_part_in_pradan_selection_process_before_when'=> $dbformatwhendate,
		   		'have_you_taken_part_in_pradan_selection_process_before_where'=> $this->input->post("where"),

		   		'annual_income'          => $this->input->post("annual_income"),
		   		'male_sibling'           => $this->input->post("no_of_male_sibling"),
		   		'female_sibling'         => $this->input->post("no_of_female_sibling"),
		   		'know_about_pradan'      => $this->input->post("have_you_come_to_know"),
		   		'know_about_pradan_other_specify'  => $this->input->post("specify"),
		   		'createdon'               => date('Y-m-d H:i:s'),
				  'createdby'               => $this->loginData->candidateid, // 
				  'isdeleted'               => 0, 

				);
		   	$this->db->where('id', $otherid);
		   	$this->db->update('tbl_other_information', $updateOtherInfo);
		   	//echo $this->db->last_query();

		   }
		   else{

		   	$insertOtherInfo	 = array(

		   		'candidateid'             => $this->loginData->candidateid,
		   		'any_assignment_of_special_interest' => $this->input->post('any_assignment_of_special_interest'),
		   		'experience_of_group_social_activities' => $this->input->post("experience_of_group_social_activities"),
		   		'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("pradan_selection_process_before"),

		   		'have_you_taken_part_in_pradan_selection_process_before_when'=> $dbformatwhendate,
		   		'have_you_taken_part_in_pradan_selection_process_before_where'=> $this->input->post("where"),

		   		'annual_income'          => $this->input->post("annual_income"),
		   		'male_sibling'           => $this->input->post("no_of_male_sibling"),
		   		'female_sibling'         => $this->input->post("no_of_female_sibling"),
		   		'know_about_pradan'      => $this->input->post("have_you_come_to_know"),
		   		'know_about_pradan_other_specify'  => $this->input->post("specify"),
		   		'createdon'               => date('Y-m-d H:i:s'),
				  'createdby'               => $this->loginData->candidateid, // 
				  'isdeleted'               => 0, 

				);

		   	$this->db->insert('tbl_other_information', $insertOtherInfo);
		   //	echo $this->db->last_query();
		   }	
		   $this->db->trans_complete();



		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   }else{

		   	$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');

		   }

		}
	}

	$fcount = $this->model->getCountFamilyMember($this->loginData->candidateid);

	$content['familycount']= $fcount;  

	$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetails($this->loginData->candidateid);

	$content['candidatedetails']         = $this->model->getCandidateDetails($this->loginData->candidateid);

	$Icount = $this->model->getCountIdentityNumber($this->loginData->candidateid);

	$content['identitycount']= $Icount;  

	$content['identitydetals'] = $this->model->getCandidateIdentityDetails($this->loginData->candidateid);


	$TEcount = $this->model->getCountTrainingExposure($this->loginData->candidateid);

	$content['TrainingExpcount']= $TEcount;  

	$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($this->loginData->candidateid);


	@ $Lcount = $this->model->getCountLanguage($this->loginData->candidateid);

	@ $content['languageproficiency']= $Lcount;  

	@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($this->loginData->candidateid);
	@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($this->loginData->candidateid);


	$WEcount = $this->model->getCountWorkExprience($this->loginData->candidateid);

	$content['WorkExperience']= $WEcount;  

	$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($this->loginData->candidateid);


	$GPYcount = $this->model->getCountGapYear($this->loginData->candidateid);

	$content['GapYearCount']= $GPYcount;  

	$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($this->loginData->candidateid);



	$content['statedetails']           = $this->model->getState();
	$content['ugeducationdetails']     = $this->model->getUgEducation();
	$content['pgeducationdetails']     = $this->model->getPgEducation();
	$content['campusdetails']          = $this->model->getCampus();
	$content['syslanguage']            = $this->model->getSysLanguage();
	$content['sysrelations']           = $this->model->getSysRelations();
	$content['sysidentity']            = $this->model->getSysIdentity();
	$content['getdistrict']            = $this->model->getDistrict();

	$content['getjoiningstatus'] = $this->model->getCandidateJoiningStatus($this->loginData->candidateid);
	$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
	$content['getgeneralform']   = $this->model->getGeneralFormStatus($this->loginData->candidateid);
	$content['getbdfformstatus']   = $this->model->getBdfFormStatus($this->loginData->candidateid);

 //print_r($getgeneralform); die;

	$content['title'] = 'Biodata_form';

	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

	$this->load->view('candidate/_main_layout', $content);

}catch (Exception $e) {
	print_r($e->getMessage());die;
}
}




public function preview()
{

	try{


		$fcount = $this->model->getCountFamilyMember($this->loginData->candidateid);

		$content['familycount']= $fcount;  

		$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetailsPrint($this->loginData->candidateid);

		$content['candidatedetails']         = $this->model->getCandidateDetailsPrint($this->loginData->candidateid);

		$Icount = $this->model->getCountIdentityNumber($this->loginData->candidateid);

		$content['identitycount']= $Icount;  

		$content['identitydetals'] = $this->model->getCandidateIdentityDetailsPrint($this->loginData->candidateid);


		$TEcount = $this->model->getCountTrainingExposure($this->loginData->candidateid);

		$content['TrainingExpcount']= $TEcount;  

		$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($this->loginData->candidateid);


		$GPYcount = $this->model->getCountGapYear($this->loginData->candidateid);

		$content['GapYearCount']= $GPYcount;  

		$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($this->loginData->candidateid);



		@ $Lcount = $this->model->getCountLanguage($this->loginData->candidateid);

		@ $content['languageproficiency']= $Lcount;  

		@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($this->loginData->candidateid);
		$content['languagedetalsprint'] = $this->model->getCandidateLanguageDetailsPrint($this->loginData->candidateid);
		@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($this->loginData->candidateid);


		$WEcount = $this->model->getCountWorkExprience($this->loginData->candidateid);

		$content['WorkExperience']= $WEcount;  

		$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($this->loginData->candidateid);

		$content['title'] = 'Biodata_form';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}




public function bdfformsubmit()
{

	try{

		$subject = "Candidates Submitted BDF Form ";
		$body = read_file(base_url().'mailtext/Candidates_Fill_BDF_Formtext'); 

		$candidatedetails = $this->model->getCandidateDetails($this->loginData->candidateid);
		 	//print_r($candidatedetails); die;
		$candidateemail  = $candidatedetails->emailid;

		 	   //$candiadteemailid = $this->input->post('emailid');
			 	$to_email1     = $candidateemail; //// Team Mail Id ////
			 	$to_email2     = 'poonamadlekha@pradan.net'; /// HRD Mail Id ////
			 	$to_email3     = 'utpalmodak@pradan.net';
		 		//$to_candidate  = $result1->emailid; ///// Candidate Email Id ////
			 	$sendmail1 = $this->Common_Model->send_email($subject, $body, $to_email1,$to_email2);

			 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email3);

			 	if($sendmail==true){

			 		$updateStatusInfo	 = array(
			 			'BDFStatusaftergd' 				  => '1',
			 		);

			 		$this->db->where('candidateid',$this->loginData->candidateid);
			 		$this->db->update('tbl_candidate_registration', $updateStatusInfo);
			 		$this->session->set_flashdata('tr_msg', 'Successfully Fill Candidate Registration');	
			 	}

			 	$content['title'] = 'Biodata_form';
			 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 	$this->load->view('candidate/_main_layout', $content);


			 }catch (Exception $e) {
			 	print_r($e->getMessage());die;
			 }
			}


		}