<?php 

/**
* General Nomination And Authorisation Form controller
*/
class General_nomination_and_authorisation_form extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		$this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	

		$check = $this->session->userdata('loginData');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('loginData');

	}


	public function index()
	{
	   
	    //echo "<pre>";

	    

	 // 	print_r($this->input->post()); die;	
		// $this->db->trans_start();
		
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){


			$dadate = $this->input->post('dadate');

		    $dadate = $this->model->changedatedbformate($dadate);

			$Sendsavebtn = $this->input->post('savebtn');

			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

			$insertarraydata = array(

				'candidateid'  						=> $this->loginData->candidateid,
				'executive_director_place'  		=> $this->input->post('executive_director_place'),
				'da_place'  						=> $this->input->post('daplace'),
				'nomination_authorisation_signed'   => $this->input->post('nomination_authorisation_signed'),
				'first_witness_name'  				=> $this->input->post('firstwitnessname'),
				'first_witness_address'  			=> $this->input->post('firstwitnessaddress'),
				'second_witness_name'  				=> $this->input->post('secondwitnessname'),
				'second_witness_address'  			=> $this->input->post('secondwitnessaddress'),
				'da_date'  							=> $dadate,
				'status'  							=> 0,
				'createdon'  						=> date('Y-m-d H:i:s'), 
				'createdby'  						=> $this->loginData->candidateid,
				
			);

			$this->db->insert('tbl_general_nomination_and_authorisation_form', $insertarraydata);
			$insertid = $this->db->insert_id();


			$countnominee = count($this->input->post('full_name_nominee'));
			

			for ($i=0; $i < $countnominee ; $i++) { 

				$insertarraynomineedata = array(
					'general_nomination_id' => $insertid,
					'nominee_name' => $this->input->post('full_name_nominee')[$i],
					'nominee_relation' => $this->input->post('relationship_nominee')[$i],
					'nominee_age' => $this->input->post('age_nominee')[$i],
					'nominee_address' => $this->input->post('address_nominee')[$i],
				);

		     $this->db->insert('tbl_nominee_details', $insertarraynomineedata);

			}

//echo $this->db->last_query(); die;
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	

		redirect('/candidate/General_nomination_and_authorisation_form/edit/'.$insertid);		
			}

		}

			$submitdatasend = $this->input->post('submitbtn');

		if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {

			$dadate = $this->input->post('dadate');

		    $dadate = $this->model->changedatedbformate($dadate);

			$insertarraydata = array(
				'candidateid'  => $this->loginData->candidateid,
				'executive_director_place'  => $this->input->post('executive_director_place'),
				'da_place'  => $this->input->post('daplace'),
				'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
				'first_witness_name'  => $this->input->post('firstwitnessname'),
				'first_witness_address'  => $this->input->post('firstwitnessaddress'),
				'second_witness_name'  => $this->input->post('secondwitnessname'),
				'second_witness_address'  => $this->input->post('secondwitnessaddress'),
				'da_date'  => $dadate,
				'status'  => 1,
				'createdon'  => date('Y-m-d H:i:s'), 
				'createdby'  => $this->loginData->candidateid,
			);

			$this->db->insert('tbl_general_nomination_and_authorisation_form', $insertarraydata);
			$insertid = $this->db->insert_id();


			$countnominee = count($this->input->post('full_name_nominee'));

			for ($i=0; $i < $countnominee ; $i++) { 

				$insertarraynomineedata = array(
					'general_nomination_id' => $insertid,
					'nominee_name' => $this->input->post('full_name_nominee')[$i],
					'nominee_relation' => $this->input->post('relationship_nominee')[$i],
					'nominee_age' => $this->input->post('age_nominee')[$i],
					'nominee_address' => $this->input->post('address_nominee')[$i],
				);

				$this->db->insert('tbl_nominee_details', $insertarraynomineedata);

			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
				$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	

				redirect('/candidate/General_nomination_and_authorisation_form/view/');		
			}
	}

}

		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($this->loginData->candidateid);

		$content['getjoiningreport']       = $this->model->getJoiningReport($this->loginData->candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
		
		$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($this->loginData->candidateid);

		$content['title'] = 'General_nomination_and_authorisation_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('candidate/_main_layout', $content);
	}



	public function edit($token)
	{
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST"){

			$savesenddata = $this->input->post('savebtn');

			if (!empty($savesenddata) && $savesenddata =='senddatasave') {

				$dadate = $this->input->post('dadate');

		       $dadate = $this->model->changedatedbformate($dadate);


			$updatearraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				'executive_director_place'  => $this->input->post('executive_director_place'),
				'da_place'  => $this->input->post('daplace'),
				'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
				'first_witness_name'  => $this->input->post('firstwitnessname'),
				'first_witness_address'  => $this->input->post('firstwitnessaddress'),
				'second_witness_name'  => $this->input->post('secondwitnessname'),
				'second_witness_address'  => $this->input->post('secondwitnessaddress'),
				'da_date'  => $dadate,
				'status'  => 0,
				'updatedon'  => date('Y-m-d H:i:s'), 
				
			);
			$this->db->where('id', $token);
			$this->db->update('tbl_general_nomination_and_authorisation_form', $updatearraydata);
			
			$countnominee = count($this->input->post('full_name_nominee'));


		$this->db->delete('tbl_nominee_details',array('general_nomination_id' => $token));

			for ($i=0; $i < $countnominee ; $i++) { 

				$insertarraynomineedata = array(
					'general_nomination_id' => $token,
					'nominee_name' => $this->input->post('full_name_nominee')[$i],
					'nominee_relation' => $this->input->post('relationship_nominee')[$i],
					'nominee_age' => $this->input->post('age_nominee')[$i],
					'nominee_address' => $this->input->post('address_nominee')[$i],
				);

		$this->db->insert('tbl_nominee_details', $insertarraynomineedata);

			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
		$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	
		
			}
			redirect('/candidate/General_nomination_and_authorisation_form/edit/'.$token);		
			}

		
			$submitsenddata = $this->input->post('submitbtn');

		if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

			$dadate = $this->input->post('dadate');

		       $dadate = $this->model->changedatedbformate($dadate);

		$updatearraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				'executive_director_place'  => $this->input->post('executive_director_place'),
				'da_place'  => $this->input->post('daplace'),
				'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
				'first_witness_name'  => $this->input->post('firstwitnessname'),
				'first_witness_address'  => $this->input->post('firstwitnessaddress'),
				'second_witness_name'  => $this->input->post('secondwitnessname'),
				'second_witness_address'  => $this->input->post('secondwitnessaddress'),
				'da_date'  => $dadate,
				'status'  => 1,
				'updatedon'  => date('Y-m-d H:i:s'), 
				
			);
			$this->db->where('id', $token);
			$this->db->update('tbl_general_nomination_and_authorisation_form', $updatearraydata);


			$countnominee = count($this->input->post('full_name_nominee'));

				$this->db->delete('tbl_nominee_details',array('general_nomination_id' => $token));


			for ($i=0; $i < $countnominee ; $i++) { 

				$insertarraynomineedata = array(
					'general_nomination_id' => $token,
					'nominee_name' => $this->input->post('full_name_nominee')[$i],
					'nominee_relation' => $this->input->post('relationship_nominee')[$i],
					'nominee_age' => $this->input->post('age_nominee')[$i],
					'nominee_address' => $this->input->post('address_nominee')[$i],
				);

				$this->db->insert('tbl_nominee_details', $insertarraynomineedata);

			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{

				// $hrdemailid    = 'poonamadlekha@pradan.net';
				 $hrdemailid    = 'amit.kum2008@gmail.com';
				 $tcemailid     = $this->loginData->EmailID;

				 $subject = "Submit Gereral Nomination And Authorisation Form ";
				  $body = 	'Dear Sir, <br><br> ';
				  $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 $to_email = $hrdemailid;
				 $to_name = $tcemailid;
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);
				
				$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	

				redirect('/candidate/General_nomination_and_authorisation_form/view/');		
			}


	}


	}
		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($this->loginData->candidateid);

		$content['genenominformdetail'] = $this->model->getGeneralnominationform($token);

        $content['countnominee'] =$this->model->getCountNominee($token);
		$content['nomineedetail'] = $this->model->getNomineedetail($token);

		$content['getjoiningreport']       = $this->model->getJoiningReport($this->loginData->candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
		$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($this->loginData->candidateid);


		$content['title'] = 'General_nomination_and_authorisation_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('candidate/_main_layout', $content);
	}



	public function view()
	{
		//print_r($this->loginData);

		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($this->loginData->candidateid);
		$content['genenominformdetail'] = $this->model->getViewGeneralnominationform($this->loginData->candidateid);

		$getGeneralnomination = $this->model->getViewGeneralnominationform($this->loginData->candidateid);
		
		$content['nomineedetail'] = $this->model->getNomineedetail($getGeneralnomination->id);
		$content['getjoiningreport']       = $this->model->getJoiningReport($this->loginData->candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);


		$content['title'] = 'General_nomination_and_authorisation_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('candidate/_main_layout', $content);
	}



}