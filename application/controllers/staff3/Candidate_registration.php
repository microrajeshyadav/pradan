<?php 

/**
* Candidate Registration controller
*/
class Candidate_registration extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	

	}


	public function index()
	{

		try{

			$content['statedetails']           = $this->model->getState();
			$content['ugeducationdetails']     = $this->model->getUgEducation();
			$content['pgeducationdetails']     = $this->model->getPgEducation();
			$content['campusdetails']          = $this->model->getCampus();
			$content['syslanguage']            = $this->model->getSysLanguage();
			$content['sysrelations']           = $this->model->getSysRelations();
			$content['sysidentity']            = $this->model->getSysIdentity();

			$content['getjoiningreport']       = $this->model->getJoiningReport($this->loginData->candidateid);
			$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);

			$content['title'] = 'Candidate_registration';

			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

			$this->load->view('candidate/_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function add($token)
	{
		try{

		 	$token_decoded_id=base64_decode(urldecode($token));  /// Decode Token Id 
		 	$expcampid = explode('/',$token_decoded_id);
		 	$content['campusdecodeid'] = $expcampid[0];
		 	$content['campuscatid'] = $expcampid[1];
		 	$RequestMethod = $this->input->server('REQUEST_METHOD');

		 	if($RequestMethod == 'POST'){


		 	    $emailid = $this->input->post("emailid");
		 		$emailidExist = $this->is_exists($emailid);
		 		if($emailidExist > 0){

		 			$this->session->set_flashdata('er_msg', 'email id allready exist, please choose other email id ');
		 			redirect(current_url());

		 		}else{




 			$dateofbirth1       = $this->input->post('dateofbirth');
 			$date_of_birth      = $this->model->changedatedbformate($dateofbirth1);
 			$candidateemailid   = $this->input->post("emailid");
 			$candidatefirstname = $this->input->post('candidatefirstname');

 			$this->db->trans_start();


 			    $this->form_validation->set_rules('candidatefirstname','Candidate First Name','trim|required');
		    	$this->form_validation->set_rules('candidatefirstname','Candidate Last Name','trim|required');
		    	$this->form_validation->set_rules('emailid','Emailid','trim|required|valid_email');

		    	$this->form_validation->set_rules('motherfirstname','Mother First Name','trim|required');
		    	$this->form_validation->set_rules('motherlastname','Mother Last Name','trim|required');
		    	$this->form_validation->set_rules('fatherfirstname','Father First Name','trim|required');
		    	$this->form_validation->set_rules('fatherlastname','Father First Name','trim|required');
		    	$this->form_validation->set_rules('dateofbirth','Date Of Birth','trim|required');
		    	$this->form_validation->set_rules('mobile','Mobile','trim|required|max_length[10]|numeric');

		    	$this->form_validation->set_rules('10thschoolcollegeinstitute','School/ College/ Institute','trim|required');
		    	$this->form_validation->set_rules('10thboarduniversity','Board/ University','trim|required');
		    	$this->form_validation->set_rules('10thpassingyear','Year','trim|required|min_length[4]|max_length[4]|numeric');
		    	$this->form_validation->set_rules('10thplace','Place','trim|required');
		    	$this->form_validation->set_rules('10thspecialisation','Specialisation ','trim|required');
		    	$this->form_validation->set_rules('10thpercentage','Percentage','trim|required|min_length[2]|max_length[6]|numeric');

		    	$this->form_validation->set_rules('12thschoolcollegeinstitute','School/ College/ Institute','trim|required');
		    	$this->form_validation->set_rules('12thboarduniversity','Board/ University','trim|required');
		    	$this->form_validation->set_rules('12thpassingyear','Year','trim|required|min_length[4]|max_length[4]|numeric');
		    	$this->form_validation->set_rules('12thplace','Place','trim|required');
		    	$this->form_validation->set_rules('12thspecialisation','Specialisation ','trim|required');
            //$this->form_validation->set_rules('hscstream ','Stream ','trim|required|min_length[2]|max_length[50]');
		    	$this->form_validation->set_rules('12thpercentage','Percentage','trim|required|min_length[2]|max_length[6]|numeric');

		    	$this->form_validation->set_rules('ugschoolcollegeinstitute','School/ College/ Institute','trim|required');
		    	$this->form_validation->set_rules('ugboarduniversity','Board/ University','trim|required');
		    	$this->form_validation->set_rules('ugpassingyear','Year','trim|required|min_length[4]|max_length[4]|numeric');
		    	$this->form_validation->set_rules('ugplace','Place','trim|required');
		    	$this->form_validation->set_rules('ugspecialisation','Specialisation ','trim|required');
           // $this->form_validation->set_rules('ugdegree','Under Graduate','trim|required');
		    	$this->form_validation->set_rules('ugpercentage','Percentage','trim|required|min_length[2]|max_length[6]|numeric');

		    	if($this->form_validation->run() == FALSE){
		    		$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
		    			'</div>');

		    		$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

		    		$hasValidationErrors    =    true;
		    		goto prepareview;

		    	}


		 			$insertArrCandidate = array(
		 				'candidatefirstname'       => $this->input->post('candidatefirstname'),
		 				'candidatemiddlename'       => $this->input->post("candidatemiddlename"),
		 				'candidatelastname'  	    => $this->input->post('candidatelastname'),
		 				'motherfirstname '    	    => $this->input->post('motherfirstname'),
		 				'mothermiddlename'          => $this->input->post("mothermiddlename"),
		 				'motherlastname'  	        => $this->input->post('motherlastname'),
		 				'fatherfirstname '    	    => $this->input->post('fatherfirstname'),
		 				'fathermiddlename'          => $this->input->post("fathermiddlename"),
		 				'fatherlastname'  	        => $this->input->post('fatherlastname'),
		 				'gender'    		        => $this->input->post('gender'),
		 				'nationality'       	    => $this->input->post("nationality"),
		 				'maritalstatus'  	        => $this->input->post('maritalstatus'),
		 				'dateofbirth '    	        => $date_of_birth,
		 				'emailid'       	        => $this->input->post("emailid"),
		 				'mobile'  		            => $this->input->post('mobile'),
		 				'bloodgroup'                => $this->input->post('bloodgroup'),
		 				'metricschoolcollege '      => $this->input->post('10thschoolcollegeinstitute'),
		 				'metricboarduniversity'     => $this->input->post("10thboarduniversity"),
		 				'metricpassingyear'  	    =>  $this->input->post('10thpassingyear')==''?0: $this->input->post('10thpassingyear'),
		 				'metricplace '    			=> $this->input->post('10thplace'),
		 				'metricspecialisation'      => $this->input->post("10thspecialisation"),
		 				'metricpercentage'  		=> $this->input->post('10thpercentage')==''?0: $this->input->post('10thpercentage') ,
		 				'hscschoolcollege '    		    => $this->input->post('12thschoolcollegeinstitute'),
		 				'hscboarduniversity'       	    => $this->input->post("12thboarduniversity"),
		 				'hscpassingyear'  			    => $this->input->post('12thpassingyear')==''?0: $this->input->post('12thpassingyear'),
		 				'hscplace '    				    => $this->input->post('12thplace'),
		 				'hscspecialisation'       		=> $this->input->post("12thspecialisation"),
		 				'hscpercentage'  			    => $this->input->post('12thpercentage')==''?0: $this->input->post('12thpercentage'),
		 				'ugschoolcollege '    			=> $this->input->post('ugschoolcollegeinstitute'),
		 				'ugboarduniversity'       		=> $this->input->post("ugboarduniversity"),
		 				'ugpassingyear'  			    => $this->input->post('ugpassingyear')==''?0:$this->input->post('ugpassingyear'),
		 				'ugplace '    					=> $this->input->post('ugplace'),
		 				'ugspecialisation'       		=> $this->input->post("ugspecialisation"),
		 				'ugpercentage'  			=> $this->input->post('ugpercentage')==''?0:$this->input->post('ugpercentage'),
		 				'pgschoolcollege '    			=> $this->input->post('pgschoolcollegeinstitute'),
		 				'pgboarduniversity'       		=> $this->input->post("pgboarduniversity"),
		 				'pgpassingyear'  			    =>$this->input->post("pgpassingyear")==''?0:$this->input->post("pgpassingyear"),
		 				'pgplace '    					=> $this->input->post('pgplace'),
		 				'pgspecialisation'       		=> $this->input->post("pgspecialisation"),
		 				'pgpercentage'  			    => $this->input->post("pgpercentage")==''?0:$this->input->post("pgpercentage"),
		 				'otherschoolcollege '    		=> $this->input->post('otherschoolcollegeinstitute'),
		 				'otherboarduniversity'       	        => $this->input->post("otherboarduniversity"),
		 				'otherpassingyear'  			=> $this->input->post("otherpassingyear")==''?0:$this->input->post("otherpassingyear"),
		 				'otherplace '    			     => $this->input->post('otherplace'),
		 				'otherspecialisation'            => $this->input->post("otherspecialisation"),
		 				'otherpercentage'  			     => $this->input->post("otherpercentage")==''?0:$this->input->post("otherpercentage"),
		 				'hscstream'  			        => $this->input->post('hscstream'),
		 				'ugdegree'  			        => $this->input->post('ugdegree'),
		 				'pgdegree'  			        => $this->input->post('pgdegree'),
		 				'campusid'  				    => $this->input->post('campusid'),
		 				'bloodgroup'                    => $this->input->post('bloodgroup'),
		 				'campustype'                    => 'off',
		 				'BDFFormStatus'                 => '99',
		 				'categoryid' 				    => $this->input->post('categoryid'),
		 				'createdon'      	    		=> date('Y-m-d H:i:s'),
		 				'isdeleted'      	    		=> 0, 

		 			);

	$this->db->insert('tbl_candidate_registration', $insertArrCandidate);
			//echo $this->db->last_query(); die;
	$candreginsert = $this->db->insert_id();

	$insertCommunicationAddress	 = array(
		'candidateid'    	    => $candreginsert,
		'presentstreet'    	    => $this->input->post('presentstreet'),
		'presentcity'       	=> $this->input->post("presentcity"),
		'presentstateid'  	    => $this->input->post('presentstateid'),
		'presentdistrict '    	=> $this->input->post('presentdistrict'),
		'presentpincode'       	=> $this->input->post("presentpincode"),
		'permanentstreet'    	=> $this->input->post('permanentstreet'),
		'permanentcity'       	=> $this->input->post("permanentcity"),
		'permanentstateid'  	=> $this->input->post('permanentstateid'),
		'permanentdistrict '    => $this->input->post('permanentdistrict'),
		'permanentpincode'      => $this->input->post("permanentpincode"),
		'createdon'      	    => date('Y-m-d H:i:s'),
		'isdeleted'      	    => 0, 
	);

	$this->db->insert('tbl_candidate_communication_address', $insertCommunicationAddress);

///////// Training Exposure Save Start Here ///////////// 
		$countnatureoftraining = count($this->input->post('natureoftraining'));

		$this->db->delete('tbl_training_exposure',array('candidateid'=> $candreginsert));

		for ($j=0; $j < $countnatureoftraining; $j++) { 
			if (!empty($this->input->post('natureoftraining')[$j])) {
			
			$fromdate1 = $this->input->post('fromdate')[$j];
			$fromdate  = $this->model->changedatedbformate($fromdate1); //die;
			$todate1   = $this->input->post('todate')[$j];	
			$todate    = $this->model->changedatedbformate($todate1);

			$insertTrainingExposure	 = array(
				'candidateid'      => $candreginsert,
				'natureoftraining' => $this->input->post('natureoftraining')[$j],
				'organizing_agency'=> $this->input->post('organizingagency')[$j],
				'fromdate'         => $fromdate,
				'todate'           => $todate,
				'createdon'        => date('Y-m-d H:i:s'),
				'isdeleted'        => 0, 
			);

					  //print_r($insertTrainingExposure); die;

			$this->db->insert('tbl_training_exposure', $insertTrainingExposure);
				//echo $this->db->last_query(); die; 	
		}
		}

			///////// Language Skill/Proficiency Save start here ///////////// 

		$countsyslanguage = count($this->input->post('syslanguage')); 


		$this->db->delete('tbl_language_proficiency',array('candidateid'=> $candreginsert));

		for ($i=0; $i < $countsyslanguage; $i++) { 

			$insertLanguageProficiency = array(
				'candidateid'  => $candreginsert,
				'languageid'   => $this->input->post("syslanguage")[$i],
				'lang_speak'   => $this->input->post("speak")[$i],
				'lang_read'    => $this->input->post("read")[$i],
				'lang_write'   => $this->input->post("write")[$i],
				'createdon'    => date('Y-m-d H:i:s'),
				'isdeleted'    => 0, 
			);

							  //print_r($insertLanguageProficiency); 

			$this->db->insert('tbl_language_proficiency', $insertLanguageProficiency);
						//echo $this->db->last_query(); 

		}
						///////// Language Skill/Proficiency Save End here  


	///////// Training Exposure Save Start Here ///////////// 

		$count_orgname = count($this->input->post('orgname'));  // die;

		$this->db->delete('tbl_work_experience',array('candidateid'=> $candreginsert));

		for ($i=0; $i < $count_orgname; $i++) { 

		if (!empty($this->input->post('orgname')[$i])) {
			# code...
		
		
	   	$workfromdate     = $this->input->post('work_experience_fromdate')[$i];
	   	$workexpfromdate  = $this->model->changedatedbformate($workfromdate);
	   	$worktodate       = $this->input->post('work_experience_todate')[$i];	
	   	$workexptodate    =$this->model->changedatedbformate($worktodate);


	   	$insertWorkExperience1 = array(
	   		'candidateid'      => $candreginsert,
	   		'organizationname' => $this->input->post('orgname')[$i],
	   		'descriptionofassignment'=> $this->input->post("descriptionofassignment")[$i],
	   		'fromdate'         => $workexpfromdate,
	   		'todate'           => $workexptodate,
	   		'palceofposting'   => $this->input->post("palceofposting")[$i],
	   		'createdon'        => date('Y-m-d H:i:s'),
	   	);

		   	$this->db->insert('tbl_work_experience', $insertWorkExperience1);
		   }
	}


	 $insertotherinformation = array(
		   	'candidateid'      => $candreginsert,
		   	'any_subject_of_interest' => trim($this->input->post('subjectinterest')),
		   	'any_achievementa_awards'=> trim($this->input->post("achievementawards")),
		   	'any_assignment_of_special_interest'         => $this->input->post("any_assignment_of_special_interest"),
		   	'experience_of_group_social_activities' => $this->input->post("experience_of_group_social_activities"),
		   	'have_you_taken_part_in_pradan_selection_process_before'   => $this->input->post("selection_process_befor"),
		   	'have_you_taken_part_in_pradan_selection_process_before_details'   => $this->input->post("have_you_taken_part_in_pradan_selection_process_before_details"),
		   	'createdon'        => date('Y-m-d H:i:s'),
		   	'isdeleted' => 0,
		   );

		   $this->db->insert('tbl_other_information', $insertotherinformation);

		 	/////////////// save record ///////////////////
		

		   $this->db->trans_complete();

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   	redirect(current_url());
		   		
		   }else{
		   	
		   		
		   		  $this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');

		   		    $campusid = $this->input->post('campusid'); 
				    $cat_id = $this->input->post('categoryid');
				    $completeurl = $campusid.'/'.$cat_id.'/'.$candreginsert; 
				  
					$enstr=base64_encode($completeurl); //
		            $url_to_be_send=urlencode($enstr);

		   		//redirect('candidate/Candidate_registration/message');
		   		redirect('candidate/Candidate_registration/preview/'.$url_to_be_send);

		  	 }	 	
	 	}
	}

		prepareview:


		$content['statedetails']           = $this->model->getState();
		$content['ugeducationdetails']     = $this->model->getUgEducation();
		$content['pgeducationdetails']     = $this->model->getPgEducation();
		$content['campusdetails']          = $this->model->getCampus();
		$content['syslanguage']            = $this->model->getSysLanguage();
		$content['sysrelations']           = $this->model->getSysRelations();
		$content['sysidentity']            = $this->model->getSysIdentity();

		$content['method'] = $this->router->fetch_method();
		$content['title'] = 'Candidate_registration';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}


}



public function preview($token=NULL)
{

	try{

	
			$token_decoded_id=base64_decode(urldecode($token));  /// Decode Token Id 
		 	$expcampid = explode('/',$token_decoded_id);
		 	//print_r($expcampid); die;
		 	
		 	$content['token'] = $token;
		 	$content['campusdecodeid'] = $expcampid[0];
		 	$content['campuscatid'] = $expcampid[1];
		 	$candidateid = $expcampid[2];

			$content['candidatedetails']         = $this->model->getCandidateDetailsPreview($candidateid);

			 $TEcount = $this->model->getCountTrainingExposure($candidateid);

			 $content['TrainingExpcount']= $TEcount;  

			 $content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($candidateid);


			 @ $Lcount = $this->model->getCountLanguage($candidateid);

			 @ $content['languageproficiency']= $Lcount;  

			 @ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($candidateid);
			 @  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($candidateid);


			 $WEcount = $this->model->getCountWorkExprience($candidateid);

			 $content['WorkExperience']= $WEcount;  

			 $content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($candidateid);


			 $content['statedetails']           = $this->model->getState();
			// $content['ugeducationdetails']     = $this->model->getUgEducation();
			// $content['pgeducationdetails']     = $this->model->getPgEducation();
			// $content['campusdetails']          = $this->model->getCampus();
			// $content['syslanguage']            = $this->model->getSysLanguage();
			// $content['sysrelations']           = $this->model->getSysRelations();
			// $content['sysidentity']            = $this->model->getSysIdentity();

			 $content['title'] = 'Candidate_registration';
			 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 $this->load->view('candidate/_main_layout', $content);


			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
}

public function bdfformsubmit($token = NULL)
{

	try{


		    $token_decoded_id=base64_decode(urldecode($token));  /// Decode Token Id 
		 	$expcampid = explode('/',$token_decoded_id);
		 	$content['campusdecodeid'] = $expcampid[0];
		 	$content['campuscatid'] = $expcampid[1];
		 	$candidateid = $expcampid[2]; 
		 	$candidatedetails = $this->model->getCandidateDetails($candidateid);
		 	//print_r($candidatedetails); die;
			$candidateemail  = $candidatedetails->emailid;
			$subject = "Candidates Submitted BDF Form ";
			$body = read_file(base_url().'mailtext/Candidates_Fill_BDF_Formtext'); 
			
		 	   //$candiadteemailid = $this->input->post('emailid');
			 	$to_email1     = $candidateemail; //// Team Mail Id ////
			 	$to_name     = 'poonamadlekha@pradan.net'; /// HRD Mail Id ////
			 	//$to_email3     = 'utpalmodak@pradan.net';
		 		//$to_candidate  = $result1->emailid; ///// Candidate Email Id ////
			 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email1, $to_name);

			 	// if($sendmail1==1){
			 	// 	$sendmail2 = $this->Common_Model->send_email($subject, $body, $to_email2);
			 	// }
		 	

			 	if($sendmail==true){

			 		$updateStatusInfo	 = array(
			 			'BDFFormStatus' 				  => 'NULL',
			 		);

			 		$this->db->where('candidateid',$candidateid);
			 		$this->db->update('tbl_candidate_registration', $updateStatusInfo);
			 		$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');
			 	}

			

			 $content['title'] = 'Candidatedfullinfo';
			 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 $this->load->view('candidate/_main_layout', $content);
			 

			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
		}



public function edit($token=NULL)
{

	try{
				

		echo $token;

			$token_decoded_id=base64_decode(urldecode($token));  /// Decode Token Id 
		 	$expcampid = explode('/',$token_decoded_id);
		 	//print_r($expcampid); die;
		 	$content['campusdecodeid'] = $expcampid[0];
		 	$content['campuscatid'] = $expcampid[1];
		 	$candidateid = $expcampid[2]; 



		$RequestMethod = $this->input->server('REQUEST_METHOD');

		 	if($RequestMethod == 'POST'){
		 		
		 		// echo "<pre>";
		 		// print_r($this->input->post());
		 		///die;
		 	
		 			$dateofbirth1 = $this->input->post('dateofbirth');
		 			$date_of_birth = $this->model->changedatedbformate($dateofbirth1);

		 			$candidateemailid   = $this->input->post("emailid");
		 			$candidatefirstname = $this->input->post('candidatefirstname');

		 			$this->db->trans_start();

		 			$insertArrCandidate = array(
		 				'candidatefirstname '       => $this->input->post('candidatefirstname'),
		 				'candidatemiddlename'       => $this->input->post("candidatemiddlename"),
		 				'candidatelastname'  	    => $this->input->post('candidatelastname'),
		 				'motherfirstname '    	    => $this->input->post('motherfirstname'),
		 				'mothermiddlename'          => $this->input->post("mothermiddlename"),
		 				'motherlastname'  	        => $this->input->post('motherlastname'),
		 				'fatherfirstname '    	    => $this->input->post('fatherfirstname'),
		 				'fathermiddlename'          => $this->input->post("fathermiddlename"),
		 				'fatherlastname'  	        => $this->input->post('fatherlastname'),
		 				'gender'    		        => $this->input->post('gender'),
		 				'nationality'       	    => $this->input->post("nationality"),
		 				'maritalstatus'  	        => $this->input->post('maritalstatus'),
		 				'dateofbirth '    	        => $date_of_birth,
		 				'emailid'       	        => $this->input->post("emailid"),
		 				'mobile'  		            => $this->input->post('mobile'),
		 				'bloodgroup'                => $this->input->post('bloodgroup'),
		 				'metricschoolcollege '      => $this->input->post('10thschoolcollegeinstitute'),
		 				'metricboarduniversity'     => $this->input->post("10thboarduniversity"),
		 				'metricpassingyear'  	    =>  $this->input->post('10thpassingyear')==''?0: $this->input->post('10thpassingyear'),
		 				'metricplace '    			=> $this->input->post('10thplace'),
		 				'metricspecialisation'      => $this->input->post("10thspecialisation"),
		 				'metricpercentage'  		=> $this->input->post('10thpercentage')==''?0: $this->input->post('10thpercentage') ,
		 				'hscschoolcollege '    		    => $this->input->post('12thschoolcollegeinstitute'),
		 				'hscboarduniversity'       	    => $this->input->post("12thboarduniversity"),
		 				'hscpassingyear'  			    => $this->input->post('12thpassingyear')==''?0: $this->input->post('12thpassingyear'),
		 				'hscplace '    				    => $this->input->post('12thplace'),
		 				'hscspecialisation'       		=> $this->input->post("12thspecialisation"),
		 				'hscpercentage'  			    => $this->input->post('12thpercentage')==''?0: $this->input->post('12thpercentage'),
		 				'ugschoolcollege '    			=> $this->input->post('ugschoolcollegeinstitute'),
		 				'ugboarduniversity'       		=> $this->input->post("ugboarduniversity"),
		 				'ugpassingyear'  			    => $this->input->post('ugpassingyear')==''?0:$this->input->post('ugpassingyear'),
		 				'ugplace '    					=> $this->input->post('ugplace'),
		 				'ugspecialisation'       		=> $this->input->post("ugspecialisation"),
		 				'ugpercentage'  			=> $this->input->post('ugpercentage')==''?0:$this->input->post('ugpercentage'),
		 				'pgschoolcollege '    			=> $this->input->post('pgschoolcollegeinstitute'),
		 				'pgboarduniversity'       		=> $this->input->post("pgboarduniversity"),
		 				'pgpassingyear'  			    =>$this->input->post("pgpassingyear")==''?0:$this->input->post("pgpassingyear"),
		 				'pgplace '    					=> $this->input->post('pgplace'),
		 				'pgspecialisation'       		=> $this->input->post("pgspecialisation"),
		 				'pgpercentage'  			    => $this->input->post("pgpercentage")==''?0:$this->input->post("pgpercentage"),
		 				'otherschoolcollege '    		=> $this->input->post('otherschoolcollegeinstitute'),
		 				'otherboarduniversity'       	        => $this->input->post("otherboarduniversity"),
		 				'otherpassingyear'  			=> $this->input->post("otherpassingyear")==''?0:$this->input->post("otherpassingyear"),
		 				'otherplace '    			     => $this->input->post('otherplace'),
		 				'otherspecialisation'            => $this->input->post("otherspecialisation"),
		 				'otherpercentage'  			     => $this->input->post("otherpercentage")==''?0:$this->input->post("otherpercentage"),
		 				'hscstream'  			        => $this->input->post('hscstream'),
		 				'ugdegree'  			        => $this->input->post('ugdegree'),
		 				'pgdegree'  			        => $this->input->post('pgdegree'),
		 				'campusid'  				    => $this->input->post('campusid'),
		 				'bloodgroup'                    => $this->input->post('bloodgroup'),
		 				'campustype'                    => 'off',
		 				'BDFFormStatus'                 => '99',
		 				'categoryid' 				    => $this->input->post('categoryid'),
		 				'createdon'      	    		=> date('Y-m-d H:i:s'),
		 				'isdeleted'      	    		=> 0, 

		 			);
$this->db->where('candidateid',$candidateid);
$this->db->update('tbl_candidate_registration', $insertArrCandidate);

	
$insertCommunicationAddress	 = array(
	'candidateid'    	    => $candidateid,
	'presentstreet'    	    => $this->input->post('presentstreet'),
	'presentcity'       	=> $this->input->post("presentcity"),
	'presentstateid'  	    => $this->input->post('presentstateid'),
	'presentdistrict '    	=> $this->input->post('presentdistrict'),
	'presentpincode'       	=> $this->input->post("presentpincode"),
	'permanentstreet'    	=> $this->input->post('permanentstreet'),
	'permanentcity'       	=> $this->input->post("permanentcity"),
	'permanentstateid'  	=> $this->input->post('permanentstateid'),
	'permanentdistrict '    => $this->input->post('permanentdistrict'),
	'permanentpincode'      => $this->input->post("permanentpincode"),
	'createdon'      	    => date('Y-m-d H:i:s'),
	'isdeleted'      	    => 0, 
);

$this->db->where('candidateid',$candidateid);
$this->db->update('tbl_candidate_communication_address', $insertCommunicationAddress);
//echo $this->db->last_query(); die;
///////// Training Exposure Save Start Here ///////////// 
 $countnatureoftraining = count($this->input->post('natureoftraining')); 

$this->db->delete('tbl_training_exposure',array('candidateid'=> $candidateid));

for ($j=0; $j < $countnatureoftraining; $j++) { 

	if (!empty($this->input->post('natureoftraining')[$j])) {
	
	
	$fromdate1 = $this->input->post('fromdate')[$j];
	$fromdate  = $this->model->changedatedbformate($fromdate1); //die;
	$todate1   = $this->input->post('todate')[$j];	
	$todate    = $this->model->changedatedbformate($todate1);

	
	

	$insertTrainingExposure	 = array(
		'candidateid'      => $candidateid,
		'natureoftraining' => $this->input->post('natureoftraining')[$j],
		'organizing_agency'=> $this->input->post('organizingagency')[$j],
		'fromdate'         => $fromdate,
		'todate'           => $todate,
		'createdon'        => date('Y-m-d H:i:s'),
		'isdeleted'        => 0, 
	);


	$this->db->insert('tbl_training_exposure', $insertTrainingExposure);
	//echo $this->db->last_query(); die; 

}
}

				///////// Training Exposure Save End Here ///////////// 

				///////// Language Skill/Proficiency Save start here ///////////// 

$countsyslanguage = count($this->input->post('syslanguage')); 


$this->db->delete('tbl_language_proficiency',array('candidateid'=> $candidateid));

for ($i=0; $i < $countsyslanguage; $i++) { 

	$insertLanguageProficiency = array(
		'candidateid'  => $candidateid,
		'languageid'   => $this->input->post("syslanguage")[$i],
		'lang_speak'   => $this->input->post("speak")[$i],
		'lang_read'    => $this->input->post("read")[$i],
		'lang_write'   => $this->input->post("write")[$i],
		'createdon'    => date('Y-m-d H:i:s'),
		'isdeleted'    => 0, 
	);

					  //print_r($insertLanguageProficiency); 

	$this->db->insert('tbl_language_proficiency', $insertLanguageProficiency);
				echo $this->db->last_query(); 

}
				///////// Language Skill/Proficiency Save End here  

		 	///////// Training Exposure Save Start Here ///////////// 

		$count_orgname = count($this->input->post('orgname'));  // die;

		$this->db->delete('tbl_work_experience',array('candidateid'=> $candidateid));

		for ($i=0; $i < $count_orgname; $i++) { 

		if (!empty($this->input->post('orgname')[$i])) {
		
		
	   	$workfromdate     = $this->input->post('work_experience_fromdate')[$i];
	   	$workexpfromdate  = $this->model->changedatedbformate($workfromdate);
	   	$worktodate       = $this->input->post('work_experience_todate')[$i];	
	   	$workexptodate    =$this->model->changedatedbformate($worktodate);


	   	$insertWorkExperience1 = array(
	   		'candidateid'      => $candidateid,
	   		'organizationname' => $this->input->post('orgname')[$i],
	   		'descriptionofassignment'=> $this->input->post("descriptionofassignment")[$i],
	   		'fromdate'         => $workexpfromdate,
	   		'todate'           => $workexptodate,
	   		'palceofposting'   => $this->input->post("palceofposting")[$i],
	   		'createdon'        => date('Y-m-d H:i:s'),
	   	);

		   	$this->db->insert('tbl_work_experience', $insertWorkExperience1);
		  // 	echo $this->db->last_query(); 
	}
}

		   $insertotherinformation = array(
		   	'candidateid'      => $candidateid,
		   	'any_subject_of_interest' => trim($this->input->post('subjectinterest')),
		   	'any_achievementa_awards'=> trim($this->input->post("achievementawards")),
		   	'any_assignment_of_special_interest'         => $this->input->post("any_assignment_of_special_interest"),
		   	'experience_of_group_social_activities' => $this->input->post("experience_of_group_social_activities"),
		   	'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("selection_process_befor"),
		   	'have_you_taken_part_in_pradan_selection_process_before_details' => $this->input->post("have_you_taken_part_in_pradan_selection_process_before_details"),
		   	'createdon' => date('Y-m-d H:i:s'),
		   	'isdeleted' => 0,
		   );

		   $this->db->where('candidateid',$candidateid);
		   $this->db->update('tbl_other_information', $insertotherinformation);
         // echo $this->db->last_query(); die; 
		 	/////////////// save record ///////////////////

		  $this->db->trans_complete();
		  // die;

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   	redirect(current_url());	
		   }else{
		   			   		
		   		$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');

		   		    $campusid = $this->input->post('campusid'); 
				    $cat_id = $this->input->post('categoryid');

				    $completeurl = $campusid.'/'.$cat_id.'/'.$candidateid; 
				  
					$enstr=base64_encode($completeurl); //
		            $url_to_be_send=urlencode($enstr);

		   		//redirect('candidate/Candidate_registration/message');
		   		 redirect('candidate/Candidate_registration/preview/'.$url_to_be_send);

		   

		 	} ///else closed////


	} ///POST closed /////




			$content['candidatedetails']         = $this->model->getCandidateDetailsPreview($candidateid);

			 $TEcount = $this->model->getCountTrainingExposure($candidateid);

			 $content['TrainingExpcount']= $TEcount;  

			 $content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($candidateid);


			 @ $Lcount = $this->model->getCountLanguage($candidateid);

			 @ $content['languageproficiency']= $Lcount;  

			 @ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($candidateid);
			 @ $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($candidateid);


			 $WEcount = $this->model->getCountWorkExprience($candidateid);

			 $content['WorkExperience']= $WEcount;  

			 $content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($candidateid);

			$content['statedetails']           = $this->model->getState();
			$content['ugeducationdetails']     = $this->model->getUgEducation();
			$content['pgeducationdetails']     = $this->model->getPgEducation();
			$content['campusdetails']          = $this->model->getCampus();
			$content['syslanguage']            = $this->model->getSysLanguage();
			$content['sysrelations']           = $this->model->getSysRelations();
			$content['sysidentity']            = $this->model->getSysIdentity();
			$content['getdistrict']           = $this->model->getDistrict();


			 $content['method'] = $this->router->fetch_method();
			 $content['title'] = 'Candidate_registration';
			 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 $this->load->view('candidate/_main_layout', $content);


			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
}



public function message()
{	
	try{

		$content['title'] = 'Candidate_registration';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}




public function view($token)
{


	$fcount = $this->model->getCountFamilyMember($token);

	$content['familycount']= $fcount;  

	$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetails($token);

	$content['candidatedetails']  = $this->model->getCandidateDetails($token);

	$Icount = $this->model->getCountIdentityNumber($token);

	$content['identitycount']= $Icount;  

	$content['identitydetals'] = $this->model->getCandidateIdentityDetails($token);


	$TEcount = $this->model->getCountTrainingExposure($token);

	$content['TrainingExpcount']= $TEcount;  

	$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($token);


	@ $Lcount = $this->model->getCountLanguage($token);

	@ $content['languageproficiency']= $Lcount;  

	@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($token);
	@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($token);


	$WEcount = $this->model->getCountWorkExprience($token);

	$content['WorkExperience']= $WEcount;  

	$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($token);


	$content['statedetails']           = $this->model->getState();
	$content['ugeducationdetails']     = $this->model->getUgEducation();
	$content['pgeducationdetails']     = $this->model->getPgEducation();
	$content['campusdetails']          = $this->model->getCampus();
	$content['syslanguage']            = $this->model->getSysLanguage();
	$content['sysrelations']           = $this->model->getSysRelations();
	$content['sysidentity']            = $this->model->getSysIdentity();

	$content['title'] = 'Candidate_registration';

	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		//echo "<pre>";
		//print_r($content); die;
	$this->load->view('candidate/_main_layout', $content);
		//$this->load->view(__CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__, $content);
}






public function is_exists($email_id)
{	
	try{

		$sql = "SELECT Count(emailid) as count FROM tbl_candidate_registration where emailid='".$email_id."' ";
		$result = $this->db->query($sql)->result();

		return $result[0]->count;



	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


public function getPassword(){

	try{

		// Generating Password
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
		$password = substr( str_shuffle( $chars ), 0, 8 );

		return $password;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



}