<?php 

/**
* Candidate Full Information controller
*/
class Staff_registration extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		$this->load->model("Candidatedetails_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	

		$check = $this->session->userdata('loginData');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('loginData');

	}

	public function index()
	{
        try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');  

			if($RequestMethod == 'POST'){

				$CampusType = $this->input->post('campustype');

				if(isset($CampusType) && $CampusType=='off' && $this->input->post('SaveDatasend')=='SaveData'){
					
					$this->db->trans_start();
					//$this->db->trans_strict(FALSE);
					if ($_FILES['photoupload']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 

						$OriginalName = $_FILES['photoupload']['name']; 
						$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['photoupload']['tmp_name'];
						$targetPath = FCPATH . "datafiles/";
						$targetFile = $targetPath . $encryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedImage = $encryptedName;
						}else{

							die("Error uploading Profile Photo ");
							$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
							redirect(current_url());
						}

					}else{
						$encryptedImage = $this->input->post('oldphotoupload');
						
					}

					$updateArrCandidate = array(
						
						'originalphotoname'	=> $OriginalName,
						'encryptedphotoname'=> $encryptedImage,
						'updatedon'      	=> date('Y-m-d H:i:s'),
						'updatedby'      	=> $this->loginData->candidateid, // login user id
						'isdeleted'      	=> 0, 
					);

					$this->db->where('candidateid', $this->loginData->candidateid);
					$this->db->update('tbl_candidate_registration', $updateArrCandidate);
				//echo $this->db->last_query(); die;
					//echo  $countfamilymember = count($_FILES['familymemberphoto']['name']); 

					 $countfamilymember = count($_FILES['familymemberphoto']['name']);
				

 				$this->db->where('candidateid', $this->loginData->candidateid);
			    $result = $this->db->get('tbl_family_members');

				$sql = 'SELECT * FROM `tbl_family_members` WHERE `candidateid` = '.$this->loginData->candidateid;
			   	$result2 = $this->db->query($sql)->result();

				for ($i=0; $i < $countfamilymember ; $i++) { 

		   		if ($result->num_rows() > 0){

		   		$familyid = $result2[$i]->id;


						if ($_FILES['familymemberphoto']['name'][$i] != NULL) {


							@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

							$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
							$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/familymemberphoto/";
							$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;

							}else{

								die("Error uploading Photo file");
								$this->session->set_flashdata("er_msg", "Error uploading registration image");
								redirect(current_url());
							}
						}
						else{
							$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
							$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
						}


						$insertFamilyMember	 = array(
							'originalphotoname'     => $OriginalFamilyMemberPhotoName,
							'encryptedphotoname'    => $encryptedFamilyMemberNameImage,
							'createdon'      	    => date('Y-m-d H:i:s'),
							'createdby'      	        => $this->loginData->candidateid, // 
							'isdeleted'      	        => 0, 
							);

						$this->db->where('id', $familyid);
						$this->db->update('tbl_family_members', $insertFamilyMember);

					}

					
				}


//echo $this->db->last_query(); die;


				$countidentityname = count($_FILES['identityphoto']['name']);

	            $this->db->where('candidateid', $this->loginData->candidateid);
			    $result = $this->db->get('tbl_identity_details');

				$sql = 'SELECT * FROM `tbl_identity_details` WHERE `candidateid` = '.$this->loginData->candidateid;
			   	$result2 = $this->db->query($sql)->result();

					for ($i=0; $i < $countidentityname; $i++) { 

						if ($result->num_rows() > 0){

		   				$identityid = $result2[$i]->id;

						if ($_FILES['identityphoto']['name'][$i] != NULL) {
							@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


							$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
							$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/identitydocuments/";
							$targetFile = $targetPath . $encryptedIdentityName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedIdentitydocument = $encryptedIdentityName;
							}else{

								die("Error uploading Identity Document !");
								$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
								redirect(current_url());
							}

						}else{
							$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
							$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
						}


						$insertIdentityDetails	 = array(
							'encryptedphotoname'  => $encryptedIdentitydocument,
							'originalphotoname'   => $OriginalIdentityName,
							'createdon'           => date('Y-m-d H:i:s'),
						    'createdby'           => $this->loginData->candidateid, // 
						    'isdeleted'           => 0, 
						    );

						$this->db->where('id',$identityid);
						$this->db->update('tbl_identity_details', $insertIdentityDetails);
						

					}



	 	/// End Step I ///////

		 	//// Step II ////////
					$pgencryptedImage ='';
					$Otherscertificateencryptedimage ='';


					if ($_FILES['10thcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['10thcertificate']['name']))); 


						$matricoriginalName = $_FILES['10thcertificate']['name']; 
						$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['10thcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $matricencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$matricencryptedImage = $matricencryptedName;
						}else{

							die("Error uploading 10thcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
							redirect(current_url());
						}

					}
					else{
						$matricoriginalName = $this->input->post('originalmatriccertificate');
						$matricencryptedImage = $this->input->post('oldmatriccertificate');
					}


					if ($_FILES['12thcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['12thcertificate']['name']))); 

						$hscoriginalName = $_FILES['12thcertificate']['name']; 
						$hscencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['12thcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $hscencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$hscencryptedImage = $hscencryptedName;
						}else{

							die("Error uploading 12thcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
							redirect(current_url());
						}

					}
					else{
						$hscoriginalName = $this->input->post('originalhsccertificate');
						$hscencryptedImage = $this->input->post('oldhsccertificate');
					}


					if ($_FILES['ugcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['ugcertificate']['name']))); 

						$ugoriginalName = $_FILES['ugcertificate']['name']; 
						$ugencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['ugcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $ugencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$ugencryptedImage = $ugencryptedName;
						}else{

							die("Error uploading ugcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
							redirect(current_url());
						}

					}
					else{
						$ugoriginalName = $this->input->post('originalugcertificate');
						$ugencryptedImage = $this->input->post('oldugcertificate');
					}


					if ($_FILES['pgcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['pgcertificate']['name']))); 


						$pgriginalName = $_FILES['pgcertificate']['name']; 
						$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['pgcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $pgencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$pgencryptedImage = $pgencryptedName;
						}else{

							die("Error uploading pgcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
							redirect(current_url());
						}

					}
					else{
						$pgriginalName = $this->input->post('originalpgcertificate');
						$pgencryptedImage = $this->input->post('oldpgcertificate');
					}



					if ($_FILES['otherscertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


						$OtheroriginalName = $_FILES['otherscertificate']['name']; 
						$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['otherscertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $otherencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$Otherscertificateencryptedimage = $otherencryptedName;
						}else{

							die("Error uploading otherscertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
							redirect(current_url());
						}

					}
					else{
						$OtheroriginalName = $this->input->post('originalotherscertificate');
						$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
					}


					$updateeducationalcert	 = array(
						'encryptmetriccertificate'  => $matricencryptedImage,
						'encrypthsccertificate'     => $hscencryptedImage,
						'encryptugcertificate'      => $ugencryptedImage,
						'encryptpgcertificate'      => $pgencryptedImage,
						'encryptothercertificate'   => $Otherscertificateencryptedimage,
						'originalmetriccertificate'  => $matricencryptedImage,
						'originalhsccertificate'     => $hscencryptedImage,
						'originalugcertificate'      => $ugencryptedImage,
						'originalpgcertificate'      => $pgencryptedImage,
						'originalothercertificate'   => $Otherscertificateencryptedimage,
						'updatedby'                  => $this->loginData->candidateid,
						'updatedon'                  => date('Y-m-d H:i:s'),

					);

					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateeducationalcert);

		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 


		   $count_orgname = count($_FILES['experiencedocument']['name']);  // die;


		   $query = $this->db->query('SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid);
		   $numrow = $query->num_rows();

		    $sql = 'SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid;
		   	$result2 = $this->db->query($sql)->result();

		   if($numrow >0){


		   	for ($i=0; $i < $count_orgname; $i++) { 

		   		$workexpid = $result2[$i]->id;	

		   		if ($_FILES['experiencedocument']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


		   			$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
		   			$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptedexpdocumentName;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument = $encryptedexpdocumentName;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
		   			$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
		   		}

		   		$insertWorkExperience1 = array(

		   			'originaldocumentname'  => $OriginalexperiencedocumentName,
		   			'encrypteddocumnetname'  => $encrypteddocument,
		   			'updateon'              => date('Y-m-d H:i:s'),
			        'updatedby'             => $this->loginData->candidateid, // 
			    );

		   		$this->db->where('id', $workexpid);
		   		$this->db->update('tbl_work_experience', $insertWorkExperience1);
			

		   	}

		   }
		   $this->db->trans_complete();

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   }else{

		   	$updateStatusInfo	 = array(

		   		'BDFFormStatus'  => '99',
		   	);

		   	$this->db->where('candidateid',$this->loginData->candidateid);
		   	$this->db->update('tbl_candidate_registration', $updateStatusInfo);

		   	//echo $this->db->last_query(); die;

		   	$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	

		   	redirect('candidate/Candidatedfullinfo/preview');
		   	exit;

		   }

		}
	}
elseif(isset($CampusType) && $CampusType=='on' && $this->input->post('SaveDatasend')=='SaveData'){//// If END //// 
 			// echo "<pre>";

 			//print_r($this->input->post());
 			//print_r($_FILES);
 			
 			

			   	//// Step I ////////////////
	$OriginalFamilyMemberPhotoName='';
	$OriginalName = '';
	//$this->db->trans_off();
	$this->db->trans_start();
	//$this->db->trans_strict(FALSE);

	if ($_FILES['photoupload']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 

		$OriginalName = $_FILES['photoupload']['name']; 
		$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['photoupload']['tmp_name'];
		$targetPath = FCPATH . "datafiles/";
		$targetFile = $targetPath . $encryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);
		

		if($uploadResult == true){
			$encryptedImage = $encryptedName;
		}else{

			die("Error uploading Profile2 Photo ");
			$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
			redirect(current_url());
		}

	}else{

		$encryptedImage = $this->input->post('oldphotoupload');

	}

	$updateArrCandidate = array(
		
		'originalphotoname'	=> $OriginalName,
		'encryptedphotoname'=> $encryptedImage,
		'updatedon'      	  => date('Y-m-d H:i:s'),
		'updatedby'      	  => $this->loginData->candidateid, // login user id
		'isdeleted'      	  => 0, 
			);

				//print_r($updateArrCandidate); die;

	$this->db->where('candidateid', $this->loginData->candidateid);

	$this->db->update('tbl_candidate_registration', $updateArrCandidate);
			//echo $this->db->last_query(); die;


	 $countfamilymember = count($_FILES['familymemberphoto']['name']);

	for ($i=0; $i < $countfamilymember ; $i++) { 

	   $this->db->where('candidateid', $this->loginData->candidateid);
	   $result = $this->db->get('tbl_family_members');

	   $sql = 'SELECT * FROM `tbl_family_members` Where `tbl_family_members`.candidateid ='.$this->loginData->candidateid.''; 
	   $result1 = $this->db->query($sql)->result();

	 //  print_r($result1); die;

	if ($result->num_rows() > 0){

		$familyid = $result1[$i]->id;

		if ($_FILES['familymemberphoto']['name'][$i] != NULL) {

			@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

			$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
			$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
			$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
			$targetPath = FCPATH . "datafiles/familymemberphoto/";
			$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
			$uploadResult = move_uploaded_file($tempFile,$targetFile);

			if($uploadResult == true){
				$encryptedFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;

			}else{

				die("Error uploading Photo file");
				$this->session->set_flashdata("er_msg", "Error uploading registration image");
				redirect(current_url());
			}
		}
		else{
			$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
			$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
		}

	
		$insertFamilyMember	 = array(

			'originalphotoname'       => $OriginalFamilyMemberPhotoName,
			'encryptedphotoname'      => $encryptedFamilyMemberNameImage,
			'updateon'      	      => date('Y-m-d H:i:s'),
			'updateby'      	      => $this->loginData->candidateid, // 
			'isdeleted'      	      => 0, 
		);

				// print_r($insertFamilyMember); die;
		$this->db->where('id', $familyid);
		$this->db->update('tbl_family_members', $insertFamilyMember);
		//echo $this->db->last_query(); 
	}
}


	$countidentityname = count($_FILES['identityphoto']['name']);

	for ($i=0; $i < $countidentityname; $i++) { 

		$this->db->where('candidateid', $this->loginData->candidateid);
	   $result = $this->db->get('tbl_identity_details');

	   $sql = 'SELECT * FROM `tbl_identity_details` Where `tbl_identity_details`.candidateid ='.$this->loginData->candidateid.''; 
	   $result1 = $this->db->query($sql)->result();

	   if ($result->num_rows() > 0){

		$identityid = $result1[$i]->id;

		if ($_FILES['identityphoto']['name'][$i] != NULL) {


			@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


			$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
			$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
			$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
			$targetPath = FCPATH . "datafiles/identitydocuments/";
			$targetFile = $targetPath . $encryptedIdentityName;
			$uploadResult = move_uploaded_file($tempFile,$targetFile);

			if($uploadResult == true){
				$encryptedIdentitydocument = $encryptedIdentityName;
			}else{

				die("Error uploading Identity Document !");
				$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
				redirect(current_url());
			}

		}else{
			$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
			$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
		}


		$insertIdentityDetails	 = array(
			'encryptedphotoname' => $encryptedIdentitydocument,
			'originalphotoname'  => $OriginalIdentityName,
			'updateon'      	  => date('Y-m-d H:i:s'),
		    'updatedby'      	  => $this->loginData->candidateid, // login user id
		    'isdeleted'          => 0, 
		);
		$this->db->where('id', $identityid);
		$this->db->update('tbl_identity_details', $insertIdentityDetails);
		 	//echo $this->db->last_query();
	}
}

		 	/// End Step I ///////

		 	//// Step II ////////
	$pgencryptedImage ='';
	$Otherscertificateencryptedimage ='';


	if ($_FILES['10thcertificate']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['10thcertificate']['name']))); 

		$matricoriginalName = $_FILES['10thcertificate']['name']; 
		$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['10thcertificate']['tmp_name'];
		$targetPath = FCPATH . "datafiles/educationalcertificate/";
		$targetFile = $targetPath . $matricencryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);

		if($uploadResult == true){
			$matricencryptedImage = $matricencryptedName;
		}else{

			die("Error uploading 10thcertificate Document ");
			$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
			redirect(current_url());
		}

	}
	else{
		$matricoriginalName = $this->input->post('originalmatriccertificate');
		$matricencryptedImage = $this->input->post('oldmatriccertificate');

	}


	if ($_FILES['12thcertificate']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['12thcertificate']['name']))); 

		$hscoriginalName = $_FILES['12thcertificate']['name']; 
		$hscencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['12thcertificate']['tmp_name'];
		$targetPath = FCPATH . "datafiles/educationalcertificate/";
		$targetFile = $targetPath . $hscencryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);

		if($uploadResult == true){
			$hscencryptedImage = $hscencryptedName;
		}else{

			die("Error uploading 12thcertificate Document ");
			$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
			redirect(current_url());
		}

	}
	else{
		$hscoriginalName = $this->input->post('originalhsccertificate');
		$hscencryptedImage = $this->input->post('oldhsccertificate');

	}


	if ($_FILES['ugcertificate']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['ugcertificate']['name']))); 

		$ugoriginalName = $_FILES['ugcertificate']['name']; 
		$ugencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['ugcertificate']['tmp_name'];
		$targetPath = FCPATH . "datafiles/educationalcertificate/";
		$targetFile = $targetPath . $ugencryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);

		if($uploadResult == true){
			$ugencryptedImage = $ugencryptedName;
		}else{

			die("Error uploading ugcertificate Document ");
			$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
			redirect(current_url());
		}

	}
	else{

		$ugoriginalName = $this->input->post('originalugcertificate');
		$ugencryptedImage = $this->input->post('oldugcertificate');

	}


	if ($_FILES['pgcertificate']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['pgcertificate']['name']))); 


		$pgriginalName = $_FILES['pgcertificate']['name']; 
		$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['pgcertificate']['tmp_name'];
		$targetPath = FCPATH . "datafiles/educationalcertificate/";
		$targetFile = $targetPath . $pgencryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);

		if($uploadResult == true){
			$pgencryptedImage = $pgencryptedName;
		}else{

			die("Error uploading pgcertificate Document ");
			$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
			redirect(current_url());
		}

	}
	else{
		$pgriginalName = $this->input->post('originalpgcertificate');
		$pgencryptedImage = $this->input->post('oldpgcertificate');

	}



	if ($_FILES['otherscertificate']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


		$OtheroriginalName = $_FILES['otherscertificate']['name']; 
		$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['otherscertificate']['tmp_name'];
		$targetPath = FCPATH . "datafiles/educationalcertificate/";
		$targetFile = $targetPath . $otherencryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);

		if($uploadResult == true){
			$Otherscertificateencryptedimage = $otherencryptedName;
		}else{

			die("Error uploading otherscertificate Document ");
			$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
			redirect(current_url());
		}

	}
	else{
		$OtheroriginalName = $this->input->post('originalotherscertificate');
		$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
	}


	$updateeducationalcert	 = array(
		'encryptmetriccertificate'  => $matricencryptedImage,
		'encrypthsccertificate'     => $hscencryptedImage,
		'encryptugcertificate'      => $ugencryptedImage,
		'encryptpgcertificate'      => $pgencryptedImage,
		'encryptothercertificate'   => $Otherscertificateencryptedimage,
		'originalmetriccertificate'  => $matricencryptedImage,
		'originalhsccertificate'     => $hscencryptedImage,
		'originalugcertificate'      => $ugencryptedImage,
		'originalpgcertificate'      => $pgencryptedImage,
		'originalothercertificate'   => $Otherscertificateencryptedimage,

	);

			// print_r($updateeducationalcert);die;
	$this->db->where('candidateid', $this->loginData->candidateid);

	$this->db->update('tbl_candidate_registration', $updateeducationalcert);



		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 

	

		   $count_orgname = count($_FILES['experiencedocument']['name']);  // die;

		    $this->db->where('candidateid', $this->loginData->candidateid);
			   $result = $this->db->get('tbl_work_experience');

			   $sql = 'SELECT * FROM `tbl_work_experience` Where `tbl_work_experience`.candidateid ='.$this->loginData->candidateid.''; 
			   $result1 = $this->db->query($sql)->result();

		   for ($i=0; $i < $count_orgname; $i++) { 

			if ($result->num_rows() > 0){

				$experienceid = $result1[$i]->id;

					//echo $i; die;

		   	if ($_FILES['experiencedocument']['name'][$i] != NULL) {

		   		@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


		   		$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
		   		$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   		$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
		   		$targetPath = FCPATH . "datafiles/workexperience/";
		   		$targetFile = $targetPath . $encryptedexpdocumentName;
		   		@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   		if($uploadResult == true){

		   			$encrypteddocument = $encryptedexpdocumentName;

		   		}else{

		   			die("Error uploading Work Experience ");
		   			$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   			redirect(current_url());
		   		}

		   	}
		   	else{

		  $OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
		  $encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
		 }
	  
		   	$insertWorkExperience1 = array(
		   		'originaldocumentname'     => $OriginalexperiencedocumentName,
		   		'encrypteddocumnetname'    => $encrypteddocument,
		   		'updateon'                 => date('Y-m-d H:i:s'),
			    'updatedby'                => $this->loginData->candidateid, // 
			);
		   	$this->db->where('id', $experienceid);
		   	$this->db->update('tbl_work_experience', $insertWorkExperience1);

		   }

		}
	

		///////// Work Experience Save End Here ///////////// 

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){

			$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		}else{

			$updateStatusInfo	 = array(

				'BDFFormStatus'  => '99',
			);

			$this->db->where('candidateid',$this->loginData->candidateid);
			$this->db->update('tbl_candidate_registration', $updateStatusInfo);
		   	//die;
			$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');
			redirect('candidate/Candidatedfullinfo/preview');

		}

		}
	}

	$fcount = $this->model->getCountFamilyMember($this->loginData->candidateid);

	$content['familycount']= $fcount;  

	$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetails($this->loginData->candidateid);

	$content['candidatedetails']         = $this->model->getCandidateDetails($this->loginData->candidateid);

	$Icount = $this->model->getCountIdentityNumber($this->loginData->candidateid);

	$content['identitycount']= $Icount;  

	$content['identitydetals'] = $this->model->getCandidateIdentityDetails($this->loginData->candidateid);


	$TEcount = $this->model->getCountTrainingExposure($this->loginData->candidateid);

	$content['TrainingExpcount']= $TEcount;  

	$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($this->loginData->candidateid);


	@ $Lcount = $this->model->getCountLanguage($this->loginData->candidateid);

	@ $content['languageproficiency']= $Lcount;  

	@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($this->loginData->candidateid);
	@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($this->loginData->candidateid);


	$WEcount = $this->model->getCountWorkExprience($this->loginData->candidateid);

	$content['WorkExperience']= $WEcount;  

	$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($this->loginData->candidateid);


	$GPYcount = $this->model->getCountGapYear($this->loginData->candidateid);

	$content['GapYearCount']= $GPYcount;  

	$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($this->loginData->candidateid);



	$content['statedetails']           = $this->model->getState();
	$content['ugeducationdetails']     = $this->model->getUgEducation();
	$content['pgeducationdetails']     = $this->model->getPgEducation();
	$content['campusdetails']          = $this->model->getCampus();
	$content['syslanguage']            = $this->model->getSysLanguage();
	$content['sysrelations']           = $this->model->getSysRelations();
	$content['sysidentity']            = $this->model->getSysIdentity();
	$content['getdistrict']            = $this->model->getDistrict();

	$content['getjoiningstatus'] = $this->model->getCandidateJoiningStatus($this->loginData->candidateid);
	$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
	$content['getgeneralform']   = $this->model->getGeneralFormStatus($this->loginData->candidateid);
	$content['getbdfformstatus']   = $this->model->getBdfFormStatus($this->loginData->candidateid);

		 	// print_r($getgeneralform);



	$content['title'] = 'Candidatedfullinfo';

	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

	$this->load->view('candidate/_main_layout', $content);

}catch (Exception $e) {
	print_r($e->getMessage());die;
}
}




public function preview()
{

	try{


		$fcount = $this->model->getCountFamilyMember($this->loginData->candidateid);

		$content['familycount']= $fcount;  

		$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetailsPrint($this->loginData->candidateid);

		$content['candidatedetails']         = $this->model->getCandidateDetailsPrint($this->loginData->candidateid);

		$Icount = $this->model->getCountIdentityNumber($this->loginData->candidateid);

		$content['identitycount']= $Icount;  

		$content['identitydetals'] = $this->model->getCandidateIdentityDetailsPrint($this->loginData->candidateid);


		$TEcount = $this->model->getCountTrainingExposure($this->loginData->candidateid);

		$content['TrainingExpcount']= $TEcount;  

		$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($this->loginData->candidateid);


		$GPYcount = $this->model->getCountGapYear($this->loginData->candidateid);

		$content['GapYearCount']= $GPYcount;  

		$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($this->loginData->candidateid);



		@ $Lcount = $this->model->getCountLanguage($this->loginData->candidateid);

		@ $content['languageproficiency']= $Lcount;  

		@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($this->loginData->candidateid);
		$content['languagedetalsprint'] = $this->model->getCandidateLanguageDetailsPrint($this->loginData->candidateid);
		@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($this->loginData->candidateid);


		$WEcount = $this->model->getCountWorkExprience($this->loginData->candidateid);

		$content['WorkExperience']= $WEcount;  

		$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($this->loginData->candidateid);

		$content['title'] = 'Staff_registration';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}




public function bdfformsubmit()
{

	try{

		$subject = "Candidates Submitted BDF Form ";
		$body = read_file(base_url().'mailtext/Candidates_Fill_BDF_Formtext'); 

		$candidatedetails = $this->model->getCandidateDetails($this->loginData->candidateid);
		 	//print_r($candidatedetails); die;
		$candidateemail  = $candidatedetails->emailid;

		 	   //$candiadteemailid = $this->input->post('emailid');
			 	$to_email1     = $candidateemail; //// Team Mail Id ////
			 	$to_email2     = 'poonamadlekha@pradan.net'; /// HRD Mail Id ////
			 	$to_email3     = 'utpalmodak@pradan.net';
		 		//$to_candidate  = $result1->emailid; ///// Candidate Email Id ////
			 	$sendmail1 = $this->Common_Model->send_email($subject, $body, $to_email1,$to_email2);

			 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email3);

			 	if($sendmail==true){

			 		$updateStatusInfo	 = array(
			 			'BDFFormStatus' 				  => '1',
			 		);

			 		$this->db->where('candidateid',$this->loginData->candidateid);
			 		$this->db->update('tbl_candidate_registration', $updateStatusInfo);
			 		$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	
			 	}

			 	$content['title'] = 'Candidatedfullinfo';
			 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 	$this->load->view('candidate/_main_layout', $content);


			 }catch (Exception $e) {
			 	print_r($e->getMessage());die;
			 }
			}


		}