<?php
class Login extends CI_controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Common_model', 'Common_Model');
	}

	function index($msg = NULL) {


		//print_r($this->input->post()); die;
		 $RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));
		
		if($username == "" or $password == ""){
			$this->session->set_flashdata('er_msg', 'Please Enter valid login details');
			redirect('candidate/Login/index');
		}else{

			$this->db->select(' `tbl_candidate_registration`.*');
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$query = $this->db->get('tbl_candidate_registration');
			//echo $this->db->last_query();
			//echo $query->num_rows(); die;
			// echo "<pre>";
			// print_r($query->row()); die;
					
			if($query->num_rows() == 1)
				{
					$arrayrow  = $query->row();
					// echo "<pre>";
					// print_r($arrayrow); die;
					if ($arrayrow->campustype=='off' && $arrayrow->confirm_attend !=1) {
						$this->session->set_userdata('loginData',$arrayrow);
					//redirect('candidate/Candidate_dashboard/index');
					redirect('candidate/Acceptance_for_interview/index');
					}else{
					$this->session->set_userdata('loginData',$arrayrow);
					//redirect('candidate/Candidate_dashboard/index');
					redirect('candidate/Candidatedfullinfo/index');
					}
				
	  //   		if($arrayrow->BDFFormStatus ==1){
			// 		$this->session->set_userdata('loginData',$arrayrow);
			// 		redirect('candidate/Candidatedfullinfo/view');
			// 	}elseif($arrayrow->yprstatus =='yes'){
			// 		$this->session->set_userdata('loginData',$arrayrow);
					
			// 		redirect('candidate/Acceptance_for_interview');
			// }else{
			// 		$this->session->set_userdata('loginData',$arrayrow);
			// 		redirect('candidate/Candidatedfullinfo');
			// 	}

				} else {
					$this->session->set_flashdata('er_msg', 'Username or Password doesn\'t matched, Plase try again');
					redirect('candidate/Login/index');
				}
		}
	}
	
		$content = [];
		$this->load->view("candidate/Login/index", $content);		
	}
	
	
	// public function logout()
	// {
	// 	$this->session->unset_userdata('login_data');

	// 	redirect('candidate/login/');
	// }


	public function logout()
	{
	     $this->session->unset_userdata('loginData');
	     $this->session->sess_destroy();
	     redirect('candidate/login/');
	}
	
}