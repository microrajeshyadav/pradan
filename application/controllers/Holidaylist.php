<?php 
class Holidaylist extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Holiday_model');
		$this->load->model("Global_model","gmodel");
		$this->load->library('calendar');
		$this->loginData = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($this->loginData)) {
			redirect('login');
		}

	}
	public function index()
	
	{
		try{

		
		$query = "SELECT * FROM role_permissions a 
		LEFT JOIN sysaccesslevel b 
		on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();

		
		$sql="SELECT * FROM  lpooffice where closed = 'No' order by officename";
		$content['getlpoffice']=$this->db->query($sql)->result();

		$sql="SELECT ID,  case when HolidayDate IS null THEN Holiday ELSE CONCAT(Holiday,' (', CAST(DATE_FORMAT(HolidayDate, '%d/%M') as char), ')')  end as Holiday FROM Holiday order by Holiday";
		$content['getholiday']=$this->db->query($sql)->result();


		$RequestMethod = $this->input->server('REQUEST_METHOD');
		

		if($RequestMethod == 'POST')
		{

			$holiday_id=$this->input->post('holiday_id');
			$holiday_name=$this->input->post('holiday_name');
		// print_r($holiday_name); die();

			
			// $insertArr = array(
			// 	'Calender_year' =>   $this->input->post('Calender_year'),
			// 	'officeid'      =>   $this->input->post('officeid'),
			// 	'holiday_date'  =>   $this->gmodel->changedatedbformate($this->input->post('holiday_date')),
			// 	'holiday_name'  =>   $this->input->post('holiday_name'),
			// 	'createdon'     =>   date('Y-m-d'),
			// 	'createdby'     =>   $this->loginData->staffid
			// );
			

			

			if($holiday_id != NULL || $holiday_id != ""  )
			{
				
				$UpdateArr = array(
					
					'holiday_date'  =>   $this->gmodel->changedatedbformate($this->input->post('holiday_date')),
					
				);

				$this->db->where('id', $holiday_id);
				$this->db->update('tbl_holidaylist', $UpdateArr);

				
				
			}else{
				$insertArr = array(
					'Calender_year' =>   $this->input->post('Calender_year'),
					'officeid'      =>   $this->input->post('officeid'),
					'holiday_date'  =>   $this->gmodel->changedatedbformate($this->input->post('holiday_date')),
					'holiday_name'  =>   $this->input->post('holiday_name'),
					'createdon'     =>   date('Y-m-d'),
					'createdby'     =>   $this->loginData->staffid
				);
				$this->db->insert('tbl_holidaylist', $insertArr);

			}

			$this->session->set_flashdata('tr_msg', 'Successfully Add Holiday List');

			
			//redirect(current_url());
			
		// echo $this->db->last_query();
		// die;

		}
		$content['Holidaylist'] = $this->Holiday_model->Holidaylist();
		// echo "<pre>";
		// print_r($content['Holidaylist']); die;
		

		$content['subview']="Holidaylist";
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	
	public function delete($token)
	
	{
		try{
		
		$query = "SELECT * FROM role_permissions a 
		LEFT JOIN sysaccesslevel b 
		on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();

		
		$sql="SELECT * FROM  lpooffice where closed = 'No' order by officename";
		$content['getlpoffice']=$this->db->query($sql)->result();

		$sql="SELECT ID,  case when HolidayDate IS null THEN Holiday ELSE CONCAT(Holiday,' (', CAST(DATE_FORMAT(HolidayDate, '%d/%M') as char), ')')  end as Holiday FROM Holiday order by Holiday";
		$content['getholiday']=$this->db->query($sql)->result();
		//print_r($content['getholiday']);

		
		$content['Holidaylist'] = $this->Holiday_model->Holidaylist();

		$this->db->where('id', $token);
		$this->db->delete('tbl_holidaylist');
		//echo $this->db->last_query();
		//die();
		
		
		$this->session->set_flashdata('tr_msg' ,"Record Deleted Successfully");
		redirect('/Holidaylist/index/');
		

		$content['subview']="Holidaylist";
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function getholidate($id){

		try{
		$sql="SELECT HolidayDate FROM  holiday where  ID = '".$id."'";
		$data = $this->db->query($sql)->row();
		$date = $this->gmodel->changedatedbformate($data->HolidayDate);//exit;
		// echo $date;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	


	
}




