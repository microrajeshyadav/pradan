<?php 
class Joining_report_of_cr extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		 $check = $this->session->userdata('login_data');
		    ///// Check Session //////  
		    if (empty($check)) {
		       redirect('login');
		    }

		     $this->loginData = $this->session->userdata('login_data');
		     /*echo "<pre>";
		     print_r($this->loginData);die();*/
	}

	public function index($token)
	{
		try{

		
		$this->load->model('Joining_report_of_newplace_posting_model');

		$this->db->trans_start();
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			
			$savebtn = $this->input->post('savebtn');
			$submitbtn = $this->input->post('submitbtn');

			$staffdutytodaydate = $this->input->post('join_programme_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);
			$dutyon = $this->gmodel->changedatedbformate($this->input->post('dutyon'));

			if (isset($savebtn) && $savebtn =='senddatasave') {

			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	           'flag'               => 0,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
	  //       echo "<pre>";
			// print_r($insertArr);
			// die;

      
          // $this->db->insert('tbl_joining_report_new_place', $insertArr);
          // $insertid = $this->db->insert_id();
      }
		
		}

		//$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($this->loginData->teamid);
			
		$content['title'] = 'Joining_report_of_cr/index2';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function add($token)
	{
		try{
		if($this->loginData->RoleID==2 ||$this->loginData->RoleID==21)
		{
		 $this->uri->segment(4);
		}
		if($this->loginData->RoleID==3)
		{
			$token= $this->uri->segment(3);
					
		}
	
		$this->load->model('Joining_report_of_cr_model');

		$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->row();
		
	    $forwardworkflowid = $result->workflowid;
		$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		// echo "<pre>";
		// print_r($content['getstaffdetailslist']);exit();
		$content['getstaffsuperiordetailslist'] = $this->model->getstaffSuperior($token);
		//print_r($content['getstaffsuperiordetailslist']);
		//die;
		 $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		
		 $receiverdetail = $this->model->get_staffDetails($content['getstaffdetailslist']->staffid);
		
		
		 	// print_r($receiverdetail);
		 	// die;
		 $t_flag='';
		 if($this->loginData->RoleID==2 ||$this->loginData->RoleID==21)
		 {
		 	$t_flag=11;
		 	$reciver=$personnel_detail->staffid;
		 }
		 else if($this->loginData->RoleID==3)
		 {
		 	$t_flag=10;	
		 	$reciver=$receiverdetail->reportingto;
		 }
  			// echo $reciver;
  			// die;
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			// echo "<pre>";
			// print_r($_POST); die;
			
			

			 $staffdutytodaydate = $this->input->post('staff_duty_today_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);
			$dutyon = $this->gmodel->changedatedbformate($this->input->post('dutyon'));

			
		if ($this->input->post('comments')) {


			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'reported_for_duty_date'   => $dutyon,
	          'transid'           =>  $token,
	          'assigned'=> $this->input->post('assignedto'),
	          'location'=> $this->input->post('new_office_name'),
	          'supervision_staffid'=> $this->input->post('supervision_id'),
	          'flag'               => 1,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
	       
      
          $this->db->insert('tbl_joining_report_new_place', $insertArr);
          $insertid = $this->db->insert_id();

          	$updateArr = array(                         
	          'trans_flag'     => $t_flag,
	          'updatedon'      => date("Y-m-d H:i:s"),
	          'updatedby'      => $this->loginData->staffid
	        );

	         $this->db->where('id',$token);
	         $this->db->update('staff_transaction', $updateArr);
	        
	        $insertworkflowArr = array(
	         'r_id'                 => $token,
	         'type'                 => 4,
	         'staffid'              => $content['getstaffdetailslist']->staffid,
	         'sender'               => $this->loginData->staffid,
	         'receiver'             =>  $reciver,
	         'forwarded_workflowid' => $forwardworkflowid,
	         'senddate'             => date("Y-m-d H:i:s"),
	         'flag'                 => $t_flag,
	         'scomments'            => $this->input->post('comments'),
	         'createdon'            => date("Y-m-d H:i:s"),
	         'createdby'            => $this->loginData->staffid,
	        );
	        // echo "<pre>";
	        // print_r($insertworkflowArr);
	        // die;
	        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

	        $staffupdateArr = array(
	        	'designation'=>$content['getstaffdetailslist']->new_designation_id,
	        	'new_office_id'=>$content['getstaffdetailslist']->new_office_id,
	        	'doj_team'=>$content['getstaffdetailslist']->proposeddate,
	        	'reportingto'=>$content['getstaffdetailslist']->reportingto
	        );
	        $this->db->where('staffid', $content['getstaffdetailslist']->staffid);
	        $this->db->update('Staff',$staffupdateArr);

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
          	$subject = "Your Process Approved";
		      $body = 'Dear,<br><br>';
		      $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
		      $body .= 'This is to certify that Mr. '.$content['getstaffdetailslist']->name.'<br> your process has Approved  .<br><br>';
		      $body .= 'Thanks<br>';
		      $body .= 'Administrator<br>';
		      $body .= 'PRADAN<br><br>';

		      $body .= 'Disclaimer<br>';
		      $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

		      
		      $to_email = $receiverdetail->emailid;
		      $to_name = $receiverdetail->name;
		      //$to_cc = $getStaffReportingtodetails->emailid;
		      $recipients = array(
		         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
		         $content['getstaffdetailslist']->emailid => $content['getstaffdetailslist']->name
		         // ..
		      );
		      $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
		      if (substr($email_result, 0, 5) == "ERROR") {
		        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
		      }
            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_cr/view/'.$insertid);

          }
		}

	}

		
		//print_r($content['getstaffdetailslist']); die;

		$content['token'] = $token;
			
		$content['title'] = 'Joining_report_of_cr/add';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}






  public function view($reportid)
	{
		try{
		
		$query = "SELECT transid, reported_for_duty_date, createdon, location FROM tbl_joining_report_new_place WHERE id=".$reportid;
		$content['queryresult'] = $this->db->query($query)->row();
		$token = $content['queryresult']->transid;
		$this->load->model('Joining_report_of_cr_model');
     	// $RequestMethod = $this->input->server('REQUEST_METHOD'); 

     	$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
     	// print_r($content['getstaffdetailslist']);
     	// die;
		$content['getstaffsuperiordetailslist'] = $this->model->getstaffSuperior($token);

		// print_r($content['getstaffsuperiordetailslist']);
		// die;
		
		
		$content['getstaffjoiningreport'] = $this->model->getstaffjoiningreport($token);
		//print_r($getstaffjoiningreport); die;
		/*echo "<pre>";
		print_r($content['getstaffjoiningreport']);exit;*/
		$content['token'] = $token;


			
		$content['title'] = 'Joining_report_of_newplace_posting/view';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
  }
	


public function approve($transid){
	try{
      // start permission 
      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
      $content['role_permission'] = $this->db->query($query)->result();
      // end permission 
$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($transid);

     
      
       
       $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();


      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail 
      WHERE r_id = $transid";
      $result  = $this->db->query($sql)->result()[0];
      

     

      $forwardworkflowid = $result->workflowid;
     
     
      $RequestMethod = $this->input->server('REQUEST_METHOD'); 

      

      if($RequestMethod == 'POST'){
        // echo "<pre>";
       //  print_r($_POST);
       // die;

      

     


      //  $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
         $comments = $this->input->post('comments');

       
       

      

               
        $this->form_validation->set_rules('status','Select Status','trim|required');
         $this->form_validation->set_rules('comments','trim|required');
        
        if($this->form_validation->run() == TRUE){
       
        $this->db->trans_start();
       

          $updateArr = array(

            'trans_flag'     => $status,
            'updatedon'      => date("Y-m-d H:i:s"),
            'updatedby'      => $this->loginData->staffid
          );
         
           $this->db->where('id',$transid);
           $this->db->update('staff_transaction', $updateArr);

         
          $insertworkflowArr = array(

           'r_id'                 => $transid,
           'type'                 => 4,
           'staffid'              => $content['getstaffdetailslist']->staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $personnel_detail->staffid,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $status,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

           $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
         
           
          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

         
            $subject = "Your Process Approved";
            $body = 'Dear,<br><br>';
            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
            $body .= 'This is to certify that Mr. '.$staffdetail->name.'<br> your process has Approved  .<br><br>';
            $body .= 'Thanks<br>';
            $body .= 'Administrator<br>';
            $body .= 'PRADAN<br><br>';

            $body .= 'Disclaimer<br>';
            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

            
            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $staffdetail->emailid => $staffdetail->name
               // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            
            $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          //redirect('/Staff_approval/'.$token);
            redirect($content['redirect_controller']);

          }

        }
        
         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_approval/'.$token);

        }
      }
  

 /*echo "<pre>";
    print_r($transfer_to_id);exit();*/
    prepareview:

   
    $content['tarnsid'] = $transid;

    // $content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();
    
    $content['subview'] = 'Joining_report_of_cr/approve';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
			print_r($e->getMessage());die;
		}
}




}