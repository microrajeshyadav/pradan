<?php 

/**
* State List
*/
class Leaverule extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from mskeyrules order by CYear desc";
    $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    $content['title'] = 'Leaverule';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add()
  {

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){
      
      
      $this->form_validation->set_rules('leave','Name','trim|required|min_length[1]|max_length[50]');
      $this->form_validation->set_rules('leaveaccured','Name','trim|required|min_length[1]|max_length[50]');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $cyear = $this->input->post('Cyear');

      $query = "SELECT count(id) as fcount from mskeyrules where CYear = '". $cyear."'";
      $check = $this->db->query($query)->row();
      if ($check->fcount == 0)
      {
         $insertArr = array(
            'CYear' => $this->input->post('Cyear'),
            'Maxcf_yearleave'=> $this->input->post('leave'),
           'maxleaveaccrued' => $this->input->post('leaveaccured')
        );

        $this->db->insert('mskeyrules', $insertArr);
        $this->session->set_flashdata('tr_msg', 'Successfully added Leave Rule');
        redirect('/Leaverule/index/');
      }
      else
      {
        $this->session->set_flashdata('er_msg', 'Record allready available for selected financial year');
       redirect('/Leaverule/index/');
      }
     // echo $this->db->last_query(); die;
      
    }

    prepareview:

   
    $content['subview'] = 'Leaverule/add';
    $this->load->view('_main_layout', $content);
  }

  public function edit($token=NULL)
  {

   
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('leavemax','Name','trim|required|min_length[1]|max_length[3]');
      $this->form_validation->set_rules('leaveaccured','Name','trim|required|min_length[1]|max_length[3]');
     if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
           'Maxcf_yearleave'      => $this->input->post('leavemax'),
           'maxleaveaccrued'      => $this->input->post('leaveaccured')
        );
      
      $this->db->where('CYear', $token);
      $this->db->update('mskeyrules', $updateArr);
 //echo  $this->db->last_query(); die;

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Leave Rule');
       redirect('/Leaverule/');
    }

    prepareview:

    $content['title'] = 'Leaverule';
    $Leave_detail = $this->Common_Model->get_data('mskeyrules', '*', 'CYear',$token);
    // print_r($Leave_details); die();
    $content['Leave_details'] = $Leave_detail;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }
    function delete($token = null)
    {
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('id', $token);
      $this->db->update('mst_oprationtype', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Sectoral Specialization Deleted Successfully');
      redirect('/Leave/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
  }

}