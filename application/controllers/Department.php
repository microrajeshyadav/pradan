<?php 

/**
* State List
*/
class Department extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     try{
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from tbl_department where isdeleted='0' ORDER BY id DESC";
    $content['mstudepartment'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
    $content['title'] = 'Department';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('depname','Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('depcode','Code','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'name'      => $this->input->post('depname'),
          'status'      => $this->input->post('status'),
          'createdon'      => date('Y-m-d H:i:s'),
          'createdby'      => $this->loginData->UserID,
           'code'      => $this->input->post('depcode'),

        );
      
      $this->db->insert('tbl_department', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added Department');
      redirect('/Department/index/');
    }

    prepareview:

   
    $content['subview'] = 'Department/add';
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  public function edit($token)
  {
    try{
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('depname','Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
         'name'      => $this->input->post('depname'),
          'status'      => $this->input->post('status'),
         'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,
           'code'      => $this->input->post('depcode'),
        );
      
      $this->db->where('id', $token);
      $this->db->update('tbl_department', $updateArr);
  $this->db->last_query(); 

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Department');
       redirect('/Department/');
    }

    prepareview:

    $content['title'] = 'Department';
    $state_details = $this->Common_Model->get_data('tbl_department', '*', 'id',$token);
    $content['mstdepartment_details'] = $state_details;

 

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
    function delete($token)
    {
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('id', $token);
      $this->db->update('tbl_department', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Department Deleted Successfully');
      redirect('/Department/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);

      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}