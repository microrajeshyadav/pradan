<?php 

/**
* State List
*/
class Salery_details extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "SELECT a.desname, b.name, b.emp_code, b.staffid, c.e_basic FROM staff AS b INNER JOIN msdesignation AS a ON b.designation = a.desid LEFT JOIN tblmstemployeesalary AS c ON b.staffid = c.staffid AND b.status=1";
    $content['mstdatass_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    
    $content['title'] = 'Salery_details';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }




}