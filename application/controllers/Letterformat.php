<?php 
class Letterformat extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
     		 redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		//index include
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission 

		$this->load->model('Batch_model');
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
			 $month     = $this->input->post('month');
			 $exp       = explode('/',$month);
			 $MonthYear = $exp[1].'-'.$exp[0];
			// $this->session->set_userdata("month",$this->input->post('month'));
		}

		//$content['usedbatch'] = $this->model->UsedBatch();
		$content['letterlist'] = $this->model->getletterformat();
		/*echo "<pre>";
		print_r($content['letterlist']);exit();*/
		//$usedbatch = $this->model->UsedBatch();

		$content['title'] = 'Letter and Email Formats';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function add(){
		try{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();		
		// end permission 
		if($this->input->server('REQUEST_METHOD') == "POST"){
			$checkavailablity = $this->lettercontentcheck($this->input->post('processid'));
			$dataset = array(
				'processid'=>$this->input->post('processid'),
				'lettercontent'=>$this->input->post('lettercontent')
			);
			$this->db->trans_start();
			if(empty($checkavailablity)){
				$this->db->insert('tbl_letter_master', $dataset);
			}else{
				$this->db->where('processid',$this->input->post('processid'));
				$this->db->update('tbl_letter_master', $dataset);
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error Assign Teams');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully Saved letter');			
			}
		}
		$content['title'] = 'Letter and Email Formats';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function edit($formatid){
		try{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();	
		$content['getletterformatdetail'] = $this->model->getletterformatdetail($formatid);
		$content['getattributeslist'] = $this->model->getattributeslist($content['getletterformatdetail']->processid);
		$content['processlist'] = $this->model->getprocesslist($content['getletterformatdetail']->type);
		/*echo "<pre>";
		print_r($content['getletterformatdetail']);exit();*/
		// end permission 
		if($this->input->server('REQUEST_METHOD') == "POST"){
			$checkavailablity = $this->lettercontentcheck($this->input->post('processid'));
			$dataset = array(
				'processid'=>$this->input->post('processid'),
				'lettercontent'=>$this->input->post('lettercontent')
			);
			$this->db->trans_start();
			if(empty($checkavailablity)){
				$this->db->insert('tbl_letter_master', $dataset);
			}else{
				$this->db->where('processid',$this->input->post('processid'));
				$this->db->update('tbl_letter_master', $dataset);
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error Assign Teams');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully Updated letter');			
			}
		}
		$content['getletterformatdetail'] = $this->model->getletterformatdetail($formatid);
		$content['getattributeslist'] = $this->model->getattributeslist($content['getletterformatdetail']->processid);
		$content['processlist'] = $this->model->getprocesslist($content['getletterformatdetail']->type);
		$content['title'] = 'Letter and Email Formats Edit';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function delete($processid){
		try{
		$dataset = array('isactive'=>0);
		$this->db->where('id', $processid);
		$this->db->update('tbl_letter_master', $dataset);
		redirect('Letterformat');

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function processlist(){
		try{
					
		$formattype = $this->input->post('formattype');
		$query = "SELECT * FROM tbl_letter_process WHERE type = ".$formattype." AND isactive=1";
		$content['processlist'] = $this->db->query($query)->result();
		//$response = '';
		$response = '<option value="">Select Process</option>';
		if($content['processlist']){
			foreach ($content['processlist'] as  $value) {
				# code...
				$response .= '<option value="'.$value->processid.'">'.$value->process.'</option>';
			}
		}
		echo $response;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	public function attributelist(){

		try{
		$processid = $this->input->post('processid');
		$query = "SELECT * FROM tbl_letter_process_attributes WHERE processid = ".$processid." ";
		$content['attributelist'] = $this->db->query($query)->result();
		//$response = '';
		$response = '';
		if($content['attributelist']){
			foreach ($content['attributelist'] as  $value) {
				# code...				
				$response .= '<li  class="ui-state-default draggable-item"><a href="">'.$value->attributes.'</a></li>';
			}
		}
		echo $response;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	public function lettercontentcheck($procssidget=NULL){
		try{
		if($procssidget){
			$processid = $procssidget;
		}else{				
			$processid = $this->input->post('processid');
		}
		$processid = $this->input->post('processid');
		$query = "SELECT * FROM tbl_letter_master WHERE processid = ".$processid." ";
		$content['lettercontent'] = $this->db->query($query)->row();
		//$response = '';
		$response = '';
		if($content['lettercontent']){
			$response = $content['lettercontent']->lettercontent;
		}else{
			$response = '';
		}
		if($procssidget){
			return $response;
		}else{
			echo $response;
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



}