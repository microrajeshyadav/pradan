<?php 
class Staff_sepexperiencecertificate extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepexperiencecertificate_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();

			$query ="SELECT * FROM staff_transaction WHERE id =".$token;
			$content['staff_experience_certificate'] = $this->db->query($query)->row();

			$staffid =  $content['staff_experience_certificate']->staffid;

			$content['experience_certificate'] = $this->Common_Model->get_staff_sep_detail($token);
			// print_r($content['experience_certificate']); die;
			$content['staffid'] = $content['staff_experience_certificate']->staffid;
			
			$query = "SELECT * FROM tbl_experience_ceritficate WHERE transid = ".$token;
			$content['experience_detail'] = $this->db->query($query)->row();
			//print_r($content['experience_detail']); 
			//echo $content['experience_detail']->id; die;

			$RequestMethod = $this->input->server("REQUEST_METHOD");


			if($RequestMethod == 'POST')
			{

				// print_r($this->input->post()); die;
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'save & submit'){

					$db_flag = 1;
				}

				$insertArray = array(

					'staffid'                   => $content['staff_experience_certificate']->staffid,
					'transid'                   => $token,
					'experience_ceritficate_no' => $this->input->post('experience_ceritficate_no'),
					'no_of_regular_employee'    => $this->input->post('no_of_regular_employee'),
					'no_of_professional'        => $this->input->post('no_of_professional'),
					'createdby'                 => $this->loginData->staffid,
					'flag'                      => $db_flag,

				);

				if($content['experience_detail'])
				{
					$updateArray = array(

						'staffid'                   => $content['staff_experience_certificate']->staffid,
						'transid'                   => $token,
						'experience_ceritficate_no' => $this->input->post('experience_ceritficate_no'),
						'no_of_regular_employee'    => $this->input->post('no_of_regular_employee'),
						'no_of_professional'        => $this->input->post('no_of_professional'),
						'updatedby'                 => $this->loginData->staffid,
						'flag'                      => $db_flag,
					);
					$this->db->where('id', $content['experience_detail']->id);
					$flag = $this->db->update('tbl_experience_ceritficate', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_experience_ceritficate',$insertArray);
				}
				//echo $this->db->last_query(); die;


				if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
				// redirect(current_url());
			} 
			else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
			// redirect(current_url());
		}
		
	}
	$query = "SELECT * FROM tbl_experience_ceritficate WHERE transid = ".$token;
	$content['experience_detail'] = $this->db->query($query)->row();
	$content['token'] = $token;
	$content['title'] = 'Staff_sepexperiencecertificate';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}