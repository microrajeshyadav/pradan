<?php 

/**
* State List
*/
class Staffpromotion extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Staffpromotion_model");
    $this->load->model("Off_campus_candidate_list_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $query = "select * from msleavecrperiod";
   $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

   $content['title'] = 'Staffpromotion';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }





 public function Add($token)
 {
  try{

    if (empty($token) || $token == '' ) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
        redirect('/Staff_list/index');
        
      } else {

  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

  $tablename = "'staff_transaction'";
  $incval = "'@a'";


    $content['staff']=$this->Staffpromotion_model->getstaffname($token);

if(!empty($content['staff']))
{

$reportingto = $content['staff']->reportingto;
$office =$content['staff']->new_office_id;
$content['d_tranfer']=$this->Staffpromotion_model->get_alloffice($content['staff']->new_office_id);
}
else{
  $content['staff'] = null;
}
  
  // print_r($content['staff']); die;


  $content['token'] = $token;
  
  // echo $reportingto; die;

   
  

  $content['new_designation']=$this->Staffpromotion_model->get_alldesignation();


  // print_r($content['d_tranfer']); die;
  $content['transfer_promption']=$this->Staffpromotion_model->get_staffDetails($token);  

   //print_r($content['transfer_promption']);
  $p_office=$content['transfer_promption']->new_office_id;
  $p_designation=$content['transfer_promption']->new_designation;
  $content['all_office']=$this->Staffpromotion_model->get_alloffice($p_office);
   //print_r($content['all_office']);
  $content['all_designation']=$this->Staffpromotion_model->get_alldesignation($p_designation);
   //print_r($content['all_designation']);

  $getstaffincrementalid = $this->Staffpromotion_model->getStaffTransactioninc($tablename,$incval); 
  $autoincval =  $getstaffincrementalid->maxincval;

  $RequestMethod = $this->input->server('REQUEST_METHOD'); 
  if($RequestMethod == 'POST'){
// print_r($_POST); die();
    $this->db->trans_start();
    $this->db->trans_strict(FALSE);

    $date1 = $this->input->post('fromdate');
    $date2 = $this->Off_campus_candidate_list_model->changedatedbformate($date1);
    $changereportingto = $this->input->post('changereportingto');
        if ($changereportingto == 'on') 
        {
          $changereportingto = 1;
        }
        else
        {
          $changereportingto = 0;
        }
    $designation=$this->input->post('new_designation');
    $datetime= date("Y-m-d H:i:s");

    $transstatus = 'Promotion';
    $sql = "SELECT count(staffid) as staffcount FROM staff_transaction WHERE trans_status ='".$transstatus."' AND staffid='".$token."' AND date_of_transfer ='".$date2."'";

    // echo $sql; die;

     $result = $this->db->query($sql)->row();
     // echo $result->staffcount; die;
     if($result->staffcount == 0) {

       $insertArr = array(
      'id'                 => $autoincval,
      'staffid'            => $token,
      'old_office_id'      => $this->input->post('old_office'),
      'old_designation'    => $this->input->post('Presentdesignation'),
      'new_designation'    => $designation,
      'reportingto'        => $this->input->post('reportingto'),
      'new_office_id'      => $this->input->post('new_office'),
      'reason'             => $this->input->post('reason'),
      'changereporting'    => $changereportingto,
      'date_of_transfer'   => $date2,
      'trans_flag'         => 1,
      'trans_status'       =>'Promotion',
      'datetime'           => $datetime,
      'createdby'          => $this->loginData->staffid,
      'createdon'          => date("Y-m-d H:i:s"),
    );
    //    echo "<pre>";
    // print_r($insertArr); die;

    $this->db->insert('staff_transaction', $insertArr);
    //$insertid = $this->db->insert_id();

    $insertworkflowArr = array(
      'r_id'           => $autoincval,
      'type'           => 4,
      'staffid'        => $token,
      'sender'         => $this->loginData->staffid,
      'receiver'       => $reportingto,
      'senddate'       => date("Y-m-d H:i:s"),
      'scomments'      => $this->input->post('remark'),
      'flag'           => 1,
      'createdon'      => date("Y-m-d H:i:s"),
      'createdby'      => $this->loginData->staffid,
    );
// print_r($insertworkflowArr); die;
    $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){
      $this->session->set_flashdata('er_msg', 'Error added  Staff  Promotion'); 
      redirect(current_url());
    }else{
     $this->session->set_flashdata('tr_msg', 'Successfully added  Staff Promotion');
     redirect('/Staffpromotion/history/'.$token);
   }   
        
     }else{
      $this->session->set_flashdata('er_msg', "Change responsibility already under process !!");
        redirect('/Staffpromotion/add/'.$token);


   
}

 }


 $content['subview'] = 'Staffpromotion/add';
 $this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function getRidincrement($token)
{

 try{
  $sql = 'SELECT count(*) as `count` FROM `tbl_workflowdetail`      
  Where receiver ='.$token.''; 
  $result = $this->db->query($sql)->row();

  return $result;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}

}


public function history($token)
{
  try{

if (empty($token) || $token == '' ) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
        redirect('/Staff_list/index');
        
      } else {
  $content['token']=$token;
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  $content['view_history']=$this->Staffpromotion_model->getview_history($token);

// end permission 

   // $query = "select * from msleavecrperiod";
    //$content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

  $content['title'] = 'Staffpromotion';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);   
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


}