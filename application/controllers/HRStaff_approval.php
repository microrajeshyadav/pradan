<?php 

/**
* State List
*/
class HRStaff_approval extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Staff_seperation_model","Staff_seperation_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');

 }

 public function index()
 {
  try{
   $this->load->model("HRStaff_approval_model");
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   /*$content['getworkflowdetail'] = $this->HRStaff_approval_model->getworkflowdetaillist();
   $content['getprobationworklist'] = $this->HRStaff_approval_model->getprobationworkflowdetaillist();
   $content['gettransferpromationworkflowdetail'] = $this->HRStaff_approval_model->get_transfer_promation_workflow_detail_list();*/


   

   $content['staff_seperation'] = $this->HRStaff_approval_model->staff_seperation();
  /*echo "<pre>";
  print_r($content['staff_seperation']);exit();*/
  
   $content['getworkflowdetail'] = $this->HRStaff_approval_model->getworkflowdetaillist();


   $content['gettransferworkflowdetail'] = $this->HRStaff_approval_model->get_transfer_promotion_workflowdetail();

   $content['getpromotionworkflowdetail'] = $this->HRStaff_approval_model->get_promotion_workflowdetail();


  /*$content['getpromationworkflowdetail'] = $this->HRStaff_approval_model->get_promation_workflow_detail_list();*/
   // print_r($content['getpromationworkflowdetail']); die;
  $content['title'] = 'HRStaff_approval';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function list_joining_report($token)
{
  try{
 $this->load->model("Staff_approval_model");
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

 $content['getstaffjoiningreport'] = $this->Staff_approval_model->getstaffjoiningreport($token);
   //$content['subview']="index";
 $content['title'] = 'Staff_approval/list_joining_report';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
 }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function add_transfer_approval()
{
  try{

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 


  $staffid = $this->uri->segment(3);
  $tarnsid  = $this->uri->segment(4);
     
      /*18 ed => 7,8
        17 pers => 5,6
        20 finance => 9,10
        2 tc => 4,3
        2=>17=>18=>17=>2=>20=>17=>18=>17

        2  tc      =>4,3  
        17 pers    =>5,6
        18 ed      =>7,8
        17 pers    =>11,12(on behalf -> 5,6)
        2  tc      =>13,14
        20 finance =>9,10
        17 pers    =>15,16
        18 ed      =>17,18
        17 pers    =>19,20
        */

        $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail 
        WHERE r_id = $tarnsid";
        $result  = $this->db->query($sql)->result()[0];

      /*echo "<pre>";
      print_r($result);exit();*/

      $forwardworkflowid = $result->workflowid;
      if ($this->loginData->RoleID == 18) { // Executive Director
        if($result->flag == 5){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 7;
          $unflag = 8;
          $content['addflag'] = 7;
          $content['addunflag'] = 8;
        }else if($result->flag == 15){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 17;
          $unflag = 18;
          $content['addflag'] = 17;
          $content['addunflag'] = 18;
        }
      }else if($this->loginData->RoleID == 17){ // 17 personal Admin
        if($result->flag == 4){
          $content['heading_text'] = 'Executive Director';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 18;
          $flag = 5;
          $unflag = 6;
        }else if($result->flag == 9){
          $content['heading_text'] = 'Executive Director';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 18;
          $flag = 15;
          $unflag = 16;
        }else{
          $content['heading_text'] = 'Team Coordinator';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 2;
          $flag = 5;
          $unflag = 6;
        }
      }else if($this->loginData->RoleID == 20){ // Finance Admin
        $content['heading_text'] = 'Personnel Administrator';
        $content['redirect_controller'] = 'Staff_finance_approval';
        $transfer_to_id = 17;
        $flag = 9;
        $unflag = 10;
      }else if($this->loginData->RoleID == 2){ // Superviser TC
        if($result->flag >= 3){
          $content['heading_text'] = 'Finance Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 20;
          $flag = 3;
          $unflag = 4;
        }else if($result->flag == 1){
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 17;
          $flag = 3;
          $unflag = 4;
        }
      }
      $RequestMethod = $this->input->server('REQUEST_METHOD'); 

      if($RequestMethod == 'POST'){

      // echo "<pre>";
      // print_r($this->input->post()); die;

        $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
        $executivedirector_administration = $this->input->post('executivedirector_administration');

        $transferstaffdetails = $this->Staff_approval_model->getTransferStaffDetails($transid);

       $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
       $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.



       // $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
       $this->form_validation->set_rules('status','Seelct Status','trim|required');

       if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }




       

/*print_r($status); die();*/
      $this->db->trans_start();
        if($status == $flag){     //// Status => Approve 

          $updateArr = array(

            'trans_flag'     => $flag,
            'updatedon'      => date("Y-m-d H:i:s"),
            'updatedby'      => $this->loginData->staffid
          );

          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $updateArr);

          
          $insertworkflowArr = array(

           'r_id'                 => $transid,
           'type'                 => 2,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $executivedirector_administration,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $flag,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{
            $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          //redirect('/Staff_approval/'.$token);
            redirect($content['redirect_controller']);

          }

        }


        if ($status ==$unflag) {

         $insertArr = array(

          'trans_flag'     => $unflag,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 2,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => $unflag,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_approval/'.$token);

        }
      }


    }

    prepareview:

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    $content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();
    $content['getexecutivedirector'] = $this->Staff_approval_model->getExecutiveDirectorList($transfer_to_id);

    //print_r($content['getexecutivedirector']);  die();

    $content['subview'] = 'Staff_approval/add_transfer_approval';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }



public function Add()
{
 try{
  // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 


  $staffid = $this->uri->segment(3);
  $tarnsid  = $this->uri->segment(4);

      /*18 ed => 7,8
        17 pers => 5,6
        20 finance => 9,10
        2 tc => 4,3
        2=>17=>18=>17=>2=>20=>17=>18=>17

        2  tc      =>4,3  
        17 pers    =>5,6
        18 ed      =>7,8
        17 pers    =>11,12(on behalf -> 5,6)
        2  tc      =>13,14
        20 finance =>9,10
        17 pers    =>15,16
        18 ed      =>17,18
        17 pers    =>19,20
        */
        $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail 
        WHERE r_id = $tarnsid";
        $result  = $this->db->query($sql)->result()[0];

      $forwardworkflowid = $result->workflowid;
      if ($this->loginData->RoleID == 18) { // Executive Director
        if($result->flag == 5){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 7;
          $unflag = 8;
          $content['addflag'] = 7;
          $content['addunflag'] = 8;
        }else if($result->flag == 15){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 17;
          $unflag = 18;
          $content['addflag'] = 17;
          $content['addunflag'] = 18;
        }
      }else if($this->loginData->RoleID == 17){ // 17 personal Admin
        if($result->flag == 4){
          $content['heading_text'] = 'Executive Director';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 18;
          $flag = 5;
          $unflag = 6;
        }else if($result->flag == 9){
          $content['heading_text'] = 'Executive Director';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 18;
          $flag = 15;
          $unflag = 16;
        }else{
          $content['heading_text'] = 'Team Coordinator';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 2;
          $flag = 5;
          $unflag = 6;
        }
      }else if($this->loginData->RoleID == 20){ // Finance Admin
        $content['heading_text'] = 'Personnel Administrator';
        $content['redirect_controller'] = 'Staff_finance_approval';
        $transfer_to_id = 17;
        $flag = 9;
        $unflag = 10;
      }else if($this->loginData->RoleID == 2){ // Superviser TC
        // echo $result->flag;exit();
        if($result->flag == 5 || $result->flag == 13){
          $content['heading_text'] = 'Finance Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 20;
          $flag = 3;
          $unflag = 4;
        }else if($result->flag == 1){
          $content['heading_text'] = 'HR';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 16;
          $flag = 21;
          $unflag = 22;
        }else if($result->flag == 21){
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 17;
          $flag = 3;
          $unflag = 4;
        }
      }

    $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($staffid); 
    $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
    $content['getexecutivedirector'] = $this->Staff_approval_model->getExecutiveDirectorList($transfer_to_id);
    $content['reporitingtodetail'] = $this->Staff_seperation_model->get_staffDetails($content['staffdetail']->reportingto);
    echo "<pre>";
    print_r($flag); 
    die;
    
      $RequestMethod = $this->input->server('REQUEST_METHOD'); 

      if($RequestMethod == 'POST'){

      

        $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
        $executivedirector_administration = $this->input->post('executivedirector_administration');
        if($executivedirector_administration){
        $receiverdetail = $this->Staff_seperation_model->get_staffDetails($executivedirector_administration);
      }
       

        $transferstaffdetails = $this->Staff_approval_model->getTransferStaffDetails($transid);
         
       $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
       $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.



       $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
       $this->form_validation->set_rules('status','Seelct Status','trim|required');

       if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }




     
//print_r($status); die();
      $this->db->trans_start();
        if($status == $flag){     //// Status => Approve 
//print_r($status); die();
          if($result->flag == 15){
            $staffUpdate = array(
            'status' => 0,
            'dateofleaving' => $transferstaffdetails->date_of_transfer,
            'separationtype' => $transferstaffdetails->trans_status,
            'separationremarks' => $transferstaffdetails->reason
            );
            $this->db->where('staffid',$staffid);
            $this->db->update('staff', $staffUpdate);
          }


          $updateArr = array(

            'trans_flag'     => $flag,
            'updatedon'      => date("Y-m-d H:i:s"),
            'updatedby'      => $this->loginData->staffid
          );

          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $updateArr);

          
          $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 3,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $executivedirector_administration,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $flag,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{
            $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
            //redirect('/Staff_approval/'.$token);
            $subject = "Your Process Approved";
            $body = 'Dear,<br><br>';
            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
            $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
            $body .= 'Thanks<br>';
            $body .= 'Administrator<br>';
            $body .= 'PRADAN<br><br>';

            $body .= 'Disclaimer<br>';
            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

            
            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;/**/
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $content['staffdetail']->emailid => $content['staffdetail']->name
               // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
            }
            redirect($content['redirect_controller']);

          }

        }


        if ($status ==$unflag) {
          //echo $unflag;exit();
         $insertArr = array(

          'trans_flag'     => $unflag,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 3,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => $unflag,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){
          $subject = "Your Process has Declined";
          $body = 'Dear,<br><br>';
          $body .= '<h2>Your Request has been Declined </h2><br>';
          $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Declined  .<br><br>';
          $body .= 'Thanks<br>';
          $body .= 'Administrator<br>';
          $body .= 'PRADAN<br><br>';

          $body .= 'Disclaimer<br>';
          $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

          
          $to_email = $content['staffdetail']->emailid;
          $to_name = $content['staffdetail']->name;
          //$to_cc = $getStaffReportingtodetails->emailid;
          $recipients = array(
             $personnel_detail->EmailID => $personnel_detail->UserFirstName,
             $content['reporitingtodetail']->emailid => $content['reporitingtodetail']->name
             // ..
          );
          $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
          if (substr($email_result, 0, 5) == "ERROR") {
            $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
          }
          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_approval/'.$token);

        }
      }


    }

    prepareview:

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    $content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();
    $content['getexecutivedirector'] = $this->Staff_approval_model->getExecutiveDirectorList($transfer_to_id);

    //print_r($content['getexecutivedirector']);  die();

    $content['subview'] = 'Staff_approval/add';
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }







  public function edit($token)
  {
    try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 


    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('shortname','Short name','trim|required|min_length[1]|max_length[10]');
      $this->form_validation->set_rules('categoryname','Category Name','trim|required|min_length[1]|max_length[50]');
      $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }

      $updateArr = array(
        'shortname'      => $this->input->post('shortname'),
        'categoryname'   => $this->input->post('categoryname'),
        'updatedon'      => date('Y-m-d H:i:s'),
        'updatedby'      => $this->loginData->UserID,
        'status'         => $this->input->post('status')
      );
      
      $this->db->where('id', $token);
      $this->db->update('mst_staff_category', $updateArr);
      $this->db->last_query(); 

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Staff Category');
      redirect('/Staff_approval/');
    }

    prepareview:

    $content['title'] = 'Staff_approval';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }



  public function add_clearance_certificate($token)
  {
    try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 
    $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
    $result  = $this->db->query($sql)->result()[0];
    $forwardworkflowid = $result->workflowid;
    $content['personaldetail'] = $this->Staff_approval_model->getExecutiveDirectorList(17);
    $content['getstafflist'] = $this->Staff_approval_model->getstaffdetailslist($token);
    $content['getsinglestaff'] = $this->Staff_approval_model->get_staff_transfer_promation_detail($token);
    $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($content['getstafflist']->staffid);
    $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
     /*echo "<pre>";
     print_r($personnel_detail);die();*/
    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      


      $btnsubmit = $this->input->post('btnsubmit');
      $btnsend = $this->input->post('btnsend');

      
      if (isset($btnsend) && $btnsend == "AcceptSaveData") {

        $this->db->trans_start();

        $insertArr = array(
         'type'                 => 6,
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           => $token,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 0,
        );


        $this->db->insert('tbl_clearance_certificate', $insertArr);
        $insertid = $this->db->insert_id();

        $name_of_location =  count($this->input->post('name_of_location'));

        for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $insertid,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );

         $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
       }

       $this->db->trans_complete();


      if ($this->db->trans_status() === FALSE){
      $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
      redirect(current_url());
      }else{

        $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
        redirect('/Staff_approval/edit_clearance_certificate/'.$insertid);

      }
    }

    

    if (trim($this->input->post('comments'))) {

      $this->db->trans_start();

      $insertArr = array(
         'type'                 => 6,
        'certify_that'      => $this->input->post('certify_that'),
        'transid'           => $token,
        'createdon'         => date('Y-m-d H:i:s'),
        'createdby'         => $this->loginData->staffid,
        'flag'              => 1,
      );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);
      $insertid = $this->db->insert_id();
      
      $name_of_location =  count($this->input->post('name_of_location'));

      for ($i=0; $i < $name_of_location; $i++) { 
       $insertchiArr = array(
        'clearance_certificate_id'      => $insertid,
        'location'                      => $this->input->post('name_of_location')[$i],
        'description'                   => $this->input->post('description')[$i],
        'item_values'                   => $this->input->post('value')[$i], 

      );

       $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);

     }

      if(trim($this->input->post('comments'))){
        // staff transaction table flag update after acceptance
        /*echo "<pre>";
        print_r($_POST);exit();*/
        $updateArr = array(                         
          'trans_flag'     => $this->input->post('status'),
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 6,
         'staffid'              => $content['getstafflist']->staffid,
         'sender'               => $this->loginData->staffid,
         'receiver'             => $this->input->post('personal_administration'),
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => $this->input->post('status'),
         'scomments'            => $this->input->post('comments'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);


        $receiverdetail = $this->Staff_seperation_model->get_staffDetails($this->input->post('personal_administration'));

        $subject = "Your Process Approved";
        $body = 'Dear,<br><br>';
        $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
        $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
        $body .= 'Thanks<br>';
        $body .= 'Administrator<br>';
        $body .= 'PRADAN<br><br>';

        $body .= 'Disclaimer<br>';
        $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

        
        $to_email = $receiverdetail->emailid;
        $to_name = $receiverdetail->name;
        //$to_cc = $getStaffReportingtodetails->emailid;
        $recipients = array(
           $personnel_detail->EmailID => $personnel_detail->UserFirstName,
           $content['staffdetail']->emailid => $content['staffdetail']->name
           // ..
        );
        $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
        if (substr($email_result, 0, 5) == "ERROR") {
          $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
        }
      }

      /*$insertArr = array(
        'trans_flag' => 9,
      );

      $this->db->where('id',$token);
      $this->db->update('staff_transaction', $insertArr);*/

     $this->db->trans_complete();


     if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
      redirect(current_url());

    }else{

      $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
      redirect('/Staff_approval/view_clearance_certificate/'.$insertid);

    }
  }

}



$content['title'] = '/add_clearance_certificate';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function edit_clearance_certificate($token)
{
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $getstaffcclist = $this->Staff_approval_model->getstaffclerancelist($token);
  $transid = $getstaffcclist->transid; 
  
  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){
      // echo "<pre>";
      // print_r($this->input->post()); die;


    $btnsubmit = $this->input->post('btnsubmit');
    $btnsend = $this->input->post('btnsend');


    if (isset($btnsend) && $btnsend == "AcceptSaveData") {

      $this->db->trans_start();

      $insertArr = array(
        'certify_that'      => $this->input->post('certify_that'),
        'transid'           =>  $transid,
        'createdon'         => date('Y-m-d H:i:s'),
        'createdby'         => $this->loginData->staffid,
        'flag'              => 0,
      );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);

      $insertid = $this->db->insert_id();
      
      $name_of_location =  count($this->input->post('name_of_location'));

      $this->db->delete('tbl_clearance_certificate_transaction',array('clearance_certificate_id'=> $token));

      for ($i=0; $i < $name_of_location; $i++) { 
       $insertchiArr = array(
        'clearance_certificate_id'      => $token,
        'location'                      => $this->input->post('name_of_location')[$i],
        'description'                   => $this->input->post('description')[$i],
        'item_values'                   => $this->input->post('value')[$i], 

      );

       $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
     }
     $this->db->trans_complete();


     if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
      redirect(current_url());

    }else{

      $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
      redirect('/Staff_approval/edit_clearance_certificate/'.$insertid);

    }
  }



  if (isset($btnsubmit) && $btnsubmit == "AcceptSendData") {

    $this->db->trans_start();

    $insertArr = array(
      'certify_that'      => $this->input->post('certify_that'),
      'transid'           =>  $transid,
      'createdon'         => date('Y-m-d H:i:s'),
      'createdby'         => $this->loginData->staffid,
      'flag'              => 1,
    );

    $this->db->where('id',$token);
    $this->db->update('tbl_clearance_certificate', $insertArr);
    $insertid = $this->db->insert_id();

    $name_of_location =  count($this->input->post('name_of_location'));

    $this->db->delete('tbl_clearance_certificate_transaction',array('clearance_certificate_id'=> $token));


    for ($i=0; $i < $name_of_location; $i++) { 

     $insertchiArr = array(
      'clearance_certificate_id'      => $token,
      'location'                      => $this->input->post('name_of_location')[$i],
      'description'                   => $this->input->post('description')[$i],
      'item_values'                   => $this->input->post('value')[$i], 

    );

     $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   }

   $insertArr = array(
    'trans_flag'              => 9,
  );

   $this->db->where('id',$transid);
   $this->db->update('staff_transaction', $insertArr);


   $this->db->trans_complete();
   if ($this->db->trans_status() === FALSE){

    $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
    redirect(current_url());

  }else{

    $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
    redirect('/Staff_approval/view_clearance_certificate/'.$token);

  }
}



}




$content['getstaffcertify_that'] = $getstaffcclist;
$content['getcountstaffitemslist'] = $this->Staff_approval_model->getcountstaffitmsclerancelist($token);
$content['getstaffitemslist'] = $this->Staff_approval_model->getstaffitmsclerancelist($token);

$content['getstafflist'] = $this->Staff_approval_model->getSinglestaffdetailslist($transid);

$content['title'] = '/edit_clearance_certificate';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




public function view_clearance_certificate($token)
{
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 


  $getstaffcclist = $this->Staff_approval_model->getstaffclerancelist($token);
  $transid = $getstaffcclist->transid;
  $content['getstaffcertify_that'] = $getstaffcclist;

  $content['getstafflist'] = $this->Staff_approval_model->getSinglestaffdetailslist($transid);


  $content['getcountstaffitemslist'] = $this->Staff_approval_model->getcountstaffitmsclerancelist($token);
  $content['getstaffitemslist'] = $this->Staff_approval_model->getstaffitmsclerancelist($token);




  $content['title'] = '/view_clearance_certificate';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  
}



public function add_joining_report($token)
{


  try{
  $this->load->model('Staff_approval_model');

  $getStaffjoningplace = $this->Staff_approval_model->getStaffjoningplace($token);
   // print_r($getStaffjoningplace); die;

  $transid = $getStaffjoningplace->transid;



  $RequestMethod = $this->input->server('REQUEST_METHOD'); 

  if($RequestMethod == "POST"){
      // echo "sdfdsf";
      // print_r($this->input->post()); die;

    $savebtn = $this->input->post('savebtn');
    $submitbtn = $this->input->post('submitbtn');

    $reporteddutyon = $this->input->post('reporteddutyon');

    $reported_duty_on = $this->gmodel->changedatedbformate($reporteddutyon);

    if (isset($savebtn) && $savebtn =='senddatasave') {

      $this->db->trans_start();

      $insertArr = array(
       'reported_for_duty_date'   => $reported_duty_on,
       'assigned'                 =>  $this->input->post('assignedto'),
       'location'                 => $this->input->post('location'),
       'supervision_staffid'      => $this->loginData->staffid,
       'updatedon'                => date('Y-m-d H:i:s'),
       'updatedby'                => $this->loginData->staffid,
     );

      $this->db->where('id', $token);
      $this->db->update('tbl_joining_report_new_place', $insertArr);

      $insertArr = array(
       'trans_flag'  => 11,
     );
      $this->db->where('id', $transid);
      $this->db->update('staff_transaction', $insertArr);

      $insertid = $this->db->insert_id();

      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
        redirect(current_url());

      }else{

        $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
        redirect('/Staff_approval/view_joining_report/'.$token);

      }
    }





  }

  $content['getstaffdetailslist'] = $this->Staff_approval_model->getstaffdetailslist1($token);

  $content['token'] = $token;

  $content['supervisionname'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.''.$this->loginData->UserLastName;

  $content['title'] = 'Staff_approval/add_joining_report';

  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}



public function edit_joining_report($token)
{

  try{

  $this->load->model('Staff_approval_model');

  

  $RequestMethod = $this->input->server('REQUEST_METHOD'); 

  if($RequestMethod == "POST"){
      // echo "sdfdsf";
      // print_r($this->input->post()); die;

    $savebtn = $this->input->post('savebtn');
    $submitbtn = $this->input->post('submitbtn');
    

    $staffdutytodaydate = $this->input->post('staff_duty_today_date');

    $staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);

    if (isset($savebtn) && $savebtn =='senddatasave') {

      $this->db->trans_start();

      $insertArr = array(
        'staff_duty_today_date'      => $staff_duty_today_date,
        'transid'           =>  $token,
        'flag'               => 0,
        'createdon'         => date('Y-m-d H:i:s'),
        'createdby'         => $this->loginData->staffid,
      );

      $this->db->where('id', $token);
      $this->db->update('tbl_joining_report_new_place', $insertArr);
         // $insertid = $this->db->insert_id();

      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
        redirect(current_url());

      }else{

        $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
        redirect('/Joining_report_of_newplace_posting/edit_joining_report/'.$token);

      }
    }



    if (isset($submitbtn) && $submitbtn =='senddatasubmit') {


      $this->db->trans_start();

      $insertArr = array(
        'staff_duty_today_date'      => $staff_duty_today_date,
        'transid'           =>  $token,
        'flag'               => 1,
        'createdon'         => date('Y-m-d H:i:s'),
        'createdby'         => $this->loginData->staffid,
      );
      
      $this->db->where('id', $token);
      $this->db->update('tbl_joining_report_new_place', $insertArr);

      $insertArr = array(
       'trans_flag'      => 10,
     );

      $this->db->where('id', $token);
      $this->db->update('staff_transaction', $insertArr);


      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
        redirect(current_url());

      }else{

        $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
        redirect('/Joining_report_of_newplace_posting/view_joining_report/'.$token);

      }
    }

  }


  $content['getstaffjopin'] = $getStaffjoningplace;

  $content['getstaffdetailslist'] = $this->Staff_approval_model->getstaffdetailslist1($token);

  $content['token'] = $token;

  $content['title'] = 'Staff_approval/edit_joining_report';

  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




public function view_joining_report($token)
{

  try{
  $this->load->model('Staff_approval_model');

  $RequestMethod = $this->input->server('REQUEST_METHOD'); 

  if($RequestMethod == "POST"){


  }

  $getStaffjoningplace = $this->Staff_approval_model->getStaffjoningplace($token);
   // print_r($getStaffjoningplace); die;

  $transid = $getStaffjoningplace->transid;

  $content['getstaffjopin'] = $getStaffjoningplace;

  $content['getstaffdetailslist']  = $this->Staff_approval_model->getstaffdetailslist1($transid);
  $content['supervisionname'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.''.$this->loginData->UserLastName;



  $content['title'] = 'Staff_approval/view_joining_report';

  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function approvemodal(){

  try{
    if(trim($this->input->post('comments'))){
      $token = $this->input->post('receiverstaffid');

      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
      $query ="SELECT * FROM staff_transaction WHERE id =".$token;
      $content['staff_transaction'] = $this->db->query($query)->row();
      $content['staffid'] = $content['staff_transaction']->staffid;;
      $content['reportingto'] = $content['staff_transaction']->reportingto;
      $result  = $this->db->query($sql)->result()[0];
      $forwardworkflowid = $result->workflowid;
      $staffid = $result->staffid;
            // staff transaction table flag update after acceptance
      $updateArr = array(                         
        'trans_flag'     => 19,
        'updatedon'      => date("Y-m-d H:i:s"),
        'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 6,
         'staffid'              => $content['staffid'],
         'sender'               => $this->loginData->staffid,
         'receiver'             => $content['reportingto'],
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => 19,
         'scomments'            => $this->input->post('comments'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
      $flag = $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
    }
    if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
      redirect('/Staff_approval');
    } 
    else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
      redirect('/Staff_approval');
    }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


public function transferapprovemodal(){

  try{
    if(trim($this->input->post('comments_transfer'))){
      $token = $this->input->post('receiverstaffid_transfer');

      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
      $query ="SELECT * FROM staff_transaction WHERE id =".$token;
      $content['staff_transaction'] = $this->db->query($query)->row();
      $content['staffid'] = $content['staff_transaction']->staffid;;
      $content['reportingto'] = $content['staff_transaction']->reportingto;
      $result  = $this->db->query($sql)->result()[0];
      $forwardworkflowid = $result->workflowid;
      $staffid = $result->staffid;
      $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($staffid);
      $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
      $receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['reportingto']);
            // staff transaction table flag update after acceptance
      $updateArr = array(                         
        'trans_flag'     => 19,
        'updatedon'      => date("Y-m-d H:i:s"),
        'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 2,
         'staffid'              => $content['staffid'],
         'sender'               => $this->loginData->staffid,
         'receiver'             => $content['reportingto'],
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => 19,
         'scomments'            => $this->input->post('comments_transfer'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
      $flag = $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
    }
    if($flag) {

        $subject = "Your Process Approved";
        $body = 'Dear,<br><br>';
        $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
        $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
        $body .= 'Thanks<br>';
        $body .= 'Administrator<br>';
        $body .= 'PRADAN<br><br>';

        $body .= 'Disclaimer<br>';
        $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

        
        $to_email = $receiverdetail->emailid;
        $to_name = $receiverdetail->name;
        //$to_cc = $getStaffReportingtodetails->emailid;
        $recipients = array(
           $personnel_detail->EmailID => $personnel_detail->UserFirstName,
           $content['staffdetail']->emailid => $content['staffdetail']->name
           // ..
        );
        $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
        if (substr($email_result, 0, 5) == "ERROR") {
          $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
        }
      $this->session->set_flashdata('tr_msg','Data Saved Successfully.');
      redirect('/Staff_approval');
    } 
    else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
      redirect('/Staff_approval');
    }

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


}