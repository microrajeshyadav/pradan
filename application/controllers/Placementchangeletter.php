<?php 
class Placementchangeletter extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->model("Assigntc_model");
		//$this->load->model("Dompdf_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		

		$this->load->model(__CLASS__ . '_model','model');
		//$mod = $this->router->class.'_model'; 
	        //$this->load->model($mod,'',TRUE);
		//$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{

		 // start permission 
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		try{


			if($this->input->post('campusid') != Null && $this->input->post('campusid') !=''){



				$campusdata = $this->input->post('campusid');
				$campexp = explode('-', $campusdata);

				$campusid = $campexp[0];
				$campusintimationid = $campexp[1];

				$content['selectedcandidatedetails']  = $this->model->getSelectedCandidate($campusid,$campusintimationid);
				$content['campusdetails'] 	          = $this->model->getCampus();

				$content['campusid'] = $campusdata;
 			/*echo "<pre>";
 			print_r($content['selectedcandidatedetails']);exit();*/

 		}else{
 			$campusid ='NULL';
 			$campusintimationid = 'NULL';
 			$content['selectedcandidatedetails']='';
 			$content['campusdetails'] 	          = $this->model->getCampus();
 		}




 		$content['title'] 		= 'Placementchangeletter';
 		$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 		$this->load->view('_main_layout', $content);

 	}catch (Exception $e) {
 		print_r($e->getMessage());die;
 	}

 }



 public function add($token=NULL){

 	try{

 		$getstaffid = '';
 		$teameFGdetail = '';
 		$teamsdetails = '';
 		$getcategory = '';
 		$getcategory = '';
 		$transid = '';
 		$officeid= '';
 		$ExpectedjoinDate = '';
 		$effective_date = '';
 		$changedatedbformateeffectivedate = '';
	    $supervisor  = '';           
	    $apprentice   = '';          
	    $newsupervisor  = '';        
	    $newsupervisorcontactno  = '';
	    $newsupervisoremailid   = '';
	    $oldsupervisoremailid   = ''; 


 		if (empty($token) && $token=='') {

 			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
 			redirect('/Staff_list/index');

 		}else{


 			$content['stafftransdetail']  = $this->model->getstafftransdetail($token);
	//	print_r($content['stafftransdetail']); 

			if (count($content['stafftransdetail'])==0) {
				$this->session->set_flashdata('er_msg', 'Required Array is either blank or empty.');
 			   redirect('/Staff_list/index');
			}else{
		

 			$transid = $content['stafftransdetail']->id;
 			$officeid = $content['stafftransdetail']->new_office_id;


			$content['getreportingto']   = $this->model->getReportingto($officeid);

 			$content['statedetail']  = $this->model->getstatedetail($content['stafftransdetail']->permanentstateid);
 			$content['districtdetail']  = $this->model->getdistrictdetail($content['stafftransdetail']->permanentdistrict);

 			//print_r($content['districtdetail']); die;

 			$tablename = "'staff_transaction'";
 			$incval = "'@a'";

 			$getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

 			$autoincval =  $getstaffincrementalid->maxincval;


 			$RequestMethod = $this->input->server('REQUEST_METHOD');

 			if($RequestMethod == 'POST'){
			   
			  // print_r($this->input->post());


				$this->db->trans_start();

 				$officeid = $this->input->post('team');

				$effective_date = $this->input->post('effective_date');

				$changedatedbformateeffectivedate = $this->gmodel->changedatedbformate($effective_date);

				$sql = "SELECT * FROM staff_transaction WHERE staffid =".$token." AND  date_of_transfer = '".date('Y-m-d')."'";
				 $result = $this->db->query($sql)->row();

				  // if (count($result) > 0) {
				  	
				  // 	$this->session->set_flashdata('er_msg', ' Sorry !!! Today you are send placement change letter, Please try next day.');			
 					// redirect('/Staff_list/index');

				  // }else{
 				

 				$this->form_validation->set_rules('team','Team ','trim|required');

 				$this->form_validation->set_rules('fieldguide','Field Guide ','trim|required');
 				$this->form_validation->set_rules('effective_date','Effective Date','trim|required');

 				if($this->form_validation->run() == FALSE){

 					$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',	'</div>');

 					$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

 					$hasValidationErrors    =    true;
 					goto prepareview;

 				}


 				$insertArr = array(
 					'staffid'          => $token,
 					'old_office_id'    => $content['stafftransdetail']->new_office_id,
 					'new_office_id'    => $officeid,
 					'date_of_transfer' => date('Y-m-d'),
 					'old_designation'  => $content['stafftransdetail']->designation,
 					'new_designation'  => $content['stafftransdetail']->designation,
 					'fgid'             => $this->input->post('fieldguide'),
 					'reportingto'      => $this->input->post('reportingto'),
 					'trans_status'     => 'PCL',
 					'id'               => $autoincval,
 					'createdon'        => date('Y-m-d H:i:s'),
 					'createdby'        => $this->loginData->staffid,
 					'datetime'         => date('Y-m-d H:i:s'),
 					'effective_date'   => $changedatedbformateeffectivedate,
 				);

 				$this->db->insert('staff_transaction', $insertArr);

 				$updateArr = array(
 					'new_office_id'     => $officeid,
 					'fgid'              => $this->input->post('fieldguide'),
 					'reportingto'       => $this->input->post('reportingto'),
 				);
 				$this->db->where('staffid',$token);
 				$this->db->update('staff', $updateArr);

 				$this->db->trans_complete();

 				  $gethrddetails = $this->model->getHRunitDetails();

 				  $placementchangedata = $this->model->getdachangeplacementdetail($token);

 				  $ExpectedjoinDate = date('F j, Y', strtotime($placementchangedata->effective_date));
 				  $d_o_j = date('F j,Y');

 				  $staff = array('$stafftransdetail','stafftransdetail_name','$d_o_j','$stafftransdetail_permanenthno','$stafftransdetail_permanentcity','$districtdetail_name','$statedetail_name','$stafftransdetail_permanentpincode','$placementchangedataoldofficename','$placementchangedataolddcname','$placementchangedatanewofficename','$placementchangedatanewdcname','$placementchangedatafgname','$placementchangedatafgdesname','$placementchangedatanewsuperwisername','$ExpectedjoinDate','$placementchangedatastreet','$placementchangedataofficedistrictname','$placementchangedataofficestate','$gethrddetailsname','$gethrddetailsdesname');

 				  $staff_replace = array($content['stafftransdetail']->id,$content['stafftransdetail']->name,$d_o_j,$content['stafftransdetail']->permanenthno,$content['stafftransdetail']->permanentcity,$content['districtdetail']->name,$content['statedetail']->name,$content['stafftransdetail']->permanentpincode,$placementchangedata->oldofficename,$placementchangedata->olddcname,$placementchangedata->newofficename,$placementchangedata->newdcname,$placementchangedata->fgname,$placementchangedata->fgdesname,$placementchangedata->newsuperwisername,$ExpectedjoinDate,$placementchangedata->street,$placementchangedata->officedistrictname,$placementchangedata->officestate,$gethrddetails->name,$gethrddetails->desname);

 
 				// $html='<table width="100%" border="0" cellspacing="0" cellpadding="0">
 				// 	<tr>
 				// 	<td width="4%">&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td width="3%">&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td width="16%">$stafftransdetail_id</td>
 				// 	<td width="29%">&nbsp;</td>
 				// 	<td width="17%">&nbsp;</td>
 				// 	<td width="31%">$d_o_j</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td><p> $stafftransdetail_name<br />  $stafftransdetail_permanenthno $stafftransdetail_permanentstreet $stafftransdetail_permanentcity, $districtdetail_name <br />$statedetail_name- $stafftransdetail_permanentpincode
 					
 				// 	</p></td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4"><p align="center"><em>Subject. </em>Change of Placement Letter </p></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td><p>Dear $stafftransdetail_name ,</p></td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4"><p>Your placement has beem changed from $placementchangedata->oldofficename team in $placementchangedata->olddcname to $placementchangedata->newofficename in $placementchangedata->newdcname. Your field guide will be $placementchangedata->fgname, $placementchangedata->fgdesname. You are requested to report to $placementchangedata->newsuperwisername on before $ExpectedjoinDate. </p></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4"><p>Our  $placementchangedata->newofficename  office address is : </p></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>PRADAN</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td> $placementchangedata->street,  $placementchangedata->officedistrictname, $placementchangedata->officestate </td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4"><p>Looking forward to a long  assoclation with you,</p></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td colspan="4">&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td><div align="right">Yours sincerely,</div></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td><p align="right"> $gethrddetails->name  </p></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td><div align="right"> $gethrddetails->desname </div></td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	<tr>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	<td>&nbsp;</td>
 				// 	</tr>
 				// 	</table>';

 					//echo $html; die;

 					$filename = 'Placementchangeletter_'.$token.'_'.md5(time() . rand(1,1000));

 			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 32 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
   			if(!empty($data))
   		    $body = str_replace($staff,$staff_replace , $data->lettercontent);
   		
					$this->load->model('Dompdf_model');
					$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Placementchangeletter.pdf');

				    $pdffilename = $filename.'.pdf';
					$attachments = array($pdffilename);
					$to_email = $placementchangedata->emailid;

					if ($generate == true) {

						 $supervisor              = $placementchangedata->oldsupervisor;
						 $apprentice              = $content['stafftransdetail']->name;
						 $newsupervisor           = $placementchangedata->newsuperwisername;
						 $newsupervisorcontactno  = $placementchangedata->newsupervisercontcat;
						 $newsupervisoremailid    = $placementchangedata->newsupervisernameemailid;
						 $oldsupervisoremailid    = $placementchangedata->oldsupervisoremailid;
						 $hrdname                 = $gethrddetails->name;
						 $hrddesignation          = $gethrddetails->desname;
						 $hrdrole                 = $gethrddetails->rolename;

						$subject = "Change of Placement Letter";

			$candidate = array('$supervisor','$newsupervisor','$newsupervisoremailid','$hrdname','$hrddesignation','$hrdrole');
			$candidate_replace = array($supervisor,$newsupervisor,$newsupervisoremailid,$hrdname,$hrddesignation,$hrdrole);
					 	// $fd = fopen("mailtext/Change_of_placement_letter.txt", "r"); 
					 	// $message .=fread($fd,4096);
					 	// eval ("\$message = \"$message\";");
					 	// $message =nl2br($message);
					 	// $body = $message;

     					$to_email  = $oldsupervisoremailid;

     		$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 33 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
   			if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

			 		$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name = null,$recipients=null, $attachments);

					}


 				if ($this->db->trans_status() === FALSE){
 					$this->session->set_flashdata('er_msg', 'Error Assign Teams');	
 				}else{


 					$this->session->set_flashdata('tr_msg', 'Successfully send Change placement letter ');			
 				}

 				redirect('/Staff_list/index');
 			//}

 		}
 	}

 			prepareview:

 			$content['teamsdetails']     = $this->model->getTeamlist();
 			$content['getstaffid']       = $this->model->getStaffDetails($token);
 			$content['teameFGdetail']     = $this->model->getTeamFGDetail($officeid);
 			
 			$content['method']            = $this->router->fetch_method();
 			$content['title']  = 'add';
 			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 			$this->load->view('_main_layout', $content);
 		}
 	}catch (Exception $e) {
 		print_r($e->getMessage());die;
 	}

 }


 



 public function chooseFieldguideteam($id) {

try{
 	$sql = "SELECT `staff`.staffid, `staff`.name FROM staff Where staff.`staffid`='".$id."'";  

 	$result = $this->db->query($sql)->result();

 	return $result;

 	}catch (Exception $e) {
 		print_r($e->getMessage());die;
 	}
 }


}