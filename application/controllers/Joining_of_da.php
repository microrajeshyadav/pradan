<?php 
class Joining_of_da extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');

		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
     		 redirect('login');
		}

	}

	public function index()
	{
		try{

		
		$hremailid = '';
		$paemailid = '';
		$tcemailid = '';
		$this->load->model('Joining_of_da_model');

		
	
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
	//	
		
		if($RequestMethod == "POST"){

			
		
		 
		$candidatename = $this->input->post('candidate_team'); 



			
			  foreach ($candidatename as $key => $value) {
			  

			  		//die;

			  $Selected_candidateDetails = $this->model->selectedCandidateDetails($value);

			  
			 $candidatefname ='';
					  
			  $candidatefname    =  $Selected_candidateDetails->candidatefirstname;
				
			  $candidateemailid  =  $Selected_candidateDetails->emailid;
			  $candidateusername = $Selected_candidateDetails->username;
			  $candidatepassword = $Selected_candidateDetails->password;
			 
		
			  $teamid='';
			   $teamid=$Selected_candidateDetails->teamid;
			  $Selected_tcDetails = $this->model->tc_data( $teamid);
			   $supervisor_name=$Selected_tcDetails->name;
			 
			
            	$joinarraydata = array(
			 	'joinstatus'  => 2,
			 );

			$this->db->where('candidateid', $value);
      		$this->db->update('tbl_candidate_registration', $joinarraydata);

      		$gethremail = $this->gmodel->getHRDEmailid(); /// Get hr Email Id at mstuser table 

				//print_r($gethremail); die;

				
				$getpaemailid = $this->gmodel->getPersonnelAdministratorEmailid(); /// Get personnel Admin Email Id at mstuser table 
	 			 $hremailid      = $gethremail->hrdemailid;
				 $paemailid      = $getpaemailid->personnelemailid;
				 $tcemailid      = $this->loginData->EmailID;
				 $candidateemail = $candidateemailid;
				 $addlink = site_url('login/login1');
				 $message="";
				 $subject = "Login details and joining formalities";

				 $candidate = array('$candidatefname','$candidateemailid','$addlink','$supervisor_name');
				 $candidate_replace = array($candidatefname,$candidateemailid,$addlink,$supervisor_name);
				 // $message=  'Dear '.$candidatefname.', <br><br> ';
				 // $body .= 'DA Join Successfully <br><br>';
				 // $body .= 'Please fill general nomination form and joining report form <br><br>';
				 // $body .= '<b>Username :- </b>'.$candidateemailid.'<br>';
				 // $body .= '<b>Password :- </b>'.$candidatefname.'<br>';
				 // $body .= 'Please <a href='.$addlink.'>Click here</a><br><br>';
				//  $data="hello";
				// // $body.=write_file(base_url().'mailtext/verifieddocumentmailtext', $data);
				//  $file=base_url().'mailtext/verifieddocumentmailtext';
				//  $body .= read_file(base_url().'mailtext/verifieddocumentmailtext');

// 			
			 // $fd = fopen("mailtext/joining.txt", "r");	
				// 		$message .=fread($fd,4096);
				// 		eval ("\$message = \"$message\";");
				// 		$message =nl2br($message);
						

			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 29 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
   			if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);	
				
				 // $to_email = $gethremail->hrdemailid;
				 // $to_name = $candidateemail;
   		// 	     $recipients= array (
     //                $tcemailid =>'TC',
     //                $paemailid =>'PA',
     //                $hremailid => 'HR',
     //            );

   		$to_email = $candidateemailid;

               $sendmail = $this->Common_Model->send_email($subject, $body,$to_email ,$to_name=null, $recipients=null);
               // echo $sendmail; die();
      	}
 		
		    $this->db->trans_complete();

		    if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', $this->db->error());	
			}else{
				
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Join');			
			}
	
		}

		 $content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($this->loginData->teamid);
		$content['title'] = 'Joining_of_da';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


 public function getTotalEmployees($candidateid){

   try{

   	//echo $candidateid; die;

    	$query = $this->db->query("SELECT max(emp_code) as emp_code  FROM `tbl_da_personal_info`"); 
    	$result = $query->result()[0];
    	// print_r($result);
    	// echo $result->emp_code; die;
    	if($result->emp_code ==''){
    		$num ='0001';
    	}else{
    		 $num = $result->emp_code + 1;
    		 $len = strlen($num);
		    for($i=$len; $i < 4; $i++) {
		        $num = '0'.$num;
		    }
    	}
    	// echo $num; die;
    	
	    return $num;

        }catch (Exception $e) {
          print_r($e->getMessage());die;
    }


 }




}