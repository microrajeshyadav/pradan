<?php 
class Candidatesinfo extends CI_Controller
{
	
	function __construct()
	{

		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model('Candidatesinfo_model','model');
		$this->load->model("Global_Model","gmodel");
	 //       $this->load->model($mod,'',TRUE);
		// $this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');


		
		
	}

	

	public function index()
	{
		try{
		$content['selectedcandidatedetails'] = $this->getSelectedCandidate();

		$content['title'] = 'Candidatesinfo';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function view($token)
	{
		try{
		
		//$content['selectedcandidatedetails'] = $this->model->getViewSelectedCandidate($token);
		$content['title'] = 'Candidateinfo';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function information()
	{
		try{
		
		$content['selectedcandidatedetails'] = $this->getSelectedCandidate();
		$content['statedetails']        = $this->getState();
		$content['ugeducationdetails']  = $this->model->getUgEducation();
		$content['pgeducationdetails']  = $this->model->getPgEducation();
		$content['title'] = 'Candidatesinfo';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	
	public function add($token){


		try{

		    $token_decoded_id=base64_decode(urldecode($token));  /// Decode Token Id 
		   // print_r($token_decoded_id); die;
		    $expcampid = explode('/',$token_decoded_id);
		    $campusdecodeid = $expcampid[0];
		    $campusintimationid = $expcampid[1];
		    $campuscatid = $expcampid[2];

		    // print_r($campuscatid); die;


		    $urlexistExist = $this->is_urlexist($campusdecodeid, $campusintimationid);
		 	
		 if ($urlexistExist==0) { 
		  redirect('http://www.pradan.net/');

		 }else{

		    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

		    if($RequestMethod == 'POST'){
		    	// echo "<pre>";
		    	// print_r($this->input->post());
		    	// die;
		    	$emailid = $this->input->post("emailid");
		    	$emailidExist = $this->is_exists($emailid);

		    	$this->form_validation->set_rules('candidatefirstname','Candidate First Name','trim|required');
		    	$this->form_validation->set_rules('candidatefirstname','Candidate Last Name','trim|required');
		    	$this->form_validation->set_rules('emailid','Emailid','trim|required|valid_email');

		    	$this->form_validation->set_rules('motherfirstname','Mother First Name','trim|required');
		    	$this->form_validation->set_rules('motherlastname','Mother Last Name','trim|required');
		    	$this->form_validation->set_rules('fatherfirstname','Father First Name','trim|required');
		    	$this->form_validation->set_rules('fatherlastname','Father First Name','trim|required');
		    	$this->form_validation->set_rules('dateofbirth','Date Of Birth','trim|required');
		    	$this->form_validation->set_rules('mobile','Mobile','trim|required|max_length[10]|numeric');

		    	$this->form_validation->set_rules('10thschoolcollegeinstitute','School/ College/ Institute','trim|required');
		    	$this->form_validation->set_rules('10thboarduniversity','Board/ University','trim|required');
		    	$this->form_validation->set_rules('10thpassingyear','Year','trim|required|min_length[4]|max_length[4]|numeric');
		    	$this->form_validation->set_rules('10thplace','Place','trim|required');
		    	$this->form_validation->set_rules('10thspecialisation','Specialisation ','trim|required');
		    	$this->form_validation->set_rules('10thpercentage','Percentage','trim|required|min_length[2]|max_length[6]|numeric');
		    	$this->form_validation->set_rules('12thschoolcollegeinstitute','School/ College/ Institute','trim|required');
		    	$this->form_validation->set_rules('12thboarduniversity','Board/ University','trim|required');
		    	$this->form_validation->set_rules('12thpassingyear','Year','trim|required|min_length[4]|max_length[4]|numeric');
		    	$this->form_validation->set_rules('12thplace','Place','trim|required');
		    	$this->form_validation->set_rules('12thspecialisation','Specialisation ','trim|required');
            //$this->form_validation->set_rules('hscstream ','Stream ','trim|required|min_length[2]|max_length[50]');
		    	$this->form_validation->set_rules('12thpercentage','Percentage','trim|required|min_length[2]|max_length[6]|numeric');
		    	$this->form_validation->set_rules('ugschoolcollegeinstitute','School/ College/ Institute','trim|required');
		    	$this->form_validation->set_rules('ugboarduniversity','Board/ University','trim|required');
		    	$this->form_validation->set_rules('ugpassingyear','Year','trim|required|min_length[4]|max_length[4]|numeric');
		    	$this->form_validation->set_rules('ugplace','Place','trim|required');
		    	$this->form_validation->set_rules('ugspecialisation','Specialisation ','trim|required');
           // $this->form_validation->set_rules('ugdegree','Under Graduate','trim|required');
		    	$this->form_validation->set_rules('ugpercentage','Percentage','trim|required|min_length[2]|max_length[6]|numeric');
		    	$this->form_validation->set_rules('presenthno','Present House Number','trim|required');
		    	$this->form_validation->set_rules('permanenthno','Permanent House Number','trim|required');
		    	$this->form_validation->set_rules('presentstreet','Present Street','trim|required');
		    	$this->form_validation->set_rules('presentcity','Present city','trim|required');
		    	$this->form_validation->set_rules('presentstateid','Present State','trim|required');
		    	$this->form_validation->set_rules('presentdistrict','Present District','trim|required');
		    	$this->form_validation->set_rules('presentpincode','Present Pincode','trim|required|max_length[6]|numeric');
		    	$this->form_validation->set_rules('permanentstreet','Permanent Street','trim|required');

		    	$this->form_validation->set_rules('presentcity','Present City','trim|required');
		    	$this->form_validation->set_rules('permanentcity','Permanent City','trim|required');
		    	$this->form_validation->set_rules('permanentstateid','Permanent State','trim|required');
		    	$this->form_validation->set_rules('permanentdistrict','Permanent District','trim|required');
		    	$this->form_validation->set_rules('permanentpincode','Permanent Pincode','trim|required|max_length[6]|numeric');

		    	if($this->form_validation->run() == FALSE){
		    		$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
		    			'</div>');

		    		$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

		    		$hasValidationErrors    =    true;
		    		goto prepareview;

		    	}
			
		    	if($emailidExist ==1){
		    		$this->session->set_flashdata('er_msg', 'email id allready exist, please choose other email id ');
		    	
		    	}else{

		    	
		    	 $dateofbirth1 = $this->input->post('dateofbirth'); 

		    	 $date_of_birth = $this->changedatedbformate($dateofbirth1); 

		    	$this->db->trans_start();

		    	

		    	    $pgstream='';
		    		$pgpassingyear='';
		    		$pgschoolcollegeinstitute='';
		    		$pgboarduniversity='';
		    		$pgplace='';

		    		 $pgstream=$this->input->post('pgpassingyear');

		    		if($pgstream=='' || $pgstream==NULL || empty($pgstream))
		    		{
		    			$pgpassingyear='';
		    			$pgschoolcollegeinstitute='';
		    			$pgboarduniversity='';
		    			$pgspecialisation='';
		    			$pgpercentage='';
		    			$pgplace='';
		    		}
		    		else
		    		{
		    			$pgpassingyear=$this->input->post("pgpassingyear");
		    			$pgschoolcollegeinstitute=$this->input->post('pgschoolcollegeinstitute');
		    			$pgboarduniversity=$this->input->post("pgboarduniversity");
		    			$pgspecialisation=$this->input->post("pgspecialisation");
		    			$pgpercentage=$this->input->post("pgpercentage");
		    			$pgplace=$this->input->post('pgplace');
		    		}

		    		$specify_degree='';
		    		$otherpassingyear='';
		    			$otherplace='';
		    			$otherschoolcollegeinstitute='';
		    			$otherboarduniversity='';
		    			$otherspecialisation='';
		    			$otherpercentage='';
		    			$specify_degree=$this->input->post('specify');
		   
		    		if($specify_degree=='' ||$specify_degree==NULL ||empty($specify_degree))
		    		{
		    			$otherpassingyear='';
		    			$otherplace='';
		    			$otherschoolcollegeinstitute='';
		    			$otherboarduniversity='';
		    			$otherspecialisation='';
		    			$otherpercentage='';
		    		}
		    		else 
		    		{
		    			$otherpassingyear=$this->input->post("otherpassingyear");
		    			$otherplace=$this->input->post('otherplace');
		    			$otherschoolcollegeinstitute=$this->input->post('otherschoolcollegeinstitute');
		    			$otherboarduniversity=$this->input->post("otherboarduniversity");
		    			$otherspecialisation=$this->input->post("otherspecialisation");
		    			$otherpercentage=$this->input->post("otherpercentage");
		    		}
		  

		    	$insertArrCandidate = array(
		    		'candidatefirstname '           => $this->input->post('candidatefirstname'),
		    		'candidatemiddlename'           => $this->input->post("candidatemiddlename"),
		    		'candidatelastname'  	        => $this->input->post('candidatelastname'),
		    		'motherfirstname '    	        => $this->input->post('motherfirstname'),
		    		'mothermiddlename'              => $this->input->post("mothermiddlename"),
		    		'motherlastname'  	            => $this->input->post('motherlastname'),
		    		'fatherfirstname '    	        => $this->input->post('fatherfirstname'),
		    		'fathermiddlename'              => $this->input->post("fathermiddlename"),
		    		'fatherlastname'  	            => $this->input->post('fatherlastname'),
		    		'gender'    		            => $this->input->post('gender'),
		    		'nationality'       	        => $this->input->post("nationality"),
		    		'maritalstatus'  	            => $this->input->post('maritalstatus'),
		    		'dateofbirth '    	            => $date_of_birth,
		    		'emailid'       	            => $this->input->post("emailid"),
		    		'mobile'  		    		    => $this->input->post('mobile'),
		    		'metricschoolstream'  			=> $this->input->post('tenthstream'),
		    		'metricschoolcollege '          => $this->input->post('10thschoolcollegeinstitute'),
		    		'metricboarduniversity'         => $this->input->post("10thboarduniversity"),
		    		'metricpassingyear'  	        => $this->input->post('10thpassingyear'),
		    		'metricplace '    				=> $this->input->post('10thplace'),
		    		'metricspecialisation'       	=> $this->input->post("10thspecialisation"),
		    		'metricpercentage'  			=> $this->input->post('10thpercentage'),
		    		'hscschoolcollege '    		    => $this->input->post('12thschoolcollegeinstitute'),
		    		'hscboarduniversity'       	    => $this->input->post("12thboarduniversity"),
		    		'hscpassingyear'  			    => $this->input->post('12thpassingyear'),
		    		'hscplace '    				    => $this->input->post('12thplace'),
		    		'hscspecialisation'       		=> $this->input->post("12thspecialisation"),
		    		'hscpercentage'  			    => $this->input->post('12thpercentage'),
		    		'ugschoolcollege '    			=> $this->input->post('ugschoolcollegeinstitute'),
		    		'ugboarduniversity'       		=> $this->input->post("ugboarduniversity"),
		    		'ugpassingyear'  			    => $this->input->post('ugpassingyear'),
		    		'ugplace '    					=> $this->input->post('ugplace'),
		    		'ugspecialisation'       		=> $this->input->post("ugspecialisation"),
		    		'ugpercentage'  			    => $this->input->post('ugpercentage'),
		    			'pgschoolcollege '    			=> $pgschoolcollegeinstitute,
		 				'pgboarduniversity'       		=> $pgboarduniversity,
		 				'pgpassingyear'  			    =>$pgpassingyear,
		 				'pgplace '    					=> $pgplace,
		 				'pgspecialisation'       		=> $pgspecialisation,
		 				'pgpercentage'  			    => $pgpercentage,
		 				'otherschoolcollege '    		=> $otherschoolcollegeinstitute,
		 				'otherboarduniversity'       	        => $otherboarduniversity,
		 				'otherpassingyear'  			=> $otherpassingyear,
		 				'otherplace '    			     => $otherplace,
		 				'otherspecialisation'            => $otherspecialisation,
		 				'otherpercentage'  			     => $otherpercentage,

		    		'hscstream'  			        => $this->input->post('twelthstream'),
		    		'ugdegree'  			        => $this->input->post('ugstream'),
		    		'pgdegree'  			        => $this->input->post('pgupstream'),
		    		'inprocess'						=> 'open',
		    		'complete_inprocess'			=> 0,
		    		'otherdegree'					=> $this->input->post('other_specify'),
		    		'other_degree_specify'			=> $this->input->post('specify'),
						
		    		'campusid'      	    		=> $campusdecodeid,
		    		'categoryid'      	    		=> $campuscatid,
		    		'campusintimationid'			=> $campusintimationid,
		    		'hrddocumentcheckstatus'        => 0,
		    		'campustype' 					=> 'on',
		    		'createdon'      	    		=> date('Y-m-d H:i:s'),
					      
		    		'isdeleted'      	    		        => 0, 

		    	);

			 

		$this->db->insert('tbl_candidate_registration', $insertArrCandidate);
		

		$insert_candidate_id = $this->db->insert_id();

		$insertCommunicationAddress	 = array(

			'candidateid'    		=> $insert_candidate_id,
			'presentstreet'    		=> $this->input->post('presentstreet'),
			'presentcity'       	=> $this->input->post("presentcity"),
			'presentstateid'  		=> $this->input->post('presentstateid'),
			'presentdistrict '    	=> $this->input->post('presentdistrict'),
			'presentpincode'       	=> $this->input->post("presentpincode"),
			'permanentstreet'    	=> $this->input->post('permanentstreet'),
			'permanentcity'       	=> $this->input->post("permanentcity"),
			'permanentstateid'  	=> $this->input->post('permanentstateid'),
			'permanentdistrict '    => $this->input->post('permanentdistrict'),
			'permanentpincode'      => $this->input->post("permanentpincode"),
			'presenthno'            => $this->input->post('presenthno'),
			'permanenthno'          => $this->input->post("permanenthno"),
			'createdon'      	    => date('Y-m-d H:i:s'),
			'createdby'      	    => $this->loginData->UserID, // login user id
			'isdeleted'      	    => 0, 

			);

		$this->db->insert('tbl_candidate_communication_address', $insertCommunicationAddress);
//echo $this->db->last_query();die; 

 			 if (!empty($this->input->post('gapfromdate')[0])) {
 			
 			$countgapfromdate = count($this->input->post('gapfromdate'));


 			$this->db->delete('tbl_gap_year',array('candidateid'=> $insert_candidate_id));


 			for ($j=0; $j<$countgapfromdate; $j++) { 


 			 $gapfromdate=$this->gmodel->changedatedbformate($this->input->post('gapfromdate')[$j]);
 				// echo $gapfromdate;
 				$gaptodate=$this->gmodel->changedatedbformate($this->input->post('gaptodate')[$j]);
 				 //echo $gaptodate;
 				

		 		

 				$insertGapyear	 = array(
 					'candidateid'      => $insert_candidate_id,
 					'fromdate'         =>  $gapfromdate,
 					'todate'           => $gaptodate,
 					'reason'           => $this->input->post('gapreason')[$j],
 					'createdon'        => date('Y-m-d H:i:s'),
					'isdeleted'        => 0
		);
 				
 				
 				$this->db->insert('tbl_gap_year', $insertGapyear);
 				
		
 			}
 			
 }


 	


		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){

			$this->session->set_flashdata('er_msg', 'Error !!! Adding Candidate Registration');	
		}else{
			$this->session->set_flashdata('tr_msg', 'You have been successfully registered');
			redirect('Candidatesinfo/message');
		}

		}

	}

}

prepareview:

$content['statedetails']          = $this->getState();
$content['ugeducationdetails']    = $this->model->getUgEducation();
$content['pgeducationdetails']    = $this->model->getPgEducation();
$content['campusdetails']         = $this->getCampus();
$content['getdistrict']           = $this->model->getDistrict();


$content['title'] = 'Candidatesinfo';

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('_main_layout', $content);

}catch (Exception $e) {
	print_r($e->getMessage());die;
}

}


public function message()
{	
	try{

		$content['title'] = 'Candidatesinfo';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



public function is_exists($email_id)
{	
	try{

		$sql = "SELECT Count(emailid) as count FROM tbl_candidate_registration where emailid='".$email_id."' ";
		$result = $this->db->query($sql)->result();

		return $result[0]->count;



	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


public function getPassword(){

	try{
		
		// Generating Password
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
		$password = substr( str_shuffle( $chars ), 0, 8 );

		return $password;


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



public function getSelectedCandidate()
{

	try{

		$sql = "SELECT * FROM `tbl_candidate_registration` 
		Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  AND `metricpercentage` >= 60 AND 
		`hscpercentage` >= 60 AND 
		`ugpercentage` >= 60 OR (`ugpercentage` >= 60 AND `pgpercentage` >= 60 )"; 

		$res = $this->db->query($sql)->result();

		return $res;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function getUgEducation()
{

	try{

		$sql = "SELECT id,ugname FROM `mstugeducation` Where isdeleted=0";

		$res = $this->db->query($sql)->result();

		return $res;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function getPgEducation()
{

	try{

		$sql = "SELECT id,pgname FROM `mstpgeducation` Where isdeleted=0";

		$res = $this->db->query($sql)->result();

		return $res;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

public function getViewSelectedCandidate($token)
{

	try{

		$sql = "SELECT * FROM `tbl_candidate_registration` 
		Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  AND `metricpercentage` >= 60 AND 
		`hscpercentage` >= 60 AND 
		`ugpercentage` >= 60 OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60 ) ";

		if (!empty($token)) {
			$sql .=" AND candidateid=$token";
		}

		$res = $this->db->query($sql)->result();

		return $res;
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}



public function getState()
{

	try{

		$sql = "SELECT * FROM state where isdeleted='0' ORDER BY name";

		$res = $this->db->query($sql)->result();

		return $res;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}



public function getCampus()
{
	try{

		$sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
		$res = $this->db->query($sql)->result();

		return $res;

	}catch(Exception $e){
		print_r($e->getMessage());die();
	}
}

function mail_exists($key)
{
	$this->db->where('emailid',$key);
	$query = $this->db->get('tbl_candidate_registration');
	if ($query->num_rows() > 0){
		return true;
	}
	else{
		return false;
	}
}

/**
   * Method getdistrict() get district list according to state.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getdistrict($stateid)
{
	try{

		$sql = "SELECT * FROM `district` WHERE stateid=$stateid";  
		$res = $this->db->query($sql)->result();

		return $res;

	}catch(Exception $e){
		print_r($e->getMessage());die();
	}
}

/**
   * Method is_urlexist() Check url is validate or not ???.
   * @access  public
   * @param Null
   * @return  Array
   */

public function is_urlexist($campusdecodeid, $campusintimationid)
{	
	try{


		if ($campusdecodeid !='' && $campusintimationid !='') {
		
		 $sql = "SELECT Count(id) as count FROM tbl_campus_intimation 
		where campusid='".$campusdecodeid."' AND id='".$campusintimationid."' ";

		$result = $this->db->query($sql)->result();
		return $result[0]->count;

	}else{
		return 0;
	}

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


 /**
   * Method changedatedbformate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


  public function changedatedbformate($Date)
  {
  	try{
     
//echo strlen($Date);
    $len = (strlen($Date)-5); 
   //echo substr($Date,$len,-4); die;
    if(substr($Date,$len,-4)=="/")
      $pattern = "/";
   else
     $pattern = "-";

   $date = explode($pattern,$Date);
   //print_r($date); die;
 //  echo $pattern;
   if($pattern == "/" )
       $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
  else
    @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 //die;
if ($date=='//') {
	# code...
}
  return $date;

  }catch (Exception $e) {
			print_r($e->getMessage());die;
		}
}


}