<?php 

/**
* General Nomination And Authorisation Form controller
*/
class General_nomination_and_authorisation_form_staff1 extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
			
		$this->load->model("General_nomination_and_authorisation_form_staff_model");
		//echo "dhgfhdsg";
		//die();
		 $this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("Provident_fund_nomination_form_model");
		
		$this->load->model("Employee_particular_form_model");
		$this->load->model("General_nomination_and_authorisation_form_staff1_model");
		$this->load->model("Provident_fund_nomination_form_staff_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Provident_fund_nomination_form_staff_model");
		$this->load->model(__CLASS__ . '_model', 'model');	
		$this->load->model('Medical_certificate_model');

		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');

//print_r();
	}


	public function index($staff=null)
			{
				try{
		
			  $this->session->set_userdata('staff', $staff);
            



      



      $staff_id=$this->loginData->staffid;
     
    
      if(isset($staff_id))
      {
        
        $staff_id=$staff_id;
       
       
      }
      else
      {
         $staff_id=$staff;
        
        
      }

	$content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);	
	/*echo "<pre>";
	print_r($content['candidatedetailwithaddress']);die;*/
	 $content['report']=$this->Medical_certificate_model->staff_reportingto($staff_id);
	  $content['personnal_mail'] = $this->Provident_fund_nomination_form_staff_model->personal_email();
      // print_r($content['personnal_mail']);
       $personal_email=$content['personnal_mail']->EmailID;
      // die;
      

      $reportingto = $content['candidatedetailwithaddress']->reportingto;
      $content['tc_email'] = $this->Provident_fund_nomination_form_staff_model->tc_email($reportingto);
			
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){

				//print_r($_POST);
				//die();
			

			$Sendsavebtn = $this->input->post('savebtn');

			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

				
  			 	//echo $updateArr['signatureplace_encrypted_filename'] ;
  			 	//die("pooja");



			$dadate = $this->input->post('dadate');

		    $dadate = $this->model->changedatedbformate($dadate);
				
				$m_date = date("Y-m-d");
				$insertarraydata = array(
					'staff_id'=>$staff_id,

				'candidateid'  						=> 0,
				
				'da_place'  						=> $this->input->post('daplace'),
				'nomination_authorisation_signed'   => $updateArr['signatureplace_encrypted_filename'],
				
				'da_date'  							=> $dadate,
				'status'  							=> 0,
				'createdon'  						=> date('Y-m-d H:i:s'),
				'type'=>'m',
        'm_date'=>$m_date
				
				
			);

			$this->db->insert('tbl_general_nomination', $insertarraydata);
			$insertid = $this->db->insert_id();
			$data1=$this->input->post('data');
				
				foreach($data1 as $value)
				{

					$arr=array('sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['name'],
						'nomination_id'=>$insertid,
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior'],
						'type'=>'m',
        'm_date'=>$m_date

						);
					
					$this->db->insert('tbl_nominee_detail_joininginduction', $arr);
				}
				

			
 $insert_data =array(
        'type'=>25,
       'r_id'=> $insertid,
       'sender'=> $staff_id,
       'receiver'=>$content['report']->reportingto,
       'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
       'createdby'=>$this->loginData->UserID,
      
       
       'flag'=>4,
       'staffid'=>$staff_id
        );
          $this->db->insert('tbl_workflowdetail', $insert_data);
			


			


			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				$subject = ': Gratuity Fund Nomination';
        $body = '<h4>'.$content['candidatedetailwithaddress']->staff_name.' submited Gratuity fund Nomination Form </h4><br />';
        $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
        <tr>
        <td width="96">Name </td>
        <td width="404">'.$content['candidatedetailwithaddress']->staff_name.'</td>
        </tr>
        <tr>
        <td>Employ Code</td>
        <td> '.$content['candidatedetailwithaddress']->emp_code.'</td>
        </tr>
        <tr>
        <td>Designation</td>
        <td>' .$content['candidatedetailwithaddress']->desiname.'</td>
        </tr>
        <tr>
        <td>Office</td>
        <td>'.$content['candidatedetailwithaddress']->officename.'</td>
        </tr>
        </table>';
        $body .= "<br /> <br />";
        $body .= "Regards <br />";
        $body .= " ". $content['candidatedetailwithaddress']->staff_name ."<br>";
        $body .= " ". $content['candidatedetailwithaddress']->desiname."<br>";
        $body .= "<b> Thanks </b><br>";
       
           

       // $to_useremail = 'pdhamija2012@gmail.com';
         $to_useremail = $content['candidatedetailwithaddress']->emailid;
         $tcemailid=$content['tc_email']->emailid;

        
         $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
        $this->Common_Model->midemreview_send_email($subject, $body, $to_useremail);
         // die("hello");
   
				
$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	

		redirect('General_nomination_and_authorisation_form_staff1/edit/'.$staff.'/'.$insertid);		
			}

		}

			$submitdatasend = $this->input->post('submitbtn');

		if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {

			
			

  			//echo "image=".$updateArr['signatureplace_encrypted_filename'];
  			//die;


			$dadate = $this->input->post('dadate');

		    $dadate = $this->model->changedatedbformate($dadate);
				
				$m_date = date("Y-m-d");
				$insertarraydata = array(
					'staff_id'=>$staff_id,

				'candidateid'  						=> 0,
				
				'da_place'  						=> $this->input->post('daplace'),
				/*'nomination_authorisation_signed'   => $updateArr['signatureplace_encrypted_filename'],*/
				
				'da_date'  							=> $dadate,
				'status'  							=> 1,
				'createdon'  						=> date('Y-m-d H:i:s'),
				'type'=>'m',
        'm_date'=>$m_date
				
				
			);

			$this->db->insert('tbl_general_nomination', $insertarraydata);
			$insertid = $this->db->insert_id();
			$data1=$this->input->post('data');
				
				foreach($data1 as $value)
				{

					$arr=array('sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['name'],
						'nomination_id'=>$insertid,
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior'],
						'type'=>'m',
        'm_date'=>$m_date

						);
					
					$this->db->insert('tbl_nominee_detail_joininginduction', $arr);
				}
				

			 $insert_data =array(
        'type'=>15,
       'r_id'=> $insertid,
       'sender'=> $staff_id,
       'receiver'=>$content['report']->reportingto,
       'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
       'createdby'=>$this->loginData->UserID,
      
       
       'flag'=>4,
       'staffid'=>$staff_id
        );
          $this->db->insert('tbl_workflowdetail', $insert_data);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				$subject = ': Gratuity Fund Nomination';
        $body = '<h4>'.$content['candidatedetailwithaddress']->staff_name.' submited Gratuity fund Nomination Form </h4><br />';
        $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
        <tr>
        <td width="96">Name </td>
        <td width="404">'.$content['candidatedetailwithaddress']->staff_name.'</td>
        </tr>
        <tr>
        <td>Employ Code</td>
        <td> '.$content['candidatedetailwithaddress']->emp_code.'</td>
        </tr>
        <tr>
        <td>Designation</td>
        <td>' .$content['candidatedetailwithaddress']->desiname.'</td>
        </tr>
        <tr>
        <td>Office</td>
        <td>'.$content['candidatedetailwithaddress']->officename.'</td>
        </tr>
        </table>';
        $body .= "<br /> <br />";
        $body .= "Regards <br />";
        $body .= " ". $content['candidatedetailwithaddress']->staff_name ."<br>";
        $body .= " ". $content['candidatedetailwithaddress']->desiname."<br>";
        $body .= "<b> Thanks </b><br>";
       
           

       // $to_useremail = 'pdhamija2012@gmail.com';
         $to_useremail = $content['candidatedetailwithaddress']->emailid;
         $tcemailid=$content['tc_email']->emailid;

        
         $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
        $this->Common_Model->midemreview_send_email($subject, $body, $to_useremail);
         // die("hello");
   
				
				$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	

				redirect('General_nomination_and_authorisation_form_staff1/view/'.$staff.'/'.$insertid);		
			}
	}
}

//die("gdfgd");
//  $content['topbar'] = $this->Provident_fund_nomination_form_staff_model->do_flag($staff_id);
// // die("gghhhg");

//        //echo "hello";
//       // print_r($content['topbar']);
//        //die;
    
//       //die(print_r($content['topbar']);

//       $var=$content['topbar']->nomination_flag;


       

      

// if ($var==null)
//   {
//   	goto preview;

//   }
   

//   if($var==0)
//   {
//     redirect('/General_nomination_and_authorisation_form_staff1/edit/'.$staff);
//   }
//   elseif($var==1)
//   {
//     redirect('/General_nomination_and_authorisation_form_staff1/view/'.$staff);
//   }
     


  		preview:
		$content['sysrelations'] = $this->model->getSysRelations();
		

		// $content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
		// $content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
		
		// $content['witnessdeclaration'] = $this->model->getWitnessDeclaration($candidateid);
		$content['office_name'] = $this->model->office_name();
		


		$content['title'] = 'General_nomination_and_authorisation_form_staff';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('_main_layout', $content);

		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}



	public function edit($staff=null,$inserted=null)
	{
		try{
		 $staff = $this->uri->segment(3);
    

            $this->session->set_userdata('staff', $staff);
            



      



      $staff_id=$this->loginData->staffid;
     
    
      if(isset($staff_id))
      {
        
        $staff_id=$staff_id;
        //echo "staff id".$staff_id;
        
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff;
        
        // echo "candidateid=".$candidateid;
        
      }

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST"){

			$savesenddata = $this->input->post('savebtn');

			if (!empty($savesenddata) && $savesenddata =='senddatasave') {


				
				$dadate = $this->input->post('dadate');

		       $dadate = $this->model->changedatedbformate($dadate);


			$updatearraydata = array(
				'staff_id'=>$staff_id,

				'candidateid'  => 0,
				
				'da_place'  => $this->input->post('daplace'),
				
				
				'da_date'  => $dadate,
				'status'  => 0,
				
				
			);
			$this->db->where('staff_id', $staff_id);
			$this->db->update('tbl_general_nomination', $updatearraydata);
			echo $this->db->last_query();
			//die;
			//$countnominee = count($this->input->post('full_name_nominee'));


		$data1=$this->input->post('data');
		//print_r($data1);
		//die;
				
				foreach($data1 as $value)
				{

					$arr=array(
						'id'=>$value['n_id'],
						'sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['full_name_nominee'],
						'nomination_id'=>$value['nomination_id'],
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior']

						);
					print_r($arr);
					//die;
					extract($arr);
					//die;
					$this->db->where('id', $id);
					$this->db->update('tbl_nominee_details', $arr);
					echo $this->db->last_query();
					//die;
					
				}
				//die;
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
		$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	
		
			}
			redirect('General_nomination_and_authorisation_form_staff1/edit/'.$staff.'/'.$inserted);		
			}

		
			$submitsenddata = $this->input->post('submitbtn');

		if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {


			
  				// die("hghh");

				$dadate = $this->input->post('dadate');

		       $dadate = $this->model->changedatedbformate($dadate);


			$updatearraydata = array(
				'staff_id'=>$staff_id,
				'candidateid'  => 0,
				
				'da_place'  => $this->input->post('daplace'),
				'nomination_authorisation_signed'  => $updateArr['signatureplace_encrypted_filename'],
				
				'da_date'  => $dadate,
				'status'  => 1,
				
				
			);
			$this->db->where('staff_id', $staff_id);
			$this->db->update('tbl_general_nomination', $updatearraydata);
			//echo $this->db->last_query();
			//die;
			//$countnominee = count($this->input->post('full_name_nominee'));


		$data1=$this->input->post('data');
		//print_r($data1);
		//die;
				
				foreach($data1 as $value)
				{

					$arr=array(
						'id'=>$value['n_id'],
						'sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['full_name_nominee'],
						'nomination_id'=>$value['nomination_id'],
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior']

						);
					//print_r($arr);
					//die;
					extract($arr);
					//die;
					$this->db->where('id', $id);
					$this->db->update('tbl_nominee_details', $arr);
					//echo $this->db->last_query();
					//die;
					
				}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}

			/*else{

				// $hrdemailid    = 'poonamadlekha@pradan.net';
				 $hrdemailid    = 'amit.kum2008@gmail.com';
				 $tcemailid     = $this->loginData->EmailID;

				 $subject = "Submit Gereral Nomination And Authorisation Form ";
				  $body = 	'Dear Sir, <br><br> ';
				  $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 $to_email = $hrdemailid;
				 $to_name = $tcemailid;
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/
				
				$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	

				redirect('General_nomination_and_authorisation_form_staff1/view/'.$staff.'/'.$inserted);		
			


	}


	}
		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);

		//$content['genenominformdetail'] = $this->model->getGeneralnominationform($staff_id);
		//print_r($content['genenominformdetail']);

		$content['nominee'] =$this->model->getGeneralnominationform($inserted);
	//	echo "<pre>";
		//print_r($content['nominee']);
		//die;

       // $content['countnominee'] =$this->model->getCountNominee($staff_id);
        //die('hdghds');
		$content['nomineedetail'] = $this->model->getNomineedetail($inserted);
		//echo "<pre>";
		//print_r($content['nomineedetail']);
		$content['office_name'] = $this->model->office_name();

		// $content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
		// $content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
		// //print_r($content['getgeneralform']);
		// $content['witnessdeclaration'] = $this->model->getWitnessDeclaration($candidateid);


		$content['title'] = 'General_nomination_and_authorisation_staff_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('_main_layout', $content);

		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}



	public function view($staff=null,$inserted=null)
	{
		try{


			 $staff = $this->uri->segment(3);
  

            $this->session->set_userdata('staff', $staff);
          



      


            $login_staff=$this->loginData->staffid;
      $staff_id=$this->loginData->staffid;
     
    
      if(isset($staff_id))
      {
        
        $staff_id=$staff_id;
       
       
      }
      else
      {
         $staff_id=$staff;
         
        
      }
       $content['id'] = $this->General_nomination_and_authorisation_form_staff_model->get_generalworkflowid($staff_id);
      // print_r($content['id']);
        $content['staff_details'] = $this->Provident_fund_nomination_form_model->staffName($staff_id,$login_staff);
		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_staff1_model->getCandidateWithAddressDetails($staff_id);
		//print_r($content['candidatedetailwithaddress']);

		// $content['genenominformdetail'] = $this->model->getViewGeneralnominationform($candidateid);
		

		// $content['getGeneralnomination'] = $this->model->getViewGeneralnominationform($candidateid);
	
		$content['nominee'] =$this->model->getGeneralnominationform($inserted);
		$content['office_name'] = $this->model->office_name();
		//print_r($content['office_name']);
		$content['nomineedetail'] = $this->model->getNomineedetail($inserted);
		//$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
		//$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
		 $content['personal']=$this->Common_Model->get_Personnal_Staff_List();
                           
                            if($this->loginData->RoleID==2)
                            {
                                 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


                             if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
                                $status=$this->input->post('status');
                                $reject=$this->input->post('reject');
                                $p_status=$this->input->post('p_status');
                                $wdtaff1=$this->input->post('wdtaff1');
                                $wdtaff2=$this->input->post('wdtaff2');
                                $r_id=$this->input->post('id');
                                
                                $check = $this->session->userdata('insert_id');
                              


                                 $insert_data =array(
        'type'=>25,
       'r_id'=> $r_id,
       'sender'=> $login_staff,
       'receiver'=>$p_status,
       'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
       'createdby'=>$login_staff,
       'scomments'=>$reject,
       'forwarded_workflowid'=>$content['id']->workflow_id,
       'flag'=> $status,
       'staff_id'=>$staff_id
        );
           
          $arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 
         
       // print_r($insert_data);
        //die();

                               
        $this->db->insert('tbl_workflowdetail', $insert_data);
       
        $this->db->where('graduity_id',$r_id);
        $this->db->update('tbl_graduitynomination',$arr_data);
        
     
                                
                             }


                            }
                            else  if($this->loginData->RoleID==17)
                            {
                                 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


                             if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
                                $status=$this->input->post('status');
                                $reject=$this->input->post('reject');
                                $p_status=$this->input->post('p_status');
                                $wdtaff1=$this->input->post('wdtaff1');
                                $wdtaff2=$this->input->post('wdtaff2');
                                $r_id=$this->input->post('id');
                                
                                $check = $this->session->userdata('insert_id');
                              


                                 $insert_data =array(
        'type'=>25,
       'r_id'=> $r_id,
       'sender'=> $login_staff,
       'receiver'=>$staff_id,
       'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
       'createdby'=>$login_staff,
       'scomments'=>$reject,
       'forwarded_workflowid'=>$content['id']->workflow_id,
       'flag'=> $status,
       'staff_id'=>$staff_id
        );
           
         

                               
        $this->db->insert('tbl_workflowdetail', $insert_data);
       
       
     
                                
                             }


                            }

    

    
   // print_r($content['nomineedetail']);




		$content['title'] = 'General_nomination_and_authorisation_form_staff1';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('_main_layout', $content);

		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}



}