<?php 
class SalaryHead extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
		
		if ($this->loginData->UserID =='') {
			redirect('login');
		}
		
	}
	
 
	public function index($financial_year = Null)
	{
		try{

		$this->load->model('Hr_report_model');		
		
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();

		// end permission

		 $dbname = $this->db->query("SELECT DATABASE() as dbname")->row();

		/// print_r($dbname); die;

		$query = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='".$dbname->dbname."' AND `TABLE_NAME`='tblmstemployeesalary' AND (`COLUMN_NAME` LIKE 'e_%' OR `COLUMN_NAME` LIKE 'd_%' ) AND `COLUMN_NAME` !='deletedemployee'  and `COLUMN_NAME` != 'e_basic' ORDER BY cast(COLUMN_COMMENT AS UNSIGNED)";
		// echo $query; die();
		$content['column_list'] = $this->db->query($query)->result();
		// print_r($content['column_list']); die();
		if($financial_year){
			$content['financial_year'] = $financial_year;
		}else{
			$content['financial_year'] = (date('Y')-1).'-'.date('Y');
			redirect('SalaryHead/index/'.$content['financial_year']);
		}
		
		foreach ($content['column_list'] as $list) {
			# code...
			$query = "SELECT * FROM salaryheads WHERE fieldname='".$list->COLUMN_NAME."' AND financial_year='".$content['financial_year']."'";
			$content['salaryheads_dtls'] = $this->db->query($query)->row();
		    @$fielddesc = $content['salaryheads_dtls']->fielddesc;
			$content['content_lists']['list'][] = $list->COLUMN_NAME;
			$content['content_lists']['list_value'][] = $fielddesc;			

		}

		$query = "SELECT * FROM lpooffice WHERE closed = 'No'";
		$content['officelist'] = $this->db->query($query)->result();
		
		$content['title'] = 'SalaryHead';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}
}