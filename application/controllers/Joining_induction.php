<?php 
class Joining_induction extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
		// print_r($this->loginData);s
	}

	public function index()
	{
		try{
		//index include
		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		 $this->load->model('Joining_induction_model');
	 $staffid=$this->loginData->staffid;
	
	 $role=$this->loginData->RoleID;
		$content['listing'] = $this->Joining_induction_model->Approve($staffid,$role);
		
		
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
			$month     = $this->input->post('month');
			$exp       = explode('/',$month);
			$MonthYear = $exp[1].'-'.$exp[0];
			// include$this->session->set_userdata("month",$this->input->post('month'));
		}

	
		$content['title'] = 'Joining_induction';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	
}