<?php 

/**
* State List
*/
class Search_transfer_promotion_history extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $this->load->model('Search_transfer_promotion_history_model');
    $check = $this->session->userdata('login_data');
///// Check Session //////  
    if (empty($check)) {
      redirect('login');
    }

    $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {

// start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    
    $tm= $this->Search_transfer_promotion_history_model->staff_transfer_list();
    $content['staff_list'] =$tm;

    $content['mstpgeducation_details'] = $this->Common_Model->query_data($query);
//$content['subview']="index";
    $content['title'] = 'Pgeducation';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }

  public function fetch_data()
  {

    $staff_id=$this->input->post('staff_id');
    $types=$this->input->post('types');
    $froms=$this->input->post('froms');
    $tos=$this->input->post('tos');
    $outputpros = '';
    $outputseppp = '';
    $proboth_index = '';

    $this->load->model('Search_transfer_promotion_history_model','model');
    $datas=$this->Search_transfer_promotion_history_model->fetch_table($staff_id,$types,$froms,$tos);
    // /print_r($datas);


    $res=$datas;

    $outputtrans=' <table id="tbltransferprohistory" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
    <thead>
    <tr>
    <th class="text-center" style="width: 50px;">S.No.</th>
    <th>Employee code</th>
    <th>Name</th>
    <th>Date</th>
    <th>Old Office</th>
    <th>New Office</th>
   </tr>
    </thead><tbody>';

    $outputpro='<table id="tbltransferprohistory" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
    <thead>
    <tr>
    <th class="text-center" style="width: 50px;">S.No.</th>
    <th>Employee code</th>
     <th>Name</th>
    <th  class="text-center" style="width: 50px;">Date</th>
     <th>Old Designation</th>
    <th>New Designation</th>
    </tr>
    </thead><tbody>';

     $outputboth='<table id="tbltransferprohistory" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
    <thead>
    <tr>
    <th class="text-center" style="width: 50px;">S.No.</th>
    <th>Employee code</th>
     <th>Name</th>
    <th  class="text-center" style="width: 50px;">Date</th>
     <th>Old Office</th>
    <th>New Office</th>
    <th>Old Designation</th>
    <th>New Designation</th>
    </tr>
    </thead><tbody>';

    $outputsep='<table id="tbltransferprohistory" class="table table-bordered table-striped dt-responsive table-hover dataTable js-exportable">
    <thead>
    <tr>
    <th class="text-center" style="width: 50px;">S.No.</th>
    <th>Employee code</th>
     <th>Name</th>
    <th class="text-center" style="width: 50px;">Date</th>
    <th>Old Office</th>
    <th>Date of leaving</th>
    <th>Type</th>
    </tr>
    </thead><tbody>';

    $promotion_index = 1;
    $sep_index = 1;
     $trans_index=1;

    foreach($res as $row)
    {

      if($row->trans_status=='Transfer')
      {

         $date_of_transfer = $this->Search_transfer_promotion_history_model->changedatedbformate($row->date_of_transfer);
        $outputtransss = '
        <tr>
        <td  class="text-center" style="width: 50px;">'.$trans_index.'</td>
        <td>'.$row->emp_code.'</td>
          <td>'.$row->name.'</td>
        <td  class="text-center">'.$date_of_transfer.'</td>
       
        <td>'.$row->Present_Designation.'</td>
        <td>'.$row->New_Designation.'</td>
        </tr>
        ';

        $trans_index++;


        
      }elseif($row->trans_status=='Promotion') {

        $date_of_transfer = $this->Search_transfer_promotion_history_model->changedatedbformate($row->date_of_transfer);
        $outputpros .= '
        <tr>
        <td  class="text-center" style="width: 50px;">'.$promotion_index.'</td>
         <td>'.$row->emp_code.'</td>
           <td>'.$row->name.'</td>
        <td  class="text-center">'.$date_of_transfer.'</td>
      
        <td>'.$row->Present_Designation.'</td>
        <td>'.$row->New_Designation.'</td>
        </tr>
        ';

        $promotion_index++;
      }elseif($row->trans_status=='Both') {

        $date_of_transfer = $this->Search_transfer_promotion_history_model->changedatedbformate($row->date_of_transfer);
        $outputboth .= '
        <tr>
        <td  class="text-center" style="width: 50px;">'.$promotion_index.'</td>
         <td>'.$row->emp_code.'</td>
           <td>'.$row->name.'</td>
        <td  class="text-center">'.$date_of_transfer.'</td>
       <td>'.$row->old_office_name.'</td>
        <td>'.$row->new_office_name.'</td>
        <td>'.$row->Present_Designation.'</td>
        <td>'.$row->New_Designation.'</td>
        </tr> ';
        $proboth_index++;
      } elseif($row->trans_status=='Termination' ||'Resign' ||'Retirement' ||'Death'){

        $date_of_transfer = $this->Search_transfer_promotion_history_model->changedatedbformate($row->date_of_transfer);
        $outputseppp .= '
        <tr>
        <td class="text-center">'.$sep_index.'</td>
        <td>'.$row->emp_code.'</td>
         <td>'.$row->name.'</td>
        <td class="text-center">'.$date_of_transfer.'</td>
        <td>'.$row->old_office_name.'</td>
         <td>'.$row->dateofleaving.'</td>
        <td>'.$row->trans_status.'</td>
        </tr>
        ';
        $sep_index++;        
        }
         
      }
      $outputpros .= "</tbody></table>";
      $outputseppp .= "</tbody></table>";

      if ($promotion_index > 1) {
        echo $outputpro.$outputpros;
      }else if ($proboth_index > 1) {
        echo $outputboth.$proboth_indexss;
      }else if ($sep_index > 1) {
        echo $outputsep.$outputseppp;
      }
      elseif($trans_index>1){
        echo $outputtrans.$outputtransss;
      }
  }





  
}