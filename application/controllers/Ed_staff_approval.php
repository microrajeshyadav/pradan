<?php 

/**
* State List
*/
class Ed_staff_approval extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
 
  }

  public function index()
  {
    try{
     $this->load->model("Ed_staff_approval_model");
    // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 

    $content['getworkflowdetail'] = $this->Ed_staff_approval_model->getworkflowdetailseperationlist();
    $content['getworkflowdetailprobation'] = $this->Ed_staff_approval_model->getworkflowdetaillist();
    $content['getworkflowdetailmidtermreview'] = $this->Ed_staff_approval_model->getworkflowdmidtermreviewetaillist();

    // echo "<pre>";
    // print_r($content['getworkflowdetail']);exit();
    
    $content['title'] = 'Ed_staff_approval';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


 public function list_joining_report($token)
  {
    try{
     $this->load->model("Ed_staff_approval_model");
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $content['getstaffjoiningreport'] = $this->Ed_staff_approval_model->getstaffjoiningreport($token);
   //$content['subview']="index";
    $content['title'] = 'Ed_staff_approval/list_joining_report';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
    }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }


public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 


      $staffid = $this->uri->segment(3);
      $tarnsid  = $this->uri->segment(4);

       $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail 
      WHERE r_id = $tarnsid";
         $result  = $this->db->query($sql)->result()[0];

         $forwardworkflowid = $result->workflowid;

         $sql1  = "SELECT flag FROM tbl_workflowdetail 
      WHERE workflowid = $forwardworkflowid";
         $result1  = $this->db->query($sql1)->row();

         $content['workflow_flag']=$result1;
         // print_r($content['flag']);
         // die;

         // $forwardworkflowid = $result->workflowid;


         // print_r($result1); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   
    if($RequestMethod == 'POST'){
        // Seperation process Exp. & Relieving letters with edsign 
        if($result1->flag == 28 && $this->input->post('status') == 29){ // Executive Director  and approved
          $sql  = "SELECT max(content) as Exp_content FROM tbl_levelwisetermination WHERE transid = $tarnsid";
          $res  = $this->db->query($sql)->row();

          $sql  = "SELECT max(content) as Rel_content FROM tbl_sep_releaseform WHERE transid = $tarnsid";
          $res1  = $this->db->query($sql)->row();

          
          $getedname = '';
          $edsign = '';
        $getedname = $this->gmodel->getExecutiveDirectorEmailid();

        if ($getedname->sign !='') {
          $edsign = site_url('datafiles/signature/'.$getedname->sign);
        }else{ 
          $edsign = site_url('datafiles/signature/Signature.png');
        }

        //Exp. letter
        $body = $res->Exp_content;

        $ed = array('$edsign');
        $ed_replace = array($edsign);

        if(!empty($body))
        $body = str_replace($ed,$ed_replace,$body); 

        $body=  html_entity_decode($body);

        $filename = "";
        $filename = md5(time() . rand(1,1000));
        $this->load->model('Dompdf_model');
        $generate =   $this->Dompdf_model->generatePDFed($body, $filename, NULL,'ExperienceLetter.pdf');

        $insert = array('filename'  => $filename,
                        'content'  => $body);

        $this->db->where('transid', $tarnsid);
          $this->db->update('tbl_levelwisetermination', $insert); // Experience letter with edsign 

          //Relieving Letter
          $body1 = $res1->Rel_content;

          $ed1 = array('$edsign');
          $ed1_replace = array($edsign);

          if(!empty($body1))
          $body1 = str_replace($ed1,$ed1_replace,$body1); 

          $body1=  html_entity_decode($body1);

          $filename1 = "";
          $filename1 = md5(time() . rand(1,1000));

          $generate1 =   $this->Dompdf_model->generatePDFed($body1, $filename1, NULL,'RelievingLetter.pdf');

          $insert1 = array('filename'  => $filename1,
                           'content'  => $body1);

          $this->db->where('transid', $tarnsid);
          $this->db->update('tbl_sep_releaseform', $insert1); // relieving letter with edsign
        }
      // echo "<pre>";
      // print_r($this->input->post()); //die;

        $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
        $personnel_administration = $this->input->post('personnel_administration');

       $transferstaffdetails = $this->Ed_staff_approval_model->getTransferStaffDetails($transid);

       // $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
       // $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       // $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.

           
      
     $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
        $this->form_validation->set_rules('status','Seelct Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


     


        $this->db->trans_start();
      
        if($status == 9){     //// Status => Approve 
           $content['getpersonnaluser'] = $this->Ed_staff_approval_model->getPersonalUserList();
           // print_r($content['getpersonnaluser']);
           // die;



          //  $insertArr = array(

          //       'new_office_id'     => $NewOfficeId,
          //       'reportingto'       => $NewReportingto ,
          //       'doj_team'          => $NewDateOfTransfer,
          //       'updatedon'         => date("Y-m-d H:i:s"),
          //       'updatedby'         => $this->loginData->staffid
          //   );

          //   $this->db->where('staffid',$staffid);
          //   $this->db->update('staff', $insertArr);

          $updateArr = array(
                         
                'trans_flag'     => 9,
                'updatedon'      => date("Y-m-d H:i:s"),
                'updatedby'      => $this->loginData->staffid
            );

            $this->db->where('id',$transid);
            $this->db->update('staff_transaction', $updateArr);

          foreach($content['getpersonnaluser'] as $val)
          {
             $personnel_administration=$val->personnelstaffid;

           $insertworkflowArr = array(

             'r_id'                 => $transid,
             'type'                 => 3,
             'staffid'              => $staffid,
             'sender'               => $this->loginData->staffid,
             'receiver'             => $personnel_administration,
             'forwarded_workflowid' => $forwardworkflowid,
             'senddate'             => date("Y-m-d H:i:s"),
             'flag'                 => 9,
             'scomments'            => $this->input->post('comments'),
             'createdon'            => date("Y-m-d H:i:s"),
             'createdby'            => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
       // echo $this->db->last_query();
      }
      //die;
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Ed_staff_approval/'.$token);

          }

        }


        if ($status ==3) {

             $insertArr = array(
                       
              'trans_flag'     => 3,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
          );

          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $insertArr);


        $insertworkflowArr = array(
             'r_id'           => $transid,
             'type'           => 3,
             'staffid'        => $staffid,
             'sender'         => $this->loginData->staffid,
             'receiver'       => $staffid,
             'senddate'       => date("Y-m-d H:i:s"),
             'flag'           => 3,
             'scomments'      => $this->input->post('comments'),
             'createdon'      => date("Y-m-d H:i:s"),
             'createdby'      => $this->loginData->staffid,
         );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Ed_staff_approval/'.$token);

          }
        }

         if ($status ==29) {

             $mstArr = array(
                       
              'UpdatedOn'      => date("Y-m-d H:i:s"),
              'UpdatedBy'      => $this->loginData->staffid,
              'IsDeleted'     =>1
              
          );


          $this->db->where('staffid',$staffid);
          $this->db->update('mstuser', $mstArr);

           $staffArr = array(
                       
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid,
              'status'     =>0
              
          );
              

          $this->db->where('staffid',$staffid);
          $this->db->update('staff', $staffArr);

            $insertArr = array(
                       
              'trans_flag'     => 29,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
          );

          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $insertArr);



        $insertworkflowArr = array(
             'r_id'           => $transid,
             'type'           => 3,
             'staffid'        => $staffid,
             'sender'         => $this->loginData->staffid,
             'receiver'       => $this->input->post('personnel_administration'),
             'senddate'       => date("Y-m-d H:i:s"),
             'flag'           => 29,
             'scomments'      => $this->input->post('comments'),
             'createdon'      => date("Y-m-d H:i:s"),
             'createdby'      => $this->loginData->staffid,
             'forwarded_workflowid'=> $forwardworkflowid
         );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
        // echo $this->db->last_query();
        // die;

         $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Ed_staff_approval/'.$token);

          }
        }


    
  }

    prepareview:

   $content['staffid'] = $staffid;
   $content['tarnsid'] = $tarnsid;

     $content['getpersonnaluser'] = $this->Ed_staff_approval_model->getPersonalUserList();
    // print_r($content['getpersonnaluser']);
    // die;

    $content['subview'] = 'Ed_staff_approval/add';
    $this->load->view('_main_layout', $content);

    }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }




  public function edit($token)
  {
    try {

              // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('shortname','Short name','trim|required|min_length[1]|max_length[10]');
        $this->form_validation->set_rules('categoryname','Category Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
          'shortname'      => $this->input->post('shortname'),
          'categoryname'   => $this->input->post('categoryname'),
          'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,
          'status'         => $this->input->post('status')
        );
      
      $this->db->where('id', $token);
      $this->db->update('mst_staff_category', $updateArr);
  $this->db->last_query(); 

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Staff Category');
       redirect('/Ed_staff_approval/');
    }

    prepareview:

    $content['title'] = 'Ed_staff_approval';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
      
    } catch (Exception $e) {
       print_r($e->getMessage());die;
    }

   }





 public function add_clearance_certificate($token)
  {
    try {
      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 


    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      // echo "<pre>";
      // print_r($this->input->post()); die;


      $btnsubmit = $this->input->post('btnsubmit');
      $btnsend = $this->input->post('btnsend');


       if (isset($btnsend) && $btnsend == "AcceptSaveData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           => $token,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 0,
        );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);
     $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $insertid,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
       }
       $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Ed_staff_approval/edit_clearance_certificate/'.$insertid);

          }
      }

    

    if (isset($btnsubmit) && $btnsubmit == "AcceptSendData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           => $token,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 1,
        );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);
     $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $insertid,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
  
       }


        $insertArr = array(
          'trans_flag'              => 9,
        );
      
      $this->db->where('id',$token);
      $this->db->update('staff_transaction', $insertArr);

       $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Ed_staff_approval/view_clearance_certificate'.$insertid);

          }
      }

  
     
    }

     $content['getstafflist'] = $this->Staff_approval_model->getstaffdetailslist($token);

    $content['title'] = '/add_clearance_certificate';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
      
    } catch (Exception $e) {
      print_r($e->getMessage());die;
    }
        // start permission 

   }
    

 public function edit_clearance_certificate($token)
  {
    try {
      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

     $getstaffcclist = $this->Staff_approval_model->getstaffclerancelist($token);
      $transid = $getstaffcclist->transid; 
  
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      // echo "<pre>";
      // print_r($this->input->post()); die;


      $btnsubmit = $this->input->post('btnsubmit');
      $btnsend = $this->input->post('btnsend');


       if (isset($btnsend) && $btnsend == "AcceptSaveData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           =>  $transid,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 0,
        );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);

      $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       $this->db->delete('tbl_clearance_certificate_transaction',array('clearance_certificate_id'=> $token));

       for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $token,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
       }
       $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Ed_staff_approval/edit_clearance_certificate/'.$insertid);

          }
      }

    

    if (isset($btnsubmit) && $btnsubmit == "AcceptSendData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           =>  $transid,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 1,
        );
      
      $this->db->where('id',$token);
      $this->db->update('tbl_clearance_certificate', $insertArr);
     $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       $this->db->delete('tbl_clearance_certificate_transaction',array('clearance_certificate_id'=> $token));


       for ($i=0; $i < $name_of_location; $i++) { 

         $insertchiArr = array(
          'clearance_certificate_id'      => $token,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
       }

        $insertArr = array(
          'trans_flag'              => 9,
        );
      
      $this->db->where('id',$transid);
      $this->db->update('staff_transaction', $insertArr);


       $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Ed_staff_approval/view_clearance_certificate/'.$token);

          }
      }

  
     
    }




    $content['getstaffcertify_that'] = $getstaffcclist;
      $content['getcountstaffitemslist'] = $this->Staff_approval_model->getcountstaffitmsclerancelist($token);
       $content['getstaffitemslist'] = $this->Staff_approval_model->getstaffitmsclerancelist($token);
      
     $content['getstafflist'] = $this->Staff_approval_model->getSinglestaffdetailslist($transid);

    $content['title'] = '/edit_clearance_certificate';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
      
    } catch (Exception $e) {
      print_r($e->getMessage());die;
    }
        // start permission 

   }
    



 public function view_clearance_certificate($token)
  {
    try {

      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
     $getstaffcclist = $this->Staff_approval_model->getstaffclerancelist($token);
      $transid = $getstaffcclist->transid;
       $content['getstaffcertify_that'] = $getstaffcclist;
      $content['getstafflist'] = $this->Staff_approval_model->getSinglestaffdetailslist($transid);
     
      $content['getcountstaffitemslist'] = $this->Staff_approval_model->getcountstaffitmsclerancelist($token);
       $content['getstaffitemslist'] = $this->Staff_approval_model->getstaffitmsclerancelist($token);
    $content['title'] = '/view_clearance_certificate';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  
      
    } catch (Exception $e) {
       print_r($e->getMessage());die;
      
    }
        // start permission 

}



public function add_joining_report($token)
  {

    $this->load->model('Staff_approval_model');

     $getStaffjoningplace = $this->Staff_approval_model->getStaffjoningplace($token);
   // print_r($getStaffjoningplace); die;

    $transid = $getStaffjoningplace->transid;



    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      // echo "sdfdsf";
      // print_r($this->input->post()); die;
      
      $savebtn = $this->input->post('savebtn');
      $submitbtn = $this->input->post('submitbtn');

      $reporteddutyon = $this->input->post('reporteddutyon');

      $reported_duty_on = $this->gmodel->changedatedbformate($reporteddutyon);

      if (isset($savebtn) && $savebtn =='senddatasave') {

      $this->db->trans_start();

      $insertArr = array(
           'reported_for_duty_date'   => $reported_duty_on,
             'assigned'                 =>  $this->input->post('assignedto'),
             'location'                 => $this->input->post('location'),
             'supervision_staffid'      => $this->loginData->staffid,
             'updatedon'                => date('Y-m-d H:i:s'),
             'updatedby'                => $this->loginData->staffid,
      );
          
       $this->db->where('id', $token);
       $this->db->update('tbl_joining_report_new_place', $insertArr);

           $insertArr = array(
             'trans_flag'  => 11,
          );
          $this->db->where('id', $transid);
          $this->db->update('staff_transaction', $insertArr);

          $insertid = $this->db->insert_id();

          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Staff_approval/view_joining_report/'.$token);

          }
    }



  

  }

    $content['getstaffdetailslist'] = $this->Staff_approval_model->getstaffdetailslist1($token);

    $content['token'] = $token;

     $content['supervisionname'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.''.$this->loginData->UserLastName;
      
    $content['title'] = 'Ed_staff_approval/add_joining_report';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

    $this->load->view('_main_layout', $content);

  }



public function edit_joining_report($token)
  {

    $this->load->model('Ed_staff_approval_model');

  

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      // echo "sdfdsf";
      // print_r($this->input->post()); die;
      
      $savebtn = $this->input->post('savebtn');
      $submitbtn = $this->input->post('submitbtn');
    

      $staffdutytodaydate = $this->input->post('staff_duty_today_date');

      $staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);

      if (isset($savebtn) && $savebtn =='senddatasave') {

      $this->db->trans_start();

      $insertArr = array(
            'staff_duty_today_date'      => $staff_duty_today_date,
            'transid'           =>  $token,
             'flag'               => 0,
            'createdon'         => date('Y-m-d H:i:s'),
            'createdby'         => $this->loginData->staffid,
          );

          $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);
         // $insertid = $this->db->insert_id();

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/edit_joining_report/'.$token);

          }
    }



    if (isset($submitbtn) && $submitbtn =='senddatasubmit') {


      $this->db->trans_start();

      $insertArr = array(
            'staff_duty_today_date'      => $staff_duty_today_date,
            'transid'           =>  $token,
            'flag'               => 1,
            'createdon'         => date('Y-m-d H:i:s'),
            'createdby'         => $this->loginData->staffid,
          );
      
         $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);

          $insertArr = array(
             'trans_flag'      => 10,
          );
        
          $this->db->where('id', $token);
          $this->db->update('staff_transaction', $insertArr);


           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/view_joining_report/'.$token);

          }
    }

  }

   
     $content['getstaffjopin'] = $getStaffjoningplace;

    $content['getstaffdetailslist'] = $this->Staff_approval_model->getstaffdetailslist1($token);

    $content['token'] = $token;
      
    $content['title'] = 'Staff_approval/edit_joining_report';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

    $this->load->view('_main_layout', $content);
  }




  public function view_joining_report($token)
  {

    $this->load->model('Ed_staff_approval_model');

      $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      

    }

   $getStaffjoningplace = $this->Staff_approval_model->getStaffjoningplace($token);
   // print_r($getStaffjoningplace); die;

    $transid = $getStaffjoningplace->transid;

     $content['getstaffjopin'] = $getStaffjoningplace;

    $content['getstaffdetailslist']  = $this->Staff_approval_model->getstaffdetailslist1($transid);
    $content['supervisionname'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.''.$this->loginData->UserLastName;
      

      
    $content['title'] = 'Staff_approval/view_joining_report';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

    $this->load->view('_main_layout', $content);
  }


}