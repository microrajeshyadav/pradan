<?php 

/**
* Central Event Controller List
*/
class Central_event extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');

    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model', 'model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }


   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{
    // start permission 
   $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //////////////// Select Batch ////////////////
   $query = "select * from mstbatch where status =0 AND isdeleted='0'";
   $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
   $query = "select * from mstphase where isdeleted='0'";
   $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
   $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
   $content['candidate_details']     = $this->Common_Model->query_data($query);

   $content['centralevent_details']  = $this->model->getCentralEvent();
   $content['event_details']         = $this->model->getEvent();

   $content['subview']="index";

   $content['title'] = 'Central_event';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }







 public function Add()
 {
  try{
        // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

    $this->db->trans_start();

    $btnsavedata =  $this->input->post('btnsevedata');
    $btnsendsevedata = $this->input->post('btnsendsevedata');
    
    if (!empty($btnsavedata) && $btnsavedata =='savedata') {

      $fromdate = $this->input->post('central_event_from_date');

      if (!empty($fromdate)) {
        $from_date = $this->model->changedatedbformate($fromdate);
      }

      $todate = $this->input->post('central_event_to_date');

      if (!empty($todate)) {
       $to_date = $this->model->changedatedbformate($todate);
     }

     
     $insertArr = array(
      'name'            => $this->input->post('central_event_name'),
      'from_date'       => $from_date,
      'to_date'         => $to_date,
      'place'           => $this->input->post('central_event_place'),
      'createdby'       => $this->loginData->UserID,
      'createdon'       => date('Y-m-d H:i:s'),
      'isdeleted'       => 0,
    );

     $this->db->insert('tbl_central_event', $insertArr);
  //echo $this->db->last_query(); die;
     $insertid = $this->db->insert_id();

     $countresouce_person =  count($this->input->post('resouce_person')); 


     for ($i=0; $i < $countresouce_person ; $i++) { 

      $staffid = $this->input->post('resouce_person')[$i];
      $expid = explode(',', $staffid);
      $resouceid  = $expid[0];
      

      $insertArr_transaction = array(
        'central_event_id'     => $insertid,
        'resouce_person_id'    => $resouceid,

      );
      
      $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid= $this->db->$this->insert_id();
    }
    // echo $this->db->last_query(); die;

    $this->db->trans_complete();

    if ($this->db->trans_status() === true){

       // echo 'Central_event/sendinvitation/'.$insertid; //die;

      $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');

      redirect('/Central_event/sendinvitation/'.$insertid);

    }else{

      $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');

      redirect('/Central_event/add');
    }

  }

  if (!empty($btnsendsevedata) && $btnsendsevedata =='sendsavedata') {


    $fromdate = $this->input->post('central_event_from_date');

    if (!empty($fromdate)) {
      $from_date = $this->model->changedatedbformate($fromdate);
    }

    $todate = $this->input->post('central_event_to_date');

    if (!empty($todate)) {
     $to_date = $this->model->changedatedbformate($todate);
   }


   $insertArr = array(
    'name'            => $this->input->post('central_event_name'),
    'from_date'       => $from_date,
    'to_date'         => $to_date,
    'place'           => $this->input->post('central_event_place'),
    'createdby'       => $this->loginData->UserID,
    'createdon'       => date('Y-m-d H:i:s'),
    'isdeleted'       => 0,
  );

   $this->db->insert('tbl_central_event', $insertArr);
  //echo $this->db->last_query(); die;
   $insertid = $this->db->insert_id();

   $countresouce_person =  count($this->input->post('resouce_person')); 


   for ($i=0; $i < $countresouce_person ; $i++) { 

    $staffid = $this->input->post('resouce_person')[$i];
    $expid = explode(',', $staffid);
    $resouceid  = $expid[0];

    $insertArr_transaction = array(
      'central_event_id'     => $insertid,
      'resouce_person_id'  =>  $resouceid,

    );

    $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid_transaction = $this->db->$this->insert_id();

  }

  $this->db->trans_complete();

  if ($this->db->trans_status() === true){
    $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');
    redirect('/Central_event/sendinvitation/'.$insertid);
  }else{
    $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
    redirect('/Central_event/add');
  }

}

}

 //////////////// Select Batch ////////////////
$query = "select * from mstbatch where status =0 AND isdeleted='0'";
$content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
$query = "select * from mstphase where isdeleted='0'";
$content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
$query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
$content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
$content['resouceperson_details'] = $this->model->getResoucePerson();
   //////////////////// Select Event Name ////////////////////////////
$content['event_details']         = $this->model->getEvent();

$content['title']   = 'Central_event';
$content['subview'] = 'Central_event/add';
$this->load->view('_main_layout', $content);
}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




public function sendinvitation($token)
{
  try{
        // start permission 
    $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //echo $token; die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
// echo "<pre>";
    // print_r($this->input->post());

      $eventdetails  = $this->model->getSingleCentralEvent($token);
     // print_r($eventdetails);
      $event_name = $eventdetails[0]->name;
      $event_from_date = $eventdetails[0]->from_date;
      $event_to_date = $eventdetails[0]->to_date;
      $event_place = $eventdetails[0]->place;


      $count_sendmail = count($this->input->post('sendmail'));



      for ($i=0; $i < $count_sendmail; $i++) { 


        $daid =  $this->input->post('sendmail')[$i];
        $daemail = $this->gmodel->getCandidateDetails($daid);
        $daemailid = $daemail->emailid;
        $daname =  $this->input->post('daname')[$i];

        $insertArr_participant = array(
          'central_event_id' =>$token,
          'participant_id' =>$this->input->post('sendmail')[$i],
        );

        $this->db->insert('tbl_participant', $insertArr_participant);
        $insertid = $this->db->insert_id();

        $html = 'Dear '.$daname.', <br><br> ';
        $html .= '<b>Center Evant - </b> '.$event_name.',<br>';
        $html .= '<b>From Date -  </b> '.$this->model->changedatedbformate($event_from_date).',<br>';
        $html .= '<b>TO Date -  </b> '.$this->model->changedatedbformate($event_to_date).',<br>';
        $html .= '<b>Place -  </b> '.$event_place.',<br>';

     $sendmail = $this->Common_Model->send_email($subject = 'Central Event Invitation', $message = $html, $daemailid);  //// Send Mail Resource Person ////

     if ($sendmail) {

       $updateArr_transaction = array(
         'send_email_status' => 1,
       );
       $this->db->where('id',$insertid);
       $this->db->update('tbl_participant', $updateArr_transaction);
      //echo $this->db->last_query(); die;
       $this->session->set_flashdata('tr_msg', 'Thank you invitation send successfull !!!');

     }else{
       $this->session->set_flashdata('er_msg', 'Error  invitation not send successfull!!!');
     }
   }
   redirect('/Central_event/index'); 

 }


 //////////////// Select Batch ////////////////
 $query = "select * from mstbatch where status =0 AND isdeleted='0'";
 $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
 $query = "select * from mstphase where isdeleted='0'";
 $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
 $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
 $content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
 $content['resouceperson_details'] = $this->model->getResoucePerson();
    ////////////////////// Select getResoucePerson ////////////////////////
 $singleresouceperson_details1 = $this->model->getSingleCentralEvent($token);
  //

 //  print_r($singleresouceperson_details1); die;

 $content['singleresouceperson_details'] = $singleresouceperson_details1[0];

 $temp_array = [];
 $resouceperson_list = $this->model->getSingleResoucePerson($token);
 foreach ($resouceperson_list as $mrow) {
  $temp_array[] = $mrow->resouce_person_id;
}
$content['map_resouce_person_array'] = $temp_array;

   //////////////////// Select Event Name ////////////////////////////
$content['event_details']  = $this->model->getEvent();
$current_financial_year    = $this->model->CurrentFinancialYear();

$content['fin_year_batch_da']  = $this->model->getDevelopmentShip($current_financial_year);

$content['title'] = 'Central_event';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch(Exception $e){
  print_r($e->getMessage());die();
}

}


public function edit($token)
{
  try{
        // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

  //print_r($this->input->post()); die;


    $this->db->trans_start();

    $btnsavedata =  $this->input->post('btnsevedata');
    $btnsendsevedata = $this->input->post('btnsendsevedata');

    if (!empty($btnsavedata) && $btnsavedata =='savedata') {


      $fromdate = $this->input->post('central_event_from_date');

      if (!empty($fromdate)) {
        $from_date = $this->model->changedatedbformate($fromdate);
      }

      $todate = $this->input->post('central_event_to_date');

      if (!empty($todate)) {
       $to_date = $this->model->changedatedbformate($todate);
     }

     
     $insertArr = array(
      'name'            => $this->input->post('central_event_name'),
      'from_date'       => $from_date,
      'to_date'         => $to_date,
      'place'           => $this->input->post('central_event_place'),
      'createdby'       => $this->loginData->UserID,
      'createdon'       => date('Y-m-d H:i:s'),
      'isdeleted'       => 0,
    );
     $this->db->where('id', $token);
     $this->db->update('tbl_central_event', $insertArr);
  //echo $this->db->last_query(); die;
  //  $insertid = $this->db->insert_id();


     $this->db->delete('tbl_central_event_transaction', array('central_event_id' => $token));

     $countresouce_person =  count($this->input->post('resouce_person')); 


     for ($i=0; $i < $countresouce_person ; $i++) { 

      $staffid = $this->input->post('resouce_person')[$i];

      $insertArr_transaction = array(
        'central_event_id'     => $token,
        'resouce_person_id'  => $staffid,
      );
      
      $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid_transaction = $this->db->$this->insert_id();

    }


    $this->db->trans_complete();

    if ($this->db->trans_status() === true){
      $this->session->set_flashdata('tr_msg', 'Successfully Update Central Event  !!!');
      redirect('/Central_event/sendinvitation/'.$token);
    }else{
      $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
      redirect('/Central_event/add');
    }

  }

  if (!empty($btnsendsevedata) && $btnsendsevedata =='sendsavedata') {

      //print_r($this->input->post()); die;

    $fromdate = $this->input->post('central_event_from_date');

    if (!empty($fromdate)) {
      $from_date = $this->model->changedatedbformate($fromdate);
    }

    $todate = $this->input->post('central_event_to_date');

    if (!empty($todate)) {
     $to_date = $this->model->changedatedbformate($todate);
   }


   $updatedArr = array(
    'name'            => $this->input->post('central_event_name'),
    'from_date'       => $from_date,
    'to_date'         => $to_date,
    'place'           => $this->input->post('central_event_place'),
    'createdby'       => $this->loginData->UserID,
    'createdon'       => date('Y-m-d H:i:s'),
    'isdeleted'       => 0,
  );
   $this->db->where('id', $token);
   $this->db->update('tbl_central_event', $updatedArr);
  //echo $this->db->last_query(); die;
   // $insertid = $this->db->insert_id();

   $this->db->delete('tbl_central_event_transaction', array('central_event_id' => $token));

   $countresouce_person =  count($this->input->post('resouce_person')); 


   for ($i=0; $i < $countresouce_person ; $i++) { 

     $staffid = $this->input->post('resouce_person')[$i];

     $insertArr_transaction = array(
      'central_event_id'     => $token,
      'resouce_person_id'    => $staffid,
    );

     $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid_transaction = $this->db->$this->insert_id();

   }

   $this->db->trans_complete();

   if ($this->db->trans_status() === true){
    $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');
    redirect('/Central_event/sendinvitation/'.$token);
  }else{
    $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
    redirect('/Central_event/add');
  }

}
}

prepareview:

  //////////////// Select Batch ////////////////
$query = "select * from mstbatch where status =0 AND isdeleted='0'";
$content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
$query = "select * from mstphase where isdeleted='0'";
$content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
$query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
$content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
$content['resouceperson_details'] = $this->model->getResoucePerson();
    ////////////////////// Select getResoucePerson ////////////////////////
$singleresouceperson_details1 = $this->model->getSingleCentralEvent($token);
  //

 //  print_r($singleresouceperson_details1); die;

$content['singleresouceperson_details'] = $singleresouceperson_details1[0];

$temp_array = [];
$resouceperson_list = $this->model->getSingleResoucePerson($token);
foreach ($resouceperson_list as $mrow) {
  $temp_array[] = $mrow->resouce_person_id;
}
$content['map_resouce_person_array'] = $temp_array;

   //////////////////// Select Event Name ////////////////////////////
$content['event_details']         = $this->model->getEventEdit();



$content['title'] = 'DA_event_detailing';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



function getDAEventDetails(){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
    LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE `tbl_da_event_detailing`.`isdeleted`='0' ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function getSingleDAEventDetails($token){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
    LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE  `tbl_da_event_detailing`.`id`='0' AND `tbl_da_event_detailing`.`isdeleted`=$token ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function delete($token)
{
try{

  $deleteArr= array(
    'isdeleted'  => 1,
  );

  $this->db->where('id',$token);  
  $this->db->update('tbl_central_event', $deleteArr);
  $this->session->set_flashdata('tr_msg' ,"Central Event Deleted Successfully");
  redirect('/Central_event/index/');

  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

}