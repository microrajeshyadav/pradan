<?php 
class Staff_sepretiringemployeeexitform extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepretiringemployeeexitform_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission    

			$query ="SELECT * FROM staff_transaction WHERE id=".$token;
			$content['staff_transaction'] = $this->db->query($query)->row();			
			$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);


			$datetime2 = new DateTime($content['staff_detail']->joiningdate);
			$datetime1 = new DateTime($content['staff_detail']->seperatedate);
			
			$interval = $datetime1->diff($datetime2);
			$content['total_servive'] = explode(" ",$interval->format('%y years %m months'));
			

			$query ="SELECT * FROM tbl_the_exit_interview_form WHERE transid=".$token;
			$content['tbl_the_exit_interview_form'] = $this->db->query($query)->row();
			
			$RequestMethod = $this->input->server('REQUEST_METHOD');
        	if($RequestMethod == "POST"){
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'Save And Submit'){
					$db_flag = 1;
					$receiverid = '';
					$updateArr = array(
			            'trans_flag'     => 23,
			            'updatedon'      => date("Y-m-d H:i:s"),
			            'updatedby'      => $this->loginData->staffid
			          );

					$this->db->where('id',$token);
					$this->db->update('staff_transaction', $updateArr);
					$insertworkflowArr = array(
			           'r_id'           => $token,
			           'type'           => 3,
			           'staffid'        => $this->loginData->staffid,
			           'sender'         => $this->loginData->staffid,
			           'receiver'       => $receiverid,
			           'senddate'       => date("Y-m-d H:i:s"),
			           'flag'           => 23,
			           'scomments'      => $this->input->post('comments'),
			           'createdon'      => date("Y-m-d H:i:s"),
			           'createdby'      => $this->loginData->staffid,
			         );
			         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
					/*echo "<pre>";
					print_r($insertworkflowArr);exit;*/
				}
        		$data = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$token,
        			'like_pradan_most_comment_1'=>$this->input->post('like_pradan_most_comment_1'),
        			'like_pradan_most_comment_2'=>$this->input->post('like_pradan_most_comment_2'),
        			'like_pradan_most_comment_3'=>$this->input->post('like_pradan_most_comment_3'),
        			'like_pradan_least_comment_1'=>$this->input->post('like_pradan_least_comment_1'),
        			'like_pradan_least_comment_2'=>$this->input->post('like_pradan_least_comment_2'),
        			'like_pradan_least_comment_3'=>$this->input->post('like_pradan_least_comment_3'),
        			'improving_the_state_of_affairs_1'=>$this->input->post('improving_the_state_of_affairs_1'),
        			'improving_the_state_of_affairs_2'=>$this->input->post('improving_the_state_of_affairs_2'),
        			'improving_the_state_of_affairs_3'=>$this->input->post('improving_the_state_of_affairs_3'),
        			'any_other_remarks'=>$this->input->post('any_other_remarks'),
        			'observations_of_the_person_facilitating_the_exit_process'=>$this->input->post('observations_of_the_person_facilitating_the_exit_process'),
        			'createdon'=>date('Y-m-d'),
        			'createdby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
        		if($content['tbl_the_exit_interview_form']){
        			$data_update = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$token,
        			'like_pradan_most_comment_1'=>$this->input->post('like_pradan_most_comment_1'),
        			'like_pradan_most_comment_2'=>$this->input->post('like_pradan_most_comment_2'),
        			'like_pradan_most_comment_3'=>$this->input->post('like_pradan_most_comment_3'),
        			'like_pradan_least_comment_1'=>$this->input->post('like_pradan_least_comment_1'),
        			'like_pradan_least_comment_2'=>$this->input->post('like_pradan_least_comment_2'),
        			'like_pradan_least_comment_3'=>$this->input->post('like_pradan_least_comment_3'),
        			'improving_the_state_of_affairs_1'=>$this->input->post('improving_the_state_of_affairs_1'),
        			'improving_the_state_of_affairs_2'=>$this->input->post('improving_the_state_of_affairs_2'),
        			'improving_the_state_of_affairs_3'=>$this->input->post('improving_the_state_of_affairs_3'),
        			'any_other_remarks'=>$this->input->post('any_other_remarks'),
        			'observations_of_the_person_facilitating_the_exit_process'=>$this->input->post('observations_of_the_person_facilitating_the_exit_process'),
        			'updatedon'=>date('Y-m-d H:i:s'),
        			'updatedby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
					$this->db->where('id', $content['tbl_the_exit_interview_form']->id);
					$flag = $this->db->update('tbl_the_exit_interview_form', $data_update);
				}else{
					$flag = $this->db->insert('tbl_the_exit_interview_form',$data);
				}
				
        		if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');} 
                else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');}
        	}
      $query ="SELECT * FROM tbl_the_exit_interview_form WHERE transid=".$token;
			$content['tbl_the_exit_interview_form'] = $this->db->query($query)->row();
			/*echo "<pre>";
			print_r($content);exit();*/
			$content['title'] = 'Staff_sepretiringemployeeexitform';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}

	public function get_staff_seperation_certificate($staffid=NULL)
   {
      try
	  {

	      $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation,g.desname as leavingdesignation,k.reason as leavingreason FROM staff_transaction as a
	         INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid
	         INNER JOIN `staff` as c on a.staffid = c.staffid
	         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
	         INNER JOIN `msdesignation` as g on k.new_designation = g.desid
	         WHERE  K.`trans_status` = 'Termination' and  a.trans_status = 'JOIN' and a.staffid =".$staffid; 
	         //echo $sql;exit();
	     return  $this->db->query($sql)->row();

		}catch (Exception $e) {
	      print_r($e->getMessage());die;
	    }

  }

	
}