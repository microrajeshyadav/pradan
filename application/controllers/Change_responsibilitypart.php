<?php 
class Change_responsibilitypart extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Change_responsibilitypart_model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token) 
	{
		//index include
		 // start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
// end permission 

			$query ="SELECT * FROM staff_transaction WHERE id =".$token;
			$content['staff_promotion'] = $this->db->query($query)->row();

			$staffid =  $content['staff_promotion']->staffid;

			$content['promotion_getchange'] = $this->Change_responsibilitypart_model->get_staff_changeresponsibility_detail($token);
			// print_r($content['promotion_getchange']); die;
			$content['staffid'] = $content['staff_promotion']->staffid;

			$query = "SELECT * FROM tbl_promotionchange_responsibility WHERE transid = ".$token;
			$content['promotion_detail'] = $this->db->query($query)->row();
			//print_r($content['experience_detail']); 
			//echo $content['experience_detail']->id; die;



			$RequestMethod = $this->input->server("REQUEST_METHOD");


			if($RequestMethod == 'POST')
			{

				// print_r($this->input->post()); die;
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'save & submit'){

					$db_flag = 1;
				}

				$insertArray = array(

					'staffid'      => $content['staff_promotion']->staffid,
					'transid'      => $token,
					'promotion_no' => $this->input->post('promotion_no'),
					'createdby'    => $this->loginData->staffid,
					'flag'         => $db_flag,
				);

				if($content['promotion_detail'])
				{
					$updateArray = array(

					'staffid'      => $content['staff_promotion']->staffid,
					'transid'      => $token,
					'promotion_no' => $this->input->post('promotion_no'),
					'updatedon'    => date("Y-m-d H:i:s"),
					'updatedby'    => $this->loginData->staffid,
					'flag'         => $db_flag,	
					);
					$this->db->where('id', $content['promotion_detail']->id);
					$flag = $this->db->update('tbl_promotionchange_responsibility', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_promotionchange_responsibility',$insertArray);
				}
				//echo $this->db->last_query(); die;


				if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
				redirect(current_url());
			} 
			else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
			redirect(current_url());
		}

	}

	$content['getchange_edname'] = $this->Change_responsibilitypart_model->getEDName();
	$content['token'] = $token;
	$content['title'] = 'Change_responsibilitypart';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}