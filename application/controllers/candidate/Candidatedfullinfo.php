<?php 

/**
* Candidate Full Information controller
*/
class Candidatedfullinfo extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		$this->load->model("Candidatedetails_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model(__CLASS__ . '_model', 'model');	

		$check = $this->session->userdata('loginData');
		///// Check Session //////	Global
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('loginData');
		/*echo "<pre>";
		print_r($this->model);exit();*/
	}

	public function index()
	{
		try{
			 //echo $this->loginData->candidateid; die; 
			$content['candidatedetails']         = $this->model->getCandidateDetails($this->loginData->candidateid);
			// echo "<pre>";
			// print_r($content['candidatedetails']);
			// die;
			$content['education'] = $this->model->getedicationstatus($this->loginData->candidateid);
			$RequestMethod = $this->input->server('REQUEST_METHOD');  

			if($RequestMethod == 'POST'){
					
				
					
				$CampusType = $this->input->post('campustype');
				/*echo "<pre>";
				print_r($_POST);
				print_r($_FILES);
				die;*/
				
				
	

				if(isset($CampusType) && $CampusType=='on' && $this->input->post('SaveDatasend')=='SaveData'  && ($this->input->post('BDFFormStatus')==97 || $this->input->post('BDFFormStatus')==1)){

						//echo $_FILES['ugmaigrationcertificate']['name'];
						//	die;

						//// Step II ////////

					if (isset($_FILES['photoupload']['name'])) {

		@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 

		$OriginalName = $_FILES['photoupload']['name']; 
		$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['photoupload']['tmp_name'];
		$targetPath = FCPATH . "datafiles/";
		$targetFile = $targetPath . $encryptedName;
		/*echo "<pre>";
		echo "target : ".$targetFile ."tempFile".$tempFile;*/
		$uploadResult = move_uploaded_file($tempFile,$targetFile);


		if($uploadResult == true){
			$encryptedImage = $encryptedName;
		}else{

			die("Error uploading Profile2 Photo ");
			$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
			redirect(current_url());
		}

	}else{

		//$encryptedImage = $this->input->post('oldphotoupload');

		$OriginalName = $this->input->post('oldphotoupload');
		$encryptedImage = $this->input->post('oldencryptedphotoname');

	}
	/*echo "<pre>";
	echo "deepak";
	print_r($OriginalName);
	print_r($encryptedImage);exit();*/
	$updateArrCandidate = array(
		
		'originalphotoname'	=> $OriginalName,
		'encryptedphotoname'=> $encryptedImage,
		'updatedon'      	  => date('Y-m-d H:i:s'),
		'updatedby'      	  => $this->loginData->candidateid, // login user id
		'isdeleted'      	  => 0, 
	);

	 $this->db->where('candidateid', $this->loginData->candidateid);

	 $this->db->update('tbl_candidate_registration', $updateArrCandidate);

	
	if(@$this->input->post('ugpercentage')){
		$updateUGmarksArr = array(
			'ugpercentage'=>$this->input->post('ugpercentage')
		);
		$this->db->where('candidateid', $this->loginData->candidateid);
	 	$this->db->update('tbl_candidate_registration', $updateUGmarksArr);
	}
	if(@$this->input->post('pgpercentage')){
		$updatePGmarksArr = array(
			'pgpercentage'=>$this->input->post('pgpercentage')
		);
		$this->db->where('candidateid', $this->loginData->candidateid);
	 	$this->db->update('tbl_candidate_registration', $updatePGmarksArr);
	}

					/*if (@ $_FILES['10thcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['10thcertificate']['name']))); 


						$matricoriginalName = $_FILES['10thcertificate']['name']; 
						$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['10thcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $matricencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$matricencryptedImage = $matricencryptedName;
						}else{

							die("Error uploading 10thcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
							redirect(current_url());
						}

					}
					else{
						$matricoriginalName = $this->input->post('originalmatriccertificate');
						$matricencryptedImage = $this->input->post('oldmatriccertificate');
					}


					if (@ $_FILES['12thcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['12thcertificate']['name']))); 

						$hscoriginalName = $_FILES['12thcertificate']['name']; 
						$hscencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['12thcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $hscencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$hscencryptedImage = $hscencryptedName;
						}else{

							die("Error uploading 12thcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
							redirect(current_url());
						}

					}
					else{
						$hscoriginalName = $this->input->post('originalhsccertificate');
						$hscencryptedImage = $this->input->post('oldhsccertificate');
					}*/

					// if (count($_FILES['10thcertificate']['name']) > 0) {
					// 	$i = 0;
					// 	$matricoriginalNameArray = array();
					// 	$matricencryptedNameArray = array();
					// 	foreach($_FILES['10thcertificate']['name'] as $res){

					// 		// echo "<pre>";
					// 		// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
					// 		//die;	
					// 		@  $ext = end((explode(".", $_FILES['10thcertificate']['name'][$j])));
					// 		$matricoriginalName1 = $_FILES['10thcertificate']['name'][$i];
					// 		array_push($matricoriginalNameArray, $matricoriginalName1); 
					// 		$matricencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
					// 		array_push($matricencryptedNameArray, $matricencryptedName1); 
					// 		$tempFile = $_FILES['10thcertificate']['tmp_name'][$i];
					// 		$targetPath = FCPATH . "datafiles/educationalcertificate/";
					// 		$targetFile = $targetPath . $matricencryptedName1;

					// 		$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
					// 		if($uploadResult == true){
					// 			$matricencryptedImage = $matricencryptedNameArray;
					// 		}else{

					// 			die("Error uploading 10thcertificate Document ");
					// 			$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
					// 			redirect(current_url());
					// 		}
					// 		$i++;
					// 	}

					// }
					// else{
					// 	$matricoriginalName = $this->input->post('originalmatriccertificate');
					// 	$matricencryptedImage = $this->input->post('oldmatriccertificate');

					// }
					

						// echo count($_FILES['10thcertificate']['name']);
						// 	die;

//$matricencryptedImage = $this->input->post('oldmatriccertificate');

					

				
					$matricoriginalName1='';
					$matricencryptedImage=array();
					$matricoriginalNameArray = array();
					$matricencryptedNameArray = array();
					$matricoriginalName = '';
					if(isset($_FILES['10thcertificate']['name'][0]))
					{
						$i = 0;
						//matricencryptedNameArray
						if($_FILES['10thcertificate']['name'][0] != NULL){
							foreach($_FILES['10thcertificate']['name'] as $res){

								
								@  $ext = end((explode(".", $_FILES['10thcertificate']['name'][$i]))); 

								$matricoriginalName1 = $_FILES['10thcertificate']['name'][$i];
								array_push($matricoriginalNameArray, $matricoriginalName1); 
								$matricencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
								array_push($matricencryptedNameArray, $matricencryptedName1); 
								$tempFile = $_FILES['10thcertificate']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $matricencryptedName1;

								$uploadResult = move_uploaded_file($tempFile,$targetFile);
								
								if($uploadResult == true){
									$matricencryptedImage = $matricencryptedNameArray;
								}else{

									die("Error uploading 10thcertificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
									redirect(current_url());
								}
								$i++;
							}
						}else{
							$matricoriginalName = $this->input->post('originalmatriccertificate');
							$matricencryptedImage = $this->input->post('oldmatriccertificate');
						}

					}
				
					else{
						$matricoriginalName = $this->input->post('originalmatriccertificate');
						$matricencryptedImage = $this->input->post('oldmatriccertificate');

					}
					
						
//print_r($matricencryptedNameArray);


				//12certificate multiple record	
				$hscoriginalNameArray = array();
						$hscencryptedNameArray = array();
						$hscencryptedImage='';	
						
					if(isset($_FILES['12thcertificate']['name'][0]))
				
					{			
					
						$j = 0;
						if($_FILES['12thcertificate']['name'][0] != NULL){
							foreach($_FILES['12thcertificate']['name'] as $res){
								@  $ext = end((explode(".", $_FILES['12thcertificate']['name'][$j]))); 

								$hscoriginalName1 = $_FILES['12thcertificate']['name'][$j];
								array_push($hscoriginalNameArray, $hscoriginalName1);  
								$hscencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
								array_push($hscencryptedNameArray, $hscencryptedName1); 
								$tempFile = $_FILES['12thcertificate']['tmp_name'][$j];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $hscencryptedName1;
								$uploadResult = move_uploaded_file($tempFile,$targetFile);


								if($uploadResult == true){
									$hscencryptedImage = $hscencryptedNameArray;
								}else{
									/*echo $uploadResult.$hscoriginalName1;
									print_r($targetPath . $hscencryptedName1);
									exit();*/
									//die("Error uploading 12thcertificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
									redirect(current_url());
								}
								$j++;
							}
						}else{
							$hscoriginalName = $this->input->post('originalhsccertificate');
							$hscencryptedImage = $this->input->post('oldhsccertificate');
						}
					
				}
					else{
						$hscoriginalName = $this->input->post('originalhsccertificate');
						$hscencryptedImage = $this->input->post('oldhsccertificate');

					}



						$ugmaigrationcheck=$this->input->post('ugmaigrationcheck');
						 //count($_FILES['ugmaigrationcertificate']['name']);

						//die;

						

					// if (@ $_FILES['ugcertificate']['name'] != NULL) {

					// 	@  $ext = end((explode(".", $_FILES['ugcertificate']['name']))); 

					// 	$ugoriginalName = $_FILES['ugcertificate']['name']; 
					// 	$ugencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					// 	$tempFile = $_FILES['ugcertificate']['tmp_name'];
					// 	$targetPath = FCPATH . "datafiles/educationalcertificate/";
					// 	$targetFile = $targetPath . $ugencryptedName;
					// 	$uploadResult = move_uploaded_file($tempFile,$targetFile);

					// 	if($uploadResult == true){
					// 		$ugencryptedImage = $ugencryptedName;
					// 	}else{

					// 		die("Error uploading ugcertificate Document ");
					// 		$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
					// 		redirect(current_url());
					// 	}

					// }
					// else{
					// 	$ugoriginalName = $this->input->post('originalugcertificate');
					// 	$ugencryptedImage = $this->input->post('oldugcertificate');
					// }

					if (@ $_FILES['signed_offer_letter']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['signed_offer_letter']['name']))); 

						$signedofferletteroriginalName = $_FILES['signed_offer_letter']['name']; 
						$signedofferletterencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_signed." . $ext;
						$tempFile = $_FILES['signed_offer_letter']['tmp_name'];
						$targetPath = FCPATH . "pdf_offerletters/";
						$targetFile = $targetPath . $signedofferletterencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$signedofferletterencryptedImage = $signedofferletterencryptedName;
						}else{

							die("Error uploading signed offer letter Document ");
							$this->session->set_flashdata("er_msg", "Error uploading signed offer letter Document");
							redirect(current_url());
						}

					}
					else{
						$signedofferletteroriginalName = $this->input->post('originalofferlettername');
						$signedofferletterencryptedImage = $this->input->post('oldsignedofferletter');
					}
						

					// if (@ $_FILES['pgcertificate']['name'] != NULL) {

					// 	@  $ext = end((explode(".", $_FILES['pgcertificate']['name']))); 


					// 	$pgriginalName = $_FILES['pgcertificate']['name']; 
					// 	$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					// 	$tempFile = $_FILES['pgcertificate']['tmp_name'];
					// 	$targetPath = FCPATH . "datafiles/educationalcertificate/";
					// 	$targetFile = $targetPath . $pgencryptedName;
					// 	$uploadResult = move_uploaded_file($tempFile,$targetFile);

					// 	if($uploadResult == true){
					// 		$pgencryptedImage = $pgencryptedName;
					// 	}else{

					// 		die("Error uploading pgcertificate Document ");
					// 		$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
					// 		redirect(current_url());
					// 	}

					// }
					// else{
					// 	$pgriginalName = $this->input->post('originalpgcertificate');
					// 	$pgencryptedImage = $this->input->post('oldpgcertificate');
					// }



					if (@ $_FILES['otherscertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


						$OtheroriginalName = $_FILES['otherscertificate']['name']; 
						$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['otherscertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $otherencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$Otherscertificateencryptedimage = $otherencryptedName;
						}else{

							die("Error uploading otherscertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
							redirect(current_url());
						}

					}
					else{
						$OtheroriginalName = $this->input->post('originalotherscertificate');
						$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
					}

					// print_r($Otherscertificateencryptedimage);
					// die;
				$pgoriginalNameArray = array();
				$pgencryptedNameArray = array();
				$pgencryptedImage = array();
				if($this->input->post('pgmaigrationcheck') == 'on'){

					// echo "string";die();
					if (isset($_FILES['pgmaigrationcertificate']['name'])) {

						@  $ext = end((explode(".", $_FILES['pgmaigrationcertificate']['name'])));

						$pgmaigrationoriginalName = $_FILES['pgmaigrationcertificate']['name']; 
						$pgmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
						$tempFile = $_FILES['pgmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $pgmaigrationencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$pgmaigrationencryptedImage = $pgmaigrationencryptedName;
						}else{

							die("Error uploading pg maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$pgmaigrationoriginalName = $this->input->post('originalpgmigrationcertificate');
						$pgmaigrationencryptedName = $this->input->post('oldpgmigrationcertificate');
					}

					
					$pgmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('pgmaigrationdate'));
					$updatepgmigrationdata = array(
						'pgmigration_certificate' => 1,
						'pgmigration_certificate_date' => $pgmaigrationdate,
						'pgmigration_original_certificate_upload' => $pgmaigrationoriginalName,
						'pgmigration_encrypted_certificate_upload' => $pgmaigrationencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgmigrationdata);
				}

				else
				{	
					$pgoriginalNameArray = array();
					$pgencryptedNameArray = array();
					$pgencryptedImage='';
					if(isset($_FILES['pgcertificate']['name'][0]))
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificate']['name'][$i]))); 

							$pgoriginalName1 = $_FILES['pgcertificate']['name'][$i];
							array_push($pgoriginalNameArray, $pgoriginalName1); 
							$pgencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgencryptedNameArray, $pgencryptedName1); 
							$tempFile = $_FILES['pgcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgencryptedImage = $pgencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgoriginalName = $this->input->post('originalpgcertificate');
						$pgencryptedImage = $this->input->post('oldpgcertificate');

					}
					
					
				}

				// pg certificate update on given date
				$pgdocoriginalNameArray = array();
				$pgdocencryptedNameArray = array();
				$pgdocencryptedImage = array();
				$pgdocoriginalName = array();
				if($this->input->post('pgcertificatecheck') == 'on'){

					// echo "string";die();
					if (isset($_FILES['pgcertificatedoc']['name'])) {
						if($_FILES['pgcertificatedoc']['name'] !=NULL){
							@  $ext = end((explode(".", $_FILES['pgcertificatedoc']['name'])));

							$pgdocoriginalName = $_FILES['pgcertificatedoc']['name']; 
							$pgdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
							$tempFile = $_FILES['pgcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgdocencryptedName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$pgdocencryptedImage = $pgdocencryptedName;
							}else{

								die("Error uploading pg Certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
								redirect(current_url());
							}
						}else{
							$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
							$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
						}

					}
					else{
						$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
						$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
					}

					
					$pgcertificatedate = $this->gmodel->changedatedbformate($this->input->post('pgcertificatedate'));
					$updatepgdocdata = array(
						'pgdoc_certificate' => 1,
						'pgdoc_certificate_date' => $pgcertificatedate,
						'pgdoc_original_certificate_upload' => $pgdocoriginalName,
						'pgdoc_encrypted_certificate_upload' => $pgdocencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgdocdata);
				}

				else
				{	
					$pgfileoriginalNameArray = array();
					$pgfileencryptedNameArray = array();
					$pgfileencryptedImage = '';
					$pgfileoriginalName = '';
					if(isset($_FILES['pgcertificatefile']['name'][0]))
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificatefile']['name'][$i]))); 

							$pgfileoriginalName1 = $_FILES['pgcertificatefile']['name'][$i];
							array_push($pgfileoriginalNameArray, $pgfileoriginalName1); 
							$pgfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgfileencryptedNameArray, $pgfileencryptedName1); 
							$tempFile = $_FILES['pgcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgfileencryptedImage = $pgfileencryptedNameArray;
							}else{

								die("Error uploading pg certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg certificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgfileoriginalName = $this->input->post('originalpgdoccertificate');
						$pgfileencryptedImage = $this->input->post('oldpgdoccertificate');
					}
				}


				// die('end');

				// echo $this->input->post('ugmaigrationcheck');
				// die;
				if($this->input->post('ugmaigrationcheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				

					
					if ($_FILES['ugmaigrationcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['ugmaigrationcertificate']['name'])));

						$ugmaigrationoriginalName = $_FILES['ugmaigrationcertificate']['name']; 

						$ugmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

						$tempFile = $_FILES['ugmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $ugmaigrationencryptedName;
						//echo $targetFile;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);
						

						if($uploadResult == true){
							$ugmaigrationencryptedImage = $ugmaigrationencryptedName;
						}else{

							die("Error uploading pg maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$ugmaigrationoriginalName = $this->input->post('originalugmigrationcertificate');
						$ugmaigrationencryptedName = $this->input->post('oldugmigrationcertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('ugmaigrationdate'));
					$updateugmigrationdata = array(
						'ugmigration_certificate' => 1,
						'ugmigration_certificate_date' => $ugmaigrationdate,
						'ugmigration_original_certificate_upload' => $ugmaigrationoriginalName,
						'ugmigration_encrypted_certificate_upload' => $ugmaigrationencryptedName
					);
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateugmigrationdata);
				}

					else
						{
							$ugoriginalNameArray = array();
						$ugencryptedNameArray = array();
						$ugencryptedImage='';
						$ugoriginalName='';
							if(isset($_FILES['ugcertificate']['name'][0]))
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificate']['name'][$i]))); 

							$ugoriginalName1 = $_FILES['ugcertificate']['name'][$i];
							array_push($ugoriginalNameArray, $ugoriginalName1); 
							$ugencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugencryptedNameArray, $ugencryptedName1); 
							$tempFile = $_FILES['ugcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugencryptedImage = $ugencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugoriginalName = $this->input->post('originalugcertificate');
						$ugencryptedImage = $this->input->post('oldugcertificate');

					}
				}


				//UG Certificate upload on given date section
				$ugfileencryptedImage='';
				$ugfileoriginalName='';
				if($this->input->post('ugcertificatecheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				

					
					if (isset($_FILES['ugcertificatedoc']['name'])) {
						if($_FILES['ugcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['ugcertificatedoc']['name'])));

							$ugdocoriginalName = $_FILES['ugcertificatedoc']['name']; 

							$ugdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['ugcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugdocencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$ugdocencryptedImage = $ugdocencryptedName;
							}else{

								die("Error uploading ug doc maigration Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ug doc maigration Document");
								redirect(current_url());
							}
						}else{
							$ugdocoriginalName = $this->input->post('originalugdoccertificate');
							$ugdocencryptedImage = $this->input->post('oldugdoccertificate');
						}

					}
					else{
						$ugdocoriginalName = $this->input->post('originalugdoccertificate');
						$ugdocencryptedImage = $this->input->post('oldugdoccertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugcertificatedate = $this->gmodel->changedatedbformate($this->input->post('ugcertificatedate'));
					$updocdateugcertificatedata = array(
						'ugdoc_certificate' => 1,
						'ugdoc_certificate_date' => $ugcertificatedate,
						'ugdoc_original_certificate_doc_upload' => $ugdocoriginalName,
						'ugdoc_encrypted_certificate_doc_upload' => $ugdocencryptedName
					);
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updocdateugcertificatedata);
				}

					else
						{
							$ugfileoriginalNameArray = array();
						$ugfileencryptedNameArray = array();
						$ugfileencryptedImage='';
						$ugfileoriginalName='';
							if(isset($_FILES['ugcertificatefile']['name'][0]))
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificatefile']['name'][$i]))); 

							$ugfileoriginalName1 = $_FILES['ugcertificatefile']['name'][$i];
							array_push($ugfileoriginalNameArray, $ugfileoriginalName1); 
							$ugdocencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugfileencryptedNameArray, $ugfileencryptedName1); 
							$tempFile = $_FILES['ugcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugfileencryptedImage = $ugfileencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugfileoriginalName = $this->input->post('originalugdoccertificate');
						$ugfileencryptedImage = $this->input->post('oldugdoccertificate');

					}
				}

					

						

					
				
				
				if(isset($_FILES['10thcertificate']['name'][0]))
				{
					if($_FILES['10thcertificate']['name'][0] != NULL){
						$matricencryptedImage = implode('|',$matricencryptedNameArray);
					}else{
						$matricencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
					}
				
				}
				else
				{
				$matricencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
				
				}

				

			



				if(isset($_FILES['12thcertificate']['name'][0]))
				{
					if($_FILES['12thcertificate']['name'][0] != NULL){
						$hscencryptedImage    =	implode('|',$hscencryptedNameArray);
					}else{
						$hscencryptedImage    =	$content['candidatedetails']->encrypthsccertificate;
					}
				}
				else
				{
				
				 	$hscencryptedImage    =	$content['candidatedetails']->encrypthsccertificate;
				

				}
				if(isset($_FILES['ugcertificate']['name'][0]))
				{
				
					$ugencryptedImage = implode('|',$ugencryptedImage);
				
				}
				else
				{
				
					$ugencryptedImage = $content['candidatedetails']->encryptugcertificate;
				
				}
				if(isset($_FILES['pgcertificate']['name'][0]))
				{
				
					$pgencryptedImage = implode('|',$pgencryptedImage);
				}
				else
				{
				
			 		$pgencryptedImage =$content['candidatedetails']->encryptpgcertificate;

				}

				
				if (isset($_FILES['ugcertificatefile']['name'])) {
					if($_FILES['ugcertificatefile']['name'] != NULL){
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}else{
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}
				}
				else
				{
				
					$ugfileencryptedImage =$content['candidatedetails']->ugdocencryptedImage;

				}
				if (isset($_FILES['pgcertificatefile']['name'])) {
					if($_FILES['pgcertificatefile']['name'] != NULL){
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}else{
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}
				}
				else
				{
				
					$pgfileencryptedImage =$content['candidatedetails']->pgdocencryptedImage;

				}




					$updateeducationalcert	 = array(
						'encryptmetriccertificate'   => $matricencryptedImage,
						'encrypthsccertificate'      => $hscencryptedImage,
						'encryptugcertificate'       => $ugencryptedImage,
						'encryptpgcertificate'       => $pgencryptedImage,
						'encryptothercertificate'    => $Otherscertificateencryptedimage,
						'encryptofferlettername'   	 => $signedofferletterencryptedImage,
						'originalmetriccertificate'  => $matricencryptedImage,
						'originalhsccertificate'     => $hscencryptedImage,
						'originalugcertificate'      => $ugencryptedImage,
						'originalpgcertificate'      => $pgencryptedImage,
						'originalothercertificate'   => $Otherscertificateencryptedimage,
						'originalofferlettername'    => $signedofferletteroriginalName,
						'ugdocencryptedImage'      => $ugfileencryptedImage,
						'pgdocencryptedImage'      => $pgfileencryptedImage,
						'ugdocoriginalName'      => $ugfileoriginalName,
						'pgdocoriginalName'      => $pgfileoriginalName,
						'updatedby'                  => $this->loginData->candidateid,
						'updatedon'                  => date('Y-m-d H:i:s'),

					);

						
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateeducationalcert);

						

		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 
					 //echo "<pre>";
					// print_r($_FILES);
					// print_r($this->input->post());
			if(isset($_FILES['experiencedocument']['name'])){		
			 	$count_orgname = count($_FILES['experiencedocument']['name']);   //die;
			}else{
			 	$count_orgname = 0;
			}

		   $query = $this->db->query('SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid);
		   $numrow = $query->num_rows();

		   //echo $this->db->last_query(); die;

		   $sql = 'SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid;
		   $result2 = $this->db->query($sql)->result();

		   if($numrow > 0){


		   	for ($i=0; $i < $count_orgname; $i++) { 

		   		 $workexpid = $result2[$i]->id;	

		   		@ $tblworkexperanceid = $this->input->post('tblworkexperanceid')[$i]; 

		   		if (@ $_FILES['experiencedocument']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


		   			$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
		   			$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptedexpdocumentName;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument = $encryptedexpdocumentName;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
		   			$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
		   		}

		   		 		// salary slip1

			if (@ $_FILES['salary_slip1']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip1']['name'][$i])); 


				$Originalnamesalaryslip1 = $_FILES['salary_slip1']['name'][$i]; 
				$encryptednamesalaryslip1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip1']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip1;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument1 = $encryptednamesalaryslip1;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip1 = $this->input->post('originalsalaryslip1')[$i];
				$encrypteddocument1 =  $this->input->post('oldexperiencesalaryslip1')[$i];
			}

		   		// salary slip2

			if (@ $_FILES['salary_slip2']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip2']['name'][$i])); 

				$Originalnamesalaryslip2 = $_FILES['salary_slip2']['name'][$i]; 
				$encryptednamesalaryslip2 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip2']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip2;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument2 = $encryptednamesalaryslip2;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip2 = $this->input->post('originalsalaryslip2')[$i];
				$encrypteddocument2 =  $this->input->post('oldexperiencesalaryslip2')[$i];
			}


		   				// // salary slip 3

			if (@ $_FILES['salary_slip3']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip3']['name'][$i])); 

				$Originalnamesalaryslip3 = $_FILES['salary_slip3']['name'][$i]; 
				$encryptednamesalaryslip3 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip3']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip3;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument3 = $encryptednamesalaryslip3;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip3 = $this->input->post('originalsalaryslip3')[$i];
				$encrypteddocument3 =  $this->input->post('oldexperiencesalaryslip3')[$i];
			}

		   		$updateWorkExperience1 = array(

		   			'originaldocumentname'  => $OriginalexperiencedocumentName,
		   			'encrypteddocumnetname' => $encrypteddocument,
		   			'originalsalaryslip1'   => $Originalnamesalaryslip1,
					'encryptedsalaryslip1'  => $encrypteddocument1,
					'originalsalaryslip2'   => $Originalnamesalaryslip2,
					'encryptedsalaryslip2'  => $encrypteddocument2,
					'originalsalaryslip3'   => $Originalnamesalaryslip3,
					'encryptedsalaryslip3'  => $encrypteddocument3,
		   			'updateon'              => date('Y-m-d H:i:s'),
			        'updatedby'             => $this->loginData->candidateid, // 
			    );

		   		$this->db->where('id', $tblworkexperanceid);
		   		$this->db->update('tbl_work_experience', $updateWorkExperience1);
	   		
		   	}
		   }

		  
		   $this->db->trans_complete();

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   }else{

		   	$updateStatusInfo	 = array(

		   		'BDFFormStatus'  => $this->input->post('BDFFormStatus'),
		   	);

		   	$this->db->where('candidateid',$this->loginData->candidateid);
		   	$this->db->update('tbl_candidate_registration', $updateStatusInfo);

		   	//echo $this->db->last_query(); die;

		   	$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	

		   	redirect('candidate/Candidatedfullinfo/preview');
		   	exit;

		   }

		}elseif(isset($CampusType) && $CampusType=='off' && $this->input->post('SaveDatasend')=='SaveData'  && $this->input->post('BDFFormStatus')==97){


			// print_r($this->input->post());
			// die;

						//// Step II ////////
			$pgencryptedImage ='';
			$Otherscertificateencryptedimage ='';


			/*if ($_FILES['10thcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['10thcertificate']['name']))); 


				$matricoriginalName = $_FILES['10thcertificate']['name']; 
				$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['10thcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $matricencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$matricencryptedImage = $matricencryptedName;
				}else{

					die("Error uploading 10thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$matricoriginalName = $this->input->post('originalmatriccertificate');
				$matricencryptedImage = $this->input->post('oldmatriccertificate');
			}


			if ($_FILES['12thcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['12thcertificate']['name']))); 

				$hscoriginalName = $_FILES['12thcertificate']['name']; 
				$hscencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['12thcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $hscencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$hscencryptedImage = $hscencryptedName;
				}else{

					die("Error uploading 12thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$hscoriginalName = $this->input->post('originalhsccertificate');
				$hscencryptedImage = $this->input->post('oldhsccertificate');
			}*/

	if(@$this->input->post('ugpercentage')){
		$updateUGmarksArr = array(
			'ugpercentage'=>$this->input->post('ugpercentage')
		);
		$this->db->where('candidateid', $this->loginData->candidateid);
	 	$this->db->update('tbl_candidate_registration', $updateUGmarksArr);
	}
	if(@$this->input->post('pgpercentage')){
		$updatePGmarksArr = array(
			'pgpercentage'=>$this->input->post('pgpercentage')
		);
		$this->db->where('candidateid', $this->loginData->candidateid);
	 	$this->db->update('tbl_candidate_registration', $updatePGmarksArr);
	}		
					$matricoriginalName1='';
					$matricencryptedImage=array();
					$matricoriginalNameArray = array();
					$matricencryptedNameArray = array();
					if(isset($_FILES['10thcertificate']['name'][0]))
					{
						$i = 0;
						//matricencryptedNameArray
						if($_FILES['10thcertificate']['name'][0]!=NULL){
							foreach($_FILES['10thcertificate']['name'] as $res){
	
								@  $ext = end((explode(".", $_FILES['10thcertificate']['name'][$i]))); 

								$matricoriginalName1 = $_FILES['10thcertificate']['name'][$i];
								array_push($matricoriginalNameArray, $matricoriginalName1); 
								$matricencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
								array_push($matricencryptedNameArray, $matricencryptedName1); 
								$tempFile = $_FILES['10thcertificate']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $matricencryptedName1;

								$uploadResult = move_uploaded_file($tempFile,$targetFile);
								
								if($uploadResult == true){
									$matricencryptedImage = $matricencryptedNameArray;
								}else{

									die("Error uploading 10thcertificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
									redirect(current_url());
								}
								$i++;
							}
						}else{
							$matricoriginalName = $this->input->post('originalmatriccertificate');
							$matricencryptedImage = $this->input->post('oldmatriccertificate');
						}

					}
				
					else{
						$matricoriginalName = $this->input->post('originalmatriccertificate');
						$matricencryptedImage = $this->input->post('oldmatriccertificate');

					}
					
						
//print_r($matricencryptedNameArray);


				//12certificate multiple record	
				$hscoriginalNameArray = array();
						$hscencryptedNameArray = array();
						$hscencryptedImage='';	
						
					if(isset($_FILES['12thcertificate']['name'][0]))
				
					{			
					
						$j = 0;
						if($_FILES['12thcertificate']['name'][0] != NULL){
							foreach($_FILES['12thcertificate']['name'] as $res){
								@  $ext = end((explode(".", $_FILES['12thcertificate']['name'][$j]))); 

								$hscoriginalName1 = $_FILES['12thcertificate']['name'][$j];
								array_push($hscoriginalNameArray, $hscoriginalName1);  
								$hscencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
								array_push($hscencryptedNameArray, $hscencryptedName1); 
								$tempFile = $_FILES['12thcertificate']['tmp_name'][$j];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $hscencryptedName1;
								$uploadResult = move_uploaded_file($tempFile,$targetFile);


								if($uploadResult == true){
									$hscencryptedImage = $hscencryptedNameArray;
								}else{
									/*echo $uploadResult.$hscoriginalName1;
									print_r($targetPath . $hscencryptedName1);
									exit();*/
									//die("Error uploading 12thcertificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
									redirect(current_url());
								}
								$j++;
							}
						}else{
							$hscoriginalName = $this->input->post('originalhsccertificate');
							$hscencryptedImage = $this->input->post('oldhsccertificate');
						}
					
				}
					else{
						$hscoriginalName = $this->input->post('originalhsccertificate');
						$hscencryptedImage = $this->input->post('oldhsccertificate');

					}



						$ugmaigrationcheck=$this->input->post('ugmaigrationcheck');
						 //count($_FILES['ugmaigrationcertificate']['name']);

						//die;

						

					// if (@ $_FILES['ugcertificate']['name'] != NULL) {

					// 	@  $ext = end((explode(".", $_FILES['ugcertificate']['name']))); 

					// 	$ugoriginalName = $_FILES['ugcertificate']['name']; 
					// 	$ugencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					// 	$tempFile = $_FILES['ugcertificate']['tmp_name'];
					// 	$targetPath = FCPATH . "datafiles/educationalcertificate/";
					// 	$targetFile = $targetPath . $ugencryptedName;
					// 	$uploadResult = move_uploaded_file($tempFile,$targetFile);

					// 	if($uploadResult == true){
					// 		$ugencryptedImage = $ugencryptedName;
					// 	}else{

					// 		die("Error uploading ugcertificate Document ");
					// 		$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
					// 		redirect(current_url());
					// 	}

					// }
					// else{
					// 	$ugoriginalName = $this->input->post('originalugcertificate');
					// 	$ugencryptedImage = $this->input->post('oldugcertificate');
					// }

					if (@ $_FILES['signed_offer_letter']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['signed_offer_letter']['name']))); 

						$signedofferletteroriginalName = $_FILES['signed_offer_letter']['name']; 
						$signedofferletterencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_signed." . $ext;
						$tempFile = $_FILES['signed_offer_letter']['tmp_name'];
						$targetPath = FCPATH . "pdf_offerletters/";
						$targetFile = $targetPath . $signedofferletterencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$signedofferletterencryptedImage = $signedofferletterencryptedName;
						}else{

							die("Error uploading signed offer letter Document ");
							$this->session->set_flashdata("er_msg", "Error uploading signed offer letter Document");
							redirect(current_url());
						}

					}
					else{
						$signedofferletteroriginalName = $this->input->post('originalofferlettername');
						$signedofferletterencryptedImage = $this->input->post('oldsignedofferletter');
					}
						

					// if (@ $_FILES['pgcertificate']['name'] != NULL) {

					// 	@  $ext = end((explode(".", $_FILES['pgcertificate']['name']))); 


					// 	$pgriginalName = $_FILES['pgcertificate']['name']; 
					// 	$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					// 	$tempFile = $_FILES['pgcertificate']['tmp_name'];
					// 	$targetPath = FCPATH . "datafiles/educationalcertificate/";
					// 	$targetFile = $targetPath . $pgencryptedName;
					// 	$uploadResult = move_uploaded_file($tempFile,$targetFile);

					// 	if($uploadResult == true){
					// 		$pgencryptedImage = $pgencryptedName;
					// 	}else{

					// 		die("Error uploading pgcertificate Document ");
					// 		$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
					// 		redirect(current_url());
					// 	}

					// }
					// else{
					// 	$pgriginalName = $this->input->post('originalpgcertificate');
					// 	$pgencryptedImage = $this->input->post('oldpgcertificate');
					// }



					if (@ $_FILES['otherscertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


						$OtheroriginalName = $_FILES['otherscertificate']['name']; 
						$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['otherscertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $otherencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$Otherscertificateencryptedimage = $otherencryptedName;
						}else{

							die("Error uploading otherscertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
							redirect(current_url());
						}

					}
					else{
						$OtheroriginalName = $this->input->post('originalotherscertificate');
						$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
					}

					// print_r($Otherscertificateencryptedimage);
					// die;
					$pgoriginalNameArray = array();
						$pgencryptedNameArray = array();
						$pgencryptedImage=array();
						// echo $this->input->post('pgmaigrationcheck');
						// die;
				if($this->input->post('pgmaigrationcheck') == 'on'){


					if ($_FILES['pgmaigrationcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['pgmaigrationcertificate']['name'])));

						$pgmaigrationoriginalName = $_FILES['pgmaigrationcertificate']['name']; 
						$pgmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
						$tempFile = $_FILES['pgmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $pgmaigrationencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$pgmaigrationencryptedImage = $pgmaigrationencryptedName;
						}else{

							die("Error uploading pg maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$pgmaigrationoriginalName = $this->input->post('originalpgmigrationcertificate');
						$pgmaigrationencryptedName = $this->input->post('oldpgmigrationcertificate');
					}

					
					$pgmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('pgmaigrationdate'));
					$updatepgmigrationdata = array(
						'pgmigration_certificate' => 1,
						'pgmigration_certificate_date' => $pgmaigrationdate,
						'pgmigration_original_certificate_upload' => $pgmaigrationoriginalName,
						'pgmigration_encrypted_certificate_upload' => $pgmaigrationencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgmigrationdata);
				}

				else
				{	
					$pgoriginalNameArray = array();
						$pgencryptedNameArray = array();
						$pgencryptedImage='';
					if($_FILES['pgcertificate']['name'][0]!=NULL)
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificate']['name'][$i]))); 

							$pgoriginalName1 = $_FILES['pgcertificate']['name'][$i];
							array_push($pgoriginalNameArray, $pgoriginalName1); 
							$pgencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgencryptedNameArray, $pgencryptedName1); 
							$tempFile = $_FILES['pgcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgencryptedImage = $pgencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgoriginalName = $this->input->post('originalpgcertificate');
						$pgencryptedImage = $this->input->post('oldpgcertificate');

					}
					
					
				}


				// pg certificate update on given date
				$pgdocoriginalNameArray = array();
				$pgdocencryptedNameArray = array();
				$pgdocencryptedImage = array();
				$pgdocoriginalName = array();
				if($this->input->post('pgcertificatecheck') == 'on'){

					// echo "string";die();
					if (isset($_FILES['pgcertificatedoc']['name'])) {
						if($_FILES['pgcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['pgcertificatedoc']['name'])));

							$pgdocoriginalName = $_FILES['pgcertificatedoc']['name']; 
							$pgdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
							$tempFile = $_FILES['pgcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgdocencryptedName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$pgdocencryptedImage = $pgdocencryptedName;
							}else{

								die("Error uploading pg Certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
								redirect(current_url());
							}
						}else{
							$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
							$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
						}

					}
					else{
						$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
						$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
					}

					
					$pgcertificatedate = $this->gmodel->changedatedbformate($this->input->post('pgcertificatedate'));
					$updatepgdocdata = array(
						'pgdoc_certificate' => 1,
						'pgdoc_certificate_date' => $pgcertificatedate,
						'pgdoc_original_certificate_upload' => $pgdocoriginalName,
						'pgdoc_encrypted_certificate_upload' => $pgdocencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgdocdata);
				}

				else
				{	
					$pgfileoriginalNameArray = array();
					$pgfileencryptedNameArray = array();
					$pgfileencryptedImage = '';
					$pgfileoriginalName = '';
					if(isset($_FILES['pgcertificatefile']['name'][0]))
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificatefile']['name'][$i]))); 

							$pgfileoriginalName1 = $_FILES['pgcertificatefile']['name'][$i];
							array_push($pgfileoriginalNameArray, $pgfileoriginalName1); 
							$pgfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgfileencryptedNameArray, $pgfileencryptedName1); 
							$tempFile = $_FILES['pgcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgfileencryptedImage = $pgfileencryptedNameArray;
							}else{

								die("Error uploading pg certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg certificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgfileoriginalName = $this->input->post('originalpgdoccertificate');
						$pgfileencryptedImage = $this->input->post('oldpgdoccertificate');
					}
				}


				// echo $this->input->post('ugmaigrationcheck');
				// die;
				if($this->input->post('ugmaigrationcheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				

					
					if ($_FILES['ugmaigrationcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['ugmaigrationcertificate']['name'])));

						$ugmaigrationoriginalName = $_FILES['ugmaigrationcertificate']['name']; 

						$ugmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

						$tempFile = $_FILES['ugmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $ugmaigrationencryptedName;
						//echo $targetFile;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);
						

						if($uploadResult == true){
							$ugmaigrationencryptedImage = $ugmaigrationencryptedName;
						}else{

							die("Error uploading pg maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$ugmaigrationoriginalName = $this->input->post('originalugmigrationcertificate');
						$ugmaigrationencryptedName = $this->input->post('oldugmigrationcertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('ugmaigrationdate'));
					$updateugmigrationdata = array(
						'ugmigration_certificate' => 1,
						'ugmigration_certificate_date' => $ugmaigrationdate,
						'ugmigration_original_certificate_upload' => $ugmaigrationoriginalName,
						'ugmigration_encrypted_certificate_upload' => $ugmaigrationencryptedName
					);
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateugmigrationdata);
				}

					else
						{
							$ugoriginalNameArray = array();
						$ugencryptedNameArray = array();
						$ugencryptedImage='';
						$ugoriginalName='';
							if($_FILES['ugcertificate']['name'][0]!=NULL)
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificate']['name'][$i]))); 

							$ugoriginalName1 = $_FILES['ugcertificate']['name'][$i];
							array_push($ugoriginalNameArray, $ugoriginalName1); 
							$ugencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugencryptedNameArray, $ugencryptedName1); 
							$tempFile = $_FILES['ugcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugencryptedImage = $ugencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugoriginalName = $this->input->post('originalugcertificate');
						$ugencryptedImage = $this->input->post('oldugcertificate');

					}
					}

					


				//UG Certificate upload on given date section
				$ugfileencryptedImage='';
				$ugfileoriginalName='';
				if($this->input->post('ugcertificatecheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				

					
					if (isset($_FILES['ugcertificatedoc']['name'])) {
						if($_FILES['ugcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['ugcertificatedoc']['name'])));

							$ugdocoriginalName = $_FILES['ugcertificatedoc']['name']; 

							$ugdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['ugcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugdocencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$ugdocencryptedImage = $ugdocencryptedName;
							}else{

								die("Error uploading ug doc maigration Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ug doc maigration Document");
								redirect(current_url());
							}
						}else{
							$ugdocoriginalName = $this->input->post('originalugdoccertificate');
							$ugdocencryptedName = $this->input->post('oldugdoccertificate');
						}

					}
					else{
						$ugdocoriginalName = $this->input->post('originalugdoccertificate');
						$ugdocencryptedName = $this->input->post('oldugdoccertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugcertificatedate = $this->gmodel->changedatedbformate($this->input->post('ugcertificatedate'));
					$updocdateugcertificatedata = array(
						'ugdoc_certificate' => 1,
						'ugdoc_certificate_date' => $ugcertificatedate,
						'ugdoc_original_certificate_doc_upload' => $ugdocoriginalName,
						'ugdoc_encrypted_certificate_doc_upload' => $ugdocencryptedName
					);

					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updocdateugcertificatedata);
				}

					else
						{
						$ugfileoriginalNameArray = array();
						$ugfileencryptedNameArray = array();
						$ugfileencryptedImage='';
						$ugfileoriginalName='';
							if(isset($_FILES['ugcertificatefile']['name'][0]))
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificatefile']['name'][$i]))); 

							$ugfileoriginalName1 = $_FILES['ugcertificatefile']['name'][$i];
							array_push($ugfileoriginalNameArray, $ugfileoriginalName1); 
							$ugfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugfileencryptedNameArray, $ugfileencryptedName1); 
							$tempFile = $_FILES['ugcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugfileencryptedImage = $ugfileencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugfileoriginalName = $this->input->post('originalugdoccertificate');
						$ugfileencryptedImage = $this->input->post('oldugdoccertificate');

					}
				}
						

					
				
				
				if(isset($_FILES['10thcertificate']['name'][0]))
				{
					if($_FILES['10thcertificate']['name'][0] != NULL){
						$matricencryptedImage = implode('|',$matricencryptedNameArray);
					}else{
						$matricencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
					}
				}
				else
				{
					$matricencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
				
				}

				

			



				if(isset($_FILES['12thcertificate']['name'][0]))
				{
					if($_FILES['12thcertificate']['name'][0] != NULL){
						$hscencryptedImage    =	implode('|',$hscencryptedNameArray);
					}else{
						$hscencryptedImage    =	$content['candidatedetails']->encrypthsccertificate;
					}
				
				}
				else
				{
				
				 $hscencryptedImage    =	$content['candidatedetails']->encrypthsccertificate;
				

				}
				if($_FILES['ugcertificate']['name'][0]!=NULL)
				{
				
				$ugencryptedImage = implode('|',$ugencryptedImage);
				
				}
				else
				{
				
				 $ugencryptedImage = $content['candidatedetails']->encryptugcertificate;
				
				}
				if($_FILES['pgcertificate']['name'][0]!=NULL)
				{
				
				$pgencryptedImage = implode('|',$pgencryptedImage);
				}
				else
				{
				
			 $pgencryptedImage =$content['candidatedetails']->encryptpgcertificate;

				}
				if (isset($_FILES['ugcertificatefile']['name'])) {
					if($_FILES['ugcertificatefile']['name'] != NULL){
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}else{
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}
				}
				else
				{
				
					$ugfileencryptedImage =$content['candidatedetails']->ugdocencryptedImage;

				}
				if (isset($_FILES['pgcertificatefile']['name'])) {
					if($_FILES['pgcertificatefile']['name'] != NULL){
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}else{
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}
				}
				else
				{
				
					$pgfileencryptedImage =$content['candidatedetails']->pgdocencryptedImage;

				}
				





					$updateeducationalcert	 = array(
						'encryptmetriccertificate'   => $matricencryptedImage,
						'encrypthsccertificate'      => $hscencryptedImage,
						'encryptugcertificate'       => $ugencryptedImage,
						'encryptpgcertificate'       => $pgencryptedImage,
						'encryptothercertificate'    => $Otherscertificateencryptedimage,
						'encryptofferlettername'   	 => $signedofferletterencryptedImage,
						'originalmetriccertificate'  => $matricencryptedImage,
						'originalhsccertificate'     => $hscencryptedImage,
						'originalugcertificate'      => $ugencryptedImage,
						'originalpgcertificate'      => $pgencryptedImage,
						'originalothercertificate'   => $Otherscertificateencryptedimage,
						'originalofferlettername'    => $signedofferletteroriginalName,
						'ugdocencryptedImage'      => $ugfileencryptedImage,
						'pgdocencryptedImage'      => $pgfileencryptedImage,
						'ugdocoriginalName'      => $ugfileoriginalName,
						'pgdocoriginalName'      => $pgfileoriginalName,
						'updatedby'                  => $this->loginData->candidateid,
						'updatedon'                  => date('Y-m-d H:i:s'),

					);

						
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateeducationalcert);

			
		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 


		   $count_orgname = count($_FILES['experiencedocument']['name']);  // die;


		   $query = $this->db->query('SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid);
		   $numrow = $query->num_rows();

		   $sql = 'SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid;
		   $result2 = $this->db->query($sql)->result();

		   if($numrow >0){


		   	for ($i=0; $i < $count_orgname; $i++) { 

		   		$workexpid = $result2[$i]->id;	

		   		if ($_FILES['experiencedocument']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


		   			$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
		   			$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptedexpdocumentName;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument = $encryptedexpdocumentName;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
		   			$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
		   		}

		   			 		// salary slip1

			if ($_FILES['salary_slip1']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip1']['name'][$i])); 


				$Originalnamesalaryslip1 = $_FILES['salary_slip1']['name'][$i]; 
				$encryptednamesalaryslip1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip1']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip1;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument1 = $encryptednamesalaryslip1;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip1 = $this->input->post('originalsalaryslip1')[$i];
				$encrypteddocument1 =  $this->input->post('oldexperiencesalaryslip1')[$i];
			}

		   		// salary slip2

			if ($_FILES['salary_slip2']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip2']['name'][$i])); 


				$Originalnamesalaryslip2 = $_FILES['salary_slip2']['name'][$i]; 
				$encryptednamesalaryslip2 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip2']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip2;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument2 = $encryptednamesalaryslip2;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip2 = $this->input->post('originalsalaryslip2')[$i];
				$encrypteddocument2 =  $this->input->post('oldexperiencesalaryslip2')[$i];
			}


		   				// // salary slip 3

			if ($_FILES['salary_slip3']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip3']['name'][$i])); 


				$Originalnamesalaryslip3 = $_FILES['salary_slip3']['name'][$i]; 
				$encryptednamesalaryslip3 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip3']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip3;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument3 = $encryptednamesalaryslip3;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip3 = $this->input->post('originalsalaryslip3')[$i];
				$encrypteddocument3 =  $this->input->post('oldexperiencesalaryslip3')[$i];
			}

		   		$insertWorkExperience1 = array(

		   			'originaldocumentname'  => $OriginalexperiencedocumentName,
		   			'encrypteddocumnetname'  => $encrypteddocument,
		   			'originalsalaryslip1'   => $Originalnamesalaryslip1,
					'encryptedsalaryslip1'  => $encrypteddocument1,
					'originalsalaryslip2'   => $Originalnamesalaryslip2,
					'encryptedsalaryslip2'  => $encrypteddocument2,
					'originalsalaryslip3'   => $Originalnamesalaryslip3,
					'encryptedsalaryslip3'  => $encrypteddocument3,
		   			'updateon'              => date('Y-m-d H:i:s'),
			        'updatedby'             => $this->loginData->candidateid // 
			    );


		   		$this->db->where('id', $workexpid);
		   		$this->db->update('tbl_work_experience', $insertWorkExperience1);


		   	}

		   }
		   $this->db->trans_complete();

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   }else{

		   	$updateStatusInfo	 = array(

		   		'BDFFormStatus'  => $this->input->post('BDFFormStatus'),
		   	);

		   	$this->db->where('candidateid',$this->loginData->candidateid);
		   	$this->db->update('tbl_candidate_registration', $updateStatusInfo);

		   //	echo $this->db->last_query(); die;

		   	$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	

		   	redirect('candidate/Candidatedfullinfo/preview');
		   	exit;

		   }

		}elseif(isset($CampusType) && $CampusType=='off' && $this->input->post('SaveDatasend')=='SaveData'){
			/*echo "<pre>";
				print_r($_POST);
				print_r($_FILES);
				die;*/
			$this->db->trans_start();
			if(@$this->input->post('ugpercentage')){
				$updateUGmarksArr = array(
					'ugpercentage'=>$this->input->post('ugpercentage')
				);
				$this->db->where('candidateid', $this->loginData->candidateid);
			 	$this->db->update('tbl_candidate_registration', $updateUGmarksArr);
			}
			if(@$this->input->post('pgpercentage')){
				$updatePGmarksArr = array(
					'pgpercentage'=>$this->input->post('pgpercentage')
				);
				$this->db->where('candidateid', $this->loginData->candidateid);
			 	$this->db->update('tbl_candidate_registration', $updatePGmarksArr);
			}
					//$this->db->trans_strict(FALSE);
			if (isset($_FILES['photoupload']['name'])) {
				if($_FILES['photoupload']['name'] !=NULL){
					@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 

				 	$OriginalName = $_FILES['photoupload']['name']; 
					$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					$tempFile = $_FILES['photoupload']['tmp_name'];
					$targetPath = FCPATH . "datafiles/";
					$targetFile = $targetPath . $encryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$encryptedImage = $encryptedName;
					}else{

						die("Error uploading Profile Photo ");
						$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
						redirect(current_url());
					}
				}else{
					$OriginalName = $this->input->post('oldphotoupload');
					$encryptedImage = $this->input->post('oldencryptedphotoname');
				}

			}else{
				$OriginalName = $this->input->post('oldphotoupload');
				$encryptedImage = $this->input->post('oldencryptedphotoname');

			}

			$updateArrCandidate = array(

				'originalphotoname'	=> $OriginalName,
				'encryptedphotoname'=> $encryptedImage,
				'updatedon'      	=> date('Y-m-d H:i:s'),
						'updatedby'      	=> $this->loginData->candidateid, // login user id
						'isdeleted'      	=> 0, 
					);

			$this->db->where('candidateid', $this->loginData->candidateid);
			$this->db->update('tbl_candidate_registration', $updateArrCandidate);
				//echo $this->db->last_query(); die;
					//echo  $countfamilymember = count($_FILES['familymemberphoto']['name']); 
			if(isset($_FILES['familymemberphoto']['name'])){
				if($_FILES['familymemberphoto']['name'] != NULL){
					$countfamilymember = count($_FILES['familymemberphoto']['name']);
				}else{
					$countfamilymember =0;
				}
			}else{
				$countfamilymember =0;
			}


			$this->db->where('candidateid', $this->loginData->candidateid);
			$result = $this->db->get('tbl_family_members');

			$sql = 'SELECT * FROM `tbl_family_members` WHERE `candidateid` = '.$this->loginData->candidateid;
			$result2 = $this->db->query($sql)->result();

			for ($i=0; $i < $countfamilymember ; $i++) { 

				if ($result->num_rows() > 0){

					$familyid = $result2[$i]->id;


					if ($_FILES['familymemberphoto']['name'][$i] != NULL) {


						@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

						$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
						$encryptedFamilyMemberNameImage = 'photo'.'-'. md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/familymemberphoto/";
						$targetFile = $targetPath . $encryptedFamilyMemberNameImage;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);
						//print_r($uploadResult); die(); 

						if($uploadResult == true){
							$encryptedFamilyMemberNameImage = $encryptedFamilyMemberNameImage;

						}else{

							die("Error uploading Photo file");
							$this->session->set_flashdata("er_msg", "Error uploading registration image");
							redirect(current_url());
						}
					}
					else{
						$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
						$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
					}

					if ($_FILES['familymemberid']['name'][$i] != NULL) {


						@  $ext = end((explode(".", $_FILES['familymemberid']['name'][$i]))); 

						$OriginalFamilyMemberPhotoid = $_FILES['familymemberid']['name'][$i]; 
						$encryptedFamilyMemberid = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['familymemberid']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/familymemberid/";
						$targetFile = $targetPath . $encryptedFamilyMemberid;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedFamilyMemberid =  $encryptedFamilyMemberid;

						}else{

							die("Error uploading Photo file");
							$this->session->set_flashdata("er_msg", "Error uploading registration image");
							redirect(current_url());
						}
					}
					else{
						$OriginalFamilyMemberPhotoid = $this->input->post('originalfamilymemberid')[$i];
						$encryptedFamilyMemberid = $this->input->post('oldfamilymemberid')[$i];
					}


					$insertFamilyMember	 = array(
						'originalphotoname'                => $OriginalFamilyMemberPhotoName,
						'encryptedphotoname'               => $encryptedFamilyMemberNameImage,
						'originalfamilyidentityphotoname'  => $OriginalFamilyMemberPhotoid,
						'encryptedfamilyidentityphotoname' => $encryptedFamilyMemberid,
						'createdon'                        => date('Y-m-d H:i:s'),
								'createdby'                        => $this->loginData->candidateid, // 
								'isdeleted'                        => 0, 
							);
					$this->db->where('id', $familyid);
					$this->db->update('tbl_family_members', $insertFamilyMember);
					//echo $this->db->last_query(); die();

				}


			}


//echo $this->db->last_query(); die;

			if(isset($_FILES['identityphoto']['name'])){
				if($_FILES['identityphoto']['name'] != NULL){
					$countidentityname = count($_FILES['identityphoto']['name']);
				}else{
					$countidentityname = 0;
				}
			}else{
				$countidentityname = 0;
			}

			

			$this->db->where('candidateid', $this->loginData->candidateid);
			$result = $this->db->get('tbl_identity_details');

			$sql = 'SELECT * FROM `tbl_identity_details` WHERE `candidateid` = '.$this->loginData->candidateid;
			$result2 = $this->db->query($sql)->result();

			for ($i=0; $i < $countidentityname; $i++) { 

				if ($result->num_rows() > 0){

					$identityid = $result2[$i]->id;

					if ($_FILES['identityphoto']['name'][$i] != NULL) {
						@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


						$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
						$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/identitydocuments/";
						$targetFile = $targetPath . $encryptedIdentityName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedIdentitydocument = $encryptedIdentityName;
						}else{

							die("Error uploading Identity Document !");
							$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
							redirect(current_url());
						}

					}else{
						$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
						$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
					}


					$insertIdentityDetails	 = array(
						'encryptedphotoname'  => $encryptedIdentitydocument,
						'originalphotoname'   => $OriginalIdentityName,
						'createdon'           => date('Y-m-d H:i:s'),
						    'createdby'           => $this->loginData->candidateid, // 
						    'isdeleted'           => 0, 
						);

					$this->db->where('id',$identityid);
					$this->db->update('tbl_identity_details', $insertIdentityDetails);


				}

			}


			if(!empty($_FILES['gapyearphoto']['name']))
			{
			$countgapname = count($_FILES['gapyearphoto']['name']);

			$this->db->where('candidateid', $this->loginData->candidateid);
			$result = $this->db->get('tbl_gap_year');

			$sql = 'SELECT * FROM `tbl_gap_year` WHERE `candidateid` = '.$this->loginData->candidateid;
			$result2 = $this->db->query($sql)->result();

			for ($i=0; $i < $countgapname; $i++) { 

				if ($result->num_rows() > 0){

					$identityid = $result2[$i]->id;

					if ($_FILES['gapyearphoto']['name'][$i] != NULL) {
						@  $ext = end((explode(".", $_FILES['gapyearphoto']['name'][$i]))); 


						$OriginalIdentityName = $_FILES['gapyearphoto']['name'][$i]; 
						$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['gapyearphoto']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/gapyeardocument/";
						$targetFile = $targetPath . $encryptedIdentityName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedIdentitydocument = $encryptedIdentityName;
						}else{

							die("Error uploading Identity Document !");
							$this->session->set_flashdata("er_msg", "Error uploading Gap year Document !");
							redirect(current_url());
						}

					}else{
						$OriginalIdentityName      = $this->input->post('originaldocumentsname')[$i];
						$encryptedIdentitydocument = $this->input->post('encrypteddocumentsname')[$i];
					}


					$insertIdentityDetails	 = array(
							'encrypteddocumentsname' => $encryptedIdentitydocument,
							'originaldocumentsname'  => $OriginalIdentityName,
							'createdon'              => date('Y-m-d H:i:s'),
							'createdby'              => $this->loginData->candidateid, // 
							'isdeleted'              => 0, 
						);

					$this->db->where('id',$identityid);
					$this->db->update('tbl_gap_year', $insertIdentityDetails);


				}

			}

		}


	 	/// End Step I ///////

		 	//// Step II ////////
			$pgencryptedImage ='';
			$Otherscertificateencryptedimage ='';


			/*if ($_FILES['10thcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['10thcertificate']['name']))); 


				$matricoriginalName = $_FILES['10thcertificate']['name']; 
				$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['10thcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $matricencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$matricencryptedImage = $matricencryptedName;
				}else{

					die("Error uploading 10thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$matricoriginalName = $this->input->post('originalmatriccertificate');
				$matricencryptedImage = $this->input->post('oldmatriccertificate');
			}


			if ($_FILES['12thcertificate']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['12thcertificate']['name']))); 

				$hscoriginalName = $_FILES['12thcertificate']['name']; 
				$hscencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['12thcertificate']['tmp_name'];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $hscencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$hscencryptedImage = $hscencryptedName;
				}else{

					die("Error uploading 12thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
					redirect(current_url());
				}

			}
			else{
				$hscoriginalName = $this->input->post('originalhsccertificate');
				$hscencryptedImage = $this->input->post('oldhsccertificate');
			}*/
			// die($_FILES['10thcertificate']['name'][0]!= NULL);
			if (isset($_FILES['10thcertificate']['name'][0])) {
				
				$i = 0;
				if($_FILES['10thcertificate']['name'][0] != NULL){
					$matricoriginalNameArray = array();
					$matricencryptedNameArray = array();
					foreach($_FILES['10thcertificate']['name'] as $res){

						// echo "<pre>";
						// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
						//die;	
						@  $ext = end((explode(".", $_FILES['10thcertificate']['name'][$i]))); 

						$matricoriginalName1 = $_FILES['10thcertificate']['name'][$i];
						array_push($matricoriginalNameArray, $matricoriginalName1); 
						$matricencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
						array_push($matricencryptedNameArray, $matricencryptedName1); 
						$tempFile = $_FILES['10thcertificate']['tmp_name'][$i];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $matricencryptedName1;

						$uploadResult = move_uploaded_file($tempFile,$targetFile);
						
						if($uploadResult == true){
							$matricencryptedImage = $matricencryptedNameArray;
						}else{

							die("Error uploading 10thcertificate Document 2");
							$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
							redirect(current_url());
						}
						$i++;
					}
				}else{
					$matricoriginalName = $this->input->post('originalmatriccertificate');
					$matricencryptedImage = $this->input->post('oldmatriccertificate');
				}

			}
			else{
				$matricoriginalName = $this->input->post('originalmatriccertificate');
				$matricencryptedImage = $this->input->post('oldmatriccertificate');

			}
			// echo "<pre>";
			// print_r($matricencryptedNameArray);
			// //print_r(implode('|',$matricencryptedNameArray));
			// die;


			if (isset($_FILES['12thcertificate']['name'][0])) {
				$j = 0;
				if($_FILES['12thcertificate']['name'][0] != NULL){
					$hscoriginalNameArray = array();
					$hscencryptedNameArray = array();
					foreach($_FILES['12thcertificate']['name'] as $res){
						@  $ext = end((explode(".", $_FILES['12thcertificate']['name'][$j]))); 

						$hscoriginalName1 = $_FILES['12thcertificate']['name'][$j];
						array_push($hscoriginalNameArray, $hscoriginalName1);  
						$hscencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
						array_push($hscencryptedNameArray, $hscencryptedName1); 
						$tempFile = $_FILES['12thcertificate']['tmp_name'][$j];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $hscencryptedName1;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);


						if($uploadResult == true){
							$hscencryptedImage = $hscencryptedNameArray;
						}else{
							/*echo $uploadResult.$hscoriginalName1;
							print_r($targetPath . $hscencryptedName1);
							exit();*/
							//die("Error uploading 12thcertificate Document ");
							$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
							redirect(current_url());
						}
						$j++;
					}
				}else{
					$hscoriginalName = $this->input->post('originalhsccertificate');
					$hscencryptedImage = $this->input->post('oldhsccertificate');
				}

			}
			else{
				$hscoriginalName = $this->input->post('originalhsccertificate');
				$hscencryptedImage = $this->input->post('oldhsccertificate');

			}




		
				// echo $this->input->post('ugmaigrationcheck');
				// die;
				if($this->input->post('ugmaigrationcheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				

					
					if ($_FILES['ugmaigrationcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['ugmaigrationcertificate']['name'])));

						$ugmaigrationoriginalName = $_FILES['ugmaigrationcertificate']['name']; 

						$ugmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
						// echo $_FILES['ugmaigrationcertificate']['name'];
						$tempFile = $_FILES['ugmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $ugmaigrationencryptedName;
						//echo $targetFile;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);
						
						// echo $targetFile; die('sdhvf');
						if($uploadResult == true){
							$ugmaigrationencryptedImage = $ugmaigrationencryptedName;
						}else{

							die("Error uploading ug maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$ugmaigrationoriginalName = $this->input->post('originalugmigrationcertificate');
						$ugmaigrationencryptedName = $this->input->post('oldugmigrationcertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('ugmaigrationdate'));
					$updateugmigrationdata = array(
						'ugmigration_certificate' => 1,
						'ugmigration_certificate_date' => $ugmaigrationdate,
						'ugmigration_original_certificate_upload' => $ugmaigrationoriginalName,
						'ugmigration_encrypted_certificate_upload' => $ugmaigrationencryptedName
					);
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateugmigrationdata);
				}

					else
						{
							$ugoriginalNameArray = array();
						$ugencryptedNameArray = array();
						$ugencryptedImage='';
						$ugoriginalName='';
							if($_FILES['ugcertificate']['name'][0]!=NULL)
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificate']['name'][$i]))); 

							$ugoriginalName1 = $_FILES['ugcertificate']['name'][$i];
							array_push($ugoriginalNameArray, $ugoriginalName1); 
							$ugencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugencryptedNameArray, $ugencryptedName1); 
							$tempFile = $_FILES['ugcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugencryptedImage = $ugencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugoriginalName = $this->input->post('originalugcertificate');
						$ugencryptedImage = $this->input->post('oldugcertificate');

					}
					}

					


				//UG Certificate upload on given date section
				$ugfileencryptedImage='';
				$ugfileoriginalName='';
				if($this->input->post('ugcertificatecheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				

					
					if (isset($_FILES['ugcertificatedoc']['name'])) {
						if($_FILES['ugcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['ugcertificatedoc']['name'])));

							$ugdocoriginalName = $_FILES['ugcertificatedoc']['name']; 

							$ugdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['ugcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugdocencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$ugdocencryptedImage = $ugdocencryptedName;
							}else{

								die("Error uploading ug doc maigration Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ug doc maigration Document");
								redirect(current_url());
							}
						}else{
							$ugdocoriginalName = $this->input->post('originalugdoccertificate');
							$ugdocencryptedName = $this->input->post('oldugdoccertificate');
						}

					}
					else{
						$ugdocoriginalName = $this->input->post('originalugdoccertificate');
						$ugdocencryptedName = $this->input->post('oldugdoccertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugcertificatedate = $this->gmodel->changedatedbformate($this->input->post('ugcertificatedate'));
					$updocdateugcertificatedata = array(
						'ugdoc_certificate' => 1,
						'ugdoc_certificate_date' => $ugcertificatedate,
						'ugdoc_original_certificate_doc_upload' => $ugdocoriginalName,
						'ugdoc_encrypted_certificate_doc_upload' => $ugdocencryptedName
					);
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updocdateugcertificatedata);
				}

					else
						{
							$ugfileoriginalNameArray = array();
						$ugfileencryptedNameArray = array();
						$ugfileencryptedImage='';
						$ugfileoriginalName='';
							if(isset($_FILES['ugcertificatefile']['name'][0]))
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificatefile']['name'][$i]))); 

							$ugfileoriginalName1 = $_FILES['ugcertificatefile']['name'][$i];
							array_push($ugfileoriginalNameArray, $ugfileoriginalName1); 
							$ugfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugfileencryptedNameArray, $ugfileencryptedName1); 
							$tempFile = $_FILES['ugcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugfileencryptedImage = $ugfileencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugfileoriginalName = $this->input->post('originalugdoccertificate');
						$ugfileencryptedImage = $this->input->post('oldugdoccertificate');

					}
				}



		$pgoriginalNameArray = array();
						$pgencryptedNameArray = array();
						$pgencryptedImage=array();
						// echo $this->input->post('pgmaigrationcheck');
						// die;
				if($this->input->post('pgmaigrationcheck') == 'on'){


					if ($_FILES['pgmaigrationcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['pgmaigrationcertificate']['name'])));

						$pgmaigrationoriginalName = $_FILES['pgmaigrationcertificate']['name']; 
						$pgmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
						$tempFile = $_FILES['pgmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $pgmaigrationencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$pgmaigrationencryptedImage = $pgmaigrationencryptedName;
						}else{

							die("Error uploading pg maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$pgmaigrationoriginalName = $this->input->post('originalpgmigrationcertificate');
						$pgmaigrationencryptedName = $this->input->post('oldpgmigrationcertificate');
					}

					
					$pgmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('pgmaigrationdate'));
					$updatepgmigrationdata = array(
						'pgmigration_certificate' => 1,
						'pgmigration_certificate_date' => $pgmaigrationdate,
						'pgmigration_original_certificate_upload' => $pgmaigrationoriginalName,
						'pgmigration_encrypted_certificate_upload' => $pgmaigrationencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgmigrationdata);
				}

				else
				{
					// die('fdghj');
					$pgoriginalNameArray = array();
						$pgencryptedNameArray = array();
						$pgencryptedImage='';
					if($_FILES['pgcertificate']['name'][0]!=NULL)
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificate']['name'][$i]))); 

							$pgoriginalName1 = $_FILES['pgcertificate']['name'][$i];
							array_push($pgoriginalNameArray, $pgoriginalName1); 
							$pgencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgencryptedNameArray, $pgencryptedName1); 
							$tempFile = $_FILES['pgcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgencryptedImage = $pgencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgoriginalName = $this->input->post('originalpgcertificate');
						$pgencryptedImage = $this->input->post('oldpgcertificate');

					}
					
					
				}



				// pg certificate update on given date
				$pgdocoriginalNameArray = array();
				$pgdocencryptedNameArray = array();
				$pgdocencryptedImage = array();
				$pgdocoriginalName = array();
				// echo $this->input->post('pgcertificatecheck');die();
				if($this->input->post('pgcertificatecheck') == 'on'){

					// echo "string";die();
					if (isset($_FILES['pgcertificatedoc']['name'])) {
						if($_FILES['pgcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['pgcertificatedoc']['name'])));

							$pgdocoriginalName = $_FILES['pgcertificatedoc']['name']; 
							$pgdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
							$tempFile = $_FILES['pgcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgdocencryptedName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$pgdocencryptedImage = $pgdocencryptedName;
							}else{

								die("Error uploading pg Certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
								redirect(current_url());
							}
						}else{
							$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
							$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
						}

					}
					else{
						$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
						$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
					}

					
					$pgcertificatedate = $this->gmodel->changedatedbformate($this->input->post('pgcertificatedate'));
					$updatepgdocdata = array(
						'pgdoc_certificate' => 1,
						'pgdoc_certificate_date' => $pgcertificatedate,
						'pgdoc_original_certificate_upload' => $pgdocoriginalName,
						'pgdoc_encrypted_certificate_upload' => $pgdocencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgdocdata);
				}

				else
				{	
					$pgfileoriginalNameArray = array();
					$pgfileencryptedNameArray = array();
					$pgfileencryptedImage = '';
					$pgfileoriginalName = '';
					if(isset($_FILES['pgcertificatefile']['name'][0]))
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificatefile']['name'][$i]))); 

							$pgfileoriginalName1 = $_FILES['pgcertificatefile']['name'][$i];
							array_push($pgfileoriginalNameArray, $pgfileoriginalName1); 
							$pgfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgfileencryptedNameArray, $pgfileencryptedName1); 
							$tempFile = $_FILES['pgcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgfileencryptedImage = $pgfileencryptedNameArray;
							}else{

								die("Error uploading pg certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg certificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgfileoriginalName = $this->input->post('originalpgdoccertificate');
						$pgfileencryptedImage = $this->input->post('oldpgdoccertificate');
					}
				}


			if (isset($_FILES['otherscertificate']['name'])) {
				// print_r($_FILES['otherscertificate']);die('fsgdj');
				if($_FILES['otherscertificate']['name'] != NULL){
					@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


					$OtheroriginalName = $_FILES['otherscertificate']['name']; 
					$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					$tempFile = $_FILES['otherscertificate']['tmp_name'];
					$targetPath = FCPATH . "datafiles/educationalcertificate/";
					$targetFile = $targetPath . $otherencryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$Otherscertificateencryptedimage = $otherencryptedName;
					}else{

						die("Error uploading otherscertificate Document ");
						$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
						redirect(current_url());
					}
				}else{
					$OtheroriginalName = $this->input->post('originalotherscertificate');
					$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
				}
			}
			else{
				$OtheroriginalName = $this->input->post('originalotherscertificate');
				$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
			}


			if (@ $_FILES['signed_offer_letter']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['signed_offer_letter']['name']))); 

				$signedofferletteroriginalName = $_FILES['signed_offer_letter']['name']; 
				$signedofferletterencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_signed." . $ext;
				$tempFile = $_FILES['signed_offer_letter']['tmp_name'];
				$targetPath = FCPATH . "pdf_offerletters/";
				$targetFile = $targetPath . $signedofferletterencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$signedofferletterencryptedImage = $signedofferletterencryptedName;
				}else{

					die("Error uploading signed offer letter Document ");
					$this->session->set_flashdata("er_msg", "Error uploading signed offer letter Document");
					redirect(current_url());
				}

			}else{
						$signedofferletteroriginalName = $this->input->post('originalofferlettername');
						$signedofferletterencryptedImage = $this->input->post('oldsignedofferletter');
					}



				// if($this->input->post('pgmaigrationcheck') == 'on'){
				// 	if ($_FILES['pgmaigrationcertificate']['name'] != NULL) {

				// 		@  $ext = end((explode(".", $_FILES['pgmaigrationcertificate']['name'])));

				// 		$pgmaigrationoriginalName = $_FILES['pgmaigrationcertificate']['name']; 
				// 		$pgmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
				// 		$tempFile = $_FILES['pgmaigrationcertificate']['tmp_name'];
				// 		$targetPath = FCPATH . "datafiles/educationalcertificate/";
				// 		$targetFile = $targetPath . $pgmaigrationencryptedName;
				// 		$uploadResult = move_uploaded_file($tempFile,$targetFile);

				// 		if($uploadResult == true){
				// 			$pgmaigrationencryptedImage = $pgmaigrationencryptedName;
				// 		}else{

				// 			die("Error uploading pg maigration Document ");
				// 			$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
				// 			redirect(current_url());
				// 		}

				// 	}
				// 	else{
				// 		$pgmaigrationoriginalName = $this->input->post('originalpgmigrationcertificate');
				// 		$pgmaigrationencryptedName = $this->input->post('oldpgmigrationcertificate');
				// 	}
				// 	$pgmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('pgmaigrationdate'));
				// 	$updatepgmigrationdata = array(
				// 		'pgmigration_certificate' => 1,
				// 		'pgmigration_certificate_date' => $pgmaigrationdate,
				// 		'pgmigration_original_certificate_upload' => $pgmaigrationoriginalName,
				// 		'pgmigration_encrypted_certificate_upload' => $pgmaigrationencryptedName
				// 	);
					
				// 	$this->db->where('candidateid', $this->loginData->candidateid);

				// 	$this->db->update('tbl_candidate_registration', $updatepgmigrationdata);
				// }
				// if($this->input->post('ugmaigrationcheck') == 'on'){
				// 	if ($_FILES['ugmaigrationcertificate']['name'] != NULL) {

				// 		@  $ext = end((explode(".", $_FILES['ugmaigrationcertificate']['name'])));

				// 		$ugmaigrationoriginalName = $_FILES['ugmaigrationcertificate']['name']; 
				// 		$ugmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
				// 		$tempFile = $_FILES['ugmaigrationcertificate']['tmp_name'];
				// 		$targetPath = FCPATH . "datafiles/educationalcertificate/";
				// 		$targetFile = $targetPath . $ugmaigrationencryptedName;
				// 		$uploadResult = move_uploaded_file($tempFile,$targetFile);

				// 		if($uploadResult == true){
				// 			$ugmaigrationencryptedImage = $ugmaigrationencryptedName;
				// 		}else{

				// 			die("Error uploading ug maigration Document ");
				// 			$this->session->set_flashdata("er_msg", "Error uploading ug maigration Document");
				// 			redirect(current_url());
				// 		}

				// 	}
				// 	else{
				// 		$ugmaigrationoriginalName = $this->input->post('originalugmigrationcertificate');
				// 		$ugmaigrationencryptedName = $this->input->post('oldugmigrationcertificate');
				// 	}
				// 	$ugmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('ugmaigrationdate'));
				// 	$updateugmigrationdata = array(
				// 		'ugmigration_certificate' => 1,
				// 		'ugmigration_certificate_date' => $ugmaigrationdate,
				// 		'ugmigration_original_certificate_upload' => $ugmaigrationoriginalName,
				// 		'ugmigration_encrypted_certificate_upload' => $ugmaigrationencryptedName
				// 	);
				// 	$this->db->where('candidateid', $this->loginData->candidateid);

				// 	$this->db->update('tbl_candidate_registration', $updateugmigrationdata);
				// }		
			if(isset($_FILES['10thcertificate']['name'][0]))
				{
					if($_FILES['10thcertificate']['name'][0] != NULL){
						$matricencryptedImage = implode('|',$matricencryptedNameArray);
					}else{
						$matricencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
					}
				
				}
				else
				{
				$matricencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
				
				}

				

			



				if(isset($_FILES['12thcertificate']['name'][0]))
				{
					if($_FILES['12thcertificate']['name'][0]!=NULL){
						$hscencryptedImage    =	implode('|',$hscencryptedNameArray);
					}else{
						$hscencryptedImage    =	$content['candidatedetails']->encrypthsccertificate;
					}
				
				}
				else
				{
				
				 $hscencryptedImage    =	$content['candidatedetails']->encrypthsccertificate;
				

				}
				if($_FILES['ugcertificate']['name'][0]!=NULL)
				{
				
				$ugencryptedImage = implode('|',$ugencryptedImage);
				
				}
				else
				{
				
				 $ugencryptedImage = $content['candidatedetails']->encryptugcertificate;
				
				}
				if($_FILES['pgcertificate']['name'][0]!=NULL)
				{
				
					$pgencryptedImage = implode('|',$pgencryptedImage);
				}
				else
				{
				
					$pgencryptedImage =$content['candidatedetails']->encryptpgcertificate;

				}
				if (isset($_FILES['ugcertificatefile']['name'])) {
					if($_FILES['ugcertificatefile']['name'] != NULL){
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}else{
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}
				}
				else
				{
				
					$ugfileencryptedImage =$content['candidatedetails']->ugdocencryptedImage;

				}
				if (isset($_FILES['pgcertificatefile']['name'])) {
					if($_FILES['pgcertificatefile']['name'] != NULL){
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}else{
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}
				}
				else
				{
				
					$pgfileencryptedImage =$content['candidatedetails']->pgdocencryptedImage;

				}

				


				/*echo "<pre>";
				print_r($pgfileencryptedImage);die();*/


					$updateeducationalcert	 = array(
						'encryptmetriccertificate'   => $matricencryptedImage,
						'encrypthsccertificate'      => $hscencryptedImage,
						'encryptugcertificate'       => $ugencryptedImage,
						'encryptpgcertificate'       => $pgencryptedImage,
						'encryptothercertificate'    => $Otherscertificateencryptedimage,
						'encryptofferlettername'   	 => $signedofferletterencryptedImage,
						'originalmetriccertificate'  => $matricencryptedImage,
						'originalhsccertificate'     => $hscencryptedImage,
						'originalugcertificate'      => $ugencryptedImage,
						'originalpgcertificate'      => $pgencryptedImage,
						'originalothercertificate'   => $Otherscertificateencryptedimage,
						'originalofferlettername'    => $signedofferletteroriginalName,
						'ugdocencryptedImage'      => $ugfileencryptedImage,
						'pgdocencryptedImage'      => $pgfileencryptedImage,
						'ugdocoriginalName'      => $ugfileoriginalName,
						'pgdocoriginalName'      => $pgfileoriginalName,
						'updatedby'                  => $this->loginData->candidateid,
						'updatedon'                  => date('Y-m-d H:i:s'),

					);

						
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateeducationalcert);

				
		 	////// Step III ///////
		 	   ///////// Training Exposure Save Start Here ///////////// 


		   $count_orgname = count($_FILES['experiencedocument']['name']);  // die;


		   $query = $this->db->query('SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid);
		   $numrow = $query->num_rows();

		   $sql = 'SELECT * FROM `tbl_work_experience` WHERE `candidateid` = '.$this->loginData->candidateid;
		   $result2 = $this->db->query($sql)->result();

		   if($numrow >0){


		   	for ($i=0; $i < $count_orgname; $i++) { 

		   		$workexpid = $result2[$i]->id;	

		   		if ($_FILES['experiencedocument']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


		   			$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
		   			$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptedexpdocumentName;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument = $encryptedexpdocumentName;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
		   			$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
		   		}


		   		// salary slip1

		   		if ($_FILES['salary_slip1']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['salary_slip1']['name'][$i])); 


		   			$Originalnamesalaryslip1 = $_FILES['salary_slip1']['name'][$i]; 
		   			$encryptednamesalaryslip1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['salary_slip1']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptednamesalaryslip1;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument1 = $encryptednamesalaryslip1;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$Originalnamesalaryslip1 = $this->input->post('originalsalaryslip1')[$i];
		   			$encrypteddocument1 =  $this->input->post('oldexperiencesalaryslip1')[$i];
		   		}


		   		// salary slip2

		   		if ($_FILES['salary_slip2']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['salary_slip2']['name'][$i])); 


		   			$Originalnamesalaryslip2 = $_FILES['salary_slip2']['name'][$i]; 
		   			$encryptednamesalaryslip2 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['salary_slip2']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptednamesalaryslip2;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument2 = $encryptednamesalaryslip2;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$Originalnamesalaryslip2 = $this->input->post('originalsalaryslip2')[$i];
		   			$encrypteddocument2 =  $this->input->post('oldexperiencesalaryslip2')[$i];
		   		}


		   		// // salary slip 3

		   		if ($_FILES['salary_slip3']['name'][$i] != NULL) {

		   			@  $ext = end(explode(".", $_FILES['salary_slip3']['name'][$i])); 


		   			$Originalnamesalaryslip3 = $_FILES['salary_slip3']['name'][$i]; 
		   			$encryptednamesalaryslip3 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   			$tempFile = $_FILES['salary_slip3']['tmp_name'][$i];
		   			$targetPath = FCPATH . "datafiles/workexperience/";
		   			$targetFile = $targetPath . $encryptednamesalaryslip3;
		   			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   			if($uploadResult == true){

		   				$encrypteddocument3 = $encryptednamesalaryslip3;

		   			}else{

		   				die("Error uploading Work Experience ");
		   				$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
		   				redirect(current_url());
		   			}

		   		}
		   		else{

		   			$Originalnamesalaryslip3 = $this->input->post('originalsalaryslip3')[$i];
		   			$encrypteddocument3 =  $this->input->post('oldexperiencesalaryslip3')[$i];
		   		}

		   		$insertWorkExperience1 = array(
		   			'originaldocumentname'  => $OriginalexperiencedocumentName,
		   			'encrypteddocumnetname' => $encrypteddocument,
		   			'originalsalaryslip1'   => $Originalnamesalaryslip1,
		   			'encryptedsalaryslip1'  => $encrypteddocument1,
		   			'originalsalaryslip2'   => $Originalnamesalaryslip2,
		   			'encryptedsalaryslip2'  => $encrypteddocument2,
		   			'originalsalaryslip3'   => $Originalnamesalaryslip3,
		   			'encryptedsalaryslip3'  => $encrypteddocument3,
		   			'updateon'              => date('Y-m-d H:i:s'),
		   			'updatedby'             => $this->loginData->candidateid, 
		   		);

		   		// $insertWorkExperience1 = array(

		   		// 	'originaldocumentname'  => $OriginalexperiencedocumentName,
		   		// 	'encrypteddocumnetname'  => $encrypteddocument,
		   		// 	'updateon'              => date('Y-m-d H:i:s'),
			    //     'updatedby'             => $this->loginData->candidateid, // 
			    // );

		   		$this->db->where('id', $workexpid);
		   		$this->db->update('tbl_work_experience', $insertWorkExperience1);

		   	}

		   }
		   $this->db->trans_complete();

		   if ($this->db->trans_status() === FALSE){

		   	$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
		   }else{

		   	$updateStatusInfo	 = array(

		   		'BDFFormStatus'  => '99',
		   	);

		   	$this->db->where('candidateid',$this->loginData->candidateid);
		   	$this->db->update('tbl_candidate_registration', $updateStatusInfo);

		   	//echo $this->db->last_query(); die;

		   	$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	

		   	redirect('candidate/Candidatedfullinfo/preview');
		   	exit;

		   }

		}

elseif(isset($CampusType) && $CampusType=='on' && $this->input->post('SaveDatasend')=='SaveData'){//// If END //// 

   	//// Step I ////////////////
   	/*echo "<pre>deepak";
				print_r($_POST);
				print_r($_FILES);
				die;*/

   
	$OriginalFamilyMemberPhotoName='';
	$OriginalName = '';
	//$this->db->trans_off();
	$this->db->trans_start();
	//$this->db->trans_strict(FALSE);

	//cho $_FILES['photoupload']['name'];

	if(@$this->input->post('ugpercentage')){
		$updateUGmarksArr = array(
			'ugpercentage'=>$this->input->post('ugpercentage')
		);
		$this->db->where('candidateid', $this->loginData->candidateid);
	 	$this->db->update('tbl_candidate_registration', $updateUGmarksArr);
	}
	if(@$this->input->post('pgpercentage')){
		$updatePGmarksArr = array(
			'pgpercentage'=>$this->input->post('pgpercentage')
		);
		$this->db->where('candidateid', $this->loginData->candidateid);
	 	$this->db->update('tbl_candidate_registration', $updatePGmarksArr);
	}

	if ($_FILES['photoupload']['name'] != NULL) {

		@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 

		$OriginalName = $_FILES['photoupload']['name']; 
		$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		$tempFile = $_FILES['photoupload']['tmp_name'];
		$targetPath = FCPATH . "datafiles/";
		$targetFile = $targetPath . $encryptedName;
		$uploadResult = move_uploaded_file($tempFile,$targetFile);
		

		if($uploadResult == true){
			$encryptedImage = $encryptedName;
		}else{

			die("Error uploading Profile2 Photo ");
			$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
			redirect(current_url());
		}

	}else{

		//$encryptedImage = $this->input->post('oldphotoupload');

		$OriginalName = $this->input->post('oldphotoupload');
		$encryptedImage = $this->input->post('oldencryptedphotoname');

	}

	$updateArrCandidate = array(
		
		'originalphotoname'	=> $OriginalName,
		'encryptedphotoname'=> $encryptedImage,
		'updatedon'      	  => date('Y-m-d H:i:s'),
		'updatedby'      	  => $this->loginData->candidateid, // login user id
		'isdeleted'      	  => 0, 
	);

	
	$this->db->where('candidateid', $this->loginData->candidateid);

	/*$this->db->update('tbl_candidate_registration', $updateArrCandidate);
	
	$countfamilymember = count($_FILES['familymemberphoto']['name']);*/
	if(isset($_FILES['familymemberphoto']['name'])){
				if($_FILES['familymemberphoto']['name'] != NULL){
					$countfamilymember = count($_FILES['familymemberphoto']['name']);
				}else{
					$countfamilymember =0;
				}
			}else{
				$countfamilymember =0;
			}

	for ($i=0; $i < $countfamilymember ; $i++) { 

		$this->db->where('candidateid', $this->loginData->candidateid);
		$result = $this->db->get('tbl_family_members');

		$sql = 'SELECT * FROM `tbl_family_members` Where `tbl_family_members`.candidateid ='.$this->loginData->candidateid.''; 
		$result1 = $this->db->query($sql)->result();

		if ($result->num_rows() > 0){

			$familyid = $result1[$i]->id;

			if ($_FILES['familymemberphoto']['name'][$i] != NULL) {

				@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

				$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
				$encryptedFamilyMemberPhotoName = 'photo-'.$this->loginData->candidateid.'-'.md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/familymemberphoto/";
				$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$encryptedFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;

				}else{

					die("Error uploading Photo file");
					$this->session->set_flashdata("er_msg", "Error uploading registration image");
					redirect(current_url());
				}
			}
			else{
				$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
				$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
			}

			if ($_FILES['familymemberid']['name'][$i] != NULL) {


				@  $ext = end((explode(".", $_FILES['familymemberid']['name'][$i]))); 

				$OriginalFamilyMemberPhotoid = $_FILES['familymemberid']['name'][$i]; 
				$encryptedFamilyMemberid = 
				'identity-'.$this->loginData->candidateid.'-'.md5(date("Y-m-d H:i:s").rand(1,100)). "." . $ext;
				$tempFile = $_FILES['familymemberid']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/familymemberid/";
				$targetFile = $targetPath . $encryptedFamilyMemberid;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$encryptedFamilyMemberIdentity = $encryptedFamilyMemberid;

				}else{

					die("Error uploading Photo file");
					$this->session->set_flashdata("er_msg", "Error uploading  Family  Identity image");
					redirect(current_url());
				}
			}
			else{
				$OriginalFamilyMemberPhotoid = $this->input->post('originalfamilymemberid')[$i];
				$encryptedFamilyMemberid = $this->input->post('oldfamilymemberid')[$i];
			}


			$insertFamilyMember	 = array(
				'originalphotoname'                => $OriginalFamilyMemberPhotoName,
				'encryptedphotoname'               => $encryptedFamilyMemberNameImage,
				'originalfamilyidentityphotoname'  => $OriginalFamilyMemberPhotoid,
				'encryptedfamilyidentityphotoname' => $encryptedFamilyMemberid,
				'updateon'                         => date('Y-m-d H:i:s'),
			'updateby'                         => $this->loginData->candidateid, // 
			'isdeleted'                        => 0, 
		);

			$this->db->where('id', $familyid);
			$this->db->update('tbl_family_members', $insertFamilyMember);
		//echo $this->db->last_query(); 
		}
	}


	// $countidentityname = count($_FILES['identityphoto']['name']);
	if(isset($_FILES['identityphoto']['name'])){
		if($_FILES['identityphoto']['name'] != NULL){
			$countidentityname = count($_FILES['identityphoto']['name']);
		}else{
			$countidentityname = 0;
		}
	}else{
		$countidentityname = 0;
	}

	for ($i=0; $i < $countidentityname; $i++) { 

		$this->db->where('candidateid', $this->loginData->candidateid);
		$result = $this->db->get('tbl_identity_details');

		$sql = 'SELECT * FROM `tbl_identity_details` Where `tbl_identity_details`.candidateid ='.$this->loginData->candidateid.''; 
		$result1 = $this->db->query($sql)->result();

		if ($result->num_rows() > 0){

			$identityid = $result1[$i]->id;

			if ($_FILES['identityphoto']['name'][$i] != NULL) {


				@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


				$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
				$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/identitydocuments/";
				$targetFile = $targetPath . $encryptedIdentityName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$encryptedIdentitydocument = $encryptedIdentityName;
				}else{

					die("Error uploading Identity Document !");
					$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
					redirect(current_url());
				}

			}else{
				$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
				$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
			}


			$insertIdentityDetails	 = array(
				'encryptedphotoname' => $encryptedIdentitydocument,
				'originalphotoname'  => $OriginalIdentityName,
				'updateon'      	  => date('Y-m-d H:i:s'),
		    'updatedby'      	  => $this->loginData->candidateid, // login user id
		    'isdeleted'          => 0, 
		);
			$this->db->where('id', $identityid);
			$this->db->update('tbl_identity_details', $insertIdentityDetails);
		 	
		}
	}
	$sql = 'SELECT * FROM `tbl_gap_year` WHERE `candidateid` = '.$this->loginData->candidateid;
	$result3 = $this->db->query($sql)->result();
	if($this->db->query($sql)->num_rows() > 0){
		$countgapname = count($_FILES['gapyearphoto']['name']);
	}else{
		$countgapname = 0;
	}

	for ($i=0; $i < $countgapname; $i++) { 

		$this->db->where('candidateid', $this->loginData->candidateid);
		$result = $this->db->get('tbl_gap_year');

		$sql = 'SELECT * FROM `tbl_gap_year` Where `tbl_gap_year`.candidateid ='.$this->loginData->candidateid.''; 
		$result1 = $this->db->query($sql)->result();

		if ($result->num_rows() > 0){

			$identityid = $result1[$i]->id;

			if ($_FILES['gapyearphoto']['name'][$i] != NULL) {


				@  $ext = end((explode(".", $_FILES['gapyearphoto']['name'][$i]))); 


				$OriginalIdentityName = $_FILES['gapyearphoto']['name'][$i]; 
				$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['gapyearphoto']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/gapyeardocument/";
				$targetFile = $targetPath . $encryptedIdentityName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$encryptedIdentitydocument = $encryptedIdentityName;
				}else{

					die("Error uploading Identity Document !");
					$this->session->set_flashdata("er_msg", "Error uploading Gap year Document !");
					redirect(current_url());
				}

			}else{
				$OriginalIdentityName      = $this->input->post('originalgapyearyphoto')[$i];
				$encryptedIdentitydocument = $this->input->post('oldgapyearphoto')[$i];
			}


			$insertIdentityDetails	 = array(
				'encrypteddocumentsname' => $encryptedIdentitydocument,
				'originaldocumentsname'  => $OriginalIdentityName,
				'updatedon'               => date('Y-m-d H:i:s'),
				'updatedby'              => $this->loginData->candidateid, // login user id
				'isdeleted'              => 0, 
		);
			// echo "<pre>";
			// print_r($insertIdentityDetails); die;
			$this->db->where('id', $identityid);
			$this->db->update('tbl_gap_year', $insertIdentityDetails);
		 	//echo $this->db->last_query();
		}
	}

		 	/// End Step I ///////

		 	//// Step II ////////



	

	$matricencryptedImage = '';
	$matricoriginalNameArray = array();
	$matricoriginalName1 = '';
	$matricencryptedNameArray = array();
	$matricoriginalName = '';
	/*echo "<pre>";
	echo $this->input->post('oldmatriccertificate');
	print_r($_FILES['10thcertificate']['name'][0] != NULL);exit();*/
	if(isset($_FILES['10thcertificate']['name'][0]))
		//if (count($_FILES['10thcertificate']['name']) > 0) 
	{
		$i = 0;
		if($_FILES['10thcertificate']['name'][0] != NULL){
			foreach($_FILES['10thcertificate']['name'] as $res){
				@  $ext = end((explode(".", $_FILES['10thcertificate']['name'][$i]))); 

				$matricoriginalName1 = $_FILES['10thcertificate']['name'][$i];
				array_push($matricoriginalNameArray, $matricoriginalName1); 

				$matricencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
				array_push($matricencryptedNameArray, $matricencryptedName1); 

				$tempFile = $_FILES['10thcertificate']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $matricencryptedName1;

				$uploadResult = move_uploaded_file($tempFile,$targetFile);
				
				if($uploadResult == true){
					$matricencryptedImage = $matricencryptedNameArray;
				}else{

					die("Error uploading 10thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
					redirect(current_url());
				}
				$i++;
			}
		}else{
			$matricoriginalName = $this->input->post('originalmatriccertificate');
			$matricencryptedImage = $this->input->post('oldmatriccertificate');
		}

	}
	else{
		// die('hfsjhfdskjhdf');
		$matricoriginalName = $this->input->post('originalmatriccertificate');
		$matricencryptedImage = $this->input->post('oldmatriccertificate');

	}
	
	// echo "hello";
	// die;
	
	// echo "<pre>";
	// //print_r(implode('|',$matricoriginalNameArray));
	// print_r(implode('|',$matricencryptedNameArray));
	// die;

	// 


	$hscoriginalNameArray = array();
	$hscencryptedNameArray = array();
	$hscoriginalNameArray=array();
	$hscencryptedNameArray=array();
	$hscencryptedImage='';
	if(isset($_FILES['12thcertificate']['name'][0]))
	{
		$j = 0;
		if($_FILES['12thcertificate']['name'][0]){
			foreach($_FILES['12thcertificate']['name'] as $res){
				@  $ext = end((explode(".", $_FILES['12thcertificate']['name'][$j]))); 

				$hscoriginalName1 = $_FILES['12thcertificate']['name'][$j];
				array_push($hscoriginalNameArray, $hscoriginalName1);  
				$hscencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
				array_push($hscencryptedNameArray, $hscencryptedName1); 
				$tempFile = $_FILES['12thcertificate']['tmp_name'][$j];
				$targetPath = FCPATH . "datafiles/educationalcertificate/";
				$targetFile = $targetPath . $hscencryptedName1;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);


				if($uploadResult == true){
					$hscencryptedImage = $hscencryptedNameArray;
				}else{
					/*echo $uploadResult.$hscoriginalName1;
					print_r($targetPath . $hscencryptedName1);
					exit();*/
					//die("Error uploading 12thcertificate Document ");
					$this->session->set_flashdata("er_msg", "Error uploading 12thcertificate Document");
					redirect(current_url());
				}
				$j++;
			}
		}else{
			$hscoriginalName = $this->input->post('originalhsccertificate');
			$hscencryptedImage = $this->input->post('oldhsccertificate');
		}

	}
	else{
		$hscoriginalName = $this->input->post('originalhsccertificate');
		$hscencryptedImage = $this->input->post('oldhsccertificate');

	}
 // print_r($hscencryptedNameArray);
 // die;


// if($_FILES['pgcertificate']['name'][0]!=NULL)
// {
// //if (@ $_FILES['pgcertificate']['name'] != NULL) {

// 						@  $ext = end((explode(".", $_FILES['pgcertificate']['name'][0]))); 


// 						$pgriginalName = $_FILES['pgcertificate']['name'][0]; 
// 						$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
// 						$tempFile = $_FILES['pgcertificate']['tmp_name'][0];
// 						$targetPath = FCPATH . "datafiles/educationalcertificate/";
// 						$targetFile = $targetPath . $pgencryptedName;
// 						$uploadResult = move_uploaded_file($tempFile,$targetFile);

// 						if($uploadResult == true){
// 							$pgencryptedImage = $pgencryptedName;
// 						}else{

// 							die("Error uploading pgcertificate Document ");
// 							$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
// 							redirect(current_url());
// 						}

// 					}
// 					else{
// 						$pgriginalName = $this->input->post('originalpgcertificate');
// 						$pgencryptedImage = $this->input->post('oldpgcertificate');
// 					}

/*if($_FILES['pgcertificate']['name'][0]!=NULL)
							 {
						$pgoriginalNameArray = array();
						$pgencryptedNameArray = array();
						$pgencryptedName1=array();
						$pgencryptedImage=array();
						$i = 0;
						
						foreach($_FILES['pgcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							// die;	
							@  $ext = end((explode(".", $_FILES['pgcertificate']['name'][$i]))); 

							$pgoriginalName1 = $_FILES['pgcertificate']['name'][$i];
							array_push($pgoriginalNameArray, $pgoriginalName1); 
							//print_r($pgoriginalNameArray);

							$pgencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgencryptedNameArray, $pgencryptedName1); 
							// print_r($pgencryptedNameArray);
							// die;
							$tempFile = $_FILES['pgcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgencryptedName1;
							

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							
							if($uploadResult == true){
								$pgencryptedImage = $pgencryptedNameArray;
								die("Error uploading pgcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgoriginalName = $this->input->post('originalpgcertificate');
						$pgencryptedImage = $this->input->post('oldpgcertificate');

					}
					
print_r($pgencryptedImage);
die;*/
	// if($_FILES['ugcertificate']['name'][0]!=NULL)	
	// {			
	// // if ($_FILES['ugcertificate']['name'] != NULL) {

	// 	@  $ext = end((explode(".", $_FILES['ugcertificate']['name'][0]))); 


	// 	$pgriginalName = $_FILES['ugcertificate']['name'][0]; 
	// 	$pgencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
	// 	$tempFile = $_FILES['ugcertificate']['tmp_name'];
	// 	$targetPath = FCPATH . "datafiles/educationalcertificate/";
	// 	$targetFile = $targetPath . $pgencryptedName;
	// 	$uploadResult = move_uploaded_file($tempFile,$targetFile);

	// 	if($uploadResult == true){
	// 		$pgencryptedImage = $pgencryptedName;
	// 	}else{

	// 		die("Error uploading pgcertificate Document ");
	// 		$this->session->set_flashdata("er_msg", "Error uploading pgcertificate Document");
	// 		redirect(current_url());
	// 	}

	// }
	// else{
	// 	$pgriginalName = $this->input->post('originalpgcertificate');
	// 	$pgencryptedImage = $this->input->post('oldpgcertificate');

	// }
$ugoriginalNameArray = array();
						$ugencryptedNameArray = array();
						$ugencryptedImage='';
if($_FILES['ugcertificate']['name'][0]!=NULL)
							 {
						$i = 0;
						
						foreach($_FILES['ugcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['ugcertificate']['name'][$i]))); 

							$ugoriginalName1 = $_FILES['ugcertificate']['name'][$i];
							array_push($ugoriginalNameArray, $ugoriginalName1); 
							$ugencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($ugencryptedNameArray, $ugencryptedName1); 
							$tempFile = $_FILES['ugcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$ugencryptedImage = $ugencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$ugoriginalName = $this->input->post('originalugcertificate');
						$ugencryptedImage = $this->input->post('oldugcertificate');

					}
		
					$pgoriginalNameArray = array();
						$pgencryptedNameArray = array();
						$pgencryptedImage=array();
						if(isset($_FILES['pgcertificate'])){
if($_FILES['pgcertificate']['name'][0]!=NULL)
							 {
						$i = 0;
						
						foreach($_FILES['pgcertificate']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificate']['name'][$i]))); 

							$pgoriginalName1 = $_FILES['pgcertificate']['name'][$i];
							array_push($pgoriginalNameArray, $pgoriginalName1); 
							$pgencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgencryptedNameArray, $pgencryptedName1); 
							$tempFile = $_FILES['pgcertificate']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgencryptedImage = $pgencryptedNameArray;
							}else{

								die("Error uploading ugcertificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgoriginalName = $this->input->post('originalpgcertificate');
						$pgencryptedImage = $this->input->post('oldpgcertificate');

					}
}
			$Otherscertificateencryptedimage='';		
		// if(!empty($content['candidatedetails']->otherdegree))	
		// {

		//$Otherscertificateencryptedimage='';	

// 	if ($_FILES['otherscertificate']['name'] != NULL) {

// 		@  $ext = end((explode(".", $_FILES['otherscertificate']['name']))); 


// 		$OtheroriginalName = $_FILES['otherscertificate']['name']; 
// 		$otherencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
// 		$tempFile = $_FILES['otherscertificate']['tmp_name'];
// 		$targetPath = FCPATH . "datafiles/educationalcertificate/";
// 		$targetFile = $targetPath . $otherencryptedName;
// 		$uploadResult = move_uploaded_file($tempFile,$targetFile);

// 		if($uploadResult == true){
// 			$Otherscertificateencryptedimage = $otherencryptedName;
// 		}else{

// 			die("Error uploading otherscertificate Document ");
// 			$this->session->set_flashdata("er_msg", "Error uploading otherscertificate Document");
// 			redirect(current_url());
// 		}

// 	}
// 	else{
// 		$OtheroriginalName = $this->input->post('originalotherscertificate');
// 		$Otherscertificateencryptedimage = $this->input->post('oldotherscertificate');
// 	}
	
// //}
// echo $Otherscertificateencryptedimage;
// die;
		

			if (@ $_FILES['signed_offer_letter']['name'] != NULL) {

				@  $ext = end((explode(".", $_FILES['signed_offer_letter']['name']))); 

				$signedofferletteroriginalName = $_FILES['signed_offer_letter']['name']; 
				$signedofferletterencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_signed." . $ext;
				$tempFile = $_FILES['signed_offer_letter']['tmp_name'];
				$targetPath = FCPATH . "pdf_offerletters/";
				$targetFile = $targetPath . $signedofferletterencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){
					$signedofferletterencryptedImage = $signedofferletterencryptedName;
				}else{

					die("Error uploading signed offer letter Document ");
					$this->session->set_flashdata("er_msg", "Error uploading signed offer letter Document");
					redirect(current_url());
				}

			}else{
						$signedofferletteroriginalName = $this->input->post('originalofferlettername');
						$signedofferletterencryptedImage = $this->input->post('oldsignedofferletter');
					}


			
				if($this->input->post('pgmaigrationcheck') == 'on'){
					if ($_FILES['pgmaigrationcertificate']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['pgmaigrationcertificate']['name'])));

						$pgmaigrationoriginalName = $_FILES['pgmaigrationcertificate']['name']; 
						$pgmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
						$tempFile = $_FILES['pgmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $pgmaigrationencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$pgmaigrationencryptedImage = $pgmaigrationencryptedName;
						}else{

							die("Error uploading pg maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading pg maigration Document");
							redirect(current_url());
						}

					}
					else{
						$pgmaigrationoriginalName = $this->input->post('originalpgmigrationcertificate');
						$pgmaigrationencryptedName = $this->input->post('oldpgmigrationcertificate');
					}
					$pgmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('pgmaigrationdate'));
					$updatepgmigrationdata = array(
						'pgmigration_certificate' => 1,
						'pgmigration_certificate_date' => $pgmaigrationdate,
						'pgmigration_original_certificate_upload' => $pgmaigrationoriginalName,
						'pgmigration_encrypted_certificate_upload' => $pgmaigrationencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgmigrationdata);
				}

				// pg certificate update on given date
				$pgdocoriginalNameArray = array();
				$pgdocencryptedNameArray = array();
				$pgdocencryptedImage = array();
				$pgdocoriginalName = array();
				$pgfileoriginalNameArray = array();
				$pgfileencryptedNameArray = array();
				$pgfileencryptedImage = '';
				$pgfileoriginalName = '';
				if($this->input->post('pgcertificatecheck') == 'on'){

					// echo "string";die();
					if (isset($_FILES['pgcertificatedoc']['name'])) {

						if($_FILES['pgcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['pgcertificatedoc']['name'])));

							$pgdocoriginalName = $_FILES['pgcertificatedoc']['name']; 
							$pgdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
							$tempFile = $_FILES['pgcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgdocencryptedName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$pgdocencryptedImage = $pgdocencryptedName;
							}else{

								die("Error uploading pg Certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
								redirect(current_url());
							}
						}else{
							$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
							$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
						}

					}
					else{
						$pgdocoriginalName = $this->input->post('originalpgdoccertificate');
						$pgdocencryptedName = $this->input->post('oldpgdoccertificate');
					}

					
					$pgcertificatedate = $this->gmodel->changedatedbformate($this->input->post('pgcertificatedate'));
					$updatepgdocdata = array(
						'pgdoc_certificate' => 1,
						'pgdoc_certificate_date' => $pgcertificatedate,
						'pgdoc_original_certificate_upload' => $pgdocoriginalName,
						'pgdoc_encrypted_certificate_upload' => $pgdocencryptedName
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updatepgdocdata);
				}

				else
				{	
					$pgfileoriginalNameArray = array();
					$pgfileencryptedNameArray = array();
					$pgfileencryptedImage = '';
					$pgfileoriginalName = '';
					if(isset($_FILES['pgcertificatefile']['name'][0]))
					
					{
						$i = 0;
						
						foreach($_FILES['pgcertificatefile']['name'] as $res){

							// echo "<pre>";
							// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
							//die;	
							@  $ext = end((explode(".", $_FILES['pgcertificatefile']['name'][$i]))); 

							$pgfileoriginalName1 = $_FILES['pgcertificatefile']['name'][$i];
							array_push($pgfileoriginalNameArray, $pgfileoriginalName1); 
							$pgfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
							array_push($pgfileencryptedNameArray, $pgfileencryptedName1); 
							$tempFile = $_FILES['pgcertificatefile']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgfileencryptedName1;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							
							if($uploadResult == true){
								$pgfileencryptedImage = $pgfileencryptedNameArray;
							}else{

								die("Error uploading pg certificate Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg certificate Document");
								redirect(current_url());
							}
							$i++;
						}

					}
					else{
						$pgfileoriginalName = $this->input->post('originalpgdoccertificate');
						$pgfileencryptedImage = $this->input->post('oldpgdoccertificate');
					}
				}

				if($this->input->post('ugmaigrationcheck') == 'on'){
					/*echo "<pre>";
					print_r($_FILES['ugmaigrationcertificate']['name'][0]);exit();*/
					if (!empty($_FILES['ugmaigrationcertificate']['name'])) {

						@  $ext = end((explode(".", $_FILES['ugmaigrationcertificate']['name'])));

						$ugmaigrationoriginalName = $_FILES['ugmaigrationcertificate']['name']; 
						$ugmaigrationencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;
						$tempFile = $_FILES['ugmaigrationcertificate']['tmp_name'];
						$targetPath = FCPATH . "datafiles/educationalcertificate/";
						$targetFile = $targetPath . $ugmaigrationencryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$ugmaigrationencryptedImage = $ugmaigrationencryptedName;
						}else{

							die("Error uploading ug maigration Document ");
							$this->session->set_flashdata("er_msg", "Error uploading ug maigration Document");
							redirect(current_url());
						}

					}
					else{
						$ugmaigrationoriginalName = $this->input->post('originalugmigrationcertificate');
						$ugmaigrationencryptedName = $this->input->post('oldugmigrationcertificate');
					}
					$ugmaigrationdate = $this->gmodel->changedatedbformate($this->input->post('ugmaigrationdate'));
					$updateugmigrationdata = array(
						'ugmigration_certificate' => 1,
						'ugmigration_certificate_date' => $ugmaigrationdate,
						'ugmigration_original_certificate_upload' => $ugmaigrationoriginalName,
						'ugmigration_encrypted_certificate_upload' => $ugmaigrationencryptedName
					);
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updateugmigrationdata);
				}




				//UG Certificate upload on given date section

				$ugfileencryptedImage='';
				$ugfileoriginalName='';
				if($this->input->post('ugcertificatecheck') == 'on'){

				 // print_r($_FILES['ugmaigrationcertificate']['name']);
				
					
					if (isset($_FILES['ugcertificatedoc']['name'])) {
						if($_FILES['ugcertificatedoc']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['ugcertificatedoc']['name'])));

							$ugdocoriginalName = $_FILES['ugcertificatedoc']['name']; 

							$ugdocencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['ugcertificatedoc']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugdocencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$ugdocencryptedImage = $ugdocencryptedName;
							}else{

								die("Error uploading ug doc maigration Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ug doc maigration Document");
								redirect(current_url());
							}
						}else{
							$ugdocoriginalName = $this->input->post('originalugdoccertificate');
							$ugdocencryptedName = $this->input->post('oldugdoccertificate');
						}

					}
					else{
						$ugdocoriginalName = $this->input->post('originalugdoccertificate');
						$ugdocencryptedName = $this->input->post('oldugdoccertificate');
					}
					

						 $ugmaigrationencryptedImage;
					$ugcertificatedate = $this->gmodel->changedatedbformate($this->input->post('ugcertificatedate'));
					$updocdateugcertificatedata = array(
						'ugdoc_certificate' => 1,
						'ugdoc_certificate_date' => $ugcertificatedate,
						'ugdoc_original_certificate_doc_upload' => $ugdocoriginalName,
						'ugdoc_encrypted_certificate_doc_upload' => $ugdocencryptedName
					);/*echo "<pre>";
					print_r($updocdateugcertificatedata);exit();*/
					$this->db->where('candidateid', $this->loginData->candidateid);

					$this->db->update('tbl_candidate_registration', $updocdateugcertificatedata);
				}

					else
						{
							$ugfileoriginalNameArray = array();
						$ugfileencryptedNameArray = array();
						$ugfileencryptedImage='';
						$ugfileoriginalName='';
							if(isset($_FILES['ugcertificatefile']['name'][0]))
							 {
						$i = 0;
						if($_FILES['ugcertificatefile']['name'][0] != NULL){
							foreach($_FILES['ugcertificatefile']['name'] as $res){

								// echo "<pre>";
								// print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
								//die;	
								@  $ext = end((explode(".", $_FILES['ugcertificatefile']['name'][$i]))); 

								$ugfileoriginalName1 = $_FILES['ugcertificatefile']['name'][$i];
								array_push($ugfileoriginalNameArray, $ugfileoriginalName1); 
								$ugfileencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;			
								array_push($ugfileencryptedNameArray, $ugfileencryptedName1); 
								$tempFile = $_FILES['ugcertificatefile']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $ugfileencryptedName1;

								$uploadResult = move_uploaded_file($tempFile,$targetFile);
								
								if($uploadResult == true){
									$ugfileencryptedImage = $ugfileencryptedNameArray;
								}else{

									die("Error uploading ugcertificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading ugcertificate Document");
									redirect(current_url());
								}
								$i++;
							}
						}else{
							$ugfileoriginalName = $this->input->post('originalugdoccertificate');
							$ugfileencryptedImage = $this->input->post('oldugdoccertificate');
						}

					}
					else{
						$ugfileoriginalName = $this->input->post('originalugdoccertificate');
						$ugfileencryptedImage = $this->input->post('oldugdoccertificate');

					}
				}


				
				$matricencryptedImage1='';
				if(isset($_FILES['10thcertificate']['name'][0]))
				{
					if($_FILES['10thcertificate']['name'][0] !=NULL){
						$matricencryptedImage1=implode('|',$matricencryptedNameArray);
					}else{
						$matricencryptedImage1=$content['candidatedetails']->encryptmetriccertificate;
					}
				}
				else
				{
				$matricencryptedImage1=$content['candidatedetails']->encryptmetriccertificate;
				
				}
				
				
				
			

				 if(isset($_FILES['12thcertificate']['name'][0])) {
				 	if($_FILES['12thcertificate']['name'][0]!=NULL){
				 		$hscencryptedImage = implode('|',$hscencryptedNameArray);
				 	}else{
				 		$hscencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
				 	}
				
				 }
				else
				 {
				 	$hscencryptedImage = $content['candidatedetails']->encryptmetriccertificate;
				 }
				 // print_r($hscencryptedImage);
				 // die;

				if($_FILES['ugcertificate']['name'][0]!=NULL)
				{
				
				$ugencryptedImage = implode('|',$ugencryptedNameArray);
				
				}
				else
				{
				
				  $ugencryptedImage = $content['candidatedetails']->encryptugcertificate;
				

				}

	
				if($_FILES['pgcertificate']['name'][0]!=NULL)
				{
				
				$pgencryptedImage = implode('|',$pgencryptedNameArray);
				}
				else
				{
				
			 $pgencryptedImage =$content['candidatedetails']->encryptpgcertificate;

				}
				
				if (isset($_FILES['ugcertificatefile']['name'])) {
					if($_FILES['ugcertificatefile']['name'] != NULL){
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}else{
						$ugfileencryptedImage = implode('|',$ugfileencryptedImage);
					}
				}
				else
				{
				
					$ugfileencryptedImage =$content['candidatedetails']->ugdocencryptedImage;

				}
				if (isset($_FILES['pgcertificatefile']['name'])) {
					if($_FILES['pgcertificatefile']['name'] != NULL){
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}else{
						$pgfileencryptedImage = implode('|',$pgfileencryptedImage);
					}
				}
				else
				{
				
					$pgfileencryptedImage =$content['candidatedetails']->pgdocencryptedImage;

				}
	
	$updateeducationalcert	 = array(
		'encryptmetriccertificate'  => $matricencryptedImage1,
		'encrypthsccertificate'     => $hscencryptedImage,
		'encryptugcertificate'      => $ugencryptedImage,
		'encryptpgcertificate'      => $pgencryptedImage,
		'encryptothercertificate'   => $Otherscertificateencryptedimage,
		'encryptofferlettername'    => $signedofferletterencryptedImage,
		'originalmetriccertificate'  => $matricoriginalName,
		'originalhsccertificate'     => $hscencryptedImage,
		'originalugcertificate'      => $ugencryptedImage,
		'originalpgcertificate'      => $pgencryptedImage,
		'originalothercertificate'   => $Otherscertificateencryptedimage,
		'originalofferlettername'   => $signedofferletteroriginalName,
		'ugdocencryptedImage'      => $ugfileencryptedImage,
		'pgdocencryptedImage'      => $pgfileencryptedImage,
		'ugdocoriginalName'      => $ugfileoriginalName,
		'pgdocoriginalName'      => $pgfileoriginalName,

	);
			/*echo "<pre>";
			print_r($updateeducationalcert);die;*/
	$this->db->where('candidateid', $this->loginData->candidateid);

	$this->db->update('tbl_candidate_registration', $updateeducationalcert);
	// echo $this->db->last_query();
	// die;



		 	/////// Step III ///////
		 	   //////////// Training Exposure Save Start Here ///////////// 

	

	$count_orgname = count($_FILES['experiencedocument']['name']);  

	$this->db->where('candidateid', $this->loginData->candidateid);
	$result = $this->db->get('tbl_work_experience');

	$sql = 'SELECT * FROM `tbl_work_experience` Where `tbl_work_experience`.candidateid ='.$this->loginData->candidateid.''; 
	$result1 = $this->db->query($sql)->result();

	for ($i=0; $i < $count_orgname; $i++) { 

		if ($result->num_rows() > 0){

			$experienceid = $result1[$i]->id;

					//echo $i; die;

			if ($_FILES['experiencedocument']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


				$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
				$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptedexpdocumentName;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument = $encryptedexpdocumentName;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
				$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
			}

		   		// salary slip1

			if ($_FILES['salary_slip1']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip1']['name'][$i])); 


				$Originalnamesalaryslip1 = $_FILES['salary_slip1']['name'][$i]; 
				$encryptednamesalaryslip1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip1']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip1;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument1 = $encryptednamesalaryslip1;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip1 = $this->input->post('originalsalaryslip1')[$i];
				$encrypteddocument1 =  $this->input->post('oldexperiencesalaryslip1')[$i];
			}


		   		// salary slip2

			if ($_FILES['salary_slip2']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip2']['name'][$i])); 


				$Originalnamesalaryslip2 = $_FILES['salary_slip2']['name'][$i]; 
				$encryptednamesalaryslip2 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip2']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip2;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument2 = $encryptednamesalaryslip2;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip2 = $this->input->post('originalsalaryslip2')[$i];
				$encrypteddocument2 =  $this->input->post('oldexperiencesalaryslip2')[$i];
			}


		   		// // salary slip 3

			if ($_FILES['salary_slip3']['name'][$i] != NULL) {

				@  $ext = end(explode(".", $_FILES['salary_slip3']['name'][$i])); 


				$Originalnamesalaryslip3 = $_FILES['salary_slip3']['name'][$i]; 
				$encryptednamesalaryslip3 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['salary_slip3']['tmp_name'][$i];
				$targetPath = FCPATH . "datafiles/workexperience/";
				$targetFile = $targetPath . $encryptednamesalaryslip3;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypteddocument3 = $encryptednamesalaryslip3;

				}else{

					die("Error uploading Work Experience ");
					$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
					redirect(current_url());
				}

			}
			else{

				$Originalnamesalaryslip3 = $this->input->post('originalsalaryslip3')[$i];
				$encrypteddocument3 =  $this->input->post('oldexperiencesalaryslip3')[$i];
			}

			$insertWorkExperience1 = array(
				'originaldocumentname'  => $OriginalexperiencedocumentName,
				'encrypteddocumnetname' => $encrypteddocument,
				'originalsalaryslip1'   => $Originalnamesalaryslip1,
				'encryptedsalaryslip1'  => $encrypteddocument1,
				'originalsalaryslip2'   => $Originalnamesalaryslip2,
				'encryptedsalaryslip2'  => $encrypteddocument2,
				'originalsalaryslip3'   => $Originalnamesalaryslip3,
				'encryptedsalaryslip3'  => $encrypteddocument3,
				'updateon'              => date('Y-m-d H:i:s'),
				'updatedby'             => $this->loginData->candidateid, 
			);
		   		// echo "<pre>";
		   		// print_r($insertWorkExperience1); die;
			$this->db->where('id', $experienceid);
			$this->db->update('tbl_work_experience', $insertWorkExperience1);

		}

	}


		///////// Work Experience Save End Here ///////////// 

	$this->db->trans_complete();

	if ($this->db->trans_status() === FALSE){

		$this->session->set_flashdata('er_msg', 'Error Adding Candidate Registration');	
	}else{

		$updateStatusInfo	 = array(

			'BDFFormStatus'  => '99',
		);

		$this->db->where('candidateid',$this->loginData->candidateid);
		$this->db->update('tbl_candidate_registration', $updateStatusInfo);
		   	//die;
		$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');
		redirect('candidate/Candidatedfullinfo/preview');

	}

}

}
$content['education'] = $this->model->getedicationstatus($this->loginData->candidateid);
 // print_r($content['education']);
 // die;
$fcount = $this->model->getCountFamilyMember($this->loginData->candidateid);

$content['familycount']= $fcount;  

$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetails($this->loginData->candidateid);

$content['candidatedetails']         = $this->model->getCandidateDetails($this->loginData->candidateid);

$Icount = $this->model->getCountIdentityNumber($this->loginData->candidateid);

$content['identitycount']= $Icount;  

$content['identitydetals'] = $this->model->getCandidateIdentityDetails($this->loginData->candidateid);


$TEcount = $this->model->getCountTrainingExposure($this->loginData->candidateid);

$content['TrainingExpcount']= $TEcount;

$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($this->loginData->candidateid);


@ $Lcount = $this->model->getCountLanguage($this->loginData->candidateid);

@ $content['languageproficiency']= $Lcount;  

@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($this->loginData->candidateid);
@ $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($this->loginData->candidateid);


$WEcount = $this->model->getCountWorkExprience($this->loginData->candidateid);

$content['WorkExperience']= $WEcount;  

$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($this->loginData->candidateid);


$GPYcount = $this->model->getCountGapYear($this->loginData->candidateid);

$content['GapYearCount']= $GPYcount;  

$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($this->loginData->candidateid);

/*echo "<pre>";
print_r($content);
die;*/


$content['statedetails']           = $this->model->getState();
$content['ugeducationdetails']     = $this->model->getUgEducation();
$content['pgeducationdetails']     = $this->model->getPgEducation();
$content['campusdetails']          = $this->model->getCampus();
$content['syslanguage']            = $this->model->getSysLanguage();
$content['sysrelations']           = $this->model->getSysRelations();
$content['sysidentity']            = $this->model->getSysIdentity();
$content['getdistrict']            = $this->model->getDistrict();

$content['getjoiningstatus'] = $this->model->getCandidateJoiningStatus($this->loginData->candidateid);
$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
$content['getgeneralform']   = $this->model->getGeneralFormStatus($this->loginData->candidateid);

$content['getbdfformstatus']   = $this->model->getBdfFormStatus($this->loginData->candidateid);
// print_r($content['getbdfformstatus']);
// die;

$content['getgapyear_newstatus']   = $this->model->getgapyear_newstatus($this->loginData->candidateid);
$content['offerletterstatus'] = $this->model->getofferletterstatus($this->loginData->candidateid);
//echo "<pre>";
//print_r($content['getgapyear_newstatus']);
		 
$content['title'] = 'Candidatedfullinfo';
/*echo "<pre>";
print_r($content);exit();*/

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('candidate/_main_layout', $content);

}catch (Exception $e) {
	print_r($e->getMessage());die;
}
}




public function preview()
{

	try{


		$fcount = $this->model->getCountFamilyMember($this->loginData->candidateid);

		$content['familycount']= $fcount;  

		$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetailsPrint($this->loginData->candidateid);

		$content['candidatedetails']         = $this->model->getCandidateDetailsPrint($this->loginData->candidateid);

		$Icount = $this->model->getCountIdentityNumber($this->loginData->candidateid);

		$content['identitycount']= $Icount;  

		$content['identitydetals'] = $this->model->getCandidateIdentityDetailsPrint($this->loginData->candidateid);


		$TEcount = $this->model->getCountTrainingExposure($this->loginData->candidateid);

		$content['TrainingExpcount']= $TEcount;  

		$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($this->loginData->candidateid);


		$GPYcount = $this->model->getCountGapYear($this->loginData->candidateid);

		$content['GapYearCount']= $GPYcount;  

		$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($this->loginData->candidateid);



		@ $Lcount = $this->model->getCountLanguage($this->loginData->candidateid);

		@ $content['languageproficiency']= $Lcount;  

		@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($this->loginData->candidateid);
		$content['languagedetalsprint'] = $this->model->getCandidateLanguageDetailsPrint($this->loginData->candidateid);
		@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($this->loginData->candidateid);


		$WEcount = $this->model->getCountWorkExprience($this->loginData->candidateid);

		$content['WorkExperience']= $WEcount;  

		$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($this->loginData->candidateid);
		$content['offerletterstatus'] = $this->model->getofferletterstatus($this->loginData->candidateid);
		/*echo "<pre>";
		print_r($content['candidatedetails']);exit();*/

		$content['title'] = 'Candidatedfullinfo';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function bdfformsubmit()
{

	try{

		// $subject = "Candidates Submitted BDF Form ";
		// $body = read_file(base_url().'mailtext/Candidates_Fill_BDF_Formtext'); 
		$candidateemail = '';
		$hrdemailid ='';
		$candidatedetails = $this->model->getCandidateDetails($this->loginData->candidateid);
		$gethremailid = $this->gmodel->getHRDEmailid();
		$getpaemailid = $this->gmodel->getPersonnelAdministratorEmailid();

		if (!empty($gethremailid)) {
			$hrdemailid = $gethremailid->hrdemailid;
		}
		if (!empty($candidatedetails)) {
			$candidateemail  = $candidatedetails->emailid;
		}

      		 	//print_r($candidatedetails); die;
		

		 	   //$candiadteemailid = $this->input->post('emailid');
			 	// $to_email     = $candidateemail; //// Team Mail Id ////
			 	// $to_name     =  $hrdemailid; /// HRD Mail Id ////
			 	
		 		//$to_candidate  = $result1->emailid; ///// Candidate Email Id ////
			 	// $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);
			 	// if($sendmail==true){

			 		$updateStatusInfo = array(
			 			'BDFFormStatus'  => '1',
			 		);

			 		$this->db->where('candidateid',$this->loginData->candidateid);
			 		$this->db->update('tbl_candidate_registration', $updateStatusInfo);
			 		$this->session->set_flashdata('tr_msg', 'Successfully Added Candidate Registration');	
			 	// }

			 	$content['title'] = 'Candidatedfullinfo';
			 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 	$this->load->view('candidate/_main_layout', $content);


			 }catch (Exception $e) {
			 	print_r($e->getMessage());die;
			 }
			}


public function joininginduction_submit()
{

	try{


			 	$content['title'] = 'Candidatedfullinfo';
			 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 	$this->load->view('candidate/_main_layout', $content);


			 }catch (Exception $e) {
			 	print_r($e->getMessage());die;
			 }
			}


		}