<?php 
class Acceptance_for_interview extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Acceptance_for_interview_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('loginData');

	}

	public function index()
	{
		
		//print_r($this->loginData); die;
		 // start permission 
//  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." "; 
// $content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

		$campusid = $this->input->post('campusid');
		
		 $RequestMethod = $this->input->server('REQUEST_METHOD');
		
		if($RequestMethod == 'POST'){
		

			    $this->db->trans_start();

			    //echo 'SELECT * FROM `tbl_candidate_registration` WHERE  `candidateid` = '.$this->loginData->candidateid.'';
			
			$query = $this->db->query('SELECT * FROM `tbl_candidate_registration` WHERE `confirm_attend`= 1 AND  `candidateid` = '.$this->loginData->candidateid.'');

			//echo $query->num_rows(); die;


			if ($query->num_rows() > 0) {

				$this->session->set_flashdata('er_msg', 'Sorry !!! you are already accepted interview call !!!');	
			   redirect('candidate/Acceptance_for_interview/index');
			} {

   		    		 $updateArr = array(
						'confirm_attend' => 1,
					);
					
					$this->db->where('candidateid', $this->loginData->candidateid);
					$this->db->update('tbl_candidate_registration', $updateArr);
    		    	//echo $this->db->last_query(); die;
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Sorry you are not acceptance for interview');	
						redirect('candidate/Acceptance_for_interview/index');
					}else{

						$canduadatedetails = $this->model->getOffCandidateDetails($this->loginData->candidateid);

					    $candidateemailid = $canduadatedetails[0]->emailid; //die;
						// $hrdemailid = 'poonamadlekha@pradan.net,amit.kum2008@gmail.com';
					    $to_email = $canduadatedetails[0]->emailid;
						$hrdemailid = $this->model->getHrdemailid();
						$recipients = array();

						$subject = 'Interview Confirmation ';
					   	$body = 'Dear '.$canduadatedetails[0]->candidatefirstname.',<br><br>';
					   	$body .= 'Thank you very much for the invitation to interview . I appreciate the opportunity, and I look forward to meeting with hrd team on Pradan office . <br>';
					   	$body .= 'If I can provide you with any further information prior to the interview, please let me know.,<br><br><br>';
					   	$body .= 'With Regards,<br>';
					   	$body .= 'Team Recruiter';
				  
					   
					 
					   	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$hrdemailid,$recipients);
					   	
					   	if($sendmail==1){
					   		
					   		$this->session->set_flashdata('tr_msg', 'Successfully acceptance for interview');

					   		redirect('candidate/Acceptance_for_interview/index');

					   	}else{
					   		$this->session->set_flashdata('er_msg', 'Error !! Email Not Send !!!');
					   	}
						
						$this->session->set_flashdata('tr_msg', 'Successfully acceptance for interview');	
						redirect('candidate/Acceptance_for_interview/index');	
					}
				}//else end
			}

			$content['getstatus'] = $this->model->getApproveStatus($this->loginData->candidateid);
            $content['method']  = $this->router->fetch_method();
			$content['title']  = 'Acceptance_for_interview';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('candidate/_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}

}