<?php 

/**
* State List
*/
class Medical_certificate extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $this->load->model('Dompdf_model');
    $this->load->model('Employee_particular_form_model');
    $this->load->model('Global_model','gmodel');
    $this->load->model('Medical_certificate_model');
    $check = $this->session->userdata('loginData');
    $this->load->model("General_nomination_and_authorisation_form_model");
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('loginData');
   //print_r($this->loginData);
 }

 public function index($staff,$candidate_id)
 {

   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    //redirect('/staff_dashboard/index');
    
  } else {

   $staff = $this->uri->segment(4);
   $candidate_id  = $this->uri->segment(5);

   $content['staff'] = $staff;
   $content['candidateid'] = $candidate_id;

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);

   if(isset($staff_id)&& isset($candidateid))
   {
    $staff_id=$staff_id;
    $candidateid=$candidateid;
  }
  else
  {
   $staff_id=$staff;
   $candidateid=$candidate_id;
 }

 $content['role']=$this->loginData;

 $content['report']=$this->Medical_certificate_model->staff_reportingto($staff_id);

 $reporting_to=$content['report']->reportingto;


 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();

  $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

 $content['candidatedetailwithaddress'] =$this->Medical_certificate_model->getCandidateWithAddressDetails($staff_id);
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);

// print_r($content['candidatedetailwithaddress']); die;
 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
 $query332="SELECT * from tbl_medical_certificate where candidate_id=$candidateid";
 $content['list_staff_tbl_medical_certificate_table']=$this->db->query($query332)->row();

 $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);

 if($content['list_staff_tbl_medical_certificate_table'])

   $var=$content['list_staff_tbl_medical_certificate_table']->flag;
 
 else $var = '';

 if ($var==null)
 {
   goto prepareviewss;
 }
 
 if($var==0)
 {
   redirect('candidate/Medical_certificate/editindex/'.$staff.'/'.$candidateid);
 }
 elseif($var==1)
 {
   redirect('candidate/Medical_certificate/previewstatement/'.$staff.'/'.$candidateid);
 }

 
 prepareviewss:


 $this->load->model('Medical_certificate_model');
 $content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);


 $RequestMethod = $this->input->server('REQUEST_METHOD'); 

 if($RequestMethod == 'POST'){

  // print_r($this->input->post()); die;

  $candidatedt=$this->input->post('candidatedate');
  $datepresence=$this->input->post('doctorsigneddate');

  $this->load->model('Medical_certificate_model');

  $duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($candidatedt);
  $dddd=$this->Medical_certificate_model->getCandidateDetailsPreview($datepresence);



  $operation=$this->input->post('operation');
   // echo "operation=".$operation;

   // die();

  if($operation == 0)
  {

    $insertArr = array (

      'candidate_id'            => $candidateid,
      'dateofpresence'          => $dddd,
      'candidatedate'           => $duuu,
      'createdon'               => date('Y-m-d H:i:s'),
      'createdby'               => $this->loginData->UserID,
      'small_pox'               => $this->input->post('small_pox') ,
      'spitting_disease'        => $this->input->post('spitting_disease'),
      'fainting_attacks'        => $this->input->post('fainting_attacks') ,
      'appendicitis'            => $this->input->post('appendicitis') ,
      'lung_disease'            => $this->input->post('lung_disease'),
      'epilepsy'                => $this->input->post('epilepsy'),
      'insanity'                => $this->input->post('insanity') ,
      'major_surgery'           => $this->input->post('major_surgery') ,
      'any_physical_disability' => $this->input->post('any_physical_disability'),
      'nervousness_depression'  => $this->input->post('nervousness_depression'),
      'flag'                    => 0,
      'anydetails'              => $this->input->post('showthis'),
    );

 // print_r($insertArr);
//die();
    $this->db->insert('tbl_medical_certificate', $insertArr);

    $insertid = $this->db->insert_id();
    if(empty($this->loginData->RoleID))
    {
      //die("dvhgfds");
      $insert_data = array (

        'type'      => 19,
        'r_id'      => $insertid,
        'sender'    => $staff_id,
        'receiver'  => $reporting_to,
        'senddate'  => date('Y-m-d'),
        'createdon' => date('Y-m-d H:i:s'),
        'createdby' => $this->loginData->UserID,
        'flag'      => 1,
        'staffid'   => $staff_id
      );
     // print_r($insert_data);
     //  die();
      
      $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
    }
    if($this->loginData->RoleID==2)
    {
      $w_details=$this->Medical_certificate_model->workflow_details($staff_id);
      // echo "<pre>";
      // print_r($w_details);
      if($w_details->r_id!=null && $w_details->type!=null)
      {
        $insert_data = array (

          'type'                 => 19,
          'r_id'                 => $insertid,
          'sender'               => $staff_id,
          'receiver'             => 1284,
          'senddate'             => date('Y-m-d'),
          'createdon'            => date('Y-m-d H:i:s'),
          'createdby'            => $this->loginData->UserID,
          'scomments'            => $this->input->post('status'),
          'forwarded_workflowid' => $w_details->workflowid,
          'flag'                 => 4,
          'staffid'              => $staff_id
        );
        $this->db->insert('tbl_workflowdetail', $insert_data);
       // echo $this->db->last_query();
      }

    }

    //  die("sdhfjdj");
     // echo $this->db->last_query(); die;
    $this->session->set_flashdata('tr_msg', 'Successfully added  Medical Certificate');
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect('candidate/Medical_certificate/editindex/'.$staff_id.'/'.$candidateid);


  } else {



    $insertArr = array (

      'candidate_id'            => $candidateid,
      'dateofpresence'          => $dddd,
      'candidatedate'           => $duuu,
      'createdon'               => date('Y-m-d H:i:s'),
      'createdby'               => $this->loginData->UserID,
      'small_pox'               => $this->input->post('small_pox') ,
      'spitting_disease'        => $this->input->post('spitting_disease'),
      'fainting_attacks'        => $this->input->post('fainting_attacks') ,
      'appendicitis'            => $this->input->post('appendicitis') ,
      'lung_disease'            => $this->input->post('lung_disease'),
      'epilepsy'                => $this->input->post('epilepsy'),
      'insanity'                => $this->input->post('insanity') ,
      'major_surgery'           => $this->input->post('major_surgery') ,
      'any_physical_disability' => $this->input->post('any_physical_disability'),
      'nervousness_depression'  => $this->input->post('nervousness_depression'),
      'flag'                    => 1,
      'anydetails'              => $this->input->post('showthis'),
    );

    $this->db->insert('tbl_medical_certificate', $insertArr);
     // echo $this->db->last_query(); die;
    $insertid = $this->db->insert_id();
    if(empty($this->loginData->RoleID))
    {
      //die("dvhgfds");
      $insert_data = array (

        'type'      => 19,
        'r_id'      => $insertid,
        'sender'    => $staff_id,
        'receiver'  => $reporting_to,
        'senddate'  => date('Y-m-d'),
        'createdon' => date('Y-m-d H:i:s'),
        'createdby' => $this->loginData->UserID,
        'flag'      => 1,
        'staffid'   => $staff_id
      );
     // print_r($insert_data);
     //  die();
      
      $this->db->insert('tbl_workflowdetail', $insert_data);


      $subject = ': Medical certificate form';
      $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Medical certificate form </h4><br />';
      $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
      <tr>
      <td width="96">Name </td>
      <td width="404">'.$content['candidateaddress']->staff_name.'</td>
      </tr>
      <tr>
      <td>Employ Code</td>
      <td> '.$content['candidateaddress']->emp_code.'</td>
      </tr>
      <tr>
      <td>Designation</td>
      <td>' .$content['candidateaddress']->desiname.'</td>
      </tr>
      <tr>
      <td>Office</td>
      <td>'.$content['candidateaddress']->officename.'</td>
      </tr>
      </table>';
      $body .= "<br /> <br />";
      $body .= "Regards <br />";
      $body .= " ". $content['candidateaddress']->staff_name ."<br>";
      $body .= " ". $content['candidateaddress']->desiname."<br>";
      $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
      $to_useremail = $content['candidateaddress']->emailid;
      $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
      $arr= array (
        $tcemailid      =>'tc',
      );

      $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
      redirect('candidate/Medical_certificate/previewstatement/'.$staff.'/'.$candidateid);

    }
    if($this->loginData->RoleID==2)
    {
      $w_details=$this->Medical_certificate_model->workflow_details($staff_id);
         // echo "<pre>";
          //print_r($w_details);
      if($w_details->r_id!=null && $w_details->type!=null)
      {
        $insert_data = array (

          'type'                 => 19,
          'r_id'                 => $insertid,
          'sender'               => $staff_id,
          'receiver'             => 1284,
          'senddate'             => date('Y-m-d'),
          'createdon'            => date('Y-m-d H:i:s'),
          'createdby'            => $this->loginData->UserID,
          'scomments'            => $this->input->post('status'),
          'forwarded_workflowid' => $w_details->workflowid,
          'flag'                 => 4,
          'staffid'              => $staff_id
        );
        $this->db->insert('tbl_workflowdetail', $insert_data);
       // echo $this->db->last_query();
      }

    }

     // echo $this->db->last_query(); die;
    $this->session->set_flashdata('tr_msg', 'Successfully added  Medical Certificate');
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect('candidate/Medical_certificate/previewstatement/'.$staff_id.'/'.$candidateid);

  }



}


prepareview:
$content['title'] = 'Medical_certificate';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('candidate/_main_layout', $content);
}
}


public function add($staff,$candidate_id)
{
 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
   // redirect('/staff_dashboard/index');
    
  } else {
 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);

 $staff_id=$this->loginData->staffid;
 $candidateid=$this->loginData->candidateid;





 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

}


 //$candidateid=($this->loginData->candidateid);

 //$staffids=($this->loginData->staffid);
$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);


$query332="SELECT * from tbl_medical_certificate where candidate_id=$candidateid";
$content['list_staff_tbl_medical_certificate_table']=$this->db->query($query332)->row();

$var=$content['list_staff_tbl_medical_certificate_table']->flag;

if ($var==null)
{
 goto prepareviewss;
}

if($var==0)
{
 redirect('candidate/Medical_certificate/editcertificate/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
 redirect('candidate/Medical_certificate/previewcertificate/'.$staff.'/'.$candidateid);
}

prepareviewss:

$this->load->model('Medical_certificate_model');

$content['candidate_name']=$this->Medical_certificate_model->fetchdatas($candidateid);



    // start permission 
// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();
// // end permission 



$RequestMethod = $this->input->server('REQUEST_METHOD'); 



if($RequestMethod == 'POST'){

 $this->form_validation->set_rules('txtnames','Name','trim|required|min_length[1]|max_length[50]');
 $this->form_validation->set_rules('placename','Name','trim|required|min_length[1]|max_length[50]');

 if($this->form_validation->run() == FALSE){
  $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
    '</div>');

  $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

  $hasValidationErrors    =    true;
  goto prepareview;

}

if ($_FILES['doctorssignature']['name'] != NULL) {

  $data=$_FILES['doctorssignature'];

  $this->load->model('Medical_certificate_model');

  $fileDatas=$this->Medical_certificate_model->do_uploadss($data);
  $updateArr['doctorssignature122'] = $fileDatas['orig_name'];
  $updateArr['doctorssignature_encrypted_filename'] = $fileDatas['file_name'];


}

$dateofexaminedocs=$this->input->post('dateofexaminedoc');
$this->load->model('Medical_certificate_model');
$duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($dateofexaminedocs);


$operation=$this->input->post('operation');

if($operation == 0){

 $updateArr = array (

  'doctor_seal_signature' => $updateArr['doctorssignature_encrypted_filename'],
  'weakness_except'       => $this->input->post('txtnames'),
  'place'                 => $this->input->post('placename'),
  'updatedon'             => date('Y-m-d H:i:s'),
  'updatedby'             => $this->loginData->UserID,
  'certificatedate'       => $duuu ,
  'flag'                  => 0
);

 $this->db->where('candidate_id', $candidateid);
 $this->db->update('tbl_medical_certificate', $updateArr);

 $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
 $this->session->set_flashdata('er_msg', $this->db->error());
 redirect('candidate/Medical_certificate/editcertificate/'.$staff_id.'/'.$candidateid);


} else {

 $updateArr = array (

  'doctor_seal_signature' => $updateArr['doctorssignature_encrypted_filename'],
  'weakness_except'       => $this->input->post('txtnames'),
  'place'                 => $this->input->post('placename'),
  'updatedon'             => date('Y-m-d H:i:s'),
  'updatedby'             => $this->loginData->UserID,
  'certificatedate'       => $duuu ,
  'flag'                  => 1
);

 $this->db->where('candidate_id', $candidateid);
 $this->db->update('tbl_medical_certificate', $updateArr);
     // echo $this->db->last_query(); die;
 $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
 $this->session->set_flashdata('er_msg', $this->db->error());
 redirect('candidate/Medical_certificate/previewcertificate/'.$staff_id.'/'.$candidateid);

}

}


prepareview:



$content['title'] = 'Medical_certificate';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('candidate/_main_layout', $content);
}
}



public function Editindex($staff,$candidate_id)
{

  if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    //redirect('/staff_dashboard/index');
    
  } else {

   $staff = $this->uri->segment(4);
   $candidate_id  = $this->uri->segment(5);

   $content['staff'] = $staff;
   $content['candidateid'] = $candidate_id;


   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);




   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;

   $candidateid=$candidate_id;


 }



 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

 $this->load->model('Medical_certificate_model');
 $content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);

 $content['tbldata']=$this->Medical_certificate_model->fetchtblcertificate($candidateid);
 
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);
 
 $candidatedt=$this->input->post('candidatedate');
 $datepresence=$this->input->post('doctorsigneddate');

 $this->load->model('Medical_certificate_model');
 // $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
 $content['candidatedetailwithaddress'] = $this->Medical_certificate_model->getCandidateWithAddressDetails($staff_id);
 $duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($candidatedt);
 $dddd=$this->Medical_certificate_model->getCandidateDetailsPreview($datepresence);

 $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);


 $RequestMethod = $this->input->server('REQUEST_METHOD'); 



 if($RequestMethod == 'POST'){

  $candidatedt=$this->input->post('candidatedate');
  $datepresence=$this->input->post('doctorsigneddate');



  $operation=$this->input->post('operation');

  if($operation == 0){



   $updateArr = array (

     'signatureofexamine'      =>  $updateArr['signatureofpresence_encrypted_filename'],
     'candidate_id'            =>  $candidateid,
     'candidatesignature'      =>  $updateArr['signatureofcandidate_encrypted_filename'],
     'dateofpresence'          =>  $dddd,
     'candidatedate'           =>  $duuu,
     'updatedon'               =>  date('Y-m-d H:i:s'),
     'updatedby'               =>  $this->loginData->UserID,
     'small_pox'               =>  $this->input->post('small_pox') ,
     'spitting_disease'        =>  $this->input->post('spitting_disease'),
     'fainting_attacks'        =>  $this->input->post('fainting_attacks') ,
     'appendicitis'            =>  $this->input->post('appendicitis') ,
     'lung_disease'            =>  $this->input->post('lung_disease'),
     'epilepsy'                =>  $this->input->post('epilepsy'),
     'insanity'                =>  $this->input->post('insanity') ,
     'major_surgery'           =>  $this->input->post('major_surgery') ,
     'any_physical_disability' =>  $this->input->post('any_physical_disability'),
     'nervousness_depression'  =>  $this->input->post('nervousness_depression'),
     'flag'                    =>  0,
     'anydetails'              =>  $this->input->post('showthis'),
   );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);

   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('candidate/Medical_certificate/editindex/'.$staff.'/'.$candidateid);


 } else {



   $updateArr = array (

     'signatureofexamine'      => $updateArr['signatureofpresence_encrypted_filename'],
     'candidate_id'            => $candidateid,
     'candidatesignature'      => $updateArr['signatureofcandidate_encrypted_filename'],
     'dateofpresence'          => $dddd,
     'candidatedate'           => $duuu,
     'updatedon'               => date('Y-m-d H:i:s'),
     'updatedby'               => $this->loginData->UserID,
     'small_pox'               => $this->input->post('small_pox') ,
     'spitting_disease'        => $this->input->post('spitting_disease'),
     'fainting_attacks'        => $this->input->post('fainting_attacks') ,
     'appendicitis'            => $this->input->post('appendicitis') ,
     'lung_disease'            => $this->input->post('lung_disease'),
     'epilepsy'                => $this->input->post('epilepsy'),
     'insanity'                => $this->input->post('insanity') ,
     'major_surgery'           => $this->input->post('major_surgery') ,
     'any_physical_disability' => $this->input->post('any_physical_disability'),
     'nervousness_depression'  => $this->input->post('nervousness_depression'),
     'flag'                    => 1,
     'anydetails'              => $this->input->post('showthis'),
   );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());

   $subject = ': Medical certificate form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Medical certificate form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('candidate/Medical_certificate/previewstatement/'.$staff.'/'.$candidateid);

 }
}

prepareview:

$content['subview'] = 'Medical_certificate/editindex';
$this->load->view('candidate/_main_layout', $content);
}
}



public function Editcertificate($staff=null,$candidate_id=null)
{

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);




  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }


  //$staffids=($this->loginData->staffid);
 // $candidateid=($this->loginData->candidateid);

 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

 $this->load->model('Medical_certificate_model');
 $content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);

 $queryss="select * from  tbl_medical_certificate where candidate_id=$candidateid";
 $content['fetch_datas']=$this->db->query($queryss)->row();
//print_r($content['fetch_datas']);

    // start permission 
 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();
// end permission  

 if ($_FILES['doctorssignature']['name'] != NULL) {

  $data=$_FILES['doctorssignature'];

  $this->load->model('Medical_certificate_model');

  $fileDatas=$this->Medical_certificate_model->do_uploadss($data);
  $updateArr['doctorssignature122'] = $fileDatas['orig_name'];
  $updateArr['doctorssignature_encrypted_filename'] = $fileDatas['file_name'];


}
else
{
 $updateArr['doctorssignature_encrypted_filename']=$this->input->post('docsig');
}

$dateofexaminedocs=$this->input->post('dateofexaminedoc');
$this->load->model('Medical_certificate_model');
$duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($dateofexaminedocs);


$RequestMethod = $this->input->server('REQUEST_METHOD'); 



if($RequestMethod == 'POST'){



  $operation=$this->input->post('operation');

  if($operation == 0){




   $updateArr = array (

    'doctor_seal_signature' => $updateArr['doctorssignature_encrypted_filename'],
    'weakness_except'       => $this->input->post('txtnames'),
    'place'                 => $this->input->post('placename'),
    'updatedon'             => date('Y-m-d H:i:s'),
    'updatedby'             => $this->loginData->UserID,
    'certificatedate'       => $duuu ,
    'flag'                  => 0
  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);

   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('candidate/Medical_certificate/Editcertificate/'.$staff_id.'/'.$candidateid);


 } else {



   $updateArr = array (

    'doctor_seal_signature' => $updateArr['doctorssignature_encrypted_filename'],
    'weakness_except'       => $this->input->post('txtnames'),
    'place'                 => $this->input->post('placename'),
    'updatedon'             => date('Y-m-d H:i:s'),
    'updatedby'             => $this->loginData->UserID,
    'certificatedate'       => $duuu ,
    'flag'                  => 1
  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('candidate/Medical_certificate/previewcertificate/'.$staff_id.'/'.$candidateid);

 }
}

prepareview:

$content['subview'] = 'Medical_certificate/editcertificate';
$this->load->view('candidate/_main_layout', $content);
}



public function previewcertificate($staff=null,$candidate_id=null)
{
  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);




  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }



 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();



 //$staffids=($this->loginData->staffid);
 //$candidateid=($this->loginData->candidateid);

 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);


  // $candidateid=($this->loginData->candidateid);

  // $staffids=($this->loginData->staffid);

 $content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

 $this->load->model('Medical_certificate_model');
 $content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);


 $queryss="select * from  tbl_medical_certificate where candidate_id=$candidateid";
 $content['fetch_datas']=$this->db->query($queryss)->result();



 $content['subview'] = 'Medical_certificate/previewcertificate';
 $this->load->view('candidate/_main_layout', $content);

}

public function previewstatement($staff,$candidate_id)
{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //redirect('/staff_dashboard/index');
  
} else {

 $staff = $this->uri->segment(4);
 $candidate_id  = $this->uri->segment(5);
 $content['staff'] = $staff;
 // echo $staff;
 // print_r($content['staff']); die;
 $content['candidate_id'] = $candidate_id;

 //$this->session->set_userdata('staff', $staff);
 //$this->session->set_userdata('candidate_id', $content['candidate_id']);

 $login_staff=$this->loginData->staffid;
 $staff_id=$this->loginData->staffid;
// $candidateid=$this->loginData->candidate_id;




 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;

 $candidateid=$candidate_id;


}



// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();

$query = "select * from tbl_medical_certificate";
$content['checked_data'] = $this->db->query($query)->result();
// echo "<pre>";
// print_r($content['checked_data']); die;



  //$staffids=($this->loginData->staffid);
 //$candidateid=($this->loginData->candidateid);
$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

$this->load->model('Medical_certificate_model');
$content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);
$content['id'] = $this->Medical_certificate_model->get_medicalworkflowid($staff_id);
// print_r($content['id']); die;

$content['tbldata']=$this->Medical_certificate_model->fetchtblcertificate($candidateid);
// echo "<pre>";
// print_r($content['tbldata']); die;
$content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
 // print_r($content['candidatedetailwithaddress']);
 // die;

$content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);


$m_id=$content['tbldata']->id;

$candidatedt=$this->input->post('candidatedate');
$datepresence=$this->input->post('doctorsigneddate');
$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$content['id']=$this->Medical_certificate_model->get_medicalworkflowid($staff_id);
// print_r($content['id']); die;
 // $content['tbldata']=$this->Medical_certificate_model->fetchtblcertificate($staff);

$query="select * from tbl_medical_certificate_documentupload where medical_certificate_id=$m_id";
$content['imagedata'] = $this->db->query($query)->result();
// print_r($content['imagedata']); die;

$content['personal']=$this->Common_Model->get_Personnal_Staff_List();

$content['report']=$this->Medical_certificate_model->staff_reportingto($staff_id);

 $reporting_to=$content['report']->reportingto;


$RequestMethod = $this->input->server('REQUEST_METHOD'); 


if($RequestMethod == 'POST'){

// print_r($_FILES);
// die;
 // print_r($_FILES[]); die;
                               // print_r($_POST);die;
  if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
  {
    $status   = $this->input->post('status');
    $reject   = $this->input->post('reject');
    $p_status = $this->input->post('p_status');
    $wdtaff1  = $this->input->post('wdtaff1');
    $wdtaff2  = $this->input->post('wdtaff2');
    $r_id     = $this->input->post('id');

    $check = $this->session->userdata('insert_id');

    if($status==2){
      $appstatus = 'approved';
    }elseif ($status==3) {
      $appstatus = 'Reject';
    }

    $insert_data = array (

      'type'                 => 19,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $p_status,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );

    $this->db->insert('tbl_workflowdetail', $insert_data);


    $subject = ': Medical certificate form';
    $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Medical certificate form</h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email =>'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('candidate/Medical_certificate/previewstatement/'.$staff.'/'.$candidateid);  
  }
  else if($this->loginData->RoleID==17)
  {

    // print_r($_POST);die;
    $status   = $this->input->post('status');
    $reject   = $this->input->post('reject');
    $p_status = $this->input->post('p_status');
    $wdtaff1  = $this->input->post('wdtaff1');
    $wdtaff2  = $this->input->post('wdtaff2');
    $r_id     = $this->input->post('id');

    $check = $this->session->userdata('insert_id');

    if($status==4){
      $appstatus = 'approved';
    }elseif ($status==3) {
      $appstatus = 'Reject';
    }

    $insert_data = array (

      'type'                 => 19,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $staff_id,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );

    $this->db->insert('tbl_workflowdetail', $insert_data);
    
    $subject = ': Medical certificate form';
    $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Medical certificate form </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email =>'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('candidate/Medical_certificate/previewstatement/'.$staff.'/'.$candidateid);  
  }

  else if($this->loginData->RoleID==3)
  {  

    // print_r($_FILES['certificate_originalname']['name']); die;

   // $this->load->library('upload');
   // $files = $_FILES;
   // $images = array();
   $cpt = count($_FILES['certificate_originalname']['name']); 

  //echo FCPATH; die;

   for($i=0; $i<$cpt; $i++)
   { 


     if ($_FILES['certificate_originalname']['name'][$i] != NULL) {
       $ext = end((explode(".", $_FILES['certificate_originalname']['name'][$i]))); 
       $OriginalName    = $_FILES['certificate_originalname']['name'][$i]; 
       $encryptedName   = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext; 
       $tempFile         = $_FILES['certificate_originalname']['tmp_name'][$i];
       $targetPath       = FCPATH . "datafiles/medical_certificate/";
       $targetFile       = $targetPath . $encryptedName;
       $uploadResult     = move_uploaded_file($tempFile,$targetFile);
     // echo $uploadResult; die;
     // print_r($uploadResult); die;
       if($uploadResult == true){
        $encryptedImage  = $encryptedName;
      }else{

        die("Error uploading Profile Photo ");
        $this->session->set_flashdata("er_msg", "Kindly submitted your documents");
        redirect(current_url());
      }

      $data = array (
       'medical_certificate_id'   => $m_id,
       'certificate_originalname' => $OriginalName,
       'certificate_encryptedname' => $encryptedName,
     );
  //     echo "<pre>";
  // print_r($data); die;
      $this->db->insert('tbl_medical_certificate_documentupload',$data);


    }
  }
  redirect('candidate/Medical_certificate/previewstatement/'.$staff.'/'.$candidateid);  

}

}
$content['subview'] = 'Medical_certificate/previewstatement';
$this->load->view('candidate/_main_layout', $content);
}
}

public function getpdfcertificate($staff,$candidate_id)

{
  $var ='';
  if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
   // redirect('/staff_dashboard/index');
    
  } else {

   $staff = $this->uri->segment(4);
   $candidate_id  = $this->uri->segment(5);

   $content['staff'] = $staff;
   $content['candidateid'] = $candidate_id;

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);




   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         // echo "staff".$staff_id; 
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid; 

 }


 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();

 //$candidateid=($this->loginData->candidateid);

 $this->load->model('Medical_certificate_model');
 $content['datas']=$this->Medical_certificate_model->fetchtblcertificate($candidateid);
 $row=$content['datas'];
 $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
 $content['candidatedetailwithaddress'] = $this->Medical_certificate_model->getCandidateWithAddressDetails($staff_id);
 // print_r($content['candidatedetailwithaddress']); die;
// print_r($row);
 
 
 // echo $this->db->last_query();
 // die;
 if($row->small_pox==1)
 {
  $small_pox="yes";
}
else {
  $small_pox="No";
}
if($row->spitting_disease==1)
{
  $spitting_disease="yes";
}
else {
  $spitting_disease="No";
}
if($row->fainting_attacks==1)
{
  $fainting_attacks="yes";
}
else {
  $fainting_attacks="No";
}
if($row->appendicitis==1)
{
  $appendicitis="yes";
}
else {
  $appendicitis="No";
}
if($row->lung_disease==1)
{
  $lung_disease="yes";
}
else {
  $lung_disease="No";
}
if($row->epilepsy==1)
{
  $epilepsy="yes";
}
else {
  $epilepsy="No";
}
if($row->insanity==1)
{
  $insanity="yes";
}
else {
  $insanity="No";
}
if($row->major_surgery==1)
{
  $major_surgery="yes";
}
else {
  $major_surgery="No";
}

if($row->any_physical_disability==1)
{
  $any_physical_disability="yes";
}
else {
  $any_physical_disability="No";
}

if($row->nervousness_depression==1)
{
  $nervousness_depression="yes";
}
else {
  $nervousness_depression="No";
}
// echo "<pre>";
// print_r($row); die;
$content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);
// print_r($content['candidate_list']); die;
$candidate=$content['candidate_list'];
if(!empty($candidate->dateofbirth))
{
 $cur_year    = date('Y');
 $year        = $candidate->dateofbirth;
 $arr         = explode('-', $year);
 $currentyear = $arr[0];
 $var         = $cur_year-$currentyear;
// echo '<b>'.$var.'</b>';
}
$dob = date("Y-m-d");
      if (!empty($candidate->dateofbirth))
      {
        $candidate->dateofbirth = date("Y-m-d");
        $dob=$this->General_nomination_and_authorisation_form_model->changedate($candidate->dateofbirth);
      }
                       

 

// print_r($candidate);
//  die;
$timestamp = strtotime($row->certificatedate);

 //die("hdghsdgf");
$dddd= date('d/m/Y', $timestamp);


$candidate = array('$staff_name','$dob','$var','$small_pox','$spitting_disease','$fainting_attacks','$appendicitis','$lung_disease','$epilepsy','$insanity','$major_surgery','$any_physical_disability','$nervousness_depression','$anydetails','$desiname');
$candidate_replace = array($content['candidatedetailwithaddress']->staff_name,$dob,$var,$small_pox,$spitting_disease,$fainting_attacks,$appendicitis,$lung_disease,$epilepsy,$insanity,$major_surgery,$any_physical_disability,$nervousness_depression,$content['datas']->anydetails,$content['candidatedetailwithaddress']->desiname);

$content['personal']=$this->Common_Model->get_Personnal_Staff_List();


$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 21 AND `isactive` = '1'";
$data = $this->db->query($sql)->row();
if(!empty($data))
  $body = str_replace($candidate,$candidate_replace , $data->lettercontent);


$filename = $staff.$candidate_id."_Medical_certificate";
 // echo $body; die;
$genpdf = $this->Dompdf_model->generatePDFMedicalcertificate($body, $filename, NULL,'Generateofferletter.pdf'); 
// print_r($genpdf); die;
if($genpdf==true)
{
  $arr=array('flag'=>99);
  $this->db->where('id',$row->id); 
  $this->db->update('tbl_medical_certificate',$arr);

}
}

}

public function pdfstatement()
{

  // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  // $content['role_permission'] = $this->db->query($query)->result();



  $staffids=($this->loginData->staffid);
  $candidateid=($this->loginData->candidateid);
  $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);




  $this->load->model('Medical_certificate_model');
  $content['candidate_list']=$this->Medical_certificate_model->fetchdatas($candidateid);

  $content['tbldata']=$this->Medical_certificate_model->fetchtblcertificate($candidateid);

  $candidatedt=$this->input->post('candidatedate');
  $datepresence=$this->input->post('doctorsigneddate');

  $content['subview'] = 'Medical_certificate/previewstatement';
  $this->load->view('candidate/_main_layout', $content);
}




}