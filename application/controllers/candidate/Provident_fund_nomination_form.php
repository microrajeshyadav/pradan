
<?php 

/**
* State List
*/
class Provident_fund_nomination_form extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('loginData');
    $this->load->model("General_nomination_and_authorisation_form_model");
    $this->load->model("Provident_fund_nomination_form_model");
    $this->load->model("Employee_particular_form_model");
    $this->load->model("Global_model","gmodel");

    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('loginData');
   /*echo "<pre>";
   print_r($this->loginData);exit();*/
 }

 public function index($staff,$candidate_id)
 {

   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //  redirect('/staff_dashboard/index');
    
  } else {


   $staff = $this->uri->segment(4);
   $candidate_id  = $this->uri->segment(5);

   $content['staff'] = $staff;
   $content['candidate_id'] = $candidate_id;

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);


   $staff_id=$this->loginData->staffid;
   @ $candidateid=$this->loginData->candidateid;




   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
        // echo "staff".$staff;
   $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

 }

 $content['report']=$this->Provident_fund_nomination_form_model->staff_reportingto($staff_id);

 $content['candidateaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Provident_fund_nomination_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Provident_fund_nomination_form_model->tc_email($reportingto);
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);
 
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 

 if($RequestMethod == "POST")
 {
//     echo "<pre>";
// print_r($this->input->post()); die;
// echo "jkbjh"; die;
  

  $Sendsavebtn = $this->input->post('savebtn');

  if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

       $data1=$this->input->post('data');

   // foreach($data1 as $key => $value)
   // {
   //  echo "<pre>";
   //  print_r($value); 
   // }
   //  die();

   $ddate = $this->input->post('date');

   $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($ddate);


   $insertarraydata = array(

    'candidate_id' => $candidateid,
    'staff_id'     => $staff_id,
    'place'        => $this->input->post('daplace'),
    'date'         => $dadate,
    'status'       => 0, 
    'type'         => 'j',
  );
   // print_r($insertarraydata); die;
   $this->db->insert('provident_fund_nomination', $insertarraydata);

   $insertid = $this->db->insert_id();

   $data1=$this->input->post('data');



   foreach($data1 as $key => $value)
   {
    if ($value['name'] != '') {
      $arr = array (
      'sr_no'         => $key,
      'name'          => $value['name'],
      'provident_id'  => $insertid,
      'relation_id'   => $value['relationship_nominee'],
      'age'           => $value['age_nominee'],
      'address'       => $value['address_nominee'],
      'share_nominee' => $value['share_nominee'],
      'minior'        => $value['minior']

    );
      $this->db->insert('provident_fund_nomination_details', $arr);
     // echo $this->db->last_query();

    }
      
   
      }

  $insert_data =array(
    'type'      => 13,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
  // print_r($insert_data); die;
  $this->db->insert('tbl_workflowdetail', $insert_data);

  $this->db->trans_complete();

  if ($this->db->trans_status() === FALSE){

    $this->session->set_flashdata('er_msg', 'Error !!! Provident fund  nomination form');  

  }else{

    $this->session->set_flashdata('tr_msg', 'Successfully save  Provident fund  nomination form'); 

    redirect('candidate/Provident_fund_nomination_form/edit/'.$staff.'/'.$candidateid);    
  }

}

$submitdatasend = $this->input->post('submitbtn');

if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {

  $ddate = $this->input->post('date');

  $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($ddate);



  $insertarraydata = array(

   'candidate_id' => $candidateid,
   'staff_id'     => $staff_id,
   'place'        => $this->input->post('daplace'),
   'date'         => $dadate,
   'status'       => 1,
   'type'         => 'j',
 );
// print_r($insertarraydata); die;
  $this->db->insert('provident_fund_nomination', $insertarraydata);
  // echo $this->db->last_query(); die;
  $insertid = $this->db->insert_id();
  $data1=$this->input->post('data');

  foreach($data1 as $value)
  {

    if ($value['name'] != '') {

    $arr = array (

      'sr_no'         => $value['sr_no'],
      'name'          => $value['name'],
      'provident_id'  => $insertid,
      'relation_id'   => $value['relationship_nominee'],
      'age'           => $value['age_nominee'],
      'address'       => $value['address_nominee'],
      'share_nominee' => $value['share_nominee'],
      'minior'        => $value['minior']

    );
    $this->db->insert('provident_fund_nomination_details', $arr);
  }
  }


  $insertid = $this->db->insert_id();

  $data1=$this->input->post('data');

  $insert_data = array (

    'type'      => 13,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
  $this->db->insert('tbl_workflowdetail', $insert_data);

  $this->db->trans_complete();

  if ($this->db->trans_status() === FALSE){

    $this->session->set_flashdata('er_msg', 'Error !!! Provident fund  nomination form');  

  }else{

    $this->session->set_flashdata('tr_msg', 'Successfully save and submit Provident fund  nomination form');


    $subject = ': Provident fund nomimnation form';
    $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Provident fund nomimnation form </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr); 

    redirect('candidate/Provident_fund_nomination_form/view/'.$staff.'/'.$candidateid);    
  }
}

}




$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
// /print_r($content['topbar']);

$var=$content['topbar']->provident_flag;
       //echo $var;
      // die;

      // print_r( $content['workexperiencedetails']);



  //$this->db->insert('mstpgeducation', $insertArr);
 // "data=".$_FILES['signature']['name'];

if ($var==null)
{
  goto preview;
    //redirect('/Provident_fund_nomination_form/index/'.$staff.'/'.$candidateid);
}


if($var==0)
{
  redirect('candidate/Provident_fund_nomination_form/edit/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
  redirect('candidate/Provident_fund_nomination_form/view/'.$staff.'/'.$candidateid);
}


preview:

$content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();



$content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

$content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
// print_r($content['candidatedetailwithaddress']); die;

$content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
$content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);

$content['witnessdeclaration'] = $this->General_nomination_and_authorisation_form_model->getWitnessDeclaration($candidateid);
// $content['office_name'] = $this->General_nomination_and_authorisation_form_model->office_name();

      //die('hjfsjdhgfs');

$content['title'] = 'Provident_fund_nomination_form';

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('candidate/_main_layout', $content);
}
}
public function edit($staff,$candidate_id)
{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //redirect('/staff_dashboard/index');
  
} else {

 $staff = $this->uri->segment(4);
 $candidate_id  = $this->uri->segment(5);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);



 $staff_id=$this->loginData->staffid;
 $candidateid=$candidate_id;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

}


$content['nomineedetail'] = $this->Provident_fund_nomination_form_model->getNomineedetail($candidateid);
$content['nominee'] =$this->Provident_fund_nomination_form_model->get_pfinformation($candidateid);
// echo "<pre>";
// print_r($content['nominee']); die;

$content['candidateaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Provident_fund_nomination_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Provident_fund_nomination_form_model->tc_email($reportingto);
$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$RequestMethod = $this->input->server('REQUEST_METHOD');

if($RequestMethod == "POST"){
   // echo "<pre>";

   // print_r($this->input->post()); die();

  $savesenddata = $this->input->post('savebtn');
  // echo $savesenddata;
  // die;

  if (!empty($savesenddata) && $savesenddata =='senddatasave') {




           //echo "image=".$updateArr['signatureplace_encrypted_filename'];
          // die;
    $dadate = $this->input->post('dadate');
    $provident_id    =$this->input->post('provident_fund_nomination_id');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



    $updatearraydata = array (

     'candidate_id' => $candidateid,
     'staff_id'     => $staff_id,
     'place'        => $this->input->post('daplace'),
     'date'         => $dadate,
     'status'       => 0
   );

    $this->db->where('id', $provident_id);
    $this->db->update('provident_fund_nomination', $updatearraydata);
     
      



    $data1=$this->input->post('data');
    


   $data1=$this->input->post('data');
   

  foreach($data1 as $value)
   {
    $countnominee = count($value['full_name_nominee']);
    if($countnominee>0)
    {
      
    $id =  $value['provident_id']; 
    $this->db->where('provident_id', $id);
      $this->db->delete('provident_fund_nomination_details');
    }
   }


 foreach($data1 as $key => $value)
   {

    $countnominee = count($value['full_name_nominee']);
    if($countnominee>0)
    {

      if ($value['full_name_nominee'] !='') {
      $arr = array (

        'sr_no'         => $key,
        'provident_id'  =>$value['id'],
        'name'          => $value['full_name_nominee'],
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior'],

      );
         
      $this->db->insert('provident_fund_nomination_details', $arr);

    }
         
    }
    }
    
     
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Provident fund  nomination form');  

    }else{

      $this->session->set_flashdata('tr_msg', 'Successfully save  Provident fund  nomination form'); 

    }
    redirect('candidate/Provident_fund_nomination_form/edit/'.$staff.'/'.$candidateid);   
  }


  $submitsenddata = $this->input->post('submitbtn');

  if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {




    $dadate = $this->input->post('dadate');
    $provident_id    =$this->input->post('provident_fund_nomination_id');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



    $updatearraydata = array (

     'candidate_id' => $candidateid,
     'staff_id'     => $staff_id,
     'place'        => $this->input->post('daplace'),
     'date'         => $dadate,
     'status'       => 1
   );

    $this->db->where('id', $provident_id);
    $this->db->update('provident_fund_nomination', $updatearraydata);
     


    $data1=$this->input->post('data');
    //print_r($data1);
    //die;

   /* $data1=$this->input->post('data');
    // echo "<pre>";
    // print_r($data1);
    // die;

    foreach($data1 as $key => $value)
    {

      if (!empty($value['id']) || $value['full_name_nominee'] !='') {
        
      $updatearry = array (
        'sr_no'         => $key,
        'name'          => $value['full_name_nominee'],
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior'],
      );
      $this->db->where('id', $id);
      $this->db->update('provident_fund_nomination_details', $updatearry);
      }else{
        $insertarry = array (
        'sr_no'         => $key,
        'provident_id'  => $content['nominee']->id,
        'name'          => $value['full_name_nominee'],
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior'],
      );
      $this->db->insert('provident_fund_nomination_details', $insertarry);



      }*/


      $data1=$this->input->post('data');
   

  foreach($data1 as $value)
   {
    $countnominee = count($value['full_name_nominee']);
    if($countnominee>0)
    {
      
    $id =  $value['provident_id']; 
    $this->db->where('provident_id', $id);
      $this->db->delete('provident_fund_nomination_details');
    }
   }


 foreach($data1 as $key => $value)
   {

    $countnominee = count($value['full_name_nominee']);
    if($countnominee>0)
    {

      if ($value['full_name_nominee'] !='') {
      $arr = array (

        'sr_no'         => $key,
        'provident_id'  =>$value['id'],
        'name'          => $value['full_name_nominee'],
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior'],

      );
         
      $this->db->insert('provident_fund_nomination_details', $arr);

    }
         
    }
    }
    
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Provident fund  nomination form');  

    }

      /*else{

        // $hrdemailid    = 'poonamadlekha@pradan.net';
         $hrdemailid    = 'amit.kum2008@gmail.com';
         $tcemailid     = $this->loginData->EmailID;

         $subject = "Submit Gereral Nomination And Authorisation Form ";
          $body =   'Dear Sir, <br><br> ';
          $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
         //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
         $to_email = $hrdemailid;
         $to_name = $tcemailid;
        
         $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/

         $this->session->set_flashdata('tr_msg', 'Successfully save and submit Provident fund  nomination form'); 


         $subject = ': Provident fund nomination form';
         $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Provident fund nomination form</h4><br />';
         $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
         <tr>
         <td width="96">Name </td>
         <td width="404">'.$content['candidateaddress']->staff_name.'</td>
         </tr>
         <tr>
         <td>Employ Code</td>
         <td> '.$content['candidateaddress']->emp_code.'</td>
         </tr>
         <tr>
         <td>Designation</td>
         <td>' .$content['candidateaddress']->desiname.'</td>
         </tr>
         <tr>
         <td>Office</td>
         <td>'.$content['candidateaddress']->officename.'</td>
         </tr>
         </table>';
         $body .= "<br /> <br />";
         $body .= "Regards <br />";
         $body .= " ". $content['candidateaddress']->staff_name ."<br>";
         $body .= " ". $content['candidateaddress']->desiname."<br>";
         $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
         $to_useremail = $content['candidateaddress']->emailid;
         $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
         $arr= array (
          $tcemailid      =>'tc',
        );

         $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
         redirect('candidate/Provident_fund_nomination_form/view/'.$staff.'/'.$candidateid);    



       }


     }
     $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();
     $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
    //print_r($content['candidatedetailwithaddress']);

     $content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);

     $content['genenominformdetail'] = $this->General_nomination_and_authorisation_form_model->getGeneralnominationform($staff_id);


     $content['nominee'] =$this->Provident_fund_nomination_form_model->get_pfinformation($candidateid);
     // print_r( $content['nominee']); die;
     $content['count_nominee'] =$this->Provident_fund_nomination_form_model->count_pfinformation($candidateid);
    //print_r($content['count_nominee']);

     



     $content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
     $content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);

     $content['witnessdeclaration'] = $this->General_nomination_and_authorisation_form_model->getWitnessDeclaration($candidateid);


     $content['title'] = 'Provident_fund_nomination_form/edit';

     $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

     $this->load->view('candidate/_main_layout', $content);
   }
 }

public function delete_row($nomnieeid = null, $proid = null)
{
  
  if ($nomnieeid == null && $proid == null) 
    {
      $this->session->set_flashdata('er_msg', "No  ID reference set to show provident fund nomination");
      redirect('Provident_fund_nomination_form');
    }
    $this->db->where('id',$nomnieeid);
    $this->db->where('provident_id',$proid);
  $result = $this->db->delete('provident_fund_nomination_details');
  if ($result) {
    echo "1";
  }else {

    echo "0";
  }

  }


 public function view($staff,$candidate_id)
 {
   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    //redirect('/staff_dashboard/index');
    
  } else {

   $staff            = $this->uri->segment(4);
   $candidate_id     = $this->uri->segment(5);

   $content['staff'] = $staff;
   $content['candidate_id'] = $candidate_id;

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);




   $staff_id         = $staff;
   $login_staff      = $this->loginData->staffid;
   $candidateid      = $candidate_id;



   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;
   $candidateid=$candidate_id;
       //echo "candidateid".$candidateid;

 }

 $content['count_nominee'] =$this->Provident_fund_nomination_form_model->count_pfinformation($candidateid);
   // print_r($content['count_nominee']);
 $content['id'] = $this->Provident_fund_nomination_form_model->get_providentworkflowid($staff_id);
   // print_r($content['id']); die;


 $content['staff_details'] = $this->Provident_fund_nomination_form_model->staffName($staff_id,$login_staff);
   // echo "<pre>";
   // print_r($content['staff_details']); die;

 $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();

 $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
 
 $content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);

 $content['get_witness'] = $this->Provident_fund_nomination_form_model->get_witness_detail($staff_id);
   // print_r($content['get_witness']); die;
 $content['nominee'] =$this->Provident_fund_nomination_form_model->get_pfinformation($candidateid);
   // print_r($content['nominee']); die;
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);
 
 $content['nomineedetail'] = $this->Provident_fund_nomination_form_model->getNomineedetail($candidateid);
   // print_r($content['nomineedetail']); die;
 
 $content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
 $content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);
 $content['candidateaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Provident_fund_nomination_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Provident_fund_nomination_form_model->tc_email($reportingto);
 $content['reporting'] = $this->Provident_fund_nomination_form_model->getCandidateWith($reportingto);

 $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;
 $content['personal']=$this->Common_Model->get_Personnal_Staff_List();
  if(!empty($this->loginData->RoleID))
  {

 if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
 {
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST')
   {
                               // print_r($_POST);die;
    $status   = $this->input->post('status');
    $reject   = $this->input->post('reject');
    $p_status = $this->input->post('p_status');
    $wdtaff1  = $this->input->post('wdtaff1');
    $wdtaff2  = $this->input->post('wdtaff2');
    $address1    = $this->input->post('w_add1'); 
    $address2    = $this->input->post('w_add2');
    $r_id     = $this->input->post('id');
    
    $check    = $this->session->userdata('insert_id');

    if($status==2){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    }

    $insert_data = array (

      'type'                 => 13,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $p_status,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );
    $this->db->insert('tbl_workflowdetail', $insert_data);

    $arr_data = array (

      'witness1'   => $wdtaff1,
      'witness2'   => $wdtaff2,
      'address1'   => $address1,
      'address2'   => $address2, 
    ); 

    

    $this->db->where('id',$r_id);
    $this->db->update('provident_fund_nomination',$arr_data);


    $subject = ': Provident fund nomination form';
    $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc  </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email => 'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('/Provident_fund_nomination_form/view/'.$staff.'/'.$candidateid);


  }


}

else if($this->loginData->RoleID == 17)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST')
 {
                               // print_r($_POST);die;
  $status   = $this->input->post('status');
    // echo $status; die;
  $reject   = $this->input->post('reject');
  $p_status = $this->input->post('p_status');
  $wdtaff1  = $this->input->post('wdtaff1');
  $wdtaff2  = $this->input->post('wdtaff2');
  $r_id     = $this->input->post('id');
  
  $check    = $this->session->userdata('insert_id');
  if($status==4){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }


  $insert_data = array (

    'type'                 => 13,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );
    // print_r($insert_data); die;
  $this->db->insert('tbl_workflowdetail', $insert_data);

  $arr_data  = array (

    'personal_date'  => $this->gmodel->changedatedbformate($this->input->post('personal_date')),
    'personal_name'  => $login_staff,

  ); 
  $this->db->where('id',$r_id);
  $this->db->update('provident_fund_nomination',$arr_data);


  $subject = ':Provident fund nomination form';
  $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel</h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
  $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email => 'personal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('/Provident_fund_nomination_form/view/'.$staff.'/'.$candidateid);

}


}
}

if(empty($this->loginData->RoleID))
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){

      // $p_status = $this->input->post('p_status');
  $r_id     = $this->input->post('id');



  $insert_data = array (

    'type'                 => 13,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => 5,
    'staffid'              => $staff_id
  );
    // print_r($insert_data); die;
  $this->db->insert('tbl_workflowdetail', $insert_data);
  

  $arr_data = array (
    'acknowlegementdate'=> $this->gmodel->changedatedbformate($this->input->post('acknowlegementdate')),
  );  
    // print_r($arr_data); die;

  $this->db->where('id',$r_id);
  $this->db->update('provident_fund_nomination',$arr_data);

}


}
$query = "SELECT acknowlegementdate from provident_fund_nomination where staff_id = '$staff_id'";
$content['acknowlegementdate'] = $this->db->query($query)->row();
// print_r($content['acknowlegementdate']); die;

$content['title'] = 'Provident_fund_nomination_form/view';

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('candidate/_main_layout', $content);
}
}





public function Add($staff=null,$candidate_id=null)
{


  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;

   $candidateid=$candidate_id;


 }


        // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){

  $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]');
  $this->form_validation->set_rules('status','Status','trim|required');

  if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }


  $insertArr = array (
    'pgname'      => $this->input->post('pgname'),
    'status'      => $this->input->post('status')
  );

  $this->db->insert('mstpgeducation', $insertArr);

  $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
  $this->session->set_flashdata('er_msg', $this->db->error());
  redirect('/Employee_particular_form/index/');
}

prepareview:


$content['subview'] = 'Employee_particular_form/add';
$this->load->view('_main_layout', $content);
}

}