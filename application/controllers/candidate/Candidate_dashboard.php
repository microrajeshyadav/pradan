<?php 
class Candidate_dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Candidate_dashboard_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		//$check = $this->session->userdata('loginData');
		///// Check Session //////	
		// if (empty($check)) {
		// 	 redirect('candidate/Login/index');
		// }

		$this->loginData = $this->session->userdata('loginData');

	}

	public function index()
	{
		
		try{
			

            $content['method']  = $this->router->fetch_method();
			$content['title']  = 'Candidate_dashboard';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('candidate/_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}

}