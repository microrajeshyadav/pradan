<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Daship components controller
*/
class Daship_components extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model("Common_model","Common_Model");
		$this->load->library('form_validation');
		$this->load->model(__CLASS__ . '_model', 'model');	

		$check = $this->session->userdata('loginData');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('loginData');

	}



	public function index()
	{

		$getdaship='';
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

 	///////////////Get Phase List ////////////// 

		$query = "select * from mstphase where isdeleted =0";
		$content['phase_details'] = $this->Common_Model->query_data($query);

		$getdaship = $this->getDashipComponentDetails();
		$content['dashipcomp_details'] = $getdaship;
		$content['daship_details'] = $this->getCandidateDetails();
		$content['dashipdocumentdetails'] = $this->model->getDashipComponentDocument();

		$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
		

		$content['title'] = 'Daship_components';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);
		
	}



	public function view($token)
	{
		try {
			
			$query = "select * from mstphase where isdeleted =0";
			$content['phase_details'] = $this->Common_Model->query_data($query);

			$getdaship = $this->getDashipComponentDetails();
			$content['dashipcomp_details'] = $getdaship;
			$content['daship_details'] = $this->getCandidateDetails();
			$content['dashipdocumentdetails'] = $this->model->getSindleDashipComponentDocument($token);
			$content['fetch_method'] = $this->router->fetch_method();

			$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
			$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
			
			$content['title'] = 'Daship_components';
			$content['subview'] = '/Daship_components/add';
			$this->load->view('candidate/_main_layout', $content);
		} catch (Exception $e) {
			
			print_r($e->getMessage());die;
		}

		
	}


	public function add()
	{

		$getdaship='';
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			
			$this->db->trans_start();

			// print_r($this->input->post()); 
			// print_r($_FILES);
			

			// 	$this->form_validation->set_rules('phase',' Phase ','trim|required');
		 //    	$this->form_validation->set_rules('phase_document',' Upload File','trim|required');

			// if($this->form_validation->run() == FALSE){
		 //    		$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
		 //    			'</div>');

		 //    		$this->session->set_flashdata('err_msg', 'There have been validation error(s), please check the error messages');

		 //    		$hasValidationErrors    =    true;
		 //    		goto prepareview;

		 //    	}


			if ($_FILES['phase_document']['name']!= NULL) {

				@  $ext = end(explode(".", $_FILES['phase_document']['name'])); 

				$original_document = $_FILES['phase_document']['name']; 
				$encryptedfirstsevenName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['phase_document']['tmp_name'];
				$targetPath = FCPATH . "datafiles/dashipcomponent/";
				$targetFile = $targetPath . $encryptedfirstsevenName;
				@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

				if($uploadResult == true){

					$encrypted_document = $encryptedfirstsevenName;

				}else{

					die("Error uploading uploading document ");
					$this->session->set_flashdata("er_msg", "Error uploading document");
					redirect(current_url());
				}

			}
			else{

				$encrypted_document = $this->input->post('encrypted_first_sevnday_orientation');
				$original_document =  $this->input->post('original_first_sevnday_orientation');
			}

			
			
			$query = $this->db->query("SELECT * FROM `tbl_daship_component` WHERE `candidateid` = ".$this->loginData->candidateid." AND `phaseid` =".$this->input->post('phase')." ");
			$numrows = $query->num_rows();
			$result = $query->result();

			if($numrows > 0){
				$this->session->set_flashdata('er_msg', 'Already upload phase document');	
				redirect(current_url());

			}

			$insertarraydata = array(
				'candidateid'  => $this->loginData->candidateid,
				'original_document_name'  => $original_document,
				'encrypted_document_name'  => $encrypted_document,
				'status'  => 1,
				'phaseid'  => $this->input->post('phase'),
				'createdon'  => date('Y-m-d H:i:s'), 
				'createdby'  => $this->loginData->candidateid,
			);

           //print_r($insertarraydata); die;
			$this->db->insert('tbl_daship_component', $insertarraydata);
		   //  echo $this->db->last_query(); die;

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DAship components save');	
				redirect(current_url());

			}else{
				
				$this->session->set_flashdata('tr_msg', 'Successfully DAship components save');	

				redirect('candidate/Daship_components');
			}
			
		}

	 	///////////////Get Phase List ////////////// 
		//prepareview:

		$content['phase_details'] = $this->model->getPhaseWithtransaction();

		$getdaship = $this->getDashipComponentDetails();
		$content['dashipcomp_details'] = $getdaship;
		$content['daship_details'] = $this->getCandidateDetails();

		$content['fetch_method'] = $this->router->fetch_method();
		
		$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);

		$content['title'] = 'Daship_components';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('candidate/_main_layout', $content);
		
	}


	public function edit($token=NULL){

		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){
				
		// print_r($this->input->post());
		// print_r($_FILES); die;
				$this->db->trans_start();
				if ($_FILES['phase_document']['name']!= NULL) {

					@  $ext = end(explode(".", $_FILES['phase_document']['name'])); 

					$original_document = $_FILES['phase_document']['name']; 
					$encryptedfirstsevenName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
					$tempFile = $_FILES['phase_document']['tmp_name'];
					$targetPath = FCPATH . "datafiles/dashipcomponent/";
					$targetFile = $targetPath . $encryptedfirstsevenName;
					@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){

						$encrypted_document = $encryptedfirstsevenName;

					}else{

						die("Error uploading uploading document ");
						$this->session->set_flashdata("er_msg", "Error uploading document");
						redirect(current_url());
					}

				}
				else{

					$encrypted_document = $this->input->post('encrypted_phase_document');
					$original_document =  $this->input->post('original_phase_document');
				}

				$updatearraydata = array(
					'candidateid'             => $this->loginData->candidateid,
					'original_document_name'  => $original_document,
					'encrypted_document_name' => $encrypted_document,
					'status'                  => 1,
					'phaseid'                 => $this->input->post('phase'),
					'updatedon'               => date('Y-m-d H:i:s'), 
					'updatedby'               => $this->loginData->candidateid,
				);

           //print_r($insertarraydata); die;
				$this->db->where('id', $token);	
				$this->db->update('tbl_daship_component', $updatearraydata);

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! DAship components update');	
					redirect(current_url());

				}else{
					
					$this->session->set_flashdata('tr_msg', 'Successfully DAship components update');	

					redirect('candidate/Daship_components');
				}
				
			}
			
			
	///////////////Get Phase List ////////////// 


			$query = "select * from mstphase where isdeleted =0";
			$content['phase_details'] = $this->Common_Model->query_data($query);

			$getdaship = $this->getDashipComponentDetails();
			$content['dashipcomp_details'] = $getdaship;
			$content['daship_details'] = $this->getCandidateDetails();
			$content['dashipdocumentdetails'] = $this->model->getSindleDashipComponentDocument($token);

			
			$content['fetch_method'] = $this->router->fetch_method();

			$content['title'] = 'Daship_components';
			$content['subview'] = 'Daship_components/edit';
			$this->load->view('candidate/_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	

	public function getCandidateDetails()
	{

		 	//print_r($this->loginData); die;
		try{

			$sql = "SELECT `tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname,`mstbatch`.batch,`tbl_da_personal_info`.emp_code FROM 
			`tbl_candidate_registration`
			left join `tbl_da_personal_info` on `tbl_candidate_registration`.candidateid = `tbl_da_personal_info`.candidateid
			left join `mstbatch` on `tbl_candidate_registration`.batchid = `mstbatch`.id
			Where `tbl_candidate_registration`.candidateid = ".$this->loginData->candidateid." "; 
			$result = $this->db->query($sql)->result()[0];
			return $result;
			//print_r($result1); die;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		

	}


	public function getDashipComponentDetails()
	{

		try{

			$sql = "SELECT * FROM `tbl_daship_component`
			Where `tbl_daship_component`.candidateid = ".$this->loginData->candidateid." "; 
			$result = $this->db->query($sql)->result();
			return $result;
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

}