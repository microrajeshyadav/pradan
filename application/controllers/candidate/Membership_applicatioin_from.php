<?php 

/**
* State List
*/
class Membership_applicatioin_from extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model');
    $this->load->model('Membership_applicatioin_from_model');
    $this->load->model("General_nomination_and_authorisation_form_model");
    $this->load->model('Employee_particular_form_model');
    $check = $this->session->userdata('loginData');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('loginData');
 }

 public function index($staff,$candidate_id)

 {
  if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
   // redirect('/staff_dashboard/index');
    
  } else {
   $staff = $this->uri->segment(4);
   $candidate_id  = $this->uri->segment(5);

   $content['staff'] = $staff;
   $content['candidateid'] = $candidate_id;

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);

   $staff_id=$this->loginData->staffid;
   @ $candidateid=$this->loginData->candidateid;

   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
       // echo "staff id".$staff_id;
    $candidateid=$candidateid;
      //  echo "candate id=".$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff;
   $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

 }


   //$candidateid=($this->loginData->candidateid);

  // $staffids=($this->loginData->staffid);
 $content['report']=$this->Membership_applicatioin_from_model->staff_reportingto($staff_id);
 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
    // start permission 
 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();
// end permission 


 $query332="SELECT * from membership_application_form where candidate_id=$candidateid";
 $content['list_staff_tblstaffjoiningreport_table']=$this->db->query($query332)->row();
  // echo "<pre>";
   //print_r($content['list_staff_tblstaffjoiningreport_table']);
 if($content['list_staff_tblstaffjoiningreport_table'])
   $var=$content['list_staff_tblstaffjoiningreport_table']->flag;

 else $var = '';

 if ($var==null)
 {

  goto prepareviewss;
}


if($var==0)
{
  redirect('candidate/Membership_applicatioin_from/editmembership/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
  redirect('candidate/Membership_applicatioin_from/previewemembership/'.$staff.'/'.$candidateid);
}


prepareviewss:




$query1="Select officename from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();



$this->load->model('Membership_applicatioin_from_model');
$content['data_application']=$this->Membership_applicatioin_from_model->fetchdatas($candidateid);
// echo "<pre>";
// print_r($content['data_application']);

$content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

$content['candidatedetailwithaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);

$content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);

$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$RequestMethod = $this->input->server('REQUEST_METHOD'); 

if($RequestMethod == 'POST'){
 $this->form_validation->set_rules('rupees','PG Name','trim|required|min_length[1]|max_length[50]');




 $du=$this->input->post('datess');
 $this->load->model('Membership_applicatioin_from_model');
 $duuu=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($du);

 $operation=$this->input->post('operation');

 if($operation == 0){

   $insertArr = array (

    'rupees'       => $this->input->post('rupees'),  
    'candidate_id' => $candidateid,
    'month'        => $this->input->post('month'),  
    'year'         => $this->input->post('years'),  
    'basic_pay'    => $this->input->post('basic_pay'),  
      // 'cansignature' => $updateArr['signatureplace_encrypted_filename'],  
    'date'         => $duuu, 
    'flag'         => 0,
    'createdon'    => date('Y-m-d H:i:s'),
    'createdby'    => $this->loginData->UserID,

  );

   $this->db->insert('membership_application_form', $insertArr);
   $insertid = $this->db->insert_id();

   $insert_data = array (

    'type'      => 16,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
   $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully added  Membership application form');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Membership_applicatioin_from/editmembership/'.$staff.'/'.$candidateid);


 } else {

   $insertArr = array (

    'rupees'       => $this->input->post('rupees'),  
    'candidate_id' => $candidateid,
    'month'        => $this->input->post('month'),  
    'year'         => $this->input->post('years'), 
    'basic_pay'    => $this->input->post('basic_pay'),   
      // 'cansignature' => $updateArr['signatureplace_encrypted_filename'],  
    'date'         => $duuu, 
    'flag'         => 1,
    'createdon'    => date('Y-m-d H:i:s'),
    'createdby'    => $this->loginData->UserID,

  );

   $this->db->insert('membership_application_form', $insertArr);
     // echo $this->db->last_query(); die;
   $insertid = $this->db->insert_id();

   $insert_data =array (

    'type'      => 16,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
   $this->db->insert('tbl_workflowdetail', $insert_data);
   $this->session->set_flashdata('tr_msg', 'Successfully added  Membership Appliation');
   $this->session->set_flashdata('er_msg', $this->db->error());


   $subject = ': Membership application form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Membership application form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('candidate/Membership_applicatioin_from/previewemembership/'.$staff.'/'.$candidateid);


 }

}
prepareview:

$content['title'] = 'Membership_applicatioin_from';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('candidate/_main_layout', $content);
}
}


public function Add($staff,$candidate_id)
{
 
 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
 // redirect('/staff_dashboard/index');
  
} else {

 $staff        = $this->uri->segment(4);
 $candidate_id = $this->uri->segment(5);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;
 
 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidateid', $candidate_id);

 $staff_id     =$this->loginData->staffid;
 @$candidateid  =$this->loginData->candidate_id;

 if(isset($staff_id) && isset($candidateid))
 {

  $staff_id=$staff_id;
       // echo "staff id".$staff_id;
  $candidateid=$candidateid;
       // echo "candate id=".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}

$content['report']=$this->Membership_applicatioin_from_model->staff_reportingto($staff_id);
  //$candidateid=($this->loginData->candidateid);

  ///$staffids=($this->loginData->staffid);
$content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

$content['candidatedetailwithaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);

$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
$query332="SELECT * from tbldeclartion_about_family where candidate_id=$candidateid";
$content['list_staff_tbldeclartion_about_family_table']=$this->db->query($query332)->row();

if($content['list_staff_tbldeclartion_about_family_table'])
  $var=$content['list_staff_tbldeclartion_about_family_table']->flag;

else $var='';

if ($var==null)
{
  goto prepareviewss;
}

if($var==0)
{
  redirect('candidate/Membership_applicatioin_from/editdeclartion/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
  redirect('candidate/Membership_applicatioin_from/previewdeclartion/'.$staff.'/'.$candidateid);
}



prepareviewss:




$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();
$query3="SELECT b.name,b.emp_code,c.desname,d.officename FROM tbl_candidate_registration as a 
left JOIN staff as b on a.candidateid=b.candidateid 
LEFT JOIN msdesignation AS c ON b.designation = c.desid
LEFT JOIN lpooffice AS d ON d.staffid = b.staffid 
 WHERE b.candidateid =$candidateid"; //die;

 $content['getstaffdetails'] = $this->db->query($query3)->result()[0];

 
 $query5="SELECT a.relationwithemployee, a.candidateid,a.Familymembername, a.familydob, a.relationwithemployee, b.relationname FROM tbl_family_members AS a INNER JOIN sysrelation as b on b.id=a.relationwithemployee WHERE candidateid= $candidateid";
  //echo $query5;
 $content['members'] = $this->db->query($query5)->result();


        // start permission 
 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();


 $content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);
// end permission 

 $content['signature'] = $this->Common_Model->staff_signature($staff_id);

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){

  $this->form_validation->set_rules('placedec','Name','trim|required|min_length[1]|max_length[50]');


  if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }

  $candidatedt=$this->input->post('datess');

  $this->load->model('Membership_applicatioin_from_model');
  $duuu=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($candidatedt);


  $operation=$this->input->post('operation');

  if($operation == 0){

    $insertArr = array (

      'place'        => $this->input->post('placedec'),
      'datecan'      => $duuu,
      'candidate_id' => $candidateid,
      'flag'         => 0
    );


    $this->db->insert('tbldeclartion_about_family', $insertArr);
    $insertid = $this->db->insert_id();



    $updateArr = array (

      'weatherliving' =>$this->input->post('weather'),
      'remarks'       => $this->input->post('remarks'),

    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_family_members', $updateArr);

    //  $insertid = $this->db->insert_id();

    $insert_data = array (

      'type'      => 17,
      'r_id'      => $insertid,
      'sender'    => $staff_id,
      'receiver'  => $content['report']->reportingto,
      'senddate'  => date('Y-m-d'),
      'createdon' => date('Y-m-d H:i:s'),
      'createdby' => $this->loginData->UserID,
      'flag'      => 1,
      'staffid'   => $staff_id
    );
    $this->db->insert('tbl_workflowdetail', $insert_data);

    $this->session->set_flashdata('tr_msg', 'Successfully added Membership application form');
    $this->session->set_flashdata('er_msg', $this->db->error());

    redirect('candidate/Membership_applicatioin_from/editdeclartion/'.$staff.'/'.$candidateid);
  }


  else {



    $insertArr = array(
      'place'        => $this->input->post('placedec'),
      // 'signaturecan' => $updateArr['sigcan_encrypted_filename'],
      'datecan'      => $duuu,
      'candidate_id' => $candidateid,
      'flag'         => 1
    );

    $this->db->insert('tbldeclartion_about_family', $insertArr);
    $insertid = $this->db->insert_id();


    $updateArr = array (
      'weatherliving'   => $this->input->post('weather'),
      'remarks'         => $this->input->post('remarks')
    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_family_members', $updateArr);
     // $insertid = $this->db->insert_id();

    $insert_data = array (

      'type'      => 17,
      'r_id'      => $insertid,
      'sender'    => $staff_id,
      'receiver'  => $content['report']->reportingto,
      'senddate'  => date('Y-m-d'),
      'createdon' => date('Y-m-d H:i:s'),
      'createdby' => $this->loginData->UserID,
      'flag'      => 1,
      'staffid'   => $staff_id
    );
    $this->db->insert('tbl_workflowdetail', $insert_data);

    $this->session->set_flashdata('tr_msg', 'Successfully added Membership application form');
    $this->session->set_flashdata('er_msg', $this->db->error());


    $subject = ': Declaration about members of family form';
    $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Declaration about members of family form </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

    redirect('candidate/Membership_applicatioin_from/previewdeclartion/'.$staff.'/'.$candidateid);
  }


     // echo $this->db->last_query(); die;
    // $this->session->set_flashdata('tr_msg', 'Successfully added Membership application form');
    // $this->session->set_flashdata('er_msg', $this->db->error());

    // redirect('/Membership_applicatioin_from/add/');
}

prepareview:
$content['subview'] = 'Membership_applicatioin_from/add';
$this->load->view('candidate/_main_layout', $content);
}
}



public function edit($staff,$candidate_id)
{

  



 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //redirect('/staff_dashboard/index');
  
} else {
 $staff = $this->uri->segment(4);
 $candidate_id  = $this->uri->segment(5);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);







 $staff_id    = $this->loginData->staffid;
 $candidateid = $candidate_id;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id        =$staff_id;
  // echo "staff id".$staff_id;
  $candidateid     =$candidateid;
  // echo "candate id =".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}


 // $candidateid=($this->loginData->candidateid);

 // $staffids=($this->loginData->staffid);
$content['report']=$this->Membership_applicatioin_from_model->staff_reportingto($staff_id);
$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
$query332="SELECT * from tbl_for_enhanced where candidate_id=$candidateid";
$content['list_staff_tbl_for_enhanced_table']=$this->db->query($query332)->row();

if($content['list_staff_tbl_for_enhanced_table'])
 
  $var=$content['list_staff_tbl_for_enhanced_table']->flag;

else $var ='';

if ($var==null)
{
  goto prepareviewss;
}

if($var==0)
{
  redirect('candidate/Membership_applicatioin_from/editenhanced/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
  redirect('candidate/Membership_applicatioin_from/previewehanced/'.$staff.'/'.$candidateid);
}

prepareviewss:


        // start permission 
// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();
// end permission 

$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();
$content['data_application']=$this->Membership_applicatioin_from_model->fetchdatas($candidateid);
$content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

// print_r($content['candidatedetailwithaddress']); die;



$query5="SELECT a.relationwithemployee, a.candidateid, a.Familymembername, a.familydob, a.relationwithemployee, b.relationname, a.remarks, a.weatherliving,a.sex FROM tbl_family_members AS a INNER JOIN sysrelation AS b ON b.id = a.relationwithemployee WHERE candidateid= $candidateid";
$content['members'] = $this->db->query($query5)->result();

$content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);

$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$RequestMethod = $this->input->server('REQUEST_METHOD'); 


if($RequestMethod == 'POST'){

   // print_r($this->input->post()); die;

  // $this->form_validation->set_rules('placecandidate','Name','trim|required|min_length[1]|max_length[50]');


  // if($this->form_validation->run() == FALSE){
  //   $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
  //     '</div>');

  //   $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

  //   $hasValidationErrors    =    true;
  //   goto prepareview;

  // }
  $candidatedt=$this->input->post('txtname');
  $datepresence=$this->input->post('datecandidate');

  $this->load->model('Membership_applicatioin_from_model');

  $duuu=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($candidatedt);
  $dddd=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($datepresence);


  $operation=$this->input->post('operation');



  if($operation == 0){

    $insertArr = array (

      'wefdate'        => $duuu,
      'coverage1'      => $this->input->post('coverage1'),
      'coverage21'     => $this->input->post('coverage21'),
      'coverage22'     => $this->input->post('coverage22'),
      'coverage31'     => $this->input->post('coverage31'),
      'coverage32'     => $this->input->post('coverage32'),
      'place'          => $this->input->post('placecandidate'),
      'date'           => $dddd,
      // 'signaturecandd' => $updateArr['sigcans_encrypted_filename'],
      'candidate_id'   => $candidateid,
      'flag'           => 0
    );



    $this->db->insert('tbl_for_enhanced', $insertArr);
     // echo $this->db->last_query(); die;
    $insertid = $this->db->insert_id();

    $insert_data = array (

      'type'      => 18,
      'r_id'      => $insertid,
      'sender'    => $staff_id,
      'receiver'  => $content['report']->reportingto,
      'senddate'  => date('Y-m-d'),
      'createdon' => date('Y-m-d H:i:s'),
      'createdby' => $this->loginData->UserID,
      'flag'      => 1,
      'staffid'   => $staff_id
    );
    $this->db->insert('tbl_workflowdetail', $insert_data);
    $this->session->set_flashdata('tr_msg', 'Successfully added ehancement');
    redirect('candidate/Membership_applicatioin_from/editenhanced/'.$staff.'/'.$candidateid);


  } else {


  // print_r($this->input->post()); die;

   $insertArr = array (

    'wefdate'        => $duuu,
    'coverage1'      => $this->input->post('coverage1'),
    'coverage21'     => $this->input->post('coverage21'),
    'coverage22'     => $this->input->post('coverage22'),
    'coverage31'     => $this->input->post('coverage31'),
    'coverage32'     => $this->input->post('coverage32'),
    'place'          => $this->input->post('placecandidate'),
    'date'           => $dddd,
    // 'signaturecandd' => $updateArr['sigcans_encrypted_filename'],
    'candidate_id'   => $candidateid,
    'flag'           => 1
  );

   $this->db->insert('tbl_for_enhanced', $insertArr);
   $insertid = $this->db->insert_id();

   $insert_data = array (

    'type'      => 18,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
   $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully added ehancement');
   $this->session->set_flashdata('er_msg', $this->db->error());


   $subject = ': GMP Enhanced form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  GMP Enhanced form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('candidate/Membership_applicatioin_from/previewehanced/'.$staff.'/'.$candidateid);

 }

}

prepareview:


$content['subview'] = 'Membership_applicatioin_from/edit';
$this->load->view('candidate/_main_layout', $content);
}
}


public function editenhanced($staff,$candidate_id)
{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
 // redirect('/staff_dashboard/index');
  
} else {

 $staff = $this->uri->segment(4);
 $candidate_id  = $this->uri->segment(5);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);







 $staff_id=$this->loginData->staffid;
 $candidateid=$this->loginData->candidateid;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
        //echo "staff id".$staff_id;
  $candidateid=$candidateid;
       // echo "candate id=".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}


  //$candidateid=($this->loginData->candidateid);

 // $staffids=($this->loginData->staffid);
$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
        // start permission 
// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();
// end permission 

$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();



  // $query3="SELECT b.emp_code FROM tbl_candidate_registration as a left JOIN staff as b on a.candidateid=b.candidateid WHERE a.candidateid = $candidateid ";

  // $content['emp_code'] = $this->db->query($query3)->result();

  // $qry="SELECT a.desname, d.officename FROM msdesignation AS a LEFT JOIN staff AS b ON a.desid = b.designation LEFT JOIN lpooffice AS d ON d.staffid = b.staffid WHERE b.candidateid =$candidateid
  // ";
  // $content['lpo_office']=$this->db->query($qry)->result();


  // $query5="select a.candidatefirstname, a.candidatelastname, a.bloodgroup from tbl_candidate_registration as a WHERE a.candidateid= $candidateid";

  // $content['bloodgroup'] = $this->db->query($query5)->result();
$content['data_application']=$this->Membership_applicatioin_from_model->fetchdatas($candidateid);
$content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);



$query5="SELECT a.relationwithemployee, a.candidateid, a.Familymembername, a.familydob, a.relationwithemployee, b.relationname, a.remarks, a.weatherliving FROM tbl_family_members AS a INNER JOIN sysrelation AS b ON b.id = a.relationwithemployee WHERE candidateid= $candidateid";
$content['members'] = $this->db->query($query5)->result();

$query6="SELECT * from tbl_for_enhanced where candidate_id=$candidateid";
$content['fetch'] = $this->db->query($query6)->row();
// print_r($content['fetch']);
$content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);

$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$RequestMethod = $this->input->server('REQUEST_METHOD'); 


if($RequestMethod == 'POST'){

  // $this->form_validation->set_rules('placecandidate','Name','trim|required|min_length[1]|max_length[50]');


  // if($this->form_validation->run() == FALSE){
  //   $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
  //     '</div>');

  //   $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

  //   $hasValidationErrors    =    true;
  //   goto prepareview;

  // }




  $candidatedt=$this->input->post('txtname');
  $datepresence=$this->input->post('datecandidate');

  $this->load->model('Membership_applicatioin_from_model');

  $duuu=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($candidatedt);
  $dddd=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($datepresence);


  $operation=$this->input->post('operation');

  if($operation == 0 ){

    $updateArr = array (

      'wefdate'        => $duuu,
      'coverage1'      => $this->input->post('coverage1'),
      'coverage21'     => $this->input->post('coverage21'),
      'coverage22'     => $this->input->post('coverage22'),
      'coverage31'     => $this->input->post('coverage31'),
      'coverage32'     => $this->input->post('coverage32'),
      'place'          => $this->input->post('placecandidate'),
      'date'           => $dddd,
      // 'signaturecandd' => $updateArr['sigcans_encrypted_filename'],
      'candidate_id'   => $candidateid,
      'flag'           => 0
    );

    $this->db->where('candidate_id', $candidateid);
    $this->db->update('tbl_for_enhanced', $updateArr);
    $this->session->set_flashdata('tr_msg', 'Successfully updated ehancement ');
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect('candidate/Membership_applicatioin_from/editenhanced/'.$staff.'/'.$candidateid);


  } else {


   $updateArr = array (

    'wefdate'        => $duuu,
    'coverage1'      => $this->input->post('coverage1'),
    'coverage21'     => $this->input->post('coverage21'),
    'coverage22'     => $this->input->post('coverage22'),
    'coverage31'     => $this->input->post('coverage31'),
    'coverage32'     => $this->input->post('coverage32'),
    'place'          => $this->input->post('placecandidate'),
    'date'           => $dddd,
    // 'signaturecandd' => $updateArr['sigcans_encrypted_filename'],
    'candidate_id'   => $candidateid,
    'flag'           => 1

  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_for_enhanced', $updateArr);

   $this->session->set_flashdata('tr_msg', 'Successfully updated ehancement');
   $this->session->set_flashdata('er_msg', $this->db->error());


   $subject = ': GMP Enhanced form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  GMP Enhanced form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('candidate/Membership_applicatioin_from/previewehanced/'.$staff.'/'.$candidateid);
 }


}

prepareview:


$content['subview'] = 'Membership_applicatioin_from/editenhanced';
$this->load->view('candidate/_main_layout', $content);
}
}


public function previewehanced($staff,$candidate_id)
{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
 // redirect('/staff_dashboard/index');
  
} else {

  $staff = $this->uri->segment(4);
  $candidate_id  = $this->uri->segment(5);

  $content['staff'] = $staff;
  $content['candidate_id'] = $candidate_id;

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);


  $staff_id=$staff;
  $candidateid=$candidate_id;
  $login_staff=$this->loginData->staffid;

  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
        //echo "staff id".$staff_id;
    $candidateid=$candidateid;
        //echo "candate id=".$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff;
   $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

 }


 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();

 $content['data_application']=$this->Membership_applicatioin_from_model->fetchdatas($candidateid);


 $content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);


 $content['getstaffempcodedes'] = $this->Membership_applicatioin_from_model->getstaffempcodedes($staff_id);

 
 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
 $query1="Select * from tbl_table_office";
 $content['officename_list']=$this->db->query($query1)->row();





 $query5="SELECT a.relationwithemployee, a.candidateid, a.Familymembername, a.familydob, a.relationwithemployee, b.relationname, a.remarks, a.weatherliving ,a.sex FROM tbl_family_members AS a INNER JOIN sysrelation AS b ON b.id = a.relationwithemployee WHERE candidateid= $candidateid";
 $content['members'] = $this->db->query($query5)->result();

 $query6="SELECT * from tbl_for_enhanced where candidate_id=$candidateid";
 $content['fetch'] = $this->db->query($query6)->row();

 $content['id'] = $this->Membership_applicatioin_from_model->get_enhancedworkflowid($staff_id);
 // print_r($content['id']);
 $content['personal']=$this->Common_Model->get_Personnal_Staff_List();
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);

 $content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);
 $content['reporting'] = $this->Membership_applicatioin_from_model->getCandidateWith($reportingto);

 $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;
if(!empty($this->loginData->RoleID))
{
 if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
 {
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST'){
                              // print_r($_POST);die;
    $status=$this->input->post('status');
    $reject=$this->input->post('reject');
    $p_status=$this->input->post('p_status');
    $r_id=$this->input->post('id');

    
    if($status==2){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    }
    
    $insert_data = array (

      'type'                 => 18,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $p_status,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );

          //$arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 

        //print_r($insert_data);
        //die();


    $this->db->insert('tbl_workflowdetail', $insert_data);


    $subject = ': GMP Enhanced form';
    $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email => 'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('candidate/Membership_applicatioin_from/previewehanced/'.$staff.'/'.$candidateid);


  }


}
else 
 if($this->loginData->RoleID==17)
 {
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST'){
                              // print_r($_POST);die;
    $status=$this->input->post('status');
    $reject=$this->input->post('reject');
    $p_status=$this->input->post('p_status');
                               // $wdtaff1=$this->input->post('wdtaff1');
                               // $wdtaff2=$this->input->post('wdtaff2');
    $r_id=$this->input->post('id');

                                //$check = $this->session->userdata('insert_id');
    if($status==4){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    }


    $insert_data = array (

      'type'                 => 18,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $staff_id,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );

          //$arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 

        //print_r($insert_data);
        //die();


    $this->db->insert('tbl_workflowdetail', $insert_data);



    $subject = ': GMP Enhanced form';
    $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel</h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name ='';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email => 'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('candidate/Membership_applicatioin_from/previewehanced/'.$staff.'/'.$candidateid);



  }
}

}

$content['subview'] = 'Membership_applicatioin_from/previewehanced';
$this->load->view('candidate/_main_layout', $content);
}
}




public function previewemembership($staff,$candidate_id)
{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //redirect('/staff_dashboard/index');
  
} else {

 $staff = $this->uri->segment(4);
 $candidate_id  = $this->uri->segment(5);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);

 
 $staff_id    = $staff;
 $candidateid = $candidate_id;
 $login_staff = $this->loginData->staffid;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id        = $staff_id;
  // echo "staff id".$staff_id;
  $candidateid     = $candidateid;
  // echo "candate id =".$candidateid;

}
else
{
 $staff_id=$staff;
         // echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}




// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();



 // $staffids=($this->loginData->staffid);
 // $candidateid=($this->loginData->candidateid);
 // $candidateid=($this->loginData->candidateid);

 // $staffids=($this->loginData->staffid);

$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

    // start permission 
// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();
// end permission 

$content['data_application']=$this->Membership_applicatioin_from_model->fetchdatas($candidateid);
$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();

$content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

$content['candidatedetailwithaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
// print_r($content['candidatedetailwithaddress']); die;

  // $qry="SELECT a.desname, d.officename FROM msdesignation AS a LEFT JOIN staff AS b ON a.desid = b.designation LEFT JOIN lpooffice AS d ON d.staffid = b.staffid WHERE b.candidateid =$candidateid
  // ";
  // $content['lpo_office']=$this->db->query($qry)->result();


  // $query3="SELECT b.emp_code FROM tbl_candidate_registration as a left JOIN staff as b on a.candidateid=b.candidateid WHERE a.candidateid = $candidateid ";

  // $content['emp_code'] = $this->db->query($query3)->result();

  // $query5="select a.candidatefirstname, a.candidatelastname, a.bloodgroup from tbl_candidate_registration as a WHERE a.candidateid= $candidateid";

  // $content['bloodgroup'] = $this->db->query($query5)->result();

$query6="select * from membership_application_form where candidate_id=$candidateid";
$content['fetch_datass'] = $this->db->query($query6)->row();

$content['personal']=$this->Common_Model->get_Personnal_Staff_List();
$content['id'] = $this->Membership_applicatioin_from_model->get_memberworkflowid($staff_id);

$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);
$content['reporting'] = $this->Membership_applicatioin_from_model->getCandidateWith($reportingto);

$personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;
// print_r($content['id']); die;

if(!empty($this->loginData->RoleID))
{
if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST')
 {
                               // print_r($_POST);die;
  $status   = $this->input->post('status');
  $reject   = $this->input->post('reject');
  $p_status = $this->input->post('p_status');
  $wdtaff1  = $this->input->post('wdtaff1');
  $wdtaff2  = $this->input->post('wdtaff2');
  $r_id     = $this->input->post('id');
  
  $check    = $this->session->userdata('insert_id');

  if($status==2){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }

 // echo $p_status; die;

  $insert_data = array (

    'type'                 => 16,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $p_status,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );

  $arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 




  $this->db->insert('tbl_workflowdetail', $insert_data);



  $subject = ': Membership application form';
  $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc </h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
  $to_name ='';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email => 'personal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('candidate/Membership_applicatioin_from/previewemembership/'.$staff.'/'.$candidateid);



}


}
else   if($this->loginData->RoleID==17)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST')
 {
                               // print_r($_POST);die;
  $status   = $this->input->post('status');
  $reject   = $this->input->post('reject');
  $p_status = $this->input->post('p_status');
  $wdtaff1  = $this->input->post('wdtaff1');
  $wdtaff2  = $this->input->post('wdtaff2');
  $r_id     = $this->input->post('id');
  $check    = $this->session->userdata('insert_id');

  if($status==4){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }

  $insert_data = array (

    'type'                 => 16,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );

  $arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 




  $this->db->insert('tbl_workflowdetail', $insert_data);




  $subject = ': Membership application form';
  $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel</h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
  $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email =>'personal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('candidate/Membership_applicatioin_from/previewemembership/'.$staff.'/'.$candidateid);



}


}
}



$content['subview'] = 'Membership_applicatioin_from/previewemembership';
$this->load->view('candidate/_main_layout', $content);
}
}







public function editmembership($staff,$candidate_id)
{
 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  redirect('/staff_dashboard/index');
  
} else {
 $staff = $this->uri->segment(4);
 $candidate_id  = $this->uri->segment(5);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);







 $staff_id=$this->loginData->staffid;
 $candidateid=$this->loginData->candidateid;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
      //  echo "staff id".$staff_id;
  $candidateid=$candidateid;
       // echo "candate id=".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}

 //$candidateid=($this->loginData->candidateid);

 //$staffids=($this->loginData->staffid);
$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
$content['data_application']=$this->Membership_applicatioin_from_model->fetchdatas($candidateid);

$content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

$content['candidatedetailwithaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
    // start permission 
// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();
// end permission 


$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();
$query6="select * from membership_application_form where candidate_id=$candidateid";
$content['fetch_datass'] = $this->db->query($query6)->row();

$content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);
 //print_r($content['fetch_datass']);
$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$RequestMethod = $this->input->server('REQUEST_METHOD'); 

if($RequestMethod == 'POST'){
 $this->form_validation->set_rules('rupees','PG Name','trim|required|min_length[1]|max_length[50]');





 $du=$this->input->post('datess');
 $this->load->model('Membership_applicatioin_from_model');
 $duuu=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($du);

 $operation=$this->input->post('operation');

 if($operation == 0){

   $updateArr = array (

    'rupees'       => $this->input->post('rupees'),  
    'candidate_id' => $candidateid,
    'month'        => $this->input->post('month'),  
    'year'         => $this->input->post('years'),  
    'cansignature' => $updateArr['signatureplace_encrypted_filename'],  
    'date'         => $duuu, 
    'flag'         => 0,
    'updatedon'    => date('Y-m-d H:i:s'),
    'updatedby'    => $this->loginData->UserID,

  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('membership_application_form', $updateArr);
   $this->session->set_flashdata('tr_msg', 'Successfully added   Membership Appliation');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('candididate/Membership_applicatioin_from/editmembership/'.$staff.'/'.$candidateid);


 } else {

   $updateArr = array (

    'rupees'       => $this->input->post('rupees'),  
    'candidate_id' => $candidateid,
    'month'        => $this->input->post('month'),  
    'year'         => $this->input->post('years'),  
    'cansignature' => $updateArr['signatureplace_encrypted_filename'],  
    'date'         => $duuu, 
    'flag'         => 1,
    'updatedon'    => date('Y-m-d H:i:s'),
    'updatedby'    => $this->loginData->UserID,

  );



   $this->db->where('candidate_id', $candidateid);
   $this->db->update('membership_application_form', $updateArr);
   $this->session->set_flashdata('tr_msg', 'Successfully added  Membership Appliation');
   $this->session->set_flashdata('er_msg', $this->db->error());


   $subject = ': Membership application form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited   Membership application form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('candidate/Membership_applicatioin_from/previewemembership/'.$staff.'/'.$candidateid);


 }

}
prepareview:

$content['title'] = 'Membership_applicatioin_from';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('candidate/_main_layout', $content);
}
}

public function editdeclartion($staff,$candidate_id)
{
 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //redirect('/staff_dashboard/index');
  
} else {

  $staff = $this->uri->segment(4);
  $candidate_id  = $this->uri->segment(5);

  $content['staff'] = $staff;
  $content['candidate_id'] = $candidate_id;

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);







  $staff_id=$this->loginData->staffid;
  $candidateid=$candidate_id;

  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
        //echo "staff id".$staff_id;
    $candidateid=$candidateid;
        //echo "candate id=".$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff;
   $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

 }


// $candidateid=($this->loginData->candidateid);

 //$staffids=($this->loginData->staffid);
 $content['candidatedetailwithaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
 // print_r($content['candidatedetailwithaddress']); die;
 $content['report']=$this->Membership_applicatioin_from_model->staff_reportingto($staff_id);
 $query1="Select * from tbl_table_office";
 $content['officename_list']=$this->db->query($query1)->row();

 // $query3="SELECT b.emp_code FROM tbl_candidate_registration as a left JOIN staff as b on a.candidateid=b.candidateid WHERE a.candidateid = $candidateid ";

 // $content['emp_code'] = $this->db->query($query3)->result();

 // $qry="SELECT a.desname, d.officename FROM msdesignation AS a LEFT JOIN staff AS b ON a.desid = b.designation LEFT JOIN lpooffice AS d ON d.staffid = b.staffid WHERE b.candidateid =$candidateid
 // ";
 // $content['lpo_office']=$this->db->query($qry)->result();


 // $query5="select a.candidatefirstname, a.candidatelastname, a.bloodgroup from tbl_candidate_registration as a WHERE a.candidateid= $candidateid";

 // $content['bloodgroup'] = $this->db->query($query5)->result();



 $query5="SELECT a.remarks,a.weatherliving , a.relationwithemployee, a.candidateid,a.Familymembername, a.familydob, a.relationwithemployee, b.relationname FROM tbl_family_members AS a INNER JOIN sysrelation as b on b.id=a.relationwithemployee WHERE candidateid= $candidateid";
 $content['members'] = $this->db->query($query5)->result();
 // print_r($content['members']); die;
 $query3="SELECT b.name,b.emp_code,c.desname,d.officename FROM tbl_candidate_registration as a 
 left JOIN staff as b on a.candidateid=b.candidateid 
 LEFT JOIN msdesignation AS c ON b.designation = c.desid
 LEFT JOIN lpooffice AS d ON d.staffid = b.staffid 
 WHERE b.candidateid =$candidateid"; //die;

 $content['getstaffdetails'] = $this->db->query($query3)->result()[0];


 $query6="select * from tbldeclartion_about_family where candidate_id=$candidateid";
 $content['newfetch'] = $this->db->query($query6)->row();
 // print_r($content['newfetch']); die;


        // start permission 
 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();

 $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);
// end permission 

 $content['signature'] = $this->Common_Model->staff_signature($staff_id);

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){

  $this->form_validation->set_rules('placedec','Name','trim|required|min_length[1]|max_length[50]');


  if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }




  
  $candidatedt=$this->input->post('datess');
  $this->load->model('Membership_applicatioin_from_model');
  $duuu=$this->Membership_applicatioin_from_model->getCandidateDetailsPreview($candidatedt);


  $operation=$this->input->post('operation');

  if($operation == 0){

    $updateArrss = array (

      'place'        => $this->input->post('placedec'),
      // 'signaturecan' => $updateArr['sigcan_encrypted_filename'],
      'datecan'      => $duuu,
      'candidate_id' => $candidateid,
      'flag'         => 0
    );

    $this->db->where('candidate_id', $candidateid);
    $this->db->update('tbldeclartion_about_family', $updateArrss);
    //$this->db->insert('tbldeclartion_about_family', $insertArr);


    $updateArr = array (

      'weatherliving' => $this->input->post('weather'),
      'remarks'       => $this->input->post('remarks'),

    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_family_members', $updateArr);



    $this->session->set_flashdata('tr_msg', 'Successfully added Membership application form');
    $this->session->set_flashdata('er_msg', $this->db->error());

    redirect('candidate/Membership_applicatioin_from/editdeclartion/'.$staff.'/'.$candidateid);
  }


  else {



    $updateArrssssss = array (

      'place'        => $this->input->post('placedec'),
      // 'signaturecan' => $updateArr['sigcan_encrypted_filename'],
      'datecan'      => $duuu,
      'candidate_id' => $candidateid,
      'flag'         => 1
    );

   // $this->db->insert('tbldeclartion_about_family', $insertArr);
    $this->db->where('candidate_id', $candidateid);
    $this->db->update('tbldeclartion_about_family', $updateArrssssss);


    $updateArr = array (

      'weatherliving' => $this->input->post('weather'),
      'remarks'       => $this->input->post('remarks')
    );

    $this->db->where('candidateid', $candidateid);
    $this->db->update('tbl_family_members', $updateArr);

    $this->session->set_flashdata('tr_msg', 'Successfully added Membership application form');
    $this->session->set_flashdata('er_msg', $this->db->error());


    $subject = ': Declaration about members of family form';
    $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Declaration about members of family form </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

    redirect('candidate/Membership_applicatioin_from/previewdeclartion/'.$staff.'/'.$candidateid);
  }


     // echo $this->db->last_query(); die;
    // $this->session->set_flashdata('tr_msg', 'Successfully added Membership application form');
    // $this->session->set_flashdata('er_msg', $this->db->error());

    // redirect('/Membership_applicatioin_from/add/');
}

prepareview:
$content['subview'] = 'Membership_applicatioin_from/editdeclartion';
$this->load->view('candidate/_main_layout', $content);
}
}


public function previewdeclartion($staff,$candidate_id)
{
 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  //redirect('/staff_dashboard/index');
  
} else {

  $staff = $this->uri->segment(4);
  $candidate_id  = $this->uri->segment(5);

  $content['staff'] = $staff;
  $content['candidate_id'] = $candidate_id;

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);

  $staff_id    = $staff;
  $candidateid = $candidate_id;
  $login_staff = $this->loginData->staffid;

  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id        = $staff_id;
    $candidateid     = $candidateid;
  }
  else
  {
   $staff_id=$staff;
   $candidateid=$candidate_id;
 }


 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

 $query1="Select * from tbl_table_office";
 $content['officename_list']=$this->db->query($query1)->result()[0];

 $query3="SELECT b.name,b.emp_code,c.desname,d.officename FROM tbl_candidate_registration as a 
 left JOIN staff as b on a.candidateid=b.candidateid 
 LEFT JOIN msdesignation AS c ON b.designation = c.desid
 LEFT JOIN lpooffice AS d ON d.staffid = b.staffid 
 WHERE b.candidateid =$candidateid"; //die;

 $content['getstaffdetails'] = $this->db->query($query3)->result()[0];

 $qry="SELECT a.desname, d.officename FROM msdesignation AS a 
 LEFT JOIN staff AS b ON a.desid = b.designation 
 LEFT JOIN lpooffice AS d ON d.staffid = b.staffid WHERE b.candidateid =$candidateid
 ";
 $content['lpo_office']=$this->db->query($qry)->row();

 $content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
 $query5="select a.candidatefirstname, a.candidatelastname, a.bloodgroup from tbl_candidate_registration as a WHERE a.candidateid= $candidateid";

 $content['familydata'] = $this->Membership_applicatioin_from_model->familydata($candidateid);

 $query6="select * from tbldeclartion_about_family where candidate_id=$candidateid";
 $content['newfetch'] = $this->db->query($query6)->row();

 $content['bloodgroup'] = $this->db->query($query5)->result();



 $query5="SELECT a.remarks,a.weatherliving , a.relationwithemployee, a.candidateid,a.Familymembername, a.familydob, a.relationwithemployee, b.relationname FROM tbl_family_members AS a INNER JOIN sysrelation as b on b.id=a.relationwithemployee WHERE candidateid= $candidateid";
 $content['members'] = $this->db->query($query5)->result();



 $query6="select * from tbldeclartion_about_family where candidate_id=$candidateid";
 $content['newfetch'] = $this->db->query($query6)->row();
 //print_r($content['newfetch']);


        // start permission 
 // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 // $content['role_permission'] = $this->db->query($query)->result();
// end permission 
 $content['id'] = $this->Membership_applicatioin_from_model->get_familyworkflowid($staff_id);
      // print_r($content['id']);

 $content['candidateaddress'] = $this->Membership_applicatioin_from_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Membership_applicatioin_from_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Membership_applicatioin_from_model->tc_email($reportingto);
 $content['reporting'] = $this->Membership_applicatioin_from_model->getCandidateWith($reportingto);
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);

 $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;
 

 $content['personal']=$this->Common_Model->get_Personnal_Staff_List();
 if(!empty($this->loginData->RoleID))
 {
 if($this->loginData->RoleID==2)
 {
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST')
   {
                               // print_r($_POST);die;
    $status   = $this->input->post('status');
    $reject   = $this->input->post('reject');
    $p_status = $this->input->post('p_status');
    $wdtaff1  = $this->input->post('wdtaff1');
    $wdtaff2  = $this->input->post('wdtaff2');
    $r_id     = $this->input->post('id');
    $check    = $this->session->userdata('insert_id');


    if($status==2){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    } 

    $insert_data = array (

      'type'                 => 17,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $p_status,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );

    $this->db->insert('tbl_workflowdetail', $insert_data);

    $insertArrdata = array (

      'supervisorname' => $login_staff,
      'supervisordate' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('supervisor_date')),
    );
    $this->db->where('id',$r_id);
    $this->db->update('tbldeclartion_about_family',$insertArrdata);

    $subject = ': Declaration about members of family form';
    $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email => 'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('/Membership_applicatioin_from/previewdeclartion/'.$staff.'/'.$candidateid);

  }


}
else if($this->loginData->RoleID==17)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST')
 {
                               // print_r($_POST);die;
  $status   = $this->input->post('status');
  $reject   = $this->input->post('reject');
  $p_status = $this->input->post('p_status');
  $wdtaff1  = $this->input->post('wdtaff1');
  $wdtaff2  = $this->input->post('wdtaff2');
  $r_id     = $this->input->post('id');
  $check    = $this->session->userdata('insert_id');

  if($status==4){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }


  $insert_data = array (

    'type'                                        => 17,
    'r_id'                                        => $r_id,
    'sender'                                      => $login_staff,
    'receiver'                                    => $staff_id,
    'senddate'                                    => date('Y-m-d'),
    'createdon'                                   => date('Y-m-d H:i:s'),
    'createdby'                                   => $login_staff,
    'scomments'                                   => $reject,
    'forwarded_workflowid'                        => $content['id']->workflow_id,
    'flag'                                        => $status,
    'staffid'                                     => $staff_id
  );
  $this->db->insert('tbl_workflowdetail', $insert_data);

  $insertArrdata = array (

    'financename' => $login_staff,
    'financedate' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('personal_date')),
  );
  $this->db->where('id',$r_id);
  $this->db->update('tbldeclartion_about_family',$insertArrdata);

  
  $subject = ': Declaration about members of family form';
  $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel</h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email => 'personal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('candidate/Membership_applicatioin_from/previewdeclartion/'.$staff.'/'.$candidateid);

}

}
}

$content['subview'] = 'Membership_applicatioin_from/previewdeclartion';
$this->load->view('candidate/_main_layout', $content);
}
}

public function previewtmtt()
{
 $staff = $this->uri->segment(3);
 $candidate_id  = $this->uri->segment(4);

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);

 $staff_id=$this->loginData->staffid;
 $candidateid=$this->loginData->candidateid;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
       // echo "staff id".$staff_id;
  $candidateid=$candidateid;
        //echo "candate id=".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}



// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();

  //$candidateid=($this->loginData->candidateid);


$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();

$this->load->model('Membership_applicatioin_from_model');
$content['fetch_datass'] = $this->Membership_applicatioin_from_model->fetchpdfdata($candidateid);
$row=$content['fetch_datass'];
$rows=$content['officename_list'];


if ($row->encrypted_signature !='') {
        $staffsign = site_url('datafiles/signature/'.$row->encrypted_signature);
      }else{ 
      // $staffsign = 'C:/xampp/htdocs/pradanhrm/datafiles/imagenotfound.jpg';
             $staffsign = site_url('datafiles/imagenotfound.jpg');
        }

$timestamp = strtotime($row->date);
$vt=date('d/m/Y', $timestamp);


$candidate = array('$rowsofficename','$rowmonth','$rowyear','$rowrupees','$rowcandidatefirstname','$rowcandidatelastname','$rowdesname','$vt','$rowemp_code','$staffsign');
$candidate_replace = array($rows->officename,$row->month,$row->year,$row->rupees,$row->candidatefirstname,$row->candidatelastname,$row->desname,$vt,$row->emp_code,$staffsign);


// $html='<div class="container">

// <div class="row clearfix">
// <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
// <div class="card">
// <div class="body">
// <div class="row clearfix doctoradvice">
// <h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME
// <br><br> Preview: MEMBERSHIP APPLICATION FORM</h4>
// <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
// <form method="POST" action=""  enctype="multipart/form-data">
// <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
// <div class="form-group">
// <div class="form-line">
// <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
// PRADAN.</label>
// <br><b>

// '. $rows->officename.'


// </b>
// </div>

// </div>
// </div>
// <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
// </div>
// <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

// </div>
// <br>
// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
// <div class="form-group">
// <label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir / Madam, </label>
// </div>
// </div>
// <br>

// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
// <div class="form-group">
// I, Ms./Mr.&nbsp;<b>'.$row->candidatefirstname.'</b>&nbsp;<b>'. $row->candidatelastname.'</b>&nbsp;(Name),&nbsp;<b>'.$row->emp_code.'</b>&nbsp;

// (Employee Code),based at&nbsp;<b>'.$row->officename.'</b>&nbsp;(Location) with a basic pay of Rs
// would like to become a member of the Employees’ Contributory Welfare Scheme with effect from  
// <b>'.$row->month.'</b>
// (Month),
// 20
// <b>'.$row->year.'  
// </b>
// (Year). I agree to contribute Rs&nbsp;
// <b>'.$row->rupees.'</b> per month as 
// provided for in
// the
// Scheme 
// and  authorize PRADAN to deduct this amount
// from my salary every month.


// I agree to abide by all the rules and regulations and confirm to the provisions specified in the Scheme.
// <br>
// <br>
// The details about the members of my family to be included under this Scheme are as under:
// <br><br>Yours faithfully,
// <br><br>


// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
// <div class="form-group">
// Signature:


// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
// <div class="form-group">


// <br>
// Place:<b>'.$row->officename.'</b>&nbsp; 
// Employee Code:<b>'.$row->emp_code.'</b>&nbsp;

// Name:<b>'.$row->candidatefirstname.'</b>&nbsp;<b>'.$row->candidatelastname.'</b>&nbsp; Date:   
// <b>
// '.$vt.'
// </b>
// Designation:
// <b>'.$row->desname.'</b>&nbsp;

// </div>
// </div>   
// <p>
// cc: - Finance-Personnel-MIS Unit
// <br>
// &emsp;&emsp;- Personal Dossier (Location)
// </p>


// </div>
// </div>
// </div>
// </div>
// </form>
// </div>
// </div>
// </div>
// </div>
// </div>
// </div>

// ';

$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 22 AND `isactive` = '1'";
$data = $this->db->query($sql)->row();
if(!empty($data))
  $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
$body=  html_entity_decode($body); 
$filename = "".$token."applications";
$this->Employee_particular_form_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf'); 

}




public function pdfofdecc($staff=null,$candidate_id=null)
{

 echo $staff = $this->uri->segment(3);
 echo  $candidate_id  = $this->uri->segment(4);

 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);

 $staff_id=$this->loginData->staffid;
 $candidateid=$this->loginData->candidateid;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id        = $staff_id;
  echo "staff id".$staff_id;
  $candidateid     = $candidateid;
  echo "candate id =".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}







 //$candidateid=($this->loginData->candidateid);

$this->load->model('Membership_applicatioin_from_model');
$content['fetch_datass'] = $this->Membership_applicatioin_from_model->fetchsssww($candidateid);

$row=$content['fetch_datass'];
//$rows=$content['officename_list'];

$html='<div class="container">


<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="body">
<div class="row clearfix doctoradvice">
<h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME
<br><br> Preview :DECLARATION ABOUT MEMBERS OF FAMILY</h4>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
<form method="POST" action="" enctype="multipart/form-data">
<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="form-group">
<div class="form-line">
<label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
PRADAN.</label>
<br>


</div>
<?php echo form_error("executive_director");?>
</div>
</div>
<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
</div>
<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

</div>

<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="form-group">
<label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir / Madam, </label>
</div>
</div>


<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
<div class="form-group">
I have enrolled myself as a member of PRADAN’s Employees Contributory Welfare Scheme (ECWS) and hereby declare that the following members of my family* (including <br><br> myself) shall be the beneficiaries under this Scheme:.

<br>
<p>* Family means:
<br>- employee her/himself<br>- spouse;<br>- minor, natural or adopted child, dependent upon the employee;<br>- child who has no income of her/his own and:<br>(i) is less than 21 years of age<br>(ii) unmarried dependent daughter(s);<br>- child who is infirm by reason of any physical and/or mental abnormality or injury, as certified by a competent medical authority, and is wholly dependent on the employee, as long as the infirmity continues;<br>- dependent parents.
</p>

<br>
Reimbursement of medical expenses, as above, will be available to the employee her/himself, spouse and the dependent children as defined above. However, if the total number<br><br> of family members (self, spouse and children) is less than four, the employee will be permitted to add her/his parent(s) as beneficiary(ies) so that the total number of members of <br><br> the family (including self) does not exceed four.<br><br>




<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<table id="tblForm09" class="table table-bordered table-striped dt-responsive" >
<thead>
<tr>
<th colspan="9" ><b> Family Details </b></th>
</tr> 
<tr>
<th>Sr No.</th>
<th>Name</th>
<th>Date of Birth</th>
<th>Age (in years)</th>
<th>Relationship</th>
<th>Whether Living With Me(Yes/No)</th>
<th>Remarks</th>
</tr>




</thead>

</table>
</div>
</div>

<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
<div class="form-group">
I hereby certify that the information provided above is true and correct. I agree to notify any change in the above particulars as laid down in the Scheme.
<br><br>
Yours faithfully,
</div>
</div>

<div class="row" style="margin-left: 5px;margin-right: 5px;">
<p style="float:left;">Place: <b>'.$row->place.'</b></p>
<p style="float:right;">Signature: </p>


</div><br>

<div class="row">
<p>Date:  <b>
'.$row->datecan.'

</b></p>
</p>
<p style="float:right;">Name:&nbsp;<b>'.$row->candidatefirstname.'</b>&nbsp;<b>'. $row->candidatelastname.'</b></p>
</div>



<div class="row" style="margin-right: 5px;">

<p style="float:right;">Designation:&nbsp;<b>'.$row->desname.'</b></p>
<br>
</div>
<div class="row" style="margin-right: 5px;">


<p>Employee Code:&nbsp;<b>'.$row->emp_code.'</b>
</div>
<br>  
</form>
</div>



</div>

</div>
</div>
</div>
</div>

</div>

';

$filename = "".$token."applications";
$this->Employee_particular_form_model->generatePDF($html, $filename, NULL,'Generateofferletter.pdf');

}

public function pdfeanced($staff=null,$candidate_id=null)
{

 //$candidateid=($this->loginData->candidateid);
 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);


 $staff_id    = $this->loginData->staffid;
 $candidateid = $this->loginData->candidateid;

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

}






$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();
$office=$content['officename_list'];

$query5="SELECT a.relationwithemployee, a.candidateid, a.Familymembername, a.familydob, a.relationwithemployee, b.relationname, a.remarks, a.weatherliving FROM tbl_family_members AS a INNER JOIN sysrelation AS b ON b.id = a.relationwithemployee WHERE candidateid= $candidateid";
$content['members'] = $this->db->query($query5)->row();
$table=$content['members'];


$this->load->model('Membership_applicatioin_from_model');
$row       = $this->Membership_applicatioin_from_model->fetchpdfehanced($candidateid);
$var       = $row->coverage1==1?'YES':'NO';
$var1      = $row->coverage21==1?'YES':'NO';
$var2      = $row->coverage22==1?'YES':'NO';
$var3      = $row->coverage31==1?'YES':'NO';
$var4      = $row->coverage32==1?'YES':'NO';

$timestamp = strtotime($row->wefdate);
$datss     = date('d/m/Y', $timestamp);


$timestamp = strtotime($row->date);
$dateee    = date('d/m/Y', $timestamp);


$html='
<div class="container">


<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="body">
<div class="row clearfix doctoradvice">
<h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>EMPLOYEES’ CONTRIBUTORY WELFARE SCHEME
<br><br>
PREVIEW DECLARATION ABOUT OPTION FOR ENHANCED INSURANCE COVER
<br><br>FOR HOSPITALISATION</h4>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
<form method="POST" action="" enctype="multipart/form-data">
<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="form-group">
<div class="form-line">
<label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
PRADAN.</label>
<br>
'.$office->officename.';


</div>

</div>
</div>
<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
</div>
<div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

</div>

<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="form-group">
<label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir / Madam, </label>
</div>
</div>

<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
<div class="form-group">
I would like to get covered under the Enhanced Insurance Cover for Hospitalisation w.e.f.
&nbsp;
<b>

'.$datss.'


</b>&nbsp; (date). I would like to opt for:
<br><br>
<p class="text-center"><b>Coverage</b></p>



<div class="row" >
<p style="float:left;">(i) Rs 4,00,000/-,

<b>
'.$var.'                   

</b>
but with addition(s) in the number of members of the family
</p>
</div>
<br>
<div>
<p style="float:left;">(ii) Raised from  Rs 4 to Rs.7
<b>
'.$var1.' 

</b>
<b>              
'.$var2.' 

</b>


lakh without any change in the number of members of the family
</p>
</div>
<br>
<div class="row">
<p style="float:left;">(ii) Raised from  Rs 4 to Rs.7
<b>
'.$var3.' 

</b>


<b>
'.$var4.' 


</b>
lakh with addition(s) in the number of members of the family
</p>
</div>
<br>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

</div>
</div>



<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
<div class="form-group">
I agree to pay the additional contribution as prescribed under the Scheme for which I hereby authorize PRADAN to deduct the required amount from my salary.
<br><br>
Yours faithfully,
</div>
</div>

<div class="row" style="margin-left: 5px;margin-right: 5px;">
<p style="float:left;">Place:<b>'.$row->place.'</b></p>
<p style="float:right;">Signature: 


</div>

<div class="row" style="margin-left: 5px;margin-right: 5px;">
<p style="float:left;">Date:
<b> 
'.$dateee.'
</b> </p>
<p style="float:right;">Name:<b>'.$row->candidatefirstname.'</b>&nbsp;<b>'.$row->candidatelastname.'</b></p>
</div>



<div class="row" style="margin-left: 5px;margin-right: 5px;">

<p style="float:right;">Designation:<b>'.$row->desname.'</b></p>
<br>
</div>
<div class="row" style="margin-left: 5px;margin-right: 5px;">


<p style="float:right;">Employee Code:<b>'.$row->emp_code.'</b></p>
</div>
<br>  
<p style="margin-left: 5px;">
cc: - Finance-Personnel-MIS Unit<br>
&emsp;&emsp;- Personal Dossier (Location)
</p>

</div>






</div>




</form>
</div>
</div>
</div>
</div>
</div>



</div>




';

$filename = "".$token."applications";
$this->Employee_particular_form_model->generatePDF($html, $filename, NULL,'Generateofferletter.pdf'); 


}



}