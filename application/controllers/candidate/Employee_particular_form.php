<?php 

/**
* State List
*/
class Employee_particular_form extends CI_controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("Stafffullinfo_model");
        $this->load->model("Global_model","gmodel");
        $this->load->model("Employee_particular_form_model");
        $this->load->model("Common_model","Common_Model");
        $this->load->model(__CLASS__ . '_model', 'model');  

       
        $check = $this->session->userdata('loginData');
       
// ///// Check Session //////  
        if (empty($check)) {
            redirect('login');
        }

        $this->loginData = $this->session->userdata('loginData');
       
    }

    public function index($staff,$candidate_id)
    {

        try{


           // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
           // $content['role_permission'] = $this->db->query($query)->result();

// end permission 

           if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

            $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
            //redirect('/staff_dashboard/index');

        } else {
            $staff = $this->uri->segment(4);
            $candidate_id  = $this->uri->segment(5);

            $content['staff'] = $staff;
            $content['candidate_id'] = $candidate_id;

            $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);


            $staff_id=$staff;
            $candidateid=$candidate_id;

            if(isset($staff_id)&& isset($candidateid))
            {

                $staff_id=$staff_id;
                $candidateid=$candidateid;
            }
            else
            {
                $staff_id=$staff;
                $candidateid=$candidate_id;
            }
           
            $content['report']=$this->Employee_particular_form_model->staff_reportingto($staff_id);

            $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
            $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
            $personal_email=$content['personnal_mail']->EmailID;

            $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
            $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);

            $RequestMethod = $this->input->server('REQUEST_METHOD'); 

            if($RequestMethod == "POST")
            {

             $save=$this->input->post('save');
             $Submit=$this->input->post('submit');
             $Sendsavebtn = $this->input->post('savebtn');

             if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

                $home_town            =(trim($this->input->post('home_town'))==""?null:$this->input->post('home_town'));
                $professional_academy =(trim($this->input->post('professional_academy'))==""?null:$this->input->post('professional_academy'));
                $hobbies              =(trim($this->input->post('hobbies'))==""?null:$this->input->post('hobbies'));
                $publication          =(trim($this->input->post('publication'))==""?null:$this->input->post('publication'));
                $achievementawards    =(trim($this->input->post('achievementawards'))==""?null:$this->input->post('achievementawards'));
                $understand           =(trim($this->input->post('understand'))==""?null:$this->input->post('understand'));
                $d_name               =$this->input->post('d_name');
                $d_palace             =$this->input->post('d_place');
                $d_date               =(trim($this->input->post('d_date'))==""?null:$this->input->post('d_date'));



                $dd=$this->gmodel->changedatedbformate($d_date);


                $arr = array (
                    'staff_id'             => $staff_id,
                    'candidate_id'         => $candidateid,
                    'Staff_hometown'       => $home_town,
                    // 'basic_salary'         => $this->input->post('basic_salary'),
                    'professional_academy' => $professional_academy,
                    'hobbiesandinterests'  => $hobbies,
                    'publications'         => $publication,
                    'understand'           => $understand,
                    'awards_prizes_won'    => $achievementawards,
                    'd_name'               => $d_name,
                    'declaration_date'     => $dd,
                    'declaration_place'    => $d_palace,
                    'status'               => 0
                );

//print_r($arr);


                $result=$this->db->insert('tbl_staffemployee_particularsform', $arr);
                $insertid = $this->db->insert_id();
// $this->session->set_userdata('login_data',$sessArr);

                $this->session->set_userdata('insert_id', $insertid);

                $insert_data = array (

                    'type'      => 8,
                    'r_id'      => (trim($insertid)==""?null:($insertid)),
                    'sender'    => (trim($staff_id)==""?null:($staff_id)),
                    'receiver'  => (trim($content['report']->reportingto)==""?null:($content['report']->reportingto)),
                    'senddate'  => date('Y-m-d'),
                    'createdon' => date('Y-m-d H:i:s'),
                    'createdby' => $this->loginData->UserID,
                    'flag'      => 1,
                    'staffid'   => $staff_id
                );

                $this->db->insert('tbl_workflowdetail', $insert_data);

                if($result)
                {
                    $this->session->set_flashdata('tr_msg', 'Successfully added Employee Particulars Form');
                }
            }
            $submitdatasend = $this->input->post('submitbtn');

            if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') 
            {


                $home_town            =$this->input->post('home_town');
                $professional_academy =$this->input->post('professional_academy');
                $hobbies              =$this->input->post('hobbies');
                $publication          =$this->input->post('publication');
                $achievementawards    =$this->input->post('achievementawards');
                $understand           =$this->input->post('understand');
                $d_name               =$this->input->post('d_name');
                $d_palace             =$this->input->post('d_place');
                $d_date               =$this->input->post('d_date');



                $dd=$this->gmodel->changedatedbformate($d_date);



                $arr = array (
                    'staff_id'             => $staff_id,
                    'candidate_id'         => $candidateid,
                    'Staff_hometown'       => $home_town,
                    // 'basic_salary'         => $this->input->post('basic_salary'),
                    'professional_academy' => $professional_academy,
                    'hobbiesandinterests'  => $hobbies,
                    'publications'         => $publication,
                    'understand'           => $understand,
                    'awards_prizes_won'    => $achievementawards,
                    'd_name'               => $d_name,
                    'declaration_date'     => $dd,
                    'declaration_place'    => $d_palace,
                    'status'               => 1
                );
                $result=$this->db->insert('tbl_staffemployee_particularsform', $arr);
                $insertid = $this->db->insert_id();
                $this->session->set_userdata('insert_id', $insertid);

                $insert_data = array (

                    'type'      => 8,
                    'r_id'      => $insertid,
                    'sender'    => $staff_id,
                    'receiver'  => $content['report']->reportingto,
                    'senddate'  => date('Y-m-d'),
                    'createdon' => date('Y-m-d H:i:s'),
                    'createdby' => $this->loginData->UserID,
                    'flag'      => 1,
                    'staffid'   => $staff_id
                );
                $this->db->insert('tbl_workflowdetail', $insert_data);
                if($result)
                {
                    $this->session->set_flashdata('tr_msg', 'Successfully added Successfully added Employee Particulars Form');
                }

                $subject = ': Employee particular form';
                $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Employee particular form </h4><br />';
                $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
                <tr>
                <td width="96">Name </td>
                <td width="404">'.$content['candidateaddress']->staff_name.'</td>
                </tr>
                <tr>
                <td>Employ Code</td>
                <td> '.$content['candidateaddress']->emp_code.'</td>
                </tr>
                <tr>
                <td>Designation</td>
                <td>' .$content['candidateaddress']->desiname.'</td>
                </tr>
                <tr>
                <td>Office</td>
                <td>'.$content['candidateaddress']->officename.'</td>
                </tr>
                </table>';
                $body .= "<br /> <br />";
                $body .= "Regards <br />";
                $body .= " ". $content['candidateaddress']->staff_name ."<br>";
                $body .= " ". $content['candidateaddress']->desiname."<br>";
                $body .= "<b> Thanks </b><br>";



                $to_useremail = $content['candidateaddress']->emailid;
                $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
                $arr= array (
                    $tcemailid      =>'tc',
                );

                $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

            }
        }
        $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

        if ($content['topbar'])
            $var=$content['topbar']->employ_flag;
        else $var='';

        if ($var==null)
        {
            goto preview;
            redirect('candidate/Employee_particular_form/index/'.$staff.'/'.$candidateid);
        }


        if($var==0)
        {
//goto preview;
            redirect('candidate/Employee_particular_form/edit/'.$staff.'/'.$candidateid);
        }
        elseif($var==1)
        {
            redirect('candidate/Employee_particular_form/view/'.$staff.'/'.$candidateid);
        }


        preview:
        $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);


        $content['TrainingExpcount'] = $this->Employee_particular_form_model->getCountTrainingExposure($candidateid);




        $content['trainingexposuredetals'] = $this->Employee_particular_form_model->getCandidateTrainingExposureDetails($candidateid);
        



        $content['familycount'] = $this->Employee_particular_form_model->getCountFamilyMember($staff_id);



        $content['familymemberdetails']    = $this->Employee_particular_form_model->getCandidateFamilyMemberDetails($staff_id);


        $content['languageproficiency'] = $this->Employee_particular_form_model->getCountLanguage($candidateid);


        $content['signature'] = $this->Common_Model->staff_signature($staff_id);
        // print_r($content['signature']); die;



        @ $content['languagedetails'] = $this->Employee_particular_form_model->getCandidateLanguageDetails($candidateid);


        @ $content['otherinformationdetails'] = $this->Employee_particular_form_model->getCandidateOtherInformationDetails($candidateid);


        $content['pradanmember'] = $this->Employee_particular_form_model->getCandidatePradanMemberDetails($staff_id);
            // print_r($content['pradanmember']); die;

        $content['staff_designation'] = $this->Employee_particular_form_model->getCandidatedesignation($staff_id);



        $content['WorkExperience'] = $this->Employee_particular_form_model->getCountWorkExprience($candidateid);



        $content['syslanguage']  = $this->Employee_particular_form_model->getSysLanguage();
        $content['workexperiencedetails'] = $this->Employee_particular_form_model->getCandidateWorkExperienceDetails($candidateid);



        $query = "select * from mstpgeducation where isdeleted='0'";
        $content['mstpgeducation_details'] = $this->Common_Model->query_data($query);
        $content['subview']="index";
        $content['title'] = 'Employee_particular_form';
        $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
        $this->load->view('candidate/_main_layout', $content);
    }

} catch (Exception $e) {
    log_message('error',$e->getMessage());
    return;
}
}


public function edit($staff,$candidate_id)
{


    try{
        // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
        // $content['role_permission'] = $this->db->query($query)->result();

// end permission 



        if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

            $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
            //redirect('/candidate/Candidatedfullinfo/index');

        } else {

         $staff = $this->uri->segment(4);
         $candidate_id  = $this->uri->segment(5);

         $content['staff'] = $staff;
         $content['candidate_id'] = $candidate_id;

         $this->session->set_userdata('staff', $staff);
         $this->session->set_userdata('candidate_id', $candidate_id);


         $staff_id=$staff;
         $candidateid=$candidate_id;

         if(isset($staff_id)&& isset($candidateid))
         {

            $staff_id=$staff_id;
        // echo "staff id".$staff_id;
            $candidateid=$candidateid;
        // echo "candate id=".$candidateid;

        }
        else
        {
            $staff_id=$staff;
//echo "staff".$staff;
            $candidateid=$candidate_id;
// echo "candidateid=".$candidateid;

        }


        $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
        $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
        $personal_email=$content['personnal_mail']->EmailID;

        $reportingto = $content['candidateaddress']->reportingto;
        $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);


// extract($_POST);
        $RequestMethod = $this->input->server('REQUEST_METHOD'); 

        if($RequestMethod == "POST")
        {



            $save=$this->input->post('save');


            $Submit=$this->input->post('submit');





            $Sendsavebtn = $this->input->post('savebtn');

            if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {





                $id                   =$this->input->post('id');
                $home_town            =$this->input->post('home_town');
                $professional_academy =$this->input->post('professional_academy');
                $hobbies              =$this->input->post('hobbies');
                $publication          =$this->input->post('publication');
                $achievementawards    =$this->input->post('achievementawards');
                $understand           =$this->input->post('understand');
                $d_name               =$this->input->post('d_name');
                $d_palace             =$this->input->post('d_place');
                $d_date               =$this->input->post('d_date');
                // $basic_salary               =$this->input->post('basic_salary');



                $dd=$this->gmodel->changedatedbformate($d_date);



                $arr = array (
                    'id'                   => $id,
                    'staff_id'             => $staff_id,
                    'candidate_id'         => $candidateid,
                    'Staff_hometown'       => $home_town,
                    'professional_academy' => $professional_academy,
                    'hobbiesandinterests'  => $hobbies,
                    'publications'         => $publication,
                    'understand'           => $understand,
                    'awards_prizes_won'    => $achievementawards,
                    'd_name'               => $d_name,
                    'declaration_date'     => $dd,
                    'declaration_place'    => $d_palace,
                    // 'basic_salary'    => $basic_salary,
                    'status'               => 0
                );
                extract($arr);



                $this->db->where('id', $id);
                $result=$this->db->update('tbl_staffemployee_particularsform', $arr);

                if($result)
                {
                    $this->session->set_flashdata('tr_msg', 'Successfully added Employee Particulars Form');
                    redirect('candidate/Employee_particular_form/edit/'.$staff.'/'.$candidateid);  
                }
            }
            $submitdatasend = $this->input->post('submitbtn');

            if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') 
            {



                $id                   =$this->input->post('id');
                $home_town            =$this->input->post('home_town');
                $professional_academy =$this->input->post('professional_academy');
                $hobbies              =$this->input->post('hobbies');
                $publication          =$this->input->post('publication');
                $achievementawards    =$this->input->post('achievementawards');
                $understand           =$this->input->post('understand');
                $d_name               =$this->input->post('d_name');
                $d_palace             =$this->input->post('d_place');
                $d_date               =$this->input->post('d_date');
                // $basic_salary               =$this->input->post('basic_salary');



                $dd=$this->gmodel->changedatedbformate($d_date);



                $arr = array (
                    'id'                   => $id,
                    'staff_id'             => $staff_id,
                    'candidate_id'         => $candidateid,
                    'Staff_hometown'       => $home_town,
                    'professional_academy' => $professional_academy,
                    'hobbiesandinterests'  => $hobbies,
                    'publications'         => $publication,
                    'understand'           => $understand,
                    'awards_prizes_won'    => $achievementawards,
                    'd_name'               => $d_name,
                    'declaration_date'     => $dd,
                    'declaration_place'    => $d_palace,
                    // 'basic_salary'         => $basic_salary,
                    'status'               => 1
                );
                extract($arr);



                $this->db->where('id', $id);
                $result=$this->db->update('tbl_staffemployee_particularsform', $arr);
                if($result)
                {
                    $this->session->set_flashdata('tr_msg', 'Successfully added Employee Particulars Form');
                }

                $subject = ': Employee particular form';
                $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Employee particular form </h4><br />';
                $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
                <tr>
                <td width="96">Name </td>
                <td width="404">'.$content['candidateaddress']->staff_name.'</td>
                </tr>
                <tr>
                <td>Employ Code</td>
                <td> '.$content['candidateaddress']->emp_code.'</td>
                </tr>
                <tr>
                <td>Designation</td>
                <td>' .$content['candidateaddress']->desiname.'</td>
                </tr>
                <tr>
                <td>Office</td>
                <td>'.$content['candidateaddress']->officename.'</td>
                </tr>
                </table>';
                $body .= "<br /> <br />";
                $body .= "Regards <br />";
                $body .= " ". $content['candidateaddress']->staff_name ."<br>";
                $body .= " ". $content['candidateaddress']->desiname."<br>";
                $body .= "<b> Thanks </b><br>";

                $to_useremail = $content['candidateaddress']->emailid;
                $tcemailid=$content['tc_email']->emailid;

                $arr= array (
                    $tcemailid      =>'tc',
                );

                $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
                redirect('candidate/Employee_particular_form/view/'.$staff.'/'.$candidateid);


            }
        }


        $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

        $var=$content['topbar']->employ_flag;

        $content['employ_details'] = $this->Employee_particular_form_model->getEmployDetails($candidateid);


        $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);


        $content['TrainingExpcount'] = $this->Employee_particular_form_model->getCountTrainingExposure($candidateid);


        $content['trainingexposuredetals'] = $this->Employee_particular_form_model->getCandidateTrainingExposureDetails($candidateid);



        $content['familycount'] = $this->Employee_particular_form_model->getCountFamilyMember($staff_id);

        $content['signature'] = $this->Common_Model->staff_signature($staff_id);


        $content['familymemberdetails']    = $this->Employee_particular_form_model->getCandidateFamilyMemberDetails($staff_id);


        $content['languageproficiency'] = $this->Employee_particular_form_model->getCountLanguage($candidateid);


        @ $content['languagedetails'] = $this->Employee_particular_form_model->getCandidateLanguageDetails($candidateid);

        @ $content['otherinformationdetails'] = $this->Stafffullinfo_model->getCandidateOtherInformationDetails($candidateid);


        $content['pradanmember'] = $this->Employee_particular_form_model->getCandidatePradanMemberDetails($staff_id);


        $content['staff_designation'] = $this->Employee_particular_form_model->getCandidatedesignation($staff_id);



        $content['WorkExperience'] = $this->Employee_particular_form_model->getCountWorkExprience($candidateid);

        $content['syslanguage']            = $this->Employee_particular_form_model->getSysLanguage();

        $content['workexperiencedetails'] = $this->Employee_particular_form_model->getCandidateWorkExperienceDetails($candidateid);


        $query = "select * from mstpgeducation where isdeleted='0'";
        $content['mstpgeducation_details'] = $this->Common_Model->query_data($query);


        $content['subview']                ="index";
        $content['title']                  = 'Employee_particular_form/edit';
        $content['subview']                = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
        $this->load->view('candidate/_main_layout', $content);
    }
} catch (Exception $e) {
    log_message('error',$e->getMessage());
    return;
}
}
public function view($staff,$candidate_id)
{

    try{


       if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

        $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
        //redirect('/staff_dashboard/index');

    } else {

        $staff = $this->uri->segment(4);
        $candidate_id  = $this->uri->segment(5);

        $content['staff'] = $staff;
        $content['candidate_id'] = $candidate_id;

        $this->session->set_userdata('staff', $staff);
        $this->session->set_userdata('candidate_id', $candidate_id);

        $staff_id=$staff;
        if(isset($staff_id)&& isset($candidateid))
        {
    //$login_staff=$staff_id;
            $staff_id=$staff_id;
        // echo $staff_id; die;
            $candidateid=$candidateid;

        }
        else
        {
            $staff_id=$staff;
            $candidateid=$candidate_id;
        }

        $content['id'] = $this->Employee_particular_form_model->getworkflowid($staff_id);

        // $content['signature'] = $this->Common_Model->staff_signature($staff_id);
        // print_r($content['signature']);
        // die;


        $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
        $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();

        $personal_email=$content['personnal_mail']->EmailID;

        $reportingto = $content['candidateaddress']->reportingto;

        $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);

        $content['reporting'] = $this->Employee_particular_form_model->getCandidateWith($reportingto);

        $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;


        $content['personal']=$this->Common_Model->get_Personnal_Staff_List();

        if(!empty($this->loginData->RoleID))
        {

        if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
        {
         $RequestMethod = $this->input->server('REQUEST_METHOD'); 


         if($RequestMethod == 'POST'){
            $status=$this->input->post('status');
            $reject=$this->input->post('reject');
            $p_status=$this->input->post('p_status');
            $r_id=$this->input->post('id');

            $check = $this->session->userdata('insert_id');

            if($status==2){
                $appstatus = 'approved';
            }elseif ($status==3) {
                $appstatus = 'Reject';
            }

            $insert_data = array (
                'type'                 => 8,
                'r_id'                 => $r_id,
                'sender'               => $this->loginData->staffid,
                'receiver'             => $p_status,
                'senddate'             => date('Y-m-d'),
                'createdon'            => date('Y-m-d H:i:s'),
                'createdby'            => $this->loginData->staffid,
                'scomments'            => $reject,
                'forwarded_workflowid' => $content['id']->workflow_id,
                'flag'                 => $status,
                'staffid'              => $staff_id
            );

            $this->db->insert('tbl_workflowdetail', $insert_data);






            $subject = ': Employee particular form';
            $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc</h4><br />';
            $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
            <tr>
            <td width="96">Name </td>
            <td width="404">'.$content['candidateaddress']->staff_name.'</td>
            </tr>
            <tr>
            <td>Employ Code</td>
            <td> '.$content['candidateaddress']->emp_code.'</td>
            </tr>
            <tr>
            <td>Designation</td>
            <td>' .$content['candidateaddress']->desiname.'</td>
            </tr>
            <tr>
            <td>Office</td>
            <td>'.$content['candidateaddress']->officename.'</td>
            </tr>
            </table>';
            $body .= "<br /> <br />";
            $body .= "Regards <br />";
            $body .= " ". $content['candidateaddress']->staff_name ."<br>";
            $body .= " ". $content['candidateaddress']->desiname."<br>";
            $body .= "<b> Thanks </b><br>";

       // $to_useremail = 'pdhamija2012@gmail.com';
            $to_useremail = $content['candidateaddress']->emailid;
            $tcemailid=$content['tc_email']->emailid;
            $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
            $arr= array (
                $tcemailid      =>'tc',
                $personal_email =>'personal'
            );

            $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
            redirect('candidate/Employee_particular_form/view/'.$staff.'/'.$candidateid);
        }

    }

    else  if($this->loginData->RoleID==17)
    {
     $RequestMethod = $this->input->server('REQUEST_METHOD'); 


     if($RequestMethod == 'POST'){
        $status   =$this->input->post('status');
        $reject   =$this->input->post('reject');
        $p_status =$this->input->post('p_status');
        $r_id     =$this->input->post('id');



        if($status==4){
            $appstatus = 'approved';
        }elseif ($status==3) {
            $appstatus = 'Reject';
        }

        $insert_data = array (

            'type'                 => 8,
            'r_id'                 => $r_id,
            'sender'               => $this->loginData->staffid,
            'receiver'             => $staff_id,
            'senddate'             => date('Y-m-d'),
            'createdon'            => date('Y-m-d H:i:s'),
            'createdby'            => $this->loginData->staffid,
            'scomments'            => $reject,
            'forwarded_workflowid' => $content['id']->workflow_id,
            'flag'                 => $status,
            'staffid'             => $staff_id
        );


        $this->db->insert('tbl_workflowdetail', $insert_data);


        $subject = ': Employee particular form';
        $body = '<h4>'.$personnelname.''.$appstatus.' by personnel</h4><br />';
        $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
        <tr>
        <td width="96">Name </td>
        <td width="404">'.$content['candidateaddress']->staff_name.'</td>
        </tr>
        <tr>
        <td>Employ Code</td>
        <td> '.$content['candidateaddress']->emp_code.'</td>
        </tr>
        <tr>
        <td>Designation</td>
        <td>' .$content['candidateaddress']->desiname.'</td>
        </tr>
        <tr>
        <td>Office</td>
        <td>'.$content['candidateaddress']->officename.'</td>
        </tr>
        </table>';
        $body .= "<br /> <br />";
        $body .= "Regards <br />";
        $body .= " ". $content['candidateaddress']->staff_name ."<br>";
        $body .= " ". $content['candidateaddress']->desiname."<br>";
        $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
        $to_useremail = $content['candidateaddress']->emailid;
        $tcemailid=$content['tc_email']->emailid;
        $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
        $arr= array (
            $tcemailid      =>'tc',
            $personal_email =>'personal'
        );

        $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
        redirect('candidate/Employee_particular_form/view/'.$staff.'/'.$candidateid);
    }


    }
}


$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);




$content['employ_details'] = $this->Employee_particular_form_model->getEmployDetails($candidateid);
$content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);




$content['TrainingExpcount'] = $this->Employee_particular_form_model->getCountTrainingExposure($staff_id);


$content['trainingexposuredetals'] = $this->Employee_particular_form_model->getCandidateTrainingExposureDetails($candidateid);



$content['familycount'] = $this->Employee_particular_form_model->getCountFamilyMember($staff_id);




$content['familymemberdetails']    = $this->Employee_particular_form_model->getCandidateFamilyMemberDetails($staff_id);

$content['languageproficiency'] = $this->Employee_particular_form_model->getCountLanguage($candidateid);





@ $content['languagedetails'] = $this->Employee_particular_form_model->getCandidateLanguageDetails($candidateid);

@ $content['otherinformationdetails'] = $this->Employee_particular_form_model->getCandidateOtherInformationDetails($candidateid);

$content['pradanmember'] = $this->Employee_particular_form_model->getCandidatePradanMemberDetails($staff_id);

$content['staff_designation'] = $this->Employee_particular_form_model->getCandidatedesignation($staff_id);


$WEcount = $this->Employee_particular_form_model->getCountWorkExprience($staff_id);

$content['WorkExperience']= $WEcount;  


$content['syslanguage']            = $this->Employee_particular_form_model->getSysLanguage();
$content['workexperiencedetails'] = $this->Employee_particular_form_model->getCandidateWorkExperienceDetails($candidateid);





// $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
// $content['role_permission'] = $this->db->query($query)->result();

// end permission 

$query = "select * from mstpgeducation where isdeleted='0'";
$content['mstpgeducation_details'] = $this->Common_Model->query_data($query);

 $content['subview']       ="index";
 $content['title']  = 'Employee_particular_form/view';
$content['subview']                = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
        $this->load->view('candidate/_main_layout', $content);


}

} catch (Exception $e) {
    log_message('error',$e->getMessage());
    return;
}
}


public function Add()
{

// start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 



    $RequestMethod = $this->input->server('REQUEST_METHOD'); 


    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
            $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
                '</div>');

            $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

            $hasValidationErrors    =    true;
            goto prepareview;

        }


        $insertArr = array (
            'pgname'      => $this->input->post('pgname'),
            'status'      => $this->input->post('status')
        );

        $this->db->insert('mstpgeducation', $insertArr);
        $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
        $this->session->set_flashdata('er_msg', $this->db->error());
        redirect('/Employee_particular_form/index/');
    }

    prepareview:


    $content['subview'] = 'Employee_particular_form/add';
    $this->load->view('_main_layout', $content);
}





public function addnarrative($staff,$candidate_id)
{
     // if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

     //            $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
     //            redirect('/staff_dashboard/index');

     //        } else {
    $staff = $this->uri->segment(4);
    $candidate_id  = $this->uri->segment(5);

    $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id;

    $this->session->set_userdata('staff', $staff);
    $this->session->set_userdata('candidate_id', $candidate_id);

    $staff_id=$staff;
    $candidateid=$candidate_id;

    if(isset($staff_id)&& isset($candidateid))
    {

        $staff_id=$staff_id;
//echo "staff id".$staff_id;
        $candidateid=$candidateid;
//echo "candate id=".$candidateid;

    }
    else
    {
        $staff_id=$staff;
//echo "staff".$staff;
        $candidateid=$candidate_id;
// echo "candidateid=".$candidateid;

    }

// print_r($this->input->post()); die;

    $content['report']=$this->Employee_particular_form_model->staff_reportingto($staff_id);
    $this->load->model('Employee_particular_form_model');
    $content['emp_code'] = $this->Employee_particular_form_model->fetch($candidateid);



//print_r($content['topbar']);die();

    $content['signature'] = $this->Common_Model->staff_signature($staff_id);

    $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
    $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
  
    $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
    $personal_email=$content['personnal_mail']->EmailID;

    $reportingto = $content['candidateaddress']->reportingto;
    
    $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);

    $query2="SELECT * from tblstaffnarrativeselfprofile where candidateid=$candidateid";
    $content['list_staff_narrative_table']=$this->db->query($query2)->row();

    if($content['list_staff_narrative_table'])

        $var=$content['list_staff_narrative_table']->flag;
    else $var= '';
// echo $var.'sdsd';
// die;

// $staff=null,$candidate_id=null

    if ($var==null) 
    {
            //redirect('/Employee_particular_form/addnarrative/'.$staff.'/'.$candidateid);
        goto prepareviewss;
    }

    if($var==0)
    {
        redirect('candidate/Employee_particular_form/editnarrative/'.$staff.'/'.$candidateid);
    }
    elseif($var==1)
    {
        redirect('candidate/Employee_particular_form/previewnarrative/'.$staff.'/'.$candidateid);
    }


// start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    prepareviewss:
    $RequestMethod = $this->input->server('REQUEST_METHOD'); 


    if($RequestMethod == 'POST'){

// print_r($this->input->post()); die;


// $this->form_validation->set_rules('narratives','Narrative Name','trim|required|min_length[1]|max_length[50]');


        // if($this->form_validation->run() == FALSE){
        //     $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        //         '</div>');

        //     $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        //     $hasValidationErrors    =    true;

        //     goto prepareview;

        // }

        $operation=$this->input->post('operation');

// echo $operation; die;

        if($operation == 0){
// echo "hbhj"; die;
            $insertArr = array (
                'narrative'   => $this->input->post('narratives'),
                'createdon'   => date('Y-m-d H:i:s'),
                'createdby'   => $this->loginData->UserID,
                'candidateid' => $candidateid,
                'flag'        => 0

            );
// print_r($insertArr); die; 
            $this->db->insert('tblstaffnarrativeselfprofile', $insertArr);
            $insertid = $this->db->insert_id();
// echo $this->db->last_query(); die;
            $insert_data = array (
                'type'      => 9,
                'r_id'      => $insertid,
                'sender'    => $staff_id,
                'receiver'  => $content['report']->reportingto,
                'senddate'  => date('Y-m-d'),
                'createdon' => date('Y-m-d H:i:s'),
                'createdby' => $this->loginData->UserID,
                'flag'      => 1,
                'staffid'  => $staff_id
            );
            $this->db->insert('tbl_workflowdetail', $insert_data);
// echo $this->db->last_query(); die;
            $this->session->set_flashdata('tr_msg', 'Successfully Save narrative profile');
            $this->session->set_flashdata('er_msg', $this->db->error());

            redirect('candidate/Employee_particular_form/editnarrative/' .$staff.'/'.$candidateid);


        } else if ($operation == 1) {

            $insertArr = array (
                'narrative'   => $this->input->post('narratives'),
                'createdon'   => date('Y-m-d H:i:s'),
                'createdby'   => $this->loginData->UserID,
                'candidateid' => $candidateid,
                'flag'        => 1 

            );
// print_r($insertArr); die;
            $this->db->insert('tblstaffnarrativeselfprofile', $insertArr);
            $insertid = $this->db->insert_id();

            $insert_data = array (
                'type'      => 9,
                'r_id'      => $insertid,
                'sender'    => $staff_id,
                'receiver'  => $content['report']->reportingto,
                'senddate'  => date('Y-m-d'),
                'createdon' => date('Y-m-d H:i:s'),
                'createdby' => $this->loginData->UserID,
                'flag'      => 1,
                'staffid'  => $staff_id
            );
            $this->db->insert('tbl_workflowdetail', $insert_data);
// echo $this->db->last_query(); die;
            $this->session->set_flashdata('tr_msg', 'Successfully Save & Submit narrative profile');
            $this->session->set_flashdata('er_msg', $this->db->error());

            $subject = ': Narrative self profile';
            $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Narrative self profile </h4><br />';
            $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
            <tr>
            <td width="96">Name </td>
            <td width="404">'.$content['candidateaddress']->staff_name.'</td>
            </tr>
            <tr>
            <td>Employ Code</td>
            <td> '.$content['candidateaddress']->emp_code.'</td>
            </tr>
            <tr>
            <td>Designation</td>
            <td>' .$content['candidateaddress']->desiname.'</td>
            </tr>
            <tr>
            <td>Office</td>
            <td>'.$content['candidateaddress']->officename.'</td>
            </tr>
            </table>';
            $body .= "<br /> <br />";
            $body .= "Regards <br />";
            $body .= " ". $content['candidateaddress']->staff_name ."<br>";
            $body .= " ". $content['candidateaddress']->desiname."<br>";
            $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
            $to_useremail = $content['candidateaddress']->emailid;
            $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
            $arr= array (
                $tcemailid      =>'tc',
            );

            $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
            
            redirect('candidate/Employee_particular_form/previewnarrative/' .$staff.'/'.$candidateid);

# code...
        }
    }



    prepareview:


    $content['subview'] = 'Employee_particular_form/addnarrative';
    $this->load->view('candidate/_main_layout', $content);
    
}


public function Editnarrative($staff,$candidate_id)
{
     // if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

     //            $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
     //            redirect('/staff_dashboard/index');

     //        } else {
    $staff = $this->uri->segment(4);
    $candidate_id  = $this->uri->segment(5);

    $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id;

    $this->session->set_userdata('staff', $staff);
    $this->session->set_userdata('candidate_id', $candidate_id);







    $staff_id=$staff;
    $candidateid=$candidate_id;

    if(isset($staff_id)&& isset($candidateid))
    {

        $staff_id        =$staff_id;
        // echo "staff id".$staff_id;
        $candidateid     =$candidateid;
        // echo "candate id =".$candidateid;

    }
    else
    {
        $staff_id=$staff;
//echo "staff".$staff;
        $candidateid=$candidate_id;
// echo "candidateid=".$candidateid;

    }




    $query4="Select * from tblstaffnarrativeselfprofile where candidateid=$candidateid";
    $content['datanarrative'] = $this->db->query($query4)->row();


    $this->load->model('Employee_particular_form_model');
    $content['emp_code'] = $this->Employee_particular_form_model->fetch($candidateid);
    $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);

    $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
    $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
    $personal_email=$content['personnal_mail']->EmailID;

    $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
    $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);

    $content['signature'] = $this->Common_Model->staff_signature($staff_id);


    $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);


    $query2="SELECT * from tblstaffnarrativeselfprofile where candidateid=$candidateid";
    $content['list_staff_narrative_table']=$this->db->query($query2)->result();
//print_r($content['list_staff_narrative_table']);die;
// start permission 
    // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    // $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 


    if($RequestMethod == 'POST'){

        // print_r($this->input->post()); //die;
        // $this->form_validation->set_rules('narratives','Narrative Name','trim|required|min_length[1]|max_length[50]');
        //$this->form_validation->set_rules('status','Status','trim|required');


        $operation=$this->input->post('operation');

       // print_r($operation);

       // echo $operation; die;

        if($operation == 0){

            $updateArr = array (
                'narrative'   => $this->input->post('narratives'),
                'updatedon'   => date('Y-m-d H:i:s'),
                'updatedby'   => $this->loginData->UserID,
                'candidateid' => $candidateid,
                'flag'        => 0

            );

            $this->db->where('candidateid', $candidateid);
            $this->db->update('tblstaffnarrativeselfprofile', $updateArr);
            $this->session->set_flashdata('tr_msg', 'Successfully Save narrative profile');
            $this->session->set_flashdata('er_msg', $this->db->error());
            redirect('candidate/Employee_particular_form/editnarrative/'.$staff.'/'.$candidateid);


        } else {

            // echo "hbuyhbj"; die;

            $updateArr = array (
                'narrative'   => $this->input->post('narratives'),
                'updatedon'   => date('Y-m-d H:i:s'),
                'updatedby'   => $this->loginData->UserID,
                'candidateid' => $candidateid,
                'flag'        => 1 

            );

            $this->db->where('candidateid', $candidateid);
            $this->db->update('tblstaffnarrativeselfprofile', $updateArr);

            $this->session->set_flashdata('tr_msg', 'Successfully Updated narrative profile');
            $this->session->set_flashdata('er_msg', $this->db->error());

            $subject = ': Narrative self profile';
            $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Narrative self profile</h4><br />';
            $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
            <tr>
            <td width="96">Name </td>
            <td width="404">'.$content['candidateaddress']->staff_name.'</td>
            </tr>
            <tr>
            <td>Employ Code</td>
            <td> '.$content['candidateaddress']->emp_code.'</td>
            </tr>
            <tr>
            <td>Designation</td>
            <td>' .$content['candidateaddress']->desiname.'</td>
            </tr>
            <tr>
            <td>Office</td>
            <td>'.$content['candidateaddress']->officename.'</td>
            </tr>
            </table>';
            $body .= "<br /> <br />";
            $body .= "Regards <br />";
            $body .= " ". $content['candidateaddress']->staff_name ."<br>";
            $body .= " ". $content['candidateaddress']->desiname."<br>";
            $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
            $to_useremail = $content['candidateaddress']->emailid;
            $tcemailid=$content['tc_email']->emailid;
            $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;
            $to_name = '';
       // $to_hremail = $hremail->EmailID;
            $arr= array (
                $tcemailid      => 'tc',
                $personal_email => 'Personnal'
            );

            $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
            redirect('candidate/Employee_particular_form/previewnarrative/'.$staff.'/'.$candidateid);
        }
    }



    $content['subview'] = 'Employee_particular_form/editnarrative';
    $this->load->view('candidate/_main_layout', $content);

}

public function previewnarrative($staff,$candidateid)
{

         // if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

         //        $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
         //        redirect('/staff_dashboard/index');

         //    } else {

    $staff = $this->uri->segment(4);
    $candidate_id  = $this->uri->segment(5);

    $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id;

    $this->session->set_userdata('staff', $staff);
    $this->session->set_userdata('candidate_id', $candidate_id);

    $staff_id    =$staff;
    $login_staff =$this->loginData->staffid;
    $candidateid =$candidate_id;

    if(isset($staff_id)&& isset($candidateid))
    {

        $staff_id=$staff_id;
// echo "staff id".$staff_id;
        $candidateid=$candidateid;
// echo "candate id=".$candidateid;

    }
    else
    {
        $staff_id=$staff;
//echo "staff".$staff;
        $candidateid=$candidate_id;
// echo "candidateid=".$candidateid;

    }


//$staffids=($this->loginData->staffid);
//$candidateid=($this->loginData->candidateid);          


 //$content['topbar'] = $this->db->query($query)->result();


    $content['flags'] = $this->Employee_particular_form_model->get_narrativeworkflowid($staff_id);
// print_r($content['flags']); die;

    $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
    $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
    $personal_email=$content['personnal_mail']->EmailID;

    $reportingto = $content['candidateaddress']->reportingto;

    $content['signature'] = $this->Common_Model->staff_signature($staff_id);
    
    // echo $reportingto; die;
    $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);
    // print_r($content['tc_email']); die;
    $content['reporting'] = $this->Employee_particular_form_model->getCandidateWith($reportingto);

    $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;

    $content['personal']=$this->Common_Model->get_Personnal_Staff_List();

        if(!empty($this->loginData->RoleID))
        {

    if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
    {
        $RequestMethod = $this->input->server('REQUEST_METHOD'); 


        if($RequestMethod == 'POST'){
       // print_r($_POST);die;
            $status   =$this->input->post('status');
            $reject   =$this->input->post('reject');
            $p_status =$this->input->post('p_status');
            $r_id     =$this->input->post('id'); 

            $check = $this->session->userdata('insert_id');

            if($status==2){
                $appstatus = 'Approved';
            }elseif ($status==3) {
                $appstatus = 'Reject';
            }

            $insert_data = array (
                'type'                 => 9,
                'r_id'                 => $r_id,
                'sender'               => $login_staff,
                'receiver'             => $p_status,
                'senddate'             => date('Y-m-d'),
                'createdon'            => date('Y-m-d H:i:s'),
                'createdby'            => $login_staff,
                'scomments'            => $reject,
                'forwarded_workflowid' => $content['flags']->workflow_id,
                'flag'                 => $status,
                'staffid'              => $staff_id
            );
// print_r($insert_data);    die;                  


            $this->db->insert('tbl_workflowdetail', $insert_data);
            $subject = ': Narrative self profile';
            $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc</h4><br />';
            $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
            <tr>
            <td width="96">Name </td>
            <td width="404">'.$content['candidateaddress']->staff_name.'</td>
            </tr>
            <tr>
            <td>Employ Code</td>
            <td> '.$content['candidateaddress']->emp_code.'</td>
            </tr>
            <tr>
            <td>Designation</td>
            <td>' .$content['candidateaddress']->desiname.'</td>
            </tr>
            <tr>
            <td>Office</td>
            <td>'.$content['candidateaddress']->officename.'</td>
            </tr>
            </table>';
            $body .= "<br /> <br />";
            $body .= "Regards <br />";
            $body .= " ". $content['candidateaddress']->staff_name ."<br>";
            $body .= " ". $content['candidateaddress']->desiname."<br>";
            $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
            $to_useremail = $content['candidateaddress']->emailid;
            $tcemailid=$content['tc_email']->emailid;
            $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
            $arr= array (
                $tcemailid      =>'tc',
                $personal_email =>'personal'
            );

            $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
            redirect('candidate/Employee_particular_form/previewnarrative/'.$staff.'/'.$candidateid);


        }
    }
        // $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

    
    else if($this->loginData->RoleID==17)
    {
        $RequestMethod = $this->input->server('REQUEST_METHOD'); 


        if($RequestMethod == 'POST'){

       //echo "<pre>";print_r($_POST);die;
            $status=$this->input->post('status');
            $reject=$this->input->post('reject');
       // $p_status=$this->input->post('p_status');
            $r_id=$this->input->post('id2');

            if($status==4){
                $appstatus = 'approved';
            }elseif ($status==3) {
                $appstatus = 'Reject';
            }


            $insert_data = array (
                'type'                 => 9,
                'r_id'                 => $r_id,
                'sender'               => $login_staff,
                'receiver'             => $staff_id,
                'senddate'             => date('Y-m-d'),
                'createdon'            => date('Y-m-d H:i:s'),
                'createdby'            => $login_staff,
                'scomments'            => $reject,
                'forwarded_workflowid' => $content['flags']->workflow_id,
                'flag'                 => $status,
                'staffid'              => $staff_id
            );
            $this->db->insert('tbl_workflowdetail', $insert_data);

            $subject = ': Narrative self profile';
            $body = '<h4>'.$personnelname.''.$appstatus.' by personnel</h4><br />';
            $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
            <tr>
            <td width="96">Name </td>
            <td width="404">'.$content['candidateaddress']->staff_name.'</td>
            </tr>
            <tr>
            <td>Employ Code</td>
            <td> '.$content['candidateaddress']->emp_code.'</td>
            </tr>
            <tr>
            <td>Designation</td>
            <td>' .$content['candidateaddress']->desiname.'</td>
            </tr>
            <tr>
            <td>Office</td>
            <td>'.$content['candidateaddress']->officename.'</td>
            </tr>
            </table>';
            $body .= "<br /> <br />";
            $body .= "Regards <br />";
            $body .= " ". $content['candidateaddress']->staff_name ."<br>";
            $body .= " ". $content['candidateaddress']->desiname."<br>";
            $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
            $to_useremail = $content['candidateaddress']->emailid;
            $tcemailid=$content['tc_email']->emailid;
            $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
            $arr= array (
                $tcemailid      =>'tc',
                $personal_email =>'personal'
            );

            $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
            redirect('candidate/Employee_particular_form/previewnarrative/'.$staff.'/'.$candidateid);


        }
    }
}
    $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);





    $query4="Select * from tblstaffnarrativeselfprofile where candidateid=$candidateid";
    $content['datanarrative'] = $this->db->query($query4)->row();
 // print_r($content['datanarrative']); die;



    $this->load->model('Employee_particular_form_model');
    $content['emp_code'] = $this->Employee_particular_form_model->fetch($candidateid);
//print_r($content['emp_code']); 
    $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);

    $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);



    $query2="SELECT * from tblstaffnarrativeselfprofile where candidateid=$candidateid";
    $content['list_staff_narrative_table']=$this->db->query($query2)->result();

// start permission 
    // $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    // $content['role_permission'] = $this->db->query($query)->result();



    prepareview:


    $content['subview'] = 'Employee_particular_form/previewnarrative';
    $this->load->view('candidate/_main_layout', $content);



}


public function getpdfnarrative($staff,$candidateid)
{
    $staff = $this->uri->segment(3);
    $candidate_id  = $this->uri->segment(4);

    $this->session->set_userdata('staff', $staff);
    $this->session->set_userdata('candidate_id', $candidate_id);







    $staff_id    =$staff;
    $login_staff =$this->loginData->staffid;
    $candidateid =$candidate_id;

    if(isset($staff_id)&& isset($candidateid))
    {

        $staff_id=$staff_id;
// echo "staff id".$staff_id;
        $candidateid=$candidateid;
// echo "candate id=".$candidateid;

    }
    else
    {
        $staff_id=$staff;
//echo "staff".$staff;
        $candidateid=$candidate_id;
// echo "candidateid=".$candidateid;

    }


//$token=($this->loginData->candidateid); 

    $this->load->model('Employee_particular_form_model');
// $data=$this->Employee_particular_form_model->
    $pdf = $this->Employee_particular_form_model->getpdf($candidateid);


    $candidate = array('$pdfcandidatefirstname','$pdfcandidatelastname','$pdfemp_code');
    $candidate_replace = array($pdf->candidatefirstname,$pdf->candidatelastname,$pdf->emp_code,$pdf->narrative);

    // $html='    <div class="row clearfix">
    // <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    // <div class="card">
    // <div class="body">
    // <div class="row clearfix doctoradvice">
    // <h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>Preview: NARRATIVE SELF PROFILE</h4>
    // <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
    // <form method="POST" action="">
    // <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    // <div class="form-group">


    // </div>
    // </div>
    // <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    // </div>

    // <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    // <div class="form-group">

    // Name: <b>'.$pdf->candidatefirstname.'</b>&nbsp;<b>'.$pdf->candidatelastname.'</b>
    // <br>


    // Emloyee Code:<b>'.$pdf->emp_code.'</b>

    // </div>
    // </div>

    // <br>
    // <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    // <div class="form-group">
    // In the space given below, please write 500 words introducing yourself to colleagues in PRADAN. You may tell us about your family, what made you choose the paths you have taken in life, describe your interests, your life until now and any other aspects you consider significant.
    // </div>
    // </div>
    // <br>
    // <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


    // <b>'.$pdf->narrative.'</b>


    // </div>





    // </form>
    // </div>
    // </div>
    // </div>
    // </div>
    // </div>';

    $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 19 AND `isactive` = '1'";
    $data = $this->db->query($sql)->row();
    if(!empty($data))
      $body = str_replace($candidate,$candidate_replace , $data->lettercontent);


  $filename = "".$token."narrative";
  $this->Employee_particular_form_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');  
// $this->session->set_flashdata('tr_msg', 'Successfully Generate Offer Letter');     




}


// public function topbar()
//  {
//   $staffids=($this->loginData->staffid);
//   $candidateid=($this->loginData->candidateid);          

//   $query="SELECT a.flag AS narrative_flage, b.flag AS joining_report_flag, c.flag AS identity_flag, d.flag AS medical_certifacte_flag, e.flag AS ehnaced_flag, f.flag AS declarte_flag, g.flag AS membership_flag FROM tblstaffnarrativeselfprofile AS a LEFT JOIN tblstaffjoiningreport AS b ON b.candidate_id = a.candidateid LEFT JOIN tblidentitycard AS c ON c.candidate_id = a.candidateid LEFT JOIN tbl_medical_certificate AS d ON a.candidateid = d.candidate_id LEFT JOIN tbl_for_enhanced AS e ON a.candidateid = e.candidate_id LEFT JOIN tbldeclartion_about_family AS f ON a.candidateid = f.candidate_id LEFT JOIN membership_application_form AS g ON a.candidateid = g.candidate_id WHERE a.candidateid = $candidateid";
//   $content['topbar'] = $this->db->query($query)->result();


//   $content['subview'] = 'Employee_particular_form/topbar';
//   $this->load->view('_main_layout', $content);



// }

}