<?php 

/**
* Joining Report Form controller
*/
class Joining_report_form extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		$this->load->model("Joining_report_form_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model(__CLASS__ . '_model', 'model');	

		$check = $this->session->userdata('loginData');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('loginData');

	}


	  public function index()
		 {

		 		$supervisor_name='';
		 		 $supervisor_mailid='';
		 		 $candidatefname='';
		 		 $emailid='';

			  $Selected_tcDetails = $this->model->tc_data($this->loginData->teamid);

			   $supervisor_name=$Selected_tcDetails->name;
			   $supervisor_mailid=$Selected_tcDetails->emailid;
			   $candidatefname=$this->loginData->candidatefirstname .' ' .$this->loginData->candidatelastname;
			   $emailid=$this->loginData->emailid;
			   // echo $emailid;

			   // die;


		 	
		 $RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){

			
			// print_r($this->loginData);
			
		 	//print_r($this->input->post()); 

			$this->db->where('candidateid', $this->loginData->candidateid);
            $query = $this->db->get('tbl_joining_report');

         	    $join_programme_date1 = trim($this->input->post('join_programme_date'));

		       $join_programme_date = $this->model->changedatedbformate($join_programme_date1);

		       $savesenddata = $this->input->post('savebtn');

			if (!empty($savesenddata) && $savesenddata =='senddatasave') {

             	$insertarraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				'apprenticeship_at_location'  => $this->input->post('apprenticeship_at_location'),
				'office_at_location'  => $this->input->post('office_at_location'),
				'join_programme_date'  => trim($join_programme_date),
				'status'  => 0,
				'createdon'  => date('Y-m-d H:i:s'), 
				'createdby'  => $this->loginData->candidateid,

			);
		
      		$this->db->insert('tbl_joining_report', $insertarraydata);
      		$insertid = $this->db->insert_id();
		    $this->db->trans_complete();

		    if ($this->db->trans_status() === FALSE){
		    	
				$this->session->set_flashdata('er_msg', 'Error !!! Candidate Joining report save');	
			}else{


				
				 $subject = "Submit Gereral Nomination And Authorisation Form ";
				  $body = 	'Dear Sir, <br><br> ';
				  $body .= 'Submit Joining Report Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 $to_email = $this->loginData->emailid;
				 $to_name = $supervisor_mailid;
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);
				
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Joining report save');	
			}

			redirect('candidate/Joining_report_form/edit/'.$insertid);
		}


			$submitsenddata = $this->input->post('submitbtn');

		if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {



			$join_programme_date1 = trim($this->input->post('join_programme_date'));

		     $join_programme_date = $this->model->changedatedbformate(trim($join_programme_date1));

			$insertarraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				'apprenticeship_at_location'  => $this->input->post('apprenticeship_at_location'),
				'office_at_location'  => $this->input->post('office_at_location'),
				'join_programme_date'  => trim($join_programme_date),
				'status'  => 1,
				'createdon'  => date('Y-m-d H:i:s'), 
				'createdby'  => $this->loginData->candidateid,

			);
		
      		$this->db->insert('tbl_joining_report', $insertarraydata);
      		//$insertid = $this->db->insert_id();
		    $this->db->trans_complete();

		    if ($this->db->trans_status() === FALSE){
		    	
				$this->session->set_flashdata('er_msg', 'Error !!! Candidate Joining report save');	
			}else{


				// $hrdemailid    = 'poonamadlekha@pradan.net';
			$candidate = array('$supervisor_name','$candidatefname');
			$candidate_replace = array($supervisor_name,$candidatefname);
				
				 $tcemailid     =  $supervisor_mailid;
				 $subject = "Completion of joining formalities";
				 // $message='';
				  // $body = 	'Dear Sir, <br><br> ';
				  // $body .= 'Submit Joining Report Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');

				  //  $fd = fopen("mailtext/completion_of_joining_formalities_joining_report.txt", "r");	
						// $message .=fread($fd,4096);
						// eval ("\$message = \"$message\";");
						// $message =nl2br($message);

			//content from db
		 	$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 42 AND `isactive` = '1'";
   		    $data = $this->db->query($sql)->row();
   		    if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

						
				 $to_email = $supervisor_mailid;
				 $to_name = $emailid;

				 $recipients = array ($supervisor_mailid => $emailid); 
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name,$recipients);

				
				 
				
				$sendmail = $this->Common_Model->send_email($subject,$message, $to_email,$to_name);
				
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Joining report save');	
			}

			redirect('/candidate/Joining_report_form/view/');
		}


	}


		 	$content['candidatedetils'] = $this->model->getCandidateDetails($this->loginData->candidateid);

		 	$content['getstatus'] = $this->model->getStatus($this->loginData->candidateid);

		 	$content['getjoiningreport']       = $this->model->getJoiningReport($this->loginData->candidateid);
		 	$content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
		 
		 
		 	$content['title'] = 'Joining_report_form';
		 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		 	$this->load->view('candidate/_main_layout', $content);
		
		 }


	public function edit($token){

		try{

		 $join_programme_date = "";
		 $this->db->trans_start();
		 $token1 = $this->input->post('token');
		 $RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){

			// echo "<pre>";
		 // 	print_r($this->input->post()); die;

			 $savesenddata = $this->input->post('savebtn');

			if (!empty($savesenddata) && $savesenddata =='senddatasave') {
				

				$join_programme_date1 = $this->input->post('join_programme_date');

		     $join_programme_date = $this->model->changedatedbformate($join_programme_date1);


            	$updatearraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				'apprenticeship_at_location'  => $this->input->post('apprenticeship_at_location'),
				'office_at_location'  => $this->input->post('office_at_location'),
				'join_programme_date'  => $join_programme_date,
				'status'  => 1,
				'createdon'  => date('Y-m-d H:i:s'), 
				'createdby'  => $this->loginData->candidateid,
				'submittedon'  => date('Y-m-d H:i:s'), 
				'submittedby'  => $this->loginData->candidateid,
			);

			$this->db->where('id',$token1);
      		$this->db->update('tbl_joining_report', $updatearraydata);
      		$this->db->trans_complete();

      		  if ($this->db->trans_status() === FALSE){
      		  
				$this->session->set_flashdata('er_msg', 'Error !!! Candidate Joining report save');	

			}else{
				
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Joining report save');	

				redirect('candidate/Joining_report_form/edit/'.$token1);		
			}

		}

			$submitsenddata = $this->input->post('submitbtn');

		if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

			$join_programme_date1 = $this->input->post('join_programme_date');

		     $join_programme_date = $this->model->changedatedbformate($join_programme_date1);

		     // $join_programme_date = $this->input->post('join_programme_date');

		     // $join_programme_date = Date('Y-m-d', strtotime($join_programme_date));
				

            	$updatearraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				'apprenticeship_at_location'  => $this->input->post('apprenticeship_at_location'),
				'office_at_location'  => $this->input->post('office_at_location'),
				'join_programme_date'  => $join_programme_date,
				'status'  => 1,
				'createdon'  => date('Y-m-d H:i:s'), 
				'createdby'  => $this->loginData->candidateid,
				'submittedon'  => date('Y-m-d H:i:s'), 
				'submittedby'  => $this->loginData->candidateid,
			);

			$this->db->where('id',$token1);
      		$this->db->update('tbl_joining_report', $updatearraydata);
      		$this->db->trans_complete();

      		  if ($this->db->trans_status() === FALSE){
      		  
				$this->session->set_flashdata('er_msg', 'Error !!! Candidate Joining report save');	

			}else{
				
				// $hrdemailid    = 'poonamadlekha@pradan.net';
				 $hrdemailid    = 'amit.kum2008@gmail.com';
				 $tcemailid     = $this->loginData->EmailID;

				 $subject = "Submit Joining Report Form ";
				  $body = 	'Dear Sir, <br><br> ';
				  $body .= 'Submit Joining Report Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 $to_email = $hrdemailid;
				 $to_name = $tcemailid;
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);

				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Joining report save');	

				redirect('/candidate/Joining_report_form/view/');		
			}


		  }



     
	}
			$content['token'] = $token;
		 	$content['candidatedetils'] = $this->model->getCandidateDetails($this->loginData->candidateid);
		 	$content['joiningreportdetails'] = $this->model->getJoiningReportDetail($token);
		 	$content['getstatus'] = $this->model->getStatus($this->loginData->candidateid);

		 	$content['getjoiningreport'] = $this->model->getJoiningReport($this->loginData->candidateid);
		   $content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
	 
		 	$content['title'] = 'Joining_report_form';
		 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		 	$this->load->view('candidate/_main_layout', $content);

		 }catch (Exception $e) {
       print_r($e->getMessage());die;
     }

}

	public function view(){ 

			
			$token = $this->loginData->candidateid;
		 	$content['candidatedetils'] = $this->model->getCandidateDetails($this->loginData->candidateid);

		 	$content['joiningreportdetails'] = 
		 	$this->model->getJoiningReportview($this->loginData->candidateid);
		 	$content['getstatus'] = $this->model->getStatus($this->loginData->candidateid);
		 	$content['getjoiningreport']       = $this->model->getJoiningReport($this->loginData->candidateid);
		   $content['getgeneralform'] = $this->model->getGeneralFormStatus($this->loginData->candidateid);
		   
		 	$content['title'] = 'Joining_report_form';
		 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		 	$this->load->view('candidate/_main_layout', $content);

}

		 public function BDFFormSubmit($id)
		 {

		 	$this->db->trans_start();

		 	$sql = 'SELECT * FROM `tbl_candidate_registration` Where `tbl_candidate_registration`.candidateid ='.$id.''; 
		 	$result1 = $this->db->query($sql)->result()[0];
			//print_r($result1); die;

					$sql = 'SELECT `lpooffice`.`email` FROM `tbl_candidate_registration` 
					INNER join  `lpooffice` ON `lpooffice`.`officeid`= `tbl_candidate_registration`.`teamid` WHERE `tbl_candidate_registration`.candidateid ='.$id.''; 
			 	$result2 = $this->db->query($sql)->result()[0];

			//print_r($result2); die;

		 	if ($result1->BDFFormStatus==0) {

		 		$candidateemailid = $result1->emailid; ///// Candidate Email Id ////

		 		$subject = "Candidates Submitted BDF Form ";
		 		$body = read_file(base_url().'mailtext/Candidates_Fill_BDF_Formtext'); 
		 		//$to_email = $result2->email; //// Team Mail Id ////
		 		$to_email1     = 'amit.kum2008@gmail.com'; //// Team Mail Id ////
		 	  $to_email2     = 'amit.kum2008@gmail.com'; /// HRD Mail Id ////
		 		 $to_email3  = 'amit.kum2008@gmail.com';
		 		//$to_candidate  = $result1->emailid; ///// Candidate Email Id ////
		 		$sendmail1 = $this->Common_Model->send_email($subject, $body, $to_email1, $to_name);

			if($sendmail1==1){
				$sendmail2 = $this->Common_Model->send_email($subject, $body, $to_email2, $to_name);
			}

			if ($sendmail2 == 1) {
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email3, $to_name);
			}
		 			
		 		if($sendmail==1){

		 			$updatesubmitedstatus	 = array(

		 				'BDFFormStatus' => '1',
		 			);

		 			$this->db->where('candidateid', $this->loginData->candidateid);
		 			$this->db->update('tbl_candidate_registration', $updatesubmitedstatus);

		 			$this->db->trans_complete();

		 			if ($this->db->trans_status() === FALSE){

		 				// $this->session->set_flashdata('er_msg', 'Error Form Submitted !!');

		 			}else{

		 				// $this->session->set_flashdata('tr_msg', 'Successfully Form Submitted !!');
		 				echo	json_encode('0');
		 			}
		 		}else{
		 			echo	json_encode('1');
		 		}

		 	}

		 }


		


		}