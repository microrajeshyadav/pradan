<?php 
class Staff_sepcacceptanceofresignation extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepcacceptanceofresignation_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("Staff_approval_model");
		$this->load->model("Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			if($token){
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    
			// $content['Staff_sepcacceptanceofresignation']='';
			$sql  = "SELECT flag,workflowid from tbl_workflowdetail where workflowid=(
SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id = $token)";
		    $result  = $this->db->query($sql)->result()[0];
		    
		    $content['workflow_flag']=$result;	
		    $forwardworkflowid = $result->workflowid;

			
			$content['staff_acceptance_resignation'] = $this->Staff_sepcacceptanceofresignation_model->staff_acceptance_resignation($token);
			// echo "<pre>";
			// print_r($content['staff_acceptance_resignation']);
			// die;

			$staffid =  $content['staff_acceptance_resignation']->staffid;

			$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
			// print_r($content['staff_detail']);
			// die;
			
			$content['ed_details']=$this->gmodel->getExecutiveDirectorEmailid();
			//print_r($content['ed_details']);

    		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
        	$receiverdetail = $this->Staff_seperation_model->geteddetail();
			/*echo "<pre>";
			print_r($receiverdetail);exit();*/

			$content['staffid'] = $content['staff_acceptance_resignation']->staffid;

			
			$content['acceptance_detail'] = $this->Staff_sepcacceptanceofresignation_model->acceptance_detail($token);
			$content['acceptance_regination_detail'] = $this->Staff_sepcacceptanceofresignation_model->staff_acceptance_resignation($token);
				// print_r($content['acceptance_detail']);
				// die;
			
			$RequestMethod = $this->input->server("REQUEST_METHOD");


			if($RequestMethod == 'POST')
			{



				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if(trim($this->input->post('comments'))){
					$db_flag = 1;
				}
				
				/*echo "<pre>";
				print_r($db_flag);exit();*/
				$this->db->trans_start();

			

		        $working_date=$this->input->post('working_date')==""?null:$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('working_date'));
		        $resignation_tendered_on=$this->input->post('resignation_tendered_on')==""?null:$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('resignation_tendered_on'));

				$insertArray = array(

					'staffid'                 => $content['staff_acceptance_resignation']->staffid,
					'transid'                 => $token,
					'resignation_tendered_on' => $resignation_tendered_on,
					'working_date'            =>  $working_date,
					'flag'                    => $db_flag,
					'createdby'               => $this->loginData->staffid,
				);


				

				if($content['acceptance_detail'])
				{
					$updateArray = array(

						'staffid'                 => $content['staff_acceptance_resignation']->staffid,
						'transid'                 => $token,
						'resignation_tendered_on' => $resignation_tendered_on,
						'working_date'            => $working_date,
						'flag'                    => $db_flag,
						'updatedby'               => $this->loginData->staffid,

					);

					$this->db->where('id', $content['acceptance_detail']->id);
					$flag = $this->db->update('tbl_acceptance_of_resignation', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_acceptance_of_resignation',$insertArray);
				}
				//echo $this->db->last_query(); die;
				$this->db->trans_complete();
				if($flag) 
				{
					$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
					// redirect(current_url());
				} 
				else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				// redirect(current_url());
			}
		}

		
		$content['acceptance_detail'] = $this->Staff_sepcacceptanceofresignation_model->acceptance_detail($token);
		$content['token'] = $token;
		$content['title'] = 'Staff_sepcacceptanceofresignation';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	  }else{
	  	$this->session->set_flashdata('er_msg','Missing trans id!!');
    	header("location:javascript://history.go(-1)", 'refresh');
	  }
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}




}