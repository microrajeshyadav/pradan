<?php 
class Staffleaveapplied extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staffleaveapplied_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
        $this->load->model('Dompdf_model');
     $this->load->model("General_nomination_and_authorisation_form_model");

		//$this->load->model(__CLASS__ . '_model');
     $mod = $this->router->class.'_model';
     $this->load->model($mod,'',TRUE);
     $this->model = $this->$mod;

     $check = $this->session->userdata('login_data');

		///// Check Session //////	
     if (empty($check)) {

         redirect('login');

     }

     $this->loginData = $this->session->userdata('login_data');
     /*echo "<pre>";
     print_r($this->loginData);die();*/
 }

 public function index()
 {
    try{

  
  $content['office'] = $this->Staffleaveapplied_model->getoffice();
  $officeid = $content['office'][0]->officeid;
  $content['staffofficeid'] = $this->Staffleaveapplied_model->staffemp_code($this->loginData->staffid);
  $RequestMethod = $this->input->server('REQUEST_METHOD'); 
  if($RequestMethod == "POST"){
   // print_r($this->input->post());

   $content['officeid'] = $this->input->post("office");
   $content['month'] = $this->input->post("month"); 
   $content['year'] = $this->input->post("year"); 
   $content['fromdate'] = $this->input->post("fromdate"); 
   $content['todate'] = $this->input->post("todate"); 
   $content['submit'] = $this->input->post("submit");

        $search = $this->input->post('submit');
        $leaveclosing = $this->input->post('approve_leave');
        if($search == 'search'){
            
            $content['ddstatus'] = $this->input->post("ddstatus");
            $content['officeid'] = $this->input->post("office");
            $fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('fromdate'));
            $todate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('todate'));
            if($fromdate!=null && $todate!=null)
            {
                $laeve_array = array (

                    'status'    => $content['ddstatus'],
                    'From_date' => $fromdate,
                    'To_date'   => $todate,
                    'officeid'  => $content['officeid'],
                );
            }
            else if($fromdate==null || $todate==null)
            {
            
                $firstdayofpreviousmonth = date('Y-m-d', strtotime('first day of previous month'));
                $lastdayofpreviousmonth = date('Y-m-d', strtotime('last day of previous month'));	
                $fromdate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($firstdayofpreviousmonth);
                $todate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($lastdayofpreviousmonth);
            	 $laeve_array = array (

                    'status'    => $content['ddstatus'],
                    'From_date' => $fromdate,
                    'To_date'   => $todate,
                    'officeid'  => $content['officeid'],
                );
            }
            if ($this->loginData->RoleID==2 || $this->loginData->RoleID==20 ) {
                $content['staffleavedetails'] = $this->Staffleaveapplied_model->staffleavedetailsforMonthlyClosing($laeve_array);
                
            }
            else
            {
            $content['staffleavedetails'] = $this->Staffleaveapplied_model->staffleavedetails($laeve_array);
            }
            /*echo "<pre>";
            print_r($content['staffleavedetails']);die();*/
        }else if($leaveclosing == 'leaveclosing'){
            $stage='';
            if($this->loginData->RoleID==20)
            {
                $stage=1;
            }
            else {
                $stage=2;

            }
            $officeid = $this->input->post('hdnOfficeID');
            $postArray = $_POST;
          // print_r($postArray);die();
            $arraytwo = array('approve_leave'=>'leaveclosing');
            $postArray1 = array_diff($postArray, $arraytwo);
            // print_r( $postArray1);
            // die;
             $firstdayofpreviousmonth = date('Y-m-d', strtotime('first day of previous month'));
             $lastdayofpreviousmonth = date('Y-m-d', strtotime('last day of previous month'));
            foreach ($postArray1 as $key => $value) {
                if ($value == 'on') {
                    # code...
                
                # code...
               
                $key = explode("_",$key);
                // echo "<pre>";
                // print_r($key);die();
                // echo "dfdsf";
                 $fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('From_date'));
                $todate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('to_date'));
                // $monthly=$this->Staffleaveapplied_model->monthly_details($fromdate,$todate);

                // die;

                if($this->loginData->RoleID==20)
                {
                $insertArray = array(
                    'fromdate' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('From_date')),
                    'todate' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('to_date')),
                    'stage' => 1,
                    'emp_code' => $key[2],
                    'officeid' => $officeid,
                    'createdby' => $this->loginData->staffid,
                    'createdon' => date('Y-m-d'),
                );
            

                $this->db->insert('monthly_leave_closing', $insertArray);
                 }
                 else if($this->loginData->RoleID==2)
                   {
                    $updateArray = array(
                   
                    'stage' => 2,
                    
                    'updatedby' => $this->loginData->staffid,
                    'updatedon' => date('Y-m-d'),
                );
                    $this->db->where('fromdate>=', $fromdate);
                 $this->db->where('todate>=', $fromdate);

                $this->db->update('monthly_leave_closing',$updateArray);



                   }
                 }

                  $updateArray = array(
                    'fromdate' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('From_date')),
                    'todate' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('to_date')),
                    'stage' => 2,
                    'emp_code' => $key[2],
                    'officeid' => $officeid,
                    'createdby' => $this->loginData->staffid,
                    'createdon' => date('Y-m-d'),
                );
            }

            $fromdate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($firstdayofpreviousmonth);
            $todate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($lastdayofpreviousmonth);    
            $laeve_array = array (
                'status'    => 1,
                'From_date' => $fromdate,
                'To_date'   => $todate,
                'officeid'  => $officeid,
            );
            $content['staffleavedetails'] = $this->Staffleaveapplied_model->staffleavedetails($laeve_array);
        }else{
            
            $laeve_array = array (

                'status'    => 1,
                'From_date' => '',
                'To_date'   => '',
                'officeid'  => $officeid,
            );
            $content['staffleavedetails'] = $this->Staffleaveapplied_model->staffleavedetails($laeve_array);
        }

}
else
{
    
    $laeve_array = array (

            'status'    => 1,
            'From_date' => '',
            'To_date'   => '',
            'officeid'  => $officeid,
        );
    $content['staffleavedetails'] = $this->Staffleaveapplied_model->staffleavedetails($laeve_array);
    // print_r($content['staffleavedetails']); die;
}
//die;
    /*echo "<pre>";
    print_r($content['staffofficeid']); die;*/

$content['title'] = 'Staffleaveapplied';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
            print_r($e->getMessage());die;
        }

}


public function sendsanctionletter(){

    // echo $token; die;
try {

   $content['id'] = $this->Staffleaveapplied_model->getEDid();
      // print_r($content['id']); die;

   $var = $content['id']->edid; 
   $edname = $content['id']->UserFirstName.' '.$content['id']->UserMiddleName.' '.$content['id']->UserLastName;

    $content['pid'] = $this->Staffleaveapplied_model->personal_email();
   
   // echo $content['pid']->staffid; die;

    // echo $content['staff_code']->emp_code; die;

   $RequestMethod = $this->input->server('REQUEST_METHOD'); 
  if($RequestMethod == "POST"){

  $empcode = $this->input->post('empcode');
  $descript = $this->input->post('descript');
  $fmdate = $this->input->post('fmdate');
  $tdate = $this->input->post('tdate');
  $mlcid = $this->input->post('mlcid');
  $content['leaverequest'] = $this->Staffleaveapplied_model->getrequestedleave($empcode);

    $currentdate = date('Y-m-d');

    $txtgender='';
    $title = '';
   $fromdate = $this->gmodel->changedatedbformate($content['leaverequest']->From_date);
   $todate = $this->gmodel->changedatedbformate($content['leaverequest']->To_date);

   if ($content['leaverequest']->gender==1) {
     $txtgender = 'Male';
     $title = 'Mr.';
   }elseif ($content['leaverequest']->gender == 2) {
     $txtgender = 'Female';
     $title = 'Ms.';
   }else{
     $txtgender = 'other';
     $title = 'Mr./Ms.';
   }

    // ed signature
            $data = "";
            $data_replace = "";
            $getedname = "";
            $edsign = "";
            $body = "";


            $getedname = $this->gmodel->getExecutiveDirectorEmailid();

            if ($getedname->sign !='') {
                $edsign = site_url('datafiles/signature/'.$getedname->sign);
            }else{ 
                $edsign = site_url('datafiles/signature/Signature.png');
            }

            $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 91 AND `isactive` = '1'";
            $body = $this->db->query($sql)->row();

            $data = array('$leaverequest_Emp_code','$currentdate','$title','$leaverequest_name','$leaverequest_officename','$leaverequest_districtname','$leaverequest_statename','$fromdate','$todate','$edsign','$edname','$description');
            $data_replace = array($content['leaverequest']->Emp_code,$this->gmodel->changedatedbformate($currentdate),$title,$content['leaverequest']->name,$content['leaverequest']->officename,$content['leaverequest']->districtname,$content['leaverequest']->statename,$fromdate,$todate,$edsign,$edname,$descript);

            if(!empty($body))
            $body = str_replace($data,$data_replace,$body->lettercontent);
            $body=  html_entity_decode($body);

            $filename = "";
            $filename = md5(time() . rand(1,1000));
            $this->load->model('Dompdf_model');
            $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Sanctionletter.pdf');

            $updteArray = array(
                    'letter_fromdate' => $this->gmodel->changedatedbformate($fmdate),
                    'letter_todate' => $this->gmodel->changedatedbformate($tdate),
                    'filename' => $filename
                    );
                    $this->db->where('mlcid', $mlcid);
                    $this->db->update('monthly_leave_closing',$updteArray);

                    $this->db->trans_complete();



    if($generate==true){
        // echo "hyih"; die;   
        $this->session->set_flashdata('tr_msg', 'Sanction letter generate successfully');
         redirect('/Staffleaveapplied/index');

    }else{

       $this->session->set_flashdata('er_msg','!!!! error in Sanction letter !!!!');
         // redirect('/Staffleaveapplied/sendsanctionletter/'.$token);

    }

      

    if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Sanction letter data has not been saved');

    }else{
        $this->session->set_flashdata('tr_msg', 'Sanction letter has been prepared successfully');

    }

  }


   // $staff = $content['leaverequest']->staffid;
   // echo $staff; die;

    // $content['forid'] = $this->Staff_leave_approval_personnel_model->get_providentworkflowid($staff);
    // print_r($content['forid']); die;

   
    
} catch (Exception $e) {
    
}

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}




}