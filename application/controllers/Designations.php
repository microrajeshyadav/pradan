<?php 

/**
* State List
*/
class Designations extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     try{
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from  msdesignation where isdeleted='0' ORDER BY desid DESC";
    $content['operation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    $content['title'] = 'Designations';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {

    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('desname','Desigantion Name','trim|required|min_length[1]|max_length[100]');
     
      $this->form_validation->set_rules('levelname','Levelname Name','trim|required|min_length[1]|max_length[1]');



       $this->form_validation->set_rules('shortname','ShortName','trim|required|min_length[1]|max_length[10]');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'desname'      => $this->input->post('desname'),
          'createdon'      => date('Y-m-d H:i:s'),
          'createdby'      => $this->loginData->UserID,
          'shortname'      => $this->input->post('shortname'),
          'level'          =>$this->input->post('levelname'),
          'payscale'       =>$this->input->post('payscale')
        );
      
      $this->db->insert('msdesignation', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added Designation');
      redirect('/Designations/index/');
    }

    prepareview:

   
    $content['subview'] = 'Designations/add';
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  public function edit($token=NULL)
  {
    try{
        // start permission 
     $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('desname','Name','trim|required|min_length[1]|max_length[100]');

      $this->form_validation->set_rules('shortname','Short Name','trim|required|min_length[1]|max_length[10]');

         $this->form_validation->set_rules('levelname','Level Name','trim|required|min_length[1]|max_length[1]');


        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
          'desname'       => $this->input->post('desname'),
          'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,
          'shortname'      => $this->input->post('shortname'),
          'level'          =>$this->input->post('levelname'),
          'payscale'       =>$this->input->post('payscale')
        );
      
      $this->db->where('desid', $token);
      $this->db->update('msdesignation', $updateArr);
 //echo  $this->db->last_query(); die;

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Designations');
       redirect('/Designations/');
    }

    prepareview:

    $content['title'] = 'Designations';
    $operation_details = $this->Common_Model->get_data('msdesignation', '*', 'desid',$token);
    // print_r($operation_details); die();
    $content['operation_details'] = $operation_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
    function delete($token = null)
    {
      try{
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('desid', $token);
      $this->db->update('msdesignation', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Designation Deleted Successfully');
      redirect('/Designations/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}