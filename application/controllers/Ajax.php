<?php
class Ajax extends Ci_controller {

  public function __construct(){
    parent::__construct();
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->loginData = $this->session->userdata('login_data');
  }

  function index($id = NULL) {
    die("No service selected");
  }






  /**
   * Method getStaffList() list of Staff with staffid .
   * @access  public
   * @param Null
   * @return  Array
   */
  
  public function getStaffList($officeid)
  {

    try{

     $sql = "SELECT
     staff.name,
     staff_transaction.staffid,
     staff_transaction.date_of_transfer,
     staff_transaction.new_office_id,
     staff_transaction.new_designation
     FROM
     staff_transaction
     INNER JOIN(
     SELECT
     staff_transaction.staffid,
     MAX(
     staff_transaction.date_of_transfer
     ) AS MaxDate
     FROM
     staff_transaction
     GROUP BY
     staffid
     ) AS TMax
     ON
     `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND staff_transaction.trans_status NOT IN(
     'Resign',
     'Termination',
     'Retirement',
     'Death'
     )

     INNER JOIN `staff` on `staff_transaction`.staffid = `staff`.`staffid` 

     Where  `staff`.new_office_id = '". $officeid ."' ORDER BY  `staff`.staffid ASC ";

     $result = $this->db->query($sql)->result();

     $option = '<option value="">--Select Staff --</option>';
     foreach($result as $row){
      $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
    } 
    echo $option;  


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }  
}




  /*
check email id check in candidate_registration
*/
public function email_editexists()
{

  extract($_POST);
  $sql = "SELECT Count(emailid) as count FROM mstcampus where  campusid NOT IN('".$campus_id."')  and emailid='".$email."'";
  $result = $this->db->query($sql)->result();
  echo $result[0]->count;
  
}

public function leaveclosing()
{
    try{
     // print_r($_POST);
     // die;
      extract($_POST);
      $fromdate=$this->gmodel->changedatedbformate($fromdate);
      $unix = strtotime($fromdate);

$fromdate1= date('Y-n-j', $unix);
      // date("Y-n-j",strtotime($fromdate));
      // echo $fromdate;
      // die;
      $todate=$this->gmodel->changedatedbformate($todate);
       $unix1 = strtotime($todate);
      $todate1= date('Y-n-j', $unix1);
     
    

       $sql = "SELECT Count(*) as count FROM monthly_leave_closing where   ('".$fromdate1."' between fromdate and todate   or '".$todate1."' between fromdate and todate) and officeid='".$office_id."' ";
      
      
      $result = $this->db->query($sql)->row();
        echo $result->count;
  
  

   }catch (Exception $e) {
   print_r($e->getMessage());die;
 }  
  
}
  /*
check email id check in candidate_registration
*/
public function email_exists()
{

  extract($_POST);
  $sql = "SELECT Count(emailid) as count FROM tbl_candidate_registration where emailid='".$email."' ";
  
  $result = $this->db->query($sql)->result();
  
  echo $result[0]->count;
  
}
public function joining()
{
  //print_r($_POST);
 try{
  extract($_POST);

  $sql="select staffid,name,doj,emp_code,designation,msdesignation.desid,msdesignation.desname from staff  inner join msdesignation on staff.designation= msdesignation.desid where staffid=$staff_id";
  //echo $sql;
  $content1=$this->db->query($sql)->row();
    //echo "office name=".$content1->officename."designation=".$content1->desname."employ=".$content1->emp_code;


  $date=$this->gmodel->changedatedbformate($content1->doj);
 // echo $date;
  //die;

   // die();
  $arr1=array('employ_code'=>$content1->emp_code,'doj'=>$date,'designation'=>$content1->desname,'employ_code'=>$content1->emp_code);

  echo json_encode($arr1);
}catch (Exception $e) {
 print_r($e->getMessage());die;
}

}


public function staff_approved()
{
  //print_r($_POST);
 try{

  extract($_POST);

  $updatearr = array(
    'flag' => 0,

  );
  //print_r(expression)

  $this->db->where('staffid',$staff_id);
  $this->db->update('staff',$updatearr);
  return true;

}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


public function staff_list()
{
  //print_r($_POST);

 try{
  extract($_POST);
  
  $sql="select staffid,name,new_office_id from staff where new_office_id=$office_id";
  //echo $sql;
  $arr;
  $content1=$this->db->query($sql)->result();
  foreach($content1 as $key)
  {

//   $val=$key->staffid;
// $arr=$val;

   $list[] = array(
    'staff_id'   => $key->staffid,
    'name' => $key->name,

  );
    //print_r($arr);

 }
 
 echo json_encode($list);

 
}
catch (Exception $e) {
 print_r($e->getMessage());die;
}


}



/*
leave_day function is used calculate  no of leave
retrun row

*/
public function leave_day()
{

 try{
    //print_r($_POST);

  extract($_POST);

  $fromdate=$this->gmodel->changedatedbformate($fromdate);
  $todate=$this->gmodel->changedatedbformate($todate);
  $sql = "select leave_days('$fromdate','$todate',$office_id, $leavetype) as leave_day";

  $result = $this->db->query($sql)->row();

  $list=array(
    'from_date'=>$fromdate,
    'to_date'=>$todate,
    'leave_day'=> $result->leave_day

  );
  echo json_encode($list);

}catch (Exception $e) {
 print_r($e->getMessage());die;
}

}


public function leave_days_availed()
{

  try{
      //print_r($_POST);

    extract($_POST);

    $fromdate= str_replace(" ", "", $this->gmodel->changedatedbformate($fromdate));
    $todate= str_replace(" ", "", $this->gmodel->changedatedbformate($todate));
    $sql = "select a.cnt + b.cnt as cnt from ((select count(*) as cnt from trnleave_ledger  where Leave_transactiontype = 'DR' AND (status =3 OR status=1) AND '". $fromdate ."'  between From_date AND To_date AND Emp_code = $hdnempcode) as a , (select count(*) as cnt from trnleave_ledger where Leave_transactiontype = 'DR' AND (status =3 OR status=1) AND '". $todate ."'  between From_date AND To_date AND Emp_code = $hdnempcode) as b)";
    // echo $sql; die();
    $result = $this->db->query($sql)->row();
    echo $result->cnt;

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}

/*
Checkholidaylist function is Check Holiday list
retrun row
*/

public function Checkholidaylist()
{

 try{

  extract($_POST);
  
  $fromdate=$this->gmodel->changedatedbformate($fromdate);
  $todate=$this->gmodel->changedatedbformate($todate);
  $expfdate = explode('-', $fromdate);
  $exptdate = explode('-', $todate);
  $expfdate[0];
  $exptdate[0];
  $returnvalue;
  $sql = "select count(*) as clyear from tbl_holidaylist where (calender_year = ".$expfdate[0]." or   calender_year = ".$exptdate[0].") and officeid=".$office_id.""; 
  $result = $this->db->query($sql)->row();
    // print_r($result); die();
  if ($expfdate[0] == $exptdate[0] || $result->clyear == 1)
  {
    $returnvalue = 1;

  }
  else if ($expfdate[0] != $exptdate[0] || $result->clyear == 2)
  {

   $returnvalue = 1;
 }
 else
 {
   $returnvalue = 0;
 }
 $arr=array(
  'check'=>$returnvalue
);
 echo json_encode($arr);



}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


/*
leave_cal function is used calculate  no of leave
retrun row

*/
public function leave_cal()
{
  //print_r($_POST);
  try{
    extract($_POST);

    $sql="select maxleaveinayear,minleave from msleavetype where Ltypeid=$leavetype";
    $content1=$this->db->query($sql)->row();
  //print_r($content1);
    $arr=array(
      'maxleave'=>$content1->maxleaveinayear,
      'minleave'=>$content1->minleave
    );
    // print_r($arr);
    // die;
    echo json_encode($arr);



  }
  catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}





public function chooseFieldguideteam($id) {


 $sql = "SELECT staff_transaction.staffid, staff_transaction.date_of_transfer, staff_transaction.new_office_id, staff_transaction.new_designation, staff.name FROM staff_transaction 
 INNER JOIN  staff ON staff_transaction.staffid = staff.staffid  
 INNER JOIN (SELECT staff_transaction.staffid,Max(staff_transaction.date_of_transfer) as MaxDate FROM staff_transaction GROUP BY staffid) as TMax ON staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
 AND staff_transaction.trans_status NOT IN('Resign','Termination','Retirement','Death') AND staff_transaction.`new_office_id`='".$id."' AND  staff_transaction.`new_designation` IN(4,10,16) ORDER BY staff_transaction.staffid";  
      //echo $sql;
      //die;

 $result = $this->Common_Model->query_data($sql);

 $option = '<option value="">--Select --</option>';
 foreach($result as $row){
  $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
} 
echo $option;   
}



public function selectedFieldguideteam() {

  extract($_POST);


  $sql = "SELECT staff_transaction.staffid, staff_transaction.date_of_transfer, staff_transaction.new_office_id, staff_transaction.new_designation, staff.name FROM staff_transaction 
  INNER JOIN  staff ON staff_transaction.staffid = staff.staffid  
  INNER JOIN (SELECT staff_transaction.staffid,Max(staff_transaction.date_of_transfer) as MaxDate FROM staff_transaction GROUP BY staffid) as TMax ON staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
  AND staff_transaction.trans_status NOT IN('Resign','Termination','Retirement','Death') AND staff_transaction.`new_office_id`='".$office_id."' AND  staff_transaction.`new_designation` IN(4,10) ORDER BY staff_transaction.staffid";  



  $result = $this->Common_Model->query_data($sql);




  $sql1 = "SELECT staff_transaction.staffid FROM staff_transaction 
  INNER JOIN  staff ON staff_transaction.staffid = staff.staffid  
  INNER JOIN (SELECT staff_transaction.staffid,Max(staff_transaction.date_of_transfer) as MaxDate FROM staff_transaction GROUP BY staffid) as TMax ON staff_transaction.staffid = TMax.staffid AND staff_transaction.date_of_transfer = TMax.MaxDate
  AND staff_transaction.trans_status NOT IN('Resign','Termination','Retirement','Death')  AND  staff_transaction.staffid=".$staff_id ." ORDER BY staff_transaction.staffid";  



   // $sql1 = "select staff.staffid from staff_transaction  
   // inner join staff on staff.staffid=staff_transaction.fgid 
   // where staff_transaction.staffid=".$staff_id ." order by staffid DESC limit 1";  
  

  $query = $res = $this->db->query($sql1)->row();
       // $row1  = $query->row();

  $option = '<option value=>--Select Field Guide--</option>';
  foreach($result as $row){

    if($row->staffid == $query->staffid){

      $option .= '<option value="'.$row->staffid.'" SELECTED>'.$row->name.'</option>';

    } else{
     $option .= '<option value="'.$row->staffid.'">"'.$row->name.'</option>';
   }
 }
 echo $option;   
}


public function getReportingto($teamid)
{
  try{

    $sql = "SELECT
    `staff_transaction`.staffid,
    `staff`.name,
    `staff`.emp_code,
    `staff`.`designation`,
    `msdesignation`.desname
    FROM
    staff_transaction
    INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid
    INNER JOIN msdesignation ON `msdesignation`.desid = staff.designation
    INNER JOIN(
    SELECT `staff_transaction`.staffid,
    MAX(
    `staff_transaction`.date_of_transfer
    ) AS MaxDate
    FROM
    staff_transaction
    GROUP BY
    staffid
  ) AS TMax
  ON
  `staff_transaction`.staffid = TMax.staffid AND `staff_transaction`.date_of_transfer = TMax.MaxDate AND `staff_transaction`.trans_status NOT IN(
  'Resign',
  'Termination',
  'Retirement',
  'Death'
  ) AND staff_transaction.`new_office_id` = '".$teamid."' AND (staff_transaction.`new_designation` = 4 OR staff_transaction.`new_designation` = 16)
  ORDER BY `staff_transaction`.staffid  DESC ";  
  $res = $this->db->query($sql)->result();

  $option = '<option value="">--Select Reporting to--</option>';

  foreach($res as $row){
    $option .= '<option value="'.$row->staffid.'">'.$row->emp_code.'-'.$row->name.'  ('. $row->desname.')'.'</option>';
  } 

  echo $option; 

}catch(Exception $e){
  print_r($e->getMessage());die();
}
}

  /**
   * Method offer_letter() get the offer letter content.
   * @access  public
   * @param Null
   * @return  Array
   * update (14-8-19) by rajat 
   */

  public function offer_letter($offerid)
  {
    // print_r($offerid); 
   try{

     $offer = explode('_', $offerid);
     $fileno  = $offer[0];
     $doj  = $offer[1];
     $offerno  = $offer[2];
     $lastdateofacceptanceofdocs  = $offer[3];
   // $candidate_arr = $offer[3];

     $doj = $this->gmodel->changedatedbformate($doj);
     $lastdateAcceptdocs = $this->gmodel->changedatedbformate($lastdateofacceptanceofdocs);

     echo json_encode($offerid);
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }

 }


 /**
   * Method getdatransfarstaff() get staff to tranfer.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function getdatransfarstaff($id){

   try{

    $sql = "SELECT 
    staff_transaction.staffid, 
    staff.name 
    FROM 
    staff_transaction 
    left join  tbl_hr_intemation on  tbl_hr_intemation.candidateid = staff_transaction.staffid 
    INNER JOIN staff ON staff_transaction.staffid = staff.staffid 
    INNER JOIN (
    SELECT 
    staff_transaction.staffid, 
    Max(
    staff_transaction.date_of_transfer
    ) as MaxDate 
    FROM 
    staff_transaction 
    GROUP BY 
    staffid
    ) as TMax ON staff_transaction.staffid = TMax.staffid 

    AND staff_transaction.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
    )
    AND tbl_hr_intemation.status = 1
    AND staff_transaction.date_of_transfer = TMax.MaxDate
    AND staff_transaction.`new_office_id` = '$id' 
    AND staff_transaction.`new_designation` IN(13)  
    
    ORDER BY 
    staff_transaction.staffid";

    $result = $this->db->query($sql)->result();

    $option = '<option value="">--Select Staff --</option>';
    foreach($result as $row){
      $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
    } 
    echo $option;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}



  /**
   * Method getTeamStaff() get staff to tranfer.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTeamStaff($id)
  {

    try{

      $sql = "SELECT 
      staff_transaction.staffid, 
      staff.name 
      FROM 
      staff_transaction 
      INNER JOIN staff ON staff_transaction.staffid = staff.staffid 
      INNER JOIN tbl_hr_intemation ON staff_transaction.staffid = tbl_hr_intemation.staffid 

      INNER JOIN (
      SELECT 
      staff_transaction.staffid, 
      Max(
      staff_transaction.date_of_transfer
      ) as MaxDate 
      FROM 
      staff_transaction 
      GROUP BY 
      staffid
      ) as TMax ON staff_transaction.staffid = TMax.staffid 
      AND staff_transaction.date_of_transfer = TMax.MaxDate 
      AND staff_transaction.trans_status NOT IN(
      'Resign', 'Termination', 'Retirement', 
      'Death'
      ) 
      AND staff_transaction.`new_office_id` = '$id' 
      AND staff_transaction.`new_designation` IN(13) 
      AND staff.probation_completed = 0
      AND tbl_hr_intemation.staffid in(SELECT staffid FROM `tbl_hr_intemation` WHERE intemation_type = 2)
      ORDER BY 
      staff_transaction.staffid"; 

      $result = $this->db->query($sql)->result();

      $option = '<option value="">--Select Staff --</option>';
      foreach($result as $row){
        $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
      } 
      echo $option;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }





  /**
   * Method getTeamStaff() get staff to tranfer.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getTeamStaffTransfer($id)
  {

    try{

      $sql = "SELECT 
      staff_transaction.staffid, 
      staff.name 
      FROM 
      staff_transaction 
      INNER JOIN staff ON staff_transaction.staffid = staff.staffid 
      INNER JOIN tbl_hr_intemation ON staff_transaction.staffid = tbl_hr_intemation.staffid
      INNER JOIN (
      SELECT 
      staff_transaction.staffid, 
      Max(
      staff_transaction.date_of_transfer
      ) as MaxDate 
      FROM 
      staff_transaction 
      GROUP BY 
      staffid
      ) as TMax ON staff_transaction.staffid = TMax.staffid 
      AND staff_transaction.date_of_transfer = TMax.MaxDate 
      AND staff_transaction.trans_status NOT IN(
      'Resign', 'Termination', 'Retirement', 
      'Death'
      ) 
      AND staff_transaction.`new_office_id` = '$id' 
      AND staff_transaction.`new_designation` IN(13) 
      AND staff.probation_completed = 0
      AND tbl_hr_intemation.staffid in(SELECT staffid FROM `tbl_hr_intemation` WHERE intemation_type = 4)

      ORDER BY 
      staff_transaction.staffid"; 

      $result = $this->db->query($sql)->result();

      $option = '<option value="">--Select DA Name --</option>';
      foreach($result as $row){
        $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
      } 
      echo $option;

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

/**
   * Method getGenerateOfferLetter() get generate Offer Letter .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getGenerateOfferLetter($id)
{
  try{

    $this->db->where('flag','Generate');
    $this->db->where('candidateid',$id);
    $query = $this->db->get('tbl_generate_offer_letter_details');

    if ($query->num_rows() > 0){
      echo  1;
    }else{

      $this->db->where('candidateid',$id);
      $query = $this->db->get('tbl_candidate_registration')->result()[0];
     // echo $this->db->last_query(); die;
      if ($query->categoryid ==1) {
        if (empty($query->teamid) || empty($query->batchid)) 
        {
          echo 0;
        }else{

          echo 2;
        }

      }else{

        if (empty($query->teamid)) 
        {
          echo 0;
        }else{

          echo 2;
        }

      }



      die;
    }

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}

/**
   * Method getCampusInchargeName() get campus incharge name .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCampusInchargeName($id){

  try{


   $sqlcamp = "SELECT max(id) as campusintimationid FROM `tbl_campus_intimation`";  
   $campusresult = $this->db->query($sqlcamp)->result()[0];
      // print_r($campusresult->campusintimationid);
   if (isset($campusresult->campusintimationid) && $campusresult->campusintimationid !='NULL') {

    $campusintimationid = $campusresult->campusintimationid;

    $autoincrementid = $campusintimationid +1;

  }else{

    $autoincrementid = 1;
  }

  $sql = "SELECT * FROM `mstcampus` WHERE `campusid` = '".$id."'";  
  $result = $this->db->query($sql)->result();
        // echo "<pre>";
        // print_r($result);
        //die;
  $option = '';
  foreach($result as $row){
    //print_r($row);
    $option .= ' <input type="text"  minlength="4" maxlength="50"  data-toggle="tooltip" title="Campus Incharge Name" name="campusinchargename" id="campusinchargename" value="'.$row->campusincharge.'" class="form-control" readonly="readonly">
    <input type="hidden" name="campusintimationid" id="campusintimationid" value="'.$autoincrementid.'" class="form-control">';
  } 

  echo $option;
  /*echo $data->file_path;exit;*/
  if ($isTouch = isset($data)) {
    read_file($data->file_path) ;
  }

}catch(Exception $e){
  print_r($e->getMessage());die();
}


} 


  // public function getCampusInchargeDetail($id,$fromdate,$todate){

  // try{

  //   //  echo $id."<br>";




  //       $sql = "SELECT * FROM `mstcampus` WHERE `campusid` = '".$id."'";  
  //       $result = $this->db->query($sql)->result()[0];
  //      //print_r($result);
  //       $campusid = $id; 
  //       $cat_id = 1;
  //       $completeurl = $campusid.'/'.$autoincrementid.'/'.$cat_id; 
  //       $enstr=base64_encode($completeurl); //
  //       $url_to_be_send=urlencode($enstr);                
  //       $addlink = site_url().'CandidatesInfo/add/'.$url_to_be_send;
  //       $link = '<a href='.$addlink.'>Click here</a>' ;
  //       $campmess = $campusmessage.$link;

  //       // echo   $shudelfromdate = $frdate; 
  //       // echo $shudeltodate = $tdate;

  //       $message  = "";
  //       $expinchargename = explode(' ', $result->campusincharge);
  //       $UserFirstName = $expinchargename[0];
  //       $EmailID       = $result->emailid;
  //       $name          = $UserFirstName;
  //       $fromdate      = $fromdate;
  //       $todate        = $todate;
  //       $campuslink    = $link;
  //       $staffname     = $this->loginData->UserFirstName; ///change name
  //       $designation   = 'HRD';

  //       $filepath =  site_url()."mailtext/Campusinchargeintimation.txt";
  //       $fd = fopen($filepath, "r"); 
  //       $message .=fread($fd,4096);
  //       eval ("\$message = \"$message\";");
  //       echo $body = nl2br($message);



  //    //echo  $body = preg_replace("<br />", "", $message);

  //   }catch(Exception $e){
  //             print_r($e->getMessage());die();
  //     }


  // } 


/**
   * Method getCampusIntimationToDateDetail() get campus incharge name .
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCampusIntimationToDateDetail($intimationid){

  try{

     // echo $intimationid;

   $campstring = explode('_', $intimationid);
   $campusid  = $campstring[0];
   $schedule_fromdate  = $campstring[1];
   $schedule_todate  = $campstring[2];
   $categoryid  = $campstring[3];

   $sqlcamp = "SELECT max(id) as campusintimationid FROM `tbl_campus_intimation`";  
   $campusresult = $this->db->query($sqlcamp)->result()[0];
   if (isset($campusresult->campusintimationid) && $campusresult->campusintimationid !='') {

    $campusintimationid = $campusresult->campusintimationid;

    $autoincrementid = $campusintimationid +1;

  } else{

    $autoincrementid = 1;
  }
  $sql = "SELECT * FROM `mstcampus` WHERE `campusid` = '".$campusid."' AND isdeleted=0 ";  
  $result = $this->db->query($sql)->row();
      // print_r($result);
       // $campusid = $id; 

        //$cat_id = 1;
  $cat_id = $categoryid;
  $completeurl = $campusid.'/'.$autoincrementid.'/'.$cat_id; 
        $enstr=base64_encode($completeurl); //
        $url_to_be_send=urlencode($enstr);  
        //echo  $url_to_be_send;   
        if ($campusid==0) {
          $addlink = site_url().'Candidate_registration/add/'.$url_to_be_send;
        }else{
         $addlink = site_url().'Candidatesinfo/add/'.$url_to_be_send;
       }           
       
       $link = '<a href='.$addlink.'>Click here</a>' ;
       if ($isTouch = isset($campusmessage)) {
        $campmess = $campusmessage.$link;
      }


        // echo   $shudelfromdate = $frdate; 
        // echo $shudeltodate = $tdate;

      $message  = "";
      $expinchargename = explode(' ', $result->campusincharge);
      $UserFirstName = $expinchargename[0];
      $EmailID       = $result->emailid;
      $name          = $UserFirstName;
      $fromdate      = $schedule_fromdate;
      $todate        = $schedule_todate;
      $campuslink    = $link;
      $campusname    = $result->campusname;
      $date          = $schedule_fromdate;

      $staffname     = $this->loginData->UserFirstName; ///change name
      $designation   = 'HRD';

      $filepath =  site_url()."mailtext/Campusinchargeintimation.txt";
      $fd = fopen($filepath, "r"); 
      $message .=fread($fd,4096);
      eval ("\$message = \"$message\";");
      echo $body = nl2br($message);

     //echo  $body = preg_replace("<br />", "", $message);

    }catch(Exception $e){
      print_r($e->getMessage());die();
    }


  } 





    /**
   * Method getPayscaledetail() get Pay scale details .
   * @access  public
   * @param Null
   * @return  Array
   */

    public function getPayscaledetail($lavlename, $financialyear){

      try{


       $sqllevel = "SELECT inc.*, d.payscale, maxi.maxnetsalary, payscale FROM `tblsalary_increment_slide` inc left join (select payscale, level from msdesignation group BY payscale, level) d on inc.level = d.level left join (select max(netsalary) maxnetsalary , level from tblsalary_increment_slide group by level) maxi on inc.level = maxi.level inner join (select distinct fyear  from tblsalary_increment_slide group by level, REPLACE(fyear,'-','') having count(*) > 3) maxfyear on   inc.fyear = maxfyear.fyear  WHERE inc.phase >= 0 AND inc.level = '".$lavlename."' AND inc.fyear ='".$financialyear."' order by phase ASC"; 

       $lavelresult = $this->db->query($sqllevel)->result();
       if (count($lavelresult) > 0) {
       // echo count($lavelresult);

         $table = '
         <tr>
         <td>Start <span style="color:red;"> * </span> </td>
         <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="Startphasebasic" name="phasebasic[]" required="required" value='.$lavelresult[0]->netsalary.' /> </td>
         <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="Startphaseinc" name="phaseinc[]" required="required" value='.$lavelresult[0]->increment.' /> </td>
         <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="Startnoofyear" name="noofyear[]" value='.$lavelresult[0]->noofyear.' required="required" /></td>
         </tr>

         <tr>
         <td>First Phase <span style="color:red;"> * </span> </td>
         <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="firstphasebasic" name="phasebasic[]" required="required" value='.$lavelresult[1]->netsalary.' /> </td>
         <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right"  max="9000" id="firstphaseinc" name="phaseinc[]" required="required"  value='.$lavelresult[1]->increment.' /> </td>
         <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="firstnoofyear" name="noofyear[]" required="required" value='.$lavelresult[1]->noofyear.' /></td>
         </tr> 
         <tr>
         <td>Second Phase <span style="color:red;"> * </span> </td>
         <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="secondphasebasic" name="phasebasic[]" required="required" value='.$lavelresult[2]->netsalary.' /> </td>
         <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="secondphaseinc" name="phaseinc[]" required="required" value='.$lavelresult[2]->increment.' /> </td>
         <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="secondnoofyear" name="noofyear[]" required="required" value='.$lavelresult[2]->noofyear.' /></td>
         </tr> 
         <tr>
         <td>Third Phase <span style="color:red;"> * </span> </td>
         <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="thirdphasebasic" name="phasebasic[]" required="required" value="'.$lavelresult[3]->netsalary.'"/> </td>
         <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="thirdphaseinc" name="phaseinc[]" required="required"  value='.$lavelresult[3]->increment.' /> </td>
         <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="thirdnoofyear" name="noofyear[]" required="required" value='.$lavelresult[3]->noofyear.' /></td>
         </tr> 
         <tr>
         <td>Fourth Phase <span style="color:red;"> * </span> </td>
         <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="fourthphasebasic" name="phasebasic[]" required="required" value='.$lavelresult[4]->netsalary.' /> </td>
         <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="fourthphaseinc" name="phaseinc[]" required="required" value='.$lavelresult[4]->increment.' /> </td>
         <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="fourthnoofyear" name="noofyear[]" required="required" value='.$lavelresult[4]->noofyear.' /></td>
         </tr> 

         <tr>
         <td>Last Basic <span style="color:red;"> * </span> </td>
         <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="200000" id="lastbasicsal" name="lastbasicsal" required="required" value='.$lavelresult[4]->maxnetsalary.' /> </td>
         <td> </td>
         <td></td>
         </tr> 

         <tr>
         <td>Slide<span style="color:red;"> * </span> </td>
         <td><input type="number" min="0" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="5000" id="slide" name="slide" required="required" value='.$lavelresult[4]->slide.' /> </td>
         <td> </td>
         <td></td>
         </tr> 
         <tr>
         <td>Pay Scale<span style="color:red;"> * </span> </td>
         <td colspan="3"><input type="text" maxlength="100" id="payscale" name="payscale" required="required" class="form-control" value='.$lavelresult[4]->payscale.' /> </td>
         </tr>';
       }else{
        $financialyear = explode('-', $financialyear);
        $financialyear = ($financialyear[0]-1).'-'.($financialyear[1]-1);
        $sqllevel = "SELECT inc.*, d.payscale, maxi.maxnetsalary, payscale FROM `tblsalary_increment_slide` inc left join (select payscale, level from msdesignation group BY payscale, level) d on inc.level = d.level left join (select max(netsalary) maxnetsalary , level from tblsalary_increment_slide group by level) maxi on inc.level = maxi.level inner join (select distinct fyear  from tblsalary_increment_slide group by level, REPLACE(fyear,'-','') having count(*) > 3) maxfyear on   inc.fyear = maxfyear.fyear  WHERE inc.phase >= 0 AND inc.level = '".$lavlename."' AND inc.fyear ='".$financialyear."' order by phase ASC"; 

        $lavelresult = $this->db->query($sqllevel)->result();
          /*$lastbasicsal1 = $lavelresult[4]->maxnetsalary+$lavelresult[4]->slide;
          $lastbasicsal =  number_format($lastbasicsal,2);
          echo $lastbasicsal;*/
          if (count($lavelresult) > 0) {
            $table = '
            <tr>
            <td>Start <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="Startphasebasic" name="phasebasic[]" required="required" value='.($lavelresult[0]->netsalary+$lavelresult[4]->slide).' /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="Startphaseinc" name="phaseinc[]" required="required" value='.$lavelresult[0]->increment.' /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="Startnoofyear" name="noofyear[]" value='.$lavelresult[0]->noofyear.' required="required" /></td>
            </tr>

            <tr>
            <td>First Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="firstphasebasic" name="phasebasic[]" required="required" value='.($lavelresult[1]->netsalary+$lavelresult[4]->slide).' /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right"  max="9000" id="firstphaseinc" name="phaseinc[]" required="required"  value='.$lavelresult[1]->increment.' /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="firstnoofyear" name="noofyear[]" required="required" value='.$lavelresult[1]->noofyear.' /></td>
            </tr> 
            <tr>
            <td>Second Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="secondphasebasic" name="phasebasic[]" required="required" value='.($lavelresult[2]->netsalary+$lavelresult[4]->slide).' /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="secondphaseinc" name="phaseinc[]" required="required" value='.$lavelresult[2]->increment.' /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="secondnoofyear" name="noofyear[]" required="required" value='.$lavelresult[2]->noofyear.' /></td>
            </tr> 
            <tr>
            <td>Third Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="thirdphasebasic" name="phasebasic[]" required="required" value="'.($lavelresult[3]->netsalary+$lavelresult[4]->slide).'"/> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="thirdphaseinc" name="phaseinc[]" required="required"  value='.$lavelresult[3]->increment.' /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="thirdnoofyear" name="noofyear[]" required="required" value='.$lavelresult[3]->noofyear.' /></td>
            </tr> 
            <tr>
            <td>Fourth Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="fourthphasebasic" name="phasebasic[]" required="required" value='.($lavelresult[4]->netsalary+$lavelresult[4]->slide).' /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="fourthphaseinc" name="phaseinc[]" required="required" value='.$lavelresult[4]->increment.' /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="fourthnoofyear" name="noofyear[]" required="required" value='.$lavelresult[4]->noofyear.' /></td>
            </tr> 

            <tr>
            <td>Last Basic <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="200000" id="lastbasicsal" name="lastbasicsal" required="required" value='.($lavelresult[4]->maxnetsalary+$lavelresult[4]->slide).' /> </td>
            <td> </td>
            <td></td>
            </tr> 

            <tr>
            <td>Slide<span style="color:red;"> * </span> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="5000" id="slide" name="slide" required="required" value='.$lavelresult[4]->slide.' /> </td>
            <td> </td>
            <td></td>
            </tr> 
            <tr>
            <td>Pay Scale<span style="color:red;"> * </span> </td>
            <td colspan="3"><input type="text" maxlength="100" id="payscale" name="payscale" required="required" class="form-control" value='.($lavelresult[0]->netsalary).'-'.$lavelresult[0]->increment.'/2-'.($lavelresult[1]->netsalary+$lavelresult[4]->slide).'-'.$lavelresult[1]->increment.'/2-'.($lavelresult[2]->netsalary+$lavelresult[4]->slide).'-'.$lavelresult[2]->increment.'/5-'.($lavelresult[3]->netsalary+$lavelresult[4]->slide).'-'.$lavelresult[3]->increment.'/8-'.($lavelresult[4]->netsalary+$lavelresult[4]->slide).'-'.$lavelresult[4]->increment.'/18-'.($lavelresult[4]->maxnetsalary+$lavelresult[4]->slide).' /> </td>
            </tr>';
          //+$lavelresult[4]->Slide
          }else{
            $table = '
            <tr>
            <td>Start <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="Startphasebasic" name="phasebasic[]" required="required" value="" /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="Startphaseinc" name="phaseinc[]" required="required" value="" /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="Startnoofyear" name="noofyear[]" value="" required="required" /></td>
            </tr>

            <tr>
            <td>First Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="firstphasebasic" name="phasebasic[]" required="required" value="" /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right"  max="9000" id="firstphaseinc" name="phaseinc[]" required="required"  value="" /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="firstnoofyear" name="noofyear[]" required="required" value="" /></td>
            </tr> 
            <tr>
            <td>Second Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="secondphasebasic" name="phasebasic[]" required="required" value="" /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="secondphaseinc" name="phaseinc[]" required="required" value="" /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="secondnoofyear" name="noofyear[]" required="required" value="" /></td>
            </tr> 
            <tr>
            <td>Third Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="thirdphasebasic" name="phasebasic[]" required="required" value=""/> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="thirdphaseinc" name="phaseinc[]" required="required"  value="" /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="thirdnoofyear" name="noofyear[]" required="required" value="" /></td>
            </tr> 
            <tr>
            <td>Fourth Phase <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="100000" id="fourthphasebasic" name="phasebasic[]" required="required" value="" /> </td>
            <td><input type="number" min="100" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="9000" id="fourthphaseinc" name="phaseinc[]" required="required" value="" /> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==2) return false;"class="isNumberKey form-control text-right" max="50" id="fourthnoofyear" name="noofyear[]" required="required" value="" /></td>
            </tr> 

            <tr>
            <td>Last Basic <span style="color:red;"> * </span> </td>
            <td><input type="number" min="5000" onKeyPress="if(this.value.length==6) return false;"class="isNumberKey form-control text-right" max="200000" id="lastbasicsal" name="lastbasicsal" required="required" value="" /> </td>
            <td> </td>
            <td></td>
            </tr> 

            <tr>
            <td>Slide<span style="color:red;"> * </span> </td>
            <td><input type="number" min="0" onKeyPress="if(this.value.length==4) return false;"class="isNumberKey form-control text-right" max="5000" id="slide" name="slide" required="required" value="" /> </td>
            <td> </td>
            <td></td>
            </tr> 
            <tr>
            <td>Pay Scale<span style="color:red;"> * </span> </td>
            <td colspan="3"><input type="text" maxlength="100" id="payscale" name="payscale" required="required" class="form-control" value="" /> </td>
            </tr>';
          }
        }
        if (isset($table)) {
          echo $table;
        }


      }catch(Exception $e){
        print_r($e->getMessage());die();
      }





    } 


  /**
   * Method getfinicaldetails() get finicalyear details.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function getfinicaldetails($financial_year){

    try{

      $sql = "SELECT * FROM salaryheads WHERE monthlyinput= 1 and financial_year = '$financial_year'";
      $result = $this->db->query($sql)->result();
      $data= '';
      if(!empty($result))
      {
       $table= '<table id="table" style="width:"<?php if(count($result) == 1) {  echo 33%;  } ?><tbody>';

       $i=1;
       foreach($result as $value)
       {
         $data .='<tr>
         <input type="hidden" id="input_'.$i.'" name="input[]" >
         <input type="hidden" id="at_'.$i.'" name="at[]" >
         <td>
         <p> '.$value->fielddesc.'</p>
         </td>
         <td > 
         <input type="text" name="amt_'.$i.'" id="amt_'.$i.'" onblur="intTo(this)" onKeyPress="if(this.value.length==6) return false;" class="form-control text-right" required>
         </td>
         </tr>
         <script>
         var cellid= "'.$i.'"; 
         var amt="#amt_"+cellid; 
         var input ="#input_"+cellid; 
         var at ="#at_"+cellid; 
         var fldname = "'.$value->fieldname.'";
         $(input).val(fldname);
         </script>';
         $i++;
       } 
       $data .= '</tbody>
       </table>
       <script>function input(){
        var cellid= 1; 
        var amt="#amt_"+cellid;
        var at ="#at_"+cellid; 
        $(at).val($(amt).val());
        if($(amt).val()!="")
        {
          document.forms["frmgenratesalary"].submit();
        }
        else{alert("Please input value");}
        
      }
      </script>';

      echo $table.$data;
    }else{
      echo "No Records Found!";
    }   

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }
}




/**
   * Method getRecruitersHead() get recruiters head name.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getRecruitersHead($id){

  try{

   $sql = "SELECT * FROM mapping_campus_recruiters INNER JOIN `staff` ON mapping_campus_recruiters.`recruiterid` = staff.`staffid` Where `campusid` = '".$id."' AND `isdeleted`='0'";   
   $result = $this->db->query($sql)->result();    
       // print_r($result[0]);
   $option = '<option value="">--Select Anchor--</option>';
   foreach($result as $row){
    $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
  } 
  echo $option;   

}catch(Exception $e){
  print_r($e->getMessage());die();
}


} 


/**
   * Method getCampuslistdata() get campus list data.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getCampuslistdata($id){

  try{

   $sql = "SELECT * FROM mapping_campus_recruiters INNER JOIN `staff` ON mapping_campus_recruiters.`recruiterid` = staff.`staffid` Where `campusid` = '".$id."' AND `isdeleted`='0'";   
   $result = $this->db->query($sql)->result();    
       // print_r($result[0]);
   $option = '<option value="">--Select Field Guide--</option>';
   foreach($result as $row){
    $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
  } 
  echo $option;   

}catch(Exception $e){
  print_r($e->getMessage());die();
}
} 

/**
   * Method get_Development_Cluster_Team() get Development Cluster Team.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_Development_Cluster_Team($id)
{
  try{

         $sql = "SELECT `lpooffice`.`officeid`, `lpooffice`.`officename` FROM `lpooffice` INNER JOIN  `dc_team_mapping` ON `dc_team_mapping`.`teamid` = `lpooffice`.`officeid` WHERE `dc_team_mapping`.`dc_cd` =$id ";  // die;
         $result = $this->db->query($sql)->result();    
       // print_r($result[0]);
         $option = '<option value="">--Select Team --</option>';
         foreach($result as $row){
          $option .= '<option value="'.$row->officeid.'">'.$row->officename.'</option>';
        } 
        echo $option; 
        
      }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }

/**
   * Method get_Central_Event_Details() get central event details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_Central_Event_Details($id)
{
  try{

          $sql = "SELECT `tbl_central_event`.`from_date`,`tbl_central_event`.`to_date`,`tbl_central_event`.`place` FROM `tbl_central_event` WHERE `tbl_central_event`.`id` =$id ";  // die;
          $result = $this->db->query($sql)->result();    
          echo json_encode($result);



        }catch (Exception $e) {
         print_r($e->getMessage());die;
       }
     }


/**
   * Method get_Central_trans_Details() get central trans details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function get_Central_trans_Details($id)
{
  try{
    $sql1 = " SELECT `resouce_person_id` FROM `tbl_central_event_transaction` WHERE `central_event_id`=$id ";

    $result1 = $this->db->query($sql1)->result();

    echo json_encode($result1);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

/**
   * Method getDistrict() get district details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getDistrict($id)
{
  try{

    $sql = "SELECT * FROM `district` WHERE `stateid`=$id AND `isdeleted`=0"; 

    $result = $this->db->query($sql)->result();

    $option = '<option value="">--Select District --</option>';
    foreach($result as $row){
      $option .= '<option value="'.$row->districtid.'">'.$row->name.'</option>';
    } 
    echo $option; 


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}
public function getrecomand($id)
{

  // print_r($id);
  // die;
  try{

    $sql = "SELECT * FROM `trnleave_ledger` WHERE `id`=$id "; 

    $result = $this->db->query($sql)->row();

    $supervisior=$result->supervisiorview;
    // echo  $supervisior;
    // die;
    // print_r($result);
    // die;

    $html='<p><strong>'.$supervisior.'</strong>';


    echo  $html; 


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


/**
   * Method getbatch() get Active Batch .
   * @access  public
   * @param 
   * @return  array
   *
   */


public function getbatch($id)
{
  try{

    $sql = "SELECT * FROM `mstbatch` WHERE `financial_year`=$id  And `status`= 0 "; 

    $result = $this->db->query($sql)->result();

    $option = '<option value="">--Select Batch --</option>';
    foreach($result as $row){
      $option .= '<option value="'.$row->id.'">'.$row->batch.'</option>';
    } 
    echo $option; 


  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}


  /**
   * Method getfinanacialYear() get Active Batch .
   * @access  public
   * @param 
   * @return  array
   *
   */


  public function getfinancialyear($id)
  {
    try{

     $sql = "SELECT a.id, a.financialyear FROM `mstfinancialyear` as a inner join `mstbatch` as b ON a.`id` = b.`financial_year` WHERE b.`id`=$id And b.`status`= 0 And a.`isdeleted`= 0"; 

     $result = $this->db->query($sql)->row();

     if (count($result) > 0) {

       echo '<input type="hidden" name="financialyear"  id="financialyear" class="form-control" value="'.$result->id.'"><input type="text" name="financialyearname"  id="financialyearname" class="form-control" value="'.$result->financialyear.'" required="required" readonly="readonly">';
     }else{

       echo '<input type="hidden" name="financialyear"  id="financialyear" class="form-control" value=""><input type="text" name="financialyearname"  id="financialyearname" class="form-control" value="" required="required" readonly="readonly">';
     }

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }


/**
   * Method getSingleResoucePerson() get all staff .
   * @access  public
   * @param 
   * @return  array
   *
   */
public function getSingleResoucePerson($token=NULL)
{

  try{

    $sql = " SELECT `resouce_person_name` FROM `tbl_central_event_transaction` WHERE `central_event_id`=$token ";

    $result = $this->db->query($sql)->result();

    return $result;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}


    /**
   * Method get_DAshipid() get all DA Ship .
   * @access  public
   * @param 
   * @return  array
   *
   */

    public function get_DAshipid($id){

     try{

    //         echo  $sql = "SELECT  Max(`tbl_probation_separation`.`probation_extension_date`) as `CMaxDate` 
    // FROM tbl_probation_separation  where da =".$id;

      $sql = "SELECT 
      staff_transaction.staffid,staff.doj, 
      staff.name 
      FROM 
      staff_transaction 

      INNER JOIN staff ON staff_transaction.staffid = staff.staffid 
      INNER JOIN (
      SELECT 
      staff_transaction.staffid, 
      Max(
      staff_transaction.date_of_transfer
      ) as MaxDate 
      FROM 
      staff_transaction 
      GROUP BY 
      staffid
      ) as TMax ON staff_transaction.staffid = TMax.staffid 
      AND staff_transaction.date_of_transfer = TMax.MaxDate 
      AND staff_transaction.trans_status NOT IN(
      'Resign', 'Termination', 'Retirement', 
      'Death'
      ) 
      AND staff_transaction.`new_designation` IN(13) 
      AND staff.`staffid` = $id
      ORDER BY 
      staff_transaction.staffid";

      $query1 = $this->db->query($sql);
      $qucount = $query1->num_rows();
      $data = $this->db->query($sql)->result()[0];
           // print_r($data);


            // if ($qucount > 0) {

            //    echo $result = '<input type="text" name="probation_completed_date" id="probation_completed_date" class="form-control"  value='. $this->changedatedbformate($data->doj).'>';
            // }else{


      $dateofjoining =  $data->doj;
      $expvar = explode('-',$dateofjoining);
      $newDateformate = $expvar[2].'/'.$expvar[1].'/'.$expvar[0];

      $date = $dateofjoining;
      $date = strtotime($date);
      $new_date = strtotime('+ 1 year', $date);
      $probation_completed_date =  date('d/m/Y', $new_date);
      
      echo $result = '<input type="text" name="probation_completed_date" id="probation_completed_date" class="form-control"  value='. $probation_completed_date.'>';
           // }




    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }


  }



 /**
   * Method get_DAshipid() get all DA Ship .
   * @access  public
   * @param 
   * @return  array
   *
   */

 public function get_Development_Apprentice($id){

   try{

    $sql = "SELECT 
    staff_transaction.staffid, 
    staff.name 
    FROM 
    staff_transaction 
    INNER JOIN staff ON staff_transaction.staffid = staff.staffid 
    INNER JOIN tbl_hr_intemation ON staff_transaction.staffid = tbl_hr_intemation.staffid
    INNER JOIN (
    SELECT 
    staff_transaction.staffid, 
    Max(
    staff_transaction.date_of_transfer
    ) as MaxDate 
    FROM 
    staff_transaction 
    GROUP BY 
    staffid
    ) as TMax ON staff_transaction.staffid = TMax.staffid 
    AND staff_transaction.date_of_transfer = TMax.MaxDate 
    AND staff_transaction.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
    ) 
    AND staff_transaction.`new_office_id` = '$id' 
    AND staff_transaction.`new_designation` IN(13) 

    AND staff.probation_completed = 0
    AND tbl_hr_intemation.staffid in(SELECT staffid FROM `tbl_hr_intemation` WHERE intemation_type = 1)
    ORDER BY 
    staff_transaction.staffid";  

    $result = $this->db->query($sql)->result();

    $option = '<option value="">--Select Staff --</option>';

    foreach($result as $row){
      $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
    } 
    echo $option;


  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }


}


 /**
   * Method is_exists() get email id exist .
   * @access  public
   * @param 
   * @return  array
   *
   */

 public function is_exists($email_id)
 { 
  try{

    echo $email_id; die;

    echo $sql = "SELECT Count(emailid) as count FROM tbl_candidate_registration where emailid='".$email_id."' ";
    $result = $this->db->query($sql)->result();

    return $result[0]->count;

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}


 /**
   * Method getYPRResponse() get YPR Respone .
   * @access  public
   * @param 
   * @return  array
   *
   */

 public function getYPRResponse(){

   try{

    // $sql = "SELECT file_path , SUBSTR(file_path,  LOCATE('http://',file_path),case when LOCATE('mailtext/',file_path) = 0 then LOCATE('mailtext/',file_path) else LOCATE('mailtext/',file_path) end - LOCATE('http://',file_path)) cgh FROM `tbl_ypr_response_master` 
    // Where `isdeleted` = '0' AND id=".$id; 

    $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 2 AND `isactive` = '1' AND id = 4 ";
    $data = $this->db->query($sql)->row();
    // echo "string"; 
    // print_r($data->lettercontent); die();

    if (count($data)>0) {
    //    # code...

    //  $newfilepath =  $data->file_path;
    //  $newfilepath = str_replace($data->cgh, site_url(),  $data->file_path); 
      echo $data->lettercontent;
     // print_r($data->lettercontent);
    }
    else
    {
     echo $data = 'File is deleted, please contact to IT team for support.';
   }


 }catch (Exception $e) {
  print_r($e->getMessage());die;
}


}


 /**
   * Method extension_letter() get Extenasion Letter for candidate .
   * @access  public
   * @param 
   * @return  array
   *
   */

 public function extension_letter($staff,$commenttransfer,$dateoftransfer,$effective_date,$becoming_executive_date){

   try{

    // $sql = "SELECT file_path , SUBSTR(file_path,  LOCATE('http://',file_path),case when LOCATE('mailtext/',file_path) = 0 then LOCATE('mailtext/',file_path) else LOCATE('mailtext/',file_path) end - LOCATE('http://',file_path)) cgh FROM `tbl_ypr_response_master` 
    // Where `isdeleted` = '0' AND id=".$id;

    $staffid = $staff;
    $comment = $commenttransfer;
    $dateoftransfer = $dateoftransfer;
    $effective_date = $effective_date;
    $becoming_executive_date = $becoming_executive_date;



    $months = ceil(abs($effective_date - $dateoftransfer) / 86400);

    $getstaffdetail =  $this->model->getDAExtentionDetails($staffid);
    $gethrddetail =  $this->model->gethrDetails($staffid);
    $gethrunitdetail =  $this->model->getHRunitDetails();

    $dateoftransferdb        =  $this->gmodel->changedatedbformate($dateoftransfer);
    $effective_db_date       = $this->gmodel->changedatedbformate($effective_date);
        // $getcentraleventdetail   =  $this->model->getCentralEventDetails($effective_db_date, $dateoftransferdb);

       // $comeingeventdate = date("F, Y", strtotime($getcentraleventdetail->eventdate));


    $staff = array('$staffname','$tcname','$desname','$months','$effective_date','$becoming_executive_date');
    $staff_replace = array($getstaffdetail->name,$getstaffdetail->reportingtoname,$getstaffdetail->desname,$months,$effective_date,$becoming_executive_date);            

    $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 61 AND `isactive` = '1'";
    $data = $this->db->query($sql)->row();
    if(!empty($data))
      $post_body = str_replace($staff,$staff_replace , $data->lettercontent); 
    // echo "string"; 
    // print_r($data->lettercontent); die();

    if (count($post_body)>0) {
    //    # code...

    //  $newfilepath =  $data->file_path;
    //  $newfilepath = str_replace($data->cgh, site_url(),  $data->file_path); 
      echo $post_body;
     // print_r($data->lettercontent);
    }
    else
    {
     echo $post_body = 'File is deleted, please contact to IT team for support.';
   }


 }catch (Exception $e) {
  print_r($e->getMessage());die;
}


}

////   Get BDF form submit candidate details //////////

public function getcandidatedetails($id){

  try{

    $sql = "SELECT `tbl_candidate_registration`.candidateid, `tbl_candidate_registration`.encryptedphotoname,`mstbatch`.batch FROM `tbl_candidate_registration` left join `mstbatch` ON `mstbatch`.id =`tbl_candidate_registration`.batchid   Where `tbl_candidate_registration`.BDFFormStatus=1 AND `tbl_candidate_registration`.candidateid= '".$id."'";  

    $result = $this->db->query($sql)->result()[0];

    echo json_encode($result);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}



public function UsedBatch($batchid =NULL)
{

  try{

   echo $sql = "SELECT * FROM  `tbl_candidate_registration` WHERE `batchid` =$batchid";
   $query = $this->db->query($sql);

   return $query->num_rows();

 }catch(Exception $e){
  print_r($e->getMessage());die();
}

}


 /**
   * Method getfeedbackTeam() get team id.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getfeedbackFieldGuide($candidate =NULL)
 {

  try{

    $sql = "SELECT 
    `staff_transaction`.staffid, 
    `staff`.name 
    FROM 
    staff_transaction 
    INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
    INNER JOIN (
    SELECT 
    `staff_transaction`.staffid, 
    Max(
    `staff_transaction`.date_of_transfer
    ) as MaxDate 
    FROM 
    staff_transaction 
    GROUP BY 
    staffid
    ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
    AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
    AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
    ) 
    AND staff.`staffid` = '$candidate' 
    AND staff_transaction.`new_designation` IN(4,10) 
    ORDER BY 
    `staff_transaction`.staffid"; 

    $result = $this->db->query($sql)->result();

    $option = '<option value="">--Select Field Guide --</option>';

    foreach($result as $row){
      $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
    } 
    echo $option;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}


 /**
   * Method getfeedbackTeam() get team id.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getfeedbackTeam($officeid =NULL)
 {

  try{

   $sql = "SELECT officeid,officename FROM  `lpooffice` WHERE `officeid` =$officeid";

   $result = $this->db->query($sql)->result();

   $option = '<option value="">--Select Team --</option>';

   foreach($result as $row){
    $option .= '<option value="'.$row->officeid.'">'.$row->officename.'</option>';
  } 
  echo $option;

}catch(Exception $e){
  print_r($e->getMessage());die();
}

}



/**
   * Method getfeedbackBatch() get Team Coordinator.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getfeedbackBatch($id =NULL)
{

  try{

   $sql = "SELECT 
   a.`teamid`,
   a.`doj`,
   a.`batchid`,
   a.`fgid`,
   b.`batch`,
   c.`officename`,
   d.`name` as fieldguidename
   FROM 
   `tbl_da_personal_info` as a 
   LEFT JOIN `mstbatch` as b ON b.`id` = a.`batchid`
   LEFT JOIN `lpooffice` as c ON c.`officeid` = a.`teamid`
   LEFT JOIN `staff` as d ON d.`staffid` = a.`fgid` 
   WHERE a.`staffid` = $id"; 

   $result = $this->db->query($sql)->result();

   echo json_encode($result);


 }catch(Exception $e){
  print_r($e->getMessage());die();
}

}


/**
   * Method get_Integrator_ED_OnlyteamTC() get all integrator ED And only team TC 
   * @access  public
   * @param Null
   * @return  array
   */
public function get_Integrator_ED_OnlyteamTC($id=NULL, $staffid =NULL)
{
 try
 {

   $sql = 'SELECT DISTINCT(a.staffid), CONCAT(name, " (", b.desname,")") as name, case when u.staffid is null then 0 else 1 end user_status From (

   SELECT DISTINCT(staff.Staffid), CONCAT(emp_code,"-",name) as name, designation, 1 as sortorder FROM Staff  where new_office_id ="'.$id.'"  AND designation = 4 AND status = 1 
   UNION  
   SELECT DISTINCT(staff.Staffid), CONCAT(emp_code,"-",name) as name, designation, 10 as sortorder FROM staff WHERE designation = 16 OR designation = 2  AND status = 1 ) as a
   INNER JOIN msdesignation as b ON b.desid = a.designation  LEFT JOIN mstuser as u ON u.staffid = a.staffid AND u.IsDeleted=0 where a.staffid !=  "'.$staffid.'" ORDER by name ASC ';

   // echo $sql; die;

   $result = $this->db->query($sql)->result();
// print_r($result);die;
   $option = '<option value="">--Select ReportingTo --</option>';

   foreach($result as $row){
    if($row->user_status ==0){

      $option .= '<option value="'.$row->staffid.'" disabled>'.$row->name.'</option>';
    }else{

      $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
    }
  } 
  echo $option;


}catch (Exception $e) 
{
 print_r($e->getMessage());die;
}

}




/**
   * Method get_Integrator_ED_OnlyteamTCRC() get all integrator ED And only team TC 
   * @access  public
   * @param Null
   * @return  array
   */
public function get_Integrator_ED_OnlyteamTCRC($id, $staffid =NULL)
{
 try
 {

   // $sql = 'SELECT DISTINCT(staff.staffid), CONCAT(name, " (", b.desname,")") as name  FROM Staff    INNER JOIN msdesignation as b ON b.desid = Staff.designation where staff.new_office_id ="'.$id.'" AND staff.status = 1 ';


   $sql = 'SELECT DISTINCT(a.staffid), CONCAT(name, " (", b.desname,")") as name, case when u.staffid is null then 0 else 1 end user_status From (

   SELECT DISTINCT(staff.Staffid), CONCAT(emp_code,"-",name) as name, designation, 1 as sortorder FROM Staff  where new_office_id ="'.$id.'"  AND designation = 4 AND status = 1 
   UNION  
   SELECT DISTINCT(staff.Staffid), CONCAT(emp_code,"-",name) as name, designation, 10 as sortorder FROM staff WHERE designation = 16 OR designation = 2  AND status = 1 ) as a
   INNER JOIN msdesignation as b ON b.desid = a.designation  LEFT JOIN mstuser as u ON u.staffid = a.staffid AND u.IsDeleted=0 where a.staffid !=  "'.$staffid.'" ORDER by name ASC ';



   // echo $sql; die;

   $result = $this->db->query($sql)->result();
// print_r($result);die;
   $option = '<option value="">--Select ReportingTo --</option>';

   foreach($result as $row){

     $option .= '<option value="'.$row->staffid.'" >'.$row->name.'</option>';
     
   } 
   echo $option;


 }catch (Exception $e) 
 {
   print_r($e->getMessage());die;
 }

}


/**
   * Method getDAbatch() get candidate batch list.
   * @access  public
   * @param Null
   * @return  Array
  */


public function getstaff($staff_id)
{
    //echo "hello";
  //print_r($_POST);
 extract($_POST);

 try
 {
   $sql="SELECT 
  *,staff.emp_code
   FROM 
   staff_transaction 
   INNER JOIN staff ON `staff_transaction`.staffid = `staff`.staffid 
   inner join lpooffice on lpooffice.officeid=`staff_transaction`.new_office_id
   inner join msdesignation on msdesignation.desid=`staff`.`designation`

   where `staff`.staffid='$staff_id' ";
  //echo "query=".$sql;
   $content1=$this->db->query($sql)->row();
    //echo "office name=".$content1->officename."designation=".$content1->desname."employ=".$content1->emp_code;

   // die();
   $arr1=array('office_name'=>$content1->officename,'designation'=>$content1->desname,'office_id'=>$content1->old_office_id,'designation_id'=>$content1->designation,'employ_code'=>$content1->emp_code);

   echo json_encode($arr1);



 }

 catch (Exception $e) {
   print_r($e->getMessage());die;
 }




}

/**
   * Method getDAbatch() get candidate batch list.
   * @access  public
   * @param Null
   * @return  Array
  */


public function staff_data()
{
  //   echo "hello";
  // print_r($_POST); die;
  // extract($_POST);

 try
 {
    // echo $staff_id; die;
   // print_r($_POST);
  extract($_POST);
    // echo $staff_id; die;
  $sql="SELECT staff.permanentstreet,d.name as districtname, s.name as statename,staff.permanentpincode,staff.permanentstateid from staff 
  LEFT join `state` s on staff.permanentstateid = s.statecode
  LEFT join `district` d on staff.permanentdistrict = d.districtid
  where staff.staffid=$staff_id"; 
  // echo "query=".$sql; die;
  //console.log($sql);
  $content1=$this->db->query($sql)->row();
 // print_r($content1); die;
  $address=$content1->permanentstreet." ".$content1->districtname." ".$content1->statename." ".$content1->permanentpincode;

   // die();
  $arr1=array('address'=>$address);

  echo json_encode($arr1);



}

catch (Exception $e) {
 print_r($e->getMessage());die;
}




}
/**
   * Method getDAbatch() get candidate batch list.
   * @access  public
   * @param Null
   * @return  Array
  */


public function staff_data1()
{
    //echo "hello";
  //print_r($_POST);
  // extract($_POST);

 try
 {
   // print_r($_POST);
  extract($_POST);
    //echo $staff_id;
  $sql="SELECT staff.permanentstreet,d.name as districtname, s.name as statename,staff.permanentpincode,staff.permanentstateid from staff 
  left join `state` s on staff.permanentstateid = s.statecode
  left join `district` d on staff.permanentdistrict = d.districtid
  where staff.staffid=$staff_id";
  //echo "query=".$sql;
  //console.log($sql);
  $content1=$this->db->query($sql)->row();
 // print_r($content1);
  $address=$content1->permanentstreet." ".$content1->districtname." ".$content1->statename." ".$content1->permanentpincode;

   // die();
  $arr1=array('address'=>$address);

  echo json_encode($arr1);



}

catch (Exception $e) {
 print_r($e->getMessage());die;
}




}





/**
   * Method getdesignation() get designation.
   * @access  public
   * @param Null
   * @return  Array
  */


public function getdesignation3()
{
   // echo "hello";
  try
  {

    //print_r($_POST);
    extract($_POST);
    $sql="SELECT `desname` FROM `msdesignation` WHERE `desid`=$des";
    //echo $sql;
    $result=$this->db->query($sql)->row();
    echo json_encode($result);

  }
  catch (Exception $e) {
   print_r($e->getMessage());die;
 }


 
}



public function get_presentoffoice($staff_id)
{
    //echo "hello";
  print_r($_POST);
  
  extract($_POST);

  try
  {
   $sql="SELECT 
  *
   FROM 
   staff_transaction 
   INNER JOIN staff ON `staff_transaction`.staffid = `staff`.staffid 
   inner join lpooffice on lpooffice.officeid=`staff_transaction`.new_office_id


   where `staff`.staffid='$staff_id' ";
  //echo "query=".$sql;
 // die;
   $content1=$this->db->query($sql)->result();
   print_r($content1);
    //die;

   // echo "office name=".$content1->officename."designation=".$content1->desname;

   $option = '<option value="">--Select Present office --</option>';

   foreach($content1 as $row){
    $option .= '<option selected value="'.$row->new_office_id.'">'.$row->officename.'</option>';
  } 
  echo $option;



   // $arr1=array('office_name'=>$content1->officename,'office_id'=>$content1->new_office_id);

    //echo json_encode($arr1);



}

catch (Exception $e) {
 print_r($e->getMessage());die;
}
}

public function getDAbatch($id =NULL)
{

  try{

    $sql = "SELECT a.`candidateid`, a.`candidatefirstname`, b.`batch` FROM  `tbl_candidate_registration` as a 
    LEFT JOIN `mstbatch` as b ON b.`id` = a.`batchid` 
    WHERE a.`candidateid` = $id"; 
    $result = $this->db->query($sql)->result()[0];

   // print_r($result);

    $name  = $result->candidatefirstname;
    $batch = $result->batch;
    $staffname     = $this->loginData->UserFirstName;
    $designation = 'HRD';
    // $filepath =  site_url()."mailtext/Prejoiningletter.txt";
    // $fd = fopen($filepath, "r"); 
    // $message = fread($fd,4096);

    $hrname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;

    $hr1 = array('$candidatefirstname','$hrname');
    $hr_replace1 = array($name,$hrname);

    $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 16 AND `isactive` = '1'";
    $data = $this->db->query($sql)->row();
    if(!empty($data))
      $message= str_replace($hr1, $hr_replace1, $data->lettercontent);

    /*echo "<pre>";
    print_r($message);exit();eval("\$str = \"$str\";");*/
    // eval("\$message = \"$message\";");
    $msgbody = nl2br($message);

    $result1 = array($result,$msgbody);

    echo json_encode($result1);
         // echo $result = '<input type="text" name="batchid" id="batchid" class="form-control" readonly="readonly"  value='. $result->batch.'>';

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}






/**
   * Method getfeedbackTeamCoordinator() get Batch id.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getfeedbackTeamCoordinator($officeid =NULL)
{

  try{

    $sql = "SELECT 
    `staff_transaction`.staffid, 
    `staff`.name 
    FROM 
    staff_transaction 
    INNER JOIN staff ON `staff_transaction`.staffid = staff.staffid 
    INNER JOIN (
    SELECT 
    `staff_transaction`.staffid, 
    Max(
    `staff_transaction`.date_of_transfer
    ) as MaxDate 
    FROM 
    staff_transaction 
    GROUP BY 
    staffid
    ) as TMax ON `staff_transaction`.staffid = TMax.staffid 
    AND `staff_transaction`.date_of_transfer = TMax.MaxDate 
    AND `staff_transaction`.trans_status NOT IN(
    'Resign', 'Termination', 'Retirement', 
    'Death'
    ) 
    AND staff_transaction.`new_office_id` = '$officeid' 
    AND staff_transaction.`new_designation` IN(4) 
    ORDER BY 
    `staff_transaction`.staffid ";

    $result = $this->db->query($sql)->result();

    $option = '<option value="">--Select Team --</option>';

    foreach($result as $row){
      $option .= '<option value="'.$row->staffid.'">'.$row->name.'</option>';
    } 
    echo $option;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}


/**
   * Method checkoffice() get Batch id.
   * @access  public
   * @param Null
   * @return  Array
   */

public function checkoffice()
{
 $idofc=$this->input->post('idd');
 try
 {
  $sql="   SELECT DISTINCT a.officename from lpooffice a
  INNER JOIN staff_transaction b
  ON a.officeid=b.old_office_id 
  WHERE b.staffid=$idofc;
  ";

  $content1=$this->db->query($sql)->row();
  $arr1=array('office_name'=>$content1->officename);
  echo json_encode($arr1);
}

catch (Exception $e) {
  print_r($e->getMessage());die;
}

}


 /**
   * Method head_detail() get head detail.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function head_detail($fieldId, $headfinancialyear){
  $query = "SELECT a.*, b.lowerrange, b.upperrange, b.maxlimit FROM salaryheads a JOIN tblheadlookup b WHERE a.fieldname='".$fieldId."' AND b.fieldname='".$fieldId."' AND a.officeid is Null AND b.officeid is Null AND a.financial_year='".$headfinancialyear."'";
  $result = $this->db->query($query)->row();
  echo json_encode($result);
}


/**
   * Method ld_head_detail() get ld head detail.
   * @access  public
   * @param Null
   * @return  Array
   */

public function ld_head_detail($fieldId){
  $query = "SELECT b.*, a.lowerrange,a.upperrange,  c.officename FROM `tblheadlookup` as a INNER JOIN salaryheads as b on a.officeid = b.officeid and a.fieldname = b.fieldname INNER JOIN lpooffice as c on c.officeid = b.officeid WHERE a.fieldname='".$fieldId."'";
  $result = $this->db->query($query)->result();
  if($result){
    $resultdata = "<table id='tblmodalstatedashboard' class='table table-bordered table-striped responsive nowrap' ><thead><tr><th>Field Name</th><th class= 'text-right'>Lower Range</th><th class= 'text-right'>Upper Range</th><th>Office</th><th>Formula</th><th class= 'text-right'>Fixed Value</th><th class= 'text-center'>Action</th></thead><tbody>";
    foreach ($result as $res) {

      $resultdata .="<tr id=".$res->officeid." ><td>".$res->fieldname."</td><td class= 'text-right'><label id='formulalrange".$res->officeid."'></label></td><td class= 'text-right'><label id='formulaurange".$res->officeid."'>".$res->upperrange."</td><td>".$res->officename."</td><td ><label id='formula".$res->officeid."'>".$res->lookuphead."</label></td><td class= 'text-right'><label id='formulafixed".$res->officeid."'>".$res->ct."</td><td class= 'text-center'><a href='#' onclick='editldheaddetail(".$res->officeid.");'>Edit</a>|<a href='#' onclick='deleteldheaddetail(".$res->officeid.");'>Delete</a></td></tr> ";


      echo "<script> var cellid= ".$res->officeid."; var cid='#formulalrange'+cellid;
      $(cid).text(intToNumberBudget(".$res->lowerrange."));
      var cellid= ".$res->officeid."; var cid='#formulaurange'+cellid;
      $(cid).text(intToNumberBudget(".$res->upperrange."));
      </script>";

    }
    $resultdata .="</tbody></table>";




    echo $resultdata;
  }else{
    echo "No Records Found!";
  }
}

  /**
   * Method SalaryHead() get Salary Head .
   * @access  public
   * @param Null
   * @return  Array
   */
  public function SalaryHead(){
    $selected_field_no = trim($this->input->post('selected_field_no'));
    // $values = array();
    $fixed_value = $this->input->post('fixed_value');
    $lowerrange = $this->input->post('lowerrange');
    $upperrange = $this->input->post('upperrange');

    $fixed_value = str_replace(',', '', $fixed_value);
    $lowerrange =str_replace(',', '', $lowerrange);
    $upperrange =str_replace(',', '', $upperrange);


    $data_set = array(
      'fieldname' => $selected_field_no,
      'financial_year' => $this->input->post('headfinancialyear'),
      'fielddesc' => $this->input->post('head_description'),
      'abbreviation' => $this->input->post('head_abbreviation'),
      'lookuphead' => $this->input->post('salary_formula'),
      'roundingupto' => $this->input->post('round_upto'),
      'formulacolumn' => $this->input->post('formulacolumn'),
      'activefield' => $this->input->post('activeField'),
      'attendancedep' => $this->input->post('workingDaysDependent'),
      'monthlyinput' => $this->input->post('monthlyInput'),
      'carryforward' => $this->input->post('carryForward'),
      'roundtohigher' => $this->input->post('roundingToHeigher'),
      'mT' => $this->input->post('locationDependent'),
      'ct' => $fixed_value,
      'c' =>$this->input->post('fixedcolumn')
    );
    $tblheadlookup = array(
      'fieldname' => $selected_field_no,
      'lowerrange'=> $lowerrange,
      'upperrange'=> $upperrange,
      'maxlimit'=> $this->input->post('maxlimit'),
    );
    if($this->fieldnoExist($selected_field_no, $this->input->post('headfinancialyear'))){
      //update code run
      $this->db->where('fieldname', $selected_field_no);
      $this->db->where('financial_year', $this->input->post('headfinancialyear'));

      $this->db->update('salaryheads', $data_set);
      $this->db->where('fieldname', $selected_field_no);
      $this->db->update('tblheadlookup', $tblheadlookup);
      echo "update";
    }else{
      //insert code run
      $this->db->insert('salaryheads', $data_set);
      $this->db->insert('tblheadlookup', $tblheadlookup); 
      echo "insert";
    }

    
  }
  /**
   * Method LDSalaryHead() get Salary Head .
   * @access  public
   * @param Null
   * @return  Array
   */
  public function LDSalaryHead(){
    // print_r($_POST); die();
    $selected_field_no = trim($this->input->post('selected_field_no'));
    $officeid = $this->input->post('officeid');

    $lowerrange = trim($this->input->post('lowerrange'));
    $upperrange = trim($this->input->post('upperrange'));
    $modalfixedvalue = trim($this->input->post('modalfixedvalue'));

    $lowerrange = str_replace(',', '',  $lowerrange);
    $upperrange = str_replace(',', '',  $upperrange);
    $modalfixedvalue = str_replace(',', '',  $modalfixedvalue);
    // echo $lowerrange; die();

    // if($lowerrange!=0)
    // {
    //   $lowerrange = number_format($lowerrange);
    // }

    // if($upperrange!=0)
    // {
    //   $upperrange = number_format($upperrange);
    // }

    $data_set = array(
      'fieldname' => $selected_field_no,
      'lookuphead' => $this->input->post('salary_formula'),
      'formulacolumn' => $this->input->post('modalformulacolumn'),
      'officeid' => $officeid,
      'mT'       => 1,
      'ct'        => $modalfixedvalue,
      'c'         => $this->input->post('modalfixedcolumn')
    );
    $tblheadlookup = array(
      'fieldname' => $selected_field_no,
      'lowerrange'=> $lowerrange,
      'upperrange'=> $upperrange,
      'officeid' => $officeid
    );
    if($this->ldfieldnoExist($selected_field_no,$officeid)){
      //update code run
      $this->db->where('fieldname', $selected_field_no);
      $this->db->where('officeid', $officeid);
      $this->db->update('salaryheads', $data_set);
      $this->db->where('fieldname', $selected_field_no);
      $this->db->where('officeid', $officeid);
      $this->db->update('tblheadlookup', $tblheadlookup);
      echo "update";

    }else{
      //insert code run
      $this->db->insert('salaryheads', $data_set);
      $this->db->insert('tblheadlookup', $tblheadlookup);
      echo "insert";
      
    }
  }


/**
   * Method fieldnoExist() get field no exist.
   * @access  public
   * @param Null
   * @return  Array
   */

public function fieldnoExist($selected_field_no, $financial_year){
  $sql = "SELECT * FROM salaryheads WHERE fieldname ='".trim($selected_field_no)."' AND officeid is Null and  financial_year = '".trim($financial_year)."'";
  $result = $this->db->query($sql)->result();
  if(count($result) > 0 ){
    return true;
  }else{
    return false;
  }
}

  /**
   * Method ldfieldnoExist() get field no exist.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function ldfieldnoExist($selected_field_no, $officeid){
    $sql = "SELECT * FROM salaryheads WHERE fieldname ='".trim($selected_field_no)."' AND officeid =".$officeid;
    $result = $this->db->query($sql)->result();
    if(count($result) > 0 ){
      return true;
    }else{
      return false;
    }
  }

/**
   * Method ajaxeditldheaddetail() get field no exist.
   * @access  public
   * @param Null
   * @return  Array
   */

public function ajaxeditldheaddetail($officeid, $fieldname){
  $query = "SELECT * FROM `tblheadlookup` as a INNER JOIN salaryheads as b on a.officeid = b.officeid WHERE a.officeid = '".$officeid."' and a.fieldname ='".$fieldname."'";
  $result = $this->db->query($query)->row();    
  echo json_encode($result);
}

  /**
   * Method ajaxdeleteldheaddetail() get field no exist.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function ajaxdeleteldheaddetail($officeid, $fieldname){
    $this->db->trans_start();
    $this->db->where('officeid', $officeid);
    $this->db->where('fieldname', $fieldname);
    $this->db->delete('tblheadlookup');
    $this->db->where('officeid', $officeid);
    $this->db->where('fieldname', $fieldname);
    $this->db->delete('salaryheads');
    $this->db->trans_complete();
    return 'delete';
  }


/**
   * Method basicsalaryupdate() get basic salary update.
   * @access  public
   * @param Null
   * @return  Array
   */

public function basicsalaryupdate(){
  $staffid = $this->input->post('staffid');
  $basicsalary = $this->input->post('basicsalary');
  ///// Add by Amit Start Code /////////////////////////////
  $newbasicsalary = str_replace(',', '',  $basicsalary); 
  $numnewbasicsalary = intval($newbasicsalary);
  ///// Add by Amit End Code /////////////////////////////

  $query = "select * from tblmstemployeesalary where staffid=".$staffid;
  $result = $this->db->query($query)->result();
  if(count($result) == 0){
    $data_set = array(
      'staffid'=>$staffid,
      'e_basic'=>$numnewbasicsalary
    );
    $this->db->insert('tblmstemployeesalary', $data_set);
    echo "insert";

  }else{
    $data_set = array(
      'e_basic'=>$numnewbasicsalary
    );
    $this->db->where('staffid',$staffid);
    $this->db->update('tblmstemployeesalary',$data_set);
    echo "update";
  }
  // echo "Basic salary add successfully";
}

/**
   * Method getreverse_leave_request() get basic salary update.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getreverse_leave_request($id){

  $data_set = array(
    'status' => 4
  );
  $this->db->where('id', $id);
  $this->db->update('trnleave_ledger',$data_set);

     // echo $this->db->last_query(); die;

  echo "leave reversal successfully";
}

/**
   * Method getOfficewisestaff() get basic salary update.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getOfficewisestaff($ddloffice)
{

 try
 {

   $sql="SELECT staffid,name,emp_code FROM `staff` WHERE new_office_id=".$ddloffice." AND status= 1 "; 
   $result = $this->db->query($sql)->result();
   if(count($result))
    {  $option = '<option value="">--Select Staff --</option>';
  foreach($result as $row){
    $option .= '<option selected value="'.$row->staffid.'">'.$row->emp_code.'-'.$row->name.'</option>';
  }
} 
else{
  $option = '<option value="">--Select Staff --</option>';
}

echo $option;




}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


/**
   * Method getOfficewisestaffdetails() get basic salary update.
   * @access  public
   * @param Null
   * @return  Array
   */


public function getOfficewisestaffdetails()
{
 try
 {

  extract($_POST);

  $sql="SELECT * FROM `tblmonthlyinput` WHERE officeid=".$ddloffice." AND staffid=".$ddlemployee; 
  $result = $this->db->query($sql)->row();
// print_r($result); die();
  if(count($result)>0)
  {
   echo $txtamount = '<input type="hidden" name="salmonth" id="salmonth" class="form-control" maxlength="10" value='.$result->salmonth.'>
   <input type="hidden" name="salyear" id="salyear" class="form-control" maxlength="10" value='.$result->salyear.'>
   <input type="text" name="txtamount" id="txtamount" class="form-control" maxlength="10" value='.$result->e_04.'>'; 
 }
 else{
   echo $txtamount = '<input type="hidden" name="salmonth" id="salmonth" class="form-control" maxlength="10" value="">
   <input type="hidden" name="salyear" id="salyear" class="form-control" maxlength="10" value="">
   <input type="text" name="txtamount" id="txtamount" class="form-control" maxlength="10" value="">'; 
 }    
}catch (Exception $e) {
 print_r($e->getMessage());die;
}
}


/**
   * Method ForwordtoNextyear() get basic salary update.
   * @access  public
   * @param Null
   * @return  Array
   */

public function ForwordtoNextyear()
{
  extract($_POST);
  $updateArr = array(
    'from_month'  =>$this->input->post('datefrom'),
    'to_month' =>$this->input->post('dateto'),
    'credit_value' => $this->input->post('credit'),
    'status' => $this->input->post('status'),
    'updatedon'  => date('Y-m-d H:i:s'),
    'updatedby'  => $this->loginData->UserID,
  );

  $this->db->insert('msleavecrperiod', $updateArr);

    //$this->db->last_query(); 

  return true; 
}
}