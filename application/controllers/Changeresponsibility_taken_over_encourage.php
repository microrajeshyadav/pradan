<?php 

///// Handed over taken over Controller   

class Changeresponsibility_taken_over_encourage extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Stafftransferpromotion_model","Stafftransferpromotion_model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("Changeresponsibility_taken_over_model","model");
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token)
	{

		try{
	    // start permission 
			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Staff_approval/index');
				
			} else {

				$content['t_id'] = $this->model->getTransid($token);
				$staff_id=$content['t_id']->staffid;
				$supervisor = $content['t_id']->reportingto;
				$id = $content['t_id']->id;

				$content['report'] = $this->model->transaction_reporting($token);
				$transaction = $content['report']->reportingto;
			// echo $transaction; die;
				

				$content['staff_details'] = $this->model->get_staffDetails($token);
				$staff=$content['staff_details']->staffid;
			// print_r($content['staff_details']);
			// die;

				$qry = "select * from tbl_hand_over_taken_over_charge where staffid=".$content['staff_details']->staffid." AND type=1";
				$content['handed_over_data'] = $this->db->query($qry)->row();
				$content['transfer_expeness_details']=$this->model->expeness_details($content['handed_over_data']->id);
				
				
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
		// end permission
				
				$personalid = $this->model->getPersonalUserList();
				$content['getstafflist'] = $this->model->getStaffList();

				

				$content['supervisor_list'] = $this->model->supervisor_detail($supervisor);

				$getdesignation = $this->model->finalstaff_detail($token);
				

				$query = "SELECT * FROM tbl_hand_over_taken_over_charge WHERE transid = ".$token;
				$content['handed_over_detail'] = $this->db->query($query)->row();

				$RequestMethod = $this->input->server('REQUEST_METHOD'); 
				if($RequestMethod == "POST")
				{	

					$submitdatasend = $this->input->post('submitbtn');

					$Sendsavebtn = $this->input->post('savetbtn');

					if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

						$transferno=$this->input->post('transferno');

						$insertArraydata=array(

							'type'                => $this->input->post('handed'),
							'staffid'             => $staff_id,
							'transfernno'         => $transferno,
							'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('tdate')),
							'responsibility_name' => $this->input->post('change_responsibility_name'),
							'trans_to'            => $this->input->post('transferto'),
							'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
							'process_type'        => 'CR',
							'transid'             => $token,
							'flag'                => 0,
						);
						$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
						$insertid = $this->db->insert_id();

						$countitem = count($this->input->post('items'));
						for ($i=0; $i < $countitem; $i++) { 

							$arr = array (
								'item'           =>$this->input->post('items')[$i],
								'handedtaken_id' =>$insertid,
								'description'    =>$this->input->post('items_description')[$i]

							);


							$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

						}




						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE)
						{

							$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
						}
						else{
							$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
							redirect('Changeresponsibility_taken_over_encourage/edit/'.$token);			
						}

					}




					else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
					{

						$sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$token";
						$result  = $this->db->query($sql)->result()[0];
						
						$transferno=$this->input->post('transferno');

						$insertArraydata=array(

							'type'                => $this->input->post('handed'),
							'staffid'             => $staff_id,
							'transfernno'         => $transferno,
							'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('tdate')),
							'responsibility_name' => $this->input->post('change_responsibility_name'),
							'trans_to'            => $this->input->post('transferto'),
							'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
							'process_type'        => 'CR',
							'counter_name'        => $this->input->post('countersigned'),
							'transid'             => $token,
							'flag'                => 1,

						);

						$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);

						$insertid = $this->db->insert_id();

						$countitem = count($this->input->post('items'));
						for ($i=0; $i < $countitem; $i++) { 

							$arr = array (

								'item'           =>$this->input->post('items')[$i],
								'handedtaken_id' =>$insertid,
								'description'    =>$this->input->post('items_description')[$i]

							);

							$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

						}


						$insertArraytrans=array(

							'trans_flag'    => 9,
							'updatedon'		=> date("Y-m-d H:i:s"),
							'updatedby'		=> $this->loginData->staffid,

						);

						$this->db->where('id', $token);
						$this->db->update('staff_transaction', $insertArraytrans);


						$insertArrtrans=array(

							'designation'    => $getdesignation->new_designation,
							'updatedon'		 => date("Y-m-d H:i:s"),
							'updatedby'		 => $this->loginData->staffid,
							'reportingto'    => $transaction,

						);
						$this->db->where('staffid', $getdesignation->staffid);
						$this->db->update('staff', $insertArrtrans);

						$insertworkflowArr = array(
							'r_id'                 => $token,
							'type'                 => 4,
							'staffid'              => $staff_id,
							'sender'               => $this->loginData->staffid,
							'receiver'             => $staff,
							'senddate'             => date("Y-m-d H:i:s"),
							'flag'                 => 9,
							'forwarded_workflowid' => $result->workflowid,
							'scomments'            => $this->input->post('remark'),
							'createdon'            => date("Y-m-d H:i:s"),
							'createdby'            => $this->loginData->staffid,
						);
					// echo "<pre>";
					// print_r($insertworkflowArr);
					// die;
						$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

					//echo $this->db->last_query();
						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE)
						{

							$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
						}
						else{
							$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');

							$subject = ': Taken responsibility';
							$body = '<h4>'.$content['supervisor_list']->name.' Taken responsibility </h4><br />';
							$body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
							<tr>
							<td width="96">Name </td>
							<td width="404">'.$content['supervisor_list']->name.'</td>
							</tr>
							<tr>
							<td>Employ Code</td>
							<td> '.$content['supervisor_list']->emp_code.'</td>
							</tr>
							<tr>
							<td>Designation</td>
							<td>' .$content['supervisor_list']->sepdesig.'</td>
							</tr>
							<tr>
							<td>Office</td>
							<td>'.$content['supervisor_list']->officename.'</td>
							</tr>
							</table>';
							$body .= "<br /> <br />";
							$body .= "Regards <br />";
							$body .= " ". $content['supervisor_list']->name ."<br>";
							$body .= " ". $content['supervisor_list']->sepdesig."<br>";
							$body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
                // $to_useremail = $content['staff_details']->staff_email;
                // $tcemailid = $content['supervisor_list']->tc_email;
							$personal_email= $personalid->personalid;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
							$arr= array (
								$personal_email      =>'personal',
							);

							$this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

							redirect('Changeresponsibility_taken_over_encourage/view/'.$token);			
						}

					}

				}


				$content['subview']="index";
				$content['title'] = 'Changeresponsibility_taken_over_encourage';	
				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);
			}

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	public function Edit($token)
	{
		try{
	    // start permission 
			$staff_id=$this->loginData->staffid;
			$content['t_id'] = $this->model->getTransid($token);
			$staff_id=$content['t_id']->staffid;
			$supervisor = $content['t_id']->reportingto;
			$id = $content['t_id']->id;

			

			$content['staff_details']=$this->model->get_staffDetails($id);

			$content['expense']=$this->model->count_handedchrges($token);

			$content['supervisor_list'] = $this->model->supervisor_detail($supervisor);

			$personalid = $this->model->getPersonalUserList();

			$content['transfer_expeness_details']=$this->model->expeness_details($token);


			$content['handed_expeness']=$this->model->handed_over_charge($token);


			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission
			$content['getstafflist'] = $this->model->getStaffList();
			$content['subview']="index";
			$RequestMethod = $this->input->server('REQUEST_METHOD'); 
			if($RequestMethod == "POST")
				{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			

			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

				$transferno=$this->input->post('transferno');

				$insertArraydata=array(
					
					'type'                => $this->input->post('handed'),
					'staffid'             => $staff_id,
					'transfernno'         => $transferno,
					'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('tdate')),
					'responsibility_name' => $this->input->post('change_responsibility_name'),
					'trans_to'            => $this->input->post('transferto'),
					'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'process_type'        => 'CR',
					'transid'             => $token,
					'flag'                => 0,
				);
				$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
				$this->db->where('id',$token);

				$countitem = count($this->input->post('items'));

				$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);


				for ($i=0; $i < $countitem; $i++) { 

					$arr=array(

						'handedtaken_id' =>$token,
						'item' =>$this->input->post('items')[$i],
						'description'=>$this->input->post('items_description')[$i],
					);

					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				}



				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Changeresponsibility_taken_over_encourage/edit/'.$token);			
				}

			}




			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{

				$transferno=$this->input->post('transferno');

				$insertArraydata=array(
					
					'type'                => $this->input->post('handed'),
					'staffid'             => $staff_id,
					'transfernno'         => $transferno,
					'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('tdate')),
					'responsibility_name' => $this->input->post('change_responsibility_name'),
					'trans_to'            => $this->input->post('transferto'),
					'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'process_type'        => 'CR',
					'transid'             => $token,
					'flag'                => 1,
				);

				$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
				$this->db->where('id',$token);


				$countitem = count($this->input->post('items'));

				$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);


				for ($i=0; $i < $countitem; $i++) { 

					$arr=array(

						'handedtaken_id' =>$token,
						'item' =>$this->input->post('items')[$i],
						'description'=>$this->input->post('items_description')[$i],
					);

					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
				}

				$insertArraytrans=array(

					'trans_flag'    => 9,
					'updatedon'		=> date("Y-m-d H:i:s"),
					'updatedby'		=> $this->loginData->staffid,

				);

				$this->db->where('id', $token);
				$this->db->update('staff_transaction', $insertArraytrans);


				$insertArrtrans=array(

					'designation'    => $getdesignation->new_designation,
					'updatedon'		 => date("Y-m-d H:i:s"),
					'updatedby'		 => $this->loginData->staffid,

				);

				$this->db->where('staffid', $getdesignation->staffid);
				$this->db->update('staff', $insertArrtrans);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 4,
					'staffid'              => $staff_id,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $personalid[0]->personnelstaffid,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 9,
					'forwarded_workflowid' => $result->workflowid,
					'scomments'            => $this->input->post('remark'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);



				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Changeresponsibility_taken_over_encourage/view/'.$token);			
				}
				
			}





		}
		$content['title'] = 'Changeresponsibility_taken_over_encourage/edit';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}
public function view($token)
{
	try{
	    // start permission 
		$staff_id=$this->loginData->staffid;

		$query = "SELECT * FROM tbl_hand_over_taken_over_charge WHERE type = 2 and transid = ".$token;
		$content['handed_detail'] = $this->db->query($query)->row();
		$transid = $content['handed_detail']->transid;
		$content['t_id'] = $this->model->getTransid($token);
		$staff_id=$content['t_id']->staffid;
		$supervisor = $this->loginData->staffid;
		$id = $content['t_id']->id;
		$takenid = $content['handed_detail']->id;

		$content['staff_details']=$this->model->get_staffDetails($token);

		$qry = "select * from tbl_hand_over_taken_over_charge where staffid=".$content['staff_details']->staffid." AND type=1";
		$content['handed_over_data'] = $this->db->query($qry)->row();
		$content['transfer_expeness_details']=$this->model->expeness_details($content['handed_over_data']->id);
		$content['expense']=$this->model->count_handedchrges($token);

		$content['transfer_expeness_details']=$this->model->expeness_details($takenid);
		$content['supervisor_list'] = $this->model->supervisor_detail($supervisor);

		
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		
		$content['title'] = 'Changeresponsibility_taken_over_encourage/view';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}