<?php 
class Profile extends Ci_controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		$UserID = $this->loginData->UserID;
		$query = "select * from mstuser as mu left join mstuser_photo as mup on mup.UserID=mu.UserID where mu.UserID='$UserID'";
		$content['user_list'] = $this->Common_Model->query_data($query);
		$content['title'] = 'Profile';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	
	function add(){

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == "POST"){

			$encryptedName = sha1(rand() . date('Y-m-d h:i:s'));
			$tempFile = $_FILES['doc_file']['tmp_name'];
			$targetPath = FCPATH . "datafiles/";
			$targetFile = $targetPath . $encryptedName;
			$uploadResult = move_uploaded_file($tempFile,$targetFile);
			if($uploadResult == true){

				//insert record attachment table
				$insertArr = array(
					'File_Name'						 =>	$_FILES['doc_file']['name'],
					'Encrypted_File_Name'  =>	$encryptedName,
					'UserID'					     =>	$this->loginData->UserID,
					);
				$this->Common_Model->insert_data('mstuser_photo', $insertArr);
				$this->session->set_flashdata('tr_msg', 'Successfully Updated Profile photo');
			}
		}
		$content['title'] = 'profile';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content); 
	}


	public function edit(){
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if ($RequestMethod == 'POST') {

            $uid = $this->loginData->UserID;
			$updateArr = array(
				'Username'       => $this->input->post('Username'),
				'Password'       => md5($this->input->post("Password")),
				'UserFirstName'  => $this->input->post('UserFirstName'),
				'UserMiddleName' => $this->input->post('UserMiddleName'),
				'UserLastName'   => $this->input->post('UserLastName'),
				'PhoneNumber'    => $this->input->post('PhoneNumber'),
				'EmailID'        => $this->input->post('EmailID'),
				);
			
			$this->Common_Model->update_data('mstuser', $updateArr,'UserID',$uid);
			$this->session->set_flashdata('tr_msg', 'Successfully Updated user');
			redirect('/profile/');
		}
		$content['title'] = 'profile';
		$user_list = $this->Common_Model->get_data('mstuser', '*', 'UserID',$uid);
		$content['user_list'] = $user_list;
		
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}


}