<?php 

/**
* TC DAship Components Controller List
*/
class Tc_daship_components_hr extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Tc_daship_components_hr_model', 'model');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model', 'model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
    try{

     
    // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $batchid= $this->input->post('batchid');

  $phaseid= $this->input->post('phaseid');
 
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

       $this->form_validation->set_rules('phasename','Phase Name','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

      $insertArr = array(
        'phase_name'   => $this->input->post('phasename'),
        'created_date' => date('Y-m-d H:i:s'),
        'isdeleted'    => $this->input->post('status'),
        );
      
      $this->db->insert('mstphase', $insertArr);
      $this->session->set_flashdata('tr_msg', 'Successfully added Phase');
      redirect('/Phase/');
    }

    prepareview:

  //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0' AND stage = 2";
  $content['batch_details'] = $this->Common_Model->query_data($query);
  
  //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);

  $content['dacomponent_details'] = $this->model->getDAComponent($batchid,$phaseid);

    $content['subview']="index";
    $content['title'] = 'Tc_daship_components_hr';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

 
    function delete($token)
    {
      try{
      $this->Common_Model->delete_row('mstphase','id', $token); 
      $this->session->set_flashdata('tr_msg' ,"Phase Deleted Successfully");
      redirect('/DA_event_detailing/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);

      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}
