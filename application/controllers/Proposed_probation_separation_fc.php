<?php 
class Proposed_probation_separation_fc extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Proposed_probation_separation_fc_model');
    // $this->load->model('Staff_seperation_model');
    $this->load->model("Common_model","Common_Model");

    //$this->load->model(__CLASS__ . '_model');
    $mod = $this->router->class.'_model';
    $this->load->model($mod,'',TRUE);
    $this->model = $this->$mod;

    $check = $this->session->userdata('login_data');

    ///// Check Session //////  
    if (empty($check)) {

     redirect('login');

   }

   $this->loginData = $this->session->userdata('login_data');

  // print_r($this->loginData->hrdemailid);

 }

 public function index()
 {
  try{


  $content['getprobationdetails'] = $this->model->get_Probation_Separation();
  $content['getgraduatedetails'] = $this->model->get_Recommended_to_graduate();
  $content['gettransferdetails'] = $this->model->get_Recommended_to_transfer();
  $content['getextentiondetails'] = $this->model->get_Recommended_to_Extention();
  
  /*echo "<pre>";
  print_r($content['getprobationdetails']);exit();*/
  $content['title'] = 'Campus';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;     
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }

}


  /**
   * Method intimate() Initiate Intemation.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function intimate($intemationid)
  {
    
    try{
      $query = "select a.*, c.intimation, b.comment  FROM staff as a LEFT JOIN tbl_hr_intemation as b ON a.staffid = b.staffid
      LEFT JOIN mst_intimation as c ON c.id = b.intemation_type  
      WHERE b.id=".$intemationid;
      $resultdata = $this->db->query($query)->row();
      /*echo "<pre>";
      print_r($resultdata);exit();*/
      $tablename = "'staff_transaction'";
      $incval = "'@a'";
      $personnel_detail = $this->model->getpersonneldetail();

      $Staffdetails =  $this->model->getstaffDetails($resultdata->staffid);
      $designation  =  $Staffdetails->designation;
      $supervisor   =  $Staffdetails->reportingto;
      $receiverdetail = $this->model->get_staffDetails($resultdata->staffid);


      $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

      $autoincval =  $getstaffincrementalid->maxincval;

      // echo $autoincval; die;

      $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
      $result  = $this->db->query($sql)->result()[0];
      $this->db->trans_start();
      $insertArr = array(
        'id'                => $autoincval,
        'staffid'           => $resultdata->staffid,
        'old_office_id'     => $resultdata->new_office_id,
        'old_designation'   => $designation,
        'trans_status'      => $resultdata->intimation,
        'reason'            => $resultdata->comment,
        'trans_flag'        => 1,
        'reportingto'       => $supervisor,
        'createdon'         => date("Y-m-d H:i:s"),
        'datetime'          => date("Y-m-d H:i:s"),
        'createdby'         => $this->loginData->staffid
      );

      $this->db->insert('staff_transaction', $insertArr);
      if ($result->workflowid !='') {
            $insertworkflowArr = array(
             'r_id'                 => $autoincval,
             'type'                 => 27,
             'staffid'              => $resultdata->staffid,
             'sender'               => $this->loginData->staffid,
             'receiver'             => $resultdata->staffid,
             'forwarded_workflowid' => $result->workflowid,
             'senddate'             => date("Y-m-d H:i:s"),
             'flag'                 => 1,
             'scomments'            => $this->input->post('discussion'),
             'createdon'            => date("Y-m-d H:i:s"),
             'createdby'            => $this->loginData->staffid,
            );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }else{

           $insertworkflowArr = array(
             'r_id'           => $autoincval,
             'type'           => 27,
             'staffid'        => $resultdata->staffid,
             'sender'         => $this->loginData->staffid,
             'receiver'       => $resultdata->staffid,
             'senddate'       => date("Y-m-d H:i:s"),
             'flag'           => 1,
             'scomments'      => $this->input->post('discussion'),
             'createdon'      => date("Y-m-d H:i:s"),
             'createdby'      => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }

        $hrintemationdataset = array(
          'status'=>2
        );
        $this->db->where('id', $intemationid);
        $this->db->update('tbl_hr_intemation', $hrintemationdataset);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Resign request has been failed !!! '); 
            redirect(current_url());

          }else{
                      
              
              $subject = ":Resign Mail";
              $body = "<h4>Hi ".$resultdata->name.", </h4><br />";
              $body .= "We have initiated Resign.<br />";
              $body .= "<b>Comment : " .  $resultdata->comment . "</b><br /><br /><br />";
              $body .= "<b> Thanks </b><br>";
              $body .= "<b>Pradan Technical Team </b><br>";

              $to_email = $receiverdetail->emailid;
              $to_name = $receiverdetail->name;
              //$to_cc = $getStaffReportingtodetails->emailid;
              $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $resultdata->emailid => $resultdata->name
               // ..
              );
              $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
              if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
              }

          $this->session->set_flashdata('tr_msg', 'Transfer request has been initiated successfully !!!');
          redirect('/Proposed_probation_separation/index');

          }      
    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  /**
   * Method intimate_Recommended_to_graduate() Initiate Intemation.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function intimate_Recommended_to_graduate($intemationid)
  {
    
    try{
      $query = "select a.*, c.intimation, b.comment  FROM staff as a LEFT JOIN tbl_hr_intemation as b ON a.staffid = b.staffid
      LEFT JOIN mst_intimation as c ON c.id = b.intemation_type  
      WHERE b.id=".$intemationid;
      $resultdata = $this->db->query($query)->row();
      /*echo "<pre>";
      print_r($resultdata);exit();*/
      $tablename = "'staff_transaction'";
      $incval = "'@a'";
      $personnel_detail = $this->model->getpersonneldetail();

      $Staffdetails =  $this->model->getstaffDetails($resultdata->staffid);
      $designation  =  $Staffdetails->designation;
      $supervisor   =  $Staffdetails->reportingto;
      $receiverdetail = $this->model->get_staffDetails($resultdata->staffid);


      $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

      $autoincval =  $getstaffincrementalid->maxincval;

      // echo $autoincval; die;

      $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
      $result  = $this->db->query($sql)->result()[0];
      $this->db->trans_start();
      $insertArr = array(
        'id'                => $autoincval,
        'staffid'           => $resultdata->staffid,
        'old_office_id'     => $resultdata->new_office_id,
        'old_designation'   => $designation,
        'trans_status'      => $resultdata->intimation,
        'reason'            => $resultdata->comment,
        'trans_flag'        => 1,
        'reportingto'       => $supervisor,
        'createdon'         => date("Y-m-d H:i:s"),
        'datetime'          => date("Y-m-d H:i:s"),
        'createdby'         => $this->loginData->staffid
      );

      $this->db->insert('staff_transaction', $insertArr);
      if ($result->workflowid !='') {
            $insertworkflowArr = array(
             'r_id'                 => $autoincval,
             'type'                 => 27,
             'staffid'              => $resultdata->staffid,
             'sender'               => $this->loginData->staffid,
             'receiver'             => $resultdata->staffid,
             'forwarded_workflowid' => $result->workflowid,
             'senddate'             => date("Y-m-d H:i:s"),
             'flag'                 => 1,
             'scomments'            => $this->input->post('discussion'),
             'createdon'            => date("Y-m-d H:i:s"),
             'createdby'            => $this->loginData->staffid,
            );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }else{

           $insertworkflowArr = array(
             'r_id'           => $autoincval,
             'type'           => 27,
             'staffid'        => $resultdata->staffid,
             'sender'         => $this->loginData->staffid,
             'receiver'       => $resultdata->staffid,
             'senddate'       => date("Y-m-d H:i:s"),
             'flag'           => 1,
             'scomments'      => $this->input->post('discussion'),
             'createdon'      => date("Y-m-d H:i:s"),
             'createdby'      => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }

        $hrintemationdataset = array(
          'status'=>2
        );
        $this->db->where('id', $intemationid);
        $this->db->update('tbl_hr_intemation', $hrintemationdataset);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Resign request has been failed !!! '); 
            redirect(current_url());

          }else{
                      
              
              $subject = ":Resign Mail";
              $body = "<h4>Hi ".$resultdata->name.", </h4><br />";
              $body .= "We have initiated Resign.<br />";
              $body .= "<b>Comment : " .  $resultdata->comment . "</b><br /><br /><br />";
              $body .= "<b> Thanks </b><br>";
              $body .= "<b>Pradan Technical Team </b><br>";

              $to_email = $receiverdetail->emailid;
              $to_name = $receiverdetail->name;
              //$to_cc = $getStaffReportingtodetails->emailid;
              $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $resultdata->emailid => $resultdata->name
               // ..
              );
              $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
              if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
              }

          $this->session->set_flashdata('tr_msg', 'Transfer request has been initiated successfully !!!');
          redirect('/Proposed_probation_separation/index');

          }      
    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }

public function add($token=NULL){
  try{

   $RequestMethod = $this->input->server('REQUEST_METHOD');   
   if($RequestMethod == 'POST'){

   // print_r($this->input->post()); //die;
    
     $da = $this->input->post('da');

     // $alreadycandidate = $this->candidate_exists($da);

     // echo $alreadycandidate; die;

      // if ($alreadycandidate==1) {

      //    $this->session->set_flashdata('er_msg', 'Already added proposed probation separation ! please choose other candidate !!!');
      //   redirect('Proposed_probation_separation');  

      // }else{

    $this->form_validation->set_rules('type','type','trim|required');
    $this->form_validation->set_rules('dc','dc','trim|required');
    $this->form_validation->set_rules('team','team','trim|required');
    $this->form_validation->set_rules('da','da','trim|required');
    $this->form_validation->set_rules('probation_completed','probation_completed','trim|required');
    $this->form_validation->set_rules('DateofSeraption','DateofSeraption','trim|required');
    $this->form_validation->set_rules('probation_extension_date','probation_extension_date ','trim|required');              
    $this->form_validation->set_rules('Reason','Reason','trim|required');
    $this->form_validation->set_rules('comment','comment','trim|required');        


    $dashiptransactions = $this->input->post('dashiptransactions');  

   // echo $dashiptransactions; die;

    $dateext = $this->input->post('extension_date');
    $extension_date = $this->model->changedatedbformate($dateext);
     $DateofSeraption = $this->input->post('DateofSeraption');
    $dateofseraption = $this->model->changedatedbformate($DateofSeraption);
    $staffDAid = $this->input->post('da');


    $tablename = 'staff_transaction';
    $incval = '@a';

    $increment_staffid_transactionid = $this->model->getStaffTransactioninc($tablename,$incval);


    if ($dashiptransactions == 1) {

     $this->db->trans_start();
     $getstaffdetail = '';
      $daemailid = '';
     



  $html = '<table  width="100%" >
  <tr>
    <td></td>
    <td></td>
    <td valign="top" ></td>
  </tr>
  <tr>
    <td colspan="3">Dear Sir ,</td>
    
  </tr>
  <tr>
    <td colspan="3" align="justify" >It is with a heavy heart that I’m saying goodbye to you and all my other dear colleagues. My personal commitments insist that I stop being an employee of this company and therefore it is quite regretfully that I shall be resigning from my post as __________ [enter designation].</td>
  
  </tr>
  <tr>
    <td colspan="3" align="justify" > I want to thank you for all, your help and support during the time I worked here. You were of the main reason why I enjoyed my work so much. I wish you all the best for your future endeavors and hope all your years in the company are successful and fruitful. </td>
    
  </tr>
  
  <tr>
    <td colspan="3" align="justify">Do keep in touch. Take care..</td>
  </tr>
  <tr>
    <td colspan="3">Regards </td>
  </tr>
  <tr>
    <td colspan="3">XYZ </td>
  </tr>

</table>';

    $filename = 'separation_'.md5(time());
    
    $this->load->model('Dompdf_model');
    $generate =   $this->Dompdf_model->separation_letter_PDF($html, $filename);

    

    if ($generate == 1) {
    
      $insertArr2 = array(
        'type'                 => $this->input->post('dashiptransactions'),
        'dc'                   => $this->input->post('dc'),
        'team'                 => $this->input->post('team'),
        'staffid'              => $this->input->post('da'),
        'dateofseraption'      => $dateofseraption,
        'reason'               => $this->input->post('Reason'),
        'comment '             => trim($this->input->post('comment')),
        'generate_letter_name' => $filename,
        'createdon'            => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'            => $this->loginData->UserID, // login user id
        'status'               => 1,
        'isdeleted'            => 0, 
      );

       $this->db->insert('tbl_probation_separation', $insertArr2);

      $insertArr2 = array(
        'staffid'             => $this->input->post('da'),
        'old_office_id'       => $this->input->post('team'),
        'new_office_id'       => $this->input->post('team'),
        'date_of_transfer'    => $dateofseraption,
        'old_designation'     => 13,
        'new_designation'     => 13,
        'trans_status'        => 'Termination',
         'id'                  => $increment_staffid_transactionid->maxincval,
        'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
        'createdon'           => date('Y-m-d H:i:s'),
        'createdby'            => $this->loginData->UserID,
      );

      $this->db->insert('staff_transaction', $insertArr2);

     $updateArr = array(
        'status' => 0,
        'dateofleaving'  => $dateofseraption,
        'separationtype'  => 'Termination',
        'separationremarks' => $this->input->post('comment'),
        'updatedon'         => date('Y-m-d H:i:s'),
        'updatedby'          => $this->loginData->UserID,
      );
       $this->db->where('staffid', $staffDAid);
       $this->db->update('staff', $updateArr);


    // $sql="UPDATE mstuser SET IsDeleted = 1 WHERE SUBSTRING(Username, LOCATE('_', Username) +1)=$staffDAid";

     $sql="UPDATE mstuser  SET IsDeleted = 1 WHERE  mstuser.staffid =$staffDAid"; 
     $query=$this->db->query($sql);

      $getstaffdetail =  $this->model->getstaffid($staffDAid);
      $daemailid = $getstaffdetail->emailid;
     if (count($getstaffdetail) ==0) {

          $this->session->set_flashdata('er_msg', 'Error  Development Apprenticeship id not found !!!');
          redirect('Proposed_probation_separation'); 
          exit;
        
      }else{
         

      $candidatesql="UPDATE tbl_candidate_registration  SET isdeleted = 1 WHERE  emailid = '".$daemailid."'"; 
     $candidatequery=$this->db->query($candidatesql); 

     }

     

       //$insertid = $this->db->insert_id();
        $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('er_msg', 'Error Proposed Separation');  
          }else{

      $getstaffdetail =  $this->model->getStaffSeparationDetails($da);
     // print_r($getstaffdetail); die;

      $staffemail = $getstaffdetail->emailid;
      $to_name = $this->loginData->hrdemailid;
      $filename = $getstaffdetail->generate_letter_name;

      $attachments = array($filename.'.pdf');

       //print_r($attachments);  //die;

        $html = 'Dear Sir, <br><br> 

          Separation of contract/employment . <br>
          Regards <br>
          Pradan Team';


       $sendmail = $this->Common_Model->send_email_separation($subject = 'Separation Letter ', $message = $html, $staffemail, $to_name, $attachments);  //// Send Mail candidates With Offer Letter ////

        $this->session->set_flashdata('tr_msg', 'Successfully Proposed Separation'); 

        redirect('Proposed_probation_separation/add'); 
        

    }

  
  }
   

    }else if ($dashiptransactions == 2) {

     // print_r($this->input->post()); die;

       $this->db->trans_start();


      $html = '<table  width="100%" >
  <tr>
    <td></td>
    <td></td>
    <td valign="top" ></td>
  </tr>
  <tr>
    <td colspan="3">Dear Sir ,</td>
    
  </tr>
  <tr>
    <td colspan="3" align="justify" >It is with a heavy heart that I’m saying goodbye to you and all my other dear colleagues. My personal commitments insist that I stop being an employee of this company and therefore it is quite regretfully that I shall be resigning from my post as __________ [enter designation].</td>
  
  </tr>
  <tr>
    <td colspan="3" align="justify" > I want to thank you for all, your help and support during the time I worked here. You were of the main reason why I enjoyed my work so much. I wish you all the best for your future endeavors and hope all your years in the company are successful and fruitful. </td>
    
  </tr>
  
  <tr>
    <td colspan="3" align="justify">Do keep in touch. Take care..</td>
  </tr>
  <tr>
    <td colspan="3">Regards </td>
  </tr>
  <tr>
    <td colspan="3">XYZ </td>
  </tr>

</table>';
    $filename = 'probation_'.md5(time());
    $this->load->model('Dompdf_model');
    $generate =   $this->Dompdf_model->probation_letter_PDF($html, $filename);

    if ( $generate==1) {
    //  print_r($this->input->post()); die;

    
    $probationda = $this->input->post('probationstaff'); 
    $probation_completed_date = $this->input->post('probation_completed_date');
    $probation_completed_date = $probation_completed_date == ''? NULL : $this->model->changedatedbformate($probation_completed_date);
    $extension_date = $this->input->post('extension_date');
    $extension_date1 = $extension_date == ''? NULL : $this->model->changedatedbformate($extension_date);

    $staffid = $this->input->post('probationstaff');
    $probationtype = $this->input->post('probation_completed');

    

      $insertArr2 = array(

        'type'                 => $this->input->post('dashiptransactions'),
        'dc'                   => $this->input->post('probationdc'),
        'team'                 => $this->input->post('probationteam'),
        'staffid'              => $this->input->post('probationstaff'),
        'probation_completed'  => $this->input->post('probation_completed'),
        'probation_completed_date'  => $probation_completed_date,
        'probation_extension_date' => $extension_date1,
        'reason'                => $this->input->post('Reason'),
        'comment '              => trim($this->input->post('comment')),
        'generate_letter_name'  => $filename,
        'status'                => 1,
        'createdon'             => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'             => $this->loginData->UserID, // login user id
        'isdeleted'             => 0, 
      );
//
 
       $this->db->insert('tbl_probation_separation', $insertArr2);

        $insertArr2 = array(
        'staffid'             => $staffid,
        'old_office_id'       => $this->input->post('probationteam'),
        'new_office_id'       => $this->input->post('probationteam'),
        'date_of_transfer'    => date('Y-m-d'),
        'old_designation'     => 13,
        'new_designation'     => 13,
        'id'                  => $increment_staffid_transactionid->maxincval,
        'trans_status'        => 'probation',
        'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
      );

       $this->db->insert('staff_transaction', $insertArr2);

if ($probationtype ==2) {
 
        $updateArr = array(
        'probation_extension_date'  => $extension_date1,
        'probation_status'  => 0,
        'probation_completed' => 0,
        'updatedon'         => date('Y-m-d H:i:s'),
        'updatedby'          => $this->loginData->UserID,
      );
       $this->db->where('staffid', $staffid);
       $this->db->update('staff', $updateArr);

}else{

  $updateArr = array(
        'probation_completed' => 1,
         'probation_status'  => 1,
         'updatedon'         => date('Y-m-d H:i:s'),
         'updatedby'          => $this->loginData->UserID,
      );
       $this->db->where('staffid', $staffid);
       $this->db->update('staff', $updateArr);
}

//        echo $this->db->last_query(); die;


       $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('er_msg', 'Error adding Proposed Probation'); 

          }else{

             $getstaffdetail =  $this->model->getStaffSeparationDetails($probationda);

            $staffemail = $getstaffdetail->emailid;
           // $hrdemail = $this->loginData->EmailID;
           $to_name = $this->loginData->hrdemailid;
            $filename = $getstaffdetail->generate_letter_name;

            $attachments = array($filename.'.pdf');

             //print_r($attachments);  //die;

              $html = 'Dear Sir, <br><br> 

                Probation . <br>
                Regards <br>
                Pradan Team';


            $sendmail = $this->Common_Model->send_email_separation($subject = 'Probation Letter ', $message = $html, $staffemail, $to_name, $attachments);  //// Send Mail candidates With Offer Letter ////

        $this->session->set_flashdata('tr_msg', 'Successfully added Proposed Probation'); 

        redirect('Proposed_probation_separation/add'); 

      }
}


    }elseif ($dashiptransactions==4) {

   // print_r($this->input->post()); die;

      $html = '<table  width="100%" >
  <tr>
    <td></td>
    <td></td>
    <td valign="top" ></td>
  </tr>
  <tr>
    <td colspan="3">Dear Sir ,</td>
    
  </tr>
  <tr>
    <td colspan="3" align="justify" >It is with a heavy heart that I’m saying goodbye to you and all my other dear colleagues. My personal commitments insist that I stop being an employee of this company and therefore it is quite regretfully that I shall be resigning from my post as __________ [enter designation].</td>
  
  </tr>
  <tr>
    <td colspan="3" align="justify" > I want to thank you for all, your help and support during the time I worked here. You were of the main reason why I enjoyed my work so much. I wish you all the best for your future endeavors and hope all your years in the company are successful and fruitful. </td>
    
  </tr>
  
  <tr>
    <td colspan="3" align="justify">Do keep in touch. Take care..</td>
  </tr>
  <tr>
    <td colspan="3">Regards </td>
  </tr>
  <tr>
    <td colspan="3">XYZ </td>
  </tr>

</table>';

//echo $html; die;
    $filename = 'transfer_'.md5(time());
    $this->load->model('Dompdf_model');
    $generate =   $this->Dompdf_model->transfer_letter_PDF($html, $filename);

    if ($generate==1) {

    $staffid = $this->input->post('staff');
    $dateoftran = $this->input->post('dateoftransfer');
    $dateoftransfer = $this->model->changedatedbformate($dateoftran);
   // $getcandidateteamfg = $this->db->getCandidateteamfg();
     $inserttranferArr = array(
        'teamid'          => $this->input->post('newteam'),
        'fg'              => $this->input->post('new_field_guide'),
        'staffid'         => $staffid,
        'doj'             => date('Y-m-d'),
        'createdon'       => date('Y-m-d H:i:s'),
        'createdby'       => $this->loginData->UserID,
      );

     $this->db->insert('tbl_transfer_history', $inserttranferArr);
 
      $insertArr2 = array(
        'staffid'             => $this->input->post('staff'),
        'old_office_id'       => $this->input->post('oldteam'),
        'new_office_id'       => $this->input->post('newteam'),
        'date_of_transfer'    => $dateoftransfer,
        'old_designation'     => 13,
        'new_designation'     => 13,
        'id'                  => $increment_staffid_transactionid->maxincval,
        'trans_status'        => 'Transfer',
        'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
      );

       $this->db->insert('staff_transaction', $insertArr2);

       $updateArr = array(
        'doj_team'  => $dateoftransfer,
        'designation' => 13,
        'new_office_id' => $this->input->post('newteam'),

      );
       $this->db->where('staffid', $staffid);
       $this->db->update('staff', $updateArr);

      $updateArrdapersonalinfo = array(
        'fgid'  => $this->input->post('new_field_guide'),
        'teamid' => $this->input->post('newteam'),
      );

       $this->db->where('staffid', $staffid);
       $this->db->update('tbl_da_personal_info', $updateArrdapersonalinfo);

      $insertArr3 = array(
        'olddc'                   => $this->input->post('olddc'),
        'oldteam'                 => $this->input->post('oldteam'),
        'staffid'                 => $this->input->post('staff'),
        'newdc'                   => $this->input->post('newdc'),
        'newteam'                 => $this->input->post('newteam'),
        'new_field_guide'         => $this->input->post('new_field_guide'),
        'dateoftransfer'          => $dateoftransfer,
        'comment'                 => $this->input->post('commenttransfer'),
        'generate_letter_name'    =>  $filename,
        'createdon'               => date('Y-m-d H:i:s'),
        'createdby'               => $this->loginData->UserID,    // Current Date and time
      );

       $this->db->insert('tbl_transfer_da', $insertArr3);

       $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('er_msg', 'Error adding Successfully added Proposed Transfer');  
          }else{


            $getstaffdetail =  $this->model->getStaffTransferDetails($staffid);
            //print_r($getstaffdetail); die;
            $staffemail = $getstaffdetail->emailid;
           // $hrdemail = $this->loginData->EmailID;
            $to_name = $this->loginData->hrdemailid;
            $filename = $getstaffdetail->generate_letter_name;

            $attachments = array($filename.'.pdf');

              $html = 'Dear Sir, <br><br> 

                Transfer  . <br><br><br>
                Regards <br>
                Pradan Team';


             $sendmail = $this->Common_Model->send_email_separation($subject = 'Tranfer Letter ', $message = $html, $staffemail, $to_name, $attachments);  //// Send Mail candidates With Offer Letter ////


            $this->session->set_flashdata('tr_msg', 'Successfully added Proposed Transfer');      
          }


        }
    }
   
    redirect('Proposed_probation_separation/add');  
  }

 //print_r($this->loginData);
  $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
      //$content['getprobseparation'] = $this->model->get_Prob_Separation($token);
  $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();

  $query ="select * from mst_reason where isdeleted=0";
  $content['Reson_list'] = $this->db->query($query)->result();

  $query ="select * from mst_extension_sepration_type  where isdeleted=0";
  $content['extension_sep_list'] = $this->db->query($query)->result();
  $content['getintimation'] = $this->model->getIntimation();

  $content['title'] = 'add';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

}catch (Exception $e) {
  print_r($e->getMessage());die;
}

}


public function send_letter($token){

  try{


          $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
          $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();
          $query ="select * from mst_reason where isdeleted=0";
          $content['Reson_list'] = $this->db->query($query)->result();

          $query ="select * from mst_extension_sepration_type  where isdeleted=0";
          $content['extension_sep_list'] = $this->db->query($query)->result();
          $content['getintimation'] = $this->model->getIntimation();

          $content['title'] = 'send_letter';
          $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
          $this->load->view('_main_layout', $content);

        }catch (Exception $e) {
          print_r($e->getMessage());die;
        }

  }

public function edit($token){

  try{

        // print_r($this->input->post()); //die;
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $probation_extension_date = $this->input->post('probation_extension_date');     
      if (!empty($probation_extension_date)) {
        $ProbationExtensionDate = date('Y-m-d', strtotime($probation_extension_date));
      }

      $DateofSeraption = $this->input->post('DateofSeraption');
      if (!empty($DateofSeraption)) {
        $Dateof_Seraption = date('Y-m-d', strtotime($DateofSeraption));
      }            

      $this->form_validation->set_rules('type','type','trim|required');
      $this->form_validation->set_rules('dc','dc','trim|required');
      $this->form_validation->set_rules('team','team','trim|required');
      $this->form_validation->set_rules('da','da','trim|required');
      $this->form_validation->set_rules('probation_completed','probation_completed','trim|required');
      $this->form_validation->set_rules('DateofSeraption','DateofSeraption','trim|required');
      $this->form_validation->set_rules('probation_extension_date','probation_extension_date ','trim|required');              
      $this->form_validation->set_rules('Reason','Reason','trim|required');
      $this->form_validation->set_rules('comment','comment','trim|required'); 

      $typeid = $this->input->post('type');       
      if ($typeid == 1) {

        $insertArr2 = array(

          'type'        => $this->input->post('type'),
          'dc'            => $this->input->post('dc'),
          'team'        => $this->input->post('team'),
          'da'            => $this->input->post('da'),
          'probation_completed'   => $this->input->post('probation_completed'),
          'probation_extension_date' => $ProbationExtensionDate,
          'extension_date'  => $this->input->post('extension_date'),
          'comment '        => trim($this->input->post('comment')),
        'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'       => $this->loginData->UserID, // login user id
        'status'      => 1,
        'isdeleted'       => 0, 
      );
      }else if ($typeid == 2) {

        $insertArr2 = array(

          'type'        => $this->input->post('type'),
          'dc'            => $this->input->post('dc'),
          'team'        => $this->input->post('team'),
          'da'            => $this->input->post('da'),
          'dateofseraption'   => $Dateof_Seraption,
          'extension_date'  => $this->input->post('extension_date'),
          'Reason'        => $this->input->post('Reason'),
          'comment '        => trim($this->input->post('comment')),
          'status'      => 1,
        'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'       => $this->loginData->UserID, // login user id
        'isdeleted'       => 0, 
      );
      }
      $this->db->where('id',$token);
      $this->db->update('tbl_probation_separation', $insertArr2);
      $this->session->set_flashdata('tr_msg', 'Successfully updated Proposed Probation Separation');
      redirect('Proposed_probation_separation');      

    }

    $content['statedetails'] = $this->model->stateList();
    $content['getprobationdetails'] = $this->model->get_Probation_Separation($token);

    $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
    $content['getprobseparation'] = $this->model->get_Prob_Separation($token);
    $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();     

      // print_r($content['getprobationdetails']); die();

    $query ="SELECT `lpooffice`.`officeid`, `lpooffice`.`officename` FROM `lpooffice` INNER JOIN  `dc_team_mapping` ON `dc_team_mapping`.`teamid` = `lpooffice`.`officeid` ";
    $content['team_list'] = $this->db->query($query)->result();

    $query ="select * from mst_reason where isdeleted=0";
    $content['Reson_list'] = $this->db->query($query)->result();

    $query ="select * from mst_extension_sepration_type where isdeleted=0";
    $content['extension_sep_list'] = $this->db->query($query)->result();
       // print_r($content['extension_sep_list']); die;

    $content['title'] = 'add';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}







function candidate_exists($da)
{
  try{

 $this->db->select('*'); 
 $this->db->from('tbl_probation_separation');
 $this->db->where('da', $da);
 
 $query = $this->db->get();
 //echo $this->db->last_query();die; 
 //echo $query->num_rows(); die;
 if ($query->num_rows() > 0) {
   return 1;
 } else {
   return 0;
 }

 }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}
  /**
   * Method delete() delete data. 
   * @access  public
   * @param 
   * @return  array
   */ 
  public function delete($token)
  {
    try{

    $this->view['detail'] = $this->model->getCampusDetails($token);
    if(count($this->view['detail']) < 1) {
      $this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
      redirect($this->router->class);
    }
    

    if($this->model->delete($token) == '1'){
      $this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
      redirect($this->router->class);
    }
    else {
      $this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
      redirect($this->router->class);
    }


    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  }

}