<?php 
class Recruitersheadmapping extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staffrecruitmentteammapping_model');
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		    // start permission 
		
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
	
			$content['campuslist']   = $this->model->getCampus();
			$content['recuterslist'] = $this->model->getRecruitershead();
			$content['title'] = 'Recruitersheadmapping';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
 		print_r($e->getMessage());die;
 	}
		
	}

	/**
	 * Method add() Add data. 
	 * @access	public
	 * @param	
	 * @return	array
	*/ 
	public function add(){
				    // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		try{

		
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

				$this->db->trans_start();

	        
			$insertArr2 = array(
				'campusid'   	   => $this->input->post('campus'),
				'recruiterheadid'  => $this->input->post("recruiter"),
				'createdon'        => date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'        => $this->loginData->UserID, // login user id
			    'isdeleted'        => 0,
			   
			  );

			
			$this->db->insert('mapping_campus_recruiters_head', $insertArr2); 
		

			$this->db->trans_complete();


			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error mapping Recruiter Head');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Recruiter Head');			
			}
			redirect('/Recruitersheadmapping/index');
			
			}


			$recruiterswithcampuslist	= $this->model->getRecruitersWithCampus();

			$campus_array = array();

			foreach ($recruiterswithcampuslist as $key => $value) {
				$campus_array[] = $value->campusid;
			}

			$impcamp = implode(",", $campus_array);

			if(empty($impcamp)) {
				$impcamp = 0;
			}

			$content['campuslist']	= $this->model->getCampus($impcamp);
			$content['recruitmentlist'] 	= $this->model->getRecruitment();
			$content['recuterslist']        = $this->model->getRecruitershead();
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}



    /**
	 * Method edit() Edit data. 
	 * @access	public
	 * @param	
	 * @return	array
	*/ 
	public function edit($token){
				    // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		
		try{

	
			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
			
				$this->db->trans_start();

				$updateArr = array(
					'campusid'   	    => $this->input->post('campus'),
					'recruiterheadid'   => $this->input->post("recruiters"),
					'createdon'         => date('Y-m-d H:i:s'),    // Current Date and time
				    'createdby'         => $this->loginData->UserID, // login user id
				    'isdeleted'         => 0,
				   
				  );


					$this->db->where("id",$token);
			        $this->db->update('mapping_campus_recruiters_head', $updateArr);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error mapping Recruiter Head');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added Recruiter Head ');			
					}
					redirect('/Recruitersheadmapping/index');
			
			}


			$content['campuslist'] 		= $this->model->getCampus();
			$content['recruitmentlist'] = $this->model->getRecruitment();
			$content['recuiterheadcampusdetail'] = $this->model->getRecuiterHeadCampusDetail($token);
			$content['title'] = 'edit';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}
	



	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete($token)
	{
		try{
		
		$this->view['detail'] = $this->model->getMappedCampusRecruitment($token);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		

		if($this->model->delete($token) == '1'){
			$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}

		}catch (Exception $e) {
 		print_r($e->getMessage());die;
 	}
	 }

}