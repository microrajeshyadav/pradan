<?php 

/**
* State List
*/
class Staff_dashboard extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Staff_dashboard_model");
      $this->load->model("Global_Model","gmodel");
    $check = $this->session->userdata('login_data');
   // $staffids=$check->staffid;
   
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');

    //print_r($this->loginData); die;
  }

  public function index()
  {
     
     
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   $check = $this->session->userdata('login_data');
   $staffids=$this->loginData->staffid;
  
   $content['staffid']= $staffids;

    $content['dashboardstat']  = $this->Staff_dashboard_model->getdashboardstat(); //dasboard statistics

    $content['getstaffStatus'] = $this->Staff_dashboard_model->getstaffStatus($this->loginData->staffid);    
    $content['policylist']  = $this->Staff_dashboard_model->getpolicylist();
    $sql="select * from tbl_workflowdetail order by `workflowid`desc LIMIT 1";
    //echo $sql;
    $content['workflow']=$this->db->query($sql)->row();
   // print_r(expression)
   $content['superAnnuation']  = $this->Staff_dashboard_model->superannuationlist();
     
    $query = "select * from mstugeducation where isdeleted='0'";
    $content['mstugeducation_details'] = $this->Common_Model->query_data($query);
   $content['subview']="index";
    $content['title'] = 'Ugeducation';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }


}