<?php 
class Probation_ed_reviewofperformance extends CI_Controller
{


	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model('Probation_ed_reviewofperformance_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Staff_midterm_review_model","Staff_midterm_review_model");


		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			
			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function add($token=NULL)
	{
		// start permission 
		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    



			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){




				$this->db->trans_start();

				$date_of_appointment = $this->input->post('date_of_appointment');
				$date_of_appointment_db = $this->gmodel->changedatedbformate($date_of_appointment);

				$period_of_review_from = $this->input->post('period_of_review_from');
				$period_of_review_from_db = $this->gmodel->changedatedbformate($period_of_review_from);

				$period_of_review_to = $this->input->post('period_of_review_to');
				$period_of_review_to_db = $this->gmodel->changedatedbformate($period_of_review_to);

				$probation_extension_date = $this->input->post('probation_extension_date');
				$probation_extension_date_db = $this->gmodel->changedatedbformate($probation_extension_date);


				$period_latestby = $this->input->post('latestby');
				$period_latestby_db = $this->gmodel->changedatedbformate($period_latestby);


				$insertArr = array(
					'staffid'                     => $this->input->post('staffid'),
					'date_of_appointment'         => $date_of_appointment_db,
					'period_of_review_from'       => $period_of_review_from_db,
					'period_of_review_to'         => $period_of_review_to_db,
					'work_habits_and_attitudes'   => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity' => $this->input->post('conduct_and_social_maturity'),
					'integrity'                   => $this->input->post('questionable'),
					'any_other_observations'      => $this->input->post('any_other_observations'),
					'latestby'                    => $period_latestby_db,
					'satisfactory'                => $this->input->post('satisfactory'),
					'probation_completed'         => $this->input->post('completed'),
					'reasons_for_not_above_recommendations' => $this->input->post('not_recommended'),
					'probation_extension_date'    => $probation_extension_date_db,
					'flag'                        => 1,
					'createdby'                   => $this->loginData->staffid,
					'createdon'                   => date('Y-m-d H:i:s'),
				);

				$this->db->insert('tbl_probation_review_performance', $insertArr);

				$insertid = $this->db->insert_id();   


				$insertworkflowArr = array(
					'r_id'                 => $insertid,
					'type'                 => 22,
					'staffid'              => $token,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $this->input->post('supervisorid'),
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 1,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


				$this->db->trans_complete();

				if ($this->db->trans_status() === true){


					$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
					redirect('/Probation_ed_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
					redirect(current_url());
				}

			}

			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);

			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
			//$content['getstaffPnndetails'] = $this->model->getstaffPnndetails();
			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}

	


	public function edit($token)
	{
		// start permission 
		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($getSupervisordetals);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
		// print_r($getstaffprobationdetail);

			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
		   // echo "<pre>";
		  	// print_r($this->input->post()); die;

				$this->db->trans_start();
				$extended_date = $this->input->post('extended_date');
				$extended_date_db = $this->gmodel->changedatedbformate($extended_date);

				$insertArr = array(
					'staffid'                  => $this->input->post('staffid'),
					'satisfactory'             => $this->input->post('satisfactory'),
					'probation_completed'       => $this->input->post('completed'),
					'reasons_for_not_above_recommendations'    => $this->input->post('not_recommended'),
					'probation_extension_date'  =>  $extended_date_db,

					'work_habits_and_attitudes'             => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity'             => $this->input->post('conduct_and_social_maturity'),
					'any_other_observations'             => $this->input->post('any_other_observations'),
					'integrity'             => $this->input->post('questionable'),
					'flag'				   => 4,
					'updatedby'              => $this->loginData->staffid,
					'updatedon'              => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				// echo $this->db->last_query(); die;

				$insertworkflowArr = array(
					'r_id'                 => $probationid,
					'type'                 => 7,
					'staffid'              => $token,
					'sender'               => $this->loginData->staffid,
					'receiver'             => 11,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 4,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				$this->db->trans_complete();

				if ($this->db->trans_status() === true){


					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_ed_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been sent to ED successfully !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			//$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}



	public function view($token)
	{
		if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				
				
			}
			else 
			{
		
		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission  
			$content['staff_details'] = $this->model->getstaff($token);

			$staff_id=$content['staff_details']->staffid;
			
				
			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			

			$content['workflowid'] = $this->model->get_providentworkflowid($token);
			//print_r($content['workflowid']);
			//die;

			$content['geted_detals'] = $this->model->getEDName();
			$ed_name=$content['geted_detals']->name;

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			// echo "<pre>";
			// print_r($getstaffprobationdetail);
			// die;
		
			$probationid = $getstaffprobationdetail->id;
			$transid = $getstaffprobationdetail->transid;
			
			
			$staff_id=$content['staff_details']->staffid;
			$staff_email=$content['staff_details']->emailid;
			$reporting_id=$content['staff_details']->reportingto;
			$content['reportingto_image'] = $this->Common_Model->staff_signature($reporting_id);

			$content['tc_email']='';
			$content['tc_email'] = $this->Staff_midterm_review_model->superviser_email($reporting_id);
			

			$content['hr_email'] = $this->Staff_midterm_review_model->hrd_email();
			$content['p_email'] = $this->Staff_midterm_review_model->personal_email();
			$content['ed_email'] = $this->Staff_midterm_review_model->ed_email();

			// 
			$ed_id= $content['ed_email']->staffid;
			$content['ed_image'] = $this->Common_Model->staff_signature($ed_id);

			$content['login_staff'] = $this->model->loginstaff($this->loginData->staffid);
			

			$content['getstaffprobationreview'] = $getstaffprobationdetail;
			/*echo "<pre>";
			print_r($content['getstaffprobationreview']);die();*/
			
			$content['getstaffhrd'] = $this->model->getStaffHRDList();
			$content['getstaffp'] = $this->model->getStaffPersonnalList();
			 
			$content['getstaffPdetails'] = $this->model->getStaffPersonnalList();
			


			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
				

				$this->db->trans_start();

				$ed_date = $this->input->post('ed_date');
				$ed_date_db = $this->gmodel->changedatedbformate($ed_date);
				$insertArr = array(

					'agree'                => $this->input->post('agree'),
					'ed_comments'          => $this->input->post('ed_comments'),
					'ed_date'              => $ed_date_db,
					'flag'				   => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);
				//print_r($insertArr);
				//die;

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				$insertArr = array(

					'trans_flag'           => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);


				$this->db->where('id',$transid);
				$this->db->update('staff_transaction', $insertArr);


				if ($res>0) {

					$insertworkflowArr = array(
						'r_id'                 => $token,
						'type'                 => 22,
						'staffid'              => $content['getstaffprovationreviewperformance']->staffid,
						'sender'               => $this->loginData->staffid,
						'receiver'             => $this->input->post('sendtohtd'),
						'senddate'             => date("Y-m-d H:i:s"),
						'forwarded_workflowid'	=>$content['workflowid']->workflowid,
						'flag'                 => $this->input->post('edstatus'),
						'scomments'            => $this->input->post('comments'),
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);

				}else{
					$insertworkflowArr = array(
						'r_id'                 =>  $token,
						'type'                 => 22,
						'staffid'              => $content['getstaffprovationreviewperformance']->staffid,
						'sender'               => $this->loginData->staffid,
						'receiver'             => $this->input->post('sendtohtd'),
						'forwarded_workflowid'	=>$content['workflowid']->workflowid,
						'senddate'             => date("Y-m-d H:i:s"),
						'flag'                 => $this->input->post('edstatus'),
						'scomments'            => $this->input->post('comments'),
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);

				}

				
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				

				$this->db->trans_complete();

				if ($this->db->trans_status() === true){

						// echo $flagupdate;
						// 	die;

					if($this->input->post('edstatus')==5)
					{
						$satus="approved by";
					}
					else if($this->input->post('edstatus')==6)
					{
						$satus="rejected by";	
					}

					$name=$content['login_staff']->name;

					$subject = ': Midterm View';
					$body = '<h4>Midtermreview ' .$satus.$name.', </h4><br />';
					$body .= '<table width="500" border="0" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
					<tr>
					<td width="96">Name </td>
					<td width="404">'.$content['staff_details']->name.'</td>
					</tr>
					<tr>
					<td width="96">Employ Code</td>
					<td width="404">'.$content['staff_details']->emp_code.'</td>
					</tr>
					<tr>
					<td>Designation</td>
					<td>' .$content['staff_details']->desname.'</td>
					</tr>
					<tr>
					<td>Office</td>
					<td>'.$content['staff_details']->officename.'</td>
					</tr>
					</table>';

					$body .= "<br /> <br />";
					$body .= "Regards <br />";
					$body .= " ". $content['login_staff']->name ."<br>";
					$body .= " ". $content['login_staff']->desname."<br>";
					$body .= "<b> Thanks </b><br>";


					$to_useremail = $staff_email;
        // $staff_email=$content['hrd_staff_details']->emailid;
					$tc_email=$content['tc_email']->emailid;
					$hremail=$content['hr_email']->EmailID;
					$personal=$content['p_email']->EmailID;
					$e_email=$content['ed_email']->EmailID;

        //$to_hremail = $hremail->EmailID;
					$arr= array (


						$tc_email      =>'tc',
						$hremail=>'hrd',


						$personal =>'personal',
						$e_email='ed'
					);

					$this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);




	$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
	redirect('/Probation_ed_reviewofperformance/demoview/'.$staff_id.'/'.$token);	
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
					redirect(current_url());
				}

			}
			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			// print_r($content['getstaffprovationreviewperformance']);
			// die;
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails($token);
			//print_r($content['getpersonnaldetals']);
		//die;
			

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);




			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
	}
		
	}

	public function demoview($staff,$token)
	{
		// start permission 

		if (empty($staff&&$token) || $token == ''&& $staff=='' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				
				
			} else {


		try{
			
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		
		//	$content['getSupervisordetals'] = $this->model->getSupervisor();
			$content['geted_detals'] = $this->model->getEDName();
			$ed_name=$content['geted_detals']->name;
	
			$content['login_staff'] = $this->model->loginstaff($this->loginData->staffid);
			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			$content['getstaffprobationreview']=$getstaffprobationdetail;
$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
/*print_r($content['getstaffprovationreviewperformance']);
die;*/

			$getstaffprobationdetail = $this->model->getstaffpeersonnelprobationdetals($token);
			/*echo "<pre>";
			print_r($content['getstaffprobationreview']); 
			die;
*/
			$probationid = $getstaffprobationdetail->id;

		//	$content['getstaffprovationreviewperformance'] = $getstaffprobationdetail;
			
			// echo "<pre>";
			// print_r($content['getstaffprovationreviewperformance']);
		//	die;
			$content['workflowid']=$this->model->get_providentworkflowid($token);
			$content['getstaffhrd'] = $this->model->getStaffHRDList();
			// print_r($content['getstaffhrd']);
			// die;

			$content['getstaffPdetails'] = $this->model->getStaffPersonnalList();
			 //print_r($content['getstaffPdetails']);
			// die;
			$content['staff_details'] = $this->model->getstaff($token);
			//print_r($content['staff_details']);
			//die;
				//$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			// print_r($content['getstaffprovationreviewperformance']);
			// die;
			$staff_id='';
			$staff_id=$content['staff_details']->staffid;
			$staff_email=$content['staff_details']->emailid;
			// print_r($staff_email);
			// die;

			$reporting_id=$content['staff_details']->reportingto;
			$content['reportingto_image'] = $this->Common_Model->staff_signature($reporting_id);
			$content['tc_email'] = $this->Staff_midterm_review_model->superviser_email($reporting_id);
			// print_r($content['tc_email']);
			// die;
$content['staff_details'] = $this->model->getstaff($token);
			$content['hr_email'] = $this->Staff_midterm_review_model->hrd_email();
			$content['p_email'] = $this->Staff_midterm_review_model->personal_email();

			$p_id=$content['p_email']->staffid;
			$content['personal_image'] = $this->Common_Model->staff_signature($p_id);
			


			$content['ed_email'] = $this->Staff_midterm_review_model->ed_email();
			// 
			$ed_id= $content['ed_email']->staffid;
			$content['ed_image'] = $this->Common_Model->staff_signature($ed_id);
			

				$this->db->trans_start();
			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
				


/*echo "<pre>";
print_r($content);exit();*/
 $effective_date= $this->gmodel->changedatedbformate($this->input->post('effective_date'));




				$html='';
						

				if ($getstaffprobationdetail->probation_completed =='yes') {

					// echo "jbkjnkj"; die;

						$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td width="10%">&nbsp;</td>
						<td colspan="3"><p align="center"><strong>  Letter Informing Employee of Completion of Probation</strong></p></td>
						<td width="10%">&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td width="27%">Ref: Personal Dossier of Employee</td>
						<td width="27%">&nbsp;</td>
						<td width="26%">Date: '. date('d/m/Y') .'</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>To</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Name of Probationer: '.$content['staff_details']->name.' </p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">Employee Code: '.$content['staff_details']->emp_code.'</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Designation:  '.$content['staff_details']->desname.'</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Location: '.$content['staff_details']->officename.'</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">Subject:<strong>Completion of Probation</strong></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Dear  '.$content['staff_details']->name.',</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>You were appointed in PRADAN as '.$content['staff_details']->desname.' with effect  from '.$this->gmodel->changedatedbformate($content['staff_details']->doj_team).' .</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>I am happy to inform you that, based  on the review of your performance, you have satisfactorily completed the period  of your probation with effect from '.$effective_date.' (date).</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>I am sure that you will continue to  perform well in the future too.</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Wishing you a long and purposeful  association with PRADAN.</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Yours sincerely,</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>('. $ed_name .')<br />
						Executive Director</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						</table>'; 

					}elseif ($getstaffprobationdetail->probation_completed =='no' && $getstaffprobationdetail->probation_extension_date != NULL) {

						$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td width="10%">&nbsp;</td>
						<td colspan="3"><p align="center"><strong> LETTER INFORMING EMPLOYEE ABOUT EXTENSION OF PERIOD OF PROBATION</strong></p></td>
						<td width="10%">&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td width="27%">Ref: Personal Dossier of Employee</td>
						<td width="27%">&nbsp;</td>
						<td width="26%">Date: '. date('d/m/Y') .'</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>To</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Name of Probationer: '.$content['staff_details']->name.' </p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">Employee Code: '.$content['staff_details']->emp_code.'</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Designation:  '.$content['staff_details']->desname.'</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Location: '.$content['staff_details']->officename.'</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">Subject:<strong>Extension of Period of Probation</strong></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Dear  '.$content['staff_details']->name.',</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>You were appointed in PRADAN as '.$content['staff_details']->desname.' (designation) with effect from '.$this->gmodel->changedatedbformate($content['staff_details']->doj_team).' (date). As per the terms and conditions of your appointment in PRADAN, you were to be on probation up to '.$this->gmodel->changedatedbformate($getstaffprobationreview->probation_extension_date).' (date).</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Based on the review of your performance during above period of six months, we have decided to extend the period of your probation for a period of  '.$content['getstaffprovationreviewperformance']->month.' (months).  We hope that this will provide you adequate opportunity to enhance your performance</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">The period of your probation is therefore extended up to and including '.$this->gmodel->changedatedbformate($getstaffprobationreview->probation_extension_date).' (date).  I hope you would view this as a challenge, and give your fullest to your work in the coming few months.</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>&nbsp;</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>

						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Yours sincerely,</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>('. $ed_name .')<br />
						Executive Director</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						</table>'; 


					}elseif($getstaffprobationdetail->probation_completed=='no' && $getstaffprobationdetail->probation_extension_date == NULL ) {

						$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td width="10%">&nbsp;</td>
						<td colspan="3"><p align="center"><strong> LETTER INFORMING EMPLOYEE ABOUT TERMINATION OF SERVICE DUE TO UNSATISFACTORY PERFORMANCE DURING THE PERIOD OF PROBATION</strong></p></td>
						<td width="10%">&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td width="27%">Ref: Personal Dossier of Employee</td>
						<td width="27%">&nbsp;</td>
						<td width="26%">Date: '. date('d/m/Y') .'</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>To</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Name of Probationer: '.$content['staff_details']->name.' </p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">Employee Code: '.$content['staff_details']->emp_code.'</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Designation:  '.$content['staff_details']->desname.'</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Location: '.$content['staff_details']->officename.'</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">Subject:<strong>Termination of Service Due to Unsatisfactory Performance During the Period of Probation</strong></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Dear  '.$content['staff_details']->name.',</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>You were appointed as '.$content['staff_details']->desname.' (designation) with effect from  '.$this->gmodel->changedatedbformate($content['getstaffprovationreviewperformance']->doj_team).'(date) and were under probation for a period of six months from the said date as per our Offer of Appointment No.'.$content['getstaffprovationreviewperformance']->offerno.' dated '.$this->gmodel->changedatedbformate($content['getstaffprovationreviewperformance']->probation_date).' duly accepted by you.</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Based on the review of your performance during the period of probation, I am sorry to inform you that we are not in a position to confirm completion of your probation. I therefore write to inform you that we would no longer require your services with effect from the afternoon of '.$this->gmodel->changedatedbformate($content['getstaffprovationreviewperformance']->probation_date).' (date).</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">I am marking a copy of this letter to the Finance-Personnel-MIS Unit, so that they may help you settle your account.  Dues, if any, will be payable after you have furnished ‘NO DUES CERTIFICATE’ from all concerned.</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>&nbsp;</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>

						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>Yours sincerely,</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3"><p>('. $ed_name .')<br />
						Executive Director</p></td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td colspan="3">&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						</table>'; 
					}else if($getstaffprobationdetail->probation_completed =='not_recommended' && $getstaffprobationdetail->satisfactory == 'notsatisfactory'){
						$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td colspan="3"><p align="center">(<em>on PRADAN&rsquo;s Letter Head</em>)<strong> </strong><br />
						  <strong> Letter Informing Employee About Termination  of Service Due to Unsatisfactory Performance During the Extended Period of  Probation</strong></p></td>
						</tr>
						<tr>
						<td width="39%">Ref: Personal Dossier of Employee&nbsp;&nbsp;&nbsp; </td>
						<td width="24%">&nbsp;</td>
						<td width="37%"><p>Date: '. date('d/m/Y') .'</p></td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td><p>To</p></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td><p>Name of Probationer: '.$content['staff_details']->name.'</p></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>Employee Code: '.$content['staff_details']->emp_code.'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td><p>Designation: '.$content['staff_details']->desname.'</p></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td><p>Location: '.$content['staff_details']->officename.'</p></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>Subject:<strong>Termination of Service Due to Unsatisfactory  Performance During the Extended Period of Probation</strong></p></td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td><p>Dear '.$content['staff_details']->name.',</p></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>You were appointed as '.$content['staff_details']->desname.' (designation) with effect  from (date) and were  under probation for a period of six months from the said date as per our Offer  of Appointment No.'.$content['staff_details']->offerno.' dated '.$this->gmodel->changedatedbformate($content['staff_details']->probation_date).' duly accepted by you.</p></td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>Based on the review of your  performance, and our assessment that it was unsatisfactory during the initial  period of probation, your probation was extended for a period of '.$this->gmodel->changedatedbformate($content['getstaffprobationreview']->probation_extension_date).' months.  We had shared this decision with you through letter no. '.$token.' dated  '.$this->gmodel->changedatedbformate($content['getstaffprobationreview']->probation_extension_date).' in the hope that this would provide you adequate opportunity to  improve your performance. Here, too, we had stated clearly that in case this  does not happen, we would not be in a position to continue your service with  PRADAN.</p></td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>Your performance during the extended  period of probation has been reviewed recently, and I am indeed sorry to inform  you that, based on the assessment of your performance, we are not in a position  to confirm completion of your probation. I therefore write to inform you that  we would no longer require your services with effect from the afternoon of  '.$this->gmodel->changedatedbformate($content['staff_details']->probation_date).' (date). </p></td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>I am marking a copy of this letter  to the Finance-Personnel-MIS Unit, so that they may help you settle your  account. Dues, if any, will be payable after you have furnished &lsquo;NO DUES  CERTIFICATE&rsquo; from all concerned.</p></td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>Yours sincerely, </p>
						<p>('. $ed_name .')<br />
						Executive Director</p></td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3"><p>cc: - Team Coordinator<br />
						- Integrator- Finance-Personnel-MIS  Unit</p>
						</td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						</table>';
					}
							
							
							$filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$generate='';
				$generate =   $this->Dompdf_model->midtem_letter_PDF($html,$filename, NULL,'filename.pdf');
				
                 $filename = $filename; 
                 $attachments = array($filename.'.pdf');




                 	if ($getstaffprobationdetail->probation_completed =='yes') {

				$insertstaffArr = array(
					'probation_completed'	=> 1,
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);

				$this->db->where('staffid',$staff_id);
				$this->db->update('staff', $insertstaffArr);
			}
			$insertArr = array(
				'flag'				   => 7,
				'filename'             => (trim($filename)==""?null:($filename)), 
				'updatedby'            => $this->loginData->staffid,
				'updatedon'            => date('Y-m-d H:i:s'),
			);





		       // print_r($insertArr); die;
			$this->db->where('id',$probationid);
			$this->db->update('tbl_probation_review_performance', $insertArr);


			
				$insertworkflowArr = array(
					'r_id'                 => (trim($token)==""?null:($token)),
					'type'                 => 22,
					'staffid'              => $staff_id,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $content['getstaffhrd']->staffid,
					'senddate'             => date("Y-m-d H:i:s"),
					'forwarded_workflowid' => $content['workflowid']->workflowid,
					'flag'                 => 9,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
			// print_r($insertworkflowArr); die;
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


				$uparray=array(
					'effective_date'=>$effective_date
				);
				$this->db->where('staffid',$staff_id);
				$this->db->update('staff_transaction', $uparray);
				
				$filename = $filename; 
				$attachments = array($filename.'.pdf');
				$extendedhtml='';
				$confirmhtml='';
				if($getstaffprobationdetail->probation_completed == 'no' && $getstaffprobationdetail->probation_extension_date != '') {

					$subject = 'Your Probation date are extended by pardan';
				}elseif ($getstaffprobationdetail->probation_completed=='notrecomme') {

					$notcompletehtml = 'Unsatisfactory performance during the period of probation';
						$subject = 'Unsatisfactory performance during the period of probation';

				}elseif ($getstaffprobationdetail->probation_completed =='yes') {

					$subject = 'You are confirm by pardan';
				}else if($getstaffprobationdetail->probation_completed =='not_recommended' && $getstaffprobationdetail->satisfactory == 'notsatisfactory'){
					$subject = 'Unsatisfactory performance during the period of probation';
				}
					
				$satus='';
				
				$flag1=$content['workflowid']->flag;
				//echo $content['getstaffPdetails']->name;
				//echo $flag1;
				//die;
		
				if($flag1==7)
					{
						$satus="Midtem Review Process approved by".$content['getstaffPdetails']->name;
					}
					else if($flag1==8)
					{
						$satus="Midtem Review Process rejected by".$content['getstaffPdetails']->name;	
					}
						


						

 				

                 $html = $satus.'<br>Dear '.$content['staff_details']->name.', <br><br> 
                 Employee Code: '.$content['staff_details']->emp_code.'<br><br>
                 Designation:  '.$content['staff_details']->desname.'<br><br>
                 Location:  '.$content['staff_details']->officename.'<br><br>

                 '.$subject.'

                 <br>
                 <br><br>
                 Regard,<br>Pradan Team';





			// echo $to_useremail;
			// die;



			// 		$to_useremail = $staff_email;



     
					$tc_email=$content['tc_email']->emailid;
					$hremail=$content['hr_email']->EmailID;
					$personal=$content['p_email']->EmailID;
					$e_email=$content['ed_email']->EmailID;

        //$to_hremail = $hremail->EmailID;
					$recipenent= array (
						$tc_email      =>'tc',
						$hremail=>'hrd',
						$personal =>'personal',
						$e_email='ed'
					);





				$this->Common_Model->midemreview_send_email($subject, $html,$staff_email ,$e_email,$recipenent,$attachments);
					





			

				

				

				
				
				$this->db->trans_complete();

				if ($this->db->trans_status() === true){
					

					$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
					redirect('/Staff_personnel_midterm_review/index/');	
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
					redirect(current_url());
				}

			}
						
		
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
			//  print_r($content['getpersonnaldetals']);
			// die;
			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);



			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
	}
		
	}

	public function finalview($staffid,$token)
	{
		if (empty($staffid&&$token) || $token == ''&& $staffid=='' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token or $staffid  is either blank or empty.');
				
				
			} else {
		// start permission 
		try{
			//echo "dsfsd=".$staffid; die;

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission  
			$content['staff_details'] = $this->model->getstaff($token);


			$staff_id = $content['staff_details']->staffid;
			
				$reporting_id = $content['staff_details']->reportingto;
			$content['reportingto_image'] = $this->Common_Model->staff_signature($reporting_id);

			
			$content['tc_email'] = $this->Staff_midterm_review_model->superviser_email($reporting_id);
			
			
			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);

			$probationid='';
			$probationid = $getstaffprobationdetail->id;
			$transid = $getstaffprobationdetail->transid;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;
			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			$content['getstaffprobationreview'] = $getstaffprobationdetail;
			
			
			$getstaffprobationdetail=$content['getstaffprobationreview'];
			//echo "<pre>";
			$content['getstaffhrd'] = $this->model->getStaffHRDList();
			// print_r($content['getstaffhrd']);
			// die;
			

			$content['getstaffPdetails'] = $this->model->getStaffPersonnalList();

			$content['getstaffprobationreview'] = $getstaffprobationdetail; 
			
			
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
			// echo "<pre>";
			// print_r($content['getpersonnaldetals']);
			// 	die;
			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);
			//$content['getstaffprovationreviewperformance']=$content['getstaffprovationreviewperformance'];	



			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		}
	}


	public function hrview($token)
	{
		// start permission 
		try{
			//echo "dsfsd"; die;

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			//$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($content['getSupervisordetals']);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			//echo "<pre>";
		// print_r($getstaffprobationdetail);
			$content['geted_detals'] = $this->model->getEDName();

			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$content['getstaffhrd'] = $this->model->getStaffHRDList();

			$content['getstaffPdetails'] = $this->model->getStaffPersonnalList();


			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
				// echo "<pre>";
				//print_r($this->input->post()); 


				$this->db->trans_start();
				// $sql2="select "

				$sql = "SELECT max(workflowid) as maxworkflowid FROM tbl_workflowdetail WHERE staffid=".$token;
				$res = $this->db->query($sql)->result()[0];


				$ed_date = $this->input->post('ed_date');
				$ed_date_db = $this->gmodel->changedatedbformate($ed_date);

				$insertArr = array(

					'agree'                => $this->input->post('agree'),
					'flag'				   => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				$insertArr = array(

					'trans_flag'           => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$token);
				$this->db->update('staff_transaction', $insertArr);
				

				if ($res>0) {

					$insertworkflowArr = array(
						'r_id'                 => $probationid,
						'type'                 => 22,
						'staffid'              => $token,
						'sender'               => $this->loginData->staffid,
						'receiver'             =>  $this->input->post('sendtohtd'),
						'senddate'             => date("Y-m-d H:i:s"),
						'forwarded_workflowid' => $res->maxworkflowid,
						'flag'                 => $this->input->post('edstatus'),
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);
					//echo $this->db->last_query();
				//die;

				}else{
					$insertworkflowArr = array(
						'r_id'                 => $probationid,
						'type'                 => 22,
						'staffid'              => $token,
						'sender'               => $this->loginData->staffid,
						'receiver'             => $this->input->post('sendtohtd'),
						'senddate'             => date("Y-m-d H:i:s"),
						'flag'                 => $this->input->post('edstatus'),
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);

				}

				
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				//echo $this->db->last_query(); die;

				$this->db->trans_complete();

				if ($this->db->trans_status() === true){


					$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
					redirect('/Probation_ed_reviewofperformance/hrview/'.$token);	
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
					redirect(current_url());
				}

			}
			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
				// print_r($content['getstaffprovationreviewperformance']);
				// die;
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
			//print_r($content['getpersonnaldetals']);

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);



			$content['title'] = 'Probation_ed_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function fullview($token)
	{
		// start permission 

		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			//print_r($content['getstaffprovationreviewperformance']); 

				// $getstaffprobationdetail=$content['getstaffprovationreviewperformance'];
				// print_r($getstaffprobationdetail);
			//echo die;
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($content['getSupervisordetals']);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			$content['getstaffprobationreview'] = $getstaffprobationdetail;
			//print_r($content['getstaffprobationreview']);
			//echo "<pre>";
		 //

			//print_r($getstaffprobationdetail); die;

			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail; 
			//print_r($content['getstaffprobationreview']);

			$edname = $this->model->getEDName($content['getstaffprobationreview']->edid);

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
				

				$this->db->trans_start();




				$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td width="10%">&nbsp;</td>
				<td colspan="3"><p align="center"><strong>  Letter Informing Employee of Completion of Probation</strong></p></td>
				<td width="10%">&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td width="27%">Ref: Personal Dossier of Employee</td>
				<td width="27%">&nbsp;</td>
				<td width="26%">Date: '. date('d/m/Y') .'</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>To</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>Name of Probationer: '.$content['getstaffprovationreviewperformance']->name.' </p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">Employee Code: '.$content['getstaffprovationreviewperformance']->emp_code.'</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>Designation:  '.$content['getstaffprovationreviewperformance']->desname.'</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>Location: </p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">Subject:<strong>Completion of Probation</strong></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>Dear  '.$content['getstaffprovationreviewperformance']->name.',</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>You were appointed in PRADAN as '.$content['getstaffprovationreviewperformance']->desname.' (designation) with effect  from '. date('d/m/Y') .' (date).</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>I am happy to inform you that, based  on the review of your performance, you have satisfactorily completed the period  of your probation with effect from '. date('d/m/Y') .'  (date).</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>I am sure that you will continue to  perform well in the future too.</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>Wishing you a long and purposeful  association with PRADAN.</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>Yours sincerely,</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3"><p>('. $edname->name .')<br />
				Executive Director</p></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td colspan="3">&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				</table>';
				$filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$generate =   $this->Dompdf_model->generatePDF($html, $filename, NULL,'Generateofferletter.pdf');


         $hrdemail       = $this->loginData->EmailID; /////// hrd email id/////
         $staffemail     = 'amit.kum2008@gmail.com';

			// echo $newpassword; die;

		 	 $filename = 'e49348d7c5fb41a69976fe0483b15b67'; //die;
		 	 $attachments = array($filename.'.pdf');

			// print_r($attachments);  die;

		 	 $subject = 'Dear '.$content['getstaffprovationreviewperformance']->name.', <br><br> 
		 	 Congratulations you are confirm by Pradan. 
		 	 <br>
		 	 <br><br>
		 	 ';
		 	 $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
		 	 <tr>
		 	 <td width="10%">&nbsp;</td>
		 	 <td colspan="3"><p align="center"><strong>  Letter Informing Employee of Completion of Probation</strong></p></td>
		 	 <td width="10%">&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td width="27%">Ref: Personal Dossier of Employee</td>
		 	 <td width="27%">&nbsp;</td>
		 	 <td width="26%">Date: '. date('d/m/Y') .'</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>To</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>Name of Probationer: '.$content['getstaffprovationreviewperformance']->name.' </p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">Employee Code: '.$content['getstaffprovationreviewperformance']->emp_code.'</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>Designation:  '.$content['getstaffprovationreviewperformance']->desname.'</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>Location: </p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">Subject:<strong>Completion of Probation</strong></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>Dear  '.$content['getstaffprovationreviewperformance']->name.',</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>You were appointed in PRADAN as '.$content['getstaffprovationreviewperformance']->desname.' (designation) with effect  from '. date('d/m/Y') .' (date).</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>I am happy to inform you that, based  on the review of your performance, you have satisfactorily completed the period  of your probation with effect from '. date('d/m/Y') .'  (date).</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>I am sure that you will continue to  perform well in the future too.</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>Wishing you a long and purposeful  association with PRADAN.</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>Yours sincerely,</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3"><p>('. $edname->name .')<br />
		 	 Executive Director</p></td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 <tr>
		 	 <td>&nbsp;</td>
		 	 <td colspan="3">&nbsp;</td>
		 	 <td>&nbsp;</td>
		 	 </tr>
		 	 </table>';



		 	 $to_useremail = 'pdhamija2012@gmail.com';

       // $to_hremail = $hremail->EmailID;
		 	 $this->Common_Model->midemreview_send_email($subject, $html, $to_useremail);
   /// Send Mai


		 	 $insertArr = array(
		 	 	'flag'				   => 5,
		 	 	'filename'             => $filename,
		 	 	'updatedby'            => $this->loginData->staffid,
		 	 	'updatedon'            => date('Y-m-d H:i:s'),
		 	 );

		 	 $this->db->where('id',$probationid);
		 	 $this->db->update('tbl_probation_review_performance', $insertArr);

		 	 $insertworkflowArr = array(
		 	 	'r_id'                 => $probationid,
		 	 	'type'                 => 7,
		 	 	'staffid'              => $token,
		 	 	'sender'               => $this->loginData->staffid,
		 	 	'receiver'             => $token,
		 	 	'senddate'             => date("Y-m-d H:i:s"),
		 	 	'flag'                 => 5,
		 	 	'createdon'            => date("Y-m-d H:i:s"),
		 	 	'createdby'            => $this->loginData->staffid,
		 	 );

		 	 $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
		 	 $this->db->trans_complete();

		 	 if ($this->db->trans_status() === true){

		 	 	$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
		 	 	redirect('/Probation_ed_reviewofperformance/fullview/'.$token);	
		 	 }else{
		 	 	$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
		 	 	redirect(current_url());
		 	 }

		 	}



			//print_r($content['getstaffpersonneldetals']);


			// $content['getedname'] = $this->model->getEDName();
		 	$content['title'] = 'Probation_ed_reviewofperformance';
		 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		 	$this->load->view('_main_layout', $content);
		 }
		 catch(Exception $e)
		 {
		 	print_r($e->getMessage());
		 	die();
		 }

		}




	}