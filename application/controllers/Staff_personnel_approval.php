<?php 

/**
* Staff Personnel Approval List
*/
class Staff_personnel_approval extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model('Dompdf_model');
   $this->load->model("Staff_personnel_approval_model", "Staff_personnel_approval_model");
   $this->load->model("Staff_seperation_model", "Staff_seperation_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');

 }


  /**
   * Method index() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */

  public function index()
  {
    try{
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $content['getworkflowdetail'] = $this->Staff_personnel_approval_model->getworkflowdetaillist();
   // echo "<pre>";
   // print_r($content['getworkflowdetail']);
   // die;


   $content['gettransferworkflowdetail'] = $this->Staff_personnel_approval_model->get_transfer_promotion_workflowdetail();

   $content['getpromotionworkflowdetail'] = $this->Staff_personnel_approval_model->get_promotion_workflowdetail();


   $content['getseprationworkflowdetail'] = $this->Staff_personnel_approval_model->getseprationworkflowdetaillist();
   // echo "<pre>";
   // print_r( $content['getseprationworkflowdetail']);exit();
   /*$content['acceptance_detail'] = $this->Staff_sepcacceptanceofresignation_model->acceptance_detail($token);*/
   
   /*echo "<pre>";
   print_r($content['gettransferworkflowdetail']);exit();*/


   
   //$content['subview']="index";
   $content['title'] = 'Staff_personnel_approval';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



 /**
   * Method add_personnel_transfer_approval() Approval process accept & rejected .
   * @access  public
   * @param Null
   * @return  Array
   */


 public function add_personnel_transfer_approval()
 {

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 


    $staffid = $this->uri->segment(3);
    $tarnsid  = $this->uri->segment(4);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == 'POST'){

      // echo "<pre>";
      // print_r($this->input->post()); //die;

      $transid = $this->input->post('tarnsid');
      $status = $this->input->post('status');


      $transferstaffdetails = $this->Staff_personnel_approval_model->getTransferStaffDetails($transid);

       //print_r($transferstaffdetails); die;

       $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
       $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.



       // $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
       $this->form_validation->set_rules('status','Select Status','trim|required');

       if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }

      $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail 
      WHERE r_id = $transid";
      $result  = $this->db->query($sql)->result()[0];

      $forwardworkflowid = $result->workflowid;


      
        if($status == 5){     //// Status => Personnel Approve 

         $this->db->trans_start();

         $insertArr = array(

          'new_office_id'     => $NewOfficeId,
          'reportingto'       => $NewReportingto ,
          'updatedon'         => date("Y-m-d H:i:s"),
          'updatedby'         => $this->loginData->staffid
        );

         /// 'doj_team'          => $NewDateOfTransfer
         
         $this->db->where('staffid',$staffid);
         $this->db->update('staff', $insertArr);

//echo $this->db->last_query(); die;

         $updateArr = array(

          'trans_flag'     => 5,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $updateArr);
//echo $this->db->last_query(); die;


         $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 2,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $this->loginData->staffid,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 5,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
      //  echo $this->db->last_query(); die;


         $this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_personnel_approval/'.$token);

        }

      }


        if ($status ==6) {   //// Status => Personnel Rejected 

         $insertArr = array(

          'trans_flag'     => 6,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 2,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $this->loginData->staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => 6,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('Staff_personnel_approval');

        }
      }

    }

    prepareview:

   //$content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    $content['subview'] = 'Staff_personnel_approval/Add_personnel_transfer_approval';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


 /**
   * Method add() Approval process accept & rejected .
   * @access  public
   * @param Null
   * @return  Array
   */


 public function Add()
 {

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 


    $staffid = $this->uri->segment(3);
    $tarnsid  = $this->uri->segment(4);
   
    
     //$sql="select flag,workflowid from tbl_workflowdetail where workflowid=(select max(workflowid) from tbl_workflowdetail) AND r_id=$tarnsid ";
     // echo $sql;
     // die;

         $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail 
         WHERE r_id = $tarnsid";
      $result  = $this->db->query($sql)->row();
     
       $forwardworkflowid =  $result->workflowid;

      $sql  = "SELECT flag FROM tbl_workflowdetail 
        WHERE workflowid = $forwardworkflowid";
      $result1  = $this->db->query($sql)->row();
      $content['workflow_flag']=$result1;
      // print_r($content['workflow_flag']);
      // die;


     // $forwardworkflowid = $content['workflow_flag']->workflowid;
      // print_r($content['workflow_flag']);
      // die;

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

     if($RequestMethod == 'POST'){

    // echo "<pre>";
    // print_r($this->input->post()); die();

      $transid = $this->input->post('tarnsid');
      $status = $this->input->post('status');

      $personnel='';
      $personnel = $this->gmodel->getpersonalAdministrator_details();
      // print_r($personnel);
      // die;
      // $arrr = array();
      //  foreach ($personnel as $key => $value)
      //  {
      //   array_push($arrr, $value->personnelstaffid);
      //  }
      // print_r($arrr); die;
      $transferstaffdetails = $this->Staff_personnel_approval_model->getTransferStaffDetails($transid);

       // print_r($transferstaffdetails); die;

      $staff_details = $this->Common_Model->get_Staff_detail($transferstaffdetails->staffid);
      // print_r($staff_details); die();

       $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
       $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.



       $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
       $this->form_validation->set_rules('status','Select Status','trim|required');

       if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }





      
        if($status == 7){     //// Status => Personnel Approve 

         $this->db->trans_start();

        //  $insertArr = array(

        //   'new_office_id'     => $NewOfficeId,
        //   'reportingto'       => $staffid,
        //   'updatedon'         => date("Y-m-d H:i:s"),
        //   'updatedby'         => $this->loginData->staffid
        // );

        //  /// 'doj_team'          => $NewDateOfTransfer
         
        //  $this->db->where('staffid',$staffid);
        //  $this->db->update('staff', $insertArr);

//echo $this->db->last_query(); die;

         $updateArr = array(

          'trans_flag'     => 7,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $updateArr);
//echo $this->db->last_query(); die;

        // foreach ($arrr as $key => $value)
     //  {
         $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 3,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => 15,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 7,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
      //  echo $this->db->last_query(); die;
      // }

         $this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }



        else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_personnel_approval/'.$token);

        }

      }
      if($status == 15){     //// Status => Personnel Approve 

         $this->db->trans_start();

        //  $insertArr = array(

        //   'new_office_id'     => $NewOfficeId,
        //   'reportingto'       => $staffid,
        //   'updatedon'         => date("Y-m-d H:i:s"),
        //   'updatedby'         => $this->loginData->staffid
        // );

        //  /// 'doj_team'          => $NewDateOfTransfer
         
        //  $this->db->where('staffid',$staffid);
        //  $this->db->update('staff', $insertArr);

//echo $this->db->last_query(); die;

         $updateArr = array(

          'trans_flag'     => 15,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $updateArr);
//echo $this->db->last_query(); die;

       //   foreach ($arrr as $key => $value)
       // {
         $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 3,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $personnel->staffid,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 15,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
      //  echo $this->db->last_query(); die;
       //}

         $this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }



        else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_personnel_approval/'.$token);

        }

      }
      if($status == 19){     //// Status => Personnel Approve 

         $this->db->trans_start();

        //  $insertArr = array(

        //   'new_office_id'     => $NewOfficeId,
        //   'reportingto'       => $staffid,
        //   'updatedon'         => date("Y-m-d H:i:s"),
        //   'updatedby'         => $this->loginData->staffid
        // );

        //  /// 'doj_team'          => $NewDateOfTransfer
         
        //  $this->db->where('staffid',$staffid);
        //  $this->db->update('staff', $insertArr);

//echo $this->db->last_query(); die;

         $updateArr = array(

          'trans_flag'     => 19,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $updateArr);
//echo $this->db->last_query(); die;

       //   foreach ($arrr as $key => $value)
       // {
         $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 3,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $staffid,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 19,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
       // echo $this->db->last_query(); die;
      //}



         // $resignation_tendered_on = $content['acceptance_detail']->resignation_tendered_on;
                // $working_date = $content['acceptance_detail']->working_date;

                $tdate= date('d/m/Y');
                $emp_code= $staff_details->emp_code;
                // $staffadd = $content['staff_detail']->address;
                $staff_name= $staff_details->name;
                $incharger_name= $incharger_name= $this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
                // $resignation_tendered_on = $this->gmodel->changedatedbformate($resignation_tendered_on);
                // $working_date = $this->gmodel->changedatedbformate($working_date);

            $staff = array('$tdate','$emp_code','$staff_name','$incharger_name');
                $staff_replace = array($tdate,$emp_code,$staff_name,$incharger_name);
                
                $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 88 AND `isactive` = 1";
                $data = $this->db->query($sql)->row();
               
                $body='';
                if(!empty($data))

                $body = str_replace($staff,$staff_replace , $data->lettercontent);

                $filename = "";
                $filename = md5(time() . rand(1,1000));
                $this->load->model('Dompdf_model');
                $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExitInterviewForm.pdf');
                $attachments = array($filename.'.pdf'); 

              $subject = "The Exit Interview Form";
                        $html = 'Dear '.$staff_details->name.',<br><br>';
                        $html .= 'Please fill The Exit Interview Form';
                         $to_email = $staff_details->emailid;
                        // echo  $to_email;
                        // die;

                 
                  // $to_name = $receiverdetail->name;
                  //$to_cc = $getStaffReportingtodetails->emailid;
                  // $recipients = array(
                  //    $personnel_detail->EmailID => $personnel_detail->UserFirstName,
                  //    $content['staff_detail']->emailid => $content['staff_detail']->name
                  //    // ..
                  // );
                  $email_result = $this->Common_Model->send_email($subject, $html, $to_email, $to_name = null, $recipients=null,$attachments);
                  // echo  $email_result;
                  // die;
                  if (substr($email_result, 0, 5) == "ERROR") {
                    $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
                  }



         $this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }



        else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_personnel_approval/'.$token);

        }

      }
       if($status == 23){     //// Status => Personnel Approve 

         $this->db->trans_start();

        

//echo $this->db->last_query(); die;

         $updateArr = array(

          'trans_flag'     => 23,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $updateArr);
//echo $this->db->last_query(); die;

         foreach ($arrr as $key => $value)
       {
         $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 3,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $staffid,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 23,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
       // echo $this->db->last_query(); die;
       }

         $this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }



        else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('Staff_personnel_approval');

        }

      }

        if ($status ==6) {   //// Status => Personnel Rejected 

         $insertArr = array(

          'trans_flag'     => 6,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 4,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => 6,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('Staff_personnel_approval');

        }
      }
}
    

    prepareview:

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    //$content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();

    $content['subview'] = 'Staff_personnel_approval/add';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


public function document_upload($token)
{

try{

  $this->db->trans_start();


if(isset($_FILES['othersreleving_document']['name'][0]))
{
            $i = 0;
            //matricencryptedNameArray
            $othersrelevingoriginalName1='';
            $othersrelevingencryptedImage=array();
            $othersrelevingoriginalNameArray = array();
            $othersrelevingencryptedNameArray = array();
            foreach($_FILES['othersreleving_document']['name'] as $res){

              // echo "<pre>";
              // print_r(end((explode(".", $_FILES['10thcertificate']['name'][$i]))));
              //die;  
              @  $ext = end((explode(".", $_FILES['othersreleving_document']['name'][$i]))); 

              $othersrelevingoriginalName1 = $_FILES['othersreleving_document']['name'][$i];
              array_push($othersrelevingoriginalNameArray, $othersrelevingoriginalName1); 
              $othersrelevingencryptedName1 = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;      
              array_push($othersrelevingencryptedNameArray, $othersrelevingencryptedName1); 
              $tempFile = $_FILES['othersreleving_document']['tmp_name'][$i];
              $targetPath = FCPATH . "pdf_separationletters/";
              $targetFile = $targetPath . $othersrelevingencryptedName1;

              $uploadResult = move_uploaded_file($tempFile,$targetFile);
              
              if($uploadResult == true){
                $othersrelevingencryptedImage = $othersrelevingencryptedNameArray;
              }else{

                die("Error uploading 10thcertificate Document ");
                $this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
                redirect(current_url());
              }
              $i++;
            }

          }
        
         
          
            
//print_r($matricencryptedNameArray);

echo $othersrelevingencryptedImage = implode('|',$othersrelevingencryptedNameArray);
$updatereleving_array=array(
  'relieved_document'=>$othersrelevingencryptedImage,
    'flag'=>2,
    'updatedby'=>$this->loginData->staffid,
    'updatedon'=>date('Y-m-d H:i:s')


);

$this->db->where('transid', $token);

  $this->db->update('tbl_sep_releaseform', $updatereleving_array);

  
$this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }



        else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_personnel_approval/');

        }




  //   $dataset=array();
  //   // print_r($_FILES['appointment_others_document']['name']);
  //   // die;
  // if(isset($_FILES['appointment_others_document']['name']))
  //          {
  //              if($_FILES['appointment_others_document']['name'][0] != NULL)
  //              {
  //     foreach($_FILES['appointment_others_document']['name'] as $k=>$v){

  //       @  $ext = end((explode(".", $_FILES['appointment_others_document']['name'][$k])));
  //       $matricoriginalName = $_FILES['appointment_others_document']['name'][$k]; 
  //       $matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
  //       $tempFile = $_FILES['appointment_others_document']['tmp_name'][$k];
  //       $targetPath = FCPATH . "pdf_offerletters/";
  //       $targetFile = $targetPath . $matricencryptedName;
  //       $uploadResult = move_uploaded_file($tempFile,$targetFile);
  //  array_push($offerattachments, $matricencryptedName);
  //      //die;

  //       $dataset  = array(
  //         'candidateid'=> $token,
  //         'originaldocumentname'=> $matricoriginalName,
  //         'encrypteddocumentname'=> $matricencryptedName,
  //         'createdon'=> date('Y-m-d H:i:s'),
  //         'createdby'=> $this->loginData->UserID,
  //       );
  //      // $this->db->insert('tbl_offers_othersdocument', $dataset);
  //       }
  //       }
  //     }
  //     echo "<pre>";
  //       print_r($dataset);
        die;

     redirect('Staff_personnel_approval');


      prepareview:

    // //$content['recruiters_list']     = $this->model->getRecruitersList();
    //   $content['title'] = 'Staff_personnel_approval/index';
    //   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    //   $this->load->view('_main_layout', $content);


    }
  
    catch (Exception $e) 
    {
        print_r($e->getMessage());die;
    }
}



 /**
   * Method add_iom() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



 public function add_iom($token)
 {

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 
      if($this->session->userdata('login_data')->RoleID == 2){
        $content['gotourl'] = 'Staff_approval';
      }else{
        $content['gotourl'] = 'Staff_personnel_approval';
      }

      $content['staff_iomtransfer'] = $this->Staff_personnel_approval_model->getstaff_iom_transfer($token);

      $staffid = $content['staff_iomtransfer']->staffid;
      $transid = $content['staff_iomtransfer']->id;
      $content['staffid'] = $staffid;
      $content['tarnsid'] = $token;
      $content['iom_transfer_detail'] = $this->Staff_personnel_approval_model->getStaffiomtransferDetail($token);
      $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
      $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($staffid);
      $content['eddetail'] = $this->Staff_personnel_approval_model->geteddetail();
      /*echo "<pre>";
      print_r($this->input->post('save'));exit();*/

      $RequestMethod = $this->input->server("REQUEST_METHOD");

      if($RequestMethod == 'POST')
      {
        $db_flag = '';
        if($this->input->post('save') == 'Save'){
          $db_flag = 0;
        }else {
          $db_flag = 1; 
          $subject = "Your Process Approved";
          $body = 'Dear,<br><br>';
          $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
          $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
          $body .= 'Thanks<br>';
          $body .= 'Administrator<br>';
          $body .= 'PRADAN<br><br>';

          $body .= 'Disclaimer<br>';
          $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

          
          $to_email = $personnel_detail->EmailID;
          $to_name = $personnel_detail->UserFirstName;
          //$to_cc = $getStaffReportingtodetails->emailid;
          $recipients = array(
             $content['staffdetail']->emailid => $content['staffdetail']->name
             // ..
          );
          $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
          if (substr($email_result, 0, 5) == "ERROR") {
            $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
          }
        }
        
        /*echo "<pre>";
        print_r($db_flag);exit();*/
        $this->db->trans_start();
       
        $insertArray = array(
          'transferno'                    => $this->input->post('transferno'),
          'staffid'                        => $content['staff_iomtransfer']->staffid,
          'transid'                        => $token,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    => $this->gmodel->changedatedbformate($this->input->post('letter_date')),
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $this->gmodel->changedatedbformate($this->input->post('charge_responsibility_on')),
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place_id'),
          'report_for_work_place_on_date'  => $this->gmodel->changedatedbformate($this->input->post('restransferno')),
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our_id'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => $db_flag,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
             'month'                           =>$this->input->post('month')
        );

        /*echo "<pre>";
        print_r($insertArray);exit();*/

        if($content['iom_transfer_detail'])
        {
          $id = $content['iom_transfer_detail']->id;
          $updateArray = array(
          'transferno'                    => $this->input->post('transferno'),
          'staffid'                        => $content['staff_iomtransfer']->staffid,
          'transid'                        => $token,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    => $this->gmodel->changedatedbformate($this->input->post('letter_date')),
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $this->gmodel->changedatedbformate($this->input->post('charge_responsibility_on')),
          'report_for_work_place'          => $this->input->post('report_for_work_at_place_id'),
          'report_for_work_place_on_date'  => $this->gmodel->changedatedbformate($this->input->post('restransferno')),
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our_id'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => $db_flag,
          'updatedon'                       => date("Y-m-d H:i:s"),
          'updatedby'                       => $this->loginData->staffid,
             'month'                           =>$this->input->post('month')

          );
          /*echo "<pre>";
          print_r($id);exit();*/
          $this->db->where('id', $id);
          $flag = $this->db->update('tbl_iom_transfer', $updateArray);
        }else{
          $flag = $this->db->insert('tbl_iom_transfer',$insertArray);
        }
       // echo $this->db->last_query(); die;
        $this->db->trans_complete();
        if($flag) 
        {
          $this->session->set_flashdata('tr_msg','Data Saved Successfully.');
          redirect(current_url());
        } 
        else {
          $this->session->set_flashdata('er_msg','Something Went Wrong!!');
          redirect(current_url());
       }
    } /// if Post close

    $content['getstaffdetails'] = $this->Staff_personnel_approval_model->getStaffDetail($token);
      /*echo "<pre>";
      print_r( $content['getstaffdetails']);exit();*/
    $officeid = $content['getstaffdetails']->new_office_id;
    $old_office_id = $content['getstaffdetails']->old_office_id;
    /*echo "<pre>";
    print_r($content['getstaffdetails']);die;*/
    $content['getofficedetails'] = $this->Staff_personnel_approval_model->getAllOffice();

   $content['getstafflist'] = $this->Staff_personnel_approval_model->getStaffList($officeid,$content['staffid']);
   $content['getstafflistold'] = $this->Staff_personnel_approval_model->getStaffList($old_office_id,$content['staffid']);


   //print_r($getstafflist); die;

    $content['subview'] = 'Staff_personnel_approval/add_iom';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}




/**
   * Method add_iom() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



public function edit($token)
{

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    // // print_r($this->loginData);
    // $staffid = $this->uri->segment(3);
    // $tarnsid  = $this->uri->segment(4);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == 'POST'){

      /*echo "<pre>";
      print_r($this->input->post()); die;*/

      $transid = $this->input->post('tarnsid');
      $staffid = $this->input->post('staffid');
      $save = $this->input->post('save');
      $submit = $this->input->post('saveandsubmit');


      $letterdate =  $this->input->post('letter_date');
      $letter_date = $this->gmodel->changedatedbformate($letterdate);
      $chargeresponsibilityon =  $this->input->post('charge_responsibility_on');
      $charge_responsibility_on = $this->gmodel->changedatedbformate($chargeresponsibilityon);


      $resdate =  $this->input->post('restransferno');
      $chargeresdate = $this->gmodel->changedatedbformate($resdate);

      if (isset($save) && $save =='SaveDataSend') {

        $this->db->trans_start();

        $insertworkflowArr = array(
          'transferno'                     =>  $this->input->post('transferno'),
          'staffid'                        => $staffid,
          'transid'                        => $transid,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    =>  $letter_date,
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $this->input->post('charge_responsibility_on'),
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place'),
          'report_for_work_place_on_date'  => $chargeresdate,
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => 0,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
        );
        $this->db->where('id', $token);
        $this->db->update('tbl_iom_transfer', $insertworkflowArr);
        //$insertid = $this->db->insert_id();
      //  echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Inter-Office Memo has been failed !!'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Inter-Office Memo has been save successfully !!!');
          redirect('/Staff_personnel_approval/edit/'.$token);

        }


      }

      if (isset($submit) && $submit =='savesubmit') {


         $transid = $this->input->post('tarnsid');
        $staffid = $this->input->post('staffid');

        $this->db->trans_start();

        $insertworkflowArr = array(
          'transferno'                     =>  $this->input->post('transferno'),
          'staffid'                        => $staffid,
          'transid'                        => $transid,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    =>  $letter_date,
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $this->input->post('charge_responsibility_on'),
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place'),
          'report_for_work_place_on_date'  => $chargeresdate,
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => 1,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
        );

        $this->db->where('id', $token);
        $this->db->update('tbl_iom_transfer', $insertworkflowArr);
      //  echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Inter-Office Memo has been failed !!'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Inter-Office Memo has been save successfully !!!');
          redirect('/Staff_personnel_approval/view/'.$token);

        }


      }

    }

    $gettransferstaffdetails = $this->Staff_personnel_approval_model->getTransferStaffDetail($token);

  // print_r($gettransferstaffdetails);


    $content['iom_staff_transfer'] = $gettransferstaffdetails;
    

    $staffid = $gettransferstaffdetails->staffid;
    $tarnsid = $gettransferstaffdetails->transid;

    $content['personnel_name'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;


    $content['getstaffdetails'] = $this->Staff_personnel_approval_model->getStaffDetail($tarnsid);

    $content['getofficedetails'] = $this->Staff_personnel_approval_model->getAllOffice();

    $content['getstafflist'] = $this->Staff_personnel_approval_model->getStaffList();

    $content['subview'] = 'Staff_personnel_approval/edit';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}



/**
   * Method view() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



public function view($token)
{

  try{

   // echo $token; 


    $getsingletransferstaffdetails = $this->Staff_personnel_approval_model->getSingleTransferStaffDetail($token);


    $content['iom_single_staff_transfer'] = $getsingletransferstaffdetails;
    $staffid = $getsingletransferstaffdetails->staffid;
    $tarnsid = $getsingletransferstaffdetails->transid;
    $gettransferstaffdetails = $this->Staff_personnel_approval_model->getTransferStaffDetail($token);
    $content['iom_staff_transfer'] = $gettransferstaffdetails;
    $content['getstaffdetails'] = $this->Staff_personnel_approval_model->getStaffDetail($tarnsid);
    $content['getofficedetails'] = $this->Staff_personnel_approval_model->getAllOffice();
    $content['getstafflist'] = $this->Staff_personnel_approval_model->getStaffList();
    $content['personnel_name'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;

   $content['token'] = $token;

    $content['subview'] = 'Staff_personnel_approval/view';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


/**
   * Method genrate_transfer_letter() genrate transfer letter PDF .
   * @access  public
   * @param Null
   * @return  Array
   */



public function genrate_transfer_letter($token)
{

  try{


   //$content['token'] = $token;

    $personnel_name = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;

    $getsingletransferstaffdetails = $this->Staff_personnel_approval_model->getSingleTransferStaffDetail($token);

  //print_r($getsingletransferstaffdetails); die;

    $iom_single_staff_transfer = $getsingletransferstaffdetails;
    

    $staffid = $getsingletransferstaffdetails->staffid;
    $tarnsid = $getsingletransferstaffdetails->transid;

   $gettransferstaffdetails = $this->Staff_personnel_approval_model->getStaffiomtransferDetail($tarnsid);

  // print_r($gettransferstaffdetails);


    $iom_staff_transfer = $gettransferstaffdetails;

    $getstaffdetails = $this->Staff_personnel_approval_model->getStaffDetail($tarnsid);


    $html = '<div class="panel-body">
    <div class="row"> 
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
    <p style="text-align: center;"><strong> Inter-Office Memo (IOM) for Transfer</strong></p>

    <p style="text-align: center;"><em>I</em><em>nter-Office Memo</em></p>
    <p style="text-align: center;">&nbsp;</p>
    <table style="width: 996px; height: 131px;">
    <tbody>
    <tr>
    <td style="width: 532px;">
    <p>Transfer No.: 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_staff_transfer->transferno.'</label>
    </p>
    </td>

    <td style="width: 448px;"></td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>To: <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$getstaffdetails->Staffname .'</label></p>
    </td>
    <td style="width: 448px;">
    <p>Ref: (from Personal Dossier)
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $personnel_name .'</label></p>
    </td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>(Name, Designation of Employee being transferred)</p>
    </td>
    <td style="width: 448px;">
    <p>Date: <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $this->gmodel->changedatedbformate($iom_staff_transfer->letter_date).'</label></p>
    </td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>From:
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$getstaffdetails->reportingtoname.'</label>
    </p>
    </td>
    <td style="width: 448px;">
    <p>Copy: See list below</p>
    </td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>(Integrator/Executive Director)</p>
    </td>
    <td style="width: 448px;">
    <p>&nbsp;</p>
    </td>
    </tr>
    </tbody>
    </table>
    <p>&nbsp;</p>
    <p>Subject: Your Transfer from 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    '. $getstaffdetails->oldoffice .'</label> to 

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$getstaffdetails->newoffice.'</label>&nbsp;</p>
    <p></p>
    <p></p>
    <p style="text-align: justify;">I write to inform you that you have been transferred from <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    '. $getstaffdetails->oldoffice .'</label> to <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    <?php echo $getstaffdetails->newoffice;?></label> .</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">Please hand over charge of your responsibilities on <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $this->gmodel->changedatedbformate($iom_staff_transfer->responsibility_on_date).'
    </label> (date), and report for work at 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_single_staff_transfer->officename.'</label> (place) on 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$this->gmodel->changedatedbformate($iom_staff_transfer->report_for_work_place_on_date).'
    </label> (date).</p>
    <p style="text-align: justify;"></p>
    <p style="text-align: justify;">Before proceeding to your new place of posting, you would need to hand over charge of your current responsibilities to

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $iom_single_staff_transfer->currentresponsibilityto.'
    </label>

    . Please also ensure that you get a &lsquo;Clearance Certificate&rsquo; from all concerned (as per the enclosed proforma).</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">On arrival at your new place of posting, please hand in your joining report in the prescribed form to&nbsp; 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_single_staff_transfer->joiningreportprescribedformto.'
    </label> . Please also ensure that you send a copy to all concerned.</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">If necessary, you may take a cash advance from our 

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_single_staff_transfer->cashadvancefromouroffice.'
    </label>
    office to meet the travel expenses to join at the new place of your posting as per PRADAN&rsquo;s Travel Rules.</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">You may avail of journey and joining time as per PRADANs travel rules. You would be eligible to claim reimbursement of fare and local conveyance for yourself and your family on account of travel to your new place of work, actual expenses incurred to transport your vehicle and personal effects there and one month&rsquo;s basic pay as lump sum transfer allowance.&nbsp; Please claim these by filling the enclosed &ldquo;Travel Expenses Bill&rdquo; and submitting it to&nbsp; 

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    '. $getstaffdetails->reportingtoname.'</label>( <em>Designation).</em></p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;"><b>List of enclosures:</b></p>
    <ol style="text-align: justify;">
    <li>Clearance Certificate</li>
    <li>Joining Report</li>
    <li>Travel Expenses Bill</li>
    <li>Handing Over/Taking over Charge</li>
    </ol>
    <p style="text-align: justify;"><strong>&nbsp;</strong></p>
    <p style="text-align: justify;">cc: - Person concerned to whom charge is being given.</p>
    <p style="text-align: justify;"> - Team Coordinator/Integrator at the old and new places of posting</p>
    <p style="text-align: justify;">- Finance-Personnel-MIS Unit</p>
    </div>  
    </div>';

    $filename = 'Transferletter-'.md5(time() . rand(1,1000));
    $updateArr = array(
      'filename'    => $filename,
    );

    $this->db->where('id', $token);
    $this->db->update('tbl_iom_transfer', $updateArr);

     // $filename = 'Tranfer-letter';
    $this->load->model('Dompdf_model');
    $generate = $this->Dompdf_model->generate_personnel_transfer_letter_PDF($html, $filename, NULL,'transferletter.pdf');
        //     echo "sczxcxzc";

    if ( $generate == true) {

     // $hrdemail       = $this->loginData->EmailID; /////// hrd email id/////

      $getmailtransferstaffdetails = $this->Staff_personnel_approval_model->getMailSingleTransferStaffDetail($token);


    //  print_r($getmailtransferstaffdetails); 

      $to_name = 'rahulkirar7@gmail.com';

      
       $firstname             = $getmailtransferstaffdetails->staffname2;
       $to_email              = $getmailtransferstaffdetails->staffemailid;
       $staffnewoffice        = $getmailtransferstaffdetails->newofficename;
       $filename              = $getmailtransferstaffdetails->filename;
     //  die;

      $attachments = array($filename);

       //print_r($attachments);  die;

      $html1 = 'Dear '. $firstname.', <br><br> 
      Congratulations your are transfer by Pradan. 
      <br><br> New Office  - '.$staffnewoffice.'
      <br><br><br><br>
      Regards <br>
      Pradan Team <br>';

$sendmail = $this->Common_Model->transfer_letter_send_email(' : Transfer Letter ', $html1, $to_email, $to_name, $attachments);  //// Send Mail candidates With Offer Letter ////

//echo $sendmail; die;
      if ($sendmail==1) {
            $updateArr = array(
                'sendmail'    => 1,
              );

              $this->db->where('id', $token);
              $this->db->update('tbl_iom_transfer', $updateArr);
           
      }
      $this->session->set_flashdata('tr_msg', 'Successfully generate & send transfer letter !!!!'); 
      redirect('Staff_personnel_approval/view/'.$token);
    

    }else{
      $this->session->set_flashdata('er_msg', 'Error!!! failed generate transfer letter !!!!');  
     redirect('Staff_personnel_approval/view/'.$token);
    }




    // $content['token'] = $token;
    $content['subview'] = 'Staff_personnel_approval/view';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}

  public function approvemodal(){
    try{
    // if(trim($this->input->post('comments'))){
      $token = $this->input->post('receiverstaffid');
      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
      $query ="SELECT * FROM staff_transaction WHERE id =".$token;
      $content['staff_transaction'] = $this->db->query($query)->row();
      $content['staffid'] = $content['staff_transaction']->staffid;;
      $content['reportingto'] = $content['staff_transaction']->reportingto;
      $result  = $this->db->query($sql)->result()[0];
      $forwardworkflowid = $result->workflowid;
      $staffid = $result->staffid;
      $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
      $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($content['staffid']);
      $receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['staffid']);
            // staff transaction table flag update after acceptance
      $updateArr = array(                         
        'trans_flag'     => $this->input->post('status'),
        'updatedon'      => date("Y-m-d H:i:s"),
        'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        if($this->input->post('status') == 9){
          $insertworkflowArr = array(
           'r_id'                 => $token,
           'type'                 => 6,
           'staffid'              => $content['staffid'],
           'sender'               => $this->loginData->staffid,
           'receiver'             => $content['staffid'],
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $this->input->post('status'),
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
          );
          $flag = $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
          if($flag) {
            $subject = "Your Process Approved";
            $body = 'Dear,<br><br>';
            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
            $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
            $body .= 'Thanks<br>';
            $body .= 'Administrator<br>';
            $body .= 'PRADAN<br><br>';

            $body .= 'Disclaimer<br>';
            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

            
            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $content['staffdetail']->emailid => $content['staffdetail']->name
               // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
            }
            $this->session->set_flashdata('tr_msg','Data Saved Successfully.');
            redirect('Staff_personnel_approval');
          } 
          else {
            $this->session->set_flashdata('er_msg','Something Went Wrong!!');
            redirect('Staff_personnel_approval');
          }
        }else{
          //rejection part here
          $insertworkflowArr = array(
           'r_id'                 => $token,
           'type'                 => 6,
           'staffid'              => $content['staffid'],
           'sender'               => $this->loginData->staffid,
           'receiver'             => $content['staffdetail']->reportingto,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $this->input->post('status'),
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
          );
          $flag = $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
          if($flag) {
            $subject = "Clearance Certificate Disapproved";
            $body = 'Dear,<br><br>';
            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
            $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
            $body .= 'Thanks<br>';
            $body .= 'Administrator<br>';
            $body .= 'PRADAN<br><br>';

            $body .= 'Disclaimer<br>';
            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

            
            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $content['staffdetail']->emailid => $content['staffdetail']->name
               // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
            }
            $updateStaffArr = array(                         
                'flag'     => 4,
                'updatedon'      => date("Y-m-d H:i:s"),
                'updatedby'      => $this->loginData->staffid
              );

              $this->db->where('staffid',$content['staffid']);
              $this->db->update('staff', $updateStaffArr);
            $this->session->set_flashdata('tr_msg','Clearance Certificate Rejected Successfully.');
            redirect('Staff_personnel_approval');
          } 
          else {
            $this->session->set_flashdata('er_msg','Something Went Wrong!!');
            redirect('Staff_personnel_approval');
          }
        }
        }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
      // }
    }
  public function approvemodaltransfer(){
    try{
    // if(trim($this->input->post('commentstransfer'))){
      $token = $this->input->post('receiverstaffidtransfer');

      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
      $query ="SELECT * FROM staff_transaction WHERE id =".$token;
      $content['staff_transaction'] = $this->db->query($query)->row();
      $content['staffid'] = $content['staff_transaction']->staffid;
      $result  = $this->db->query($sql)->result()[0];
      $forwardworkflowid = $result->workflowid;
      $staffid = $result->staffid;
      $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
      $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($content['staffid']);
      $content['reportingto'] = $content['staffdetail']->reportingto;
      $receiverdetail = $this->Staff_seperation_model->get_staffDetails($token);
            // staff transaction table flag update after acceptance
      $updateArr = array(                         
        'trans_flag'     => 9,
        'updatedon'      => date("Y-m-d H:i:s"),
        'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 2,
         'staffid'              => $content['staffid'],
         'sender'               => $this->loginData->staffid,
         'receiver'             => $content['staffid'],
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => 9,
         'scomments'            => $this->input->post('commentstransfer'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
      $flag = $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

    // }
    if($flag) {
      $subject = "Your Process Approved";
      $body = 'Dear,<br><br>';
      $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
      $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
      $body .= 'Thanks<br>';
      $body .= 'Administrator<br>';
      $body .= 'PRADAN<br><br>';

      $body .= 'Disclaimer<br>';
      $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

      
      $to_email = $receiverdetail->emailid;
      $to_name = $receiverdetail->name;
      //$to_cc = $getStaffReportingtodetails->emailid;
      $recipients = array(
         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
         $content['staffdetail']->emailid => $content['staffdetail']->name
         // ..
      );
      $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
      if (substr($email_result, 0, 5) == "ERROR") {
        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
      }
      $this->session->set_flashdata('tr_msg','Data Saved Successfully.');
      redirect('Staff_personnel_approval');
    } 
    else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
      redirect('Staff_personnel_approval');
    }

    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  }




}