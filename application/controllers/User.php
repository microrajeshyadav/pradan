<?php
class User extends Ci_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
	}

	public function index()
	{

		 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission
		

		$sql = "SELECT * FROM `mstuser` inner join `sysaccesslevel` ON   `mstuser`. `RoleID` = `sysaccesslevel`.`Acclevel_Cd` ";
		$content['user_list'] = $this->db->query($sql)->result();
		$content['title'] = 'user';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}

	public function add(){
				 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if ($RequestMethod == 'POST') {

			$insertArr = array(
				'Username'       => $this->input->post('Username'),
				'Password'       => md5($this->input->post("password")),
				'UserFirstName'  => $this->input->post('UserFirstName'),
				'UserMiddleName' => $this->input->post('UserMiddleName'),
				'UserLastName'   => $this->input->post('UserLastName'),
				'PhoneNumber'    => $this->input->post('PhoneNumber'),
				'EmailID'        => $this->input->post('EmailID'),
				'RoleID'         => $this->input->post('RoleID'),
				'CreatedOn'      => date('Y-m-d H:i:s'),
				'CreatedBy'      => 1, // login user id
				'IsDeleted'      => 0, 
				);
			$this->Common_Model->insert_data('mstuser', $insertArr);
			$this->session->set_flashdata('tr_msg', 'Successfully added User');
			redirect('/user/');
		}
		$content['title'] = 'user';
		$role_list = $this->Common_Model->get_all_data('sysaccesslevel', '*');
		// echo "<pre>";
		// print_r($role_list); die;

		$content['role_list'] = $role_list;



		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}

	public function edit($UserID = NULL){
		
				 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if ($RequestMethod == 'POST') {

			$updateArr = array(
				'Username'       => $this->input->post('Username'),
				'Password'       => md5($this->input->post("password")),
				'UserFirstName'  => $this->input->post('UserFirstName'),
				'UserMiddleName' => $this->input->post('UserMiddleName'),
				'UserLastName'   => $this->input->post('UserLastName'),
				'PhoneNumber'    => $this->input->post('PhoneNumber'),
				'EmailID'        => $this->input->post('EmailID'),
				'RoleID'         => $this->input->post('RoleID'),
				'UpdatedOn'      => date('Y-m-d H:i:s'),
				'UpdatedBy'      => 1, // login user id
				'IsDeleted'      => 0, 
				);
			$this->Common_Model->update_data('mstuser', $updateArr,'UserID',$UserID);
			$this->session->set_flashdata('tr_msg', 'Successfully Updated user');
			redirect('/user/');
		}
		$content['title'] = 'user';
		//$user_list = $this->Common_Model->get_data('mstuser', '*', 'UserID',$UserID);
		//$content['user_list'] = $user_list[0];

		$sql = "SELECT * FROM `mstuser` inner join sysaccesslevel ON  `mstuser`. `RoleID` = `sysaccesslevel`.`Acclevel_Cd`  
				Where `mstuser`. `UserID`='$UserID'";

		$user_list = $this->db->query($sql)->row();
		$content['user_list'] = $user_list;
		// echo "<pre>";
		// print_r($user_list); die;

		$role_list = $this->Common_Model->get_all_data('sysaccesslevel', '*');
		$content['role_list'] = $role_list;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	function delete($UserID = null)
	{
		$this->Common_Model->delete_row('mstuser','UserID', $UserID); 
		$this->session->set_flashdata('tr_msg' ,"user Deleted Successfully");
		redirect('/user/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}


}