<?php 
class Staff_appointmentletterforregular extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_appointmentletterforregular_model');
		$this->load->model('General_nomination_and_authorisation_form_model');
		 $this->load->model('Dompdf_model');
		
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index()
	{
		// start permission 
		try{
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission   


		$content['designation']=$this->model->alldesignation();
		//print_r($content['designation']);
		$content['office']=$this->model->alloffice();
		//print_r($content['office']);
		
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			//print_r($_POST);
			//die();

      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
      		echo "<pre>";
			print_r($_POST);
			$ref_no=$this->input->post('ref_no');
			$dadate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('dadate'));
			$staff_name=$this->input->post('staff_name');
			$staff_address=$this->input->post('staff_address');
			$designation=$this->input->post('designation');
			$office=$this->input->post('office');
			$fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('fromdate'));
			$name_dc=$this->input->post('name_dc');
			$valid_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('valid_date'));
			$basic_salary=$this->input->post('basic_salary');
			$handover_format=$this->input->post('handover_format');
			$placeofposting=$this->input->post('placeofposting');
			
			$monthly_salary=$this->input->post('monthly_salary');
			
			
			$joining_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('joining_date'));
			$appointmentletter=array('ref_no'=>$ref_no,
				'date_appointment'=>$dadate,
				'staff_name'=>$staff_name,
				'staff_address'=>$staff_address,
				'designation'=>$designation,
				'location'=>$office,
				'fromdate'=>$fromdate,
				'dc'=>$name_dc,
				'valid_date'=>$valid_date,
				'basic_salary'=>$basic_salary,
				'joining_report'=>$handover_format,
				'monthly_salary'=>$monthly_salary,
				'joining_date'=>$joining_date,
				'placeofposting'=>$placeofposting,
				'type'=>3,
				'flag'=>0
				);

			print_r($appointmentletter);
			$this->db->insert('appointmentletterforregular', $appointmentletter);
				//$this->db->insert($insertArraydata);
				 $insertid = $this->db->insert_id();
				echo $this->db->last_query(); 

			//die();

			

				//die();
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Staff_appointmentletterforregular/edit/'.$insertid);			
				}

			}



		
			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				
				echo "<pre>";
			print_r($_POST);
			$ref_no=$this->input->post('ref_no');
			$dadate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('dadate'));
			$staff_name=$this->input->post('staff_name');
			$staff_address=$this->input->post('staff_address');
			$designation=$this->input->post('designation');
			$office=$this->input->post('office');
			$fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('fromdate'));
			$name_dc=$this->input->post('name_dc');
			$valid_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('valid_date'));
			$basic_salary=$this->input->post('basic_salary');
			$handover_format=$this->input->post('handover_format');
			$placeofposting=$this->input->post('placeofposting');
			
			$monthly_salary=$this->input->post('monthly_salary');
			
			
			$joining_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('joining_date'));
			$appointmentletter=array('ref_no'=>$ref_no,
				'date_appointment'=>$dadate,
				'staff_name'=>$staff_name,
				'staff_address'=>$staff_address,
				'designation'=>$designation,
				'location'=>$office,
				'fromdate'=>$fromdate,
				'dc'=>$name_dc,
				'valid_date'=>$valid_date,
				'basic_salary'=>$basic_salary,
				'joining_report'=>$handover_format,
				'monthly_salary'=>$monthly_salary,
				'joining_date'=>$joining_date,
				'placeofposting'=>$placeofposting,
				'type'=>3,
				'flag'=>1
				);

			print_r($appointmentletter);
			$this->db->insert('appointmentletterforregular', $appointmentletter);
				//$this->db->insert($insertArraydata);
				 $insertid = $this->db->insert_id();
				echo $this->db->last_query(); 

			//die();
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Staff_appointmentletterforregular/view/'.$insertid);			
				}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}

	


			
		}

			
			$content['title'] = 'Staff_appointmentletterforregular';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}
	public function edit($token)
	{
		// start permission 
		try{
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission   


		$content['designation']=$this->model->alldesignation();
		//print_r($content['designation']);
		$content['office']=$this->model->alloffice();
		$content['a_details']=$this->model->details_appointmentletterforregular($token);
		//print_r($content['a_details']);

		
		//print_r($content['office']);
		
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			//print_r($_POST);
			//die();

      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
      		echo "<pre>";
			print_r($_POST);
			$ref_no=$this->input->post('ref_no');
			$dadate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('dadate'));
			$staff_name=$this->input->post('staff_name');
			$staff_address=$this->input->post('staff_address');
			$designation=$this->input->post('designation');
			$office=$this->input->post('office');
			$fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('fromdate'));
			$name_dc=$this->input->post('name_dc');
			$valid_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('valid_date'));
			$basic_salary=$this->input->post('basic_salary');
			$handover_format=$this->input->post('handover_format');
			$placeofposting=$this->input->post('placeofposting');
			
			$monthly_salary=$this->input->post('monthly_salary');
			
			
			$joining_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('joining_date'));
			$appointmentletter=array('ref_no'=>$ref_no,
				'date_appointment'=>$dadate,
				'staff_name'=>$staff_name,
				'staff_address'=>$staff_address,
				'designation'=>$designation,
				'location'=>$office,
				'fromdate'=>$fromdate,
				'dc'=>$name_dc,
				'valid_date'=>$valid_date,
				'basic_salary'=>$basic_salary,
				'joining_report'=>$handover_format,
				'monthly_salary'=>$monthly_salary,
				'joining_date'=>$joining_date,
				'placeofposting'=>$placeofposting,
				'type'=>3,
				'flag'=>0
				);

			print_r($appointmentletter);
			$this->db->update('appointmentletterforregular', $appointmentletter);
			$this->db->where('appointment_id', $token);
				//$this->db->insert($insertArraydata);
				 $insertid = $this->db->insert_id();
				//echo $this->db->last_query(); 

			//die();

			

				//die();
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Staff_appointmentletterforregular/edit/'.$token);			
				}

			}



		
			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				
				echo "<pre>";
			print_r($_POST);
			$ref_no=$this->input->post('ref_no');
			$dadate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('dadate'));
			$staff_name=$this->input->post('staff_name');
			$staff_address=$this->input->post('staff_address');
			$designation=$this->input->post('designation');
			$office=$this->input->post('office');
			$fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('fromdate'));
			$name_dc=$this->input->post('name_dc');
			$valid_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('valid_date'));
			$basic_salary=$this->input->post('basic_salary');
			$handover_format=$this->input->post('handover_format');
			$placeofposting=$this->input->post('placeofposting');
			
			$monthly_salary=$this->input->post('monthly_salary');
			
			
			$joining_date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('joining_date'));
			$appointmentletter=array('ref_no'=>$ref_no,
				'date_appointment'=>$dadate,
				'staff_name'=>$staff_name,
				'staff_address'=>$staff_address,
				'designation'=>$designation,
				'location'=>$office,
				'fromdate'=>$fromdate,
				'dc'=>$name_dc,
				'valid_date'=>$valid_date,
				'basic_salary'=>$basic_salary,
				'joining_report'=>$handover_format,
				'monthly_salary'=>$monthly_salary,
				'joining_date'=>$joining_date,
				'placeofposting'=>$placeofposting,
				'type'=>3,
				'flag'=>1
				);

			print_r($appointmentletter);
			$this->db->update('appointmentletterforregular', $appointmentletter);
			$this->db->where('appointment_id', $token);
				 //$insertid = $this->db->insert_id();
				echo $this->db->last_query(); 

			//die();
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Staff_appointmentletterforregular/view/'.$token);			
				}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}

	


			
		}

			
			$content['title'] = 'Staff_appointmentletterforregular/edit';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}

	public function view($token)
	{
		// start permission 

		try{
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		
		// end permission   


		$content['designation']=$this->model->alldesignation();
		//print_r($content['designation']);
		$content['office']=$this->model->alloffice();
		//print_r($content['office']);
		$content['a_details']=$this->model->details_appointmentletterforregular($token);
		
		
			
			$content['title'] = 'Staff_appointmentletterforregular/view';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}

	public function pdf_generate($token)
	{
		try{
		// echo "token=".$token;/
		$content['a_details']=$this->model->details_appointmentletterforregular($token);
		$date_appointment= $this->General_nomination_and_authorisation_form_model->changedate($content['a_details']->date_appointment);
		$ref_no=$content['a_details']->ref_no;
		$staff_name=$content['a_details']->staff_name;
		$staff_address=$content['a_details']->staff_address;
		$desname=$content['a_details']->desname;
		$fromdate=$this->General_nomination_and_authorisation_form_model->changedate($content['a_details']->fromdate);
		$officename=$content['a_details']->officename;
		$placeofposting=$content['a_details']->placeofposting;
		$joining_report=$content['a_details']->joining_report;
		$monthly_salary=$content['a_details']->monthly_salary;
		$basic_salary=$content['a_details']->basic_salary;
		$joining_date=$this->General_nomination_and_authorisation_form_model->changedate($content['a_details']->joining_date);

		$staff = array('$ref_no','$date_appointment','$staff_name','$staff_address','$desname','$fromdate','$officename','$placeofposting','$joining_report','$basic_salary','$monthly_salary','$joining_date');
		$staff_replace = array($ref_no,$date_appointment,$staff_name,$staff_address,$desname,$fromdate,$officename,$placeofposting,$joining_report,$basic_salary,$monthly_salary,$joining_date);

// 		$html='
//     <div class="panel thumbnail shadow-depth-2 listcontainer" >
//       <div class="panel-heading">
//         <div class="row">
//          <h4 class="col-md-12 panel-title pull-center" style="margin-left:50px;">SAMPLE OFFER OF APPOINTMENT FOR REGULAR POSTS  
//          </h4>
//        </div>
//        <hr class="colorgraph"><br>
//      </div>
//      <div class="panel-body">
//       <div class="row">
//        <div class="col-md-12 text-center" style="margin-bottom: 50px;"><h5> (LEVELS 1-3)</h5></div>      

//        <div class="col-md-6 pull-left"><em>Ref.: 300/ PER/<label  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;"> $ref_no</label></div>
//        <div class="col-md-6 pull-right text-right"><em>Date: <label  style="border-radius: 0px; border: none; border-bottom: 1px solid black;" >$date_appointment</label> </em></div>
//      </div>
//      <div class="row" style="line-height: 3">
//       <div class="col-md-12">
//         Name : <label  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >$staff_name</label>
//       </div>
//       <div class="col-md-12">
//         Addres : <label  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >$staff_address</label>
//       </div>     

//     </div>
//     <p></p>
//     <p></p>
//     <p>Subject: <strong>
//             <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;">$desname</label>
                
                
//     <div class="row" style="line-height: 3">
//       <div class="col-md-12">
//         Dear Ms./Mr.,
//       </div>     
//     </div>
//     <div class="row text-left">
//       <div class="col-md-12">
//         On behalf of PRADAN, I am happy to appoint you as<label  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" > $desname</label> in PRADAN with effect from<label style="border-radius: 0px; border: none; border-bottom: 1px solid black;"  >$fromdate</label>  (d/m/y) in 

//                <label  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" > $officename</label>  (d/m/y).
//       </div>
//     </div>
//     <div class="row text-left" style="margin-top: 20px; ">
//       <div class="col-md-12">
//        Though your place of posting has been mentioned as <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >$placeofposting</label> it is subject to change due to organizational needs and in such a case you will be intimated within a period of 15 days before your joining date. The terms and conditions of your appointment are given below. Kindly read those carefully and return us a copy of this letter with your signature as a token of your agreement.
//      </div>
//    </div>
//    <div class="row text-left" style="margin-top: 20px; ">
//     <div class="col-md-12">
//       1.  Please hand over your joining report as per the enclosed format to <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >$joining_report</label> and send its duplicates to the persons/units copied on this letter.
//     </div>
//   </div>
//   <div class="row text-left" style="margin-top: 20px; ">
//     <div class="col-md-12">
//      2. You will be paid a basic salary of Rs. <label  style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  >$basic_salary</label> per month in the scale of pay of <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  >$monthly_salary</label>. In addition to your basic pay, you will be eligible for allowances/benefits detailed in the enclosed Summary of General Service Rules of PRADAN that may be modified/amended from time to time.
//    </div>
//  </div>
//  <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    3. Your services are subject to the policies and rules of PRADAN as detailed in the Summary of General Service Rules that may be modified/amended from time to time.
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    4. You will be on probation for a period of six months from the date of joining PRADAN. You will be able to continue in service only if your performance (work, conduct and suitability) is found satisfactory during the period of probation. If your performance is not found satisfactory, the period of probation may be extended up to a period of six months. However, you shall continue to be on probation until you are otherwise informed in writing.
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//     During the period of probation (including the extended period of probation, if any), your services may be terminated without any notice and/or without assigning any reason whatsoever. Similarly, during the period of probation, you can also leave PRADAN without any notice. However, the formalities of submitting proper letter of resignation and obtaining ‘No-Dues Certificate’ from all concerned shall have to be complied with before your being released from PRADAN.”
//   </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    5. After completion of the period of your probation, your services can be terminated by either side after giving one month’s (30 days’) notice in writing or payment of salary (basic pay) in lieu thereof without assigning any reason:
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//     PROVIDED that if any of the information furnished by you through your bio-data/ application form or any other document in connection with your employment with PRADAN is found to be false or incorrect OR it is found at any time that you have not revealed any relevant information, your services may be liable to be terminated forthwith without any compensation whatsoever.
//   </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//     6.  As a full-time employee of PRADAN, you cannot accept any other employment on any terms without the prior written approval of the Executive Director of PRADAN. You will also not make yourself directly or indirectly interested in the business of any other person or entity without the prior written approval of the Executive Director of PRADAN.
//   </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    7. You will be responsible for safe-keeping and return in good condition of all the office property, equipment, instruments, tools, books, vehicles, etc., which may be given to you for your use or under your custody/charge. In the event of your failure to account for the aforesaid property, etc., PRADAN shall have the right to deduct/recover the money from your dues or otherwise, and/or take such action as PRADAN may deem fit/proper.
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    8. You will observe due confidentiality with respect to all transactions and activities of PRADAN and shall not, except in performance in good faith of the duties assigned to you, disclose, communicate or part with any confidential, classified or technical information, know-how, details or data, etc., to any other person(s) at any time during your employment in PRADAN. Failure to observe this condition of employment entitles PRADAN to summarily dispense with your services without any notice.
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//     9.  Your appointment in PRADAN shall be subject to your medical examination by a registered medical practitioner of the allopathic system of medicine, possessing at least a MBBS qualification, and upon your being found fit as per her/his certificate and the test reports being furnished at the time of your joining PRADAN. Actual expenses will be reimbursed to you by PRADAN upon your filling the prescribed form for claiming reimbursement of medical expenses available at each office. This is besides the reimbursement of medical expenses referred to above.
//   </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//     10. Please submit (a) four copies of your latest passport size photograph, (b) your blood group, and (c) your certificates, testimonials, pan card, etc., (one photocopy of each) in support of your date of birth, qualifications and experience to complete your joining formalities.
//   </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    We hope that you will stay with us for long and derive a sense of fulfillment by contributing significantly and meaningfully to the organization and society. In course of time, we are sure you will also help others to learn about the practice of development and initiating action to change things on the ground.
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//     We take this opportunity to welcome you on board and look forward to a fruitful and purposive association with you.
//   </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    With my best wishes,
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//    Yours sincerely,
//  </div>
// </div>

// <div class="row text-left" style="margin-top: 50px; ">
//   <div class="col-md-12">
//   ( <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >&nbsp</label> )
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//   Executive Director
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 50px; ">
//   <div class="col-md-12">
//   <strong> Encl.:</strong> As above
//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//   cc: - Team Coordinator <br>
// - Integrator <br>
// - Finance-Personnel-MIS Unit<br>

//  </div>
// </div>
// <div class="row text-left" style="margin-top: 20px; ">
//   <div class="col-md-12">
//   I have carefully read the terms and conditions of this offer of appointment and these are acceptable to me. I shall join duty on <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >$joining_date </label>forenoon
//  </div>
// </div>

// <div class="row text-left" style="margin-top: 50px; ">
//   <div class="col-md-6">
//  Place: <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >&nbsp;</label>
//  </div>
//  <div class="col-md-6 text-right">
//  Name:      <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >&nbsp;</label> </div>
//   <div class="col-md-6">
//  Date: <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >&nbsp;</label>
//  </div>
//  <div class="col-md-6 text-right">
//  Address:       <label style="min-width: 5px; max-width:600px; border-radius: 0px; border: none; border-bottom: 1px solid black;" >&nbsp;</label>
//  </div>

// </div>





// </div>
// <div class="panel-footer text-right">
  
        
// </div>
// </div>

// ';

					//letter to generate pdf 
			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 39 AND `isactive` = '1'";
			$data = $this->db->query($sql)->row();
			if(!empty($data))
				$body = str_replace($staff,$staff_replace , $data->lettercontent);

//echo $html;
$filename = $token."_Staff_appointmentletterforregular";
$genpdf = $this->Dompdf_model->generatePDFMedicalcertificate($body, $filename, NULL,'Generateofferletter.pdf'); 
	}catch (Exception $e) {
     print_r($e->getMessage());die;
   }

	}



	
}