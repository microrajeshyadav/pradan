<?php 
class Payscaleimport extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_Model","gmodel");
		$this->load->model('Payscaleimport_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{

		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
				
		if($this->input->server('REQUEST_METHOD') == "POST")
		{

			$levelname = $this->input->post('levelname');
			$financialyear = $this->input->post('financialyear');

			$content['fyear'] = $this->gmodel->getcurrentfyear();
			$content['currentyeardata'] = $this->model->getpaysldata($levelname,$financialyear);
			
		}
		else{
			$content['fyear'] = $this->gmodel->getcurrentfyear(); 
			$content['currentyeardata'] = $this->model->getcurrentyeardata($content['fyear']);
		}

		$content['title'] = 'Pay Scale Import';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}

	
	public function add(){


		try{

			// start permission 
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
// end permission 

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				// echo "<pre>";
				// print_r($this->input->post()); 
				$copyto = $this->input->post('copyto');
				$financialyear = $this->input->post('financialyear');
				$levelname = $this->input->post('levelname');
				$Copylevelname = $this->input->post('Copylevelname');
				$iserror = false;

				$errordescription = '';
				if ($copyto== true)
				{

					if ($copyto == $Copylevelname) {
						$iserror = true;
						$errordescription += 'You can not copy data within same level.';
					}

					if ($iserror == true) {

						$errordescription = substr($errordescription, 1 ,strlen($errordescription));
						$this->session->set_flashdata('er_msg', $errordescription);
						redirect('/Payscaleimport/add');		 	

					}

					$sqldele = "DELETE FROM tblsalary_increment_slide WHERE  level='".$Copylevelname."' AND  LTRIM(fyear)=LTRIM('".$financialyear."') ";
					$resdel = $this->db->query($sqldele);



					 $sql = "insert into tblsalary_increment_slide (level, noofyear, basicsalary, increment,slide,netsalary,fyear,phase) SELECT '".$Copylevelname."', noofyear, basicsalary, increment,slide,netsalary,fyear,phase FROM tblsalary_increment_slide  WHERE level='".$levelname."' AND LTRIM(fyear)=LTRIM('".$financialyear."') order by noofyear ASC";
					
					$res = $this->db->query($sql);

					$upsql = "UPDATE msdesignation d1, msdesignation d2 SET d1.payscale = d2.payscale WHERE d1.level = '".$Copylevelname."' and d2.level = '".$levelname."'";
					$upres = $this->db->query($upsql);

					$this->session->set_flashdata('tr_msg', ' Data Copied Successfully for level '.$Copylevelname. ' !!!');
					redirect('/Payscaleimport/add');

				}
				else
				{

				$deletesql = "delete FROM tblsalary_increment_slide WHERE `level`='".$levelname."' AND LTRIM(fyear)=LTRIM('".$financialyear."')";
					 $this->db->query($deletesql);


					$payscale = $this->input->post('payscale');
					$Startphasebasic = $this->input->post('phasebasic');
					$lastbasicsal = $this->input->post('lastbasicsal');							
					$slide = $this->input->post('slide');

					$Startphaseinc = $this->input->post('Startphaseinc');
					$noofyear = $this->input->post('noofyear');
					$phasebasic = $this->input->post('phasebasic');
					$phaseinc = $this->input->post('phaseinc');
					$phaseval =0;
					$basic=0;
					$j=0;
					$inc1 = 0;				
					for ($i=0; $i < count($noofyear) -1; $i++) { 

						if ($noofyear[$i] > $noofyear[$i+1]) {
							$iserror = true;
							$errordescription = 'Kindly check no of year values, It should be in incremental order 1 !!!';
						}
					} 				 
					for ($i=0; $i < count($phasebasic) -1; $i++) { 
						if ($phasebasic[$i] > $phasebasic[$i+1]) {
							$iserror = true;
							$errordescription += ', Kindly check basic salary values, It should be in incremental order 2 !!!';
						}
					} 
					for ($i=0; $i < count($phaseinc) -1; $i++) { 
						if ($phaseinc[$i] > $phaseinc[$i+1]) {
							$iserror = true;
							$errordescription += ', Kindly check increment values, It should be in incremental order 3 !!!';
						}
					} 
					if ($lastbasicsal <= $phasebasic[count($phasebasic)-1]) {
						$iserror = true;
						$errordescription += ', Last basic should be grather than fourth phase basic !!!';
					}				
					if ($iserror == true) {
						if (substr($errordescription, 0 ,1)==',') {
							$errordescription = substr($errordescription, 1 ,strlen($errordescription));
						}

						$this->session->set_flashdata('er_msg', $errordescription);
						redirect('/Payscaleimport/add');
					}

					for ($i=$Startphasebasic[0]; $i <= $lastbasicsal ; $i++) {
						$condmatch = false;
						for ($k=0; $k < count($noofyear); $k++) { 
							if ($j==$noofyear[$k]) {
								$basic = $phasebasic[$k];
								$inc = $phaseinc[$k];
								$condmatch= true;
								$phaseval = $j;
								$inc1 =$inc;
							}
						}
						if ($condmatch== false) {
							$inc1 = $inc;						
							$basic = $basic + $inc;
							$phaseval = null;
						}
						$i = $basic;

						$sql = "SELECT Count(id) as countid,id FROM tblsalary_increment_slide WHERE `level`='".$levelname."' AND noofyear ='".$j."' AND LTRIM(fyear)=LTRIM('".$financialyear."')";
						$result = $this->db->query($sql)->result();
						$arrinsert  = array(
							'level' => $levelname, 
							'noofyear' => $j, 
							'basicsalary' => $basic - $slide, 
							'increment' => $inc1,
							'slide' => $slide,  
							'netsalary' => $basic,
							'fyear' => $financialyear,
							'phase' =>  $phaseval
						); 				

						if ($result[0]->countid ==0) {
							$this->db->insert('tblsalary_increment_slide', $arrinsert);
						}else{
							$this->db->WHERE('id',$result[0]->id);
							$this->db->update('tblsalary_increment_slide', $arrinsert);
						}	


						$j++;

					}



					$sql = "update msdesignation set payscale ='".$payscale."' WHERE level='".$levelname."' ";
					$res = $this->db->query($sql);	
					$this->session->set_flashdata('tr_msg', ' Data Submitted Successfully for level '.$levelname. ' !!!');
					redirect('/Payscaleimport/add');
				}
			}	

			$content['fyear'] = $this->gmodel->getcurrentfyear(); 
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}



}