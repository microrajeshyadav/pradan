<?php 
class Change_responsibility extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Change_responsibility_model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token) 
	{
		//index include
		 // start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission 
			if($this->session->userdata('login_data')->RoleID == 2){
		        $content['gotourl'] = 'Staff_approval';
		      }else{
		        $content['gotourl'] = 'Staff_personnel_approval';
		      }
			$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
		    $result  = $this->db->query($sql)->result()[0];
		    $forwardworkflowid = $result->workflowid;
			$query ="SELECT * FROM staff_transaction WHERE id =".$token;
			$content['staff_promotion'] = $this->db->query($query)->row();
			$staffid =  $content['staff_promotion']->staffid;

			$content['promotion_getchange'] = $this->Change_responsibility_model->get_staff_changeresponsibility_detail($token);
			$content['staffid'] = $content['staff_promotion']->staffid;
      		$content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($staffid);
      		/*echo "<pre>";
      		print_r($content['staffdetail']->reportingto);exit();*/
			$content['reportingto'] = $content['staffdetail']->reportingto;

			$query = "SELECT * FROM tbl_promotionchange_responsibility WHERE transid = ".$token;
			$content['promotion_detail'] = $this->db->query($query)->row();

			$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
      		$receiverdetail=$this->Staff_seperation_model->get_staffDetails($content['reportingto']);

			$RequestMethod = $this->input->server("REQUEST_METHOD");


			if($RequestMethod == 'POST')
			{

				// print_r($this->input->post()); die;
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}
				else if(trim($this->input->post('comments'))){

					$db_flag = 1;
				}

			/*echo "<pre>";
			print_r($db_flag);exit();*/
				$this->db->trans_start();
				if(trim($this->input->post('comments'))){
					// staff transaction table flag update after acceptance
					$updateArr = array(                         
		                'trans_flag'     => 5,
		                'updatedon'      => date("Y-m-d H:i:s"),
		                'updatedby'      => $this->loginData->staffid
		            );

		            $this->db->where('id',$token);
		            $this->db->update('staff_transaction', $updateArr);
		            
		            $insertworkflowArr = array(
		             'r_id'                 => $token,
		             'type'                 => 6,
		             'staffid'              => $content['staffid'],
		             'sender'               => $this->loginData->staffid,
		             'receiver'             => $content['reportingto'],
		             'forwarded_workflowid' => $forwardworkflowid,
		             'senddate'             => date("Y-m-d H:i:s"),
		             'flag'                 => 5,
		             'scomments'            => $this->input->post('comments'),
		             'createdon'            => date("Y-m-d H:i:s"),
		             'createdby'            => $this->loginData->staffid,
		            );
			        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			        $subject = "Your Process Approved";
		            $body = 'Dear,<br><br>';
		            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
		            $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
		            $body .= 'Thanks<br>';
		            $body .= 'Administrator<br>';
		            $body .= 'PRADAN<br><br>';

		            $body .= 'Disclaimer<br>';
		            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

		            
		            $to_email = $receiverdetail->emailid;
		            $to_name = $receiverdetail->name;
		            //$to_cc = $getStaffReportingtodetails->emailid;
		            $recipients = array(
		               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
		               $content['staffdetail']->emailid => $content['staffdetail']->name
		               // ..
		            );
		            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
		            if (substr($email_result, 0, 5) == "ERROR") {
		              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
		            }
		        }

				$insertArray = array(

					'staffid'      => $content['staff_promotion']->staffid,
					'transid'      => $token,
					'promotion_no' => $this->input->post('promotion_no'),
					'createdby'    => $this->loginData->staffid,
					'flag'         => $db_flag,
				);

				if($content['promotion_detail'])
				{
					$updateArray = array(

					'staffid'      => $content['staff_promotion']->staffid,
					'transid'      => $token,
					'promotion_no' => $this->input->post('promotion_no'),
					'updatedon'    => date("Y-m-d H:i:s"),
					'updatedby'    => $this->loginData->staffid,
					'flag'         => $db_flag,	
					);
					$this->db->where('id', $content['promotion_detail']->id);
					$flag = $this->db->update('tbl_promotionchange_responsibility', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_promotionchange_responsibility',$insertArray);
				}
				//echo $this->db->last_query(); die;
				$this->db->trans_complete();

				if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
				redirect(current_url());
			} 
			else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
			redirect(current_url());
		}

	}

	$content['getchange_edname'] = $this->Change_responsibility_model->getEDName();
	$content['token'] = $token;
	$content['title'] = 'Change_responsibility';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}