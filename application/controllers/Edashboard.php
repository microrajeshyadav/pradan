<?php 
class Edashboard extends CI_Controller
{
	
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model(__CLASS__ . '_model','model'); 
		//$mod = $this->router->class.'_model';
	        //$this->load->model($mod,'',TRUE);
		//$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 


		try{
			if($this->input->post('campusid') != NULL && $this->input->post('campusid') !=''){
				$campusdata = $this->input->post('campusid');
				$expcampusdata = explode('-',$campusdata);
				$campusid = $expcampusdata[0];
				$campusintimationid = $expcampusdata[1];
			//print_r($expcampusdata); die;
				

			}else {
				$campusdata = NULL;
				$campusid = 'NULL';
				$campusintimationid = 'NULL';
			}


			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				/*echo "<pre>";
					print_r($_POST);
					die;*/
				$saveoffer = $this->input->post('send');
				if (isset($saveoffer) && $saveoffer =='send' && $savehrscore !='') {
					/*echo "<pre>";
					print_r($_POST);
					die;*/

					$getHRDDetails = $this->model->getRoleHRDDetails();
		//	print_r($getHRDDetails); die;

		 $hrdemail = $getHRDDetails->EmailID;  //// HRD Email Id Role ID 4 //////

		 $edemail = $this->loginData->EmailID;
		 
		 //$html = 'Dear Sir, <br><br> Greetings from Bapurao Deshmukh College of Engineering, Sevagram.!!!!!!!!!!!!!!!!! We are planning to start recruitment drives for XXXXXX Batch in the month of XXXXXXXXX. It is our great privilege to invite you to visit Bapurao Deshmukh College of Engineering, Sevagram, one of the oldest Colleges in Vidarbha region for Campus Placement Recruitment Drives. We are ready to coordinate Closed/Pool drive & open to invite nearby Engineering Colleges affiliated University & Amravati University as per company requirements. So we are requesting you to finalize an early date to pick up the best candidates from our region.';

				//$sendmail = $this->Common_Model->send_email($subject = 'Approve Offer Letter', $message = $html, $hrdemail);  //// Send Mail HRD ////

				
				// if (!$sendmail == True) {
				// 	echo "Not send";
				// 	$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
				// }else{
				// 	$this->session->set_flashdata('tr_msg','Successfully Send to HRD !!!!');					
				// }

			}
		}

		$content['selectedcandidatedetails']      = $this->model->getSelectedCandidate($campusid,$campusintimationid);
		$content['campusdetails'] 	              = $this->model->getCampus();
		$content['recruiters_list']               = $this->model->getRecruitersList();
		$content['campusalldetails'] 			  = $this->model->getAllCampus();
		//$content['comments']                    = $this->model->getComments();
		$content['appointmentApprovallist'] = $this->model->getSelectedCandidateForAppointment();
		/*echo "<pre>";
		print_r($content['selectedcandidatedetails']);exit();*/

		$content['title'] = 'Edashboard';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}




public function Offercomments($token,$roleid){
	
	try{
		

		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    	// end permission 			
		 $candidateqry = "SELECT * FROM tbl_candidate_registration  WHERE 	isdeleted =0 and candidateid = ".$token."";
		$content['candidate'] = $this->db->query($candidateqry)->row();
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

			// print_r($_POST); die();
			$getHRDDetails = $this->model->getRoleHRDDetails();
			//echo "<pre>";
			//print_r($getHRDDetails);
		//	die;

			$this->db->trans_start();

	            //  $status = $this->input->post('status'); 
	            // 	if ($status == 0) {
	            // 		$flag = 2; 
	            // 	}else{
	            // 		$flag = 2;
	            // 	}

	            // die;

			$this->form_validation->set_rules('status','Status','trim|required');
			// $this->form_validation->set_rules('comments','Comment','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
					'</div>');

				$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors    =    true;
				goto prepareview;

			}

			// ed signature
			$candidate = "";
			$getedname = "";
			$edsign = "";
			$body = "";

			$candidate = $this->gmodel->getCandidateDetails($token);
			$getedname = $this->gmodel->getExecutiveDirectorEmailid();

			if ($getedname->sign !='') {
				$edsign = site_url('datafiles/signature/'.$getedname->sign);
			}else{ 
				$edsign = site_url('datafiles/signature/Signature.png');
			}

			$body = $candidate->generate_offerletter;

			$cand = array('$edsign');
			$cand_replace = array($edsign);

			if(!empty($body))
			$body = str_replace($cand,$cand_replace,$body);
			$body = str_replace('<img src="','<img src="'.$edsign, $body); 

			$body=  html_entity_decode($body);

			$filename = "";
			$filename = md5(time() . rand(1,1000));
			$this->load->model('Dompdf_model');
        	$generate =   $this->Dompdf_model->generatePDFed($body, $filename, NULL,'Generateofferletter.pdf');

        	$insert = array('filename'  => $filename);

        	$this->db->where('candidateid', $token);
        	$this->db->update('tbl_generate_offer_letter_details', $insert);


			$insertArr2 = array(
				'candidateid'       => $token,
				'status'            => $this->input->post('status'),
				'comments'			=> $this->input->post('comments'),
				'createdon'      	=> date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'      	=> $this->loginData->UserID, // login user id
			);

			// print_r($insertArr2); 
			// die;
			$this->db->insert('edcomments', $insertArr2);
			$insertedid = $this->db->insert_id();

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding ED Comments');	
			}else{

				$getHRDDetails = $this->model->getRoleHRDDetails();
				
			$hrdemail = $getHRDDetails->emailid;  //// HRD Email Id Role ID 4 //////
			$hrdname=$getHRDDetails->name;
			$PAemailid='';
			$pa='';
			$pa = $this->gmodel->getPersonnelAdministratorEmailid();
			$PAemailid = $pa->personnelemailid; 
			$edemail = $this->loginData->EmailID;
			$edname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
			
			// $html = 'Dear Sir, <br><br> 
			// Comments : '.$this->input->post('comments');

					$message='';
					 $subject = "after approval of offer letter from ED end";

					 // $fd = fopen("mailtext/edapproved.txt","r");	
						// $message .=fread($fd,4096);
						// eval ("\$message = \"$message\";");
						// $message =nl2br($message);

			$candidate = array('$edname','$hrdname');
			$candidate_replace = array($edname,$hrdname);

			if($this->input->post('status') ==0) //approved 
			{
				if ($roleid == 17)
				{
	        	$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 43 AND `isactive` = '1'";
	            $data = $this->db->query($sql)->row();
	            if(!empty($data))
	            $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
							
				$sendmail = $this->Common_Model->send_email($subject,$body,$PAemailid);  //// Send Mail HR
			    }

			    if ($roleid == 16)
				{
	        	$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 43 AND `isactive` = '1'";
	            $data = $this->db->query($sql)->row();
	            if(!empty($data))
	            $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
							
				$sendmail = $this->Common_Model->send_email($subject,$body,$hrdemail);  //// Send Mail HR
			    }
			}

			if($this->input->post('status') ==1) //rejected
			{
				if ($roleid == 17)
				{
				   $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 60 AND `isactive` = '1'";
		           $data = $this->db->query($sql)->row();
		           if(!empty($data))
		           $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
							
				$sendmail = $this->Common_Model->send_email($subject,$body,$PAemailid);  //// Send Mail personnel 
				}

				if ($roleid == 16)
				{
		        	$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 60 AND `isactive` = '1'";
		            $data = $this->db->query($sql)->row();
		            if(!empty($data))
		            $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
							
				$sendmail = $this->Common_Model->send_email($subject,$body,$hrdemail);  //// Send Mail HR 
			    }
			}
				 
				// echo "send=".$sendmail;
				// die;
				
				if (!$sendmail == True) {
					echo "Not send";
					if ($roleid == 17)
					{
					$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to Personnel. Please Try Again !!!!');
					}
					if ($roleid == 16)
					{
					$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!');
					} 
				}else{

					$insertArr2 = array(
						'flag'	=> 2,
						'send_hrd_mail_status' => 1,
						
					);
					
					$this->db->where('id',$insertedid);		
					$this->db->update('edcomments', $insertArr2);

					if ($roleid == 17)
					{
					$this->session->set_flashdata('tr_msg','Successfully Send to Personnel !!!!');	
					}

					if ($roleid == 16)
					{
					$this->session->set_flashdata('tr_msg','Successfully Send to HR !!!!');	
					}				
				}
				$this->session->set_flashdata('er_msg', 'Rejected by Ed Successfully added ED Comments');			
			}
			redirect('/Edashboard/index');
			
		}
		
		prepareview:
		$content['getpdf'] = $this->model->getOfferletterPDF($token);
		$content['title'] = 'Offercomments';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}
public function Appointmentcomments($token){
	try{
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    	// end permission 			
		 $candidateqry = "SELECT * FROM tbl_candidate_registration  WHERE 	isdeleted =0 and candidateid = ".$token."";
		$content['candidate'] = $this->db->query($candidateqry)->row();
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

			//print_r($_POST);
			// $getHRDDetails = $this->model->getRoleHRDDetails();
			//echo "<pre>";
			//print_r($getHRDDetails);
		//	die;

			$this->db->trans_start();

	            //  $status = $this->input->post('status'); 
	            // 	if ($status == 0) {
	            // 		$flag = 2; 
	            // 	}else{
	            // 		$flag = 2;
	            // 	}

	            // die;

			$this->form_validation->set_rules('personnel','Personnel','trim|required');
			$this->form_validation->set_rules('status','Status','trim|required');
			// $this->form_validation->set_rules('comments','Comment','trim|required');
			
			if($this->form_validation->run() == FALSE){
				$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
					'</div>');

				$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors    =    true;
				goto prepareview;

			}

			$personnelId = $this->input->post('personnel');
			$personneldetail = $this->Common_Model->get_Staff_detail($personnelId);
			/*echo "<pre>";
			print_r($personneldetail);exit();*/

			$updateArr2 = array(
				'personnelId'            => $personnelId,
				'edstatus'            => $this->input->post('status'),
				'edcomments'			=> $this->input->post('comments'),
			);

			// print_r($insertArr2); 
			// die;
			$this->db->where('transid', $token);
			$this->db->update('tbl_offer_of_appointment', $updateArr2);
			// $insertedid = $this->db->insert_id();

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding ED Comments');	
			}else{

				// $getHRDDetails = $this->model->getRoleHRDDetails();
				
			/*$hrdemail = $getHRDDetails->emailid;  //// HRD Email Id Role ID 4 //////
			$hrdname=$getHRDDetails->name;*/

			$edemail = $this->loginData->EmailID;
			$edname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
			$personnelEmail = $personneldetail->emailid;
			$personnelName = $personneldetail->name;
			// $html = 'Dear Sir, <br><br> 
			// Comments : '.$this->input->post('comments');

			$message='';
			$subject = "after approval of offer letter from ED’s end";

			/*$fd = fopen("mailtext/edapproved.txt","r");	
			$message .=fread($fd,4096);
			eval ("\$message = \"$message\";");
			$message =nl2br($message);*/
			$message .= 'Dear '.$personnelName.'<br><br>';
			$message .= 'I have approved the Executive offer. Please check.<br><br>';
			$message .= 'Warm regards, <br>'.$edname;


			$sendmail = $this->Common_Model->send_email($subject,$message,$hrdemail);  //// Send Mail Personnel /
			// echo "send=".$sendmail;
			// die;
				
			if (!$sendmail == True) {
				echo "Not send";
				$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
			}else{

				/*$insertArr2 = array(
					'flag'	=> 2,
					'send_hrd_mail_status' => 1,
					
				);
				
				$this->db->where('id',$insertedid);		
				$this->db->update('edcomments', $insertArr2);*/

				$this->session->set_flashdata('tr_msg','Successfully Send to HRD !!!!');					
			}
			$this->session->set_flashdata('er_msg', 'Rejected by Ed Successfully added ED Comments');			
		}
		redirect('/Edashboard/index');
		
	}
		
	prepareview:
	$content['getpdf'] = $this->model->getAppointmentletterPDF($token);
	$content['personnellist'] = $this->Common_Model->get_Personnal_Staff_List();
	/*echo "<pre>";
	print_r($content['personnellist']);exit();*/
	$content['title'] = 'Appointmentcomments';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}