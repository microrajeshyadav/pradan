<?php 
class Generatesalary extends CI_Controller
{

	// echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Generatesalary_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model('Payrollreport_model');

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		try{
		// start permission 
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission  
			$content['fyear'] = $this->gmodel->getcurrentfyear(); 
		// data for input fieldname and amount 

			$officeid = $this->input->post('transfer_office');
			$financialyear = $this->input->post('financialyear');
			$level = $this->input->post('levelname');
			$noofyear = $this->input->post('Experience');



			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){
				// echo "<pre>";
				// print_r($_POST); 

				$financialyear = '';
				$salmonth = $this->input->post('salmonth');
				$salmonth = $this->input->post('salmonth');
				$cea = $this->input->post('amt_1');		
				$financialyear = $this->input->post('financialyear');
				if (!empty($salmonth) && !empty($financialyear)) {
					$fyear='';
			if($salmonth >=04 && $salmonth<=12) // previous year
			{
				$fyear  = explode('-', $financialyear); 
				$salyear = $fyear[0];
			}
			elseif($salmonth >=01 && $salmonth<=03) // next year
			{   
				$fyear  = explode('-', $financialyear);
				$salyear = $fyear[1];
			}
			else{
				$salyear = date('Y');
			}
		}
		else{
			$this->session->set_flashdata('er_msg', 'Select Financial year');
			redirect('Generatesalary');
		}
		$period = $salyear.$salmonth;

		$input = $this->input->post('input'); // array of fieldname 
		$at = $this->input->post('at'); // array of amount
		// echo $input[0];
		// die();
		// foreach ($input as $key => $value) {
		// 	echo $value;
		// }
		$datastopreason = array();
		$this->db->trans_start();
		$basicsql = "select netsalary as e_basic from tblsalary_increment_slide WHERE level ='".$level."' and noofyear = '".$noofyear."' and fyear = '".$financialyear."'" ;

		$basicresult = $this->db->query($basicsql)->row();

		if (count($basicresult) > 0) {
			$staffbasicsalary = $basicresult->e_basic;

			$salaryheadsql = "select sh.fieldname,sh.fielddesc,sh.formulacolumn,sh.c as fixedval,sh.dw as slab,sh.roundtohigher,sh.monthlyinput,sh.mT,sh.ct as fixedammount, CASE WHEN sh.officeid IS NULL THEN lookuphead ELSE NULL END as formula, lh.lowerrange, lh.upperrange from (SELECT `COLUMN_NAME`,`COLUMN_COMMENT` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='pradan' AND `TABLE_NAME`='tblmstemployeesalary' AND (`COLUMN_NAME` LIKE 'e_%' OR `COLUMN_NAME` LIKE 'd_%' ) AND `COLUMN_NAME` !='deletedemployee' ) as a LEFT join pradan.salaryheads as sh on sh.fieldname = a.`COLUMN_NAME` LEFT join pradan.tblheadlookup as lh on sh.fieldname = lh.fieldname and sh.officeid = lh.officeid WHERE sh.activefield = 1 and sh.officeid is null and sh.financial_year = '".$financialyear."' GROUP by sh.fieldname,sh.fielddesc,sh.formulacolumn,sh.c,sh.dw,sh.roundtohigher,sh.monthlyinput,sh.mT,sh.ct ORDER BY cast(a.COLUMN_COMMENT AS UNSIGNED)";

			$salaryheadresult = $this->db->query($salaryheadsql)->result();
			 // print_r($salaryheadsql); die();

				$finalmonthlysalaryarray = array(
					'officeid' => $this->input->post('transfer_office'),
					'seqno'                => 1, 
					'dateofgeneratesalary' => date('Y-m-d'),
					'officeid'        => $officeid,
					'designationcode' => $level, 
					'salmonth'        => $salmonth, 
					'salyear'         => $salyear, 
					'recordtype'      => $noofyear,
					'staffid'         => 1, 
					'e_basic'         => 0,
					'e_01'            => 0, 
					'e_02'            => 0, 
					'e_03'            => 0, 
					'e_04'            => 0, 
					'e_05'            => 0, 
					'e_06'            => 0, 
					'e_07'            => 0, 
					'e_08'            => 0, 
					'e_09'            => 0, 
					'e_10'            => 0, 
					'e_11'            => 0, 
					'e_12'            => 0, 
					'e_13'            => 0, 
					'e_14'            => 0, 
					'e_15'   		  => 0, 
					'd_pf'            => 0, 
					'd_vpf'           => 0, 
					'd_01'            => 0, 
					'd_02'            => 0, 
					'd_03'            => 0, 
					'd_04'            => 0, 
					'd_05'            => 0, 
					'd_06'            => 0, 
					'd_07'            => 0, 
					'd_08'            => 0, 
					'd_09'            => 0, 
					'd_10'            => 0, 
					'd_11'            => 0, 
					'd_12'            => 0, 
					'd_13'            => 0, 
					'd_14'            => 0, 
					'd_15'            => 0,
					'salarylock'      => 0,
					'c_totearn'       => 0, 
					'c_netsal'        => 0, 
					'c_grosssalary'   => 0, 
					'reason'          => '');


			foreach ($salaryheadresult as $key => $val) {

				if ($val->fieldname == 'e_basic') {

					foreach ($finalmonthlysalaryarray as $key => $values) {
						if ($key==$val->fieldname) {
							$finalmonthlysalaryarray[$key] = $staffbasicsalary;
						}
					}


				}elseif ($val->formulacolumn == 1 && $val->mT == 0) {

					$formulamTfalse = $val->formula;
					$expdata = explode('%', $formulamTfalse);
					$percentage = $expdata[0];

					$headvalue = ($staffbasicsalary * $percentage / 100);
					if ($val->upperrange > 0 and $headvalue > $val->upperrange)
					{
						$headvalue = $val->upperrange;
					}

					foreach ($finalmonthlysalaryarray as $key => $values) {
						if ($key==$val->fieldname) {
								//echo $key;
							$finalmonthlysalaryarray[$key] = $headvalue;
						}
					}


				}elseif ($val->formulacolumn == 1 && $val->mT == 1) {

					$formulasql = "select a.lookuphead, lh.lowerrange, lh.upperrange from salaryheads as a LEFT join tblheadlookup as lh on  a.fieldname = lh.fieldname and a.officeid = lh.officeid
					where a.financial_year = '".$financialyear."' and a.fieldname ='".$val->fieldname."' AND a.officeid = ".$officeid; 
					$formularesult = $this->db->query($formulasql)->row();
					if (count($formularesult)> 0) {
						$formulamTtrue = $formularesult->lookuphead;
						$expdata = explode('%', $formulamTtrue);
						$percentage = $expdata[0];
							//$percentage = substr($formulamTtrue, 0, 2);
						$headvalue = ($staffbasicsalary * $percentage / 100);
						if ($val->upperrange > 0 and $headvalue > $val->upperrange)
						{
							$headvalue = $val->upperrange;
						}
						if ($val->roundtohigher ==1) {
							foreach ($finalmonthlysalaryarray as $key => $values) {
								if ($key==$val->fieldname) {
									$finalmonthlysalaryarray[$key] =round($headvalue);
								}
							}
						}else{
							foreach ($finalmonthlysalaryarray as $key => $values) {
								if ($key==$val->fieldname) {
									$finalmonthlysalaryarray[$key] =$headvalue;
								}
							}

						}

					}else{
						$formulamTfalse = $val->formula;

						$expdata = explode('%', $formulamTfalse);
						$percentage = $expdata[0];

						$headvalue = ($staffbasicsalary * $percentage / 100);
						if ($val->roundtohigher ==1) {
							foreach ($finalmonthlysalaryarray as $key => $values) {
								if ($key==$val->fieldname) {
									$finalmonthlysalaryarray[$key] =round($headvalue);
								}
							}
						}else{
							foreach ($finalmonthlysalaryarray as $key => $values) {
								if ($key==$val->fieldname) {
									$finalmonthlysalaryarray[$key] =$headvalue;
								}
							}
						}
					}

				}
				elseif ($val->fixedval == 1 && $val->mT == 0) {
					$headvalue = $val->fixedammount;
					foreach ($finalmonthlysalaryarray as $key => $values) {
						if ($key==$val->fieldname) {
							$finalmonthlysalaryarray[$key] =$headvalue;
						}
					}

				}
				elseif ($val->fixedval == 1 && $val->mT == 1) {

					$formulasql = "select a.ct as fixedamount from salaryheads as a where financial_year = '".$financialyear."' a.fieldname ='".$val->fieldname."' AND a.officeid = ".$value->new_office_id; 
					$formularesult = $this->db->query($formulasql)->row();
					if (count($formularesult)> 0) {
						$headvalue = $formularesult->fixedamount;
						foreach ($finalmonthlysalaryarray as $key => $values) {
							if ($key==$val->fieldname) {
								$finalmonthlysalaryarray[$key] =$headvalue;
							}
						}
					}
					else{
						$headvalue = $val->fixedammount;
						foreach ($finalmonthlysalaryarray as $key => $values) {
							if ($key==$val->fieldname) {
								$finalmonthlysalaryarray[$key] =$headvalue;
							}
						}
					}

				}
				elseif ($val->monthlyinput == 1) {
					
					foreach ($input as $inputkey => $value1) {
						
						$inputamount=0;
						if ($val->fieldname==$value1) {
							foreach ($at as $amountkey => $value11) {
								$inputamount = $value11;
								if ($inputkey == $amountkey)
								{
									$inputamount = str_replace(',','',$inputamount);
									// echo $inputamount; 
									foreach ($finalmonthlysalaryarray as $key => $values) {
								if ($key==$value1) {
									$finalmonthlysalaryarray[$key] = $inputamount;
								}
							}

								}
								
							}

						}

					}


				}

				elseif ($val->slab == 1) {

					$formulasql = "select amount from payrollslab WHERE financial_year = '".$financialyear."' AND fieldname ='".$val->fieldname."' AND ".$staffbasicsalary." between lowerlimit and upperlimit "; 
					$formularesult = $this->db->query($formulasql)->row();
					if (count($formularesult)> 0) {
						$headvalue = $formularesult->amount;
						foreach ($finalmonthlysalaryarray as $key => $values) {
							if ($key==$val->fieldname) {
								$finalmonthlysalaryarray[$key] = $headvalue;
							}
						}

					}
				}
				$staffCTC= 0;
// echo $finalmonthlysalaryarray['e_04'];
				$staffCTC =  $finalmonthlysalaryarray['e_basic']+ $finalmonthlysalaryarray['e_01'] +
				$finalmonthlysalaryarray['e_02'] + $finalmonthlysalaryarray['e_03'] + $finalmonthlysalaryarray['e_04']  + $finalmonthlysalaryarray['e_05']  + $finalmonthlysalaryarray['e_06']  + $finalmonthlysalaryarray['e_07'] + $finalmonthlysalaryarray['e_08'];

				foreach ($finalmonthlysalaryarray as $key => $values) {
					if ($key==$val->fieldname) {
						$finalmonthlysalaryarray['c_grosssalary'] = $staffCTC;
					}
				}


				$gross_salary = $staffCTC - ($finalmonthlysalaryarray['e_06'] + $finalmonthlysalaryarray['e_07'] + $finalmonthlysalaryarray['e_08']);

				foreach ($finalmonthlysalaryarray as $key => $values) {
					if ($key==$val->fieldname) {
						$finalmonthlysalaryarray['c_netsal'] = $gross_salary;
					}
				}


				$takehomesalary = $gross_salary - $finalmonthlysalaryarray['d_pf'];

				foreach ($finalmonthlysalaryarray as $key => $values) {
					if ($key==$val->fieldname) {
						$finalmonthlysalaryarray['c_totearn'] = $takehomesalary;
					}
				}


				}
				 ////end foreach loop
				$this->db->where('officeid', $officeid);
				$this->db->where('salmonth' ,$salmonth);
				$this->db->where('salyear' ,$salyear);
				$this->db->delete('tblfinalmonthlysalary');
				
				$this->db->insert('tblfinalmonthlysalary', $finalmonthlysalaryarray);
				// echo $this->db->last_query(); die();
				$this->session->set_flashdata('tr_msg', 'CTC calculation done successfully.');

			} ////end if
			

			///////////// Save record salaryhead history ////////////////



			$sql = "SELECT count(id) as count  FROM `salaryheadshistory` WHERE `period` ='".$period."'";
			$salaryhistoryresult = $this->db->query($sql)->row();

			if ($salaryhistoryresult->count > 0) {
				$this->db->delete('salaryheadshistory', array('period'=> $period));
    		 	//echo $this->db->last_query(); die;
			}
    		// echo $this->db->last_query(); die;

			$sqlhistory = "INSERT INTO salaryheadshistory(`fieldname`,`fielddesc`,`abbreviation`,`formulacolumn`,`lookuphead`,`roundingupto`,
			`attendancedep`,`roundtohigher`,`monthlyinput`,`seqno`,`lookupheadname`,`activefield`,`specialfield`,
			`specialfieldmaster`,`carryforward`,`officeid`,`conditional`,`mt`,`c`,`dw`,`a`,`ct`,`dc`,`cw`,`period`)
			SELECT `fieldname`,`fielddesc`,`abbreviation`,`formulacolumn`,`lookuphead`,`roundingupto`,`attendancedep`,
			`roundtohigher`,`monthlyinput`,`seqno`,`lookupheadname`,`activefield`,`specialfield`,`specialfieldmaster`,
			`carryforward`,`officeid`,`conditional`,`mt`,`c`,`dw`,`a`,`ct`,`dc`,`cw`, '".$period ."' FROM `salaryheads`";

			$historyresult = $this->db->query($sqlhistory);


			   ///////////// Save record lookuphead history ////////////////

			    ///////////// Save start record lookuphistory history ////////////////


			$sql = "SELECT count(id) as count  FROM `tblheadlookuphistory` WHERE `period` ='".$period."'";
			$lookuphistoryresult = $this->db->query($sql)->row();

			if ($lookuphistoryresult->count > 0) {
				$this->db->delete('tblheadlookuphistory', array('period'=> $period));
    		 	//echo $this->db->last_query(); die;
			}
    		// echo $this->db->last_query(); die;

			$sqlhistory = "INSERT INTO tblheadlookuphistory(`fieldname`,`lowerrange`,`upperrange`,`headvalue`,`formulafield`,`seqno`,
			`headvaluename`,`maxlimit`,`period`)
			SELECT `fieldname`,`lowerrange`,`upperrange`,`headvalue`,`formulafield`,`seqno`,
			`headvaluename`,`maxlimit`,$period FROM `tblheadlookup`";
			$historyresult = $this->db->query($sqlhistory);

              ///////////// Save End record lookuphistory history ////////////////          

			$this->db->trans_complete();

	if ($this->db->trans_status() === FALSE){
		$this->session->set_flashdata('er_msg', 'Error Increment has not been done');

	}else{
		$this->session->set_flashdata('tr_msg', 'Increment has been done successfully');

	}

		// 	$content['getactivehead'] = $this->Payrollreport_model->getActivesalaryheads($period);
		// $content['staffsalary'] = $this->Payrollreport_model->getstaffsalary($period,$salmonth,$salyear);

		}////end  post
		// new code add by bks


		// end permission    



		$content['getactivehead'] = NULL;
		$content['staffsalary'] = NULL;
		$content['all_office'] = $this->Payrollreport_model->get_alloffice();
		
				
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			$officeid = $this->input->post('transfer_office');
			$salmonth = $this->input->post('salmonth');
			$salyear = $this->input->post('salyear');		
			$financialyear = $this->input->post('financialyear');
			if (!empty($salmonth)) {
				$fyear='';
			if($salmonth >=04 && $salmonth<=12) // previous year
			{
				$fyear  = explode('-', $financialyear); 
				$salyear = $fyear[0];
			}
			elseif($salmonth >=01 && $salmonth<=03) // next year
			{   
				$fyear  = explode('-', $financialyear);
				$salyear = $fyear[1];
			}
			else{
				$salyear = date('Y');
			}
		}
		else{
			redirect('Generatesalary');
		}
		$period = $salyear.$salmonth;

		$content['getactivehead'] = $this->Payrollreport_model->getActivesalaryheads($period);
		$content['staffsalary'] = $this->Payrollreport_model->getstaffsalary($period,$salmonth,$salyear,$officeid);

	}

		// print_r($content['staffsalary']); 
//end new ode by bks
	// $content['stafflist'] = $this->Generatesalary_model->getStaffList($officeid,$salmonth,$salyear);
	$content['all_office'] = $this->Generatesalary_model->get_alloffice();
	$content['title'] = 'Generate CTC';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);


}catch (Exception $e) {
	print_r($e->getMessage());die;
}

} /// end index

}