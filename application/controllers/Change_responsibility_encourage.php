<?php 
class Change_responsibility_encourage extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Change_responsibility_encourage_model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token) 
	{
		//index include
		 // start permission 
		try{
			  if (empty($token) || $token == '' ) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
        redirect('/Staff_personnel_approval/index');
        
      } else {
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
				// end permission 
				$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
			    $result  = $this->db->query($sql)->result()[0];
			   
			    $forwardworkflowid = $result->workflowid;
				$query ="SELECT * FROM staff_transaction WHERE id =".$token;
				$content['staff_promotion'] = $this->db->query($query)->row();
				
				$staffid =  $content['staff_promotion']->staffid;
				

				$content['promotion_getchange'] = $this->Change_responsibility_encourage_model->get_staff_changeresponsibility_detail($token);
				
				$content['staffid'] = $content['staff_promotion']->staffid;;
				$content['reportingto'] = $content['staff_promotion']->reportingto;

				$query = "SELECT * FROM tbl_promotionchange_responsibility WHERE transid = ".$token;
				$content['promotion_detail'] = $this->db->query($query)->row();
				



				$RequestMethod = $this->input->server("REQUEST_METHOD");


				if($RequestMethod == 'POST')
				{

					
					$db_flag = '';
					if($this->input->post('Save') == 'Save'){
						$db_flag = 0;
					}else if(trim($this->input->post('comments'))){

                        $db_flag = 1;
                    }

				
					$this->db->trans_start();
					if(trim($this->input->post('comments'))){
						// staff transaction table flag update after acceptance
						$updateArr = array(                         
			                'trans_flag'     => 5,
			                'updatedon'      => date("Y-m-d H:i:s"),
			                'updatedby'      => $this->loginData->staffid
			            );

			            $this->db->where('id',$token);
			            $this->db->update('staff_transaction', $updateArr);
			            
			            $insertworkflowArr = array(
			             'r_id'                 => $token,
			             'type'                 => 4,
			             'staffid'              => $content['staffid'],
			             'sender'               => $this->loginData->staffid,
			             'receiver'             => $staffid,
			             'forwarded_workflowid' => $forwardworkflowid,
			             'senddate'             => date("Y-m-d H:i:s"),
			             'flag'                 => 5,
			             'scomments'            => $this->input->post('comments'),
			             'createdon'            => date("Y-m-d H:i:s"),
			             'createdby'            => $this->loginData->staffid,
			            );
				        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			        }

					$insertArray = array(

						'staffid'      => $content['staff_promotion']->staffid,
						'transid'      => $token,
						'promotion_no' => $this->input->post('promotion_no'),
						'createdby'    => $this->loginData->staffid,
						'flag'         => $db_flag,
					);

					if($content['promotion_detail'])
					{
						$updateArray = array(

						'staffid'      => $content['staff_promotion']->staffid,
						'transid'      => $token,
						'promotion_no' => $this->input->post('promotion_no'),
						'updatedon'    => date("Y-m-d H:i:s"),
						'updatedby'    => $this->loginData->staffid,
						'flag'         => $db_flag,	
						);
						$this->db->where('id', $content['promotion_detail']->id);
						$flag = $this->db->update('tbl_promotionchange_responsibility', $updateArray);
					}else{
						$flag = $this->db->insert('tbl_promotionchange_responsibility',$insertArray);
					}
					$this->db->trans_complete();

					if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
					$subject = ': Inter Office Memo Form';
	                $body = '<h4>'.$content['promotion_getchange']->name.' submited  Inter office memo form </h4><br />';
	                $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
	                <tr>
	                <td width="96">Name </td>
	                <td width="404">'.$content['promotion_getchange']->name.'</td>
	                </tr>
	                <tr>
	                <td>Employ Code</td>
	                <td> '.$content['promotion_getchange']->emp_code.'</td>
	                </tr>
	                <tr>
	                <td>Designation</td>
	                <td>' .$content['promotion_getchange']->changedesig.'</td>
	                </tr>
	                <tr>
	                <td>Office</td>
	                <td>'.$content['promotion_getchange']->newoffice.'</td>
	                </tr>
	                </table>';
	                $body .= "<br /> <br />";
	                $body .= "Regards <br />";
	                $body .= " ". $content['promotion_getchange']->name ."<br>";
	                $body .= " ". $content['promotion_getchange']->changedesig."<br>";
	                $body .= "<b> Thanks </b><br>";



	       // $to_useremail = 'pdhamija2012@gmail.com';
	                $to_useremail = $this->loginData->emailid;
	                // $tcemailid = $content['tc_email']->emailid;
	        // $personal_email=$content['personnal_mail']->EmailID;
	        // echo $personal_email;
	        // die;

	       // $to_hremail = $hremail->EmailID;
	                // $arr= array (
	                //     $tcemailid      =>'tc',
	                // );

	                $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name);
					redirect(current_url());
				} 
				else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				redirect(current_url());
			}

		}

		$content['getchange_edname'] = $this->Change_responsibility_encourage_model->getEDName();
		$content['token'] = $token;
		$content['title'] = 'Change_responsibility_encourage';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}