<?php 
class Staff_sepreleasefromservice extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepreleasefromservice_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("Staff_approval_model");


		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			  if($token){
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
			// end permission    
				$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
			    $result  = $this->db->query($sql)->result()[0];
			    $forwardworkflowid = $result->workflowid;

				$content['getexecutivedirector'] = $this->Staff_approval_model->getExecutiveDirectorList(18);
				$query ="SELECT * FROM staff_transaction WHERE id=".$token;
				$content['staff_sepreleasefrom_service'] = $this->db->query($query)->row();

				$staffid =  $content['staff_sepreleasefrom_service']->staffid;

				$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
				// print_r($content['staff_detail']); die();
				$query = "SELECT * FROM tbl_sep_releaseform WHERE transid = ".$token;
				$content['release_detail'] = $this->db->query($query)->row();
				// print_r($content['release_detail']); die();
					$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 85 AND `isactive` = '1'";
					$data = $this->db->query($sql)->row();

				$tdate = date('d/m/Y');
				$staffname = $content['staff_detail']->name;
				$staffaddress = $content['staff_detail']->address;

				if(!empty($content['release_detail']))
				{
					$pdr_no = $content['release_detail']->pdr_no;
				$letter_no = $content['release_detail']->letter_no;
				$letter_date = $this->gmodel->changedatedbformate($content['release_detail']->letter_date);
				$wef_date = $this->gmodel->changedatedbformate($content['release_detail']->wef_date);
				$relieved_date = $this->gmodel->changedatedbformate($content['release_detail']->relieved_date);

					$staff1 = array();
					$staff1_replace = array();

					$staff1 = array('$pdr_no','$tdate','$staffname','$staffaddress','$letter_no','$letter_date','$wef_date','$relieved_date');
					$staff1_replace = array($pdr_no,$tdate,$staffname,$staffaddress,$letter_no,$letter_date,$wef_date,$relieved_date);
					if(!empty($data))
					$body = str_replace($staff1,$staff1_replace , $data->lettercontent); 
				// echo $body; die();
				}
				else{
					$staff1 = array();
					$staff1_replace = array();

					$staff1 = array('$pdr_no','$tdate','$staffname','$staffaddress','$letter_no','$letter_date','$wef_date','$relieved_date');
					$staff1_replace = array('',$tdate,$staffname,$staffaddress,'','','','');
					if(!empty($data))
					$body = str_replace($staff1,$staff1_replace , $data->lettercontent);
				}
				

					if(!empty($body))
					{
						$content['content'] = $body;
					}

					// $body = str_replace('<img src="','<img src="'.$edsign, $body); 

					// $body=  html_entity_decode($body);

					$content['staffid'] = $content['staff_sepreleasefrom_service']->staffid;

					$query = "SELECT * FROM tbl_sep_releaseform WHERE transid = ".$token;
					$content['release_detail'] = $this->db->query($query)->row();

					$RequestMethod = $this->input->server("REQUEST_METHOD");


					if($RequestMethod == 'POST')
					{
					// print_r($this->input->post()); die();
						$db_flag = '';
						if($this->input->post('Save') == 'Save'){
							$db_flag = 0;
						}else if(trim($this->input->post('comments'))){
							$db_flag = 1;
						}

					$body = '';
					$body = $this->input->post('lettercontent');
					$filename = "";
					$filename = md5(time() . rand(1,1000));
					$this->load->model('Dompdf_model');
					$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'RelievingLetter.pdf');


					/*echo "<pre>";
					print_r($forwardworkflowid);exit();*/
					if(trim($this->input->post('comments'))){
						// staff transaction table flag update after acceptance
						$updateArr = array(                         
							'trans_flag'     => 28,
							'updatedon'      => date("Y-m-d H:i:s"),
							'updatedby'      => $this->loginData->staffid
						);

						$this->db->where('id',$token);
						$this->db->update('staff_transaction', $updateArr);

						$insertworkflowArr = array(
							'r_id'                 => $token,
							'type'                 => 3,
							'staffid'              => $content['staffid'],
							'sender'               => $this->loginData->staffid,
							'receiver'             => $content['getexecutivedirector'][0]->edstaffid,
							'forwarded_workflowid' => $forwardworkflowid,
							'senddate'             => date("Y-m-d H:i:s"),
							'flag'                 => 28,
							'scomments'            => $this->input->post('comments'),
							'createdon'            => date("Y-m-d H:i:s"),
							'createdby'            => $this->loginData->staffid,
						);
						$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
					}

					$insertArray = array(

						'staffid'        => $content['staff_sepreleasefrom_service']->staffid,
						'transid'        => $token,
						'pdr_no'         => $this->input->post('pdr_no'),
						'letter_no'      => $this->input->post('letter_no'),
						'letter_date'    => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('letter_date')),
						'wef_date'       => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('wef_date')),
						'relieved_date ' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('relieved_date')),
						'flag'           => $db_flag,
						'createdby'      => $this->loginData->staffid,
						'updatedon'		 => date('Y-m-d H:i:s'),
        				'createdon'		 => date('Y-m-d H:i:s')
					);

					if($content['release_detail'])
					{
						$updateArray = array(

							'staffid'        => $content['staff_sepreleasefrom_service']->staffid,
							'transid'        => $token,
							'pdr_no'         => $this->input->post('pdr_no'),
							'letter_no'      => $this->input->post('letter_no'),
							'letter_date'    => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('letter_date')),
							'wef_date'       => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('wef_date')),
							'relieved_date ' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('relieved_date')),
							'filename'		 => $filename,
							'content'		 => $body,
							'flag'           => $db_flag,
							'updatedby'      => $this->loginData->staffid,	
							'updatedon'		 => date('Y-m-d H:i:s'),
        					'createdon'		 => date('Y-m-d H:i:s')
						);

						$this->db->where('id', $content['release_detail']->id);
						$flag = $this->db->update('tbl_sep_releaseform', $updateArray);
					}else{
						$flag = $this->db->insert('tbl_sep_releaseform',$insertArray);
					}
					//echo $this->db->last_query(); die;tbl_sep_releaseform

					if($flag) 
					{
						$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
						// redirect(current_url());
					} 
					else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
					// redirect(current_url());
				}



			}
				/*echo "<pre>";
				print_r($content['release_detail']);exit();*/
			$content['token'] = $token;
			$content['title'] = 'Staff_sepreleasefromservice';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}else{
			$this->session->set_flashdata('er_msg','Missing trans id!!');
        	header("location:javascript://history.go(-1)", 'refresh');
		}
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}







}