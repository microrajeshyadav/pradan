<?php 

/**
* TC DAship Components Comments Controller List
*/
class Tc_daship_component_comments extends CI_controller
{

  function __construct()
  {
    
    parent::__construct();
    $this->load->library('form_validation');
   
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model', 'model');
    $this->load->model('Global_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }
    $this->loginData = $this->session->userdata('login_data');
  }

  

  public function add($token=NULL)
  {

    // print_r($this->loginData); die;

  
    // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
       $this->db->trans_start();

       $this->form_validation->set_rules('comments','Cpmments','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

      $insertArr = array(
        'dashipid'   => $token,
        'comments'   => $this->input->post('comments'),
        'status'     => $this->input->post('status'),
        'createdby'  => $this->loginData->UserID,
        'createdon'  => date('Y-m-d H:i:s'),
       
        );
      
      $this->db->insert('tbl_tc_daship_component_comment', $insertArr);
      $this->db->trans_complete();

      $check_status = $this->input->post('status');

      //email for accept = 2 or modify = 3
      if ($check_status == 2) {
        // echo "string"; die();
        //staff emailid
        $getstaff = $this->model->getDAComponent($token);
        // print_r($getstaff); die();
        $daemailid     = $getstaff[0]->emailid;
        //hr emailid
        // echo $daemailid; die();
        $hremail       =$this->Global_model->getHRDEmailid();
        $hremailid     = $this->loginData->EmailID;

        $getreportingto = $this->model->getReportingTo($getstaff->teamid);
          // echo $hremailid; die;
        $subject = "Phase-Activity Accepted";
        $body = 'Dear '.$getreportingto->name.', <br><br> 
        Approval for Phase-Activity. Below are the details. <br><br>.
        <br> Staff Name - '.$getstaff[0]->candidatefirstname.' '.$getstaff[0]->candidatemiddlename.' '.$getstaff[0]->candidatelastname.'<br><br>
        Comment from TC: '.$this->input->post('comments').'<br>';
        $body .= 'Please check Phase-Activity <br>';
        $body .= 'Congratulation, Your Phase-Activity'.$getstaff[0]->phase_name.' is Accepted.';
 
        $to_email = $daemailid;
        // $to_name = $tcemailid;
        $recipients = $hremailid; 
        
        $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$recipients);
      
      }
      else
      {
        //staff emailid
        $getstaff = $this->model->getDAComponent($token);
        $daemailid     = $getstaff[0]->emailid;
          
        $subject = "TC Comments";
        $body = 'Dear '.$getstaff[0]->candidatefirstname.' '.$getstaff[0]->candidatemiddlename.' '.$getstaff[0]->candidatelastname.', <br><br>Greetings from '.$this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName.' <br>
        <br>Comment from TC: '.$this->input->post('comments').'<br>';
        $body .= 'Please check Phase-Activity: '.$getstaff[0]->phase_name.' and re-modify it and submit';

        $to_email = $daemailid;
        // $to_name = $tcemailid;
        // $recipients = $tcemailid; 
      
        $sendmail = $this->Common_Model->send_email($subject, $body, $to_email);
      }


      if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Error adding Comments'); 
         redirect(current_url());

      }else{


        $updateArr = array(
       
        'status'     => $this->input->post('status'),
        );
      $this->db->where('id',$token);
      $this->db->update('tbl_daship_component', $updateArr);
     

      $this->session->set_flashdata('tr_msg', 'Successfully added Comments');
         redirect('/Tc_daship_components');     
      }
     
      
    }
  

  prepareview:

  //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0'";
  $content['batch_details'] = $this->Common_Model->query_data($query);
  
  //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);

  $content['dacomponent_details'] = $this->model->getDAComponent($token);

    $content['subview']="add";
    $content['title'] = 'Tc_daship_component_comments';
    $content['subview'] = 'Tc_daship_component_comments/add';
    $this->load->view('_main_layout', $content);
  }

 
    function delete($token = null)
    {
      $this->Common_Model->delete_row('mstphase','id', $token); 
      $this->session->set_flashdata('tr_msg' ,"Phase Deleted Successfully");
      redirect('/DA_event_detailing/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
  }

}
