<?php 

/**
* District List
*/
class District extends Ci_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model','model');
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		$content['state_list'] = $this->model->getstate();

		$state_code = $this->input->get('state_code');



		if ($state_code != NULL) {

			$this->db->select('a.districtid, a.name as DistrictName,a.isdeleted as status, b.name as StateName');
			$this->db->from('district as a');
			$this->db->join('state as b', 'a.stateid = b.statecode');
			$this->db->where('a.stateid', $state_code);
			$this->db->where('a.isdeleted', 0);
			$content['district_list'] = $this->db->get()->result();

		}else{
			$content['district_list'] = [];
		}

		$content['subview']="index";

		$content['title'] = 'District';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}

	public function add()
	{
		try{


		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){

				// $this->form_validation->set_rules('DistrictCode','District Code','trim|required|min_length[1]|max_length[2]|numeric');
			// $this->form_validation->set_rules('Name','District Name','trim|required');
			

			// if($this->form_validation->run() == FALSE){
			// 	$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
			// 		'</div>');

			// 	$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

			// 	$hasValidationErrors    =    true;
			// 	goto prepareview;

			// }

		$statecode = $this->input->post('state_code');
		$DistrictCode = $this->input->post('state_code').'/'.$this->input->post('DistrictCode');

		  $query = "SELECT count(districtid) as distcount FROM `district` WHERE districtid ='". $DistrictCode."'";
         $check = $this->db->query($query)->row(); 
   

         if ($check->distcount==0) {
         	$insertArr = array(
			'stateid'      => $this->input->post('state_code'),
			'districtid'   => $DistrictCode,
			'name'         => $this->input->post('Name'),
			'isdeleted'    => 0,
		);
			// print_r($insertArr); die();
		$this->Common_Model->insert_data('district', $insertArr);
		
		$this->session->set_flashdata('tr_msg', 'Successfully added District');
		redirect('/District/index?state_code='.$statecode.'');
         }else{
         	 $this->session->set_flashdata('er_msg', 'Record allready available for selected district code');
		   redirect('/District/index?state_code='.$statecode.'');
         }
			
		
		}
		 
		$content['state_list'] = $this->db->get('state')->result();

		$state_code = $this->input->get('state_code');

		if ($state_code != NULL) {

			$this->db->select('a.districtid, a.name as DistrictName, b.name as StateName');
			$this->db->from('district as a');
			$this->db->join('state as b', 'a.stateid = b.statecode');
			$this->db->where('a.stateid', $state_code);
			$content['district_list'] = $this->db->get()->result();

		}else{
			$content['district_list'] = [];
		}
		//prepareview:
		$content['title'] = 'District';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}

	public function edit($DistrictCode)
	{
		try{
		
		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		$statecode = $this->uri->segment(3);
		$DistCode = $this->uri->segment(4);

		$DistrictCode = $statecode.'/'.$DistCode;

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
			$updateArr = array(
				'stateid'       => $statecode,
				'districtid'    => $DistrictCode,
				'name'          => $this->input->post('Name'),
				'isdeleted'     =>  $this->input->post('status'),
			);
			
			$this->Common_Model->update_data('district', $updateArr,'districtid',$DistrictCode);
			//echo $this->db->last_query(); die;
			$this->session->set_flashdata('tr_msg', 'Successfully Updated District');
			redirect('/District/index?state_code='.$statecode.'');
		}

		$content['state_list'] = $this->db->get('state')->result();

		$state_code = $this->input->get('state_code');

		if ($state_code != NULL) {

			$this->db->select('a.districtid, a.name as DistrictName, b.name as StateName');
			$this->db->from('district as a');
			$this->db->join('state as b', 'a.stateid = b.statecode');
			$this->db->where('a.stateid', $state_code);
			$content['district_list'] = $this->db->get()->result();

		}else{
			$content['district_list'] = [];
		}

		$content['statecode'] = $statecode;
		$content['districtcode'] = $DistCode;
		$content['title'] = 'District';
		$district_details = $this->Common_Model->get_data('district', '*', 'districtid',$DistrictCode);
		$content['district_details'] = $district_details;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

	}

	function delete()
	{
		try{
		 // start permission 
		
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 
		
		$statecode = $this->uri->segment(3);
		$DistCode = $this->uri->segment(4);

		$DistrictCode = $statecode.'/'.$DistCode;
		$updateArr = array (
			'isdeleted'   => 1,
		);

		$this->Common_Model->update_data('district', $updateArr,'districtid',$DistrictCode);

		$this->session->set_flashdata('tr_msg' ,"District Deleted Successfully");
		redirect('/District/index?state_code='.$statecode);
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}

}