<?php 

/**
* State List
*/
class Leave extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
    try{
     
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from msleavetype where Isdeleted=0";
    $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    $content['title'] = 'Leave';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){
      
      
      $this->form_validation->set_rules('leavecategory','Leave Category','trim|required|min_length[1]|max_length[50]');
     //$this->form_validation->set_rules('Deduct','Deduct Name','trim|required|min_length[1]|max_length[50]');
      $this->form_validation->set_rules('Remarks','Remarks','trim|required|min_length[1]|max_length[50]');

     
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

          }

        if ($this->input->post('Deduct') != null) {
          $deduct = Yes;
        } else {
          $deduct = No;
        }


      $insertArr = array(
          'Ltypename'      => $this->input->post('leavecategory'),
          'deduct'      => $deduct,
          'remarks'      => $this->input->post('Remarks'),
          'createdon'      => date('Y-m-d H:i:s'),
          'createdby'      => $this->loginData->UserID,

          'status'      => $this->input->post('status')
        );
      



      $this->db->insert('msleavetype', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added Leave Category');
      redirect('/Leave/index/');
    }

    prepareview:

   
    $content['subview'] = 'Leave/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  public function edit($token)
  {
    try{

 
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('leavecategory','Name','trim|required|min_length[1]|max_length[50]');
      // $this->form_validation->set_rules('Deduct','Name','trim|required|min_length[1]|max_length[50]');
      $this->form_validation->set_rules('Remarks','Name','trim|required|min_length[1]|max_length[50]');

         $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }
 if ($this->input->post('Deduct') != null) {
          $deduct = Yes;
        } else {
          $deduct = No;
        }



if($this->input->post('Deduct') !=null){
      $deduct=Yes;
}else{
  $deduct=No;
}



        $updateArr = array(
         'Ltypename'      => $this->input->post('leavecategory'),
          
          'deduct'      => $deduct,

          //'deduct'      => $this->input->post('Deduct'),
          'remarks'      => $this->input->post('Remarks'),
          'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,

          'status'      => $this->input->post('status')
        );
      
      $this->db->where('Ltypeid', $token);
      $this->db->update('msleavetype', $updateArr);
 //echo  $this->db->last_query(); die;

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Leave Category');
       redirect('/Leave/');
    }

    prepareview:

    $content['title'] = 'Leave';
    $Leave_details = $this->Common_Model->get_data('msleavetype', '*', 'Ltypeid',$token);
   $content['Leave_details'] = $Leave_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
    function delete($token)
    {
      try{
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('Ltypeid', $token);
      $this->db->update('msleavetype', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Leave Category Deleted Successfully');
      redirect('/Leave/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}