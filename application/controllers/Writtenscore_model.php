<?php 

/**
* Dashboard Model
*/
class Writtenscore_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index()
	{

	}


	
  public function getSelectedCandidate($campusid)
  {
    // echo "$campusid=".$campusid;
    // die;
    try{
          $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename, `tbl_candidate_registration`.candidatelastname,`tbl_candidate_registration`.emailid,`tbl_candidate_writtenscore`.writtenscore FROM `tbl_candidate_registration` 
         left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
       Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` AND  `tbl_candidate_registration`.`metricpercentage` >= 60 AND ((`tbl_candidate_registration`.`hscpercentage` >= 60 And `tbl_candidate_registration`.`ugpercentage` >= 60) OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) "; 

      
       if ($campusid !='') {
          $sql .=' AND  `tbl_candidate_registration`.campusid ='.$campusid.'';
       }

       $sql .= ' ORDER BY `tbl_candidate_registration`.`candidateid` ASC ';


      // echo $sql; die;

        $res = $this->db->query($sql)->result();
        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }



  public function getSelectedCandidateWrittenScore($token = NULL)
  {
    
    try{

        $sql = 'SELECT * FROM `tbl_candidate_registration`
         left join `tbl_candidate_writtenscore` on `tbl_candidate_registration`.candidateid =`tbl_candidate_writtenscore`.candidateid 
          Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30  And (`hscpercentage`+4) <= `ugpassingyear` AND  metricpercentage >= 60 AND ((hscpercentage >= 60 And ugpercentage >= 60) OR (`ugpercentage` >= 55 AND `pgpercentage` >= 60)) 
         ';
          if(!empty($token)){

              $sql .= '  AND `tbl_candidate_registration`.campusid='.$token.'';
          }
          $sql .= ' ORDER BY `tbl_candidate_registration`.candidateid ASC';
          echo $sql; die;


        $res = $this->db->query($sql)->result();
        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


    public function getRecruiters()
	{
		try{
           
        $sql = "SELECT staffid,name FROM `staff`";  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}


	public function getCampus()
	{
		try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
    		$res = $this->db->query($sql)->result();

    		return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
	}


    public function getRecruitersCampus()
    {
        try{

            $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, cam.`campusincharge`,cam.`campusname`,cam.`city`,st1.`name` as name1, st2.`name` as name2, st3.`name` as name3 from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.IsDeleted = 0"; 

            
            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }




    public function getSingelRecruitersCampus($token)
    {
        try{

            $sql = "select rec.`dateofselecionprocess`, rec.`recruiterid`, rec.`recruitersname1`, rec.`recruitersname2`,rec.`recruitersname3`, cam.`campusincharge`,cam.`campusid`,cam.`city` from mstrecruiters rec left join `mstcampus` cam on rec.campusid = cam.campusid left join `staff` st1 on rec.recruitersname1 = st1.staffid left join `staff` st2 on rec.recruitersname2 = st2.staffid left join `staff` st3 on rec.recruitersname3 = st3.staffid Where rec.recruiterid='".$token."' and  rec.IsDeleted = 0"; 

            $res = $this->db->query($sql)->result();
            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }



 public  function get_quick_list($campusid)  
{  
    $this->db->select('*');    
    $this->db->from('tbl_candidate_registration');  
    $this->db->where("TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND metricpercentage > 60 AND hscpercentage > 60 And ugpercentage > 60 AND campusid LIKE '$distct'");
    $query=$this->db->get()->result_array(); 
     return $query;
}








}