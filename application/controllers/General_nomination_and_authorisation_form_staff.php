<?php 

/**
* General Nomination And Authorisation Form controller
*/
class General_nomination_and_authorisation_form_staff extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("General_nomination_and_authorisation_form_staff_model");
		$this->load->model("Employee_particular_form_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	

		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');

//print_r();
	}


	public function index($staff,$candidate_id)
	{
		try{

		if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

			$this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
			redirect('/staff_dashboard/index');

		} else {

			$staff = $this->uri->segment(3);
			$candidate_id  = $this->uri->segment(4);

			$content['staff'] = $staff;
			$content['candidate_id'] = $candidate_id;

			$this->session->set_userdata('staff', $staff);
			$this->session->set_userdata('candidate_id', $candidate_id);

			$staff_id= $staff;
      //echo "staff_id=".$staff_id;
			$candidateid=$candidate_id;
      //$candidateid=198;
    	//echo "candidate id=". $candidateid;
    	//die();
			if(isset($staff_id)&& isset($candidateid))
			{

				$staff_id=$staff_id;
        //echo "staff id".$staff_id;
				$candidateid=$candidateid;
        //echo "candate id=".$candidateid;

			}
			else
			{
				$staff_id=$staff;
         //echo "staff".$staff;
				$candidateid=$candidate_id;
        //echo "candidateid=".$candidateid;

			}


			$content['report']=$this->General_nomination_and_authorisation_form_staff_model->staff_reportingto($staff_id);
			$content['candidateaddress'] = $this->General_nomination_and_authorisation_form_staff_model->getCandidateWithAddress($staff_id);
			$content['personnal_mail'] = $this->General_nomination_and_authorisation_form_staff_model->personal_email();
      // print_r($content['personnal_mail']); die;
			$personal_email=$content['personnal_mail']->EmailID;

			$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
			$content['signature'] = $this->Common_Model->staff_signature($staff_id);

			$content['tc_email'] = $this->General_nomination_and_authorisation_form_staff_model->tc_email($reportingto);

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){

				//print_r($_POST);
				//die();


				$Sendsavebtn = $this->input->post('savebtn');

				if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {


  			 	//echo $updateArr['signatureplace_encrypted_filename'] ;
  			 	//die("pooja");



					$dadate = $this->input->post('dadate');

					$dadate = $this->model->changedatedbformate($dadate);


					$insertarraydata = array(
						'staff_id'                         => $staff_id,
						'candidateid'                     => $candidateid,
						'da_place'                        => $this->input->post('daplace'),
						'da_date'                         => $dadate,
						'status'                          => 0,
						'createdon'                       => date('Y-m-d H:i:s'),
						'type'                            => 'GN',

					);
				// print_r($insertarraydata); die;
					$this->db->insert('tbl_general_nomination', $insertarraydata);
					$insertid = $this->db->insert_id();
					$data1=$this->input->post('data');

   					foreach($data1 as $key => $value)
   					{
   						if ($value['name'] != '') {
						$arr= array (

							'sr_no'            => $key,
							'nominee_name'     => $value['name'],
							'nomination_id'    => $insertid,
							'nominee_relation' => $value['relationship_nominee'],
							'nominee_age'      => $value['age_nominee'],
							'nominee_address'  => $value['address_nominee'],
							'share_nomine'     => $value['share_nominee'],
							'minior'           => $value['minior']

						);

						$this->db->insert('tbl_nominee_detail_joininginduction', $arr);
					}
					}



					$insert_data = array (

						'type'      => 15,
						'r_id'      => $insertid,
						'sender'    => $staff_id,
						'receiver'  => $content['report']->reportingto,
						'senddate'  => date('Y-m-d'),
						'createdon' => date('Y-m-d H:i:s'),
						'createdby' => $this->loginData->UserID,
						'flag'      => 1,
						'staffid'  => $staff_id
					);
					$this->db->insert('tbl_workflowdetail', $insert_data);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

					}else{

						$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	

						redirect('General_nomination_and_authorisation_form_staff/edit/'.$staff.'/'.$candidateid);		
					}

				}

				$submitdatasend = $this->input->post('submitbtn');

				if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {




  			//echo "image=".$updateArr['signatureplace_encrypted_filename'];
  			//die;


					$dadate = $this->input->post('dadate');

					$dadate = $this->model->changedatedbformate($dadate);


					$insertarraydata = array (

						'staff_id'                         => $staff_id,
						'candidateid'                     => $candidateid,
						'da_place'                        => $this->input->post('daplace'),
						'nomination_authorisation_signed' => $updateArr['signatureplace_encrypted_filename'],
						'da_date'                         => $dadate,
						'status'                          => 1,
						'createdon'                       => date('Y-m-d H:i:s'),
						'type'                            => 'GN',


					);

					$this->db->insert('tbl_general_nomination', $insertarraydata);
				// echo $this->db->last_query(); die;
					$insertid = $this->db->insert_id();
					$data1=$this->input->post('data');

					foreach($data1 as $value)
					{
						if ($value['name'] != '') {

						$arr= array ( 

							'sr_no'            => $value['sr_no'],
							'nominee_name'     => $value['name'],
							'nomination_id'    => $insertid,
							'nominee_relation' => $value['relationship_nominee'],
							'nominee_age'      => $value['age_nominee'],
							'nominee_address'  => $value['address_nominee'],
							'share_nomine'     => $value['share_nominee'],
							'minior'           => $value['minior']

						);

						$this->db->insert('tbl_nominee_detail_joininginduction', $arr);
					}
				    }


					$insert_data = array (

						'type'      => 15,
						'r_id'      => $insertid,
						'sender'    => $staff_id,
						'receiver'  => $content['report']->reportingto,
						'senddate'  => date('Y-m-d'),
						'createdon' => date('Y-m-d H:i:s'),
						'createdby' => $this->loginData->UserID,
						'flag'      => 1,
						'staffid'  => $staff_id
					);
					$this->db->insert('tbl_workflowdetail', $insert_data);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

					}else{

						$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	


						$subject = ': General nomination and authorisation form';
						$body = '<h4>'.$content['candidateaddress']->staff_name.' submited  General nomination and authorisation form </h4><br />';
						$body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
						<tr>
						<td width="96">Name </td>
						<td width="404">'.$content['candidateaddress']->staff_name.'</td>
						</tr>
						<tr>
						<td>Employ Code</td>
						<td> '.$content['candidateaddress']->emp_code.'</td>
						</tr>
						<tr>
						<td>Designation</td>
						<td>' .$content['candidateaddress']->desiname.'</td>
						</tr>
						<tr>
						<td>Office</td>
						<td>'.$content['candidateaddress']->officename.'</td>
						</tr>
						</table>';
						$body .= "<br /> <br />";
						$body .= "Regards <br />";
						$body .= " ". $content['candidateaddress']->staff_name ."<br>";
						$body .= " ". $content['candidateaddress']->desiname."<br>";
						$body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
						$to_useremail = $content['candidateaddress']->emailid;
						$tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
						$arr= array (
							$tcemailid      =>'tc',
						);

						$this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

						redirect('General_nomination_and_authorisation_form_staff/view/'.$staff.'/'.$candidateid);		
					}
				}

			}

			$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
			$var=$content['topbar']->nomination_flag;

			if ($var==null)
			{
				goto preview;

			}


			if($var==0)
			{
				redirect('/General_nomination_and_authorisation_form_staff/edit/'.$staff.'/'.$candidateid);
			}
			elseif($var==1)
			{
				redirect('/General_nomination_and_authorisation_form_staff/view/'.$staff.'/'.$candidateid);
			}



			preview:
			$content['sysrelations'] = $this->model->getSysRelations();
			$content['candidatedetailwithaddressc'] = $this->model->getCandidateWithAddressDetails($candidateid);

			$content['candidatedetailwithaddress']  = $this->model->getCandidateWithAddress($staff_id);

			$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
			$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);

			$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($candidateid);
			$content['office_name'] = $this->model->office_name();



			$content['title'] = 'General_nomination_and_authorisation_form_staff';

			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

			$this->load->view('_main_layout', $content);
		}

		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

	}

	public function edit($staff,$candidate_id)
	{
		try{

		$staff = $this->uri->segment(3);
		$candidate_id  = $this->uri->segment(4);

		$content['staff'] = $staff;
		$content['candidate_id'] = $candidate_id;

		$this->session->set_userdata('staff', $staff);
		$this->session->set_userdata('candidate_id', $candidate_id);







		$staff_id=$staff;
		$candidateid=$candidate_id;

		if(isset($staff_id)&& isset($candidateid))
		{

			$staff_id=$staff_id;
			// echo "staff id".$staff_id;
			$candidateid=$candidateid;
			// echo "candate id=".$candidateid;

		}
		else
		{
			$staff_id=$staff;
         //echo "staff".$staff;
			$candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

		}

		$content['report']=$this->General_nomination_and_authorisation_form_staff_model->staff_reportingto($staff_id);

		$content['candidateaddress'] = $this->General_nomination_and_authorisation_form_staff_model->getCandidateWithAddress($staff_id);
		// print_r($content['candidateaddress']); die;
		$content['personnal_mail'] = $this->General_nomination_and_authorisation_form_staff_model->personal_email();
      // print_r($content['personnal_mail']); die;
		$personal_email=$content['personnal_mail']->EmailID;

		$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
		$content['tc_email'] = $this->General_nomination_and_authorisation_form_staff_model->tc_email($reportingto);
		$content['signature'] = $this->Common_Model->staff_signature($staff_id);

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST"){
			// print_r($this->input->post()); die();

			$savesenddata = $this->input->post('savebtn');

			if (!empty($savesenddata) && $savesenddata =='senddatasave') {


				
				$dadate = $this->input->post('dadate');

				$dadate = $this->model->changedatedbformate($dadate);


				$updatearraydata = array
				(

					'staff_id'     => $staff_id,
					'candidateid' => $candidateid,
					'da_place'    => $this->input->post('daplace'),
					'da_date'     => $dadate,
					'status'      => 0,


				);
				$this->db->where('staff_id', $staff_id);
				$this->db->update('tbl_general_nomination', $updatearraydata);
				// echo $this->db->last_query();
			//die;
			//$countnominee = count($this->input->post('full_name_nominee'));


				$data1=$this->input->post('data');
		//print_r($data1);
		//die;

	foreach($data1 as $key => $value)
   {
    $id =  $value['nomination_id']; 
    $this->db->where('nomination_id', $id);
      $this->db->delete('tbl_nominee_detail_joininginduction');
   }
				
    				foreach($data1 as $key => $value)
   					{
					if ($value['full_name_nominee'] != '') {
					$arr = array (

						'sr_no'            => $key,
						'nominee_name'     => $value['full_name_nominee'],
						'nomination_id'    => $value['nomination_id'],
						'nominee_relation' => $value['relationship_nominee'],
						'nominee_age'      => $value['age_nominee'],
						'nominee_address'  => $value['address_nominee'],
						'share_nomine'     => $value['share_nominee'],
						'minior'           => $value['minior']

					);
					// print_r($arr);
					// die;
					// extract($arr);
					//die;
					// $this->db->where('id', $id);
					$this->db->insert('tbl_nominee_detail_joininginduction', $arr);
					//echo $this->db->last_query();
					//die;
					
				}
				}

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

				}else{

					$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	

				}
				redirect('General_nomination_and_authorisation_form_staff/edit/'.$staff.'/'.$candidateid);		
			}


			$submitsenddata = $this->input->post('submitbtn');

			if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {



  				// die("hghh");

				$dadate = $this->input->post('dadate');

				$dadate = $this->model->changedatedbformate($dadate);


				$updatearraydata = array (

					'staff_id'                         => $staff_id,
					'candidateid'                     => $candidateid,
					'da_place'                        => $this->input->post('daplace'),
					'da_date'                         => $dadate,
					'status'                          => 1,


				);
				$this->db->where('staff_id', $staff_id);
				$this->db->update('tbl_general_nomination', $updatearraydata);
			//echo $this->db->last_query();
			//die;
			//$countnominee = count($this->input->post('full_name_nominee'));


				$data1=$this->input->post('data');
		//print_r($data1);
		//die;

				  foreach($data1 as $value)
   {
    $id =  $value['nomination_id']; 
    $this->db->where('nomination_id', $id);
      $this->db->delete('tbl_nominee_detail_joininginduction');
   }
   // print_r($data1); die();

				
				       foreach($data1 as $key => $value)
   					{
					if ($value['full_name_nominee'] != '') {
					$arr = array (

						'sr_no'            => $key,
						'nominee_name'     => $value['full_name_nominee'],
						'nomination_id'    => $value['nomination_id'],
						'nominee_relation' => $value['relationship_nominee'],
						'nominee_age'      => $value['age_nominee'],
						'nominee_address'  => $value['address_nominee'],
						'share_nomine'     => $value['share_nominee'],
						'minior'           => $value['minior']

					);
					// print_r($arr);
					// die;
					// extract($arr);
					//die;
					// $this->db->where('id', $id);
					$this->db->insert('tbl_nominee_detail_joininginduction', $arr);
					//echo $this->db->last_query();
					//die;
					
				}
				}
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

				}

			/*else{

				// $hrdemailid    = 'poonamadlekha@pradan.net';
				 $hrdemailid    = 'amit.kum2008@gmail.com';
				 $tcemailid     = $this->loginData->EmailID;

				 $subject = "Submit Gereral Nomination And Authorisation Form ";
				  $body = 	'Dear Sir, <br><br> ';
				  $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 $to_email = $hrdemailid;
				 $to_name = $tcemailid;
				
				 $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/

				 $this->session->set_flashdata('tr_msg', 'Successfully save and submit  general nomination and authorisation form');	

				 
				 $subject = ': General nomination and authorisation form';
				 $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  General nomination and authorisation form </h4><br />';
				 $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
				 <tr>
				 <td width="96">Name </td>
				 <td width="404">'.$content['candidateaddress']->staff_name.'</td>
				 </tr>
				 <tr>
				 <td>Employ Code</td>
				 <td> '.$content['candidateaddress']->emp_code.'</td>
				 </tr>
				 <tr>
				 <td>Designation</td>
				 <td>' .$content['candidateaddress']->desiname.'</td>
				 </tr>
				 <tr>
				 <td>Office</td>
				 <td>'.$content['candidateaddress']->officename.'</td>
				 </tr>
				 </table>';
				 $body .= "<br /> <br />";
				 $body .= "Regards <br />";
				 $body .= " ". $content['candidateaddress']->staff_name ."<br>";
				 $body .= " ". $content['candidateaddress']->desiname."<br>";
				 $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
				 $to_useremail = $content['candidateaddress']->emailid;
				 $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
				 $arr= array (
				 	$tcemailid      =>'tc',
				 );

				 $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

				 redirect('General_nomination_and_authorisation_form_staff/view/'.$staff.'/'.$candidateid);		



				}


			}
			$content['sysrelations'] = $this->model->getSysRelations();
			$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($candidateid);

			$content['genenominformdetail'] = $this->model->getGeneralnominationform($staff_id);
		//print_r($content['genenominformdetail']);

			$content['nominee'] =$this->model->getGeneralnominationform($candidateid);
	//	echo "<pre>";
		//print_r($content['nominee']);
		//die;

       // $content['countnominee'] =$this->model->getCountNominee($staff_id);
        //die('hdghds');
			$content['nomineedetail'] = $this->model->getNomineedetail($candidateid);
		//echo "<pre>";
		//print_r($content['nomineedetail']);
			$content['office_name'] = $this->model->office_name();

			$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
			$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
		//print_r($content['getgeneralform']);
			$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($candidateid);


			$content['title'] = 'General_nomination_and_authorisation_staff_form';

			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

			$this->load->view('_main_layout', $content);

			}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
		}



		public function view($staff,$candidate_id)
		{
			try{
			if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {

				$this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
				redirect('/staff_dashboard/index');

			} else {

				$staff = $this->uri->segment(3);
				$candidate_id  = $this->uri->segment(4);

				$content['staff'] = $staff;
				$content['candidate_id'] = $candidate_id;

				$this->session->set_userdata('staff', $staff);
				$this->session->set_userdata('candidate_id', $candidate_id);






				$login_staff=$staff;
				$staff_id=$staff;
				$candidateid=$candidate_id;

				if(isset($staff_id)&& isset($candidateid))
				{

					$staff_id=$staff_id;
				// echo "staff id".$staff_id;
					$candidateid=$candidateid;
				// echo "candate id=".$candidateid;

				}
				else
				{
					$staff_id=$staff;
         //echo "staff".$staff;
					$candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

				}
				$content['id'] = $this->General_nomination_and_authorisation_form_staff_model->get_generalworkflowid($staff_id);
      // print_r($content['id']); die;
				$content['staff_details'] = $this->General_nomination_and_authorisation_form_staff_model->staffName($staff_id,$login_staff);
			// echo "<pre>";
			// print_r($content['staff_details']); die;
				$content['sysrelations'] = $this->model->getSysRelations();
				$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($candidateid);

				$content['genenominformdetail'] = $this->model->getViewGeneralnominationform($candidateid);

				$content['get_witness'] = $this->General_nomination_and_authorisation_form_staff_model->get_witness_detail($staff_id);


				$content['getGeneralnomination'] = $this->model->getViewGeneralnominationform($candidateid);

				$content['nominee'] =$this->model->getGeneralnominationform($candidateid);
				$content['office_name'] = $this->model->office_name();
		//print_r($content['office_name']);
				$content['nomineedetail'] = $this->model->getNomineedetail($candidateid);
				$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
				$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
				$content['personal']=$this->Common_Model->get_Personnal_Staff_List();

				$content['candidateaddress'] = $this->General_nomination_and_authorisation_form_staff_model->getCandidateWithAddress($staff_id);
				$content['personnal_mail'] = $this->General_nomination_and_authorisation_form_staff_model->personal_email();
      // print_r($content['personnal_mail']); die;
				$personal_email=$content['personnal_mail']->EmailID;

				$reportingto = $content['candidateaddress']->reportingto;

    // echo $reportingto; die;
				$content['tc_email'] = $this->General_nomination_and_authorisation_form_staff_model->tc_email($reportingto);
    // print_r($content['tc_email']); die;
				$content['reporting'] = $this->General_nomination_and_authorisation_form_staff_model->getCandidateWith($reportingto);

				$personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;

				$content['signature'] = $this->Common_Model->staff_signature($staff_id);
				

				if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
				{
					$RequestMethod = $this->input->server('REQUEST_METHOD'); 


					if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
						$status=$this->input->post('status');
						$reject=$this->input->post('reject');
						$p_status=$this->input->post('p_status');
						$wdtaff1=$this->input->post('wdtaff1');
						$wdtaff2=$this->input->post('wdtaff2');
						$r_id=$this->input->post('id');

						$check = $this->session->userdata('insert_id');

						if($status==2){
							$appstatus = 'approved';
						}elseif ($status==3) {
							$appstatus = 'Reject';
						}

						$insert_data = array (
							'type'                 => 15,
							'r_id'                 => $r_id,
							'sender'               => $login_staff,
							'receiver'             => $p_status,
							'senddate'             => date('Y-m-d'),
							'createdon'            => date('Y-m-d H:i:s'),
							'createdby'            => $login_staff,
							'scomments'            => $reject,
							'forwarded_workflowid' => $content['id']->workflow_id,
							'flag'                 => $status,
							'staffid'              => $staff_id
						);

						$arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2); 

       // print_r($insert_data);
        //die();


						$this->db->insert('tbl_workflowdetail', $insert_data);

						$this->db->where('graduity_id',$r_id);
						$this->db->update('tbl_graduitynomination',$arr_data);


						$subject = ': General nomination and authorisation form';
						$body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc </h4><br />';
						$body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
						<tr>
						<td width="96">Name </td>
						<td width="404">'.$content['candidateaddress']->staff_name.'</td>
						</tr>
						<tr>
						<td>Employ Code</td>
						<td> '.$content['candidateaddress']->emp_code.'</td>
						</tr>
						<tr>
						<td>Designation</td>
						<td>' .$content['candidateaddress']->desiname.'</td>
						</tr>
						<tr>
						<td>Office</td>
						<td>'.$content['candidateaddress']->officename.'</td>
						</tr>
						</table>';
						$body .= "<br /> <br />";
						$body .= "Regards <br />";
						$body .= " ". $content['candidateaddress']->staff_name ."<br>";
						$body .= " ". $content['candidateaddress']->desiname."<br>";
						$body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
						$to_useremail = $content['candidateaddress']->emailid;
						$tcemailid=$content['tc_email']->emailid;
						$personal_email=$content['personnal_mail']->EmailID;
						$to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
						$arr= array (
							$tcemailid      =>'tc',
							$personal_email =>'Personnel'
						);

						$this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
						redirect('General_nomination_and_authorisation_form_staff/view/'.$staff.'/'.$candidateid);		


					}


				}
				else  if($this->loginData->RoleID==17)
				{
					$RequestMethod = $this->input->server('REQUEST_METHOD'); 


					if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
						$status=$this->input->post('status');
						$reject=$this->input->post('reject');
						$p_status=$this->input->post('p_status');
						$wdtaff1=$this->input->post('wdtaff1');
						$wdtaff2=$this->input->post('wdtaff2');
						$r_id=$this->input->post('id');

						$check = $this->session->userdata('insert_id');

						if($status==4){
							$appstatus = 'approved';
						}elseif ($status==3) {
							$appstatus = 'Reject';
						}

						$insert_data = array (

							'type'                 => 15,
							'r_id'                 => $r_id,
							'sender'               => $login_staff,
							'receiver'             => $staff_id,
							'senddate'             => date('Y-m-d'),
							'createdon'            => date('Y-m-d H:i:s'),
							'createdby'            => $login_staff,
							'scomments'            => $reject,
							'forwarded_workflowid' => $content['id']->workflow_id,
							'flag'                 => $status,
							'staffid'             => $staff_id
						);




						$this->db->insert('tbl_workflowdetail', $insert_data);


						$subject = ': General nomination and authorisation form';
						$body = '<h4>'.$personnelname.' '.$appstatus.' By Personnel </h4><br />';
						$body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
						<tr>
						<td width="96">Name </td>
						<td width="404">'.$content['candidateaddress']->staff_name.'</td>
						</tr>
						<tr>
						<td>Employ Code</td>
						<td> '.$content['candidateaddress']->emp_code.'</td>
						</tr>
						<tr>
						<td>Designation</td>
						<td>' .$content['candidateaddress']->desiname.'</td>
						</tr>
						<tr>
						<td>Office</td>
						<td>'.$content['candidateaddress']->officename.'</td>
						</tr>
						</table>';
						$body .= "<br /> <br />";
						$body .= "Regards <br />";
						$body .= " ". $content['candidateaddress']->staff_name ."<br>";
						$body .= " ". $content['candidateaddress']->desiname."<br>";
						$body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
						$to_useremail = $content['candidateaddress']->emailid;
						$tcemailid=$content['tc_email']->emailid;
						$personal_email=$content['personnal_mail']->EmailID;
						$to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
						$arr= array (
							$tcemailid      =>'tc',
							$personal_email =>'personal'
						);

						$this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
						redirect('General_nomination_and_authorisation_form_staff/view/'.$staff.'/'.$candidateid);		



					}


				}




   // print_r($content['nomineedetail']);




				$content['title'] = 'General_nomination_and_authorisation_staff_form';

				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

				$this->load->view('_main_layout', $content);
			}


			}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
		}



	}