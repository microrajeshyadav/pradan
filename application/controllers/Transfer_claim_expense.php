<?php 
class Transfer_claim_expense extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("Stafftransferpromotion_model","Stafftransferpromotion_model");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
		  $this->load->model("Provident_fund_nomination_form_model");
		  $this->load->model("General_nomination_and_authorisation_form_model");
		  
		  $this->load->model("Transfer_claim_expense_model");
	}

	public function index($token)
	{
		try{

		$staff_id=$this->loginData->staffid;
		$content['token'] = $token;
		$sql = "SELECT max(workflowid) as workflowid, max(flag) as flag, staffid FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->result()[0];
	    $forwardworkflowid = $result->workflowid;
		/*echo "<pre>";
		print_r($result->staffid);exit();*/
		//$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		$content['staff_details']=$this->Stafftransferpromotion_model->get_staffDetails($result->staffid);
		$content['transfer_claim_expense_detail'] = $this->Transfer_claim_expense_model->transfer_claim_expense_detail($content['staff_details']->staffid);
		/*echo "<pre>";
		print_r($content['transfer_claim_expense_detail']);die();*/
		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		$receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['staff_details']->reportingto);

		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST")
		{	
			$submitdatasend = $this->input->post('submitbtn');
			$Sendsavebtn = $this->input->post('savetbtn');

      // if ($this->input->post('comments')) {
			$date_release= $this->input->post('date_release');
			$location= $this->input->post('location');
			$travel_exp= $this->input->post('travel_exp');
			$local_conveyance= $this->input->post('local_conveyance');
			$train_claim= $this->input->post('train_claim');
			$truck_con_claim= $this->input->post('truck_con_claim');
			$any_other_claim= $this->input->post('any_other_claim');
			
			$total= $this->input->post('total');	
			$car_claim= $this->input->post('car_claim');
			$motor_cycle_claim = $this->input->post('motor_cycle_claim');	
			$bicycle_claim = $this->input->post('bicycle_claim');	
			$v_con_total= $this->input->post('v_con_total');
			$l_allowance= $this->input->post('l_allowance');
			$grand_total= $this->input->post('grand_total');
			$ta_advance= $this->input->post('ta_advance');
			$net_amount_payable= $this->input->post('payable_amount');

			$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
			$zero = 0;
			$trvexproriginalName = '';
			$trvexprencryptedName = '';
			if (isset($_FILES['attach_travel_exp']['name'])) {
				if($_FILES['attach_travel_exp']['name'] !=NULL){
					@  $ext = end((explode(".", $_FILES['attach_travel_exp']['name'])));

					$trvexproriginalName = $_FILES['attach_travel_exp']['name']; 
					$trvexprencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)).".".$ext;
					$tempFile = $_FILES['attach_travel_exp']['tmp_name'];
					$targetPath = FCPATH . "datafiles/educationalcertificate/";
					$targetFile = $targetPath . $trvexprencryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$trvexprencryptedName = $trvexprencryptedName;
					}else{

						die("Error uploading pg Certificate Document ");
						$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
						redirect(current_url());
					}
				}else{
					$trvexproriginalName = '';
					$trvexprencryptedName = '';
				}

			}
			else{
				$trvexproriginalName = '';
				$trvexprencryptedName = '';
			}

			$lclconoriginalName = '';
			$lclconcryptedName = '';
			if (isset($_FILES['attach_local_conveyance']['name'])) {
				if($_FILES['attach_local_conveyance']['name'] !=NULL){
					@  $ext = end((explode(".", $_FILES['attach_local_conveyance']['name'])));

					$lclconoriginalName = $_FILES['attach_local_conveyance']['name']; 
					$lclconcryptedName = md5(date("Y-m-d H:i:s").rand(1,100)).".".$ext;
					$tempFile = $_FILES['attach_local_conveyance']['tmp_name'];
					$targetPath = FCPATH . "datafiles/educationalcertificate/";
					$targetFile = $targetPath . $lclconcryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$lclconcryptedName = $lclconcryptedName;
					}else{

						die("Error uploading pg Certificate Document ");
						$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
						redirect(current_url());
					}
				}else{
					$lclconoriginalName = '';
					$lclconcryptedName = '';
				}

			}
			else{
				$lclconoriginalName = '';
				$lclconcryptedName = '';
			}



				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp ? $travel_exp : $zero,
					'local_conveyance'=>$local_conveyance ? $local_conveyance : $zero,
					'conveyance_of_personal_train'=>$train_claim ? $train_claim : $zero,
					'conveyance_of_personal_truck'=>$truck_con_claim ? $truck_con_claim : $zero,
					'conveyance_of_personal_other'=>$any_other_claim ? $any_other_claim : $zero,
					'total'=>$total ? $total : $zero,
					'conveyance_of_vehicle_car'=>$car_claim ? $car_claim : $zero,
					'conveyance_of_vehicle_motor'=>$motor_cycle_claim ? $motor_cycle_claim : $zero,
					'conveyance_of_vehicle_bicycle'=>$bicycle_claim ? $bicycle_claim : $zero,
					'total_vehicle_con'=>$v_con_total ? $v_con_total : $zero,
					'lumpsum_transfer_allowance'=>$l_allowance ? $l_allowance : $zero,
					'less_amount_of_transfer_ta_advance'=>$ta_advance ? $ta_advance : $zero,
					'grand_total'=>$grand_total ? $grand_total : $zero,
					'net_amount_payable'=>$net_amount_payable ? $net_amount_payable : $zero,
					'tdate'=>$date,
					'flag'=>1,
					'createdon'=>date("Y-m-d H:i:s"),
					'createdby'=>$this->loginData->staffid,
					'attach_travel_exp' => $trvexprencryptedName,
					'attach_local_conveyance' => $lclconcryptedName
					);
				/*echo "<pre>";
				print_r($insertArraydata);
				die('test');*/
				 $this->db->insert('tbl_transfer_expenses_claim', $insertArraydata);
				
				$updateArr = array(                         
          'trans_flag'     => 17,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 6,
         'staffid'              => $content['staff_details']->staffid,
         'sender'               => $this->loginData->staffid,
         'receiver'             => $content['staff_details']->reportingto,
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => 17,
         'scomments'            => $this->input->post('comments'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$subject = "Your Process Approved";
			      $body = 'Dear,<br><br>';
			      $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
			      $body .= 'This is to certify that Mr. '.$content['staff_details']->name.'<br> your process has Approved  .<br><br>';
			      $body .= 'Thanks<br>';
			      $body .= 'Administrator<br>';
			      $body .= 'PRADAN<br><br>';

			      $body .= 'Disclaimer<br>';
			      $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

			      
			      $to_email = $receiverdetail->emailid;
			      $to_name = $receiverdetail->name;
			      //$to_cc = $getStaffReportingtodetails->emailid;
			      $recipients = array(
			         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
			         $content['staff_details']->emailid => $content['staff_details']->name
			         // ..
			      );
			      $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
			      if (substr($email_result, 0, 5) == "ERROR") {
			        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
			      }
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate save  expense calim details');
					redirect('Transfer_claim_expense/index/'.$token);			
				}

			// }else{
			// 	$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			// }
	
		}
		/*$submitdatasend = $this->input->post('submitbtn');
		if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				echo "hgjuy";
				$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
				$v_con_total=$this->input->post('v_con_total');
				$v_con_total=$this->input->post('v_con_total');
				$l_allowance=$this->input->post('l_allowance');
				$grand_total=$this->input->post('grand_total');
				$ta_advance=$this->input->post('ta_advance');
				$net_amount_payable=$this->input->post('payable_amount');

				$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp,
					'local_conveyance'=>$local_conveyance,
					'conveyance_of_personal_train'=>$train_claim,
					'conveyance_of_personal_truck'=>$truck_con_claim,
					'conveyance_of_personal_other'=>$any_other_claim,
						'local_conveyance'=>$total,
						'conveyance_of_vehicle_car'=>$car_claim,
						'conveyance_of_vehicle_motor'=>$motor_cycle_claim,
						'conveyance_of_vehicle_bicycle'=>$bicycle_claim,
						'total_vehicle_con'=>$v_con_total,
						'lumpsum_transfer_allowance'=>$l_allowance,
						'less_amount_of_transfer_ta_advance'=>$ta_advance,
						'grand_total'=>$grand_total,
						'net_amount_payable'=>$net_amount_payable,
						'tdate'=>$date,
						'flag'=>1


					);
				//echo "<pre>";
				//print_r($insertArraydata);
				//die();
				 $this->db->insert('tbl_transfer_expenses_claim', $insertArraydata);
				//$this->db->insert($insertArraydata);
				//echo $this->db->last_query(); 

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate save and submit  expense calim details');
					redirect('Transfer_claim_expense/view/'.$staff_id);			
				}



				//die();
			}*/
	

				

		//$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($this->loginData->teamid);

			
		$content['title'] = 'Transfer_claim_expense/index';
		 //die("hghgh");

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {

		print_r($e->getMessage());die;
	}

	}

		public function edit_record($token)
		{
			try{
				
			$staff_id=$this->loginData->staffid;
			//die("shgdsgh");
			
		
		$content['staff_details']=$this->Stafftransferpromotion_model->get_staffDetails($staff_id);
		$content['title'] = 'Transfer_claim_expense/edit';
		 //die("hghgh");

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
		}
		/*
		public function view($token)
		{
				try
				{
			$staff_id=$this->loginData->staffid;
		
		$content['staff_details']=$this->Stafftransferpromotion_model->get_staffDetails($staff_id);
		$content['title'] = 'Transfer_claim_expense/view';
		 //die("hghgh");

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {

		print_r($e->getMessage());die;
	}
		}
*/
/**
   * Method GeneralNominationWithWitnessform() get General Nomination With Witnessform.
   * @access  public
   * @param Null
   * @return  Array
   */

public function GeneralNominationWithWitnessform($token)
{

	try{

		$this->load->model('Joining_of_da_document_verification_model');
     	
		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){

			$savedata = $this->input->post('savebtn');

			if (!empty($savedata) && $savedata =='senddatasave') {

				$InsertArrayData = array(
					'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
					'first_witness_name'  => $this->input->post('firstwitnessname'),
					'first_witness_address'  => $this->input->post('firstwitnessaddress'),
					'second_witness_name'  => $this->input->post('secondwitnessname'),
					'second_witness_address'  => $this->input->post('secondwitnessaddress'),					
				);

				$this->db->where('candidateid', $token);
				$this->db->update('tbl_general_nomination_and_authorisation_form', $InsertArrayData);

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! save witness information !!!');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully save witness information !!!');		
				}
			}

			$submitsendbtn = $this->input->post('submitbtn'); 
			if (!empty($submitsendbtn) && $submitsendbtn =='senddatasubmit') {

				$InsertArrayData = array(
					'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
					'first_witness_name'  => $this->input->post('firstwitnessname'),
					'first_witness_address'  => $this->input->post('firstwitnessaddress'),
					'second_witness_name'  => $this->input->post('secondwitnessname'),
					'second_witness_address'  => $this->input->post('secondwitnessaddress'),
					'status'  => 2,					
				);

				$this->db->where('candidateid', $token);
				$this->db->update('tbl_general_nomination_and_authorisation_form', $InsertArrayData);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! save witness information !!!');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully save witness information !!!');		
				}

			}


		}

        $content['token'] = $token;
		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($token);
		$content['genenominformdetail'] = $this->model->getViewGeneralnominationform($token);

		$getGeneralnomination = $this->model->getViewGeneralnominationform($token);
		
		$content['nomineedetail'] = $this->model->getNomineedetail($getGeneralnomination->id);

		$getTeamid = $this->model->getCandidateTeam($token);

		$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($getTeamid->teamid);
		
		$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

		$content['generalnominationFormUploadpdf'] = $this->model->getGeneralNominationWithWitnessformFormUploadStatus($token);

		$content['generaljoiningFormUploadpdf'] = $this->model->getJoiningReportFormPdfStatus($token);


		$content['title'] = 'Joining_of_da_document_verification';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {

		print_r($e->getMessage());die;
	}


} 



public function Joining_report_form($token){

	try{
		//echo $token; die;
		 $join_programme_date = "";
		 $this->db->trans_start();
		 //$token = $this->input->post('token');
		 $RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){


			$candidatedetails = $this->model->getSingleCandidateDetails($token);
			$JoiningReport = $this->model->getJoiningReportDetail($token);
			$joiningreportid = $JoiningReport->id;
			$candidateemailid = $candidatedetails->emailid;
			$candidatename = $candidatedetails->candidatefirstname;
			$candidatedetails->candidateid;
			

			$submitsenddata = $this->input->post('submitbtn');

		if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

			$join_programme_date1 = $this->input->post('join_programme_date');

		     $join_programme_date = $this->model->changedatedbformate($join_programme_date1);

            	$updatearraydata = array(

				'status'  => 2,
				'submittedon'  => date('Y-m-d H:i:s'), 
				'submittedby'  => $this->loginData->UserID,
			);

			$this->db->where('id',$joiningreportid);
      		$this->db->update('tbl_joining_report', $updatearraydata);
      		//echo $this->db->last_query(); die;
      		$this->db->trans_complete();

      		  if ($this->db->trans_status() === FALSE){
      		  
				$this->session->set_flashdata('er_msg', 'Error !!! Candidate Joining report save');	

			}else{
				
		// $hrdemailid    = 'poonamadlekha@pradan.net';
		 $hrdemailid      = 'amit.kum2008@gmail.com';
		 $tcemailid       = $this->loginData->EmailID;
		 $candidateemail  = $candidateemailid;

		 $subject = "Submit Joining Report Form ";
		 $body  = 	'Dear '.$candidatename.', <br><br> ';
		 $body .= 'Submit Joining Report Form Successfully ';
		 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
		 $to_email = $hrdemailid;
		 $to_name = $tcemailid;
		 $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);

		 if ($sendmail) {
		 	$sendmail1 = $this->Common_Model->send_email($subject, $body, $to_email);
		 }

		$this->session->set_flashdata('tr_msg', 'Successfully Candidate Joining report save');	

		redirect('/Joining_of_da_document_verification/Joining_report_form/'.$token);		
			}


		  }
		}


		$content['token'] = $token;
		$content['candidatedetils']        = $this->model->getCandidateDetails($token);
		$content['joiningreportdetails']   = $this->model->getJoiningReportDetail($token);

		//print_r($joiningreportdetails); die;

		
		//$content['getstatus'] = $this->model->getStatus($this->loginData->candidateid);

		$content['title'] = 'Joining_report_form';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);


	}catch (Exception $e) {

		print_r($e->getMessage());die;
	}
	
}


public function verification($token)
{
	try{
	

$this->load->model('Joining_of_da_document_verification_model');
	

$this->db->trans_start();

	$RequestMethod = $this->input->server('REQUEST_METHOD'); 

	$DevelopmentApprenticeshipDetails = $this->model->getDevelopmentApprenticeship($token);
	$candidateemailid = $DevelopmentApprenticeshipDetails->emailid;

	if($RequestMethod == "POST"){

		//  echo "<pre>";
	 // print_r($this->input->post()); die();

		$savebtndata = $this->input->post('savebtn');

		if (!empty($savebtndata) && $savebtndata =='senddatasave') {


			//print_r($this->input->post()); 

			$verificationarraydata = array(

				'candidateid'  => $token,
				'metric_certificate'  => $this->input->post('metric_certificate_verified'),
				'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
				'ug_certificate'  => $this->input->post('ug_certificate_verified'),
				'pg_certificate'  => $this->input->post('pg_certificate_verified'),
				'other_certificate'  => $this->input->post('other_certificate_verified'),
				'general_nomination_form'  => $this->input->post('general_nomination_form'),
				'joining_report_da'  => $this->input->post('joining_report_da'),
				'comment'  => $this->input->post('comments'),
				'status'=> $this->input->post('verified_status'), 
				'createdon'  => date('Y-m-d H:i:s'),
				'createdby'  => $this->loginData->UserID,
			);

			$this->db->insert('tbl_verification_document', $verificationarraydata);
			$insertid = $this->db->insert_id();

			 //$this->db->last_query(); //die;

			$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
			$this->db->delete('tbl_work_experience_verified', array('candidateid' => $token)); 

			for ($i=0; $i < $countworkexpverified; $i++) { 

				$insertArraydata = array(
					'candidateid'  => $token,
					'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
				);

			$this->db->insert('tbl_work_experience_verified', $insertArraydata);
			}
			
		 $countdocumentname = count($this->input->post('documentname'));
			
		$this->db->delete('tbl_other_documents', array('candidateid' => $token)); 

		for ($i=0; $i < $countdocumentname; $i++) { 
			if (!empty($this->input->post('documentname')[$i]) && isset($this->input->post('documentname')[$i])) {				
			

		   	if ($_FILES['otherdocumentupload']['name'][$i] != NULL) {

		   		@  $ext = end(explode(".", $_FILES['otherdocumentupload']['name'][$i])); 

					if($_FILES['otherdocumentupload']['size'] < 10485760) {
						$this->session->set_flashdata('er_msg', "filesize too large. Please upload file of size-2MB");
						redirect(current_url());
					}

		   	$OriginaldocumentName = $_FILES['otherdocumentupload']['name'][$i]; 
		   	$encrypteddocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   		$tempFile = $_FILES['otherdocumentupload']['tmp_name'][$i];
		   	    $targetPath = FCPATH . "otherdocuments/";
		   		$targetFile = $targetPath . $encrypteddocumentName;
		   		@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   		if($uploadResult == true){

		   			$encrypteddocument = $encrypteddocumentName;

		   		}else{

		   			die("Error uploading Other Document !!!");
		   			$this->session->set_flashdata("er_msg", "Error uploading Other Document ");
		   			redirect(current_url());
		   		}

		   	}
		   	// else{

		   	// 	$OriginaldocumentName = $this->input->post('originalexperiencedocument')[$i];
		   	// 	$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
		   	// }



				$insertArraydata = array(
					'candidateid'  => $token,
					'documentname'=> $this->input->post('documentname')[$i],
					'documenttype'=> $this->input->post('documenttype')[$i],
					'documentupload'=> $encrypteddocument,
					'createdon'=> date('Y-m-d H:i:s'),
					'createdby'=> $this->loginData->UserID,
				);

				$this->db->insert('tbl_other_documents', $insertArraydata);

				//echo $this->db->last_query(); die;
			}
		}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! Not save DA document verification !!!');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully save DA document verification !!!');		
				redirect('Joining_of_da_document_verification/edit/'.$token);	
			}

		}
	
		$savebtndatasend = $this->input->post('submitbtn');

		if (!empty($savebtndatasend) && $savebtndatasend =='senddatasubmit') {

		//print_r($this->input->post()); //die;

			//print_r($this->loginData); die;

			$candidateid  = $this->input->post('candidateid');
			
			$query = $this->db->query("SELECT * FROM `tbl_verification_document` WHERE `candidateid` = ".$candidateid);
            //echo $query->num_rows();die;
          // $query = $this->db->query("SELECT * FROM `tbl_verification_document` WHERE `candidateid` = ".$candidateid);

            $result = $query->result()[0];


           // echo $query->num_rows(); //die;

           // print_r($result); die;

			$currentstatus  = $this->input->post('verified_status');


             if ($query->num_rows() == 0){

             	$verificationarraydata = array(

				'candidateid'  => $token,
				'metric_certificate'  => $this->input->post('metric_certificate_verified'),
				'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
				'ug_certificate'  => $this->input->post('ug_certificate_verified'),
				'pg_certificate'  => $this->input->post('pg_certificate_verified'),
				'other_certificate'  => $this->input->post('other_certificate_verified'),
				'general_nomination_form'  => $this->input->post('general_nomination_form'),
				'joining_report_da'  => $this->input->post('joining_report_da'),
				'comment'  => $this->input->post('comments'),
				'status' => $currentstatus,
				'createdby' => $this->loginData->UserID, 	
				'createdon'  => date('Y-m-d H:i:s'),
				'approvedby' => $this->loginData->UserID, 	
				'approvedon'  => date('Y-m-d H:i:s'),

			);

			$this->db->insert('tbl_verification_document', $verificationarraydata);

			//echo $this->db->last_query(); die;

			if ($currentstatus ==0) {


				//echo "dsfsdf"; die;


			// $gerenalarraydata = array(

			// 	'status' => 0,
			// );
			// $this->db->where('candidateid', $candidateid);
			// $this->db->update('tbl_general_nomination_and_authorisation_form', $gerenalarraydata);


			// $joiningreportarraydata = array(

			// 	'status' => 0,
			// );
			// $this->db->where('candidateid', $candidateid);
			// $this->db->update('tbl_joining_report', $joiningreportarraydata);


			// $registrationarraydata = array(

			// 	'BDFFormStatus' => 99,
			// );
			// $this->db->where('candidateid', $candidateid);
			// $this->db->update('tbl_candidate_registration', $registrationarraydata);
				
				

		}

			 }else{

             	$verificationarraydata = array(

				'candidateid'  => $token,
				'metric_certificate'  => $this->input->post('metric_certificate_verified'),
				'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
				'ug_certificate'  => $this->input->post('ug_certificate_verified'),
				'pg_certificate'  => $this->input->post('$candidateid)g_certificate_verified'),
				'other_certificate'  => $this->input->post('other_certificate_verified'),
				'general_nomination_form'  => $this->input->post('general_nomination_form'),
				'joining_report_da'  => $this->input->post('joining_report_da'),
				'comment'  => $this->input->post('comments'),
				'status' => $this->input->post('verified_status'),
				'approvedby' => $this->loginData->UserID, 	
				'approvedon'  => date('Y-m-d H:i:s'),

			);

		$this->db->where('candidateid', $token);
		$this->db->update('tbl_verification_document', $verificationarraydata);


			if ($currentstatus ==0) {
					$gerenalarraydata = array(

					'status' => 0,
				);
				$this->db->where('candidateid', $token);
				$this->db->update('tbl_general_nomination_and_authorisation_form', $gerenalarraydata);


				$joiningreportarraydata = array(

					'status' => 0,
				);
				$this->db->where('candidateid', $token);
				$this->db->update('tbl_joining_report', $joiningreportarraydata);


				$registrationarraydata = array(

					'BDFFormStatus' => 99,
				);
				$this->db->where('candidateid', $candidateid);
				$this->db->update('tbl_candidate_registration', $registrationarraydata);
					

			}
        }

		$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
		$this->db->delete('tbl_work_experience_verified', array('candidateid' => $token)); 

			for ($i=0; $i < $countworkexpverified; $i++) { 

				$insertArraydata = array(
					'candidateid'  => $token,
					'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
				);

				$this->db->insert('tbl_work_experience_verified', $insertArraydata);
			}

			
			
			$countdocumentname = count($this->input->post('documentname'));			
			$this->db->delete('tbl_other_documents', array('candidateid' => $token)); 

			for ($i=0; $i < $countdocumentname; $i++) { 

		if (!empty($this->input->post('documentname')[$i]) && isset($this->input->post('documentname')[$i])) {		

		   	if ($_FILES['otherdocumentupload']['name'][$i] != NULL) {

		   		@  $ext = end(explode(".", $_FILES['otherdocumentupload']['name'][$i])); 

					if($_FILES['otherdocumentupload']['size'] < 10485760) {
						$this->session->set_flashdata('er_msg', "filesize too large. Please upload file of size-2MB");
						redirect(current_url());
					}

		   		 $OriginaldocumentName = $_FILES['otherdocumentupload']['name'][$i]; 
		   		 $encrypteddocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		   		$tempFile = $_FILES['otherdocumentupload']['tmp_name'][$i];
		   	    $targetPath = FCPATH . "otherdocuments/";
		   		$targetFile = $targetPath . $encrypteddocumentName;
		   		@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		   		if($uploadResult == true){

		   			$encrypteddocument = $encrypteddocumentName;

		   		}else{

		   			die("Error uploading Other Document !!!");
		   			$this->session->set_flashdata("er_msg", "Error uploading Other Document ");
		   			redirect(current_url());
		   		}

		   	}
		   

				$insertArraydata = array(
					'candidateid'  => $token,
					'documentname'=> $this->input->post('documentname')[$i],
					'documenttype'=> $this->input->post('documenttype')[$i],
					'documentupload'=> $encrypteddocument,
					'createdon'=> date('Y-m-d H:i:s'),
					'createdby'=> $this->loginData->UserID,
				);

				$this->db->insert('tbl_other_documents', $insertArraydata);
			}
		}

			$this->db->trans_complete();


		 //// Send Mail To HRD //////////////////////////////   
			$this->loginData->EmailID;
	     $hrdemailid    = $this->loginData->EmailID;
	    // $tcdemailid    = $this->loginData->EmailID;
	     $candidateemailid    = $candidateemailid; 

		 $subject = "Verified document";
		  $body = 	'Dear Sir, <br><br> ';
		  $body .= $this->input->post('comments');
		 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
		 $to_email = $hrdemailid;
		 $to_name =  $candidateemailid;
		
		$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! Not update DA document verification !!!');	
			}else{

				$this->session->set_flashdata('tr_msg', 'Successfully update DA document verification !!!');	
				redirect('Joining_of_da_document_verification/');		
			}
		

		}
	}


	$content['token'] = $token; 

	$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

	$content['getcandateverified'] = $this->model->getCandateVerified($token);

	$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);

	$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);

	$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);
	$content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);



	$content['getdocuments'] = $this->model->getOtherDocuments();

	$content['title'] = 'Joining_of_da_document_verification';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
	}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
}



public function edit($token)
{
	
	try
		{

		$staff_id=$this->loginData->staffid;
		
		$content['staff_details']=$this->Stafftransferpromotion_model->get_staffDetails($staff_id);
		//print_r($content['staff_details']);
		
		$content['exp_details']=$this->Transfer_claim_expense_model->getexpense_claimdetail($token);
		

		//die();		
		

		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST")
		{	$submitdatasend = $this->input->post('submitbtn');
			$Sendsavebtn = $this->input->post('savetbtn');

      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
			

			$id=$this->input->post('exp_id');
			$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
				$v_con_total=$this->input->post('v_con_total');
				$v_con_total=$this->input->post('v_con_total');
				$l_allowance=$this->input->post('l_allowance');
				$grand_total=$this->input->post('grand_total');
				$ta_advance=$this->input->post('ta_advance');
				$net_amount_payable=$this->input->post('payable_amount');

				$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp,
					'local_conveyance'=>$local_conveyance,
					'conveyance_of_personal_train'=>$train_claim,
					'conveyance_of_personal_truck'=>$truck_con_claim,
					'conveyance_of_personal_other'=>$any_other_claim,
						'local_conveyance'=>$total,
						'conveyance_of_vehicle_car'=>$car_claim,
						'conveyance_of_vehicle_motor'=>$motor_cycle_claim,
						'conveyance_of_vehicle_bicycle'=>$bicycle_claim,
						'total_vehicle_con'=>$v_con_total,
						'lumpsum_transfer_allowance'=>$l_allowance,
						'less_amount_of_transfer_ta_advance'=>$ta_advance,
						'grand_total'=>$grand_total,
						'net_amount_payable'=>$net_amount_payable,
						'tdate'=>$date,
						'flag'=>0


					);
								
				 $this->db->update('tbl_transfer_expenses_claim', $insertArraydata);
				$this->db->where('id',$id);
				
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Join');
					redirect('Transfer_claim_expense/edit/'.$staff_id);			
				}

			}else{
				$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}

	


			
		}
		$submitdatasend = $this->input->post('submitbtn');
		if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				//echo "hgjuy";
				$id=$this->input->post('exp_id');
				$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
				$v_con_total=$this->input->post('v_con_total');
				$v_con_total=$this->input->post('v_con_total');
				$l_allowance=$this->input->post('l_allowance');
				$grand_total=$this->input->post('grand_total');
				$ta_advance=$this->input->post('ta_advance');
				$net_amount_payable=$this->input->post('payable_amount');

				$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp,
					'local_conveyance'=>$local_conveyance,
					'conveyance_of_personal_train'=>$train_claim,
					'conveyance_of_personal_truck'=>$truck_con_claim,
					'conveyance_of_personal_other'=>$any_other_claim,
						'local_conveyance'=>$total,
						'conveyance_of_vehicle_car'=>$car_claim,
						'conveyance_of_vehicle_motor'=>$motor_cycle_claim,
						'conveyance_of_vehicle_bicycle'=>$bicycle_claim,
						'total_vehicle_con'=>$v_con_total,
						'lumpsum_transfer_allowance'=>$l_allowance,
						'less_amount_of_transfer_ta_advance'=>$ta_advance,
						'grand_total'=>$grand_total,
						'net_amount_payable'=>$net_amount_payable,
						'tdate'=>$date,
						'flag'=>1


					);
				
				  $this->db->update('tbl_transfer_expenses_claim', $insertArraydata);
				$this->db->where('id',$id);


				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Join');
					redirect('Transfer_claim_expense/view/'.$staff_id);			
				}



				
			}
	

				

	

			
		$content['title'] = 'Transfer_claim_expense/edit';
		 

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {

		print_r($e->getMessage());die;
	}
}




public function view($token)
{

	try
		{

		$staff_id=$this->loginData->staffid;
		
			$content['staff_details']=$this->Stafftransferpromotion_model->get_staffDetails($staff_id);
		//print_r($content['staff_details']);
		
		$content['exp_details']=$this->Transfer_claim_expense_model->getexpense_claimdetail($token);
		

		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST")
		{	$submitdatasend = $this->input->post('submitbtn');
			$Sendsavebtn = $this->input->post('savetbtn');

      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
			print_r($_POST);

			$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
				$v_con_total=$this->input->post('v_con_total');
				$v_con_total=$this->input->post('v_con_total');
				$l_allowance=$this->input->post('l_allowance');
				$grand_total=$this->input->post('grand_total');
				$ta_advance=$this->input->post('ta_advance');
				$net_amount_payable=$this->input->post('payable_amount');

				$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp,
					'local_conveyance'=>$local_conveyance,
					'conveyance_of_personal_train'=>$train_claim,
					'conveyance_of_personal_truck'=>$truck_con_claim,
					'conveyance_of_personal_other'=>$any_other_claim,
						'local_conveyance'=>$total,
						'conveyance_of_vehicle_car'=>$car_claim,
						'conveyance_of_vehicle_motor'=>$motor_cycle_claim,
						'conveyance_of_vehicle_bicycle'=>$bicycle_claim,
						'total_vehicle_con'=>$v_con_total,
						'lumpsum_transfer_allowance'=>$l_allowance,
						'less_amount_of_transfer_ta_advance'=>$ta_advance,
						'grand_total'=>$grand_total,
						'net_amount_payable'=>$net_amount_payable,
						'tdate'=>$date,
						'flag'=>0


					);
				echo "<pre>";
				print_r($insertArraydata);
				//die();
				 $this->db->insert('tbl_transfer_expenses_claim', $insertArraydata);
				//$this->db->insert($insertArraydata);
				//echo $this->db->last_query(); die();
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Join');
					redirect('Transfer_claim_expense/edit'.$staff_id);			
				}

			}else{
				$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}

	


			
		}
		$submitdatasend = $this->input->post('submitbtn');
		if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				echo "hgjuy";
				$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
				$v_con_total=$this->input->post('v_con_total');
				$v_con_total=$this->input->post('v_con_total');
				$l_allowance=$this->input->post('l_allowance');
				$grand_total=$this->input->post('grand_total');
				$ta_advance=$this->input->post('ta_advance');
				$net_amount_payable=$this->input->post('payable_amount');

				$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp,
					'local_conveyance'=>$local_conveyance,
					'conveyance_of_personal_train'=>$train_claim,
					'conveyance_of_personal_truck'=>$truck_con_claim,
					'conveyance_of_personal_other'=>$any_other_claim,
						'local_conveyance'=>$total,
						'conveyance_of_vehicle_car'=>$car_claim,
						'conveyance_of_vehicle_motor'=>$motor_cycle_claim,
						'conveyance_of_vehicle_bicycle'=>$bicycle_claim,
						'total_vehicle_con'=>$v_con_total,
						'lumpsum_transfer_allowance'=>$l_allowance,
						'less_amount_of_transfer_ta_advance'=>$ta_advance,
						'grand_total'=>$grand_total,
						'net_amount_payable'=>$net_amount_payable,
						'tdate'=>$date,
						'flag'=>1


					);
				//echo "<pre>";
				//print_r($insertArraydata);
				//die();
				 $this->db->insert('tbl_transfer_expenses_claim', $insertArraydata);
				//$this->db->insert($insertArraydata);
				//echo $this->db->last_query(); 

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Join');
					redirect('Transfer_claim_expense/view'.$staff_id);			
				}



				//die();
			}
	

				

		$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($this->loginData->teamid);

			
		$content['title'] = 'Transfer_claim_expense/index';
		 //die("hghgh");

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {

		print_r($e->getMessage());die;
	}
}







public function generalnominationform($token=null){

	try{

		//echo $token; 
		$apprivedby = "";
		$apprivedby = $this->loginData->UserFirstName.' '. $this->loginData->UserMiddleName.' '. $this->loginData->UserLastName;
	
		$generalnominationform = $this->model->getGeneral_Nomination_Form_Detail1($token);	

		//print_r($generalnominationform);die;

	//print_r($generalnominationform); die;
		$firstwitnessname  = $this->model->getSingleNameWitnessDeclaration($generalnominationform->first_witness_name);
		$secondwitnessname  = $this->model->getSingleNameWitnessDeclaration($generalnominationform->second_witness_name);
		


		$generalformid = $generalnominationform->id;
		$nomineedetails = $this->model->getNomieeDetail($generalformid);

		//print_r($nomineedetails); die;
		
		$html = '<table  width="100%" >
		<tr>
		<td> </td>
		<td></td>
		<td valign="top" ></td>
		</tr>
		<tr>
		<td colspan="3"</td>

		</tr>
		<tr>
		<td colspan="3" align="center">Professional Assistance for Development Action (PRADAN)</td>
		</tr>
		<tr>
		<td colspan="3" align="center">GENERAL NOMINATION AND AUTHORISATION FORM</td>
		</tr>
		<tr>
		<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
		<td colspan="3" align="center">&nbsp;</td>
		</tr>
		
		
		<tr>
		<td align="justify">The Executive Director, </td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify">PRADAN. </td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify">'. $generalnominationform->executive_director_place.'</td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify"></td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td colspan="3" align="justify">
		Sir,
		</td>

		</tr>
		<tr>
		<td colspan="3" align="justify">
		<b> Subject:  General Nomination and Authorization.</b>
		</td>

		</tr>
		<tr>
		<td colspan="3" align="justify">I, '. $generalnominationform->candidatefirstname.' '.$generalnominationform->candidatemiddlename.' '.$generalnominationform->candidatelastname.'(name in full) whose particulars are given below, hereby nominate the following person(s) to receive, in the event of my death, all monies payable to me by Professional Assistance for Development Action (PRADAN) or its successors or assignees, other than the monies on those accounts in respect of which specific nominations exist or are required to exist under the rules governing the relevant matters, and want that the monies first aforesaid shall be paid in proportion indicated against the name(s) of the nominee(s). </td>

		</tr>
		<tr>
		<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
		<td  colspan="3">
		<table width="100%" border="2">
		<tr>
		<td>S.No </td>
		<td>Name of the Nominee in Full</td>
		<td>Nominees Relationship with the Apprentice</td>
		<td>Age of Nominee</td>
		<td>Full Address of the Nominee</td>
		</tr>
		<tr>
		<td>1</td>
		<td>'.$nomineedetails[0]->nominee_name.'</td>
		<td>'.$nomineedetails[0]->relationname.'</td>
		<td>'.$nomineedetails[0]->nominee_age.'</td>
		<td>'.$nomineedetails[0]->nominee_address.'</td>
		</tr>
		<tr>
		<td>2</td>
		<td>'.$nomineedetails[1]->nominee_name.'</td>
		<td>'.$nomineedetails[1]->relationname.'</td>
		<td>'.$nomineedetails[1]->nominee_age.'</td>
		<td>'.$nomineedetails[1]->nominee_address.'</td>
		</tr>
		</table></td>

		</tr>
		<tr>
		<td colspan="3" align="justify">This nomination and authorisation shall remain valid unless substituted by a fresh nomination and authorisation.</td>
		</tr>
		<tr>
		<td colspan="3" align="center"></td>
		</tr>
		<tr>
		<td align="center">Submited By</td>
		<td align="center"></td>
		<td align="center">Approved By</td>
		<td align="center"></td>
		</tr>
		<tr>
		<td align="center">'.$generalnominationform->candidatefirstname.' '.$generalnominationform->candidatemiddlename.' '.$generalnominationform->candidatelastname .'</td>
		<td align="center"></td>
		<td align="center"> '. $apprivedby .' </td>
		<td align="center"></td>
		</tr>

		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		</tr>
		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		</tr>
		<tr>
		<td colspan="3" align="center"></td>
		</tr>
		<tr>
		<td colspan="3" align="center"><b> Declaration by Witness </b></td>
		</tr>
		<tr>
		<td colspan="3" align="center"> (Persons other than Nominees) </td>
		</tr>
		<tr>
		<td colspan="3" align="justify">The above nomination and authorisation has been signed by Mr. /Ms. '.$generalnominationform->nomination_authorisation_signed.'  in our presence: </td>
		</tr>
		<tr>
		<td colspan="3"> 
		<table width="80%">
		<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		</tr>
		<tr>
		<td></td>
		<td>1</td>
		<td align="center">Name : </td>
		<td align="left">'. $firstwitnessname->name .'</td>
		<td></td>
		</tr>
		<tr>
		<td></td>
		<td></td>
		<td align="center">Address</td>
		<td align="left">'.$generalnominationform->first_witness_address.'</td>
		<td></td>
		</tr>
		<tr>
		<td></td>
		<td>2</td>
		<td align="center">Name : </td>
		<td align="left">'. $secondwitnessname->name .'</td>
		<td></td>
		</tr>
		<tr>
		<td></td>
		<td></td>
		<td align="center">Address : </td>
		<td align="left">'.$generalnominationform->second_witness_address.'</td>
		<td></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>';
//echo $html; die;

		$filename = $token.'-'.md5(time() . rand(1,1000)).'.pdf';

		$updateArr = array(
			'filename'    		    => $filename,
		);
		$this->db->where('status', 2);
		$this->db->where('candidateid', $token);
		$this->db->update('tbl_general_nomination_and_authorisation_form', $updateArr);

		$this->load->model('Dompdf_model');
		$this->Dompdf_model->GeneralNominationformPDF($html,  $filename,NULL,'GeneralNominationform.pdf');

		$this->session->set_flashdata('tr_msg', 'Successfully Generate General Nomination Form');

		

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function JoiningReportForm($token){

	try{

		$apprivedby = "";
		$apprivedby = $this->loginData->UserFirstName.' '. $this->loginData->UserMiddleName.' '. $this->loginData->UserLastName;

		$candidatedetils = $this->model->getCandidateDetails($token);
		$getsubmitedby = $this->model->getSubmittedBy($token);
		$JoiningReport = $this->model->getJoiningReportDetail($token);
		$joiningreportid = $JoiningReport->id;
	

		$joiningreportdetails = $this->model->getJoiningReportDetail($token);
	// echo "<pre>";
	// print_r($joiningreportdetails); die;

		$html = '<table  width="100%" >
		<tr>
		<td> </td>
		<td></td>
		<td valign="top" ></td>
		</tr>
		<tr>
		<td colspan="3"</td>

		</tr>
		<tr>
		<td colspan="3" align="center"><b>PRADAN’s APPRENTICESHIP PROGRAMME FOR RURAL DEVELOPMENT</b></td>
		</tr>
		<tr>
		<td colspan="3" align="center"><b>JOINING REPORT </b></td>
		</tr>

		<tr>
		<td align="justify">Address: </td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify">'.$candidatedetils->permanentstreet.'</td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify">'.$candidatedetils->permanentcity.' '. $candidatedetils->permanentdistrict.'</td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify">'.$candidatedetils->name.', '.$candidatedetils->permanentpincode.'</td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td align="justify">To,</td>
		<td align="justify"> </td>
		<td align="justify"></td>
		</tr>
		<tr>
		<td colspan="3" align="justify">The Executive Director, <br> PRADAN</td>
		</tr>
		<tr>
		<td colspan="3" align="justify">E 1/A, Ground Floor and Basement <br>
		Kailash Colony <br>
		New Delhi - 110 048 <br>
		Tel: 4040 7700</td>
		</tr>
		<tr>
		<td colspan="3" align="center">
		<b>Subject: Joining Report</b> 
		</td>
		</tr>
		<tr>
		<td colspan="3" align="center">

		</td>
		</tr>
		<tr>
		<td colspan="3" align="center">

		</td>
		</tr>

		<tr>
		<td colspan="3" align="justify">Please refer to your offer no. '. $candidatedetils->offerno .'   dated &nbsp; <b>'. $this->model->changedatedbformate($candidatedetils->doj)  .'</b>&nbsp; offering me apprenticeship at '. $joiningreportdetails->apprenticeship_at_location .'.</td>    
		</tr>
		<tr>
		<td colspan="3" align="justify"></td>    
		</tr>
		<tr>
		<td colspan="3" align="justify">I reported at PRADAN’s office at '. $joiningreportdetails->office_at_location .' and have joined the programme in the  forenoon of today, the '. $this->model->changedatedbformate($joiningreportdetails->join_programme_date) .' (date/month/year).</td>    
		</tr>
		<tr>
		<td colspan="3" align="justify"></td>    
		</tr>
		<tr>
		<td colspan="3" align="justify">I shall inform you of any change in my address, given above, when it occurs.
		</td>    
		</tr>
		<tr>
		<td colspan="3" align="center"></td>
		</tr>
		<tr>
		<td align="center">Submited By</td>
		<td align="center"></td>
		<td align="center">Approved By</td>
		<td align="center"></td>
		</tr>
		<tr>
		<td align="center">'.$getsubmitedby->candidatefirstname.' '.$getsubmitedby->candidatemiddlename.' '.$getsubmitedby->candidatelastname  .'</td>
		<td align="center"></td>
		<td align="center">'.$apprivedby.'</td>
		<td align="center"></td>
		</tr>

		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		</tr>
		<tr>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		</tr>
		<tr>
		<td colspan="3" align="left"></td>
		</tr>
		<tr>
		<td colspan="3" align="left"></td>
		</tr>
		</table>';

		$filename = $token.'-'.md5(time() . rand(1,1000)).'.pdf';
		$updateArr = array(
			'filename'    		    => $filename,
		);
		$this->db->where('status', 2);
		$this->db->where('id', $joiningreportid);
		$this->db->update('tbl_joining_report', $updateArr);

		$this->load->model('Dompdf_model');
		$this->Dompdf_model->joiningreportfordaPDF($html, $filename, NULL,'Generateofferletter.pdf');

		$this->session->set_flashdata('tr_msg', 'Successfully Generate Joining Report Form');


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


	public function transfer($token)
	{
		try
		{
			if($this->loginData->RoleID == 2){
			$content['gotolist'] = 'Staff_approval';
		}else{
			$content['gotolist'] = 'Staff_personel_records';
		}
		
		$staff_id=$this->loginData->staffid;
		$content['token'] = $token;
		$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag, staffid FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->result()[0];
	    $content['workflow_flag'] = $result; 
	    $forwardworkflowid = $result->workflowid;
		//$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		$content['staff_details']=$this->Stafftransferpromotion_model->get_staffDetails($result->staffid);
		// echo "<pre>";
		// print_r($content['staff_details']);

		$content['finance_detail'] = $this->Common_Model->get_staff_finance_detail($content['staff_details']->new_office_id,20);
		$content['supervisor_detail'] = $this->Transfer_claim_expense_model->get_newsupervisor_detail($content['staff_details']->reportingto);
		
		
		
		$content['transfer_claim_expense_detail'] = $this->Transfer_claim_expense_model->transfer_claim_expense_detail($content['staff_details']->staffid);
		
		/*echo "<pre>";
		print_r($content['transfer_claim_expense_detail']);exit();*/

		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST")
		{	
			$submitdatasend = $this->input->post('submitbtn');
			$Sendsavebtn = $this->input->post('savetbtn');

      if ($this->input->post('comments')) {
      		/*echo "<pre>";
			print_r($_FILES);die();*/
			$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
			$v_con_total=$this->input->post('v_con_total');
			$l_allowance=$this->input->post('l_allowance');
			$grand_total=$this->input->post('grand_total');
			$ta_advance=$this->input->post('ta_advance');
			$net_amount_payable=$this->input->post('payable_amount');

			$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
			$zero = 0;
			$trvexproriginalName = '';
			$trvexprencryptedName = '';
			if (isset($_FILES['attach_travel_exp']['name'])) {

				if($_FILES['attach_travel_exp']['name'] !=NULL){
					@  $ext = end((explode(".", $_FILES['attach_travel_exp']['name'])));

					$trvexproriginalName = $_FILES['attach_travel_exp']['name']; 
					$trvexprencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)).".".$ext;
					$tempFile = $_FILES['attach_travel_exp']['tmp_name'];
					$targetPath = FCPATH . "datafiles/educationalcertificate/";
					$targetFile = $targetPath . $trvexprencryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$trvexprencryptedName = $trvexprencryptedName;
					}else{

						die("Error uploading pg Certificate Document ");
						$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
						redirect(current_url());
					}
				}else{
					$trvexproriginalName = '';
					$trvexprencryptedName = '';
				}

			}
			else{
				$trvexproriginalName = '';
				$trvexprencryptedName = '';
			}
			/*echo "<pre>";
			print_r($trvexprencryptedName);die();*/
			$lclconoriginalName = '';
			$lclconcryptedName = '';
			if (isset($_FILES['attach_local_conveyance']['name'])) {
				if($_FILES['attach_local_conveyance']['name'] !=NULL){
					@  $ext = end((explode(".", $_FILES['attach_local_conveyance']['name'])));

					$lclconoriginalName = $_FILES['attach_local_conveyance']['name']; 
					$lclconcryptedName = md5(date("Y-m-d H:i:s").rand(1,100)).".".$ext;
					$tempFile = $_FILES['attach_local_conveyance']['tmp_name'];
					$targetPath = FCPATH . "datafiles/educationalcertificate/";
					$targetFile = $targetPath . $lclconcryptedName;
					$uploadResult = move_uploaded_file($tempFile,$targetFile);

					if($uploadResult == true){
						$lclconcryptedName = $lclconcryptedName;
					}else{

						die("Error uploading pg Certificate Document ");
						$this->session->set_flashdata("er_msg", "Error uploading pg Certificate Document");
						redirect(current_url());
					}
				}else{
					$lclconoriginalName = '';
					$lclconcryptedName = '';
				}

			}
			else{
				$lclconoriginalName = '';
				$lclconcryptedName = '';
			}
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp ? $travel_exp : $zero,
					'local_conveyance'=>$local_conveyance ? $local_conveyance : $zero,
					'conveyance_of_personal_train'=>$train_claim ? $train_claim : $zero,
					'conveyance_of_personal_truck'=>$truck_con_claim ? $truck_con_claim : $zero,
					'conveyance_of_personal_other'=>$any_other_claim ? $any_other_claim : $zero,
					'total'=>$total ? $total : $zero,
					'conveyance_of_vehicle_car'=>$car_claim ? $car_claim : $zero,
					'conveyance_of_vehicle_motor'=>$motor_cycle_claim ? $motor_cycle_claim : $zero,
					'conveyance_of_vehicle_bicycle'=>$bicycle_claim ? $bicycle_claim : $zero,
					'total_vehicle_con'=>$v_con_total ? $v_con_total : $zero,
					'lumpsum_transfer_allowance'=>$l_allowance ? $l_allowance : $zero,
					'less_amount_of_transfer_ta_advance'=>$ta_advance ? $ta_advance : $zero,
					'grand_total'=>$grand_total ? $grand_total : $zero,
					'net_amount_payable'=>$net_amount_payable ? $net_amount_payable : $zero,
						'flag'=>1,
					'attach_travel_exp' => $trvexprencryptedName,
					'attach_local_conveyance' => $lclconcryptedName
					);
				/*echo "<pre>";
				print_r($insertArraydata);die();*/
				 $this->db->insert('tbl_transfer_expenses_claim', $insertArraydata);
				
				$updateArr = array(                         
          'trans_flag'     => 17,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 6,
         'staffid'              => $content['staff_details']->staffid,
         'sender'               => $this->loginData->staffid,
         'receiver'             => $content['staff_details']->reportingto,
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => 17,
         'scomments'            => $this->input->post('comments'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate save  expense calim details');
					redirect('Transfer_claim_expense/transfer/'.$token);			
				}

			}else{
				$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}
	
		}
		/*$submitdatasend = $this->input->post('submitbtn');
		if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				echo "hgjuy";
				$date_release=$this->input->post('date_release');
			$location=$this->input->post('location');
			$travel_exp=$this->input->post('travel_exp');
			$local_conveyance=$this->input->post('local_conveyance');
			$train_claim=$this->input->post('train_claim');
			$truck_con_claim=$this->input->post('truck_con_claim');
			$any_other_claim=$this->input->post('any_other_claim');
			
			$total=$this->input->post('total');	
			$car_claim=$this->input->post('car_claim');
			$motor_cycle_claim=$this->input->post('motor_cycle_claim');	
			$bicycle_claim=$this->input->post('bicycle_claim');	
				$v_con_total=$this->input->post('v_con_total');
				$v_con_total=$this->input->post('v_con_total');
				$l_allowance=$this->input->post('l_allowance');
				$grand_total=$this->input->post('grand_total');
				$ta_advance=$this->input->post('ta_advance');
				$net_amount_payable=$this->input->post('payable_amount');

				$date=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('date'));
				$insertArraydata=array(

					'staffid'=>$staff_id,

					'travel_fare'=>$travel_exp,
					'local_conveyance'=>$local_conveyance,
					'conveyance_of_personal_train'=>$train_claim,
					'conveyance_of_personal_truck'=>$truck_con_claim,
					'conveyance_of_personal_other'=>$any_other_claim,
						'local_conveyance'=>$total,
						'conveyance_of_vehicle_car'=>$car_claim,
						'conveyance_of_vehicle_motor'=>$motor_cycle_claim,
						'conveyance_of_vehicle_bicycle'=>$bicycle_claim,
						'total_vehicle_con'=>$v_con_total,
						'lumpsum_transfer_allowance'=>$l_allowance,
						'less_amount_of_transfer_ta_advance'=>$ta_advance,
						'grand_total'=>$grand_total,
						'net_amount_payable'=>$net_amount_payable,
						'tdate'=>$date,
						'flag'=>1


					);
				//echo "<pre>";
				//print_r($insertArraydata);
				//die();
				 $this->db->insert('tbl_transfer_expenses_claim', $insertArraydata);
				//$this->db->insert($insertArraydata);
				//echo $this->db->last_query(); 

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate save and submit  expense calim details');
					redirect('Transfer_claim_expense/view/'.$staff_id);			
				}



				//die();
			}*/
	

				

		//$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($this->loginData->teamid);

			
		$content['title'] = 'Transfer_claim_expense/transfer';
		 //die("hghgh");

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {

		print_r($e->getMessage());die;
	}

	}





}