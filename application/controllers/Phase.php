<?php 

/**
* Phase List
*/
class Phase extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Phase_model","model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }
    $this->loginData = $this->session->userdata('login_data');

    
  }

  public function index()
  {
     
    // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

     $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      if ($this->input->post('btnsend')=='send_mail') {

      }
      }

    $query = "select * from mstphase where isdeleted='0'";
    $content['phase_details'] = $this->Common_Model->query_data($query);
    $content['subview']="index";

    $content['title'] = 'Phase';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add()
  {

        // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
    // print_r($this->input->post()); //die();
     $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      
        $this->form_validation->set_rules('phasename','Phase Name','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
        'phase_name'   => $this->input->post('phasename'),
        'created_date' => date('Y-m-d H:i:s'),
        'isdeleted'    => $this->input->post('status'),
        );
      
      $this->db->insert('mstphase', $insertArr);
      $this->session->set_flashdata('tr_msg', 'Successfully added Phase');
      redirect('/Phase/');
    }

    prepareview:

    $content['title']   = 'Phase';
    $content['subview'] = 'Phase/add';
    $this->load->view('_main_layout', $content);
  }

  public function edit($id=NULL)
  {
        // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      
        $this->form_validation->set_rules('phasename','Phase Name','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

      $updateArr = array(
        'phase_name'   => $this->input->post('phasename'),
        'isdeleted'    => $this->input->post('status'),
        );
      
      $this->Common_Model->update_data('mstphase', $updateArr,'id',$id);
      $this->session->set_flashdata('tr_msg', 'Successfully Updated Phase');
       redirect('/Phase/');
    }

    prepareview:

    $content['title'] = 'Phase';
    $phase_details = $this->Common_Model->get_data('mstphase', '*', 'id',$id);
    $content['phase_details'] = $phase_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }


     public function sendmail($id=NULL)
  {
        // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
    if($id == 1)
    {
      // $message ='';
      $subject = "Joining of <batch> of Apprentices";
      // $fd = fopen("mailtext/7 DAY ORIENTATION.txt", "r"); 
      // $message .=fread($fd,4096);
      // eval ("\$message = \"$message\";");
      // $body = nl2br($message);
      // $body = preg_replace("/<br\W*?\/>/", "\n", $body);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 47 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
        if(!empty($data))
          $body = $data->lettercontent;
    }
    elseif ($id == 2) {
      // $message ='';
      $subject = "Village Stay component";
      // $fd = fopen("mailtext/VILLAGE STAY.txt", "r"); 
      // $message .=fread($fd,4096);
      // eval ("\$message = \"$message\";");
      // $body = nl2br($message);
      // $body = preg_replace("/<br\W*?\/>/", "\n", $body);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 48 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
        if(!empty($data))
          $body = $data->lettercontent;
    }
    elseif ($id == 3) {
      // $message ='';
      $subject = "Village Study Component";
      // $fd = fopen("mailtext/VILLAGE STUDY.txt", "r"); 
      // $message .=fread($fd,4096);
      // eval ("\$message = \"$message\";");
      // $body = nl2br($message);
      // $body = preg_replace("/<br\W*?\/>/", "\n", $body);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 49 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
        if(!empty($data))
          $body = $data->lettercontent;
    }
    elseif ($id == 4) {
      // $message ='';
      $subject = "DC Event for Reflection and Consolidation of Immersion Phase";
      // $fd = fopen("mailtext/DC EVENT FOR REFLECTION AND CONSOLIDATION OF IMMERSION.txt", "r"); 
      // $message .=fread($fd,4096);
      // eval ("\$message = \"$message\";");
      // $body = nl2br($message);
      // $body = preg_replace("/<br\W*?\/>/", "\n", $body);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 50 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
        if(!empty($data))
          $body = $data->lettercontent;
    }
     

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $message = $this->input->post('summernote');
      $subject = $this->input->post('subject'); 


    //   if($id == 1)
    // {
    //   $message='';
    //   $subject = "Joining of <batch> of Apprentices";
    //   $fd = fopen("mailtext/7 DAY ORIENTATION.txt", "r");  
    // }
    // elseif ($id == 2) {
    //   $message='';
    //   $subject = "Village Stay component";
    //   $fd = fopen("mailtext/VILLAGE STAY.txt", "r"); 
    // }
    // elseif ($id == 3) {
    //   $message='';
    //   $subject = "Village Study Component";
    //   $fd = fopen("mailtext/VILLAGE STUDY.txt", "r");  
    // }
    // elseif ($id == 4) {
    //   $message='';
    //   $subject = "DC Event for Reflection and Consolidation of Immersion Phase";
    //   $fd = fopen("mailtext/DC EVENT FOR REFLECTION AND CONSOLIDATION OF IMMERSION.txt", "r"); 
    // }
        $this->form_validation->set_rules('subject','Subject','trim|required');
        $this->form_validation->set_rules('summernote','Message','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }
        
     $AllTcemailid = $this->model->getAllSuprvsn();

        $recipients1 = array();
        foreach($AllTcemailid as  $key=>$value) {
          $email=$value->emailid;
          $name=$value->name;

          $recipients2 = array(
          $value->emailid => $name);
          $recipients1 = array_merge($recipients1,$recipients2);
        }
        $message =nl2br($message);

      $sendmail = $this->Common_Model->send_email($subject, $message,$recipients1);

      
      // $this->Common_Model->update_data('mstphase', $updateArr,'id',$id);
      $this->session->set_flashdata('tr_msg', 'Successfully Send Phase Event mail');
       redirect('/Phase/');
    }

    prepareview:

    $content['title'] = 'Phase';
    $phase_details = $this->Common_Model->get_data('mstphase', '*', 'id',$id);
    $content['phase_details'] = $phase_details;
    $content['body'] = $body;
    $content['subject'] = $subject; 
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }


    function delete($token = null)
    {
      $this->Common_Model->delete_row('mstphase','id', $token); 
      $this->session->set_flashdata('tr_msg' ,"Phase Deleted Successfully");
      redirect('/phase/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
  }

}