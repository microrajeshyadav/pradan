<?php 
class Hrd_document_checklist extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		
		$this->load->model('Hrd_document_checklist_model');

		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			$candidatename = $this->input->post('candidatename'); 


			$this->db->where('joinstatus', 1);
			$this->db->where('candidateid', $candidatename);
			$query = $this->db->get('tbl_candidate_registration');

			if ($query->num_rows() == 0){

				$joinarraydata = array(

					'joinstatus'  => 1,
					'tc_hrd_document_verfied'=>1
				);

				$this->db->where('candidateid', $candidatename);
				$this->db->update('tbl_candidate_registration', $joinarraydata);
		    //echo $this->db->last_query(); die;
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! Candidate Not Join Please try again');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Join');			
				}

			}else{
				$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}
		}



		$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit();
		/*echo "<pre>";
		print_r($content['candidatebdfformsubmit']);
		exit();*/
		$content['title'] = 'Hrd_document_checklist';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




	public function verification($token)
	{
		try{

		$this->load->model('Hrd_document_checklist_model');
		

		$DevelopmentApprenticeshipDetails = $this->model->getDevelopmentApprenticeship($token);
		//print_r($this->loginData);
		$hr_name=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
		//die;
		//print_r($DevelopmentApprenticeshipDetails);
		$candidateemailid = $DevelopmentApprenticeshipDetails->emailid;
		$candidatefirstname=$DevelopmentApprenticeshipDetails->candidatefirstname;

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 


		if($RequestMethod == "POST"){
			$this->db->trans_start();
			
			$savebtndatasend = $this->input->post('submitbtn');

			if (!empty($savebtndatasend) && $savebtndatasend =='senddatasubmit') {
			// 	echo "<pre>";
			// print_r($this->input->post()); 
				
				

				$candidateid  = $this->input->post('candidateid');
				
				$query = $this->db->query("SELECT * FROM `tbl_hrd_verification_document` WHERE `candidateid` = ".$candidateid);
           		// echo $query->num_rows();die;
				$query1 = $this->db->query("SELECT * FROM `tbl_hrd_verification_document` WHERE `candidateid` = ".$candidateid);

				$result = $query1->row();
				
				$currentstatus  = $this->input->post('Approve');
			// print_r($query->num_rows()); die;

				if ($query->num_rows() == 0){

             	// echo "pooja";
             	// die;

					$verificationarraydata = array(
						'candidateid'         => $token,
						'metric_certificate'  => $this->input->post('metric_certificate_verified'),
						'hsc_certificate'     => $this->input->post('hsc_certificate_verified'),
						'ug_certificate'      => $this->input->post('ug_certificate_verified'),
						'pg_certificate'      => $this->input->post('pg_certificate_verified'),
						'signed_offerletter' => $this->input->post('signed_certificate_verified'),
						'ug_migration_certificate' => $this->input->post('ug_migration_certificate_verified'),
						'pg_migration_certificate' => $this->input->post('pg_migration_certificate_verified'),
			//	'gapyear_certificate'      => $this->input->post('gapyear_certificate_verified'),
						'other_certificate'   => $this->input->post('other_certificate_verified'),
						'comment'             => $this->input->post('comments'),
						'status'              => $currentstatus,
						'createdby'           => $this->loginData->UserID, 	
						'createdon'           => date('Y-m-d H:i:s'),
						'approvedby'          => $this->loginData->UserID, 	
						'approvedon'          => date('Y-m-d H:i:s'),
					);
             	//print_r($verificationarraydata);
             	//die;
					$this->db->insert('tbl_hrd_verification_document', $verificationarraydata);


					$registrationarraydata = array(
						'hrddocumentcheckstatus' => $currentstatus,
					);
					$this->db->where('candidateid', $candidateid);
					$this->db->update('tbl_candidate_registration', $registrationarraydata);

					if ($currentstatus ==0) {
						
						$registrationarraydata = array(

							'BDFFormStatus' => 97,
						);
						$this->db->where('candidateid', $candidateid);
						$this->db->update('tbl_candidate_registration', $registrationarraydata);
					}

				}else{

					$verificationarraydata = array(
						'candidateid'         => $token,
						'metric_certificate'  => $this->input->post('metric_certificate_verified'),
						'hsc_certificate'     => $this->input->post('hsc_certificate_verified'),
						'ug_certificate'      => $this->input->post('ug_certificate_verified'),
						'pg_certificate'      => $this->input->post('pg_certificate_verified'),
						'signed_offerletter' => $this->input->post('signed_certificate_verified'),
						'ug_migration_certificate' => $this->input->post('ug_migration_certificate_verified'),
						'pg_migration_certificate' => $this->input->post('pg_migration_certificate_verified'),
				//'gapyear_certificate'      => $this->input->post('gapyear_certificate_verified'),
						'other_certificate'   => $this->input->post('other_certificate_verified'),
						'comment'             => $this->input->post('comments'),
						'status'              => $currentstatus,
						'approvedby'          => $this->loginData->UserID, 	
						'approvedon'          => date('Y-m-d H:i:s'),

					);

             	// print_r($verificationarraydata);
             	// die;
					$this->db->where('candidateid', $token);
					$this->db->update('tbl_hrd_verification_document', $verificationarraydata);
					
					$registrationarraydata = array(
						'hrddocumentcheckstatus' => $currentstatus,
					);
					$this->db->where('candidateid', $candidateid);
					$this->db->update('tbl_candidate_registration', $registrationarraydata);

					if ($currentstatus ==0) {
						
						$registrationarraydata = array(

							'BDFFormStatus' => 97,
						);
						$this->db->where('candidateid', $candidateid);
						$this->db->update('tbl_candidate_registration', $registrationarraydata);
						
					}else{
						$registrationarraydata = array(

							'BDFFormStatus' => 1,
						);
						$this->db->where('candidateid', $candidateid);
						$this->db->update('tbl_candidate_registration', $registrationarraydata);
					}

					
				}

				$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
				$this->db->delete('tbl_hrd_work_experience_verified', array('candidateid' => $token)); 

				for ($i=0; $i < $countworkexpverified; $i++) { 

					$insertArraydata = array(
						'candidateid'  => $token,
						'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
					);

					$this->db->insert('tbl_hrd_work_experience_verified', $insertArraydata);
				}


				$countgapverified =$this->input->post('gapyear_certificate');
				
				
				$this->db->delete('tbl_hrd_gap_year_verified', array('candidateid' => $token)); 

				for ($i=0; $i < $countgapverified; $i++) { 

					$insertArraygapdata = array(
						'candidateid'  => $token,
						'gapyear_verified'=> $this->input->post('gapyear_certificate_verified')[$i],
					);
					//print_r($insertArraygapdata);
					
					$this->db->insert('tbl_hrd_gap_year_verified', $insertArraygapdata);
				//  echo $this->db->last_query();
				// // die;
				}

				

				
				
				$this->db->trans_complete();

		 //// Send Mail To HRD //////////////////////////////  


				$this->loginData->EmailID;
				$hrdemailid    = $this->loginData->EmailID;
				$candidateemailid    = $candidateemailid; 
				$msg = '';
				if ($currentstatus==0) {

					$msg = 'Candidate documents should be re-upload !!!';

					$metric_certificate= $this->input->post('metric_certificate_verified');
					$hsc_certificate= $this->input->post('hsc_certificate_verified');
					$ug_certificate=$this->input->post('ug_certificate_verified');
					$pg_certificate= $this->input->post('pg_certificate_verified');
					$signed_offerletter = $this->input->post('signed_certificate_verified');
					$ug_migration_certificate=$this->input->post('ug_migration_certificate_verified');
					$pg_migration_certificate = $this->input->post('pg_migration_certificate_verified');
					$gapyear_certificate= $this->input->post('gapyear_certificate_verified');
					$other_certificate   = $this->input->post('other_certificate_verified');
					
					$actionstatus = "Not Approved";

					$subject = 'HRD '. $actionstatus .' documents';
					$subject = 'Acknowledgment of offer letter and document. <br>';
					$body = 	'Dear '.$candidatefirstname.', <br>';
					$body .= 	'I acknowledge the receipt of the signed copy of your offer letter and documents. I request you to kindly provide us with the self-attested scan copy of following documents.<br><br>';
		//$body.='1.'.$metric_certificate.' <br>';
					if($metric_certificate==2)
					{
						$body.= 'Matric Certificate<br>';
					}
					if($hsc_certificate==2)
					{
						$body.='HSC Marksheet Certificate<br>';	
					}
					if($ug_certificate==2)
					{
						$body.='Graduation Certificate<br>';	
					}
					if($pg_certificate==2)
					{
						$body.='Post Graduation Certificate<br>';	
					}
					if($signed_offerletter==2)
					{
						$body.='Signed Offerletter<br>';	
					}
					if($ug_migration_certificate==2)
					{
						$body.='Graduation Migration Certificate<br>';	
					}
					
					if($pg_migration_certificate==2)
					{
						$body.='Post Graduation Migration Certificate <br>';	
					}
					if($gapyear_certificate==2)
					{
						$body.='Gapyear Certificate <br>';	
					}
					if($other_certificate==2)
					{
						$body.='Other Certificate <br>';	
					}

		//$body .=   $this->input->post('comments');
					
					$body .= 	'We will get back to you in case we need any further clarification with regard to your documents.<br><br>';

					$body .= 	'Regards.<br>'.$hr_name.'<br>
					HRD Unit<br>
					PRADAN
					';

				}

				else{

					$actionstatus = "Approved";
					$msg = 'Candidate documents verified successfully. !!!';
					
					$subject = 'Acknowledgment of offer letter and document';
					$body = 	'Dear '.$candidatefirstname.', <br>';
					$body .= 	'I acknowledge the receipt of the signed copy of your offer letter and documents. I request you to kindly provide us with the self-attested scan copy .<br><br>';
		//$body .=   $this->input->post('comments');
					
					$body .= 	'We will get back to you in case we need any further clarification with regard to your documents.<br><br>';

					$body .= 	'Regards.<br>'.$hr_name.'<br>
					HRD Unit<br>
					PRADAN';
				}

		// echo $body;
		// die;
				
		 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				$to_email = $candidateemailid;
				$to_name =  $hrdemailid;
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error !!! Not candidate document verification !!!');	
				}else{

					$this->session->set_flashdata('tr_msg', ' '. $msg.' ');	
					redirect('Hrd_document_checklist/');		
				}
				

			}
		}


		$content['token'] = $token; 

		$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

		$content['getcandateverified'] = $this->model->getCandateVerified($token);

		$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);

		$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);
		$content['gapyearcount'] = $this->model->getCandateyapyearexpcount($token);
	// print_r($content['gapyearcount']);
	// die;
		
		$content['gapyeardetails'] = $this->model->gapyearcandidate_details($token);
	

		$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);
		$content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);



		$content['getdocuments'] = $this->model->getOtherDocuments();
		/*echo "<pre>";
		print_r($content);
		die;*/
		$content['title'] = 'Hrd_document_checklist';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function edit($token)
	{
		try{

		if (empty($token) || $token == '' ) {
			
			redirect('/Hrd_document_checklist/index');
			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
			
		}
		else { 

			$this->load->model('Hrd_document_checklist_model');


		//print_r($this->input->post()); die;

			$this->db->trans_start();

			$DevelopmentApprenticeshipDetails = $this->model->getDevelopmentApprenticeship($token);
			$candidateemailid = $DevelopmentApprenticeshipDetails->emailid;

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){

				$candidatename = $this->input->post('candidatename'); 

				$savebtnsave = $this->input->post('savebtn');

				if (!empty($savebtnsave) && $savebtnsave =='senddatasave') {

					$verificationarraydata = array(

						'candidateid'  => $token,
						'metric_certificate'  => $this->input->post('metric_certificate_verified'),
						'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
						'ug_certificate'  => $this->input->post('ug_certificate_verified'),
						'pg_certificate'  => $this->input->post('pg_certificate_verified'),
						'other_certificate'  => $this->input->post('other_certificate_verified'),
						'general_nomination_form'  => $this->input->post('general_nomination_form'),
						'joining_report_da'  => $this->input->post('joining_report_da'),
						'comment'  => $this->input->post('comments'),
						'status' => $this->input->post('Approve'),
						'createdon'  => date('Y-m-d H:i:s'),
						'createdby'  => $this->loginData->UserID,
					);

					$this->db->where('candidateid', $token);
					$this->db->update('tbl_verification_document', $verificationarraydata);

					$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
					$this->db->delete('tbl_work_experience_verified', array('candidateid' => $token)); 

					for ($i=0; $i < $countworkexpverified; $i++) { 

						$insertArraydata = array(
							'candidateid'  => $token,
							'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
						);

						$this->db->insert('tbl_work_experience_verified', $insertArraydata);
					}

		 $countdocumentname = count($this->input->post('documentname'));//	die;		
		 $this->db->delete('tbl_other_documents', array('candidateid' => $token)); 

		 for ($i=0; $i < $countdocumentname; $i++) { 

		 	if (!empty($this->input->post('documentname')[$i]) && isset($this->input->post('documentname')[$i])) {		

		 		if ($_FILES['otherdocumentupload']['name'][$i] != NULL) {

		 			@  $ext = end(explode(".", $_FILES['otherdocumentupload']['name'][$i])); 

					// if($_FILES['otherdocumentupload']['size'] < 10485760) {
					// 	$this->session->set_flashdata('er_msg', "filesize too large. Please upload file of size-2MB");
					// 	redirect(current_url());
					// }

		 			$OriginaldocumentName = $_FILES['otherdocumentupload']['name'][$i]; 
		 			$encrypteddocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
		 			$tempFile = $_FILES['otherdocumentupload']['tmp_name'][$i];
		 			$targetPath = FCPATH . "otherdocuments/";
		 			$targetFile = $targetPath . $encrypteddocumentName;
		 			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

		 			if($uploadResult == true){

		 				$encrypteddocument = $encrypteddocumentName;

		 			}else{

		 				die("Error uploading Other Document !!!");
		 				$this->session->set_flashdata("er_msg", "Error uploading Other Document ");
		 				redirect(current_url());
		 			}

		 		}
		 		else{

		 			$encrypteddocument =  $this->input->post('oldotherdocumentupload')[$i];
		 		}

		 		$insertArraydata = array(
		 			'candidateid'  => $token,
		 			'documentname'=> $this->input->post('documentname')[$i],
		 			'documenttype'=> $this->input->post('documenttype')[$i],
		 			'documentupload'=> $encrypteddocument,
		 			'createdon'=> date('Y-m-d H:i:s'),
		 			'createdby'=> $this->loginData->UserID,
		 		);

		 		$this->db->insert('tbl_other_documents', $insertArraydata);
				//echo  $this->db->last_query(); 
		 	}

		 }	
		 $this->db->trans_complete();

		 if ($this->db->trans_status() === FALSE){

		 	$this->session->set_flashdata('er_msg', 'Error !!! Not update DA document verification !!!');	
		 }else{
		 	$this->session->set_flashdata('tr_msg', 'Successfully update DA document verification !!!');			
		 }

		}

		$savebtnsubmitsave = $this->input->post('submitbtn');
		if (!empty($savebtnsubmitsave) && $savebtnsubmitsave =='senddatasubmit') {

			$currentstatus = $this->input->post('Approve');

			$verificationarraydata = array(

				'candidateid'  => $token,
				'metric_certificate'  => $this->input->post('metric_certificate_verified'),
				'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
				'ug_certificate'  => $this->input->post('ug_certificate_verified'),
				'pg_certificate'  => $this->input->post('pg_certificate_verified'),
				'other_certificate'  => $this->input->post('other_certificate_verified'),
				'general_nomination_form'  => $this->input->post('general_nomination_form'),
				'joining_report_da'  => $this->input->post('joining_report_da'),
				'comment'  => $this->input->post('comments'),
				'status' => $currentstatus,
				'approvedby' => $this->loginData->UserID, 	
				'approvedon'  => date('Y-m-d H:i:s'),

			);

			$this->db->where('candidateid', $token);
			$this->db->update('tbl_verification_document', $verificationarraydata);

			if ($currentstatus ==0) {


				$gerenalarraydata = array(

					'status' => 0,
				);
				$this->db->where('candidateid', $candidateid);
				$this->db->update('tbl_general_nomination_and_authorisation_form', $gerenalarraydata);


				$joiningreportarraydata = array(

					'status' => 0,
				);
				$this->db->where('candidateid', $candidateid);
				$this->db->update('tbl_joining_report', $joiningreportarraydata);


				$registrationarraydata = array(

					'BDFFormStatus' => 99,
				);
				$this->db->where('candidateid', $candidateid);
				$this->db->update('tbl_candidate_registration', $registrationarraydata);
				
				

			}


			$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
			$this->db->delete('tbl_work_experience_verified', array('candidateid' => $token)); 

			for ($i=0; $i < $countworkexpverified; $i++) { 

				$insertArraydata = array(
					'candidateid'  => $token,
					'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
				);

				$this->db->insert('tbl_work_experience_verified', $insertArraydata);
			}

			 $countdocumentname = count($this->input->post('documentname'));//	die;		
			 $this->db->delete('tbl_other_documents', array('candidateid' => $token)); 

			 for ($i=0; $i < $countdocumentname; $i++) { 

			 	if (!empty($this->input->post('documentname')[$i]) && isset($this->input->post('documentname')[$i])) {		

			 		if ($_FILES['otherdocumentupload']['name'][$i] != NULL) {

			 			@  $ext = end(explode(".", $_FILES['otherdocumentupload']['name'][$i])); 

			 			$OriginaldocumentName = $_FILES['otherdocumentupload']['name'][$i]; 
			 			$encrypteddocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
			 			$tempFile = $_FILES['otherdocumentupload']['tmp_name'][$i];
			 			$targetPath = FCPATH . "otherdocuments/";
			 			$targetFile = $targetPath . $encrypteddocumentName;
			 			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

			 			if($uploadResult == true){

			 				$encrypteddocument = $encrypteddocumentName;

			 			}else{

			 				die("Error uploading Other Document !!!");
			 				$this->session->set_flashdata("er_msg", "Error uploading Other Document ");
			 				redirect(current_url());
			 			}

			 		}
			 		else{

			 			$encrypteddocument =  $this->input->post('oldotherdocumentupload')[$i];
			 		}

			 		$insertArraydata = array(
			 			'candidateid'  => $token,
			 			'documentname'=> $this->input->post('documentname')[$i],
			 			'documenttype'=> $this->input->post('documenttype')[$i],
			 			'documentupload'=> $encrypteddocument,
			 			'createdon'=> date('Y-m-d H:i:s'),
			 			'createdby'=> $this->loginData->UserID,
			 		);

			 		$this->db->insert('tbl_other_documents', $insertArraydata);
				//echo  $this->db->last_query(); 
			 	}

			 }	

			 $this->db->trans_complete();



		//// Send Mail To HRD //////////////////////////////   

			 $hrdemailid    = 'amit.kum2008@gmail.com';
			 $candemailid   = $candidateemailid;

			 $subject = "Verified document";
			 $body = 	'Dear Sir, <br><br> ';
			 $body .= $this->input->post('comments');
		//$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
			 $to_email = $hrdemailid;
			 $to_name = $candemailid;
			 
			 $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);


			 if ($this->db->trans_status() === FALSE){

			 	$this->session->set_flashdata('er_msg', 'Error !!! Not update DA document verification !!!');	
			 }else{
			 	if($currentstatus==0){
			 		$this->session->set_flashdata('er_msg', 'Successfully update DA document verification !!!');
			 	}else{
			 		$this->session->set_flashdata('tr_msg', 'Successfully update DA document verification !!!');
			 	}	
			 	redirect('Joining_of_da_document_verification');		
			 }

			}
 } /// Post ///////


 $content['token'] = $token; 

 $content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

 $content['getcandateverified'] = $this->model->getCandateVerified($token);

 $content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);

 $content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);

 $content['getworkexperinceverifiedvalue'] = $this->model->getWorkExperinceVerifiedValue($token);
 $content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

 $content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);
//$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($token);

 $getTeamid = $this->model->getCandidateTeam($token);

 $content['witnessdeclaration'] = $this->model->getWitnessDeclaration($getTeamid->teamid);


 $content['getverified'] = $this->model->getVerifiedDetailes($token);

 $content['getdocuments'] = $this->model->getOtherDocuments();

 $content['countotherdoc'] = $this->model->getCountOtherDocument($token);

 $content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);

 $content['title'] = 'Hrd_document_checklist';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
}




public function view($token)
{
	try{

	if (empty($token) || $token == '' ) {
		
		redirect('/Campusinchargeintimation/index');
		$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
		
	}
	else { 

		$this->load->model('Hrd_document_checklist_model');

		$content['token'] = $token; 

		$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit();

		$content['getcandateverified'] = $this->model->getCandateVerified($token);

		$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);
		$content['getCandateyapyearexpcount'] = $this->model->getCandateyapyearexpcount($token);
		
		

		$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);
		
		$content['getgapyearverified'] = $this->model->gapyearcandidate_details($token);
		

		$content['getverified'] = $this->model->getVerifiedDetailes($token);
		$content['getverifiedstatus'] = $this->model->getverifiedstatus($token);

		$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

		$content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);
		$content['getdocuments'] = $this->model->getOtherDocuments();
		$content['countotherdoc'] = $this->model->getCountOtherDocument($token);
		$content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);


	/*echo "<pre>";
	print_r($content['candidatebdfformsubmit']);exit();*/


	$content['title'] = 'Hrd_document_checklist';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
}
















}