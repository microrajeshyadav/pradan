<?php 

/**
* State List testing changes
*/
class Stafftransfer extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Stafftransfer_model","model");
    $this->load->model("Staff_approval_model","Staff_approval_model");
    $this->load->model("Staff_approval_changerespons_model","Staff_approval_changerespons_model");
    $this->load->model("Staff_personnel_approval_model","Staff_personnel_approval_model");
    $this->load->model("Staffpromotion_model","Staffpromotion_model");
    $this->load->model("Staff_seperation_model","Staff_seperation_model");
    $this->load->model('Global_model', 'gmodel');
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {

     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
  /*echo "<pre>";
  print_r($this->loginData);exit();*/
 }

 public function index(){
  try{
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
   $query = "select * from msleavecrperiod";
   $content['Leave_details'] = $this->Common_Model->query_data($query);
   $content['title'] = 'Staffpromotion';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }
 public function Add($token = NULL)
 {
  try{

   // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
        /*$Staffdetails =  $this->model->getstaffDetails($token);
  echo "<pre>";
  print_r($Staffdetails);exit();*/
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD'); 

  if($RequestMethod == 'POST'){

   // print_r($this->input->post()); die;

        $tablename = "'staff_transaction'";
        $incval = "'@a'";

        $Staffdetails =  $this->model->getstaffDetails($token);
        $designation  =  $Staffdetails->designation;
        $supervisor   =  $Staffdetails->reportingto;
        $newofficeid = $this->input->post('transfer_office');
        $changereportingto = $this->input->post('changereportingto');
        if ($changereportingto == 'on') 
        {
          $changereportingto = 1;
        }
        else
        {
          $changereportingto = 0;
        }
       
        $receiverdetail = $this->Staff_seperation_model->get_staffDetails($supervisor);


        $proposeddate=$this->input->post('fromdate'); ///// date Standared formate
        
        $proposed_date_db=$this->gmodel->changedatedbformate($proposeddate); //// date convert db formate

         $effective_date= $this->input->post('effective_date'); ///// date Standared formate
        
        $effective_date_db=$this->gmodel->changedatedbformate($effective_date); //// date convert db formate

        $comments = $this->input->post('reason');

        $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

        $autoincval =  $getstaffincrementalid->maxincval;

       // echo $autoincval; die;

        $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
        $result  = $this->db->query($sql)->result()[0];

        $transstatus = 'Transfer';
        $sql = "select count(staffid) as staffcount from staff_transaction where trans_status ='".$transstatus."' and staffid= '".$token."' and date_of_transfer = '".$proposed_date_db."' ";

        $staffcountresult = $this->db->query($sql)->row();

       // echo $result->staffcount; die;

        if($staffcountresult->staffcount==0){


        $this->db->trans_start();

          $insertArr = array(
            'id'                => $autoincval,
            'staffid'           => $token,
            'old_office_id'     => $this->input->post('Presentoffice'),
            'new_office_id'     => $this->input->post('transfer_office'),
            'old_designation'   => $designation,
            'new_designation'   => $designation,
            'trans_status'      => 'Transfer',
            'date_of_transfer'  => $proposed_date_db,
            'effective_date'    => $effective_date_db,
            'changereporting'    => $changereportingto,
            'reason'            => $this->input->post('reason'),
            'trans_flag'        => 1,
            'reportingto'       => $this->input->post('reportingto'),
            'createdon'         => date("Y-m-d H:i:s"),
            'datetime'          => date("Y-m-d H:i:s"),
            'createdby'         => $this->loginData->staffid
         );

        $this->db->insert('staff_transaction', $insertArr);

      // echo  $this->db->last_query(); die;

        if ($result->workflowid !='') {
           $insertworkflowArr = array(
             'r_id'                 => $autoincval,
             'type'                 => 2,
             'staffid'              => $token,
             'sender'               => $this->loginData->staffid,
             'receiver'             => $supervisor,
             'forwarded_workflowid' => $result->workflowid,
             'senddate'             => date("Y-m-d H:i:s"),
             'flag'                 => 1,
             'scomments'            => $this->input->post('discussion'),
             'createdon'            => date("Y-m-d H:i:s"),
             'createdby'            => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }else{

           $insertworkflowArr = array(
             'r_id'           => $autoincval,
             'type'           => 2,
             'staffid'        => $token,
             'sender'         => $this->loginData->staffid,
             'receiver'       => $supervisor,
             'senddate'       => date("Y-m-d H:i:s"),
             'flag'           => 1,
             'scomments'      => $this->input->post('discussion'),
             'createdon'      => date("Y-m-d H:i:s"),
             'createdby'      => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }
         
          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Transfer request has been failed !!! '); 
            redirect(current_url());

          }else{
                      
              $newofficename =  $this->model->getNewOfficeName($newofficeid);
              // print_r($newofficename); die;

                // Mail Send
              $subject = ":Transfer Mail";
              $body = "<h4>Hi ".$Staffdetails->name.", </h4><br />";
              $body .= "We have transfer new location.<br />";
              $body .= "<b>Office: " . $newofficename->officename . "</b><br />";
              $body .= "<b>Comment : " .  $comments . "</b><br /><br /><br />";
              $body .= "See you soon on Pradan <br><br>";
              $body .= "<b> Thanks </b><br>";
              $body .= "<b>Pradan Technical Team </b><br>";

              $to_email = $receiverdetail->emailid;
              $to_name = $receiverdetail->name;
              //$to_cc = $getStaffReportingtodetails->emailid;
              $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $Staffdetails->emailid => $Staffdetails->name
               // ..
              );
              $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
              if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
              }

          $this->session->set_flashdata('tr_msg', 'Transfer request has been initiated successfully !!!');
          redirect('/Stafftransfer/view_history/'.$token);

          }   
         
        }else{
        $this->session->set_flashdata('er_msg', "Transfer  already under process !!");
        redirect('/Stafftransfer/add/'.$token);
      }
    }




       $content['token']=$token;
       $content['staff']= $this->model->getstaffname($token);
       $content['transfer_promption']=$this->model->get_staffDetails($token);
       // print_r($content['staff']); die;

       $p_office=$content['transfer_promption']->new_office_id;
       $p_designation= $content['transfer_promption']->new_designation;
       $content['all_office']= $this->model->get_alloffice($p_office);
      // $content['getintegrator']= $this->model->get_Integrator_ED_OnlyteamTC($p_office);

      // $content['all_designation']=$this->model->get_alldesignation($p_designation);
      


        $content['subview'] = 'Stafftransfer/add';
        $this->load->view('_main_layout', $content);
        }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
      }




      public function transfer_approval()
      {
        try{
     //echo "hello";
    // die;
    // start permission 
       $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
       $content['role_permission'] = $this->db->query($query)->result();
       $content['subview'] = 'Stafftransfer/transfer_approval';
       $this->load->view('_main_layout', $content);
       }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
     }



     public function presentoffice(){
      try{
      $staffidd=$this->input->post('staff');
      $this->load->model('Staffpromotion_model');
      $presentoffice=$this->Staffpromotion_model->fetchpresent($staffidd);
//$preseentoffce=$presentoffice->officename;

      $arr1=array('office_name'=>$presentoffice->officename,'office_id'=>$presentoffice->officeid);


      echo json_encode($arr1);

      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

    }

    public function view_history($token){

      try{

     $content['token']=$token;
     $this->load->model('Stafftransfer_model');
     $content['gethistory']=$this->Stafftransfer_model->getdata($token);

     $content['token']=$token;
     $content['subview'] = 'Stafftransfer/view_history';
     $this->load->view('_main_layout', $content);

     }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   public function approve(){
    try{
      // start permission 
      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
      $content['role_permission'] = $this->db->query($query)->result();
      // end permission 

      $staffid = $this->uri->segment(3);
      $tarnsid  = $this->uri->segment(4);
      
      $staffdetail = $this->Staff_seperation_model->get_staffDetails($staffid);
      $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();

      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail 
      WHERE r_id = $tarnsid";
      $result  = $this->db->query($sql)->result()[0];

      /*echo "<pre>";
      print_r($result->flag);
      exit();*/

      // flag = approval
      // unflag = reject

      $forwardworkflowid = $result->workflowid;
      if ($this->loginData->RoleID == 18) { // Executive Director
        if($result->flag == 5){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 7;
          $unflag = 8;
          $content['addflag'] = 7;
          $content['addunflag'] = 8;
        }else if($result->flag == 15){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 17;
          $unflag = 18;
          $content['addflag'] = 17;
          $content['addunflag'] = 18;
        }
      }else if($this->loginData->RoleID == 17){ // 17 personal Admin
        if($result->flag == 7){
          $content['heading_text'] = 'Staff';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 18;
          $flag = 9;
          $unflag = 10;
        }
      }else if($this->loginData->RoleID == 20){ // Finance Admin
        $content['heading_text'] = 'Personnel Administrator';
        $content['redirect_controller'] = 'Staff_finance_approval';
        $transfer_to_id = 17;
        $flag = 9;
        $unflag = 10;
      }else if($this->loginData->RoleID == 2 || $this->loginData->RoleID == 21){ // Superviser TC
        if($result->flag == 1){
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 17;
          $flag = 3;
          $unflag = 24;
          $content['addflag'] = 3;
          $content['addunflag'] = 4;
        }else if($result->flag == 17){
          $content['heading_text'] = 'Finance Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 20;
          $flag = 19;
          $unflag = 20;
          $content['addflag'] = $flag;
          $content['addunflag'] = $unflag;
        }else{
          $content['heading_text'] = 'Finance Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 20;
          $flag = 21;
          $unflag = 22;
          $content['addflag'] = 21;
          $content['addunflag'] = 22;
        }
      }
      /*echo "<pre>";
      print_r($transfer_to_id);exit();*/
      // echo $content['addflag']."string".$content['addunflag'];
      $RequestMethod = $this->input->server('REQUEST_METHOD'); 

      $getexecutivedirector ="";
      $getexecutivedirector = $this->Staff_approval_model->getExecutiveDirectorList($transfer_to_id);
      // echo "<pre>";
      // print_r($getexecutivedirector); die();

      if($RequestMethod == 'POST'){
        // echo "<pre>";
        // print_r($_POST);
      //  die;

      $arrr = array();
       foreach ($getexecutivedirector as $key => $value)
       {
        array_push($arrr, $value->edstaffid);
       }

       // echo "<pre>";
      // print_r($edarrr); die();


        $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
        $executivedirector_administration = $this->input->post('executivedirector_administration');
       // die("dhdgwheg");
        $receiverdetail = $this->Staff_seperation_model->get_staffDetails($staffid);
        // echo "<pre>";
        // print_r($receiverdetail);exit();
        $transferstaffdetails = $this->Staff_approval_model->getTransferStaffDetails($transid);

        $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
        $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
        $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.
        $getpersonnaluser = $this->Staff_approval_model->getPersonalUserList();

       //  $arrr = array();
       // foreach ($getpersonnaluser as $key => $value)
       // {
       //  array_push($arrr, $value->personnelstaffid);
       // }

                if($status == 4){
        $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
      }
        $this->form_validation->set_rules('status','Select Status','trim|required');
        
        if($this->form_validation->run() == TRUE){
        /*$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;*/
          //echo $status.$this->form_validation->run();
          /*echo "<br>".$flag;
          print_r($_POST);
          exit;*/

        $this->db->trans_start();
        if($status == $flag){     //// Status => Approve 

          $updateArr = array(

            'trans_flag'     => $flag,
            'updatedon'      => date("Y-m-d H:i:s"),
            'updatedby'      => $this->loginData->staffid
          );
          // echo "<pre>";
          // print_r($updateArr);
          // die;
          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $updateArr);

          foreach ($arrr as $key => $value) {
          $insertworkflowArr = array(

           'r_id'                 => $transid,
           'type'                 => 2,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $value,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $flag,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );
// echo "<pre>";
// print_r( $insertworkflowArr);
// die;
          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
          // echo $this->db->last_query();
          // die;
           }
          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{
            $subject = "Your Process Approved";
            $body = 'Dear,<br><br>';
            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
            $body .= 'This is to certify that Mr. '.$staffdetail->name.'<br> your process has Approved  .<br><br>';
            $body .= 'Thanks<br>';
            $body .= 'Administrator<br>';
            $body .= 'PRADAN<br><br>';

            $body .= 'Disclaimer<br>';
            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

            
            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $staffdetail->emailid => $staffdetail->name
               // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
            }

            $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          //redirect('/Staff_approval/'.$token);
            redirect($content['redirect_controller']);

          }

        }
        if ($status ==$unflag) {

         $insertArr = array(

          'trans_flag'     => $unflag,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 6,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => $unflag,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_approval/'.$token);

        }
      }
    }
  }

 /*echo "<pre>";
    print_r($transfer_to_id);exit();*/
    prepareview:

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    $content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();
    
    $content['subview'] = 'Stafftransfer/approve';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

 /**
   * Method add_iom() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



 public function add_iom($token)
 {

  try{
      // start permission 
      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
      $content['role_permission'] = $this->db->query($query)->result();
      // end permission 
      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
      $result  = $this->db->query($sql)->result()[0];
      $forwardworkflowid = $result->workflowid;
      $content['staff_iomtransfer'] = $this->Staff_personnel_approval_model->getstaff_iom_transfer($token);
      $staffid = $content['staff_iomtransfer']->staffid;
      $transid = $content['staff_iomtransfer']->id;
      $content['staffid'] = $staffid;
      $content['tarnsid'] = $token;
      $content['getstaffdetails'] = $this->Staff_personnel_approval_model->getStaffDetail($token);
      // print_r($content['getstaffdetails']);
      // die;
      $content['reportingto'] = $content['getstaffdetails']->reportingto;
      $content['iom_transfer_detail'] = $this->Staff_personnel_approval_model->
      getStaffiomtransferDetail($token);

      
      $content['eddetail'] = $this->Staff_personnel_approval_model->geteddetail();
  
      /*echo "<pre>";
      print_r($content['getstaffdetails']);exit();*/
       $staffdetail = $this->Staff_seperation_model->get_staffDetails($staffid);
       $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
       $receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['reportingto']);
      /*echo "<pre>";
      print_r($receiverdetail);exit();*/
      
      $RequestMethod = $this->input->server("REQUEST_METHOD");

      if($RequestMethod == 'POST')
      {
       /* echo "<pre>";
        print_r($_POST);
        die();*/
         $approve_command=$this->input->post('approve_command');
        

        $db_flag = '';
        if(trim($this->input->post('approve_command'))){
          $db_flag = 1;
        }else{
          $db_flag = 0;
        }
        // echo $db_flag;
        // die;
        /*echo "<pre>";
        print_r($db_flag);exit();*/
        $this->db->trans_start();
       
        $insertArray = array(
          'transferno'                    => $this->input->post('transferno'),
          'staffid'                        => $content['staff_iomtransfer']->staffid,
          'transid'                        => $token,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    => $this->gmodel->changedatedbformate($this->input->post('letter_date')),
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $this->gmodel->changedatedbformate($this->input->post('charge_responsibility_on')),
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place_id'),
          'report_for_work_place_on_date'  => $this->gmodel->changedatedbformate($this->input->post('restransferno')),
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our_id'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => $db_flag,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
          'month'                           =>$this->input->post('month')
        );
       
        if($content['iom_transfer_detail'])
        {
          $id = $content['iom_transfer_detail']->id;
          $updateArray = array(
          'transferno'                    => $this->input->post('transferno'),
          'staffid'                        => $content['staff_iomtransfer']->staffid,
          'transid'                        => $token,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    => $this->gmodel->changedatedbformate($this->input->post('letter_date')),
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $this->gmodel->changedatedbformate($this->input->post('charge_responsibility_on')),
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place_id'),
          'report_for_work_place_on_date'  => $this->gmodel->changedatedbformate($this->input->post('restransferno')),
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our_id'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => $db_flag,
          'updatedon'                       => date("Y-m-d H:i:s"),
          'updatedby'                       => $this->loginData->staffid,
          'month'                           =>$this->input->post('month')

          );
          /*echo "<pre>";
          print_r($id);exit();*/
          $this->db->where('id', $id);
          $flag = $this->db->update('tbl_iom_transfer', $updateArray);
        }else{
          $flag = $this->db->insert('tbl_iom_transfer',$insertArray);
        }
       // echo $this->db->last_query(); die;
        if(trim($this->input->post('approve_command'))){
          // staff transaction table flag update after acceptance
          $updateArr = array(                         
              'trans_flag'     => 5,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
          );

          $this->db->where('id',$token);
          $this->db->update('staff_transaction', $updateArr);
          
          $insertworkflowArr = array(
           'r_id'                 => $token,
           'type'                 => 2,
           'staffid'              => $content['staffid'],
           'sender'               => $this->loginData->staffid,
           'receiver'             => $content['reportingto'],
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 5,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
          );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
      }
        $this->db->trans_complete();
        if($flag) 
        {
          $subject = "Your Process Approved";
          $body = 'Dear,<br><br>';
          $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
          $body .= 'This is to certify that Mr. '.$staffdetail->name.'<br> your process has Approved  .<br><br>';
          $body .= 'Thanks<br>';
          $body .= 'Administrator<br>';
          $body .= 'PRADAN<br><br>';

          $body .= 'Disclaimer<br>';
          $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


          $to_email = $receiverdetail->emailid;
          $to_name = $receiverdetail->name;
          //$to_cc = $getStaffReportingtodetails->emailid;
          $recipients = array(
             $personnel_detail->EmailID => $personnel_detail->UserFirstName,
             $staffdetail->emailid => $staffdetail->name
             // ..
          );
          $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
          if (substr($email_result, 0, 5) == "ERROR") {
            $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
          }
          $this->session->set_flashdata('tr_msg','Data Saved Successfully.');
          redirect(current_url());
        } 
        else {
          $this->session->set_flashdata('er_msg','Something Went Wrong!!');
          redirect(current_url());
       }
    } /// if Post close

    $officeid = $content['getstaffdetails']->new_office_id;
    $old_office_id = $content['getstaffdetails']->old_office_id;/*
   echo "<pre>";
   print_r($content['staffid']); die;*/
    $content['getofficedetails'] = $this->Staff_personnel_approval_model->getAllOffice();

   $content['getstafflist'] = $this->Staff_personnel_approval_model->getStaffList($officeid, $content['staffid']);
   $content['getstafflistold'] = $this->Staff_personnel_approval_model->getStaffList($old_office_id, $content['staffid']);

    
   //print_r($getstafflist); die;

    $content['subview'] = 'Stafftransfer/add_iom';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}

  public function add_clearance_certificate($token)
  {
    try{

        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 
    $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
    $result  = $this->db->query($sql)->result()[0];
    $forwardworkflowid = $result->workflowid;
    $content['personaldetail'] = $this->Staff_approval_model->getExecutiveDirectorList(17);
    $content['getstafflist'] = $this->Staff_approval_model->getstaffdetailslist($token);
    $content['getsinglestaff'] = $this->Staff_approval_model->get_staff_transfer_promation_detail($token);
    //print_r($getsinglestaff);
    $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($content['getstafflist']->staffid);
  // print_r($content['staffdetail']);
    $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
    
     /*echo "<pre>";
     print_r($content['staffdetail']);die();*/

    $RequestMethod = $this->input->server('REQUEST_METHOD');
    if($RequestMethod == 'POST'){
      // $getpersonnaluser = '';
      // $getpersonnaluser = $this->Staff_approval_model->getExecutiveDirectorList(17); //copied as previous

       $getpersonnaluser ="";
      $getpersonnaluser = $this->Staff_approval_changerespons_model->getPersonalUserList();
      $arrr = array();
       foreach ($getpersonnaluser as $key => $value)
       {
        array_push($arrr, $value->personnelstaffid);
       }

      $btnsubmit = $this->input->post('btnsubmit');
      $btnsend = $this->input->post('btnsend');

      
      if (isset($btnsend) && $btnsend == "AcceptSaveData") {

        $this->db->trans_start();

        $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           => $token,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 0,
        );


        $this->db->insert('tbl_clearance_certificate', $insertArr);
        $insertid = $this->db->insert_id();

        $name_of_location =  count($this->input->post('name_of_location'));

        for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $insertid,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );

         $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
       }

       $this->db->trans_complete();



      if ($this->db->trans_status() === FALSE){
      $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
      redirect(current_url());
      }else{

        $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
        redirect('/Staff_approval/edit_clearance_certificate/'.$insertid);

      }
    }

   
    // if (trim($this->input->post('comments'))) {
      $this->db->trans_start();

      $insertArr = array(
        'certify_that'      => $this->input->post('certify_that'),
        'transid'           => $token,
        'createdon'         => date('Y-m-d H:i:s'),
        'createdby'         => $this->loginData->staffid,
        'flag'              => 1,
      );
       
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);
      $insertid = $this->db->insert_id();
      
      $name_of_location =  count($this->input->post('name_of_location'));

      for ($i=0; $i < $name_of_location; $i++) { 
       $insertchiArr = array(
        'clearance_certificate_id'      => $insertid,
        'location'                      => $this->input->post('name_of_location')[$i],
        'description'                   => $this->input->post('description')[$i],
        'item_values'                   => $this->input->post('value')[$i], 

      );

       $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);

     }

// echo $this->input->post('status');die;
     $receiverdetail = '';
      // if(trim($this->input->post('comments'))){
        if($this->input->post('status') == 7){
          foreach ($arrr as $key => $value)
       {
          $receiverdetail = $this->Staff_seperation_model->get_staffDetails($value);
          // $receiver = $this->input->post('personal_administration');
        }
        }
        // else{
        //   $receiverdetail = '';
        //   $receiver = '';
        // }
        // staff transaction table flag update after acceptance
        
        $updateArr = array(                         
          'trans_flag'     => $this->input->post('status'),
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('id',$token);
        $this->db->update('staff_transaction', $updateArr);
        foreach ($arrr as $key => $value)
       {
        $insertworkflowArr = array(
         'r_id'                 => $token,
         'type'                 => 2,
         'staffid'              => $content['getstafflist']->staffid,
         'sender'               => $this->loginData->staffid,
         'receiver'             => $value,
         'forwarded_workflowid' => $forwardworkflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => $this->input->post('status'),
         'scomments'            => $this->input->post('comments'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
        );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
        }
      // }

      /*$insertArr = array(
        'trans_flag' => 9,
      );

      $this->db->where('id',$token);
      $this->db->update('staff_transaction', $insertArr);*/

     $this->db->trans_complete();


     if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
      redirect(current_url());

    }else{
      $subject = "Your Process Approved";
      $body = 'Dear,<br><br>';
      $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
      $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
      $body .= 'Thanks<br>';
      $body .= 'Administrator<br>';
      $body .= 'PRADAN<br><br>';

      $body .= 'Disclaimer<br>';
      $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

      
      if($this->input->post('status') == 7){
      $to_email = $receiverdetail->emailid;
      $to_name = $receiverdetail->name;
    }else{
       $to_email = $content['staffdetail']->emailid;
      $to_name = $content['staffdetail']->name;
    }
      //$to_cc = $getStaffReportingtodetails->emailid;
      $recipients = array(
         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
         $content['staffdetail']->emailid => $content['staffdetail']->name
         // ..
      );
      $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
      if (substr($email_result, 0, 5) == "ERROR") {
        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
      }
       if($this->input->post('status') == 7){
      $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
      redirect('/Stafftransfer/view_clearance_certificate/'.$insertid);
    }else if($this->input->post('status') == 8){
      $updateStaffArr = array(                         
          'flag'     => 4,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

        $this->db->where('staffid',$content['getstafflist']->staffid);
        $this->db->update('staff', $updateStaffArr);
      $this->session->set_flashdata('tr_msg', 'clearance certificate has Rejected  successfully !!!');
      redirect('/Stafftransfer/view_clearance_certificate/'.$insertid);
    }else{
      $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
      redirect('/Stafftransfer/view_clearance_certificate/'.$insertid);
    }

    }
  // }

}



$content['title'] = '/add_clearance_certificate';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function view_clearance_certificate($token)
{

  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 


  $getstaffcclist = $this->Staff_approval_model->getstaffclerancelist($token);
  $transid = $getstaffcclist->transid;
  $content['getstaffcertify_that'] = $getstaffcclist;

  $content['getstafflist'] = $this->Staff_approval_model->getSinglestaffdetailslist($transid);
/*echo "<pre>";
print_r($content['getstafflist']); die();*/
  $content['getcountstaffitemslist'] = $this->Staff_approval_model->getcountstaffitmsclerancelist($token);
  $content['getstaffitemslist'] = $this->Staff_approval_model->getstaffitmsclerancelist($token);




  $content['title'] = '/view_clearance_certificate';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  
}

}
