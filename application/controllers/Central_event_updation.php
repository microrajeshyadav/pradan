<?php 

/**
* Central Event Updation Controller List
*/
class Central_event_updation extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');

    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model', 'model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }


   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{
   $this->load->model('Central_event_model','model');
    // start permission 
   $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //////////////// Select Batch ////////////////
   $query = "select * from mstbatch where status =0 AND isdeleted='0'";
   $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
   $query = "select * from mstphase where isdeleted='0'";
   $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
   $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
   $content['candidate_details'] = $this->Common_Model->query_data($query);

   $content['centralevent_details'] = $this->model->getCentralEvent();
   $content['centralevent_list'] = $this->model->getCentralEventList();




   $content['subview']="index";

   $content['title'] = 'Central_event_updation';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }



 public function Add()
 {
try{
        // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   
   $countupload_material = '';
  $RequestMethod = $this->input->server('REQUEST_METHOD');
  // print_r($RequestMethod); die();
  if($RequestMethod == 'POST'){

   // echo "<pre>";
  // print_r($_FILES);
  //print_r($this->input->post());
   //  die;

    $this->db->trans_start();




 $countupload_material = count($_FILES['upload_material']['name']);


      for ($i=0; $i < $countupload_material ; $i++) { 


        if ($_FILES['upload_material']['name'][$i] != NULL) {

      // echo "xczxc"; die;

          @  $ext = end((explode(".", $_FILES['upload_material']['name'][$i]))); 

         $OriginalUploadMaterialPhotoName = $_FILES['upload_material']['name'][$i]; 
          $encryptedupload_materialName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
          $tempFile = $_FILES['upload_material']['tmp_name'][$i];
          $targetPath = FCPATH . "datafiles/centraleventdocument/";
          $targetFile = $targetPath . $encryptedupload_materialName;
          $uploadResult = move_uploaded_file($tempFile,$targetFile);

          if($uploadResult == true){
            $encryptedPloadMaterialNameImage = $encryptedupload_materialName;

          }else{

            die("Error uploading Photo file");
            $this->session->set_flashdata("er_msg", "Error uploading event document");
            redirect(current_url());
          }
        }
        // else{
        //   $OriginalUploadMaterialPhotoName = $this->input->post('upload_material')[$i];
        //   $encryptedPloadMaterialNameImage = $this->input->post('upload_material')[$i];
        // }

        $insertArr_matrial = array(
        'central_event_id'     => $this->input->post('central_event_name'),
        'encrypted_document_upload' => $encryptedPloadMaterialNameImage,
        'original_document_upload' => $OriginalUploadMaterialPhotoName,
   );
  $this->db->insert('tbl_central_event_updation_metrial', $insertArr_matrial);
  }


     $countpaticipants =  count($this->input->post('paticipants'));  
  
    for ($i=0; $i < $countpaticipants ; $i++) { 
      $central_event_id = $this->input->post('central_event_name');
      $paticipants = $this->input->post('paticipants')[$i];

      $insertArr_transaction = array(
     'central_event_join_status' => 1,
      );
      $this->db->where('central_event_id', $central_event_id);
       $this->db->where('participant_id',$paticipants);
      $this->db->update('tbl_participant', $insertArr_transaction);
       //echo $this->db->last_query(); die;
    }
    $this->db->trans_complete();

    if ($this->db->trans_status() === true){
      $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event Updateion  !!!');
      redirect('/Central_event_updation/index');
    }else{
      $this->session->set_flashdata('er_msg', 'Error Central Event Updation  !!!');
      redirect('/Central_event_updation/add');
    }
}


  //prepareview:

  //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0'";
  $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
  $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
  $content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
  $content['resouceperson_details'] = $this->model->getResoucePerson();
  $content['centralevent_details'] = $this->model->getCentralEvent();
  $content['event_details'] = $this->model->getEvent();
  $current_financial_year    = $this->model->CurrentFinancialYear();
  $content['fin_year_batch_da']  = $this->model->getDevelopmentShip($current_financial_year);

  // $temp_array = [];
  // $resouceperson_list = $this->model->getSingleResoucePerson();
  // foreach ($resouceperson_list as $mrow) {
  //   $temp_array[] = $mrow->resouce_person_name;
  // }
  // $content['map_resouce_person_array'] = $temp_array;

  $content['title']   = 'Central_event_updation';
  $content['subview'] = 'Central_event_updation/add';
  $this->load->view('_main_layout', $content);
  }catch(Exception $e){
  print_r($e->getMessage());die();
}
}


public function edit($token)
{
  try{
        // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){
    //echo "<pre>";
   //print_r($this->input->post()); die;


  $this->db->trans_start();

  if ($this->input->post('old_original_upload_material') !='') {
    $countupload_material = count($this->input->post('old_original_upload_material')); 
  }else{
    $countupload_material = count($_FILES['upload_material']['name']);
  }
    
   $this->db->delete('tbl_central_event_updation_metrial',array('central_event_id'=> $token));

      for ($i=0; $i < $countupload_material ; $i++) { 


        if ($_FILES['upload_material']['name'][$i] != NULL) {


          @  $ext = end((explode(".", $_FILES['upload_material']['name'][$i]))); 

         $OriginalUploadMaterialPhotoName = $_FILES['upload_material']['name'][$i]; 
          $encryptedupload_materialName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
          $tempFile = $_FILES['upload_material']['tmp_name'][$i];
          $targetPath = FCPATH . "datafiles/centraleventdocument/";
          $targetFile = $targetPath . $encryptedupload_materialName;
          $uploadResult = move_uploaded_file($tempFile,$targetFile);

          if($uploadResult == true){
            $encryptedPloadMaterialNameImage = $encryptedupload_materialName;

          }else{

            die("Error uploading Photo file");
            $this->session->set_flashdata("er_msg", "Error uploading event document !!");
            redirect(current_url());
          }
        }
        else{
          $OriginalUploadMaterialPhotoName = $this->input->post('old_original_upload_material')[$i];
          $encryptedPloadMaterialNameImage = $this->input->post('old_encrypted_upload_material')[$i];
        }

        $insertArr_matrial = array(
        'central_event_id'     => $token,
        'encrypted_document_upload' => $encryptedPloadMaterialNameImage,
        'original_document_upload' => $OriginalUploadMaterialPhotoName,
   );
   
  $this->db->insert('tbl_central_event_updation_metrial', $insertArr_matrial);
 // echo $this->db->last_query();

  }



 $countpaticipants =  count($this->input->post('paticipants')); 

  for ($i=0; $i < $countpaticipants ; $i++) { 

   $paticipants =  $this->input->post('paticipants')[$i];

     $query = $this->db->query("SELECT * FROM `tbl_participant` WHERE `central_event_id`=$token AND `participant_id`= $paticipants");
      $result  = $query->result();
      $id = $result[0]->id;
     $numrows = $query->num_rows();
     if ($numrows==0) {
       $insertArr_transaction = array(
        'central_event_join_status'    => 1,
       );
     
     $this->db->insert('tbl_participant', $insertArr_transaction);
     }else{
      $updateArr_transaction = array(
        'central_event_join_status'    => 1,
   );
   $this->db->where('id',$id);  
  $this->db->update('tbl_participant', $updateArr_transaction);
   }
  
}



    $this->db->trans_complete();

    if ($this->db->trans_status() === true){
      $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event Updation  !!!');
      redirect('/Central_event_updation/index');
    }else{
      $this->session->set_flashdata('er_msg', 'Error Adding Central Event Updation !!!');
      redirect('/Central_event_updation/add');
    }

  }

  prepareview:

  //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0'";
  $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);



     //////////////// Select Phase ////////////////
  $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
  $content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
   $content['resouceperson_details'] = $this->model->getResoucePerson();
    ////////////////////// Select getResoucePerson ////////////////////////
   $singleresouceperson_details1 = $this->model->getSingleCentralEvent($token);
  
  
   $content['singleresouceperson_details'] = $singleresouceperson_details1[0];

   
   $resouceperson_list = $this->model->getSingleResoucePerson($token);
    $temp_array = [];
   foreach ($resouceperson_list as $mrow) {
    $temp_array[] = $mrow->resouce_person_id;
  }
  $content['map_resouce_person_array'] = $temp_array;

  $content['centralevent_details'] = $this->model->getCentralEventEdit();
  $content['resouceperson_details'] = $this->model->getResoucePerson();
  $content['centralevent_list'] = $this->model->getCentralEventList();
  $content['event_details'] = $this->model->getEvent();
  $current_financial_year    = $this->model->CurrentFinancialYear();
  $content['fin_year_batch_da']  = $this->model->getDevelopmentShip($current_financial_year);

  $paticipants_details = $this->model->getPaticipants($token);
  //print_r($paticipants_details);
   $temp_paticipants_array = [];
  foreach ($paticipants_details as $mrow) {
    $temp_paticipants_array[] = $mrow->participant_id;
  }
  $content['map_paticipants_array'] = $temp_paticipants_array;

  $content['document_details']    = $this->model->getEventDocument($token);
  $content['token'] = $token;
  $content['title'] = 'DA_event_detailing';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



function getDAEventDetails(){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
       LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE `tbl_da_event_detailing`.`isdeleted`='0' ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function getSingleDAEventDetails($token){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
       LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE  `tbl_da_event_detailing`.`id`='0' AND `tbl_da_event_detailing`.`isdeleted`=$token ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function delete($token)
{
 try{
 $this->Common_Model->delete_row('tbl_central_event_transaction','central_event_id', $token); 
$this->Common_Model->delete_row('tbl_central_event','id', $token);
$this->session->set_flashdata('tr_msg' ,"Central Event Deleted Successfully");
  redirect('/Central_event_updation/index/');

  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

}