<?php 
class Leaveadjustment extends CI_Controller
{

	// echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Leaveadjustment_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index()
	{
		try{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission  

			$content['stafflist'] = $this->Leaveadjustment_model->getStaffList();


			$content['title'] = 'Leave Adjustment';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}
	public function adjustment($token)
	{
		try{
		// start permission 
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				echo "<pre>";
				print_r($this->input->post()); 

				$fromdate = $this->input->post('fromdate');
			$todate = $this->input->post('todate'); 

			$noofdays = $this->input->post('noofdays');
			$reason = $this->input->post('reason');
		    $leavetyped = $this->input->post('leavetype');
		    $longleave=$this->model->getlongleave($leavetyped);
	    	if ($noofdays > $longleave->longleavedays)
	    	{
	    		$longleave = 1;
	    	}
	    	else
	    	{
	    		$longleave = 0;
	    	}
	    	echo $longleave;
	    	// die;
			// $leavetyped = "";
			switch ($leavetyped) {
				case '1':
				$leavename = "Maternity";
				break;

				case '2':
				$leavename = "LWP(Leave without pay)";
				break;

				case '3':
				$leavename = "Special";
				break;

				case '4':
				$leavename = "Study";
				break;

				case '5':
				$leavename = "Sabbatical";
				break;

				case '6':
				$leavename = "General Leave";
				break;

				case '7':
				$leavename = "Leave with pay";
				break;

				case '8':
				$leavename = "Paternity";
				break;

				case '1':
				$leavename = "Maternity";
				break;

				case '2':
				$leavename = "LWP(Leave without pay)";
				break;

				case '3':
				$leavename = "Special";
				break;

				case '4':
				$leavename = "Study";
				break;

				case '5':
				$leavename = "Sabbatical";
				break;

				case '6':
				$leavename = "General Leave";
				break;

				case '7':
				$leavename = "Leave with pay";
				break;

				case '8':
				$leavename = "Paternity";
				break;
			}
			echo $leavename; die;
				$this->db->trans_start();



				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){
					$this->session->set_flashdata('er_msg', 'Error adding Recruitment');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully added Recruitment');		
				}
				redirect('/Leaveadjustment/index');

			}



			$empcode = $this->Leaveadjustment_model->get_empid($token);
			
			$content['getreportingdetails']  = $this->model->getreportingtodetails($empcode->emp_code);


			$content['leaverequest'] = $this->Leaveadjustment_model->getrequestedleave($empcode->emp_code);
			//echo "<pre>";
			//print_r($content['leaverequest']); die;
			$content['leavebalance'] = $this->model->getleavedetail_balance($empcode->emp_code);
				// print_r($content['leavebalance']); die;
			$content['leavecategory'] = $this->model->getleavectegory();
// 			echo "<pre>";
// print_r($content['leavecategory']); die;
			$content['leaverequest'] = $this->model->getleavedetails('1');

			$content['title'] = 'Leave Adjustment';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}
}