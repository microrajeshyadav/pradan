<?php 
class Changefieldguide extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->model("Assigntc_model");
		$this->load->model("Dompdf_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		

		$this->load->model(__CLASS__ . '_model','model');
		//$mod = $this->router->class.'_model'; 
	        //$this->load->model($mod,'',TRUE);
		//$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	}

	

	
	public function add($token){

		// echo "hello";
		// die;

		try{

			if (empty($token) && $token=='') {

				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Staff_list/index');
			}
			else
			{
				$getcategory = '';
				$staff_details ='';
				$hr_data = '';
				$candidateid = '';

				$getcategory = $this->model->getCategoryid($token);

				$content['office_id']=$this->model->getofficeid($token);

				$content['Fieldguide_id']=$this->model->Fieldguide_name($token);

				$staff_details=$this->model->getstafftransdetail($token);

				if (empty($staff_details) && $staff_details=='') 
				{


					$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
					redirect('/Staff_list/index');
				}
				else
				{
					$hr_data='';
					$hr_data=$this->model->hr_data($token);
					$hrname='';
					$hrname=$hr_data->name;
					
				    $hr_reportingto=$hr_data->reportingto;
					$offer_no=$staff_details->offerno;
					$candidateid=$staff_details->candidateid;

					$joining_date=$this->gmodel->changedatedbformate($staff_details->doj);
					$createdon=$this->gmodel->changedatedbformate($staff_details->createdon);
					$offer_date=explode(" ",$createdon);


					$candidate_name=$staff_details->name;

					$tablename = "'staff_transaction'";
					$incval = "'@a'";

					$getstaffincrementalid = $this->model->getfieldguidemmax($tablename,$incval); 
					$autoincval =  $getstaffincrementalid->maxincval;

					$old_Fieldguide=$this->model->old_Fieldguide($staff_details->candidateid);
					$content['old_Fieldguide']=$old_Fieldguide;

					$da_personnalinfo=$this->model->da_personnalinfo($candidateid);
					$id=$da_personnalinfo->id;

					$new_fgid=$content['Fieldguide_id']->name;



					$reporting_to=$this->model->reporting_to($staff_details->reportingto);
					$supervisior=$reporting_to->name;

					$getHRunitDetails=$this->model->getHRunitDetails();
					$hr_name=$getHRunitDetails->name;

				}


				$RequestMethod = $this->input->server('REQUEST_METHOD');

				if($RequestMethod == 'POST'){

					$this->db->trans_start();

					$this->form_validation->set_rules('team','Team ','trim|required');

					$this->form_validation->set_rules('fieldguide','Field Guide ','trim|required');
					
					$this->form_validation->set_rules('oldfg','Old Field Guide ','trim|required');

					if($this->form_validation->run() == FALSE){
						$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',	'</div>');

						$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

						$hasValidationErrors    =    true;
						goto prepareview;

					}	
					$newfieldguide='';
					$newfieldguide=$this->model->New_Fieldguidename($this->input->post('fieldguide'));
					$newfieldname='';

					$newfieldname=$newfieldguide->name;

					$insertArr = array(
						'id'               => $autoincval,
						'staffid'          => $staff_details->staffid,
						'old_office_id'    => $staff_details->new_office_id,
						'old_designation'  => $staff_details->designation,
						'new_designation'  => $staff_details->designation,
						'new_office_id'    => $staff_details->new_office_id,
						'date_of_transfer' => date("Y-m-d"),
						'trans_status'     => 'CFG',
						'datetime'         => date("Y-m-d H:i:s"),
						'fgid'     		   => $this->input->post('fieldguide'),
						'createdon'        => date("Y-m-d H:i:s"),
						'createdby'        => $this->loginData->staffid,
						'trans_flag'       => 1,

					);
				
					$this->db->insert('staff_transaction', $insertArr);	
						
					$oldfg=$this->input->post('oldfg');

					$staffarrupdate = array(
						'fgid' => $this->input->post('fieldguide'),
					);
					
					$this->db->where('staffid',$token);
					$this->db->update('staff',$staffarrupdate);


					$Arrupdate = array(
						'fgid'=> $this->input->post('fieldguide'),
					);
					
					$this->db->where('candidateid',$candidateid);
					$this->db->update('tbl_candidate_registration',$Arrupdate);

					$arrupdate = array(
						'fgid'=> $this->input->post('fieldguide'),
					);

					$this->db->where('candidateid',$candidateid);
					$this->db->update('tbl_da_personal_info',$arrupdate);

					$d_o_j = date('F j,Y');

					//email
					$staff1 = array('$supervisior','$newfieldname','$candidate_name','$hrname');
					$staff1_replace = array($supervisior,$newfieldname,$candidate_name,$hrname);

					//letter
					$staff = array('$staff_details->id','$d_o_j','$staff_details->name','$staff_details->permanenthno','$staff_details->permanentstreet','$staff_details->state_name','$staff_details->permanentcity','$staff_details->permanentpincode','$newfieldguide->name','$old_Fieldguide->name','$offer_no','$offer_date[0]','$joining_date','$hr_name');

					$staff_replace = array($staff_details->id,$d_o_j,$staff_details->name,$staff_details->permanenthno,$staff_details->permanentstreet,$staff_details->state_name,$staff_details->permanentcity,$staff_details->permanentpincode,$newfieldguide->name,$old_Fieldguide->name,$offer_no,$offer_date[0],$joining_date,$hr_name);

					$this->db->trans_complete();

				    if ($this->db->trans_status() === FALSE){

				     $this->session->set_flashdata('er_msg', 'Error !!! Something went wrong while creating Event ');  

				    }else{

					// $html='
					// <body>
					// <table width="100%" border="0" cellspacing="0" cellpadding="0">
					// <tr>
					// <td width="4%">&nbsp;</td>
					// <td colspan="4">&nbsp;</td>
					// <td width="3%">&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td width="28%"><p>'.$staff_details->id.'</p></td>
					// <td width="17%">&nbsp;</td>
					// <td width="17%">&nbsp;</td>
					// <td width="31%"><p>'.$d_o_j.'</p></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td><p>'.$staff_details->name.'<br />
					// '.$staff_details->name.$staff_details->permanenthno.$staff_details->permanentstreet.$staff_details->state_name.'<br /> Dist: 
					// '.$staff_details->permanentcity.'-'.$staff_details->permanentpincode.'</p></td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4"><p align="center"><em>Subject. </em>Change of Field Guide letter</p></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td><p>'.$staff_details->name.',</p></td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4">&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4"><p>This is to inform you that on account o&pound; transfer of your Field Guide '.$old_Fieldguide->name.',  your designated Field Guide will be '.$newfieldguide->name.' Pradhan,  Team Coordinator with mmediate  effect.</p></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4">&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4"><p>All the perspective and terms set out in your offer  letter ' .$offer_no.' dated '.$offer_date[0].' for joining on '.$joining_date.' will continue.</p></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4">&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4"><p>Looking forward to a long  assoclation with you,</p></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4">&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td colspan="4">&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td><div align="right">Yours sincerely,</div></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td><p align="right">'.$hr_name.'</p></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td><div align="right">Team  Coordinator</div></td>
					// <td>&nbsp;</td>
					// </tr>
					// <tr>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// <td>&nbsp;</td>
					// </tr>
					// </table>

					// </body>';


						
					$subject='Change of Field Guide Letter';
					// $fd = fopen("mailtext/change_fieldguide.txt","r");	
					$message='';
					// $message .=fread($fd,4096);
					// eval ("\$message = \"$message\";");
					// $message =nl2br($message);

					//email 
					$sql1 = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 27 AND `isactive` = '1'";
           		    $data1 = $this->db->query($sql1)->row();
   					if(!empty($data1))
   		    		$message = str_replace($staff1,$staff1_replace , $data1->lettercontent);

					//letter to generate pdf 
					$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 28 AND `isactive` = '1'";
            		$data = $this->db->query($sql)->row();
   					if(!empty($data))
   		    		$body = str_replace($staff,$staff_replace , $data->lettercontent);

					$filename = 'changefieldguide_'.$token.'_'.md5(time() . rand(1,1000));
					$this->load->model('Dompdf_model');
					$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ChangeFieldguide.pdf');
					if($generate ==true)

					$pdffilename = $filename.'.pdf';
					$attachments = array($pdffilename);
					$to_email = $reporting_to->emailid;
					
					$sendmail = $this->Common_Model->send_email($subject, $message, $to_email,$to_name = null,$recipients=null, $attachments);

					$this->session->set_flashdata('tr_msg', 'Successfully update FGC ');			
					
				 redirect('/staff_list/index');
				}
			}

				prepareview:

				$content['getcategoryid']    = $this->model->getCategoryid($token);

				$content['method']              = $this->router->fetch_method();
				$content['title']  = 'add';
				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);

				

			}

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


}