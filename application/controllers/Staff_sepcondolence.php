<?php 
class Staff_sepcondolence extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		 $this->load->model('Staff_sepcondolence_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("Staff_approval_model");


		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token = null)
	{
		
		
	
			// echo "dsfsd";
			// die;
		// start permission 
		try{
			  if($token){
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
			// end permission   

				$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
				// print_r($content['staff_detail']); 
				$tdate = date('d/m/Y');
       $staffemp_code = $content['staff_detail']->emp_code;
     //  $name = $content['staff_detail']->death_name;
       $staffname = $content['staff_detail']->name;

				 $staffname=$content['staff_detail']->name;
				 $incharger_name= $this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
				 // echo $staffname;
				 // die;
				$staff = array('$staff_name','$tdate','$emp_code','$name','$incharger_name');

       $staff_replace = array($staffname,$tdate,$staffemp_code,$staffname,$incharger_name);
       
       $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 89 AND `isactive` = '1'";
       $data = $this->db->query($sql)->row();
				$query = "SELECT * FROM tbl_sep_releaseform WHERE transid = ".$token;
				

					
           

				
        $body='';
       if(!empty($data))

       $body = str_replace($staff,$staff_replace , $data->lettercontent);

				
				

					if(!empty($body))
					{
						$content['content'] = $body;
					}

					$RequestMethod = $this->input->server("REQUEST_METHOD");


					if($RequestMethod == 'POST')
					{
					 // print_r($this->input->post()); die();
						$db_flag = '';
						if($this->input->post('Save') == 'Save'){
							$db_flag = 0;
						}else if(trim($this->input->post('comments'))){
							$db_flag = 1;
						}
						$name = '';
						$name = $this->input->post('to_name');

					$subject = "Condolence Form Letter";
                        $html = 'Dear '.$staffname.',<br><br>';
                        $html .= 'I was devastated to hear the news of '.$staffname.'. Please know that my thoughts and prayers are with you. Please accept my sincere and heartfelt condolences at this most difficult of times and please pass these feelings onto your family. I was absolutely saddened to hear of '.$staffname.' passing.<br><br>';
                        $html .= 'We do hope that you make a smooth transition to your new assignment, and sincerely wish you the very best in your life.<br><br>';
                        $html .='With regards.<br>';
                        $html .=$incharger_name;
			            $to_email = $name;
			            $to_name = $content['staff_detail']->emailid;
			            //$to_cc = $getStaffReportingtodetails->emailid;
			            // $recipients = array(
			               // $personnel_detail->EmailID => $personnel_detail->UserFirstName,
			               // $content['staff_detail']->emailid => $content['staff_detail']->name
			               // ..
			            // );
			            $email_result = $this->Common_Model->send_email($subject, $html, $to_email,$to_name);
			            if (substr($email_result, 0, 5) == "ERROR") {
			              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
			            }
			            else{
			            	$this->session->set_flashdata('tr_msg','Condolence Send Successfully.');
						redirect('Staff_personnel_approval');
			            }



						// echo $generate;
						// die;
					/*echo "<pre>";
					print_r($forwardworkflowid);exit();*/
					// if(trim($this->input->post('comments'))){
					// 	// staff transaction table flag update after acceptance
					// 	$updateArr = array(                         
					// 		'trans_flag'     => 15,
					// 		'updatedon'      => date("Y-m-d H:i:s"),
					// 		'updatedby'      => $this->loginData->staffid
					// 	);

					// 	$this->db->where('id',$token);
					// 	$this->db->update('staff_transaction', $updateArr);

					// 	$insertworkflowArr = array(
					// 		'r_id'                 => $token,
					// 		'type'                 => 3,
					// 		'staffid'              => $content['staffid'],
					// 		'sender'               => $this->loginData->staffid,
					// 		'receiver'             => $content['getexecutivedirector'][0]->edstaffid,
					// 		'forwarded_workflowid' => $forwardworkflowid,
					// 		'senddate'             => date("Y-m-d H:i:s"),
					// 		'flag'                 => 15,
					// 		'scomments'            => $this->input->post('comments'),
					// 		'createdon'            => date("Y-m-d H:i:s"),
					// 		'createdby'            => $this->loginData->staffid,
					// 	);
					// 	$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
					// }

				// 	$insertArray = array(

				// 		'staffid'        => $content['staff_sepreleasefrom_service']->staffid,
				// 		'transid'        => $token,
				// 		'pdr_no'         => $this->input->post('pdr_no'),
				// 		'letter_no'      => $this->input->post('letter_no'),
				// 		'letter_date'    => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('letter_date')),
				// 		'wef_date'       => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('wef_date')),
				// 		'relieved_date ' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('relieved_date')),
				// 		'flag'           => $db_flag,
				// 		'createdby'      => $this->loginData->staffid,
				// 		'updatedon'		 => date('Y-m-d H:i:s'),
    //     				'createdon'		 => date('Y-m-d H:i:s')
				// 	);

				// 	if($content['release_detail'])
				// 	{
				// 		$updateArray = array(

				// 			'staffid'        => $content['staff_sepreleasefrom_service']->staffid,
				// 			'transid'        => $token,
				// 			'pdr_no'         => $this->input->post('pdr_no'),
				// 			'letter_no'      => $this->input->post('letter_no'),
				// 			'letter_date'    => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('letter_date')),
				// 			'wef_date'       => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('wef_date')),
				// 			'relieved_date ' => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('relieved_date')),
				// 			'filename'		 => $filename,
				// 			'content'		 => $body,
				// 			'flag'           => $db_flag,
				// 			'updatedby'      => $this->loginData->staffid,	
				// 			'updatedon'		 => date('Y-m-d H:i:s'),
    //     					'createdon'		 => date('Y-m-d H:i:s')
				// 		);

				// 		$this->db->where('id', $content['release_detail']->id);
				// 		$flag = $this->db->update('tbl_sep_releaseform', $updateArray);
				// 	}else{
				// 		$flag = $this->db->insert('tbl_sep_releaseform',$insertArray);
				// 	}
				// 	//echo $this->db->last_query(); die;tbl_sep_releaseform

				// 	if($flag) 
				// 	{
				// 		$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
				// 		// redirect(current_url());
				// 	} 
				// 	else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				// 	// redirect(current_url());
				// }



			}
			//die;
				/*echo "<pre>";
				print_r($content['release_detail']);exit();*/
			$content['token'] = $token;
			$content['title'] = 'Staff_sepcondolence';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}else{
			$this->session->set_flashdata('er_msg','Missing trans id!!');
        	header("location:javascript://history.go(-1)", 'refresh');
		}
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}


}