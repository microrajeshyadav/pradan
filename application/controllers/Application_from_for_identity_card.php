<?php 

/**
* State List
*/
class Application_from_for_identity_card extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');

    $this->load->model('Application_from_for_identity_card_model');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $this->load->model('Employee_particular_form_model');
    $check = $this->session->userdata('login_data');
    $this->load->model("General_nomination_and_authorisation_form_model");
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index($staff,$candidate_id)
 {
  try{

   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    redirect('/staff_dashboard/index');
    
  } else {

   $staff = $this->uri->segment(3);
                       // $joining_staff=$staff;
   $candidate_id  = $this->uri->segment(4);

   $content['staff'] = $staff;
   $content['candidate_id'] = $candidate_id;

  //$staffids=($this->loginData->staffid);
  //$candidateid=($this->loginData->candidateid);

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);


   $staff_id=$this->loginData->staffid;
   @$candidateid=$this->loginData->candidateid;

   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }


 $content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
 $content['report']=$this->Application_from_for_identity_card_model->staff_reportingto($staff_id);

 $content['candidateaddress'] = $this->Application_from_for_identity_card_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Application_from_for_identity_card_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);
 
 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Application_from_for_identity_card_model->tc_email($reportingto);
 


 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

 $query332="SELECT * from tblidentitycard where candidate_id=$candidateid";
 $content['list_staff_tblidentitycard_table']=$this->db->query($query332)->row();

 if($content['list_staff_tblidentitycard_table'])

   $var=$content['list_staff_tblidentitycard_table']->flag;

 else $var= '';


 if ($var==null)
 {
  goto prepareviewss;
}

if($var==0)
{
  redirect('/Application_from_for_identity_card/edit/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
  redirect('/Application_from_for_identity_card/previewapplication/'.$staff.'/'.$candidateid);
}




prepareviewss:
$query1="Select officename from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();



$this->load->model('Application_from_for_identity_card_model');
$content['data_application']=$this->Application_from_for_identity_card_model->fetchdatas($candidateid);
  // print_r($content['data_application']); die;


$RequestMethod = $this->input->server('REQUEST_METHOD'); 
if($RequestMethod == 'POST')
{
  if (!empty($_FILES['Photo1']['name'])) {
    $fileData = $this->Application_from_for_identity_card_model->do_upload('Photo1');
    $updateArr['Photo1'] = $fileData['orig_name'];
    $updateArr['Photo1_encrypted_filename'] = $fileData['file_name'];
  }
  if (!empty($_FILES['Photo2']['name'])) {
    $fileData = $this->Application_from_for_identity_card_model->do_upload('Photo2');
    $updateArr['Photo2'] = $fileData['orig_name'];
    $updateArr['Photo2_encrypted_filename'] = $fileData['file_name'];
  }

  $ss=$this->input->post('dates');


  $this->load->model('Application_from_for_identity_card_model');
  $datess=$this->Application_from_for_identity_card_model->changedate($ss);

  $operation=$this->input->post('operation');

  if($operation == 0){

   $insertArr = array(

     'dear'         => $this->input->post('dear'),
     'photo1'       => $updateArr['Photo1_encrypted_filename'],
     'photo2'       => $updateArr['Photo2_encrypted_filename'],
     'candidate_id' => $candidateid,
     'createdon'    => date('Y-m-d H:i:s'),
     'createdby'    => $this->loginData->UserID,
     'date'         => $datess,
     'flag'         => 0

   );
   // echo "<pre>";
   // print_r($insertArr); die;
   $this->db->insert('tblidentitycard', $insertArr);
   $insertid = $this->db->insert_id();

   $insert_data =array(
    'type'      => 12,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'  => $staff_id
  );

   $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully added  Application for identity card form');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Application_from_for_identity_card/edit/'.$staff.'/'.$candidateid);


 } else {

   $insertArr = array(

    'dear'         =>  $this->input->post('dear'),
    'photo1'       =>  $updateArr['Photo1_encrypted_filename'],
    'photo2'       =>  $updateArr['Photo2_encrypted_filename'],
    'candidate_id' =>  $candidateid,
    'createdon'    =>  date('Y-m-d H:i:s'),
    'createdby'    =>  $this->loginData->UserID,
    'date'         =>  $datess,
    'flag'         =>  1

  );

   $this->db->insert('tblidentitycard', $insertArr);
   $insertid = $this->db->insert_id();

   $insert_data = array(
    'type'      => 12,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'  => $staff_id
  );

   $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully added  Application for identity card form');
   $this->session->set_flashdata('er_msg', $this->db->error());


   $subject = ': Application for identity card form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Application for identity card form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('/Application_from_for_identity_card/previewapplication/'.$staff.'/'.$candidateid);

 }

}

prepareview:


$content['title'] = 'Application_from_for_identity_card'.$staff.'/'.$candidateid;
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}







public function Edit($staff,$candidate_id)
{
  try{
 
  if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    redirect('/staff_dashboard/index');
    
  } else {

    $staff = $this->uri->segment(3);
    $candidate_id  = $this->uri->segment(4);

    $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id;

    $this->session->set_userdata('staff', $staff);
    $this->session->set_userdata('candidate_id', $candidate_id);

    $staff_id=$this->loginData->staffid;
    $candidateid=$this->loginData->candidateid;


    if(isset($staff_id)&& isset($candidateid))
    {

      $staff_id=$staff_id;
      $candidateid=$candidateid;

    }
    else
    {
     $staff_id=$staff;
         //echo "staff".$staff_id;
     $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

   }



   $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
        // start permission 
   $query1="Select officename from tbl_table_office";
   $content['officename_list']=$this->db->query($query1)->row();
   $content['candidateaddress'] = $this->Application_from_for_identity_card_model->getCandidateWithAddressDetails($staff_id);
   $content['personnal_mail'] = $this->Application_from_for_identity_card_model->personal_email();
      // print_r($content['personnal_mail']); die;
   $personal_email=$content['personnal_mail']->EmailID;

   $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
   $content['tc_email'] = $this->Application_from_for_identity_card_model->tc_email($reportingto);
   $content['signature'] = $this->Common_Model->staff_signature($staff_id);
   
 //$staffids=($this->loginData->staffid);
 //$candidateid=($this->loginData->candidateid);


   


// $staffids=($this->loginData->staffid);
// $candidateid=($this->loginData->candidateid);

   $this->load->model('Application_from_for_identity_card_model');
   $content['data_application']=$this->Application_from_for_identity_card_model->fetchdatas($candidateid);
   $content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

   $query6="select * from tblidentitycard where candidate_id=$candidateid";
   $content['dataofidentity'] = $this->db->query($query6)->row();



   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST'){


    $this->form_validation->set_rules('dear','Dear','trim|required|min_length[1]|max_length[50]');
        //$this->form_validation->set_rules('status','Status','trim|required');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }


    if ($_FILES['Photo2']['name'] != NULL) {

      $data=$_FILES['Photo2'];


      $this->load->model('Application_from_for_identity_card_model');

      $fileDatas=$this->Application_from_for_identity_card_model->do_uploads2($data);

      $updateArr['Photo2'] = $fileDatas['orig_name'];
      $updateArr['Photo2_encrypted_filename'] = $fileDatas['file_name'];

    }
    else
    {
      $updateArr['Photo2_encrypted_filename']=$this->input->post('photo222');

    }

    if ($_FILES['Photo1']['name'] != NULL) {

      $data=$_FILES['Photo1'];


      $this->load->model('Application_from_for_identity_card_model');

      $fileDatas=$this->Application_from_for_identity_card_model->do_uploads3($data);

      $updateArr['Photo1'] = $fileDatas['orig_name'];
      $updateArr['Photo1_encrypted_filename'] = $fileDatas['file_name'];

    }
    else
    {
     $updateArr['Photo1_encrypted_filename']=$this->input->post('photo111');
   }

   $ss=$this->input->post('dates');


   $this->load->model('Application_from_for_identity_card_model');
   $datess=$this->Application_from_for_identity_card_model->changedate($ss);
//print_r($datess);

   $operation=$this->input->post('operation');

   if($operation == 0){

    $updateArr = array(

      'dear'      => $this->input->post('dear'),
      'photo1'    => $updateArr['Photo1_encrypted_filename'],
      'photo2'    => $updateArr['Photo2_encrypted_filename'],
      'updatedon' => date('Y-m-d H:i:s'),
      'updatedby' => $this->loginData->UserID,
      'date'      => $datess,
      'flag'      => 0
    );

    $this->db->where('candidate_id', $candidateid);
    $this->db->update('tblidentitycard', $updateArr);
    $this->session->set_flashdata('tr_msg', 'Successfully Save Application for identity card form');
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect('/Application_from_for_identity_card/edit/'.$staff.'/'.$candidateid);


  } else {


   $updateArr = array(

    'dear'         => $this->input->post('dear'),
    'photo1'       => $updateArr['Photo1_encrypted_filename'],
    'photo2'       => $updateArr['Photo2_encrypted_filename'],
    'candidate_id' => $candidateid,
    'updatedon'    => date('Y-m-d H:i:s'),
    'updatedby'    => $this->loginData->UserID,
    'date'         => $datess,
    'flag'         => 1

  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tblidentitycard', $updateArr);

   $this->session->set_flashdata('tr_msg', 'Successfully Updated Application for identity card form');
   $this->session->set_flashdata('er_msg', $this->db->error());
   
   $subject = ': Application for identity card form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Application for identity card form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('/Application_from_for_identity_card/previewapplication/'.$staff.'/'.$candidateid);
 }
}
prepareview:
  // $this->load->model('Application_from_for_identity_card_model');
  // $content['datajoing']=$this->Application_from_for_identity_card_model->fetchdata($staffids);

$content['subview'] = 'Application_from_for_identity_card/edit';
$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function previewapplication($staff,$candidate_id)
{
  try{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  redirect('/staff_dashboard/index');
  
} else {

 $staff        = $this->uri->segment(3);
 $candidate_id = $this->uri->segment(4);

 $content['staff'] = $staff;
 $content['candidate_id'] = $candidate_id;
 
 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);


 $staff_id    = $staff;
 $candidateid = $candidate_id;
 $login_staff = $this->uri->segment(3);

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id= $staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

}

$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();

$content['id'] = $this->Application_from_for_identity_card_model->get_applicationworkflowid($staff_id);
// print_r($content['id']); die;

$content['candidatedetailwithaddress'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

$content['candidateaddress'] = $this->Application_from_for_identity_card_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Application_from_for_identity_card_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Application_from_for_identity_card_model->tc_email($reportingto);
$content['reporting'] = $this->Application_from_for_identity_card_model->getCandidateWith($reportingto);
$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;
$content['personal']=$this->Common_Model->get_Personnal_Staff_List();

if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
  $status   = $this->input->post('status');
  $reject   = $this->input->post('reject');
  $p_status = $this->input->post('p_status');
  $r_id     = $this->input->post('id');
  $check    = $this->session->userdata('insert_id');

  if($status==2){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }

  $insert_data =array(
    'type'                 => 12,
    'r_id'                 => $r_id,
    'sender'               => $this->loginData->staffid,
    'receiver'             => $p_status,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );
// print_r($insert_data); die;
  $this->db->insert('tbl_workflowdetail', $insert_data);


  $subject = ': Application for identity card form';
  $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.'Approved by Tc  </h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
  $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email =>'personal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('/Application_from_for_identity_card/previewapplication/'.$staff.'/'.$candidateid);

}


}
if($this->loginData->RoleID==17)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST')
 {
                               // print_r($_POST);die;
  $status   = $this->input->post('status');
  $reject   = $this->input->post('reject');
  $p_status = $this->input->post('p_status');
  $r_id     = $this->input->post('id');
  $check    = $this->session->userdata('insert_id');

  if($status==4){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }

  $insert_data = array(
    'type'                 => 12,
    'r_id'                 => $r_id,
    'sender'               => $this->loginData->staffid,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );

  $this->db->insert('tbl_workflowdetail', $insert_data);


  $subject = ': Application for identity card form';
  $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel </h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
  $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email =>'Personnal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('/Application_from_for_identity_card/previewapplication/'.$staff.'/'.$candidateid);
}


}

 // $staffids=($this->loginData->staffid);
  //$candidateid=($this->loginData->candidateid);



$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
$query1="Select officename from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();


$this->load->model('Application_from_for_identity_card_model');
$content['data_application']=$this->Application_from_for_identity_card_model->fetchdatas($candidateid);
//print_r($content['data_application']);//die();

$query6="select * from tblidentitycard where candidate_id=$candidateid";
$content['dataofidentity'] = $this->db->query($query6)->row();
  //print_r($content['dataofidentity']);

  //die("hello");

$content['subview'] = 'Application_from_for_identity_card/previewapplication';
$this->load->view('_main_layout', $content);
}
}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function getpdfapplication ($staff=null,$candidate_id=null)
{
  try{

  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);




  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }




 // $candidateid=($this->loginData->candidateid);
 $query1="Select * from tbl_table_office";
 $content['officename_list']=$this->db->query($query1)->row();
 $rows=$content['officename_list'];



 $this->load->model('Application_from_for_identity_card_model');
 $content['previewdata']=$this->Application_from_for_identity_card_model->pdfdata($candidateid);

 $row=$content['previewdata'];
 $timestamp = strtotime($rows->date);
 $datee=date('d/m/Y', $timestamp);

 $candidate = array('$rowsofficename','$rowdear','$rowcandidatefirstname','$rowcandidatelastname','$rowdesname','$rowemp_code','$rowbloodgroup','$datee');
 $candidate_replace = array($rows->officename,$row->dear,$row->candidatefirstname,$row->candidatelastname,$row->desname,$row->emp_code,$row->bloodgroup,$datee);

 // $html=' <div class="container">

 // <div class="row clearfix">
 // <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 // <div class="card">
 // <div class="body">
 // <div class="row clearfix doctoradvice">
 // <h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br>Preview:APPLICATION FORM FOR IDENTITY CARD</h4>

 // <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
 // <form method="POST" action=""  enctype="multipart/form-data">
 // <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
 // <div class="form-group">
 // <!--   <div class="form-line"> -->
 // <label for="ExecutiveDirector" class="field-wrapper required-field">The Executive Director,<br>
 // PRADAN.</label>
 // <br>
 // '.$rows->officename.'

 // </div>
 // </div>
 // <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
 // </div>
 // <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
 // </div>
 // <br>
 // <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 // <div class="form-group">
 // <label for="StateNameEnglish" class="field-wrapper required-field">Dear,&nbsp;</label>'. $row->dear.'</input>
 // </div>
 // </div>

 // <br>
 // <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
 // <div class="form-group">
 // I, Ms./Mr.&nbsp;<b>'.$row->candidatefirstname.'</b>
 // &nbsp;<b>'.$row->candidatelastname.'</b>&nbsp;


 // (Name),&nbsp;<b> '.$row->desname.'</b>&nbsp; (Designation)&nbsp;
 // <b>'.$row->emp_code.'</b>
 // (Employee Code), based at &nbsp;<b>'. $row->officename.'</b>&nbsp;(Location)
 // &nbsp;would like you to issue an 

 // Identity card.My blood&nbsp;<b>'.$row->bloodgroup.'</b>&nbsp;group is Enclosed please find a 
 // copy of proof of blood group 

 // and two passport size photographs to process my request.
 // <br>

 // <p class="text-center" style="margin-right: 180px;">Photo1:

 // </p>

 // <p class="text-center" style="margin-right: 180px;">Photo2:
 // </p>

 // </div>
 // </div>


 // <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
 // <p class="text-left">Signature: 


 // </p>
 // <p class="text-left">Name:&nbsp;<b>&nbsp;'. $row->candidatefirstname.'</b>
 // &nbsp;<b>'.$row->candidatelastname.'&nbsp;</b></p>
 // <p style="float: left">Date: &nbsp;&nbsp;&nbsp;&nbsp; 
 // <b>
 // '.$datee.'

 // </b>
 // </p>

 // </div>


 // </form>

 // </div>

 // </div>
 // </div>
 // </div>
 // </div>
 // </div>
 

 // ';

 $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 18 AND `isactive` = '1'";
 $data = $this->db->query($sql)->row();
 if(!empty($data))
  $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

$filename = "".$token."applications";
$this->Employee_particular_form_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');  


}catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}


}