<?php 
class Policy extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		//index include
		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		$this->load->model('Batch_model');
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
			$this->db->trans_start();
			 $name=$this->input->post('name_policy');
			//echo $_FILES['policy_upload']['name'];
			//print_r($_POST);
			if ($_FILES['policy_upload']['name'] != NULL) {

								@  $ext = end(explode(".", $_FILES['policy_upload']['name'])); 


								$OriginalexperiencedocumentName = $_FILES['policy_upload']['name']; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['policy_upload']['tmp_name'];
								$targetPath = FCPATH . "datafiles/policy/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encryptedpolicy_name= $encryptedexpdocumentName;
									//$Originalsalary_slip2=$_FILES['policy_upload']['name'];

								}else{

									die("Error uploading  ");
									$this->session->set_flashdata("er_msg", "Error policy");
									redirect(current_url());
								}

							}
									$insert_policy=array(
								'policy_name'=>$name,
								'docpath'=>$encryptedpolicy_name


							);
							//print_r($insert_policy);
							$this->db->insert('policy', $insert_policy);
							
							$this->db->trans_complete();
							if ($this->db->trans_status() === FALSE){
					$this->session->set_flashdata('er_msg', 'Error adding Policy');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully added Policy');	
					redirect('/policy/index');		
				}
				
		

						$month     = $this->input->post('month');
			$exp       = explode('/',$month);
			$MonthYear = $exp[1].'-'.$exp[0];
			// $this->session->set_userdata("month",$this->input->post('month'));
		}

		//$content['usedbatch'] = $this->model->UsedBatch();
		$content['policylist'] = $this->model->getpolicy();

		//$usedbatch = $this->model->UsedBatch();

		$content['title'] = 'Policy';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	
	public function add(){
				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				
				$this->db->trans_start();

				$this->form_validation->set_rules('batch','Batch','trim|required|min_length[1]|max_length[2]|numeric|is_unique[mstbatch.batch]');

				$this->form_validation->set_rules('dateofjoining','Date of joining','trim|required');
				$this->form_validation->set_rules('financialyear','Current Financial Year','trim|required');
				
				$this->form_validation->set_rules('status','Status','trim|required');

				if($this->form_validation->run() == FALSE){
					$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
						'</div>');

					$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

					$hasValidationErrors    =    true;
					goto prepareview;

				}

				$date_of_join = $this->input->post("dateofjoining");
				$date_of_joindate = date('Y-m-d', strtotime($date_of_join));
				$insertArrBatch = array (
						'batch'          => $this->input->post('batch'),
						'financial_year' => $this->input->post('financialyear'),
						'dateofjoining'  => $date_of_joindate,
						'status'         => $this->input->post('status'),
						'createdon'      => date('Y-m-d H:i:s'),
						'createdby'      => $this->loginData->UserID, // login user id
						'isdeleted'      => 0, 
					);
				$this->Common_Model->insert_data('mstbatch', $insertArrBatch);

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){
					$this->session->set_flashdata('er_msg', 'Error adding Batch');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully added Batch');			
				}
				redirect('/Batch/index');
				
			}

			prepareview:

			//$content['getfinancialyear'] = $this->fiscalYear();
			$content['getfinancialyear'] = $this->model->getfinancialyear();
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function edit($token){
				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		
		try{

			
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				
				$this->db->trans_start();

				//print_r($_FILE); die;

				// print_r($this->input->post()); die;
				 $plocy_name=$this->input->post('name_policy');

				if ($_FILES['policy_upload']['name'] != NULL) {
					echo $_FILES['policy_upload']['name'];
							@  $ext = end((explode(".", $_FILES['policy_upload']['name']))); 

//echo $ext; die; 
							$OriginalIdentityName = $_FILES['policy_upload']['name']; 
							$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['policy_upload']['tmp_name'];
							$targetPath = FCPATH . "datafiles/policy/";
							$targetFile = $targetPath . $encryptedIdentityName;
							//echo $targetFile; die;

							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$OriginalIdentityName = $_FILES['policy_upload']['name'];
							}else{

								die("Error uploading Identity Document !");
								$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
								redirect(current_url());
							}

						}else{
							$OriginalIdentityName      = $this->input->post('edit_policypath');
							
						}
						// echo $OriginalIdentityName;
						
						// die;
						$updateArray=array(
							'policy_name'=>$plocy_name,
							 'docpath'=>$encryptedIdentityName
						);

						$this->db->where("policy_id",$token);
				        $this->db->update('policy', $updateArray);

				       // echo $this->db->last_query(); die;
				

				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error update Policy');	
				}else{
					$this->session->set_flashdata('tr_msg', 'Successfully update Policy');			
				}
				redirect('/Policy/index');
				
			}

			prepareview:
			//$content['getfinancialyear'] = $this->fiscalYear();
			$content['edit_policy'] = $this->model->editpolicy($token);
			//print_r($content['edit_policy']);

			
			$content['title'] = 'edit.php';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


/////// Get Current Financial Year ///////////////////

	function fiscalYear()
	{
		try{

		if (date('m') > 6) {
			$year = date('Y')."-".(date('Y') +1);
		}
		else {
			$year = (date('Y')-1)."-".date('Y');
		}
		return $year;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	function delete($token)
	{
		try{
		
		$updatearray = array(
			'isdeleted' => 1,
		);
		
		$this->db->where('policy_id', $token);
		$this->db->delete('policy');
		//$this->db->update('mstbatch',$updatearray);
		$this->session->set_flashdata('tr_msg' ,"Policy Deleted Successfully");
		redirect('/policy/index/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


}