<?php 
class Developmentapprentice_sepclearancecertificate extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepclearancecertificate_model');
		
		$this->load->model('Developmentapprentice_sepclearancecertificate_model','model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_Model","Common_Model");
		$this->load->model("Staff_approval_model");
		$this->load->model("Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
		/*echo "<pre>";
		print_r($this->loginData);exit();*/

	}

	public function index($token)
	{
		// start permission 
		try{
				if($token){
					$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
					$content['role_permission'] = $this->db->query($query)->result();
					// end permission   
					$query ="SELECT * FROM staff_transaction WHERE id=".$token;
					$content['staff_transaction'] = $this->db->query($query)->row();
					$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
				  	$result  = $this->db->query($sql)->result()[0];
				  	$forwardworkflowid = $result->workflowid;		  
				  	/*$content['filnancelist'] = $this->Common_Model->get_finance_list();*/	
				  	$content['finance'] = $this->Common_Model->get_staff_finance_detail($content['staff_transaction']->new_office_id,20);	 

				  	$content['filnancedetail'] = $this->model->get_staffDetails($content['finance']->staffid);
				  	
				  	/*echo "<pre>";
				  	print_r($content['staff_transaction']);exit();*/
					$content['token'] = $token;
					$content['clearance_detail'] = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_clearance_detail($token);
					/*echo "<pre>";
					print_r($content['clearance_detail']);exit();*/
			
					$RequestMethod = $this->input->server("REQUEST_METHOD");
					if($RequestMethod == 'POST')
					{						
						$receiverdetail = $this->model->get_staffDetails($content['staff_transaction']->reportingto);
						$this->db->trans_start();
					if($this->input->post('btnsave') == 'Save'){
					$financedate = $this->gmodel->changedatedbformate($this->input->post('financedate'));
						$insertArray = array(
							'type'              => 27,
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $token,
							'financeid'         => $this->input->post('financeid'),
							'approvaldate'         => $financedate,
							'flag'              => 0,
						);
						// print_r($insertArray); die;
						$flag=$this->db->insert('tbl_da_clearance_certificate',$insertArray);

						$insertid = $this->db->insert_id();

						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
						for ($i=0; $i < $projectcount; $i++) { 
							
							$insertArrayTran = array(

							'clearance_certificate_id'  => $insertid,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
						$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$insertArrayTran);

						}

						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Clearance certificate form not submitted successfully');	
						}else{
							$this->session->set_flashdata('tr_msg', 'Clearance certificate form submitted successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate/edit/'.$insertid);			
						}



				}
				else if($this->input->post('btnsubmit') == 'Save And Submit')
				{
						$financedate = $this->gmodel->changedatedbformate($this->input->post('financedate'));
						$insertArray = array(

							'type'              => 27,
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $token,
							'financeid'         => $this->input->post('financeid'),
							'approvaldate'         => $financedate,
							'flag'              => 1,
						);
						// print_r($insertArray); die;
						$flag=$this->db->insert('tbl_da_clearance_certificate',$insertArray);
					

					
						$insertid = $this->db->insert_id();




	
						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
						for ($i=0; $i < $projectcount; $i++) { 
							
							$insertArrayTran = array(

							'clearance_certificate_id'  => $insertid,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
							
								$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$insertArrayTran);

						}

						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Clearance certificate form not submitted successfully');	
						}else{
							/*$comments = 'Clearance certificate form filled by Superviser';
							$updateArr = array(
					            'trans_flag'     => 5,
					            'updatedon'      => date("Y-m-d H:i:s"),
					            'updatedby'      => $this->loginData->staffid
					          );

							$this->db->where('id',$token);
							$this->db->update('staff_transaction', $updateArr);
							$insertworkflowArr = array(
								'r_id'           => $token,
								'type'           => 27,
								'staffid'        => $content['clearance_detail']->staffid,
								'sender'         => $this->loginData->staffid,
								'receiver'       => $content['staff_transaction']->reportingto,
								'senddate'       => date("Y-m-d H:i:s"),
								'flag'           => 5,
								'scomments'      => $comments,
								'createdon'      => date("Y-m-d H:i:s"),
								'createdby'      => $this->loginData->staffid,
							);
							$this->db->insert('tbl_workflowdetail', $insertworkflowArr);*/
							$subject = "Clearance certificate filled";
							$body = 'Dear '.$receiverdetail->name.',<br><br>';
							$body .= 'Clearance certificate request Submited Successfully for '.$content['clearance_detail']->name.'<br>';							
							$body .= 'Thanks,<br>';
							$body .= ''.$content['filnancedetail']->name.'<br>';
							$body .= 'PRADAN<br><br>';

							// $body .= 'Disclaimer<br>';
							// $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


							$to_email = $receiverdetail->emailid;
							// $to_name = $receiverdetail->name;
							//$to_cc = $getStaffReportingtodetails->emailid;
							// $recipients = array(
							// $content['clearance_detail']->emailid => $content['clearance_detail']->name
							// // ..
							// );
							// print_r($recipients); die();
							$email_result = $this->Common_Model->send_email($subject, $body, $to_email);
							if (substr($email_result, 0, 5) == "ERROR") {
								$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
							}
							$this->session->set_flashdata('tr_msg', 'Clearance certificate form submitted successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate/view/'.$insertid);			
						}

					}
					
					}


					$content['title'] = 'Developmentapprentice_sepclearancecertificate';
					$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
					$this->load->view('_main_layout', $content);
				}else{
					$this->session->set_flashdata('er_msg','Missing trans id!!');
    			header("location:javascript://history.go(-1)", 'refresh');
				}

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


public function view($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission   
			$clear_seperate_detail = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_sepration_clearance_detail($token);
			$transid = $clear_seperate_detail->transid;
			$query ="SELECT * FROM staff_transaction WHERE id=".$transid;
			$content['staff_transaction'] = $this->db->query($query)->row();
			$content['tcdetail'] = $this->model->get_staffDetails($content['staff_transaction']->reportingto);
			$content['finance'] = $this->Common_Model->get_staff_finance_detail($content['staff_transaction']->new_office_id,20);	 
			
			$content['filnancedetail'] = $this->model->get_staffDetails($content['finance']->staffid);	  
			$content['hrdetail'] = $this->model->get_staffDetails($content['staff_transaction']->createdby);
			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;
			$content['clearance_transaction'] = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_sepration_clearance_transaction($token);
			$content['clearance_detail'] = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_clearance_detail($transid);
			/*echo "<pre>";
			print_r($content['clearance_detail']);exit();*/			
			$RequestMethod = $this->input->server("REQUEST_METHOD");
			if($RequestMethod == 'POST')
			{
				if($this->input->post('btntcapprove') == 'Approve'){
					$superviserapprovaldate = $this->gmodel->changedatedbformate($this->input->post('superviserapprovaldate'));
					$dataset = array(
						'superviserapprovaldate'=>$superviserapprovaldate,
						'superviserid'=>$this->input->post('superviserid'),
						'updatedon' =>date("Y-m-d H:i:s"),
						'updatedby' => $this->loginData->staffid
					);
					$this->db->where('transid',$content['staff_transaction']->id);
					$flag = $this->db->update('tbl_da_clearance_certificate', $dataset);
					$subject = "Clearance certificate approved by Superviser";
					$body = 'Dear, '.$content['hrdetail']->name.'<br><br>';
					$body .= 'Clearance certificate request approved by '.$content['tcdetail']->name.'<br>';							
					$body .= 'Thanks,<br>';
					$body .= ''.$content['tcdetail']->name.'<br>';
					$body .= 'PRADAN<br><br>';

					// $body .= 'Disclaimer<br>';
					// $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


					$to_email = $content['hrdetail']->emailid;
					// $to_name = $content['hrdetail']->name;
					//$to_cc = $getStaffReportingtodetails->emailid;
					// $recipients = array(
					// $content['clearance_detail']->emailid => $content['clearance_detail']->name
					// // ..
					// );
					$email_result = $this->Common_Model->send_email($subject, $body, $to_email);
					if (substr($email_result, 0, 5) == "ERROR") {
						$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					}
				}else if($this->input->post('btnhrapprove') == 'Approve'){
					$hrapprovaldate = $this->gmodel->changedatedbformate($this->input->post('hrapprovaldate'));
					$dataset = array(
						'hrapprovaldate'=>$hrapprovaldate,
						'hrid'=>$this->input->post('hrid'),
						'updatedon' =>date("Y-m-d H:i:s"),
						'updatedby' => $this->loginData->staffid
					);
					$this->db->where('transid',$content['staff_transaction']->id);
					$flag = $this->db->update('tbl_da_clearance_certificate', $dataset);
					$subject = "Clearance certificate approved by HRD";
					$body = 'Dear '.$content['clearance_detail']->name.',<br><br>';
					$body .= 'Clearance certificate request approved by '.$content['hrdetail']->name.'<br><br>';							
					$body .= 'Thanks,<br>';
					$body .= ''.$content['hrdetail']->name.'<br>';
					$body .= 'PRADAN<br><br>';

					// $body .= 'Disclaimer<br>';
					// $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


					$to_email = $content['clearance_detail']->emailid;
					// $to_name = $content['clearance_detail']->name;
					//$to_cc = $getStaffReportingtodetails->emailid;
					/*$recipients = array(
					$content['clearance_detail']->emailid => $content['clearance_detail']->name
					// ..
					);*/
					$email_result = $this->Common_Model->send_email($subject, $body, $to_email);
					if (substr($email_result, 0, 5) == "ERROR") {
						$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					}
				}
				if ($flag){
					$this->session->set_flashdata('tr_msg', 'Approve !!! Process approved successfully');	
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Process not approved successfully');	
				}
			}
			$clear_seperate_detail = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_sepration_clearance_detail($token);
			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;
			$content['title'] = 'Staff_sepclearancecertificate';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}

	}



	public function edit($token){
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission   
			
			$clear_seperate_detail = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_sepration_clearance_detail($token);
			// print_r($clear_seperate_detail);
			// die;
			$query ="SELECT * FROM staff_transaction WHERE id=".$clear_seperate_detail->transid;
			$content['staff_transaction'] = $this->db->query($query)->row();
			/*$content['filnancelist'] = $this->Common_Model->get_finance_list();*/
			$content['finance'] = $this->Common_Model->get_staff_finance_detail($content['staff_transaction']->new_office_id,20);	 
			// echo "<pre>";
			// print_r($content['finance']); die(); 
			$content['filnancedetail'] = $this->model->get_staffDetails($content['finance']->staffid);

			//print_r($clear_seperate_detail); die;
			$transid = $clear_seperate_detail->transid;
			// echo $transid;
			// die;

			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;

			$content['clearance_transaction'] = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_sepration_clearance_transaction($token);
			// echo "<pre>";
			// print_r($content['clearance_transaction']);exit();

			$content['clearance_detail'] = $this->Developmentapprentice_sepclearancecertificate_model->get_staff_clearance_detail($transid);
			// print_r($content['clearance_detail']);
			// die;





					$RequestMethod = $this->input->server("REQUEST_METHOD");
					if($RequestMethod == 'POST')
					{

						$receiverdetail = $this->model->get_staffDetails($content['staff_transaction']->reportingto);
						$this->db->trans_start();

					
				if($this->input->post('btnsave') == 'Save'){
					$financedate = $this->gmodel->changedatedbformate($this->input->post('financedate'));
					$insertArray = array(

							'type'              => 27,
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $clear_seperate_detail->transid,
							'financeid'         => $this->input->post('financeid'),
							'approvaldate'         => $financedate,
							'flag'              => 0,
						);
						// print_r($insertArray); die;
					$this->db->where('id', $token);
						$flag=$this->db->update('tbl_da_clearance_certificate',$insertArray);

						//$insertid = $this->db->insert_id();

						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
					$this->db->delete('tbl_da_clearance_certificate_transaction',array('clearance_certificate_id'=>$token));
					
						for ($i=0; $i < $projectcount; $i++) { 
							
							$updateArrayTran = array(

							'clearance_certificate_id'  => $token,
							
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
							
								$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$updateArrayTran);

						}


						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully');	
						}else{
							$this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate/edit/'.$token);			
						}



				}
				else if($this->input->post('btnsubmit') == 'Save And Submit')
				{
					
						$financedate = $this->gmodel->changedatedbformate($this->input->post('financedate'));
						$insertArray = array(
							'type'              => 27,
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $clear_seperate_detail->transid,
							'financeid'         => $this->input->post('financeid'),
							'approvaldate'         => $financedate,
							'flag'              => 1,
						);
						// print_r($insertArray); die;
						$this->db->where('id', $token);
						$flag=$this->db->update('tbl_da_clearance_certificate',$insertArray);
					

					
						//$insertid = $this->db->insert_id();




	
						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
					$this->db->delete('tbl_da_clearance_certificate_transaction',array('clearance_certificate_id'=>$token));

						for ($i=0; $i < $projectcount; $i++) { 
							
							$updateArrayTran = array(

							'clearance_certificate_id'  => $token,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
							
								$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$updateArrayTran);

						}

						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully');	
						}else{
							$comments = 'Clearance certificate form filled by Superviser';
							/*$updateArr = array(
					            'trans_flag'     => 5,
					            'updatedon'      => date("Y-m-d H:i:s"),
					            'updatedby'      => $this->loginData->staffid
					          );

							$this->db->where('id',$clear_seperate_detail->transid);
							$this->db->update('staff_transaction', $updateArr);
							$insertworkflowArr = array(
								'r_id'           => $clear_seperate_detail->transid,
								'type'           => 27,
								'staffid'        => $content['clearance_detail']->staffid,
								'sender'         => $this->loginData->staffid,
								'receiver'       => $content['staff_transaction']->reportingto,
								'senddate'       => date("Y-m-d H:i:s"),
								'flag'           => 5,
								'scomments'      => $comments,
								'createdon'      => date("Y-m-d H:i:s"),
								'createdby'      => $this->loginData->staffid,
							);
							$this->db->insert('tbl_workflowdetail', $insertworkflowArr);*/
							$subject = "Clearance certificate filled";
							$body = 'Dear, '.$receiverdetail->name.'<br><br>';
							$body .= '<h2>Clearance certificate request Submited Successfully for '.$content['clearance_detail']->name.'</h2><br>';							
							$body .= 'Thanks,<br>';
							$body .= ''.$content['filnancedetail']->name.'<br>';
							$body .= 'PRADAN<br><br>';

							$body .= 'Disclaimer<br>';
							$body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


							$to_email = $receiverdetail->emailid;
							$to_name = $receiverdetail->name;
							//$to_cc = $getStaffReportingtodetails->emailid;
							$recipients = array(
							$content['clearance_detail']->emailid => $content['clearance_detail']->name
							// ..
							);
							$email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
							if (substr($email_result, 0, 5) == "ERROR") {
								$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
							}
							$this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate/view/'.$token);			
						}

					}
				}
	




			
			/*$RequestMethod = $this->input->server("REQUEST_METHOD");
			if($RequestMethod == 'POST')
			{
			
			}*/












			$content['title'] = 'Staff_sepclearancecertificate/edit';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


	}