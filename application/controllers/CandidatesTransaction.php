<?php 
class CandidatesTransaction extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		$this->load->model('Batch_model');
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
			 $month     = $this->input->post('month');
			 $exp       = explode('/',$month);
			 $MonthYear = $exp[1].'-'.$exp[0];
			// $this->session->set_userdata("month",$this->input->post('month'));
		}

		
		$query = "SELECT * FROM `mstbatch` WHERE `status`='0' and `isdeleted`='0'";


		$content['candidatestransactionlist'] = $this->Common_Model->query_data($query);

		$content['title'] = 'Batch';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}






	public function AddTransaction($token){

		try{

				// echo $token; 


			$content['title'] = 'addtransaction';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}


}
	
	public function add(){

		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
					$this->db->trans_start();

					$insertArrBatch = array(
						'batch '    			=> $this->input->post('batch'),
						'dateofjoining'       	=> $this->input->post("dateofjoining"),
						'status'  			    => $this->input->post('status'),
						'createdon'      	    => date('Y-m-d H:i:s'),
					    'createdby'      	    => 2, // login user id
					    'isdeleted'      	    => 0, 
					  );
					$this->Common_Model->insert_data('mstbatch', $insertArrBatch);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Application');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added Application');			
					}
					redirect('/Application/index');
			
			}


			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


	public function edit($token){
		
		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
					$this->db->trans_start();

					$updateArray = array(
						'batch '    			=> $this->input->post('batch'),
						'dateofjoining'       	=> $this->input->post("dateofjoining"),
						'status'  			    => $this->input->post('status'),
						'createdon'      	    => date('Y-m-d H:i:s'),
					    'createdby'      	    => 2, // login user id
					    'isdeleted'      	    => 0, 
					  );

					$this->db->where("id",$token);
			        $this->db->update('mstbatch', $updateArray);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error update Batch');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully update Batch');			
					}
					redirect('/Application/index');
			
			}

			$query = " SELECT * FROM `mstbatch` WHERE `status`='0' and `isdeleted`='0' and id=".$token."";
			$content['batchupdate'] = $this->Common_Model->query_data($query);

			$content['title'] = 'edit.php';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}



	function delete($token = null)
	{
		try{
		$this->Common_Model->delete_row('mstbatch','id', $token); 
		$this->session->set_flashdata('tr_msg' ,"Campus Deleted Successfully");
		redirect('/Batch/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


}