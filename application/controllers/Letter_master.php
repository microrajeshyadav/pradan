<?php 

/**
* Letter Master List
*/
class Letter_master extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }
    $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
    try{
     
    // start permission 
    
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 




    $query = "select * from mst_letter_master where isdeleted='1'";
    $content['letter_details'] = $this->Common_Model->query_data($query);
    $content['subview']="index";

    $content['title'] = 'Letter_master';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
    
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('statecode','State Code','trim|required|min_length[1]|max_length[2]|numeric');
        $this->form_validation->set_rules('statename','State Name','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
        'name'         => $this->input->post('statename'),
        'type'     => $this->input->post('statecode'),
        'path'      => $this->input->post('statecode'),
        'created_date' => date('Y-m-d H:i:s'),
        'IsDeleted'    => $this->input->post('status'),
        );
      
      $this->Common_Model->insert_data('mst_letter_master', $insertArr);
      $this->session->set_flashdata('tr_msg', 'Successfully added letter master !!!');
      redirect('/Letter_master/');
    }

    prepareview:

    $content['title']   = 'Letter_master';
    $content['subview'] = 'Letter_master/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  public function edit($id)
  {
    try{
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

     
        $this->form_validation->set_rules('lettername','Letter Name','trim|required');
        $this->form_validation->set_rules('path','File Path','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

      $updateArr = array(
        'type'           => $this->input->post('lettername'),
        'file_path'      => $this->input->post('path'),
        'updatedon'      => date('Y-m-d H:i:s'),
        'updatedby'      => $this->loginData->Userid,
        );
      
      $this->Common_Model->update_data('mst_letter_master', $updateArr,'id',$id);
      $this->session->set_flashdata('tr_msg', 'Successfully Updated letter');
       redirect('/Letter_master/');
    }

    prepareview:

    $content['title'] = 'Letter_master';
    $letter_details = $this->Common_Model->get_data('mst_letter_master', '*', 'id',$id);
    $content['letter_details'] = $letter_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
    function delete($token)
    {
      try{

        $updateArr = array(
        'isdeleted' => 0,
       );
      
      $this->Common_Model->update_data('mst_letter_master', $updateArr,'id',$token);
      $this->session->set_flashdata('tr_msg' ,"Letter Deleted Successfully");
      redirect('/Letter_master/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);

      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}