<?php 

/**
* State List
*/
class Applyforleave extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     try{
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   // $query = "select * from dsd where isdeleted='0'";
    $content['mstpgeducation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
    $content['title'] = 'Applyforleave';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'pgname'      => $this->input->post('pgname'),
          'status'      => $this->input->post('status')
        );
      
      $this->db->insert('mstpgeducation', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
      $this->session->set_flashdata('er_msg', $this->db->error());
      redirect('/Pgeducation/index/');
    }

    prepareview:

   
    $content['subview'] = 'Pgeducation/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

 public function applyforleaves()
 {
  try{
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    $query="SELECT Ltypename FROM msleavetype ";
     $content['ltype_details'] = $this->Common_Model->query_data($query);
 

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('fromdate','From Date','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('todate','To Date','trim|required');
        $this->form_validation->set_rules('types','Types','trim|required');
        $this->form_validation->set_rules('Days','Types','trim|required');
         $this->form_validation->set_rules('Reason','Types','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        

      // $insertArr = array(
      //     'pgname'      => $this->input->post('fromdate')
      //     'pgname'      => $this->input->post('todate'),
      //     'pgname'      => $this->input->post('types'),
      //     'pgname'      => $this->input->post('Days'),
      //     'pgname'      => $this->input->post('Reason')
          
      //   );
      $this->db->insert('', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
      $this->session->set_flashdata('er_msg', $this->db->error());
      redirect('/Applyforleave/index/');
      }

     prepareview:

    $content['subview'] = 'Applyforleave/addapply';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }

 }

   public function monthlyleavedatafrezzing()
 {
  try{
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();



    $content['subview'] = 'Applyforleave/monthlyleavedata';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }


}