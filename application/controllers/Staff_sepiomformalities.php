<?php 
class Staff_sepiomformalities extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepiomformalities_model');
		$this->load->model("Staff_sepcacceptanceofresignation_model","Staff_sepcacceptanceofresignation_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");
		$this->load->model("Global_model","gmodel");
		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			  if($token){

				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
				// end permission    
				$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
			    $result  = $this->db->query($sql)->result()[0];
			    $forwardworkflowid = $result->workflowid;
				$query ="SELECT * FROM staff_transaction WHERE id=".$token;
				$content['staff_sepformality'] = $this->db->query($query)->row();

				// echo "<pre>";
				// print_r($content['staff_sepformality']);exit();


				$staffid =  $content['staff_sepformality']->staffid;

				$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
				// print_r($content['staff_detail']);
				// die;
				$reportingto=$content['staff_detail']->reportingto;
				$officeid=$content['staff_detail']->new_office_id;
				$finance=$this->Common_Model->get_staff_finance_detail($officeid,20);
				
				$content['staffid'] = $content['staff_sepformality']->staffid;

				$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
	        	$receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['staff_sepformality']->reportingto);
	        	$content['acceptance_detail'] = $this->Staff_sepcacceptanceofresignation_model->acceptance_detail($token);

				$query = "SELECT * FROM tbl_sepformality WHERE transid = ".$token;
				$content['formality_detail'] = $this->db->query($query)->row();


				$RequestMethod = $this->input->server("REQUEST_METHOD");


				if($RequestMethod == 'POST')
				{

					$db_flag = '';
					if($this->input->post('Save') == 'Save'){
						$db_flag = 0;
					}else if(trim($this->input->post('comments'))){
						$db_flag = 1;
					}
			
					$this->db->trans_start();

					$insertArray = array(

						'staffid'     => $content['staff_sepformality']->staffid,
						'transid'     => $token,
						'createdby'   => $this->loginData->staffid,
						'flag'        => $db_flag,
					);
					// print_r($insertArray); die;

					if($content['formality_detail'])
					{
						$updateArray = array(

							'staffid'     => $content['staff_detail']->staffid,
							'transid'     => $token,
							'updatedby'   => $this->loginData->staffid,
							'flag'        => $db_flag,
						);
						$this->db->where('id', $content['formality_detail']->id);
						$flag = $this->db->update('tbl_sepformality', $updateArray);
					}else{
						$flag = $this->db->insert('tbl_sepformality',$insertArray);
					}
					if(trim($this->input->post('comments'))){
						// staff transaction table flag update after acceptance
						$updateArr = array(                         
			                'trans_flag'     => 11,
			                'updatedon'      => date("Y-m-d H:i:s"),
			                'updatedby'      => $this->loginData->staffid
			            );

			            $this->db->where('id',$token);
			            $this->db->update('staff_transaction', $updateArr);
			            
			            $insertworkflowArr = array(
			             'r_id'                 => $token,
			             'type'                 => 3,
			             'staffid'              => $content['staff_sepformality']->staffid,
			             'sender'               => $this->loginData->staffid,
			             'receiver'             => $content['staff_detail']->staffid,
			             'forwarded_workflowid' => $forwardworkflowid,
			             'senddate'             => date("Y-m-d H:i:s"),
			             'flag'                 => 11,
			             'scomments'            => $this->input->post('comments'),
			             'createdon'            => date("Y-m-d H:i:s"),
			             'createdby'            => $this->loginData->staffid,
			            );
			            					
				// echo "<pre>";
				// print_r($insertworkflowArr);exit();
				        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				        // ed signature
				        $getedname = '';
				        $getedname = $this->gmodel->getExecutiveDirectorEmailid();
				        $edsign = '';
				        if ($getedname->sign !='') {
				        	$edsign = site_url('datafiles/signature/'.$getedname->sign);
				        }else{ 
				        	$edsign = site_url('datafiles/signature/Signature.png');
				        }

				        $resignation_tendered_on = $content['acceptance_detail']->resignation_tendered_on;
				        $working_date = $content['acceptance_detail']->working_date;

				        $tdate = date('d/m/Y');
				        $staffemp_code = $content['staff_detail']->emp_code;
				        $staffadd = $content['staff_detail']->address;
				        $staffname = $content['staff_detail']->name;
				        $resignation_tendered_on = $this->gmodel->changedatedbformate($resignation_tendered_on);
				        $working_date = $this->gmodel->changedatedbformate($working_date);

						$staff = array('$edsign','$staffname','$resignation_tendered_on','$working_date','$tdate','$staffemp_code','$staffadd','$staffname');
				        $staff_replace = array($edsign,$staffname,$resignation_tendered_on,$working_date,$tdate,$staffemp_code,$staffadd,$staffname);
				        
				        $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 83 AND `isactive` = '1'";
				        $data = $this->db->query($sql)->row();
				       
				        $body='';
				        if(!empty($data))

				        $body = str_replace($staff,$staff_replace , $data->lettercontent);
				        $body = str_replace('<img src="','<img src="'.$edsign, $body); 

				        $body=  html_entity_decode($body);

				        $filename = "";
				        $filename = md5(time() . rand(1,1000));
				        $this->load->model('Dompdf_model');
				        $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'AcceptanceOfSeparation.pdf');
				        $attachments = array($filename.'.pdf'); 

				        // $subject = "Your Process Approved";
				        // $body = 'Dear,<br><br>';
				        // $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
				        // $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your process has Approved  .<br><br>';
				        // $body .= 'Thanks<br>';
				        // $body .= 'Personnel Unit<br>';
			         //    $body .= 'PRADAN<br><br>';

			         //    $body .= 'Disclaimer<br>';
			         //    $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

				       	$subject = "Separation formalities";
                        $html = 'Dear '.$staffname.',<br><br>';
                        $html .= '<p>File  :  301- '.$content['staff_detail']->emp_code.'/PDR</p><br>';
                        $html .= '<br><br>
                        As per our separation procedure, when someone separates from PRADAN we will deactivate her/his PRADAN email account. We will be deactivating your account on next day of your last working day. Hence request you to save all your important emails, as after deletion of account, emails cannot retrieve. Please inform us after saving your important emails and share your alternate email ID, contact number and complete correspondence address for future communication.<br><br>
                        Handover PRADAN ID card to your supervisor. Fill up and send relevant attached applications.
                        <br><br/>
                        Please send us enclosed check sheet, clearance certificate, exit interview form and handover format for further process.<br><br>
                            <b>The Check sheet and Clearance should be forwarded to Personnel Unit only after the clearance at the level of team and employee and update of PAFS and leave MIS</b>.<br><br>';

			            // $to_email = $receiverdetail->emailid;
			            // $to_name = $receiverdetail->name;
			            //$to_cc = $getStaffReportingtodetails->emailid;
			            // $recipients = array(
			            //    $personnel_detail->EmailID => $personnel_detail->UserFirstName,
			            //    $content['staff_detail']->emailid => $content['staff_detail']->name
			            //    // ..
			            // );
			            // $email_result = $this->Common_Model->send_email($subject, $html, $to_email, $to_name, $recipients,$attachments);
			            // if (substr($email_result, 0, 5) == "ERROR") {
			            //   $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
			            // }
			        }

					
					//echo $this->db->last_query(); die;
					$this->db->trans_complete();
					if($flag) 
					{
						$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
						// redirect(current_url());
					} 
					else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
					// redirect(current_url());
				}


			}

			$query = "SELECT * FROM tbl_sepformality WHERE transid = ".$token;
			$content['formality_detail'] = $this->db->query($query)->row();
			$content['token'] = $token;
			$content['title'] = 'Staff_sepiomformalities';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		  }else{
		  	$this->session->set_flashdata('er_msg','Missing trans id!!');
    		header("location:javascript://history.go(-1)", 'refresh');
		  }
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}


}