<?php 
class Campus extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Campus_model');
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index()
	{
		try{
		     // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission    

			$content['campuslist'] = $this->model->getCampus();
			$content['title'] = 'Campus';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	
	public function add(){
		try{

			    // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
			
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			
			if($RequestMethod == 'POST'){

				//print_r($this->input->post()); die;
				  $this->db->trans_start();

			    $this->form_validation->set_rules('campusincharge','Campus Incharge','trim|required');
	            $this->form_validation->set_rules('campusname','Campus Name','trim|required');
	            $this->form_validation->set_rules('emailid','Emailid','trim|required|valid_email|is_unique[mstcampus.emailid]');
	            // $this->form_validation->set_rules('emailid2','Emailid2','trim|required|valid_email|is_unique[mstcampus.emailid2]');
	            $this->form_validation->set_rules('city','City','trim|required');
	            $this->form_validation->set_rules('mobile1','Mobile','trim|required|max_length[10]|numeric');
	            $this->form_validation->set_rules('telephone','Telephone','trim|max_length[12]|numeric');
	            $this->form_validation->set_rules('address','Address ','trim|required|min_length[5]|max_length[150]');
	            $this->form_validation->set_rules('fax','Fax','trim|max_length[12]|numeric');
	            $this->form_validation->set_rules('stateid','State','trim|required');
	        
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
         
			$insertArr2 = array(
				'campusincharge'    => $this->input->post('campusincharge'),
				'campusincharge2'    => $this->input->post('campusincharge2'),
				'campusname'       	=> $this->input->post("campusname"),
				'emailid'  			=> $this->input->post('emailid'),
				'emailid2'  			=> $this->input->post('emailid2'),
				'city' 				=> $this->input->post('city'),
				'stateid'  			=> $this->input->post('stateid'),
				'telephone' 		=> $this->input->post('telephone'),
				'fax' 		        => $this->input->post('fax'),
				'mobile' 		    => $this->input->post('mobile1'),
				'mobile2' 		    => $this->input->post('mobile2'),
				'address ' 		    => $this->input->post('address'),
				'createdon'      	=> date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'      	=> $this->loginData->UserID, // login user id
			    'isdeleted'      	=> 0, 
			  );
			// echo "<pre>";
			// print_r($insertArr2); 
			// die;
			$this->db->insert('mstcampus', $insertArr2);
			//echo $this->db->last_query(); die;


			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding Campus');	
			redirect('/Campus/add');
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Campus');
			redirect('/Campus/index');			
			}
			
			
			}

			prepareview:

			$content['statedetails'] = $this->model->stateList();
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


	public function edit($token){
		
		try{
			  // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
			
			$RequestMethod = $this->input->server('REQUEST_METHOD'); //die;
			
			if($RequestMethod == 'POST'){
			
				$this->db->trans_start();

		   $this->form_validation->set_rules('campusincharge','Campus Incharge','trim|required');
	            $this->form_validation->set_rules('campusname','Campus Name','trim|required');
	            $this->form_validation->set_rules('emailid','Emailid','trim|required|valid_email');
	            // $this->form_validation->set_rules('emailid2','Emailid2','trim|required|valid_email');
	            $this->form_validation->set_rules('city','City','trim|required');
	            $this->form_validation->set_rules('mobile1','Mobile','trim|required|max_length[10]|numeric');
	            //$this->form_validation->set_rules('mobile2','Mobile','trim|required|max_length[10]|numeric');
	            $this->form_validation->set_rules('telephone','Telephone','trim|max_length[12]|numeric');
	            $this->form_validation->set_rules('address','Address ','trim|required|min_length[5]|max_length[150]');	            
	            $this->form_validation->set_rules('fax','Fax','trim|max_length[12]|numeric');
	            $this->form_validation->set_rules('stateid','State','trim|required');
	           
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }

	         

					$updateArray = array(
						'campusincharge'    => $this->input->post('campusincharge'),
						'campusincharge2'    => $this->input->post('campusincharge2'),
						'campusname'       	=> $this->input->post("campusname"),
						'emailid'  			=> $this->input->post('emailid'),
						'emailid2'  			=> $this->input->post('emailid2'),
						'city' 				=> $this->input->post('city'),
						'stateid'  			=> $this->input->post('stateid'),
						'telephone' 		=> $this->input->post('telephone'),
						'fax' 		        => $this->input->post('fax'),
						'mobile' 		    => $this->input->post('mobile1'),
						'mobile2' 		    => $this->input->post('mobile2'),
						'address ' 		    => $this->input->post('address'),
						'createdon'      	=> date('Y-m-d H:i:s'),    // Current Date and time
					        'createdby'      	=> $this->loginData->UserID, // login user id
					    'isdeleted'      	=> 0, 
					  );
				//print_r($updateArray); die;

					$this->db->where("campusid",$token);
			        $this->db->update('mstcampus', $updateArray);
				//echo $this->db->last_query(); die;

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error update Campus');	
                                              redirect('/Campus/edit'.$token);
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully update Campus');	
redirect('/Campus/index');		
					}
					
			
			}

			prepareview:

			$content['statedetails'] = $this->model->stateList();
			$content['campusupdate'] = $this->model->getCampusDetails($token);
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}
	

	// function delete($token = null)
	// {
	// 	$this->Model->delete_row('mstcampus','campusid', $token); 
	// 	$this->session->set_flashdata('tr_msg' ,"Campus Deleted Successfully");
	// 	redirect('/Campus/');
	// 	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	// 	$this->load->view('_main_layout', $content);
	// }

	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete($token)
	{
		try{
		$this->view['detail'] = $this->model->getCampusDetails($token);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		

		if($this->model->delete($token) == '1'){
			$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	 }

}