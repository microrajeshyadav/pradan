<?php 

/**
* State List
*/
class Operation extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from  mst_oprationtype where isdeleted='0'";
    $content['operation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    $content['title'] = 'Operation';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add()
  {

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('Name','name','trim|required|min_length[1]|max_length[50]');
     
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'name'      => $this->input->post('Name'),
          
          'createdon'      => date('Y-m-d H:i:s'),
          'createdby'      => $this->loginData->UserID,
          'status'      => $this->input->post('status')
        );
      
      $this->db->insert('mst_oprationtype', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added  Sectoral Specialization');
      redirect('/Operation/index/');
    }

    prepareview:

   
    $content['subview'] = 'Operation/add';
    $this->load->view('_main_layout', $content);
  }

  public function edit($token=NULL)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('name','name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
          'name'      => $this->input->post('name'),
          'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,
          'status'         => $this->input->post('status')
        );
      
      $this->db->where('id', $token);
      $this->db->update('mst_oprationtype', $updateArr);
 //echo  $this->db->last_query(); die;

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Sectoral Specialization');
       redirect('/Operation/');
    }

    prepareview:

    $content['title'] = 'Operation';
    $operation_details = $this->Common_Model->get_data('mst_oprationtype', '*', 'id',$token);
    $content['operation_details'] = $operation_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }
    function delete($token = null)
    {
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('id', $token);
      $this->db->update('mst_oprationtype', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Sectoral Specialization Deleted Successfully');
      redirect('/Operation/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
  }

}