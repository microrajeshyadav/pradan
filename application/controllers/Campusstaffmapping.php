<?php 
class Campusstaffmapping extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staffrecruitmentteammapping_model');
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
	      // start permission 
		
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

			$content['campuslist']   = $this->model->getCampus();
			$content['recuterslist'] = $this->model->getRecuters();
			$content['title'] = 'map';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	/**
	 * Method add() Add data. 
	 * @access	public
	 * @param	
	 * @return	array
	*/ 
	public function add(){

  // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

		
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

				$this->db->trans_start();


				//print_r($this->input->post()); die;

				$campusidwithintimationid = $this->input->post('campus');
				$explcampwithintimation = explode('-', $campusidwithintimationid);
				$campusid = $explcampwithintimation[0]; ////// campus id
				$campusintimationid  = $explcampwithintimation[1]; ////// schedule campus id
				//print_r($explcampwithintimation); die;

				$chackcamp = $this->is_campus($campusid, $campusintimationid);

				if ($chackcamp >0) {
					$this->session->set_flashdata('er_msg', 'Sorry !!! Recruiters allready maped, please choose other camp ');
		    		redirect(current_url());
				}



	            $count = count($this->input->post("recruiter"));

	          for ($i=0; $i < $count; $i++) { 

			$insertArr2 = array(
				'campusid'   	     => $campusid,
				'recruiterid'        => $this->input->post("recruiter")[$i],
				'anchor'        	 => $this->input->post("anchor")[$i],
				'campusintimationid' => $campusintimationid,
				'createdon'          => date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'          => $this->loginData->UserID, // login user id
			    'isdeleted'          => 0, 
			  );
			
			$this->db->insert('mapping_campus_recruiters', $insertArr2); 
			//$insertedid = $this->db->insert_id(); 
			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error mapping Recruiters');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Recruiters');			
			}
			redirect('/Campusstaffmapping/index');
			
			}



			$content['campuslist']		 	= $this->model->getCampus();
			$content['recruitmentlist'] 	= $this->model->getRecruitment();
			$content['title'] = 'map';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


   /**
	 * Method edit() Edit data. 
	 * @access	public
	 * @param	
	 * @return	array
	*/ 
	public function edit($token){

		  // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		
		try{



			 $campusintimationid    = $this->uri->segment(3);
			 $campusid 				= $this->uri->segment(4);
			 $token = $campusid.'-'.$campusintimationid;
	
			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
			
				$this->db->trans_start();

				//print_r($this->input->post()); die;

				
			  	 $count = count($this->input->post("recruiter"));

			//   	for ($i=0; $i < $count; $i++) { 

			//   		$recruitersid = $this->input->post("recruiter")[$i];
			//   		 $sql = "SELECT * FROM mapping_campus_recruiters WHERE `recruiterid`='".$recruitersid."' AND `campusintimationid`='".$campusintimationid."' AND `isdeleted`= 0 "; 
			//   		$query = $this->db->query($sql);
			//   		$numrows = $query->num_rows(); 
			//   		$row = $query->result();
			//   		$campus_recruiterid = $row[$i]->id_mapping_campus_recruiter;
			  		
			  		
			//   		if ($numrows>0) {

			//   			$updateArray = array(
			// 			'campusid'   	=> $campusid,
			// 			'recruiterid'   => $this->input->post("recruiter")[$i],
			// 			'anchor'        => $this->input->post("anchor")[$i],
			// 			'campusintimationid' => $campusintimationid,
			// 			'createdon'     => date('Y-m-d H:i:s'),// Current Date and time
			// 		    'createdby'     => $this->loginData->UserID, // login user id
			// 		    'isdeleted'     => 0,
			// 		  );
			// //	print_r($updateArray); die;

			// 		$this->db->where("id_mapping_campus_recruiter",$campus_recruiterid);
			//         $this->db->update('mapping_campus_recruiters', $updateArray);
			//        // echo $this->db->last_query(); die;
			  			
			//   		}else{

			// 		$insertArray = array(
			// 			'campusid'   	=> $campusid,
			// 			'recruiterid'   => $this->input->post("recruiter")[$i],
			// 			'anchor'        => $this->input->post("anchor")[$i],
			// 			'campusintimationid' => $campusintimationid,
			// 			'createdon'     => date('Y-m-d H:i:s'),// Current Date and time
			// 		    'createdby'     => $this->loginData->UserID, // login user id
			// 		    'isdeleted'     => 0,
			// 		  );
			// //	print_r($updateArray); die;

			// 		//$this->db->where("id_mapping_campus_recruiter",$token);
			//         $this->db->insert('mapping_campus_recruiters', $insertArray);
			//     }

			//   }
			
			//   	//die;


			  	$this->db->delete('mapping_campus_recruiters',array('campusid'=> $campusid,'campusintimationid'=> $campusintimationid,'isdeleted'=>0));
	         
			  	 for ($i=0; $i < $count; $i++) { 
					$insertArray = array(
						'campusid'   	=> $campusid,
						'recruiterid'   => $this->input->post("recruiter")[$i],
						'anchor'        => $this->input->post("anchor")[$i],
						'campusintimationid' => $campusintimationid,
						'createdon'     => date('Y-m-d H:i:s'),// Current Date and time
					    'createdby'     => $this->loginData->UserID, // login user id
					    'isdeleted'     => 0,
					  );
			//	print_r($updateArray); die;

					//$this->db->where("id_mapping_campus_recruiter",$token);
			        $this->db->insert('mapping_campus_recruiters', $insertArray);

			  }
			
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error update Campus to staff mapping');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully update Campus to staff mapping');			
					}
					redirect('/Campusstaffmapping/index');
			
			}

			$content['campusid'] 		= $token;

			$content['campuslist'] 		= $this->model->getCampus();
			$content['recruitmentlist'] = $this->model->getRecruitment();
			$content['countrecruitment']    = $this->model->getCountCampusRecruitment($campusid);
			$content['MappedCampusRecruitmentlist']    = $this->model->getMappedCampusRecruitment($campusid,$campusintimationid);
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


/**
	 * Method is_campus() Check schedule campus is exist or not . 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 

public function is_campus($campusid, $campusintimationid)
{	
	try{

		$sql = "SELECT Count(id_mapping_campus_recruiter) as count FROM mapping_campus_recruiters where campusid='".$campusid."' AND campusintimationid = '".$campusintimationid."' ";
		$result = $this->db->query($sql)->result();

		return $result[0]->count;



	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}
	

	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete($token)
	{
		try{
		 $campusintimationid    = $this->uri->segment(3);
		 $campusid 				= $this->uri->segment(4);
		
		$this->view['detail'] = $this->model->getMappedCampusRecruitment($campusid,$campusintimationid);
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		

		if($this->model->delete($token) == '1'){
			$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	 }

}