<?php 

/**
* DA_event_detailing List
*/
class Da_event_detailing extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');
 }


 public function index()
 {

  try{
  //print_r($this->loginData);die;

   $this->load->model('Da_event_detailing_model', 'model');
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //////////////// Select Batch ////////////////
   $query = "select * from mstbatch where status =0 AND isdeleted='0'";
   $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
   $query = "select * from mstphase where isdeleted='0'";
   $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
   $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1 AND tc_hrd_document_verfied=2 AND teamid = ".$this->loginData->teamid;

   $content['candidate_details'] = $this->Common_Model->query_data($query);

   $content['daeventdetails_details'] = $this->model->getDAEventDetails();

   $content['subview']="index";

   $content['title'] = 'Da_event_detailing';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }


 public function Add()
 {
  try{

  // print_r($this->input->post('DAname')); die();
  $this->load->model('Da_event_detailing_model', 'model');
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
    // print_r($this->input->post()); //die();
  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

    // print_r($_POST); die;

   $this->db->trans_start();

   /*$this->form_validation->set_rules('batch','Batch Name','trim|required');
   $this->form_validation->set_rules('financialyear','Financial Year','trim|required');*/
   $this->form_validation->set_rules('phase','Phase Name','trim|required');

   if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }
  $phase = $this->input->post('phase');

 
    // echo $phase; die;

  $countdaname =  count($this->input->post('DAname'));
  // print_r($this->input->post('batch')[32]);die();  

  for ($i=0; $i < $countdaname ; $i++) {
    $candidateid = $this->input->post('DAname')[$i];
    $batch = $this->input->post('batch')[$candidateid];
    $financialyear = $this->input->post('financialyear')[$candidateid];
    $sql = "select * from tbl_da_event_detailing where batchid=$batch AND phaseid=$phase AND financial_year=$financialyear AND isdeleted=0"; //die();
    $checkexistance = $this->db->query($sql)->row();
    
    if($checkexistance){
        $insertid = $checkexistance->id;echo "<pre>";
    // print_r($insertid);exit();
    }else{
      $insertArr = array(
        'batchid'       => $this->input->post('batch')[$candidateid],
        'phaseid'       => $this->input->post('phase'),
        'financial_year'=> $this->input->post('financialyear')[$candidateid],
        'createdby'     => $this->loginData->UserID,
        'createdon'     => date('Y-m-d H:i:s'),
      );
      $this->db->insert('tbl_da_event_detailing', $insertArr);
      $insertid = $this->db->insert_id();
      //echo "string";
    } 
/*echo "<pre>";
    print_r($this->db->affected_rows());exit();*/
    $fromdate = $this->input->post('fromdate')[$candidateid];
    $from_date = $this->model->changedatedbformate($fromdate);
    $todate = $this->input->post('todate')[$candidateid];
    $to_date = $this->model->changedatedbformate($todate);
    $sql = "select * from tbl_da_event_detailing_transaction where da_event_detailing=".$insertid." AND candidateid = ".$candidateid;
    $checkexistanceda = $this->db->query($sql)->row();
    // print_r($checkexistanceda); die;
    $afftected = FALSE;
    if(empty($checkexistanceda)){
      $insertArr_transaction = array(
        'da_event_detailing'     => $insertid,
        'candidateid'            => $this->input->post('DAname')[$i],
        'fromdate'               => $from_date,
        'todate'                 => $to_date,       
      );
      $this->db->insert('tbl_da_event_detailing_transaction', $insertArr_transaction);
      $afftected = TRUE;
    }
    // echo $afftected;die();
    $this->db->trans_complete();
    if ($afftected){
      //$phase= '';
      $phaseno = $this->input->post('phase');
      $query = "select phase_name from mstphase where id = ".$phaseno." and isdeleted =0"; 
      $phaseres = $this->Common_Model->query_data($query);
      $subject = "Reflective Reports";
      $body = '';
      $da ='';
      $todate='';
      $daid='';
      $to_date ='';
      $todate = $this->input->post('todate')[$candidateid];
      $daid = $this->input->post('DAname')[$i];
      $da = $this->model->getDADetails($daid);
      $body = 'Dear '.$da->candidatefirstname .$da->candidatelastname.', <br><br> 
      Kindly submit your Reflective Report '.$phaseres[0]->phase_name.' Report by '.$todate.'<br><br>';
      $body .= 'Regards,<br>'
      .$this->loginData->UserFirstName.' '.$this->loginData->UserLastName.'<br>'; 
      $to_email = $da->emailid;

      $sendmail = $this->Common_Model->send_email($subject, $body, $to_email);
      $this->session->set_flashdata('tr_msg', 'Successfully Add DA Event Detailing !!!');
    }else if($this->db->trans_status() == FALSE){
      $this->session->set_flashdata('er_msg', $this->db->error());
      redirect('/Da_event_detailing/add');
    }
  }
  redirect('/Da_event_detailing/index');
  

}

prepareview:
  //////////////// Select Batch ////////////////
$query = "select * from mstbatch where status =0 AND isdeleted='0'";
$content['batch_details'] = $this->Common_Model->query_data($query);


//////////////// Select Batch ////////////////
$query = "select * from mstfinancialyear where isdeleted='0'";
$content['finance_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
$query = "select * from mstphase where isdeleted='0'";
$content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
$query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname , `tblcr`.emailid, mstfinancialyear.id as financialyearid, `tblcr`.mobile, b.batch, b.id as batchid from  tbl_candidate_registration as `tblcr` inner join mstbatch b on `tblcr`.batchid = b.id LEFT JOIN mstfinancialyear on mstfinancialyear.id=b.financial_year where categoryid NOT in(2,3) AND  joinstatus=1 AND teamid = ".$this->loginData->teamid;  
$content['candidate_details'] = $this->Common_Model->query_data($query);

$content['title']   = 'Da_event_detailing';
$content['subview'] = 'Da_event_detailing/add';
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function edit($token)
{
  try{

  $this->load->model('Da_event_detailing_model', 'model');
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

   $this->db->trans_start();

   $this->form_validation->set_rules('batch','Batch Name','trim|required');
   $this->form_validation->set_rules('financialyear','Financial Year','trim|required');
   $this->form_validation->set_rules('phase','Phase Name','trim|required');

   if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }

  $updateArr = array(
    'batchid'       => $this->input->post('batch'),
    'phaseid'       => $this->input->post('phase'),
    'financial_year'=> $this->input->post('financialyear'),
    'updatedby'     => $this->loginData->UserID,
    'updatedon'     => date('Y-m-d H:i:s'),
  );
  $this->db->where('id',$token);
  $this->db->update('tbl_da_event_detailing', $updateArr);
    //$insertid = $this->db->insert_id();
  $countdaname =  count($this->input->post('DAname')); 

  $this->db->delete('tbl_da_event_detailing_transaction', array('da_event_detailing' => $token)); 

  for ($i=0; $i < $countdaname ; $i++) { 

    $fromdate = $this->input->post('fromdate')[$i];
    $from_date = $this->model->changedatedbformate($fromdate);
    $todate = $this->input->post('todate')[$i];
    $to_date = $this->model->changedatedbformate($todate);

    $insertArr_transaction = array(
      'da_event_detailing'     => $token,
      'candidateid'            => $this->input->post('DAname')[$i],
      'fromdate'               => $from_date,
      'todate'                 => $to_date,       
    );

    $this->db->insert('tbl_da_event_detailing_transaction', $insertArr_transaction);
  }

  $this->db->trans_complete();

  if ($this->db->trans_status() === true){
    $this->session->set_flashdata('tr_msg', 'Successfully Add DA Event Detailing !!!');
    redirect('/Da_event_detailing/index');
  }else{
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect(current_url());
  }

}

prepareview:

  //////////////// Select Batch ////////////////
$query = "select * from mstbatch where status =0 AND isdeleted='0'";
$content['batch_details'] = $this->Common_Model->query_data($query);

  //////////////// Select Batch ////////////////
$query = "select * from mstfinancialyear where isdeleted='0'";
$content['finance_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
$query = "select * from mstphase where isdeleted='0'";
$content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
$query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1 AND teamid = ".$this->loginData->teamid;
$content['candidate_details'] = $this->Common_Model->query_data($query);


   //////////////// Select Single DA Event Details ////////////////
$query = "SELECT `tbl_da_event_detailing`.batchid,`tbl_da_event_detailing`.phaseid,`tbl_da_event_detailing`.financial_year,`mstfinancialyear`.financialyear,`tbl_da_event_detailing_transaction`.candidateid,
`tbl_da_event_detailing_transaction`.fromdate,`tbl_da_event_detailing_transaction`.todate FROM `tbl_da_event_detailing`
inner join `mstfinancialyear` on `tbl_da_event_detailing`.financial_year = `mstfinancialyear`.id 
inner join `tbl_da_event_detailing_transaction`  on `tbl_da_event_detailing`.id = `tbl_da_event_detailing_transaction`.da_event_detailing
WHERE  `tbl_da_event_detailing`.`id`=$token AND `tbl_da_event_detailing`.`isdeleted`='0' AND `mstfinancialyear`.isdeleted=0 ORDER BY `tbl_da_event_detailing`.`id` DESC"; 

$singledaeventdetails_details1 = $this->Common_Model->query_data($query);
 //print_r($singledaeventdetails_details1);
$content['singledaeventdetails_details'] =$singledaeventdetails_details1[0];

$content['countdatrancation'] = $this->model->getCountDatrancation($token);
$content['datrancation']     = $this->model->getDaTransaction($token);

$content['title'] = 'Da_event_detailing';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}










function getSingleDAEventDetails($token){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
    LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE  `tbl_da_event_detailing`.`id`='0' AND `tbl_da_event_detailing`.`isdeleted`=$token ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function delete($token)
{
  try{

 $this->db->trans_start();
 $updateArrtrans = array(

  'isdeleted'     => 1,
);
 $this->db->where('da_event_detailing',$token);
 $this->db->update('tbl_da_event_detailing_transaction', $updateArrtrans);

 $updateArr = array(

  'isdeleted'  => 1,
  'updatedon'  => date('Y-m-d H:i:s'),
  'updatedby'  => $this->loginData->UserID,
);
 $this->db->where('id',$token);
 $this->db->update('tbl_da_event_detailing', $updateArr);

 $this->db->trans_complete();

 if ($this->db->trans_status() === true){
  $this->session->set_flashdata('tr_msg', 'DA Event Detailing Deleted Successfully');
  redirect('/Da_event_detailing/index');
}else{
  $this->session->set_flashdata('er_msg', $this->db->error());
  redirect(current_url());
}

//$delete_row_transaction = $this->Common_Model->delete_row('tbl_da_event_detailing_transaction','da_event_detailing', $token); 
//$this->Common_Model->delete_row('tbl_da_event_detailing','id', $token);
//$this->session->set_flashdata('tr_msg' ,"DA Event Detailing Deleted Successfully");
  //redirect('/DA_event_detailing/index/');

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




 /**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


 public function changedate($Date)
 {
  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
  // print_r($date); die;
 if($pattern == "/" )
  @  $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
else
  @  $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 // echo $date; die;
return $date;

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

/**
   * Method changedate() Find the pattern and change the format according to the pattern.
   * @access  public
   * @param Null
   * @return  Array
   */


public function changedatedbformate($Date)
{

  try{
//echo strlen($Date);
  $len = (strlen($Date)-5); 
  if(substr($Date,$len,-4)=="/")
   $pattern = "/";
 else
   $pattern = "-";

 $date = explode($pattern,$Date);
   //print_r($date); 
 if($pattern == "/" )
   @ $date = trim($date[2])."-".trim($date[1])."-".trim($date[0]);  
 else
   @ $date = trim($date[2])."/".trim($date[1])."/".trim($date[0]);
 return $date;

 }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



}