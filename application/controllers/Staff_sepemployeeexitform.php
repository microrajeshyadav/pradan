<?php 
class Staff_sepemployeeexitform extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepemployeeexitform_model');
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($id)
	{
		// start permission
		$content['token'] = $id; 
		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			$query ="SELECT * FROM staff_transaction WHERE id=".$id;
			$content['staff_transaction'] = $this->db->query($query)->row();
			$content['staff_detail'] = $this->get_staff_clearance_detail($id);
			// print_r($content['staff_transaction']); die();
			$content['join_relive_data'] = $this->get_staff_seperation_certificate($content['staff_transaction']->staffid);
			// echo "<pre>";
			// print_r($content['join_relive_data']);exit();
			$query ="SELECT * FROM tbl_exit_interview_form WHERE transid=".$id;
			$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();

			// converted in year and mmonths
			$doj = '';
			$doj = $content['staff_detail']->doj;
			$doj1 = '';
			$tdate1 = '';
			$datesqldiff = '';
			$doj1 = date_create($doj);
			$tdate1 = date_create(date('Y/m/d'));

		    $datesqldiff = date_diff($doj1,$tdate1);
			
			if(!empty($datesqldiff)){
			$content['service_yr'] = $datesqldiff->y;
			$content['service_mths'] = $datesqldiff->m;	
			}
			else{
			$content['service_yr'] = null;
			$content['service_mths'] = null;
			}			

			// end permission    

        
        	$RequestMethod = $this->input->server('REQUEST_METHOD');
        	if($RequestMethod == "POST"){
        		// print_r($_POST);
        		// die;
				
			 if($this->input->post('saveandsubmit') == 'Save And Submit'){
					$db_flag = 1;
				}

				if($this->input->post('Save') == 'Save')
				{
					$observation=$this->input->post('question11');
					$obser_array=array(
						
        			'currentdate'=>date('Y-m-d'),
        			'createdby'=>$this->loginData->staffid,

						'tc_observation'=>$observation,
						'flag'=>2
					);



					$this->db->where('transid', $id);
					$this->db->update('tbl_exit_interview_form', $obser_array);
					// echo $this->db->last_query();
					// die;
					
					$this->session->set_flashdata('tr_msg','Supervisior observisation Saved Successfully.');
					

				}
				else
				{

				if($content['staff_transaction']->trans_status != 'Super Annuation')
				{
        		$data = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$id,
        			'contents_confidential'=>$this->input->post('content_confedential'),
        			'an_experience_1'=>$this->input->post('question1'),
        			'any_learning_2'=>$this->input->post('question2'),
        			'specifically_3'=>$this->input->post('question3'),
        			'two_characteristics_4'=>$this->input->post('question4'),
        			'any_experience_5'=>$this->input->post('question5'),
        			'any_experience_6'=>$this->input->post('question6'),
        			'any_episode_7'=>$this->input->post('question7'),
        			'taking_an_overview_8'=>$this->input->post('question8'),
        			'proposed_assignment_9'=>$this->input->post('question9'),
        			'such_things_be_that_you_may_like_to_focus_on_10'=>$this->input->post('question10'),
        			'you_leave_this_organization_11'=>$this->input->post('question11'),
        			'currentdate'=>date('Y-m-d'),
        			'createdby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
        		if($content['staff_sepemployeeexitform']){
        			$data_update = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$id,
        			'contents_confidential'=>$this->input->post('content_confedential'),
        			'an_experience_1'=>$this->input->post('question1'),
        			'any_learning_2'=>$this->input->post('question2'),
        			'specifically_3'=>$this->input->post('question3'),
        			'two_characteristics_4'=>$this->input->post('question4'),
        			'any_experience_5'=>$this->input->post('question5'),
        			'any_experience_6'=>$this->input->post('question6'),
        			'any_episode_7'=>$this->input->post('question7'),
        			'taking_an_overview_8'=>$this->input->post('question8'),
        			'proposed_assignment_9'=>$this->input->post('question9'),
        			'such_things_be_that_you_may_like_to_focus_on_10'=>$this->input->post('question10'),
        			'you_leave_this_organization_11'=>$this->input->post('question11'),
        			'updatedon'=>date('Y-m-d H:i:s'),
        			'updatedby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
					$this->db->where('id', $content['staff_sepemployeeexitform']->id);
					$flag = $this->db->update('tbl_exit_interview_form', $data_update);
				}else{
					$flag = $this->db->insert('tbl_exit_interview_form',$data);
				}

				}

				// Super Annuation
				else{
					$data = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$id,
        			'most_liked'=>$this->input->post('most_liked'),
        			'least_liked'=>$this->input->post('least_liked'),
        			'suggestions'=>$this->input->post('suggestions'),
        			'remarks'=>$this->input->post('remarks'),
        			'future_address'=>$this->input->post('future_address'),
        			'currentdate'=>date('Y-m-d'),
        			'createdby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
        		if($content['staff_sepemployeeexitform']){
        			$data_update = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$id,
        			'most_liked'=>$this->input->post('most_liked'),
        			'least_liked'=>$this->input->post('least_liked'),
        			'suggestions'=>$this->input->post('suggestions'),
        			'remarks'=>$this->input->post('remarks'),
        			'future_address'=>$this->input->post('future_address'),
        			'updatedon'=>date('Y-m-d H:i:s'),
        			'updatedby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
					$this->db->where('id', $content['staff_sepemployeeexitform']->id);
					$flag = $this->db->update('tbl_exit_interview_form', $data_update);
				}else{
					$flag = $this->db->insert('tbl_exit_interview_form',$data);
				}
				}
				// Super Annuation end
				
        		if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');} 
                else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');}
        	}
        }
			$query ="SELECT * FROM tbl_exit_interview_form WHERE transid=".$id;
			$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();
			$content['title'] = 'Staff_sepemployeeexitform';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}
	public function get_staff_clearance_detail($token)
   {
      try
	  {

	     $sql="SELECT c.name,c.doj,c.permanentstreet,a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code,c.encrypted_signature

	       FROM staff_transaction as a
	       INNER JOIN `staff` as c on a.staffid = c.staffid
	       INNER JOIN `msdesignation` as b on a.new_designation = b.desid

	     WHERE a.id = $token";


	     //$query=$this->db->get();
	     return  $this->db->query($sql)->row();

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   public function get_staff_seperation_certificate($staffid)
   {
      try
	  {
	      $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation,g.desname as leavingdesignation,k.reason as leavingreason,officename as location FROM staff_transaction as a
	         INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid
	         INNER JOIN `staff` as c on a.staffid = c.staffid
	         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
	         INNER JOIN `msdesignation` as g on k.new_designation = g.desid
	         INNER JOIN `lpooffice` as l on c.new_office_id = l.officeid
	         WHERE   a.trans_status = 'JOIN' and a.staffid =".$staffid; 
	     return  $this->db->query($sql)->row();

		}catch (Exception $e) {
	      print_r($e->getMessage());die;
	    }

  }

	
}