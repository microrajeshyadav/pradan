<?php 
class Team_coordinator_dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		
		
		//print_r($this->loginData);

		
		$content['selectedcandidatedetails']      = $this->model->getSelectedCandidate();
		$content['campusdetails'] 	              = $this->model->getCampus();
		$content['recruiters_list']               = $this->model->getRecruitersList();
		$content['comments']                      = $this->model->getComments();

		
		
		$content['title'] = 'HRDDashboard';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}


	public function Offercomments($token){

	try{
			
			 $RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

			    $this->form_validation->set_rules('recruiters','Recruiters','trim|required');
	            $this->form_validation->set_rules('comments','Comments ','trim|required');
	                   
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }


	            $this->db->trans_start();

			$insertArr2 = array(
				'candidateid'     => $token,
				'recutersid'      => $this->input->post("recruiters"),
				'comments'  	  => $this->input->post('comments'),
				'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'       => $this->loginData->UserID, // login user id
			    
			  );
			$this->db->insert('edcomments', $insertArr2);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding Comments');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Comments');			
			}
			redirect('/EDashboard/Offercomments');
			
			}

			prepareview:

			$content['recruiters_list']               = $this->model->getRecruitersList();
			$content['title'] = 'Offercomments';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	public function sendofferlettertocandidate($token){

	try{

			//echo $token; die; 

			 $getCandidateDetails = $this->model->getCandidateDetails($token); //// candidate Details
			 	// echo "<pre>";
			 	// print_r($getCandidateDetails); die;

		     $candidateemail = $getCandidateDetails[0]->emailid;  //// Email Id ////
			 $candidatname   = $getCandidateDetails[0]->candidatefirstname;  //// candidate Name ////
			 $filename       = $getCandidateDetails[0]->filename;  //// file Name ////
			 $newpassword    = $this->getPassword();

			 
			// echo $newpassword; die;
			 
			 $attachments = array($filename.'.pdf');

		    $html = 'Dear '.$candidatname.', <br><br> 

					Congratulations you are selected by Pradan. <br><br> Username - '.$candidateemail.'
					<br>Password - '.$newpassword.'';


			 $sendmail = $this->Common_Model->send_email($subject = 'Job Offer letter ', $message = $html, $candidateemail, $candidateemail, $attachments);  //// Send Mail candidates With Offer Letter ////
			 //echo $sendmail; die;
		
				if ($sendmail == false) {
					$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
				}else{

					 $UpdateArr = array(
					 	'password'  => $newpassword,
			            );

					 $this->db->where('candidateid',$token);
                     $this->db->update('tbl_candidate_registration', $UpdateArr);

					 $InsertArr = array(
					 	'candidateid'  => $token,
			            'flag'         => 'Send',
			            'senddate'       => date('Y-m-d H:i:s'),    // Current Date and time
			    		'sendby'       => $this->loginData->UserID, // login user id
			            );

                     $this->db->insert('tbl_sendofferletter', $InsertArr);
					 $this->session->set_flashdata('tr_msg','Successfully Send Offer Letter !!!!');				
				}

			redirect('HRDDashboard/index');

			$content['sentmail']      = $this->SentMail($token);
			$content['title'] = 'HRDDashboard';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}


 public function getPassword()
    {

    try{
    
    // Generating Password
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
    $password = substr( str_shuffle( $chars ), 0, 8 );

    return $password;


        }catch (Exception $e) {
          print_r($e->getMessage());die;
    }

  }

 public function SentMail($token){


    try{
    	
    	$this->db->select('*');
		$this->db->where('candidateid', $token);
		$query = $this->db->get('tbl_sendofferletter');
		$num = $query->num_rows();
   		return $num;

        }catch (Exception $e) {
          print_r($e->getMessage());die;
    }


 }




}