<?php 
class Probation_tc_reviewofperformance extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Probation_tc_reviewofperformance_model","model");
		$this->load->model("Global_model","gmodel");

		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['title'] = 'Probation_tc_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function add($token)
	{
		// start permission 
		try{
			
			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Staff_review/index');

			} else {

				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
		// end permission   
		

				$this->load->model("Probation_tc_reviewofperformance_model","model");
				$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
				$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
				$img = $content['getpersonnaldetals']->personnalstaffid;

				
				
				//die;

				$content['staffsignature'] = $this->Common_Model->staff_signature($img);
				// print_r($content['staffsignature']);
				// die;
							
				$site='datafiles/signature/'.$content['staffsignature']->encrypted_signature;
				
				$getstaffdetail = $this->Probation_tc_reviewofperformance_model->getstaffid($token);

			    $staffmail = $getstaffdetail->emailid;
				$staffid = $getstaffdetail->staffid;

				$content['id'] = $this->Probation_tc_reviewofperformance_model->getEDid();
				// print_r($content['id']->edid); die;
			    

			   $content['nameofed'] = $this->Probation_tc_reviewofperformance_model->getEDName($content['id']->edid);
			   

			    $edmmailid =$content['id']->edmail; 
				$workflowdetail = $this->model->get_providentworkflowid($staffid); 


				$content['pid'] = $this->model->getStaffPersonnalList();


				$RequestMethod = $this->input->server('REQUEST_METHOD');

				if($RequestMethod == 'POST'){
				

					$this->db->trans_start();

					$date_of_appointment = $this->input->post('date_of_appointment');
					$date_of_appointment_db = $this->gmodel->changedatedbformate($date_of_appointment);

					$period_of_review_from = $this->input->post('period_of_review_from');
					$period_of_review_from_db = $this->gmodel->changedatedbformate($period_of_review_from);

					$period_of_review_to = $this->input->post('period_of_review_to');
					$period_of_review_to_db = $this->gmodel->changedatedbformate($period_of_review_to);

					$probation_extension_date = $this->input->post('probation_extension_date');
					$probation_extension_date_db = $this->gmodel->changedatedbformate($probation_extension_date);

					$period_latestby = $this->input->post('latestby');
					$period_latestby_db = $this->gmodel->changedatedbformate($period_latestby);

					$probationcomplete = $this->input->post('probation_completed');


					$insertArr = array(
						'transid'                     => $token,
						'staffid'                     => $this->input->post('staffid'),
						'date_of_appointment'         => $date_of_appointment_db,
						'period_of_review_from'       => $period_of_review_from_db,
						'period_of_review_to'         => $period_of_review_to_db,
						'work_habits_and_attitudes'   => (trim($this->input->post('work_habits_and_attitudes'))==""?null:$this->input->post('work_habits_and_attitudes')),
						'conduct_and_social_maturity' => (trim($this->input->post('conduct_and_social_maturity'))==""?null:$this->input->post('conduct_and_social_maturity')),
						'integrity'                   => (trim($this->input->post('questionable'))==""?null:$this->input->post('questionable')),
						'any_other_observations'      => (trim($this->input->post('any_other_observations'))==""?null:$this->input->post('any_other_observations')),
						'latestby'                    => (trim($period_latestby_db)==""?null:($period_latestby_db)),
						'satisfactory'                => (trim($this->input->post('satisfactory'))==""?null:$this->input->post('satisfactory')),
						'probation_completed'         => (trim($probationcomplete)==""?null:($probationcomplete)),
						'probation_extension_date'    => (trim($probation_extension_date_db)==""?null:($probation_extension_date_db)),

						'flag'                        => 1,
						'createdby'                   => $this->loginData->staffid,
						'createdon'                   => date('Y-m-d H:i:s'),
					);
					  $this->db->insert('tbl_probation_review_performance', $insertArr);
					

					$insertid = $this->db->insert_id();   

					$updateArr = array(

						'trans_flag'           => 3,
						'updatedon'            => date("Y-m-d H:i:s"),
						'updatedby'            => $this->loginData->staffid,
					);

					$this->db->where('transid', $token);
					$this->db->update('staff_transaction', $updateArr);
					

					$sql = "SELECT * from tbl_probation_review_performance where transid ='$token'";
					// echo $sql; die;


					$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
					$probationid = $getstaffprobationdetail->id;

					// if($probationcomplete == 'yes'){

					// tc flag records update
					$insertworkflowArr = array(
						'r_id'                 => (trim($token)==""?null:($token)),
						'type'                 => 7,
						'staffid'              => $staffid,
						'sender'               => $this->loginData->staffid,
						'receiver'             =>  (trim($content['id']->edid)==""?null:($content['id']->edid)),
						'senddate'             => date("Y-m-d H:i:s"),
						'forwarded_workflowid' => (trim($workflowdetail->workflowid)==""?null:($workflowdetail->workflowid)),
						'flag'                 => 3,
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);

					 $this->db->insert('tbl_workflowdetail', $insertworkflowArr);







					// direct ed flag aproval records update 
				// 	$insertArr = array(

		  //   		'ed_comments'          => 'Direct ed approval while completion by tc',
		  //   		'ed_date'              => date('Y-m-d H:i:s'),
		  //   		'flag'				   => 5,
		  //   		'agree'	               => 'yes',
		  //   		'updatedby'            => $this->loginData->staffid,
		  //   		'updatedon'            => date('Y-m-d H:i:s'),
		  //   	);
				// // print_r($insertArr); die;
		  //   	$this->db->where('id',$probationid);
		  //   	$this->db->update('tbl_probation_review_performance', $insertArr);

		  //   	$insertworkflowArr = array(
		  //   		'r_id'                 => (trim($token)==""?null:($token)),
		  //   		'type'                 => 7,
		  //   		'staffid'              => $staffid,
		  //   		'sender'               => $this->loginData->staffid,
		  //   		'receiver'             => 1284,
		  //   		'senddate'             => date("Y-m-d H:i:s"),
		  //   		'forwarded_workflowid' => (trim($workflowdetail->workflowid)==""?null:($workflowdetail->workflowid)),
		  //   		'flag'                 => 3,
		  //   		'createdon'            => date("Y-m-d H:i:s"),
		  //   		'createdby'            => $this->loginData->staffid,
		  //   	);
				// // echo "<pre>";
				// // print_r($insertworkflowArr); die;
		  //   	$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				// 			}else{

				// 	$insertworkflowArr = array(
				// 		'r_id'                 => (trim($token)==""?null:($token)),
				// 		'type'                 => 7,
				// 		'staffid'              => $staffid,
				// 		'sender'               => $this->loginData->staffid,
				// 		'receiver'             => (trim($content['id']->edid)==""?null:($content['id']->edid)),
				// 		'senddate'             => date("Y-m-d H:i:s"),
				// 		'forwarded_workflowid' => (trim($workflowdetail->workflowid)==""?null:($workflowdetail->workflowid)),
				// 		'flag'                 => 3,
				// 		'createdon'            => date("Y-m-d H:i:s"),
				// 		'createdby'            => $this->loginData->staffid,
				// 	);

				// 	 $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				// }
				

		    	$insertArr = array (

		    		'trans_flag'      => 3,
		    		'updatedby'       => $this->loginData->staffid,
		    		'updatedon'       => date('Y-m-d H:i:s'),
		    	);
				// print_r($insertArr); die;
		    	$this->db->where('id',$token);
		    	$this->db->update('staff_transaction', $insertArr);




				

					$html = 'Dear '.$getstaffdetail->name.', <br> 
					Probation review of process 
					<br>
					Regard,<br>From,<br>Team Co-ordinator of PRADAN';

					
						


						$current_date=date('d/m/Y');
					$html2='<div>
         <h4 >REVIEW OF PERFORMANCE OF PROBATIONERS</h4>
       </div>
        <div syle="align:center">
          <p><h5>Part- I</h5></p>
          <p>(<em>to be filled in by the Finance-Personnel-MIS Unit)</em></p>
        </div>
        <div class="col-md-12">
            1. Name :<input type="text"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"   value='.$content['getstaffprovationreviewperformance']->name.'>
          </div>
           <div>
           2. Designation : <input type="text" name="staffdesignation" id="staffdesignation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$content['getstaffprovationreviewperformance']->desname.'>
         </div>   
       
       <div class="col-md-12">
          3. Employee Code :  <input type="text" name="empcode" id="empcode"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$content['getstaffprovationreviewperformance']->emp_code.' readonly="readonly">
        </div>
         <div class="col-md-12">
          4. Location :<input type="text" name="stafflocation" id="stafflocation"  style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" maxlength="150" value='.$content['getstaffprovationreviewperformance']->officename.' readonly="readonly">
        </div>
        <div class="col-md-12">
          5. Date of Appointment:<input type="text" name="date_of_appointment" id="date_of_appointment" class="" style="min-width: 5px; max-width:300px; border-radius: 0px; border: none; border-bottom: 1px solid black;" required="required"  value='.$content['getstaffprovationreviewperformance']->appointmentdate.' readonly="readonly">
        </div>
        <div class="col-md-12">
          6. Period of Review : From <input type="text" name="period_of_review_from" id="period_of_review_from" class=""  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" readonly="readonly" value='.$content['getstaffprovationreviewperformance']->appointmentdate.'  required="required" > (date) To <input type="text" name="period_of_review_to" id="period_of_review_to" class="" readonly="readonly"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$content['getstaffprovationreviewperformance']->probation_date.'  required="required" > (date)
        </div>
      </div>
      <p></p>
      <p></p>
      <p><strong>TO BE REVIEWED BY</strong></p>
      <div class="row" style="line-height: 3">
        <div class="col-md-12">
          
          1. Name : <input type="text" name="review_by" id="review_by" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$content['getpersonnaldetals']->personnalname.' readonly="readonly">
        </div>
        <div class="col-md-12">
         2. Designation : <input type="text" name="review_designation" id="review_designation" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$content['getpersonnaldetals']->personnaldesignationname.' readonly="readonly">
       </div>
        <div class="col-md-12">
        3. Employee Code : <input type="text" name="review_emp_code" id="review_emp_code" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$content['getpersonnaldetals']->personnalempcode.' readonly="readonly">
      </div>

      <div class="col-md-12">
        4. Location : <input type="text" name="review_location" readonly="readonly" id="review_location" style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;" maxlength="150" value='.$content['getpersonnaldetals']->officename.'>
      </div>
    </div>
    <hr/>
    <p><strong>Notes for the Reviewer</strong></p>
    <p></p>
    <ol>
      <li>Please assess the employee&rsquo;s performance during the period of probation mentioned above (Item 6).</li>
    </ol>
    <p></p>
    <ol start="2">
      <li>Please be objective in your assessment. Each factor should be assessed independently, uninfluenced by assessment of the other factor(s).</li>
    </ol>
    <p></p>
    <ol start="3">
      <li>Please return the duly filled in assessment report in a closed cover marked <em>confidential</em> to</li>
    </ol>
    <p>the Finance-Personnel-MIS Unit latest by  <input type="text" name="latestby" id="latestby" class="datepicker"  style="min-width: 5px; max-width:200px; border-radius: 0px; border: none; border-bottom: 1px solid black;"  value='.$period_latestby_db.'  required="required" >  (date).</p>
    <p></p>
     <div class="text-center">
        <p><h5>Part-II</h5></p>
        <p><h5>REVIEW</h5></p>
        <p>(<em>to be filled in by the Reviewing Supervisor</em>)</p>
      </div>
      <p>The probation has provided you an opportunity to interact with and observe the probationer in the work setting for six months. This is a significant period of time, for both sides.</p>
      <p></p>
      <p>&ldquo;<strong>How do you see the Probationer fulfilling the primary responsibilities s/he would need to shoulder to be an effective employee of this organization?</strong>&rdquo;</p>
      <p><strong></strong></p>
      <p>Could you please write about <em>500 words</em> addressing the issue (primary responsibilities s/he is expected to handle). Please give anecdotes/examples wherever possible.</p>


      <p><strong>Work Habits and Attitudes</strong></p>
      <p></p>
      <p>How would you rate the probationer&rsquo;s interest in and aptitude for work? What is the quality of her/his work? Does s/he demonstrate proaction and take initiative? Is s/he serious about work, and shows a sense of commitment to it? How resourceful is the probationer? Is s/he punctual, disciplined and regular at work? How does s/he fit into PRADAN&rsquo;s work culture? What is her/his attitude to the communities we work with? How does s/he deal with the conditions of work, absence of regular timings, physical hardship and unstructured work situation?</p>

      
      <p style="width:100%;border: 1px solid black; padding:10px;">'.$this->input->post('work_habits_and_attitudes').'</p>

      <p><strong>Conduct and Social Maturity</strong></p>
      <p></p>
      <p>What is the probationer&rsquo;s general behaviour like? Does s/he possess the requisite inter-personal competence? What is her/his demeanour such as&mdash;e.g., is s/he moody, short-tempered, adjusting, reserved, talkative, a gossip, etc.?</p>

     
      <p style="width:100%;border: 1px solid black; padding:10px;">'.$this->input->post('conduct_and_social_maturity').'</p>


      <p><strong>Integrity</strong></p>

      <p>Is the probationers integrity<em> </em><br>
        <em> Questionable</em><strong><em> </em></strong></p>
        <p><strong><em> </em></strong>or</p>
        <p>
        
        <em>Above Board</em><em> <strong> </strong></em></p>
        <input type="text" value='.$this->input->post('questionable').'  style="width:100%" >

        <p><strong>Any Other Observations</strong></p>
        <p>Please share any other observations you would like to make about the probationer. For instance, does s/he have any areas that s/he could improve upon? Is there anything significant about her/him that has not been captured in the above?</p>

         <p style="width:100%;border: 1px solid black; padding:10px;" >'.$this->input->post('any_other_observations').' </p><em>
       
          <p><strong><br /> </strong></p>

          <p><h5>Part III</h5></p>
          <p><h5>OVERALL REVIEW AND RECOMMENDATIONS</h5></p>
        </div>
        <div class="container-fluid">
          <div class="row text-left">
            1. The probationer&rsquo;s overall performance (work, conduct and suitability) during the period of probation 
          </div>
          <div class="row text-left">
            <div class="col-md-3"> (a)  Satisfactory   </div>
            
            <div class="col-md-12">  OR</div>
            <div class="col-md-3"> (b)  Not satisfactory </div> 
            <div class="row text-left">
            <div class="col-md-12">  <input type="text" value='.$this->input->post('satisfactory').' style="width:100%;" > </div>
           
          </div>
          <div class="row text-left">
            2. Her/his probation:
          </div>
          <div class="row text-left">
            <div class="col-md-3"> (a)  May be completed </div>
            
            <div class="col-md-12">  OR</div>
            <div class="col-md-12"> (b) May not be completed now as her/his performance needs to be observed further for a period of'.$probation_extension_date_db.' months.
            </div>




            <div class="col-md-12">  (c)  Not recommended for absorption in PRADAN </div>

            <div class="col-md-12">  <input type="text" value='.$probationcomplete.' style="width:100%;" > </div>


          </div>

          <div >
            <div >
              Location:  '.$content['getpersonnaldetals']->officename.'
            </div>
            <div >
            <img src='.$site.'>

            <div >
              Signature of Reviewing Supervisor
            </div></div>

      

            <div>
              Date:  '.$current_date.' 
            </div>
            <div >
              Name:'.$content['getpersonnaldetals']->personnalname.'
            </div>
            <div>

            </div>
            <div>
             Designation:'.$content['getpersonnaldetals']->personnaldesignationname.'
           </div>
        ';

      
     	
       $dc_name=$this->model->getdcdetails($getstaffdetail->new_office_id,$getstaffdetail->staffid);
       $dc_email=$dc_name->emailid;


       
       $filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$attachments = array($filename.'.pdf');
				$generate='';
				$generate =   $this->Dompdf_model->probation_letter_PDF($html2,$filename, NULL,'filename.pdf');
				
				$subject='';
				$teamincharge='team cordinator';
				$recipients = array(
               $dc_email => 'name'
               
              
              );

				$subject='';
				if ($this->input->post('satisfactory')== 'satisfactory') {
					$subject='Probation review/proposed for satifaction';
				}elseif ($this->input->post('satisfactory')=='notsatisfactory' &&  $this->input->post('probation_extension_date')!=null){
					$subject='Probation review/proposed for extend';
				}elseif ($this->input->post('satisfactory')=='notsatisfactory' && $this->input->post('probation_completed')=='not_recommended') {
					$subject='Probation review/proposed for sepration';
				}

					
				//$send=$this->Common_Model->send_email($subject, $html2,$dc_name,$dc_email,$recipenent=null,$attachments);
				$sendmail = $this->Common_Model->send_email($subject, $message = $html, $dc_email,$teamincharge,$recipients,$attachments);
				
				
				

					$this->db->trans_complete();
					if ($this->db->trans_status() === true){
						$this->session->set_flashdata('tr_msg', 'Review of performance has been Verified successfully !!!');
						redirect('/Probation_tc_reviewofperformance/view/'.$token);

					}else{
						$this->session->set_flashdata('er_msg', 'Error !!! Review of performance  !!!');
						redirect(current_url());
					}
				}

				$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);


				//print_r($content['getstaffprovationreviewperformance']); die;
				
				// print_r($content['getpersonnaldetals']); die;
				
				

				$content['title'] = 'Probation_tc_reviewofperformance';
				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);
			}
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}

	


	public function edit($token)
	{
		// start permission 
		try{
			
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);

			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){


				$this->db->trans_start();
				$extended_date = $this->input->post('extended_date');
				$extended_date_db = $this->gmodel->changedatedbformate($extended_date);

				$insertArr = array(
					'transid'                               => $token,
					'staffid'                               => $this->input->post('staffid'),
					'satisfactory'                          => $this->input->post('satisfactory'),
					'probation_completed'                   => $this->input->post('completed'),
					'reasons_for_not_above_recommendations' => $this->input->post('not_recommended'),
					'probation_extension_date'              =>  $extended_date_db,
					
					'work_habits_and_attitudes'             => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity'           => $this->input->post('conduct_and_social_maturity'),
					'any_other_observations'                => $this->input->post('any_other_observations'),
					'integrity'                             => $this->input->post('questionable'),
					'flag'                                  => 1,
					'updatedby'                             => $this->loginData->staffid,
					'updatedon'                             => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);


				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $token,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $content['id']->edid,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 3,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				$this->db->trans_complete();

				if ($this->db->trans_status() === true){


					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been sent to ED successfully !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_tc_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}



	public function ededit($token)
	{
		// start permission 
		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);

			$probationid = $getstaffprobationdetail->id;
			$staffid = $getstaffprobationdetail->staffid;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){


				$this->db->trans_start();
				$ed_date = $this->input->post('ed_date');
				$ed_date_db = $this->gmodel->changedatedbformate($ed_date);

				$insertArr = array(
					
					'ed_comments'          => $this->input->post('ed_comments'),
					'ed_date'              => $ed_date_db,
					'flag'				   => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $this->input->post('review_by_id'),
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => $this->input->post('edstatus'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


				$this->db->trans_complete();

				if ($this->db->trans_status() === true){
					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been sent to ED successfully !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_tc_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function view($token=NULL)
	{
		// start permission 
		try{
			
			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Staff_review/index');

			} else {

				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

				$content['getSupervisordetals'] = $this->model->getSupervisor();

				$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);

				$probationid = $getstaffprobationdetail->id;
				$staffid = $getstaffprobationdetail->staffid;

				$content['getstaffprobationreview'] = $getstaffprobationdetail;

				$RequestMethod = $this->input->server('REQUEST_METHOD');

				if($RequestMethod == 'POST'){


					$this->db->trans_start();

					$ed_date = $this->input->post('ed_date');
					$ed_date_db = $this->gmodel->changedatedbformate($ed_date);

					$insertArr = array(

						'ed_comments'          => $this->input->post('ed_comments'),
						'ed_date'              => $ed_date_db,
						'flag'				   => 7,
						'probation_completed'    => $this->input->post('recommendations'),
						'updatedby'            => $this->loginData->staffid,
						'updatedon'            => date('Y-m-d H:i:s'),
					);

					$this->db->where('id',$probationid);
					$this->db->update('tbl_probation_review_performance', $insertArr);

					$insertworkflowArr = array(
						'r_id'                 => $token,
						'type'                 => 7,
						'staffid'              => $staffid,
						'sender'               => $this->loginData->staffid,
						'receiver'             => 1284,
						'senddate'             => date("Y-m-d H:i:s"),
						'flag'                 => $this->input->post('edstatus'),
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);

					$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


					$this->db->trans_complete();

					if ($this->db->trans_status() === true){


						$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
						redirect('/Probation_reviewofperformance/view/'.$token);	
					}else{
						$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
						redirect(current_url());
					}

				}
				$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
				$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

				$img = $content['getpersonnaldetals']->personnalstaffid;

				$content['staffsignature'] = $this->Common_Model->staff_signature($img);

				$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);





				$content['title'] = 'Probation_tc_reviewofperformance';
				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);
			}
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}





	public function fullview($token)
	{
		// start permission 
		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['getSupervisordetals'] = $this->model->getSupervisor();

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);


			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail; 

			$edname = $this->model->getEDName($content['getstaffprobationreview']->edid);

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

			$d_o_j = date('d/m/Y');
			$getstaff_name = $content['getstaffprovationreviewperformance']->name;
			$getstaff_emp_code = $content['getstaffprovationreviewperformance']->emp_code;
			$getstaff_desname = $content['getstaffprovationreviewperformance']->desname;

			$staff = array('$d_o_j','$getstaffprovationreviewperformance_name','$ednamename');
			$staff_replace = array($d_o_j,$getstaffprovationreviewperformance_name,$edname->name);

				$this->db->trans_start();

				// $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
				// <tr>
				// <td width="10%">&nbsp;</td>
				// <td colspan="3"><p align="center">  Letter Informing Employee of Completion of Probation</strong></p></td>
				// <td width="10%">&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td width="27%">Ref: Personal Dossier of Employee</td>
				// <td width="27%">&nbsp;</td>
				// <td width="26%">Date:  $d_o_j </td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>To</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>Name of Probationer: $getstaff_name </p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">Employee Code: $getstaff_emp_code</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>Designation:  $getstaff_desname</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>Location: </p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">Subject:<strong>Completion of Probation</strong></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>Dear  $getstaff_name,</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>You were appointed in PRADAN as $getstaff_desname  with effect  from  $d_o_j .</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>I am happy to inform you that, based  on the review of your performance, you have satisfactorily completed the period  of your probation with effect from  $d_o_j  .</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>I am sure that you will continue to  perform well in the future too.</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>Wishing you a long and purposeful  association with PRADAN.</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>Yours sincerely,</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3"><p>( $edname->name )<br />
				// Executive Director</p></td>
				// <td>&nbsp;</td>
				// </tr>
				// <tr>
				// <td>&nbsp;</td>
				// <td colspan="3">&nbsp;</td>
				// <td>&nbsp;</td>
				// </tr>
				// </table>';

					//letter to generate pdf 
			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 38 AND `isactive` = '1'";
			$data = $this->db->query($sql)->row();
			if(!empty($data))
			$body = str_replace($staff,$staff_replace , $data->lettercontent);


				$filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');


         $hrdemail       = $this->loginData->EmailID; /////// hrd email id/////
         $staffemail     = 'amit.kum2008@gmail.com';


		 	 $filename = 'e49348d7c5fb41a69976fe0483b15b67'; //die;
		 	 $attachments = array($filename.'.pdf');


		 	 $html = 'Dear '.$content['getstaffprovationreviewperformance']->name.', <br><br> 
		 	 Congratulations you are confirm by Pradan. 
		 	 <br>
		 	 <br><br>
		 	 Regard,<br>Pradan Team';



			$sendmail = $this->Common_Model->send_email($subject = 'Confirmation by Pradan', $message = $html, $staffemail, $attachments);  //// Send Mai


			$insertArr = array(
				'flag'				   => 5,
				'filename'             => $filename,
				'updatedby'            => $this->loginData->staffid,
				'updatedon'            => date('Y-m-d H:i:s'),
			);

			$this->db->where('id',$probationid);
			$this->db->update('tbl_probation_review_performance', $insertArr);

			$insertworkflowArr = array(
				'r_id'                 => $probationid,
				'type'                 => 7,
				'staffid'              => $token,
				'sender'               => $this->loginData->staffid,
				'receiver'             => $token,
				'senddate'             => date("Y-m-d H:i:s"),
				'flag'                 => 5,
				'createdon'            => date("Y-m-d H:i:s"),
				'createdby'            => $this->loginData->staffid,
			);

			$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			$this->db->trans_complete();

			if ($this->db->trans_status() === true){

				$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
				redirect('/Probation_reviewofperformance/fullview/'.$token);	
			}else{
				$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
				redirect(current_url());
			}

		}




		$content['title'] = 'Probation_reviewofperformance';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}




}