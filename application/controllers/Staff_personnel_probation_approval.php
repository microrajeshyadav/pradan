<?php 

/**
* State List
*/
class Staff_personnel_probation_approval extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Staff_personnel_probation_approval_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');

 }

 public function index()
 {
  try{
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $content['getworkflowdetail'] = $this->Staff_approval_model->getworkflowdetaillist();
   $content['getprobationworklist'] = $this->Staff_approval_model->getprobationworkflowdetaillist();
   $content['gettransferpromationworkflowdetail'] = $this->Staff_approval_model->get_transfer_promation_workflow_detail_list();
    // print_r( $content['gettransferpromationworkflowdetail']); die;
   $content['staff_seperation'] = $this->Staff_approval_model->staff_seperation();
  /*echo "<pre>";
  print_r($content['staff_seperation']);exit();*/

  $content['getpromationworkflowdetail'] = $this->Staff_approval_model->get_promation_workflow_detail_list();
   // print_r($content['getpromationworkflowdetail']); die;
  $content['title'] = 'Staff_personnel_probation_approval';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }

}



public function add($token=NULL,$candidate_id=NULL)
{
  try{
  
     
if (empty($token) || $token == '' ) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
        redirect('/Staff_personnel_probation/index');
        
      } else {

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
  $content['staffdetaillist'] = $this->Staff_personnel_probation_approval_model->getStaffDetails($token);
     $content['staffdetaillist']->reportingto;

     $staffmail = $content['staffdetaillist']->emailid; 

     $content['Tcmail'] = $this->Staff_personnel_probation_approval_model->getReportingemail($content['staffdetaillist']->staffid);
    $content['Tcmail']->name; 
        $mail = $content['Tcmail']->emailid; 
       
$candidate = $content['staffdetaillist']->candidateid;
// print_r($candidate); die;

  if(count($candidate) ==null){

     $this->session->set_flashdata('er_msg', 'Error !!! There is no candidate id !!!');
     redirect('/Staff_personnel_probation/index/'.$token);  
     // exit("hbuhbj");
    }

   $tablename = "'staff_transaction'";
   $incval = "'@a'";

   $getstaffincrementalid = $this->Staff_personnel_probation_approval_model->getStaffTransactioninc($tablename,$incval); 
   $autoincval =  $getstaffincrementalid->maxincval;
       // echo $autoincval; die;

   $this->db->trans_status();

   $insertArr = array(
    'id'                   => $autoincval,
    'staffid'              => $content['staffdetaillist']->staffid,
    'old_office_id'        => (trim($content['staffdetaillist']->new_office_id)==""?null:($content['staffdetaillist']->new_office_id)),
    'new_office_id'        => (trim($content['staffdetaillist']->new_office_id)==""?null:($content['staffdetaillist']->new_office_id)),
    'date_of_transfer'     => date("Y-m-d"),
    'old_designation'      => (trim($content['staffdetaillist']->designation)==""?null:($content['staffdetaillist']->designation)),
    'new_designation'      => (trim($content['staffdetaillist']->designation)==""?null:($content['staffdetaillist']->designation)),
    'trans_status'         => 'Probation',
    'reportingto'          => (trim($content['staffdetaillist']->reportingto)==""?null:($content['staffdetaillist']->reportingto)),
    'trans_flag'           => 1,
    'createdon'            => date("Y-m-d H:i:s"),
    'createdby'            => $this->loginData->staffid
  );
   // print_r($insertArr); die;
   $this->db->insert('staff_transaction', $insertArr);

   $updateArr = array(
    'probation_status'     => 1,
    'createdon'            => date("Y-m-d H:i:s"),
    'createdby'            => $this->loginData->staffid
  );

   $this->db->WHERE('staffid', $content['staffdetaillist']->staffid); 
   $this->db->update('staff', $updateArr);


   $insertworkflowArr = array(
    'r_id'           => (trim($autoincval)==""?null:($autoincval)),
     'type'           => 7,
     'staffid'        => $token,
     'sender'         => $this->loginData->staffid,
     'receiver'       => (trim($content['staffdetaillist']->reportingto)==""?null:($content['staffdetaillist']->reportingto)),
     'senddate'       => date("Y-m-d H:i:s"),
     'flag'           => 1,
     // 'scomments'      => (trim($this->input->post('comments'))==""?null:$this->input->post('comments')),
     'createdon'      => date("Y-m-d H:i:s"),
     'createdby'      => $this->loginData->staffid,
   );
  $this->db->insert('tbl_workflowdetail', $insertworkflowArr);


   // $html = 'Dear '.$Tcmail->name.', <br><br> 
   //     Congratulations you are confirm by Pradan. 
   //     <br>
   //     <br><br>
   //     Regard,<br>Pradan Team';

       $html = 'Dear '.$content['staffdetaillist']->name.', <br><br> 
       Your probation review process has been initiated. 
       <br>
       Your Team Co-ordinator will review your performance and will confirm same to you.
       <br>
       In case you have any queries related to your probation, do not hesitate to reach your Team Co-ordinator. 
       <br>
       <br>
       From,<br>Personnel Department of PRADAN'
       ;


       

      $sendmail = $this->Common_Model->send_email($subject = 'Probation Review Process Initiated', $message = $html, $to_name=null,$staffmail);

      $htmltc = 'Dear '.$content['Tcmail']->name.', <br><br> 
       You have to review probation for staff '.$content['staffdetaillist']->name.'
       <br>
       Kindly submit your probation review report on time.
       <br>
       In case you have any queries related to staff probation, do not hesitate to call us. 
       <br>
       <br>
       From,<br>Personnel Department of PRADAN'
       ; 

      $sendmailtc = $this->Common_Model->send_email($subject = 'Probation Review Process Initiated', $message = $htmltc, $to_name=null, $mail);
      



  $this->db->trans_complete();


        if ($this->db->trans_status() === true){

          redirect('/Staff_personnel_probation/'); 
          $this->session->set_flashdata('tr_msg', 'Successfully Probation process  initiated !!!');
           
        }else{
          $this->session->set_flashdata('er_msg', 'Error in Probation  !!!');
          redirect(current_url());
        }


 



$content['subview'] = 'Staff_personnel_probation_approval/add';
$this->load->view('_main_layout', $content);
}
}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}
}
