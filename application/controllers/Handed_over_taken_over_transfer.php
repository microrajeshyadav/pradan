<?php 

///// Handed over taken over Controller   

class Handed_over_taken_over_transfer extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Stafftransferpromotion_model","Stafftransferpromotion_model");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->load->model('Global_model', 'gmodel');
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
		//print_r($this->loginData);
	}

	public function index($token)
	{//die("ddhgdh");

	try{
	    // start permission 

		

		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission

		$content['staff_details'] = $this->model->get_staffDetails($token);

		$content['expense'] = $this->model->count_handedchrges($token);

		$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
		$result  = $this->db->query($sql)->result()[0];
		$forwardworkflowid = $result->workflowid;
		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		$sql="select * from tbl_iom_transfer where transid=$token";
		$staff= $this->db->query($sql)->row();
		//print_r($staff);
		$taken=$staff->current_responsibility_to;
		$handed=$staff->staffid;

			/*echo "<pre>";
			print_r($taken);exit();*/
			$content['transfer_expeness_details']  = array();
			if($this->loginData->staffid == $taken){
				$newtcquery = "SELECT * FROM staff_transaction WHERE trans_status='Transfer' AND staffid=".$content['staff_details']->staffid." AND id=".$token;
				$newtc = $this->db->query($newtcquery)->row();
				$qry = "select * from tbl_hand_over_taken_over_charge where staffid=".$content['staff_details']->staffid." AND type=1";
				$content['handed_over_data'] = $this->db->query($qry)->row();
				$content['transfer_expeness_details']=$this->model->expeness_details($content['handed_over_data']->id);
			/*echo "<pre>";
			print_r($content['transfer_expeness_details']);exit();*/
			$receiverdetail = $this->Staff_seperation_model->get_staffDetails($taken);
			$receiver = $newtc->reportingto;
			$staff_id = $content['staff_details']->staffid;
			$content['t_id']=$this->model->getTransid($staff_id);

			$id = $content['t_id']->id;
			$flag = 13;
		}else if($this->loginData->staffid ==$handed)
		{
			$staff_id=$this->loginData->staffid;

			$content['t_id']=$this->model->getTransid($staff_id);

			$id = $content['t_id']->id;
			$flag = 11;
			$receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['staff_details']->reportingto);
			//$receiver = $content['staff_details']->reportingto;
			$receiver = $taken;
		}

		$content['flag'] = $flag;

		/*echo "<pre>";
		print_r($content['flag']);
		die;*/
		$content['getstafflist'] = $this->model->getStaffList();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{	
			// echo "<pre>";
			// print_r($_POST);
			// echo $btnsubmit=$this->input->post('btnsubmit');
			// die;
			$submitdatasend = $this->input->post('submitbtn');
			//echo "submit=".$submitdatasend;

			$Sendsavebtn = $this->input->post('savetbtn');
			//echo "save=".$Sendsavebtn;
			//die;
			
			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
      		// print_r($_POST);
      		// die;

				$transferno=$this->input->post('transferno');
				$tdate=$this->input->post('transferno');
				$t1date=$this->gmodel->changedatedbformate($tdate);

				$insertArraydata=array(

					'type'         =>$this->input->post('handed'),
					'transfernno'  =>$this->input->post('transferno'),
					'staffid'      =>$staff_id,
					'transfernno'  =>$transferno,
					'flag'         =>0,
				);
				

				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
				$insertid = $this->db->insert_id();

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 

					$arr = array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

					);		
					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				}


				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Handed_over_taken_over_transfer/edit/'.$insertid);			
				}

			}




			else if($this->input->post('comments'))
			{

				 //secho "<pre>";
				// 'transfernno'  =$this->input->post('transferno');
			//print_r($this->input->post()); 
				$transferno=$this->input->post('transferno');
				
				$tdate=$this->input->post('tdate');
				$t1date=$this->gmodel->changedatedbformate($tdate);

				$insertArraydata=array(
					'type'         =>$this->input->post('handed'),
					
					'trans_date'   =>$t1date,
					'staffid'      =>$staff_id,
					'transfernno'  =>$transferno,
					'transid'=>$token,
					'flag'         =>1
				);
			// 	echo "<pre>";
			// 	print_r($insertArraydata);
			// die();
				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);

				//$this->db->insert($insertArraydata);

				$insertid = $this->db->insert_id();
				//echo $this->db->last_query();  die;

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

					);

					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

				}
				
				$updateArr = array(                         
					'trans_flag'     => $flag,
					'updatedon'      => date("Y-m-d H:i:s"),
					'updatedby'      => $this->loginData->staffid
				);

				$this->db->where('id',$token);
				$this->db->update('staff_transaction', $updateArr);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 2,
					'staffid'              => $content['staff_details']->staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $receiver,
					'forwarded_workflowid' => $forwardworkflowid,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => $flag,
					'scomments'            => $this->input->post('comments'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
         // echo $this->db->last_query();
         // die;

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$subject = "Your Process Approved";
					$body = 'Dear,<br><br>';
					$body .= '<h2>Your Request has been Submited Successfully </h2><br>';
					$body .= 'This is to certify that Mr. '.$content['staff_details']->name.'<br> your process has Approved  .<br><br>';
					$body .= 'Thanks<br>';
					$body .= 'Administrator<br>';
					$body .= 'PRADAN<br><br>';

					$body .= 'Disclaimer<br>';
					$body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


					$to_email = $receiverdetail->emailid;
					$to_name = $receiverdetail->name;
					//$to_cc = $getStaffReportingtodetails->emailid;
					$recipients = array(
						$personnel_detail->EmailID => $personnel_detail->UserFirstName,
						$content['staff_details']->emailid => $content['staff_details']->name
					// ..
					);
					$email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
					if (substr($email_result, 0, 5) == "ERROR") {
						$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					}
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Handed_over_taken_over_transfer/view/'.$token.'/'.$insertid);			
				}

			}

		}

		/*echo "<pre>";
		print_r($content);die();*/
		$content['subview']="index";
		$content['title'] = 'Handed_over_taken_over_transfer';	
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

public function Edit($token)
{
	try{
	    // start permission 
		$staff_id=$this->loginData->staffid;
		//echo "staff_id=".$staff_id;


		$content['t_id']=$this->model->getTransid($staff_id);
		$id=$content['t_id']->id;

		$content['staff_details']=$this->model->get_staffDetails($id);			
			$content['expense']=$this->model->count_handedchrges($token);			
			$content['transfer_expeness_details']=$this->model->expeness_details($token);
			$content['handed_expeness']=$this->model->handed_over_charge($token);
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();		
			$content['getstafflist'] = $this->model->getStaffList();
			$content['subview']="index";
			$RequestMethod = $this->input->server('REQUEST_METHOD'); 
			if($RequestMethod == "POST")
				{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');			
			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

				$transferno=$this->input->post('transferno');
				$insertArraydata=array(
					'type'         =>$this->input->post('handed'),
					'staffid'=>$staff_id,
					'transfernno'=>$transferno,
					'flag'=>0
				);
				
				$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
				$this->db->where('id',$token);

				$countitem = count($this->input->post('items'));

				$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);


				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array(
						
						'handedtaken_id' =>$token,
						'item' =>$this->input->post('items')[$i],
						'description'=>$this->input->post('items_description')[$i],
					);
					
					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
				}

				//die();
				

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Handed_over_taken_over_transfer/edit/'.$token);			
				}

			}




			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				
				$transferno=$this->input->post('transferno');

				$insertArraydata=array(
					'type'         =>$this->input->post('handed'),
					'staffid'      =>$staff_id,
					'transfernno'  =>$transferno,
					'flag'         =>1
				);
				
				$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
				$this->db->where('id',$token);

				$countitem = count($this->input->post('items'));

				$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);


				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array(
						
						'handedtaken_id' =>$token,
						'item' =>$this->input->post('items')[$i],
						'description'=>$this->input->post('items_description')[$i],
					);
					
					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
				}


				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Handed_over_taken_over_transfer/view/'.$token);			
				}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}




			
		}
		$content['title'] = 'Handed_over_taken_over_transfer/edit';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}
public function view($token,$inserted)
{
	try{


		$staffidquery = "SELECT * FROM tbl_hand_over_taken_over_charge WHERE id=".$inserted;
		$staffidres   = $this->db->query($staffidquery)->row();	 		 	

		$staff_id     = $staffidres->staffid;
		$content['t_id']=$this->model->getTransid($staff_id);			
		$id=$content['t_id']->id;
		$content['staff_details']=$this->model->get_staffDetails($id);
		$content['expense']=$this->model->count_handedchrges($inserted);
		$content['transfer_expeness_details_handed_over'] = array();
		if($staffidres->type == 2){
			$qry = "select * from tbl_hand_over_taken_over_charge where staffid=".$staffidres->staffid." AND type=1";
			$content['handed_over_data'] = $this->db->query($qry)->row();
			$content['transfer_expeness_details_handed_over']=$this->model->expeness_details($content['handed_over_data']->id);
		}			
		$content['transfer_expeness_details'] = $this->model->expeness_details($inserted);
		$content['handed_expeness']=$this->model->handed_over_charge($inserted);
 		//print_r($content['handed_expeness']);
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		
		$content['title'] = 'Handed_over_taken_over_transfer/view';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}