<?php 
class Campusincharge extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Campusincharge_model",'model');

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');



	}

	public function index()
	{
		 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." "; 
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			$campusid = $this->input->post('campusid');

			if ($campusid =='') {
				$campusid = 'NULL';
			}

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				$savewrittenscore = $this->input->post('save_writtenscore');

				if (isset($savewrittenscore) &&  $savewrittenscore =='save') {

					$writtenscore = $this->input->post('writtenscore'); 
					foreach ($writtenscore as $key => $value) {

						$this->db->trans_start();

						$this->db->where('candidateid', $key);
						$num_rows =  $this->db->count_all_results('tbl_candidate_writtenscore'); 

						if($num_rows > 0 ){

						$sql = "SELECT * FROM `tbl_candidate_writtenscore` where `candidateid`=$key"; 
						$query = $this->db->query($sql);
						$result = $this->db->query($sql)->result();
					
						$writtenscoreid = $result[0]->writtenscoreid;

							$updateArr = array(
								'writtenscore'    		    => ($value ==''? 0: $value),
								'candidateid'    		    => $key,
								'flag'						=> 'save',
								'updatedon'      		    => date('Y-m-d H:i:s'),
					            'updatedby'      		    => $this->loginData->UserID, // login user id
					       );

							$this->db->where('writtenscoreid', $writtenscoreid);
							$this->db->update('tbl_candidate_writtenscore', $updateArr);

							 $candidateupdateArr = array(
								'wstatus'    		    => 0,
							);
					
							$this->db->where('candidateid', $key);
							$this->db->update('tbl_candidate_registration', $candidateupdateArr);
						
						}else{

						$insertArr = array(
								'writtenscore'    		    => ($value ==''? 0: $value),
								'candidateid'    		    => $key,
								'flag'				        => 'save',
								'Createdon'      		    => date('Y-m-d H:i:s'),
					            'Createdby'      		    => $this->loginData->UserID, // login user id
					);

//print_r($insertArr); die;
							$this->db->insert('tbl_candidate_writtenscore', $insertArr);
//echo $this->db->last_query(); die;


							 $candidateupdateArr = array(
								'wstatus'    		    => 0,
							);
					
							$this->db->where('candidateid', $key);
							$this->db->update('tbl_candidate_registration', $candidateupdateArr);

						}

						$this->db->trans_complete();
					}
					
					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Written Score');	
						redirect('/Writtenscore/index');
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added Written Score');	
						redirect('/Writtenscore/index');		
					}
					
				}
			}

			$content['selectedcandidatedetails']  = $this->model->getSelectedCandidate($campusid);
			$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
			$content['selectedcandidatewrittenscore'] 	 = $this->model->getSelectedCandidateWrittenScore($campusid);
			$content['method']  = $this->router->fetch_method();
			$content['title']  = 'Campusincharge';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	public function Selectcandidates() 
	{
				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

		//	print_r($this->loginData); 

			$campusid = $this->loginData->campusid;
			if ($campusid =='') {
				$campusid ='NULL';
			}else{
				$content['searchcampusid'] = $campusid;
			}


			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				// print_r($this->input->post()); 

					$status = $this->input->post('status');
					$campusid = $this->input->post('campusid'); 

					foreach ($status as $key => $value) {

					  $sql = " SELECT * FROM  tbl_accept_campusinchargetocandidate WHERE campusid =".$campusid." AND candidateid = ".$key."";
						$query = $this->db->query($sql);
						$numrowdata = $query->num_rows();

						//echo $numrowdata; die;

						if ($numrowdata > 0) {

							$this->session->set_flashdata('er_msg', 'Already accepted this campus candidate list ! Please try other candidates list !!!');	
							redirect(current_url());
					
						}else{





					$Insertarra = array(
					'campusid'    		        => $this->input->post('campusid'),
					'candidateid'    		    => $key,
					'status'				    => $value,
					'createdon'      		    => date('Y-m-d H:i:s'),
		            'createdby'      		    => $this->loginData->UserID,
					);

					$this->db->insert('tbl_accept_campusinchargetocandidate', $Insertarra);
				}
			}
					
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error Candidate not accept ');	
					redirect(current_url());	
				}else{

					$this->session->set_flashdata('tr_msg', 'Successfully Candidate accept');
					redirect('Campusincharge/Selectcandidates');
				}

			}

		
			$selected_candidate_list = $this->model->getElihibalCandidateListForWrittenExam($campusid);

			$content['selectedcandidatedetails'] = $selected_candidate_list;


		    $content['getstatuscandidate'] = $this->model->getAcceptedcandidate($campusid);

		    $content['getcountstatus'] = $this->model->getcampusinchargestatus($campusid);


		    
		

			$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);

			$content['title']    = 'Campusincharge';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




	public function Shortlistedcandidates() 
	{
				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

		//	print_r($this->loginData); 

			$campusid = $this->loginData->campusid;
			if ($campusid =='') {
				$campusid ='NULL';
			}else{
				$content['searchcampusid'] = $campusid;
			}


			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				// print_r($this->input->post()); 

				
			}

		
			$selected_candidate_list = $this->model->getShortlistCandidate($campusid);

			$content['selectedcandidatedetails'] = $selected_candidate_list;

			$content['title']    = 'Campusincharge';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	public function generate_pdf_selected_candidate($token) 
	{
				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			$selected_candidate_list = $this->model->getElihibalCandidateListForWrittenExam($token);
			$valcount = count($selected_candidate_list);

			if ($valcount > 0) {
			
			$table = ' <table>';
			$table .= '<tr>';
			$table .= '<td>S.No.</td>';
			$table .= '<td>Candidate</td>';
			$table .= '<td> Email Id</td>';
			$table .= '</tr>'; 
			$i=0;
			foreach ($selected_candidate_list as $key => $value) { 

				$table .=  '<tr>';
				$table .=  ' <td>'.$i.'</td>';
				$table .=  '<td>'.$value->candidatefirstname.'</td>';
				$table .=  '<td>'.$value->emailid.'</td>';
				$table .=  '</tr> ';
			$i++; }
			$table .=  '</table> ';

			//echo $table; die;
			$filename = md5(time() . rand(1,1000));

			 $insertArr = array(
						'name' => $filename,
						'flag' => 0,
			);
			$this->db->insert('genratepdflist', $insertArr);
			
			$this->load->model('Dompdf_model');
			$generate =   $this->Dompdf_model->generateexamlistPDF($table, $filename, NULL,'Generateofferletter.pdf');
			redirect('/Writtenscore/Selecttowrittenexamcandidates');
		}

				
			$content['title']    = 'Writtenscore';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}





	public function send($token) 
	{
				
		try{

		
			$campusdetail = $this->model->getcampusInchargeEmailid($token);

			$CampusInchargeemailid = $campusdetail->emailid;
			$CampusInchargeename   = $campusdetail->campusincharge;

			$selected_candidate_list = $this->model->getgenratepdf($token);
		
			$filename = $selected_candidate_list->name;
 			
			$hrdemail = $this->loginData->EmailID; /////// hrd email id/////
			 
		    $attachments = array($filename.'.pdf');

		    $html = 'Dear Sir, <br><br> 
					Congratulations you are short list by Pradan.';

			$sendmail = $this->Common_Model->send_email($subject = 'Selected candidate list ', $message = $html, $CampusInchargeemailid, $hrdemail, $attachments);  //// Send Mail 
		
			if ($sendmail == false) {
				$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
			}
			
			redirect('/Writtenscore/Selecttowrittenexamcandidates/');
						
			$content['title']    = 'Writtenscore';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

}