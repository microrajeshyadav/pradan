<?php 
class Hrscore extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Hrscore_model','model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");
		// $this->load->model(__CLASS__ . '_model','model');
		// $mod = $this->router->class.'_model'; 
	       
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
    
     // start permission 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{
			if($this->input->post('campusid') != NULL && $this->input->post('campusid') !=''){
			$campusdata = $this->input->post('campusid');
			$expcampusdata = explode('-',$campusdata);
			$campusid = $expcampusdata[0];
			$campusintimationid = $expcampusdata[1];
			//print_r($expcampusdata); die;
		

		}else {
			$campusdata = NULL;
			$campusid = 'NULL';
			$campusintimationid = 'NULL';
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){

			$savehrscore = $this->input->post('save_hrscore');
			if (isset($savehrscore) && $savehrscore =='save' && $savehrscore !='') {
				

				if($this->input->post('hrscore') !='' && $this->input->post('hrscore') !=NULL){	
				$this->db->trans_start();
				$email_ = '';
				$email_ = $this->input->post('email_');
				
	            $HRscoreVal = $this->input->post('hrscore'); 

	            foreach ($HRscoreVal as $key => $value) {
	            	$cand = $this->gmodel->getCandidateDetails($key); 

	            	   $this->db->where('candidateid', $key);
						$num_rows =  $this->db->count_all_results('tbl_candidate_hrscore'); 

	            	if($num_rows > 0 ){

	            	$sql = "SELECT * FROM `tbl_candidate_hrscore` where `candidateid`=$key"; 
    		    	$result = $this->db->query($sql)->result();
    		    	$hrscoreid = $result[0]->hrscoreid;

    		    		$updateArr = array(
						'candidateid'    		    => $key,
						'hrscore'    		        => ($value ==''? 0: $value),
						'flag'			            => 'save',
						'updatedon'      		    => date('Y-m-d H:i:s'),
					    'updatedby'      		    => $this->loginData->UserID, // login user id
					  );
					
					$this->db->where('hrscoreid', $hrscoreid);
					$this->db->update('tbl_candidate_hrscore', $updateArr);

					$candidateupdateArr = array(
						'gdstatus'    	=> 1,
						'hrstatus'		=> 1,
						'inprocess'		=> 'closed',
					);
					
				$this->db->where('candidateid', $key);
				$this->db->update('tbl_candidate_registration', $candidateupdateArr);


    		    	}else{

    		    		$insertArr = array(
						'candidateid'    	=> $key,
						'hrscore'    		=> ($value ==''? 0: $value),
						'flag'				=> 'save',
						'createdon'      	=> date('Y-m-d H:i:s'),
					    'createdby'      	=> $this->loginData->UserID, // login user id
					  );
					
					$this->db->insert('tbl_candidate_hrscore', $insertArr);

					$candidateupdateArr = array(
						'gdstatus'    		    => 1,
						'hrstatus'		        => 1,
						'inprocess'		=> 'closed',
					);
					
				$this->db->where('candidateid', $key);
				$this->db->update('tbl_candidate_registration', $candidateupdateArr);

   		    	}

   		    	// for offcampus
   		    	if($campusid == 0)
   		    	{
   		    		foreach ($email_ as $key1 => $value1) {
				 	if($value1 == 2)
				 	{
   		    		$candidateemail = '';
   		    		$candidatefirstname = '';
   		    		$candidateemail = $cand->emailid; 
   		    		$candidatefirstname  =$cand->candidatefirstname;  
				// 			// $candidatemiddlename =$this->input->post('candidatemname')[$value]; 
				// 			$candidatelastname = $this->input->post('candidatelname')[$value];
   		    		$add_link = site_url('login/login1');
				// 			$subject = 'Shortlisted in GD on submission';
   		    		$hrname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
   		    		$candidate = array('$candidateemail','$candidatfirstname','$to_email','$addlink','$hrname');
   		    		$candidate_replace = array($candidateemail,$candidatefirstname,$candidateemail,$add_link,$hrname);


							// eval ("\$message = \"$message\";");
							// $message =nl2br($message);
							// $body = $message; 
							//update from db
   		    		$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 73 AND `isactive` = '1'";
   		    		$data = $this->db->query($sql)->row();
   		    		if(!empty($data))
   		    			$body = str_replace($candidate,$candidate_replace , $data->lettercontent);
							// print_r($body); die;
   		    		$sendmail = $this->Common_Model->send_email($subject="Submit your Documents",$body,$candidateemail);
					}
				}
				}


				$this->db->trans_complete();
			}
		}
					
				if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding HR Score');	
						redirect('/Hrscore/add');
				}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added HR Score');	
						redirect('/Hrscore/index');
		
				}
					
				}
			}

			$content['campusid'] = $campusdata;
			$content['selectedcandidatedetails']          = $this->model->getSelectedCandidate($campusid, $campusintimationid);
			$content['campusdetails'] 	                  = $this->model->getCampus($this->loginData->UserID);
			$content['selectedcandidatehrscore'] 	      = $this->model->getSelectedCandidateHRScore($campusid, $campusintimationid); 
			/*echo "<pre>";
			print_r($content['selectedcandidatehrscore']);exit();*/

			$content['title'] 		= 'Hrscore';
			$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
						print_r($e->getMessage());die;
		}
	}


public function preview($token)
{

if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Hrscore/index');
				
			} else { 


	try{

		//echo $token;
		

		$fcount = $this->model->getCountFamilyMember($token);

		$content['familycount']= $fcount;  

		$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetailsPrint($token);

		$content['candidatedetails']         = $this->model->getCandidateDetailsPrint($token);

		$Icount = $this->model->getCountIdentityNumber($token);

		$content['identitycount']= $Icount;  

		$content['identitydetals'] = $this->model->getCandidateIdentityDetailsPrint($token);


		$TEcount = $this->model->getCountTrainingExposure($token);

		$content['TrainingExpcount']= $TEcount;  

		$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($token);


		$GPYcount = $this->model->getCountGapYear($token);

		$content['GapYearCount']= $GPYcount;  

		$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($token);



		@ $Lcount = $this->model->getCountLanguage($token);

		@ $content['languageproficiency']= $Lcount;  

		@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($token);
		$content['languagedetalsprint'] = $this->model->getCandidateLanguageDetailsPrint($token);
		@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($token);


		$WEcount = $this->model->getCountWorkExprience($token);

		$content['WorkExperience']= $WEcount;  

		$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($token);

		$content['title'] = 'Hrscore';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

		}
}


	
	public function select_personal_interview_candidates(){
	try{
		  // start permiss0ion 
$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		$expcampusdata = '';
		$campusid = 0;
		$campusintimationid = 0;
		

		if($this->input->post('campusid') != NULL && $this->input->post('campusid') !=''){
			$campusdata = $this->input->post('campusid');
			$expcampusdata = explode('-',$campusdata);
			$campusid = $expcampusdata[0];
			$campusintimationid = $expcampusdata[1];
			//print_r($expcampusdata); die;
		

		}else {
			$campusdata = NULL;
			$campusid = 'NULL';
			$campusintimationid = 'NULL';
		}

		$content['campusid'] = $campusdata;
		$content['selectedcandidatedetails']  = $this->model->getSelectedCandidateforPersonalInterview($campusid,$campusintimationid);
		$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
		$content['selectedcandidatehrscore']= $this->model->getSelectedCandidateHRScore($campusid,$campusintimationid); 

		$content['title'] 		= 'Hrscore';
		$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
					print_r($e->getMessage());die;
	}

	
	}


}