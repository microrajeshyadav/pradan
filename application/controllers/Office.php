<?php 
class Office extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Office_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_Model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token = null)
	{
		// start permission 
		try{
			$query = "SELECT o.lpo, o.officeid,  o.officetype, o.officename, o.street, s.name as statename, st.name as staffname, o.closed FROM lpooffice o inner join state s on o.state = s.id inner join staff st on o.staffid = st.staffid ORDER BY id DESC";
			$content['lpooffice'] = $this->db->query($query)->result();
		// end permission   

			$content['token'] = $token;	
			$content['title'] = 'Staff_sepclearancecertificate';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


public function view($token = null)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission   

			$clear_seperate_detail = $this->Staff_sepclearancecertificate_model->get_staff_sepration_clearance_detail($token);



			// print_r($clear_seperate_detail); die;
			$transid = $clear_seperate_detail->transid;

			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;
			// print_r($clear_seperate_detail); die();

			$content['clearance_transaction'] = $this->Staff_sepclearancecertificate_model->get_staff_sepration_clearance_transaction($token);

			$content['clearance_detail'] = $this->Staff_sepclearancecertificate_model->get_staff_clearance_detail($transid);
			
			$RequestMethod = $this->input->server("REQUEST_METHOD");
			if($RequestMethod == 'POST')
			{
			
			}



			$content['title'] = 'Staff_sepclearancecertificate';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


	}