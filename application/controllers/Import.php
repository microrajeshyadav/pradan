<?php 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Import Controller
 *
 * @author TechArise Team
 *
 * @email  info@techarise.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import extends CI_Controller {



    public function __construct() {
        parent::__construct();
        $this->load->library('excel');
        $this->load->library("upload");
        $this->load->helper(array('form', 'url'));

        $this->load->model('Import_model', 'import');
    }

    // upload xlsx|xls file
    public function index() {
        $data['page'] = 'import';
        $data['title'] = 'Import XLSX | TechArise';
        $this->load->view('import/index', $data);
    }
    // import excel data
    public function save() {
        $this->load->library('excel');


        
        if ($this->input->post('importfile')) {
            // $path = ROOT_UPLOAD_IMPORT_PATH; 

           //  print_r($this->input->post());
             //print_r($_FILES);
           //  $path = BASE_URL()."datafiles/uploads/";
           
           // //echo  $_FILES['userfile']['name']; die;
           //  $config['upload_path'] = $path;
           //  $config['allowed_types'] = 'xlsx|xls';
           //  $config['remove_spaces'] = TRUE;
           //  $this->upload->initialize($config);
           //  $this->load->library('upload', $config);
           // // echo $this->upload->do_upload('userfile'); 
           //  if (!$this->upload->do_upload('userfile')) {
           //      $error = array('error' => $this->upload->display_errors());
           //  } else {
           //      $data = array('upload_data' => $this->upload->data());
           //  }

           // // echo $data['upload_data']['file_name']; die;
            
           //  if (!empty($data['upload_data']['file_name'])) {
           //      $import_xls_file = $data['upload_data']['file_name'];
           //  } else {
           //      $import_xls_file = 0;
           //  }

          if ($_FILES['userfile']['name'] != NULL) {

            @  $ext = end((explode(".", $_FILES['userfile']['name']))); 

            $OriginalName = $_FILES['userfile']['name']; 
            $encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
            $tempFile = $_FILES['userfile']['tmp_name'];
            $targetPath = FCPATH . "datafiles/uploads/";
            $targetFile = $targetPath . $OriginalName;
            $uploadResult = move_uploaded_file($tempFile,$targetFile);

            if($uploadResult == true){
              $encryptedImage = $OriginalName;
            }else{

              die("Error uploading Profile Photo ");
              $this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
              redirect(current_url());
            }

          }

            $inputFileName = $targetPath . $OriginalName;
           // echo $inputFileName; die;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                //print_r($objReader);
                $objPHPExcel = $objReader->load($inputFileName);
                //print_r($objPHPExcel); die;
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('First_Name', 'Last_Name', 'Email', 'DOB', 'Contact_NO');
            $makeArray = array('First_Name' => 'First_Name', 'Last_Name' => 'Last_Name', 'Email' => 'Email', 'DOB' => 'DOB', 'Contact_NO' => 'Contact_NO');
            $SheetDataKey = array();
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    } else {
                        
                    }
                }
            }
            $data = array_diff_key($makeArray, $SheetDataKey);
           
            if (empty($data)) {
                $flag = 1;
            }
            if ($flag == 1) {
                for ($i = 2; $i <= $arrayCount; $i++) {
                    $addresses = array();
                    $firstName = $SheetDataKey['First_Name'];
                    $lastName = $SheetDataKey['Last_Name'];
                    $email = $SheetDataKey['Email'];
                    $dob = $SheetDataKey['DOB'];
                    $contactNo = $SheetDataKey['Contact_NO'];
                    $firstName = filter_var(trim($allDataInSheet[$i][$firstName]), FILTER_SANITIZE_STRING);
                    $lastName = filter_var(trim($allDataInSheet[$i][$lastName]), FILTER_SANITIZE_STRING);
                    $email = filter_var(trim($allDataInSheet[$i][$email]), FILTER_SANITIZE_EMAIL);
                    $dob = filter_var(trim($allDataInSheet[$i][$dob]), FILTER_SANITIZE_STRING);
                    $contactNo = filter_var(trim($allDataInSheet[$i][$contactNo]), FILTER_SANITIZE_STRING);
                    $fetchData[] = array('first_name' => $firstName, 'last_name' => $lastName, 'email' => $email, 'dob' => $dob, 'contact_no' => $contactNo);
                }              
                $data['employeeInfo'] = $fetchData;
                $this->import->setBatchImport($fetchData);
                $this->import->importData();
            } else {
                echo "Please import correct file";
            }
        }
        $this->load->view('import/display', $data);
        
    }
}
?>