<?php 
/**
* Village List
*/
class Village extends Ci_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
	}

	public function index()
	{
		try{
          //state list
		
		$content['state_list'] = $this->db->get('mststate')->result();

		$state_code = $this->input->get('state_code');

		if ($state_code != NULL) {
			$this->db->select('a.DistrictCode,a.DistrictUID, a.Name as DistrictName, b.Name as StateName');
			$this->db->from('mstdistrict as a');
			$this->db->join('mststate as b', 'a.StateCode = b.StateCode');
			$this->db->where('a.StateCode', $state_code);
			$content['district_list'] = $this->db->get('')->result();
		}else{
			$content['district_list'] = [];
		}
           //district list
        $content['district_list'] = $this->db->get('mstdistrict')->result();

		$district_code = $this->input->get('district_code');

		if ($district_code != NULL) {
			$this->db->select('*,a.Name as sateName,b.Name as distName,c.Name as blockName,c.DistrictCode,b.DistrictCode,a.StateCode');    
			$this->db->from('mststate as a');
			$this->db->join('mstdistrict as b', 'b.StateCode = a.StateCode');
			$this->db->join('mstblock as c', 'c.DistrictCode = b.DistrictCode');
			$this->db->where('c.DistrictCode', $district_code);
			$content['block_list'] = $this->db->get('')->result();
		}else{
			$content['block_list'] = [];
		} 
         
          //Block list
        $content['block_list'] = $this->db->get('mstblock')->result();

		$block_code = $this->input->get('block_code');

		if ($block_code != NULL) {
			$this->db->select('*,a.Name as sateName,b.Name as distName,c.Name as blockName,c.DistrictCode,b.DistrictCode,a.StateCode,d.Name,d.PanchayatCode');    
			$this->db->from('mststate as a');
			$this->db->join('mstdistrict as b', 'b.StateCode = a.StateCode');
			$this->db->join('mstblock as c', 'c.DistrictCode = b.DistrictCode');
			$this->db->join('mstpanchayat as d', 'd.BlockCode = c.BlockCode');
			$this->db->where('d.BlockCode', $block_code);
			$content['panchayat_list'] = $this->db->get('')->result();
		}else{
			$content['panchayat_list'] = [];
		} 
		  //Panchayat list
        $content['panchayat_list'] = $this->db->get('mstpanchayat')->result();

		$panchayat_code = $this->input->get('panchayat_code');

		if ($panchayat_code != NULL) {
			$this->db->select('*,a.Name as sateName,b.Name as distName,c.Name as blockName,c.DistrictCode,b.DistrictCode,a.StateCode,d.Name,d.PanchayatCode,e.PanchayatCode,e.Name as panchayatName');    
			$this->db->from('mststate as a');
			$this->db->join('mstdistrict as b', 'b.StateCode = a.StateCode');
			$this->db->join('mstblock as c', 'c.DistrictCode = b.DistrictCode');
			$this->db->join('mstpanchayat as d', 'd.BlockCode = c.BlockCode');
			$this->db->join('mstvillage as e', 'e.PanchayatCode = d.PanchayatCode');
			$this->db->where('e.PanchayatCode', $panchayat_code);
			$content['village_list'] = $this->db->get('')->result();
		}else{
			$content['village_list'] = [];
		} 

		$content['subview']="index";

		$content['title'] = 'Panchayat';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function Add()
	{
		try{
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
			$insertArr = array(
				'StateCode'      => $this->input->post('StateCode'),
				'DistrictCode'   => $this->input->post('DistrictCode'),
				'BlockCode'      => $this->input->post('BlockCode'),
				'PanchayatCode'  => $this->input->post('PanchayatCode'),
				'VillageCode'  => $this->input->post('VillageCode'),
				'Name'         => $this->input->post('Name'),
				'Name_H'       => $this->input->post('Name_H'),
				'Name_SN'      => $this->input->post('Name_SN'),
				'Age15_29M'    => $this->input->post('Age15_29M'),
				'Age15_29F'    => $this->input->post('Age15_29F'),
				'Age30_70M'    => $this->input->post('Age30_70M'),
				'Age30_70F'    => $this->input->post('Age30_70F'),
				'IsDeleted'    => 0,
				);
			// print_r($insertArr); die();
			$this->Common_Model->insert_data('mstvillage', $insertArr);
			$this->session->set_flashdata('tr_msg', 'Successfully added Village');
			redirect('/Village/');
		}
		$content['title'] = 'Village';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function edit($id)
	{
		try{
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
			$updateArr = array(
				'VillageCode'  => $this->input->post('VillageCode'),
				'Name'         => $this->input->post('Name'),
				'Name_H'       => $this->input->post('Name_H'),
				'Name_SN'      => $this->input->post('Name_SN'),
				'Age15_29M'    => $this->input->post('Age15_29M'),
				'Age15_29F'    => $this->input->post('Age15_29F'),
				'Age30_70M'    => $this->input->post('Age30_70M'),
				'Age30_70F'    => $this->input->post('Age30_70F'),
				'IsDeleted'    => 0,
				);
			
			$this->Common_Model->update_data('mstvillage', $updateArr,'VillageCode',$id);
			$this->session->set_flashdata('tr_msg', 'Successfully Updated Village');
			redirect('/Village/');
		}
		$content['title'] = 'Village';
		$village_list = $this->Common_Model->get_data('mstvillage', '*', 'VillageCode',$id);
		$content['village_list'] = $village_list;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	function delete($PanchayatUID)
	{
		try{
		$this->Common_Model->delete_row('mstpanchayat','PanchayatUID', $PanchayatUID); 
		$this->session->set_flashdata('tr_msg' ,"Village Deleted Successfully");
		redirect('/Village/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

}