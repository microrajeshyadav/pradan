<?php 

/**
* General Nomination And Authorisation Form controller
*/
class General_nomination_and_authorisation_form_staff extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		//$this->load->Libraries('form_validation');
		
		$this->load->model("General_nomination_and_authorisation_form_staff_model");
		//echo "dhgfhdsg";
		//die();
		$this->load->model("Provident_fund_nomination_form_model");
		
		$this->load->model("Employee_particular_form_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	

		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');

	}


	public function index($staff=null,$candidate_id=null)
	{
		// die("dhfg");
		try{
		  $staff = $this->uri->segment(3);
   $candidate_id  = $this->uri->segment(4);

            $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);



      



      $staff_id=$this->loginData->staffid;
     $candidateid=$this->loginData->candidateid;
    
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        echo "staff id".$staff_id;
        $candidateid=$candidateid;
        echo "candate id=".$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff;
         $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;
        
      }

		
	
		
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){


			

			$Sendsavebtn = $this->input->post('savebtn');

			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
				//$data=$_FILES['signatureplace']['name'];
			//	die($data);


				if ($_FILES['signatureplace']['name'] != NULL) {
     				$data=$_FILES['signatureplace'];
    						 $this->load->model('Membership_applicatioin_from_model');

    				 $fileDatasa=$this->General_nomination_and_authorisation_form_model->do_uploadd($data);

     				$updateArr['signatureplace'] = $fileDatasa['orig_name'];
    					 $updateArr['signatureplace_encrypted_filename'] = $fileDatasa['file_name'];

  			 }


  			


			$dadate = $this->input->post('dadate');

		    $dadate = $this->model->changedatedbformate($dadate);
				
				
				$insertarraydata = array(

				'candidateid'  						=> $this->loginData->candidateid,
				
				'da_place'  						=> $this->input->post('daplace'),
				'nomination_authorisation_signed'   => $updateArr['signatureplace_encrypted_filename'],
				
				'da_date'  							=> $dadate,
				'status'  							=> 0,
				'createdon'  						=> date('Y-m-d H:i:s')
				
				
			);

			$this->db->insert('tbl_general_nomination', $insertarraydata);
			$insertid = $this->db->insert_id();
			$data1=$this->input->post('data');
				
				foreach($data1 as $value)
				{

					$arr=array('sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['name'],
						'nomination_id'=>$insertid,
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior']

						);
					
					$this->db->insert('tbl_nominee_details', $arr);
				}
				

			

			


			


			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	

		redirect('General_nomination_and_authorisation_form/edit/'.$insertid);		
			}

		}

			$submitdatasend = $this->input->post('submitbtn');

		if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {

			
			if ($_FILES['signatureplace']['name'] != NULL) {
     				$data=$_FILES['signatureplace'];
    						 $this->load->model('Membership_applicatioin_from_model');

    				 $fileDatasa=$this->General_nomination_and_authorisation_form_model->do_uploadd($data);

     				$updateArr['signatureplace'] = $fileDatasa['orig_name'];
    					 $updateArr['signatureplace_encrypted_filename'] = $fileDatasa['file_name'];

  			 }


  			//echo "image=".$updateArr['signatureplace_encrypted_filename'];
  			//die;


			$dadate = $this->input->post('dadate');

		    $dadate = $this->model->changedatedbformate($dadate);
				
				
				$insertarraydata = array(

				'candidateid'  						=> $this->loginData->candidateid,
				
				'da_place'  						=> $this->input->post('daplace'),
				'nomination_authorisation_signed'   => $updateArr['signatureplace_encrypted_filename'],
				
				'da_date'  							=> $dadate,
				'status'  							=> 0,
				'createdon'  						=> date('Y-m-d H:i:s')
				
				
			);

			$this->db->insert('tbl_general_nomination', $insertarraydata);
			$insertid = $this->db->insert_id();
			$data1=$this->input->post('data');
				
				foreach($data1 as $value)
				{

					$arr=array('sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['name'],
						'nomination_id'=>$insertid,
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior']

						);
					
					$this->db->insert('tbl_nominee_details', $arr);
				}
				

			

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
				$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	

				redirect('General_nomination_and_authorisation_form/view/'.$insertid);		
			}
	}

}
 $content['topbar'] = $this->Employee_particular_form_staff_model->do_uploadsssss($candidateid);
       //echo "hello";
       //print_r($content['topbar']);
       //die;
    
      //die(print_r($content['topbar']);

      $var=$content['topbar']->nomination_flag;

       

      // print_r( $content['workexperiencedetails']);

 

   // $this->db->insert('mstpgeducation', $insertArr);
      // echo "data=".$_FILES['signature']['name'];

if ($var==null)
  {
  	goto preview;
   // redirect('/General_nomination_and_authorisation_form/index/'.$staff.'/'.$candidateid);
  }
   

  if($var==0)
  {
    redirect('/General_nomination_and_authorisation_staff_form/edit/'.$staff.'/'.$candidateid);
  }
  elseif($var==1)
  {
    redirect('/General_nomination_and_authorisation_staff_form/view/'.$staff.'/'.$candidateid);
  }
     


  		preview:
		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($candidateid);

		$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
		
		$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($candidateid);
		$content['office_name'] = $this->model->office_name();
		


		$content['title'] = 'General_nomination_and_authorisation_staff_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function edit($staff=null,$candidate_id=null)
	{
		try{
		 $staff = $this->uri->segment(3);
     $candidate_id  = $this->uri->segment(4);

            $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);



      



      $staff_id=$this->loginData->staffid;
     $candidateid=$this->loginData->candidateid;
    
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        echo "staff id".$staff_id;
        $candidateid=$candidateid;
        echo "candate id=".$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff;
         $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;
        
      }

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST"){

			$savesenddata = $this->input->post('savebtn');

			if (!empty($savesenddata) && $savesenddata =='senddatasave') {


				if ($_FILES['signatureplace']['name'] != NULL) {
     				$data=$_FILES['signatureplace'];
    						 $this->load->model('Membership_applicatioin_from_model');

    				 $fileDatasa=$this->General_nomination_and_authorisation_form_model->do_uploadd($data);

     				$updateArr['signatureplace'] = $fileDatasa['orig_name'];
    					 $updateArr['signatureplace_encrypted_filename'] = $fileDatasa['file_name'];

  				
  				 }
  				 else
  				 {
  				 	$updateArr['signatureplace_encrypted_filename']=$this->input->post('candidate_sign');
  				 }


				$dadate = $this->input->post('dadate');

		       $dadate = $this->model->changedatedbformate($dadate);


			$updatearraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				
				'da_place'  => $this->input->post('daplace'),
				'nomination_authorisation_signed'  => $updateArr['signatureplace_encrypted_filename'],
				
				'da_date'  => $dadate,
				'status'  => 0,
				
				
			);
			$this->db->where('id', $token);
			$this->db->update('tbl_general_nomination', $updatearraydata);
			echo $this->db->last_query();
			//die;
			//$countnominee = count($this->input->post('full_name_nominee'));


		$data1=$this->input->post('data');
		//print_r($data1);
		//die;
				
				foreach($data1 as $value)
				{

					$arr=array(
						'id'=>$value['n_id'],
						'sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['full_name_nominee'],
						'nomination_id'=>$token,
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior']

						);
					//print_r($arr);
					//die;
					extract($arr);
					//die;
					$this->db->where('id', $id);
					$this->db->update('tbl_nominee_details', $arr);
					//echo $this->db->last_query();
					//die;
					
				}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}else{
				
		$this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form');	
		
			}
			redirect('General_nomination_and_authorisation_staff_form/edit/'.$token);		
			}

		
			$submitsenddata = $this->input->post('submitbtn');

		if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

			if ($_FILES['signatureplace']['name'] != NULL) {
     				$data=$_FILES['signatureplace'];
    						 $this->load->model('Membership_applicatioin_from_model');

    				 $fileDatasa=$this->General_nomination_and_authorisation_form_model->do_uploadd($data);

     				$updateArr['signatureplace'] = $fileDatasa['orig_name'];
    					 $updateArr['signatureplace_encrypted_filename'] = $fileDatasa['file_name'];

  				
  				 }
  				 else
  				 {
  				 	$updateArr['signatureplace_encrypted_filename']=$this->input->post('candidate_sign');
  				 }


				$dadate = $this->input->post('dadate');

		       $dadate = $this->model->changedatedbformate($dadate);


			$updatearraydata = array(

				'candidateid'  => $this->loginData->candidateid,
				
				'da_place'  => $this->input->post('daplace'),
				'nomination_authorisation_signed'  => $updateArr['signatureplace_encrypted_filename'],
				
				'da_date'  => $dadate,
				'status'  => 0,
				
				
			);
			$this->db->where('id', $token);
			$this->db->update('tbl_general_nomination', $updatearraydata);
			//echo $this->db->last_query();
			//die;
			//$countnominee = count($this->input->post('full_name_nominee'));


		$data1=$this->input->post('data');
		//print_r($data1);
		//die;
				
				foreach($data1 as $value)
				{

					$arr=array(
						'id'=>$value['n_id'],
						'sr_no'=>$value['sr_no'],
						'nominee_name'=>$value['full_name_nominee'],
						'nomination_id'=>$token,
						'nominee_relation'=>$value['relationship_nominee'],
						'nominee_age'=>$value['age_nominee'],
						'nominee_address'=>$value['address_nominee'],
						'share_nomine'=>$value['share_nominee'],
						'minior'=>$value['minior']

						);
					//print_r($arr);
					//die;
					extract($arr);
					//die;
					$this->db->where('id', $id);
					$this->db->update('tbl_nominee_details', $arr);
					//echo $this->db->last_query();
					//die;
					
				}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');	

			}

			/*else{

				// $hrdemailid    = 'poonamadlekha@pradan.net';
				 $hrdemailid    = 'amit.kum2008@gmail.com';
				 $tcemailid     = $this->loginData->EmailID;

				 $subject = "Submit Gereral Nomination And Authorisation Form ";
				  $body = 	'Dear Sir, <br><br> ';
				  $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
				 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 $to_email = $hrdemailid;
				 $to_name = $tcemailid;
				
				$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/
				
				$this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form');	

				redirect('General_nomination_and_authorisation_staff_form/view/');		
			


	}


	}
		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($candidateid);

		$content['genenominformdetail'] = $this->model->getGeneralnominationform($staff_id);
		//print_r($content['genenominformdetail']);

		$content['nominee'] =$this->model->getGeneralnominationform($candidateid);
	//	echo "<pre>";
		//print_r($content['nominee']);
		//die;

        $content['countnominee'] =$this->model->getCountNominee($staff_id);
        //die('hdghds');
		$content['nomineedetail'] = $this->model->getNomineedetail($candidateid);
		//echo "<pre>";
		//print_r($content['nomineedetail']);
		$content['office_name'] = $this->model->office_name();

		$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);
		//print_r($content['getgeneralform']);
		$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($candidateid);


		$content['title'] = 'General_nomination_and_authorisation_staff_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function view($staff=null,$candidate_id=null)
	{
		try{

		 $staff = $this->uri->segment(3);
   $candidate_id  = $this->uri->segment(4);

            $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);



      



      $staff_id=$this->loginData->staffid;
     $candidateid=$this->loginData->candidateid;
    
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        echo "staff id".$staff_id;
        $candidateid=$candidateid;
        echo "candate id=".$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff;
         $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;
        
      }

		$content['sysrelations'] = $this->model->getSysRelations();
		$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($candidateid);

		$content['genenominformdetail'] = $this->model->getViewGeneralnominationform($candidateid);
		// print_r($content['genenominformdetail']);

		$content['getGeneralnomination'] = $this->model->getViewGeneralnominationform($candidateid);
		// print_r($content['getGeneralnomination']);
		// echo "pooja";
				//die;
		
		$content['nomineedetail'] = $this->model->getNomineedetail($candidateid);
		$content['getjoiningreport']       = $this->model->getJoiningReport($candidateid);
		$content['getgeneralform'] = $this->model->getGeneralFormStatus($candidateid);


		$content['title'] = 'General_nomination_and_authorisation_staff_form';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



}