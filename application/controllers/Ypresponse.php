<?php 
class Ypresponse extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
  		$this->load->model("Global_model","gmodel");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		

    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

		
		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				// echo "<pre>";
				// print_r($this->input->post()); die();
			//	print_r($this->loginData);
			 
				

				$this->db->trans_start();

				$interview_time = $this->input->post('interview_time');
				$interview_date = $this->input->post('interview_date');
				// $latter = $this->input->post('latter');
				$summernote = $this->input->post('summernote');
				

					$countaccept = count($this->input->post('accept'));
					
					for ($i=0; $i < $countaccept ; $i++) {

						$candidateid = $this->input->post('accept')[$i];
						
						if ($candidateid !='') {
							$checkaccept = "yes";
						}else{
							$checkaccept = 'Null';
						}
						 $interview_date = $this->input->post('interview_date'); 
						$interview_date1 = date('Y-m-d', strtotime($interview_date));
						//echo $interview_date;



						$updateAcceptReject	 = array(

							'yprststus'      => $checkaccept,
							'interview_date' => $interview_date1,
							'yprdate'        => date('Y-m-d'),
							'yprcreatedby'	 => $this->loginData->UserID,
						);
						$this->db->where('candidateid',$candidateid);
						$this->db->update('tbl_candidate_registration', $updateAcceptReject);
					//echo $this->db->last_query(); die;
						$hremail='';
                       $hremail = $this->gmodel->getHRDEmailid();

						$candidatedetails = $this->model->getSelectedCandidate($candidateid);
						//print_r($this->loginData);
						//print_r($candidatedetails); die;
						 // $password = $this->getPassword();
						$firstname = $candidatedetails->candidatefirstname;
						$username = $candidatedetails->emailid;
						$Candidateid  = $candidatedetails->candidateid;
						$candidateemailid = $candidatedetails->emailid; 

						$subject      = 'Invitation to attend the selection process in Noida';
						$message      = "";
						$name         = $firstname;
						$username     = $username;
						$password     =  $firstname;
						$date         = $interview_date;
						$staffname    = $this->loginData->UserFirstName; ///change name
						$designation  = 'HRD';
						$candidatelink = site_url().'login/login1';
					 @ $day=date('l',$interview_date1);

					 $ypr = array('$name','$username','$password','$date','$staffname','$day','$candidatelink','$hremail');
					 $ypr_replace = array($candidatedetails->candidatefirstname,$candidatedetails->emailid,$candidatedetails->candidatefirstname,$date,$this->loginData->UserFirstName,$day,$candidatelink,$hremail->hrdemailid);
					 		// $day;					
						// $fd = fopen("mailtext/YPR.txt", "r");	
						// $message .=fread($fd,4096);
						// eval ("\$message = \"$message\";");
						// $message =nl2br($message);

					 $body= str_replace($ypr, $ypr_replace, $summernote);
					 // $body= str_replace('$effective_date',$new = 2, $summernote);

					 // echo html_entity_decode($body); 
						// echo $body;


				
						//$to_email = 'amit.kum2008@gmail.com';
				          $to_email = $candidateemailid;
					 		//$to_candidate  = $result1->emailid; ///// HRD Email Id ////
						$sendmail = $this->Common_Model->send_email($subject, $body, $to_email);

						if($sendmail==1){

							$this->session->set_flashdata('tr_msg', 'Successfully Email Sent');

							$updateSentmail	 = array(
								'yprsent'  => 'yes',
								'username' => $username,
								'password'  => md5($password),
								
							);
							$this->db->where('candidateid',$Candidateid);
							$this->db->update('tbl_candidate_registration', $updateSentmail);

							//echo $this->db->last_query(); die;
						}else{

							$this->session->set_flashdata('er_msg', 'Error !! Email Not Send !!!');
						}


					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error Candidate not save and sent YPR ');	
						redirect(current_url());	
					}else{
						

						 $this->session->set_flashdata('tr_msg', 'Email send successfully.');
						
					}
				
			}

			redirect('Ypresponse/index');

		}
		
		$content['acceptedcandidatedetails'] = $this->model->getSelectedAccptedCandidate();

		$content['getyprresponsetype'] = $this->model->getYPRResponseType();
			// echo "<pre>";
			// print_r($content['acceptedcandidatedetails']); die;

			$content['title'] = 'Ypresponse';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);



		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


public function lettercontentcheck($procssidget=NULL){
	try{
		if($procssidget){
			$processid = $procssidget;
		}else{				
			$processid = $this->input->post('processid');
		}
		$processid = $this->input->post('processid');
		$query = "SELECT * FROM tbl_letter_master WHERE processid = ".$processid." ";
		$content['lettercontent'] = $this->db->query($query)->row();
		//$response = '';
		$response = '';
		if($content['lettercontent']){
			$response = $content['lettercontent']->lettercontent;
		}else{
			$response = '';
		}
		if($procssidget){
			return $response;
		}else{
			echo $response;
		}

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	public function getPassword()
	{

		try{

    // Generating Password
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
			$password = substr( str_shuffle( $chars ), 0, 8 );

			return $password;


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


}