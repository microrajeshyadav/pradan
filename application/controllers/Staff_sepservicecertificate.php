<?php 
class Staff_sepservicecertificate extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepservicecertificate_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Staff_seperation_model");
		//$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			
			  if($token){
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();

			// end permission    
				$query ="SELECT * FROM staff_transaction WHERE id =".$token; 
				$content['staff_service_certificate'] = $this->db->query($query)->row();

				
				$staffid =  $content['staff_service_certificate']->staffid;  //die;


				$content['reporting_detail'] = $this->Common_Model->get_staffReportingto($content['staff_service_certificate']->staffid);


				$content['staff_certificate'] = $this->Staff_sepservicecertificate_model->get_staff_seperation_certificate($content['staff_service_certificate']->staffid);
				
				/*echo "<pre>";
				print_r($content['reporting_detail']); exit();*/

				$content['staffid'] = $content['staff_service_certificate']->staffid;

				$content['ed_detail'] = $this->gmodel->getExecutiveDirectorEmailid();
				// print_r($content['ed_detail']);
				

				$query = "SELECT * FROM tbl_separation_service_certificate WHERE transid = ".$token;
				$content['certificate_detail'] = $this->db->query($query)->row();

				$RequestMethod = $this->input->server("REQUEST_METHOD");
				$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
				$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
				/*echo "<pre>";
				print_r($personnel_detail);exit();*/
				if($RequestMethod == 'POST')
				{

					// echo "<pre>";
					// 	print_r($this->input->post());
					$db_flag = '';
					if($this->input->post('Save') == 'Save'){
						$db_flag = 0;
					}else if($this->input->post('saveandsubmit') == 'save & submit'){

						$db_flag = 1;
						$subject = "Your Process Approved";
			            $body = 'Dear,<br><br>';
			            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
			            $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your process has Approved  .<br><br>';
			            $body .= 'Thanks<br>';
			            $body .= 'Administrator<br>';
			            $body .= 'PRADAN<br><br>';

			            $body .= 'Disclaimer<br>';
			            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

			            
			          /*  $to_email = $personnel_detail->EmailID;
			            $to_name = $personnel_detail->UserFirstName;
			            //$to_cc = $getStaffReportingtodetails->emailid;
			            $recipients = array(
			               $content['staff_detail']->emailid => $content['staff_detail']->name
			               // ..
			            );
			            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
			            if (substr($email_result, 0, 5) == "ERROR") {
			              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
			            }*/
					}
					
					//$this->db->trans_start();

					$insertArray = array(

						'staffid'         =>  $content['staff_service_certificate']->staffid,
						'transid'         =>  $token,
						'separation_no'   =>  $this->input->post('separation_no'),
						'currentdate'     =>  $this->input->post('currentdate'),
						'basic_pay'       =>  $this->input->post('basic_pay'),
						'hra'             =>  $this->input->post('hra'),
						'flag'            =>  $db_flag,
						'createdby'       =>  $this->loginData->staffid,
					);

					if($content['certificate_detail'])
					{
						$updateArray = array(

							'staffid'         =>  $content['staff_service_certificate']->staffid,
							'transid'         =>  $token,
							'separation_no'   =>  $this->input->post('separation_no'),
							'currentdate'     =>  $this->input->post('currentdate'),
							'basic_pay'       =>  $this->input->post('basic_pay'),
							'hra'             =>  $this->input->post('hra'),
							'flag'            =>  $db_flag,
							'updatedby'       =>  $this->loginData->staffid,

						);

						$this->db->where('id', $content['certificate_detail']->id);
						$flag = $this->db->update('tbl_separation_service_certificate', $updateArray);
					}else{
						$flag = $this->db->insert('tbl_separation_service_certificate',$insertArray);
					}
					//echo $this->db->last_query(); die;

					if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
					// redirect(current_url());
				} 
				else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				// redirect(current_url());
			}
			
				// $this->db->trans_complete();

				// if ($this->db->trans_status() === FALSE){
				// 	$this->session->set_flashdata('er_msg', 'Error adding Service Certificate');	
			
				// }else{
				// 	$this->session->set_flashdata('tr_msg', 'Successfully added Service Certificate');
				// }
		}

		$query = "SELECT * FROM tbl_separation_service_certificate WHERE transid = ".$token;
		$content['certificate_detail'] = $this->db->query($query)->row();
		$content['token'] = $token;
		$content['title'] = 'Staff_sepservicecertificate';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	  }else{
	  	$this->session->set_flashdata('er_msg','Missing trans id!!');
    	header("location:javascript://history.go(-1)", 'refresh');
	  }
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}