<?php 

///// Handed over taken over Controller   



class Handed_over_taken_over_seperation extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Stafftransferpromotion_model","Stafftransferpromotion_model");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");
		$this->load->model("Staff_approval_model","Staff_approval_model");
		$this->load->model("Global_Model","gmodel");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token)
	{
		
		try{
	    // start permission 
			
			

			$content['token'] = $token;
			$content['staff_details']=$this->model->get_staffDetails($token);
			//echo "<pre>";
		//	print_r($content['staff_details']);
			
			
			$content['supervisior_details']=$this->model->supervisiorName($content['staff_details']->reportingto);
			// print_r($content['supervisior_details']);
			// die;

			$content['expense']=$this->model->count_handedchrges($token);
		/*echo "<pre>";
		print_r($content['staff_details']);exit();*/
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";

		$result  = $this->db->query($sql)->result()[0];
		$forwardworkflowid = $result->workflowid;
	    // echo $forwardworkflowid;exit;
		
		
		$content['getstafflist'] = $this->model->getStaffListByOfficeId($content['staff_details']->officeid);
		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		/*echo "<pre>";
		print_r($content['getstafflist']);exit();*/
		
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{
			// print_r($_POST);
			// die;

			if($this->loginData->staffid == $content['staff_details']->staffid){	    	
				$receiver = $this->input->post('change_responsibility_name');
				$staff_id = $this->loginData->staffid;
				$content['t_id']=$this->model->getTransid($staff_id);

				$id = $content['t_id']->id;
				$flag = 13;
			}else{
				$staff_id= $content['staff_details']->staffid;
				$content['t_id']=$this->model->getTransid($staff_id);

				$id = $content['t_id']->id;
				$flag = 15;
		    	 $receiver = $content['staff_details']->current_responsibility_to_id;
			}	

			
			$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			
			// if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {
				
			// 	$transferno=$this->input->post('seperationno');
				
				
			// 	$insertArraydata=array(

			// 		'type'         =>$this->input->post('handed'),
			// 		'staffid'      =>$staff_id,
			// 		'transfernno'  =>$transferno,
			// 		'trans_date'  =>$this->gmodel->changedatedbformate($this->input->post('tdate')),
			// 		'responsibility_date'  =>$this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
			// 		'flag'         =>0,
			// 	);

					
			// 	$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
			// 	$insertid = $this->db->insert_id();

			// 	$countitem = count($this->input->post('items'));
			// 	for ($i=0; $i < $countitem; $i++) { 

			// 		$arr=array('item'=>$this->input->post('items')[$i],
			// 			'handedtaken_id'=>$insertid,
			// 			'description'=>$this->input->post('items_description')[$i]

			// 		);

					
			// 		$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

			// 	}

				
			// 	$this->db->trans_complete();

			// 	if ($this->db->trans_status() === FALSE)
			// 	{

			// 		$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
			// 	}
			// 	else{
			// 		$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
			// 		redirect('Handed_over_taken_over_seperation/edit/'.$insertid);			
			// 	}

			// }



			
			 if($this->input->post('comments'))
			{
				// echo "<pre>";
				
				// print_r($this->input->post()); die;	
				$transferno=$this->input->post('seperationno');
				// $receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['staff_details']->reportingto);
				 // echo $receiverdetail;
				 // die;
				$insertArraydata=array(
					'type'         =>$this->input->post('handed'),
					'staffid'      =>$staff_id,
					'transid'  =>$transferno,
					'trans_date'  =>$this->gmodel->changedatedbformate($this->input->post('tdate')),
					'responsibility_date'  =>$this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'flag'         =>1,
					'process_type'=>3,
					'responsibility_name'=>$content['staff_details']->reportingto

				);

				
				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
				

				$insertid = $this->db->insert_id();
				//echo $this->db->last_query();  die;

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

					);
					
					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
					
						}
				
				// $updateArr = array(                         
				// 	'trans_flag'     => $flag,
				// 	'updatedon'      => date("Y-m-d H:i:s"),
				// 	'updatedby'      => $this->loginData->staffid
				// );

				// $this->db->where('id',$token);
				// $this->db->update('staff_transaction', $updateArr);

				
				// $insertworkflowArr = array(
				// 	'r_id'                 => $token,
				// 	'type'                 => 3,
				// 	'staffid'              => $content['staff_details']->staffid,
				// 	'sender'               => $this->loginData->staffid,
				// 	'receiver'             => $receiver,
				// 	'forwarded_workflowid' => $forwardworkflowid,
				// 	'senddate'             => date("Y-m-d H:i:s"),
				// 	'flag'                 => $flag,
				// 	'scomments'            => $this->input->post('comments'),
				// 	'createdon'            => date("Y-m-d H:i:s"),
				// 	'createdby'            => $this->loginData->staffid,
				// );
				// $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				// echo $this->db->last_query();
				// die;
				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					// $subject = "Your Process Approved";
					// $body = 'Dear,<br><br>';
					// $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
					// $body .= 'This is to certify that Mr. '.$content['staff_details']->name.'<br> your process has Approved  .<br><br>';
					// $body .= 'Thanks<br>';
					// $body .= 'Administrator<br>';
					// $body .= 'PRADAN<br><br>';

					// $body .= 'Disclaimer<br>';
					// $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

					
					// $to_email = $receiverdetail->emailid;
					// $to_name = $receiverdetail->name;
			  //     //$to_cc = $getStaffReportingtodetails->emailid;
					// $recipients = array(
					// 	$personnel_detail->EmailID => $personnel_detail->UserFirstName,
					// 	$content['staff_details']->emailid => $content['staff_details']->name
			  //        // ..
					// );
					// $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
					// if (substr($email_result, 0, 5) == "ERROR") {
					// 	$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					// }
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Handed_over_taken_over_seperation/view/'.$token.'/'.$insertid);			
				}
				
			}

		}


		$content['subview']="index";
		$content['title'] = 'Handed_over_taken_over_seperation';	
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	
}

public function taken($token)
{
	
	try{
	    // start permission 
		
		

		$content['token'] = $token;
		$content['staff_details']=$this->model->get_staffDetails($token);
		$content['taken_staff_details']=$this->model->get_takenstaffDetails($this->loginData->staffid);
		$content['handed_over_charge_date']=$this->model->handed_over_charge_date($content['staff_details']->staffid);
		/*echo "<pre>";
		print_r($content['handed_over_charge_date']); exit();*/

		$content['expense']=$this->model->count_handedchrges($token);

		$content['handed']=$this->model->handed_over_charge($token);

		// print_r($content['handed']);
		// die;

		$content['transfer_expeness_details']=$this->model->expeness_details($content['handed']->id);

		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$sql  = "SELECT workflowid,flag, receiver FROM tbl_workflowdetail WHERE r_id = $token ORDER BY workflowid DESC LIMIT 1";
		
		$result  = $this->db->query($sql)->row();
		$forwardworkflowid = $result->workflowid;
		// $content['financepersondetail'] = $this->Staff_approval_model->getExecutiveDirectorList(20);
		// echo "<pre>";
		// print_r($content['financepersondetail']);exit();
		$content['persondetail'] = $this->Staff_approval_model->getPersonalUserList();
		// echo "<pre>";
		// print_r($content['financepersondetail']);exit();
		
		
		$content['getstafflist'] = $this->model->getStaffListByOfficeId($content['staff_details']->officeid);
		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		/*echo "<pre>";
		print_r($content['getstafflist']);exit();*/
		
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{

			
			$staff_id= $content['staff_details']->staffid;
			$content['t_id']=$this->model->getTransid($staff_id);

			$id = $content['t_id']->id;
			$flag = 27;
			// $receiver = $content['staff_details']->current_responsibility_to_id;
			$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			
			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

				
				$transferno=$this->input->post('seperationno');
				
				$insertArraydata=array(

					'type'         =>$this->input->post('handed'),
					'staffid'      =>$staff_id,
					'transfernno'  =>$transferno,
					'trans_date'  =>$this->gmodel->changedatedbformate($this->input->post('tdate')),
					'responsibility_date'  =>$this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'flag'         =>0,
				);

				// print_r($insertArraydata);
				// die;

				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
				$insertid = $this->db->insert_id();

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 

					$arr=array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

					);

					
					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

				}

				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Handed_over_taken_over_seperation/edit/'.$insertid);			
				}

			}



			
			else if($this->input->post('comments'))
			{
				
				$transferno=$this->input->post('seperationno');
				$receiverdetail = $this->Staff_seperation_model->get_staffDetails($this->input->post('executivedirector_administration'));

				// $arrr = '';
				// $arrr= array();
				// foreach ($receiverdetail as $key => $value) {
				// 	array_push($arrr, $value->staffid);
				// }
				/*echo "<pre>";
				print_r($receiverdetail);exit();*/
				$insertArraydata=array(
					'type'         =>$this->input->post('handed'),
					'staffid'      =>$staff_id,
					'transfernno'  =>$transferno,
					'transid'=>$transferno,
					'trans_date'  =>$this->gmodel->changedatedbformate($this->input->post('tdate')),
					'responsibility_date'  =>$this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'flag'         =>1
				);
				// print_r($insertArraydata);
				// die;
				//die();
				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
				//$this->db->insert($insertArraydata);

				$insertid = $this->db->insert_id();
				//echo $this->db->last_query();  die;

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

					);
					
					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
					
				}
				
				$updateArr = array(                         
					'trans_flag'     => $flag,
					'updatedon'      => date("Y-m-d H:i:s"),
					'updatedby'      => $this->loginData->staffid
				);

				$this->db->where('id',$token);
				$this->db->update('staff_transaction', $updateArr);

				// foreach ($arrr as $key => $value) {
				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 3,
					'staffid'              => $content['staff_details']->staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $receiverdetail->staffid,
					'forwarded_workflowid' => $forwardworkflowid,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => $flag,
					'scomments'            => $this->input->post('comments'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
				// print_r($insertworkflowArr);
				// die;
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				// } 
				// end of foreach
				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$subject = "Your Process Approved";
					$body = 'Dear '.$content['staff_details']->name.',<br><br>';
					$body .= '<h2>Your Request has been Submited Successfully </h2><br>';
					$body .= 'This is to certify that Mr. '.$content['staff_details']->name.'<br> your process has Approved  .<br><br>';
					$body .= 'Thanks<br>';
					$body .= 'Administrator<br>';
					$body .= 'PRADAN<br><br>';

					$body .= 'Disclaimer<br>';
					$body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

					
					$to_email = $receiverdetail->emailid;
					$to_name = $receiverdetail->name;
			      //$to_cc = $getStaffReportingtodetails->emailid;
					//$recipients = array(
					// 	$personnel_detail->EmailID => $personnel_detail->UserFirstName,
					// 	$content['staff_details']->emailid => $content['staff_details']->name
			  //        // ..
					// );
					// $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
					if (substr($email_result, 0, 5) == "ERROR") {
						$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					}
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Handed_over_taken_over_seperation/viewtaken/'.$token.'/'.$insertid);
				}
				
			}

		}
/*
		echo "<pre>";
		print_r($receiverdetail);exit();*/
		$content['subview']="taken";
		$content['title'] = 'Handed_over_taken_over_seperation';	
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
	
}

public function Edit($token)
{
	try{
			// start permission 
		$staff_id=$this->loginData->staffid;
		$content['t_id']=$this->model->getTransid($staff_id);
		$id=$content['t_id']->id;
		$content['staff_details']=$this->model->get_staffDetails($id);	
		$content['expense']=$this->model->count_handedchrges($token);
		$content['transfer_expeness_details']=$this->model->expeness_details($token);
		$content['handed_expeness']=$this->model->handed_over_charge($token);

		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
			{	$submitdatasend = $this->input->post('submitbtn');

		$Sendsavebtn = $this->input->post('savetbtn');
			//print_r($_POST);
			//die();

		if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
			
			$transferno=$this->input->post('transferno');
			
			$insertArraydata=array(
				'type'         =>$this->input->post('handed'),
				'staffid'=>$staff_id,
				'transfernno'=>$transferno,
				'flag'=>0
			);
			
			$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
			$this->db->where('id',$token);
			
			$countitem = count($this->input->post('items'));
			
			$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);

			
			for ($i=0; $i < $countitem; $i++) { 
				
				$arr=array(
					
					'handedtaken_id' =>$token,
					'item' =>$this->input->post('items')[$i],
					'description'=>$this->input->post('items_description')[$i],
				);
				
				$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
			}

				//die();
			
			
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{

				$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
			}
			else{
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
				redirect('Handed_over_taken_over_seperation/edit/'.$token);			
			}

		}



		
		else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
		{
			
			$transferno=$this->input->post('transferno');
			
			$insertArraydata=array(
				'type'         =>$this->input->post('handed'),
				'staffid'      =>$staff_id,
				'transfernno'  =>$transferno,
				'flag'         =>1
			);
			
			$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
			$this->db->where('id',$token);
			
			$countitem = count($this->input->post('items'));
			
			$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);

			
			for ($i=0; $i < $countitem; $i++) { 
				
				$arr=array(
					
					'handedtaken_id' =>$token,
					'item' =>$this->input->post('items')[$i],
					'description'=>$this->input->post('items_description')[$i],
				);
				
				$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
			}

			
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{

				$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
			}
			else{
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
				redirect('Handed_over_taken_over_seperation/view/'.$token);			
			}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
		}

		


		
	}
	$content['title'] = 'Handed_over_taken_over_seperation/edit';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
	
}catch (Exception $e) {
	print_r($e->getMessage());die;
}
}
public function view($token,$inserted)
{
	try{
		
		$staffidquery = "SELECT * FROM tbl_hand_over_taken_over_charge WHERE id=".$inserted;
		$staffidres   = $this->db->query($staffidquery)->row();
		$staff_id     = $staffidres->staffid;
		$content['t_id']=$this->model->getTransid($staff_id);
		$id=$content['t_id']->id;
		$content['staff_details'] =$this->model->get_staffDetails($id);
		$sql  = "SELECT workflowid,flag, receiver FROM tbl_workflowdetail WHERE r_id = $token ORDER BY workflowid DESC LIMIT 1";
		$result  = $this->db->query($sql)->row();
		$forwardworkflowid = $result->workflowid;

		$content['receiverdetail'] = $this->Staff_seperation_model->get_staffDetails($result->receiver);
			/*echo "<pre>";
			print_r($receiverdetail); exit();*/
			
			$content['expense']=$this->model->count_handedchrges($inserted);
			
			$content['transfer_expeness_details']=$this->model->expeness_details($inserted);
			
			
			$content['handed_expeness']=$this->model->handed_over_charge($inserted);
	 		//print_r($content['handed_expeness']);
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission
			// $content['getstafflist'] = $this->model->getStaffList();
			$content['getstafflist'] = $this->model->getStaffListByOfficeId($content['staff_details']->officeid);
			$content['subview']="index";
			
			$content['title'] = 'Handed_over_taken_over_seperation/view';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
			
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	public function viewtaken($token,$inserted)
	{
		try{
			
			$staffidquery = "SELECT * FROM tbl_hand_over_taken_over_charge WHERE id=".$inserted;
			$staffidres   = $this->db->query($staffidquery)->row();
			$staff_id     = $staffidres->staffid;
			$content['t_id']=$this->model->getTransid($staff_id);
			$id=$content['t_id']->id;
			$content['staff_details'] =$this->model->get_staffDetails($id);
			$content['taken_staff_details']=$this->model->get_takenstaffDetails($this->loginData->staffid);
			$content['taken_over_charge_date']=$this->model->taken_over_charge_date($content['staff_details']->staffid);
			/*echo "<pre>";
			print_r($content['taken_over_charge_date']);exit();*/
			$sql  = "SELECT workflowid,flag, receiver FROM tbl_workflowdetail WHERE r_id = $token ORDER BY workflowid DESC LIMIT 1";
			$result  = $this->db->query($sql)->row();
			$forwardworkflowid = $result->workflowid;

			$content['receiverdetail'] = $this->Staff_seperation_model->get_staffDetails($result->receiver);
			
			$content['expense']=$this->model->count_handedchrges($inserted);
			
			$content['transfer_expeness_details']=$this->model->expeness_details($inserted);
			
			
			$content['handed_expeness']=$this->model->handed_over_charge($token);
	 		// echo "<pre>";
	 		// print_r($content['handed_expeness']);exit();
	 		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
	 		$content['role_permission'] = $this->db->query($query)->result();
			// end permission
			// $content['getstafflist'] = $this->model->getStaffList();
	 		$content['getstafflist'] = $this->model->getStaffListByOfficeId($content['staff_details']->officeid);
	 		$content['subview']="index";
	 		
	 		$content['title'] = 'Handed_over_taken_over_seperation/viewtaken';
	 		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	 		$this->load->view('_main_layout', $content);
	 		
	 	}catch (Exception $e) {
	 		print_r($e->getMessage());die;
	 	}
	 }

	}