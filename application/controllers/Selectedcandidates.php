<?php 
class Selectedcandidates extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Campus_model');
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		try{
	 // start permission 
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 	

		// print_r($this->input->post());
		if($this->input->post('campusid') != NULL && $this->input->post('campusid') !=''){
			// print_r($this->input->post()); die();
			$campusdata = $this->input->post('campusid');
			$campexp = explode('-', $campusdata);
			$campusid = $campexp[0];
			$campusintimationid = $campexp[1];
			$content['campusid'] = $campusdata;
			$content['selectedcandidatedetails']  = $this->model->getSelectedCandidate($campusid, $campusintimationid);
			$content['campusdetails']             = $this->model->getCampus($this->loginData->UserID);
		}else{
			$campusid ='NULL';
			$campusintimationid = 'NULL';
			$content['selectedcandidatedetails'] = array();
			$content['campusdetails']             = $this->model->getCampus($this->loginData->UserID);
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

			$send_toCampus =  $this->input->post('send_campus');

			if (!empty($send_toCampus) && $send_toCampus =='sendcamp') {

				$selectedcandidatedetails = $this->model->getSelectedCandidate($campusid, $campusintimationid);
				if (count($selectedcandidatedetails) > 0)
				{
					$html = '<table style="width:100%">
					<thead>
					<tr>';
                        // $html .='<th>S. No.</th>';
					$html .='                        
					<th>Candidate Name</th>
					<th>Gender</th>
					<th>Email Id</th>
					<th>Mobile </th>
					<th>Stream</th>
					</tr> 
					</thead>
					<tbody>'; 
					$i=0; foreach ($selectedcandidatedetails as $key => $value) {
						$html .= '<tr>';
                       // $html .='<td class="text-center">';
						$i=$i+1;  
                        // $html .=''.$i.'</td>';
						$html .='<td>'. $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname.'</td>
						<td>'. $value->gender.'</td>
						<td>'. $value->emailid.' </td>
						<td>'. $value->mobile.'</td>
						<td>'. $value->stream.'</td>
						</tr>';
						$i++; }
						$html .= '</tbody>
						</table>';

						$filename = md5(time() . rand(1,1000));
						$this->load->model('Dompdf_model');
						$generate =   $this->Dompdf_model->generatePDF($html, $filename, NULL,'FinalSelectedList.pdf');
						$pdffilename = $filename.'.pdf';
						$attachments = array($pdffilename);
						$message='';
						$subject = "Selection Report: PRADAN";
						$campus = $selectedcandidatedetails[0]->campusname; 						
						$campusemail = $this->model->getCampusEmail($campusid);
						
						$campus_incharge = $campusemail[0]->emailid;
						$campus_fromdate = $campusemail[0]->fromdate;
						$date = strtotime($campus_fromdate);
						$date = date('F,j,Y', $date);
						$campuss = array('$campus_fromdate','$campusincharge');
						$campus_replace = array($date,$campusemail[0]->campusincharge);						
						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 13 AND `isactive` = '1' AND id = 9";
						$data = $this->db->query($sql)->row();
						if(!empty($data))
						{
							$body = str_replace($campuss,$campus_replace , $data->lettercontent);

							$to_email     = $campus_incharge;

							$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name = null,$recipients=null, $attachments);
						}
					}
					$this->session->set_flashdata('tr_msg', 'Email send successfully with attachments.');
					redirect('Selectedcandidates');

				}
			}

			$content['title'] = 'Campus';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
     print_r($e->getMessage());die;
   }

		}

		public function SendMail($token){


			if (empty($token) || $token == '' ) {

				redirect('/Selectedcandidates/SendMail');
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');

			} else { 

				try{

					$getCandidateDetails = $this->model->getCandidateDetails($token);
					if (count($getCandidateDetails) > 0) {
						$candidateid = $getCandidateDetails[0]->candidateid;
						$candidateemail = $getCandidateDetails[0]->emailid;  			
						$candidatfirstname   = $getCandidateDetails[0]->candidatefirstname;  
						$addlink = site_url('login/login1');

						$html = 'Dear '.$candidatfirstname.', <br><br> 
						Congratulations you are selected in Pradan.  
						<br><br> Username - '.$candidateemail.'
						<br>Password - '.$candidatfirstname.'<br>
						Please fill Boidata form <a href='.$addlink.'>Click here</a>';


			$sendmail = $this->Common_Model->send_email($subject = 'Fill Biodata Form ', $message = $html, $candidateemail);  //// Send Mail candidates fill BFD Form ////

			if ($sendmail == false) {
				$this->session->set_flashdata('er_msg','Sorry !!! Email failed to HRD. Please Try Again !!!!'); 
			}else{

				$this->session->set_flashdata('tr_msg', 'Email id send Successfully .');     

				redirect('Selectedcandidates/index');
			}

		}

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}



public function preview($token)
{

	if (empty($token) || $token == '' ) {

		redirect('/Selectedcandidates/index');
		$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');

	} else {

		try{

		//echo $token;


			$fcount = $this->model->getCountFamilyMember($token);

			$content['familycount']= $fcount;  

			$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetailsPrint($token);

			$content['candidatedetails']         = $this->model->getCandidateDetailsPrint($token);

			$Icount = $this->model->getCountIdentityNumber($token);

			$content['identitycount']= $Icount;  

			$content['identitydetals'] = $this->model->getCandidateIdentityDetailsPrint($token);


			$TEcount = $this->model->getCountTrainingExposure($token);

			$content['TrainingExpcount']= $TEcount;  

			$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($token);


			$GPYcount = $this->model->getCountGapYear($token);

			$content['GapYearCount']= $GPYcount;  

			$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($token);



			@ $Lcount = $this->model->getCountLanguage($token);

			@ $content['languageproficiency']= $Lcount;  

			@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($token);
			$content['languagedetalsprint'] = $this->model->getCandidateLanguageDetailsPrint($token);
			@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($token);


			$WEcount = $this->model->getCountWorkExprience($token);

			$content['WorkExperience']= $WEcount;  

			$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($token);

			$content['title'] = 'Hrscore';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
}



public function Send_Email_Campusincharge_recruiters(){

	try{

		$campusid = $this->input->post('campusname');
		$selectedcandidatedetails = $this->model->getSelectedCandidate($campusid);
		// echo "<pre>";
		// print_r($selectedcandidatedetails); die;

		$html = "<table>"; 
		$html  .= "<tr>"; 
		$html  .= "<th>Name</th>"; 
		$html  .= "<th>Email Id </th>"; 
		$html  .= "</tr>";
		foreach ($selectedcandidatedetails as $key => $value) {
			$html  .= '<tr>';
			$html  .= '<td>'.$value->candidatefirstname.'</td>'; 
			$html  .= '<td>'.$value->emailid.'</td>'; 
			$html  .= '</tr>';
		}
		$html .='</table>';
        //  $filename = md5(time() . rand(1,1000));
		$this->load->model('Dompdf_model');
		$this->Dompdf_model->generatePDF($html, $filename, NULL,'Generateofferletter.pdf');

		$subject = "Candidate Selected By Pradan";
		$body = '<h4> Dear  Sir, <h4><br>';
		$body .= " ";
		
		$to_email = $edmail;
		$to_name = 'amit.kum2008@gmail.com';

		$this->Common_Model->send_email($subject, $body, $to_email, $to_nam);



	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete($token)
	{
		try{

		if (empty($token) || $token == '' ) {

			redirect('/Selectedcandidates/index');
			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');

		} else { 

			$this->view['detail'] = $this->model->getCampusDetails($token);
			if(count($this->view['detail']) < 1) {
				$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
				redirect($this->router->class);
			}


			if($this->model->delete($token) == '1'){
				$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
				redirect($this->router->class);
			}
			else {
				$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
				redirect($this->router->class);
			}
		}

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}
}