<?php 

/**
* State List
*/
class Stafftransfer_request extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Stafftransfer_request_model","Stafftransfer_request_model");
     $this->load->model('Global_model', 'gmodel');
   
    $this->load->model("Staffpromotion_model","Staffpromotion_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
      
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     //echo "hello";
    // die;
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 
 //print_r($content['role_permission']);

    $query = "select * from msleavecrperiod";
    $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
    //Stafftransfer_request_model


    $content['title'] = 'Staffpromotion';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add($token)
  {
    try{


        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
   $content['transfer_request']= $this->Stafftransfer_request_model->get_staffname();

   $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){
     // print_r($_POST);
      //die;

       $this->form_validation->set_rules('remarks','transfer','trim|required|min_length[1]|max_length[150]');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

          }

   
        
          $daterequest=$this->input->post('date_request');
          $daterequestdb=$this->gmodel->changedatedbformate($daterequest);

          $insertArr = array(
              'staffid'          => $this->loginData->staffid,
              'requestDate'      => $daterequestdb,
              'newofficeid'      => $this->input->post('newofficeid'),
              'status'           => 'Transfer Request',
              'supervisorid'     => '',
              'remarks'          => $this->input->post('remarks'),
              'createdon'        => date("Y-m-d H:i:s"),
              'createdby'        => $this->loginData->staffid,
            );

           $this->db->insert('stafftransferrequest', $insertArr);

         $insertworkflowArr = array(
            'r_id'           => 1,
            'type'           => 2,
            'sender'         => $this->loginData->staffid,
            'receiver'       => 5,
            'senddate'       => date("Y-m-d H:i:s"),
            'scomments'      => 'Submitted',
            'flag'           => 1,
            'createdon'      => date("Y-m-d H:i:s"),
            'createdby'      => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Transfer has not been done successfully'); 
            redirect(current_url());

          }else{
                      
             // $newofficename =  $this->model->getNewOfficeName();
             // print_r($newofficename); die;

                // Mail Send
              $subject = ":Transfer Mail";
              $body = "<h4>Hi ".$Staffdetails->name.", </h4><br />";
              $body .= "We have transfer new location.<br />";
              $body .= "<b>Office: " . $newofficename->officename . "</b><br />";
              $body .= "<b>Comment : " .  $comments . "</b><br /><br /><br />";
              $body .= "See you soon on Pradan <br><br>";
              $body .= "<b> Thanks </b><br>";
              $body .= "<b>Pradan Technical Team </b><br>";


              $to_useremail = $Staffdetails->emailid;;
              $to_username = NULL;

              $sendmail = $this->Common_Model->send_email($subject, $body, $to_useremail, $to_username);

          $this->session->set_flashdata('tr_msg', 'Transfer request has been sent successfully !!!');
          redirect('/staff_dashboard/index/');

          }      

     // $this->session->set_flashdata('tr_msg', 'Successfully added staff transfer request');
    //  redirect('/staff_dashboard/index/');
    }

    prepareview:


    $content['subview'] = 'Stafftransfer_request/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  
 

}