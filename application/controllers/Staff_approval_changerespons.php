<?php 

/**
* State List
*/
class Staff_approval_changerespons extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');

  }

  public function index()
  {
     $this->load->model("Staff_approval_changerespons_model");
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $content['getworkflowdetail'] = $this->Staff_approval_changerespons_model->getworkflowdetaillist();
   $content['getprobationworklist'] = $this->Staff_approval_changerespons_model->getprobationworkflowdetaillist();
    $content['gettransferpromationworkflowdetail'] = $this->Staff_approval_changerespons_model->get_transfer_promation_workflow_detail_list();

     $content['getpromationworkflowdetail'] = $this->Staff_approval_changerespons_model->get_promation_workflow_detail_list();

    
   //$content['subview']="index";
    $content['title'] = 'Staff_approval';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }


 public function list_joining_report($token)
  {
     $this->load->model("Staff_approval_changerespons_model");
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $content['getstaffjoiningreport'] = $this->Staff_approval_changerespons_model->getstaffjoiningreport($token);
   //$content['subview']="index";
    $content['title'] = 'Staff_approval/list_joining_report';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }


public function Add()
  {

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 


      $staffid = $this->uri->segment(3);
      $tarnsid  = $this->uri->segment(4);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   
    if($RequestMethod == 'POST'){

      // echo "<pre>";

      $getpersonnaluser ="";
      $getpersonnaluser = $this->Staff_approval_changerespons_model->getPersonalUserList();
      $arrr = array();
       foreach ($getpersonnaluser as $key => $value)
       {
        array_push($arrr, $value->personnelstaffid);
       }

        $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
        // $personnel_administration = $this->input->post('personnel_administration');

       $transferstaffdetails = $this->Staff_approval_changerespons_model->getTransferStaffDetails($transid);

     echo $NewOfficeId = $transferstaffdetails->new_office_id; 
      
       $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.

           
      if($status == 4){
        $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
      }
        $this->form_validation->set_rules('status','Seelct Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail 
      WHERE r_id = $transid";
         $result  = $this->db->query($sql)->result()[0];

         $forwardworkflowid = $result->workflowid;

        // print_r($result); die;


        $this->db->trans_start();
      
        if($status == 3){     //// Status => Approve 

          $updateArr = array(
                         
                'trans_flag'     => 3,
                'updatedon'      => date("Y-m-d H:i:s"),
                'updatedby'      => $this->loginData->staffid
            );

            $this->db->where('id',$transid);
            $this->db->update('staff_transaction', $updateArr);

          foreach ($arrr as $key => $value) {
            $insertworkflowArr = array(
             'r_id'                 => $transid,
             'type'                 => 4,
             'staffid'              => $staffid,
             'sender'               => $this->loginData->staffid,
             'receiver'             => $value,
             'forwarded_workflowid' => $forwardworkflowid,
             'senddate'             => date("Y-m-d H:i:s"),
             'flag'                 => 3,
             'scomments'            => $this->input->post('comments'),
             'createdon'            => date("Y-m-d H:i:s"),
             'createdby'            => $this->loginData->staffid,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
          }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_approval/'.$token);

          }

        }


        if ($status ==4) {

             $insertArr = array(
                       
              'trans_flag'     => 4,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
          );
// print_r($insertArr); die;
          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $insertArr);


        $insertworkflowArr = array(
             'r_id'           => $transid,
             'type'           => 4,
             'staffid'        => $staffid,
             'sender'         => $this->loginData->staffid,
             'receiver'       => $staffid,
             'senddate'       => date("Y-m-d H:i:s"),
             'flag'           => 4,
             'scomments'      => $this->input->post('comments'),
             'createdon'      => date("Y-m-d H:i:s"),
             'createdby'      => $this->loginData->staffid,
         );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Rejected by TC !!!');
          redirect('/Staff_approval/'.$token);

          }
        }

    
  }

    prepareview:

   $content['staffid'] = $staffid;
   $content['tarnsid'] = $tarnsid;

    $content['getpersonnaluser'] = $this->Staff_approval_changerespons_model->getPersonalUserList();

    $content['subview'] = 'Staff_approval_changerespons/add';
    $this->load->view('_main_layout', $content);
  }




  public function edit($token)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('shortname','Short name','trim|required|min_length[1]|max_length[10]');
        $this->form_validation->set_rules('categoryname','Category Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
          'shortname'      => $this->input->post('shortname'),
          'categoryname'   => $this->input->post('categoryname'),
          'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,
          'status'         => $this->input->post('status')
        );
      
      $this->db->where('id', $token);
      $this->db->update('mst_staff_category', $updateArr);
  $this->db->last_query(); 

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Staff Category');
       redirect('/Staff_approval/');
    }

    prepareview:

    $content['title'] = 'Staff_approval';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }



 public function add_clearance_certificate($token=NULL)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 


    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      // echo "<pre>";
      // print_r($this->input->post()); die;


      $btnsubmit = $this->input->post('btnsubmit');
      $btnsend = $this->input->post('btnsend');


       if (isset($btnsend) && $btnsend == "AcceptSaveData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           => $token,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 0,
        );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);
     $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $insertid,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
       }
       $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Staff_approval/edit_clearance_certificate/'.$insertid);

          }
      }

    

    if (isset($btnsubmit) && $btnsubmit == "AcceptSendData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           => $token,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 1,
        );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);
     $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $insertid,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
  
       }


        $insertArr = array(
          'trans_flag'              => 9,
        );
      
      $this->db->where('id',$token);
      $this->db->update('staff_transaction', $insertArr);

       $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Staff_approval/view_clearance_certificate'.$insertid);

          }
      }
     
  }

     $content['getstafflist'] = $this->Staff_approval_changerespons_model->getstaffdetailslist($token);
     $content['getsinglestaff'] = $this->Staff_approval_changerespons_model->get_staff_transfer_promation_detail($token);

    $content['title'] = '/add_clearance_certificate';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }
    

 public function edit_clearance_certificate($token)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

     $getstaffcclist = $this->Staff_approval_changerespons_model->getstaffclerancelist($token);
      $transid = $getstaffcclist->transid; 
  
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      // echo "<pre>";
      // print_r($this->input->post()); die;


      $btnsubmit = $this->input->post('btnsubmit');
      $btnsend = $this->input->post('btnsend');


       if (isset($btnsend) && $btnsend == "AcceptSaveData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           =>  $transid,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 0,
        );
      
      
      $this->db->insert('tbl_clearance_certificate', $insertArr);

      $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       $this->db->delete('tbl_clearance_certificate_transaction',array('clearance_certificate_id'=> $token));

       for ($i=0; $i < $name_of_location; $i++) { 
         $insertchiArr = array(
          'clearance_certificate_id'      => $token,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
   //  
       }
       $this->db->trans_complete();


          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Staff_approval/edit_clearance_certificate/'.$insertid);

          }
      }

    

    if (isset($btnsubmit) && $btnsubmit == "AcceptSendData") {
       
        $this->db->trans_start();

      $insertArr = array(
          'certify_that'      => $this->input->post('certify_that'),
          'transid'           =>  $transid,
          'createdon'         => date('Y-m-d H:i:s'),
          'createdby'         => $this->loginData->staffid,
          'flag'              => 1,
        );
      
      $this->db->where('id',$token);
      $this->db->update('tbl_clearance_certificate', $insertArr);
     $insertid = $this->db->insert_id();
      
       $name_of_location =  count($this->input->post('name_of_location'));

       $this->db->delete('tbl_clearance_certificate_transaction',array('clearance_certificate_id'=> $token));


       for ($i=0; $i < $name_of_location; $i++) { 

         $insertchiArr = array(
          'clearance_certificate_id'      => $token,
          'location'                      => $this->input->post('name_of_location')[$i],
          'description'                   => $this->input->post('description')[$i],
          'item_values'                   => $this->input->post('value')[$i], 
          
        );
      
      $this->db->insert('tbl_clearance_certificate_transaction', $insertchiArr);
       }

        $insertArr = array(
          'trans_flag'              => 9,
        );
      
      $this->db->where('id',$transid);
      $this->db->update('staff_transaction', $insertArr);


       $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! clearance certificate has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'clearance certificate has been done successfully !!!');
          redirect('/Staff_approval/view_clearance_certificate/'.$token);

          }
      }

  
     
    }




    $content['getstaffcertify_that'] = $getstaffcclist;
      $content['getcountstaffitemslist'] = $this->Staff_approval_changerespons_model->getcountstaffitmsclerancelist($token);
       $content['getstaffitemslist'] = $this->Staff_approval_changerespons_model->getstaffitmsclerancelist($token);
      
     $content['getstafflist'] = $this->Staff_approval_changerespons_model->getSinglestaffdetailslist($transid);

    $content['title'] = '/edit_clearance_certificate';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }
    



 public function view_clearance_certificate($token)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
 

     $getstaffcclist = $this->Staff_approval_changerespons_model->getstaffclerancelist($token);
      $transid = $getstaffcclist->transid;
       $content['getstaffcertify_that'] = $getstaffcclist;

      $content['getstafflist'] = $this->Staff_approval_changerespons_model->getSinglestaffdetailslist($transid);

     
      $content['getcountstaffitemslist'] = $this->Staff_approval_changerespons_model->getcountstaffitmsclerancelist($token);
       $content['getstaffitemslist'] = $this->Staff_approval_changerespons_model->getstaffitmsclerancelist($token);
      
    


    $content['title'] = '/view_clearance_certificate';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  
}



public function add_joining_report($token)
  {

    $this->load->model('Staff_approval_changerespons_model');

     $getStaffjoningplace = $this->Staff_approval_changerespons_model->getStaffjoningplace($token);
   // print_r($getStaffjoningplace); die;

    $transid = $getStaffjoningplace->transid;



    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      // echo "sdfdsf";
      // print_r($this->input->post()); die;
      
      $savebtn = $this->input->post('savebtn');
      $submitbtn = $this->input->post('submitbtn');

      $reporteddutyon = $this->input->post('reporteddutyon');

      $reported_duty_on = $this->gmodel->changedatedbformate($reporteddutyon);

      if (isset($savebtn) && $savebtn =='senddatasave') {

      $this->db->trans_start();

      $insertArr = array(
           'reported_for_duty_date'   => $reported_duty_on,
             'assigned'                 =>  $this->input->post('assignedto'),
             'location'                 => $this->input->post('location'),
             'supervision_staffid'      => $this->loginData->staffid,
             'updatedon'                => date('Y-m-d H:i:s'),
             'updatedby'                => $this->loginData->staffid,
      );
          
       $this->db->where('id', $token);
       $this->db->update('tbl_joining_report_new_place', $insertArr);

           $insertArr = array(
             'trans_flag'  => 11,
          );
          $this->db->where('id', $transid);
          $this->db->update('staff_transaction', $insertArr);

          $insertid = $this->db->insert_id();

          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Staff_approval/view_joining_report/'.$token);

          }
    }



  

  }

    $content['getstaffdetailslist'] = $this->Staff_approval_changerespons_model->getstaffdetailslist1($token);

    $content['token'] = $token;

     $content['supervisionname'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.''.$this->loginData->UserLastName;
      
    $content['title'] = 'Staff_approval/add_joining_report';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

    $this->load->view('_main_layout', $content);

  }



public function edit_joining_report($token)
  {

    $this->load->model('Staff_approval_changerespons_model');

  

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      // echo "sdfdsf";
      // print_r($this->input->post()); die;
      
      $savebtn = $this->input->post('savebtn');
      $submitbtn = $this->input->post('submitbtn');
    

      $staffdutytodaydate = $this->input->post('staff_duty_today_date');

      $staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);

      if (isset($savebtn) && $savebtn =='senddatasave') {

      $this->db->trans_start();

      $insertArr = array(
            'staff_duty_today_date'      => $staff_duty_today_date,
            'transid'           =>  $token,
             'flag'               => 0,
            'createdon'         => date('Y-m-d H:i:s'),
            'createdby'         => $this->loginData->staffid,
          );

          $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);
         // $insertid = $this->db->insert_id();

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/edit_joining_report/'.$token);

          }
    }



    if (isset($submitbtn) && $submitbtn =='senddatasubmit') {


      $this->db->trans_start();

      $insertArr = array(
            'staff_duty_today_date'      => $staff_duty_today_date,
            'transid'           =>  $token,
            'flag'               => 1,
            'createdon'         => date('Y-m-d H:i:s'),
            'createdby'         => $this->loginData->staffid,
          );
      
         $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);

          $insertArr = array(
             'trans_flag'      => 10,
          );
        
          $this->db->where('id', $token);
          $this->db->update('staff_transaction', $insertArr);


           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/view_joining_report/'.$token);

          }
    }

  }

   
     $content['getstaffjopin'] = $getStaffjoningplace;

    $content['getstaffdetailslist'] = $this->Staff_approval_changerespons_model->getstaffdetailslist1($token);

    $content['token'] = $token;
      
    $content['title'] = 'Staff_approval/edit_joining_report';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

    $this->load->view('_main_layout', $content);
  }




  public function view_joining_report($token)
  {

    $this->load->model('Staff_approval_changerespons_model');

      $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      

    }

   $getStaffjoningplace = $this->Staff_approval_changerespons_model->getStaffjoningplace($token);
   // print_r($getStaffjoningplace); die;

    $transid = $getStaffjoningplace->transid;

     $content['getstaffjopin'] = $getStaffjoningplace;

    $content['getstaffdetailslist']  = $this->Staff_approval_changerespons_model->getstaffdetailslist1($transid);
    $content['supervisionname'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.''.$this->loginData->UserLastName;
      

      
    $content['title'] = 'Staff_approval/view_joining_report';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

    $this->load->view('_main_layout', $content);
  }


}