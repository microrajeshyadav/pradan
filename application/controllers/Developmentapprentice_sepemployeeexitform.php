<?php 
class Developmentapprentice_sepemployeeexitform extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepemployeeexitform_model');
		$this->load->model('Developmentapprentice_sepemployeeexitform_model','model');
		$this->load->model("Global_model","gmodel");
    	$this->load->model("Common_model","Common_Model");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
		/*echo "<pre>";
		print_r($this->loginData);exit();*/
	
	}

	public function index($id)
	{
		// start permission
		if($id){
		$content['token'] = $id; 
		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			$query ="SELECT * FROM staff_transaction WHERE id=".$id;
			$content['staff_transaction'] = $this->db->query($query)->row();
		    $content['staff_detail'] = $this->model->get_staff_sep_detail($id);
			$content['join_relive_data'] = $this->model->get_staff_seperation_certificate($content['staff_transaction']->id);
			$content['finance_detail'] = $this->Common_Model->get_staff_finance_detail($content['staff_transaction']->new_office_id,20);
			/*echo "<pre>";
			print_r($content['join_relive_data']);exit();*/
			// $content['hrlist'] = $this->Common_Model->get_hr_Staff_List(); 
			
			$query ="SELECT * FROM tbl_da_exit_interview_form WHERE transid=".$id;
			$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();
			$receiverdetail = $this->model->get_staffDetails($content['staff_transaction']->reportingto);
			$content['tcdetail'] = $receiverdetail;
		    /*echo "<pre>";
		  	print_r($content['tcdetail']);die();*/

        
        	$RequestMethod = $this->input->server('REQUEST_METHOD');
        	if($RequestMethod == "POST"){
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					// echo "save"; die();
					$db_flag = 0;
					$data = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$id,
        			'contents_confidential'=>$this->input->post('content_confedential'),
        			'an_experience_1'=>$this->input->post('question1'),
        			'any_learning_2'=>$this->input->post('question2'),
        			'specifically_3'=>$this->input->post('question3'),
        			'two_characteristics_4'=>$this->input->post('question4'),
        			'any_experience_5'=>$this->input->post('question5'),
        			'any_experience_6'=>$this->input->post('question6'),
        			'any_episode_7'=>$this->input->post('question7'),
        			'taking_an_overview_8'=>$this->input->post('question8'),
        			'proposed_assignment_9'=>$this->input->post('question9'),
        			'such_things_be_that_you_may_like_to_focus_on_10'=>$this->input->post('question10'),
        			'you_leave_this_organization_11'=>$this->input->post('question11'),
        			'currentdate'=>date('Y-m-d'),
        			'createdby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
        		if($content['staff_sepemployeeexitform']){
        			$data_update = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$id,
        			'contents_confidential'=>$this->input->post('content_confedential'),
        			'an_experience_1'=>$this->input->post('question1'),
        			'any_learning_2'=>$this->input->post('question2'),
        			'specifically_3'=>$this->input->post('question3'),
        			'two_characteristics_4'=>$this->input->post('question4'),
        			'any_experience_5'=>$this->input->post('question5'),
        			'any_experience_6'=>$this->input->post('question6'),
        			'any_episode_7'=>$this->input->post('question7'),
        			'taking_an_overview_8'=>$this->input->post('question8'),
        			'proposed_assignment_9'=>$this->input->post('question9'),
        			'such_things_be_that_you_may_like_to_focus_on_10'=>$this->input->post('question10'),
        			'you_leave_this_organization_11'=>$this->input->post('question11'),
        			'updatedon'=>date('Y-m-d H:i:s'),
        			'updatedby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
					$this->db->where('id', $content['staff_sepemployeeexitform']->id);
					$flag = $this->db->update('tbl_da_exit_interview_form', $data_update);
				}else{
					$flag = $this->db->insert('tbl_da_exit_interview_form',$data);
			 	}
				}else if($this->input->post('saveandsubmit') == 'Save And Submit'){
					$db_flag = 1;
					// echo "Save And Submit"; die();
					$comments = 'Exit interview form filled by DA';
					$updateArr = array(
			            'trans_flag'     => 3,
			            'updatedon'      => date("Y-m-d H:i:s"),
			            'updatedby'      => $this->loginData->staffid
			          );

					$this->db->where('id',$id);
					$this->db->update('staff_transaction', $updateArr);
					$insertworkflowArr = array(
			           'r_id'           => $id,
			           'type'           => 27,
			           'staffid'        => $this->loginData->staffid,
			           'sender'         => $this->loginData->staffid,
			           'receiver'       => $content['staff_transaction']->reportingto,
			           'senddate'       => date("Y-m-d H:i:s"),
			           'flag'           => 3,
			           'scomments'      => $comments,
			           'createdon'      => date("Y-m-d H:i:s"),
			           'createdby'      => $this->loginData->staffid,
			         );
			         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			         $data = array(
	        			'staffid'=>$content['staff_transaction']->staffid,
	        			'transid'=>$id,
	        			'contents_confidential'=>$this->input->post('content_confedential'),
	        			'an_experience_1'=>$this->input->post('question1'),
	        			'any_learning_2'=>$this->input->post('question2'),
	        			'specifically_3'=>$this->input->post('question3'),
	        			'two_characteristics_4'=>$this->input->post('question4'),
	        			'any_experience_5'=>$this->input->post('question5'),
	        			'any_experience_6'=>$this->input->post('question6'),
	        			'any_episode_7'=>$this->input->post('question7'),
	        			'taking_an_overview_8'=>$this->input->post('question8'),
	        			'proposed_assignment_9'=>$this->input->post('question9'),
	        			'such_things_be_that_you_may_like_to_focus_on_10'=>$this->input->post('question10'),
	        			'you_leave_this_organization_11'=>$this->input->post('question11'),
	        			'currentdate'=>date('Y-m-d'),
	        			'createdby'=>$this->loginData->staffid,
	        			'flag'=>$db_flag
	        		);
	        		if($content['staff_sepemployeeexitform']){
	        			$data_update = array(
	        			'staffid'=>$content['staff_transaction']->staffid,
	        			'transid'=>$id,
	        			'contents_confidential'=>$this->input->post('content_confedential'),
	        			'an_experience_1'=>$this->input->post('question1'),
	        			'any_learning_2'=>$this->input->post('question2'),
	        			'specifically_3'=>$this->input->post('question3'),
	        			'two_characteristics_4'=>$this->input->post('question4'),
	        			'any_experience_5'=>$this->input->post('question5'),
	        			'any_experience_6'=>$this->input->post('question6'),
	        			'any_episode_7'=>$this->input->post('question7'),
	        			'taking_an_overview_8'=>$this->input->post('question8'),
	        			'proposed_assignment_9'=>$this->input->post('question9'),
	        			'such_things_be_that_you_may_like_to_focus_on_10'=>$this->input->post('question10'),
	        			'you_leave_this_organization_11'=>$this->input->post('question11'),
	        			'updatedon'=>date('Y-m-d H:i:s'),
	        			'updatedby'=>$this->loginData->staffid,
	        			'flag'=>$db_flag
	        		);
					$this->db->where('id', $content['staff_sepemployeeexitform']->id);
					$flag = $this->db->update('tbl_da_exit_interview_form', $data_update);
					}else{
						$flag = $this->db->insert('tbl_da_exit_interview_form',$data);
				 	}
				 	$subject = "Exit Interview Form Filled BY DA";
					$body = "<h4>Dear, ".$content['tcdetail']->name.", </h4><br />";
					$body .= "Successfully filled Exit Interview Form.<br>";
					$body .= "<b> Thanks </b><br>";
					$body .= "<b>Pradan Technical Team </b><br>";

					// $to_email = $content['staff_detail']->emailid;
					$to_email = $receiverdetail->emailid; // tc 
					// $to_name = $content['staff_detail']->name;
					//$to_cc = $getStaffReportingtodetails->emailid;
					// $receiverdetail->emailid => $receiverdetail->name;

					// da 
					// $recipients = array(
					// $content['staff_detail']->emailid => $content['staff_detail']->name
					// // ..
					// );
					$email_result = $this->Common_Model->send_email($subject, $body, $to_email);
					if (substr($email_result, 0, 5) == "ERROR") {
					$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					}
				}else if($this->input->post('tcfeedback') == 'Submit feedback'){
					// die();
					/*echo "<pre>";
					print_r($content['staff_sepemployeeexitform']);
					print_r($_POST);exit;*/
					$db_flag = 1;
					$comments = 'Feedback sent by Superviser';
					$updateArr = array(
			            'trans_flag'     => 4,
			            'updatedon'      => date("Y-m-d H:i:s"),
			            'updatedby'      => $this->loginData->staffid
			          );

					$this->db->where('id',$id);
					$this->db->update('staff_transaction', $updateArr);
					$insertworkflowArr = array(
			           'r_id'           => $id,
			           'type'           => 27,
			           'staffid'        => $content['staff_transaction']->staffid,
			           'sender'         => $this->loginData->staffid,
			           'receiver'       => $content['finance_detail']->staffid,
			           'senddate'       => date("Y-m-d H:i:s"),
			           'flag'           => 4,
			           'scomments'      => $comments,
			           'createdon'      => date("Y-m-d H:i:s"),
			           'createdby'      => $this->loginData->staffid,
			         );
			         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			         $observer_date = $this->gmodel->changedatedbformate($this->input->post('observer_date'));
					$data_update = array(
						'observer_sign'=> $this->input->post('observer_sign'),
						'observer_date'=> $observer_date,
						'observer_observation'=> $this->input->post('observer_observation'),
						'observer_recommendation'=> $this->input->post('observer_recommendation'),
						'updatedby'=>$this->loginData->staffid,
						'updatedon'=>date("Y-m-d H:i:s")
					);
					$this->db->where('id', $content['staff_sepemployeeexitform']->id);
					$flag = $this->db->update('tbl_da_exit_interview_form', $data_update);
					$subject = "Check sheet and Clearance form of Apprentices";
					$body = "<h4>Dear ".$content['finance_detail']->name.", </h4><br />";
					$body .= "Please fill the Checksheet and Clearance form for ".$content['staff_detail']->name.".<br>";
					$body .= "<b> Thanks </b><br>";
					$body .= "<b>".$this->loginData->UserFirstName.' '.$this->loginData->UserLastName."</b><br>";
					$receiverdetail = $content['finance_detail'];
					// $to_email = $content['staff_detail']->emailid;
					$to_email = $receiverdetail->emailid; //fa
					// $to_name = $content['staff_detail']->name;
					//$to_cc = $getStaffReportingtodetails->emailid;
					// $receiverdetail->emailid => $receiverdetail->name

					
					// $recipients = array(
					// $content['staff_detail']->emailid => $content['staff_detail']->name
					// // ..
					// );
					$email_result = $this->Common_Model->send_email($subject, $body, $to_email);
					if (substr($email_result, 0, 5) == "ERROR") {
					$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					}

				}

        		
				
         		if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');} 
               else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');}
        	}
			$query ="SELECT * FROM tbl_da_exit_interview_form inner join tbl_hr_intemation on tbl_da_exit_interview_form.staffid=tbl_hr_intemation.staffid WHERE transid=".$id;
			$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();
			
			$content['title'] = 'Staff_sepemployeeexitform';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		}else{
			redirect('staff_dashboard');
		}
	}
	public function get_staff_clearance_detail($token)
   {
      try
	  {

	     $sql="SELECT c.name,a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code

	       FROM staff_transaction as a
	       INNER JOIN `staff` as c on a.staffid = c.staffid
	       INNER JOIN `msdesignation` as b on a.new_designation = b.desid

	     WHERE a.id = $token";


	     //$query=$this->db->get();
	     return  $this->db->query($sql)->row();

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   public function get_staff_seperation_certificate($staffid)
   {
      try
	  {
	      $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation,g.desname as leavingdesignation,k.reason as leavingreason FROM staff_transaction as a
	         INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid
	         INNER JOIN `staff` as c on a.staffid = c.staffid
	         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
	         INNER JOIN `msdesignation` as g on k.new_designation = g.desid
	         WHERE  K.`trans_status` = 'Resign' and  a.trans_status = 'JOIN' and a.staffid =".$staffid; 
	     return  $this->db->query($sql)->row();

		}catch (Exception $e) {
	      print_r($e->getMessage());die;
	    }

  }

	
}