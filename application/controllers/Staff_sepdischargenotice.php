<?php 
class Staff_sepdischargenotice extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepdischargenotice_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("General_nomination_and_authorisation_form_model");
		$this->load->model("Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			  if($token){
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
			// end permission    
				
				// echo $token; 
				$query ="SELECT * FROM staff_transaction WHERE id=".$token;
				$content['staff_discharge_notice'] = $this->db->query($query)->row();

				$staffid =  $content['staff_discharge_notice']->staffid;

				$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
				$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
				$content['personnel_detail']=$personnel_detail;
				
	        	//$receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['staff_discharge_notice']->reportingto);
	        	/*echo "<pre>";
				print_r($personnel_detail); die;*/

				$content['staffid'] = $content['staff_discharge_notice']->staffid;

				

				$query = "SELECT count(id) as decount FROM tbl_sepdischargenotice WHERE transid = ".$token;
				$result = $this->db->query($query)->row();
				$content['discharge_detail'] = '';  
				$content['distransaction_detail'] = '';  
				// echo $result->decount; 
				if($result->decount > 0)
				{
					$query = "SELECT * FROM tbl_sepdischargenotice WHERE transid = ".$token;
					$content['discharge_detail'] = $this->db->query($query)->row();

					$query = "SELECT * FROM tbl_discharge_transaction WHERE discharge_notice_id = ".$content['discharge_detail']->id;
					$content['distransaction_detail'] = $this->db->query($query)->row();
					
				}


				

	        //  echo $discharge_count = $result->num_rows();
				


				// print_r($content['transaction_detail']); die;

				$RequestMethod = $this->input->server("REQUEST_METHOD");	

				if($RequestMethod == 'POST')
				{

					// print_r($this->input->post()); die;
					$db_flag = '';
					if($this->input->post('Save') == 'Save'){
						$db_flag = 0;
					}else if($this->input->post('saveandsubmit') == 'Save & Submit'){

						$db_flag = 1;
						$subject = "Your Process Approved";
			            $body = 'Dear,<br><br>';
			            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
			            $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your process has Approved  .<br><br>';
			            $body .= 'Thanks<br>';
			            $body .= 'Administrator<br>';
			            $body .= 'PRADAN<br><br>';

			            $body .= 'Disclaimer<br>';
			            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

			            foreach ($personnel_detail as $key => $value) {
			            	$to_email = $value->EmailID;
			            	$to_name = $value->UserFirstName;

			            	//$to_cc = $getStaffReportingtodetails->emailid;
			            	$recipients = array(
			               $content['staff_detail']->emailid => $content['staff_detail']->name
			               // ..
			            	);
			            	$email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
			            }
			            
			            
			            if (substr($email_result, 0, 5) == "ERROR") {
			              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
			            }
					}
					$insertArray = array(

						'staffid'          => $content['staff_discharge_notice']->staffid,
						'transid'          => $token,
						'sep_discharge_no' => $this->input->post('sep_discharge_no'),
						'terminate_date'   =>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('terminate_date')),
						'relieved_date'    => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('relieved_date')),
						'cheque_rs'        => $this->input->post('cheque_rs'),
						'bank_name'        => $this->input->post('bank_name'),
						'sum_rs'           => $this->input->post('sum_rs'),
						'flag'             => $db_flag,
						'createdby'        => $this->loginData->staffid,
					);
					// print_r($insertArray); die;

					if($content['discharge_detail'] )
					{

					//echo $db_flag;exit();
						$updateArray = array(

							'staffid'          => $content['staff_discharge_notice']->staffid,
							'transid'          => $token,
							'sep_discharge_no' => $this->input->post('sep_discharge_no'),
							'terminate_date'   =>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('terminate_date')),
							'relieved_date'    => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('relieved_date')),
							'cheque_rs'        => $this->input->post('cheque_rs'),
							'bank_name'        => $this->input->post('bank_name'),
							'sum_rs'           => $this->input->post('sum_rs'),
							'flag'             => $db_flag,
							'updatedby'        => $this->loginData->staffid,	
						);

						$this->db->where('id', $content['discharge_detail']->id);
						$flag = $this->db->update('tbl_sepdischargenotice', $updateArray);

						$updateArray = array(
								'txtdue1'    => $this->input->post('txtdue1'),
								'dueamount1' => $this->input->post('dueamount1'),
								'txtdue2'    => $this->input->post('txtdue2'),
								'dueamount2' => $this->input->post('dueamount2'),
								'txtdue3'    => $this->input->post('txtdue3'),
								'dueamount3' => $this->input->post('dueamount3'),
								'txtdue4'    => $this->input->post('txtdue4'),
								'dueamount4' => $this->input->post('dueamount4'),
								'txtdue5'    => $this->input->post('txtdue5'),
								'dueamount5' => $this->input->post('dueamount5'),
								'flag'       => $db_flag,	
							);
						$this->db->where('id', $content['distransaction_detail']->id);
						$flag = $this->db->update('tbl_discharge_transaction', $updateArray);

					}else{
						$flag = $this->db->insert('tbl_sepdischargenotice',$insertArray);
						// echo $this->db->last_query();
						// die;
						$insertid = $this->db->insert_id();
						$insertArray = array(

							'discharge_notice_id'         => $insertid,
							'txtdue1'    => $this->input->post('txtdue1'),
							'dueamount1' => $this->input->post('dueamount1'),
							'txtdue2'    => $this->input->post('txtdue2'),
							'dueamount2' => $this->input->post('dueamount2'),
							'txtdue3'    => $this->input->post('txtdue3'),
							'dueamount3' => $this->input->post('dueamount3'),
							'txtdue4'    => $this->input->post('txtdue4'),
							'dueamount4' => $this->input->post('dueamount4'),
							'txtdue5'    => $this->input->post('txtdue5'),
							'dueamount5' => $this->input->post('dueamount5'),
							'flag'       => $db_flag,
						);
						$flag = $this->db->insert('tbl_discharge_transaction',$insertArray);

					}

					if($flag) 
					{
						$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
						// redirect(current_url());
					} 
					else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
					// redirect(current_url());
				}

			}
			$query = "SELECT count(id) as decount FROM tbl_sepdischargenotice WHERE transid = ".$token;
				$result = $this->db->query($query)->row();  
				// echo $result->decount; 
				if($result->decount > 0)
				{
					$query = "SELECT * FROM tbl_sepdischargenotice WHERE transid = ".$token;
					$content['discharge_detail'] = $this->db->query($query)->row();

					$query = "SELECT * FROM tbl_discharge_transaction WHERE discharge_notice_id = ".$content['discharge_detail']->id;
					$content['distransaction_detail'] = $this->db->query($query)->row();
					
				}
			$content['token'] = $token;
			$content['title'] = 'Staff_sepdischargenotice';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}else{
			$this->session->set_flashdata('er_msg','Missing trans id!!');
    		header("location:javascript://history.go(-1)", 'refresh');
		}
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}


}