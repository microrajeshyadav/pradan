<?php 
class Staff_sepclearancecertificate extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepclearancecertificate_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_Model","Common_Model");
		$this->load->model("Staff_approval_model");
		$this->load->model("Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		
		try{
				if($token){
					$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
					$content['role_permission'] = $this->db->query($query)->result();
					// end permission   
					$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
				  	$result  = $this->db->query($sql)->result()[0];
				  	$forwardworkflowid = $result->workflowid;		  

					$content['token'] = $token;
					$content['clearance_detail'] = $this->Staff_sepclearancecertificate_model->get_staff_clearance_detail($token);

					$content['staffpersondetail'] = $this->Staff_approval_model->getTransferStaffDetails($token);
				
					$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
					
					
					$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
					$content['finance_detail'] = $this->Common_Model->get_staff_finance_detail($content['staff_detail']->new_office_id,20);
					

					$finance_detail1 ='';
					$finance_detail1 = $this->Common_Model->get_staff_finance_detail($content['staff_detail']->new_office_id,20);
					// if(!empty($finance_detail1))
					// {
					// 		$this->session->set_flashdata('er_msg', "No Finance Avilable for this office, please contact system administrator");
					// }
					// 	else
					// 	{
				
					// $arrr = array();

					// if(!empty($finance_detail1))
     //   				foreach($finance_detail1 as  $value)
     //   				{
     //   					// print_r($value);
     //   					// die;
     //    			array_push($arrr, $value);
     //   				}
		    	// echo "<pre>";
		    	// print_r($arrr);exit();
					$RequestMethod = $this->input->server("REQUEST_METHOD");
					if($RequestMethod == 'POST')
					{
						// echo "<pre>";
						// print_r($_POST);
						// die;

						// print_r($this->input->post('separation_due_to')); die();
						$this->db->trans_start();

						if(empty($finance_detail1))
						{
							$this->session->set_flashdata('er_msg', 'Error !!! Finance are not available for this Team.Please contact your Administrator');
							redirect('/Staff_approval/index/');
						}
						else
						{
						$insertArray = array(

							'type'              => 3,
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $token,
							'flag'              => 1,
						);
						// print_r($insertArray); die;
						$this->db->insert('tbl_clearance_certificate',$insertArray);
						$insertid = $this->db->insert_id();

						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
						for ($i=0; $i < $projectcount; $i++) { 
							
							$insertArrayTran = array(

							'clearance_certificate_id'  => $insertid,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
						$this->db->insert('tbl_clearance_certificate_transaction',$insertArrayTran);

						}
						// if(trim($this->input->post('comments'))){
									// staff transaction table flag update after acceptance

		        	// $receiverdetail = $this->Staff_seperation_model->get_staffDetails($this->input->post('executivedirector_administration'));
									$updateArr = array(                         
				                'trans_flag'     => $this->input->post('status'),
				                'updatedon'      => date("Y-m-d H:i:s"),
				                'updatedby'      => $this->loginData->staffid
				            );

				            $this->db->where('id',$token);
				            $this->db->update('staff_transaction', $updateArr);
				            // foreach ($arrr as $key => $value)
				            // {
				            $insertworkflowArr = array(
				             'r_id'                 => $token,
				             'type'                 => 3,
				             'staffid'              => $content['clearance_detail']->staffid,
				             'sender'               => $this->loginData->staffid,
				             'receiver'             => $finance_detail1->staffid,
				             'forwarded_workflowid' => $forwardworkflowid,
				             'senddate'             => date("Y-m-d H:i:s"),
				             'flag'                 => $this->input->post('status'),
				             'scomments'            => $this->input->post('comments'),
				             'createdon'            => date("Y-m-d H:i:s"),
				             'createdby'            => $this->loginData->staffid,
				            );
				            // echo "<pre>";
				            // print_r($insertworkflowArr);
				            // die;

					        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
					         
					    // echo $this->db->last_query();
					    // die;
					    //     $subject = "Your Process Approved";
				            $body = 'Dear, '.$content['staff_detail']->name.'<br><br>';
				            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
				            $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your process has Approved  .<br><br>';
				            $body .= 'Thanks<br>';
				            $body .= 'Administrator<br>';
				            $body .= 'PRADAN<br><br>';

				            $body .= 'Disclaimer<br>';
				            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

				         //    foreach($arrr as $key => $value)
       						// {
       						// $receiverdetail = '';
       						// $receiverdetail = $this->Staff_seperation_model->get_staffDetails($value);

       						// $to_email = $receiverdetail->emailid;
       						// $to_name = $receiverdetail->name;
				         //    //$to_cc = $getStaffReportingtodetails->emailid;
       						// $recipients = array(
       						// 	$personnel_detail->EmailID => $personnel_detail->UserFirstName,
       						// 	$content['staff_detail']->emailid => $content['staff_detail']->name
				         //       // ..
				         //    );
				         //    $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);

       						// }

				            if (substr($email_result, 0, 5) == "ERROR") {
				              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
				            }
				        // }

							$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully');	
						}else{
							$this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
							redirect('/Staff_sepclearancecertificate/view/'.$insertid);			
						}
							
					
					}
				}


					$content['title'] = 'Staff_sepclearancecertificate';
					$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
					$this->load->view('_main_layout', $content);
				}else{
					$this->session->set_flashdata('er_msg','Missing trans id!!');
    			header("location:javascript://history.go(-1)", 'refresh');
				}

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


public function view($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission   

			$clear_seperate_detail = $this->Staff_sepclearancecertificate_model->get_staff_sepration_clearance_detail($token);



			
			$transid = $clear_seperate_detail->transid;

			

 $sql=" select max(workflowid) as workflowid from tbl_workflowdetail where  r_id = $transid";  
             
              
              $result  = $this->db->query($sql)->row();
               $forwardworkflowid = $result->workflowid; 
            

            
             
                $sql1="SELECT flag FROM tbl_workflowdetail   WHERE workflowid=$result->workflowid";
                $result1=$this->db->query($sql1)->row();
              $content['workflow_flag']=$result1;
             
             
              $forwardworkflowid = $result->workflowid; 	

			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;
			

			$content['clearance_transaction'] = $this->Staff_sepclearancecertificate_model->get_staff_sepration_clearance_transaction($token);
		
			$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($transid);
			


			$content['finance_detail'] = $this->Common_Model->get_staff_finance_detail($content['staff_detail']->new_office_id,20);

			
			$content['super_detail'] = $this->Common_Model->get_staffReportingto($content['staff_detail']->staffid);

			

			$content['clearance_detail'] = $this->Staff_sepclearancecertificate_model->get_staff_clearance_detail($transid);
			
			


			$content['title'] = 'Staff_sepclearancecertificate';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


	}