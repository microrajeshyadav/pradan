<?php 
class Developmentapprentice_sepclearancecertificate_fc extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepclearancecertificate_model');
		
		$this->load->model('Developmentapprentice_sepclearancecertificate_fc_model','model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_Model","Common_Model");
		$this->load->model("Staff_approval_model");
		$this->load->model("Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}
		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
				if($token){
					$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
					$content['role_permission'] = $this->db->query($query)->result();
					// end permission   
					$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
				  	$result  = $this->db->query($sql)->result()[0];
                    $forwardworkflowid = $result->workflowid;
                    $query ="SELECT * FROM staff_transaction WHERE id=".$token;
              		$content['staff_transaction'] = $this->db->query($query)->row(); 

              		$query ="SELECT * FROM staff WHERE staffid=".$content['staff_transaction']->reportingto;
              		$content['tc'] = $this->db->query($query)->row();   

                    $content['filnancelist'] = $this->Common_Model->get_finance_list();

					$content['token'] = $token;
                    $content['clearance_detail'] = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_clearance_detail($token);
                    if($content['clearance_detail']->flag)
                    {
                        redirect('/Developmentapprentice_sepclearancecertificate_fc/view/'.$content['clearance_detail']->tbl_da_exit_interview_form_id);
                    }
                    else
                    {
                        if($content['clearance_detail']->tbl_da_exit_interview_form_id)
                        {
                            redirect('/Developmentapprentice_sepclearancecertificate_fc/edit/'.$content['clearance_detail']->tbl_da_exit_interview_form_id);
                        }                        
                    }
					/*echo "<pre>";
					print_r($content['clearance_detail']);exit();*/
			
					$RequestMethod = $this->input->server("REQUEST_METHOD");
					if($RequestMethod == 'POST')
					{

					$Faname = '';
					$Faname  = $this->loginData->UserFirstName." ".$this->loginData->UserLastName;

                        $approvaldate = DateTime::createFromFormat('d/m/Y', $this->input->post('approvaldate'));
						$this->db->trans_start();

					
				if($this->input->post('btnsave') == 'Save'){
					$insertArray = array(

							'type'              => '',
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $token,
                            'flag'              => 0,
                            
						);
						// print_r($insertArray); die;
						$flag=$this->db->insert('tbl_da_clearance_certificate',$insertArray);

						$insertid = $this->db->insert_id();

						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();

						$this->db->where('clearance_certificate_id',  $insertid);
						$this->db->delete('tbl_da_clearance_certificate_transaction');

						for ($i=0; $i < $projectcount; $i++) { 
							
							$insertArrayTran = array(

							'clearance_certificate_id'  => $insertid,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
						$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$insertArrayTran);

						}

						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Clearance certificate form not submitted successfully');	
						}else{
							$this->session->set_flashdata('tr_msg', 'Clearance certificate form submitted successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate_fc/edit/'.$insertid);			
						}



				}
				else if($this->input->post('btnsubmit') == 'Save And Submit')
				{				
						
						$insertArray = array(

							'type'              => '',
							'separation_due_to' => $this->input->post('separation_due_to'),
							'transid'           => $token,
                            'flag'              => 1,
                            
						);
						// print_r($insertArray); die;
						$flag=$this->db->insert('tbl_da_clearance_certificate',$insertArray);					

					
						$insertid = $this->db->insert_id();
	
						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
						$this->db->where('clearance_certificate_id',  $insertid);
						$this->db->delete('tbl_da_clearance_certificate_transaction');
						for ($i=0; $i < $projectcount; $i++) { 
							
							$insertArrayTran = array(

							'clearance_certificate_id'  => $insertid,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
							
						$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction', $insertArrayTran);

						}

						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Clearance certificate form not submitted successfully');	
						}else{
							$comments = 'Clearance certificate form filled by Finance ';
							$updateArr = array(
					            'trans_flag'     => 4,
					            'updatedon'      => date("Y-m-d H:i:s"),
					            'updatedby'      => $this->loginData->staffid
					          );

							$this->db->where('id',$token);
							$this->db->update('staff_transaction', $updateArr);
							$insertworkflowArr = array(
					           'r_id'           => $token,
					           'type'           => '29',
					           'staffid'        => $content['clearance_detail']->staffid,
					           'sender'         => $this->loginData->staffid,
					           'receiver'       => $content['staff_transaction']->reportingto,
					           'senddate'       => date("Y-m-d H:i:s"),
					           'flag'           => 4,
					           'scomments'      => $comments,
					           'createdon'      => date("Y-m-d H:i:s"),
					           'createdby'      => $this->loginData->staffid,
					         );
					         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

					$subject1 = "Completion of graduation formalities";
					$body1  = "<h4>Dear ".$content['tc']->name.", </h4><br />";
					$body1 .= "I have completed the formalities. Kindly check."."<br/>";
					$body1 .= "<b> Regards, </b><br>";
					$body1 .= "<b>".$Faname." </b><br>";
	  
					$to_email1 = $content['tc']->emailid;
					// $to_name = $content['staff_transaction']->hr_name;
					$recipients = [];
					$email_result = '';
					$email_result = $this->Common_Model->send_email($subject1, $body1, $to_email1, $to_name= null, $recipients);


							$this->session->set_flashdata('tr_msg', 'Clearance certificate form submitted successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate_fc/view/'.$insertid);			
						}

					}
					
					}


					$content['title'] = 'Developmentapprentice_sepclearancecertificate';
					$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
					$this->load->view('_main_layout', $content);
				}else{
					$this->session->set_flashdata('er_msg','Missing trans id!!');
    			header("location:javascript://history.go(-1)", 'refresh');
				}

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


public function view($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission   

			$clear_seperate_detail = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_sepration_clearance_detail($token);

			// print_r($clear_seperate_detail);
			// die;

            $content['filnancelist'] = $this->Common_Model->get_finance_list();

			 //print_r($clear_seperate_detail); die;
			$transid = $clear_seperate_detail->transid;

			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;
			// print_r($clear_seperate_detail); die();

			$content['clearance_transaction'] = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_sepration_clearance_transaction($token);
			/*echo "<pre>";
			print_r($content['clearance_transaction']);exit();*/

			$content['clearance_detail'] = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_clearance_detail($transid);

			$content['da_image'] = $this->Common_Model->staff_signature($content['clearance_detail']->staffid);

               $content['supervisor_image'] = $this->Common_Model->staff_signature($content['clearance_detail']->reportingto);
			;
			/*$RequestMethod = $this->input->server("REQUEST_METHOD");
			if($RequestMethod == 'POST')
			{
			
			}*/



			$content['title'] = 'Staff_sepclearancecertificate';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}



public function edit($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission   

			$clear_seperate_detail = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_sepration_clearance_detail($token);
			
            $query ="SELECT * FROM staff_transaction WHERE id=".$token;
      		$content['staff_transaction'] = $this->db->query($query)->row();


			// print_r($clear_seperate_detail); die;
			$transid = $clear_seperate_detail->transid;
			// echo $transid;
			// die;

			$content['clearance_seprate_detail'] = 	$clear_seperate_detail;
			// print_r($clear_seperate_detail); die();

			$content['clearance_transaction'] = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_sepration_clearance_transaction($token);
			// echo "<pre>";
			// print_r($content['clearance_transaction']);exit();

			$content['clearance_detail'] = $this->Developmentapprentice_sepclearancecertificate_fc_model->get_staff_clearance_detail($transid);

            $content['filnancelist'] = $this->Common_Model->get_finance_list();
            


					$RequestMethod = $this->input->server("REQUEST_METHOD");
					if($RequestMethod == 'POST')
					{

                       
                        $approvaldate = DateTime::createFromFormat('d/m/Y', $this->input->post('approvaldate'));
                        
						$this->db->trans_start();

					
				if($this->input->post('btnsave') == 'Save'){
					$updateArr = array(

							'type'              => '',
							'separation_due_to' => $this->input->post('separation_due_to'),
							'flag'              => 0,
                            
						);
                        // print_r($insertArray); die;
                        $this->db->where('id', $token);
						$flag=$this->db->update('tbl_da_clearance_certificate', $updateArr);

						//$insertid = $this->db->insert_id();

						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
					$this->db->delete('tbl_da_clearance_certificate_transaction',array('clearance_certificate_id'=>$token));
					
						for ($i=0; $i < $projectcount; $i++) { 
							
							$updateArrayTran = array(

							'clearance_certificate_id'  => $token,
							
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
							
								$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$updateArrayTran);

						}


						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully');	
						}else{
							$this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate_fc/edit/'.$token);			
						}



				}
				else if($this->input->post('btnsubmit') == 'Save And Submit')
				{
					
						
						$updateArr = array(
							'type'              => '',
							'separation_due_to' => $this->input->post('separation_due_to'),
							'flag'              => 1,
                            
                            
						);
						// print_r($insertArray); die;
						$this->db->where('id', $token);
						$flag=$this->db->update('tbl_da_clearance_certificate', $updateArr);
					

					
						//$insertid = $this->db->insert_id();




	
						$projectcount = count($this->input->post('project'));
						// echo $projectcount;exit();
					$this->db->delete('tbl_da_clearance_certificate_transaction',array('clearance_certificate_id'=>$token));

						for ($i=0; $i < $projectcount; $i++) { 
							
							$updateArrayTran = array(

							'clearance_certificate_id'  => $token,
							'project'        			=> $this->input->post('project')[$i],
							'description'    			=> $this->input->post('description')[$i],
							
						);
							
								$flag_trans=$this->db->insert('tbl_da_clearance_certificate_transaction',$updateArrayTran);

						}

						$this->db->trans_complete();

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully');	
						}else{
							$comments = 'Clearance certificate form filled by Finance Administrator';
							$updateArr = array(
					            'trans_flag'     => 4,
					            'updatedon'      => date("Y-m-d H:i:s"),
					            'updatedby'      => $this->loginData->staffid
					          );

							$this->db->where('id',$token);
							$this->db->update('staff_transaction', $updateArr);
							$insertworkflowArr = array(
					           'r_id'           => $token,
					           'type'           => '',
					           'staffid'        => $content['clearance_detail']->staffid,
					           'sender'         => $this->loginData->staffid,
					           'receiver'       => $content['staff_transaction']->reportingto,
					           'senddate'       => date("Y-m-d H:i:s"),
					           'flag'           => 4,
					           'scomments'      => $comments,
					           'createdon'      => date("Y-m-d H:i:s"),
					           'createdby'      => $this->loginData->staffid,
					         );
					         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
							$this->session->set_flashdata('tr_msg', 'Clearance certificate form submitted successfully !!!');
							redirect('/Developmentapprentice_sepclearancecertificate_fc/view/'.$token);	
						}

					}
				}
	




			
			/*$RequestMethod = $this->input->server("REQUEST_METHOD");
			if($RequestMethod == 'POST')
			{
			
			}*/









            


			$content['title'] = 'Staff_sepclearancecertificate/edit';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}
			catch(Exception $e)
			{
				print_r($e->getMessage());
				die();
			}

		}


	}