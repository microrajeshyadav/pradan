<?php 

///// Handed over taken over Controller   

class Changeresponsibility_handed_over_taken_over_encourage extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Stafftransferpromotion_model","Stafftransferpromotion_model");
		$this->load->model("Changeresponsibility_handed_over_taken_over_encourage_model");
		$this->load->model("Global_model","gmodel");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token)
	{

		try{
	    // start permission 
			// echo "hjbjh"; die;
			//$staff_id=$this->loginData->staffid;

			$content['t_id'] = $this->model->getTransid($token);
			// print_r($content['t_id']); die;
			$staff_id=$content['t_id']->staffid;
			// print_r($staff_id); die;
			$supervisor = $content['t_id']->reportingto;
			// print_r($supervisor); die;
			$id = $content['t_id']->id;

			$content['staff_details'] = $this->model->get_staffDetails($token);
			// print_r($content['staff_details']); die;
			
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission

			

			$content['getstafflist'] = $this->model->getStaffList();

			$content['supervisor_list'] = $this->model->supervisor_detail($supervisor);
			// print_r($content['supervisor_list']); die;

			$query = "SELECT * FROM tbl_hand_over_taken_over_charge WHERE staffid = ".$token;
			$content['handed_over_detail'] = $this->db->query($query)->row();
			// print_r($content['handed_over_detail']); die;

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 
			if($RequestMethod == "POST")
				{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			
			if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

				$transferno=$this->input->post('transferno');

				$insertArraydata = array (
					
					'type'                => $this->input->post('handed'),
					'staffid'             => $staff_id,
					'transfernno'         => $transferno,
					'process_type'        => 'CR',
					'responsibility_name' => $this->input->post('change_responsibility_name'),
					'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('tdate')),
					'trans_to'            => $this->input->post('transferto'),
					'transid'             => $id,
					'flag'                => 0,
				);
				// print_r($insertArraydata); die;
				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
				$insertid = $this->db->insert_id();

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr = array (

						'item'           => $this->input->post('items')[$i],
						'handedtaken_id' => $insertid,
						'description'    => $this->input->post('items_description')[$i]
					);
					

					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

				}

			


				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Changeresponsibility_handed_over_taken_over_encourage/edit/'.$token);			
				}

			}




			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				// echo "<pre>";

				$sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$token";
				$result  = $this->db->query($sql)->result()[0];
				//print_r($result); die;
				// print_r($this->input->post()); die;	
				$transferno=$this->input->post('transferno');

				$insertArraydata=array(

					'type'                => $this->input->post('handed'),
					'staffid'             => $staff_id,
					'transfernno'         => $transferno,
					'process_type'        => 'CR',
					'responsibility_name' => $this->input->post('change_responsibility_name'),
					'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('tdate')),
					'trans_to'            => $this->input->post('transferto'),
					'transid'             => $id,
					'flag'                => 1,

				);
				// print_r($insertArraydata); die;
				//die();
				$this->db->insert('tbl_hand_over_taken_over_charge', $insertArraydata);
				//$this->db->insert($insertArraydata);

				$insertid = $this->db->insert_id();
				//echo $this->db->last_query();  die;

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr = array (
						
						'item'           => $this->input->post('items')[$i],
						'handedtaken_id' => $insertid,
						'description'    => $this->input->post('items_description')[$i]
					);

					$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);

				}


				$insertArraytrans = array (

					'trans_flag'    => 7,
					'updatedon'		=> date("Y-m-d H:i:s"),
					'updatedby'		=> $this->loginData->staffid,

				);
				
				//die();
				$this->db->where('id', $token);
				$this->db->update('staff_transaction', $insertArraytrans);


				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 4,
					'staffid'              => $staff_id,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $supervisor,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 7,
					'forwarded_workflowid' => $result->workflowid,
					'scomments'            => $this->input->post('remark'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
				// print_r($insertworkflowArr); die;

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Changeresponsibility_handed_over_taken_over_encourage/view/'.$insertid);			
				}

			}

		}


		$content['subview']="index";
		$content['title'] = 'Changeresponsibility_handed_over_taken_over_encourage';	
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}

public function Edit($token)
{
	try{
	    // start permission 
		
			$content['t_id'] = $this->model->getTransid($token);
			// print_r($content['t_id']); die;
			$staff_id=$content['t_id']->staffid;
			// print_r($staff_id); die;
			$supervisor = $content['t_id']->reportingto;
			// print_r($supervisor); die;
			$id = $content['t_id']->id;
			// print_r($id);

			$content['staff_details'] = $this->model->get_staffDetails($token);
			// print_r($content['staff_details']); die;

		$content['expense'] = $this->model->count_handedchrges($token);
		// print_r($content['expense']); die;

		$content['supervisor_list'] = $this->model->supervisor_detail($supervisor);
		$content['handed_expeness']=$this->model->handed_over_charge($token);
		// print_r($content['handed_expeness']); die;

		$content['transfer_expeness_details']=$this->model->expeness_details($content['handed_expeness']->id);
		// print_r($content['transfer_expeness_details']); die;



		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
			{	$submitdatasend = $this->input->post('submitbtn');

		$Sendsavebtn = $this->input->post('savetbtn');
			//print_r($_POST);
			//die();

		if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
			
			$transferno=$this->input->post('transferno');
			
			$insertArraydata = array (
					'type'                => $this->input->post('handed'),
					'staffid'             => $staff_id,
					'transfernno'         => $transferno,
					'process_type'        => 'CR',
					'responsibility_name' => $this->input->post('change_responsibility_name'),
					'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('trans_date')),
					'trans_to'            => $this->input->post('transferto'),
					'transid'             => $id,
					'flag'                => 0,
			);
			// print_r($insertArraydata); die;

			$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
			$this->db->where('id',$token);

			$countitem = count($this->input->post('items'));
			
			$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $content['handed_expeness']->id);


			for ($i=0; $i < $countitem; $i++) { 

				$arr=array(

					'handedtaken_id' => $content['handed_expeness']->id,
					'item'           => $this->input->post('items')[$i],
					'description'    => $this->input->post('items_description')[$i],
				);

				$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
			}

				//die();

			
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{

				$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
			}
			else{
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
				redirect('Changeresponsibility_handed_over_taken_over_encourage/edit/'.$token);			
			}

		}



		
		else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
		{

			$transferno=$this->input->post('transferno');
			
			$insertArraydata = array (
					'type'                => $this->input->post('handed'),
					'staffid'             => $staff_id,
					'transfernno'         => $transferno,
					'process_type'        => 'CR',
					'responsibility_name' => $this->input->post('change_responsibility_name'),
					'responsibility_date' => $this->gmodel->changedatedbformate($this->input->post('change_responsibility_date')),
					'trans_date'          => $this->gmodel->changedatedbformate($this->input->post('trans_date')),
					'trans_to'            => $this->input->post('transferto'),
					'transid'             => $id,
					'flag'                => 1,
			);

			$this->db->update('tbl_hand_over_taken_over_charge', $insertArraydata);
			$this->db->where('id',$token);

			$countitem = count($this->input->post('items'));
			
			$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);


			for ($i=0; $i < $countitem; $i++) { 

				$arr=array(

					'handedtaken_id' =>$token,
					'item'           =>$this->input->post('items')[$i],
					'description'    =>$this->input->post('items_description')[$i],
				);

				$this->db->insert('tbl_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
			}

					$insertArraytrans = array (

					'trans_flag'    => 7,
					'updatedon'		=> date("Y-m-d H:i:s"),
					'updatedby'		=> $this->loginData->staffid,

				);
				
				//die();
				$this->db->where('id', $token);
				$this->db->update('staff_transaction', $insertArraytrans);


				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 4,
					'staffid'              => $staff_id,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $supervisor,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 7,
					'forwarded_workflowid' => $result->workflowid,
					'scomments'            => $this->input->post('remark'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{

				$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
			}
			else{
				$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
				redirect('Changeresponsibility_handed_over_taken_over_encourage/view/'.$token);			
			}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
		}





	}
	$content['title'] = 'Changeresponsibility_handed_over_taken_over_encourage/edit';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);

}catch (Exception $e)		//print_r($staff_details);
 {
	print_r($e->getMessage());die;
}
}
public function view($token)
{
	try{
	    // start permission 
		$staff_id=$this->loginData->staffid;
		//echo "staff_id=".$staff_id;

		$query = "SELECT staffid,transid FROM tbl_hand_over_taken_over_charge WHERE id = ".$token;

		$content['handed_detail'] = $this->db->query($query)->row();

		$transid = $content['handed_detail']->transid;

		$content['t_id']=$this->model->getTransid($token);
			// print_r($content['t_id']);
		$id=$content['t_id']->id;

		$content['staff_details']=$this->model->get_staffDetails($token);

		$content['expense']=$this->model->count_handedchrges($token);

		$content['transfer_expeness_details']=$this->model->expeness_details($token);


		$content['handed_expeness']=$this->model->handed_over_charge($token);
 //print_r($content['handed_expeness']);
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		
		$content['title'] = 'Changeresponsibility_handed_over_taken_over_encourage/view';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}