
<?php 

/**
* State List
*/
class Revised_provident_fund_nomination_listing extends CI_controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->library('form_validation');
    $this->load->model("Revised_provident_listing_model","model");
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");

    $check = $this->session->userdata('login_data');

    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');

 }

 public function index()
 {
 	
 	

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();

  // end permission 

   $this->load->model("Revised_provident_fund_nomination_listing_model","model");

   $content['staff_details'] = $this->model->getstaffname();

   $RequestMethod = $this->input->server("REQUEST_METHOD");

   prepareview:
   $content['title'] = 'Revised_provident_fund_nomination_listing';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

 }

 

}