<?php 
class Generateofferletter extends Ci_controller
{
	function __construct()
	{
		parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model("Generateofferletter_model",'model');

		// $mod = $this->router->class.'_model';
	  // $this->load->model($mod,'',TRUE);
		// $this->model = $this->$mod;
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');
 }



  public function offerletterdetails($token)
 {
  try{
  // $token = 1;

        // $edsign = 'C:\xampp\htdocs\pradan\datafiles\imagenotfound.jpg';
        // echo $edsign; die();

  if (empty($token) || $token == '' ) {
    $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
    redirect('/Assigntc/index');

  } else {


    $getpayscale ='';
    $letterdetails ='';
    $getedname = '';
    $d_o_j = ''; 
    $doj = '';
    $lastdateAccept_docs = ''; 

    $this->db->delete('tbl_generate_offer_letter_details',  array('candidateid' => $token));

    $letterdetails = $this->model->getOfferDetails($token);
    // echo "<pre>";
    // print_r($letterdetails); die;

    if ($letterdetails == '-1') {
      $this->session->set_flashdata('er_msg', 'Sorry !!! Offer letter data not found !!!');
      redirect('/Assigntc/index');
      exit;
    }

    $getedname = $this->gmodel->getExecutiveDirectorEmailid();

    // echo $getedname->sign; die();

    if ($getedname->sign !='') {
        $edsign = site_url('datafiles/signature/'.$getedname->sign);
      }else{ 
      // $edsign = 'C:/xampp/htdocs/pradanhrm/datafiles/imagenotfound.jpg';
             $edsign = site_url('datafiles/imagenotfound.jpg');
        } 

        // echo $edsign; die();

    if ($letterdetails[0]->categoryid==1) {

    $getpayscale = $this->model->getpayscale($letterdetails[0]->categoryid);

      $Tdate = date("d/m/Y");
      $tydate = date("F jS, Y");
      $batch = $letterdetails[0]->batch;
      $candidatefirstname = $letterdetails[0]->candidatefirstname;


      $candidate = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs',
        '$batch',
        '$candidatefirstname',
        '$candidatemiddlename',
        '$candidatelastname',
        '$presentstreet',
        '$street',
        '$district',
        '$presentcity',
        '$presentdistrict',
        '$statename',
        '$presentpincode',
        '$tydate',
        '$officename',
        '$fieldguide',
        '$Tdate',
        '$district',
        '$phone1',
        '$email',
        '$edsign',
        '$edname',
        '$presentcity');

      $candidate_replace = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs',$batch,
        $candidatefirstname,
        $letterdetails[0]->candidatemiddlename,
        $letterdetails[0]->candidatelastname,
        $letterdetails[0]->presentstreet,
        $letterdetails[0]->street,
        $letterdetails[0]->district,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->presentdistrict,
        $letterdetails[0]->statename,
        $letterdetails[0]->presentpincode,
        $tydate,
        $letterdetails[0]->officename,
        $letterdetails[0]->fieldguide,
        $Tdate,
        $letterdetails[0]->district,
        $letterdetails[0]->phone1,
        $letterdetails[0]->email,
        '',
        $getedname->edname,
        $letterdetails[0]->presentcity);

            $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 23 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
            if(!empty($data))
            $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

            $body=  html_entity_decode($body); 

  }

  $lastdateAccept_docs = ''; 

      if ($letterdetails[0]->categoryid==2) {


    $getpayscale = $this->model->getpayscale($letterdetails[0]->categoryid);
    // echo $getpayscale->payscale; die();

      // $d_o_j = $this->model->changedatedbformate($doj);
      // $lastdateAccept_docs = $this->model->changedatedbformate($lastdateAcceptdocs);
      $Tdate = date("d/m/Y");
      $tydate = date("F jS, Y");
      // $batch = $letterdetails[0]->batch;
      $candidatefirstname = $letterdetails[0]->candidatefirstname;


      $candidate = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs',
        '$candidatefirstname',
        '$candidatemiddlename',
        '$candidatelastname',
        '$categoryname',
        '$presentstreet',
        '$street',
        '$district',
        '$presentcity',
        '$presentdistrict',
        '$statename',
        '$presentpincode',
        '$tydate',
        '$officename',
        '$Tdate',
        '$district',
        '$phone1',
        '$edsign',
        '$edname',
        '$presentcity',
        '$name',
        '$desname',
        '$basicsalary',
        '$payscale',
        '$dc_name');

      $candidate_replace = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs',
        $candidatefirstname,
        $letterdetails[0]->candidatemiddlename,
        $letterdetails[0]->candidatelastname,
        $letterdetails[0]->categoryname,
        $letterdetails[0]->presentstreet,
        $letterdetails[0]->street,
        $letterdetails[0]->district,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->presentdistrict,
        $letterdetails[0]->statename,
        $letterdetails[0]->presentpincode,
        $tydate,
        $letterdetails[0]->officename,
        $Tdate,
        $letterdetails[0]->district,
        $letterdetails[0]->phone1,
        '',
        $getedname->edname,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->name,
        $letterdetails[0]->desname,
        $getpayscale->basicsalary,
        $getpayscale->payscale,
        $letterdetails[0]->dc_name);

            $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid =58 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
            if(!empty($data))
            $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

          $body=  html_entity_decode($body);

  }
  $lastdateAccept_docs = ''; 

      if ($letterdetails[0]->categoryid==3) {


    $getpayscale = $this->model->getpayscale($letterdetails[0]->categoryid);

                    // $d_o_j = $this->model->changedatedbformate($doj);
      // $lastdateAccept_docs = $this->model->changedatedbformate($lastdateAcceptdocs);
      $Tdate = date("d/m/Y");
      $tydate = date("F jS, Y");
      // $batch = $letterdetails[0]->batch;
      $candidatefirstname = $letterdetails[0]->candidatefirstname;

      // if ($getedname->sign !='') {
      //   $edsign = site_url().'datafiles/signature/'.$getedname->sign;
      // }else{ 
      // $edsign = 'C:/xampp/htdocs/pradan/datafiles/imagenotfound.jpg';
      //       // $edsign = site_url().'datafiles/imagenotfound.jpg';
      //   }      


      $candidate = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs',
        '$candidatefirstname',
        '$candidatemiddlename',
        '$candidatelastname',
        '$categoryname',
        '$presentstreet',
        '$street',
        '$district',
        '$presentcity',
        '$presentdistrict',
        '$statename',
        '$presentpincode',
        '$tydate',
        '$officename',
        '$fieldguide',
        '$Tdate',
        '$district',
        '$phone1',
        '$edsign',
        '$edname',
        '$presentcity',
        '$name',
        '$desname',
        '$basicsalary',
        '$payscale',
        '$dc_name');

      $candidate_replace = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs',$candidatefirstname,
        $letterdetails[0]->candidatemiddlename,
        $letterdetails[0]->candidatelastname,
        $letterdetails[0]->categoryname,
        $letterdetails[0]->presentstreet,
        $letterdetails[0]->street,
        $letterdetails[0]->district,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->presentdistrict,
        $letterdetails[0]->statename,
        $letterdetails[0]->presentpincode,
        $tydate,
        $letterdetails[0]->officename,
        $letterdetails[0]->fieldguide,
        $Tdate,
        $letterdetails[0]->district,
        $letterdetails[0]->phone1,
        $letterdetails[0]->email,
        '',
        $getedname->edname,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->name,
        $letterdetails[0]->desname,
        $getpayscale->basicsalary,
        $getpayscale->payscale,
        $letterdetails[0]->dc_name);

            $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid =59 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
            if(!empty($data))
            $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

          $body=  html_entity_decode($body);
  }



    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){
      $summernote = $this->input->post('summernote');
     // print_r($this->input->post()); die;

      $this->db->trans_start();


      if ($letterdetails[0]->categoryid==1) {


       $getpayscale = $this->model->getpayscale($letterdetails[0]->categoryid);

       $fileno  = $this->input->post('fileno');
       $offerno = $this->input->post('offerno');
       $doj1     = $this->input->post('doj');
       $lastdateAcceptdocs1   = $this->input->post('lastdateofacceptanceofdocs');

       $doj = $this->model->changedatedbformate($doj1);
       $lastdateAcceptdocs = $this->model->changedatedbformate($lastdateAcceptdocs1);

       $this->form_validation->set_rules('fileno','file no ','trim|required|is_unique[tbl_generate_offer_letter_details.fileno]');
       $this->form_validation->set_rules('offerno','offer no ','trim|required|is_unique[tbl_generate_offer_letter_details.offerno]');
       $this->form_validation->set_rules('doj','Date Of Joining  ','trim|required');
       $this->form_validation->set_rules('lastdateofacceptanceofdocs','Last Date Of Acceptance Of Docs ','trim|required');

       if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }
      $filename = md5(time() . rand(1,1000));


      $insertArr = array(
        'candidateid'                => $token,
        'fileno'                     => $this->input->post('fileno'),
        'offerno'                    => $this->input->post('offerno'),
        'doj'                        => $doj,
        'lastdateofacceptanceofdocs' => $lastdateAcceptdocs,
        'flag'                       => 'Generate',
        'sendflag'                   => 0,
        'filename'                   => $filename,
       // 'basicsalary'                => $getpayscale->basicsalary==''?0.00:$getpayscale->basicsalary,
     //   'payscale'                   => $getpayscale->payscale==''?NULL:$getpayscale->payscale,
        'CreatedOn'                  => date('Y-m-d H:i:s'),
            'CreatedBy'                  => $this->loginData->UserID, // login user id
            'isdeleted'                  => 0, 
          );


      $this->db->insert('tbl_generate_offer_letter_details', $insertArr);
      // echo $this->db->last_query();





      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Error Generate Offer Letter');  
      }else{


        // $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 23 AND `isactive` = '1'";
        //     $data = $this->db->query($sql)->row();
        //     if(!empty($data))
        $cand = array('$fileno',
        '$offerno',
        '$doj',
        '$lastdateAccept_docs');

        $cand_replace = array($fileno,
        $offerno,
        $doj,
        $lastdateAccept_docs);
        $body = str_replace($cand,$cand_replace,$summernote);

        $insertArr = array(
        'generate_offerletter'                => $body 
          );


      $this->db->where('candidateid', $token);
      $this->db->update('tbl_candidate_registration', $insertArr);


             //echo $body; die();
     //$filename = md5(time() . rand(1,1000));
        $this->load->model('Dompdf_model');
        $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');


        $this->session->set_flashdata('tr_msg', 'Successfully Generate Offer Letter');     
         //}
        redirect('Assigntc/index');
        // die();
      }
    }else{


     $getpayscale = $this->model->getpayscale($letterdetails[0]->categoryid);

   // print_r($getpayscale); die;

     if (count($getpayscale) == 0) {
      $this->session->set_flashdata('er_msg', 'Basic salary data not found !!!');     
      redirect('Assigntc/index');
    }else{

      $fileno  = $this->input->post('fileno');
      $offerno = $this->input->post('offerno');
      $doj1     = $this->input->post('doj');
      $lastdateAcceptdocs1   = $this->input->post('lastdateofacceptanceofdocs');

      $doj = $this->model->changedatedbformate($doj1);
      $lastdateAcceptdocs = $this->model->changedatedbformate($lastdateAcceptdocs1);

      $this->form_validation->set_rules('fileno','file no ','trim|required|is_unique[tbl_generate_offer_letter_details.fileno]');
      $this->form_validation->set_rules('offerno','offer no ','trim|required|is_unique[tbl_generate_offer_letter_details.offerno]');
      $this->form_validation->set_rules('doj','Date Of Joining  ','trim|required');
      $this->form_validation->set_rules('lastdateofacceptanceofdocs','Last Date Of Acceptance Of Docs ','trim|required');

      if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }

      $filename = md5(time() . rand(1,1000));

      $insertArr = array(
        'candidateid'                => $token,
        'fileno'                     => $this->input->post('fileno'),
        'offerno'                    => $this->input->post('offerno'),
        'doj'                        => $doj,
        'lastdateofacceptanceofdocs' => $lastdateAcceptdocs,
        'flag'                       => 'Generate',
        'sendflag'                   => 0,
        'filename'                   => $filename,
        'basicsalary'                => $getpayscale->basicsalary==''?0.00:$getpayscale->basicsalary,
        'payscale'                   => $getpayscale->payscale==''?NULL:$getpayscale->payscale,
        'CreatedOn'                  => date('Y-m-d H:i:s'),
        'CreatedBy'                  => $this->loginData->UserID, // login user id
        'isdeleted'                  => 0, 
      );

     // print_r($insertArr); die;
      $this->db->insert('tbl_generate_offer_letter_details', $insertArr);

      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Error Generate Offer Letter');  
      }else{

        if($letterdetails[0]->categoryid==2){

        $cand = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs');

        $cand_replace = array($fileno,
        $offerno,
        $d_o_j,
        $doj,
        $lastdateAccept_docs);

        $body = str_replace($cand,$cand_replace,$summernote);
        
        $insertArr = array(
        'generate_offerletter'                => $body 
          );


      $this->db->where('candidateid', $token);
      $this->db->update('tbl_candidate_registration', $insertArr);


        }elseif($letterdetails[0]->categoryid==3){

        $cand = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$lastdateAccept_docs');

        $cand_replace = array($fileno,
        $offerno,
        $d_o_j,
        $doj,
        $lastdateAccept_docs);

        $body = str_replace($candidate,$candidate_replace,$summernote);

        $insertArr = array(
        'generate_offerletter'                => $body 
          );


      $this->db->where('candidateid', $token);
      $this->db->update('tbl_candidate_registration', $insertArr);

     }

//  $filename = md5(time() . rand(1,1000));
         $this->load->model('Dompdf_model');
         // echo $body; die;
         if($letterdetails[0]->categoryid==3){
          // echo $body; die;
         $generate =   $this->Dompdf_model->generatePDFAssistant($body, $filename, NULL,'Generateofferletter.pdf');
       }

         if($letterdetails[0]->categoryid==2){
          $generate =   $this->Dompdf_model->generatePDFExecutive($body, $filename, NULL,'Generateofferletter.pdf');
         }
        //     echo "sczxcxzc";
        // echo $generate; die;

         $this->session->set_flashdata('tr_msg', 'Successfully Generate Offer Letter');     
         //}
         redirect('Assigntc/index');



     }


   }
 }



}


prepareview:

$content['title'] = 'offerletterdetail';
$content['body'] = $body;
$content['candidate'] = $candidate;
$content['candidate_replace'] = $candidate_replace;
$content['candidateid'] = $token;
$content['getedname'] = $getedname;
$content['letterdetails'] = $letterdetails;
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);




}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

}