<?php 
class Recommended_to_graduate extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Recommended_to_graduate_model');
        // $this->load->model('Staff_seperation_model');
        $this->load->model("Common_model","Common_Model");
        $this->load->model("Global_model","gmodel");

        //$this->load->model(__CLASS__ . '_model');
        $mod = $this->router->class.'_model';
        $this->load->model($mod,'',TRUE);
        $this->model = $this->$mod;

        $check = $this->session->userdata('login_data');

        ///// Check Session //////  
        if (empty($check)) {

            redirect('login');

        }

        $this->loginData = $this->session->userdata('login_data');

        // print_r($this->loginData->hrdemailid);

    }

    public function index()
    {
        try{
        $content['recommend_to_graduate_list'] = $this->model->getRecommendedDA();
        $content['designation'] = $this->Common_Model->get_Staff_detail($this->loginData->staffid);
        /*echo "<pre>";
        print_r($content['recommend_to_graduate_list']);exit();*/

        $RequestMethod = $this->input->server('REQUEST_METHOD');  

        if($RequestMethod == 'POST')
        {

            // $staffid = $this->input->post('staffid');
            $data      = $this->input->post('IntegratorApproval');
            $id     = $this->input->post('id');
            // print_r($id); die;
            foreach ($data   as $key =>  $value) {


              $update = array(

                'integratorApproval'  => $value
            );
              // print_r($update); die;
              $this->db->where('staffid',$key);
              $this->db->update('staff',$update);

          }
          redirect('Recommended_to_graduate');
          
          


          // $update1 = array(

          //   'sendflag'  => 2
          // );
          // // print_r($update1); die;
          // $this->db->where('id', $id);
          // $this->db->update('tbl_offer_of_appointment',$update1);
      }


      $content['title'] = 'Recommended to Graduate';
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;     
      $this->load->view('_main_layout', $content);

      }catch (Exception $e) {
        print_r($e->getMessage());die;
    }
  }

  public function Appointment_letter($token){
        // echo "jkni"; die;

    try{

     $query = "select staffid from tbl_workflowdetail WHERE r_id =".$token;
     $staff1 = $this->db->query($query)->row();
// print_r($cand); die();

     $query = "select candidateid from staff Where staffid =".$staff1->staffid;
     $cand = $this->db->query($query)->row();

     $getEdDetails = $this->model->getRoleEdDetails();
            // print_r($getEdDetails);exit();
            //echo "token=".$token;

     $ed_name = $getEdDetails[0]->name;
     $hrd_name = $this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;

            //$edmail = $getEdDetails[0]->EmailID;
     $hrdemail = $this->loginData->EmailID;

     $this->db->where('transid', $token);
     $this->db->where('flag','Generate');
     $generate_query=$this->db->get('tbl_offer_of_appointment');
     $content['userDetail'] = $this->model->get_staffDetails($token);
     $content['officeList'] = $this->db->get('lpooffice')->result();
     $this->db->where('desid', 10);
     $content['designationData'] = $this->db->get('msdesignation')->row();
     $this->db->where('transid', $token);
     $content['dasummaryData'] = $this->db->get('tbl_da_summary')->row();

     $getpayscale ='';
     $letterdetails ='';
     $getedname = '';
     $d_o_j = ''; 
     $doj = '';
     $lastdateAccept_docs = ''; 

     $this->db->delete('tbl_generate_offer_letter_details',  array('candidateid' => $token));

     $letterdetails = $this->model->getOfferDetails($cand->candidateid);
    /*echo "<pre>";
    print_r($letterdetails); die;*/

            // echo $letterdetails[0]->categoryid; die();

     if ($letterdetails == '-1') {
      $this->session->set_flashdata('er_msg', 'Sorry !!! Offer letter data not found !!!');
      redirect('/Recommended_to_graduate/index');
      exit;
  }

  $getedname = $this->gmodel->getExecutiveDirectorEmailid();

  // if ($getedname->sign !='') {
  //       $edsign = site_url('datafiles/signature/'.$getedname->sign);
  //     }else{ 
  //     // $edsign = 'C:/xampp/htdocs/pradanhrm/datafiles/imagenotfound.jpg';
  //            $edsign = site_url('datafiles/imagenotfound.jpg');
  //       } 

  if ($letterdetails[0]->categoryid==2) {

    $getpayscale = $this->model->getpayscale(2);

    $Tdate = date("d/m/Y");
    $tydate = date("F jS, Y");
      // $batch = $letterdetails[0]->batch;
    $candidatefirstname = $letterdetails[0]->candidatefirstname;


    $candidate = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        '$candidatefirstname',
        '$candidatemiddlename',
        '$candidatelastname',
        '$categoryname',
        '$presentstreet',
        '$street',
        '$district',
        '$presentcity',
        '$presentdistrict',
        '$statename',
        '$presentpincode',
        '$tydate',
        '$officename',
        '$Tdate',
        '$district',
        '$phone1',
        '$edsign',
        '$edname',
        '$presentcity',
        '$name',
        '$desname',
        '$basicsalary',
        '$payscale',
        '$dc_name');

    $candidate_replace = array('$fileno',
        '$offerno',
        '$d_o_j',
        '$doj',
        $candidatefirstname,
        $letterdetails[0]->candidatemiddlename,
        $letterdetails[0]->candidatelastname,
        $letterdetails[0]->categoryname,
        $letterdetails[0]->presentstreet,
        $letterdetails[0]->street,
        $letterdetails[0]->district,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->presentdistrict,
        $letterdetails[0]->statename,
        $letterdetails[0]->presentpincode,
        $tydate,
        $letterdetails[0]->officename,
        $Tdate,
        $letterdetails[0]->district,
        $letterdetails[0]->phone1,
        '',
        $getedname->edname,
        $letterdetails[0]->presentcity,
        $letterdetails[0]->name,
        $letterdetails[0]->desname,
        $letterdetails[0]->basic,
        $getpayscale->payscale,
        $letterdetails[0]->dc_name);

    $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 65 AND `isactive` = '1'";
    $data = $this->db->query($sql)->row();

    if(!empty($data))
        $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

    $body=  html_entity_decode($body);

}




                    /*echo "<pre>";
                    print_r($generate_query->num_rows());exit();*/
                    if ($generate_query->num_rows() ==0 ) {
                // echo  'notgenerate';

                        $getpayscale ='';
                        $letterdetails ='';
                        $getedname = '';
                        $this->db->delete('tbl_offer_of_appointment',  array('transid' => $token));
                /*$letterdetails = $this->model->getOfferDetails($token);
                
                if ($letterdetails == '-1') {
                $this->session->set_flashdata('er_msg', 'Sorry !!! Offer letter data not found !!!');
                redirect('/Assigntc/index');
                exit;
            }*/
                // print_r($getpayscale);
                // die;
            $getedname = $this->gmodel->getExecutiveDirectorEmailid();
            $RequestMethod = $this->input->server('REQUEST_METHOD');
            if($RequestMethod == 'POST'){

                $fileno        = $this->input->post('fileno');
                $offerno       = $this->input->post('offerno');
                $doj1          = $this->input->post('doj');
                $officeid      = $this->input->post('officeid');
                $summernote    = $this->input->post('summernote');
                $doj           = $this->gmodel->changedatedbformate($doj1);
                $doj2          = date_create($doj);
                $dojtextformat = date_format($doj2, "F d, Y");
                $currentDate   = date("F d, Y");
                    // echo $currentDate;exit();
                $this->form_validation->set_rules('fileno','file no ','trim|required|is_unique[tbl_offer_of_appointment.fileno]');
                $this->form_validation->set_rules('offerno','offer no ','trim|required|is_unique[tbl_offer_of_appointment.offerno]');
                $this->form_validation->set_rules('doj','Date Of Joining  ','trim|required');
                $this->form_validation->set_rules('officeid','Selection of Office  ','trim|required');
                if($this->form_validation->run() == FALSE){
                    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
                        '</div>');

                    $this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

                    $hasValidationErrors    =    true;
                    goto prepareview;
                }
                $this->db->trans_start();
                $filename = md5(time() . rand(1,1000));
                $insertArr = array(
                    'candidateid'                => $content['userDetail']->candidateid,
                    'staffid'                    => $content['userDetail']->staffid,
                    'transid'                    => $token,
                    'fileno'                     => $this->input->post('fileno'),
                    'offerno'                    => $this->input->post('offerno'),
                    'doj'                        => $doj,
                    'proposed_officeid'          => $officeid,
                    'flag'                       => 'Generate',
                    'sendflag'                   => 0,
                    'filename'                   => $filename,
                    'CreatedOn'                  => date('Y-m-d H:i:s'),
                    'CreatedBy'                  => $this->loginData->UserID, // login user id
                    'personnelId'                  => $this->loginData->UserID,
                    'isdeleted'                  => 0 
                );

                $query = "select msdc_details.dc_name from msdc_details left join dc_team_mapping ON msdc_details.dc_cd=dc_team_mapping.dc_cd WHERE dc_team_mapping.teamid=".$officeid;
                $dcresult = $this->db->query($query)->row();
                $this->db->insert('tbl_offer_of_appointment', $insertArr);
                    // echo $this->db->last_query();
                    // die;
                $userName = $content['userDetail']->name;
                $address  = $content['userDetail']->officename;     
                $dcname = $dcresult->dc_name;
                $designation = $content['designationData']->desname;
                $content['superviserDetail'] = $this->model->getReportingto($officeid);
                $superviserName = $content['superviserDetail']->name;
                $this->db->where(array('level'=>4,'noofyear'=>0));
                $content['basicsalaryDetail'] = $this->db->get('tblsalary_increment_slide')->row();
                $basicsalary = $content['basicsalaryDetail']->basicsalary;
                $netsalary = $content['basicsalaryDetail']->netsalary;
                $this->db->where('officeid', $officeid);
                $content['officeData'] = $this->db->get('lpooffice')->row();
                $officename = $content['officeData']->officename;
                    // $filenamebypath = "mailtext/Offer_of_ appointment.txt";
                    // $fd = fopen("mailtext/Offer_of_ appointment.txt", "r");
                    // $message ='';
                    // $message .=fread($fd,filesize($filenamebypath));
                    // eval ("\$message = \"$message\";");
                    // $message =nl2br($message);

                    // $candidate = array('$fileno','$offerno','$currentDate','$designation','$userName','$address','$dojtextformat','$officename','$dcname','$superviserName','$basicsalary','$netsalary');

                    // $candidate_replace = array($fileno,$offerno,$currentDate,$designation,$userName,$address,$dojtextformat,$officename,$dcname,$superviserName,$basicsalary,$netsalary);

                $cand = array('$fileno',
                    '$offerno',
                    '$doj'
                );

                $cand_replace = array($fileno,
                    $offerno,
                    $dojtextformat
                );
                $body = str_replace($cand,$cand_replace,$summernote);


                $this->load->model('Dompdf_model');
                $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');

                    /*echo "<pre>";
                    echo $filename;
                    print_r($message);exit();*/
                    $this->db->trans_complete();
                    redirect('Recommended_to_graduate');
                }
                //die;
            }else{
                $this->session->set_flashdata('er_msg', 'Appointment letter already Generated');
                redirect('Recommended_to_graduate/index');
            }

        }catch (Exception $e) {
            print_r($e->getMessage());die;
        }
        prepareview:

        $content['title'] = 'appointment_letter';
        $content['body'] = $body;
        $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
        $this->load->view('_main_layout', $content);

    }
    public function sendofferlettered($token){

        try{

            $getEdDetails = $this->model->getRoleEdDetails();
            
            $ed_name=$getEdDetails[0]->name;
            $hrd_name=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;

            $edmail = $getEdDetails[0]->emailid;
            // $edmail = 'deepranjan5453@gmail.com';
            $hrdemail = $this->loginData->EmailID;


            $this->db->where('transid', $token);
            $this->db->where('flag','Generate');
            $generate_query=$this->db->get('tbl_offer_of_appointment');


            if ($generate_query->num_rows() ==0) {
                echo  'notgenerate';
                //die;
            }
            $this->db->where('transid', $token);
            $this->db->where('sendflag >=',1);
            $query=$this->db->get('tbl_offer_of_appointment');

            // echo  $query->num_rows();  die;

            if($query->num_rows() == 0){

            //  $html = 'Dear Sir, <br><br> Greetings from Bapurao Deshmukh College of Engineering, Sevagram.!!!!!!!!!!!!!!!!! We are planning to start recruitment drives for XXXXXX Batch in the month of XXXXXXXXX. It is our great privilege to invite you to visit Bapurao Deshmukh College of Engineering, Sevagram, one of the oldest Colleges in Vidarbha region for Campus Placement Recruitment Drives. We are ready to coordinate Closed/Pool drive & open to invite nearby Engineering Colleges affiliated University & Amravati University as per company requirements. So we are requesting you to finalize an early date to pick up the best candidates from our region.';
                $this->db->where('transid', $token);
                $filedata = $this->db->get('tbl_offer_of_appointment')->row();
                $message='';
                //echo $message;
                //die;
                $subject ="for approving the appointment letter approval";
                // $body =    'Dear Sir, <br><br> ';
                // $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
                //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
                // $fd = fopen("mailtext/approving_offerletter.txt","r"); 
                // $message .=fread($fd,4096);
                // eval ("\$message = \"$message\";");
                // $message =nl2br($message);
                // echo $message;
                // die;
                $attachments = array($filedata->filename.'.pdf');
                $recipients = array();

                $ed = array('$ed_name','$hrd_name');
                $ed_replace = array($ed_name,$hrd_name);
                /*echo "<pre>";
                print_r($sendmail = $this->Common_Model->send_email($subject, $message = $message, $edmail, $ed_name, $recipients, $attachments));exit();*/
                $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 35 AND `isactive` = '1'";
                $data = $this->db->query($sql)->row();
                if(!empty($data))
                    // $body = str_replace($staff,$staff_replace , $data->lettercontent);

                $sendmail = $this->Common_Model->send_email($subject, $body, $edmail, $ed_name, $recipients, $attachments);  //// Send Mail ED ////

            // $sendmailhrd = $this->Common_Model->send_email($subject, $message = $message, $hrdemail);  //// Send Mail HRD ////

                if ($sendmail != True) {
                    echo  'fail';
                    //$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send. Please Try Again !!!!'); 
                }else{

                    $updateArr1 = array(
                        'sendflag'          => 1,

                    );
                    $this->db->where('transid',$token);
                    $this->db->update('tbl_offer_of_appointment', $updateArr1);

                /*$updateArr2 = array(
                    'flag'          => 3,
                );
                $this->db->where('transid',$token);
                $this->db->update('edcomments', $updateArr2);*/

                $this->session->set_flashdata('tr_msg','Successfully Send to ED !!!!'); 


                echo 'send';                
            }

        }else{

            echo  'sent';

        }
    }catch (Exception $e) {
        print_r($e->getMessage());die;
    }

}
}