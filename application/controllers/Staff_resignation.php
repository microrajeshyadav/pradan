<?php 

/**
* State List
*/
class Staff_resignation  extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     try{
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from mskeyrules where isdeleted=0";
    $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";


    $content['title'] = 'Staff_resignation';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){
      
      
      $this->form_validation->set_rules('leave','Name','trim|required|min_length[1]|max_length[50]');
      $this->form_validation->set_rules('leaveaccured','Name','trim|required|min_length[1]|max_length[50]');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

   




      $insertArr = array(
          'Maxcf_yearleave'      => $this->input->post('leave'),
           'maxleaveaccrued'      => $this->input->post('leaveaccured'),
            'CYear'      => $this->input->post('cyear'),

        );
      
      $this->db->insert('mskeyrules', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added Leave Rule');
      redirect('/Staff_resignation/index/');
    }

    prepareview:

    $content['subview'] = 'Staff_resignation/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  public function edit($token)
  {
    try{

   
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('leavemax','Name','trim|required|min_length[1]|max_length[3]');
      $this->form_validation->set_rules('leaveaccured','Name','trim|required|min_length[1]|max_length[3]');
     if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


        $updateArr = array(
           'Maxcf_yearleave'      => $this->input->post('leavemax'),
           'maxleaveaccrued'      => $this->input->post('leaveaccured'),
            'CYear'      => $this->input->post('cyear'),
        );
      
      $this->db->where('CYear', $token);
      $this->db->update('mskeyrules', $updateArr);
 //echo  $this->db->last_query(); die;

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Leave Rule');
       redirect('/Staff_seperation/');
    }

    prepareview:

    $content['title'] = 'Staff_seperation';
    $Leave_details = $this->Common_Model->get_data('mskeyrules', '*', 'CYear',$token);
    $content['Leave_details'] = $Leave_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
    function delete($token)
    {
      try{
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('id', $token);
      $this->db->update('mskeyrules', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Leave Rules Deleted Successfully');
      redirect('/Staff_seperation/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);

      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

	public function preview()
  {
     try{
    /*// start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from mskeyrules where isdeleted=0";
    $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
*/

    $content['title'] = 'preview';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
 

}