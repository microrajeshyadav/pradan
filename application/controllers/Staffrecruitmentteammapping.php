<?php 
class Staffrecruitmentteammapping extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staffrecruitmentteammapping_model');
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		
	 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 					
					
			$content['recuitementdetails'] = $this->model->getRecruitment();
			$content['title'] = 'map';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	
	public function add(){
		 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 	
		try{


			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				// echo "<pre>";
				// print_r($this->input->post()); die;

				$this->db->trans_start();
	          
	            $currentfinancialyear = date('Y').'-'.date('Y', strtotime('+1 year'));

	            $count = count($this->input->post('recruitmentteam'));
	            // echo $count; die;
	         
	                 
	        for ($i=0; $i < $count; $i++) { 


	        $mapping_staff_recruitment = $this->input->post('recruitmentteam')[$i];


             $staffinfo = $this->model->staffDetailList($mapping_staff_recruitment);
             // print_r($staffinfo); 

              $stafffirstname='';
              $staffmiddlename='';
              $stafflastname='';
              // print_r($this->input->post('recruitmentteam'));  die;
             $expstaffname = explode(' ', $staffinfo->name);
             $staffnameaaraycount = count($expstaffname);
             // echo $staffnameaaraycount; die;
             if($staffnameaaraycount == 2){
             	$stafffirstname = $expstaffname[0];
             	$stafflastname= $expstaffname[1];
             }else if($staffnameaaraycount == 3){
             	$stafffirstname =  $expstaffname[0];
	             $staffmiddlename =  $expstaffname[1];
	             $stafflastname =  $expstaffname[2];
             }else{
             	$stafffirstname='';
	              $staffmiddlename='';
	              $stafflastname='';
             }
             
             $username = $stafffirstname.'_'.$staffinfo->staffid;
             $staffemailid = $staffinfo->emailid;
             $staffcontactno = $staffinfo->contact;
             $staffid = $staffinfo->staffid;
             $addlink = site_url('login');

			
			// echo "SELECT * FROM `mapping_staff_recruitment` 
   //          WHERE `year`= ".$currentfinancialyear." AND  `recruitmentteamid`=".$mapping_staff_recruitment; die;


            $query = $this->db->query("SELECT * FROM `mapping_staff_recruitment` 
            WHERE `year`= ".$currentfinancialyear." AND  `recruitmentteamid`=".$mapping_staff_recruitment." ");
            
            $numrows = $query->num_rows();
       		$result = $query->result();
       		// echo "<pre>";
       		// print_r($result); die;
       		if($result){
       		$mappingid = $result[0]->id_mapping_staff_recruitment;
		}

	           if($numrows > 0){

	           	 $updateArr2 = array(
				'recruitmentteamid ' => $this->input->post('recruitmentteam')[$i],
				'year'				=> $currentfinancialyear,
				'createdon'      	=> date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'      	=> $this->loginData->UserID, // login user id
			    'isdeleted'      	=> 0, 
			  );

			$this->db->where('id_mapping_staff_recruitment', $mappingid);
			$this->db->update('mapping_staff_recruitment', $updateArr2);


			 $InsertArr = array(
					 	'Username' 			=> $username,
					 	'Password'  		=> md5($stafffirstname),
					 	'UserFirstName'  	=> $stafffirstname,
					 	'UserMiddleName'  	=> $staffmiddlename,
					 	'UserLastName'  	=> $stafflastname,
					 	'PhoneNumber'  		=> $staffcontactno,
					 	'EmailID'  			=> $staffemailid,
					 	'RoleID'  			=> 30,
					 	'staffid'  			=> $staffid,
					 	'CreatedOn' 		=> date('Y-m-d H:i:s'),
					 	'CreatedBy' 		=> $this->loginData->UserID,
					  );
					
            $this->db->insert('mstuser', $InsertArr);

             $html = 'Dear '.$stafffirstname.', <br><br> 
			 Congratulations you are selected to recruitment team by Pradan. 
			 <br><br> Username - '.$username.'
			 <br>Password - '.$stafffirstname.'
			 <br>Role - Recruiter <br>
			 Please <a href='.$addlink.'>Click here</a>';

			$sendmail = $this->Common_Model->send_email($subject = 'Recruiter Login Details ', $message = $html, $staffemailid);


		 }else{

           	 $insertArr2 = array(
				'recruitmentteamid ' => $this->input->post('recruitmentteam')[$i],
				'year'				=> $currentfinancialyear,
				'createdon'      	=> date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'      	=> $this->loginData->UserID, // login user id
			    'isdeleted'      	=> 0, 
			  );
		
			$this->db->insert('mapping_staff_recruitment', $insertArr2);

		}


          $queryuser = $this->db->query("SELECT * FROM `mstuser` WHERE `Username` ='".$username."' AND `RoleID` = 30 ");
           $usernumrows = $queryuser->num_rows(); 

           if ($usernumrows == 0) {
           
          
			        $InsertArr = array(
					 	'Username' 			=> $username,
					 	'Password'  		=> md5($stafffirstname),
					 	'UserFirstName'  	=> $stafffirstname,
					 	'UserMiddleName'  	=> $staffmiddlename,
					 	'UserLastName'  	=> $stafflastname,
					 	'PhoneNumber'  		=> $staffcontactno,
					 	'EmailID'  			=> $staffemailid,
					 	'RoleID'  			=> 30,
					 	'staffid'  			=> $staffid,
					 	'CreatedOn' 		=> date('Y-m-d H:i:s'),
					 	'CreatedBy' 		=> $this->loginData->UserID,
					  );
					
                   $this->db->insert('mstuser', $InsertArr);


             $html = 'Dear '.$stafffirstname.', <br><br> 
			 Congratulations you are selected to recruitment team by Pradan. 
			 <br><br> Username - '.$username.'
			 <br>Password - '.$stafffirstname.'
			 <br>Role - Recruiter <br>
			 Please <a href='.$addlink.'>Click here</a>';


			 $sendmail = $this->Common_Model->send_email($subject = 'Recruiter Login Details ', $message = $html, $staffemailid);

			  }else{

			 $html = 'Dear '.$stafffirstname.', <br><br> 
			 Congratulations you are selected to recruitment team by Pradan. 
			 <br><br> Username - '.$username.'
			 <br>Password - '.$stafffirstname.'
			 <br>Role - Recruiter <br>
			 Please <a href='.$addlink.'>Click here</a>';

			 $sendmail = $this->Common_Model->send_email($subject = 'Recruiter Login Details ', $message = $html, $staffemailid);

			  }

}
	          
	          
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
			$this->session->set_flashdata('er_msg', 'Error adding Recruitment');	
			}else{
			$this->session->set_flashdata('tr_msg', 'Successfully added Recruitment');		
			}
			redirect('/Staffrecruitmentteammapping/index');
			
			}

			prepareview:

			$currentfinancialyear = date('Y').'-'.date('Y', strtotime('+1 year'));

			$selectedrecruiter      = $this->model->get_Listed_Recruiter($currentfinancialyear);
			$recruitid = array();
			foreach ($selectedrecruiter as $key => $value) {
				$recruitid[] = $value->recruitmentteamid;
			}
			$staffrecruitid = implode(", ", $recruitid);
			if (empty($staffrecruitid)) {
				$staffrecruitid = 0;
			}

			$content['selectrecruiters']= $selectedrecruiter;
		    $content['recuitementdetails'] = $this->model->getRecruitment();
			$content['staffdetails'] = $this->model->staffList($staffrecruitid);
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


	public function edit(){
		 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 	
		
		try{


			//print_r($this->input->post()); //die;
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
				$this->db->trans_start();

				$this->db->truncate('mapping_staff_recruitment');

				   $count = count($this->input->post('recruitmentteam'));
				
				   for ($i=0; $i < $count; $i++) { 
				

					$updateArray = array(
						'recruitmentteamid '    => $this->input->post('recruitmentteam')[$i],
						'createdon'      		=> date('Y-m-d H:i:s'),    // Current Date and time
					    'createdby'      		=> $this->loginData->UserID, // login user id
					    'isdeleted'      		=> 0, 
					  );
				
			        $this->db->insert('mapping_staff_recruitment', $updateArray);
			       }
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error update Recruitment');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully update Recruitment');			
					}
					redirect('/Staffrecruitmentteammapping/index');
			
			}

		
			$content['staffdetails'] = $this->model->staffList();
			$content['recruitmentupdate'] = $this->model->getRecruitmentDetails();
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}
	
	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete($token)
	{
		try{
		
		$this->view['detail'] = $this->model->getRecruitmentDeletes($token);
		//echo count($this->view['detail']); die;
		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		

		if($this->model->delete($token) == '1'){
			$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	 }

}