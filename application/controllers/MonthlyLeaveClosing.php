<?php 
class MonthlyLeaveClosing extends CI_Controller
{

	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('MonthlyLeaveClosing_model');
        $this->load->model('Staffleaveapplied_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
        $this->load->model('Dompdf_model');
        $this->load->model("General_nomination_and_authorisation_form_model");

		//$this->load->model(__CLASS__ . '_model');
        $mod = $this->router->class.'_model';
        $this->load->model($mod,'',TRUE);
        $this->model = $this->$mod;

        $check = $this->session->userdata('login_data');

		///// Check Session //////	
        if (empty($check)) {

         redirect('login');

     }

     $this->loginData = $this->session->userdata('login_data');
     /*echo "<pre>";
     print_r($this->loginData);die();*/
 }

 public function index()
 {
    try{

        $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
        $content['role_permission'] = $this->db->query($query)->result();
        // end permission  

        $content['office'] = $this->Staffleaveapplied_model->getoffice();
        $officeid = $content['office'][0]->officeid;
        $content['staffofficeid'] = $this->Staffleaveapplied_model->staffemp_code($this->loginData->staffid);
        $RequestMethod = $this->input->server('REQUEST_METHOD'); 
        if($RequestMethod == "POST"){
            $search = $this->input->post('submit');
            if($search == 'search'){
                //print_r($this->input->post()); 

                $content['officeid'] = $this->input->post("office");
                $fromdate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('fromdate'));
                $todate=$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('todate'));

                // echo $fromdate;
                if($fromdate!=null && $todate!=null)
                {
                    $laeve_array = array (
                        'From_date' => $fromdate,
                        'To_date'   => $todate,
                        'officeid'  => $content['officeid'],
                    );
                }
                else if($fromdate==null || $todate==null)
                {
                    // echo "1";

                    $firstdayofpreviousmonth = date('Y-m-d', strtotime('first day of previous month'));
                    $lastdayofpreviousmonth = date('Y-m-d', strtotime('last day of previous month'));    
                    $fromdate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($firstdayofpreviousmonth);
                    $todate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($lastdayofpreviousmonth);
                    $laeve_array = array (
                        'From_date' => $fromdate,
                        'To_date'   => $todate,
                        'officeid'  => $content['officeid'],
                    );
                }

                // print_r($laeve_array); die();

                $content['staffleavedetails'] = $this->MonthlyLeaveClosing_model->monthlyleaveclosing($laeve_array);
                

            }

            else
            {

                $laeve_array = array (
                    'From_date' => '',
                    'To_date'   => '',
                    'officeid'  => $officeid,
                );
                $content['staffleavedetails'] = $this->MonthlyLeaveClosing_model->monthlyleaveclosing($laeve_array);
    // print_r($content['staffleavedetails']); die;
            }

        }
        else
        {

            $laeve_array = array (
                'From_date' => '',
                'To_date'   => '',
                'officeid'  => $officeid,
            );
            $content['staffleavedetails'] = $this->MonthlyLeaveClosing_model->monthlyleaveclosing($laeve_array);
    // print_r($content['staffleavedetails']); die;
        }


        $content['title'] = 'Monthly Leave Closing';
        $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
        $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
        print_r($e->getMessage());die;
    }



}
}