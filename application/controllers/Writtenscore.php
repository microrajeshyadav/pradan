<?php 
class Writtenscore extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model("Writtenscore_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');



	}

	public function index()
	{
		 // start permission 
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." "; 
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{
			if($this->input->post('campusid')){
			 $campusdata = $this->input->post('campusid');
			 $campexp = explode('-', $campusdata);
			 $campusid = $campexp[0];
			 $campusintimationid = $campexp[1];

			}else {
				$campusid = 'NULL';
				$campusintimationid = 'NULL';
				$campusdata = '';
			}

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				$campusid = $this->input->post('campusid');
				$syscampusid = $this->session->set_userdata('campusid', $campusid);
				$campexp = explode('-', $campusdata);
				$campusid = $campexp[0];
				$campusintimationid = $campexp[1];
				
				$savewrittenscore = $this->input->post('save_writtenscore');

				if (isset($savewrittenscore) &&  $savewrittenscore =='save') {

				$updateArr = array(
					'campus_written_cutoffmarks' => ($this->input->post('cutoffmarks') ==''? 0: $this->input->post('cutoffmarks')),
					'campus_status'       => 0,
					'updatedon'    => date('Y-m-d H:i:s'),
					'updatedby'    => $this->loginData->UserID, // login user id
				);
				
				$this->db->where('id', $campusintimationid);
				$this->db->where('campusid', $campusid);
				$this->db->update('tbl_campus_intimation', $updateArr);

				$writtenscore = $this->input->post('writtenscore'); 

					foreach ($writtenscore as $key => $value) {

						$this->db->trans_start();

						$this->db->where('candidateid', $key);
						$num_rows =  $this->db->count_all_results('tbl_candidate_writtenscore'); 

						if($num_rows > 0 ){

						$sql = "SELECT * FROM `tbl_candidate_writtenscore` where `candidateid`=$key"; 
						$query = $this->db->query($sql);
						$result = $this->db->query($sql)->result();
					
						$writtenscoreid = $result[0]->writtenscoreid;
						
						$updateArr = array(
						'writtenscore'    		    =>  $value,
						'candidateid'    		    => $key,
						'cutoffmarks'				=> ($this->input->post('cutoffmarks') ==''? 0: $this->input->post('cutoffmarks')),
						'flag'						=> 'save',
						
						'updatedon'      		    => date('Y-m-d H:i:s'),
						'updatedby'      		    => $this->loginData->UserID, // login user id
						);

					$this->db->where('writtenscoreid', $writtenscoreid);
					$this->db->update('tbl_candidate_writtenscore', $updateArr);

					 $candidateupdateArr = array(
						'wstatus'    		    => 0,
						'campusintimationid'    => $campusintimationid,
					);
		
				$this->db->where('candidateid', $key);
				$this->db->update('tbl_candidate_registration', $candidateupdateArr);
						
			}else{

			$insertArr = array(
					'writtenscore'    		    => ($value ==''? 0: $value),
					'candidateid'    		    => $key,
					'cutoffmarks'				=> ($this->input->post('cutoffmarks') ==''? 0: $this->input->post('cutoffmarks')),
					'flag'				        => 'save',
					'Createdon'      		    => date('Y-m-d H:i:s'),
		            'Createdby'      		    => $this->loginData->UserID, // login user id
				);

				$this->db->insert('tbl_candidate_writtenscore', $insertArr);

//echo $this->db->last_query(); die;


				$candidateupdateArr = array(
					'wstatus'    => 0,
					'campusintimationid'    => $campusintimationid,
				);
		
				$this->db->where('candidateid', $key);
				$this->db->update('tbl_candidate_registration', $candidateupdateArr);

				}

				$this->db->trans_complete();
			}
					
			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding Written Score');	
				redirect('/Writtenscore/index');
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Written Score');	
				redirect('/Writtenscore/index');		
			}
			
		}
	}


			$content['campusid'] = $campusdata;
		
			$content['selectedcandidatedetails']  = $this->model->getSelectedCandidate($campusid,$campusintimationid);
			 //print_r($content['selectedcandidatedetails']);
			// die;

			$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
			$content['campuscutoffmarks'] = $this->model->getCampusCutOffMarks($campusid, $campusintimationid);

			$content['selectedcandidatewrittenscore'] 	 = $this->model->getSelectedCandidateWrittenScore($campusid);
			// print_r($content['selectedcandidatewrittenscore']);
			// die;

			$content['method']  = $this->router->fetch_method();
			$content['title']  = 'Writtenscore';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	public function Selecttowrittenexamcandidates() 
	{
				 // start permission 
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			//print_r($this->loginData); 
			if($this->input->post('campusid') !=NULL && $this->input->post('campusid') !=''){
				// print_r($this->input->post('campusid')); die();
			$campusdata = $this->input->post('campusid');
			$campexp = explode('-', $campusdata);
			$campusid = $campexp[0];
			$campusintimationid = $campexp[1];

			}else {
				$campusdata =NULL;
				$campusid ='NULL';
				$campusintimationid = 'NULL';

			}

			$content['campusid'] = $campusdata;
			$selected_candidate_list = $this->model->getElihibalCandidateListForWrittenExam($campusid, $campusintimationid);


			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				
				$this->db->trans_start();

				if ($this->input->post('btnsend')=='candidate_list') {

						$message='';
					// 	echo "<pre>";
					// 	print_r($candidatedetails);
						$campus = $this->model->getCampusEmail($campusid);
						$campus_fromdate = $campus[0]->fromdate;
						$date = strtotime($campus_fromdate);
					    $date = date('F,j,Y', $date); 
					 //    $campus_fromdate = $date;
						// $campus_name = $campus[0]->campusname;
						// $campus_mobile = $campus[0]->mobile;
						// $campus_telephone = $campus[0]->telephone;
						// $campusincharge = $campus[0]->campusincharge;

						$campuss = array('$campus_fromdate','$campus_name','$campus_mobile','$campus_telephone','$campusincharge');
						$campus_replace = array($date,$campus[0]->campusname,$campus[0]->mobile,$campus[0]->telephone,$campus[0]->campusincharge); 
						$subject = "From PRADAN"; 
		 	//print_r($candidatedetails); die;
						// $fd = fopen("mailtext/On_campus_Selection.txt", "r"); 
						// $message .=fread($fd,4096);
						// eval ("\$message = \"$message\";");
						// $message =nl2br($message);
						// $body = $message;

						//update from db
						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 11 AND `isactive` = '1' AND id = 8";
   		    			$data = $this->db->query($sql)->row();
   		    			 if(!empty($data))
   		   				 {
   		   				 $body = str_replace($campuss,$campus_replace , $data->lettercontent);

						$selectedcandidatedetails = $this->model->getElihibalCandidateListForWrittenExam($campusid, $campusintimationid);
						$html = '<table style="width:100%">
                        <thead>
                         <tr>';
                        // $html .='<th>S. No.</th>';
                        $html .='                        
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email Id</th>
                        <th>Mobile </th>
                        <th>Stream</th>
                        </tr> 
                      </thead>
                      <tbody>'; 
                        $i=0; foreach ($selectedcandidatedetails as $key => $value) {
                        $html .= '<tr>';
                       // $html .='<td class="text-center">';
                        $i=$i+1;  
                        // $html .=''.$i.'</td>';
                      $html .='<td>'. $value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname.'</td>
                      <td>'. $value->gender.'</td>
                      <td>'. $value->emailid.' </td>
                      <td>'. $value->mobile.'</td>
                      <td>'. $value->stream.'</td>
                      </tr>';
                       $i++; }
                      $html .= '</tbody>
                    </table>';
							// echo $html;
							$filename = md5(time() . rand(1,1000));
							$this->load->model('Dompdf_model');
						    $generate =   $this->Dompdf_model->generatePDF($html, $filename, NULL,'GenerateSelectedList.pdf');

						    $pdffilename = $filename.'.pdf';
							$attachments = array($pdffilename);
			 	$to_email    = $campus[0]->emailid; //// Candidate Mail Id ////
			 	
			 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name = null,$recipients=null, $attachments);
			 }
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error Candidate not accept ');	
						redirect(current_url());	
					}else{

						$this->session->set_flashdata('tr_msg', 'Successfully Candidate accept');
						redirect('Writtenscore/Selecttowrittenexamcandidates');
					}

				}


				

			}
			
			$content['selectedcandidatedetails'] = $selected_candidate_list;
			$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
			$content['title']    = 'Writtenscore';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	public function generate_pdf_selected_candidate($token=NULL) 
	{

if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Writtenscore/index');
				
			} else { 

				 // start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			//echo $token; die;

			
			$selectedcandidatedetails = $this->model->getSelectedCandidateWrittenScore($token);
			//print_r($selectedcandidatedetails); die;


			echo $table = ' <table>';
			$table .= '<tr>';
			$table .= '<td>S.No.</td>';
			$table .= '<td>Candidate</td>';
			$table .= '<td> Email Id</td>';
			$table .= '</tr>'; 
			$i=0;
			foreach ($selectedcandidatedetails as $key => $value) { 

				$table .=  '<tr>';
				$table .=  ' <td>'.$i.'</td>';
				$table .=  '<td>'.$value->candidatefirstname.'</td>';
				$table .=  '<td>'.$value->emailid.'</td>';
				$table .=  '</tr> ';
			}
			$table .=  '</table> ';
			die;
			
			$content['campusdetails'] = $this->model->getCampus();
			$content['title']    = 'Writtenscore';
			$content['subview']  = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
}

}