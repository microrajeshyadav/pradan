<?php 

/**
* State List
*/
class Staff_personnel_midterm_review extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
     $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_personnel_midterm_review_model","Staff_personnel_midterm_review_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
  // end permission   


   $content['hrd_staff_details'] = $this->Staff_personnel_midterm_review_model->getmidtermreviewworkflowList();
  
   $content['title'] = 'Staff_personnel_midterm_review';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

 public function send_to_personnel()
 {
  try{

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
  // end permission   


   $content['hrd_staff_details'] = $this->Staff_personnel_midterm_review_model->getmidtermreviewworkflowList();


  $RequestMethod = $this->input->server('REQUEST_METHOD');

      if($RequestMethod == 'POST'){

    //      echo "<pre>";
        // print_r($this->input->post()); die;

        $this->db->trans_start();
    

        $sql = "SELECT max(workflowid) as maxworkflowid,r_id FROM tbl_workflowdetail WHERE staffid=".$token;
        $res = $this->db->query($sql)->result()[0];
  

        $insertworkflowArr = array(
          'r_id'                 => $res->r_id,
          'type'                 => 22,
          'staffid'              => $token,
          'sender'               => $this->loginData->staffid,
          'receiver'             => $this->input->post('supervisorid'),
          'senddate'             => date("Y-m-d H:i:s"),
          'flag'                 => 25,
          'createdon'            => date("Y-m-d H:i:s"),
          'createdby'            => $this->loginData->staffid,
        );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);


        $this->db->trans_complete();

        if ($this->db->trans_status() === true){
          $this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
          redirect('/Staff_personnel_midterm_review/hrview/'.$token);
        }else{
          $this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
          redirect(current_url());
        }

      }

  
  
   $content['title'] = 'Staff_personnel_midterm_review';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }




}