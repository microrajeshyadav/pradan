<?php 
class Developmentapprentice_sepchecksheet extends CI_Controller
{

      ///echo  "zZxZxZX"; die; 
      
      function __construct()
      {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Staff_sepchecksheet_model');
            $this->load->model("Common_model","Common_Model");
            $this->load->model("Global_model","gmodel");
            $this->load->model("Staff_approval_model");
            $this->load->model("Staff_seperation_model");
             $this->load->model('Developmentapprentice_sepchecksheet_model','model');

            $this->load->model(__CLASS__ . '_model');
            $mod = $this->router->class.'_model';
          $this->load->model($mod,'',TRUE);
            $this->model = $this->$mod;

            $check = $this->session->userdata('login_data');

            ///// Check Session //////    
            if (empty($check)) {
                   redirect('login');                  
            }

            $this->loginData = $this->session->userdata('login_data');
      
      }

      public function index($token)
      {
            // start permission 
        try{
            // echo "token=".$token;
            // die;
            if($token){
              $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
              $content['role_permission'] = $this->db->query($query)->result();
              // end permission   
              // $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
              // $result  = $this->db->query($sql)->result()[0];
              // $forwardworkflowid = $result->workflowid; 
              $query ="SELECT * FROM staff_transaction WHERE id=".$token;
              $content['staff_transaction'] = $this->db->query($query)->row();              
              $content['staff_detail'] = $this->model->get_staff_sep_detail($token);
              // $content['personaldetail'] = $this->Staff_approval_model->getExecutiveDirectorList(17);

              // $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
              $content['hrlist'] = $this->Common_Model->get_hr_Staff_List();
              $superviserquery = "select receiver from tbl_workflowdetail where r_id=".$token." AND flag= 5";
              $content['superviserreceiver'] = $this->db->query($superviserquery)->row();
              $hrquery = "select sender from tbl_workflowdetail where r_id=".$token." AND flag= 1";
              $content['hrreceiver'] = $this->db->query($hrquery)->row();
              $content['curUserDetails'] = $this->model->get_staffDetails($this->loginData->staffid);
             /* echo "<pre>";
              print_r($content['staff_detail']);exit();*/
              $content['token'] = $token;
              
              $query = "SELECT a.*, b.name as financename, c.name as supervisername, d.name as hrname, e.officename as financeoffice, f.officename as superviseroffice, g.officename as hroffice FROM tbl_da_check_sheet as a 
              LEFT JOIN staff as b ON b.staffid = a.financeid
              LEFT JOIN staff as c ON c.staffid = a.superviserid
              LEFT JOIN staff as d ON d.staffid = a.hrdid
              LEFT JOIN lpooffice as e ON e.officeid = b.new_office_id
              LEFT JOIN lpooffice as f ON f.officeid = c.new_office_id
              LEFT JOIN lpooffice as g ON g.officeid = d.new_office_id 
              WHERE a.transid=".$token;
              $content['tbl_check_sheet'] = $this->db->query($query)->row();
              /*echo "<pre>";
              print_r($content);
              die;*/
              $RequestMethod = $this->input->server('REQUEST_METHOD');
              if($RequestMethod == "POST"){
                    $flag='';
                    $transaction_flag = '';
                    if($content['staff_transaction']->trans_flag == 4){
                      $comments = 'Check Sheet form filled by finance';
                      $transaction_flag = 5;
                      $receiverdetail = $this->model->get_staffDetails($content['staff_transaction']->reportingto);
                      /*echo "<pre>";
                      print_r($receiverdetail);exit();*/
                      $mergeddataset = array(                    
                        'financeid'=>$this->input->post('curuserid'),
                        'financeapprovaldate'=>$this->gmodel->changedatedbformate($this->input->post('financeapprovaldate')),
                      );
                    }else if($content['staff_transaction']->trans_flag == 5){
                      $comments = 'Check Sheet form approved by Superviser';
                      $transaction_flag = 6;
                      $receiverdetail = $this->model->get_staffDetails($content['hrreceiver']->sender);
                      $mergeddataset = array(                    
                        'superviserid'=>$this->input->post('curuserid'),
                        'superviserapprovaldate'=> $this->gmodel->changedatedbformate($this->input->post('superviserapprovaldate')),
                      );
                    }else if($content['staff_transaction']->trans_flag == 6){
                      $comments = 'Check Sheet form approved by HRD';
                      $staff_update_data = array(
                        'dateofleaving' => $content['staff_transaction']->effective_date,
                        'separationtype'=> $content['staff_transaction']->trans_status,
                        'separationremarks' => $content['staff_transaction']->reason,
                        'status' => 0
                      );
                      $transaction_flag = 7;
                      $receiverdetail = $this->model->get_staffDetails($content['hrreceiver']->sender);
                      $mergeddataset = array(                    
                        'hrdid'=>$this->input->post('curuserid'),
                        'hrdapprovaldate'=>$this->gmodel->changedatedbformate($this->input->post('hrdapprovaldate')),
                      );
                    }  

                    $db_flag = '';

                    /*echo "<pre>";
                    print_r($comments);
                    print_r($transaction_flag);
                    print_r($receiverdetail);
                    print_r($mergeddataset);exit();*/
                    if($this->input->post('Save') == 'Save'){
                          $db_flag = 0;
                    }else{
                      
                      /*echo "<pre>";
                      print_r($token);exit();*/
                      $db_flag = 1;
                      $updateArr = array(
                              'trans_flag'     => $transaction_flag,
                              'updatedon'      => date("Y-m-d H:i:s"),
                              'updatedby'      => $this->loginData->staffid
                            );

                      $this->db->where('id',$token);
                      $this->db->update('staff_transaction', $updateArr);
                      $insertworkflowArr = array(
                        'r_id'           => $token,
                        'type'           => 27,
                        'staffid'        => $content['staff_detail']->staffid,
                        'sender'         => $this->loginData->staffid,
                        'receiver'       => $receiverdetail->staffid,
                        'senddate'       => date("Y-m-d H:i:s"),
                        'flag'           => $transaction_flag,
                        'scomments'      => $comments,
                        'createdon'      => date("Y-m-d H:i:s"),
                        'createdby'      => $this->loginData->staffid,
                      );
                      $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
                      // $subject = "Check Sheet Form Detail";
                      // $body = 'Dear, '.$receiverdetail->name.'<br><br>';
                      // $body .= '<h2>Check Sheet Form Submited Successfully </h2><br>';
                      // $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your Check Sheet Form has filled  .<br><br>';
                      // $body .= 'Thanks<br>';
                      // $body .= 'Administrator<br>';
                      // $body .= 'PRADAN<br><br>';

                      // $body .= 'Disclaimer<br>';
                      // $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


                      // $to_email = $receiverdetail->emailid;
                      // $to_name = $receiverdetail->name;
                      // //$to_cc = $getStaffReportingtodetails->emailid;
                      // $recipients = array(
                      // $content['staff_detail']->emailid => $content['staff_detail']->name
                      // // ..
                      // );
                      // $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
                      // if (substr($email_result, 0, 5) == "ERROR") {
                      //   $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
                      // }
                    }
                    

              if($content['tbl_check_sheet']){
                
                //$data_updateres = array_merge($data_update, $mergeddataset);
                /*echo "<pre>";
                print_r($data_updateres);exit();*/
                if($content['staff_transaction']->trans_flag == 4){
                  $data_update = array(
                  'staffid'=>$content['staff_transaction']->staffid,
                  'transid'=>$token,
                  'date_resignation_submitted_7'=>$this->gmodel->changedate($this->input->post('date_resignation_submitted_7')),
                  'date_from_which_separation_takes_effect_8'=>$this->gmodel->changedate($this->input->post('date_from_which_separation_takes_effect_8')),
                  'whether_under_probation_9'=>$this->input->post('whether_under_probation_9'),
                  'if_not_under_probation_10'=>$this->input->post('if_not_under_probation_10'),
                  'Vehicle_Loan'=>$this->input->post('Vehicle_Loan'),
                  'stipend_in_lieu_of_excess_leave'=>$this->input->post('stipend_in_lieu_of_excess_leave'),
                  'other_loans_and_advances'=>$this->input->post('other_loans_and_advances'),

                  'outstanding_dues'=>$this->input->post('outstanding_dues'),
                  'stipend'=>$this->input->post('stipend'),
                  'personal_claim'=>$this->input->post('personal_claim'),
                  'any_other'=>$this->input->post('any_other'),
                  'payments_due_to_employee'=>$this->input->post('payments_due_to_employee'),
                  'amounts_recoverable'=>$this->input->post('amounts_recoverable'),
                  'net_payable_recoverable'=>$this->input->post('net_payable_recoverable'),
                  'updatedon'=>date('Y-m-d H:i:s'),
                  'updatedby'=>$this->loginData->staffid,
                  'flag'=>$db_flag
                  );
                  $data_updateres = array_merge($data_update, $mergeddataset);
                  $this->db->where('id', $content['tbl_check_sheet']->id);
                  $flag = $this->db->update('tbl_da_check_sheet', $data_update);
                }
                if($content['staff_transaction']->trans_flag == 5){
                  $this->db->where('id', $content['tbl_check_sheet']->id);
                  $flag = $this->db->update('tbl_da_check_sheet', $mergeddataset);
                }
                if($content['staff_transaction']->trans_flag == 6){
                  $tbl_candidate_dataset = array('isdeleted' => 1);
                  $this->db->where('candidateid', $content['staff_detail']->candidateid);
                  $this->db->update('tbl_candidate_registration', $tbl_candidate_dataset);
                  $mstuserdataset = array('IsDeleted' => 1);
                  $this->db->where('staffid', $content['staff_transaction']->staffid);
                  $this->db->update('mstuser', $mstuserdataset);
                  $this->db->where('staffid', $content['staff_transaction']->staffid);
                  $flag = $this->db->update('staff', $staff_update_data);
                  $this->db->where('id', $content['tbl_check_sheet']->id);
                  $flag = $this->db->update('tbl_da_check_sheet', $mergeddataset);
                }
                }
                else
                {
                  $data = array(
                  'staffid'=>$content['staff_transaction']->staffid,
                  'transid'=>$token,
                  'date_resignation_submitted_7'=>$this->gmodel->changedate($this->input->post('date_resignation_submitted_7')),
                  'date_from_which_separation_takes_effect_8'=>$this->gmodel->changedate($this->input->post('date_from_which_separation_takes_effect_8')),
                  'whether_under_probation_9'=>$this->input->post('whether_under_probation_9'),
                  'if_not_under_probation_10'=>$this->input->post('if_not_under_probation_10'),
                  'Vehicle_Loan'=>$this->input->post('Vehicle_Loan'),
                  'stipend_in_lieu_of_excess_leave'=>$this->input->post('stipend_in_lieu_of_excess_leave'),
                  'other_loans_and_advances'=>$this->input->post('other_loans_and_advances'),

                  'outstanding_dues'=>$this->input->post('outstanding_dues'),
                  'stipend'=>$this->input->post('stipend'),
                  'personal_claim'=>$this->input->post('personal_claim'),
                  'any_other'=>$this->input->post('any_other'),

                  'payments_due_to_employee'=> $this->input->post('payments_due_to_employee'),
                  'amounts_recoverable'=> $this->input->post('amounts_recoverable'),
                  'net_payable_recoverable'=>$this->input->post('net_payable_recoverable'),
                  'createdon'=>date('Y-m-d'),
                  'createdby'=>$this->loginData->staffid,
                  'flag'=>$db_flag,
                  'financeid'=>$this->input->post('curuserid'),
                  'financeapprovaldate'=>$this->gmodel->changedate($this->input->post('financeapprovaldate')),
              );
              /*echo "<pre>";
              print_r($data);exit();*/
              $flag=$this->db->insert('tbl_da_check_sheet', $data);
            }
          if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');} 
          else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');}
        }


              
          }
          // echo "<pre>";
          // print_r($data_tbl_check_sheet_transaction['head1']);
          // exit();
          $query = "SELECT a.*, b.name as financename, c.name as supervisername, d.name as hrname, e.officename as financeoffice, f.officename as superviseroffice, g.officename as hroffice FROM tbl_da_check_sheet as a 
              LEFT JOIN staff as b ON b.staffid = a.financeid
              LEFT JOIN staff as c ON c.staffid = a.superviserid
              LEFT JOIN staff as d ON d.staffid = a.hrdid
              LEFT JOIN lpooffice as e ON e.officeid = b.new_office_id
              LEFT JOIN lpooffice as f ON f.officeid = c.new_office_id
              LEFT JOIN lpooffice as g ON g.officeid = d.new_office_id 
              WHERE a.transid=".$token;
          $content['tbl_check_sheet'] = $this->db->query($query)->row(); 
          $query ="SELECT * FROM staff_transaction WHERE id=".$token;
          $content['staff_transaction'] = $this->db->query($query)->row();
          $superviserquery = "select receiver from tbl_workflowdetail where r_id=".$token." AND flag= 5";
          $content['superviserreceiver'] = $this->db->query($superviserquery)->row();
          /*echo "<pre>";
          print_r($content);exit;*/
          $content['title'] = 'Staff_sepchecksheet';
          $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
          $this->load->view('_main_layout', $content);
       
         }
        catch(Exception $e)
        {
          print_r($e->getMessage());
          die();
        }
            
      }

      
}