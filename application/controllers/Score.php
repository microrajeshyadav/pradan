<?php 
class Score extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Score_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');

		

	}

	public function index()
	{
		try{

		
		//$content['recruitercampusdetails'] = $this->model->getRecruitersCampus(); //// List of Recruiter Campus
		$content['recruitersdetails']   = $this->model->getRecruiters();
		$content['campusdetails'] 	    = $this->model->getCampus();
		$content['title'] 		= 'Score';
		$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}

	public function add(){

		try{
		
		$RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){
			

				$this->db->trans_start();
					
				$this->form_validation->set_rules('campusname','Campus Name','trim|required');
	            $this->form_validation->set_rules('dateofselecionprocess','Date Of Selection Process','trim|required');
	            $this->form_validation->set_rules('recruiters1','Recruiters 1','trim|required');
	            $this->form_validation->set_rules('recruiters2','Recruiters 2','trim|required');
	            $this->form_validation->set_rules('recruiters3','Recruiters 3','trim|required');
	          
	           
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
				
					$insertArr = array(
						'campusid'    		        => $this->input->post('campusname'),
						'dateofselecionprocess'     => $this->input->post('dateofselecionprocess'),
						'recruitersname1'    		=> $this->input->post('recruiters1'),
						'recruitersname2'       	=> $this->input->post("recruiters2"),
						'recruitersname3'  			=> $this->input->post('recruiters3'),
						'CreatedOn'      		    => date('Y-m-d H:i:s'),
					    'CreatedBy'      		    => $this->loginData->UserID, // login user id
					    'IsDeleted'      		    => 0, 
					  );
					
					$this->db->insert('mstrecruiters', $insertArr);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Recruiters');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added Recruiters');			
					}

				
					redirect('/Score/index');
			
			}


			prepareview:

			$content['recruitersdetails']   = $this->model->getRecruiters();
			$content['campusdetails'] 	    = $this->model->getCampus();
			$content['method']              = $this->router->fetch_method();
			$content['title']  = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


	public function edit($token){
		
		try{

				$RequestMethod = $this->input->server('REQUEST_METHOD');
			
				if($RequestMethod == 'POST'){
			

				$this->db->trans_start();
					
				$this->form_validation->set_rules('campusname','Campus Name','trim|required');
	            $this->form_validation->set_rules('dateofselecionprocess','Date Of Selection Process','trim|required');
	            $this->form_validation->set_rules('recruiters1','Recruiters 1','trim|required');
	            $this->form_validation->set_rules('recruiters2','Recruiters 2','trim|required');
	            $this->form_validation->set_rules('recruiters3','Recruiters 3','trim|required');
	          
	           
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
				
					$updateArr = array(
						'campusid'    		        => $this->input->post('campusname'),
						'dateofselecionprocess'     => $this->input->post('dateofselecionprocess'),
						'recruitersname1'    		=> $this->input->post('recruiters1'),
						'recruitersname2'       	=> $this->input->post("recruiters2"),
						'recruitersname3'  			=> $this->input->post('recruiters3'),
						'CreatedOn'      		    => date('Y-m-d H:i:s'),
					    'CreatedBy'      		    => $this->loginData->UserID, // login user id
					    'IsDeleted'      		    => 0, 
					  );
					
					$this->db->where('recruiterid', $token);
					$this->db->update('mstrecruiters', $updateArr);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error update Recruiters');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully update Recruiters');			
					}
			
					redirect('/Score/index');
			}
			
			prepareview:

			$content['recruitdetails']      = $this->model->getSingelRecruitersCampus($token);
			$content['recruitersdetails']   = $this->model->getRecruiters();
			$content['campusdetails'] 	    = $this->model->getCampus();
			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}
	}



	public function AddWrittenScore(){

		try{

				
		$RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){
			

				$this->db->trans_start();
					
				$this->form_validation->set_rules('writtenscore','Written Score ','trim|required|max_length[3]|numeric');
	          
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
				
					$insertArr = array(
						'writtenscore'    		        => $this->input->post('writtenscore'),
						'CreatedOn'      		    => date('Y-m-d H:i:s'),
					    'CreatedBy'      		    => $this->loginData->UserID, // login user id
					    'IsDeleted'      		    => 0, 
					  );
					
					$this->db->insert('mstrecruiters', $insertArr);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Written Score');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added  Written Score');			
					}

				
					redirect('/Score/index');
			
			}

			prepareview:

			//$content['recruitersdetails']   = $this->model->getRecruiters();
			//$content['campusdetails'] 	    = $this->model->getCampus();
			//$content['method']              = $this->router->fetch_method();
			$content['title']  = 'Score/addwrittenscore';
			$content['subview'] = 'Score/addwrittenscore';
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}



	public function AddGDScore(){

		try{
				
		$RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){
			

				$this->db->trans_start();
					
				$this->form_validation->set_rules('gdscore','GD Score ','trim|required|max_length[3]|numeric');
	          
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
				
					$insertArr = array(
						'gdscore'    		        => $this->input->post('gdscore'),
						'CreatedOn'      		    => date('Y-m-d H:i:s'),
					    'CreatedBy'      		    => $this->loginData->UserID, // login user id
					    'IsDeleted'      		    => 0, 
					  );
					
					$this->db->insert('mstrecruiters', $insertArr);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Written Score');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added  Written Score');			
					}
				
				redirect('/Score/index');
			
			}

			prepareview:

			//$content['recruitersdetails']   = $this->model->getRecruiters();
			//$content['campusdetails'] 	    = $this->model->getCampus();
			//$content['method']              = $this->router->fetch_method();
			$content['title']  = 'Score/addgdscore';
			$content['subview'] = 'Score/addgdscore';
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}





	public function AddHRScore(){

		try{
				
		$RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){

				$this->db->trans_start();
					
				$this->form_validation->set_rules('hrscore','HR Score ','trim|required|max_length[3]|numeric');
	          
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
				
					$insertArr = array(
						'hrscore'    		        => $this->input->post('hrscore'),
						'CreatedOn'      		    => date('Y-m-d H:i:s'),
					    'CreatedBy'      		    => $this->loginData->UserID, // login user id
					    'IsDeleted'      		    => 0, 
					  );
					
					$this->db->insert('mstrecruiters', $insertArr);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Written Score');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added  Written Score');			
					}

				
					redirect('/Score/index');
			
			}

			prepareview:
			$content['title']  = 'Score/addhrscore';
			$content['subview'] = 'Score/addhrscore';
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}

	
	function delete($token)
	{
		try{
		$this->Common_Model->delete_row('mstrecruiters','recruiterid', $token); 
		$this->session->set_flashdata('tr_msg' ,"Recruiters Deleted Successfully");
		redirect('/Score/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}


}