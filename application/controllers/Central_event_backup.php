<?php 

/**
* Central Event Controller List
*/
class Central_event extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');

    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model', 'model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }


   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
   try{
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //////////////// Select Batch ////////////////
   $query = "select * from mstbatch where status =0 AND isdeleted='0'";
   $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
   $query = "select * from mstphase where isdeleted='0'";
   $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
   $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
   $content['candidate_details']     = $this->Common_Model->query_data($query);

   $content['centralevent_details']  = $this->model->getCentralEvent();
   $content['event_details']         = $this->model->getEvent();

   $content['subview']="index";

   $content['title'] = 'Central_event';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }







 public function Add()
 {
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   
  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

  //print_r($this->input->post()); die;


    $this->db->trans_start();

   $btnsavedata =  $this->input->post('btnsevedata');
    $btnsendsevedata = $this->input->post('btnsendsevedata');
    if (!empty($btnsavedata) && $btnsavedata =='savedata') {


      $fromdate = $this->input->post('central_event_from_date');

      if (!empty($fromdate)) {
        $from_date = date('Y-m-d', strtotime($fromdate));
      }

      $todate = $this->input->post('central_event_to_date');

      if (!empty($todate)) {
       $to_date = date('Y-m-d', strtotime($todate));
      }

     
    $insertArr = array(
      'name'            => $this->input->post('central_event_name'),
      'from_date'       => $from_date,
      'to_date'         => $to_date,
      'place'           => $this->input->post('central_event_place'),
      'createdby'       => $this->loginData->UserID,
      'createdon'       => date('Y-m-d H:i:s'),
      'isdeleted'       => 0,
    );

    $this->db->insert('tbl_central_event', $insertArr);
  //echo $this->db->last_query(); die;
    $insertid = $this->db->insert_id();

    $countresouce_person =  count($this->input->post('resouce_person')); 
  

    for ($i=0; $i < $countresouce_person ; $i++) { 

      $staffid = $this->input->post('resouce_person')[$i];

      $insertArr_transaction = array(
        'central_event_id'     => $insertid,
        'resouce_person_id'  => $staffid,
        'send_email_status'    => 0,
      );
      
      $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid_transaction = $this->db->$this->insert_id();

     }

     $this->db->trans_complete();

      if ($this->db->trans_status() === true){
        $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');
        redirect('/Central_event/index/');
      }else{
        $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
        redirect('/Central_event/add');
      }
      
    }

     if (!empty($btnsendsevedata) && $btnsendsevedata =='sendsavedata') {


  
    $fromdate = $this->input->post('central_event_from_date');

      if (!empty($fromdate)) {
        $from_date = date('Y-m-d', strtotime($fromdate));
      }

      $todate = $this->input->post('central_event_to_date');

      if (!empty($todate)) {
       $to_date = date('Y-m-d', strtotime($todate));
      }

     
    $insertArr = array(
      'name'            => $this->input->post('central_event_name'),
      'from_date'       => $from_date,
      'to_date'         => $to_date,
      'place'           => $this->input->post('central_event_place'),
      'createdby'       => $this->loginData->UserID,
      'createdon'       => date('Y-m-d H:i:s'),
      'isdeleted'       => 0,
    );

    $this->db->insert('tbl_central_event', $insertArr);
  //echo $this->db->last_query(); die;
    $insertid = $this->db->insert_id();

    $countresouce_person =  count($this->input->post('resouce_person')); 
  

    for ($i=0; $i < $countresouce_person ; $i++) { 

      $staffid = $this->input->post('resouce_person')[$i];

      $insertArr_transaction = array(
        'central_event_id'     => $insertid,
        'resouce_person_id'  => $staffid,
        'send_email_status'    => 0,
      );
      
      $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid_transaction = $this->db->$this->insert_id();

   }

    $this->db->trans_complete();

    if ($this->db->trans_status() === true){
      $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');
      redirect('/Central_event/sendinvitation/'.$insertid);
    }else{
      $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
      redirect('/Central_event/add');
    }

  }

  }

 

  //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0'";
  $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
  $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
  $content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
   $content['resouceperson_details'] = $this->model->getResoucePerson();
   //////////////////// Select Event Name ////////////////////////////
   $content['event_details']         = $this->model->getEvent();

  $content['title']   = 'Central_event';
  $content['subview'] = 'Central_event/add';
  $this->load->view('_main_layout', $content);


  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}




public function sendinvitation($token)
{
    try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //echo $token; die;
    
    $query = $this->db->query('SELECT * FROM tbl_central_event_transaction WHERE central_event_id = '.$token.'');
   $count_transaction =  $query->num_rows();

    $sql = " SELECT id,`resouce_person_id` FROM `tbl_central_event_transaction` WHERE `central_event_id`=$token ";
      $result = $this->db->query($sql)->result();
     // print_r($result);

      $eventdetails  = $this->model->getSingleCentralEvent($token);
      $event_name = $eventdetails[0]->name;
      $event_from_date = $eventdetails[0]->from_date;
      $event_to_date = $eventdetails[0]->to_date;
      $event_place = $eventdetails[0]->place;
   
  for ($i=0; $i < $count_transaction; $i++) { 

     $staffid = $result[$i]->resouce_person_id;
     $id_transaction = $result[$i]->id;

    $staffdetails  = $this->model->getStaffDetails($staffid);

   // print_r($staffdetails);

     // $staffemailid = $staffdetails[0]->emailid; //// get Email id staff
      $staffemailid = 'amit.kum2008@gmail.com'; //// get Email id staff
      $staffname    = $staffdetails[0]->name;      //// get Name staff

     $html = 'Dear '.$staffname.', <br><br> ';
     $html .= '<b>Center Evant - </b> '.$event_name.',<br>';
     $html .= '<b>From Date -  </b> '.$event_from_date.',<br>';
     $html .= '<b>TO Date -  </b> '.$event_to_date.',<br>';
     $html .= '<b>Place -  </b> '.$event_place.',<br>';

     $sendmail = $this->Common_Model->send_email($subject = 'Central Evavnt Invitation', $message = $html, $staffemailid);  //// Send Mail Resource Person ////
         
        if ($sendmail) {

           $updateArr_transaction = array(
           'send_email_status' => 1,
      );
      $this->db->where('id',$id_transaction);
      $this->db->update('tbl_central_event_transaction', $updateArr_transaction);
      //echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully Add & Send Central Event  !!!');
      }
  
  }
 redirect('/Central_event/index');   


 //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0'";
  $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
  $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
  $content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
   $content['resouceperson_details'] = $this->model->getResoucePerson();
    ////////////////////// Select getResoucePerson ////////////////////////
   $singleresouceperson_details1 = $this->model->getSingleCentralEvent($token);
  //
  
 //  print_r($singleresouceperson_details1); die;
  
   $content['singleresouceperson_details'] = $singleresouceperson_details1[0];

    $temp_array = [];
   $resouceperson_list = $this->model->getSingleResoucePerson($token);
  foreach ($resouceperson_list as $mrow) {
    $temp_array[] = $mrow->resouce_person_id;
  }
  $content['map_resouce_person_array'] = $temp_array;

   //////////////////// Select Event Name ////////////////////////////
   $content['event_details']         = $this->model->getEvent();



  $content['title'] = 'DA_event_detailing';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch(Exception $e){
      print_r($e->getMessage());die();
  }

}


public function edit($token)
{
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

    $this->db->trans_start();

     $btnsavedata =  $this->input->post('btnsevedata');
    $btnsendsevedata = $this->input->post('btnsendsevedata');

    if (!empty($btnsavedata) && $btnsavedata =='savedata') {


      $fromdate = $this->input->post('central_event_from_date');

      if (!empty($fromdate)) {
        $from_date = date('Y-m-d', strtotime($fromdate));
      }

      $todate = $this->input->post('central_event_to_date');

      if (!empty($todate)) {
       $to_date = date('Y-m-d', strtotime($todate));
      }

     
    $insertArr = array(
      'name'            => $this->input->post('central_event_name'),
      'from_date'       => $from_date,
      'to_date'         => $to_date,
      'place'           => $this->input->post('central_event_place'),
      'createdby'       => $this->loginData->UserID,
      'createdon'       => date('Y-m-d H:i:s'),
      'isdeleted'       => 0,
    );
    $this->db->where('id', $token);
    $this->db->update('tbl_central_event', $insertArr);
  
    $this->db->delete('tbl_central_event_transaction', array('central_event_id' => $token));

    $countresouce_person =  count($this->input->post('resouce_person')); 
  

    for ($i=0; $i < $countresouce_person ; $i++) { 

      $staffid = $this->input->post('resouce_person')[$i];

      $insertArr_transaction = array(
        'central_event_id'     => $token,
        'resouce_person_id'  => $staffid,
        'send_email_status'    => 0,
      );
      
      $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
     

     }


     $this->db->trans_complete();

      if ($this->db->trans_status() === true){
        $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');
        redirect('/Central_event/index/');
      }else{
        $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
        redirect('/Central_event/add');
      }
      
    }

     if (!empty($btnsendsevedata) && $btnsendsevedata =='sendsavedata') {

      //print_r($this->input->post()); die;
  
    $fromdate = $this->input->post('central_event_from_date');

      if (!empty($fromdate)) {
        $from_date = date('Y-m-d', strtotime($fromdate));
      }

      $todate = $this->input->post('central_event_to_date');

      if (!empty($todate)) {
       $to_date = date('Y-m-d', strtotime($todate));
      }

     
    $insertArr = array(
      'name'            => $this->input->post('central_event_name'),
      'from_date'       => $from_date,
      'to_date'         => $to_date,
      'place'           => $this->input->post('central_event_place'),
      'createdby'       => $this->loginData->UserID,
      'createdon'       => date('Y-m-d H:i:s'),
      'isdeleted'       => 0,
    );
    $this->db->where('id', $token);
    $this->db->update('tbl_central_event', $insertArr);
  //echo $this->db->last_query(); die;
   // $insertid = $this->db->insert_id();

     $this->db->delete('tbl_central_event_transaction', array('central_event_id' => $token));

    $countresouce_person =  count($this->input->post('resouce_person')); 
  

    for ($i=0; $i < $countresouce_person ; $i++) { 

     $staffid = $this->input->post('resouce_person')[$i];

      $insertArr_transaction = array(
        'central_event_id'     => $token,
        'resouce_person_id'    => $staffid,
        'send_email_status'    => 0,
      );
      
      $this->db->insert('tbl_central_event_transaction', $insertArr_transaction);
      //$insertid_transaction = $this->db->$this->insert_id();

   }

    $this->db->trans_complete();

    if ($this->db->trans_status() === true){
      $this->session->set_flashdata('tr_msg', 'Successfully Add Central Event  !!!');
      redirect('/Central_event/sendinvitation/'.$token);
    }else{
      $this->session->set_flashdata('er_msg', 'Error Adding Central Event  !!!');
      redirect('/Central_event/add');
    }

  }
  }

  prepareview:

  //////////////// Select Batch ////////////////
  $query = "select * from mstbatch where status =0 AND isdeleted='0'";
  $content['batch_details'] = $this->Common_Model->query_data($query);

    //////////////// Select Phase ////////////////
  $query = "select * from mstphase where isdeleted='0'";
  $content['phase_details'] = $this->Common_Model->query_data($query);

     //////////////// Select Phase ////////////////
  $query = "select `tblcr`.candidateid, `tblcr`.candidatefirstname,`tblcr`.candidatemiddlename,`tblcr`.candidatelastname from  tbl_candidate_registration as `tblcr` where joinstatus=1";
  $content['candidate_details'] = $this->Common_Model->query_data($query);

  ////////////////////// Select getResoucePerson ////////////////////////
   $content['resouceperson_details'] = $this->model->getResoucePerson();
    ////////////////////// Select getResoucePerson ////////////////////////
   $singleresouceperson_details1 = $this->model->getSingleCentralEvent($token);
  //
  
 //  print_r($singleresouceperson_details1); die;
  
   $content['singleresouceperson_details'] = $singleresouceperson_details1[0];

    $temp_array = [];
   $resouceperson_list = $this->model->getSingleResoucePerson($token);
  foreach ($resouceperson_list as $mrow) {
    $temp_array[] = $mrow->resouce_person_id;
  }
  $content['map_resouce_person_array'] = $temp_array;

   //////////////////// Select Event Name ////////////////////////////
   $content['event_details']         = $this->model->getEvent();



  $content['title'] = 'DA_event_detailing';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



function getDAEventDetails(){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
       LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE `tbl_da_event_detailing`.`isdeleted`='0' ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function getSingleDAEventDetails($token){

  try{
    $sql = "SELECT `tbl_da_event_detailing`.id, `mstbatch`.batch, `mstbatch`.financial_year, `mstphase`.phase_name FROM `tbl_da_event_detailing`
       LEFT  join  `mstbatch` on `tbl_da_event_detailing`.batchid =`mstbatch`.id
    LEFT  join  `mstphase` on `tbl_da_event_detailing`.phaseid =`mstphase`.id
    WHERE  `tbl_da_event_detailing`.`id`='0' AND `tbl_da_event_detailing`.`isdeleted`=$token ORDER BY `id` DESC";
    $res = $this->db->query($sql)->result();

    return $res;

  }catch(Exception $e){
    print_r($e->getMessage());die();
  }

}



function delete($token)
{
 try{
 $this->Common_Model->delete_row('tbl_central_event_transaction','central_event_id', $token); 
$this->Common_Model->delete_row('tbl_central_event','id', $token);
$this->session->set_flashdata('tr_msg' ,"Central Event Deleted Successfully");
  redirect('/Central_event/index/');

  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

}