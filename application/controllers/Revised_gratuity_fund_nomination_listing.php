
<?php 

/**
* State List
*/
class Revised_gratuity_fund_nomination_listing extends CI_controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->library('form_validation');
    $this->load->model("Revised_gratuity_listing_model","model");

    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
         

    $check = $this->session->userdata('login_data');

    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');

 }

 public function index()
 {
 	
 	

    // start permissi`on 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
   //$content['officelist'] = $this->Staff_list_model->getOfficeList();

  // end permission 

   //$this->load->model("Revised_provident_fund_nomination_listing_model","model");

$content['staff_details'] = $this->model->getstaffname();
//print_r($content['staff_details']);
   $RequestMethod = $this->input->server("REQUEST_METHOD");

 prepareview:
   $content['title'] = 'Revised_gratuity_fund_nomination_listing';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);





 }

 public function stfflistdectivated($token)
 {


  $this->load->model('Staff_list_model');
  $fetch=$this->Staff_list_model->fetchdatas($token);


  if($fetch==-1)
  {
    $this->session->set_flashdata('er_msg', 'Sorry');

  }
  else
  {

    $updateArrs = array(
      'status'    => 0,
    );
    $this->db->where('staffid', $token);
    $this->db->update('staff', $updateArrs);
    $this->session->set_flashdata('tr_msg', 'Staff is deactivated');
  }



  redirect('/Staff_list/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  
} 


}