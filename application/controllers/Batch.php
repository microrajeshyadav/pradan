<?php 
class Batch extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
     		 redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{
		//index include
		 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		$this->load->model('Batch_model');
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
			 $month     = $this->input->post('month');
			 $exp       = explode('/',$month);
			 $MonthYear = $exp[1].'-'.$exp[0];
			// $this->session->set_userdata("month",$this->input->post('month'));
		}

		//$content['usedbatch'] = $this->model->UsedBatch();
		$content['batchlist'] = $this->model->getBatch();

		//$usedbatch = $this->model->UsedBatch();

		$content['title'] = 'Batch';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	
	public function add(){
				 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
					$this->db->trans_start();

				$this->form_validation->set_rules('batch','Batch','trim|required|min_length[1]|max_length[2]|numeric|is_unique[mstbatch.batch]');

	            $this->form_validation->set_rules('dateofjoining','Date of joining','trim|required');
	            $this->form_validation->set_rules('financialyear','Current Financial Year','trim|required');
	            
	            $this->form_validation->set_rules('status','Status','trim|required');

            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }

				    $date_of_join = $this->input->post("dateofjoining");

				   $date_of_joindate =  $this->model->changedatedbformate($date_of_join);
				    //$date_of_joindate = date('Y-m-d', strtotime($date_of_join));
					$insertArrBatch = array(
						'batch'    			=> $this->input->post('batch'),
						'financial_year' => $this->input->post('financialyear'),
						'dateofjoining'    	=> $date_of_joindate,
						'stage'  	 => $this->input->post('stage'),
						'status'  	 => $this->input->post('status'),
						'createdon'  => date('Y-m-d H:i:s'),
					    'createdby'  =>$this->loginData->UserID, // login user id
					    'isdeleted'  => 0, 
					  );
					$this->Common_Model->insert_data('mstbatch', $insertArrBatch);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Batch');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Batch added successfully.');			
					}
					redirect('/Batch/index');
			
			}

			prepareview:

			//$content['getfinancialyear'] = $this->fiscalYear();
			$content['getfinancialyear'] = $this->model->getfinancialyear();
	  		$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


	public function edit($token){
				 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		
		try{

			
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
					$this->db->trans_start();

				$this->form_validation->set_rules('batch','Batch','trim|required|min_length[1]|max_length[2]|numeric');

	            $this->form_validation->set_rules('dateofjoining','Date of joining','trim|required');
	            $this->form_validation->set_rules('status','Status','trim|required');

            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }
	             $date_of_join = $this->input->post("dateofjoining");
				// $date_of_joindate = date('Y-m-d', strtotime($date_of_join));
	             $date_of_joindate =  $this->model->changedatedbformate($date_of_join);


					$updateArray = array(
						'batch '    			=> $this->input->post('batch'),
						'financial_year '  => $this->input->post('financialyear'),
						'dateofjoining'       	=> $date_of_joindate,
						'stage'  	 => $this->input->post('stage'),
						'status'  			    => $this->input->post('status'),
						'createdon'      	    => date('Y-m-d H:i:s'),
					    'createdby'      	    => 2, // login user id
					    'isdeleted'      	    => 0, 
					  );

					$this->db->where("id",$token);
			        $this->db->update('mstbatch', $updateArray);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error update Batch');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Batch updated successfully.');			
					}
					redirect('/Batch/index');
			
			}

			prepareview:
			//$content['getfinancialyear'] = $this->fiscalYear();
			$content['getfinancialyear'] = $this->model->getfinancialyear();
			$query = " SELECT *, case when stage = 1 then 'Will Join' when stage = 2 then 'Ongoing' when stage = 3  then 'Gratuated'  else 'Will Join' end batchstage FROM `mstbatch` as a  
			inner join `mstfinancialyear` as b ON `b`.id = `a`.financial_year
                  WHERE a.`isdeleted`='0' and `a`.id=".$token."";
			$content['batchupdate'] = $this->Common_Model->query_data($query);

			$content['title'] = 'edit.php';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


/////// Get Current Financial Year ///////////////////

	function fiscalYear()
    {
       
try{
		if (date('m') > 6) {
		$year = date('Y')."-".(date('Y') +1);
		}
		else {
		$year = (date('Y')-1)."-".date('Y');
		}
		return $year;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
    }



	function delete($token = null)
	{
		try{
		
		$updatearray = array(
			'isdeleted' => 1,
		);
		
		$this->db->where('id', $token);
		$this->db->update('mstbatch',$updatearray);
 		$this->session->set_flashdata('tr_msg' ,"Batch Deleted Successfully");
		redirect('/Batch/index/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


}