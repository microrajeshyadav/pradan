<?php 

/**
* State List
*/
class Gratuity_nomination_form_staff extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    $this->load->model("General_nomination_and_authorisation_form_model");
    $this->load->model("Provident_fund_nomination_form_model");
    $this->load->model("Employee_particular_form_model");
    $this->load->model("Gratuity_nomination_form_model");
     $this->load->model("Provident_fund_nomination_form_staff_model");

    
    $check = $this->session->userdata('login_data');
     $this->load->model('Medical_certificate_model');

    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
    /* echo "<pre>";
     print_r($this->loginData);exit();*/
  }

  public function index($staff=null)
  {
    try{
    $this->session->set_userdata('staff', $staff);
    $staff_id=$this->loginData->staffid;
    if(isset($staff_id))
    {
      $staff_id=$staff_id;
    }
    else
    {
       $staff_id=$staff;
    }

    //getCandidateWithAddressDetails($token)

    $content['candidatedetailwithaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
    // $content['candidateaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
    $content['candidateaddress'] = $this->Provident_fund_nomination_form_model->getCandidateWithAddressDetails($staff_id);
    $content['report']=$this->Medical_certificate_model->staff_reportingto($staff_id);
    $content['personnal_mail'] = $this->Provident_fund_nomination_form_staff_model->personal_email();
    // print_r($content['personnal_mail']);
    $personal_email=$content['personnal_mail']->EmailID;
    // die;

    //print_r($content['candidatedetailwithaddress']);

    $reportingto = $content['candidateaddress']->reportingto;
    // print_r($reportingto);
    // die;
    $content['tc_email'] = $this->Provident_fund_nomination_form_staff_model->tc_email($reportingto);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == "POST"){
      $Sendsavebtn = $this->input->post('savebtn');
      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
        //echo "image=".$updateArr['signatureplace_encrypted_filename'];
        //die;
          
        $dadate = $this->input->post('dadate');

        $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);
          $m_date = date("Y-m-d");


        $insertarraydata = array(
          'staff_id'=>$staff_id,

        'candidate_id'             => 0,

        'place'              => $this->input->post('daplace'),


        'date'               => $dadate,
        'isupdate'                => 0,
         'type'=>'m',
        'm_date'=>$m_date, 
        'status'=>0



        );

        $this->db->insert('tbl_graduitynomination', $insertarraydata);
        //echo $this->db->last_query();
        //die;
        $insertid = $this->db->insert_id();
        $data1=$this->input->post('data');

        foreach($data1 as $value)
        {
          if ($value['name'] != '') {
          $arr=array('sr_no'=>$value['sr_no'],
            'name'=>$value['name'],
            'graduity_id'=>$insertid,
            'relation_id'=>$value['relationship_nominee'],
            'age'=>$value['age_nominee'],
            'address'=>$value['address_nominee'],
            'share_nominee'=>$value['share_nominee'],
            'minior'=>$value['minior'],
            'type'=>'m',
            'm_date'=>$m_date

            );

          $this->db->insert('tbl_graduitynomination_detail', $arr);
        }
      }
        

      
      $insert_data =array(
        'type'=>24,
        'r_id'=> $insertid,
        'sender'=> $staff_id,
        'receiver'=>$content['report']->reportingto,
        'senddate'=>date('Y-m-d'),
        'createdon'=>date('Y-m-d H:i:s'),
        'createdby'=>$this->loginData->UserID,
        'flag'=>1,
        'staffid'=>$staff_id
      );
      $this->db->insert('tbl_workflowdetail', $insert_data);
      // print_r($content['candidateaddress']);
      // die;

      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

      }else{
        
        $subject = ': Gratuity Fund Nomination';
        $body = '<h4>'.$content['candidatedetailwithaddress']->staff_name.' submited Gratuity fund Nomination Form </h4><br />';
        $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
        <tr>
        <td width="96">Name </td>
        <td width="404">'.$content['candidatedetailwithaddress']->staff_name.'</td>
        </tr>
        <tr>
        <td>Employ Code</td>
        <td> '.$content['candidatedetailwithaddress']->emp_code.'</td>
        </tr>
        <tr>
        <td>Designation</td>
        <td>' .$content['candidatedetailwithaddress']->desiname.'</td>
        </tr>
        <tr>
        <td>Office</td>
        <td>'.$content['candidatedetailwithaddress']->officename.'</td>
        </tr>
        </table>';
        $body .= "<br /> <br />";
        $body .= "Regards <br />";
        $body .= " ". $content['candidatedetailwithaddress']->staff_name ."<br>";
        $body .= " ". $content['candidatedetailwithaddress']->desiname."<br>";
        $body .= "<b> Thanks </b><br>";
       
           

        // $to_useremail = 'pdhamija2012@gmail.com';
        $to_useremail = $content['candidatedetailwithaddress']->emailid;
        $tcemailid=$content['tc_email']->emailid;

        
        $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

        // $to_hremail = $hremail->EmailID;
        $this->Common_Model->midemreview_send_email($subject, $body, $to_useremail);
        // die("hello");
        
        $this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form'); 

        redirect('Gratuity_nomination_form_staff/edit/'.$staff_id.'/'.$insertid);    
      }
    }

    $submitdatasend = $this->input->post('submitbtn');

    if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {

      
       /*if ($_FILES['signatureplace']['name'] != NULL) {
            $data=$_FILES['signatureplace'];
                $this->load->model('Membership_applicatioin_from_model');

             $fileDatasa=$this->Membership_applicatioin_from_model->do_uploadd($data);

            $updateArr['signatureplace'] = $fileDatasa['orig_name'];
               $updateArr['signatureplace_encrypted_filename'] = $fileDatasa['file_name'];

         }*/


          //echo "image=".$updateArr['signatureplace_encrypted_filename'];
          //die;
          
      $dadate = $this->input->post('dadate');
      
        $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);

        
      $m_date = date("Y-m-d");
      $insertarraydata = array(
        'staff_id'=>$staff_id,
        'candidate_id'             => 0,
        'place'              => $this->input->post('daplace'),
        /* 'signature'   => $updateArr['signatureplace_encrypted_filename'],*/
        'date'               => $dadate,
        'isupdate'                => 0,
        'type'=>'m',
        'm_date'=>$m_date,
        'status'=>1
      );
        
      $this->db->insert('tbl_graduitynomination', $insertarraydata);
      //echo $this->db->last_query();
      //die;
      $insertid = $this->db->insert_id();
      $data1=$this->input->post('data');
        
        foreach($data1 as $value)
        {
      if ($value['name'] != '') {

          $arr=array('sr_no'=>$value['sr_no'],
            'name'=>$value['name'],
            'graduity_id'=>$insertid,
            'relation_id'=>$value['relationship_nominee'],
            'age'=>$value['age_nominee'],
            'address'=>$value['address_nominee'],
            'share_nominee'=>$value['share_nominee'],
            'minior'=>$value['minior'],
            'type'=>'m',
        'm_date'=>$m_date

            );

          $this->db->insert('tbl_graduitynomination_detail', $arr);
        }
      }
        

      
        $insert_data =array(
          'type'=>24,
          'r_id'=> $insertid,
          'sender'=> $staff_id,
          'receiver'=>$content['report']->reportingto,
          'senddate'=>date('Y-m-d'),
          'createdon'=>date('Y-m-d H:i:s'),
          'createdby'=>$this->loginData->UserID,
          'flag'=>1,
          'staffid'=>$staff_id
        );
        $this->db->insert('tbl_workflowdetail', $insert_data);

      

      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

      }else{
        

        $subject = ': Provident Fund Nomination';
        $body = '<h4>'.$content['candidatedetailwithaddress']->staff_name.' submited Provident fund Nomination Form </h4><br />';
        $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
        <tr>
        <td width="96">Name </td>
        <td width="404">'.$content['candidatedetailwithaddress']->staff_name.'</td>
        </tr>
        <tr>
        <td>Employ Code</td>
        <td> '.$content['candidatedetailwithaddress']->emp_code.'</td>
        </tr>
        <tr>
        <td>Designation</td>
        <td>' .$content['candidatedetailwithaddress']->desiname.'</td>
        </tr>
        <tr>
        <td>Office</td>
        <td>'.$content['candidatedetailwithaddress']->officename.'</td>
        </tr>
        </table>';
        $body .= "<br /> <br />";
        $body .= "Regards <br />";
        $body .= " ". $content['candidatedetailwithaddress']->staff_name ."<br>";
        $body .= " ". $content['candidatedetailwithaddress']->desiname."<br>";
        $body .= "<b> Thanks </b><br>";
       
           

       // $to_useremail = 'pdhamija2012@gmail.com';
         $to_useremail = $content['candidatedetailwithaddress']->emailid;
         $tcemailid=$content['tc_email']->emailid;

        
         $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
        $this->Common_Model->midemreview_send_email($subject, $body, $to_useremail);
         // die("hello");
        
        
        $this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form'); 

        redirect('Gratuity_nomination_form_staff/view/'.$staff_id.'/'.$insertid);    
      }
  }

}

 
//     $content['topbar'] = $this->Gratuity_nomination_form_staff_model->do_flag($staff_id);
      
//        $var=$content['topbar']->graduity_flag;
 
// if ($var==null)
//   {
//     goto preview;
//    // redirect('/Gratuity_nomination_form/index/'.$staff.'/'.$candidateid);
//   }
   

//   if($var==0)
//   {
//     redirect('/Gratuity_nomination_form_staff/edit/'.$$staff_id);
//   }
//   elseif($var==1)
//   {
//     redirect('/Gratuity_nomination_form_staff/view/'.$staff_id);
//   }

  

    

    // die("hello");
    // end permission 
    preview:
    $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();



    $content['candidatedetailwithaddress'] = $this->Gratuity_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);
    //s($content['candidatedetailwithaddress']);

    //$content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport( $candidateid);
    // $content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);

    // $content['witnessdeclaration'] = $this->General_nomination_and_authorisation_form_model->getWitnessDeclaration($candidateid);
    $content['office_name'] = $this->General_nomination_and_authorisation_form_model->office_name();
    // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();

    $query = "select * from mstpgeducation where isdeleted='0'";
    $content['mstpgeducation_details'] = $this->Common_Model->query_data($query);
    //$content['subview']="index";
    $content['title'] = 'Gratuity_nomination_form_staff';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function edit($staff=null,$inserted=null)
  {
    try{
      $staff = $this->uri->segment(3);
   

            $this->session->set_userdata('staff', $staff);
            



      



      $staff_id=$this->loginData->staffid;
     
    
      if(isset($staff_id))
      {
        
        $staff_id=$staff_id;
       // echo "staff id".$staff_id;
       
        //echo "candate id=".$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff;
        
        // echo "candidateid=".$candidateid;
        
      }

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == "POST"){
      // print_r($this->input->post()); die();

      $savesenddata = $this->input->post('savebtn');

      if (!empty($savesenddata) && $savesenddata =='senddatasave') {


        
           //echo "image=".$updateArr['signatureplace_encrypted_filename'];
          // die;
        $dadate = $this->input->post('dadate');

           $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);
          
            $m_date = date("Y-m-d");

      $updatearraydata = array(
              'staff_id'=>$staff_id,

                'candidate_id'             => 0,
        
        'place'              => $this->input->post('daplace'),
        
        
        'date'               => $dadate,
        'isupdate'                => 0,
        'status'=>0
        
        
        
      );
      $this->db->where('graduity_id', $inserted);
      $this->db->update('tbl_graduitynomination', $updatearraydata);
     // echo $this->db->last_query();
      //die;
      //$countnominee = count($this->input->post('full_name_nominee'));


    $data1=$this->input->post('data');
    //print_r($data1);
    //die;

   foreach($data1 as $key => $value)
   {
    $id =  $value['graduity_id']; 
    $this->db->where('graduity_id', $id);
      $this->db->delete('tbl_graduitynomination_detail');
   }

    foreach($data1 as $key => $value)
    {
      if ($value['full_name_nominee'] != '') {

          $arr=array(
            
            'name'=>$value['full_name_nominee'],
            'sr_no'=>$key,
            'graduity_id'=>$value['graduity_id'],
            'relation_id'=>$value['relationship_nominee'],
            'age'=>$value['age_nominee'],
            'address'=>$value['address_nominee'],
            'share_nominee'=>$value['share_nominee'],
            'minior'=>$value['minior'],
             'type'=>'m',
        'm_date'=>$m_date

            );
          //print_r($arr);
       
          // extract($arr);
          //echo $graduity_id;
            //die;
          //die;
          // $this->db->where('id', $id);
          $this->db->insert('tbl_graduitynomination_detail', $arr);
         // echo $this->db->last_query();
          //die;
          }
        }
      //  die;
      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

      }else{
        
    $this->session->set_flashdata('tr_msg', 'Successfully save  DA general nomination and authorisation form'); 
    
      }
      redirect('Gratuity_nomination_form_staff/edit/'.$staff.'/'.$inserted);   
      }

    
      $submitsenddata = $this->input->post('submitbtn');

    if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

     


        $dadate = $this->input->post('dadate');

           $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);
          


      $updatearraydata = array(
            'staff_id'=>$staff_id,

                'candidate_id'             => $candidateid,
        
        'place'              => $this->input->post('daplace'),
        
        'date'               => $dadate,
        'isupdate'                => 0,
        'status'=>1
        
        
      );
      $this->db->where('graduity_id', $inserted);
      $this->db->update('tbl_graduitynomination', $updatearraydata);
     // echo $this->db->last_query();
      //die;
      //$countnominee = count($this->input->post('full_name_nominee'));


    $data1=$this->input->post('data');
    //print_r($data1);
    //die;
   foreach($data1 as $key => $value)
   {
    $id =  $value['graduity_id']; 
    $this->db->where('graduity_id', $id);
      $this->db->delete('tbl_graduitynomination_detail');
   }

    foreach($data1 as $key => $value)
    {
      if ($value['full_name_nominee'] != '') {

          $arr=array(
            
            'name'=>$value['full_name_nominee'],
            'sr_no'=>$key,
            'graduity_id'=>$value['graduity_id'],
            'relation_id'=>$value['relationship_nominee'],
            'age'=>$value['age_nominee'],
            'address'=>$value['address_nominee'],
            'share_nominee'=>$value['share_nominee'],
            'minior'=>$value['minior'],
             'type'=>'m',
        'm_date'=>$m_date

            );
          //print_r($arr);
       
          // extract($arr);
          //echo $graduity_id;
            //die;
          //die;
          // $this->db->where('id', $id);
          $this->db->insert('tbl_graduitynomination_detail', $arr);
         // echo $this->db->last_query();
          //die;
          }
        }
      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! DA general nomination and authorisation form');  

      }

      /*else{

        // $hrdemailid    = 'poonamadlekha@pradan.net';
         $hrdemailid    = 'amit.kum2008@gmail.com';
         $tcemailid     = $this->loginData->EmailID;

         $subject = "Submit Gereral Nomination And Authorisation Form ";
          $body =   'Dear Sir, <br><br> ';
          $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
         //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
         $to_email = $hrdemailid;
         $to_name = $tcemailid;
        
        $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/
        
        $this->session->set_flashdata('tr_msg', 'Successfully save and submit DA general nomination and authorisation form'); 

        redirect('Gratuity_nomination_form_staff/view/'.$staff.'/'.$inserted);    
      


  }


  }


    $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();
    $content['candidatedetailwithaddress'] = $this->Gratuity_nomination_form_staff_model->getCandidateWithAddressDetails($staff_id);

   // $content['genenominformdetail'] = $this->General_nomination_and_authorisation_form_model->getGeneralnominationform($candidateid);
   // print_r($content['genenominformdetail']);
//die("dhgdhs");
    $content['nominee'] =$this->Gratuity_nomination_form_staff_model->get_pfinformation($inserted);
  // echo "<pre>";
   //print_r($content['nominee']);
    //die;
 
        //die('hdghds');
    $content['nomineedetail'] = $this->Gratuity_nomination_form_staff_model->getNomineedetail($inserted);
    //echo "<pre>";
  //print_r($content['nomineedetail']);
  
    
    
    

    //$content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
    //$content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);
    //print_r($content['getgeneralform']);
    //$content['witnessdeclaration'] = $this->General_nomination_and_authorisation_form_model->getWitnessDeclaration($candidateid);


    $content['title'] = 'Gratuity_nomination_form_staff/edit';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }



   public function view($staff=null,$inserted=null)
   {
    try{
    $staff = $this->uri->segment(3);
   

    $this->session->set_userdata('staff', $staff);






    $login_staff =$this->loginData->staffid;
    $staff_id    =$this->loginData->staffid;

    $content['id'] = $this->Gratuity_nomination_form_staff_model->get_gratuityworkflowid($staff);
    $content['staff_details'] = $this->Gratuity_nomination_form_model->staffName($staff,$login_staff);
    $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();
    $content['candidatedetailwithaddress'] = $this->Gratuity_nomination_form_staff_model->getCandidateWithAddressDetails($staff);
    $content['nominee'] =$this->Gratuity_nomination_form_staff_model->get_pfinformation($inserted);
    /*echo "<pre>";
    print_r($content['id']);die;*/
    

        
        
    $content['nomineedetail'] = $this->Gratuity_nomination_form_staff_model->getNomineedetail($inserted);


    $content['personal']=$this->Common_Model->get_Personnal_Staff_List();
                           
                            if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
                            {
                                 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


                             if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
                                $status=$this->input->post('status');
                                $reject=$this->input->post('reject');
                                $p_status=$this->input->post('p_status');
                                $wdtaff1=$this->input->post('wdtaff1');
                                $wdtaff2=$this->input->post('wdtaff2');
                                $address1=$this->input->post('w_add1');
                                $address2=$this->input->post('w_add2');
                                $r_id=$this->input->post('id');
                                
                                $check = $this->session->userdata('insert_id');
                              


                                $insert_data =array(
                                  'type'=>24,
                                  'r_id'=> $r_id,
                                  'sender'=> $login_staff,
                                  'receiver'=>$p_status,
                                  'senddate'=>date('Y-m-d'),
                                  'createdon'=>date('Y-m-d H:i:s'),
                                  'createdby'=>$login_staff,
                                  'scomments'=>$reject,
                                  'forwarded_workflowid'=>$content['id']->workflow_id,
                                  'flag'=> $status,
                                  'staffid'=>$staff
                                );
           
          $arr_data=array('witness1'=>$wdtaff1,'witness2'=>$wdtaff2, 'address1'=> $address1,'address2'=>$address2); 
         
       // print_r($insert_data);
        //die();

                               
        $this->db->insert('tbl_workflowdetail', $insert_data);
       
        $this->db->where('graduity_id',$r_id);
        $this->db->update('tbl_graduitynomination',$arr_data);
        
     
                                
                             }


                            }
                            if($this->loginData->RoleID==17)
                            {
                                 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


                             if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
                                $status=$this->input->post('status');
                                $reject=$this->input->post('reject');
                                /*$p_status=$this->input->post('p_status');*/
                                $wdtaff1=$this->input->post('wdtaff1');
                                $wdtaff2=$this->input->post('wdtaff2');
                                $r_id=$this->input->post('id');
                                
                                
                              


              $insert_data =array(
              'type'=>24,
              'r_id'=> $r_id,
              'sender'=> $login_staff,
              'receiver'=>$staff,
              'senddate'=>date('Y-m-d'),
              'createdon'=>date('Y-m-d H:i:s'),
              'createdby'=>$login_staff,
              'scomments'=>$reject,
              'forwarded_workflowid'=>$content['id']->workflow_id,
              'flag'=> $status,
              'staffid'=>$staff
              );
           
          $arr_data=array(
            'personal_date'=>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('personal_date')),
            'personal_place'=>$this->input->post('personal_place'),
            'personal_name'=>$this->loginData->UserFirstName.' '.$this->loginData->UserLastName,

            ); 
         
         
       // print_r($insert_data);
        //die();

                               
        $this->db->insert('tbl_workflowdetail', $insert_data);
       
        $this->db->where('graduity_id',$r_id);
        $this->db->update('tbl_graduitynomination',$arr_data);
        
     
                                
      }


    }


    
   // print_r($content['nomineedetail']);

    
    //$content['nomineedetail'] = $this->General_nomination_and_authorisation_form_model->getNomineedetail($getGeneralnomination->id);
   // $content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
    //$content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);
    /*echo "<pre>";
    print_r($content);exit();*/
    if($content['id']->flag == 2){
      $resquery1 = "select staff.name, tbl_graduitynomination.address1 from staff left join tbl_graduitynomination on tbl_graduitynomination.witness1=staff.staffid where tbl_graduitynomination.graduity_id =".$inserted;
      $content['witness1result'] = $this->db->query($resquery1)->row();
      $resquery2 = "select staff.name, tbl_graduitynomination.address2 from staff left join tbl_graduitynomination on tbl_graduitynomination.witness2=staff.staffid where tbl_graduitynomination.graduity_id =".$inserted;
      $content['witness2result'] = $this->db->query($resquery2)->row();
    }else if($content['id']->flag == 4){
      $resquery1 = "select staff.name, tbl_graduitynomination.address1 from staff left join tbl_graduitynomination on tbl_graduitynomination.witness1=staff.staffid where tbl_graduitynomination.graduity_id =".$inserted;
      $content['witness1result'] = $this->db->query($resquery1)->row();
      $resquery2 = "select staff.name, tbl_graduitynomination.address2 from staff left join tbl_graduitynomination on tbl_graduitynomination.witness2=staff.staffid where tbl_graduitynomination.graduity_id =".$inserted;
      $content['witness2result'] = $this->db->query($resquery2)->row();
      $resquery3 = "select a.personal_date, a.personal_place, a.personal_name FROM tbl_graduitynomination as a where a.graduity_id = $inserted";
      $content['personalresult'] = $this->db->query($resquery3)->row();
    }

      /*echo "<pre>";
      print_r($content['personalresult']);exit();*/
    $content['title'] = 'Gratuity_nomination_form_staff/view';

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }



  public function Add()
  {
    try{
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'pgname'      => $this->input->post('pgname'),
          'status'      => $this->input->post('status')
        );
      
      $this->db->insert('mstpgeducation', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
      $this->session->set_flashdata('er_msg', $this->db->error());
      redirect('/Employee_particular_form/index/');
    }

    prepareview:

   
    $content['subview'] = 'Employee_particular_form/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}