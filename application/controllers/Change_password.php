<?php 
class Change_password extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model("Common_model","Common_Model");
		//$this->load->model('Common_Model');
		$this->load->model(__CLASS__ . '_model', 'model');
		$this->loginData = $this->session->userdata('login_data');	
	}

	public function index()
	{
		try{
	 	$uid = $this->loginData->Username;
	 	$rid = $this->loginData->RoleID;
	 	$EmailID = $this->loginData->EmailID;
	 	$UserFirstName =  $this->loginData->UserFirstName;

        $RequestMethod = $this->input->server('REQUEST_METHOD');
				
		if ($RequestMethod == 'POST') {
		// print_r($this->input->post()); die();
			   $username  = $this->input->post("username");
			   $password  = md5($this->input->post("oldpassword"));
               $Newppassword   = $this->input->post("newpassword");
               $confirmpassword  = $this->input->post("confirmpassword");

               $is_exists_count  = $this->is_exists_password($username,$password);

              // echo $is_exists_count; die;

            if ($is_exists_count==1) {

            	$sql = " Select Acclevel_Name from `sysaccesslevel` WHERE Acclevel_Cd =".$rid;
            	$result  = $this->db->query($sql)->result()[0];
            	$rolename = $result->Acclevel_Name;
            
          if($Newppassword == $confirmpassword){

          	$updateArr = array(
				'Password'       => md5($this->input->post("newpassword")),
				);

			//$this->Common_Model->update_data('mstuser', $updateArr,'UserID',$uid);
          	$this->db->where('Username', $uid);
          	$this->db->where('RoleID', $rid);
			$this->db->update('mstuser', $updateArr);

			//echo $this->db->last_query(); die;

			    // Mail Send
			$subject = ": Change Password";
			$body = "<h4>Hi ".$UserFirstName.", </h4><br />";
			$body .= "We have received a request to reset your role Pradan 
			account password.<br />";
			$body .= "<b>User Name: " .  $uid . "</b><br />";
			$body .= "<b>Role Name : " . $rolename . "</b><br />";
			$body .= "<b>Password: " .   $Newppassword . "</b><br /><br /><br />";
			$body .= "See you soon on Pradan <br><br>";
			$body .= "<b> Thanks </b><br>";
			$body .= "<b>Pradan Technical Team </b><br>";


			$to_useremail = $EmailID;
			$to_username = NULL;

			$sendmail = $this->Common_Model->send_email($subject, $body, $to_useremail, $to_username);
			$this->session->set_flashdata('tr_msg', "Please check your email id. Password has been send successfully !!!");
			$this->session->unset_userdata($this->loginData);
            $this->session->sess_destroy('login_data');
			redirect('login');
          
          }else {

          	$this->session->set_flashdata('er_msg', 'New password and confirm password not matched kindly retype');
			redirect('Change_password');
          }

      }else{
          	$this->session->set_flashdata('er_msg', 'Old password not matched kindly retype');
			redirect('Change_password');
          }
    }
      	


	    $content['title'] = 'Change_password';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		//print_r($content); die;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
    }



public function is_exists_password($Username,$password)
{	
	try{

		  $sql = "SELECT Count(UserID) as count FROM mstuser where
		 Username='".$Username."' AND RoleID='".$this->loginData->RoleID."' AND Password='".$password."'"; 
		 $result = $this->db->query($sql)->row();
		 $numrows = $result->count;
		 return $numrows; 
	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


}