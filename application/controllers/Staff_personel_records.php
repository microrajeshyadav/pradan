<?php 

/**
* Staff Personnel Approval List
*/
class Staff_personel_records extends CI_controller
{


  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model('Dompdf_model');
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');


 }


  /**
   * Method index() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */

public function index(){
 
   $this->load->model("Staff_approval_model");
  $this->load->model("Staff_personel_records_model");
  // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 


  $query1 = "SELECT * FROM tbl_iom_transfer where tbl_iom_transfer.staffid=".$this->loginData->staffid;

  $content['staff_id'] = $this->db->query($query1)->row();
/*  
  echo "<pre>";
  print_r($content['staff_id']);exit();*/

  if($content['staff_id']){
    $content['getworkflowdetail'] = $this->Staff_personel_records_model->getworkflowdetaillist($content['staff_id']->staffid);

  }else{
    $query1 = "SELECT * FROM tbl_iom_transfer where `tbl_iom_transfer`.current_responsibility_to=".$this->loginData->staffid;
    $content['staff_id'] = $this->db->query($query1)->row();
    if($content['staff_id']){
      $content['getworkflowdetail'] = $this->Staff_personel_records_model->getworkflowdetaillisttaken($content['staff_id']->staffid);
     /* echo "<pre>";
      print_r($content['staff_id']);exit;*/
    }else{
      $content['getworkflowdetail'] = array();
    }
  }

  
  $content['gettransferworkflowdetail'] = $this->Staff_personel_records_model->get_transfer_promotion_workflowdetail();

  $content['getpromotionworkflowdetail'] = $this->Staff_personel_records_model->get_promotion_workflowdetail();
  $content['staff_seperation'] = $this->Staff_approval_model->staff_seperation();
  /*echo "<pre>";
  print_r($content['gettransferworkflowdetail']);exit();*/
 

  $content['title'] = 'Staff_personel_records';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
}

 public function taken()
  {

     // die("hgfhg");
   $this->load->model("Staff_personel_records_model");
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 
  //$staffid=$this->loginData->staffid;
 $query1 = "SELECT * FROM tbl_iom_transfer where tbl_iom_transfer.current_responsibility_to=".$this->loginData->staffid;

  $content['staff_id'] = $this->db->query($query1)->row();  
 // print_r($content['staff_id']);
 // echo $content['staff_id']->staffid;
 //  die;
   
   $content['getworkflowdetail'] = $this->Staff_personel_records_model->getworkflowdetaillist($content['staff_id']->staffid);
   

   $content['gettransferworkflowdetail'] = $this->Staff_personel_records_model->get_transfer_promotion_workflowdetail();
   
   $content['getpromotionworkflowdetail'] = $this->Staff_personel_records_model->get_promotion_workflowdetail();

   /*echo "<pre>";
   print_r($content['getworkflowdetail']);exit();*/
   //$content['subview']="index";
   $content['title'] = 'Staff_personel_records';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }

 /**
   * Method add() Approval process accept & rejected .
   * @access  public
   * @param Null
   * @return  Array
   */


 public function Add()
 {

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 


    $staffid = $this->uri->segment(3);
    $tarnsid  = $this->uri->segment(4);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == 'POST'){

      // echo "<pre>";
      // print_r($this->input->post()); //die;

      $transid = $this->input->post('tarnsid');
      $status = $this->input->post('status');


      $transferstaffdetails = $this->Staff_personel_records_model->getTransferStaffDetails($transid);

       //print_r($transferstaffdetails); die;

       $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
       $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
       $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.



       // $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
       $this->form_validation->set_rules('status','Seelct Status','trim|required');

       if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }

      $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail 
      WHERE r_id = $transid";
      $result  = $this->db->query($sql)->result()[0];

      $forwardworkflowid = $result->workflowid;


      
        if($status == 5){     //// Status => Personnel Approve 

         $this->db->trans_start();

         $insertArr = array(

          'new_office_id'     => $NewOfficeId,
          'reportingto'       => $NewReportingto ,
          'updatedon'         => date("Y-m-d H:i:s"),
          'updatedby'         => $this->loginData->staffid
        );

         /// 'doj_team'          => $NewDateOfTransfer
         
         $this->db->where('staffid',$staffid);
         $this->db->update('staff', $insertArr);

//echo $this->db->last_query(); die;

         $updateArr = array(

          'trans_flag'     => 5,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $updateArr);
//echo $this->db->last_query(); die;


         $insertworkflowArr = array(
           'r_id'                 => $transid,
           'type'                 => 4,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $this->loginData->staffid,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 5,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
      //  echo $this->db->last_query(); die;


         $this->db->trans_complete();

         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_personel_records/'.$token);

        }

      }


        if ($status ==6) {   //// Status => Personnel Rejected 

         $insertArr = array(

          'trans_flag'     => 6,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 4,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $this->loginData->staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => 6,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('Staff_personel_records');

        }
      }

    }

    prepareview:

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    //$content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();

    $content['subview'] = 'Staff_personel_records/add';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}




 /**
   * Method add_iom() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



 public function add_iom()
 {

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    // print_r($this->loginData);
    $staffid = $this->uri->segment(3);
    $tarnsid  = $this->uri->segment(4);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == 'POST'){

     // echo "<pre>";
     // print_r($this->input->post()); die;

      $transid = $this->input->post('tarnsid');
      $staffid = $this->input->post('staffid');
      $save    = $this->input->post('save');
      $submit  = $this->input->post('submit');





      $letterdate =  $this->input->post('letter_date');
      $letter_date = $this->gmodel->changedatedbformate($letterdate);
      $chargeresponsibilityon =  $this->input->post('charge_responsibility_on');
      $charge_responsibility_on = $this->gmodel->changedatedbformate($chargeresponsibilityon);


      $resdate =  $this->input->post('restransferno');
      $chargeresdate = $this->gmodel->changedatedbformate($resdate);

      if (isset($save) && $save =='SaveDataSend') {

        $this->db->trans_start();

        $insertworkflowArr = array(
          'transferno'                     =>  $this->input->post('transferno'),
          'staffid'                        => $staffid,
          'transid'                        => $transid,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    =>  $letter_date,
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $charge_responsibility_on,
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place'),
          'report_for_work_place_on_date'  => $chargeresdate,
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => 0,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
        );

        $this->db->insert('tbl_iom_transfer', $insertworkflowArr);
        $insertid = $this->db->insert_id();
      //  echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Inter-Office Memo has been failed !!'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Inter-Office Memo has been save successfully !!!');
          redirect('/Staff_personel_records/edit/'.$insertid);

        }


      }



      if (isset($submit) && $submit =='SendDataSave') {

        $this->db->trans_start();

        $insertworkflowArr = array(
          'transferno'                     =>  $this->input->post('transferno'),
          'staffid'                        => $staffid,
          'transid'                        => $transid,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    =>  $letter_date,
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $charge_responsibility_on,
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place'),
          'report_for_work_place_on_date'  => $chargeresdate,
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => 1,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
        );

        $this->db->insert('tbl_iom_transfer', $insertworkflowArr);
        $insertid = $this->db->insert_id();
      //  echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Inter-Office Memo has been failed !!'); 

          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Inter-Office Memo has been save successfully !!!');

          redirect('/Staff_personel_records/view/'.$insertid);

        }


      }


    }



    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    $content['personnel_name'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;


    $content['getstaffdetails'] = $this->Staff_personel_records_model->getStaffDetail($tarnsid);

    $content['getofficedetails'] = $this->Staff_personel_records_model->getAllOffice();

    $content['getstafflist'] = $this->Staff_personel_records_model->getStaffList();

   //print_r($getstafflist); die;

    $content['subview'] = 'Staff_personel_records/add_iom';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}



/**
   * Method add_iom() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



public function edit($token = NULL)
{

  try{
        // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    // // print_r($this->loginData);
    // $staffid = $this->uri->segment(3);
    // $tarnsid  = $this->uri->segment(4);

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 

    if($RequestMethod == 'POST'){

      // echo "<pre>";
      // print_r($this->input->post()); die;

      $transid = $this->input->post('tarnsid');
      $staffid = $this->input->post('staffid');
      $save = $this->input->post('save');
      $submit = $this->input->post('submit');


      $letterdate =  $this->input->post('letter_date');
      $letter_date = $this->gmodel->changedatedbformate($letterdate);
      $chargeresponsibilityon =  $this->input->post('charge_responsibility_on');
      $charge_responsibility_on = $this->gmodel->changedatedbformate($chargeresponsibilityon);


      $resdate =  $this->input->post('restransferno');
      $chargeresdate = $this->gmodel->changedatedbformate($resdate);

      if (isset($save) && $save =='SaveDataSend') {

        $this->db->trans_start();

        $insertworkflowArr = array(
          'transferno'                     =>  $this->input->post('transferno'),
          'staffid'                        => $staffid,
          'transid'                        => $transid,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    =>  $letter_date,
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $charge_responsibility_on,
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place'),
          'report_for_work_place_on_date'  => $chargeresdate,
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => 0,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
        );
        $this->db->where('id', $token);
        $this->db->update('tbl_iom_transfer', $insertworkflowArr);
        //$insertid = $this->db->insert_id();
      //  echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Inter-Office Memo has been failed !!'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Inter-Office Memo has been save successfully !!!');
          redirect('/Staff_personel_records/edit/'.$token);

        }


      }

      if (isset($submit) && $submit =='SendDataSave') {

        $transid = $this->input->post('tarnsid');
        $staffid = $this->input->post('staffid');

        $this->db->trans_start();

        $insertworkflowArr = array(
          'transferno'                     =>  $this->input->post('transferno'),
          'staffid'                        => $staffid,
          'transid'                        => $transid,
          'personnel_staffid'              => $this->input->post('personnel_id'),
          'letter_date'                    =>  $letter_date,
          'integrator_ed'                  => $this->input->post('reportingto'),
          'responsibility_on_date'         => $charge_responsibility_on,
          'report_for_work_place'          
          => $this->input->post('report_for_work_at_place'),
          'report_for_work_place_on_date'  => $chargeresdate,
          'current_responsibility_to'      => $this->input->post('current_responsibilities_to'),

          'joining_report_prescribed_form_to'   => $this->input->post('joining_report_prescribed_form_to'),
          'cash_advance_from_our_office'        => $this->input->post('cash_advance_from_our'),
          'travel_expenses_bill_submitting_to'  => $this->input->post('travel_expenses_bill_submitting_to_id'),
          'flag'                            => 1,
          'createdon'                       => date("Y-m-d H:i:s"),
          'createdby'                       => $this->loginData->staffid,
        );

        $this->db->where('id', $token);
        $this->db->update('tbl_iom_transfer', $insertworkflowArr);
      //  echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Inter-Office Memo has been failed !!'); 
          redirect(current_url());

        }else{
          $this->session->set_flashdata('tr_msg', 'Inter-Office Memo has been save successfully !!!');
          redirect('/Staff_personel_records/view/'.$token);

        }


      }

    }

    $gettransferstaffdetails = $this->Staff_personel_records_model->getTransferStaffDetail($token);

  // print_r($gettransferstaffdetails);


    $content['iom_staff_transfer'] = $gettransferstaffdetails;
    

    $staffid = $gettransferstaffdetails->staffid;
    $tarnsid = $gettransferstaffdetails->transid;

    $content['personnel_name'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;


    $content['getstaffdetails'] = $this->Staff_personel_records_model->getStaffDetail($tarnsid);

    $content['getofficedetails'] = $this->Staff_personel_records_model->getAllOffice();

    $content['getstafflist'] = $this->Staff_personel_records_model->getStaffList();

    $content['subview'] = 'Staff_personel_records/edit';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}



/**
   * Method view() get staff with workflow list .
   * @access  public
   * @param Null
   * @return  Array
   */



public function view($token = NULL)
{

  try{

   // echo $token; 


    $getsingletransferstaffdetails = $this->Staff_personel_records_model->getSingleTransferStaffDetail($token);


    $content['iom_single_staff_transfer'] = $getsingletransferstaffdetails;
    $staffid = $getsingletransferstaffdetails->staffid;
    $tarnsid = $getsingletransferstaffdetails->transid;
    $gettransferstaffdetails = $this->Staff_personel_records_model->getTransferStaffDetail($token);
    $content['iom_staff_transfer'] = $gettransferstaffdetails;
    $content['getstaffdetails'] = $this->Staff_personel_records_model->getStaffDetail($tarnsid);
    $content['getofficedetails'] = $this->Staff_personel_records_model->getAllOffice();
    $content['getstafflist'] = $this->Staff_personel_records_model->getStaffList();
    $content['personnel_name'] = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;

   $content['token'] = $token;

    $content['subview'] = 'Staff_personel_records/view';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


/**
   * Method genrate_transfer_letter() genrate transfer letter PDF .
   * @access  public
   * @param Null
   * @return  Array
   */



public function genrate_transfer_letter($token = NULL)
{

  try{


   //$content['token'] = $token;

    $personnel_name = $this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName;

    $getsingletransferstaffdetails = $this->Staff_personel_records_model->getSingleTransferStaffDetail($token);

  //print_r($getsingletransferstaffdetails); die;

    $iom_single_staff_transfer = $getsingletransferstaffdetails;
    

    $staffid = $getsingletransferstaffdetails->staffid;
    $tarnsid = $getsingletransferstaffdetails->transid;


    $gettransferstaffdetails = $this->Staff_personel_records_model->getTransferStaffDetail($token);

  // print_r($gettransferstaffdetails);


    $iom_staff_transfer = $gettransferstaffdetails;

    $getstaffdetails = $this->Staff_personel_records_model->getStaffDetail($tarnsid);


    $html = '<div class="panel-body">
    <div class="row"> 
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <p style="text-align: center;">Professional Assistance for Development Action (PRADAN)</p>
    <p style="text-align: center;"><strong> Inter-Office Memo (IOM) for Transfer</strong></p>

    <p style="text-align: center;"><em>I</em><em>nter-Office Memo</em></p>
    <p style="text-align: center;">&nbsp;</p>
    <table style="width: 996px; height: 131px;">
    <tbody>
    <tr>
    <td style="width: 532px;">
    <p>Transfer No.: 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_staff_transfer->transferno.'</label>
    </p>
    </td>

    <td style="width: 448px;"></td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>To: <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$getstaffdetails->Staffname .'</label></p>
    </td>
    <td style="width: 448px;">
    <p>Ref: (from Personal Dossier)
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $personnel_name .'</label></p>
    </td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>(Name, Designation of Employee being transferred)</p>
    </td>
    <td style="width: 448px;">
    <p>Date: <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $this->gmodel->changedatedbformate($iom_staff_transfer->letter_date).'</label></p>
    </td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>From:
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$getstaffdetails->reportingtoname.'</label>
    </p>
    </td>
    <td style="width: 448px;">
    <p>Copy: See list below</p>
    </td>
    </tr>
    <tr>
    <td style="width: 532px;">
    <p>(Integrator/Executive Director)</p>
    </td>
    <td style="width: 448px;">
    <p>&nbsp;</p>
    </td>
    </tr>
    </tbody>
    </table>
    <p>&nbsp;</p>
    <p>Subject: Your Transfer from 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    '. $getstaffdetails->oldoffice .'</label> to 

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$getstaffdetails->newoffice.'</label>&nbsp;</p>
    <p></p>
    <p></p>
    <p style="text-align: justify;">I write to inform you that you have been transferred from <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    '. $getstaffdetails->oldoffice .'</label> to <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    <?php echo $getstaffdetails->newoffice;?></label> .</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">Please hand over charge of your responsibilities on <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $this->gmodel->changedatedbformate($iom_staff_transfer->responsibility_on_date).'
    </label> (date), and report for work at 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_single_staff_transfer->officename.'</label> (place) on 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$this->gmodel->changedatedbformate($iom_staff_transfer->report_for_work_place_on_date).'
    </label> (date).</p>
    <p style="text-align: justify;"></p>
    <p style="text-align: justify;">Before proceeding to your new place of posting, you would need to hand over charge of your current responsibilities to

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'. $iom_single_staff_transfer->currentresponsibilityto.'
    </label>

    . Please also ensure that you get a &lsquo;Clearance Certificate&rsquo; from all concerned (as per the enclosed proforma).</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">On arrival at your new place of posting, please hand in your joining report in the prescribed form to&nbsp; 
    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_single_staff_transfer->joiningreportprescribedformto.'
    </label> . Please also ensure that you send a copy to all concerned.</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">If necessary, you may take a cash advance from our 

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">'.$iom_single_staff_transfer->cashadvancefromouroffice.'
    </label>
    office to meet the travel expenses to join at the new place of your posting as per PRADAN&rsquo;s Travel Rules.</p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;">You may avail of journey and joining time as per PRADANs travel rules. You would be eligible to claim reimbursement of fare and local conveyance for yourself and your family on account of travel to your new place of work, actual expenses incurred to transport your vehicle and personal effects there and one month&rsquo;s basic pay as lump sum transfer allowance.&nbsp; Please claim these by filling the enclosed &ldquo;Travel Expenses Bill&rdquo; and submitting it to&nbsp; 

    <label style="width: 200px; border-radius: 0px; border: none; border-bottom: 1px solid black;">
    '. $getstaffdetails->reportingtoname.'</label>( <em>Designation).</em></p>
    <p style="text-align: justify;">&nbsp;</p>
    <p style="text-align: justify;"><b>List of enclosures:</b></p>
    <ol style="text-align: justify;">
    <li>Clearance Certificate</li>
    <li>Joining Report</li>
    <li>Travel Expenses Bill</li>
    <li>Handing Over/Taking over Charge</li>
    </ol>
    <p style="text-align: justify;"><strong>&nbsp;</strong></p>
    <p style="text-align: justify;">cc: - Person concerned to whom charge is being given.</p>
    <p style="text-align: justify;"> - Team Coordinator/Integrator at the old and new places of posting</p>
    <p style="text-align: justify;">- Finance-Personnel-MIS Unit</p>
    </div>  
    </div>';

    $filename = 'Transferletter-'.md5(time() . rand(1,1000));
    $updateArr = array(
      'filename'    => $filename,
    );

    $this->db->where('id', $token);
    $this->db->update('tbl_iom_transfer', $updateArr);

     // $filename = 'Tranfer-letter';
    $this->load->model('Dompdf_model');
    $generate = $this->Dompdf_model->generate_personnel_transfer_letter_PDF($html, $filename, NULL,'transferletter.pdf');
        //     echo "sczxcxzc";

    if ( $generate == true) {

     // $hrdemail       = $this->loginData->EmailID; /////// hrd email id/////

      $getmailtransferstaffdetails = $this->Staff_personel_records_model->getMailSingleTransferStaffDetail($token);


    //  print_r($getmailtransferstaffdetails); 

      $to_name = 'rahulkirar7@gmail.com';

      
       $firstname             = $getmailtransferstaffdetails->staffname2;
       $to_email              = $getmailtransferstaffdetails->staffemailid;
       $staffnewoffice        = $getmailtransferstaffdetails->newofficename;
       $filename              = $getmailtransferstaffdetails->filename;
     //  die;

      $attachments = array($filename);

       //print_r($attachments);  die;

      $html1 = 'Dear '. $firstname.', <br><br> 
      Congratulations your are transfer by Pradan. 
      <br><br> New Office  - '.$staffnewoffice.'
      <br><br><br><br>
      Regards <br>
      Pradan Team <br>';

$sendmail = $this->Common_Model->transfer_letter_send_email(' : Transfer Letter ', $html1, $to_email, $to_name, $attachments);  //// Send Mail candidates With Offer Letter ////

//echo $sendmail; die;
      if ($sendmail==1) {
            $updateArr = array(
                'sendmail'    => 1,
              );

              $this->db->where('id', $token);
              $this->db->update('tbl_iom_transfer', $updateArr);
           
      }
      $this->session->set_flashdata('tr_msg', 'Successfully generate & send transfer letter !!!!'); 
      redirect('Staff_personel_records/view/'.$token);
    

    }else{
      $this->session->set_flashdata('er_msg', 'Error!!! failed generate transfer letter !!!!');  
     redirect('Staff_personel_records/view/'.$token);
    }




    // $content['token'] = $token;
    $content['subview'] = 'Staff_personel_records/view';
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}




}