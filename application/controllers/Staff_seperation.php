<?php 

/**
* State List
*/
class Staff_seperation extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    $this->load->model("Staff_seperation_model","Staff_seperation_model");
    
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $query = "select * from mskeyrules where isdeleted=0";
   $content['staff_seperation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";


   $content['title'] = 'Staff_seperation';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }
 public function Add($token)
 {
  try{

  // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

  $tablename = "'staff_transaction'";
  $incval = "'@a'";

  $content['token']=$token;
  //print_r( $this->input->post()); 
  $content['transfer_promption']=$this->Staff_seperation_model->get_staffDetails($token);
  // print_r($content['transfer_promption']);
  // die;

  $getStaffReportingtodetails =$this->Common_Model->get_staffReportingto($token);
  $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
  // echo "dhsgc";
  // echo "<pre>";
  // print_r($content['transfer_promption']);exit();
  if(!empty($getStaffReportingtodetails))
  $stdesignation = $getStaffReportingtodetails->designation;
  // echo $stdesignation;
  // die;
  $content['getStaffReportingto'] =  $getStaffReportingtodetails;
  //print_r($content['getStaffReportingto']);exit;
  $content['superannuation'] = $this->Staff_seperation_model->superannuationcheck($token);
  /*echo "<pre>";
  print_r($content['superannuation']);exit();*/
  $getedname = $this->gmodel->getExecutiveDirectorEmailid();
  $content['login_data']=$this->loginData;

  $getstaffincrementalid = $this->Staff_seperation_model->getStaffTransactioninc($tablename,$incval);

  $autoincval =  $getstaffincrementalid->maxincval;


  $RequestMethod = $this->input->server('REQUEST_METHOD'); 


  if($RequestMethod == 'POST'){
    // print_r($this->input->post()); 
    $this->form_validation->set_rules('remark','Name','trim|required|min_length[1]|max_length[50]');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }
    $this->load->model('Off_campus_candidate_list_model');

    $proposed_seperate_date=$this->input->post('proposed_seperate_date');
    $proposed_seperate_date_db=$this->Off_campus_candidate_list_model->changedatedbformate($proposed_seperate_date);
    $supervisor = $this->input->post('reportingto');

    // $transstatus = 'seperation';
    
    $sql ="SELECT count(staffid) as staffcount FROM staff_transaction where trans_status in('Retiremnt','Death','Termination','Desertion cases','Termination during Probation','Discharge simpliciter/Dismissal','Super Annuation') and staffid = '".$token."' and date_of_transfer = '".$proposed_seperate_date_db."'";

    // echo $sql; die;

    $result = $this->db->query($sql)->row();
    // echo $result->staffcount; die;
    if($result->staffcount==0){
      $insertArr = array(

        'id'               => $autoincval,
        'staffid'          => $token,
        'old_office_id'    => $this->input->post('Presentoffice'),
        'new_office_id'    => $this->input->post('Presentoffice'),
        'date_of_transfer' => $proposed_seperate_date_db,
        'trans_status'     => $this->input->post('trans_status'),
        'datetime'         => date("Y-m-d H:i:s"),
        'reason'           => $this->input->post('reason'),
        'reportingto'      => $this->input->post('reportingto'),
        'trans_flag'       => 1,
        'createdon'        => date("Y-m-d H:i:s"),
        'createdby'        => $this->loginData->staffid,
        'id'               => $autoincval,
        'old_designation'  => $content['transfer_promption']->old_designation,
        'new_designation'  => $content['transfer_promption']->new_designation,
      );

      

      $this->db->insert('staff_transaction', $insertArr);

      $insertid = $this->db->insert_id();

      $insertworkflowArr = array(
      'r_id'           => $autoincval,
      'type'           => 3,
      'staffid'        => $token,
      'sender'         => $this->loginData->staffid,
      'receiver'       => $supervisor,
      'senddate'       => date("Y-m-d H:i:s"),
      'flag'           => 1,
      'scomments'      => $this->input->post('remark'),
      'createdon'      => date("Y-m-d H:i:s"),
      'createdby'      => $this->loginData->staffid,
      );

      $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

      $this->db->trans_complete();

      if ($this->db->trans_status() === TRUE){

        // if ($this->input->post('trans_status') == 'Termination') {
          $subject = "Your Request for Separation Submited Successfully";
        $body = 'Dear '.$content['transfer_promption']->name.',<br><br>';

        $body .= '<h3>'.$this->input->post('trans_status').' separation process has been initiate for the staff.</h3><br>';
        $body .= 'Staff Code :'.$content['transfer_promption']->emp_code.'<br>';
        $body .= 'Name :'.$content['transfer_promption']->name.'<br>';
        $body .= 'Designation :'.$content['transfer_promption']->desname.'<br>';
        $body .= 'Office:'.$content['transfer_promption']->officename.'<br>';
        $body .= 'Proposed last working day :'.$this->input->post('proposed_seperate_date').'<br><br>';

        $body .= '<small><b>This email is system generated. Please do not reply to this email ID.  For any queries contact PRDAN IT/Personnel/HR team</b></small>'; 
        // }
        // else{
        //   $subject = "Your Request Submited Successfully";
        // $body = 'Dear,<br><br>';
        // $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
        // $body .= 'This is to certify that Mr. '.$content['transfer_promption']->name.'<br> has '.$this->input->post('trans_status').' .<br><br>';
        // $body .= 'Thanks<br>';
        // $body .= 'Administrator<br>';
        // $body .= 'PRADAN<br><br>';

        // $body .= 'Disclaimer<br>';
        // $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';
        // }
        
        // echo $body;
        $to_email = $content['transfer_promption']->emailid;
        $to_name = $content['transfer_promption']->name; 
        //$to_cc = $getStaffReportingtodetails->emailid;
        $recipients = array(
        $getStaffReportingtodetails->emailid => $getStaffReportingtodetails->name,
        $personnel_detail->EmailID => $personnel_detail->UserFirstName,
        $getedname->edmailid => $getedname->edname
        // ..
        );
        // print_r($recipients); die();
        $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
        if(substr($email_result, 0, 5) == "ERROR") {
          $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
        }
        // $this->session->set_flashdata('tr_msg', 'Successfully added staff seperation');
        // redirect(current_url());
        $this->session->set_flashdata('tr_msg', 'Successfully added  Staff Seperation');
          redirect('/Staff_seperation/viewhistory/'.$token);

        }else{
          // $this->session->set_flashdata('tr_msg', 'Successfully added  Staff Transfer and Promotion');
          // redirect('/Staff_seperation/viewhistory/'.$token);
          $this->session->set_flashdata('tr_msg', 'Successfully added staff seperation');
        redirect(current_url());
        }
      }else{
        $this->session->set_flashdata('er_msg', "Seperation already under process !!");
        redirect('/Staff_seperation/add/'.$token);   
      }
    }

    prepareview:
    $content['staff_list'] = $this->Staff_seperation_model->getdata();
    $content['subview'] = 'Staff_seperation/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }




 // public function checkoffice()
 //  {
 //    $idofc=$this->input->post('idd');

 //    $data=" SELECT DISTINCT a.officename from lpooffice a
 //            INNER JOIN staff_transaction b
 //            ON a.officeid=b.old_office_id 
 //            WHERE b.staffid=$idofc;
 //            ";

 //      $data1=$this->db->query($data)->row();
 //      $arr1=array('office_name'=>$data1->officename);

 //       echo json_encode($arr1);

 //  }


public function staffresignationui()
{
  try{

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 


  $content['subview'] = 'Staff_seperation/staffresignationui';
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function viewhistory($token)
{
  try{
//echo $token;die();
  $this->load->model('Staff_seperation_model');
  $content['getseperation']=$this->Staff_seperation_model->get_staff_seperation_detail($token);
  $content['view_history']=$this->Staff_seperation_model->fetchdatas($token);


  $content['subview'] = 'Staff_seperation/viewhistory';
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


}