<?php 
class Developmentapprentice_graduatingemployeeexitform extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Developmentapprentice_graduatingemployeeexitform_model','model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_Model");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($token)
	{
		$fa = '';
		$fa = $this->gmodel->getFinanceAdministratorEmailid();

		$daname = '';
		$daname  = $this->loginData->UserFirstName." ".$this->loginData->UserLastName;
		// start permission
		 // echo $token; die;

		try{

			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Developmentapprentice_graduatingemployeeexitform/index'.$token);
				
			} else {

				// /echo $token; die;

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();

			$query ="SELECT a.*, b.name as hr_name, b.emailid as hr_email, c.name  FROM staff_transaction a 
			inner join staff b on a.createdby=b.staffid 
			inner join staff c on a.staffid=c.staffid
			WHERE id=".$token;
			//echo $query;
		    $content['staff_transaction'] = $this->db->query($query)->row();
			
     	    $content['staff_detail'] = $this->model->get_staff_sep_detail($token);     	  
     	    $content['finance_detail'] = $this->Common_Model->get_staff_finance_detail($content['staff_transaction']->new_office_id,20); 	
		   	$query ="SELECT * FROM staff WHERE staffid=".$content['staff_transaction']->reportingto;
            $content['tc'] = $this->db->query($query)->row();

			$content['join_relive_data'] = $this->model->get_staff_seperation_certificate($content['staff_detail']->staffid);
			
			// echo "<pre>";
			// print_r($content['join_relive_data']);exit();

			$content['da_image'] = $this->Common_Model->staff_signature($content['staff_detail']->staffid);
			


			$query ="SELECT * FROM tbl_da_exit_interview_graduating_form WHERE transid=".$token;
			$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();
			
			//transid to insert
			$id = $content['staff_transaction']->id;
			// // end permission    

        
        	$RequestMethod = $this->input->server('REQUEST_METHOD');
        	if($RequestMethod == "POST"){
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'Save And Submit'){
					$db_flag = 1;
					$comments = 'Exit interview form filled by DA';
					$updateArr = array(
			            'trans_flag'     => 7,
			            'updatedon'      => date("Y-m-d H:i:s"),
			            'updatedby'      => $this->loginData->staffid
			          );

					$this->db->where('id', $id);
					$this->db->update('staff_transaction', $updateArr);
					$insertworkflowArr = array(
			           'r_id'           => $id,
			           'type'           => 29,
			           'staffid'        => $this->loginData->staffid,
			           'sender'         => $this->loginData->staffid,
			           'receiver'       => $content['finance_detail']->staffid,
			           'senddate'       => date("Y-m-d H:i:s"),
			           'flag'           => 7,
			           'scomments'      => $comments,
			           'createdon'      => date("Y-m-d H:i:s"),
			           'createdby'      => $this->loginData->staffid,
			         );
					// echo "<pre>";
					// print_r($insertworkflowArr); die;
			         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				}

				
				//print_r($content['staff_transaction']); die;
        		
        		if($content['staff_sepemployeeexitform']){
        			$data_update = array(
						'staffid'=>$content['staff_transaction']->staffid,
						'transid'=>$id,
						'da_liked_most_1'=>$this->input->post('da_liked_most_1'),
						'da_liked_most_2'=>$this->input->post('da_liked_most_2'),
						'da_liked_most_3'=>$this->input->post('da_liked_most_3'),
						'da_liked_least_1'=>$this->input->post('da_liked_least_1'),
						'da_liked_least_2'=>$this->input->post('da_liked_least_2'),
						'da_liked_least_3'=>$this->input->post('da_liked_least_3'),
						'da_suggestion_1'=>$this->input->post('da_suggestion_1'),
						'da_suggestion_2'=>$this->input->post('da_suggestion_2'),
						'da_suggestion_3'=>$this->input->post('da_suggestion_3'),
						'updatedon'=>date('Y-m-d H:i:s'),
						'updatedby'=>$this->loginData->staffid,
						'flag'=>$db_flag
					);
					$this->db->where('id', $content['staff_sepemployeeexitform']->id);
					$flag = $this->db->update('tbl_da_exit_interview_graduating_form', $data_update);
				}
				else
				{
					$data = array(
						'staffid'=>$content['staff_transaction']->staffid,
						'transid'=> $id,
						'da_liked_most_1'=>$this->input->post('da_liked_most_1'),
						'da_liked_most_2'=>$this->input->post('da_liked_most_2'),
						'da_liked_most_3'=>$this->input->post('da_liked_most_3'),
						'da_liked_least_1'=>$this->input->post('da_liked_least_1'),
						'da_liked_least_2'=>$this->input->post('da_liked_least_2'),
						'da_liked_least_3'=>$this->input->post('da_liked_least_3'),
						'da_suggestion_1'=>$this->input->post('da_suggestion_1'),
						'da_suggestion_2'=>$this->input->post('da_suggestion_2'),
						'da_suggestion_3'=>$this->input->post('da_suggestion_3'),
						'currentdate'=>date('Y-m-d'),
						'createdby'=>$this->loginData->staffid,
						'flag'=>$db_flag
					);
					$flag = $this->db->insert('tbl_da_exit_interview_graduating_form',$data);
				 }
				 if($db_flag)
				 {
					 // mail to tc
					$subject = "Completion of graduation formalities";
					$body  = "<h4>Dear ".$content['tc']->name.", </h4><br />";
					$body .= "I have completed the formalities. Kindly check."."<br />";
					$body .= "<b> Thanks </b><br>";
					$body .= "<b>".$daname." </b><br>";
	  
					$to_email = $content['tc']->emailid;
					// $to_name = $content['staff_transaction']->hr_name;
					// $recipients = [];
					$email_result = $this->Common_Model->send_email($subject, $body, $to_email);
					if (substr($email_result, 0, 5) == "ERROR") {
						$this->session->set_flashdata('er_msg', "Error sending exit form submission email, please contact system administrator");
					}

					// mail to fa
					$subject1 = "Confirmation of Balance Form of Apprentices";
					$body1  = "<h4>Dear ".$fa->name.", </h4><br />";
					$body1 .= "Please fill the confirmation of balance form for ".$daname.",<br />";
					$body1 .= "<b> Thanks </b><br>";
					$body1 .= "<b>".$content['tc']->name." </b><br>";
	  
					$to_email1 = $fa->financeemailid;
					// $to_name = $content['staff_transaction']->hr_name;
					$recipients = [];
					$email_result1 = $this->Common_Model->send_email($subject1, $body1, $to_email1, $to_name= null, $recipients);
					if (substr($email_result1, 0, 5) == "ERROR") {
						$this->session->set_flashdata('er_msg', "Error sending exit form submission email, please contact system administrator");
					}
				 }
				
         		if($flag) { $this->session->set_flashdata('tr_msg','Data Saved Successfully.'); } 
               	else { $this->session->set_flashdata('er_msg','Something Went Wrong!!'); }
        	}



			$query ="SELECT * FROM tbl_da_exit_interview_graduating_form WHERE transid=".$token;
			$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();

			$content['token'] = $token;
			$content['title'] = 'Staff_sepemployeeexitform';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}
	public function get_staff_clearance_detail($token)
    {
		try
		{

	    	$sql=" SELECT c.name,a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code
				FROM staff_transaction as a
				INNER JOIN `staff` as c on a.staffid = c.staffid
				INNER JOIN `msdesignation` as b on a.new_designation = b.desid
				WHERE a.id = $token";
			//$query=$this->db->get();
			return  $this->db->query($sql)->row();

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   public function get_staff_seperation_certificate($staffid)
   {
      try
	  {
	      $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation,g.desname as leavingdesignation,k.reason as leavingreason FROM staff_transaction as a
	         INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid
	         INNER JOIN `staff` as c on a.staffid = c.staffid
	         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
	         INNER JOIN `msdesignation` as g on k.new_designation = g.desid
	         WHERE  K.`trans_status` = 'Resign' and  a.trans_status = 'JOIN' and a.staffid =".$staffid; 


	     return  $this->db->query($sql)->row();

		}catch (Exception $e) {
	      print_r($e->getMessage());die;
	    }

  }

	
}