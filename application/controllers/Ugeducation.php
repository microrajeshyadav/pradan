<?php 

/**
* State List
*/
class Ugeducation extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     try{
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from mstugeducation where isdeleted='0'";
    $content['mstugeducation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
    $content['title'] = 'Ugeducation';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }
  public function Add()
  {
    try{

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('ugname','UG Name','trim|required|min_length[1]|max_length[50]|is_unique[mstugeducation.ugname]');
        // $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'ugname'      => $this->input->post('ugname'),
          // 'status'      => $this->input->post('status')
        );
      
      $this->db->insert('mstugeducation', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added  UG education');
      redirect('/Ugeducation/index/');
    }

    prepareview:

   
    $content['subview'] = 'Ugeducation/add';
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

  public function edit($token)
  {
    try{
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('ugname','UG name','trim|required|min_length[1]|max_length[50]|is_unique[mstugeducation.ugname]');
        // $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
          'ugname'      => $this->input->post('ugname'),
          // 'status'         => $this->input->post('status')
        );
      
      $this->db->where('id', $token);
      $this->db->update('mstugeducation', $updateArr);
  $this->db->last_query(); 

      $this->session->set_flashdata('tr_msg', 'Successfully Updated UG');
       redirect('/Ugeducation/');
    }

    prepareview:

    $content['title'] = 'Ugeducation';
    $state_details = $this->Common_Model->get_data('mstugeducation', '*', 'id',$token);
    $content['mstugeducation_details'] = $state_details;

 

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

    }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
   }
    function delete($token)
    {
      try{
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('id', $token);
      $this->db->update('mstugeducation', $updateArr);
      $this->session->set_flashdata('tr_msg', 'UG Education Deleted Successfully');
      redirect('/Ugeducation/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);

      }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
  }

}