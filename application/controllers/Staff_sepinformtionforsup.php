<?php 
class Staff_sepinformtionforsup extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepinformtionforsup_model');
		$this->load->model('Staff_seperation_model');
		$this->load->model("Common_model", "Common_Model");
		$this->load->model("Global_model", "gmodel");
		$this->load->model("General_nomination_and_authorisation_form_model");


		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token = null)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    
			$sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
		    $result  = $this->db->query($sql)->result()[0];
		    $forwardworkflowid = $result->workflowid;
		    $query ="SELECT * FROM staff_transaction WHERE id=".$token;
			$content['staff_sepinformation'] = $this->db->query($query)->row();

			$staffid =  $token;


			$content['staff_detail'] = $this->Common_Model->get_Staff_detail($token);

			$content['staffid'] = $token;

			$query = "SELECT * FROM tbl_sepinformation_annuation WHERE staffid = ".$token;
			$content['annuation_detail'] = $this->db->query($query)->row();
			$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
        	$receiverdetail = $this->Staff_seperation_model->geteddetail();
			$RequestMethod = $this->input->server("REQUEST_METHOD");

			/*echo "<pre>";
			print_r($personnel_detail);exit;*/
			if($RequestMethod == 'POST')
			{
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'saveandsubmit'){
					$db_flag = 1;
					$html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td colspan="3"><p align="center"><strong>Letter Providing Information for Superannuation</strong></p></td>
					  </tr>
					  <tr>
					    <td width="31%">No.: 
					    '.$this->input->post('sep_annuation_no').'</td>
					    <td width="48%">&nbsp;</td>
					    <td width="21%">Date: '.$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('attaining_age_date')).'</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>Name : </td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>Address : '.$content['staff_detail']->permanentstreet.'</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td><p>Dear </p></td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td colspan="3"><p>We would be formally parting  with you on '.$this->input->post('attaining_age_date').' (date) upon your attaining the age of superannuation. On  behalf of all of us in PRADAN, I  would like to say that our association over these years has been meaningful.</p></td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td colspan="3"><p>I hope you are taking  necessary action to plan your retirement. I also take this opportunity to  assist you in timely completion of pre-retirement formalities so as to finalise  your retirement benefits, etc. I am writing to our Finance-Personnel-MIS Unit to  keep your account up to and including '.$this->input->post('finance_personnel_date').' (date) ready and also to let  you know the formalities that are required to be completed by you in this  regard. Please also arrange to get a &lsquo;Clearance Certificate&rsquo; from all concerned  (in duplicate) in the prescribed proforma enclosed, well in time to facilitate  your release and settlement of your account.</p></td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td colspan="3"><p>For any assistance in this  regard, please contact our Finance-Personnel-MIS Unit.</p></td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td><p>With best wishes.</p></td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td><p>Yours sincerely,</p></td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td><p>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )<br>
					    Executive Director</p></td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					  </tr>
					</table>';
					$filename = $token.'-'.md5(time() . rand(1,1000));
					$this->load->model('Dompdf_model');
					$generate =   $this->Dompdf_model->probation_letter_PDF($html, $filename, NULL,'filename.pdf');
                 	$attachments = array($filename.'.pdf');
					// $arrmail  = array(
		   //               	$tc_email = 'tc',
		   //               );

					$staffemail = $content['staff_detail']->emailid;
					$personnelemail = $personnel_detail->EmailID;
					$this->Common_Model->send_email($subject = 'Confirmation by Pradan', $message = $html, $staffemail, $personnelemail, $arrmail = null, $attachments);
					// print_r($attachments); die();
				}




				// if(trim($this->input->post('comments'))){
					// staff transaction table flag update after acceptance
					$updateArr = array(                         
		                'trans_flag'     => 5,
		                'updatedon'      => date("Y-m-d H:i:s"),
		                'updatedby'      => $this->loginData->staffid
		            );
					
		            $this->db->where('id',$token);
		            $this->db->update('staff_transaction', $updateArr);
		            
		            $insertworkflowArr = array(
		             'r_id'                 => $token,
		             'type'                 => 3,
		             'staffid'              => $content['staff_sepinformation']->staffid,
		             'sender'               => $this->loginData->staffid,
		             'receiver'             => $receiverdetail->staffid,
		             'forwarded_workflowid' => $forwardworkflowid,
		             'senddate'             => date("Y-m-d H:i:s"),
		             'flag'                 => 5,
		             'scomments'            => $this->input->post('comments'),
		             'createdon'            => date("Y-m-d H:i:s"),
		             'createdby'            => $this->loginData->staffid,
		            );
			        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			        
					// echo "<pre>";
				// print_r($insertworkflowArr);exit();
			        $subject = "Your Process Approved";
		            $body = 'Dear,<br><br>';
		            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
		            $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your process has Approved  .<br><br>';
		            $body .= 'Thanks<br>';
		            $body .= 'Administrator<br>';
		            $body .= 'PRADAN<br><br>';

		            $body .= 'Disclaimer<br>';
		            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

		            
		            $to_email = $receiverdetail->emailid;
		            $to_name = $receiverdetail->name;
		            //$to_cc = $getStaffReportingtodetails->emailid;
		            $recipients = array(
		               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
		               $content['staff_detail']->emailid => $content['staff_detail']->name
		               // ..
		            );
		            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
		            if (substr($email_result, 0, 5) == "ERROR") {
		              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
		            }
			        
		        // }

				$insertArray = array(

					'staffid'                => $token,
					'sep_annuation_no'       => $this->input->post('sep_annuation_no'),
					'attaining_age_date'     => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('attaining_age_date')),
					'finance_personnel_date' =>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('finance_personnel_date')),
					'flag'                   => $db_flag,
					'createdby'              => $this->loginData->staffid,
				);

				if($content['annuation_detail'])
				{
					$updateArray = array(

						'staffid'                => $token,
						'sep_annuation_no'       => $this->input->post('sep_annuation_no'),
						'attaining_age_date'     => $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('attaining_age_date')),
						'finance_personnel_date' =>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('finance_personnel_date')),
						'flag'                   => $db_flag,
						'updatedby'              => $this->loginData->staffid,
						'createdon'				 => date("Y-m-d H:i:s"),
						'updatedon'				 => date("Y-m-d H:i:s")

					);
					$this->db->where('id', $content['annuation_detail']->id);
					$flag = $this->db->update('tbl_sepinformation_annuation', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_sepinformation_annuation',$insertArray);
				}
				//echo $this->db->last_query(); die;

				if($flag) 
				{
					$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
					// redirect(current_url());
				} 
				else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				// redirect(current_url());
			}
		}

		$query = "SELECT * FROM tbl_sepinformation_annuation WHERE staffid = ".$token;
		$content['annuation_detail'] = $this->db->query($query)->row();
		/*$content['token'] = $token;*/
		$content['title'] = 'Staff_sepinformtionforsup';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}


}