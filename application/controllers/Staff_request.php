<?php 

/**
* State List
*/
class Staff_request extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Staff_request_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

  $this->load->model("Staff_request_model");

   $content['staff_details'] = $this->Staff_request_model->getstaffRequest();
 //  $content['transfer_request']= $this->Stafftransfer_request_model->get_staffname();
   $content['title'] = 'Staff_list';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }


public function add($token)
 {

    // start permission quest
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

  $this->load->model("Staff_request_model");


 
        $RequestMethod = $this->input->server('REQUEST_METHOD');
        if($RequestMethod == 'POST'){
        //  print_r($this->input->post());

       $getstaffdetail = $this->Staff_request_model->get_staffrequestdetails($token);
       $getstaffcount = $this->Staff_request_model->get_staffrequestcountdetails($token);
       $getstaffrequestid = $this->Staff_request_model->get_staffrequestid($token);
       $requestid = $getstaffrequestid->requested;

     

     // print_r($getstaffdetail); die;

          if ($this->input->post('status')==4) {
              
      
         $insertArr = array(
          'staffid'            => $token,
          'old_office_id'      => $this->input->post('old_office'),
          'new_office_id'      => $getstaffdetail->newofficeid,
          'date_of_transfer'   => $getstaffdetail->requestDate,
          'trans_status'       =>'Transfer',
          'datetime'           => date("Y-m-d H:i:s")
        );
      
      $this->db->insert('staff_transaction', $insertArr);

      $arr=array(
        'doj_team'=>$getstaffdetail->requestDate,
       
      );

      $this->db->where('staffid', $token);
      $this->db->update('staff',$arr);



       $insertworkflowArr = array(
            'r_id'           => 2,
            'type'           => 2,
            'sender'         => $token,
            'receiver'       => $this->loginData->UserID,
            'senddate'       => date("Y-m-d H:i:s"),
            'scomments'      => 'Approved',
            'flag'           => 4,
            'createdon'      => date("Y-m-d H:i:s"),
            'createdby'      => $this->loginData->UserID,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         if ($getstaffcount->rcount >0 ) {

           $insertArr = array(
              
              'status'           => 'Approved',
              'updatedon'        => date("Y-m-d H:i:s"),
              'updatedby'        => $this->loginData->UserID,
            );
           $this->db->where('requested',$requestid);
           $this->db->update('stafftransferrequest', $insertArr);

       
      }
         

          }else{
            

        $insertworkflowArr = array(
            'r_id'           => 2,
            'type'           => 2,
            'sender'         => $token,
            'receiver'       => $this->loginData->UserID,
            'senddate'       => date("Y-m-d H:i:s"),
            'scomments'      => 'Approved',
            'flag'           => 3,
            'createdon'      => date("Y-m-d H:i:s"),
            'createdby'      => $this->loginData->UserID,
         );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         if ($getstaffcount->rcount >0 ) {

           $insertArr = array(
              
              'status'           => 'Rejected',
              'updatedon'        => date("Y-m-d H:i:s"),
              'updatedby'        => $this->loginData->UserID,
            );
           $this->db->where('requested',$requestid);
           $this->db->update('stafftransferrequest', $insertArr);

            }
          }

      $this->session->set_flashdata('tr_msg', 'Transfer approve has been successfully !!!');
     redirect('/Staff_request/index/');




        }

  // $content['staff_details'] = $this->Staff_request_model->getstaffRequest();
   $content['transfer_request']= $this->Staff_request_model->get_staffname();
   $content['title'] = 'Staff_list';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }


 public function stfflistdectivated($token)
 {


  $this->load->model('Staff_list_model');
 $fetch=$this->Staff_list_model->fetchdatas($token);


if($fetch==-1)
{
  $this->session->set_flashdata('er_msg', 'Sorry');

}
else
{

  $updateArrs = array(
    'status'    => 0,
  );
  $this->db->where('staffid', $token);
  $this->db->update('staff', $updateArrs);
  $this->session->set_flashdata('tr_msg', 'Staff is deactivated');
}
 

 
  redirect('/Staff_list/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  
} 


}