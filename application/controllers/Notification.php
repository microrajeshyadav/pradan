<?php
class Notification extends Ci_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_Model");
		$this->load->model(__CLASS__ . '_model');
	}

	function index($id = NULL) {

    $content['Notification_List'] = $this->db->get('mstasha')->result();

		$content['title'] = 'Notification';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	
	function open_notification($id = null){

		// Mark the notification as read 
    $ashaid = $this->uri->segment(3);
		$this->Common_Model->update_data('mstasha',$ashaid,'IsDeleted',$id);


		// Redirect to the section
		$query = "select * from mstasha where IsDeleted = 0";
		$Notification_details = $this->Common_Model->query_data($query);
		
		if(count($Notification_details) < 1){
			$this->session->set_flashdata('er_msg', 'The record does not exist / permission denied. Please contact your system administrator.');

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}

		$this->session->set_flashdata($Notification_details[0]->Msg_Type, $Notification_details[0]->Message);
		$section = $Notification_details[0]->Section;
		$sub_section = $Notification_details[0]->Sub_Section;
		$sectionId = $Notification_details[0]->SectionId;
		
		
		$redirectUrl = site_url($this->getInterfaceName() . '/' . $this->getSectionName($section) . '/' . $sectionId . '/' . $sub_section);
		redirect($redirectUrl);
	}

	

}