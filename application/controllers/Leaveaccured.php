<?php 
class Leaveaccured extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Leaveaccured_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');
		// print_r($check); die();

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');/*
		echo "<pre>";
		print_r($this->loginData);exit();*/

	}

	public function index()
	{

		try{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission  
		//$empcode = $this->model->get_empid($token);
		//$ed_id=$this->model->geted_id();
		//print_r($ed_id);
		//echo $ed_id->staffid;
		
		
		//$content['getreportingdetails']  = $this->model->getreportingtodetails();
		//print_r($content['getreportingdetails']);
		
		$fromdate='';
		$todate = '';
		
		$RequestMethod = $this->input->server("REQUEST_METHOD");

		if($RequestMethod == 'POST')
		{

			
		$currentyear = $this->gmodel->getcurrentfyear();

		// print_r($this->input->post());
		//  die;

		$fromdate = $this->input->post('period');

		$checkaccuredavailaibility = $this->Leaveaccured_model->checkaccuredavailaibility($currentyear, $fromdate);
		//echo $checkaccuredavailaibility;exit();
		if($checkaccuredavailaibility ==1){
			$this->session->set_flashdata('tr_msg', "Leave credited to all the staff successfully");
			redirect('leaveaccured/index/'.$this->loginData->staffid);
		}elseif ($checkaccuredavailaibility ==2) {
			$this->session->set_flashdata('er_msg', "Leave credited already done for the selected period");
			redirect('leaveaccured/index/'.$this->loginData->staffid);
		}else{
			$this->session->set_flashdata('er_msg', "Leave credited rule not define for the selected period or already processed");
			redirect('leaveaccured/index/'.$this->loginData->staffid);
		}


		}
		
		
	    $content['fyear'] = $this->gmodel->getcurrentfyear();

		$content['creditperiod'] = $this->model->getCurrentfyearperiod($content['fyear']);
		$fromdate; $todate;
		foreach ($content['creditperiod'] as $key => $value) {
			  if (date('Y-m-d') <= $value->monthtos && date('Y-m-d') >= $value->monthfroms) {
			  	$fromdate = $value->monthfroms;
			  	$todate = $value->monthtos;
			  }
		}

		echo $fromdate; echo $todate; 
		$content['leavebalance'] = $this->model->getleavedetail_balance($fromdate, $todate);

		// print_r($content['creditperiod']); die;

		$content['leaverequest'] = $this->model->getleavedetails('1');
		$content['title'] = 'Leaveapplication';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


	public function getDaysdiff(){
		try{

		$sql = "select leave_no_days('2018-02-22','2018-02-25')";
		$result = $this->db->query()->result(); 
		return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	

	public function accuredApply(){
		try{
		//$currentmonth = date('m');
		//$currentyear = date('Y');

		$currentyear = $this->getcurrentfyear();

		// print_r($this->input->post());
		//  die;

		$checkaccuredavailaibility = $this->Leaveaccured_model->checkaccuredavailaibility($currentyear);
		//echo $checkaccuredavailaibility;exit();
		if($checkaccuredavailaibility ==1){
			$this->session->set_flashdata('tr_msg', "Leave credited to all the staff successfully .");
			redirect('leaveaccured/index/'.$this->loginData->staffid);
		}elseif ($checkaccuredavailaibility ==2) {
			$this->session->set_flashdata('er_msg', "Leave credited already done for the selected period.");
			redirect('leaveaccured/index/'.$this->loginData->staffid);
		}else{
			$this->session->set_flashdata('er_msg', "Leave credited rule not define for the selected period.");
			redirect('leaveaccured/index/'.$this->loginData->staffid);
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}





}