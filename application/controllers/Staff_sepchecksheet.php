<?php 
class Staff_sepchecksheet extends CI_Controller
{

      ///echo  "zZxZxZX"; die; 
      
      function __construct()
      {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('Staff_sepchecksheet_model');
            $this->load->model("Common_model","Common_Model");
            $this->load->model("Global_model","gmodel");
            $this->load->model("Staff_approval_model");
            $this->load->model("Staff_seperation_model");

            //$this->load->model(__CLASS__ . '_model');
            $mod = $this->router->class.'_model';
          $this->load->model($mod,'',TRUE);
            $this->model = $this->$mod;

            $check = $this->session->userdata('login_data');

            ///// Check Session //////    
            if (empty($check)) {
                   redirect('login');                  
            }

            $this->loginData = $this->session->userdata('login_data');
      
      }

      public function index($token)
      {
            // start permission 
        try{

          // print_r( $this->loginData);
          // die;
            if($token){
              $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
              $content['role_permission'] = $this->db->query($query)->result();
              // end permission 
              $sql=" select max(workflowid) as workflowid from tbl_workflowdetail where  r_id = $token";  
             
              
              $result  = $this->db->query($sql)->row();
               $forwardworkflowid = $result->workflowid; 
            
             
                $sql1="SELECT flag FROM tbl_workflowdetail   WHERE workflowid=$result->workflowid";
                $result1=$this->db->query($sql1)->row();
              $content['workflow_flag']=$result1;
             
             
              $forwardworkflowid = $result->workflowid; 
              // echo $forwardworkflowid;
              // die;
              $query ="SELECT * FROM staff_transaction WHERE id=".$token;
              $content['staff_transaction'] = $this->db->query($query)->row();              
              $content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);

              $content['personaldetail'] = $this->Common_Model->get_Personnal_Staff_List();
             // $content['personaldetail'] = $this->Staff_approval_model->getExecutiveDirectorList(17);

              $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
              // $content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
      

               

      $content['finance_detail'] = $this->Common_Model->get_staff_finance_detail($content['staff_detail']->new_office_id, 20);
      
     $content['ho_detail'] = $this->Common_Model->get_ho_finance_detail(123,22);
    

              $content['token'] = $token;
              $content['tbl_check_sheet_transaction_right'] = '';
              $content['tbl_check_sheet_transaction_left'] = '';
              $content['head1'] = $content['head2'] = $content['head3'] = $content['head4'] = $content['head5'] = $content['head6'] = $content['head7'] = $content['head8'] = $content['head9'] = $content['head10'] = $content['head11'] = $content['head12'] = $content['head13'] = '';
              $content['head'] = array(
                    '1'=>'Pay',
                    '2'=>'HRA',
                    '3'=>'Leave Encashment ( ___ days)',
                    '4'=>'Personal Claim',
                    '5'=>'Provident Fund',
                    '6'=>'Gratuity',
                    '7'=>'Any Other'
              );
              // print_r($content['tbl_check_sheet_transaction_right']);
              // die;
              // if($this->loginData->RoleID==20)
              // {
               $query ="SELECT * FROM tbl_check_sheet WHERE transid=$token order by id desc limit 1";
              //}
              // else if($this->loginData->RoleID==22)
              // {
              //   $query ="SELECT * FROM tbl_check_sheet WHERE transid=$token && isduplicate=1";
              // }

              $content['tbl_check_sheet'] = $this->db->query($query)->row();
              // print_r( $content['tbl_check_sheet']);
              // die;
              $RequestMethod = $this->input->server('REQUEST_METHOD');
              if($RequestMethod == "POST"){
                    $db_flag = '';
                    if($this->input->post('Save') == 'Save'){
                          $db_flag = 0;
                    }else if(trim($this->input->post('comments'))){
                          $db_flag = 1;
                    }
                   
                     //echo "<pre>";
                    // print_r($forwardworkflowid);exit();
                    // print_r($_POST);
                    // die;
              if(trim($this->input->post('comments'))){
                $receiverdetail = $this->Staff_seperation_model->get_staffDetails($this->input->post('personal_administration'));
                // staff transaction table flag update after acceptance
                $updateArr = array(                         
                    'trans_flag'     => $this->input->post('status'),
                    'updatedon'      => date("Y-m-d H:i:s"),
                    'updatedby'      => $this->loginData->staffid
                );

                $this->db->where('id',$token);
                $this->db->update('staff_transaction', $updateArr);
                
                $insertworkflowArr = array(
                 'r_id'                 => $token,
                 'type'                 => 3,
                 'staffid'              => $content['staff_detail']->staffid,
                 'sender'               => $this->loginData->staffid,
                 'receiver'             =>  $this->input->post('personal_administration'),
                 'forwarded_workflowid' => $forwardworkflowid,
                 'senddate'             => date("Y-m-d H:i:s"),
                 'flag'                 => $this->input->post('status'),
                 'scomments'            => $this->input->post('comments'),
                 'createdon'            => date("Y-m-d H:i:s"),
                 'createdby'            => $this->loginData->staffid,
                );
              $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          $sql  = "SELECT filename as Exp_file FROM tbl_levelwisetermination WHERE transid = $token";
          $res  = $this->db->query($sql)->row();

          $sql  = "SELECT filename as Rel_file1 FROM tbl_sep_releaseform WHERE transid = $token";
          $res1  = $this->db->query($sql)->row();
            if(!empty($res->Exp_file) && !empty($res1->Rel_file1)) {
              $attachments = array($res->Exp_file.'.pdf',$res1->Rel_file1.'.pdf');
            }
            else
            {
              $attachments = null;
            }
               

              // $subject = "Your Process Approved";
              //   $body = 'Dear,<br><br>';
              //   $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
              //   $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your process has Approved  .<br><br>';
              //   $body .= 'Thanks<br>';
              //   $body .= 'Administrator<br>';
              //   $body .= 'PRADAN<br><br>';

              //   $body .= 'Disclaimer<br>';
              //   $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

                
              //   $to_email = $receiverdetail->emailid;
              //   $to_name = $receiverdetail->name;
                //$to_cc = $getStaffReportingtodetails->emailid;
                // $recipients = array(
                //    $personnel_detail->EmailID => $personnel_detail->UserFirstName,
                //    $content['staff_detail']->emailid => $content['staff_detail']->name
                //    // ..
                // );
                // $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients,$attachments);
                // if (substr($email_result, 0, 5) == "ERROR") {
                //   $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
                // }
            }

              $data = array(
                    'staffid'=>$content['staff_transaction']->staffid,
                    'transid'=>$token,
                    'date_resignation_submitted_7'=>$this->gmodel->changedate($this->input->post('date_resignation_submitted_7')),
                    'date_from_which_separation_takes_effect_8'=>$this->gmodel->changedate($this->input->post('date_from_which_separation_takes_effect_8')),
                    'whether_under_probation_9'=>$this->input->post('whether_under_probation_9'),
                    'if_not_under_probation_10'=>$this->input->post('if_not_under_probation_10'),
                    'short_fall_in_notice_period_11'=>$this->input->post('short_fall_in_notice_period_11'),
                    'separating_employee_against_12'=>$this->input->post('separating_employee_against_12'),
                    'under_obligation_12'=>$this->input->post('under_obligation_12'),
                    'what_date_13'=>$this->gmodel->changedate($this->input->post('what_date_13')),
                    'pending_against_the_separating_employee_14'=>$this->input->post('pending_against_the_separating_employee_14'),
                    'occupying_accommo_15dation_15'=> $this->input->post('occupying_accommo_15dation_15'),
                    'no_dues_certificate_16'=> $this->input->post('no_dues_certificate_16'),
                    'leave_encashment_17'=> $this->input->post('leave_encashment_17'),
                    'payments_due_to_employee'=> $this->input->post('payments_due_to_employee'),
                    'amounts_recoverable'=> $this->input->post('amounts_recoverable'),
                    'net_payable_recoverable'=>$this->input->post('net_payable_recoverable'),
                    'createdon'=>date('Y-m-d'),
                    'createdby'=>$this->loginData->staffid,
                    'flag'=>$db_flag
              );
              
              // $this->db->insert('tbl_check_sheet', $data);

                          //$flag = $this->db->update('tbl_check_sheet', $data_update)

              if($content['tbl_check_sheet']){
                    $data_update = array(
                    'staffid'=>$content['staff_transaction']->staffid,
                    'transid'=>$token,
                    'date_resignation_submitted_7'=>$this->gmodel->changedate($this->input->post('date_resignation_submitted_7')),
                    'date_from_which_separation_takes_effect_8'=>$this->gmodel->changedate($this->input->post('date_from_which_separation_takes_effect_8')),
                    'whether_under_probation_9'=>$this->input->post('whether_under_probation_9'),
                    'if_not_under_probation_10'=>$this->input->post('if_not_under_probation_10'),
                    'short_fall_in_notice_period_11'=>$this->input->post('short_fall_in_notice_period_11'),
                    'separating_employee_against_12'=>$this->input->post('separating_employee_against_12'),
                    'under_obligation_12'=>$this->input->post('under_obligation_12'),
                    'what_date_13'=>$this->gmodel->changedate($this->input->post('what_date_13')),
                    'pending_against_the_separating_employee_14'=>$this->input->post('pending_against_the_separating_employee_14'),
                    'occupying_accommo_15dation_15'=>$this->input->post('occupying_accommo_15dation_15'),
                    'no_dues_certificate_16'=>$this->input->post('no_dues_certificate_16'),
                    'leave_encashment_17'=>$this->input->post('leave_encashment_17'),
                    'payments_due_to_employee'=>$this->input->post('payments_due_to_employee'),
                    'amounts_recoverable'=>$this->input->post('amounts_recoverable'),
                    'net_payable_recoverable'=>$this->input->post('net_payable_recoverable'),
                    'updatedon'=>date('Y-m-d H:i:s'),
                    'updatedby'=>$this->loginData->staffid,
                    'flag'=>$db_flag
              );
                   
                          $this->db->where('id', $content['tbl_check_sheet']->id);
                          $flag = $this->db->update('tbl_check_sheet', $data_update);

                          if($this->input->post('head1') !=0){
                    $data_tbl_check_sheet_transaction['head1'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>$this->input->post('head1'),
                          'amount'=>$this->input->post('amount1')
                    );
              }
                          if($this->input->post('head2') !=0){
                    $data_tbl_check_sheet_transaction['head2'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>$this->input->post('head2'),
                          'amount'=>$this->input->post('amount2')
                    );
              }
              if($this->input->post('head3') !=0){
                    $data_tbl_check_sheet_transaction['head3'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>$this->input->post('head3'),
                          'amount'=>$this->input->post('amount3')
                    );
              }
              if($this->input->post('head4') !=0){
                    $data_tbl_check_sheet_transaction['head4'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>$this->input->post('head4'),
                          'amount'=>$this->input->post('amount4')
                    );
              }
              if($this->input->post('head5') !=0){
                    $data_tbl_check_sheet_transaction['head5'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>$this->input->post('head5'),
                          'amount'=>$this->input->post('amount5')
                    );
              }
              if($this->input->post('head6') !=0){
                    $data_tbl_check_sheet_transaction['head6'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>$this->input->post('head6'),
                          'amount'=>$this->input->post('amount6')
                    );
              }
              $data_tbl_check_sheet_transaction['head7'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>7,
                          'amount'=>$this->input->post('amount7')
                    );
              $data_tbl_check_sheet_transaction['head8'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>8,
                          'amount'=>$this->input->post('amount8')
                    );
              $data_tbl_check_sheet_transaction['head9'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>9,
                          'amount'=>$this->input->post('amount9')
                    );
              $data_tbl_check_sheet_transaction['head10'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>10,
                          'amount'=>$this->input->post('amount10')
                    );
              $data_tbl_check_sheet_transaction['head11'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>11,
                          'amount'=>$this->input->post('amount11')
                    );
              $data_tbl_check_sheet_transaction['head12'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>12,
                          'amount'=>$this->input->post('amount12')
                    );
              $data_tbl_check_sheet_transaction['head13'] = array(
                          'check_sheet_id'=>$content['tbl_check_sheet']->id,
                          'headid'=>13,
                          'amount'=>$this->input->post('amount13')
                    );
              
              $this->db->where('check_sheet_id',$content['tbl_check_sheet']->id);
              $this->db->delete('tbl_check_sheet_transaction');
              foreach ($data_tbl_check_sheet_transaction as $value) {
                    # code...
                    $this->db->insert('tbl_check_sheet_transaction',$value);
              }
                    }else{
                          $flag = $this->db->insert('tbl_check_sheet',$data);
                          $last_insert_id = $this->db->insert_id();
              if($this->input->post('head1') !=0){
                    $data_tbl_check_sheet_transaction['head1'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>$this->input->post('head1'),
                          'amount'=>$this->input->post('amount1')
                    );
              }
              if($this->input->post('head2') !=0){
                    $data_tbl_check_sheet_transaction['head2'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>$this->input->post('head2'),
                          'amount'=>$this->input->post('amount2')
                    );
              }
              if($this->input->post('head3') !=0){
                    $data_tbl_check_sheet_transaction['head3'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>$this->input->post('head3'),
                          'amount'=>$this->input->post('amount3')
                    );
              }
              if($this->input->post('head4') !=0){
                    $data_tbl_check_sheet_transaction['head4'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>$this->input->post('head4'),
                          'amount'=>$this->input->post('amount4')
                    );
              }
              if($this->input->post('head5') !=0){
                    $data_tbl_check_sheet_transaction['head5'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>$this->input->post('head5'),
                          'amount'=>$this->input->post('amount5')
                    );
              }
              if($this->input->post('head6') !=0){
                    $data_tbl_check_sheet_transaction['head6'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>$this->input->post('head6'),
                          'amount'=>$this->input->post('amount6')
                    );
              }
              $data_tbl_check_sheet_transaction['head7'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>7,
                          'amount'=>$this->input->post('amount7')
                    );
              $data_tbl_check_sheet_transaction['head8'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>8,
                          'amount'=>$this->input->post('amount8')
                    );
              $data_tbl_check_sheet_transaction['head9'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>9,
                          'amount'=>$this->input->post('amount9')
                    );
              $data_tbl_check_sheet_transaction['head10'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>10,
                          'amount'=>$this->input->post('amount10')
                    );
              $data_tbl_check_sheet_transaction['head11'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>11,
                          'amount'=>$this->input->post('amount11')
                    );
              $data_tbl_check_sheet_transaction['head12'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>12,
                          'amount'=>$this->input->post('amount12')
                    );
              $data_tbl_check_sheet_transaction['head13'] = array(
                          'check_sheet_id'=>$last_insert_id,
                          'headid'=>13,
                          'amount'=>$this->input->post('amount13')
                    );
              foreach ($data_tbl_check_sheet_transaction as $value) {
                  
                    $this->db->insert('tbl_check_sheet_transaction',$value);
              }
                    }
                    
          if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');} 
          else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');}
        }
        // if($this->loginData->RoleID==22)
        // {
         $query ="SELECT * FROM tbl_check_sheet WHERE transid=$token  order by id desc limit 1";
         // }
         // else if($this->loginData->RoleID==20)
         // {
         //  $query ="SELECT * FROM tbl_check_sheet WHERE transid=$token && isduplicate=0";
         // }

          $content['tbl_check_sheet'] = $this->db->query($query)->row();
          /*echo "<pre>";
          print_r($content['tbl_check_sheet']);exit();*/
          if($content['tbl_check_sheet']){
                $query ="SELECT * FROM tbl_check_sheet_transaction WHERE check_sheet_id=".$content['tbl_check_sheet']->id." AND headid >=7";
                $content['tbl_check_sheet_transaction_right'] = $this->db->query($query)->result();
                $query ="SELECT * FROM tbl_check_sheet_transaction WHERE check_sheet_id=".$content['tbl_check_sheet']->id." AND headid < 7";
                $content['tbl_check_sheet_transaction_left'] = $this->db->query($query)->result();
                /*echo "<pre>";*/
                
                if($content['tbl_check_sheet_transaction_left']){
                      foreach ($content['tbl_check_sheet_transaction_left'] as $value) {
                            # code...
                            $content['head'.$value->headid] = array(
                                  'headid'=> $value->headid,
                                  'amount'=> $value->amount
                            );
                      }
                }
          }
          /*echo "<pre>";
          print_r($data_tbl_check_sheet_transaction['head1']);
          exit();*/ 
            
    



          $content['title'] = 'Staff_sepchecksheet';
          $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
          $this->load->view('_main_layout', $content);
        }else{
          $this->session->set_flashdata('er_msg','Missing trans id!!');
          header("location:javascript://history.go(-1)", 'refresh');
        }
        }
        catch(Exception $e)
        {
            print_r($e->getMessage());
            die();
        }
            
      }
        public function check_duplicaterecord($token)
        {
          try{
           

            $content['checksheet_detail'] = $this->model->check_duplicaterecord($token);
             redirect('Staff_sepchecksheet/index/'.$token); 
            // print_r($content['checksheet_detail']);

            // die;

           
            $content['title'] = 'Staff_sepchecksheet';
          $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
          $this->load->view('_main_layout', $content);

          }
        catch(Exception $e)
        {
            print_r($e->getMessage());
            die();
        }
        }

      
}