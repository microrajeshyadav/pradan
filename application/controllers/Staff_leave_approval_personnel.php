<?php 

/**
* State Review
*/
class Staff_leave_approval_personnel  extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model('Dompdf_model');
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index($token)
 {
  try{
// echo $staffid; die;
    // start permission 

   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();

  // end permission 
   $content['staff_code'] = $this->Staff_leave_approval_personnel_model->get_empid($this->loginData->staffid);
// echo "<pre>";
//    print_r($content['staff_code']);
//     die();

   // $staff = $content['leaverequest']->staffid;
   // echo $staff; die;
  
   // print_r($content['forid']); die;

   $content['id'] = $this->Staff_leave_approval_personnel_model->getEDid();
      // print_r($content['id']); die;
   $var = $content['id']->edid; 
   $edname = $content['id']->UserFirstName.' '.$content['id']->UserMiddleName.' '.$content['id']->UserLastName;


   $this->load->model('Staff_review_model');

  // echo $content['staff_code']->emp_code; die;

   $content['leaverequest'] = $this->Staff_leave_approval_personnel_model->getrequestedleave($content['staff_code']->emp_code);
  





   // $staff = $content['leaverequest']->staffid;
   // echo $staff; die;

    // $content['forid'] = $this->Staff_leave_approval_personnel_model->get_providentworkflowid($staff);
    // print_r($content['forid']); die;

   $currentdate = date('Y-m-d');

    $txtgender='';
    $title = '';
   $fromdate = $this->gmodel->changedatedbformate($content['leaverequest']->From_date);
   $todate = $this->gmodel->changedatedbformate($content['leaverequest']->To_date);

   if ($content['leaverequest']->gender==1) {
     $txtgender = 'Male';
     $title = 'Mr.';
   }elseif ($content['leaverequest']->gender == 2) {
     $txtgender = 'Female';
     $title = 'Ms.';
   }else{
     $txtgender = 'other';
     $title = 'Mr./Ms.';
   }
   $gender = 

   $RequestMethod = $this->input->server('REQUEST_METHOD');

      if($RequestMethod == 'POST'){

    $this->db->trans_start();

   $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td width="1%">&nbsp;</td>
    <td width="28%">&nbsp;</td>
    <td width="28%">&nbsp;</td>
    <td width="7%">&nbsp;</td>
    </tr>
    <tr>
    <td colspan="4"><p align="center"><strong> LETTER OF LEAVE WITH PAY</strong></p></td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>Ref.:  301-'.$content['leaverequest']->Emp_code.'/PDR/</td>
    <td align="right">DATE  :'. $this->gmodel->changedatedbformate($currentdate) .'</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>'.$title.''.$content['leaverequest']->name.'</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>'.$content['leaverequest']->officename.','.$content['leaverequest']->districtname.','.$content['leaverequest']->statename.'</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>Dear '.$content['leaverequest']->name.',</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td colspan="2"><p>I am happy to approve your request for leave as leave with pay  with effect from '.$fromdate.' to '.$todate.'.</p></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>With  all my good wishes.</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><p>Yours sincerely,</p></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">('.$edname.')</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><p>Executive  Director</p></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    </table>';


    $filename = $content['leaverequest']->Emp_code.'-'.md5(time() . rand(1,1000));
    $this->load->model('Dompdf_model');
    $generate =   $this->Dompdf_model->leave_letter_PDF($html, $filename, NULL,'filename.pdf');

    if($generate==true){
      // $insertarray = array(
      //   'empcode'         => $content['leaverequest']->Emp_code,
      //   'sanctiondate'    => date('Y-m-d H:i:s'),
      //   'filename'        => (trim($filename)==""?null:($filename)), 
      //   'createdby'       => $this->loginData->staffid,
      //   'createdon'       => date('Y-m-d H:i:s'),
      // );
      // // print_r($insertarray); die;
      // $this->db->insert('tbl_leave_sanction_letter',$insertarray);



      // $insertworkflowArr = array(
      //      'type'     =>  5,
      //      'r_id'      => $content['leaverequest']->id,
      //      'sender'    => $this->loginData->staffid,
      //      'receiver'  => $staffid,
      //      'senddate'  => date('Y-m-d'),
      //      'forwarded_workflowid' => $content['forid']->workflow_id,
      //      'createdon' => date('Y-m-d H:i:s'),
      //      'createdby' => $this->loginData->staffid,
      //      'flag'      => 5,
      //      'staffid'   => $staff,
      // );
      // // print_r($insertworkflowArr); die;
      // $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
      // $this->db->trans_complete();



          if ($this->db->trans_status() === true){

            $personalmail = $this->loginData->EmailID;

            // echo $personalmail; die;

              $filename = $filename; 
                 $attachments = array($filename.'.pdf');


               // $mail =  $this->Common_Model->send_email($subject = 'Send sanction letter by pradan',$message = $html, $personalmail, $attachments); 

               // echo $mail; die;
                  $this->session->set_flashdata('tr_msg', 'send mail successfully !!!');
      redirect('/Staff_leave_approval_personnel/index/'.$staff);  
    }else{
      $this->session->set_flashdata('er_msg', 'Error !!! Mail not send  !!!');
      redirect(current_url());
    }


          }

     }

  




   //$content['staff_seperation'] = $this->Staff_review_model->get_staff_seperation_detail();
   /*echo "<pre>";
   print_r($content['staff_seperation']);exit();*/
   $content['staff_details'] = $this->Staff_review_model->getstaffname();
   

   //print_r($content['staff_details']);




   $content['title'] = 'Staff_leave_approval_personnel';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }



public function sendsanctionletter($token=null,$id=null)
{
  try{

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

 $staff_code = $this->Staff_leave_approval_personnel_model->get_empid($this->loginData->staffid);
 //print_r($staff_code); 


   $content['id'] = $this->Staff_leave_approval_personnel_model->getEDid();
   
   $var = $content['id']->edid; 
   $edname = $content['id']->UserFirstName.' '.$content['id']->UserMiddleName.' '.$content['id']->UserLastName;


  // echo $content['staff_code']->emp_code; die;

   $content['leaverequest'] = $this->Staff_leave_approval_personnel_model->getrequestedleave($content['staff_code']->emp_code);
  
// echo "<pre>";

// print_r($content['leaverequest']);

// die;


   // $staff = $content['leaverequest']->staffid;
   // echo $staff; die;

    // $content['forid'] = $this->Staff_leave_approval_personnel_model->get_providentworkflowid($staff);
    // print_r($content['forid']); die;

   $currentdate = date('Y-m-d');

    $txtgender='';
    $title = '';
   $fromdate = $this->gmodel->changedatedbformate($content['leaverequest']->From_date);
   $todate = $this->gmodel->changedatedbformate($content['leaverequest']->To_date);

   if ($content['leaverequest']->gender==1) {
     $txtgender = 'Male';
     $title = 'Mr.';
   }elseif ($content['leaverequest']->gender == 2) {
     $txtgender = 'Female';
     $title = 'Ms.';
   }else{
     $txtgender = 'other';
     $title = 'Mr./Ms.';
   }
   $gender = 

   

    $this->db->trans_start();

   $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td width="1%">&nbsp;</td>
    <td width="28%">&nbsp;</td>
    <td width="28%">&nbsp;</td>
    <td width="7%">&nbsp;</td>
    </tr>
    <tr>
    <td colspan="4"><p align="center"><strong> LETTER OF LEAVE WITH PAY</strong></p></td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>Ref.:  301-'.$content['leaverequest']->Emp_code.'/PDR/</td>
    <td align="right">DATE  :'. $this->gmodel->changedatedbformate($currentdate) .'</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>'.$title.''.$content['leaverequest']->name.'</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>'.$content['leaverequest']->officename.','.$content['leaverequest']->districtname.','.$content['leaverequest']->statename.'</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>Dear '.$content['leaverequest']->name.',</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td colspan="2"><p>I am happy to approve your request for leave as leave with pay  with effect from '.$fromdate.' to '.$todate.'.</p></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><p>With  all my good wishes.</p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><p>Yours sincerely,</p></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">('.$edname.')</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><p>Executive  Director</p></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    </table>'; die;


    $filename = $content['leaverequest']->Emp_code.'-'.md5(time() . rand(1,1000));
    $this->load->model('Dompdf_model');
    $generate =   $this->Dompdf_model->leave_letter_PDF($html, $filename, NULL,'filename.pdf');

    if($generate==true){
    


          if ($this->db->trans_status() === true){

            $personalmail = $this->loginData->EmailID;

            // echo $personalmail; die;

              $filename = $filename; 
                 $attachments = array($filename.'.pdf');


               // $mail =  $this->Common_Model->send_email($subject = 'Send sanction letter by pradan',$message = $html, $personalmail, $attachments); 

               // echo $mail; die;
                  $this->session->set_flashdata('tr_msg', 'send mail successfully !!!');
      redirect('/Staff_leave_approval_personnel/index/'.$staff);  
    }else{
      $this->session->set_flashdata('er_msg', 'Error !!! Mail not send  !!!');
      redirect(current_url());
    }


          }




 





 
 $content['subview'] = 'Staff_leave_approval_personnel/approve';
 $this->load->view('_main_layout', $content);

 }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}



}