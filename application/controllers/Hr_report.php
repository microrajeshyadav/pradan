<?php 
class Hr_report extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_Model","gmodel");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
		
		if ($this->loginData->UserID =='') {
			redirect('login');
		}
		
	}
	
 
	public function index()
	{
		try{
		$this->load->model('Hr_report_model');
		
		$getbatch =  $this->model->getbatch();

	
		if($this->input->post('batch')!='') {
			$batchid = $this->input->post('batch'); 
		}else{
			$batchid = $getbatch[0]->id;
			// $batchid = '';
		}


		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
	
		}

		

		$content['batchid'] = $batchid;
		
		$content['offeredaccepted']  = $this->model->getState_Institute_Offered($batchid);

		$content['dashboardstat']  = $this->model->getdashboardstat();
		// $content['offeredacceptedcandidate']   = $this->model->getState_Institute_Offered_Accepted($batchid); 

		$content['getdcname']  = $this->model->getDC();
		


		$content['batchlist']  = $this->model->getfinancialyear();
		$content['policylist']  = $this->model->getpolicylist();
		
		$content['candidate_details'] = $this->model->candidate_details();
		// echo "<pre>";
		// print_r($content['candidate_details']); die;
		
		$content['title'] = 'Hr_report';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function accepted_candidate_list($token){
		try{
		
		 $batchid = $this->uri->segment(3);
		 $campusid = $this->uri->segment(4);

		$content['offeredacceptrdlist']  = $this->model->getState_Institute_Offered_Accepted_List($batchid,$campusid);
	
		$content['batchlist']  = $this->model->getbatch();

		$content['title'] = 'Hr_report';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function joincandidate_list($batchid ,$teamid ){

		try{
		// $getbatch =  $this->model->getbatch();
		// if ($this->input->post('batch')!='') {
		// 	$batchid = $this->input->post('batch');
		// }else{
		// 	$batchid = $getbatch->batch;
		// }


		$content['joincandidatelist']  = $this->model->getJoin_Candidate_List($batchid,$teamid);
	
		$content['batchlist']  = $this->model->getbatch();

		$content['title'] = 'Hr_report';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

}