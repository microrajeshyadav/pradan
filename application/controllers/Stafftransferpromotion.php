<?php 

/**
* State List
*/
class Stafftransferpromotion extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Stafftransferpromotion_model");
    $this->load->model("Staff_approval_model");
    $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_seperation_model","Staff_seperation_model");


    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {


    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $query = "select * from msleavecrperiod";
   $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

   $content['title'] = 'Stafftransferpromotion';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }
 public function Add($token)
 {
    //echo "staff=".$token;


  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

  $tablename = "'staff_transaction'";
  $incval = "'@a'";
  $Staffdetails =  $this->Stafftransferpromotion_model->getstaffDetails($token);
   $supervisor   =  $Staffdetails->reportingto;
  /*echo "<pre>";
  print_r($Staffdetails->reportingto);exit;*/
  $content['token'] = $token;
  $content['staff'] = $this->Stafftransferpromotion_model->getstaffname($token);
  

  $content['new_designation'] = $this->Stafftransferpromotion_model->get_alldesignation();
  $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();


  $content['d_tranfer'] = $this->Stafftransferpromotion_model->get_alloffice();
  $content['transfer_promption'] = $this->Stafftransferpromotion_model->get_staffDetails($token);

   //print_r($content['transfer_promption']);
  $p_office=$content['transfer_promption']->new_office_id;
  $p_designation=$content['transfer_promption']->new_designation;
  $content['all_office']=$this->Stafftransferpromotion_model->get_alloffice($p_office);
   //print_r($content['all_office']);
  $content['all_designation']=$this->Stafftransferpromotion_model->get_alldesignation($p_designation);

  $getstaffincrementalid = $this->Stafftransferpromotion_model->getStaffTransactioninc($tablename,$incval); 
  $autoincval =  $getstaffincrementalid->maxincval;
  $getStaffReportingtodetails =$this->Common_Model->get_staffReportingto($token);
  /*echo "<pre>";
  print_r($getStaffReportingtodetails);exit();*/

  $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
  $result  = $this->db->query($sql)->result()[0];

  
  $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   // echo "methods=".$RequestMethod;


  if($RequestMethod == 'POST'){



   $this->db->trans_start();
   $this->db->trans_strict(FALSE);

   $date1=$this->input->post('fromdate');
   $date2=$this->gmodel->changedatedbformate($date1);

   $designation=$this->input->post('new_designation');
   $changereportingto = $this->input->post('changereportingto');
        if ($changereportingto == 'on') 
        {
          $changereportingto = 1;
        }
        else
        {
          $changereportingto = 0;
        }

   $transstatus = 'BOTH';
    $sql = "SELECT count(staffid) as staffcount FROM staff_transaction WHERE trans_status ='".$transstatus."' AND staffid='".$token."' AND date_of_transfer ='".$date2."'";

    // echo $sql; die;

     $result = $this->db->query($sql)->row();
     // echo $result->staffcount; die;
     if($result->staffcount == 0) {

   $insertArr = array(
    'id'               => $autoincval,
    'staffid'          => $token,
    'old_office_id'    => $this->input->post('old_office'),
    'old_designation'  => $this->input->post('Presentdesignation'),
    'new_designation'  => $designation,
    'new_office_id'    => $this->input->post('transfer_office'),
    'reportingto'      => $this->input->post('reportingto'),
    'date_of_transfer' => $date2,
    'reason'           => $this->input->post('reason'),
    'changereporting'    => $changereportingto,
    'trans_status'     => 'BOTH',
    'datetime'         => date("Y-m-d H:i:s"),
    'createdon'        => date("Y-m-d H:i:s"),
    'createdby'        => $this->loginData->staffid,
    'trans_flag'       => 1,
  );

   $this->db->insert('staff_transaction', $insertArr); 


   $insertworkflowArr = array(
     'r_id'                 => $autoincval,
     'type'                 => 6,
     'staffid'              => $token,
     'sender'               => $this->loginData->staffid,
     'receiver'             => $supervisor,
     'senddate'             => date("Y-m-d H:i:s"),
     'flag'                 => 1,
     'scomments'            => $this->input->post('remark'),
     'createdon'            => date("Y-m-d H:i:s"),
     'createdby'            => $this->loginData->staffid,
   );

   $this->db->insert('tbl_workflowdetail', $insertworkflowArr);




   $this->db->trans_complete();

   if ($this->db->trans_status() === FALSE){
    $subject = "Your Request Submited Successfully";
    $body = 'Dear,<br><br>';
    $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
    $body .= 'This is to certify that Mr. '.$content['transfer_promption']->name.'<br> has '.$this->input->post('trans_status').' .<br><br>';
    $body .= 'Thanks<br>';
    $body .= 'Administrator<br>';
    $body .= 'PRADAN<br><br>';

    $body .= 'Disclaimer<br>';
    $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

    $to_email = $content['transfer_promption']->emailid;
    $to_name = $content['transfer_promption']->name;
    //$to_cc = $getStaffReportingtodetails->emailid;
    $recipients = array(
       $getStaffReportingtodetails->emailid => $getStaffReportingtodetails->name,
       $personnel_detail->EmailID => $personnel_detail->UserFirstName
       // ..
    );
    $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
    if (substr($email_result, 0, 5) == "ERROR") {
      $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
    }

    $this->session->set_flashdata('er_msg', 'Error added  Staff Transfer and Promotion'); 
    redirect(current_url());
  }else{
   $this->session->set_flashdata('tr_msg', 'Successfully added  Staff Transfer and Promotion');
   redirect('/Stafftransferpromotion/history/'.$token);

 }   
  }else{
      $this->session->set_flashdata('er_msg', "Transfer & Promotion already under process !!");
        redirect('/Stafftransferpromotion/add/'.$token);


   
}

}

$content['subview'] = 'Stafftransferpromotion/add';
$this->load->view('_main_layout', $content);
}


public function history($token)
{
 $content['token']=$token;


 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
 $content['view_history']=$this->Stafftransferpromotion_model->getview_history($token);
    // print_r($result['view_history']);
// end permission 

   // $query = "select * from msleavecrperiod";
    //$content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

 $content['title'] = 'Stafftransferpromotion';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);   
}

  public function approve(){
      // start permission 
      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
      $content['role_permission'] = $this->db->query($query)->result();
      // end permission 


      $staffid = $this->uri->segment(3);
      $tarnsid  = $this->uri->segment(4);
   
      $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail 
      WHERE r_id = $tarnsid";
      $result  = $this->db->query($sql)->result()[0];


      $forwardworkflowid = $result->workflowid;
      if ($this->loginData->RoleID == 18) { // Executive Director
        if($result->flag == 5){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 7;
          $unflag = 8;
          $content['addflag'] = 7;
          $content['addunflag'] = 8;
        }else if($result->flag == 15){
          $transfer_to_id = 17;
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Ed_staff_approval';
          $flag = 17;
          $unflag = 18;
          $content['addflag'] = 17;
          $content['addunflag'] = 18;
        }
      }else if($this->loginData->RoleID == 17){ // 17 personal Admin
        if($result->flag == 7){
          $content['heading_text'] = 'Staff';
          $content['redirect_controller'] = 'Staff_personnel_approval';
          $transfer_to_id = 18;
          $flag = 9;
          $unflag = 10;
        }
      }else if($this->loginData->RoleID == 20){ // Finance Admin
        $content['heading_text'] = 'Personnel Administrator';
        $content['redirect_controller'] = 'Staff_finance_approval';
        $transfer_to_id = 17;
        $flag = 9;
        $unflag = 10;
      }else if($this->loginData->RoleID == 2){ // Superviser TC
        if($result->flag == 1){
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 17;
          $flag = 3;
          $unflag = 4;
        }
      }else if($this->loginData->RoleID == 21){ // Superviser TC
        if($result->flag == 1){
          $content['heading_text'] = 'Personnel Administrator';
          $content['redirect_controller'] = 'Staff_approval';
          $transfer_to_id = 17;
          $flag = 3;
          $unflag = 4;
        }
      }
      $RequestMethod = $this->input->server('REQUEST_METHOD'); 
      $content['getexecutivedirector'] = $this->Staff_approval_model->getExecutiveDirectorList($transfer_to_id);
      $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
      $content['staffdetail']=$this->Staff_seperation_model->get_staffDetails($staffid);
      if($RequestMethod == 'POST'){
        $transid = $this->input->post('tarnsid');
        $status = $this->input->post('status');
        $executivedirector_administration = $this->input->post('executivedirector_administration');
        if($executivedirector_administration){
          $receiverdetail = $this->Staff_seperation_model->get_staffDetails($executivedirector_administration);
        }
      /*echo "<pre>";
      print_r($receiverdetail);
      exit();*/
        $transferstaffdetails = $this->Staff_approval_model->getTransferStaffDetails($transid);

        $NewOfficeId = $transferstaffdetails->new_office_id;    ////  New Office Id 
        $NewDateOfTransfer = $transferstaffdetails->date_of_transfer; ///// New Date Of Transfer
        $NewReportingto = $transferstaffdetails->reportingto;  ////////// New Team Reporting to.
        $getpersonnaluser = $this->Staff_approval_model->getPersonalUserList();
        $arrr = array();
       foreach ($getpersonnaluser as $key => $value)
       {
        array_push($arrr, $value->personnelstaffid);
       }


        if($status == 4){
        $this->form_validation->set_rules('comments','Comments','trim|required|min_length[1]|max_length[250]');
        }
        $this->form_validation->set_rules('status','Select Status','trim|required');
        
        if($this->form_validation->run() == TRUE){
        /*$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

        $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;*/
          echo $status.$this->form_validation->run();
          /*echo "<br>".$flag;
          print_r($_POST);
          exit;*/

        $this->db->trans_start();
        if($status == $flag){     //// Status => Approve 

          $updateArr = array(

            'trans_flag'     => $flag,
            'updatedon'      => date("Y-m-d H:i:s"),
            'updatedby'      => $this->loginData->staffid
          );

          $this->db->where('id',$transid);
          $this->db->update('staff_transaction', $updateArr);

          foreach ($arrr as $key => $value) {
          $insertworkflowArr = array(

           'r_id'                 => $transid,
           'type'                 => 6,
           'staffid'              => $staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $value,
           'forwarded_workflowid' => $forwardworkflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => $flag,
           'scomments'            => $this->input->post('comments'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
          }

          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
            redirect(current_url());

          }else{
            $subject = "Your Process Approved";
            $body = 'Dear,<br><br>';
            $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
            $body .= 'This is to certify that Mr. '.$content['staffdetail']->name.'<br> your process has Approved  .<br><br>';
            $body .= 'Thanks<br>';
            $body .= 'Administrator<br>';
            $body .= 'PRADAN<br><br>';

            $body .= 'Disclaimer<br>';
            $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

            
            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
               $personnel_detail->EmailID => $personnel_detail->UserFirstName,
               $content['staffdetail']->emailid => $content['staffdetail']->name
               // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            if (substr($email_result, 0, 5) == "ERROR") {
              $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
            }
            $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          //redirect('/Staff_approval/'.$token);
            redirect($content['redirect_controller']);

          }

        }
        if ($status ==$unflag) {

         $insertArr = array(

          'trans_flag'     => $unflag,
          'updatedon'      => date("Y-m-d H:i:s"),
          'updatedby'      => $this->loginData->staffid
        );

         $this->db->where('id',$transid);
         $this->db->update('staff_transaction', $insertArr);


         $insertworkflowArr = array(
           'r_id'           => $transid,
           'type'           => 6,
           'staffid'        => $staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => $unflag,
           'scomments'      => $this->input->post('comments'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );
         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

         $this->db->trans_complete();


         if ($this->db->trans_status() === FALSE){

          $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
          redirect(current_url());

        }else{

          $this->session->set_flashdata('tr_msg', 'Approval has been done successfully !!!');
          redirect('/Staff_approval/'.$token);

        }
      }
    }
  }

 /*echo "<pre>";
    print_r($transfer_to_id);exit();*/
    prepareview:

    $content['staffid'] = $staffid;
    $content['tarnsid'] = $tarnsid;

    $content['getpersonnaluser'] = $this->Staff_approval_model->getPersonalUserList();
    
    $content['subview'] = 'Stafftransferpromotion/approve';
    $this->load->view('_main_layout', $content);
}


}