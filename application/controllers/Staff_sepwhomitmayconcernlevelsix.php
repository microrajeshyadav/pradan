<?php 
class Staff_sepwhomitmayconcernlevelsix extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepwhomitmayconcernlevelsix_model');
		$this->load->model("Common_model","Common_Model");
      $this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission    
			$query ="SELECT * FROM staff_transaction WHERE id=".$token;
			$content['staff_transaction'] = $this->db->query($query)->row();
			$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
			$content['staff_transaction_detail'] = $this->Staff_sepwhomitmayconcernlevelsix_model->staff_transaction_detail($content['staff_detail']->staffid);
			$query ="SELECT * FROM tbl_levelwisetermination WHERE transid=".$token." AND type=1";
			$content['tbl_levelwisetermination'] = $this->db->query($query)->row();
			/*echo "<pre>";
			print_r($content['staff_detail']);exit();*/
			$RequestMethod = $this->input->server('REQUEST_METHOD');
        	if($RequestMethod == "POST"){
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'Save And Submit'){
					$db_flag = 1;
				}

        		$data = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$token,
					'sep_no'             => $this->input->post('sep_no'),
        			'desid'=>$content['staff_detail']->sepdesigid,
        			'total_staff'=>$this->input->post('total_staff'),
        			'total_professional'=>$this->input->post('total_professional'),
        			'type'=>1,
        			'createdby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
        		if($content['tbl_levelwisetermination']){
        			$data_update = array(
	        			'staffid'=>$content['staff_transaction']->staffid,
	        			'transid'=>$token,
						'sep_no'             => $this->input->post('sep_no'),
	        			'desid'=>$content['staff_detail']->sepdesigid,
	        			'total_staff'=>$this->input->post('total_staff'),
	        			'total_professional'=>$this->input->post('total_professional'),
	        			'type'=>1,
	        			'updatedon'=>date('Y-m-d H:i:s'),
	        			'updatedby'=>$this->loginData->staffid,
	        			'flag'=>$db_flag
	        		);
					$this->db->where('id', $content['tbl_levelwisetermination']->id);
					$flag = $this->db->update('tbl_levelwisetermination', $data_update);
				}else{
					$flag = $this->db->insert('tbl_levelwisetermination',$data);
				}
				
				if($flag) {
					$subject = "Your Request Submited Successfully";
				  $body = 'Dear,<br><br>';
				  $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
				  $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> has issued experience certificate.<br><br>';
				  $body .= 'Thanks<br>';
				  $body .= 'Administrator<br>';
				  $body .= 'PRADAN<br><br>';

				  $body .= 'Disclaimer<br>';
				  $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

				  $to_email = $content['staff_detail']->emailid;
				  $to_name = $content['staff_detail']->name;
				  $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name);
				  if (substr($email_result, 0, 5) == "ERROR") {
				  	$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
				  }
					$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
				}else {
					$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				}
        	}
			$query ="SELECT * FROM tbl_levelwisetermination WHERE transid=".$token." AND type=1"; 
			$content['tbl_levelwisetermination'] = $this->db->query($query)->row();
			$content['title'] = 'Staff_sepwhomitmayconcernlevelsix';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}

	
}