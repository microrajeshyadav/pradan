<?php 
class Joining_of_da_document_verification extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');

		$check = $this->session->userdata('login_data');
		///// Check Session //////
		if(empty($check))
		{
			redirect('login');
		}

	}


	public function index()
	{
		
		$this->load->model('Joining_of_da_document_verification_model');
	
		$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($this->loginData->teamid);
		$content['title'] = 'Joining_of_da_document_verification';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}


/**
   * Method GeneralNominationWithWitnessform() get General Nomination With Witnessform.
   * @access  public
   * @param Null
   * @return  Array
   */

public function GeneralNominationWithWitnessform($token)
{

	try{

		if (empty($token)) {

			$this->session->set_flashdata('tr_msg', 'Token is not found !!!');	
			redirect('/Joining_of_da_document_verification/index');

		}else{

			$this->load->model('Joining_of_da_document_verification_model');

			$this->db->trans_start();

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){

				$savedata = $this->input->post('savebtn');

				if (!empty($savedata) && $savedata =='senddatasave') {

					$InsertArrayData = array(
			'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
						'first_witness_name'  => $this->input->post('firstwitnessname'),
						'first_witness_address'  => $this->input->post('firstwitnessaddress'),
						'second_witness_name'  => $this->input->post('secondwitnessname'),
						'second_witness_address'  => $this->input->post('secondwitnessaddress'),					
					);

					$this->db->where('candidateid', $token);
					$this->db->update('tbl_general_nomination_and_authorisation_form', $InsertArrayData);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error !!! save witness information !!!');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully save witness information !!!');		
					}
				}

				$submitsendbtn = $this->input->post('submitbtn'); 
				if (!empty($submitsendbtn) && $submitsendbtn =='senddatasubmit') {

					$InsertArrayData = array(
						'nomination_authorisation_signed'  => $this->input->post('nomination_authorisation_signed'),
						'first_witness_name'  => $this->input->post('firstwitnessname'),
						'first_witness_address'  => $this->input->post('firstwitnessaddress'),
						'second_witness_name'  => $this->input->post('secondwitnessname'),
						'second_witness_address'  => $this->input->post('secondwitnessaddress'),
						'status'  => 2,					
					);

					$this->db->where('candidateid', $token);
					$this->db->update('tbl_general_nomination_and_authorisation_form', $InsertArrayData);
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error !!! save witness information !!!');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully save witness information !!!');		
					}

				}


			}

			$content['token'] = $token;
			$content['sysrelations'] = $this->model->getSysRelations();
			$content['candidatedetailwithaddress'] = $this->model->getCandidateWithAddressDetails($token);

			$content['genenominformdetail'] = $this->model->getViewGeneralnominationform($token);

			$content['nomineedetail'] = $this->model->getNomineedetail($content['genenominformdetail']->id);

			$getTeamid = $this->model->getCandidateTeam($token);

			$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($getTeamid->teamid);

			$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

			$content['generalnominationFormUploadpdf'] = $this->model->getGeneralNominationWithWitnessformFormUploadStatus($token);

			$content['generaljoiningFormUploadpdf'] = $this->model->getJoiningReportFormPdfStatus($token);


			$content['title'] = 'Joining_of_da_document_verification';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}

	}catch (Exception $e) {

		print_r($e->getMessage());die;
	}


} 



public function Joining_report_form($token){

	try{


		if (empty($token)) {
			$this->session->set_flashdata('tr_msg', 'Token is not found !!!');	
			redirect('/Joining_of_da_document_verification/Joining_report_form/index');
		}else{


		//echo $token; die;
			$join_programme_date = "";
			$this->db->trans_start();
		 //$token = $this->input->post('token');
			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){


				$candidatedetails = $this->model->getSingleCandidateDetails($token);
				$JoiningReport = $this->model->getJoiningReportDetail($token);
				$joiningreportid = $JoiningReport->id;
				$candidateemailid = $candidatedetails->emailid;
				$candidatename = $candidatedetails->candidatefirstname;
				$candidatedetails->candidateid;


				$submitsenddata = $this->input->post('submitbtn');

				if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {

					$join_programme_date1 = $this->input->post('join_programme_date');

					$join_programme_date = $this->model->changedatedbformate($join_programme_date1);

					$updatearraydata = array(

						'status'  => 2,
						'submittedon'  => date('Y-m-d H:i:s'), 
						'submittedby'  => $this->loginData->UserID,
					);

					$this->db->where('id',$joiningreportid);
					$this->db->update('tbl_joining_report', $updatearraydata);
      		//echo $this->db->last_query(); die;
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error !!! Candidate Joining report save');	

					}else{

						// $hrdemailid    = 'poonamadlekha@pradan.net';
		 // $hrdemailid      = 'amit.kum2008@gmail.com';
						$hrdemailid = '';
		 			 $hrdemailid = $this->gmodel->getHRDEmailid();
						$tcemailid       = $this->loginData->EmailID;
						$candidateemail  = $candidateemailid;

						$subject = "Submit Joining Report Form ";
						$body  = 	'Dear '.$candidatename.', <br><br> ';
						$body .= 'Submit Joining Report Form Successfully ';
		 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
						$to_email = $hrdemailid->EmailID;
						// $to_name = $tcemailid;
						$sendmail = $this->Common_Model->send_email($subject, $body, $to_email);

						if ($sendmail) {
							// $sendmail1 = $this->Common_Model->send_email($subject, $body, $to_email);
						}

						$this->session->set_flashdata('tr_msg', 'Successfully Candidate Joining report save');	

						redirect('/Joining_of_da_document_verification/Joining_report_form/'.$token);		
					}


				}
			}


			$content['token'] = $token;
			$content['candidatedetils']        = $this->model->getCandidateDetails($token);
			$content['joiningreportdetails']   = $this->model->getJoiningReportDetail($token);

		//print_r($joiningreportdetails); die;


		//$content['getstatus'] = $this->model->getStatus($this->loginData->candidateid);

			$content['title'] = 'Joining_report_form';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}
	}catch (Exception $e) {

		print_r($e->getMessage());die;
	}
	
}


public function verification($token)
{
	try{
		$content['getcandateverified'] = $this->model->getCandateVerified($token);
		// echo "<pre>";
		// print_r($content['getcandateverified']);
		// echo $token;
		// die();	

		

		if (empty($token) || $token == '') {

			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
			redirect('/Joining_of_da_document_verification/index');
		}else{
		// 	if($content['getcandateverified']->categoryid)
		// {	

			$this->load->model('Joining_of_da_document_verification_model');
			
			 $content['getcandateverified'] = $this->model->getCandateVerified($token);
			// print_r($content['getcandateverified']);
			// die;


			   $teamid='';
			    $teamid=$content['getcandateverified']->teamid;
			   $Selected_tcDetails = $this->model->tc_data( $teamid);
			    $supervisor_name=$Selected_tcDetails->name;
			    $candidate_emailid=$content['getcandateverified']->emailid;
			    $candidate_firstname=$content['getcandateverified']->candidatefirstname;

			   

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			$DevelopmentApprenticeshipDetails = $this->model->getDevelopmentApprenticeship($token);
			@ $candidateemailid = $DevelopmentApprenticeshipDetails->emailid;

			if($RequestMethod == "POST"){
				
				/*echo "<pre>";
				print_r($this->input->post());
				die;*/

				$savebtndatasend = $this->input->post('submitbtn');

				if (!empty($savebtndatasend) && $savebtndatasend =='senddatasubmit') {
					// 	echo "sdgsd";
					// 	echo "<pre>";
					//  print_r($this->input->post());
					// 	die;
					$this->db->trans_start();

					$candidateid  = $this->input->post('candidateid');
					// echo $this->input->post('metric_certificate_verified');
					// die;

					$query = $this->db->query("SELECT * FROM `tbl_verification_document` WHERE `candidateid` = ".$candidateid);

					$result = $query->row();

					$currentstatus  = $this->input->post('verified_status');
					if ($currentstatus ==1) {
						$subjectstatus = 'Approved';
					}else{
						$subjectstatus = 'Rejected';
					}


					if ($query->num_rows() == 0){

						$verificationarraydata = array(

							'candidateid'  => $token,
							'metric_certificate'  => $this->input->post('metric_certificate_verified'),
							'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
							'ug_certificate'  => $this->input->post('ug_certificate_verified'),
							'pg_certificate'  => $this->input->post('pg_certificate_verified'),
							'signed_offerletter'  => $this->input->post('signed_certificate_verified'),
							'ugmigration_certificate'  => $this->input->post('ugmigration_certificate_verified'),
							'pgmigration_certificate'  => $this->input->post('pgmigration_certificate_verified'),
							'other_certificate'  => $this->input->post('other_certificate_verified'),
							'general_nomination_form'  => $content['getcandateverified']->categoryid==1?$this->input->post('general_nomination_form'):null,
							'joining_report_da'  => $content['getcandateverified']->categoryid==1?$this->input->post('joining_report_da'):null,
							'comment'  => $this->input->post('comments'),
							'status' => $currentstatus,
							'createdby' => $this->loginData->UserID, 	
							'createdon'  => date('Y-m-d H:i:s'),
							'approvedby' => $this->loginData->UserID, 	
							'approvedon'  => date('Y-m-d H:i:s'), 	
							'self_declaration_tc'  => $this->input->post('self_declaration_tc'),

						);
//
						$this->db->insert('tbl_verification_document', $verificationarraydata);

			//echo $this->db->last_query(); die;


					}else{

						$verificationarraydata = array(

							'candidateid'  => $token,
							'metric_certificate'  => $this->input->post('metric_certificate_verified'),
							'hsc_certificate'  => $this->input->post('hsc_certificate_verified'),
							'ug_certificate'  => $this->input->post('ug_certificate_verified'),
							'pg_certificate'  => $this->input->post('pg_certificate_verified'),
							'signed_offerletter'  => $this->input->post('signed_certificate_verified'),
							'ugmigration_certificate'  => $this->input->post('ugmigration_certificate_verified'),
							'pgmigration_certificate'  => $this->input->post('pgmigration_certificate_verified'),
							'other_certificate'  => $this->input->post('other_certificate_verified'),
							'general_nomination_form'  => $this->input->post('general_nomination_form'),
							'joining_report_da'  => $this->input->post('joining_report_da'),
							'comment'  => $this->input->post('comments'),
							'status' => $this->input->post('verified_status'),
							'approvedby' => $this->loginData->UserID, 	
							'approvedon'  => date('Y-m-d H:i:s'), 	
							'self_declaration_tc'  => $this->input->post('self_declaration_tc'),

						);

						$this->db->where('candidateid', $token);
						$this->db->update('tbl_verification_document', $verificationarraydata);

					}


					if ($currentstatus ==1) {
 
						$verifarraydata = array(
				       'tc_hrd_document_verfied'  => 1,  //// Approvees

			         );

						$this->db->where('candidateid', $token);
						$this->db->update('tbl_candidate_registration', $verifarraydata);


					}else if ($currentstatus ==2) {
						$verifarraydata = array(

						'tc_hrd_document_verfied'  => 3
							);
						 //// Rejected






$this->db->where('candidateid', $token);
						$this->db->update('tbl_candidate_registration', $verifarraydata);
					}					

						


					$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
					$this->db->delete('tbl_work_experience_verified', array('candidateid' => $token)); 

					for ($i=0; $i < $countworkexpverified; $i++) { 

						$insertArraydata = array(
							'candidateid'  => $token,
							'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
						);

						$this->db->insert('tbl_work_experience_verified', $insertArraydata);
					}


					$countdocumentname = count($this->input->post('documentname'));			
					$this->db->delete('tbl_other_documents', array('candidateid' => $token)); 

					for ($i=0; $i < $countdocumentname; $i++) { 

						if (!empty($this->input->post('documentname')[$i]) && isset($this->input->post('documentname')[$i])) {		

							if ($_FILES['otherdocumentupload']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['otherdocumentupload']['name'][$i])); 

								if($_FILES['otherdocumentupload']['size'] < 10485760) {
									$this->session->set_flashdata('er_msg', "filesize too large. Please upload file of size-2MB");
									redirect(current_url());
								}

								$OriginaldocumentName = $_FILES['otherdocumentupload']['name'][$i]; 
								$encrypteddocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['otherdocumentupload']['tmp_name'][$i];
								$targetPath = FCPATH . "otherdocuments/";
								$targetFile = $targetPath . $encrypteddocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encrypteddocument = $encrypteddocumentName;

								}else{

									die("Error uploading Other Document !!!");
									$this->session->set_flashdata("er_msg", "Error uploading Other Document ");
									redirect(current_url());
								}

							}


							$insertArraydata = array(
								'candidateid'  => $token,
								'documentname'=> $this->input->post('documentname')[$i],
								'documenttype'=> $this->input->post('documenttype')[$i],
								'documentupload'=> $encrypteddocument,
								'createdon'=> date('Y-m-d H:i:s'),
								'createdby'=> $this->loginData->UserID,
							);

							$this->db->insert('tbl_other_documents', $insertArraydata);
						}



					}

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error !!! Not update DA document verification !!!');	
					}else{

						//// Send Mail To HRD //////////////////////////////   
						// echo "tc to hrd";
						// die();

					$candidateemailid=$this->loginData->EmailID;
					$hrdemailid    = $this->loginData->EmailID;
	     $tcdemailid    = $this->loginData->EmailID;
					$candidateemailid    = $candidateemailid; 
					if($currentstatus==1)
						{
					$subject = "Completion of joining formalities";
				}
				else if($currentstatus==2)
				{
					$subject = "Rejected of joining formalities";
				}
					// $body = 	'Dear Sir, <br><br> ';
					// $body .= $this->input->post('comments');
		 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
					// $message='';

			$candidate = array('$candidate_firstname','$supervisor_name');
			$candidate_replace = array($candidate_firstname,$supervisor_name);

					// $fd = fopen("mailtext/completion_of_joining_formalities_verified.txt", "r");	
					// 	$message .=fread($fd,4096);
					// 	eval ("\$message = \"$message\";");
					// 	$message =nl2br($message);
						//echo $message;

			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 57 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
      		if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);


						$hrdemailid = $this->model->hr_data();
						$hrd=$hrdemailid->emailid;
						// echo $hrd;
						// die;
						

					$to_email = $hrdemailid;
					$to_name =  $candidateemailid;

					$sendmail = $this->Common_Model->send_email($subject, $body, $hrd);
					// echo $sendmail;
					// die;
					


						$this->session->set_flashdata('tr_msg', 'Successfully update DA document verification !!!');	
						redirect('Joining_of_da_document_verification/');		
					}
				}
			}




			$content['token'] = $token; 

			$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

		
			$content['getcandateverified'] = $this->model->getCandateVerified($token);
			//print_r($content['getcandateverified']);

			$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);
			
			$content['getgapyearcount'] = $this->model->getCandategapyearcount($token);
			//print_r($content['getgapyearcount']);
			

			$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);
			

			$content['getgapyearpverified'] = $this->model->getgapyeareCandateVerified($token);
			// print_r($content['getgapyearpverified']);
			// die;

			$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);
			$content['get_joiningreportstatus'] = $this->model->getJoiningReportStatus($token);
	// print_r($content['get_joiningreportstatus']);
	// die

			$content['getgeneralstatus']        = $this->model->getGeneralFormStatus($token);

		// print_r($content['getgeneralstatus']);
		// die;

			$content['getjoiningreportstatus'] = $this->model->getJoiningReport($token);
// 	print_r($content['getjoiningreportstatus']);
// die;


			$content['getdocuments'] = $this->model->getOtherDocuments();

			$content['title'] = 'Joining_of_da_document_verification';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}

	}catch (Exception $e) {
		log_message('error',$e->getMessage());
		return;
	}
}



public function edit($token)
{

	$this->load->model('Joining_of_da_document_verification_model');


		//print_r($this->input->post()); die;

	$this->db->trans_start();

	$DevelopmentApprenticeshipDetails = $this->model->getDevelopmentApprenticeship($token);
	if (!empty($DevelopmentApprenticeshipDetails)) {
		$candidateemailid = $DevelopmentApprenticeshipDetails->emailid;
	}
	

	$RequestMethod = $this->input->server('REQUEST_METHOD'); 

	if($RequestMethod == "POST"){



		$candidatename = $this->input->post('candidatename'); 
		$status = $this->input->post('verified_status'); 

		

		if ($status==1) {
			$subjectstatus = 'Approved';
		}else{
			$subjectstatus = 'Rejected';
		}

		

		$savebtnsubmitsave = $this->input->post('submitbtn');
		if (!empty($savebtnsubmitsave) && $savebtnsubmitsave =='senddatasubmit') {

			$currentstatus = $this->input->post('verified_status');

			$verificationarraydata = array(

				'candidateid'              => $token,
				'metric_certificate'        => $this->input->post('metric_certificate_verified'),
				'hsc_certificate'          => $this->input->post('hsc_certificate_verified'),
				'ug_certificate'           => $this->input->post('ug_certificate_verified'),
				'pg_certificate'           => $this->input->post('pg_certificate_verified'),
				'other_certificate'        => $this->input->post('other_certificate_verified'),
				'general_nomination_form'  => $this->input->post('general_nomination_form'),
				'joining_report_da'        => $this->input->post('joining_report_da'),
				'comment'                  => $this->input->post('comments'),
				'status'                   => $status,
				'approvedby'               => $this->loginData->UserID, 	
				'approvedon'               => date('Y-m-d H:i:s'),

			);

			$this->db->where('candidateid', $token);
			$this->db->update('tbl_verification_document', $verificationarraydata);


			$countworkexpverified = count($this->input->post('workexp_certificate_verified'));
			$this->db->delete('tbl_work_experience_verified', array('candidateid' => $token)); 

			for ($i=0; $i < $countworkexpverified; $i++) { 

				$insertArraydata = array(
					'candidateid'  => $token,
					'experience_verified'=> $this->input->post('workexp_certificate_verified')[$i],
				);

				$this->db->insert('tbl_work_experience_verified', $insertArraydata);
			}

			 $countdocumentname = count($this->input->post('documentname'));//	die;		
			 $this->db->delete('tbl_other_documents', array('candidateid' => $token)); 

			 for ($i=0; $i < $countdocumentname; $i++) { 

			 	if (!empty($this->input->post('documentname')[$i]) && isset($this->input->post('documentname')[$i])) {		

			 		if ($_FILES['otherdocumentupload']['name'][$i] != NULL) {

			 			@  $ext = end(explode(".", $_FILES['otherdocumentupload']['name'][$i])); 

			 			$OriginaldocumentName = $_FILES['otherdocumentupload']['name'][$i]; 
			 			$encrypteddocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
			 			$tempFile = $_FILES['otherdocumentupload']['tmp_name'][$i];
			 			$targetPath = FCPATH . "otherdocuments/";
			 			$targetFile = $targetPath . $encrypteddocumentName;
			 			@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

			 			if($uploadResult == true){

			 				$encrypteddocument = $encrypteddocumentName;

			 			}else{

			 				die("Error uploading Other Document !!!");
			 				$this->session->set_flashdata("er_msg", "Error uploading Other Document ");
			 				redirect(current_url());
			 			}

			 		}
			 		else{

			 			$encrypteddocument =  $this->input->post('oldotherdocumentupload')[$i];
			 		}

			 		$insertArraydata = array(
			 			'candidateid'  => $token,
			 			'documentname'=> $this->input->post('documentname')[$i],
			 			'documenttype'=> $this->input->post('documenttype')[$i],
			 			'documentupload'=> $encrypteddocument,
			 			'createdon'=> date('Y-m-d H:i:s'),
			 			'createdby'=> $this->loginData->UserID,
			 		);

			 		$this->db->insert('tbl_other_documents', $insertArraydata);
				//echo  $this->db->last_query(); 
			 	}

			 }	

			 $this->db->trans_complete();



		//// Send Mail To HRD //////////////////////////////   
			 if (!empty($candidateemailid)) {
			 	$hrdemailid    = 'amit.kum2008@gmail.com';
			 $candemailid   = $candidateemailid;

			 $subject = "TC ".$subjectstatus." document";
			 $body = 	'Dear Sir, <br><br> ';
			 $body .= $this->input->post('comments');
		//$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
			 $to_email = $hrdemailid;
			 $to_name  = $candemailid;

			 $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);
			 }
			 


			 if ($this->db->trans_status() === FALSE){

			 	$this->session->set_flashdata('er_msg', 'Error !!! Not update DA document verification !!!');	
			 }else{

			 	$this->session->set_flashdata('tr_msg', 'Successfully update DA document verification !!!');
			 	redirect('Joining_of_da_document_verification');		
			 }



			}
 } /// Post ///////


 $content['token'] = $token; 

 $content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

 $content['getcandateverified'] = $this->model->getCandateVerified($token);

 $content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);

 $content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);

 $content['getworkexperinceverifiedvalue'] = $this->model->getWorkExperinceVerifiedValue($token);
 $content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

 $content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);
//$content['witnessdeclaration'] = $this->model->getWitnessDeclaration($token);

 $getTeamid = $this->model->getCandidateTeam($token);

 $content['witnessdeclaration'] = $this->model->getWitnessDeclaration($getTeamid->teamid);


 $content['getverified'] = $this->model->getVerifiedDetailes($token);

 $content['getdocuments'] = $this->model->getOtherDocuments();

 $content['countotherdoc'] = $this->model->getCountOtherDocument($token);

 $content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);

 $content['title'] = 'Joining_of_da_document_verification';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
}




public function view($token)
{

	$this->load->model('Joining_of_da_document_verification_model');

	$content['token'] = $token; 

	$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

	$content['getcandateverified'] = $this->model->getCandateVerified($token);

	$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);

	$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);

	$content['getverified'] = $this->model->getVerifiedDetailes($token);
	$content['getverifiedstatus'] = $this->model->getverifiedstatus($token);

	$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

	$content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);
	$content['getgapyearcount'] = $this->model->getCandategapyearcount($token);
$content['getgapyearpverified'] = $this->model->getgapyeareCandateVerified($token);
	$content['getdocuments'] = $this->model->getOtherDocuments();
	$content['countotherdoc'] = $this->model->getCountOtherDocument($token);
	$content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);


	/*echo "<pre>";
	print_r($content);exit();*/

	$content['title'] = 'Joining_of_da_document_verification';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}







public function generalnominationform($token=null){

	try{

		if (empty($token) || $token == '' ) {
			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
			redirect('/Joining_of_da_document_verification/index');

		} else { 

			$apprivedby = "";
			$apprivedby = $this->loginData->UserFirstName.' '. $this->loginData->UserMiddleName.' '. $this->loginData->UserLastName;

			$generalnominationform = $this->model->getGeneral_Nomination_Form_Detail1($token);	

			// $firstwitnessname  = $this->model->getSingleNameWitnessDeclaration($generalnominationform->first_witness_name);
			// $secondwitnessname  = $this->model->getSingleNameWitnessDeclaration($generalnominationform->second_witness_name);

			$generalformid = $generalnominationform->id;
			$nomineedetails = $this->model->getNomieeDetail($generalformid);

			$tdate = date("d/m/y");
			// if(!empty($generalnominationform->candidatemiddlename))
			// $candidatemiddlename = $generalnominationform->candidatemiddlename;


			$candidate = array('$tdate','$generalnominationformexecutive_director_place','$generalnominationformcandidatefirstname','$generalnominationformcandidatelastname','$nomineedetails0nominee_name','$nomineedetails0relationname','$nomineedetails0nominee_age','$nomineedetails0nominee_address','$nomineedetails1nominee_name','$nomineedetails1relationname','$nomineedetails1nominee_age','$nomineedetails1nominee_address','$apprivedby','$generalnominationformnomination_authorisation_signed');

			$candidate_replace = array($tdate,$generalnominationform->executive_director_place,$generalnominationform->candidatefirstname,$generalnominationform->candidatelastname,$nomineedetails[0]->nominee_name,$nomineedetails[0]->relationname,$nomineedetails[0]->nominee_age,$nomineedetails[0]->nominee_address,$nomineedetails[1]->nominee_name,$nomineedetails[1]->relationname,$nomineedetails[1]->nominee_age,$nomineedetails[1]->nominee_address,$apprivedby,$generalnominationform->nomination_authorisation_signed);


			$filename = $token.'-'.md5(time() . rand(1,1000)).'.pdf';

			$updateArr = array(
				'filename'    		    => $filename,
				'generatepdf'    		=> 1,
			);
			// $this->db->where('status', 2);
			// $this->db->where('candidateid', $token);
			// $this->db->update('tbl_general_nomination_and_authorisation_form', $updateArr);

			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 25 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
   			if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
   			  // echo  $body; die;

   			$body = str_replace("980.364px","100%", $body);
   			$body = str_replace('<table  width="100%"></table >','', $body);

   			
   			 
			$this->load->model('Dompdf_model');

		 $generatepdf = $this->Dompdf_model->GeneralNominationformPDF($body,$filename,NULL,'GeneralNominationform.pdf'); 

			if ($generatepdf == true) {
				$this->session->set_flashdata('tr_msg', 'Successfully Generate General Nomination Form');
				redirect('Joining_of_da_document_verification/verification/'.$token);
			}else{
				$this->session->set_flashdata('er_msg', 'Sorry!!! generate general nomination pdf Not generate');
				redirect('Joining_of_da_document_verification/Joining_report_form/'.$token);
			}

		}	

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function JoiningReportForm($token){

	try{

		if (empty($token) || $token == '' ) {
			$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
			redirect('/Joining_of_da_document_verification/index');

		} else { 
			$apprivedby = "";
			$apprivedby = $this->loginData->UserFirstName.' '. $this->loginData->UserMiddleName.' '. $this->loginData->UserLastName;

			$candidatedetils = $this->model->getCandidateDetails($token);
			$getsubmitedby = $this->model->getSubmittedBy($token);
			$JoiningReport = $this->model->getJoiningReportDetail($token);
			$joiningreportid = $JoiningReport->id;


			$joiningreportdetails = $this->model->getJoiningReportDetail($token);
			// echo "<pre>";
			// print_r($joiningreportdetails); die;

			$tdate = date("d/m/y");
			$d_o_j = $this->model->changedatedbformate($candidatedetils->doj);
			$join_programme_date = $this->model->changedatedbformate($joiningreportdetails->join_programme_date);

			$candidate1 = array('$tdate','$candidatedetilspermanentstreet','$candidatedetilspermanentcity','$candidatedetilsname','$candidatedetilspermanentstatename','$candidatedetilspermanentpincode','$candidatedetilsofferno','$d_o_j','$apprivedby','$joiningreportdetailsapprenticeship_at_location','$join_programme_date','$getsubmitedbycandidatefirstname','$getsubmitedbycandidatelastname','$joiningreportdetailsoffice_at_location');

			$candidate_replace1 = array($tdate,$candidatedetils->permanentstreet,$candidatedetils->permanentcity,$candidatedetils->name,$candidatedetils->permanentstatename,$candidatedetils->permanentpincode,$candidatedetils->offerno,$d_o_j,$apprivedby,$joiningreportdetails->apprenticeship_at_location,$join_programme_date,$getsubmitedby->candidatefirstname,$getsubmitedby->candidatelastname,$joiningreportdetails->office_at_location);

			// $html = '<table  width="100%" >
			// <tr>
			// <td> </td>
			// <td></td>
			// <td valign="top" ></td>
			// </tr>
			// <tr>
			// <td colspan="3"</td>

			// </tr>
			// <tr>
			// <td colspan="3" align="center"><b>PRADAN’s APPRENTICESHIP PROGRAMME FOR RURAL DEVELOPMENT</b></td>
			// </tr>
			// <tr>
			// <td colspan="3" align="center"><b>JOINING REPORT </b></td>
			// </tr>
			// <tr>
			// <td align="right"></td>
			// <td align="justify">&nbsp;</td>
			// <td align="right">$tdate</td>
			// </tr>

			// <tr>
			// <td align="justify">Address: </td>
			// <td align="justify"> </td>
			// <td align="justify"></td>
			// </tr>
			// <tr>
			// <td align="justify">$candidatedetils->permanentstreet</td>
			// <td align="justify"> </td>
			// <td align="justify"></td>
			// </tr>
			// <tr>
			// <td align="justify">$candidatedetils->permanentcity  $candidatedetils->name</td>
			// <td align="justify"> </td>
			// <td align="justify"></td>
			// </tr>
			// <tr>
			// <td align="justify">$candidatedetils->permanentstatename, $candidatedetils->permanentpincode</td>
			// <td align="justify"> </td>
			// <td align="justify"></td>
			// </tr>
			// <tr>
			// <td align="justify">To,</td>
			// <td align="justify"> </td>
			// <td align="justify"></td>
			// </tr>
			// <tr>
			// <td colspan="3" align="justify">The Executive Director, <br> PRADAN</td>
			// </tr>
			// <tr>
			// <td colspan="3" align="justify">E 1/A, Ground Floor and Basement <br>
			// Kailash Colony <br>
			// New Delhi - 110 048 <br>
			// Tel: 4040 7700</td>
			// </tr>
			// <tr>
			// <td colspan="3" align="center">
			// <b>Subject: Joining Report</b> 
			// </td>
			// </tr>
			// <tr>
			// <td colspan="3" align="center">

			// </td>
			// </tr>
			// <tr>
			// <td colspan="3" align="center">

			// </td>
			// </tr>

			// <tr>
			// <td colspan="3" align="justify">Please refer to your offer no.  $candidatedetils->offerno    dated &nbsp; <b> $d_o_j  </b>&nbsp; offering me apprenticeship at  $joiningreportdetails->apprenticeship_at_location.</td>    
			// </tr>
			// <tr>
			// <td colspan="3" align="justify"></td>    
			// </tr>
			// <tr>
			// <td colspan="3" align="justify">I reported at PRADAN’s office at  $joiningreportdetails->office_at_location  and have joined the programme in the  forenoon of today, the  $join_programme_date .</td>    
			// </tr>
			// <tr>
			// <td colspan="3" align="justify"></td>    
			// </tr>
			// <tr>
			// <td colspan="3" align="justify">I shall inform you of any change in my address, given above, when it occurs.
			// </td>    
			// </tr>
			// <tr>
			// <td colspan="3" align="center"></td>
			// </tr>
			// <tr>
			// <td align="center">Submited By</td>
			// <td align="center"></td>
			// <td align="center">Approved By</td>
			// <td align="center"></td>
			// </tr>
			// <tr>
			// <td align="center">$getsubmitedby->candidatefirstname $getsubmitedby->candidatelastname </td>
			// <td align="center"></td>
			// <td align="center">$apprivedby</td>
			// <td align="center"></td>
			// </tr>

			// <tr>
			// <td align="center"></td>
			// <td align="center"></td>
			// <td align="center"></td>
			// <td align="center"></td>
			// </tr>
			// <tr>
			// <td align="center"></td>
			// <td align="center"></td>
			// <td align="center"></td>
			// <td align="center"></td>
			// </tr>
			// <tr>
			// <td colspan="3" align="left"></td>
			// </tr>
			// <tr>
			// <td colspan="3" align="left"></td>
			// </tr>
			// </table>';

			$filename = $token.'-'.md5(time() . rand(1,1000)).'.pdf';
			$updateArr = array(
				'filename'    		    => $filename,
				'generatepdf'    		=> 1,
			);
			// $this->db->where('status', 2);
			// $this->db->where('id', $joiningreportid);
			// $this->db->update('tbl_joining_report', $updateArr);

			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 26 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
   			if(!empty($data))
   		    $body = str_replace($candidate1,$candidate_replace1 , $data->lettercontent);

			$this->load->model('Dompdf_model');
			$generatepdf = $this->Dompdf_model->joiningreportfordaPDF($body, $filename, NULL,'Generateofferletter.pdf');

			if ($generatepdf == true) {
				$this->session->set_flashdata('tr_msg', 'Successfully generate joining report form');
				redirect('Joining_of_da_document_verification/verification/'.$token);
			}else{
				$this->session->set_flashdata('er_msg', 'Sorry!!! joining report form pdf Not generate');
				redirect('Joining_of_da_document_verification/Joining_report_form/'.$token);
			}

		}


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}





}