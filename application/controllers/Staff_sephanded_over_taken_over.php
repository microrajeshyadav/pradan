<?php 

///// Handed over taken over Controller   

class Staff_sephanded_over_taken_over extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Stafftransferpromotion_model","Stafftransferpromotion_model");
	//	$this->load->model("Handed_over_taken_over_model","Handed_over_taken_over_model");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		
		$this->load->model("Staff_sephanded_over_taken_over_model","Staff_sephanded_over_taken_over_model");
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token)
	{
	   
		try{
	    // start permission 
			
			//$staff_id=$this->loginData->staffid;
			$query1 = "SELECT * FROM staff_transaction where id=$token ";

		$content['staff_id'] = $this->db->query($query1)->row();
		// print_r($content['staff_id']);
		$staff_id=$content['staff_id']->staffid;
		//echo "staff=".$staff_id;
		//die;
		// end permission
		
			
			
			$content['t_id']=$this->Staff_sephanded_over_taken_over_model->getTransid($staff_id);
			//print_r($content['t_id']);
			//die();
			
			$id=$content['t_id']->id;
			//echo "id=".$id;

			$content['staff_details']=$this->Staff_sephanded_over_taken_over_model->get_staffDetails($id);
			//print_r($content['staff_details']);
			//die;
			
	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		
		
		$content['getstafflist'] = $this->Staff_sephanded_over_taken_over_model->getStaffList();
		//print_r($content['getstafflist']);
	
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			//print_r($_POST);
			//die();

      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
			//print_r($_POST);
			//die();

			$transferno=$this->input->post('transferno');
			
				$insertArraydata=array(

					
						'staffid'=>$staff_id,
						'transfernno'=>$transferno,
						'flag'=>0

					);
				//echo "<pre>";
				//print_r($insertArraydata);
				//die();
				 $this->db->insert('tbl_staff_handed_taken_over_charge_transac', $insertArraydata);
				//$this->db->insert($insertArraydata);
				 $insertid = $this->db->insert_id();
				//echo $this->db->last_query(); 

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

						);
					//print_r($arr);
				/*$insertIdentityDetails	 = array(
							//'id'=>$id,
							'item'         =>$this->input->post('items')[$i])
							
							
						   

						    );*/
						
									
					$this->db->insert('tbl_staff_sep_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
						}

				//die();
				//die()

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Staff_sephanded_over_taken_over/edit/'.$token.'/'.$insertid);			
				}

			}



		
			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				
				$transferno=$this->input->post('transferno');
			
				$insertArraydata=array(

					
						'staffid'=>$staff_id,
						'transfernno'=>$transferno,
						'flag'=>0

					);
				//echo "<pre>";
				//print_r($insertArraydata);
				//die();
				 $this->db->insert('tbl_staff_handed_taken_over_charge_transac', $insertArraydata);
				//$this->db->insert($insertArraydata);
				 $insertid = $this->db->insert_id();
				//echo $this->db->last_query(); 

				$countitem = count($this->input->post('items'));
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array('item'=>$this->input->post('items')[$i],
						'handedtaken_id'=>$insertid,
						'description'=>$this->input->post('items_description')[$i]

						);
					//print_r($arr);
				/*$insertIdentityDetails	 = array(
							//'id'=>$id,
							'item'         =>$this->input->post('items')[$i])
							
							
						   

						    );*/
						
									
					$this->db->insert('tbl_staff_sep_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
						}

				//die();

					//die();
			
			//die(print_r($insertArraydata));
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Staff_sephanded_over_taken_over/view/'.$insertid.'/'.$token);			
				}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}

	


			
		}
			//$content['subview']="edit";
		$content['title'] = 'Staff_sephanded_over_taken_over';	
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	 }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
		
	}

	public function Edit($token,$t_id)
	{
	 	try{
	    // start permission 
	 		//$staff_id=$this->loginData->staffid;
	 		
			$query1 = "SELECT * FROM staff_transaction where $t_id=$token ";

		$content['staff_id'] = $this->db->query($query1)->row();
		// print_r($content['staff_id']);
		$staff_id=$content['staff_id']->staffid;
		//echo "staff=".$staff_id;
		//die;
		// end permission
		
			
			
			$content['t_id']=$this->Staff_sephanded_over_taken_over_model->getTransid($staff_id);
			//print_r($content['t_id']);
			//die();
			
			$id=$content['t_id']->id;
			//echo "id=".$id;

			$content['staff_details']=$this->Staff_sephanded_over_taken_over_model->get_staffDetails($id);
			//print_r($content['staff_details']);
			//die;
			
	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		
		
		$content['getstafflist'] = $this->Staff_sephanded_over_taken_over_model->getStaffList();
		//print_r($content['getstafflist']);
		$content['expense']=$this->model->count_handedchrges($token);
			
			$content['transfer_expeness_details']=$this->model->expeness_details($token);
			
 
 $content['handed_expeness']=$this->model->handed_over_charge($token);
 //print_r($content['handed_expeness']);


	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		$RequestMethod = $this->input->server('REQUEST_METHOD'); 
		if($RequestMethod == "POST")
		{	$submitdatasend = $this->input->post('submitbtn');

			$Sendsavebtn = $this->input->post('savetbtn');
			//print_r($_POST);
			//die();

      if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {
			
			$transferno=$this->input->post('transferno');
			
				$insertArraydata=array(
	
						'staffid'=>$staff_id,
						'transfernno'=>$transferno,
						'flag'=>0
					);
				
				 $this->db->update('tbl_staff_hand_over_taken_over_charge', $insertArraydata);
				$this->db->where('id',$token);
				 
	$countitem = count($this->input->post('items'));
			
$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);

							
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array(
						
						'handedtaken_id' =>$token,
						'item' =>$this->input->post('items')[$i],
						'description'=>$this->input->post('items_description')[$i],
						);
					
			$this->db->insert('tbl_staff_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
						}

				//die();
				
			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate handed  our responsibilities');
					redirect('Staff_sephanded_over_taken_over/edit/'.$token.$t_id);			
				}

			}



		
			else if(!empty($submitdatasend) && $submitdatasend =='senddatasubmit')
			{
				
				$transferno=$this->input->post('transferno');
			
				$insertArraydata=array(
	
						'staffid'=>$staff_id,
						'transfernno'=>$transferno,
						'flag'=>1
					);
				
				 $this->db->update('tbl_staff_hand_over_taken_over_charge', $insertArraydata);
				$this->db->where('id',$token);
				 
	$countitem = count($this->input->post('items'));
			
$this->db->query("delete from tbl_handed_taken_over_charge_transac where handedtaken_id =". $token);

							
				for ($i=0; $i < $countitem; $i++) { 
					
					$arr=array(
						
						'handedtaken_id' =>$token,
						'item' =>$this->input->post('items')[$i],
						'description'=>$this->input->post('items_description')[$i],
						);
					
			$this->db->insert('tbl_staff_handed_taken_over_charge_transac', $arr);
				//echo $this->db->last_query();
						}

			
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{

					$this->session->set_flashdata('er_msg', 'Error !!!  Please try again');	
				}
				else{
					$this->session->set_flashdata('tr_msg', 'Successfully Candidate Handed our responsibilities');
					redirect('Staff_sephanded_over_taken_over/view/'.$token.$t_id);			
				}
				//echo "submit";
				//die();
				//$this->session->set_flashdata('er_msg', 'Error !!! DA already join Please choose other DA');	
			}

	


			
		}

		$content['title'] = 'Staff_sephanded_over_taken_over/edit';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
			
	}catch (Exception $e) {
       print_r($e->getMessage());die;
     }
}
public function view($token)
	{
	 	try{
	    // start permission 
	 		$staff_id=$this->loginData->staffid;
		//echo "staff_id=".$staff_id;
			
			
			$content['t_id']=$this->model->getTransid($staff_id);
			//print_r($content['t_id']);
			$id=$content['t_id']->id;

			$content['staff_details']=$this->model->get_staffDetails($id);
				
			$content['expense']=$this->model->count_handedchrges($token);
			
			$content['transfer_expeness_details']=$this->model->expeness_details($token);
			
 
 $content['handed_expeness']=$this->model->handed_over_charge($token);
 //print_r($content['handed_expeness']);
	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		
		$content['title'] = 'Staff_sephanded_over_taken_over/view';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
			
	}catch (Exception $e) {
       print_r($e->getMessage());die;
     }
}

}