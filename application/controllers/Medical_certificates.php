<?php 

/**
* State List
*/
class Medical_certificate extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {

         // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   $candidateid=($this->loginData->candidateid);

   $staffids=($this->loginData->staffid);


   $querys="SELECT b.dateofbirth, a.offerno, a.doj, b.candidatefirstname, b.candidatelastname, d.desname, e.officename FROM tbl_generate_offer_letter_details AS a INNER JOIN tbl_candidate_registration AS b ON b.candidateid = a.candidateid INNER JOIN staff AS c ON c.candidateid = b.candidateid INNER JOIN msdesignation AS d ON d.desid = c.designation INNER JOIN lpooffice AS e ON e.staffid = c.staffid WHERE b.candidateid =$candidateid";

   $content['candidate_list']=$this->db->query($querys)->result();


   $RequestMethod = $this->input->server('REQUEST_METHOD'); 



   if($RequestMethod == 'POST'){

    if ($_FILES['signatureofpresence']['name'] != NULL) {

      $data=$_FILES['signatureofpresence'];

      $this->load->model('Medical_certificate_model');

      $fileDatas=$this->Medical_certificate_model->do_uploads($data);
      $updateArr['signatureofpresence122'] = $fileDatas['orig_name'];
      $updateArr['signatureofpresence_encrypted_filename'] = $fileDatas['file_name'];


    }



    if ($_FILES['signatureofcandidate']['name'] != NULL) {
// echo "sdsd";
// die();
      $data=$_FILES['signatureofcandidate'];

      $this->load->model('Medical_certificate_model');

      $fileDatasa=$this->Medical_certificate_model->do_uploadd($data);
      $updateArr['signatureofcandidate'] = $fileDatas['orig_name'];
      $updateArr['signatureofcandidate_encrypted_filename'] = $fileDatas['file_name'];


    }


    $candidatedt=$this->input->post('candidatedate');
    $datepresence=$this->input->post('doctorsigneddate');

    $this->load->model('Medical_certificate_model');

    $duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($candidatedt);
    $dddd=$this->Medical_certificate_model->getCandidateDetailsPreview($datepresence);



    $operation=$this->input->post('operation');

    if($operation == 0){



      $insertArr = array(
        'signatureofexamine'      =>  $updateArr['signatureofpresence_encrypted_filename'],
        'candidate_id'=> $candidateid,
        'candidatesignature'      =>  $updateArr['signatureofcandidate_encrypted_filename'],
        'dateofpresence'=> $dddd,
        'candidatedate'=> $duuu,
        'createdon'      => date('Y-m-d H:i:s'),
        'createdby'      => $this->loginData->UserID,
        'small_pox'      =>$this->input->post('smallyes1') ,
        'spitting_disease'      =>$this->input->post('Spittingyes'),
        'fainting_attacks'      =>$this->input->post('Faintingyes') ,
        'appendicitis'      =>$this->input->post('Appendicitisyes') ,
        'lung_disease'      =>$this->input->post('Diseaseyes'),
        'epilepsy'      =>$this->input->post('Epilepsyyes'),
        'insanity'      =>$this->input->post('Insanityyes') ,
        'major_surgery'      =>$this->input->post('Majoryes') ,
        'any_physical_disability'      =>$this->input->post('Physicalyes'),
        'nervousness_depression'      =>$this->input->post('tendencyyes'),
        'flag'=>0,
        'anydetails'=>$this->input->post('showthis'),
      );

      $this->db->insert('tbl_medical_certificate', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added  Medical Certificate');
      $this->session->set_flashdata('er_msg', $this->db->error());
      redirect('/Medical_certificate/editindex/');


    } else {



     $insertArr = array(
      'signatureofexamine'      =>  $updateArr['signatureofpresence_encrypted_filename'],
      'candidate_id'=> $candidateid,
      'candidatesignature'      =>  $updateArr['signatureofcandidate_encrypted_filename'],
      'dateofpresence'=> $dddd,
      'candidatedate'=> $duuu,
      'createdon'      => date('Y-m-d H:i:s'),
      'createdby'      => $this->loginData->UserID,
      'small_pox'      =>$this->input->post('smallyes1') ,
      'spitting_disease'      =>$this->input->post('Spittingyes'),
      'fainting_attacks'      =>$this->input->post('Faintingyes') ,
      'appendicitis'      =>$this->input->post('Appendicitisyes') ,
      'lung_disease'      =>$this->input->post('Diseaseyes'),
      'epilepsy'      =>$this->input->post('Epilepsyyes'),
      'insanity'      =>$this->input->post('Insanityyes') ,
      'major_surgery'      =>$this->input->post('Majoryes') ,
      'any_physical_disability'      =>$this->input->post('Physicalyes'),
      'nervousness_depression'      =>$this->input->post('tendencyyes'),
      'flag'=>1,
      'anydetails'=>$this->input->post('showthis'),
    );

     $this->db->insert('tbl_medical_certificate', $insertArr);
     // echo $this->db->last_query(); die;
     $this->session->set_flashdata('tr_msg', 'Successfully added  Medical Certificate');
     $this->session->set_flashdata('er_msg', $this->db->error());
     redirect('/Medical_certificate/editindex/');

   }



 }


 prepareview:
 $content['title'] = 'Medical_certificate';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
}


public function add()
{

 $candidateid=($this->loginData->candidateid);

 $staffids=($this->loginData->staffid);


 $querys="SELECT b.desname FROM staff as a LEFT JOIN msdesignation as b on b.desid=a.designation WHERE a.candidateid=$candidateid";
 $content['des_list']=$this->db->query($querys)->result();

 $querys1="SELECT a.candidatefirstname, a.candidatelastname FROM tbl_candidate_registration as a WHERE a.candidateid=$candidateid";
 $content['candidate_name']=$this->db->query($querys1)->result();




    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 



 $RequestMethod = $this->input->server('REQUEST_METHOD'); 



 if($RequestMethod == 'POST'){

   $this->form_validation->set_rules('txtnames','Name','trim|required|min_length[1]|max_length[50]');
   $this->form_validation->set_rules('placename','Name','trim|required|min_length[1]|max_length[50]');

   if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }

  if ($_FILES['doctorssignature']['name'] != NULL) {

    $data=$_FILES['doctorssignature'];

    $this->load->model('Medical_certificate_model');

    $fileDatas=$this->Medical_certificate_model->do_uploadss($data);
    $updateArr['doctorssignature122'] = $fileDatas['orig_name'];
    $updateArr['doctorssignature_encrypted_filename'] = $fileDatas['file_name'];


  }

  $dateofexaminedocs=$this->input->post('dateofexaminedoc');
  $this->load->model('Medical_certificate_model');
  $duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($dateofexaminedocs);


  $operation=$this->input->post('operation');

  if($operation == 0){

   $updateArr = array(
    'doctor_seal_signature'      => $updateArr['doctorssignature_encrypted_filename'],
    'weakness_except'=> $this->input->post('txtnames'),
    'place'=>$this->input->post('placename'),
    'updatedon'      => date('Y-m-d H:i:s'),
    'updatedby'      => $this->loginData->UserID,
    'certificatedate'      =>$duuu ,
    'flag'        =>0


  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);

   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Medical_certificate/editcertificate/');


 } else {

   $updateArr = array(
    'doctor_seal_signature'      => $updateArr['doctorssignature_encrypted_filename'],
    'weakness_except'=> $this->input->post('txtnames'),
    'place'=>$this->input->post('placename'),
    'updatedon'      => date('Y-m-d H:i:s'),
    'updatedby'      => $this->loginData->UserID,
    'certificatedate'      =>$duuu ,
    'flag'        =>1


  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Medical_certificate/editcertificate/');

 }

}


prepareview:



$content['title'] = 'Medical_certificate';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}



public function Editindex()
{

 $staffids=($this->loginData->staffid);
 $candidateid=($this->loginData->candidateid);

 $querys="SELECT a.candidatefirstname, a.candidatelastname, a.dateofbirth FROM tbl_candidate_registration as a WHERE a.candidateid=$candidateid";

 $content['candidate_list']=$this->db->query($querys)->result();

 $queryss="Select * from  tbl_medical_certificate where candidate_id=$candidateid";
 $content['tbldata']=$this->db->query($queryss)->result();



 $RequestMethod = $this->input->server('REQUEST_METHOD'); 



 if($RequestMethod == 'POST'){

  if ($_FILES['signatureofpresence']['name'] != NULL) {

    $data=$_FILES['signatureofpresence'];

    $this->load->model('Medical_certificate_model');

    $fileDatas=$this->Medical_certificate_model->do_uploads($data);
    $updateArr['signatureofpresence122'] = $fileDatas['orig_name'];
    $updateArr['signatureofpresence_encrypted_filename'] = $fileDatas['file_name'];


  }



  if ($_FILES['signatureofcandidate']['name'] != NULL) {
// echo "sdsd";
// die();
    $data=$_FILES['signatureofcandidate'];

    $this->load->model('Medical_certificate_model');

    $fileDatasa=$this->Medical_certificate_model->do_uploadd($data);
    $updateArr['signatureofcandidate'] = $fileDatas['orig_name'];
    $updateArr['signatureofcandidate_encrypted_filename'] = $fileDatas['file_name'];


  }


  $candidatedt=$this->input->post('candidatedate');
  $datepresence=$this->input->post('doctorsigneddate');

  $this->load->model('Medical_certificate_model');

  $duuu=$this->Medical_certificate_model->getCandidateDetailsPreview($candidatedt);
  $dddd=$this->Medical_certificate_model->getCandidateDetailsPreview($datepresence);



  $operation=$this->input->post('operation');

  if($operation == 0){



   $updateArr = array(

     'signatureofexamine'      =>  $updateArr['signatureofpresence_encrypted_filename'],
     'candidate_id'=> $candidateid,
     'candidatesignature'      =>  $updateArr['signatureofcandidate_encrypted_filename'],
     'dateofpresence'=> $dddd,
     'candidatedate'=> $duuu,
     'updatedon'      => date('Y-m-d H:i:s'),
     'updatedby'      => $this->loginData->UserID,
     'small_pox'      =>$this->input->post('smallyes1') ,
     'spitting_disease'      =>$this->input->post('Spittingyes'),
     'fainting_attacks'      =>$this->input->post('Faintingyes') ,
     'appendicitis'      =>$this->input->post('Appendicitisyes') ,
     'lung_disease'      =>$this->input->post('Diseaseyes'),
     'epilepsy'      =>$this->input->post('Epilepsyyes'),
     'insanity'      =>$this->input->post('Insanityyes') ,
     'major_surgery'      =>$this->input->post('Majoryes') ,
     'any_physical_disability'      =>$this->input->post('Physicalyes'),
     'nervousness_depression'      =>$this->input->post('tendencyyes'),
     'flag'=>0,
     'anydetails'=>$this->input->post('showthis'),
   );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);

   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Medical_certificate/editindex/');


 } else {



   $updateArr = array(

     'signatureofexamine'      =>  $updateArr['signatureofpresence_encrypted_filename'],
     'candidate_id'=> $candidateid,
     'candidatesignature'      =>  $updateArr['signatureofcandidate_encrypted_filename'],
     'dateofpresence'=> $dddd,
     'candidatedate'=> $duuu,
     'updatedon'      => date('Y-m-d H:i:s'),
     'updatedby'      => $this->loginData->UserID,
     'small_pox'      =>$this->input->post('smallyes1') ,
     'spitting_disease'      =>$this->input->post('Spittingyes'),
     'fainting_attacks'      =>$this->input->post('Faintingyes') ,
     'appendicitis'      =>$this->input->post('Appendicitisyes') ,
     'lung_disease'      =>$this->input->post('Diseaseyes'),
     'epilepsy'      =>$this->input->post('Epilepsyyes'),
     'insanity'      =>$this->input->post('Insanityyes') ,
     'major_surgery'      =>$this->input->post('Majoryes') ,
     'any_physical_disability'      =>$this->input->post('Physicalyes'),
     'nervousness_depression'      =>$this->input->post('tendencyyes'),
     'flag'=>1,
     'anydetails'=>$this->input->post('showthis'),
   );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tbl_medical_certificate', $updateArr);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Medical_certificate/editindex/');

 }
}

prepareview:

$content['subview'] = 'Medical_certificate/editindex';
$this->load->view('_main_layout', $content);
}



public function Editcertificate()
{


  $staffids=($this->loginData->staffid);
  $candidateid=($this->loginData->candidateid);

  $candidateid=($this->loginData->candidateid);

  $staffids=($this->loginData->staffid);


  $querys="SELECT b.desname FROM staff as a LEFT JOIN msdesignation as b on b.desid=a.designation WHERE a.candidateid=$candidateid";
  $content['des_list']=$this->db->query($querys)->result();

  $querys1="SELECT a.candidatefirstname, a.candidatelastname FROM tbl_candidate_registration as a WHERE a.candidateid=$candidateid";
  $content['candidate_name']=$this->db->query($querys1)->result();

$queryss="select * from  tbl_medical_certificate where candidate_id=$candidateid";
$content['fetch_datas']=$this->db->query($querys1)->result();


    // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission  



  $RequestMethod = $this->input->server('REQUEST_METHOD'); 



  if($RequestMethod == 'POST'){




    $operation=$this->input->post('operation');

    if($operation == 0){



   
   $updateArr = array(
    'doctor_seal_signature'      => $updateArr['doctorssignature_encrypted_filename'],
    'weakness_except'=> $this->input->post('txtnames'),
    'place'=>$this->input->post('placename'),
    'updatedon'      => date('Y-m-d H:i:s'),
    'updatedby'      => $this->loginData->UserID,
    'certificatedate'      =>$duuu ,
    'flag'        =>0


  );

     $this->db->where('candidate_id', $candidateid);
     $this->db->update('tbl_medical_certificate', $updateArr);

     $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
     $this->session->set_flashdata('er_msg', $this->db->error());
     redirect('/Medical_certificate/editindex/');


   } else {



     $updateArr = array(

       'signatureofexamine'      =>  $updateArr['signatureofpresence_encrypted_filename'],
       'candidate_id'=> $candidateid,
       'candidatesignature'      =>  $updateArr['signatureofcandidate_encrypted_filename'],
       'dateofpresence'=> $dddd,
       'candidatedate'=> $duuu,
       'updatedon'      => date('Y-m-d H:i:s'),
       'updatedby'      => $this->loginData->UserID,
       'small_pox'      =>$this->input->post('smallyes1') ,
       'spitting_disease'      =>$this->input->post('Spittingyes'),
       'fainting_attacks'      =>$this->input->post('Faintingyes') ,
       'appendicitis'      =>$this->input->post('Appendicitisyes') ,
       'lung_disease'      =>$this->input->post('Diseaseyes'),
       'epilepsy'      =>$this->input->post('Epilepsyyes'),
       'insanity'      =>$this->input->post('Insanityyes') ,
       'major_surgery'      =>$this->input->post('Majoryes') ,
       'any_physical_disability'      =>$this->input->post('Physicalyes'),
       'nervousness_depression'      =>$this->input->post('tendencyyes'),
       'flag'=>1,
       'anydetails'=>$this->input->post('showthis'),
     );

     $this->db->where('candidate_id', $candidateid);
     $this->db->update('tbl_medical_certificate', $updateArr);
     // echo $this->db->last_query(); die;
     $this->session->set_flashdata('tr_msg', 'Successfully Updated  Medical Certificate');
     $this->session->set_flashdata('er_msg', $this->db->error());
     redirect('/Medical_certificate/editindex/');

   }
 }

 prepareview:

 $content['subview'] = 'Medical_certificate/editcertificate';
 $this->load->view('_main_layout', $content);
}



}