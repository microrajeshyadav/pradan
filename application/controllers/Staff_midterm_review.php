<?php 

/**
* State List
*/
class Staff_midterm_review extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");

    $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_midterm_review_model","Staff_midterm_review_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
  // end permission   


   $content['hrd_staff_details'] = $this->Staff_midterm_review_model->getstaffname();
    // echo "<pre>";
    // print_r($content['hrd_staff_details']);

    //     die;
  

   $content['title'] = 'Staff_midterm_review';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

 public function Add($staff=null,$token=null)
 {
  try{
   
   

      if (empty($token) || $token == '' ) {
        $this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
        redirect('/Ed_staff_approval/index');
        
      } else {
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  

    if($token)
    {
       $tablename = "'staff_transaction'";
  $incval = "'@a'";

   $getstaffincrementalid = $this->Staff_midterm_review_model->getmidtermmax($tablename,$incval); 
  $autoincval =  $getstaffincrementalid->maxincval;
 // echo "increment="+$autoincval;

  //  $staff=$this->loginData->staffid;
    $content['login_details'] = $this->Staff_midterm_review_model->loginstaff($staff);
    //print_r($content['login_details']);
    //print_r($this->loginData);
    $login_person='';
    $login_person=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
    //die;
    
    $hr_admin=$content['login_details']->emailid;
    $content['hrd_staff_details'] = $this->Staff_midterm_review_model->getstaff($staff);
    // print_r( $content['hrd_staff_details']);
    // die;
    $midterm=$content['hrd_staff_details'];
    $getSupervisor = $this->Staff_midterm_review_model->getSupervisername($staff);

    $reporting_id=$getSupervisor->reportingto;
   // echo $reporting_id;
    if($reporting_id=='' || $reporting_id==Null)
    {

        $this->session->set_flashdata('er_msg', 'Error !!! reporting to is not available in staff list'); 
        redirect(current_url());
    }
    else
  
    {
     $content['tc_email'] = $this->Staff_midterm_review_model->superviser_email($reporting_id);
     
    }
    
     $content['hr_email'] = $this->Staff_midterm_review_model->hrd_email();
     $content['p_email'] = $this->Staff_midterm_review_model->personal_email();
     $content['ed_email'] = $this->Staff_midterm_review_model->ed_email();

       

    $this->db->trans_start();

   
    
    

       $insertArr = array(
          'id'               => $autoincval,
          'staffid'          => $staff,
          'old_office_id'    => $midterm->new_office_id,
          'old_designation'  => $midterm->new_designation,
           'new_designation'  => $midterm->new_designation,
          'new_office_id'    => $midterm->new_office_id,
          'date_of_transfer' => date("Y-m-d"),
          'reason'           => $midterm->reason,
          'trans_status'     => 'Midtermreview',
          'datetime'         => date("Y-m-d H:i:s"),
          'createdon'        => date("Y-m-d H:i:s"),
          'createdby'        => $this->loginData->staffid,
          'trans_flag'       => 1,
       
      );
       $this->db->insert('staff_transaction', $insertArr);
      
        $insertworkflowArr = array(
        
         'probation_status'    => 1,
      
       );
       $this->db->where('staffid', $staff);
       $this->db->update('staff', $insertworkflowArr);


      
       $insertworkflowArr = array(
         'r_id'           => $autoincval,
         'type'           => 22,
         'staffid'        => $staff,
         'sender'         => $this->loginData->staffid,
         'receiver'       => $getSupervisor->reportingto,
         'senddate'       => date("Y-m-d H:i:s"),
         'flag'           => 1,
         'scomments'      => $midterm->reason,
         'createdon'      => date("Y-m-d H:i:s"),
         'createdby'      => $this->loginData->staffid,
       );

      $this->db->insert('tbl_workflowdetail', $insertworkflowArr);


       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! Approval has not been done successfully'); 
        redirect(current_url());

      }else{
        $name=$content['login_details']->name;


        $subject = ': Midterm View';
        $body = '<h4>Midtermreview approved by' .$login_person.', </h4><br />';
        $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
        <tr>
        <td width="96">Name </td>
        <td width="404">'.$content['hrd_staff_details']->name.'</td>
        </tr>
        <tr>
        <td>Employ Code</td>
        <td> '.$content['hrd_staff_details']->emp_code.'</td>
        </tr>
        <tr>
        <td>Designation</td>
        <td>' .$content['hrd_staff_details']->desname.'</td>
        </tr>
        <tr>
        <td>Office</td>
        <td>'.$content['hrd_staff_details']->officename.'</td>
        </tr>
        </table>';
        $body .= "<br /> <br />";
        $body .= "Regards <br />";
        $body .= " ". $content['login_details']->name ."<br>";
        $body .= " ". $content['login_details']->desname."<br>";
        $body .= "<b> Thanks </b><br>";

       // print_r($content['hr_email']);
      //  die;
        $hremail='';
         $staff_email=$content['hrd_staff_details']->emailid;
          $to_useremail = $staff_email;
         $tc_email=$content['tc_email']->emailid;
         $hremail=$content['hr_email']->emailid;
         $personal=$content['p_email']->EmailID;
         $e_email=$content['ed_email']->EmailID;
          $arr= array (
            $tc_email  =>'tc',
            $hremail   =>'hrd',
            $personal  =>'personal',
            $e_email   =>'ed'
          );
            $to_name='staff';

 

        $this->session->set_flashdata('tr_msg', 'Initiate has been done successfully !!!');
       redirect('/Staff_midterm_review/index/'.$staff);

      }
    }
    else
    {
      $this->session->set_flashdata('tr_msg', 'Transid is not found !!!');
    }
  prepareview:
  $content['staffid'] = $staffid;
  $content['tarnsid'] = $tarnsid;
  $content['subview'] = 'Staff_midterm_review/add';
  $this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
}






}