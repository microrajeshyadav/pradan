<?php 
class Staff_sepwithdrawalofgratuity extends CI_Controller
{

	
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepwithdrawalofgratuity_model');
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->load->model("Common_model","Common_Model");
		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($token)
	{
		// start permission
		//$content['token'] = $id; 
		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			$query ="SELECT * FROM staff_transaction WHERE id=".$token;
			$content['staff_transaction'] = $this->db->query($query)->row();
			$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
			//print_r($content['staff_detail']);
			//die();
			//$content['join_relive_data'] = $this->get_staff_seperation_certificate($content['staff_transaction']->staffid);
			/*echo "<pre>";
			print_r($content['join_relive_data']);exit();*/
			//$query ="SELECT * FROM tbl_exit_interview_form WHERE transid=".$token;
			//$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();

			
			// end permission    
				
				 $query = "SELECT * FROM tbl_wthdrawal_of_gratuity WHERE transid = ".$token;
      $content['gratuity_withdraw'] = $this->db->query($query)->row();
        
        	$RequestMethod = $this->input->server('REQUEST_METHOD');
      if($RequestMethod == "POST"){
                        $db_flag = '';
                        if($this->input->post('Save') == '0'){
                              $db_flag = 0;
                        }else if($this->input->post('saveandsubmit') == '1'){
                              $db_flag = 1;
                        }

                         $insertarray=array(
                          'staffid'=>$content['staff_transaction']->staffid,
                          'transid'=>$token,
                          'sir'=>$this->input->post('name'),
                          'bankname'=>$this->input->post('bank_name'),
                          'bankaddress'=>$this->input->post('b_address'),
                          'accountnumber'=>$this->input->post('account_number'),
                          'flag'=>$db_flag,
                          'updatedby'=>  $this->loginData->staffid,

                          ); 
                         if($content['gratuity_withdraw'])
                         {
                         $updatearray=array(
                          'staffid'=>$content['staff_transaction']->staffid,
                          'transid'=>$token,
                          'sir'=>$this->input->post('name'),
                          'bankname'=>$this->input->post('bank_name'),
                          'bankaddress'=>$this->input->post('b_address'),
                          'accountnumber'=>$this->input->post('account_number'),
                          'flag'=>$db_flag,
                          'updatedby'=>  $this->loginData->staffid,


                          ); 
                         $this->db->where('id', $content['gratuity_withdraw']->id);
          $flag = $this->db->update('tbl_wthdrawal_of_gratuity', $updatearray);
          
                       }
                       else
                       {
                        $flag = $this->db->insert('tbl_wthdrawal_of_gratuity',$insertarray);
                      
                       }
                       if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
        redirect(current_url());
      } 
                else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
               redirect(current_url());
            }
      



                        
}

			//$query ="SELECT * FROM tbl_exit_interview_form WHERE transid=".$id;
			//$content['staff_sepemployeeexitform'] = $this->db->query($query)->row();
			$content['title'] = 'Staff_sepwithdrawalofgratuity';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}
	public function get_staff_clearance_detail($token)
   {
      try
	  {

	     $sql="SELECT c.name,a.date_of_transfer as seperatedate, a.staffid, b.desname,c.emp_code

	       FROM staff_transaction as a
	       INNER JOIN `staff` as c on a.staffid = c.staffid
	       INNER JOIN `msdesignation` as b on a.new_designation = b.desid

	     WHERE a.id = $token";


	     //$query=$this->db->get();
	     return  $this->db->query($sql)->row();

	}

	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }

   }

   public function get_staff_seperation_certificate($staffid=NULL)
   {
      try
	  {
	      $sql="SELECT c.`name`,c.`emp_code`, a.`date_of_transfer` as joniningdate, k.`date_of_transfer` as leavingdate,b.desname as joiningdesignation,g.desname as leavingdesignation,k.reason as leavingreason FROM staff_transaction as a
	         INNER JOIN `staff_transaction` as k ON a.staffid = k.staffid
	         INNER JOIN `staff` as c on a.staffid = c.staffid
	         INNER JOIN `msdesignation` as b on a.new_designation = b.desid
	         INNER JOIN `msdesignation` as g on k.new_designation = g.desid
	         WHERE  K.`trans_status` = 'Retirement' and  a.trans_status = 'JOIN' and a.staffid =".$staffid; 
	     return  $this->db->query($sql)->row();

		}catch (Exception $e) {
	      print_r($e->getMessage());die;
	    }

  }

	
}