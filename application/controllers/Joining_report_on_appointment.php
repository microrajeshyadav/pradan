<?php 


class Joining_report_on_appointment extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $this->load->model('Employee_particular_form_model');
    $this->load->model('General_nomination_and_authorisation_form_model');
    $this->load->model('Joining_report_on_appointment_model');
    $this->load->model("Global_model","gmodel");
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');

 }

 public function index($staff,$candidate_id)
 {
  try{

   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
            
                $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
                redirect('/staff_dashboard/index');
                
            } else {
   
   $staff = $this->uri->segment(3);
   $candidate_id  = $this->uri->segment(4);

    $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id; 

   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);

   $staff_id=$staff;
   $candidateid=$candidate_id;

   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id= $staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }
 $content['candidatedetailwithaddress'] = $this->Joining_report_on_appointment_model->getCandidateWithAddressDetails($staff_id);

 $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);

 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
 $content['report']=$this->Joining_report_on_appointment_model->staff_reportingto($staff_id);


 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);

 $content['candidateaddress'] = $this->Joining_report_on_appointment_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Joining_report_on_appointment_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;

$content['signature'] = $this->Common_Model->staff_signature($staff_id);

    // echo $reportingto; die;
 $content['tc_email'] = $this->Joining_report_on_appointment_model->tc_email($reportingto);

 $query332="SELECT * from tblstaffjoiningreport where candidate_id=$candidateid";
 $content['list_staff_tblstaffjoiningreport_table']=$this->db->query($query332)->row();

 if($content['list_staff_tblstaffjoiningreport_table'])

 $var=$content['list_staff_tblstaffjoiningreport_table']->flag;

else $var ='';

 if ($var==null)
 {
  goto prepareviewss;
}

if($var==0)
{
  redirect('/Joining_report_on_appointment/edit/'.$staff.'/'.$candidateid);
}
elseif($var==1)
{
  redirect('/Joining_report_on_appointment/previewofjoining/'.$staff.'/'.$candidateid);
}


prepareviewss:
$this->load->model('Joining_report_on_appointment_model');

$content['fetchresult']=$this->Joining_report_on_appointment_model->fetchdatas($candidateid);
// print_r($content['fetchresult']); die;


$query2="SELECT * from tblstaffnarrativeselfprofile where candidateid=$candidateid";
$content['list_staff_narrative_table']=$this->db->query($query2)->row();

$query1="Select officename from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();

$content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);
 //print_r($content['candidatedetails']);
$RequestMethod = $this->input->server('REQUEST_METHOD'); 


if($RequestMethod == 'POST'){

 //  print_r($this->input->post()); 


  $operation=$this->input->post('operation'); //die;


  $du=$this->input->post('dutydate');
  $dd=$this->input->post('declarationdate');
    //$co=$this->input->post('Countersigneddate');


  $this->load->model('Joining_report_on_appointment_model');
  $duuu=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($du);
  $dddd=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($dd);
    //$co=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($co);

  if($operation == 0){

   $insertArr = array(
    'address'           => $this->input->post('address'),
    'duty_date'         => $duuu,
    'declaration_date'  => $dddd,
    'declaration_place' => $this->input->post('declarationplace'),
    'candidate_id'      => $candidateid,
    'createdon'         => date('Y-m-d H:i:s'),
    'createdby'         => $this->loginData->staffid,
    'flag'              => 0

  );

   $this->db->insert('tblstaffjoiningreport', $insertArr);
   $insertid = $this->db->insert_id();

   $insert_data = array(
    'type'      => 11,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->staffid,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
   $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully added  JOINING REPORT ON APPOINTMENT');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Joining_report_on_appointment/edit/'.$staff.'/'.$candidateid);


 } else {



   $insertArr = array(
    'address'           => $this->input->post('address'),
    'duty_date'         => $duuu,
    'declaration_date'  => $dddd,
    'declaration_place' => $this->input->post('declarationplace'),
    'candidate_id'      => $candidateid,
    'createdon'         => date('Y-m-d H:i:s'),
    'createdby'         => $this->loginData->UserID,
    'flag'              => 1

  );


   $this->db->insert('tblstaffjoiningreport', $insertArr);
   $insertid = $this->db->insert_id();

   $insert_data =array(
    'type'      => 11,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   => $staff_id
  );
   $this->db->insert('tbl_workflowdetail', $insert_data);
     // echo $this->db->last_query(); die;
   $this->session->set_flashdata('tr_msg', 'Successfully added  JOINING REPORT ON APPOINTMENT');
   $this->session->set_flashdata('er_msg', $this->db->error());


   $subject = ': Joining report on appointment form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Joining report on appointment form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('/Joining_report_on_appointment/previewofjoining/'.$staff.'/'.$candidateid);

 }


}


$content['title'] = 'Joining_report_on_appointment';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function previewofjoining($staff,$candidate_id)
{
  try{

   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
            
                $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
                redirect('/staff_dashboard/index');
                
            } else {

  $staff = $this->uri->segment(3);
                       // $joining_staff=$staff;
  $candidate_id  = $this->uri->segment(4);

   $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id;



  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);





  $staff_id=$staff;
  $login_staff=$this->loginData->staffid;
  $candidateid=$candidate_id;
  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         // echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }

 $content['candidatedetailwithaddress'] = $this->Joining_report_on_appointment_model->getCandidateWithAddressDetails($staff_id);
 // print_r($content['candidatedetailwithaddress']); die; 
 $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);
 $content['id']=$this->Joining_report_on_appointment_model->getworkflowid($staff_id);
 // print_r($content['id']); die;
 $content['candidateaddress'] = $this->Joining_report_on_appointment_model->getCandidateWithAddressDetails($staff_id);
        $content['personnal_mail'] = $this->Joining_report_on_appointment_model->personal_email();
      // print_r($content['personnal_mail']); die;
        $personal_email=$content['personnal_mail']->EmailID;

        $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
        $content['tc_email'] = $this->Joining_report_on_appointment_model->tc_email($reportingto);
        $content['reporting'] = $this->Joining_report_on_appointment_model->getCandidateWith($reportingto);

        $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;

$content['dataaa'] = $this->Joining_report_on_appointment_model->fetchsssssss($candidateid);
   
$content['signature'] = $this->Common_Model->staff_signature($staff_id);
     

 $content['personal']=$this->Common_Model->get_Personnal_Staff_List();

 if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
 {
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST'){
                               //print_r($_POST);die;
    $status=$this->input->post('status');
    $reject=$this->input->post('reject');
    $p_status=$this->input->post('p_status');
    $r_id=$this->input->post('id');

    $check = $this->session->userdata('insert_id');

    if($status==2){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    }

    $insert_data =array(
      'type'                 => 11,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $p_status,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );

    $this->db->insert('tbl_workflowdetail', $insert_data);
    $arr = array (
      'countersigned_date'  =>$this->gmodel->changedatedbformate($this->input->post('Countersigneddate')),
      'countersigned_place' =>$this->input->post('Countersignedplace'),
      'countersigned_name'  =>$login_staff
    );
    $this->db->where('id',$r_id); 
    $this->db->update('tblstaffjoiningreport',$arr);


$subject = ': Joining report on appointment form';
   $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.'Approved by Tc </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
    $personal_email => 'personal'
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('/Joining_report_on_appointment/previewofjoining/'.$staff.'/'.$candidateid);

  }


}
else if($this->loginData->RoleID==17)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){
                               //print_r($_POST);die;
  $status=$this->input->post('status');
  $reject=$this->input->post('reject');
  $p_status=$this->input->post('p_status');
  $r_id=$this->input->post('id');

  $check = $this->session->userdata('insert_id');

   if($status==4){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    }


  $insert_data =array(
    'type'                 => 11,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );



  $arr= array (

    'location'                  => $this->input->post('newoffice'),
    'finance_personnel_misunit' => $login_staff
  );
  $this->db->where('id',$r_id); 
  $this->db->update('tblstaffjoiningreport',$arr);
  $this->db->insert('tbl_workflowdetail', $insert_data);
       // echo $this->db->last_query();
       //  die;

$subject = ': Joining report on appointment form';
   $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel</h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
    $personal_email => 'personal'
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('/Joining_report_on_appointment/previewofjoining/'.$staff.'/'.$candidateid);

}


}





 // $candidateid=($this->loginData->candidateid);
  //$staffids=($this->loginData->staffid);

$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();

$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);


$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();
  //print_r($content['officename_list']);
  //die();

$this->load->model('Joining_report_on_appointment_model');
$content['fetchresult']=$this->Joining_report_on_appointment_model->fetchdatas($candidateid);
  //print_r($content['fetchresult']);
  //die();



$this->load->model('Joining_report_on_appointment_model');
$content['dataofidentity']=$this->Joining_report_on_appointment_model->fetchdata($candidateid);
$content['locationname']=$this->Joining_report_on_appointment_model->all_officename($staff_id);
/* echo "<pre>"; 
   print_r($content['dataofidentity']); die;*/





$content['subview'] = 'Joining_report_on_appointment/previewofjoining';
$this->load->view('_main_layout', $content);
}
}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function Add($staff=null,$candidate_id=null)
{
  try{


 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);




 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

}



$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();

$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
prepareview:


$content['subview'] = 'Joining_report_on_appointment/add';
$this->load->view('_main_layout', $content);
}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



public function Edit($staff,$candidate_id)
{
  try{

   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
            
                $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
                redirect('/staff_dashboard/index');
                
            } else {

  $staff = $this->uri->segment(3);
  $candidate_id  = $this->uri->segment(4);

  $content['staff'] = $staff;
    $content['candidate_id'] = $candidate_id;

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);


  $staff_id=$this->loginData->staffid;
  $candidateid=$this->loginData->candidateid;




  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
    $candidateid=$candidateid;

  }
  else
  {
   $staff_id=$staff;
         // echo "staff".$staff_id;
   $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

 }


 $content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
 $content['candidateaddress'] = $this->Joining_report_on_appointment_model->getCandidateWithAddressDetails($staff_id);
        $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
        $personal_email=$content['personnal_mail']->EmailID;

        $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
        $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);
        // start permission 



 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

 $content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);
 $content['candidatedetailwithaddress'] = $this->Joining_report_on_appointment_model->getCandidateWithAddressDetails($staff_id);

$content['dataaa'] = $this->Joining_report_on_appointment_model->fetchsssssss($candidateid);

$content['signature'] = $this->Common_Model->staff_signature($staff_id);

 $this->load->model('Joining_report_on_appointment_model');
 $content['fetchresult']=$this->Joining_report_on_appointment_model->fetchdatas($candidateid);



 $query1="Select * from tbl_table_office";
 $content['officename_list']=$this->db->query($query1)->result();

 $this->load->model('Joining_report_on_appointment_model');
 $content['dataofidentity']=$this->Joining_report_on_appointment_model->fetchdata($candidateid);
// print_r($content['dataofidentity']); die;

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){


  $du=$this->input->post('dutydate');

  $dd=$this->input->post('declarationdate');
    //$co=$this->input->post('Countersigneddate');


  $this->load->model('Joining_report_on_appointment_model');
  $duuu=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($du);

  $dddd=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($dd);
     //$co=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($co);

  $operation=$this->input->post('operation');

  if($operation == 0){

   $updateArr = array(
    'address'           => $this->input->post('address'),
    'duty_date'         => $duuu,
    'declaration_date'  => $dddd,
    'declaration_place' => $this->input->post('declarationplace'),
    'candidate_id'      => $candidateid,
    'updatedon'         => date('Y-m-d H:i:s'),
    'updatedby'         => $this->loginData->UserID,
    'flag'              => 0

  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tblstaffjoiningreport', $updateArr);
   
   $this->session->set_flashdata('tr_msg', 'Successfully added  JOINING REPORT ON APPOINTMENT');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Joining_report_on_appointment/edit/'.$staff.'/'.$candidateid);


 } else {



   $updateArrs = array(
    'address'           => $this->input->post('address'),
    'duty_date'         => $duuu,
    'declaration_date'  => $dddd,
    'declaration_place' => $this->input->post('declarationplace'),
    'signature'         => $updateArr['photoupload1_encrypted_filename'],
    'candidate_id'      => $candidateid,
    'updatedon'         => date('Y-m-d H:i:s'),
    'updatedby'         => $this->loginData->UserID,
    'flag'              => 1

  );


   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tblstaffjoiningreport', $updateArrs);


   $this->session->set_flashdata('tr_msg', 'Successfully added  JOINING REPORT ON APPOINTMENT');
   $this->session->set_flashdata('er_msg', $this->db->error());

   $subject = ': Joining report on appointment form';
   $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Joining report on appointment form </h4><br />';
   $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
   <tr>
   <td width="96">Name </td>
   <td width="404">'.$content['candidateaddress']->staff_name.'</td>
   </tr>
   <tr>
   <td>Employ Code</td>
   <td> '.$content['candidateaddress']->emp_code.'</td>
   </tr>
   <tr>
   <td>Designation</td>
   <td>' .$content['candidateaddress']->desiname.'</td>
   </tr>
   <tr>
   <td>Office</td>
   <td>'.$content['candidateaddress']->officename.'</td>
   </tr>
   </table>';
   $body .= "<br /> <br />";
   $body .= "Regards <br />";
   $body .= " ". $content['candidateaddress']->staff_name ."<br>";
   $body .= " ". $content['candidateaddress']->desiname."<br>";
   $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
   $to_useremail = $content['candidateaddress']->emailid;
   $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
   $arr= array (
    $tcemailid      =>'tc',
  );

   $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
   redirect('/Joining_report_on_appointment/previewofjoining/'.$staff.'/'.$candidateid);

 }


}


$content['subview'] = 'Joining_report_on_appointment/edit';
$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function getpdfjoining($staff=null,$candidate_id=null)
{
  try{

 // $candidateid=($this->loginData->candidateid);
  //$staffids=($this->loginData->staffid);
 $this->session->set_userdata('staff', $staff);
 $this->session->set_userdata('candidate_id', $candidate_id);




 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
  $candidateid=$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff_id;
 $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;

}





$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();

$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);


$query1="Select * from tbl_table_office";
$content['officename_list']=$this->db->query($query1)->row();
$rows=$content['officename_list'];

$content['dataaa'] = $this->Joining_report_on_appointment_model->fetchsssssss($candidateid);

$row=$content['dataaa'];
  //print_r($content['dataaa']);

$timestamp = strtotime($row->declaration_date);
$datee=date('d/m/Y', $timestamp);
// $staffsign=site_url('datafiles/signature/'.$row->signature);

if ($row->signature !='') {
        $staffsign = site_url('datafiles/signature/'.$row->signature);
      }else{ 
      // $staffsign = 'C:/xampp/htdocs/pradanhrm/datafiles/imagenotfound.jpg';
             $staffsign = site_url('datafiles/imagenotfound.jpg');
        }

 $candidate = array('$rowsofficename','$rowaddress','$rowofferno','$rowcandidatefirstname','$rowdoj','$rowdesname','$rowduty_date','$staffsign','$datee','$rowdeclaration_place');
 $candidate_replace = array($rows->officename,$row->address,$row->offerno,$row->candidatefirstname,$row->doj,$row->desname,$row->duty_date,$staffsign,$datee,$row->declaration_place);

// $html='  
// <div class="container">

// <div class="row clearfix">
// <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
// <div class="card">

// <div class="body">
// <div class="row clearfix doctoradvice">
// <h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br> PREVIEW: JOINING REPORT ON APPOINTMENT </h4>

// <form method="POST" action="" enctype="multipart/form-data">
// <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
// <div class="form-group">

// <label for="ExecutiveDirector" class="field-wrapper required-field">To:<br>The Executive Director,<br>
// PRADAN.</label>
// <br>
// '.$rows->officename.'
// </br>
// </div>
// </div>
// <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
// </div>



// <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right">
// <label for="Name">Address:&nbsp;</label>&nbsp;
// '.$row->address.'


// </div>

// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
// <div class="form-group">
// <label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir, </label>
// </div>
// </div>

// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
// <div class="form-group">
// <label class="field-wrapper required-field">Subject: Joining Report </label>
// </div>
// </div>
// <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
// <div class="form-group">
// Please refer to your offer of appointment no &nbsp;<b>'. $row->offerno.'</b>&nbsp;dated
// &nbsp;<b>
// '.$row->doj.'</b>&nbsp;
// offering me appointment as  &nbsp;<b>'.$row->desname.'</b>&nbsp;at
// &nbsp;<b>'.$row->officename.'</b>&nbsp;
// I hereby report for duty in the forenoon of today,<br><br> the 
// &nbsp; 
// <b>
// '.$row->duty_date.'
// </b>
// (date/month/year). I shall inform you of any change in my address,given above, when it occurs.
// <br><br>
// Yours faithfully,
// <br>
// <br></div></div>
// <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
// <p class="text-left">Signature
// :<img src="'.$sig.'"  style="height:64px;width:256px;" >

// </p>
// <p class="text-left">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$row->candidatefirstname.'</b></p>
// </div>
// <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
// <p class="text-right">Place:&nbsp;
// <b>'.$row->declaration_place.'</b>
// </p>
// <p class="text-right" >Date:&nbsp;
// <b>
// '.$datee.'

// </b>
// </p>
// </div><br><br>


// </form>
// </div>
// </div></div></div></div></div>';

 $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 20 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
            if(!empty($data))
              $body = str_replace($candidate,$candidate_replace , $data->lettercontent);
$body=  html_entity_decode($body); 
$filename = "".$token."joining";
$this->Employee_particular_form_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');  
  //$this->session->set_flashdata('tr_msg', 'Successfully Generate Offer Letter');     

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }


}

}