<?php 

/**
* State List
*/
class Staff_resignation_withdrawal extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    $this->load->model("Staff_resignation_withdrawal_model","Staff_resignation_withdrawal_model");
    
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $query = "select * from mskeyrules where isdeleted=0";
   $content['Staff_resignation_withdrawal_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";


   $content['title'] = 'Staff_resignation_withdrawal';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }

   /**
   * Method resignation_withdrawal() resignation request withdrawal.
   * @access  public
   * @param Null
   * @return  row
   */

 public function resignation_withdrawal($token)
 {

  


        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
  $RequestMethod = $this->input->server('REQUEST_METHOD'); 


  if($RequestMethod == 'POST'){

$this->db->trans_start();

    $insertArr = array(
   
      'trans_flag'       => 0,
      'updatedon'        => date("Y-m-d H:i:s"),
      'updatedby'        => $this->loginData->staffid,
     
    );
    // print_r($insertArr); die;
    $this->db->where('id',$token);
    $this->db->update('staff_transaction', $insertArr);
  //  echo $this->db->last_query(); die;
    //$insertid = $this->db->insert_id();

   
    

    $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
         echo 0;
        $this->session->set_flashdata('tr_msg', 'Not resignation withdrawal !!!');
     }else{
         echo 1;
         $this->session->set_flashdata('tr_msg', 'Successfully resignation withdrawal !!!');
     }

   
   // redirect('/Staff_resignation_withdrawal/viewhistory/'.$token);

  
  }



  //$content['staff_list'] = $this->Staff_resignation_withdrawal_model->getdata();
  //die("hello");

  $content['subview'] = 'Staff_resignation_withdrawal/viewhistory';
  $this->load->view('_main_layout', $content);
}



public function staffresignationui()
{

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 


  $content['subview'] = 'Staff_resignation_withdrawal/staffresignationui';
  $this->load->view('_main_layout', $content);
}

public function viewhistory($token)
{

//echo $token;die();
 // $this->load->model('Staff_resignation_withdrawal_model');
  $content['getseperation']=$this->Staff_resignation_withdrawal_model->get_staff_seperation_detail($token);
  $content['view_history']=$this->Staff_resignation_withdrawal_model->fetchdatas($token);


  $content['subview'] = 'Staff_resignation_withdrawal/viewhistory';
  $this->load->view('_main_layout', $content);
}


}