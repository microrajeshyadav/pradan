<?php 
class Developmentapprentice_sepchecksheet_fc extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Staff_sepchecksheet_model');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_approval_model");
    $this->load->model("Staff_seperation_model");
    $this->load->model('Developmentapprentice_sepchecksheet_fc_model','model');

    $this->load->model(__CLASS__ . '_model');
    $mod = $this->router->class.'_model';
    $this->load->model($mod,'',TRUE);
    $this->model = $this->$mod;

    $check = $this->session->userdata('login_data');

    ///// Check Session //////    
    if (empty($check)) {
            redirect('login');                  
    }

    $this->loginData = $this->session->userdata('login_data');
  
  }

      public function index($token)
      {
        // echo "hhhh";
        // die;
       // print_r($this->loginData);

            // start permission 
        try{

            // echo "token=".$token;
            // die;
            if($token){
              $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
              $content['role_permission'] = $this->db->query($query)->result();
              // end permission   
              // $sql  = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
              // $result  = $this->db->query($sql)->result()[0];
              // $forwardworkflowid = $result->workflowid; 
              $query ="SELECT * FROM staff_transaction WHERE id=".$token;
              $content['staff_transaction'] = $this->db->query($query)->row();              
              $content['staff_detail'] = $this->model->get_staff_sep_detail($token);
              // echo "<pre>";print_r($content['staff_detail']);
              // die;
              $content['finance_image']=$this->gmodel->getFinanceAdministratorEmailid();
               
             $financeid=$content['finance_image']->staffid;
              $content['finance_image'] = $this->Common_Model->staff_signature($financeid);
        
                $content['da_image'] = $this->Common_Model->staff_signature($content['staff_detail']->staffid);

                $content['supervisor_image'] = $this->Common_Model->staff_signature($content['staff_detail']->reportingto);
                //$sql2="select * from tbl_workflowdetail where "
                /*echo "<pre>";
                print_r($content['staff_transaction']);
                die;*/
              // $content['personaldetail'] = $this->Staff_approval_model->getExecutiveDirectorList(17);

              // $personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
              $content['hrlist'] = $this->Common_Model->get_hr_Staff_List();
              
              $hrquery = "select receiver from tbl_workflowdetail where r_id='$token'";
              // echo $hrquery;
              // die;

              
              $content['selectedhr'] = $this->db->query($hrquery)->row();
              // echo "<pre>";
              //  print_r($content['selectedhr']);exit();
              $content['token'] = $token;
              
              $query ="SELECT * FROM tbl_da_check_sheet WHERE transid=".$token;
              $content['tbl_check_sheet'] = $this->db->query($query)->row();
              
             /* echo "<pre>";
              print_r( $content['tbl_check_sheet']);
              die;*/
              $RequestMethod = $this->input->server('REQUEST_METHOD');
              if($RequestMethod == "POST"){
                    $flag='';  

                    $db_flag = '';
                    if($this->input->post('Save') == 'Save'){
                          $db_flag = 0;
                    }else{
                      
                      //die('sdfs');
                      $receiverdetail = $this->model->get_staffDetails($this->input->post('curuserid'));
                      //die('sdfs');
                      
                      /*echo "<pre>";
                      print_r($token);exit();*/
                      $db_flag = 1;
                      $comments = 'Check Sheet form filled by finance';
                      $updateArr = array(
                              'trans_flag'     => 5,
                              'updatedon'      => date("Y-m-d H:i:s"),
                              'updatedby'      => $this->loginData->staffid
                            );

                      $this->db->where('id',$token);
                      $this->db->update('staff_transaction', $updateArr);
                      $insertworkflowArr = array(
                        'r_id'           => $token,
                        'type'           => 29,
                        'staffid'        => $content['staff_detail']->staffid,
                        'sender'         => $this->loginData->staffid,
                        'receiver'       => $content['staff_transaction']->reportingto,
                        'senddate'       => date("Y-m-d H:i:s"),
                        'flag'           => 5,
                        'scomments'      => $comments,
                        'createdon'      => date("Y-m-d H:i:s"),
                        'createdby'      => $this->loginData->staffid,
                      );
                      $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
                      // $subject = "Check Sheet Form Detail";
                      // $body = 'Dear,<br><br>';
                      // $body .= '<h2>Check Sheet Form Submited Successfully </h2><br>';
                      // $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> your Check Sheet Form has filled  .<br><br>';
                      // $body .= 'Thanks<br>';
                      // $body .= 'Administrator<br>';
                      // $body .= 'PRADAN<br><br>';

                      // $body .= 'Disclaimer<br>';
                      // $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';


                      // $to_email = $receiverdetail->emailid;
                      // $to_name = $receiverdetail->name;
                      //$to_cc = $getStaffReportingtodetails->emailid;
                      // $recipients = array(
                      // $content['staff_detail']->emailid => $content['staff_detail']->name
                      // // ..
                      // );
                      // $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
                      // if (substr($email_result, 0, 5) == "ERROR") {
                      //   $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
                      // }
                    }
                    

              if($content['tbl_check_sheet']){
                  $data_update = array(
                  'staffid'=>$content['staff_transaction']->staffid,
                  'transid'=>$token,
                  //'date_resignation_submitted_7'=>$this->gmodel->changedate($this->input->post('date_resignation_submitted_7')),
                  //'date_from_which_separation_takes_effect_8'=>$this->gmodel->changedate($this->input->post('date_from_which_separation_takes_effect_8')),
                  'whether_under_probation_9'=>$this->input->post('whether_under_probation_9'),
                  'if_not_under_probation_10'=>$this->input->post('if_not_under_probation_10'),
                  'Vehicle_Loan'=>$this->input->post('Vehicle_Loan'),
                  'stipend_in_lieu_of_excess_leave'=>$this->input->post('stipend_in_lieu_of_excess_leave'),
                  'other_loans_and_advances'=>$this->input->post('other_loans_and_advances'),

                  'outstanding_dues'=>$this->input->post('outstanding_dues'),
                  'stipend'=>$this->input->post('stipend'),
                  'personal_claim'=>$this->input->post('personal_claim'),
                  'any_other'=>$this->input->post('any_other'),
                  'payments_due_to_employee'=>$this->input->post('payments_due_to_employee'),
                  'amounts_recoverable'=>$this->input->post('amounts_recoverable'),
                  'net_payable_recoverable'=>$this->input->post('net_payable_recoverable'),
                  'updatedon'=>date('Y-m-d H:i:s'),
                  'updatedby'=>$this->loginData->staffid,
                  'flag'=>$db_flag,
                  'financeid'=>$this->input->post('curuserid'),
                  'financeapprovaldate'=>$this->gmodel->changedate($this->input->post('approvaldate')),              
                );




                          $this->db->where('id', $content['tbl_check_sheet']->id);
                          $flag = $this->db->update('tbl_da_check_sheet', $data_update);

                       
             
            


            
                                 }
                                 else
                                 {


              $data = array(
                      'staffid'=>$content['staff_transaction']->staffid,
                      'transid'=>$token,
                      //'date_resignation_submitted_7'=>$this->gmodel->changedate($this->input->post('date_resignation_submitted_7')),
                      //'date_from_which_separation_takes_effect_8'=>$this->gmodel->changedate($this->input->post('date_from_which_separation_takes_effect_8')),
                      'whether_under_probation_9'=>$this->input->post('whether_under_probation_9'),
                      'if_not_under_probation_10'=>$this->input->post('if_not_under_probation_10'),
                      'Vehicle_Loan'=>$this->input->post('Vehicle_Loan'),
                      'stipend_in_lieu_of_excess_leave'=>$this->input->post('stipend_in_lieu_of_excess_leave'),
                      'other_loans_and_advances'=>$this->input->post('other_loans_and_advances'),

                      'outstanding_dues'=>$this->input->post('outstanding_dues'),
                      'stipend'=>$this->input->post('stipend'),
                      'personal_claim'=>$this->input->post('personal_claim'),
                      'any_other'=>$this->input->post('any_other'),
                    
                      'payments_due_to_employee'=> $this->input->post('payments_due_to_employee'),
                      'amounts_recoverable'=> $this->input->post('amounts_recoverable'),
                      'net_payable_recoverable'=>$this->input->post('net_payable_recoverable'),
                      'createdon'=>date('Y-m-d'),
                      'createdby'=>$this->loginData->staffid,
                      'flag'=>$db_flag,
                      'financeid'=>$this->input->post('curuserid'),
                      'financeapprovaldate'=>$this->gmodel->changedate($this->input->post('approvaldate')),
                      );
              
                $flag=$this->db->insert('tbl_da_check_sheet', $data);
               
                        
              }

          // $hrdname = '';
          // $email_result = '';
          // $hrdname  = $this->loginData->UserFirstName." ".$this->loginData->UserLastName;
          // $pa = $this->gmodel->getPersonnelAdministratorEmailid();


          // $subject1 = "Graduating Apprentices";
          // $body1  = "<h4>Dear Personnel Unit".", </h4><br />";
          // $body1 .= $content['tbl_check_sheet']->id." are going to graduate. Kindly check their data and process their offer letters as Executives.".",<br />";
          // $body1 .= "<b> Regards, </b><br>";
          // $body1 .= "<b> Barsha Mishra </b><br>";
          // $body1 .= "<b> HRD Unit </b><br>";
    
          // $to_email1 = $pa->EmailID;
          // // $to_name = $content['staff_transaction']->hr_name;
          // $recipients = [];
          // $email_result = $this->Common_Model->send_email($subject1, $body1, $to_email1, $to_name= null, $recipients);
                    


          if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');} 
          else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');}
          }


              
          }
          // echo "<pre>";
          // print_r($data_tbl_check_sheet_transaction['head1']);
          // exit();
          $query = "SELECT a.*, b.name as finance_name, c.officename as finance_office, d.name as superviser_name,
                  e.officename as superviser_office, f.createdby as hrid  FROM tbl_da_check_sheet a
                  left join staff b on a.financeid = b.staffid
                  left join lpooffice c on b.new_office_id = c.officeid
                  left join staff d on a.superviserid = d.staffid
                  
                  left join lpooffice e on d.new_office_id = e.officeid
                  left join staff_transaction f on a.transid = f.id
                  WHERE a.transid=".$token;
          $content['tbl_check_sheet'] = $this->db->query($query)->row(); 
          // print_r($content['tbl_check_sheet']);
          // die;

          $query = "SELECT a.name, b.officename as location  FROM staff a INNER JOIN lpooffice b on a.new_office_id=b.officeid where a.staffid=".$this->loginData->staffid;
          $content['curUserDetails'] = $this->db->query($query)->row(); 

          $content['title'] = 'Staff_sepchecksheet';
          $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
          $this->load->view('_main_layout', $content);
       
         }
        catch(Exception $e)
        {
            print_r($e->getMessage());
            die();
         }
            
      }

      
}