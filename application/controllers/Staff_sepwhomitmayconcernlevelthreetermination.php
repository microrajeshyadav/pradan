<?php 
class Staff_sepwhomitmayconcernlevelthreetermination extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepwhomitmayconcernlevelthreetermination_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");
		
		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token = null)
	{
		// start permission 
		try{
			  if($token){
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
			// end permission    


				$query ="SELECT * FROM staff_transaction WHERE id =".$token; 
				$content['seplevel_threetermination'] = $this->db->query($query)->row();

				$content['level_threetermination'] = $this->Common_Model->get_staff_sep_detail($token);
				$content['staffid'] = $content['seplevel_threetermination']->staffid;
				

				$query = "SELECT * FROM tbl_levelwisetermination WHERE type = 2 and transid = ".$token;
				$content['levelthreetermination_detail'] = $this->db->query($query)->row();

				// print_r($content['levelthreetermination_detail']); die();
				$tdate = date('d/m/Y');
				$emp_code = $content['level_threetermination']->emp_code;
			$staffname = $content['level_threetermination']->name;
			$dc_name = $content['level_threetermination']->dc_name;
			$sepdesig = $content['level_threetermination']->sepdesig;
			$joiningdate = $this->gmodel->changedatedbformate($content['level_threetermination']->joiningdate);
			$seperatedate = $this->gmodel->changedatedbformate($content['level_threetermination']->seperatedate);
			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 86 AND `isactive` = '1'";
   		    $data = $this->db->query($sql)->row();

   		    if (!empty($content['levelthreetermination_detail'])) {
   		    	// print_r($content['levelthreetermination_detail']->sep_no); die();
   		    	$sep_no = $content['levelthreetermination_detail']->sep_no;
   		    	$total_staff = $content['levelthreetermination_detail']->total_staff;
				$total_professional = $content['levelthreetermination_detail']->total_professional;
				$staff1 = array();
   		    	$staff1_replace = array();
				$staff1 = array('$sep_no','$emp_code','$tdate','$name','$dc_name','$sepdesig','$joiningdate','$total_staff','$total_professional','$seperatedate','$edsign');
				$staff1_replace = array($sep_no,$emp_code,$tdate,$staffname,$dc_name,$sepdesig,$joiningdate,$total_staff,$total_professional,$seperatedate,'');

				if(!empty($data))
				$body = str_replace($staff1,$staff1_replace , $data->lettercontent);

				$body = str_replace('$total_staff',$total_staff, $body);
				$body = str_replace('$total_professional',$total_professional, $body);
				// echo $body; die();

			}
			else{
				$staff1 = array();
   		    	$staff1_replace = array();
				$staff1 = array('$sep_no','$emp_code','$tdate','$name','$dc_name','$sepdesig','$joiningdate','$total_staff','$total_professional','$seperatedate','$edsign');
				$staff1_replace = array('',$emp_code,$tdate,$staffname,$dc_name,$sepdesig,$joiningdate,'','',$seperatedate,'');
				if(!empty($data))
					$body = str_replace($staff1,$staff1_replace , $data->lettercontent);
			}

			if(!empty($body))
   		    {
   		    	$content['content'] = $body;
   		    }



				// print_r($content['level_threetermination']); die;

				// print_r($content['levelfour_detail']); die;

				$content['staffid'] = $content['seplevel_threetermination']->staffid;

				$RequestMethod = $this->input->server("REQUEST_METHOD");


				if($RequestMethod == 'POST')
				{

					
					$db_flag = '';
					if($this->input->post('Save') == 'Save'){
						$db_flag = 0;
					}else if($this->input->post('saveandsubmit') == 'Save And Submit'){
						$db_flag = 1;
					}

					// print_r($this->input->post()); die();
					$body = '';
				$body = $this->input->post('lettercontent');
				$filename = "";
				$filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'EXPERIENCECERTIFICATE.pdf');

					$insertArray = array(

						'desid'              => $this->input->post('sepdesigid'),
						'staffid'            => $content['seplevel_threetermination']->staffid,
						'transid'            => $token,
						'sep_no'             => $this->input->post('sep_no'),
						'total_staff'        => $this->input->post('total_staff'),
						'total_professional' => $this->input->post('total_professional'),
						'filename'		 	 => $filename,
						'content'		 	 => $body,
						'flag'               => $db_flag,
						'type'               => 2,
						'createdby'          => $this->loginData->staffid,
					);

					if($content['levelthreetermination_detail'])
					{
						$updateArray = array(
							'desid'              => $this->input->post('sepdesigid'),	
							'staffid'            => $content['seplevel_threetermination']->staffid,
							'transid'            => $token,
							'sep_no'             => $this->input->post('sep_no'),
							'total_staff'        => $this->input->post('total_staff'),
							'total_professional' => $this->input->post('total_professional'),
							'flag'               => $db_flag,
							'filename'		 	 => $filename,
							'content'		     => $body,
							'type'               => 2,
							'updatedby'          => $this->loginData->staffid,
							'updatedon'          => date("Y-m-d H:i:s"),
						);
						$this->db->where('id', $content['levelthreetermination_detail']->id);
						$flag = $this->db->update('tbl_levelwisetermination', $updateArray);
					}else{
						$flag = $this->db->insert('tbl_levelwisetermination',$insertArray);
					}
					//echo $this->db->last_query(); die;

					if($flag) {
					  $subject = "Your Request Submited Successfully";
					  $body = 'Dear,<br><br>';
					  $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
					  $body .= 'This is to certify that Mr. '.$content['level_threetermination']->name.'<br> has issued experience certificate.<br><br>';
					  $body .= 'Thanks<br>';
					  $body .= 'Administrator<br>';
					  $body .= 'PRADAN<br><br>';

					  $body .= 'Disclaimer<br>';
					  $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

					  $to_email = $content['level_threetermination']->emailid;
					  $to_name = $content['level_threetermination']->name;
					  $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name);
					  if (substr($email_result, 0, 5) == "ERROR") {
					  	$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
					  }
						$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
						redirect(current_url());
					} 
					else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
					redirect(current_url());
					}


		}


		$content['title'] = 'Staff_sepwhomitmayconcernlevelthreetermination';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}else{
		$this->session->set_flashdata('er_msg','Missing trans id!!');
        header("location:javascript://history.go(-1)", 'refresh');
	}
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}