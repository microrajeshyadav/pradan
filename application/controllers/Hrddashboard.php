<?php 
class Hrddashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
		// print_r($this->loginData);exit();
	}

	public function index()
	{
		try{
		$this->load->model('Hrddashboard_model');
		
		$content['selectedcandidatedetails']      = $this->model->getSelectedCandidate();		
		$content['selectedexecutivedetails']      = $this->model->getSelectedExecutive();
		$content['campusdetails'] 	              = $this->model->getCampus();
		$content['recruiters_list']               = $this->model->getRecruitersList();
		$content['comments']                      = $this->model->getComments();
		/*echo "<pre>";
		print_r($content['selectedexecutivedetails']);exit();*/
		
		$content['title'] = 'Hrddashboard';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	public function convert_executive($token)
	{
		try{
		
			$sql="select candidateid from staff where staffid='$token'"; 
			$res = $this->db->query($sql)->row();
			// print_r($res);
			// die;
			$candidate_id=$res->candidateid;

		$UpdateArr = array(
				 	'RoleID' => 3,

				 	

				 	
				  );

				 $this->db->where('staffid',$token);
                 $this->db->update('mstuser', $UpdateArr);


                 

                 $Updatedes = array(
				 	'designation' => 10,
				 	
				  );

				 $this->db->where('staffid',$token);
                 $this->db->update('staff', $Updatedes);

                 $Updatecat = array(
				 	'categoryid' => 2
				 	
				  );

				 $this->db->where('candidateid',$candidate_id);
                 $this->db->update('tbl_candidate_registration', $Updatecat);


					$this->session->set_flashdata('tr_msg', 'Successfully DA converted to Executive');	
					redirect('Hrddashboard/index');

		
		
		$content['title'] = 'Hrddashboard';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

	public function Offercomments($token){

	try{
			
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

			    $this->form_validation->set_rules('recruiters','Recruiters','trim|required');
	            $this->form_validation->set_rules('comments','Comments ','trim|required');
	                   
	            if($this->form_validation->run() == FALSE){
	                $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
	                    '</div>');

	                $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

	                $hasValidationErrors    =    true;
	                goto prepareview;

	            }


	            $this->db->trans_start();

			$insertArr2 = array(
				'candidateid'     => $token,
				'recutersid'      => $this->input->post("recruiters"),
				'comments'  	  => $this->input->post('comments'),
				'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'       => $this->loginData->UserID, // login user id
			    
			  );
			$this->db->insert('edcomments', $insertArr2);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding Comments');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Comments');			
			}
			redirect('/EDashboard/Offercomments');
			
			}

			prepareview:

			$content['recruiters_list']     = $this->model->getRecruitersList();
			$content['title'] = 'Offercomments';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}
	
	
	public function sendofferlettertocandidate(){

	try{

			$candidatemobile = 0;
			$candidateid = 0;
			$candidateemail = '';
			$candidatfirstname = '';
			$candidatmiddlename = '';
			$candidatlastname ='';
			$filename = '';
			$newpassword ='';
			// echo "<pre>";
			// print_r($_POST);
			// echo "0"; 
			$hrname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
			$token = $this->input->post('token');
			
			$getCandidateDetails = $this->model->getCandidateDetails($token); //// candidate 
			$getCandidateDoj = $this->model->get_offerdate($token); //// candidate date of joining
			// print_r($getCandidateDoj); 
			//print_r($getCandidateDetails); die;
			$categoryid = $getCandidateDetails->categoryid;

			if (!empty($categoryid) && $categoryid == 3 ) {
		 		$category = 'Assistant';
		 	}else if (!empty($categoryid) && $categoryid == 2 ) {
		 		$category = 'Executive';
		 	}else if (!empty($categoryid) && $categoryid == 1 ) {
		 		$category = 'Development Apprentice';
		 	}

			$date_joining= $this->gmodel->changedatedbformate($getCandidateDoj->doj); 
			$officename= $getCandidateDetails->officename;
			$stream= $getCandidateDetails->stream;
			$candidatemobile = $getCandidateDetails->mobile;
 			$candidateid = $getCandidateDetails->candidateid;
		    $to_email = $getCandidateDetails->emailid;  //// Email Id ////
		    // echo $to_email;die();
			$candidatfirstname   = $getCandidateDetails->candidatefirstname;  //// candidate Name ////
			$candidatmiddlename   = $getCandidateDetails->candidatemiddlename;  //// candidate Name ////
			$candidatlastname   = $getCandidateDetails->candidatelastname;  //// candidate Name ////
			$filename       = $getCandidateDetails->filename;  //// file Name ////
			$newpassword    = md5($candidatfirstname);
			$addlink = site_url('login/login1');

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			/*Author : Deepak Kumar 11-06-2019
			multiple file upload*/
			$others_document = $this->input->post('others_document'); 

			$hrdemail       = $this->loginData->EmailID; /////// hrd email id/////

			$filename1 = $filename.'.pdf';

			$offerattachments = array($filename1);

			$recipients = array(
				$this->loginData->EmailID => 'Personnel',
			 );


			if(isset($_FILES['others_document']['name']))
           {
               if($_FILES['others_document']['name'][0] != NULL)
               {
			foreach($_FILES['others_document']['name'] as $k=>$v){
				@  $ext = end((explode(".", $_FILES['others_document']['name'][$k])));
				$matricoriginalName = $_FILES['others_document']['name'][$k]; 
				$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['others_document']['tmp_name'][$k];
				$targetPath = FCPATH . "pdf_offerletters/";
				$targetFile = $targetPath . $matricencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);
				array_push($offerattachments, $matricencryptedName);
				$dataset  = array(
					'candidateid'=> $token,
					'originaldocumentname'=> $matricoriginalName,
					'encrypteddocumentname'=> $matricencryptedName,
					'createdon'=> date('Y-m-d H:i:s'),
					'createdby'=> $this->loginData->UserID,
				);
				$this->db->insert('tbl_offers_othersdocument', $dataset);
				}
				}
			}
			/*echo "<pre>";
			print_r($offerattachments);
			exit();*/

			
			$to_name = $getCandidateDetails->candidatefirstname;
			$message = '';
			$subject = "Sharing the offer letter and other documents";

			// $fd = fopen("mailtext/offerletter_to_candidate.txt","r");	
			// $message .=fread($fd,4096);
			// eval ("\$message = \"$message\";");
			// $message =nl2br($message);
						
			$candidateI = array('$candidatfirstname','$category','$date_joining','$hrname','$to_email','$addlink');
			$candidate_replace = array($candidatfirstname,$category,$date_joining,$hrname,$to_email,$addlink);
			// $arr = array_combine($candidateI, $candidate_replace);
			// print_r($arr);
			

			//  $html = 'Dear '.$candidatfirstname.', <br><br> 
			// Congratulations you are selected by Pradan. 
			// <br><br> Username - '.$to_email.'
			// <br>Password - '.$candidatfirstname.'<br>
			// Please <a href='.$addlink.'>Click here</a>';


        $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 24 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
            if(!empty($data))
              $body = str_replace($candidateI,$candidate_replace , $data->lettercontent);

		 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients, $offerattachments);  //// Send Mail candidates With Offer Letter ////
		
		 		
			if ($sendmail == false) {
				$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
			}else{
				$UpdateArr = array(
					'username' => $to_email,
					'password'  => md5($candidatfirstname),
					'complete_inprocess' => 1,
				);

				$this->db->where('candidateid',$token);
				$this->db->update('tbl_candidate_registration', $UpdateArr);

				$InsertArr = array(
					'candidateid'  => $token,
					'flag'         => 'Send',
					'senddate'       => date('Y-m-d H:i:s'),    // Current Date and time
					'sendby'       => $this->loginData->UserID, // login user id
				);

				$this->db->insert('tbl_sendofferletter', $InsertArr);
				$updateOfferFlag = array('sendflag' => 2);
				$this->db->where('candidateid', $token);
				$this->db->update('tbl_generate_offer_letter_details', $updateOfferFlag);
				// echo $this->db->last_query(); die;
				$this->session->set_flashdata('tr_msg','Offer letter has been sent !!!!');				
			}

			redirect('Hrddashboard/index');

			$content['sentmail']      = $this->SentMail($token);
			$content['title'] = 'Hrddashboard';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}	
	/*Author : Deepak Kumar 11-07-2019
	multiple file upload*/
	public function sendappointmentlettertocandidate(){

	try{

			$candidatemobile = 0;
			$candidateid = 0;
			$candidateemail = '';
			$candidatfirstname = '';
			$candidatmiddlename = '';
			$candidatlastname = '';
			$filename = '';
			$newpassword = '';
			$officename = '';
			$date_joining = '';
			$to_email = '';
			$addlink = '';
			$hrname = '';
			$token = '';
			$category1 = '';
			$password1 = '';
			// echo "<pre>";
			// print_r($_POST);
			// die;			$hrname=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
			$token = $this->input->post('staffid');
			
			$getCandidateDetails = $this->model->get_staffDetails($token); //// candidate 

			$category = $this->model->get_Category($token); //// candidate category

			if (!empty($category) && $category == 3 ) {
		 		$category1 = 'Assistant';
		 	}else if (!empty($category) && $category == 2 ) {
		 		$category1 = 'Executive';
		 	}else if (!empty($category) && $category == 1 ) {
		 		$category1 = 'Development Apprentice';
		 	}


			/*echo "<pre>";
			print_r($getCandidateDetails); die;*/
			$date_joining=$this->gmodel->changedatedbformate($getCandidateDetails->joiningdate);
			$officename=$getCandidateDetails->officename;
			// $stream=$getCandidateDetails->stream;
			$candidatemobile = $getCandidateDetails->contact;
 			$candidateid = $getCandidateDetails->candidateid;
		    $to_email = $getCandidateDetails->emailid;  //// Email Id ////
			$candidatfirstname   = $getCandidateDetails->name;  //// candidate Name ////
			$password1 = explode(' ', $candidatfirstname, 2);
			$password1 = $password1[0];
			$filename       = $getCandidateDetails->filename;  //// file Name ////
			$newpassword    = md5($password1);
			$addlink = site_url('login');

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			$appointment_others_document = $this->input->post('appointment_others_document'); 

			$hrdemail       = $this->loginData->EmailID; /////// hrd email id/////

			$filename1 = $filename.'.pdf';

			$offerattachments = array($filename1);

			$recipients = array(
				$this->loginData->EmailID => 'hr',
			 );


				
			foreach($_FILES['appointment_others_document']['name'] as $k=>$v){
				@  $ext = end((explode(".", $_FILES['appointment_others_document']['name'][$k])));
				$matricoriginalName = $_FILES['appointment_others_document']['name'][$k]; 
				$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
				$tempFile = $_FILES['appointment_others_document']['tmp_name'][$k];
				$targetPath = FCPATH . "pdf_offerletters/";
				$targetFile = $targetPath . $matricencryptedName;
				$uploadResult = move_uploaded_file($tempFile,$targetFile);
				array_push($offerattachments, $matricencryptedName);
				$dataset  = array(
					'staffid'=> $token,
					'candidateid'=> $getCandidateDetails->candidateid,
					'originaldocumentname'=> $matricoriginalName,
					'encrypteddocumentname'=> $matricencryptedName,
					'createdon'=> date('Y-m-d H:i:s'),
					'createdby'=> $this->loginData->UserID,
				);
				// $this->db->insert('tbl_offers_othersdocument', $dataset);
			}

			// $to_name = $getCandidateDetails->name;
			$to_email = $getCandidateDetails->emailid;
			/*echo "<pre>";
			print_r($offerattachments);
			exit();*/
			$candidate = array('$candidatfirstname','$date_joining','$category','$officename','$to_email','$addlink','$hrname');
			$candidate_replace = array($candidatfirstname,$date_joining,$category1,$officename,$to_email,$addlink,$hrname);
			

			// $message='';
			$subject = "sharing the offer letter and other documents";
			// $appointmentletterfilename = "mailtext/appointmentletter_to_candidate.txt";
			// $fd = fopen("mailtext/appointmentletter_to_candidate.txt","r");	
			// $message .=fread($fd,filesize($appointmentletterfilename));
			// eval ("\$message = \"$message\";");
			// $message =nl2br($message);
						
			 
      		$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 56 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
      		if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);	 
			//die;


			//  $html = 'Dear '.$candidatfirstname.', <br><br> 
			// Congratulations you are selected by Pradan. 
			// <br><br> Username - '.$to_email.'
			// <br>Password - '.$candidatfirstname.'<br>
			// Please <a href='.$addlink.'>Click here</a>';


		 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email, $to_name = null, $recipients, $offerattachments);  //// Send Mail candidates With Offer Letter ////
		 		
			if ($sendmail == false) {
				$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
			}else{

				 /*$UpdateArr = array(
				 	'username' => $to_email,
				 	'password'  => md5($candidatfirstname),
				 	'complete_inprocess' => 1,
				  );

				 $this->db->where('candidateid',$token);
                 $this->db->update('tbl_candidate_registration', $UpdateArr);*/

				 $UpdateArr = array(
		            'sendflag'         => 2,
		            );
				 $this->db->where('staffid', $token);
                 $this->db->update('tbl_offer_of_appointment', $UpdateArr);
                // echo $this->db->last_query(); die;
				 $this->session->set_flashdata('tr_msg','Offer letter has been sent !!!!');				
			}

			redirect('Hrddashboard/index');

			// $content['sentmail']      = $this->SentMail($token);
			$content['title'] = 'Hrddashboard';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
				print_r($e->getMessage());die;
		}
	}


 public function getPassword()
    {

    try{
    
    // Generating Password
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
    $password = substr( str_shuffle( $chars ), 0, 8 );

    return $password;


        }catch (Exception $e) {
          print_r($e->getMessage());die;
    }

  }

 public function SentMail($token){


    try{
    	
    	$this->db->select('*');
		$this->db->where('candidateid', $token);
		$query = $this->db->get('tbl_sendofferletter');
		$num = $query->num_rows();
   		return $num;

        }catch (Exception $e) {
          print_r($e->getMessage());die;
    }


 }




}