<?php 

/**
* State List
*/
class Leave_credit_rule extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $query = "select id, case when from_month = '01' then 'Apr' when from_month = '02' then 'May' when from_month = '03' then 'Jun' when from_month = '04' then 'Jul' when from_month = '05' then 'Aug' when from_month = '06' then 'Sep' when from_month = '07' then 'Oct' when from_month = '08' then 'Nov' when from_month = '09' then 'Dec' when from_month = '10' then 'Jan' when from_month = '11' then 'Feb' when from_month = '12' then 'Mar' end as from_month, case when to_month = '01' then 'Apr' when to_month = '02' then 'May' when to_month = '03' then 'Jun' when to_month = '04' then 'Jul' when to_month = '05' then 'Aug' when to_month = '06' then 'Sep' when to_month = '07' then 'Oct' when to_month = '08' then 'Nov' when to_month = '09' then 'Dec' when to_month = '10' then 'Jan' when to_month = '11' then 'Feb' when to_month = '12' then 'Mar' end as to_month
   , credit_value, CYear, status, isdeleted from msleavecrperiod where isdeleted=0 ORDER BY id DESC";
   $content['Leave_credit'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

   $content['title'] = 'Leave_credit_rule';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
 }
 public function Add()
 {

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 
  

  $RequestMethod = $this->input->server('REQUEST_METHOD'); 


  if($RequestMethod == 'POST'){
   $this->form_validation->set_rules('credit','Credit','trim|required|min_length[1]|max_length[50]');

   if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }

 // $this->load->model('Leave_credit_rule_model');
 $frommonth  = $this->input->post('datefrom');
 $tomonth  = $this->input->post('dateto');

  $Cyear = $this->input->post('Cyear');
//die;

 

 if (!empty($Cyear)) {
 // echo $this->input->post('Cyear');
   $dyear = substr($this->input->post('Cyear'), 0,4);
  
 }else{
  $dyear = date('Y');
 }

  // if ($frommonth >=1 && $frommonth <=9) {

  //   $fdate = substr($this->input->post('Cyear'), 0,4).'-'.$frommonth+3.'-'.'01';
  // }

  //  if ($tomonth >=1 && $tomonth <=9) {

  //   $tdate = substr($this->input->post('Cyear'), 0,4).'-'.$tomonth+3.'-'.'30';

  // }else{

  //   $tdate = substr($this->input->post('Cyear'), 5,4).'-'.$tomonth-9.'-'.'30';
  // }



 if (!empty($tomonth)) {

   $todatemonth = $tomonth-3;
 }else{
  $todatemonth = date('m');

}


if (!empty($frommonth)) {
 $fromdatemonth = $frommonth+3;
}else{
  $fromdatemonth = date('m');
}


if ($fromdatemonth >=1 && $fromdatemonth <=9) {
  $fdate = $dyear.'-'.$fromdatemonth.'-'.'01';
}else{
  //  $dyear = substr($this->input->post('Cyear'), 0,4);
  // $day=cal_days_in_month(CAL_GREGORIAN,$tomonth,$dyear);
  $fdate = $dyear.'-'.$fromdatemonth.'-'.'01';
}

//echo $fdate; die;
  //echo $todatemonth; die;

if ($tomonth >=1 && $tomonth <=9) {
  $dyear = substr($this->input->post('Cyear'), 0,4);
  $day=cal_days_in_month(CAL_GREGORIAN,$tomonth,$dyear);
  $tdate = $dyear.'-'.$tomonth.'-'.$day;
}else{

   $todyear1 = substr($this->input->post('Cyear'), 5,4); 
   $todatemonth = $tomonth - 9;
   $day=cal_days_in_month(CAL_GREGORIAN,$todatemonth,$todyear1);
   $tdate = $todyear1.'-'.$todatemonth.'-'.$day;
}
//  echo $todatemonth;
// echo $fdate;
// echo $tdate; 
//  die;



$insertArr = array(
  'CYear'         => $this->input->post('Cyear'),
  'from_month'    =>$this->input->post('datefrom'),
  'to_month'      =>$this->input->post('dateto'),
  'credit_value'  => $this->input->post('credit'),
  'status'        => $this->input->post('status'),
  'createdon'     => date('Y-m-d H:i:s'),
  'createdby'     => $this->loginData->UserID,
  'monthfroms'    => $fdate,
  'monthtos'      => $tdate,
);



$query = "SELECT * from msleavecrperiod where CYear = ".$this->input->post('Cyear')." ";

$check = $this->db->query($query)->result();

if (count($check)==0)
{

  $this->db->insert('msleavecrperiod', $insertArr);
   // echo $this->db->last_query(); die;
  $this->session->set_flashdata('tr_msg', 'Successfully added Leave Credit Period');
  redirect('/Leave_credit_rule/index/');
}
else
{
  $this->session->set_flashdata('tr_msg', 'Record alfready available for selected financial year');
  redirect('/Leave_credit_rule/index/');
}
}

prepareview:


$content['subview'] = 'Leave_credit_rule/add';
$this->load->view('_main_layout', $content);
}

public function edit($token=NULL)
{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

  $this->form_validation->set_rules('credit','Credit','trim|required|min_length[1]|max_length[50]');
  if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

  $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

  $hasValidationErrors = true;
  goto prepareview;

    }

    //$this->load->model('Leave_credit_rule_model');
    // $vt=$this->Leave_credit_rule_model->changedate($this->input->post('datefrom'));
    // $todat=$this->Leave_credit_rule_model->changedate($this->input->post('dateto'));
    $updateArr = array(
     'from_month'  =>$this->input->post('datefrom'),
     'to_month' =>$this->input->post('dateto'),
     'credit_value' => $this->input->post('credit'),
     'status' => $this->input->post('status'),
     'updatedon'  => date('Y-m-d H:i:s'),
     'updatedby'  => $this->loginData->UserID,
   );

    $this->db->where('id', $token);
    $this->db->update('msleavecrperiod', $updateArr);
    $this->db->last_query(); 

    $this->session->set_flashdata('tr_msg', 'Successfully Updated Leave Credit Period');
    redirect('/Leave_credit_rule/');
  }

  prepareview:

  $content['title'] = 'Leave_credit_rule';
  $Leave_credit = $this->Common_Model->get_data('msleavecrperiod', '*', 'id',$token);
  $content['Leave_credit'] = $Leave_credit;


  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
}


function delete($token = null)
{
      //print_r($token);die();
 $updateArr = array(
  'Isdeleted'    => 1,
);
 $this->db->where('id', $token);
 $this->db->update('msleavecrperiod', $updateArr);
 $this->session->set_flashdata('tr_msg', 'Leave Credit Deleted Successfully');
 redirect('/Leave_credit_rule/');
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);
}




}