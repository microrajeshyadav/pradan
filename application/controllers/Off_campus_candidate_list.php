<?php 
class Off_campus_candidate_list extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		
		try{
		

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

				$this->db->trans_start();
				if ($this->input->post('btnaccept')=='Submit') {
					$candidateid = $this->input->post('accept');

					$countaccept = count($this->input->post('accept'));
					if ($countaccept > 0)
					{
					for ($i=0; $i < $countaccept ; $i++) {

						$candidateid = $this->input->post('accept')[$i]; 

						if ($candidateid !='') {
							$checkaccept = "accepted";
						}else{
							$checkaccept = 'Null';
						}

						$updateAcceptReject	 = array(

							'frstatus'      => $checkaccept,
							'frdate'        => date('Y-m-d H:i:s'),
							'frcreatedby'	=> $this->loginData->UserID,
							'inprocess'     => 'closed',
						);
						$this->db->where('candidateid',$candidateid);
						$this->db->update('tbl_candidate_registration', $updateAcceptReject);

					}
					

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error Candidate not accept ');	
						redirect(current_url());	
					}else{
			 
			 		/*$this->session->set_flashdata('tr_msg', 'Successfully Candidate accept');
			 		redirect('Off_campus_candidate_list');*/
			 }
			}
			 $countreject = count($this->input->post('reject'));

			 if ($countreject > 0)
				for ($i=0; $i < $countreject ; $i++) { 

					$Candidateid = $this->input->post('reject')[$i];

					if ($Candidateid !='') {
						$checkaccept = 'rejected';
					}else{
						$checkaccept = 0;
					}

					$updateAcceptReject	 = array(
						'frstatus'     => $checkaccept,
						'frdate'       => date('Y-m-d H:i:s'),
						'frcreatedby'  => $this->loginData->UserID,
						'inprocess'     => 'closed',
					);
					$this->db->where('candidateid',$Candidateid);
					$this->db->update('tbl_candidate_registration', $updateAcceptReject);
				}

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error Candidate not reject');	
					redirect(current_url());	
				}else{
				// 	$countreject = count($this->input->post('reject'));
				// 	for ($i=0; $i < $countreject ; $i++) { 

				// 	$Candidateid = $this->input->post('reject')[$i];

				// 	$subject = "From PRADAN"; 
				// 	$candidatedetails = $this->model->getCandidateDetailsPreview($Candidateid);
				// 	$candidate = array('$candidatefirstname','$candidatemiddlename','$candidatelastname');
				// 	$candidate_replace = array($candidatedetails->candidatefirstname,$candidatedetails->candidatemiddlename,$candidatedetails->candidatelastname);
		 	// 		//print_r($candidatedetails); die;
				// 	// $fd = fopen("mailtext/HRDRejection_Off_campus.txt", "r"); 
				// 	// $message .=fread($fd,4096);
				// 	// eval ("\$message = \"$message\";");
				// 	// $message =nl2br($message);
				// 	// $body = $message;

				// 	$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 9 AND `isactive` = '1' AND id = 7";
   	// 	    		$data = $this->db->query($sql)->row();
   	// 	    		if(!empty($data))
   	// 	    		{
   	// 	    	 	$body = str_replace($candidate,$candidate_replace , $data->lettercontent);

				// 	$candidateemail  = $candidatedetails->emailid;
				// 	$hrdemailid = $this->model->getHrdemailid();
			 // 	$to_email    = $candidateemail; //// Candidate Mail Id ////
			 // 	$to_name     = $hrdemailid; /// HRD Mail Id ////
			 // 	$sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);
			 // }
			 // }

			}
		}
			$this->session->set_flashdata('tr_msg', 'Data submitted successfully.');
			 	redirect('Off_campus_candidate_list/');

		}
	
		$content['selectedcandidatedetails']      = $this->model->getSelectedCandidate();


		$content['title'] = 'Off_campus_candidate_list';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);



	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


public function view($token=NULL)
{


	if (empty($token) || $token == '' ) {
		$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
		redirect('/Off_campus_candidate_list/index');

	} else { 	
		try{


			$content['candidatedetails']   = $this->model->getCandidateDetailsPreview($token);

			$TEcount = $this->model->getCountTrainingExposure($token);

			$content['TrainingExpcount']= $TEcount;  

			$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($token);


			@ $Lcount = $this->model->getCountLanguage($token);

			@ $content['languageproficiency']= $Lcount;  

			@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($token);
			@  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($token);


			$WEcount = $this->model->getCountWorkExprience($token);

			$content['WorkExperience']= $WEcount;  

			$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($token);


			$GPYcount = $this->model->getCountGapYear($token);

			$content['GapYearCount']= $GPYcount;  

			$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($token);

			$content['statedetails']           = $this->model->getState();
			$content['ugeducationdetails']     = $this->model->getUgEducation();
			$content['pgeducationdetails']     = $this->model->getPgEducation();
			$content['campusdetails']          = $this->model->getCampus();
			$content['syslanguage']            = $this->model->getSysLanguage();
			$content['sysrelations']           = $this->model->getSysRelations();
			$content['sysidentity']            = $this->model->getSysIdentity();

			$content['title'] = 'Candidate_registration';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
}
public function Offercomments($token){


	try{

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

			$this->form_validation->set_rules('recruiters','Recruiters','trim|required');
			$this->form_validation->set_rules('comments','Comments ','trim|required');

			if($this->form_validation->run() == FALSE){
				$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
					'</div>');

				$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors    =    true;
				goto prepareview;

			}


			$this->db->trans_start();

			$insertArr2 = array(
				'candidateid'     => $token,
				'recutersid'      => $this->input->post("recruiters"),
				'comments'  	  => $this->input->post('comments'),
				'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
			    'createdby'       => $this->loginData->UserID, // login user id
			    
			);
			$this->db->insert('edcomments', $insertArr2);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error adding Comments');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully added Comments');			
			}
			redirect('/EDashboard/Offercomments');

		}

		prepareview:

		$content['recruiters_list']               = $this->model->getRecruitersList();
		$content['title'] = 'Offercomments';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


public function sendofferlettertocandidate($token){

	if (empty($token) || $token == '' ) {
		$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
		redirect('/Campusinchargeintimation/index');

	} else { 

		try{

			//echo $token; die; 

			 $getCandidateDetails = $this->model->getCandidateDetails($token); //// candidate Details
			 	// echo "<pre>";
			 	// print_r($getCandidateDetails); die;

		     $candidateemail = $getCandidateDetails[0]->emailid;  //// Email Id ////
			 $candidatname   = $getCandidateDetails[0]->candidatefirstname;  //// candidate Name ////
			 $filename       = $getCandidateDetails[0]->filename;  //// file Name ////
			 $newpassword    = $this->getPassword();

			 
			// echo $newpassword; die;
			 
			 $attachments = array($filename.'.pdf');

			 $html = 'Dear '.$candidatname.', <br><br> 

			 Congratulations you are selected by Pradan. <br><br> Username - '.$candidatname.'
			 <br>Password - '.$newpassword.'';


			 $sendmail = $this->Common_Model->send_email($subject = 'Job Offer letter ', $message = $html, $candidateemail, $candidateemail, $attachments);  //// Send Mail candidates With Offer Letter ////
			 //echo $sendmail; die;

			 if ($sendmail == false) {
			 	$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
			 }else{

			 	$UpdateArr = array(
			 		'username' => $candidatname,
			 		'password'  => $newpassword,
			 	);

			 	$this->db->where('candidateid',$token);
			 	$this->db->update('tbl_candidate_registration', $UpdateArr);

			 	$InsertArr = array(
			 		'candidateid'  => $token,
			 		'flag'         => 'Send',
			            'senddate'       => date('Y-m-d H:i:s'),    // Current Date and time
			    		'sendby'       => $this->loginData->UserID, // login user id
			    	);

			 	$this->db->insert('tbl_sendofferletter', $InsertArr);
			 	$this->session->set_flashdata('tr_msg','Successfully Send Offer Letter !!!!');				
			 }

			 redirect('Hrddashboard/index');

			 $content['sentmail']      = $this->SentMail($token);
			 $content['title'] = 'Hrddashboard';
			 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 $this->load->view('_main_layout', $content);


			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
		}
	}


	public function getPassword()
	{

		try{

    // Generating Password
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
			$password = substr( str_shuffle( $chars ), 0, 8 );

			return $password;


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	public function SentMail($token){


		try{

			$this->db->select('*');
			$this->db->where('candidateid', $token);
			$query = $this->db->get('tbl_sendofferletter');
			$num = $query->num_rows();
			return $num;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}


	}




}