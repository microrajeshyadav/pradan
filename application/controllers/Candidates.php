<?php 
class Candidates extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
		
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{

				 // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 
    
		$this->load->model('Candidates_model');
		
		if($this->input->server('REQUEST_METHOD') == "POST")
		{
			 $month     = $this->input->post('month');
			 $exp       = explode('/',$month);
			 $MonthYear = $exp[1].'-'.$exp[0];
			// $this->session->set_userdata("month",$this->input->post('month'));
		}

		
		$query = "SELECT * FROM `mstcampus`";
		$content['campuslist'] = $this->Common_Model->query_data($query);

		$content['title'] = 'Campus';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}

	
	public function add(){

		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
					$this->db->trans_start();

					$insertArr2 = array(
						'campusincharge'    => $this->input->post('campusincharge'),
						'campusname'       	=> $this->input->post("campusname"),
						'emailid'  			=> $this->input->post('emailid'),
						'city' 				=> $this->input->post('City'),
						'CreatedOn'      	=> date('Y-m-d H:i:s'),
					    'CreatedBy'      	=> 2, // login user id
					    'IsDeleted'      	=> 0, 
					  );
					$this->Common_Model->insert_data('mstcampus', $insertArr2);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Campus');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully added Campus');			
					}
					redirect('/Campus/index');
			
			}


			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}


	public function edit($token){
		
		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
			
					$this->db->trans_start();

					$updateArray = array(
						'campusincharge'    => $this->input->post('campusincharge'),
						'campusname'       	=> $this->input->post("campusname"),
						'emailid'  			=> $this->input->post('emailid'),
						'city' 				=> $this->input->post('City'),
						'CreatedOn'      	=> date('Y-m-d H:i:s'),
					    'CreatedBy'      	=> 2, // login user id
					    'IsDeleted'      	=> 0, 
					  );
				//print_r($updateArray); die;

					$this->db->where("campusid",$token);
			        $this->db->update('mstcampus', $updateArray);

					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error update Campus');	
					}else{
						$this->session->set_flashdata('tr_msg', 'Successfully update Campus');			
					}
					redirect('/Campus/index');
			
			}

			$query = "SELECT * FROM `mstcampus` WHERE campusid=".$token."";
			$content['campusupdate'] = $this->Common_Model->query_data($query);

			$content['title'] = 'edit.php';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}



	function delete($token = null)
	{
		try{
		$this->Common_Model->delete_row('mstcampus','campusid', $token); 
		$this->session->set_flashdata('tr_msg' ,"Campus Deleted Successfully");
		redirect('/Campus/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


}