<?php 
class Campusinchargeintimation extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model('Campusinchargeintimation_model', 'model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
		
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index($token=null)
	{
		try{
		
		 // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

  // end permission 
	
	$content['campusinchargedetails'] = $this->model->getCampusInchargeDetail($token);
	//print_r($content['campusinchargedetails']); die;
		$content['title'] = 'index';
		 //echo __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__; die;
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function add(){

		try{
			// start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 
			
		 	 $save_intemation = $this->input->post('intemationssave'); 
		 	 $send_intemations = $this->input->post('intemationssend'); 

      		$this->load->model('Campusinchargeintimation_model');
		
		    $RequestMethod = $this->input->server('REQUEST_METHOD');

		    $campusid = $this->input->post('campusid'); 
			
			if($RequestMethod == 'POST'){

			if (isset($save_intemation) && $save_intemation=='save') {

				$this->db->trans_start();
				 $campusid = $this->input->post('campusid'); 
				 $categoryid = $this->input->post('categoryid'); 

				 //echo $campusid;

				$goingonCampusExist = $this->is_campus_going_on($campusid,$categoryid);

				// print_r($goingonCampusExist); die;

			    if($goingonCampusExist >= 1){
		    		$this->session->set_flashdata('er_msg', 'Camp allready running, please closed running camp !!! ');
		    		redirect(current_url());
		    	}


				$schedule_fromdate = $this->input->post('schedule_fromdate');
				//$schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);

				$explodefromdate = explode('-', $schedule_fromdate);
				$schedule_fromdate_db = $explodefromdate[2].'-'.$explodefromdate[1].'-'.$explodefromdate[0];

				$schedule_todate = $this->input->post('schedule_todate');

				$explodetodate = explode('-', $schedule_todate);
				$schedule_todate_db = $explodetodate[2].'-'.$explodetodate[1].'-'.$explodetodate[0];

				//echo $schedule_todate_db = $this->model->changedatedbformate($schedule_todate);
				//die;

					$insertArr1 = array(
						'id'                => $this->input->post('campusintimationid'),
						'campusid'          => $this->input->post('campusid'),
						'categoryid'        => $this->input->post('categoryid'),
						'messsage'       	=> $this->input->post("message"),
						'fromdate'      	=> $schedule_fromdate_db,
						'todate'      	    => $schedule_todate_db,
						'createdon'      	=> date('Y-m-d H:i:s'),
					    'createdby'      	=> $this->loginData->UserID, // login user id
					    'isdeleted'      	=> 0, 
					    'mailstatus' 		=> 0, 
					  );
					/*echo "<pre>";
					print_r($insertArr1);exit();*/
					$this->Common_Model->insert_data('tbl_campus_intimation', $insertArr1);
                    $insertid = $this->db->insert_id();


					$this->db->trans_complete();

					if ($this->db->trans_status() === true){
						$this->session->set_flashdata('tr_msg', 'Successfully Add Campus Incharge intemation !!!');
					}else{
						$this->session->set_flashdata('er_msg', 'Error adding ampus Incharge intemation !!!');
					}

					redirect('/Campusinchargeintimation/SendMail/'.$insertid);
					//redirect('/Campusinchargeintimation/index');
			}
			
			if (isset($send_intemations) && $send_intemations=='send') {

				$this->db->trans_start();
				 $campusid = $this->input->post('campusid'); 
				 $categoryid = $this->input->post('categoryid'); 

				$goingonCampusExist = $this->is_campus_going_on($campusid,$categoryid);
				//echo $goingonCampusExist;exit();

			    if($goingonCampusExist >= 1){
		    		$this->session->set_flashdata('er_msg', 'Camp allready running, please closed running camp !!! ');
		    		redirect(current_url());
		    	}

				//print_r($this->input->post()); die;

				// $schedule_fromdate = $this->input->post('schedule_fromdate');
				// $schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);
				// $schedule_todate = $this->input->post('schedule_todate');
				// $schedule_todate_db = $this->model->changedatedbformate($schedule_todate);
				$schedule_fromdate = $this->input->post('schedule_fromdate');
				$explodefromdate = explode('-', $schedule_fromdate);
				$schedule_fromdate_db = $explodefromdate[2].'-'.$explodefromdate[1].'-'.$explodefromdate[0];

				$schedule_todate = $this->input->post('schedule_todate');

				$explodetodate = explode('-', $schedule_todate);
				$schedule_todate_db = $explodetodate[2].'-'.$explodetodate[1].'-'.$explodetodate[0];

			    $campusid = $this->input->post('campusid');

				$getinchargedetails = $this->model->getInchargeDetail($campusid);
	     		 $expinchargename = explode(' ', $getinchargedetails->campusincharge);
		      	$UserFirstName = $expinchargename[0];
		      	$UserMiddleName = $expinchargename[1];
		      	$UserLastName	= $expinchargename[2];
		      
		        $EmailID     = $getinchargedetails->emailid;
		        $PhoneNumber = $getinchargedetails->telephone;

				$updateArr1 = array(
				'id'                => $this->input->post('campusintimationid'),
				'campusid'          => $this->input->post('campusid'),
				'categoryid'		=> $this->input->post('categoryid'),
				'messsage'       	=> $this->input->post("message"),
				'fromdate'      	=> $schedule_fromdate_db,
				'todate'      	    => $schedule_todate_db,
				'createdon'      	=> date('Y-m-d H:i:s'),
				'createdby'      	=> $this->loginData->UserID, // login user id
				'isdeleted'      	=> 0, 
				'mailstatus' 		=> 0, 
				);

				$this->db->insert('tbl_campus_intimation', $updateArr1);
				$insertid = $this->db->insert_id();

                  
			$this->db->trans_complete();

			if ($this->db->trans_status() === true){

		    $campusid = $this->input->post('campusid'); 
            $getcampusDetail = $this->model->getCampusEmailid($campusid);
			$campusEmail   = $getcampusDetail[0]->emailid;
			$hrdemailid    = $this->loginData->EmailID;
			$campusmessage = $this->input->post("message");
			$subject  = "Campus Incharge Intimation";
			$body     = $campusmessage;
	    	$staffname    		= $this->loginData->UserFirstName; ///change name
			$designation  		= 'HRD';
			$to_email = $campusEmail;
			$sendmail = $this->Common_Model->send_email($subject, $campusmessage, $to_email,$hrdemailid);
			
	    	 if($sendmail==true){
	    	 $updatestatusArr = array(
				    'mailstatus'   => 1,
			    );

			$this->db->where('id',$insertid);
			$this->db->update('tbl_campus_intimation', $updatestatusArr);

			$this->session->set_flashdata('tr_msg', 'Successfully Send Mail Campus Incharge'); 

	    	}else{

	    	   	$this->session->set_flashdata('tr_msg', 'Mail Not Send please try again'); 

	    	   }
				
			}else{
				$this->session->set_flashdata('er_msg', 'Error adding ampus Incharge intemation !!!');
			}
			redirect('/Campusinchargeintimation/index/');

	}
			
			}

			$content['campusdetails'] = $this->model->getCampus();
			$content['categorydetails'] = $this->model->getCategory();

			$content['title'] = 'add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}



	public function send($token){
		
		try{
			 // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 
		
			$send_intemations = $this->input->post('intemationssend'); 

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){


				if (isset($save_intemation) && $save_intemation=='save') {

					$schedule_fromdate = $this->input->post('schedule_fromdate');
				 $schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);


				$schedule_todate = $this->input->post('schedule_todate');
				 $schedule_todate_db = $this->model->changedatedbformate($schedule_todate);



				$this->db->trans_start();

					$updateArr1 = array(
						'id'                => $this->input->post('campusintimationid'),
						'campusid'          => $this->input->post('campusid'),
						'messsage'       	=> $this->input->post("message"),
						'categoryid'		=> $this->input->post('categoryid'),
						'fromdate'      	=> $schedule_fromdate_db,
						'todate'      	    => $schedule_todate_db,
						'createdon'      	=> date('Y-m-d H:i:s'),
					    'createdby'      	=> $this->loginData->UserID, // login user id
					    'isdeleted'      	=> 0, 
					    'mailstatus' 		=> 0, 
					  );

					$this->db->where('id',$token);
					$this->db->update('tbl_campus_intimation', $updateArr1);
                    $insertid = $this->db->insert_id();

					$this->db->trans_complete();

					if ($this->db->trans_status() === true){
						$this->session->set_flashdata('tr_msg', 'Successfully Add Campus Incharge intimation !!!');
					}else{
						$this->session->set_flashdata('er_msg', 'Error adding ampus Incharge intimation !!!');
					}
			}
			
					

			if (isset($send_intemations) && $send_intemations=='send') {

				$this->db->trans_start();
			    $schedule_fromdate = $this->input->post('schedule_fromdate');
				$schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);
				$schedule_todate = $this->input->post('schedule_todate');
				$schedule_todate_db = $this->model->changedatedbformate($schedule_todate);

				 $campusid = $this->input->post('campusid');

					$getinchargedetails = $this->model->getInchargeDetail($campusid);
		     		 $expinchargename = explode(' ', $getinchargedetails->campusincharge);
		     // print_r($expinchargename);
		      if (count($expinchargename) > 2) {
			      	$UserFirstName = $expinchargename[0];
			      	$UserMiddleName = $expinchargename[1];
			      	$UserLastName	= $expinchargename[2];
		      }else{
		      		$UserFirstName = $expinchargename[0];
			       	$UserLastName	= $expinchargename[1];
		      }

		      $EmailID     = $getinchargedetails->emailid;
		      $PhoneNumber = $getinchargedetails->telephone;

			$updateArr1 = array(
				'campusid'          => $this->input->post('campusid'),
				'messsage'       	=> $this->input->post("message"),
				'categoryid'		=> $this->input->post('categoryid'),
				'fromdate'      	=> $schedule_fromdate_db,
				'todate'      	    => $schedule_todate_db,
				'createdon'      	=> date('Y-m-d H:i:s'),
			    'createdby'      	=> $this->loginData->UserID, // login user id
			    'isdeleted'      	=> 0, 
			    'mailstatus' 		=> 0, 
			  );

			$this->db->where('id',$token);
			$this->db->update('tbl_campus_intimation', $updateArr1);
            $insertid = $this->db->insert_id();
       
		    $this->db->trans_complete();

					if ($this->db->trans_status() === true){

					 $campusid = $this->input->post('campusid'); 
					 $cat_id = 1;
				     $completeurl = $campusid.'/'.$cat_id; 

					 $getcampusDetail = $this->model->getCampusEmailid($campusid);
					 $campusEmail   = $getcampusDetail[0]->emailid;
					 $hrdemailid    = $this->loginData->EmailID;
					 $body = $this->input->post("message");
					 $campusencodeid  = base64_encode($completeurl);
					  
					 // $addlink = site_url().'CandidatesInfo/add/'.$campusencodeid;
					 // $link = '<a href='.$addlink.'>Click here</a>' ;
					 // $body = $campusmessage.$link;

					$subject      = 'Campus Incharge Intimation';
			    	 $sendmail = $this->Common_Model->send_email($subject, $message = $body, $campusEmail,$hrdemailid);  //// Send Mail ////

			    	 if($sendmail==true){
			    	 $updateArr1 = array(
						    'mailstatus'   => 1,
					    );

					   $this->db->where('id',$insertid);
					   $this->db->update('tbl_campus_intimation', $updateArr1);
					   $this->session->set_flashdata('tr_msg', 'Successfully Send Mail Campus Incharge'); 

			    	   }else{

			    	   	$this->session->set_flashdata('tr_msg', 'Mail Not Send please try again'); 

			    	   }
						
					}else{
						$this->session->set_flashdata('er_msg', 'Error adding ampus Incharge intemation !!!');
					}

			}

 				redirect('/Campusinchargeintimation/index');
			
			}
			
			$content['getcampusinchargeintemationdetail'] = $this->model->getCampusInchargeIntemationDetail($token);
			$content['campusdetails'] 			= $this->model->getCampus();

			$content['title'] = 'send';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
					print_r($e->getMessage());die;
			}

	}

	public function SendMail($token){

		try{

			// start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  // end permission 

		    $RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){


				 $campusid = $this->input->post('campusid');

				// $schedule_fromdate = $this->input->post('schedule_fromdate');
				// $schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);


				// $schedule_todate = $this->input->post('schedule_todate');
				// $schedule_todate_db = $this->model->changedatedbformate($schedule_todate);

				$campusid = $this->input->post('campusid');
				$schedule_fromdate = $this->input->post('schedule_fromdate');
				//$schedule_fromdate_db = $this->model->changedatedbformate($schedule_fromdate);

				$explodefromdate = explode('-', $schedule_fromdate);
				$schedule_fromdate_db = $explodefromdate[2].'-'.$explodefromdate[1].'-'.$explodefromdate[0];

				$schedule_todate = $this->input->post('schedule_todate');

				$explodetodate = explode('-', $schedule_todate);
				$schedule_todate_db = $explodetodate[2].'-'.$explodetodate[1].'-'.$explodetodate[0];

					$getinchargedetails = $this->model->getInchargeDetail($campusid);
		     		 $expinchargename = explode(' ', $getinchargedetails->campusincharge);
				     // print_r($expinchargename);
				      if (count($expinchargename) > 2) {
					      	$UserFirstName = $expinchargename[0];
					      	$UserMiddleName = $expinchargename[1];
					      	$UserLastName	= $expinchargename[2];
				      }else{
				      		$UserFirstName = $expinchargename[0];
					       	$UserLastName	= $expinchargename[1];
				      }

				      $EmailID     = $getinchargedetails->emailid;
				      $PhoneNumber = $getinchargedetails->telephone;

				    $campusinchargedetails = $this->model->getCampusInchargeDetail($token);

				   // print_r($campusinchargedetails); die;

					$this->db->trans_start();

					$insertArr1 = array(
						'campusid'          => $this->input->post('campusid'),
						'messsage'       	=> $this->input->post("message"),
						/*'categoryid'		=> $this->input->post('categoryid'),*/
						'fromdate'      	=> $schedule_fromdate_db,
						'todate'      	    => $schedule_todate_db,					
						'updatedon'      	=> date('Y-m-d H:i:s'),
					    'updatedby'      	=> $this->loginData->UserID, // login user id
					    'isdeleted'      	=> 0, 
					    'mailstatus' 		=> 0, 
					  );

					$this->db->where('id', $token);
					$this->db->update('tbl_campus_intimation', $insertArr1);

					
					$this->db->trans_complete();

					if ($this->db->trans_status() === true){
						
				    /////////  Send E Mail ////////////////////

					$getcampusDetail = $this->model->getCampusEmailid($campusid);

		           // print_r($getcampusDetail);

					$campusEmail   = $getcampusDetail[0]->emailid;

					$subject = "Campus Incharge Intimation";
					$to_email = $campusEmail;
					//$to_name = $hrdemailid;

     				$message      = "";
					$name         = $UserFirstName;
					$fromdate     = $schedule_fromdate;
					$todate 	  = $schedule_todate;
					$campuslink   = $campmess;
					$staffname    = $this->loginData->UserFirstName; ///change name
					$designation  = 'HRD';
					$body         = $this->input->post("message");
					//echo $body; die;
					
					
					$sendmail = $this->Common_Model->send_email($subject, $body, $to_email);
					//echo $sendmail; die;

			    	 if($sendmail==true){
			    	 
			    	 $updateArr1 = array(
						    'mailstatus'   => 1,
					    );

					$this->db->where('id',$token);
					$this->db->update('tbl_campus_intimation', $updateArr1);

					$this->session->set_flashdata('tr_msg', 'Successfully Send Mail Campus Incharge'); 

		    	   }else{

		    	   	$this->session->set_flashdata('tr_msg', 'Mail Not Send please try again'); 
		    	   }


					}else{
						$this->session->set_flashdata('er_msg', 'Error adding ampus Incharge intemation !!!');
					}

			  redirect('/Campusinchargeintimation/index');
			 }

			$content['campusinchargedet'] = $this->model->getCampusInchargeDetail($token);
			$content['campusdetails'] = $this->model->getCampus();
			$content['categorydetails'] = $this->model->getCategory();
			$content['title'] = 'SendMail';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
					print_r($e->getMessage());die;
		}

	}


/* Check campus running or not */

public function is_campus_going_on($campusid,$categoryid)
{	
	try{

	 $sql = "SELECT count(campus_status) as count FROM tbl_campus_intimation where `campusid`= '".$campusid."' AND categoryid  = '".$categoryid."' AND campus_status = 0 "; 
	 $result = $this->db->query($sql)->row();
	return $result->count;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}



	function delete($token)
	{
		try{

// start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
  $role_permission = $this->db->query($query)->result();
  // end permission 

  //print_r($role_permission); die;

  foreach ($role_permission as $row) { 
  	if ($row->Controller == "Campusinchargeintimation" && $row->Action == "delete"){ 
		 
		$insertArr1 = array(
		    'isdeleted'      	=> 1, 
		    'campus_status'    	=> 1,
		  );

		$this->db->where('id', $token);
		$this->db->update('tbl_campus_intimation', $insertArr1);

		$this->session->set_flashdata('tr_msg' ,"Campus Intimation Deleted Successfully");
	}else{
		$this->session->set_flashdata('er_msg' ,"Sorry !!! you do not have permission to delete. please contact your administrator !!!");
	}
}

		redirect('/Campusinchargeintimation/');
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



}