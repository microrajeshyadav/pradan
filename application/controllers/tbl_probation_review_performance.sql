-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2019 at 12:20 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pradan_personnal`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_probation_review_performance`
--

CREATE TABLE `tbl_probation_review_performance` (
  `id` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `date_of_appointment` date NOT NULL,
  `period_of_review_from` date NOT NULL,
  `period_of_review_to` date NOT NULL,
  `satisfactory` varchar(20) DEFAULT NULL,
  `probation_completed` varchar(10) DEFAULT NULL,
  `probation_extension_date` date DEFAULT NULL,
  `reasons_for_not_above_recommendations` text,
  `work_habits_and_attitudes` text,
  `conduct_and_social_maturity` text,
  `any_other_observations` text,
  `integrity` varchar(20) DEFAULT NULL,
  `flag` int(1) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `latestby` date DEFAULT NULL,
  `ed_comments` text,
  `ed_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_probation_review_performance`
--
ALTER TABLE `tbl_probation_review_performance`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_probation_review_performance`
--
ALTER TABLE `tbl_probation_review_performance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
