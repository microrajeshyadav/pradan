<?php 

/**
* State List
*/
class Gratuity_nomination_form extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
  
    $this->load->model("Employee_particular_form_model");
    $this->load->model("General_nomination_and_authorisation_form_model");
    $this->load->model("Gratuity_nomination_form_model");
    $check = $this->session->userdata('login_data');
    $this->load->model("Global_model","gmodel");

    ///// Check Session //////  
   //  if (empty($check)) {
   //   redirect('login');
   // }

   $this->loginData = $this->session->userdata('login_data');
   // echo "<pre>";
   // print_r($this->loginData);exit();
 }

 public function index($staff,$candidate_id)
 {
  try{

  if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    redirect('/staff_dashboard/index');
    
  } else {
   $staff = $this->uri->segment(3);
   $candidate_id  = $this->uri->segment(4);

   $content['staff'] = $staff;
   $content['candidate_id'] = $candidate_id;
   
   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);

   $staff_id=$this->loginData->staffid;

   if($this->loginData->RoleID==3)
   {
    $candidateid=$this->loginData->candidateid;
  }
  else 
  {
   $candidateid=$candidate_id;
 }

 if(isset($staff_id)&& isset($candidateid))
 {

  $staff_id=$staff_id;
       // echo "staff id".$staff_id;
  $candidateid=$candidateid;
        //echo "candate id=".$candidateid;

}
else
{
 $staff_id=$staff;
         //echo "staff".$staff;
 $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

}

$content['candidateaddress'] = $this->Gratuity_nomination_form_model->getCandidateWithAddressDetails($staff_id);
$content['personnal_mail'] = $this->Gratuity_nomination_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
$personal_email=$content['personnal_mail']->EmailID;

$reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
$content['tc_email'] = $this->Gratuity_nomination_form_model->tc_email($reportingto);

$content['report']=$this->Gratuity_nomination_form_model->staff_reportingto($staff_id);
$content['signature'] = $this->Common_Model->staff_signature($staff_id);

$RequestMethod = $this->input->server('REQUEST_METHOD'); 

if($RequestMethod == "POST"){

// print_r($this->input->post()); die;


  $Sendsavebtn = $this->input->post('savebtn');

  if (!empty($Sendsavebtn) && $Sendsavebtn =='senddatasave') {

          //echo "image=".$updateArr['signatureplace_encrypted_filename'];
          //die;

    $dadate = $this->input->post('dadate');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



    $insertarraydata = array (

      'staff_id'     => $staff_id,
      'candidate_id' => $candidateid,
      'place'        => $this->input->post('daplace'),
      'date'         => $dadate,
      'isupdate'     => 0,
      'type'         => 'Gr',

    );
    // print_r($insertarraydata); die;
    $this->db->insert('tbl_graduitynomination', $insertarraydata);
      //echo $this->db->last_query();
      //die;
    $insertid = $this->db->insert_id();
    $data1=$this->input->post('data');

   foreach($data1 as $key => $value)
   {
    if ($value['name'] != '') {
      $arr = array (

        'sr_no'         => $key,
        'name'          => $value['name'],
        'graduity_id'   => $insertid,
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior']

      );

      $this->db->insert('tbl_graduitynomination_detail', $arr);
    }
    }



    $insert_data = array (

      'type'      => 14,
      'r_id'      => $insertid,
      'sender'    => $staff_id,
      'receiver'  => $content['report']->reportingto,
      'senddate'  => date('Y-m-d'),
      'createdon' => date('Y-m-d H:i:s'),
      'createdby' => $this->loginData->UserID,
      'flag'      => 1,
      'staffid'   =>  $staff_id
    );
    $this->db->insert('tbl_workflowdetail', $insert_data);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! gratuity nomination and authorisation form');  

    }else{

      $this->session->set_flashdata('tr_msg', 'Successfully save  gratuity nomination and authorisation form'); 

      redirect('Gratuity_nomination_form/edit/'.$staff_id.'/'.$candidateid);   
    }

  }

  $submitdatasend = $this->input->post('submitbtn');

  if (!empty($submitdatasend) && $submitdatasend =='senddatasubmit') {


   if ($_FILES['signatureplace']['name'] != NULL) {
    $data=$_FILES['signatureplace'];
    $this->load->model('Membership_applicatioin_from_model');

    $fileDatasa=$this->Membership_applicatioin_from_model->do_uploadd($data);

    $updateArr['signatureplace'] = $fileDatasa['orig_name'];
    $updateArr['signatureplace_encrypted_filename'] = $fileDatasa['file_name'];

  }


          //echo "image=".$updateArr['signatureplace_encrypted_filename'];
          //die;

  $dadate = $this->input->post('dadate');

  $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



  $insertarraydata = array (

    'staff_id'     => $staff_id,
    'candidate_id' => $candidateid,
    'place'        => $this->input->post('daplace'),
    'signature'    => $updateArr['signatureplace_encrypted_filename'],
    'date'         => $dadate,
    'isupdate'     => 1,
    'type'         => 'Gr',
  );

  $this->db->insert('tbl_graduitynomination', $insertarraydata);
      // echo $this->db->last_query();
      // die;
  $insertid = $this->db->insert_id();
  $data1=$this->input->post('data');

  foreach($data1 as $value)
  {
    if ($value['name'] != '') {

    $arr = array (

      'sr_no'         => $value['sr_no'],
      'name'          => $value['name'],
      'graduity_id'   => $insertid,
      'relation_id'   => $value['relationship_nominee'],
      'age'           => $value['age_nominee'],
      'address'       => $value['address_nominee'],
      'share_nominee' => $value['share_nominee'],
      'minior'        => $value['minior']

    );

    $this->db->insert('tbl_graduitynomination_detail', $arr);
  }
}



  $insert_data = array (
    'type'      => 14,
    'r_id'      => $insertid,
    'sender'    => $staff_id,
    'receiver'  => $content['report']->reportingto,
    'senddate'  => date('Y-m-d'),
    'createdon' => date('Y-m-d H:i:s'),
    'createdby' => $this->loginData->UserID,
    'flag'      => 1,
    'staffid'   =>$staff_id
  );
  $this->db->insert('tbl_workflowdetail', $insert_data);



  $this->db->trans_complete();

  if ($this->db->trans_status() === FALSE){

    $this->session->set_flashdata('er_msg', 'Error !!! gratuity nomination and authorisation form');  

  }else{

    $this->session->set_flashdata('tr_msg', 'Successfully save and submit gratuity nomination and authorisation form'); 


    $subject = ': Gratuity nomination form';
    $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Gratuity nomination form </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

    redirect('Gratuity_nomination_form/view/'.$staff.'/'.$candidateid); 

  }
}

}



//print_r($content['candidatedetailwithaddress']);

$content['topbar'] = $this->Employee_particular_form_model->table_flag($candidateid);
$var=$content['topbar']->graduity_flag;
//die("hello");

if ($var==null)
{
  goto preview;
   // redirect('/Gratuity_nomination_form/index/'.$staff.'/'.$candidateid);
}


if($var==0)
{
  redirect('/Gratuity_nomination_form/edit/'.$staff.'/'.$candidateid);
  
}
elseif($var==1)
{
  redirect('/Gratuity_nomination_form/view/'.$staff.'/'.$candidateid);

}





    // die("hello");
// end permission 
preview:
$content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();



$content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
     //print_r($content['candidatedetailwithaddress']);

$content['candidatedetailwithaddress'] = $this->Gratuity_nomination_form_model->getCandidateWithAddressDetails($staff_id);

$content['office_name'] = $this->General_nomination_and_authorisation_form_model->office_name();
 // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();

$query = "select * from mstpgeducation where isdeleted='0'";
$content['mstpgeducation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
$content['title'] = 'Gratuity_nomination_form';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function edit($staff,$candidate_id)
{
  try{

 if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
  
  $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
  redirect('/staff_dashboard/index');
  
} else {

  $staff = $this->uri->segment(3);
  $candidate_id  = $this->uri->segment(4);

  $content['staff'] = $staff;
  $content['candidate_id'] = $candidate_id;

  $this->session->set_userdata('staff', $staff);
  $this->session->set_userdata('candidate_id', $candidate_id);







  $staff_id=$this->loginData->staffid;
  $candidateid=$this->loginData->candidateid;

  if(isset($staff_id)&& isset($candidateid))
  {

    $staff_id=$staff_id;
       // echo "staff id".$staff_id;
    $candidateid=$candidateid;
        // echo "candate id=".$candidateid;

  }
  else
  {
   $staff_id=$staff;
         //echo "staff".$staff;
   $candidateid=$candidate_id;
        // echo "candidateid=".$candidateid;

 }

 $content['candidateaddress'] = $this->Employee_particular_form_model->getCandidateWithAddressDetails($staff_id);
 $content['personnal_mail'] = $this->Employee_particular_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
 $personal_email=$content['personnal_mail']->EmailID;

 $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
 $content['tc_email'] = $this->Employee_particular_form_model->tc_email($reportingto);
 $content['signature'] = $this->Common_Model->staff_signature($staff_id);
 
 $RequestMethod = $this->input->server('REQUEST_METHOD');

 if($RequestMethod == "POST"){
  // print_r($this->input->post()); die();

  $savesenddata = $this->input->post('savebtn');

  if (!empty($savesenddata) && $savesenddata =='senddatasave') {


           //echo "image=".$updateArr['signatureplace_encrypted_filename'];
          // die;
    $dadate = $this->input->post('dadate');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



    $updatearraydata = array(
      'staff_id'     =>$staff_id,
      'candidate_id' => $candidateid,
      'place'        => $this->input->post('daplace'),                
      'date'         => $dadate,
      'isupdate'     => 0           
    );
    // $this->db->where('candidate_id', $candidateid);
    // $this->db->update('tbl_graduitynomination', $updatearraydata);
     // echo $this->db->last_query();
      //die;
      //$countnominee = count($this->input->post('full_name_nominee'));


    $data1=$this->input->post('data');

   //      $insertid = $this->db->insert_id();
   //  $data1=$this->input->post('data');

   // foreach($data1 as $key => $value)
   // {
   //  if ($value['name'] != '') {
   //    $arr = array (

   //      'sr_no'         => $key,
   //      'name'          => $value['name'],
   //      'graduity_id'   => $insertid,
   //      'relation_id'   => $value['relationship_nominee'],
   //      'age'           => $value['age_nominee'],
   //      'address'       => $value['address_nominee'],
   //      'share_nominee' => $value['share_nominee'],
   //      'minior'        => $value['minior']

   //    );

   //    $this->db->insert('tbl_graduitynomination_detail', $arr);
   //  }
   //  }
    //print_r($data1);
    //die; 
    foreach($data1 as $key => $value)
   {
    $id =  $value['graduity_id']; 
    $this->db->where('graduity_id', $id);
      $this->db->delete('tbl_graduitynomination_detail');
   }
   // die();


       foreach($data1 as $key => $value)
   {

      if (!empty($value['id']) || $value['full_name_nominee'] !='') {

      $arr = array (
        'sr_no'            => $key,
        'name'          => $value['full_name_nominee'],
        'graduity_id'   => $value['graduity_id'],
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior']
      );
         // print_r($arr);
         // die;
      // extract($arr);
          //die;
      // $this->db->where('id', $id);
      $this->db->insert('tbl_graduitynomination_detail', $arr);
         // echo $this->db->last_query();
          
    }
    // die;
    }
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! gratuity nomination and authorisation form');  

    }else{

      $this->session->set_flashdata('tr_msg', 'Successfully save  gratuity nomination and authorisation form'); 

    }
    redirect('Gratuity_nomination_form/edit/'.$staff.'/'.$candidateid);   
  }


  $submitsenddata = $this->input->post('submitbtn');

  if (!empty($submitsenddata) && $submitsenddata =='senddatasubmit') {


// print_r($this->input->post()); die();

    $dadate = $this->input->post('dadate');

    $dadate = $this->General_nomination_and_authorisation_form_model->changedatedbformate($dadate);



    $updatearraydata = array (

      'staff_id'     => $staff_id,
      'candidate_id' => $candidateid,
      'place'        => $this->input->post('daplace'),
      'date'         => $dadate,
      'isupdate'     => 1


    );
    $this->db->where('candidate_id', $candidateid);
    $this->db->update('tbl_graduitynomination', $updatearraydata);
     // echo $this->db->last_query();
      //die;
      //$countnominee = count($this->input->post('full_name_nominee'));


    $data1=$this->input->post('data');
    //print_r($data1);
    //die;

  foreach($data1 as $value)
   {
    $id =  $value['graduity_id']; 
    $this->db->where('graduity_id', $id);
      $this->db->delete('tbl_graduitynomination_detail');
   }



       foreach($data1 as $key => $value)
   {
      if ($value['full_name_nominee'] !='') {
      $arr = array (

        'sr_no'            => $key,        
        'name'          => $value['full_name_nominee'],
        'graduity_id'   => $value['graduity_id'],
        'relation_id'   => $value['relationship_nominee'],
        'age'           => $value['age_nominee'],
        'address'       => $value['address_nominee'],
        'share_nominee' => $value['share_nominee'],
        'minior'        => $value['minior']

      );
         // print_r($arr);
         // die;
      // extract($arr);
          //die;
      // $this->db->where('id', $id);
      $this->db->insert('tbl_graduitynomination_detail', $arr);
         // echo $this->db->last_query();
         //  die;
    }
    }
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! gratuity nomination and authorisation form');  

    }

      /*else{

        // $hrdemailid    = 'poonamadlekha@pradan.net';
         $hrdemailid    = 'amit.kum2008@gmail.com';
         $tcemailid     = $this->loginData->EmailID;

         $subject = "Submit Gereral Nomination And Authorisation Form ";
          $body =   'Dear Sir, <br><br> ';
          $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
         //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
         $to_email = $hrdemailid;
         $to_name = $tcemailid;
        
         $sendmail = $this->Common_Model->send_email($subject, $body, $to_email,$to_name);*/

         $this->session->set_flashdata('tr_msg', 'Successfully save and submit gratuity nomination and authorisation form'); 


         $subject = ': Gratuity nomination form';
         $body = '<h4>'.$content['candidateaddress']->staff_name.' submited  Gratuity nomination form </h4><br />';
         $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
         <tr>
         <td width="96">Name </td>
         <td width="404">'.$content['candidateaddress']->staff_name.'</td>
         </tr>
         <tr>
         <td>Employ Code</td>
         <td> '.$content['candidateaddress']->emp_code.'</td>
         </tr>
         <tr>
         <td>Designation</td>
         <td>' .$content['candidateaddress']->desiname.'</td>
         </tr>
         <tr>
         <td>Office</td>
         <td>'.$content['candidateaddress']->officename.'</td>
         </tr>
         </table>';
         $body .= "<br /> <br />";
         $body .= "Regards <br />";
         $body .= " ". $content['candidateaddress']->staff_name ."<br>";
         $body .= " ". $content['candidateaddress']->desiname."<br>";
         $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
         $to_useremail = $content['candidateaddress']->emailid;
         $tcemailid=$content['tc_email']->emailid;
        // $personal_email=$content['personnal_mail']->EmailID;
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
         $arr= array (
          $tcemailid      =>'tc',
        );

         $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);

         redirect('Gratuity_nomination_form/view/'.$staff_id.'/'.$candidateid);   



       }


     }


     $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();
     $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);

     $content['candidatedetailwithaddress'] = $this->Gratuity_nomination_form_model->getCandidateWithAddressDetails($staff_id);



     $content['genenominformdetail'] = $this->General_nomination_and_authorisation_form_model->getGeneralnominationform($staff_id);
   // print_r($content['genenominformdetail']);
//die("dhgdhs");
     $content['nominee'] =$this->Gratuity_nomination_form_model->get_pfinformation($candidateid);
  // echo "<pre>";
  //  print_r($content['nominee']);
  //   die;

        //die('hdghds');
     $content['nomineedetail'] = $this->Gratuity_nomination_form_model->getNomineedetail($candidateid);
   //  echo "<pre>";
   // print_r($content['nomineedetail']);





     $content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
     $content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);
    //print_r($content['getgeneralform']);
     $content['witnessdeclaration'] = $this->General_nomination_and_authorisation_form_model->getWitnessDeclaration($candidateid);


     $content['title'] = 'Gratuity_nomination_form_form/edit';

     $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

     $this->load->view('_main_layout', $content);
   }

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }



 public function view($staff,$candidate_id)
 {
  try{
   if (empty($staff) || empty($candidate_id) || $staff == '' || $candidate_id == '' ) {
    
    $this->session->set_flashdata('er_msg', 'Required parameter $staffid and $candidate_id is either blank or empty.');
    redirect('/staff_dashboard/index');
    
  } else {

   $staff = $this->uri->segment(3);
   $candidate_id  = $this->uri->segment(4);

   $content['staff'] = $staff;
   $content['candidate_id'] = $candidate_id;


   $this->session->set_userdata('staff', $staff);
   $this->session->set_userdata('candidate_id', $candidate_id);
   $login_staff      = $this->loginData->staffid;






   $staff_id=$staff;
   $candidateid=$candidate_id;

   if(isset($staff_id)&& isset($candidateid))
   {

    $staff_id=$staff_id;
       // echo "staff id".$staff_id;
    $candidateid=$candidateid;
        //echo "candate id=".$candidateid;

  }
  else
  {
    $staff_id=$staff;
       // echo "staff id".$staff_id;
    $candidateid=$candidate_id;
  }
// echo $staff_id.$candidateid;
// die;

  $content['id'] = $this->Gratuity_nomination_form_model->get_gratuityworkflowid($staff_id);
  //   print_r($content['id']);
  // die;


  $content['staff_details'] = $this->Gratuity_nomination_form_model->staffName($staff_id,$login_staff);


  
  $content['sysrelations'] = $this->General_nomination_and_authorisation_form_model->getSysRelations();
  //  print_r($content['sysrelations']);
  // die;

  $content['candidatedetailwithaddressc'] = $this->General_nomination_and_authorisation_form_model->getCandidateWithAddressDetails($candidateid);
  // print_r($content['candidatedetailwithaddress']);
  // die;

  $content['candidatedetailwithaddress'] = $this->Gratuity_nomination_form_model->getCandidateWithAddressDetails($staff_id);

  $content['nominee'] =$this->Gratuity_nomination_form_model->get_pfinformation($candidateid);

   // print_r($content['nominee']); die;



  $content['get_witness'] = $this->Gratuity_nomination_form_model->get_witness_detail($staff_id);
  $content['signature'] = $this->Common_Model->staff_signature($staff_id);
  
  $content['nomineedetail'] = $this->Gratuity_nomination_form_model->getNomineedetail($candidateid);
    // echo "<pre>";
    // print_r($content['nomineedetail']); die;
  $content['candidateaddress'] = $this->Gratuity_nomination_form_model->getCandidateWithAddressDetails($staff_id);
  $content['personnal_mail'] = $this->Gratuity_nomination_form_model->personal_email();
      // print_r($content['personnal_mail']); die;
  $personal_email=$content['personnal_mail']->EmailID;

  $reportingto = $content['candidateaddress']->reportingto;
    // echo $reportingto; die;
  $content['tc_email'] = $this->Gratuity_nomination_form_model->tc_email($reportingto);
  $content['reporting'] = $this->Gratuity_nomination_form_model->getCandidateWith($reportingto);

  $personnelname = $content['personnal_mail']->UserFirstName.' '.$content['personnal_mail']->UserMiddleName.' '.$content['personnal_mail']->UserLastName;


  $content['personal']=$this->Common_Model->get_Personnal_Staff_List();

  if($this->loginData->RoleID==2 || $this->loginData->RoleID==21)
  {

      //print_r($this->input->post()); die;
   $RequestMethod = $this->input->server('REQUEST_METHOD'); 


   if($RequestMethod == 'POST'){
                               // print_r($_POST);die;
    $status     = $this->input->post('status');
    $reject     = $this->input->post('reject');
      // print_r($reject); die;
    $p_status   = $this->input->post('p_status');
    $wdtaff1    = $this->input->post('wdtaff1');
    $wdtaff2    = $this->input->post('wdtaff2');
    $address1    = $this->input->post('w_add1'); 
    $address2    = $this->input->post('w_add2'); 
    $r_id       = $this->input->post('id');

    if($status==2){

      $appstatus = 'approved';
    }elseif($status==3){
      $appstatus = 'rejected';
    }
      // $this->db->start_trans();
    $check    = $this->session->userdata('insert_id');
      // print_r($check); die;

    $arr_data = array (

      'witness1' => $wdtaff1,
      'witness2' => $wdtaff2,
      'address1' => $address1,
      'address2' => $address2, 
    );
      // print_r($arr_data); die;

    $this->db->where('graduity_id',$r_id);
    $this->db->update('tbl_graduitynomination',$arr_data);


    $insert_data = array (

      'type'                 => 14,
      'r_id'                 => $r_id,
      'sender'               => $login_staff,
      'receiver'             => $p_status,
      'senddate'             => date('Y-m-d'),
      'createdon'            => date('Y-m-d H:i:s'),
      'createdby'            => $login_staff,
      'scomments'            => $reject,
      'forwarded_workflowid' => $content['id']->workflow_id,
      'flag'                 => $status,
      'staffid'              => $staff_id
    );
    $this->db->insert('tbl_workflowdetail', $insert_data);


    $subject = ': Gratuity nomination form';
    $body = '<h4>'.$content['reporting']->reprotingname.' '.$appstatus.' by Tc </h4><br />';
    $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
    <tr>
    <td width="96">Name </td>
    <td width="404">'.$content['candidateaddress']->staff_name.'</td>
    </tr>
    <tr>
    <td>Employ Code</td>
    <td> '.$content['candidateaddress']->emp_code.'</td>
    </tr>
    <tr>
    <td>Designation</td>
    <td>' .$content['candidateaddress']->desiname.'</td>
    </tr>
    <tr>
    <td>Office</td>
    <td>'.$content['candidateaddress']->officename.'</td>
    </tr>
    </table>';
    $body .= "<br /> <br />";
    $body .= "Regards <br />";
    $body .= " ". $content['candidateaddress']->staff_name ."<br>";
    $body .= " ". $content['candidateaddress']->desiname."<br>";
    $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
    $to_useremail = $content['candidateaddress']->emailid;
    $tcemailid=$content['tc_email']->emailid;
    $personal_email=$content['personnal_mail']->EmailID;
    $to_name = '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
    $arr= array (
      $tcemailid      =>'tc',
      $personal_email => 'personal'
    );

    $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
    redirect('/Gratuity_nomination_form/view/'.$staff.'/'.$candidateid);



  }


}
if($this->loginData->RoleID==17)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){
     // print_r($_POST);die;
   $status   =$this->input->post('status');
   $reject   =$this->input->post('reject');
   $p_status =$this->input->post('p_status');
   $wdtaff1  =$this->input->post('wdtaff1');
   $wdtaff2  =$this->input->post('wdtaff2');
   $r_id     =$this->input->post('id');



   if($status==4){

    $appstatus = 'approved';
  }elseif($status==3){
    $appstatus = 'rejected';
  }

  $insert_data = array (

    'type'                 => 14,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => $status,
    'staffid'              => $staff_id
  );
    // print_r($insert_data); die;
  $this->db->insert('tbl_workflowdetail', $insert_data);

  $arr_data =array (

    'personal_date'  =>   $this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('personal_date')),
      // 'personal_place' =>$this->input->post('personal_place'),
    'personal_name'  =>$login_staff,

  ); 
       // print_r($insert_data);
        //die();
  $this->db->where('graduity_id',$r_id);
  $this->db->update('tbl_graduitynomination',$arr_data);


  $subject = ': Gratuity nomination form';
  $body = '<h4>'.$personnelname.' '.$appstatus.' by personnel</h4><br />';
  $body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
  <tr>
  <td width="96">Name </td>
  <td width="404">'.$content['candidateaddress']->staff_name.'</td>
  </tr>
  <tr>
  <td>Employ Code</td>
  <td> '.$content['candidateaddress']->emp_code.'</td>
  </tr>
  <tr>
  <td>Designation</td>
  <td>' .$content['candidateaddress']->desiname.'</td>
  </tr>
  <tr>
  <td>Office</td>
  <td>'.$content['candidateaddress']->officename.'</td>
  </tr>
  </table>';
  $body .= "<br /> <br />";
  $body .= "Regards <br />";
  $body .= " ". $content['candidateaddress']->staff_name ."<br>";
  $body .= " ". $content['candidateaddress']->desiname."<br>";
  $body .= "<b> Thanks </b><br>";



       // $to_useremail = 'pdhamija2012@gmail.com';
  $to_useremail = $content['candidateaddress']->emailid;
  $tcemailid=$content['tc_email']->emailid;
  $personal_email=$content['personnal_mail']->EmailID;
  $to_name= '';
        // echo $personal_email;
        // die;

       // $to_hremail = $hremail->EmailID;
  $arr= array (
    $tcemailid      =>'tc',
    $personal_email => 'personal'
  );

  $this->Common_Model->send_email($subject, $body,$to_useremail ,$to_name,$arr);
  redirect('/Gratuity_nomination_form/view/'.$staff.'/'.$candidateid);

}


}

if($this->loginData->RoleID==3)
{
 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){

   $status   =$this->input->post('status');
   $reject   =$this->input->post('reject');
   $p_status =$this->input->post('p_status');
   $r_id     =$this->input->post('id');

   $insert_data = array (

    'type'                 => 14,
    'r_id'                 => $r_id,
    'sender'               => $login_staff,
    'receiver'             => $staff_id,
    'senddate'             => date('Y-m-d'),
    'createdon'            => date('Y-m-d H:i:s'),
    'createdby'            => $login_staff,
    'scomments'            => $reject,
    'forwarded_workflowid' => $content['id']->workflow_id,
    'flag'                 => 5,
    'staffid'              => $staff_id
  );
    // print_r($insert_data); die;
   $this->db->insert('tbl_workflowdetail', $insert_data);


   $arr_data = array (
    'acknowlegement_date'=> $this->gmodel->changedatedbformate($this->input->post('acknowlegement_date')),
  ); 
    // print_r($arr_data); die;

   $this->db->where('graduity_id',$r_id);
   $this->db->update('tbl_graduitynomination',$arr_data);
   redirect('/Gratuity_nomination_form/view/'.$staff.'/'.$candidateid);



 }


}
$query= "select acknowlegement_date from tbl_graduitynomination where staff_id = '$staff_id'";
$content['acknowlegement_date'] = $this->db->query($query)->row();


   // print_r($content['acknowlegement_date']); die;


    //$content['nomineedetail'] = $this->General_nomination_and_authorisation_form_model->getNomineedetail($getGeneralnomination->id);
$content['getjoiningreport']       = $this->General_nomination_and_authorisation_form_model->getJoiningReport($candidateid);
$content['getgeneralform'] = $this->General_nomination_and_authorisation_form_model->getGeneralFormStatus($candidateid);


$content['title'] = 'Gratuity_nomination_form/view';

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

$this->load->view('_main_layout', $content);
}

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



public function Add()
{
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


  $RequestMethod = $this->input->server('REQUEST_METHOD'); 


  if($RequestMethod == 'POST'){

    $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]');
    $this->form_validation->set_rules('status','Status','trim|required');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }


    $insertArr = array(
      'pgname'      => $this->input->post('pgname'),
      'status'      => $this->input->post('status')
    );

    $this->db->insert('mstpgeducation', $insertArr);
     // echo $this->db->last_query(); die;
    $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect('/Employee_particular_form/index/');
  }

  prepareview:


  $content['subview'] = 'Employee_particular_form/add';
  $this->load->view('_main_layout', $content);
  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

}