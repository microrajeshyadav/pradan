<?php 

/**
* ED Summary END of the List
*/
class Edsummaryeod extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		//$this->load->model('EDsummaryeod_model');
		$this->load->model(__CLASS__ . '_model','model');
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');
		}
	}

	public function index()
	{
		try{
			  	 // start permission 
    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();
    // end permission 
    

		$isssuepending = $this->EdIssuePending();
		//print_r($isssuepending); die;
		$getEdDetails   = $this->getRoleEdDetails();
		$edmail         = $getEdDetails[0]->EmailID;
		//$msgcontent =  $this->load->view('EDsummaryeod/email_template'); 
		

	$subject = "Following Issue Pending";
	$body = '<h4> Dear  Sir, <h4><br>';
	$body .= " <h4>Kindly Check it pending isue.</h4>";
	$body .= "<table style='border: 2px dashed #FB4314; width: 500px; height: 200px;''>
        <tr>
          <td >Category</td>
          <td >Candidate Name </td>
          <td >Created Date</td>
          </tr>";
	foreach ($isssuepending as $key => $value) {

	$body .= '<tr>';
	$body .= '<td>DA</td><td>'.$value->candidatefirstname.' '.$value->candidatemiddlename.' '.$value->candidatelastname.'</td>';
  $body .= '<td>'.$value->createdon.'</td></tr></table>';
 
	}
	
	$to_email = $edmail;
	$to_name = 'amit.kum2008@gmail.com';

	$this->Common_Model->send_email($subject, $body, $to_email, $to_name);

		 
    	 // $sendmail = $this->Common_Model->send_email($subject = 'Following Issue Pending', $message = $msg, $edmail);
		$content['listisssuepending']  = $this->EdIssuePending();
		$content['title'] = 'EDsummaryeod';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
	}
	

   /**
   * Method EdIssuePending() get Candidates List who is pending to approvel.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function EdIssuePending()
  {
    
    try{

		  $sql = 'SELECT  `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_generate_offer_letter_details`. sendflag, 
		  `tbl_generate_offer_letter_details`.createdon,`tbl_generate_offer_letter_details`.filename
            FROM `tbl_candidate_registration`
             left join `tbl_generate_offer_letter_details` ON `tbl_candidate_registration`.`candidateid` = `tbl_generate_offer_letter_details`.`candidateid`  WHERE `tbl_generate_offer_letter_details`.`sendflag`=0 '; 

        $result = $this->db->query($sql)->result();

      //  print_r($result); die;

        return $result;

     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }


 /**
   * Method getRoleEdDetails() get Candidates List who is pending to approvel.
   * @access  public
   * @param Null
   * @return  Array
   */

 public function getRoleEdDetails()
    {
        try{
           
            $sql = "SELECT * FROM `mstuser`  WHERE RoleID=6";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }	
        

}