<?php 
class Role_assign extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");

		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{

	    // start permission 
	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		$RequestMethod = $this->input->server('REQUEST_METHOD');			
		if($RequestMethod == 'POST'){
			$staffid = $this->input->post('staffid');
			$query = "Select CONCAT(IFNULL(mstuser.UserFirstName,''), ' ', IFNULL(mstuser.UserMiddleName,''), ' ', IFNULL(mstuser.UserLastName, '')) as Username, mstuser.UserID, sysaccesslevel.Acclevel_Name FROM mstuser RIGHT JOIN sysaccesslevel ON mstuser.RoleID = sysaccesslevel.Acclevel_Cd WHERE mstuser.IsDeleted = 0 AND mstuser.staffid=".$staffid;
			$content['roledetail'] = $this->db->query($query)->result();
			$content['title'] = 'Role_assign';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}else{
			$staffid = $this->loginData->staffid;
			$query = "Select CONCAT(IFNULL(mstuser.UserFirstName,''), ' ', IFNULL(mstuser.UserMiddleName,''), ' ', IFNULL(mstuser.UserLastName, '')) as Username, mstuser.UserID, sysaccesslevel.Acclevel_Name FROM mstuser RIGHT JOIN sysaccesslevel ON mstuser.RoleID = sysaccesslevel.Acclevel_Cd WHERE mstuser.IsDeleted = 0 AND mstuser.staffid=".$staffid;
			$content['roledetail'] = $this->db->query($query)->result();
			
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}

		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}


public function add()
	{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		
		/*echo "<pre>";
		print_r($content['role_permission']);exit;*/
		// end permission
		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				$this->db->trans_start();

				//print_r($this->input->post());

	           $roleid = $this->input->post('chooserole');
				$staffid = $this->input->post('staffid');
			    $query = "SELECT RoleID FROM mstuser WHERE RoleID = $roleid  AND  staffid=".$staffid;
				$mstrole = $this->db->query($query)->result();
					// print_r($mstrole);
						echo count($mstrole);
				if(count($mstrole)==0)
				{
						$roleid = $this->input->post('chooserole');
				$staffid = $this->input->post('staffid');

				$staffinfo = $this->model->staffDetailList($staffid);
				$roledetail = $this->model->getSingleRole($roleid);
					//print_r($roledetail);
		        $rolename = $roledetail[0]->Acclevel_Name;

				// print_r($staffinfo);  
				$expstaffname = explode(' ', $staffinfo->name);
				$stafffirstname =  $expstaffname[0];
				$staffmiddlename =  $expstaffname[1];
				$stafflastname =  $expstaffname[2];
				$username = $stafffirstname.'_'.$staffinfo->staffid;
				$staffemailid = $staffinfo->emailid;
				$staffcontactno = $staffinfo->contact;
				$addlink = site_url('login');
				$InsertArr = array(
					'Username' 			=> $username,
					'Password'  		=> md5($stafffirstname),
					'UserFirstName'  	=> $stafffirstname,
					'UserMiddleName'  	=> $staffmiddlename,
					'UserLastName'  	=> $stafflastname,
					'PhoneNumber'  		=> $staffcontactno,
					'EmailID'  			=> $staffemailid,
					'RoleID'  			=> $this->input->post('chooserole'),
					'CreatedOn' 		=> date('Y-m-d H:i:s'),
					'CreatedBy' 		=> $this->loginData->UserID,
					'staffid'           => $staffid,
				);
				$this->db->insert('mstuser', $InsertArr);
				}
				else
				{
						$this->session->set_flashdata('er_msg', 'Error Role assigned already to staff');
						redirect('/Role_assign/index');	
				}
				
	            $this->db->trans_complete();

				if ($this->db->trans_status() === FALSE){

					$this->session->set_flashdata('er_msg', 'Error Not Assign Role');

				}else{

					$html = 'Dear '.$stafffirstname.', <br><br> 
					Assign New Role by Pradan. 
					<br><br> Username - '.$username.'
					<br>Password - '.$stafffirstname.'
					<br>Role - '. $rolename .' <br><br>
					Please <a href='.$addlink.'>Click here</a>';

					$sendmail = $this->Common_Model->send_email($subject = 'New Role Login Details ', $message = $html, $staffemailid);

					$this->session->set_flashdata('tr_msg', 'Successfully Assign Role !!!');

					redirect(current_url());
				}

			}
			
			$content['getrole'] = $this->model->getRole($roleid = NULL);
			$content['getstafflist'] = $this->model->getStaffList();
			$content['getofficelist'] = $this->model->getOfficeList();
			$content['title'] = 'Ypresponse';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function getPassword()
	{

		try{

    // Generating Password
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
			$password = substr( str_shuffle( $chars ), 0, 8 );

			return $password;


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	public function delete($token){
		try{

		if (!empty($token)) {
			$updateArrstatus = array(
				'IsDeleted'     => 1,
			);
			$this->db->where('UserID', $token);
			$this->db->update('mstuser', $updateArrstatus);
			$this->session->set_flashdata('tr_msg' ,"Role assign Deleted Successfully");
			redirect('/Role_assign/index');
		}else{
			$this->session->set_flashdata('er_msg' ,"Sorry !!! Error Role assign Deleted ");
			redirect(current_url());
		}
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
     print_r($e->getMessage());die;
   }
	}


}