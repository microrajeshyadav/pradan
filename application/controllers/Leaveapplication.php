<?php 
class Leaveapplication extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Leaveapplication_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');
		// print_r($check); die();

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		try{

		// start permission 

		// echo $token; die;
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$empcode = $this->model->get_empid($token);
		$ed_id=$this->model->geted_id();
		//print_r($ed_id);
		//echo $ed_id->staffid;
		
		
		$content['getreportingdetails']  = $this->model->getreportingtodetails($empcode->emp_code);
		 //print_r($content['getreportingdetails']); die;
		$content['getscheduleholiday']  = $this->model->getscheduleholiday($content['getreportingdetails']->new_office_id);
		/*echo "<pre>";
		print_r($content['getscheduleholiday']); die();*/
	
		$content['personal']  = $this->Common_Model->get_Personnal_Staff_List();

		 
		 $content['hrdetails']  = $this->Common_Model->get_hrdetails();
	
		$RequestMethod = $this->input->server("REQUEST_METHOD");

		if($RequestMethod == 'POST')
		{
				
				$office_id=$this->input->post('office_id');

			$fromdate = $this->input->post('fromdate');
			$todate = $this->input->post('todate'); 

			$noofdays = $this->input->post('noofdays');
			$monthlyclosing  = $this->model->leaveclosing($fromdate,$todate,$office_id);
			if(!empty($fromdate)&& !empty($todate)&& !empty($office_id))
			{
				if($monthlyclosing>0)
				{
					$this->session->set_flashdata('tr_msg',"Request  not saved & send email successfully");

				redirect('leaverequest/index/'.$token);
				}
			}
			$reason = $this->input->post('reason');
		    $leavetyped = $this->input->post('leavetype');
		    $longleave=$this->model->getlongleave($leavetyped);
	    	if ($noofdays > $longleave->longleavedays)
	    	{
	    		$longleave = 1;
	    	}
	    	else
	    	{
	    		$longleave = 0;
	    	}
	    	// echo $longleave;
	    	// die;
			// $leavetyped = "";
			switch ($leavetyped) {
				case '1':
				$leavename = "Maternity";
				break;

				case '2':
				$leavename = "LWP(Leave without pay)";
				break;

				case '3':
				$leavename = "Special";
				break;

				case '4':
				$leavename = "Study";
				break;

				case '5':
				$leavename = "Sabbatical";
				break;

				case '6':
				$leavename = "General Leave";
				break;

				case '7':
				$leavename = "Leave with pay";
				break;

				case '8':
				$leavename = "Paternity";
				break;
			}
			// echo $leavename; die;

			$this->db->trans_start();

// 			echo   $sql = "SELECT count(id) as countid FROM trnleave_ledger WHERE  Emp_code=".$empcode->emp_code."  AND From_date BETWEEN '".$this->gmodel->changedatedbformate($fromdate)."' AND '".$this->gmodel->changedatedbformate($todate)."' OR  To_date  BETWEEN '".$this->gmodel->changedatedbformate($fromdate)."' AND '".$this->gmodel->changedatedbformate($todate)."'"; 
// die;

// 	$result = $this->db->query($sql)->row();


	// if ($result->countid == 0) {

			$insertArray = array(
				'Emp_code'              => $empcode->emp_code,
				'From_date'             => $this->gmodel->changedatedbformate($fromdate),
				'To_date'               => $this->gmodel->changedatedbformate($todate),
				'Leave_type'            => $leavetyped,
				'Description'           => 'Leave Applied',
				'Leave_transactiontype' => 'DR',
				'Noofdays'              => $noofdays,
				'status'                => 1,
				'reason'                => $reason,
				'purpose'               => $this->input->post('purpose'),
				'appliedby'             => $this->loginData->staffid,
				'appliedon'             => date("Y-m-d H:i:s"),
				'maturnityduedate'             => $this->gmodel->changedatedbformate($this->input->post('inputmaternityduedate')),
				'longleave'       => $longleave



			);
			 //print_r($insertArray); die;
			$this->db->insert('trnleave_ledger',$insertArray);
			// echo $this->db->last_query();
			// die;
			
			$insertid = $this->db->insert_id(); 

			$firstrow=$this->input->post('firstrow');
			
			$secondrow=$this->input->post('secondrow');
			
			if (!empty($firstrow) && !empty($secondrow)) {
				$laeverray = array (

					'From_date'          => $this->gmodel->changedatedbformate($fromdate),
					'Leave_from_type'    => $firstrow,
					'To_date'            => $this->gmodel->changedatedbformate($todate),
					'leave_to_type'      => $secondrow,
					'trnleave_ledger_id' => $insertid
				);

				$this->db->insert('trnleave_ledger_transaction', $laeverray);
			}
			

			//echo $this->db->last_query();

			if ($leavetyped ==5) {
				
				$fromdate1 = $this->gmodel->changedatedbformate($fromdate);
				$todate1   = $this->gmodel->changedatedbformate($todate);
				$expfrom   = explode('-', $fromdate1);
				$exptod    = explode('-', $todate1);
				$date1     = $expfrom[0].$expfrom[1];
				$date2     = $exptod[0].$exptod[1];
				$monthdiff = 0;

				while ($date1 <=$date2) {
					$monthdiff++;
					$date1=$date1+1;

				}

				$currentyear = $this->gmodel->getcurrentfyear();

				$totalcrsql = "select SUM(credit_value) as credit_value from msleavecrperiod where CYear='".$currentyear."'"; 
				$totalcrres = $this->db->query($totalcrsql)->row();
				$perdayleave =  $totalcrres->credit_value/365;

				
				
				$incrementedmonth = $this->gmodel->changedatedbformate($fromdate);
				for ($i=0; $i < $monthdiff; $i++) { 


					if ($i==0 && $monthdiff ==1) {

						$fromdate1 = $this->gmodel->changedatedbformate($fromdate);
						$todate1 = $this->gmodel->changedatedbformate($todate);

						$date1       = date_create($fromdate1);
						$date2       = date_create($todate1);
						$diff        = date_diff($date1,$date2);
						$datesqldiff = $diff->format("%R%a");

						//$datesqldiff = date_diff($fromdate1, $todate1);
						$noofdays=0;
						$noofdays = $this->gmodel->roundvalue($datesqldiff*$perdayleave);

						$insertmonthdiffArray = array(
							'Emp_code'              => $empcode->emp_code,
							'From_date'             => $fromdate1,
							'To_date'               => $todate1,
							'Leave_type'            => 100,
							'Description'           => 'DLWP',
							'Leave_transactiontype' => 'CR',
							'Noofdays'              => $noofdays,
							'status'                => $insertid,
							'reason'                => $reason,
							'purpose'               => $this->input->post('purpose'),
							'appliedby'             => $this->loginData->staffid,
							'appliedon'             => date("Y-m-d H:i:s")

						);
			 //print_r($insertmonthdiffArray); //die;
						$this->db->insert('trnleave_ledger',$insertmonthdiffArray);

					//	echo $this->db->last_query(); 
						//echo "1";

					}elseif ($i==0 && $monthdiff !=1 ) {

						

						$expdata = explode('/', $fromdate);
						$expdata[2];
						$expdata[1];
						$lastdayofthemonth = cal_days_in_month(CAL_GREGORIAN, $expdata[1], $expdata[2]); 

						$todate34 = $expdata[2].'-'.$expdata[1].'-'.$lastdayofthemonth;
						$nfromdate = $this->gmodel->changedatedbformate($fromdate);

						$date1        = date_create($todate34);
						$date2        = date_create($nfromdate);
						$diff         = date_diff($date2,$date1);
						$datesqldiff1 = $diff->format("%R%a");
						$datesqldiff  = $datesqldiff1 +1;
						$noofdays     = 0;
						$noofdays     = $this->gmodel->roundvalue($datesqldiff*$perdayleave);



						$insertmonthdiffArray = array(
							'Emp_code'              => $empcode->emp_code,
							'From_date'             => $nfromdate,
							'To_date'               => $todate34,
							'Leave_type'            => 100,
							'Description'           => 'DLWP',
							'Leave_transactiontype' => 'CR',
							'Noofdays'              => $noofdays,
							'status'                => $insertid,
							'reason'                => $reason,
							'purpose'               => $this->input->post('purpose'),
							'appliedby'             => $this->loginData->staffid,
							'appliedon'             => date("Y-m-d H:i:s")
						);

			// print_r($insertmonthdiffArray); //die;
						$this->db->insert('trnleave_ledger',$insertmonthdiffArray);
				//echo $this->db->last_query(); 
					//	echo "2"; 

					}elseif ($i==$monthdiff-1) {

						$newfromdate = date('Y-m-d', strtotime($incrementedmonth . '+1 month'));
						$incrementedmonth = $newfromdate;

						$expdata = explode('-', $incrementedmonth);
						$lastdayofthemonth = cal_days_in_month(CAL_GREGORIAN, $expdata[1], $expdata[0]); 

						$newfromdate  = $expdata[0].'-'.$expdata[1].'-01';
						

						$newtodate = $this->gmodel->changedatedbformate($todate);

						$date1=date_create($newtodate);
						$date2=date_create($newfromdate);
						$diff=date_diff($date2,$date1);
						$datesqldiff =  $diff->format("%R%a");
						//$datesqldiff = $datesqldiff1 +1;


						// $datesqldiff = date_diff($this->gmodel->changedatedbformate($newfromdate), $this->gmodel->changedatedbformate($todate));

						$noofdays=0;
						$noofdays = $this->gmodel->roundvalue($datesqldiff*$perdayleave);

						$insertmonthdiffArray = array (
							'Emp_code'              => $empcode->emp_code,
							'From_date'             => $newfromdate,
							'To_date'               => $newtodate,
							'Leave_type'            => 100,
							'Description'           => 'DLWP',
							'Leave_transactiontype' => 'CR',
							'Noofdays'              => $noofdays,
							'status'                => $insertid,
							'reason'                => $reason,
							'purpose'               => $this->input->post('purpose'),
							'appliedby'             => $this->loginData->staffid,
							'appliedon'             => date("Y-m-d H:i:s")

						);
			// print_r($insertmonthdiffArray);
						$this->db->insert('trnleave_ledger',$insertmonthdiffArray);
						//echo $this->db->last_query(); 
						//echo "3"; 
						
					}else{

						$newfromdate = date('Y-m-d', strtotime($incrementedmonth . '+1 month'));
						$incrementedmonth = $newfromdate;

						$expfromdate = explode('-', $newfromdate);
						$newfromdate = $expfromdate[0].'-'.$expfromdate[1].'-01';

						$expdata = explode('-', $newfromdate);
						$expdata[2];
						$expdata[1];
						$lastdayofthemonth = cal_days_in_month(CAL_GREGORIAN, $expdata[1], $expdata[2]); 

						$newtodate  = $expdata[0].'-'.$expdata[1].'-'.$lastdayofthemonth;

						$date1=date_create($newtodate);
						$date2=date_create($newfromdate);
						$diff=date_diff($date2,$date1);
						$datesqldiff =  $diff->format("%R%a");


						//echo $datesqldiff = date_diff($newtodate,$newfromdate);
						$noofdays=0;
						$noofdays = $this->gmodel->roundvalue($datesqldiff*$perdayleave);
						$insertmonthdiffArray = array(
							'Emp_code'              => $empcode->emp_code,
							'From_date'             => $newfromdate,
							'To_date'               => $newtodate,
							'Leave_type'            => 100,
							'Description'           => 'DLWP',
							'Leave_transactiontype' => 'CR',
							'Noofdays'              => $noofdays,
							'status'                => $insertid,
							'reason'                => $reason,
							'purpose'               => $this->input->post('purpose'),
							'appliedby'             => $this->loginData->staffid,
							'appliedon'             => date("Y-m-d H:i:s")

						);
						//print_r($insertmonthdiffArray);

						$this->db->insert('trnleave_ledger',$insertmonthdiffArray);
						//echo $this->db->last_query(); 
					//echo "4";
					}

				}

				//echo "dsfsdf"; die;

			}


			if($longleave==0)
			{
				// echo "dfdf";
				// die;

				$insert_data =array(
					'type'      => 5,
					'r_id'      => $insertid,
					'sender'    => $token,
					'receiver'  => $content['getreportingdetails']->reportingstaffid,
					'senddate'  => date('Y-m-d'),
					'createdon' => date('Y-m-d H:i:s'),
					'createdby' => $this->loginData->UserID,
					'flag'      => 1,
					'staffid'   => $token
				);
				$this->db->insert('tbl_workflowdetail', $insert_data);

			}
			else
			{
					// if($this->loginData->loginData==18)
					// {
				$insert_data =array(
					'type'      => 5,
					'r_id'      => $insertid,
					'sender'    => $token,
					'receiver'  => $ed_id->staffid,
					'senddate'  => date('Y-m-d'),
					'createdon' => date('Y-m-d H:i:s'),
					'createdby' => $this->loginData->UserID,
					'flag'      => 1,
					'staffid'   => $token
				);
			

			$this->db->insert('tbl_workflowdetail', $insert_data);
			}
			// echo $this->db->last_query();
			// die;

		// 		// 
		// 		$insert_data =array(
		// 			'type'      => 5,
		// 			'r_id'      => $insertid,
		// 			'sender'    => $token,
		// 			'receiver'  => $content['personal'][0]->staffid,
		// 			'senddate'  => date('Y-m-d'),
		// 			'createdon' => date('Y-m-d H:i:s'),
		// 			'createdby' => $this->loginData->UserID,
		// 			'flag'      => 1,
		// 			'staffid'   => $token
		// 		);
			

		// 	$this->db->insert('tbl_workflowdetail', $insert_data);
		// 	// }
		// 	// else if($this->loginData->loginData==16)
		// 	// {
		// 		$insert_data =array(
		// 			'type'      => 5,
		// 			'r_id'      => $insertid,
		// 			'sender'    => $token,
		// 			'receiver'  => $content['hrdetails'][0]->staffid,
		// 			'senddate'  => date('Y-m-d'),
		// 			'createdon' => date('Y-m-d H:i:s'),
		// 			'createdby' => $this->loginData->UserID,
		// 			'flag'      => 1,
		// 			'staffid'   => $token
		// 		);
			

		// 	$this->db->insert('tbl_workflowdetail', $insert_data);
		// 	// }
		// 	// else if($this->loginData->loginData==2)
		// 	// {
		// 		$insert_data =array(
		// 			'type'      => 5,
		// 			'r_id'      => $insertid,
		// 			'sender'    => $token,
		// 			'receiver'  => $content['getreportingdetails']->reportingstaffid,
		// 			'senddate'  => date('Y-m-d'),
		// 			'createdon' => date('Y-m-d H:i:s'),
		// 			'createdby' => $this->loginData->UserID,
		// 			'flag'      => 1,
		// 			'staffid'   => $token
		// 		);
			

		// 	$this->db->insert('tbl_workflowdetail', $insert_data);
		// 	//}
		// }
			//echo $this->db->last_query(); die;
			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$this->session->set_flashdata('er_msg', "Request not save!! Please Try Again");
			}else{

				$getreportingdetails  = $this->model->getreportingtodetails($empcode->emp_code);

				$staff_name = $getreportingdetails->staffname;
				$staffreporting_emailid = $getreportingdetails->reportingtoemailid;
				$staff_designation = $getreportingdetails->staffdesignation;
				$hremail = $this->Common_Model->get_hr_Staff_List();


				$subject = ': Leave Application';
				$body = '<h4>Dear Sir/Madam, </h4><br />';
				$body .= '<table width="500" border="2" cellspacing="0" cellpadding="0" style="border-color:#000000; border-bottom-style:outset;">
				<tr>
				<td width="96">Leave Type </td>
				<td width="404">'.$leavename.'</td>
				</tr>
				<tr>
				<td>Period</td>
				<td> '.$fromdate.' - '.$todate.'</td>
				</tr>
				<tr>
				<td>Days</td>
				<td>' .$noofdays.'</td>
				</tr>
				<tr>
				<td>Reason</td>
				<td>'.$reason.'</td>
				</tr>
				</table>';
				$body .= "<br /> <br />";
				$body .= "Regards <br />";
				$body .= " ". $staff_name ."<br>";
				$body .= " ". $staff_designation."<br>";
				$body .= "<b> Thanks </b><br>";
				

				$to_useremail = $staffreporting_emailid;
				$to_hremail = $hremail->EmailID;

			$this->session->set_flashdata('tr_msg',"Request saved & send email successfully");

				redirect('leaverequest/index/'.$token);
			}

		// }else{

		// 	$this->session->set_flashdata('tr_msg',"Leave applied same date !!! Please select other date");

		// 		redirect('leaverequest/index/'.$token);
		// }


		}
		
		$content['leavebalance'] = $this->model->getleavedetail_balance($empcode->emp_code);
		// echo "<pre>";
		// print_r($content['leavebalance']);exit(); 
		$content['leavecategory'] = $this->model->getleavectegory();

		$content['leaverequest'] = $this->model->getleavedetails('1');
		$content['title'] = 'Leaveapplication';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}


	public function getDaysdiff(){
		try{

		$sql = "select leave_no_days('2018-02-22','2018-02-25')";
		$result = $this->db->query()->result(); 
		return $result;

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}

	





}