<?php 
class Joining_report_of_newplace_posting extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Staff_seperation_model","Staff_seperation_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		 $check = $this->session->userdata('login_data');
		    ///// Check Session //////  
		    if (empty($check)) {
		       redirect('login');
		    }

		     $this->loginData = $this->session->userdata('login_data');
		     /*echo "<pre>";
		     print_r($this->loginData);die();*/
	}

	public function index()
	{
		try{

		$this->load->model('Joining_report_of_newplace_posting_model');

		$this->db->trans_start();

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
		


		$savebtn = $this->input->post('savebtn');
			$submitbtn = $this->input->post('submitbtn');

			$staffdutytodaydate = $this->input->post('staff_duty_today_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);
			$dutyon = $this->gmodel->changedatedbformate($this->input->post('dutyon'));

			if (isset($savebtn) && $savebtn =='senddatasave') {

			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	           'flag'               => 0,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
      
          $this->db->insert('tbl_joining_report_new_place', $insertArr);
          $insertid = $this->db->insert_id();
      }
			
		$content['title'] = 'Joining_report_of_newplace_posting/index2';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

	}
	}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

}
	public function add($token)
	{
		try{
		//die('gfhg');
		$this->load->model('Joining_report_of_newplace_posting_model');

			$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->result()[0];
	    // print_r($result);
	    // die;
	    $content['workflow_flag']=$result;



		$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->result()[0];
		
	    $forwardworkflowid = $result->workflowid;
		$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		/*echo "<pre>";
		print_r($content['getstaffdetailslist']);exit();*/
		$content['getstaffsuperiordetailslist'] = $this->model->getstaffSuperior($token);
		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		$receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['getstaffdetailslist']->staffid);

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			/*echo "<pre>";
			print_r($_POST); die;*/
			
			$savebtn = $this->input->post('savebtn');
			$submitbtn = $this->input->post('submitbtn');

			$staffdutytodaydate = $this->input->post('staff_duty_today_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);
			$dutyon = $this->gmodel->changedatedbformate($this->input->post('dutyon'));

			if (isset($savebtn) && $savebtn =='senddatasave') {

			$this->db->trans_start();

			$newtcquery = "SELECT * FROM staff_transaction WHERE trans_status='BOTH' AND staffid=".$content['getstaffdetailslist']->staffid." AND id=".$token;
	    	$newtc = $this->db->query($newtcquery)->row();
	    	$receiver = $newtc->reportingto;
	    	$comments = 'Joining Report filled By Staff';
			$updateArr = array(                         
	          'trans_flag'     => 13,
	          'updatedon'      => date("Y-m-d H:i:s"),
	          'updatedby'      => $this->loginData->staffid
	        );

	        $this->db->where('id',$token);
	        $this->db->update('staff_transaction', $updateArr);

	        $insertworkflowArr = array(
	         'r_id'                 => $token,
	         'type'                 => 6,
	         'staffid'              => $content['getstaffdetailslist']->staffid,
	         'sender'               => $this->loginData->staffid,
	         'receiver'             => $receiver,
	         'forwarded_workflowid' => $forwardworkflowid,
	         'senddate'             => date("Y-m-d H:i:s"),
	         'flag'                 => 13,
	         'scomments'            => $comments,
	         'createdon'            => date("Y-m-d H:i:s"),
	         'createdby'            => $this->loginData->staffid,
	        );
	        /*echo "<pre>";
			print_r($insertworkflowArr); die;*/
	        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

			/*$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	           'flag'               => 0,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );*/
      
          /*$this->db->insert('tbl_joining_report_new_place', $insertArr);
          $insertid = $this->db->insert_id();*/

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          //redirect('/Joining_report_of_newplace_posting/edit/'.$insertid);
          redirect('/Staff_approval_process');
          }
		}



		// if ($this->input->post('comments')) {


			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'reported_for_duty_date'   => $dutyon,
	          'transid'           =>  $token,
	          'assigned'=> $this->input->post('assignedto'),
	          'location'=> $this->input->post('new_office_name'),
	          'supervision_staffid'=> $this->input->post('supervision_id'),
	          'flag'               => 1,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
	        /*echo "<pre>";
	        print_r($insertArr);die();*/
      
          $this->db->insert('tbl_joining_report_new_place', $insertArr);
          $insertid = $this->db->insert_id();

          	$updateArr = array(                         
	          'trans_flag'     => 15,
	          'updatedon'      => date("Y-m-d H:i:s"),
	          'updatedby'      => $this->loginData->staffid
	        );

	        $this->db->where('id',$token);
	        $this->db->update('staff_transaction', $updateArr);
	        
	        $insertworkflowArr = array(
	         'r_id'                 => $token,
	         'type'                 => 6,
	         'staffid'              => $content['getstaffdetailslist']->staffid,
	         'sender'               => $this->loginData->staffid,
	         'receiver'             => $content['getstaffdetailslist']->staffid,
	         'forwarded_workflowid' => $forwardworkflowid,
	         'senddate'             => date("Y-m-d H:i:s"),
	         'flag'                 => 15,
	         'scomments'            => $this->input->post('comments'),
	         'createdon'            => date("Y-m-d H:i:s"),
	         'createdby'            => $this->loginData->staffid,
	        );
	        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

	        $staffupdateArr = array(
	        	'designation'=>$content['getstaffdetailslist']->new_designation_id,
	        	'new_office_id'=>$content['getstaffdetailslist']->new_office_id,
	        	'doj_team'=>$content['getstaffdetailslist']->proposeddate,
	        	'reportingto'=>$content['getstaffdetailslist']->reportingto
	        );
	        $this->db->where('staffid', $content['getstaffdetailslist']->staffid);
	        $this->db->update('Staff',$staffupdateArr);

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
          	$subject = "Your Process Approved";
		      $body = 'Dear,<br><br>';
		      $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
		      $body .= 'This is to certify that Mr. '.$content['getstaffdetailslist']->name.'<br> your process has Approved  .<br><br>';
		      $body .= 'Thanks<br>';
		      $body .= 'Administrator<br>';
		      $body .= 'PRADAN<br><br>';

		      $body .= 'Disclaimer<br>';
		      $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

		      
		      $to_email = $receiverdetail->emailid;
		      $to_name = $receiverdetail->name;
		      //$to_cc = $getStaffReportingtodetails->emailid;
		      $recipients = array(
		         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
		         $content['getstaffdetailslist']->emailid => $content['getstaffdetailslist']->name
		         // ..
		      );
		      $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
		      if (substr($email_result, 0, 5) == "ERROR") {
		        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
		      }
            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/view/'.$insertid);

          }
		// }

	}

		
		//print_r($content['getstaffdetailslist']); die;

		$content['token'] = $token;
			
		$content['title'] = 'Joining_report_of_newplace_posting/add';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}



public function edit($token)
	{
		try{


		$this->load->model('Joining_report_of_newplace_posting_model');

	

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			// echo "sdfdsf";
			// print_r($this->input->post()); die;
			
			$savebtn = $this->input->post('savebtn');
			$submitbtn = $this->input->post('submitbtn');
		

			$staffdutytodaydate = $this->input->post('staff_duty_today_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);

			if (isset($savebtn) && $savebtn =='senddatasave') {

			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	           'flag'               => 0,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );

      	  $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);
         // $insertid = $this->db->insert_id();

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/edit/'.$token);

          }
		}



		if (isset($submitbtn) && $submitbtn =='senddatasubmit') {


			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	          'flag'               => 1,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
      
         $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);

          $insertArr = array(
	           'trans_flag'      => 10,
	        );
      	
      	  $this->db->where('id', $token);
          $this->db->update('staff_transaction', $insertArr);


           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/view/'.$token);

          }
		}

	}

		$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);

		$content['token'] = $token;
			
		$content['title'] = 'Joining_report_of_newplace_posting/edit';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
  }




  public function view($reportid)
	{
		try{
		$query = "SELECT transid, reported_for_duty_date, createdon, location FROM tbl_joining_report_new_place WHERE id=".$reportid;
		$content['queryresult'] = $this->db->query($query)->row();
		$token = $content['queryresult']->transid;

		$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->result()[0];
	    // print_r($result);
	    // die;
	    $content['workflow_flag']=$result;

		$this->load->model('Joining_report_of_newplace_posting_model');
     	$RequestMethod = $this->input->server('REQUEST_METHOD'); 

     	$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		$content['getstaffsuperiordetailslist'] = $this->model->getstaffSuperior($token);
		
		
		$content['getstaffjoiningreport'] = $this->model->getstaffjoiningreport($token);
		//print_r($getstaffjoiningreport); die;
		/*echo "<pre>";
		print_r($content['getstaffjoiningreport']);exit;*/
		$content['token'] = $token;


			
		$content['title'] = 'Joining_report_of_newplace_posting/view';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
  }
	public function add_transfer($token)
	{
		try{

		$this->load->model('Joining_report_of_newplace_posting_model');

		$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $token";
	    $result  = $this->db->query($sql)->result()[0];
	    // print_r($result);
	    // die;
	    $content['workflow_flag']=$result;
	    $forwardworkflowid = $result->workflowid;
		$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		// echo "<pre>";
		//  print_r($content['getstaffdetailslist']);exit();
		$content['getstaffsuperiordetailslist'] = $this->model->getstaffSuperior($token);
		$new_tc=$content['getstaffsuperiordetailslist']->reportingto;
		//print_r($content['getstaffsuperiordetailslist']);
		//die;
		$personnel_detail = $this->Staff_seperation_model->getpersonneldetail();
		 //print_r($personnel_detail);
		// die;
			// echo $content['getstaffdetailslist']->staffid;
			// die;
		$receiverdetail = $this->Staff_seperation_model->get_staffDetails($content['getstaffdetailslist']->staffid);
		
		$content['staff_data']=$receiverdetail;

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			
			
			$savebtn = $this->input->post('savebtn');
			$submitbtn = $this->input->post('submitbtn');

			$staffdutytodaydate = $this->input->post('staff_duty_today_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);
			$dutyon = $this->gmodel->changedatedbformate($this->input->post('dutyon'));

			if (isset($savebtn) && $savebtn =='senddatasave') {

			$this->db->trans_start();
			$newtcquery = "SELECT * FROM staff_transaction WHERE trans_status='Transfer' AND staffid=".$content['getstaffdetailslist']->staffid." AND id=".$token;
	    	$newtc = $this->db->query($newtcquery)->row();
	    	$receiver = $newtc->reportingto;
	    	$comments = 'Joining Report filled By Staff';
			$updateArr = array(                         
	          'trans_flag'     => 13,
	          'updatedon'      => date("Y-m-d H:i:s"),
	          'updatedby'      => $this->loginData->staffid
	        );

	        $this->db->where('id',$token);
	        $this->db->update('staff_transaction', $updateArr);

	        $insertworkflowArr = array(
	         'r_id'                 => $token,
	         'type'                 => 2,
	         'staffid'              => $content['getstaffdetailslist']->staffid,
	         'sender'               => $this->loginData->staffid,
	         'receiver'             => $receiver,
	         'forwarded_workflowid' => $forwardworkflowid,
	         'senddate'             => date("Y-m-d H:i:s"),
	         'flag'                 => 13,
	         'scomments'            => $comments,
	         'createdon'            => date("Y-m-d H:i:s"),
	         'createdby'            => $this->loginData->staffid,
	        );
	        /*echo "<pre>";
			print_r($insertworkflowArr); die;*/
	        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			/*$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	           'flag'               => 0,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
      
          $this->db->insert('tbl_joining_report_new_place', $insertArr);
          $insertid = $this->db->insert_id();*/

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          // redirect('/Joining_report_of_newplace_posting/add_transfer/'.$token);
          redirect('/Staff_approval_process');
          }
		}



		if ($this->input->post('comments')) {


			$this->db->trans_start();
			  $s_id=$content['getstaffdetailslist']->staffid;
			   $staff_updatearray=array(

			  'reportingto'=>$new_tc

			  );
			   $this->db->where('staffid',$s_id);
     $this->db->update('staff', $staff_updatearray);
				 // echo $this->db->last_query();
				 // die;

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'reported_for_duty_date'   => $dutyon,
	          'transid'           =>  $token,
	          'assigned'=> $this->input->post('assignedto'),
	          'location'=> $this->input->post('new_office_name'),
	          'supervision_staffid'=> $this->input->post('supervision_id'),
	          'flag'               => 1,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
	        /*echo "<pre>";
	        print_r($insertArr);die();*/
      
          $this->db->insert('tbl_joining_report_new_place', $insertArr);
          $insertid = $this->db->insert_id();

          	$updateArr = array(                         
	          'trans_flag'     => 15,
	          'updatedon'      => date("Y-m-d H:i:s"),
	          'updatedby'      => $this->loginData->staffid
	        );

	        $this->db->where('id',$token);
	        $this->db->update('staff_transaction', $updateArr);
	        
	        $insertworkflowArr = array(
	         'r_id'                 => $token,
	         'type'                 => 2,
	         'staffid'              => $content['getstaffdetailslist']->staffid,
	         'sender'               => $this->loginData->staffid,
	         'receiver'             => $content['getstaffdetailslist']->staffid,
	         'forwarded_workflowid' => $forwardworkflowid,
	         'senddate'             => date("Y-m-d H:i:s"),
	         'flag'                 => 15,
	         'scomments'            => $this->input->post('comments'),
	         'createdon'            => date("Y-m-d H:i:s"),
	         'createdby'            => $this->loginData->staffid,
	        );
	        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
          	  $subject = "Your Process Approved";
		      $body = 'Dear,<br><br>';
		      $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
		      $body .= 'This is to certify that Mr. '.$content['getstaffdetailslist']->name.'<br> your process has Approved  .<br><br>';
		      $body .= 'Thanks<br>';
		      $body .= 'Administrator<br>';
		      $body .= 'PRADAN<br><br>';

		      $body .= 'Disclaimer<br>';
		      $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

		      
		      $to_email = $receiverdetail->emailid;
		      $to_name = $receiverdetail->name;
		      //$to_cc = $getStaffReportingtodetails->emailid;
		      $recipients = array(
		         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
		         $content['getstaffdetailslist']->emailid => $content['getstaffdetailslist']->name
		         // ..
		      );
		      $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
		      if (substr($email_result, 0, 5) == "ERROR") {
		        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
		      }
            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/view_transfer/'.$insertid);

          }
		}

	}

		/*echo "<pre>";
		print_r($content); die;*/

		$content['token'] = $token;
			
		$content['title'] = 'Joining_report_of_newplace_posting/add_transfer';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}



public function edit_transfer($token)
	{
		try{

		$this->load->model('Joining_report_of_newplace_posting_model');

	

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			// echo "sdfdsf";
			// print_r($this->input->post()); die;
			
			$savebtn = $this->input->post('savebtn');
			$submitbtn = $this->input->post('submitbtn');
		

			$staffdutytodaydate = $this->input->post('staff_duty_today_date');

			$staff_duty_today_date = $this->gmodel->changedatedbformate($staffdutytodaydate);

			if (isset($savebtn) && $savebtn =='senddatasave') {

			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	           'flag'               => 0,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );

      	  $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);
         // $insertid = $this->db->insert_id();

           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/edit_transfer/'.$token);

          }
		}



		if (isset($submitbtn) && $submitbtn =='senddatasubmit') {


			$this->db->trans_start();

			$insertArr = array(
	          'staff_duty_today_date'      => $staff_duty_today_date,
	          'transid'           =>  $token,
	          'flag'               => 1,
	          'createdon'         => date('Y-m-d H:i:s'),
	          'createdby'         => $this->loginData->staffid,
	        );
      
         $this->db->where('id', $token);
          $this->db->update('tbl_joining_report_new_place', $insertArr);

          $insertArr = array(
	           'trans_flag'      => 10,
	        );
      	
      	  $this->db->where('id', $token);
          $this->db->update('staff_transaction', $insertArr);


           $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Joining report has not been done successfully'); 
            redirect(current_url());

          }else{

          $this->session->set_flashdata('tr_msg', 'Joining report has been done successfully !!!');
          redirect('/Joining_report_of_newplace_posting/view_transfer/'.$token);

          }
		}

	}

		$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);

		$content['token'] = $token;
			
		$content['title'] = 'Joining_report_of_newplace_posting/edit_transfer';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
  }




  public function view_transfer($token)
	{
		try{

		$query = "SELECT transid, reported_for_duty_date, createdon, location FROM tbl_joining_report_new_place WHERE id=".$token;
		$content['queryresult'] = $this->db->query($query)->row();
		//print_r($content['queryresult']);

		$trans_id = $content['queryresult']->transid;
			$sql = "SELECT max(workflowid) as workflowid,max(flag) as flag FROM tbl_workflowdetail WHERE r_id = $trans_id";
	    $result  = $this->db->query($sql)->result()[0];
	    // print_r($result);
	    // die;
	    $content['workflow_flag']=$result;
	    // print_r($content['workflow_flag']);
	    // die;
		
		$this->load->model('Joining_report_of_newplace_posting_model');
     	$RequestMethod = $this->input->server('REQUEST_METHOD'); 
     	$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($trans_id);
     	// print_r($content['getstaffdetailslist']);
     	// die;
		$content['getstaffsuperiordetailslist'] = $this->model->getstaffSuperior($trans_id);

		// print_r($content['getstaffsuperiordetailslist']);

		
		
		$content['getstaffjoiningreport'] = $this->model->getstaffjoiningreport($token);
		// //print_r($getstaffjoiningreport); die;
		// echo "<pre>";
		// print_r($content['getstaffjoiningreport']);exit;*/
		// //$content['getstaffdetailslist'] = $this->model->getstaffdetailslist($token);
		$content['token'] = $token;


			
		$content['title'] = 'Joining_report_of_newplace_posting/view_transfer';

		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
  }




}