<?php 
class payrollslab extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model('payrollslab_model','model');
		$this->load->model('Global_Model','gmodel');

		
		
		$check = $this->session->userdata('login_data');

			
		if (empty($check)) {
     		 redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()	
	{
		try{
		//die("hello");
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		$content['getsalaryhead'] = $this->model->getsalaryhead();
		$content['getsalaryheaddetail'] = $this->model->getsalaryheaddetaiil();
		$content['fyear'] = $this->gmodel->getcurrentfyear(); 
		/*echo "<pre>";
		print_r($content['role_permission']);
		die;*/
		
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		 	if($RequestMethod == 'POST'){
		 		

		 		
		 		$salary_id=$this->input->post('salary_id');
		 		$salary_head=$this->input->post('ddlhead');
		 		$financialyear=$this->input->post('financialyear');
		 		$salary_lowerlimit=$this->input->post('lower');
		 		$salary_upperlimit=$this->input->post('upper');
		 		$amount=$this->input->post('amount');
		 		$salaryarray=array(
		 				'fieldname'=>$salary_head,
		 				'financial_year'=>$financialyear,
		 				'lowerlimit'=>$salary_lowerlimit,
		 				'upperlimit'=>$salary_upperlimit,
		 				'amount'=>$amount,
		 				'active'=>0

		 			);
		 		if($salary_id)
		 		{

		 			$this->db->where('id', $salary_id);
        			$this->db->update('payrollslab', $salaryarray);
        			// echo $this->db->last_query();
        			$this->session->set_flashdata('tr_msg', 'Successfully update Salary detail');
        			redirect(current_url());
		 			
		 		}
		 		else
		 		{
		 			
		 			$this->db->insert('payrollslab',$salaryarray);
		 			$this->session->set_flashdata('tr_msg', 'Successfully Added Salary detail');
		 			redirect(current_url());
		 		}
		 		
		 		

		 		
		 		


		 	}
		 		


		$content['title'] = 'payrollslab';
		$content['subview'] ='payrollslab/index';
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	public function delete($token)	
	{
		try{
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		$content['getsalaryhead'] = $this->model->getsalaryhead();
		$content['getsalaryheaddetail'] = $this->model->getsalaryheaddetaiil();
		
		
		
		$this->db->where('id', $token);
		$this->db->delete('payrollslab');
		//echo $this->db->last_query();
		//die();
		$this->session->set_flashdata('tr_msg' ,"record Deleted Successfully");
		redirect('/payrollslab/index/');
		
		 		

		 		
		 		
		 		
		 		

		$content['title'] = 'payrollslab';
		$content['subview'] ='payrollslab/index';
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




}
