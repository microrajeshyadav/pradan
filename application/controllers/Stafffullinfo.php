<?php 

/**
* Staff Full Information controller
*/
class Stafffullinfo extends CI_Controller
{
	
	function __construct()
	{

		parent::__construct();


		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model("Stafffullinfo_model");
		
		$this->load->model("Staff_list_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model', 'model');	
		$check = $this->session->userdata('login_data');
		
		//print_r($check);
		//die;
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	//	print_r($this->loginData);
		

	}

	public function delete_record($id)
	{

		try{
			$staff = $this->uri->segment(4);
				//$candidate_id  = $this->uri->segment(4);

			$staff_id=$this->loginData->staffid;
				//$candidateid=$this->loginData->candidateid;



			if(isset($staff_id))
			{
				$staff_id=$staff_id;

					//echo "staff=".$staff_id;
					//$candidateid=$candidateid;
					//echo "candidateid=".$candidateid;

			}
			else
			{
				$staff_id=$staff;
				//echo "staff".$staff_id;
        // $candidateid=$this->loginData->candidateid;//$candidate_id;
					//$candidateid=$candidate_id;
					//echo "candidateid".$candidateid;

			}
				//die();

			$content = $this->model->delete_familyrecord($id);
			//echo $content;
			if($content==1)
			{
				$this->session->set_flashdata('tr_msg', 'Successfully Adding Staff Profile Redgistration');

				redirect('Stafffullinfo/edit/'.$staff_id);
			}
		}

		catch (Exception $e) {
			print_r($e->getMessage());die;
		}


		
	}

	public function view($staff_id)
	{

		if (empty($staff_id) || $staff_id == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $staff_id is either blank or empty.');
				
				
			} else { 

		
		try{
			// $staff_id = $this->uri->segment(3);
			

			//$staff_id=$this->loginData->staffid;
			



			// if(isset($staff_id))
			// {
			// 	$staff_id=$staff_id;
				
			// }
			// else
			// {
			// 	$staff_id=$staff;
				
				
			// }
			// echo $staff_id; die();
			$content['candidatedetails']= $this->model->getCandidateDetailsPreview($staff_id);
			/*echo "<pre>";
			print_r($content['candidatedetails']); die();*/
			if(!empty($content['candidatedetails']))
				 $tc=$content['candidatedetails']->reportingto;

				 if (empty($tc) || $tc == ''|| $tc==NULL) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				 	//echo "hello";
					}
					else
					{
				$content['select_reporting']= $this->model->selectedsuperviser($tc);
			
				}
		
			$content['office_details'] = $this->model->getoffice_details($staff_id);
			//print_r($content['office_details']);
								//echo $staff_id;die();


			$content['count_education']=$this->Stafffullinfo_model->countStaffEducationDetails($staff_id);
			$content['education_details'] = $this->model->fetcheducationdata($staff_id);
			/*echo "<pre>";
			print_r($content['education_details']);
			die();*/
						
			$fcount = $this->model->getCountfamilty($staff_id);
			$content['familycount']= $fcount;  
			//die("hdghd");
			//print_r($content['familycount']);die();
			$content['familymemberdetails']    = $this->model->getfamilydata($staff_id);
			//print_r($content['familymemberdetails']);die();
			$content['countdependmember'] = $this->model->getcount_dependmember($staff_id);


			$content['dependmember'] = $this->model->getdependmember($staff_id);

			$TEcount = $this->model->getCountTrainingExposure($staff_id);

			$content['TrainingExpcount']= $TEcount;  

			$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($staff_id);
			$WEcount = $this->model->getCountWorkExprience($staff_id);

			$content['WorkExperience']= $WEcount;  

			$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($staff_id);
								  //print_r($content['workexperiencedetails']);die();




			$Icount = $this->model->getCountIdnumber($staff_id);

			$content['identitycount']= $Icount;  
								 

			$content['identitydetals'] = $this->model->getcandidateiddata($staff_id);
								 //print_r($content['identitydetals']);die();






			$GPYcount = $this->model->fetchgapyearcount($staff_id);

			$content['GapYearCount']= $GPYcount;  


			$content['gapyeardetals'] = $this->model->fetchgapyeardata($staff_id);
								// print_r($content['gapyeardetals']);die();



			$Lcount = $this->model->languagecount($staff_id);

			$content['languageproficiency']= $Lcount;  
								 //print_r($content['languageproficiency']);
								 //die();
			$content['languagedetals'] = $this->model->getlanguageDetails($staff_id);
								//print_r($content['languagedetals']);die();


			$content['languagedetalsprint'] = $this->model->get($staff_id);
							
			$content['wirkflowid'] = $this->model->getworkflowid($staff_id);
		
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			if($RequestMethod == 'POST'){

				//print_r($this->input->post()); die;

				$Submit=$this->input->post('SubmitDatasend');

				if(!empty($Submit) && $Submit =='SubmitData')
				{
					$status=$this->input->post('status');


					$this->db->trans_start();
					
					if ($status==3) {

						$insertworkdetailarr=array(
						'type'         => 1,
						'r_id'         => $staff_id,
						'sender'       => $this->loginData->staffid,
						'receiver'     => $staff_id,
						'scomments'    => $this->input->post('comments'),
						'senddate'     => date('Y-m-d H:i:s'),
						'readdate'     => date('Y-m-d H:i:s'),
						'createdon'    => date('Y-m-d H:i:s'),
						'createdby'    => $staff_id,
						'forwarded_workflowid' => $content['wirkflowid']->workflowid,
						'flag'                 => $status,
						'staffid'	           => $staff_id,
					);
					$this->db->insert('tbl_workflowdetail',$insertworkdetailarr);

					$updatestaffarr=array(
						'flag'         => $status,
						'updatedon'    => date('Y-m-d H:i:s'),
						'updatedby'    => $staff_id
					);
					$this->db->where('staffid', $staff_id);
					$this->db->update('staff',$updatestaffarr);

					}else if ($status== 4) {

						

						$insertworkdetailarr=array(
						'type'         => 1,
						'r_id'         => $content['candidatedetails']->staffid,
						'sender'       => $this->loginData->staffid,
						'receiver'     => $staff_id,
						'scomments'    => $this->input->post('comments'),
						'senddate'     => date('Y-m-d H:i:s'),
						'readdate'     => date('Y-m-d H:i:s'),
						'createdon'    => date('Y-m-d H:i:s'),
						'createdby'    => $staff_id,
						'forwarded_workflowid'=> $content['wirkflowid']->workflowid,
						'flag'         => $status,
						'staffid'	   => $staff_id,
						);
						$this->db->insert('tbl_workflowdetail',$insertworkdetailarr);
						//echo $this->db->last_query(); die;

					$updatestaffarr=array(
						'flag'         => $status,
						'updatedon'    => date('Y-m-d H:i:s'),
						'updatedby'    => $staff_id
					);
					$this->db->where('staffid', $staff_id);
					$this->db->update('staff',$updatestaffarr);
		


					}



					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error !!! Approval is not successfully');	
						//redirect('/Stafffullinfo/view/'.$staff_id);
					}else{
							$this->session->set_flashdata('tr_msg', 'Approval is  Successfully');	
							redirect('Stafffullinfo/view/'.$staff_id);
					}
		
				}else if(!empty($Submit) && $Submit =='SubmitDataDocument'){

					if (isset($_FILES['ugmarkssheet']['name'])) {
						if($_FILES['ugmarkssheet']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['ugmarkssheet']['name'])));

							$ugmarkssheetoriginalName = $_FILES['ugmarkssheet']['name']; 

							$ugmarkssheetencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['ugmarkssheet']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugmarkssheetencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$ugmarkssheetencryptedName = $ugmarkssheetencryptedName;
							}else{

								// die("Error uploading ug marks sheet Document ");
								$this->session->set_flashdata("er_msg", "Error uploading ug marks sheet Document");
								redirect(current_url());
							}
						}else{
							$ugmarkssheetoriginalName = '';
							$ugmarkssheetencryptedName = '';
						}
					}else{
						$ugmarkssheetoriginalName = '';
						$ugmarkssheetencryptedName = '';
					}

					if (isset($_FILES['pgmarkssheet']['name'])) {
						if($_FILES['pgmarkssheet']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['pgmarkssheet']['name'])));

							$pgmarkssheetoriginalName = $_FILES['pgmarkssheet']['name']; 

							$pgmarkssheetencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['pgmarkssheet']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgmarkssheetencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$pgmarkssheetencryptedName = $pgmarkssheetencryptedName;
							}else{

								// die("Error uploading ug marks sheet Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg marks sheet Document");
								redirect(current_url());
							}
						}else{
							$pgmarkssheetoriginalName = '';
							$pgmarkssheetencryptedName = '';
						}

					}
					else{
						$pgmarkssheetoriginalName = '';
						$pgmarkssheetencryptedName = '';
					}

					if (isset($_FILES['ugcertificate']['name'])) {
						if($_FILES['ugcertificate']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['ugcertificate']['name'])));

							$ugcertificateoriginalName = $_FILES['ugcertificate']['name']; 

							$ugcertificateencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['ugcertificate']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $ugcertificateencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$ugcertificateencryptedName = $ugcertificateencryptedName;
							}else{

								// die("Error uploading ug marks sheet Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg marks sheet Document");
								redirect(current_url());
							}
						}else{
							$ugcertificateoriginalName = '';
							$ugcertificateencryptedName = '';
						}

					}
					else{
						$ugcertificateoriginalName = '';
						$ugcertificateencryptedName = '';
					}

					if (isset($_FILES['pgcertificate']['name'])) {
						if($_FILES['pgcertificate']['name'] != NULL){
							@  $ext = end((explode(".", $_FILES['pgcertificate']['name'])));

							$pgcertificateoriginalName = $_FILES['pgcertificate']['name']; 

							$pgcertificateencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "_migration." . $ext;

							$tempFile = $_FILES['pgcertificate']['tmp_name'];
							$targetPath = FCPATH . "datafiles/educationalcertificate/";
							$targetFile = $targetPath . $pgcertificateencryptedName;
							//echo $targetFile;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);
							

							if($uploadResult == true){
								$pgcertificateencryptedName = $pgcertificateencryptedName;
							}else{

								// die("Error uploading ug marks sheet Document ");
								$this->session->set_flashdata("er_msg", "Error uploading pg marks sheet Document");
								redirect(current_url());
							}
						}else{
							$pgcertificateoriginalName = '';
							$pgcertificateencryptedName = '';
						}

					}
					else{
						$pgcertificateoriginalName = '';
						$pgcertificateencryptedName = '';
					}

					$udateCertificateArr = array(
						'encryptugcertificate'   => $ugmarkssheetencryptedName,
						'encryptpgcertificate'   => $pgmarkssheetencryptedName,
						'originalugcertificate'  => $ugmarkssheetoriginalName,
						'originalpgcertificate'  => $pgmarkssheetoriginalName,
						'ugdocencryptedImage'    => $ugcertificateencryptedName,
						'pgdocencryptedImage'    => $pgcertificateencryptedName,
						'ugdocoriginalName'      => $ugcertificateoriginalName,
						'pgdocoriginalName'      => $pgcertificateoriginalName
					);

					$this->db->where('candidateid', $content['candidatedetails']->candidateid);
					$this->db->update('tbl_candidate_registration', $udateCertificateArr);

				}

			}
			//  print_r($content['otherinformationdetails']);





			$content['title'] = 'Stafffullinfo';

			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;

			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}
	}



	public function preview($token)
	{

		try{


			$RequestMethod = $this->input->server('REQUEST_METHOD');
			if($RequestMethod == 'POST'){

				$candidateid = $this->input->post('candidateid');

				$updateStatusInfo	 = array(
					'BDFFormStatus' => '1',
				);

				$this->db->where('candidateid', $candidateid);
				$this->db->update('tbl_candidate_registration', $updateStatusInfo);
				$this->session->set_flashdata('tr_msg', 'Successfully Added Staff Details ');
				redirect('Stafffullinfo/bdfformsubmit/');
		 } //// End Post




		 $content['token']= $token;  
		 $content['candidatedetails'] = $this->model->getCandidateDetailsPreview($token);
					     //print_r($candidatedetails);

		 $fcount = $this->model->getCountFamilyMember($token);

		 $dcount['mem']=$this->model->getcount_dependmember($token);
				//echo "depemt count=".$dcount;
				//print_r($dcount['mem']);
		 $content['familycount']= $fcount;  

		 $content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetailsPrint($token);

			 // $content['candidatedetails']         = $this->model->getCandidateDetailsPrint($this->loginData->candidateid);

		 $Icount = $this->model->getCountIdentityNumber($token);

		 $content['identitycount']= $Icount;  

		 $content['identitydetals'] = $this->model->getCandidateIdentityDetailsPrint($token);


		 $TEcount = $this->model->getCountTrainingExposure($token);

		 $content['TrainingExpcount']= $TEcount;  
			 //print_r($content['TrainingExpcount']);

		 $content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($token);
			 //print_r($content['trainingexposuredetals']);



		 $GPYcount = $this->model->getCountGapYear($token);

		 $content['GapYearCount']= $GPYcount;  

		 $content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($token);



		 @ $Lcount = $this->model->getCountLanguage($token);

		 @ $content['languageproficiency']= $Lcount;  

		 @ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($token);
		 $content['languagedetalsprint'] = $this->model->getCandidateLanguageDetailsPrint($token);
		 @  $content['otherinformationdetails'] = $this->model->getCandidateOtherInformationDetails($token);


		 $WEcount = $this->model->getCountWorkExprience($token);

		 $content['WorkExperience']= $WEcount;  

		 $content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($token);

		 $content['title'] = 'Stafffullinfo';
		 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		 $this->load->view('_main_layout', $content);


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




	public function edit($staff_id)
	{
		
		try{

		if(empty($staff_id) || $staff_id == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $staff_id is either blank or empty.');
			
				
			} 

			else {

		

			

				$content['candidatedetails']= $this->model->getCandidateDetailsPreview($staff_id);
				$office_id=$content['candidatedetails']->new_office_id;
				
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			if($RequestMethod == 'POST'){
				
				if(!empty($content['candidatedetails']->reportingto))
					{
					$supervisor=$this->input->post("supervisorname");
					}
					else
					{
						$supervisor=$this->input->post("new_supervisorname");
					}

				$save=$this->input->post('SaveDatasend');

				$Submit=$this->input->post('SubmitDatasend');

				if(!empty($save) && $save =='SaveData')
				{					
		 		 	
					

		 			
					$dateofbirth1 = $this->input->post('dateofbirth');
					$date_of_birth = $this->model->changedatedbformate($dateofbirth1);

					$candidateemailid   = $this->input->post("emailid");
					$candidatefirstname = $this->input->post('candidatefirstname');

					$this->db->trans_start();
					//	$this->db->trans_strict(FALSE);
			$this->form_validation->set_rules('candidatefirstname','Candidate First Name','trim|required');
	         $this->form_validation->set_rules('candidatefirstname','Candidate Last Name','trim|required');
	         $this->form_validation->set_rules('emailid','Emailid','trim|required|valid_email');
	         $this->form_validation->set_rules('motherfirstname','Mother First Name','trim|required');
	    	 $this->form_validation->set_rules('motherlastname','Mother Last Name','trim|required');
	    	 $this->form_validation->set_rules('fatherfirstname','Father First Name','trim|required');
	    	 $this->form_validation->set_rules('fatherlastname','Father First Name','trim|required');
	    	 $this->form_validation->set_rules('dateofbirth','Date Of Birth','trim|required');
	    	// $this->form_validation->set_rules('mobile','Mobile','trim|required|max_length[10]|numeric');
	    	 $this->form_validation->set_rules('presenthno','Present House Number','trim|required');
	    	$this->form_validation->set_rules('permanenthno','Permanent House Number','trim|required');
	    	$this->form_validation->set_rules('presentstreet','Present Street','trim|required');
	    	$this->form_validation->set_rules('presentcity','Present city','trim|required');
	    	$this->form_validation->set_rules('presentstateid','Present State','trim|required');
	    	$this->form_validation->set_rules('presentdistrict','Present District','trim|required');
	    	$this->form_validation->set_rules('presentpincode','Present Pincode','trim|required|max_length[6]|numeric');
	    	$this->form_validation->set_rules('permanentstreet','Permanent Street','trim|required');
	    	$this->form_validation->set_rules('presentcity','Present City','trim|required');
	    	$this->form_validation->set_rules('permanentcity','Permanent City','trim|required');
	    	$this->form_validation->set_rules('permanentstateid','Permanent State','trim|required');
	    	$this->form_validation->set_rules('permanentdistrict','Permanent District','trim|required');
	    	$this->form_validation->set_rules('permanentpincode','Permanent Pincode','trim|required|max_length[6]|numeric');
	    	if($this->form_validation->run() == FALSE){
		    		$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
		    			'</div>');

		    		$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

		    		$hasValidationErrors    =    true;
		    		goto prepareview;

		    	}


	    	
					$staff_name=$this->input->post('candidatefirstname')." ".$this->input->post("candidatemiddlename")." ".$this->input->post('candidatelastname');
					$father_name=$this->input->post('fatherfirstname')." ".$this->input->post("fathermiddlename")." ".$this->input->post('fatherlastname');
					$mother_name=$this->input->post('motherfirstname')." ".$this->input->post("mothermiddlename")." ".$this->input->post('motherlastname');

					$status=$this->input->post('status');
					 
					$where = $this->input->post("where");
					if(!empty($where)){
						$valwhere = $where;
					}else{
						$valwhere = NULL;
					}

					$whendate       = $this->input->post('when');	
					$dbformatwhendate    =$this->model->changedatedbformate($whendate);
					// echo $_FILES['photoupload']['name'];
					// 	die;

				if ($_FILES['photoupload']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 


						$OriginalName = $_FILES['photoupload']['name']; 
						$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['photoupload']['tmp_name'];
						$targetPath = FCPATH . "datafiles/";
						$targetFile = $targetPath . $encryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedImage = $encryptedName;

						}else{

							die("Error uploading Profile Photo ");
							$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
							redirect(current_url());
						}

					}else{

						$encryptedImage = $this->input->post('oldphotoupload');

					}


					// echo $_FILES['signature_upload']['name'];
					// die;

					if ($_FILES['signature_upload']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['signature_upload']['name']))); 


						$original_sagnature = $_FILES['signature_upload']['name']; 
						$encrypted_signature = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['signature_upload']['tmp_name'];
						$targetPath = FCPATH . "datafiles/signature/";
						$targetFile = $targetPath . $encrypted_signature;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encrypted_signature = $encrypted_signature;
							$original_sagnature=$original_sagnature;

						}else{

							die("Error uploading Profile Photo ");
							$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
							redirect(current_url());
						}

					}else{

						$encryptedImage = $this->input->post('old_signature_upload');

					}

				
					
					
					$insertArrCandidate = array(
						'name'              => $staff_name,
						'father_name'       => $father_name,
						'mother_name'  	    => $mother_name,

						'gender'    		        => $this->input->post('gender'),
						'nationality'       	    => $this->input->post("nationality"),
						'marital_status'  	        => $this->input->post('maritalstatus'),
						'dob '    	        => $date_of_birth,
						'emailid'       	        => $this->input->post("emailid"),
						'contact'  		            => $this->input->post('mobile'),
					'originalphotoname'=>$_FILES['photoupload']['name'],
					'encryptedphotoname'=>$encryptedImage,


						'presentstreet'    	    => $this->input->post('presentstreet'),
						'presentcity'       	=> $this->input->post("presentcity"),
						'presentstateid'  	    => $this->input->post('presentstateid'),
						'presentdistrict '    	=> $this->input->post('presentdistrict'),
						'presentpincode'       	=> $this->input->post("presentpincode"),
						'permanentstreet'    	=> $this->input->post('permanentstreet'),
						'permanentcity'       	=> $this->input->post("permanentcity"),
						'permanentstateid'  	=> $this->input->post('permanentstateid'),
						'permanentdistrict '    => $this->input->post('permanentdistrict'),
						'permanentpincode'      => $this->input->post("permanentpincode"),
						'presenthno '           => $this->input->post('presenthno'),
						'permanenthno'          => $this->input->post("permanenthno"),
						'flag'                  => 0,
						'any_subject_of_interest'        => trim($this->input->post('subjectinterest')),
						'any_achievementa_awards'         => trim($this->input->post("achievementawards")),
						'any_assignment_of_special_interest'     => trim($this->input->post("any_assignment_of_special_interest")),
						'experience_of_group_social_activities' => trim($this->input->post("experience_of_group_social_activities")),
						'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("pradan_selection_process_before"),
						'have_you_taken_part_in_pradan_selection_process_before_when'=> $dbformatwhendate,
						'have_you_taken_part_in_pradan_selection_process_before_where'=> $valwhere,
						'annual_income'          => $this->input->post("annual_income"),
						'male_sibling'                    => $this->input->post("no_of_male_sibling"),
						'female_sibling'                   => $this->input->post("no_of_female_sibling"),
						'know_about_pradan'                => $this->input->post("have_you_come_to_know"),
						'know_about_pradan_other_specify'  => $this->input->post("specify"),
						'bloodgroup'                       =>$this->input->post('bloodgroup'),
						'reportingto'=>$supervisor,
						'original_sagnature'=>$original_sagnature,
						'encrypted_signature'=>$encrypted_signature

					

					);


					$this->db->where('staffid',$staff_id);
					$this->db->update('staff', $insertArrCandidate);
					// echo $this->db->last_query();
					// die;


					



					$countidentityname = count($this->input->post('identityname'));

					$isAadhar= false;
					$isPan = false;
					for ($i=0; $i < $countidentityname; $i++) { 
						$identityname1=$this->input->post('identityname')[$i];
						if ($identityname1 ==1) {
							$isAadhar= true;
				

						}elseif ($identityname1 ==2) {
							$isPan = true;
			
						}
					}
					
					
					if ($isAadhar == false || $isPan == false) {

						$this->session->set_flashdata("er_msg","PAN Card and Aadhar Card must be mandatory");

						redirect(current_url());
					}


					$this->db->delete('msstaff_identity_details',array('staffid'=> $staff_id));


					for ($i=0; $i < $countidentityname; $i++) { 

						if ($_FILES['identityphoto']['name'][$i] != NULL) {
							@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


							$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
							$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/identitydocuments/";
							$targetFile = $targetPath . $encryptedIdentityName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedIdentitydocument = $encryptedIdentityName;
							}else{

								die("Error uploading Identity Document !");
								$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
								redirect(current_url());
							}

						}else{
							$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
							$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
						}


						$insertIdentityDetails	 = array(
							//'id'=>$id,
							'staffid'         =>$staff_id,
							'identityname'        =>trim($this->input->post('identityname')[$i]),
							'identitynumber'      =>trim($this->input->post('identitynumber')[$i]),
							'encryptedphotoname'  => $encryptedIdentitydocument,
							'originalphotoname'   => $OriginalIdentityName,
							'createdon'           => date('Y-m-d H:i:s'),
						    'createdby'           => $this->loginData->UserID, // 
						    'isdeleted'           => 0, 

						);
						
						$this->db->insert('msstaff_identity_details', $insertIdentityDetails);
					}

							


					if (!empty($this->input->post('education_level')[0])) {


						$countgapfromdate = count($this->input->post('education_level'));

						$this->db->delete('msstaffeducation_details',array('staffid'=>$staff_id));

						for ($j=0; $j < $countgapfromdate; $j++) { 

							if ($_FILES['certificate']['name'][$j] != NULL) {	

								//echo $_FILES['certificate']['name'][$j];

								@  $ext = end((explode(".", $_FILES['certificate']['name'][$j]))); 

								$matricoriginalName = $_FILES['certificate']['name'][$j]; 
								$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['certificate']['tmp_name'][$j];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $matricencryptedName;
								$uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){
									$matricencryptedImage = $matricencryptedName;

								}else{

									die("Error uploading certificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
									redirect(current_url());
								}

							}
							else{
								$matricoriginalName = $this->input->post('originalmatriccertificate')[$j];
								$matricencryptedImage = $this->input->post('oldmatriccertificate')[$j];
							}
						// 	echo "matricencryptedImage=".$matricencryptedImage;
						// //	die;




	$education_arr=array(
		'staffid'                  => $staff_id,
		'edulevel_cd'              => $this->input->post('education_level')[$j],
		'year_passing'             => $this->input->post('passingyear')[$j],
		'collg_name'               => $this->input->post('schoolcollegeinstitute')[$j],
		'university'               => $this->input->post('boarduniversity')[$j],
		'place'                    => $this->input->post('place')[$j],
		'stream'                   => $this->input->post('specialisation')[$j],
		'percentage'               => $this->input->post('percentage')[$j],
		'originalcertificate'      => $_FILES['certificate']['name'][$j],
		'encryptedcertificatename' => $matricencryptedImage[$j]
	);
	// echo "<pre>";print_r($education_arr);
	// die;
	$this->db->insert('msstaffeducation_details', $education_arr);
	//echo $this->db->last_query();

}
// echo "<pre>";
// print_r($education_arr);

//die;
}





					$countsyslanguage = count($this->input->post('syslanguage')); 

					$this->db->delete('msstaff_language_proficiency',array('staffid'=> $staff_id));

					for ($i=0; $i < $countsyslanguage; $i++) { 

						$insertLanguageProficiency = array(
							'staffid'  => $staff_id,
							'languageid'   => $this->input->post("syslanguage")[$i],
							'lang_speak'   => $this->input->post("speak")[$i],
							'lang_read'    => $this->input->post("read")[$i],
							'lang_write'   => $this->input->post("write")[$i],
							'createdon'    => date('Y-m-d H:i:s'),
							'isdeleted'    => 0, 
						);

						$this->db->insert('msstaff_language_proficiency', $insertLanguageProficiency);

					}

					if (!empty($this->input->post('gapfromdate')[0])) {

						$countgapfromdate = count($this->input->post('gapfromdate'));

						$this->db->delete('msstaff_gap_year',array('staffid'=> $staff_id));

						for ($j=0; $j < $countgapfromdate; $j++) { 
							$gapfromdate    = $this->input->post('gapfromdate')[$j];
							$gapfromdate1   = $this->model->changedatedbformate($gapfromdate);
							$gaptodate      = $this->input->post('gaptodate')[$j];
							$gaptodate1     = $this->model->changedatedbformate($gaptodate);

							$insertGapyear	 = array(
								'staffid'          => $staff_id,
								'fromdate'         => $gapfromdate1,
								'todate'           => $gaptodate1,
								'reason'           => trim($this->input->post('gapreason')[$j]),
								'createdon'        => date('Y-m-d H:i:s'),
								'isdeleted'        => 0, 
							);
							$this->db->insert('msstaff_gap_year', $insertGapyear);
						}
					}


					$count_orgname = count($this->input->post('orgname'));   

					$this->db->delete('msstaffexp_details',array('staffid'=> $staff_id));

					for ($i=0; $i < $count_orgname; $i++) { 

						if (!empty($this->input->post('orgname')[$i])) {

							if ($_FILES['experiencedocument']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['experiencedocument']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['experiencedocument']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encrypteddocument = $encryptedexpdocumentName;

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
								$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
							}


							if ($_FILES['salary_slip1']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['salary_slip1']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['salary_slip1']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['salary_slip1']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encryptedsalary_slip1 = $encryptedexpdocumentName;
									$Originalsalary_slip1=$_FILES['salary_slip1']['name'][$i];

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$Originalsalary_slip1 = $this->input->post('originalsalary_slip1')[$i];
								$encryptedsalary_slip1 =  $this->input->post('oldsalary_slip1')[$i];
							}


							if ($_FILES['salary_slip2']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['salary_slip2']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['salary_slip2']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['salary_slip2']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encryptedsalary_slip2 = $encryptedexpdocumentName;
									$Originalsalary_slip2=$_FILES['salary_slip2']['name'][$i];

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$Originalsalary_slip2 = $this->input->post('originalsalary_slip2')[$i];
								$encryptedsalary_slip2 =  $this->input->post('oldsalary_slip2')[$i];
							}


							if ($_FILES['salary_slip3']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['salary_slip3']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['salary_slip3']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['salary_slip3']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encryptedsalary_slip3 = $encryptedexpdocumentName;
									$Originalsalary_slip3=$_FILES['salary_slip3']['name'][$i];

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$Originalsalary_slip3 = $this->input->post('originalsalary_slip3')[$i];
								$encryptedsalary_slip3 =  $this->input->post('oldsalary_slip3')[$i];
							}


							
							$workfromdate     = $this->input->post('work_experience_fromdate')[$i];
							$workexpfromdate  = $this->model->changedatedbformate($workfromdate);
							$worktodate       = $this->input->post('work_experience_todate')[$i];	
							$workexptodate    =$this->model->changedatedbformate($worktodate);


							$insertWorkExperience1 = array(
								
								'staffid'      			=> $staff_id,
								'organization' 			=> trim($this->input->post('orgname')[$i]),
								'reason_leaving'		=> trim($this->input->post("descriptionofassignment")[$i]),
								'fromdate'              => $workexpfromdate,
								'todate'                => $workexptodate,
								'position'        		=> trim($this->input->post("palceofposting")[$i]),
								'encrypteddocumnetname'     =>$encrypteddocument,
								'originalexperiencedocument'=>$OriginalexperiencedocumentName,
								'encryptedsalaryslip1'      =>$encryptedsalary_slip1,
								'originalsalaryslip1'      =>$Originalsalary_slip1,
								'encryptedsalaryslip2'    =>$encryptedsalary_slip2,
								'originalsalaryslip2'=>$Originalsalary_slip2,
								'encryptedsalaryslip3'=>$encryptedsalary_slip3,
								'originalsalaryslip3'=>$Originalsalary_slip3,
								'designation'	        => trim($this->input->post("designation")[$i]),
								'lastsalary'       		=> trim($this->input->post("lastsalarydrawn")[$i]),
								'createdon'             => date('Y-m-d H:i:s'),
							);
							$this->db->insert('msstaffexp_details',$insertWorkExperience1);
					//echo $this->db->last_query();
					//die;
						}
					}




///////// Training Exposure Save Start Here ///////////// 
					
					$countnatureoftraining = count($this->input->post('natureoftraining')); 


					$this->db->delete('msstafftraining_programs_attend',array('staffid'=> $staff_id));

					for ($j=0; $j < $countnatureoftraining; $j++) { 

						if (!empty($this->input->post('natureoftraining')[$j])) {


							$fromdate1 = $this->input->post('fromdate')[$j];
							$fromdate  = $this->model->changedatedbformate($fromdate1); //die;
							$todate1   = $this->input->post('todate')[$j];	
							$todate    = $this->model->changedatedbformate($todate1);
							$id=$this->input->post('t_id')[$j];

							$insertTrainingExposure	 = array(
								'staffid'      => $staff_id,
								'Training_program' => trim($this->input->post('natureoftraining')[$j]),
								'organizing_agency'=> trim($this->input->post('organizingagency')[$j]),
								'fromdate'         => $fromdate,
								'todate'           => $todate,
								'createdon'        => date('Y-m-d H:i:s'),
								'isdeleted'        => 0, 
							);


							$this->db->insert('msstafftraining_programs_attend', $insertTrainingExposure);
						}
					}



					$countfamilymember = count($this->input->post('familymembername'));

					$this->db->delete('msstaff_family_members',array('staffid' => $staff_id));

					for ($i=0; $i < $countfamilymember ; $i++) { 

						if ($_FILES['familymemberphoto']['name'][$i] != NULL) {


							@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

							$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
							$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/familymemberphoto/";
							$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;

							}else{

								die("Error uploading Photo file");
								$this->session->set_flashdata("er_msg", "Error uploading registration image");
								redirect(current_url());
							}
						}
						else{
							$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
							$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
						}


						if ($_FILES['identity_familymemberphoto']['name'][$i] != NULL) {


							@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

							$OriginalFamilyMemberPhotoName = $_FILES['identity_familymemberphoto']['name'][$i]; 
							$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['identity_familymemberphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/familymemberphoto/";
							$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedIdentityFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;
								$OriginalIdentityFamilyMemberPhotoName=$_FILES['identity_familymemberphoto']['name'][$i];

							}else{

								die("Error uploading Photo file");
								$this->session->set_flashdata("er_msg", "Error uploading registration image");
								redirect(current_url());
							}
						}
						else{
							$OriginalIdentityFamilyMemberPhotoName = $this->input->post('originalidentityfamilymemberphoto')[$i];
							$encryptedIdentityFamilyMemberNameImage = $this->input->post('oldidentityfamilymemberphoto')[$i];
						}

						$familydob = $this->input->post('familymemberdob')[$i];
						$familydobformat = $this->model->changedatedbformate($familydob);
						

						$insertFamilyMember	 = array(
							'staffid'          => $staff_id,
							'Familymembername'     => $this->input->post('familymembername')[$i],
							'relationwithemployee'  => $this->input->post('relationwithenployee')[$i],
							'familydob'             => $familydobformat,
							'originalphotoname'     => $OriginalFamilyMemberPhotoName,
							'encryptedphotoname'    => $encryptedFamilyMemberNameImage,
							'originalfamilyidentityphotoname'     => $OriginalIdentityFamilyMemberPhotoName,
							'encryptedfamilyidentityphotoname'    => $encryptedIdentityFamilyMemberNameImage,
							'createdon'      	    => date('Y-m-d H:i:s'),
							 'createdby'      	        => $this->loginData->UserID, // 
							 'isdeleted'      	        => 0, 
							);
						
						$this->db->insert('msstaff_family_members', $insertFamilyMember);
						
					}
					$this->db->trans_complete();
					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error Adding Staff Profile Redgistration');	
						redirect(current_url());	
					}else{
 //die;
						$this->session->set_flashdata('tr_msg', 'Successfully Adding Staff Profile Redgistration');

						redirect('Stafffullinfo/edit/'.$staff_id);

					} 
				} 
				

				else if(!empty($Submit) && $Submit =='SubmitData')
				{					
		 
		 // echo "<pre>";
		 // print_r($this->input->post());  




					if (!empty($this->input->post('passingyear')[0])) {

						$countgapfromdate = count($this->input->post('passingyear'));

					$this->db->delete('msstaffeducation_details',array('staffid'=>$staff_id));

						for ($j=0; $j < $countgapfromdate; $j++) { 

							if ($_FILES['certificate']['name'][$j] != NULL) {	

								echo $_FILES['certificate']['name'][$j];

								@  $ext = end((explode(".", $_FILES['certificate']['name'][$j]))); 

								$matricoriginalName = $_FILES['certificate']['name'][$j]; 
								$matricencryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['certificate']['tmp_name'][$j];
								$targetPath = FCPATH . "datafiles/educationalcertificate/";
								$targetFile = $targetPath . $matricencryptedName;
								$uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){
									$matricencryptedImage[$j] = $matricencryptedName[$j];

								}else{

									die("Error uploading certificate Document ");
									$this->session->set_flashdata("er_msg", "Error uploading 10thcertificate Document");
									redirect(current_url());
								}

							}
							else{
								$matricoriginalName = $this->input->post('originalmatriccertificate')[$j];
								$matricencryptedImage = $this->input->post('oldmatriccertificate')[$j];
							}


				$education_arr=array(
					'staffid'                  => $staff_id,
					'edulevel_cd'              => $this->input->post('education_level')[$j],
					'year_passing'             => $this->input->post('passingyear')[$j],
					'collg_name'               => $this->input->post('schoolcollegeinstitute')[$j],
					'university'               => $this->input->post('boarduniversity')[$j],
					'place'                    => $this->input->post('place')[$j],
					'stream'                   => $this->input->post('specialisation')[$j],
					'percentage'               => $this->input->post('percentage')[$j],
					'originalcertificate'      => $_FILES['certificate']['name'][$j],
					'encryptedcertificatename' => $matricencryptedImage[$j]
				);

					/*echo "<pre>";
					print_r($education_arr);*/
				$this->db->insert('msstaffeducation_details', $education_arr);
//echo $this->db->last_query(); 
						}
					}

//	die;				

					$dateofbirth1 = $this->input->post('dateofbirth');
					$date_of_birth = $this->model->changedatedbformate($dateofbirth1);

					$candidateemailid   = $this->input->post("emailid");
					$candidatefirstname = $this->input->post('candidatefirstname');

					$this->db->trans_start();
					

					$staff_name=$this->input->post('candidatefirstname')." ".$this->input->post("candidatemiddlename")." ".$this->input->post('candidatelastname');
					$father_name=$this->input->post('fatherfirstname')." ".$this->input->post("fathermiddlename")." ".$this->input->post('fatherlastname');
					$mother_name=$this->input->post('motherfirstname')." ".$this->input->post("mothermiddlename")." ".$this->input->post('motherlastname');

					$status=$this->input->post('status');
					


					
					$where = $this->input->post("where");
					if(!empty($where)){
						$valwhere = $where;
					}else{
						$valwhere = NULL;
					}

					$whendate       = $this->input->post('when');	
					$dbformatwhendate    =$this->model->changedatedbformate($whendate);


					if ($_FILES['photoupload']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['photoupload']['name']))); 


						$OriginalName = $_FILES['photoupload']['name']; 
						$encryptedName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['photoupload']['tmp_name'];
						$targetPath = FCPATH . "datafiles/";
						$targetFile = $targetPath . $encryptedName;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encryptedImage = $encryptedName;

						}else{

							die("Error uploading Profile Photo ");
							$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
							redirect(current_url());
						}

					}else{

						$encryptedImage = $this->input->post('oldphotoupload');

					}

					if ($_FILES['signature_upload']['name'] != NULL) {

						@  $ext = end((explode(".", $_FILES['signature_upload']['name']))); 


						$original_sagnature = $_FILES['signature_upload']['name']; 
						$encrypted_signature = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
						$tempFile = $_FILES['signature_upload']['tmp_name'];
						$targetPath = FCPATH . "datafiles/signature/";
						$targetFile = $targetPath . $encrypted_signature;
						$uploadResult = move_uploaded_file($tempFile,$targetFile);

						if($uploadResult == true){
							$encrypted_signature = $encrypted_signature;
							$original_sagnature=$original_sagnature;

						}else{

							die("Error uploading Profile Photo ");
							$this->session->set_flashdata("er_msg", "Error uploading Profile Photo");
							redirect(current_url());
						}

					}else{

						$encryptedImage = $this->input->post('old_signature_upload');

					}

				

					
					$insertArrCandidate = array(
						'name'              => $staff_name,
						'father_name'       => $father_name,
						'mother_name'  	    => $mother_name,
						'gender'    		        => $this->input->post('gender'),
						'nationality'       	    => $this->input->post("nationality"),
						'marital_status'  	        => $this->input->post('maritalstatus'),
						'dob '    	        => $date_of_birth,
						'emailid'       	        => $this->input->post("emailid"),
						'contact'  		            => $this->input->post('mobile'),
						'originalphotoname'=>$_FILES['photoupload']['name'],
						'encryptedphotoname'=>$encryptedImage,
						'presentstreet'    	    => $this->input->post('presentstreet'),
						'presentcity'       	=> $this->input->post("presentcity"),
						'presentstateid'  	    => $this->input->post('presentstateid'),
						'presentdistrict '    	=> $this->input->post('presentdistrict'),
						'presentpincode'       	=> $this->input->post("presentpincode"),
						'permanentstreet'    	=> $this->input->post('permanentstreet'),
						'permanentcity'       	=> $this->input->post("permanentcity"),
						'permanentstateid'  	=> $this->input->post('permanentstateid'),
						'permanentdistrict '    => $this->input->post('permanentdistrict'),
						'permanentpincode'      => $this->input->post("permanentpincode"),
						'presenthno '           => $this->input->post('presenthno'),
						'permanenthno'          => $this->input->post("permanenthno"),
						'flag'                  => 1,
						'any_subject_of_interest'        => trim($this->input->post('subjectinterest')),
						'any_achievementa_awards'         => trim($this->input->post("achievementawards")),
						'any_assignment_of_special_interest'     => trim($this->input->post("any_assignment_of_special_interest")),
						'experience_of_group_social_activities' => trim($this->input->post("experience_of_group_social_activities")),
						'have_you_taken_part_in_pradan_selection_process_before' => $this->input->post("pradan_selection_process_before"),
						'have_you_taken_part_in_pradan_selection_process_before_when'=> $dbformatwhendate,
						'have_you_taken_part_in_pradan_selection_process_before_where'=> $valwhere,
						'annual_income'          => $this->input->post("annual_income"),
						'male_sibling'                    => $this->input->post("no_of_male_sibling"),
						'female_sibling'                   => $this->input->post("no_of_female_sibling"),
						'know_about_pradan'                => $this->input->post("have_you_come_to_know"),
						'know_about_pradan_other_specify'  => $this->input->post("specify"),
						'bloodgroup'                       =>$this->input->post('bloodgroup'),

						'reportingto'=>$supervisor,
							'original_sagnature'=>$original_sagnature,
						'encrypted_signature'=>$encrypted_signature
						

					);


					$this->db->where('staffid',$staff_id);
					$this->db->update('staff', $insertArrCandidate);
					// echo $this->db->last_query();
					// die;



			$insertid = $this->db->insert_id();

		 	$id= $this->input->post('s_id');

		 	$insert_data =array(
		 		'type'    => 1,
		 		'r_id'    => $id,
		 		'sender'  => $this->loginData->staffid,
		 		'receiver' => 1284,
		 		'senddate' => date('Y-m-d'),
		 		'createdon' => date('Y-m-d H:i:s'),
		 		'createdby' => $staff_id,
		 		'flag'      => 1,
		 		'staffid'   => $staff_id
		 	);
		  $this->db->insert('tbl_workflowdetail', $insert_data);




					//  	$updatestaffid	 = array(
					//  		'candidateid' => $candiate_id,
					//  	);

		 	 //         $this->db->where('staffid', $staff_id);
					// $this->db->update('staff', $updatestaffid); /// update staff candidateid /////


					// $Arruser = array(
					// 	'Candidateid'   => $candiate_id,
					// );

					// $this->db->where('UserID',$this->loginData->UserID);
					// $this->db->update('mstuser', $Arruser); ////// Update Mst user /////



					$countidentityname = count($this->input->post('identityname'));

					$isAadhar= false;
					$isPan = false;
					for ($i=0; $i < $countidentityname; $i++) { 
						$identityname1=$this->input->post('identityname')[$i];
						if ($identityname1 ==1) {
							$isAadhar= true;
				////$isPan=false;

						}elseif ($identityname1 ==2) {
							$isPan = true;
			//	$isAadhar= false;
						}
					}
					//echo $isAadhar.$isPan;
					
					if ($isAadhar == false || $isPan == false) {

						$this->session->set_flashdata("er_msg","PAN Card and Aadhar Card must be mandatory");

						redirect(current_url());
					}


					$this->db->delete('msstaff_identity_details',array('staffid'=> $staff_id));


					for ($i=0; $i < $countidentityname; $i++) { 

						if ($_FILES['identityphoto']['name'][$i] != NULL) {
							@  $ext = end((explode(".", $_FILES['identityphoto']['name'][$i]))); 


							$OriginalIdentityName = $_FILES['identityphoto']['name'][$i]; 
							$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['identityphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/identitydocuments/";
							$targetFile = $targetPath . $encryptedIdentityName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedIdentitydocument = $encryptedIdentityName;
							}else{

								die("Error uploading Identity Document !");
								$this->session->set_flashdata("er_msg", "Error uploading Identity Document !");
								redirect(current_url());
							}

						}else{
							$OriginalIdentityName      = $this->input->post('originalidentityphoto')[$i];
							$encryptedIdentitydocument = $this->input->post('oldidentityphoto')[$i];
						}


						$insertIdentityDetails	 = array(
							//'id'=>$id,
							'staffid'         =>$staff_id,
							'identityname'        =>trim($this->input->post('identityname')[$i]),
							'identitynumber'      =>trim($this->input->post('identitynumber')[$i]),
							'encryptedphotoname'  => $encryptedIdentitydocument,
							'originalphotoname'   => $OriginalIdentityName,
							'createdon'           => date('Y-m-d H:i:s'),
						    'createdby'           => $this->loginData->UserID, // 
						    'isdeleted'           => 0, 

						);
						
						$this->db->insert('msstaff_identity_details', $insertIdentityDetails);
					}





					$countsyslanguage = count($this->input->post('syslanguage')); 

					$this->db->delete('msstaff_language_proficiency',array('staffid'=> $staff_id));

					for ($i=0; $i < $countsyslanguage; $i++) { 

						$insertLanguageProficiency = array(
							'staffid'  => $staff_id,
							'languageid'   => $this->input->post("syslanguage")[$i],
							'lang_speak'   => $this->input->post("speak")[$i],
							'lang_read'    => $this->input->post("read")[$i],
							'lang_write'   => $this->input->post("write")[$i],
							'createdon'    => date('Y-m-d H:i:s'),
							'isdeleted'    => 0, 
						);

						$this->db->insert('msstaff_language_proficiency', $insertLanguageProficiency);

					}

					if (!empty($this->input->post('gapfromdate')[0])) {

						$countgapfromdate = count($this->input->post('gapfromdate'));

						$this->db->delete('msstaff_gap_year',array('staffid'=> $staff_id));

						for ($j=0; $j < $countgapfromdate; $j++) { 
							$gapfromdate    = $this->input->post('gapfromdate')[$j];
							$gapfromdate1   = $this->model->changedatedbformate($gapfromdate);
							$gaptodate      = $this->input->post('gaptodate')[$j];
							$gaptodate1     = $this->model->changedatedbformate($gaptodate);

							$insertGapyear	 = array(
								'staffid'          => $staff_id,
								'fromdate'         => $gapfromdate1,
								'todate'           => $gaptodate1,
								'reason'           => trim($this->input->post('gapreason')[$j]),
								'createdon'        => date('Y-m-d H:i:s'),
								'isdeleted'        => 0, 
							);
							$this->db->insert('msstaff_gap_year', $insertGapyear);
						}
					}


					$count_orgname = count($this->input->post('orgname'));   

					$this->db->delete('msstaffexp_details',array('staffid'=> $staff_id));

					for ($i=0; $i < $count_orgname; $i++) { 

						if (!empty($this->input->post('orgname')[$i])) {

							if ($_FILES['experiencedocument']['name'][$i] != NULL) {
							@  $ext = end((explode(".", $_FILES['experiencedocument']['name'][$i]))); 


							$OriginalIdentityName = $_FILES['experiencedocument']['name'][$i]; 
							$encryptedIdentityName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['experiencedocument']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/workexperience/";
							$targetFile = $targetPath . $encryptedIdentityName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);



								if($uploadResult == true){

									$encrypteddocument = $encryptedexpdocumentName;

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$OriginalexperiencedocumentName = $this->input->post('originalexperiencedocument')[$i];
								$encrypteddocument =  $this->input->post('oldexperiencedocument')[$i];
							}


							if ($_FILES['salary_slip1']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['salary_slip1']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['salary_slip1']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['salary_slip1']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);


								if($uploadResult == true){

									$encryptedsalary_slip1 = $encryptedexpdocumentName;
									$Originalsalary_slip1=$_FILES['salary_slip1']['name'][$i];

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$Originalsalary_slip1 = $this->input->post('originalsalary_slip1')[$i];
								$encryptedsalary_slip1 =  $this->input->post('oldsalary_slip1')[$i];
							}


							if ($_FILES['salary_slip2']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['salary_slip2']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['salary_slip2']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['salary_slip2']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encryptedsalary_slip2 = $encryptedexpdocumentName;
									$Originalsalary_slip2=$_FILES['salary_slip2']['name'][$i];

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$Originalsalary_slip2 = $this->input->post('originalsalary_slip2')[$i];
								$encryptedsalary_slip2 =  $this->input->post('oldsalary_slip2')[$i];
							}


							if ($_FILES['salary_slip3']['name'][$i] != NULL) {

								@  $ext = end(explode(".", $_FILES['salary_slip3']['name'][$i])); 


								$OriginalexperiencedocumentName = $_FILES['salary_slip3']['name'][$i]; 
								$encryptedexpdocumentName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
								$tempFile = $_FILES['salary_slip3']['tmp_name'][$i];
								$targetPath = FCPATH . "datafiles/workexperience/";
								$targetFile = $targetPath . $encryptedexpdocumentName;
								@ $uploadResult = move_uploaded_file($tempFile,$targetFile);

								if($uploadResult == true){

									$encryptedsalary_slip3 = $encryptedexpdocumentName;
									$Originalsalary_slip3=$_FILES['salary_slip3']['name'][$i];

								}else{

									die("Error uploading Work Experience ");
									$this->session->set_flashdata("er_msg", "Error uploading Work Experience");
									redirect(current_url());
								}

							}
							else{

								$Originalsalary_slip3 = $this->input->post('originalsalary_slip3')[$i];
								$encryptedsalary_slip3 =  $this->input->post('oldsalary_slip3')[$i];
							}



							$workfromdate     = $this->input->post('work_experience_fromdate')[$i];
							$workexpfromdate  = $this->model->changedatedbformate($workfromdate);
							$worktodate       = $this->input->post('work_experience_todate')[$i];	
							$workexptodate    =$this->model->changedatedbformate($worktodate);


							$insertWorkExperience1 = array(

								'staffid'      			=> $staff_id,
								'organization' 			=> trim($this->input->post('orgname')[$i]),
								'reason_leaving'		=> trim($this->input->post("descriptionofassignment")[$i]),
								'fromdate'              => $workexpfromdate,
								'todate'                => $workexptodate,
								'position'        		=> trim($this->input->post("palceofposting")[$i]),
								'encrypteddocumnetname'     =>$encrypteddocument,
								'originalexperiencedocument'=>$OriginalexperiencedocumentName,
								'encryptedsalaryslip1'      =>$encryptedsalary_slip1,
								'originalsalaryslip1'      =>$Originalsalary_slip1,
								'encryptedsalaryslip2'    =>$encryptedsalary_slip2,
								'originalsalaryslip2'=>$Originalsalary_slip2,
								'encryptedsalaryslip3'=>$encryptedsalary_slip3,
								'originalsalaryslip3'=>$Originalsalary_slip3,
								'designation'	        => trim($this->input->post("designation")[$i]),
								'lastsalary'       		=> trim($this->input->post("lastsalarydrawn")[$i]),
								'createdon'             => date('Y-m-d H:i:s'),
							);
							$this->db->insert('msstaffexp_details',$insertWorkExperience1);
						}
					}




///////// Training Exposure Save Start Here ///////////// 
					
					$countnatureoftraining = count($this->input->post('natureoftraining')); 


					$this->db->delete('msstafftraining_programs_attend',array('staffid'=> $staff_id));

					for ($j=0; $j < $countnatureoftraining; $j++) { 

						if (!empty($this->input->post('natureoftraining')[$j])) {


							$fromdate1 = $this->input->post('fromdate')[$j];
							$fromdate  = $this->model->changedatedbformate($fromdate1); //die;
							$todate1   = $this->input->post('todate')[$j];	
							$todate    = $this->model->changedatedbformate($todate1);
							$id=$this->input->post('t_id')[$j];

							$insertTrainingExposure	 = array(
								'staffid'      => $staff_id,
								'Training_program' => trim($this->input->post('natureoftraining')[$j]),
								'organizing_agency'=> trim($this->input->post('organizingagency')[$j]),
								'fromdate'         => $fromdate,
								'todate'           => $todate,
								'createdon'        => date('Y-m-d H:i:s'),
								'isdeleted'        => 0, 
							);


							$this->db->insert('msstafftraining_programs_attend', $insertTrainingExposure);
						}
					}



				$countfamilymember = count($this->input->post('familymembername'));

				$this->db->delete('msstaff_family_members',array('staffid' => $staff_id));

					for ($i=0; $i < $countfamilymember ; $i++) { 

						if ($_FILES['familymemberphoto']['name'][$i] != NULL) {


							@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

							$OriginalFamilyMemberPhotoName = $_FILES['familymemberphoto']['name'][$i]; 
							$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['familymemberphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/familymemberphoto/";
							$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;

							}else{

								die("Error uploading Photo file");
								$this->session->set_flashdata("er_msg", "Error uploading registration image");
								redirect(current_url());
							}
						}
						else{
							$OriginalFamilyMemberPhotoName = $this->input->post('originalfamilymemberphoto')[$i];
							$encryptedFamilyMemberNameImage = $this->input->post('oldfamilymemberphoto')[$i];
						}


						if ($_FILES['identity_familymemberphoto']['name'][$i] != NULL) {


							@  $ext = end((explode(".", $_FILES['familymemberphoto']['name'][$i]))); 

							$OriginalFamilyMemberPhotoName = $_FILES['identity_familymemberphoto']['name'][$i]; 
							$encryptedFamilyMemberPhotoName = md5(date("Y-m-d H:i:s").rand(1,100)) . "." . $ext;
							$tempFile = $_FILES['identity_familymemberphoto']['tmp_name'][$i];
							$targetPath = FCPATH . "datafiles/familymemberphoto/";
							$targetFile = $targetPath . $encryptedFamilyMemberPhotoName;
							$uploadResult = move_uploaded_file($tempFile,$targetFile);

							if($uploadResult == true){
								$encryptedIdentityFamilyMemberNameImage = $encryptedFamilyMemberPhotoName;
								$OriginalIdentityFamilyMemberPhotoName=$_FILES['identity_familymemberphoto']['name'][$i];

							}else{

								die("Error uploading Photo file");
								$this->session->set_flashdata("er_msg", "Error uploading registration image");
								redirect(current_url());
							}
						}
						else{
							$OriginalIdentityFamilyMemberPhotoName = $this->input->post('originalidentityfamilymemberphoto')[$i];
							$encryptedIdentityFamilyMemberNameImage = $this->input->post('oldidentityfamilymemberphoto')[$i];
						}

						$familydob = $this->input->post('familymemberdob')[$i];
						$familydobformat = $this->model->changedatedbformate($familydob);
						

						$insertFamilyMember	 = array(
							'staffid'          => $staff_id,
							'Familymembername'     => $this->input->post('familymembername')[$i],
							'relationwithemployee'  => $this->input->post('relationwithenployee')[$i],
							'familydob'             => $familydobformat,
							'originalphotoname'     => $OriginalFamilyMemberPhotoName,
							'encryptedphotoname'    => $encryptedFamilyMemberNameImage,
							'originalfamilyidentityphotoname'     => $OriginalIdentityFamilyMemberPhotoName,
							'encryptedfamilyidentityphotoname'    => $encryptedIdentityFamilyMemberNameImage,
							'createdon'      	    => date('Y-m-d H:i:s'),
							 'createdby'      	        => $this->loginData->UserID, // 
							 'isdeleted'      	        => 0, 
							);
						
						$this->db->insert('msstaff_family_members', $insertFamilyMember);

					}
					//echo $this->db->last_query(); 
					$this->db->trans_complete();
					if ($this->db->trans_status() === FALSE){

						$this->session->set_flashdata('er_msg', 'Error Adding Staff Profile Redgistration');	
						redirect(current_url());	
					}else{
 //die;
						$this->session->set_flashdata('tr_msg', 'Successfully Adding Staff Profile Redgistration');
						redirect('Stafffullinfo/view/'.$staff_id);

//						redirect('Stafffullinfo/view/'.$staff_id);

					} 
				} 
			}

				prepareview:
				
				$content['candidatedetails']= $this->model->getCandidateDetailsPreview($staff_id);
				
				 $tc=$content['candidatedetails']->reportingto;

				 //echo "tc=".$tc;
				 if (empty($tc) || $tc == ''||$tc==NULL ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				
				$content['all_tc']= $this->model->superviser_list();
			} else { 
				
				 // $content['select_reporting']= $this->model->selectedsuperviser($tc);

			 	$content['all_tc']= $this->model->superviser_list($tc);
			 	 // print_r($content['all_tc']); die();
				}
			
				  
				

		
			$content['count_education']=$this->Stafffullinfo_model->countStaffEducationDetails($staff_id);
			
			//die;
		
			$content['educationcount']= $this->model->countStaffEducationDetails($staff_id);
			//print_r($content['educationcount']);
			//die;
			$content['staffeducationdetail']= $this->model->getStaffEducationDetails($staff_id);
			$fcount = $this->model->getCountFamilyMember($staff_id);
			 $content['familycount']= $fcount;   
			$content['familymemberdetails']    = $this->model->getCandidateFamilyMemberDetails($staff_id);
			$Icount = $this->model->getCountIdentityNumber($staff_id);
			$content['identitycount']= $Icount; 
			$content['identitydetals'] = $this->model->getCandidateIdentityDetails($staff_id);
			$TEcount = $this->model->getCountTrainingExposure($staff_id);

			$content['TrainingExpcount']= $TEcount;  
		

			$content['trainingexposuredetals'] = $this->model->getCandidateTrainingExposureDetails($staff_id);
			$content['edu_level']=$this->model->getStaffEducation_level();
			
			$content['login_data']=$this->loginData;


			@ $Lcount = $this->model->getCountLanguage($staff_id);

			@ $content['languageproficiency']= $Lcount;  
			

			@ $content['languagedetals'] = $this->model->getCandidateLanguageDetails($staff_id);
			


			$WEcount = $this->model->getCountWorkExprience($staff_id);

			$content['WorkExperience']= $WEcount;  
	
			$content['workexperiencedetails'] = $this->model->getCandidateWorkExperienceDetails($staff_id);
	
			$GPYcount = $this->model->getCountGapYear($staff_id);

			$content['GapYearCount']= $GPYcount;  
			

			$content['gapyeardetals'] = $this->model->getCandidateGapYearDetails($staff_id);
			
			$content['statedetails']           = $this->model->getState();
			$content['ugeducationdetails']     = $this->model->getUgEducation();
			$content['pgeducationdetails']     = $this->model->getPgEducation();
			$content['campusdetails']          = $this->model->getCampus();
			$content['syslanguage']            = $this->model->getSysLanguage();
			$content['sysrelations']           = $this->model->getSysRelations();
			$content['sysidentity']            = $this->model->getSysIdentity();
			$content['getdistrict']            = $this->model->getDistrict();

			$content['token']=$staff_id;
			$content['method'] = $this->router->fetch_method();
			$content['title'] = 'Stafffullinfo';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	
	
	}



	public function bdfformsubmit()
	{

		try{

			$content['title'] = 'Stafffullinfo';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);


		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


}