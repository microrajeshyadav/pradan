<?php 
class Tc_verify_documents extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		try{

			$this->load->model('Tc_verify_documents_model');

			$this->db->trans_start();

			$RequestMethod = $this->input->server('REQUEST_METHOD'); 

			if($RequestMethod == "POST"){

			}

			$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit();
		// echo "<pre>";
		// print_r($content['candidatebdfformsubmit']);
		// die;
		// $content['code_generate']=$this->model->status_codegenerate();


			$content['title'] = 'Tc_verify_documents';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




	public function verification($token)
	{
		try{


			if (empty($token)) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Tc_verify_documents/index');
				die;
			}else{


				$this->load->model('Tc_verify_documents_model');
				$content['getcandateverified'] = $this->model->getCandateVerified($token);
				// echo "<pre>";
				// print_r($content['getcandateverified']);
				// die;

				$teamid = 0;  
				$dateofjoin = '';
				$designationid = '';
				$Cname      ='';
				$hrdemailid='';
				$Fathername ='';
				$Mothername ='';
				$username  ='';
				$password  ='';
				$candidateemailid;
				$filename = '';
				$feedbackdatedb = '';
				$categoryiddetail ='';
				$RoleID = '';
				$StaffEmpnumber = '';
				$getreportingto = '';
				$tcemailid = '';
				$FAemailid = '';

				$candidate  = $this->model->getCandidate_BDFFormSubmit($token);
				// echo "<pre>";
				// print_r($candidate);
				// die;
				//$content['candidate']=$candidate;

				$getreportingto = $this->gmodel->getReportingTo($candidate[0]->teamid);
				//print_r($getreportingto);
			//	die;
				$tcemailid     = $getreportingto->emailid;
				$FAemailid     = $this->gmodel->getFinanceAdministratorEmailid();

				$DevelopmentApprenticeshipDetails = $this->model->getDevelopmentApprenticeship($token);
				//echo $DevelopmentApprenticeshipDetails->teamid;

				$teamid='';

				$Selected_tcDetails = $this->model->tc_data($DevelopmentApprenticeshipDetails->teamid);
				$supervisor_name=$Selected_tcDetails->name;
				$sup_email=$Selected_tcDetails->emailid;
				$f_name=$DevelopmentApprenticeshipDetails->candidatefirstname;


				if (!empty($DevelopmentApprenticeshipDetails->emailid)) {
					$candidateemailid = $DevelopmentApprenticeshipDetails->emailid;
				}
				$gethrdmail = $this->gmodel->getHRDEmailid();
				if (!empty($gethrdmail->hrdemailid)) {
					$hrdemailid = $gethrdmail->hrdemailid;
				}
				$Personnelemailid = $this->gmodel->getPersonnelAdministratorEmailid();

				if (!empty($Personnelemailid->personnelemailid)) {
					$personnelemailid =$Personnelemailid->personnelemailid;
				}


				$RequestMethod = $this->input->server('REQUEST_METHOD'); 

				if($RequestMethod == "POST"){
					$savebtndatasend = $this->input->post('submitbtn');

					if (!empty($savebtndatasend) && $savebtndatasend =='senddatasubmit') {

						// echo "<pre>";
						// print_r($_POST);
						// die;

						$candidateid  = $this->input->post('candidateid');

						$query = $this->db->query("SELECT * FROM `tbl_verification_document_hrd` WHERE `candidateid` = ".$candidateid);

						$result = $query->row();

						$currentstatus  = $this->input->post('verified_status'); 
							// echo "<pre>";
							// print_r($content['getcandateverified']);
							// die;

							// if($content['getcandateverified']->categoryid==1)
							// {
								// echo $query->num_rows();
								// die;


						if ($query->num_rows() > 0){


							$this->session->set_flashdata('er_msg', 'Already candidate document verified !!!');	
							redirect('Tc_verify_documents/');
						}


						
						else{


								//echo $currentstatus;


							if (isset($currentstatus) && $currentstatus ==1) {
								$this->db->trans_start();


								$query = $this->db->query("SELECT * FROM `tbl_verification_document_hrd` WHERE `candidateid` = ".$candidateid);

								$result = $query->row();
						 //count($result);
					// 	echo count($result);
					// //print_r(count($result));
					// die;


								if(count($result)==0)	{



									$verificationarraydata = array(
										'candidateid'             => $token,
										'metric_certificate'      => $this->input->post('metric_certificate_verified'),
										'hsc_certificate'         => $this->input->post('hsc_certificate_verified'),
										'ug_certificate'          => $this->input->post('ug_certificate_verified'),
										'pg_certificate'          => $this->input->post('pg_certificate_verified'),
										'other_certificate'   	  => $this->input->post('other_certificate_verified'),
										'general_nomination_form' => $content['getcandateverified']->categoryid==1?$this->input->post('general_nomination_form'):null,
										'joining_report_da'       => $content['getcandateverified']->categoryid==1?$this->input->post('joining_report_da'):null,
										'comment'                 => $this->input->post('comments'),
										'status'                  => $currentstatus,
										'createdby'               => $this->loginData->UserID, 	
										'createdon'               => date('Y-m-d H:i:s'),
										'approvedby'              => $this->loginData->UserID, 	
										'approvedon'              => date('Y-m-d H:i:s'),
									);

								// print_r($verificationarraydata);
								// die;
									$this->db->insert('tbl_verification_document_hrd', $verificationarraydata);


								}

								// echo "hhh";
								// die;
				///// currentstatus start here $currentstatus=1 //////////////////////// 
								//b.fileno, b.offerno, b.doj, b.lastdateofacceptanceofdocs, b.flag, c.presentstreet, c.presentcity, c.presentstateid, c.presentdistrict, c.presentpincode, c.permanentstreet, c.permanentcity, c.permanentstateid, c.permanentdistrict, c.permanentpincode, c.presenthno, c.permanenthno,
								$query = $this->db->query("SELECT a.*,
									(CASE WHEN b.fileno is NULL THEN apt.fileno else b.fileno END)as fileno,
									(CASE WHEN b.offerno is NULL THEN apt.offerno else b.offerno END)as offerno,
									(CASE WHEN b.doj is NULL THEN apt.doj else b.doj END)as doj,
									(CASE WHEN b.lastdateofacceptanceofdocs is NULL THEN apt.lastdateofacceptanceofdocs else b.lastdateofacceptanceofdocs END)as lastdateofacceptanceofdocs,
									(CASE WHEN b.flag is NULL THEN apt.flag else b.flag END)as flag,
									(CASE WHEN b.filename is NULL THEN apt.filename else b.filename END)as filename,
									(CASE WHEN b.sendflag is NULL THEN apt.sendflag else b.sendflag END)as sendflag,
									(CASE WHEN b.payscale is NULL THEN apt.payscale else b.payscale END)as payscale,
									(CASE WHEN b.basicsalary is NULL THEN apt.basicsalary else b.basicsalary END)as basicsalary, 
									apt.transid,
									apt.staffid,
									apt.proposed_officeid,
									apt.edcomments,
									apt.edstatus,
									apt.personnelId,
									staff.staffid,
									staff.name,
									staff.contact,
									staff.emailid,
									staff.gender,
									staff.professional,
									staff.designation,
									staff.reportingto,
									staff.Prog_id,
									staff.doj_team,
									staff.emp_code,
									staff.father_name,
									staff.mother_name,
									staff.dob,
									staff.marital_status,
									staff.hometown,
									staff.informaton,
									staff.title,
									staff.noofchilds,
									staff.Joining_basicpay,
									staff.Joining_scaleofpay,
									staff.contactno2,
									staff.contactno3,
									staff.presenthno,
									staff.presentstreet,
									staff.presentcity,
									staff.presentstateid,
									staff.presentdistrict,
									staff.presentpincode,
									staff.permanenthno,
									staff.permanentstreet,
									staff.permanentcity,
									staff.permanentstateid,
									staff.permanentdistrict,
									staff.permanentpincode,
									staff.deletedemployee,
									staff.dateofleaving,
									staff.separationtype,
									staff.separationremarks,
									staff.nationality,
									staff.joiningandinduction,
									staff.status,
									staff.flag,
									staff.encryptedphotoname,
									staff.any_subject_of_interest,
									staff.any_achievementa_awards,
									staff.any_assignment_of_special_interest,
									staff.experience_of_group_social_activities,
									staff.any_subject_of_interest,
									staff.have_you_taken_part_in_pradan_selection_process_before,
									staff.have_you_taken_part_in_pradan_selection_process_before_when,
									staff.have_you_taken_part_in_pradan_selection_process_before_where,
									staff.annual_income,
									staff.male_sibling,
									staff.female_sibling,
									staff.know_about_pradan,
									staff.know_about_pradan_other_specify,
									staff.bloodgroup,
									staff.new_office_id,
									staff.probation_date,
									staff.probation_extension_date,
									staff.probation_status,
									staff.super_annuation,
									staff.probation_completed,
									staff.encrypted_signature,
									staff.recommend_to_graduate,
									staff.fgid,
									staff.integratorapproval,

									
									a.emailid as candidate_emailid,
									a.fgid as candidate_fgid,
									a.gender  as candidate_gender,
									b.doj as candidate_doj,
									a.candidateid as s_candidate_id,
									a.nationality as candidate_nationality,
									a.encryptedphotoname as candidate_encryptedphotoname,
									a.originalphotoname as candidate_originalphotoname,
									c.presenthno as candidate_presenthno,
									c.presentcity as candidate_presentcity,
									c.presentdistrict as candidate_presentdistrict,
									c.presentstreet as candidate_presentstreet,
									c.presentstateid as candidate_presentstateid,
									c.presentpincode as candidate_presentpincode,
									c.permanentstreet as candidate_permanentstreet,
									c.permanentcity as candidate_permanentcity,
									c.permanentstateid as candidate_permanentstateid,
									c.permanentdistrict as candidate_permanentdistrict,
									c.permanentpincode as candidate_permanentpincode,
									c.permanenthno as candidate_permanenthno,
									d.any_subject_of_interest as candidate_any_subject_of_interest,
									d.any_achievementa_awards as candidate_any_achievementa_awards,
									d.any_assignment_of_special_interest as candidate_any_assignment_of_special_interest,
									d.experience_of_group_social_activities as candidate_experience_of_group_social_activities,
									d.have_you_taken_part_in_pradan_selection_process_before as candidate_have_you_taken_part_in_pradan_selection_process_before,
									d.have_you_taken_part_in_pradan_selection_process_before_when as candidate_have_you_taken_part_in_pradan_selection_process_before_when,
									d.have_you_taken_part_in_pradan_selection_process_before_where as candidate_have_you_taken_part_in_pradan_selection_process_before_where,
									d.annual_income as candidate_annual_income,
									d.male_sibling as candidate_male_sibling,
									d.female_sibling as candidate_female_sibling,
									d.know_about_pradan as candidate_know_about_pradan,
									d.know_about_pradan_other_specify as candidate_know_about_pradan_other_specify,

									ct.*, des.* FROM `tbl_candidate_registration` as a 

									left join `staff` on  a.candidateid = `staff`.candidateid
									left join `tbl_generate_offer_letter_details` b ON a.candidateid = b.candidateid
									left join `tbl_offer_of_appointment` apt ON a.candidateid = apt.candidateid
									inner join `tbl_candidate_communication_address` c ON a.candidateid = c.candidateid
									inner join `tbl_other_information` d ON a.candidateid = d.candidateid
									inner join `mstcategory` as ct on a.categoryid = ct.id
									inner join `msdesignation` as des on ct.categoryname = des.desname
									WHERE a.`candidateid`= ".$candidateid."");
$result = $query->row();
								/*echo  "<pre>";
								print_r($result);
								die;*/

								
								if (count($result) == 0) {
									$this->session->set_flashdata('er_msg', 'Please check !!! Record Not Found !!!');	
								}
								else
								{

									$teamid = $result->teamid;
									$dateofjoin = $result->doj;
									$designationid = $result->designationid;
									$categoryiddetail = $result->categoryid;
									$recommnendedtograduate = $result->recommend_to_graduate;	
									$title='';		
									if($result->candidate_gender ==1){
										$title = "Mr";
									}elseif($result->candidate_gender ==2 && $result->maritalstatus ==2){
										$title = "Miss";
									}elseif($result->candidate_gender ==2 && $result->maritalstatus !=2){
										$title = "Mrs";
									}elseif($result->candidate_gender ==3){
										$title = "Mx";
									}

									$getreportingto = $this->model->getReportingTo($teamid);
									$reportingto = $getreportingto->staffid;

			            $increment_staffid               = $this->model->getStaffId(); //die;

			            $tablename = 'staff_transaction';
			            $incval = '@a';

			            $increment_staffid_transactionid = $this->model->getStaffTransactioninc($tablename,$incval); 
			           // echo $increment_staffid_transactionid; die;
			            //die;
			            $Empnumber    = $this->getTotalEmployees();

			            $StaffEmpnumber   = $this->model->getStaffEmployeeCode();

			            $Cname = $result->candidatefirstname.' '.$result->candidatemiddlename.' '.$result->candidatelastname;

			            $Fathername = $result->fatherfirstname.' '.$result->fathermiddlename.' '.$result->fatherlastname;

			            $Mothername = $result->motherfirstname.' '.$result->mothermiddlename.' '.$result->motherlastname;

			            $username = $result->candidatefirstname.'_'.$increment_staffid;
			            $password = $result->candidatefirstname;

			            $query = $this->db->query("SELECT * FROM `tbl_da_personal_info` WHERE `candidateid` = ".$candidateid);

			            $result1 = $query->row();
			            if (count($result1)==0)
			            {
			            	$insertArraydata = array(
			            		'candidateid'   => $candidateid,
			            		'name'			=> $Cname,
			            		'mobile'		=> $result->mobile,
			            		'emailid'		=> $result->candidate_emailid,
			            		'gender'		=> $result->candidate_gender,
			            		'doj'			=> $result->doj,
			            		'dob'			=> $result->dateofbirth,
			            		'maritalstatus'	=> $result->maritalstatus,
			            		'teamid'       	=> $result->teamid,
			            		'fgid'       	=> $result->fgid,
			            		'batchid'       => $result->batchid,
			            		'emp_code'		=> $Empnumber,
			            		'staffid'		=> $increment_staffid,
			            		'createdon'		=> date('Y-m-d H:i:s'),
			            		'createdby'		=> $this->loginData->UserID,
			            	);



			            	$this->db->insert('tbl_da_personal_info', $insertArraydata);


			            }



			            if ($designationid == 13 || $recommnendedtograduate == 1) {
			            	$newdate = strtotime('+1 years' , strtotime($dateofjoin));
			            	$probationdate = date ('Y-m-j', $newdate);

			            }else{
			            	$newdate = strtotime('+6 month' , strtotime($dateofjoin));
			            	$probationdate = date ('Y-m-j', $newdate);
			            }

			            $query1 = $this->db->query("SELECT * FROM `staff` WHERE `candidateid` = ".$candidateid);

			            $result_staff = $query1->row();

			            if(count($result_staff)==0)
			            {



			            	$insertArraystaffdata = array(
			            		'staffid'      			=> $increment_staffid,
			            		'name'         			=> $Cname,
			            		'contact'	   			=> $result->mobile==""?null:$result->mobile,
			            		'emailid'	   			=> $result->candidate_emailid==""?null:$result->candidate_emailid,
			            		'doj'          			=> $dateofjoin,
			            		'gender'	   			=> $result->candidate_gender,
			            		'professional' 			=> 'APP',
			            		'designation'  			=> $designationid,
			            		'reportingto'  			=> $reportingto==""?null:$reportingto,
			            		'Prog_id'	   			=> 'O',
			            		'doj_team'	   			=> $dateofjoin,
			            		'emp_code'     			=> $StaffEmpnumber,
			            		'candidateid'          => $result->s_candidate_id==""?null:$result->s_candidate_id,
			            		'father_name'          => $Fathername==""?null:$Fathername,
			            		'mother_name'		   => $Mothername==""?null:$Mothername,
			            		'title'		           => $title,
			            		'dob'	               => $result->dateofbirth==""?null:$result->dateofbirth,
			            		'marital_status'       => $result->maritalstatus,
			            		'hometown'	           => $result->candidate_presentcity==""?null:$result->candidate_presentcity,
			            		'presenthno'	       => $result->candidate_presenthno==""?null:$result->candidate_presenthno,
			            		'presentstreet'	       => $result->candidate_presentstreet==""?null:$result->candidate_presentstreet,
			            		'presentcity'	       => $result->candidate_presentcity==""?null:$result->candidate_presentcity,
			            		'presentstateid'       => $result->candidate_presentstateid==""?null:$result->candidate_presentstateid,
			            		'presentdistrict'      => $result->candidate_presentdistrict==""?null:$result->candidate_presentdistrict,
			            		'presentpincode'       => $result->candidate_presentpincode==""?null:$result->candidate_presentpincode,
			            		'permanenthno'	       => $result->candidate_permanenthno==""?null:$result->candidate_permanenthno,
			            		'permanentstreet'      => $result->candidate_permanentstreet==""?null:$result->candidate_permanentstreet,
			            		'permanentcity'	       => $result->candidate_permanentcity==""?null:$result->candidate_permanentcity,
			            		'permanentstateid'     => $result->candidate_permanentstateid==""?null:$result->candidate_permanentstateid,
			            		'permanentdistrict'	   => $result->candidate_permanentdistrict==""?null:$result->candidate_permanentdistrict,
			            		'permanentpincode'	   => $result->candidate_permanentpincode==""?null:$result->candidate_permanentpincode,
			            		'nationality' 		   => $result->candidate_nationality=="" ?null:$result->candidate_nationality,
			            		'encryptedphotoname'   => $result->candidate_encryptedphotoname==""?null:$result->candidate_encryptedphotoname,
			            		'originalphotoname'    => $result->candidate_originalphotoname==""?null:$result->candidate_originalphotoname,
			            		'any_subject_of_interest' => $result->candidate_any_subject_of_interest,
			            		'any_achievementa_awards' => $result->candidate_any_achievementa_awards==""?null:$result->candidate_any_achievementa_awards,
			            		'any_assignment_of_special_interest' => $result->candidate_any_assignment_of_special_interest==""?null:$result->candidate_any_assignment_of_special_interest,
			            		'experience_of_group_social_activities' => $result->candidate_experience_of_group_social_activities==""?null:$result->candidate_experience_of_group_social_activities,
			            		'have_you_taken_part_in_pradan_selection_process_before' =>$result->candidate_have_you_taken_part_in_pradan_selection_process_before==""?null:$result->candidate_have_you_taken_part_in_pradan_selection_process_before, 
			            		'have_you_taken_part_in_pradan_selection_process_before_when' => $result->candidate_have_you_taken_part_in_pradan_selection_process_before_when==""?null:$result->candidate_have_you_taken_part_in_pradan_selection_process_before_when,

			            		'have_you_taken_part_in_pradan_selection_process_before_where' => $result->candidate_have_you_taken_part_in_pradan_selection_process_before_where==""?null:$result->candidate_have_you_taken_part_in_pradan_selection_process_before_where,
			            		'annual_income' => $result->candidate_annual_income==""?null:$result->candidate_annual_income,
			            		'male_sibling' => $result->candidate_male_sibling==""?null:$result->candidate_male_sibling,
			            		'female_sibling' => $result->candidate_female_sibling==""?null:$result->candidate_female_sibling,
			            		'know_about_pradan' => $result->candidate_know_about_pradan==""?null:$result->candidate_know_about_pradan,
			            		'know_about_pradan_other_specify' => $result->candidate_know_about_pradan_other_specify==""?null:$result->candidate_know_about_pradan_other_specify,
			            		'new_office_id' => $teamid,
			            		'probation_date' => $probationdate==""?null:$probationdate,
			            		'Joining_basicpay' => $result->basicsalary==""?0.00:$result->basicsalary,
			            		'Joining_scaleofpay' => $result->payscale==""?null:$result->payscale,
			            		'fgid'               => $result->candidate_fgid==""?null:$result->candidate_fgid,
			            		'status' => 1,
			            		'createdon'	           => date('Y-m-d H:i:s'),
			            		'createdby'	           => $this->loginData->UserID,
			            	);

$this->db->insert('staff', $insertArraystaffdata);
}



$query2 = $this->db->query("SELECT * FROM `staff_transaction`  left join staff on staff.staffid=staff_transaction.staffid WHERE staff.`candidateid` = ".$candidateid);

$result_trans = $query2->row();
if(count($result_trans)==0)
{

	$insertArraystafftransactiondata = array(
		'staffid'               => $increment_staffid,
		'id'                   => $increment_staffid_transactionid->maxincval,
		'old_office_id'         => null,
		'new_office_id'	        => $teamid==""?null:$teamid,
		'date_of_transfer'      => $dateofjoin==""?null:$dateofjoin,
		'old_designation'       => null,
		'new_designation'	    => $designationid==""?null:$designationid,
		'trans_status'          => 'JOIN',
		'fgid'                  => $result->fgid==""?null:$result->fgid,
		'reportingto'			=> $reportingto==""?null:$reportingto,
		'datetime'              => date('Y-m-d H:i:s'),
		'createdon'	           => date('Y-m-d H:i:s'),
		'createdby'	           => $this->loginData->UserID,
	);
	$this->db->insert('staff_transaction', $insertArraystafftransactiondata);
}



 $this->db->last_query(); 

            // if ($designationid  != 13) {

            // 	@ $currentyear = $this->gmodel->getcurrentfyear();


            // 	$sql = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms <= '".date('Y-m-d')."' Order by DESC LIMIT 1 "; 
            // 	$res = $this->db->query($sql)->row();

            // 	if(count($res) > 0){  
            // 		$Accuredvalue = $res->credit_value;
            // 		$From_date = $res->monthfroms;
            // 		$TO_date = $res->monthtos;

            // 		$totalcrsql = "select SUM(credit_value) as credit_value from msleavecrperiod where CYear='".$currentyear."'"; 
            // 		$totalcrres = $this->db->query($totalcrsql)->row();
            // 		$perdayleave =  $totalcrres->credit_value/365;
            // 		$datesqldiff = date_diff($dateofjoin, $TO_date);
            // 		$Noofdays=0;
            // 		$Noofdays = $this->gmodel->roundvalue($datesqldiff*$perdayleave);
            // 		$sql1 = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms = '".$fromdate."'  "; 
            // 		$res1 = $this->db->query($sql1)->row();

            // 		$insertarray = array(
            // 			'Emp_code'=> $Empnumber,
            // 			'From_date'=> $From_date,
            // 			'To_date'=> $TO_date,
            // 			'Leave_type'=> 0,
            // 			'Leave_transactiontype'=> 'CR',
            // 			'Description' => 'Credit leaves posting',
            // 			'Noofdays'=> $Noofdays,
            // 			'appliedon'=> date('Y-m-d')
            // 		);

            // 		$this->db->insert('trnleave_ledger', $insertarray);

            // 	}

            // }

		//die;

if ($designationid  == 13) {

	$sysqueryfeedback = $this->db->query("SELECT * FROM `sysfeedbackphase`  WHERE sysfeedbackphase.`isdeleted`=0 ");
	$sysresultfeedback = $sysqueryfeedback->result();
            // echo "<pre>";
            // print_r($sysresultfeedback); die;

	foreach ($sysresultfeedback as $key => $val) {

		$month = $val->month;
		$days = $val->duration_in_days;

		$dueDate = date('Y-m-d', strtotime("+".$month." months", strtotime($result->doj)));
		$enddate = date('Y-m-d', strtotime("+".$days." days", strtotime($dueDate)));
		$excelsextremely       = $this->input->post('excels_in_or_extremely_good');
		$daneedpayattention    = $this->input->post('da_needs_to_pay_attention');
		$daleaveorganization   = $this->input->post('da_to_leave_organization');
		$tcid                  = $this->input->post("tcid");


		$insertArr = array(
			'apprenticeid'               => $increment_staffid,
			'teamid'                     => $result->teamid,
			'tcid'                       => $this->input->post("tcid")==''? '0':' ',
			'batchid'                    => $result->batchid==''?0:$result->batchid,
			'field_guideid'              => $result->fgid==''?0:$result->fgid,
			'da_extremely_good'          => $excelsextremely==''? NULL:' ',
			'da_needs_to_pay_attention'  => $daneedpayattention==''? NULL:' ',
			'da_to_leave_organization'   => $daleaveorganization==''? NULL:' ',
			'feedbackdate'               => $feedbackdatedb==''? NULL:' ',
			'duedate'                    => $dueDate,
			'enddate'                    => $enddate,
			'status'                     =>  0,
			'monthyear'                  => $month==''? '0': $month,
			'feedback_letter'            => $filename==''? NULL:' ',
			'createdon'                  => date('Y-m-d H:i:s'),
			    'createdby'                  => $this->loginData->UserID, // login user id
			);

		$this->db->insert('tbl_feedback', $insertArr);

        	// echo $this->db->last_query(); die;

	}

}
$query_education = $this->db->query("SELECT * FROM `msstaffeducation_details` 
	left join staff on msstaffeducation_details.staffid=staff.staffid
	WHERE staff.`candidateid` = ".$candidateid);

$result_education = $query_education->result();

if(count($result_education)==0)
{

	$sql_edu = $this->db->query("SELECT
		metricschoolstream,
		metricschoolcollege,
		metricboarduniversity,
		metricpassingyear,
		metricplace,
		metricspecialisation,
		metricpercentage,
		encryptmetriccertificate,
		originalmetriccertificate,
		hscstream,  
		hscschoolcollege,
		hscboarduniversity,
		hscpassingyear,
		hscplace,
		hscspecialisation,
		hscpercentage,
		encrypthsccertificate,
		originalhsccertificate,
		CASE WHEN ugdegree='BTech' THEN 3 WHEN ugdegree='B.Sc Agriculture' THEN 3 WHEN ugdegree='B.Sc (AH)' THEN 3 WHEN ugdegree='B.E.' THEN 3 WHEN ugdegree='B.Sc' THEN 3 
		WHEN ugdegree='B.Com' THEN 3
		WHEN ugdegree='B.A' THEN 3 
		WHEN ugdegree='B.Arch' THEN 3 ELSE 3 END as ugdegree,
		ugschoolcollege,
		ugboarduniversity,
		ugpassingyear,
		ugplace,
		ugspecialisation,
		ugpercentage,
		encryptugcertificate,
		originalugcertificate,
		CASE WHEN pgdegree='M.A' THEN 5 WHEN pgdegree='M.com' THEN 5 WHEN pgdegree='M.Sc' THEN 5 WHEN pgdegree='MSW' THEN 5 WHEN pgdegree='MBA' THEN 5 
		WHEN pgdegree='MBBS' THEN 5
		WHEN pgdegree='MBAs' THEN 5 ELSE 5 END as pgdegree, 

		pgschoolcollege,
		pgboarduniversity,
		pgpassingyear,
		pgplace,
		pgspecialisation,
		pgpercentage,
		encryptpgcertificate,
		originalpgcertificate,
		CASE WHEN otherdegree='other' THEN 9 ELSE 9 END as otherdegree, 
		otherschoolcollege,
		otherboarduniversity,
		otherpassingyear,
		otherplace,
		otherspecialisation,
		otherpercentage,
		encryptothercertificate,
		originalothercertificate,
		other_degree_specify
		FROM
		`tbl_candidate_registration` AS a
		WHERE
		a.`candidateid` = ".$candidateid);
	$resultedu = $sql_edu->row();
	$data_array=array();
	foreach ($resultedu as $key => $value) {
		array_push($data_array,$value);
	}
	$k=0;
	$l = 9;
	$edu_array=array();
	for ($m=0; $m < 5; $m++) { 
		if ($m==4) {
			$edu_array = array_slice($data_array, $k, 10);

			if (!empty($edu_array[0]) && !empty($edu_array[1])) {
				$insertArraystaffedu = array(
					'staffid'                => $increment_staffid==""?0:$increment_staffid,
					'edulevel_cd'            => $edu_array[0]==""?0:$edu_array[0],
					'collg_name'             => $edu_array[1]==""?null:$edu_array[1],
					'university'              => $edu_array[2]==""?null:$edu_array[2],
					'year_passing'            => $edu_array[3]==""?0:$edu_array[3],
					'place'	                  => $edu_array[4]==""?null:$edu_array[4],
					'stream'                   => $edu_array[5]==""?null:$edu_array[5],
					'percentage'              => $edu_array[6]==""?null:$edu_array[6],
					'encryptedcertificatename' => $edu_array[7]==""?null:$edu_array[7],
					'originalcertificate'      => $edu_array[8]==""?null:$edu_array[8],
					'other_degree_specify'	    => $edu_array[9]==""?null:$edu_array[9],
					'createdon'	           => date('Y-m-d H:i:s'),
					'createdby'	           => $this->loginData->UserID,
				);
				$this->db->insert('msstaffeducation_details', $insertArraystaffedu);
	// echo $this->db->last_query();
	// die;
			}

			$k= $l;
			$l= $l + 9;

		}
		else
		{
			$edu_array = array_slice($data_array, $k, 9);
		//print_r($edu_array);

			if ($edu_array[0] !='' && $edu_array[1] !='') {
				$insertArraystaffedu = array(
					'staffid'                => $increment_staffid==""?0:$increment_staffid,
					'edulevel_cd'            => $edu_array[0]==""?0:$edu_array[0],
					'collg_name'             => $edu_array[1]==""?null:$edu_array[1],
					'university'              => $edu_array[2]==""?null:$edu_array[2],
					'year_passing'            => $edu_array[3]==""?0:$edu_array[3],
					'place'	                  => $edu_array[4]==""?null:$edu_array[4],
					'stream'                   => $edu_array[5]==""?null:$edu_array[5],
					'percentage'              => $edu_array[6]==""?null:$edu_array[6],
					'encryptedcertificatename' => $edu_array[7]==""?null:$edu_array[7],
					'originalcertificate'      => $edu_array[8]==""?null:$edu_array[8],
					'createdon'	           => date('Y-m-d H:i:s'),
					'createdby'	           => $this->loginData->UserID,
				);
				$this->db->insert('msstaffeducation_details', $insertArraystaffedu);
	//echo $this->db->last_query();
			}

			$k= $l;
			$l= $l + 9;

		}

	}
}

  ///////// Start work Experience ///////////

$query_exper = $this->db->query("SELECT * FROM `msstaffexp_details` 
	left join staff on msstaffexp_details.staffid=staff.staffid
	WHERE staff.`candidateid` = ".$candidateid);

$result_experience = $query_exper->row();
if(count($result_experience)==0)
{


	$countworkexp     = $this->model->getCountWorkExperience($candidateid);


	if ($countworkexp > 0) {

		$candidateworkexp = $this->model->getWorkExperience($candidateid);

		foreach ($candidateworkexp as $key => $value) {


			$insertArrstaffexpdata = array(
				'staffid'             => $increment_staffid,
				'organization'        => $value->organizationname,
				'position'	          => $value->palceofposting,
				'lastsalary'          => $value->lastsalarydrawn,
				'reason_leaving'      => $value->descriptionofassignment,
				'fromdate'	          => $value->fromdate,
				'todate'                         => $value->todate,
				'designation'         			 => $value->designation,
				'encrypteddocumnetname'	         => $value->encrypteddocumnetname==""?null:$value->encrypteddocumnetname,
				'originalexperiencedocument'      => $value->originalexperiencedocument==""?null:$value->originalexperiencedocument,
				'originalsalaryslip1'             => $value->originalsalaryslip1==""?null:$value->originalsalaryslip1,
				'encryptedsalaryslip1'	          => $value->encryptedsalaryslip2==""?null:$value->encryptedsalaryslip2,
				'originalsalaryslip2'             => $value->originalsalaryslip2==""?null:$value->originalsalaryslip2,
				'encryptedsalaryslip2'            => $value->encryptedsalaryslip2==""?null:$value->encryptedsalaryslip2,
				'originalsalaryslip3'	          => $value->originalsalaryslip3==""?null:$value->originalsalaryslip3,
				'encryptedsalaryslip3'            => $value->encryptedsalaryslip3==""?null:$value->encryptedsalaryslip3,
				'createdon'	                      => date('Y-m-d H:i:s'),
				'createdby'	                      => $this->loginData->UserID,
			);
			$this->db->insert('msstaffexp_details', $insertArrstaffexpdata);
				//echo $this->db->last_query(); 
		}
	} 

}


   ///////// Close work Experience ///////////



			   //////////// Start family members ///////////


$query_family = $this->db->query("SELECT * FROM `msstaff_family_members` 
	left join staff on msstaff_family_members.staffid=staff.staffid
	WHERE staff.`candidateid` = ".$candidateid);

$result_family = $query_family->result();
if(count($result_family)==0)
{

	$countfamilymember     = $this->model->getCountFamilymember($candidateid);

	if ($countfamilymember > 0) {

		$candidatefamilymember = $this->model->getFamilyMember($candidateid);
				// echo "<pre>";
				// print_r($candidatefamilymember); die;

		foreach ($candidatefamilymember as $key => $value) {

			$insertArrstafffamilydata = array(
				'staffid'                 => $increment_staffid,
				'Familymembername'        => $value->Familymembername==""?null:$value->Familymembername,
				'relationwithemployee'	  => $value->relationwithemployee==""?null:$value->relationwithemployee,
				'familydob'               => $value->familydob,
				'originalphotoname'       => $value->originalphotoname==""?null:$value->originalphotoname,
				'encryptedphotoname'	  => $value->encryptedphotoname==""?null:$value->encryptedphotoname,
				'originalfamilyidentityphotoname'     => $value->originalfamilyidentityphotoname==""?null:$value->originalfamilyidentityphotoname,
				'encryptedfamilyidentityphotoname'    => $value->encryptedfamilyidentityphotoname==""?null:$value->encryptedfamilyidentityphotoname,
				'originalfamilyidentityphotoname'     => $value->encryptedphotoname==""?null:$value->encryptedphotoname,
				'createdon'	          => date('Y-m-d H:i:s'),
				'createdby'	          => $this->loginData->UserID,
			);
			$this->db->insert('msstaff_family_members', $insertArrstafffamilydata);
				//echo $this->db->last_query();  die;
		}
			}    ///////// Close family members  ///////////

		}


			//////////// Start identity details ///////////

		$query_identity = $this->db->query("SELECT * FROM `msstaff_identity_details` 
			left join staff on msstaff_identity_details.staffid=staff.staffid
			WHERE staff.`candidateid` = ".$candidateid);

		$result_identity = $query_identity->result();
		if(count($result_identity)==0)
		{

			$countidentitydetails     = $this->model->getCountidentitydetails($candidateid);

			if ($countidentitydetails > 0) {

				$candidateidentitydetails = $this->model->getIdentityDetails($candidateid);
				
				foreach ($candidateidentitydetails as $key => $value) {

					$insertArrstaffidentitydetailsdata = array(
						'staffid'             => $increment_staffid==""?null:$increment_staffid,
						'identityname'        => $value->identityname==""?null:$value->identityname,
						'identitynumber'	  => $value->identitynumber==""?null:$value->identitynumber,
						'originalphotoname'   => $value->originalphotoname==""?null:$value->originalphotoname,
						'encryptedphotoname'  => $value->encryptedphotoname==""?null:$value->encryptedphotoname,
						'createdon'	          => date('Y-m-d H:i:s'),
						'createdby'	          => $this->loginData->UserID,
					);
					$this->db->insert('msstaff_identity_details', $insertArrstaffidentitydetailsdata);
				//echo $this->db->last_query(); 
				}
			}    ///////// Close identity details ///////////

		}


			//////////// Start Gap Year ///////////

		$query_gap = $this->db->query("SELECT * FROM `msstaff_gap_year` 
			left join staff on msstaff_gap_year.staffid=staff.staffid
			WHERE staff.`candidateid` = ".$candidateid);

		$result_gap = $query_gap->result();
		if(count($result_gap)==0)
		{

			$countgapyear     = $this->model->getCountgapyear($candidateid);

			if ($countgapyear > 0) {

				$candidategapyear = $this->model->getGapYear($candidateid);
				
				foreach ($candidategapyear as $key => $value) {

					$insertArrstaffgapyeardata = array(
						'staffid'             => $increment_staffid,
						'fromdate'            => $value->fromdate,
						'todate'	         => $value->todate,
						'reason'             => $value->reason==""?null:$value->reason,
						'createdon'	          => date('Y-m-d H:i:s'),
						'createdby'	          => $this->loginData->UserID,
					);
					$this->db->insert('msstaff_gap_year', $insertArrstaffgapyeardata);
				//echo $this->db->last_query(); 
				}
			}  
		}  ///////// Close Gap Year ///////////

			//////////// Start language proficiency ///////////


		$countlangprof     = $this->model->getCountlanguageproficiency($candidateid);

		if ($countlangprof > 0) {

			$candidatelangprof = $this->model->getLanguageProficiency($candidateid);

			foreach ($candidatelangprof as $key => $value) {

				$insertArrstafflangprfodata = array(
					'staffid'                => $increment_staffid==""?null:$increment_staffid,
					'languageid'             => $value->languageid==""?null:$value->languageid,
					'lang_speak'	         => $value->lang_speak==""?null:$value->lang_speak,
					'lang_read'              => $value->lang_read==""?null:$value->lang_read,
					'lang_write'	         => $value->lang_write==""?null:$value->lang_write,
					'createdon'	             => date('Y-m-d H:i:s'),
					'createdby'	             => $this->loginData->UserID,
				);
				$this->db->insert('msstaff_language_proficiency', $insertArrstafflangprfodata);
				//echo $this->db->last_query(); 
			}
			}    ///////// Close language proficiency ///////////


				//////////// Start work Experience ///////////

			$query_training = $this->db->query("SELECT * FROM `msstafftraining_programs_attend` 
				left join staff on msstafftraining_programs_attend.staffid=staff.staffid
				WHERE staff.`candidateid` = ".$candidateid);

			$result_training = $query_training->result();
			if(count($result_training)==0)
			{
				$counttrainingexposure    = $this->model->getCountTrainingExposure($candidateid);

				if ($counttrainingexposure > 0) {

					$candidatetrainingexposure = $this->model->getTrainingExposure($candidateid);

					foreach ($candidatetrainingexposure as $key => $value) {

						$insertArrstafftrainingexposuredata = array(
							'staffid'             => $increment_staffid==""?null:$increment_staffid,
							'Training_program'    => $value->natureoftraining==""?null:$value->natureoftraining,
							'organizing_agency'	  => $value->organizing_agency==""?null:$value->organizing_agency,
							'fromdate'            => $value->fromdate==""?null:$value->fromdate,
							'todate'               => $value->todate==""?null:$value->todate,
							'createdon'	          => date('Y-m-d H:i:s'),
							'createdby'	          => $this->loginData->UserID,
						);
						$this->db->insert('msstafftraining_programs_attend', $insertArrstafftrainingexposuredata);
				//echo $this->db->last_query(); 
					}
				}   
			}

			 ///////// Close work Experience ///////////

			$query_tranfer = $this->db->query("SELECT * FROM `msstaffexp_details` 
				left join staff on msstaffexp_details.staffid=staff.staffid
				WHERE staff.`candidateid` = ".$candidateid);

			$result_experience = $query_tranfer->row();
			if(count($result_experience)==0)
			{	
				$inserttranferArr = array(
					'staffid'         => $increment_staffid,
					'teamid'          => $result->teamid,
					'fg'              => $result->fgid,
					'doj'             => date('Y-m-d'),
					'createdon'       => date('Y-m-d H:i:s'),
					'createdby'       => $this->loginData->UserID,
				);

				$this->db->insert('tbl_transfer_history', $inserttranferArr);
			}

		   //  $this->db->last_query();
			if ($categoryiddetail==1) {
				$RoleID = 19;
			}else{
				$RoleID = '';
			}

			$query_mstuser = $this->db->query("SELECT * FROM `mstuser` 
				WHERE `candidateid` = ".$candidateid);

			$result_mstuser = $query_mstuser->row();
			if(count($result_experience)==0)
			{
				$insertArrayUserdata = array(
					'Username'        => $username,
					'Password'		  => md5($password),
					'UserFirstName'	  => $result->candidatefirstname,
					'UserMiddleName'  => $result->candidatemiddlename,
					'UserLastName'	  => $result->candidatelastname,
					'PhoneNumber'	  => $result->mobile,
					'EmailID'		  => $result->emailid,
					'RoleID'	      => $RoleID,
					'Candidateid'     => $candidateid,
					'staffid'		  => $increment_staffid,
					'CreatedOn'		  => date('Y-m-d H:i:s'),
					'CreatedBy'		  => $this->loginData->UserID,
				);

				$this->db->insert('mstuser', $insertArrayUserdata);
			}
			else
			{

				$mstuserarraydata = array(
					'RoleID' => 3

				);
				$this->db->where('candidateid', $candidateid);
				$this->db->update('mstuser', $mstuserarraydata);
				//echo $this->db->last_query();
			}

			$registrationarraydata = array(
				'joinstatus' => 1,
				'tc_hrd_document_verfied' => 2,
			);
			$this->db->where('candidateid', $candidateid);
			$this->db->update('tbl_candidate_registration', $registrationarraydata);

			$oldcandidatequery = "SELECT old_candidate_id from tbl_candidate_registration where candidateid=".$candidateid;
			$resquery = $this->db->query($oldcandidatequery)->row();
			if(!empty($resquery->old_candidate_id) && $resquery->old_candidate_id !=NULL){

				$oldcandidateupdate = array(
					'IsDeleted' =>1
				);
				$this->db->where('Candidateid', $resquery->old_candidate_id);
				$this->db->update('mstuser', $oldcandidateupdate);
			}
						// echo "sdfdsfsdf"; die;



			$this->db->trans_complete(); 

			if ($this->db->trans_status() === FALSE){

				

				$this->session->set_flashdata('er_msg', $this->db->error());	
			}else{



			 //// Send Mail To HRD //////////////////////////////   
					// $this->loginData->EmailID;
				if(!empty($content['getcandateverified']->flag))
				{

					$hrdemailid    = $this->loginData->EmailID;
					$FAemailid     = $this->gmodel->getFinanceAdministratorEmailid();
					$addlink = site_url('login');

					
					//die;

					$candidatedetails = $this->model->getCandidate_BDFFormSubmit($token);
					$cand_name = '';
					$cand_name = $candidatedetails[0]->candidatefirstname;
					$recipients = array('Hremailid' => $hrdemailid, 'tcemailid' => $sup_email,'FAemailid' => $FAemailid->financeemailid);
					$subject = "Apprentice Codes";
					// $html = 'Dear '.$candidatedetails[0]->candidatefirstname.', <br><br> 
					// Congratulations joining formalities completed. Below are the details. <br><br>.
					// <br> Employee Code - '.$StaffEmpnumber.'
					// <br>Comments:- '. $this->input->post('comments').'  
					// <br> Username - '.$username.'
					// <br>Password - '.$password.'<br>
					// Please fill Boidata form <a href='.$addlink.'>Click here</a>';
					// $message='';

					// $fd = fopen("mailtext/verification_and_code_generation.txt", "r");	
					// 	$message .=fread($fd,4096);
					// 	eval ("\$message = \"$message\";");
					// 	$message =nl2br($message);
						//echo $message;
						//die;

					$to_email = $candidateemailid;
					// $to_name =  $candidateemailid;

					$candidate = array('$supervisor_name','$f_name','$StaffEmpnumber','$username','$password','$addlink');
					$candidate_replace = array($supervisor_name,$cand_name,$StaffEmpnumber,$username,$password,$addlink);

					$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 31 AND `isactive` = '1'";
					$data = $this->db->query($sql)->row();
					if(!empty($data))
						$body = str_replace($candidate,$candidate_replace , $data->lettercontent);

					//echo $to_email;
					//print_r($recipients);
					//die;
					$sendmail = $this->Common_Model->send_email($subject,$body,$to_email,$to_name=null,$recipients);
				}
				

				
    	 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				
				$this->session->set_flashdata('tr_msg', 'Successfully candidate join and document verification !!!');	
				redirect('Tc_verify_documents/');		

			}
		}


	}else{

		$this->db->trans_start();

		if($content['getcandateverified']->categoryid==1)
		{
			$verificationarraydata = array(
				'candidateid'             => $token,
				'metric_certificate'      => $this->input->post('metric_certificate_verified'),
				'hsc_certificate'         => $this->input->post('hsc_certificate_verified'),
				'ug_certificate'          => $this->input->post('ug_certificate_verified'),
				'pg_certificate'          => $this->input->post('pg_certificate_verified'),
				'other_certificate'   	  => $this->input->post('other_certificate_verified'),
				'general_nomination_form' => $content['candidate']->categoryid==1?$this->input->post('general_nomination_form'):null,


				'joining_report_da'       => $content['candidate']->categoryid==1?$this->input->post('joining_report_da'):null,
				'comment'                 => $this->input->post('comments'),
				'status'                  => $currentstatus,
				'createdby'               => $this->loginData->UserID, 	
				'createdon'               => date('Y-m-d H:i:s'),
				'approvedby'              => $this->loginData->UserID, 	
				'approvedon'              => date('Y-m-d H:i:s'),
			);

			$this->db->insert('tbl_verification_document_hrd', $verificationarraydata);

		}
		else 
		{

			$query_hrdverification = $this->db->query("SELECT * FROM `tbl_verification_document_hrd` 
				left join staff on tbl_verification_document_hrd.staffid=staff.staffid
				WHERE staff.`candidateid` = ".$candidateid);

			$result_hrdverification = $query_hrdverification->row();
			if(count($result_hrdverification)==0)
			{

				$verificationarraydata = array(
					'candidateid'             => $token,
					'metric_certificate'      => $this->input->post('metric_certificate_verified'),
					'hsc_certificate'         => $this->input->post('hsc_certificate_verified'),
					'ug_certificate'          => $this->input->post('ug_certificate_verified'),
					'pg_certificate'          => $this->input->post('pg_certificate_verified'),
					'other_certificate'   	  => $this->input->post('other_certificate_verified'),
					'general_nomination_form' => $content['candidate']->categoryid==1?$this->input->post('general_nomination_form'):null,


					'joining_report_da'       => $content['candidate']->categoryid==1?$this->input->post('joining_report_da'):null,
					'comment'                 => $this->input->post('comments'),
					'status'                  => $currentstatus,
					'createdby'               => $this->loginData->UserID, 	
					'createdon'               => date('Y-m-d H:i:s'),
					'approvedby'              => $this->loginData->UserID, 	
					'approvedon'              => date('Y-m-d H:i:s'),
				);

				$this->db->insert('tbl_verification_document_hrd', $verificationarraydata);
			}
			else
			{
				$verificationarray = array(
					'status' =>2
				);
				$this->db->where('Candidateid', $resquery->old_candidate_id);
				$this->db->update('tbl_verification_document_hrd', $oldcandidateupdate);
			}
		}
	}
}	
		//	echo $this->db->trans_complete(); ; die;

$this->db->trans_complete(); 

if ($this->db->trans_status() === FALSE){

	$this->session->set_flashdata('er_msg', $this->db->error());	
}else{

	 //// Send Mail To HRD //////////////////////////////   
	$hrdemailid    = $this->loginData->EmailID;
	$addlink = site_url('login');

	$subject = "HRD  Reject all documents verified and Emp. code generated";
	$candidatedetails = $this->model->getCandidate_BDFFormSubmit($token);
	$html = 'Dear '.$candidatedetails[0]->candidatefirstname.', <br><br> 
	Your joining formalities not completed.<br>
	Please Contact to pradan admin.<br>
	<br><br>.
	<br>Comments:- '. $this->input->post('comments').' ';

	$to_email = $hrdemailid;
	$to_name =  $candidateemailid;

	$sendmail = $this->Common_Model->send_email($subject, $html, $to_email,$to_name);


    	 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');

	$this->session->set_flashdata('tr_msg', 'Successfully candidate not join and document verification !!!');	
	redirect('Tc_verify_documents/');		
}

				///// currentstatus end here $currentstatus=1 //////////////////////// 

}
}
}



$content['token'] = $token; 


$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);


// echo "<pre>";
// print_r($content['getcandateverified']);
// die;

$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);
$content['getgapyearcount'] = $this->model->getCandategapyearcount($token);
$content['getgapyearpverified'] = $this->model->getgapyeareCandateVerified($token);

$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);

if($content['getcandateverified']->categoryid==1)
{

	$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);
	$content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);
}

$content['getdocuments'] = $this->model->getOtherDocuments();
$content['countotherdoc'] = $this->model->getCountOtherDocument($token);
$content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);

$content['title'] = 'Tc_verify_documents';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);




}catch (Exception $e) {
	print_r($e->getMessage());die;
}

}


public function verify_joininginduction($token)
{
	try{

		$this->load->model('Tc_verify_documents_model');
		$content['getcandateverified'] = $this->model->getCandateVerified($token);

		$RequestMethod = $this->input->server('REQUEST_METHOD'); 

		if($RequestMethod == "POST"){
			$savebtndatasend = $this->input->post('submitbtn');

			if (!empty($savebtndatasend) && $savebtndatasend =='senddatasubmit') {


				$query = $this->db->query("SELECT a.*,
					(CASE WHEN b.fileno is NULL THEN apt.fileno else b.fileno END)as fileno,
					(CASE WHEN b.offerno is NULL THEN apt.offerno else b.offerno END)as offerno,
					(CASE WHEN b.doj is NULL THEN apt.doj else b.doj END)as doj,
					(CASE WHEN b.lastdateofacceptanceofdocs is NULL THEN apt.lastdateofacceptanceofdocs else b.lastdateofacceptanceofdocs END)as lastdateofacceptanceofdocs,
					(CASE WHEN b.flag is NULL THEN apt.flag else b.flag END)as flag,
					(CASE WHEN b.filename is NULL THEN apt.filename else b.filename END)as filename,
					(CASE WHEN b.sendflag is NULL THEN apt.sendflag else b.sendflag END)as sendflag,
					(CASE WHEN b.payscale is NULL THEN apt.payscale else b.payscale END)as payscale,
					(CASE WHEN b.basicsalary is NULL THEN apt.basicsalary else b.basicsalary END)as basicsalary, 
					apt.transid,
					apt.staffid,
					apt.proposed_officeid,
					apt.edcomments,
					apt.edstatus,
					apt.personnelId,
					staff.staffid,
					staff.name,
					staff.contact,
					staff.emailid,
					staff.gender,
					staff.professional,
					staff.designation,
					staff.reportingto,
					staff.Prog_id,
					staff.doj_team,
					staff.emp_code,
					staff.father_name,
					staff.mother_name,
					staff.dob,
					staff.marital_status,
					staff.hometown,
					staff.informaton,
					staff.title,
					staff.noofchilds,
					staff.Joining_basicpay,
					staff.Joining_scaleofpay,
					staff.contactno2,
					staff.contactno3,
					staff.presenthno,
					staff.presentstreet,
					staff.presentcity,
					staff.presentstateid,
					staff.presentdistrict,
					staff.presentpincode,
					staff.permanenthno,
					staff.permanentstreet,
					staff.permanentcity,
					staff.permanentstateid,
					staff.permanentdistrict,
					staff.permanentpincode,
					staff.deletedemployee,
					staff.dateofleaving,
					staff.separationtype,
					staff.separationremarks,
					staff.nationality,
					staff.joiningandinduction,
					staff.status,
					staff.flag,
					staff.encryptedphotoname,
					staff.any_subject_of_interest,
					staff.any_achievementa_awards,
					staff.any_assignment_of_special_interest,
					staff.experience_of_group_social_activities,
					staff.any_subject_of_interest,
					staff.have_you_taken_part_in_pradan_selection_process_before,
					staff.have_you_taken_part_in_pradan_selection_process_before_when,
					staff.have_you_taken_part_in_pradan_selection_process_before_where,
					staff.annual_income,
					staff.male_sibling,
					staff.female_sibling,
					staff.know_about_pradan,
					staff.know_about_pradan_other_specify,
					staff.bloodgroup,
					staff.new_office_id,
					staff.probation_date,
					staff.probation_extension_date,
					staff.probation_status,
					staff.super_annuation,
					staff.probation_completed,
					staff.encrypted_signature,
					staff.recommend_to_graduate,
					staff.fgid,
					staff.integratorapproval,


					a.emailid as candidate_emailid,
					a.fgid as candidate_fgid,
					a.gender  as candidate_gender,
					b.doj as candidate_doj,
					a.candidateid as s_candidate_id,
					a.nationality as candidate_nationality,
					a.encryptedphotoname as candidate_encryptedphotoname,
					a.originalphotoname as candidate_originalphotoname,
					c.presenthno as candidate_presenthno,
					c.presentcity as candidate_presentcity,
					c.presentdistrict as candidate_presentdistrict,
					c.presentstreet as candidate_presentstreet,
					c.presentstateid as candidate_presentstateid,
					c.presentpincode as candidate_presentpincode,
					c.permanentstreet as candidate_permanentstreet,
					c.permanentcity as candidate_permanentcity,
					c.permanentstateid as candidate_permanentstateid,
					c.permanentdistrict as candidate_permanentdistrict,
					c.permanentpincode as candidate_permanentpincode,
					c.permanenthno as candidate_permanenthno,
					d.any_subject_of_interest as candidate_any_subject_of_interest,
					d.any_achievementa_awards as candidate_any_achievementa_awards,
					d.any_assignment_of_special_interest as candidate_any_assignment_of_special_interest,
					d.experience_of_group_social_activities as candidate_experience_of_group_social_activities,
					d.have_you_taken_part_in_pradan_selection_process_before as candidate_have_you_taken_part_in_pradan_selection_process_before,
					d.have_you_taken_part_in_pradan_selection_process_before_when as candidate_have_you_taken_part_in_pradan_selection_process_before_when,
					d.have_you_taken_part_in_pradan_selection_process_before_where as candidate_have_you_taken_part_in_pradan_selection_process_before_where,
					d.annual_income as candidate_annual_income,
					d.male_sibling as candidate_male_sibling,
					d.female_sibling as candidate_female_sibling,
					d.know_about_pradan as candidate_know_about_pradan,
					d.know_about_pradan_other_specify as candidate_know_about_pradan_other_specify,

					ct.*, des.* FROM `tbl_candidate_registration` as a 

					left join `staff` on  a.candidateid = `staff`.candidateid
					left join `tbl_generate_offer_letter_details` b ON a.candidateid = b.candidateid
					left join `tbl_offer_of_appointment` apt ON a.candidateid = apt.candidateid
					inner join `tbl_candidate_communication_address` c ON a.candidateid = c.candidateid
					inner join `tbl_other_information` d ON a.candidateid = d.candidateid
					inner join `mstcategory` as ct on a.categoryid = ct.id
					inner join `msdesignation` as des on ct.categoryname = des.desname
					WHERE a.`candidateid`= ".$token."");
$result = $query->row();
								/*echo  "<pre>";
								print_r($result);
								die;*/

								
								if (count($result) == 0) {
									$this->session->set_flashdata('er_msg', 'Please check !!! Record Not Found !!!');	
								}
								else
								{
									$StaffEmpnumber   = $result->emp_code;

									$teamid = $result->teamid;
									$dateofjoin = $result->doj;
									$designationid = $result->designationid;
								}
								if ($designationid  != 13) {

									@ $currentyear = $this->gmodel->getcurrentfyear();


									$sql = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms <= '".date('Y-m-d')."' Order by monthfroms DESC LIMIT 1 "; 
									$res = $this->db->query($sql)->row();

									if(count($res) > 0){  
										$Accuredvalue = $res->credit_value;
										$From_date = $res->monthfroms;
										$TO_date = $res->monthtos;

										$totalcrsql = "select SUM(credit_value) as credit_value from msleavecrperiod where CYear='".$currentyear."'"; 
										$totalcrres = $this->db->query($totalcrsql)->row();
										$perdayleave =  $totalcrres->credit_value/365;
										 $date1 =  date_create($dateofjoin); 
										  $date2 =  date_create($TO_date);
										$datesqldiff =  date_diff( $date1, $date2)->format('%a');
										
										$Noofdays=0;
										$Noofdays = $this->gmodel->roundvalue($datesqldiff*$perdayleave);
										$sql1 = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms = '".$From_date."'  "; 
										$res1 = $this->db->query($sql1)->row();

										$insertarray = array(
											'Emp_code'=> $StaffEmpnumber,
											'From_date'=> $From_date,
											'To_date'=> $TO_date,
											'Leave_type'=> 0,
											'Leave_transactiontype'=> 'CR',
											'Description' => 'Credit leaves posting',
											'Noofdays'=> $Noofdays,
											'appliedon'=> date('Y-m-d')
										);
									// echo "<pre>";
									// print_r($insertarray);
									// die;
										$this->db->insert('trnleave_ledger', $insertarray);

									}

								}
						



								$currentstatus  = $this->input->post('verified_status'); 

								$verificationarray = array(
									'status' =>3
								);
								$this->db->where('Candidateid', $token);
								$this->db->update('tbl_verification_document_hrd',$verificationarray);
								$oldcandidateupdate = array(
									'RoleID' =>3
								);
								$this->db->where('Candidateid', $token);
								$this->db->update('mstuser', $oldcandidateupdate);



				// echo $this->db->last_query();
				// die;
			// 	$hrdemailid    = $this->loginData->EmailID;
			// 		$FAemailid     = $this->gmodel->getFinanceAdministratorEmailid();
			// 		$addlink = site_url('login');




			// 		$candidatedetails = $this->model->getCandidate_BDFFormSubmit($token);
			// 		$cand_name = '';
			// 		$cand_name = $candidatedetails[0]->candidatefirstname;
			// 		$recipients = array('Hremailid' => $hrdemailid, 'tcemailid' => $sup_email,'FAemailid' => $FAemailid->financeemailid);
			// 		$subject = "Apprentice Codes";

			// 		$to_email = $candidateemailid;


			// 	 $candidate = array('$supervisor_name','$f_name','$StaffEmpnumber','$username','$password','$addlink');
			// 	 $candidate_replace = array($supervisor_name,$cand_name,$StaffEmpnumber,$username,$password,$addlink);

			// $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 31 AND `isactive` = '1'";
   //          $data = $this->db->query($sql)->row();
   // 			if(!empty($data))
   // 		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);


			// 		$sendmail = $this->Common_Model->send_email($subject,$body,$to_email,$to_name=null,$recipients);






								$this->session->set_flashdata('tr_msg', 'Successfully candidate join and document verification !!!');	
								redirect('Tc_verify_documents/');		



							}else{






								$currentstatus  = $this->input->post('verified_status'); 

								$verificationarray = array(
									'status' =>4
								);
								$this->db->where('status', $token);
								$this->db->update('tbl_verification_document_hrd');




								$hrdemailid    = $this->loginData->EmailID;
								$addlink = site_url('login');

								$subject = "HRD  Reject all documents verified and Emp. code generated";
								$candidatedetails = $this->model->getCandidate_BDFFormSubmit($token);
								$html = 'Dear '.$candidatedetails[0]->candidatefirstname.', <br><br> 
								Your joining formalities not completed.<br>
								Please Contact to pradan admin.<br>
								<br><br>.
								<br>Comments:- '. $this->input->post('comments').' ';

								$to_email = $hrdemailid;
								$to_name =  $candidateemailid;

								$sendmail = $this->Common_Model->send_email($subject, $html, $to_email,$to_name);


    	 //$body = read_file(base_url().'mailtext/verifieddocumentmailtext');

								$this->session->set_flashdata('tr_msg', 'Successfully candidate not join and document verification !!!');	
								redirect('Tc_verify_documents/');					


							}
						}

				///// currentstatus end here $currentstatus=1 //////////////////////// 



						$content['token'] = $token; 


						$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);


// echo "<pre>";
// print_r($content['getcandateverified']);
// die;

						$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);
						$content['getgapyearcount'] = $this->model->getCandategapyearcount($token);
						$content['getgapyearpverified'] = $this->model->getgapyeareCandateVerified($token);

						$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);



						$content['getdocuments'] = $this->model->getOtherDocuments();
						$content['countotherdoc'] = $this->model->getCountOtherDocument($token);
						$content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);

						$content['title'] = 'Tc_verify_documents';
						$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
						$this->load->view('_main_layout', $content);




					}catch (Exception $e) {
						print_r($e->getMessage());die;
					}

				}






				public function view($token)
				{
					try{

						$this->load->model('Tc_verify_documents_model');

						$content['token'] = $token; 

						$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

						$content['getcandateverified'] = $this->model->getCandateVerified($token);

						$content['getworkexpcount'] = $this->model->getCandateWorkExperincecount($token);

						$content['getworkexpverified'] = $this->model->getWorkExperinceCandateVerified($token);


						$content['getgapyearcount'] = $this->model->getCandategapyearcount($token);
						$content['getgapyearpverified'] = $this->model->getgapyeareCandateVerified($token);

						$content['getverified'] = $this->model->getVerifiedDetailes($token);
						$content['getverifiedstatus'] = $this->model->getverifiedstatus($token);

						$content['getgeneralnominationformpdf'] = $this->model->getGeneralNominationFormPDF($token);

						$content['getjoiningreportstatus'] = $this->model->getJoiningReportStatus($token);
						$content['getdocuments'] = $this->model->getOtherDocuments();
						$content['countotherdoc'] = $this->model->getCountOtherDocument($token);
						$content['getotherdocdetails'] =$this->model->getOtherDocumentDetails($token);

						$content['title'] = 'Tc_verify_documents';
						$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
						$this->load->view('_main_layout', $content);

					}catch (Exception $e) {
						print_r($e->getMessage());die;
					}
				}
				public function view_joininginduction($token)
				{

					try{
						$this->load->model('Tc_verify_documents_model');

						$content['token'] = $token; 

						$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit($token);

						$content['getcandateverified'] = $this->model->getCandateVerified($token);



						$content['getverified'] = $this->model->getVerifiedDetailes($token);


						$content['title'] = 'Tc_verify_documents';
						$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
						$this->load->view('_main_layout', $content);

					}catch (Exception $e) {
						print_r($e->getMessage());die;
					}
				}


				public function getTotalEmployees(){

					try{

   	//echo $candidateid; die;

						$query = $this->db->query("SELECT max(emp_code) as emp_code  FROM `tbl_da_personal_info`"); 
						$result = $query->result()[0];
    	// print_r($result);
    	// echo $result->emp_code; die;
						if($result->emp_code ==''){
							$num ='0001';
						}else{
							$num = $result->emp_code + 1;
							$len = strlen($num);
							for($i=$len; $i < 4; $i++) {
								$num = '0'.$num;
							}
						}
    	// echo $num; die;

						return $num;

					}catch (Exception $e) {
						print_r($e->getMessage());die;
					}


				}








			}