<?php 
class Staff_sepwhomitmayconcernlevelsixtermination extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepwhomitmayconcernlevelsixtermination_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			 redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
			// end permission    

			$query ="SELECT * FROM staff_transaction WHERE id=".$token;
			$content['staff_transaction'] = $this->db->query($query)->row();
			$content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
			$query ="SELECT * FROM staff_transaction WHERE staffid=".$content['staff_detail']->staffid;
			$content['staff_transaction_step'] = $this->db->query($query)->result();
			foreach ($content['staff_transaction_step'] as $value) {
				# code...
				$content['staff_transaction_step_detail'][] = $value;
			}


			$query ="SELECT * FROM tbl_levelwisetermination WHERE transid=".$token." AND type=2";
			$content['tbl_levelwisetermination'] = $this->db->query($query)->row();

			$tdate = date('d/m/Y');
			$staffname = $content['staff_detail']->name;
			$dc_name = $content['staff_detail']->dc_name;
			$sepdesig = $content['staff_detail']->sepdesig;
			$joiningdate = $this->gmodel->changedatedbformate($content['staff_detail']->joiningdate);

			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 84 AND `isactive` = '1'";
   		    $data = $this->db->query($sql)->row();

   		    if (!empty($content['tbl_levelwisetermination'])) {
   		    	$total_staff = $content['tbl_levelwisetermination']->total_staff;
				$total_professional = $content['tbl_levelwisetermination']->total_professional;
				$staff1 = array();
   		    	$staff1_replace = array();
				$staff1 = array('$tdate','$staffname','$dc_name','$sepdesig','$joiningdate','$total_staff','$total_professional');
				$staff1_replace = array($tdate,$staffname,$dc_name,$sepdesig,$joiningdate,$total_staff,$total_professional);

				if(!empty($data))
				$body = str_replace($staff1,$staff1_replace , $data->lettercontent);

				$body = str_replace('$total_staff',$total_staff, $body);
				$body = str_replace('$total_professional',$total_professional, $body);
				// echo $body; die();

			}
			else{
				$staff1 = array();
   		    	$staff1_replace = array();
				$staff1 = array('$tdate','$staffname','$dc_name','$sepdesig','$joiningdate','$total_staff','$total_professional');
				$staff1_replace = array($tdate,$staffname,$dc_name,$sepdesig,$joiningdate,'','');
				if(!empty($data))
					$body = str_replace($staff1,$staff1_replace , $data->lettercontent);
			}

			if(!empty($body))
   		    {
   		    	$content['content'] = $body;
   		    }

			$content['staff_transaction_detail'] = $this->Staff_sepwhomitmayconcernlevelsixtermination_model->staff_transaction_detail($content['staff_detail']->staffid);

			$RequestMethod = $this->input->server('REQUEST_METHOD');
        	if($RequestMethod == "POST"){
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'Save And Submit'){
					$db_flag = 1;
				}

				$query ="SELECT * FROM tbl_levelwisetermination WHERE transid=".$token." AND type=2";
				$content['tbl_levelwisetermination'] = $this->db->query($query)->row();

				$body = '';
				$body = $this->input->post('lettercontent');
				$filename = "";
				$filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'EXPERIENCECERTIFICATE.pdf');


				$data = array(
					'staffid'=>$content['staff_transaction']->staffid,
					'transid'=>$token,
					'desid'=>$content['staff_detail']->sepdesigid,
					'total_staff'=>$this->input->post('total_staff'),
					'total_professional'=>$this->input->post('total_professional'),
					'filename'		 	=> $filename,
					'content'		 => $body,
					'type'=>2,
        			'createdby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		); 

        		if($content['tbl_levelwisetermination']){
        			$data_update = array(
        			'staffid'=>$content['staff_transaction']->staffid,
        			'transid'=>$token,
        			'desid'=>$content['staff_detail']->sepdesigid,
        			'total_staff'=>$this->input->post('total_staff'),
        			'total_professional'=>$this->input->post('total_professional'),
        			'filename'		 	=> $filename,
					'content'		 => $body,
        			'type'=>2,
        			'updatedon'=>date('Y-m-d H:i:s'),
        			'updatedby'=>$this->loginData->staffid,
        			'flag'=>$db_flag
        		);
					$this->db->where('id', $content['tbl_levelwisetermination']->id);
					$flag = $this->db->update('tbl_levelwisetermination', $data_update);
				}else{
					$flag = $this->db->insert('tbl_levelwisetermination',$data);
				}
				
				if($flag) {
					$subject = "Your Request Submited Successfully";
				  $body = 'Dear,<br><br>';
				  $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
				  $body .= 'This is to certify that Mr. '.$content['staff_detail']->name.'<br> has issued experience certificate.<br><br>';
				  $body .= 'Thanks<br>';
				  $body .= 'Administrator<br>';
				  $body .= 'PRADAN<br><br>';

				  $body .= 'Disclaimer<br>';
				  $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

				  $to_email = $content['staff_detail']->emailid;
				  $to_name = $content['staff_detail']->name;
				  $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name);
				  if (substr($email_result, 0, 5) == "ERROR") {
				  	$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
				  }
					$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
				}else {
					$this->session->set_flashdata('er_msg','Something Went Wrong!!');
				}
      }
			
			$content['title'] = 'Staff_sepwhomitmayconcernlevelsixtermination';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
                print_r($e->getMessage());
                die();
		}
		
	}

	
}