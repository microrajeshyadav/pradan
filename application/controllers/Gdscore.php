<?php 

class Gdscore extends CI_Controller
{
	


	function __construct()
	{

		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Gdscore_model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model'; 
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{

		try{

		// start permission 
			$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
// end permission
			if($this->input->post('campusid') !=null && $this->input->post('campusid') !=''){
				$campusdata = $this->input->post('campusid');
				$campexp = explode('-', $campusdata);
				$campusid = $campexp[0];
				$campusintimationid = $campexp[1];
				// echo $campusdata;exit();
			}else{
				$campusid = 'NULL';
				$campusintimationid = 'NULL';
				$campusdata = 'NULL';
			}


			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

				//print_r($this->input->post()); die;

				$savegdscore = $this->input->post('save_gdscore');
				$sendintimation = $this->input->post('send_intimation');
				
				if (isset($sendintimation) && $sendintimation =='SendIntimation') {

					$candidateintimation = $this->input->post('candidateintimation');
					$GdpercentageVal = $this->input->post('gdpercentage');

					$cutoffmarks = $this->input->post('cutoffmarks');
					$gd=$this->input->post('gd');
			//print_r($gd); die;
					$message ='';
					// $filenameread = "mailtext/Shortlisted_GD_Submission.txt";
					// $fd = fopen($filenameread, "r"); 
					// $message .=fread($fd,filesize($filenameread));

					foreach ($candidateintimation as $key => $value	) {

				//echo floor($GdpercentageVal[$value]); die;

						if ($gd[$value] >= $cutoffmarks) {

							
							$candidateid = $value;
			

							$candidateemail = $this->input->post('emailid')[$value]; 
							$candidatefirstname  =$this->input->post('candidatefname')[$value];  
							// $candidatemiddlename =$this->input->post('candidatemname')[$value]; 
							$candidatelastname = $this->input->post('candidatelname')[$value];
							$add_link = site_url('login/login1');
							$subject = 'Shortlisted in GD on submission';

							$candidate = array('$candidateemail','$candidatefirstname','$candidatelastname','$addlink');
							$candidate_replace = array($this->input->post('emailid')[$value],$this->input->post('candidatefname')[$value],$this->input->post('candidatelname')[$value],$add_link);


							// eval ("\$message = \"$message\";");
							// $message =nl2br($message);
							// $body = $message; 
							//update from db
						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 12 AND `isactive` = '1'";
   		    			$data = $this->db->query($sql)->row();
   		    			 if(!empty($data))
   		    			$body = str_replace($candidate,$candidate_replace , $data->lettercontent);
							// print_r($body); die;
				$sendmail = $this->Common_Model->send_email($subject,$body,$candidateemail);  //// Send Mail candidates fill BFD Form ////

				if ($sendmail == TRUE) {

					$this->db->trans_start();

					$UpdateArr = array(
						'username' => $candidateemail,
						'password'  => md5($candidatefirstname),
						'gdemail_status'  => 1,
					);
					$this->db->where('candidateid',$value);
					$this->db->update('tbl_candidate_registration', $UpdateArr);
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg','Sorry !!! Username and Password is not generated !!!!'); 
					}else{
						$this->session->set_flashdata('tr_msg','Successfully  Username and Password is generated!!!!');	
					}
				}
			}else{
				$this->session->set_flashdata('er_msg','Sorry !!! GD Score lessthen of cut off marks. Please Try Again !!!!'); 
			}
		}

		redirect('Gdscore/index');
	}


	if (isset($savegdscore) && $savegdscore =='save') {

		$this->db->trans_start();

		$updateArr = array(
			'campus_gd_cutoffmarks' => ($this->input->post('cutoffmarks') ==''? 0: $this->input->post('cutoffmarks')),
			'campus_status'       => 0,
			'updatedon'           => date('Y-m-d H:i:s'),
				'updatedby'          => $this->loginData->UserID, // login user id
			);

		$this->db->where('id', $campusintimationid);
		$this->db->where('campusid', $campusid);
		$this->db->update('tbl_campus_intimation', $updateArr);

				//echo $this->db->last_query(); die;


		$GdscoreVal = $this->input->post('gd');

		$SsscoreVal = $this->input->post('ssscore'); 

		$RsscoreVal = $this->input->post('rsscore'); 
		$gdpercentage=$this->input->post('gdpercentage');


		foreach ($RsscoreVal as $key => $value) {

			$this->db->where('candidateid', $key);
			$num_rows =  $this->db->count_all_results('tbl_candidate_gdscore'); 

			if($num_rows > 0 ){

				$sql = "SELECT * FROM `tbl_candidate_gdscore` where `candidateid`=$key"; 
				$query = $this->db->query($sql);
				$result = $this->db->query($sql)->result();
				$gdscoreid = $result[0]->gdscoreid;

				$updateArr = array(
					'candidateid'    		    => $key,
					'rsscore'    		        => ($value ==''? 0: $value),
					'flag'						=> 'save',
					'cutoffmarks'			    => ($this->input->post('cutoffmarks') ==''? 0: $this->input->post('cutoffmarks')),
					'updatedon'      		    => date('Y-m-d H:i:s'),
						'updatedby'      		    => $this->loginData->UserID, // login user id
					);


				$this->db->where('gdscoreid', $gdscoreid);
				$this->db->update('tbl_candidate_gdscore', $updateArr);
								// echo $this->db->last_query();
								// die;

				$candidateupdateArr = array(
					'wstatus'    		    => 0,
					'gdstatus'    		    => 0,


				);

				$this->db->where('candidateid', $key);
				$this->db->update('tbl_candidate_registration', $candidateupdateArr);
					           //echo $this->db->last_query();

			}else{

				$insertArr = array(
					'candidateid'    	     => $key,
					'rsscore'    		     => ($value ==''? 0: $value),
					'flag'			         => 'save',
					'cutoffmarks'			=> ($this->input->post('cutoffmarks') ==''? 0: $this->input->post('cutoffmarks')),
					'createdon'      	     => date('Y-m-d H:i:s'),
					    'createdby'      		 => $this->loginData->UserID, // login user id
					);

				$this->db->insert('tbl_candidate_gdscore', $insertArr);
								//echo $this->db->last_query();

				$candidateupdateArr = array(
					'wstatus'    		    => 0,
					'gdstatus'    		    => 0,
				);

				$this->db->where('candidateid', $key);
				$this->db->update('tbl_candidate_registration', $candidateupdateArr);
			}

		} 


		foreach ($SsscoreVal as $key => $value) {

			$this->db->where('candidateid', $key);
			$num_rows =  $this->db->count_all_results('tbl_candidate_gdscore'); 

			if($num_rows > 0 ){

				$sql = "SELECT * FROM `tbl_candidate_gdscore` where `candidateid`=$key"; 
				$query = $this->db->query($sql);
				$result = $this->db->query($sql)->result();
				$gdscoreid = $result[0]->gdscoreid;

				$updateArr = array(
					'candidateid'    		    => $key,
					'ssscore'    		        => $value,
					'flag'						=> 'save',
					'updatedon'      		    => date('Y-m-d H:i:s'),
	    					'updatedby'      		    => $this->loginData->UserID, // login user id
	    				);

				$this->db->where('gdscoreid', $gdscoreid);
				$this->db->update('tbl_candidate_gdscore', $updateArr);
								//echo $this->db->last_query();

			}else{

				$insertArr = array(
					'candidateid'    => $key,
					'ssscore'    	 => $value ,
					'flag'			 => 'save',
					'createdon'      => date('Y-m-d H:i:s'),
					'createdby'      => $this->loginData->UserID
				);

				$this->db->insert('tbl_candidate_gdscore', $insertArr);
								//echo $this->db->last_query();
			}

		}

		foreach($GdscoreVal as $key => $value) { 


			$this->db->where('candidateid', $key);
			$num_rows =  $this->db->count_all_results('tbl_candidate_gdscore'); 

			if($num_rows > 0 ){

				$sql = "SELECT * FROM `tbl_candidate_gdscore` where `candidateid`=$key"; 
				$query = $this->db->query($sql);
				$result = $this->db->query($sql)->result();
				$gdscoreid = $result[0]->gdscoreid;

				$updateArr = array(
					'candidateid'    		    => $key,
					'gdscore'    		        => $value,
					'flag'						=> 'save',
					'updatedon'      		    => date('Y-m-d H:i:s'),
	    					'updatedby'      		    => $this->loginData->UserID, // login user id
	    				);

				$this->db->where('gdscoreid', $gdscoreid);
				$this->db->update('tbl_candidate_gdscore', $updateArr);
							//echo $this->db->last_query();
			}else{

				$insertArr = array(
					'candidateid'    	        => $key,
					'gdscore'    		        => ($value ==''? 0: $value),
					'flag'						=> 'save',
					'createdon'      		    => date('Y-m-d H:i:s'),
	    				'createdby'      		    => $this->loginData->UserID, // login user id
	    			);

				$this->db->insert('tbl_candidate_gdscore', $insertArr);
							//echo $this->db->last_query();
			}

		}



					//percentage 

		foreach($gdpercentage as $key => $value) {

			$this->db->where('candidateid', $key);
			$num_rows =  $this->db->count_all_results('tbl_candidate_gdscore'); 
				//echo $num_rows; 
					//die;

			if($num_rows > 0 ){

				$sql = "SELECT * FROM `tbl_candidate_gdscore` where `candidateid`=$key"; 
				$query = $this->db->query($sql);
				$result = $this->db->query($sql)->result();
				$gdscoreid = $result[0]->gdscoreid;

				$updateArr = array(
					'candidateid'    		    => $key,
					'gdpercentage'    		        => $value,
					'flag'						=> 'save',
					'updatedon'      		    => date('Y-m-d H:i:s'),
	    				'updatedby'      		    => $this->loginData->UserID, // login user id
	    			);

				$this->db->where('gdscoreid', $gdscoreid);
				$this->db->update('tbl_candidate_gdscore', $updateArr);

			}else{

				$insertArr = array(
					'candidateid'    	        => $key,
					'gdpercentage'    		        => $value,
					'flag'						=> 'save',
					'createdon'      		    => date('Y-m-d H:i:s'),
	    				'createdby'      		    => $this->loginData->UserID, // login user id
	    			);

				$this->db->insert('tbl_candidate_gdscore', $insertArr);

			}

		}


		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){

			$this->session->set_flashdata('er_msg', 'Error adding GD Score');	
		}else{

			$this->session->set_flashdata('tr_msg', 'Successfully added GD Score');			
		}
		redirect('/Gdscore/index');
	}
}

$content['campusid'] = $campusdata;
$content['selectedcandidatedetails'] = $this->model->getSelectedCandidate($campusid,$campusintimationid);
$content['campusdetails'] = $this->model->getCampus($this->loginData->UserID);
$content['selectedcandidategdscore'] = $this->model->getSelectedCandidateGDScore($campusid,$campusintimationid);
		//echo "<pre>";
		//print_r($content['selectedcandidategdscore']);

$content['campuscutoffmarks'] = $this->model->getCampusCutOffMarks($campusid, $campusintimationid);

			//print_r($selectedcandidategdscore); die;

$content['title'] 		= 'Gdscore';
$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
	print_r($e->getMessage());die;
}

}





public function sendintimationtocandidate($token){

	try{


			//echo $token; die; 


		//print_r($this->loginData); die;

			 $getCandidateDetails = $this->model->getCandidateDetails($token); //// e;

			// print_r($getCandidateDetails); die;
			 $candidateid = $getCandidateDetails[0]->candidateid;
		    $candidateemail = $getCandidateDetails[0]->emailid;  //// Email Id ////
			$candidatfirstname   = $getCandidateDetails[0]->candidatefirstname;  //// 
			$addlink = site_url('login/login1');

			$html = 'Dear '.$candidatfirstname.', <br><br> 
			Congratulations you are selected by Pradan. 
			<br><br> Username - '.$candidateemail.'
			<br>Password - '.$candidatfirstname.'<br>
			Please <a href='.$addlink.'>Click here</a>';


			 $sendmail = $this->Common_Model->send_email($subject = 'Job Offer letter ', $message = $html, $candidateemail);  //// Send Mail candidates fill BFD Form ////

			 if ($sendmail == false) {
			 	$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send to HRD. Please Try Again !!!!'); 
			 }else{



			 	$UpdateArr = array(
			 		'username' => $candidateemail,
			 		'password'  => md5($candidatfirstname),
			 	);

			 	$this->db->where('candidateid',$token);
			 	$this->db->update('tbl_candidate_registration', $UpdateArr);

			 	$this->session->set_flashdata('tr_msg','Successfully Intimation !!!!');				
			 }

			 redirect('Gdscore/index');

			//$content['sentmail']      = $this->SentMail($token);
			 $content['title'] = 'Gdscore';
			 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			 $this->load->view('_main_layout', $content);


			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}
		}





		public function Selecttogdscoreexamcandidates()
		{

			try{
		// start permission 
				$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
// end permission
				$campexp = '';
				$campusid = 0;
				$campusintimationid = 0;
				$campusdata = $this->input->post('campusid');
				$campexp = explode('-', $campusdata);

				$campusid = $campexp[0];
				@	$campusintimationid = $campexp[1];

				if ($campusid =='' && $campusintimationid =='') {
					$campusid = 'NULL';
					$campusintimationid = 'NULL';
				}

				$content['selectedcandidatedetails']  = $this->model->getSelectedCandidate($campusid, $campusintimationid);
				$content['campusdetails'] 	          = $this->model->getCampus($this->loginData->UserID);
				$content['selectedcandidategdscore'] = $this->model->getSelectedCandidateGDScore($campusid, $campusintimationid);

				$content['title'] 		= 'Gdscore';
				$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);

			}catch (Exception $e) {
				print_r($e->getMessage());die;
			}

		}


		function delete($token = null)
		{


			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Gdscore/index');
				
			} 
			else { 

				$this->Common_Model->delete_row('mstrecruiters','recruiterid', $token); 
				$this->session->set_flashdata('tr_msg' ,"Recruiters Deleted Successfully");
				redirect('/Score/');
				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);
			}
		}


	}