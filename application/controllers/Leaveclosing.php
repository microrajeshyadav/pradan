<?php 
class Leaveclosing extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Leaveclosing_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');
		// print_r($check); die();

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');/*
		echo "<pre>";
		print_r($this->loginData);exit();*/

	}

	public function index()
	{
		try{

		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission  
		

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

				// echo "<pre>";
				// print_r($this->input->post()); 
			$period = $this->input->post('period');
			$expperiod = explode('to', $period);
			$monthfroms = $expperiod[0];
			$monthtos = $expperiod[1];

			if(!empty($period))
			{


			$sql = "SELECT count(id) as countid, monthfroms,monthtos FROM `msleavecrperiod` WHERE `monthtos` > '".$monthtos."' ORDER BY monthfroms ASC LIMIT 1";
			$result = $this->db->query($sql)->row();

			

			if ($result->countid > 0) {

				$fromdate = $result->monthfroms;
				$todate = $result->monthtos;

				$checktrnsql = "SELECT count(id) as counttrnleaveid FROM `trnleave_ledger` WHERE 
				`From_date`= '".$fromdate."' AND Leave_transactiontype = 'OB' AND Leave_type = 0 AND Description = 'Leave opening balance' ";

			    $checktrnresult = $this->db->query($checktrnsql)->row();
			 if($checktrnresult->counttrnleaveid > 0) {
				$this->session->set_flashdata('tr_msg', "Opening balance already exist for next period!!!");
				redirect('Leaveclosing/index/');

			  }else{

			  	$sql = "SELECT
				staff.emp_code,
				 0 as Leave_type,
				 'OB' AS Leave_transactiontype,
				 'Leave opening balance' as Description, CASE WHEN CAST(( CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE  DATEDIFF('".$monthtos."', EXP.Date_of_transfer) END - IFNULL(trnleave_ledger.LWP, 0))/ 365  as decimal(5,1)) > 10 AND( IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) - IFNULL(trnleave_ledger.DLWP, 0) ) > 348 THEN 348 WHEN CAST(( CASE WHEN EXP.Date_of_transfer IS NULL THEN 0 ELSE  DATEDIFF('".$monthtos."', EXP.Date_of_transfer) END - IFNULL(trnleave_ledger.LWP, 0))/ 365  as decimal(5,1)) <= 10 AND (IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) - IFNULL(trnleave_ledger.DLWP, 0) ) > 300 THEN 300 ELSE( IFNULL(trnleave_ledger.OB, 0) + IFNULL(trnleave_ledger.Accured, 0) - IFNULL(trnleave_ledger.Availed, 0) - IFNULL(trnleave_ledger.DLWP, 0) ) END AS noofdays FROM ( SELECT `trnleave_ledger`.`Emp_code`, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'OB' AND Leave_type = 0 AND From_date = '".$monthfroms."' THEN Noofdays ELSE 0 END, 0 ) ) OB, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'CR' AND Leave_type = 0 THEN Noofdays ELSE 0 END, 0 ) ) Accured, SUM( IFNULL( tl.nofdays, 0 ) ) DLWP, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 9 AND (STATUS
        = 3 OR STATUS= 1) THEN Noofdays ELSE 0 END, 0 ) ) Availed, SUM( IFNULL( CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 5 AND (STATUS
        = 3 OR STATUS= 1) THEN Noofdays ELSE 0 END, 0 ) ) LWP FROM `trnleave_ledger` LEFT JOIN (SELECT SUM(Noofdays) as nofdays,Emp_code,status as sid FROM `trnleave_ledger` WHERE Leave_transactiontype = 'CR' AND Leave_type = 100 AND Description = 'DLWP' GROUP BY Emp_code, status ) as tl on `trnleave_ledger`.`Emp_code` = tl.`Emp_code` And `trnleave_ledger`.id = tl.sid And `trnleave_ledger`.status = 3 WHERE From_date >= '".$monthfroms."' AND to_date <= '".$monthtos."' GROUP BY `trnleave_ledger`.Emp_code ) AS trnleave_ledger INNER JOIN staff ON trnleave_ledger.Emp_code = staff.emp_code LEFT JOIN `msdesignation` AS msd ON staff.designation = msd.desid INNER JOIN `lpooffice` AS lpo ON staff.new_office_id = lpo.officeid LEFT JOIN ( SELECT staff.Staffid, case when st1.Date_of_transfer is NULL then st.Date_of_transfer else st1.Date_of_transfer end Date_of_transfer FROM `staff` INNER JOIN staff_transaction st ON staff.staffid = st.staffid AND st.new_designation != 13 AND st.trans_status = 'JOIN' lEFT JOIN staff_transaction_old as st1 on st.staffid = st1.staffid AND st1.new_designation != 13 AND st1.trans_status = 'JOIN' ) EXP ON staff.staffid = EXP.staffid WHERE staff.status = 1 GROUP BY staff.emp_code, staff.Name ORDER BY staff.emp_code ";

// print_r($sql);  die;
   	// 		$sql = "SELECT
				// staff.emp_code,
				// 0 as Leave_type,
				// 'OB' AS Leave_transactiontype,
				// 'Leave opening balance' as Description,

				// CASE WHEN FLOOR(
				// DATEDIFF(CURDATE(),
				// EXP.Date_of_transfer) / 365) > 10 AND(
				// IFNULL(trnleave_ledger.OB,
				// 0) + IFNULL(trnleave_ledger.Accured,
				// 0) - IFNULL(trnleave_ledger.Availed,
				// 0) - IFNULL(trnleave_ledger.DLWP,
				// 0)
				// ) > 348 THEN 348 WHEN FLOOR(
				// DATEDIFF(CURDATE(),
				// EXP.Date_of_transfer) / 365) <= 10 AND(
				// IFNULL(trnleave_ledger.OB,
				// 0) + IFNULL(trnleave_ledger.Accured,
				// 0) - IFNULL(trnleave_ledger.Availed,
				// 0) - IFNULL(trnleave_ledger.DLWP,
				// 0)
				// ) > 300 THEN 300 ELSE(
				// IFNULL(trnleave_ledger.OB,
				// 0) + IFNULL(trnleave_ledger.Accured,
				// 0) - IFNULL(trnleave_ledger.Availed,
				// 0) - IFNULL(trnleave_ledger.DLWP,
				// 0)
				// ) END AS noofdays
				// FROM
				// (
				// SELECT
				// `trnleave_ledger`.`Emp_code`,
				// SUM(
				// IFNULL(
				// CASE WHEN Leave_transactiontype = 'OB' AND Leave_type = 0 AND From_date = '".$monthfroms."' THEN Noofdays ELSE 0 END,
				// 0
				// )
				// ) OB,
				// SUM(
				// IFNULL(
				// CASE WHEN Leave_transactiontype = 'CR' AND Leave_type = 0 THEN Noofdays ELSE 0 END,
				// 0
				// )
				// ) Accured,
				// SUM(
				// IFNULL(
				// tl.nofdays, 
				// 0
				// )
				// ) DLWP,
				// SUM(
				// IFNULL(
				// CASE WHEN Leave_transactiontype = 'DR' AND Leave_type = 9 AND STATUS = 3 THEN Noofdays ELSE 0 END,
				// 0
				// )
				// ) Availed
				// FROM
				// `trnleave_ledger`
				// LEFT JOIN (SELECT SUM(Noofdays) as nofdays,Emp_code,status as sid FROM  `trnleave_ledger` WHERE Leave_transactiontype = 'CR' AND Leave_type = 100 AND Description = 'DLWP'  GROUP BY Emp_code, status ) as tl on `trnleave_ledger`.`Emp_code` = tl.`Emp_code` And `trnleave_ledger`.id = tl.sid And `trnleave_ledger`.status = 3
				// WHERE
				// From_date >= '".$monthfroms."' AND to_date <= '".$monthtos."'
				// GROUP BY
				// `trnleave_ledger`.Emp_code
				// ) AS trnleave_ledger
				// INNER JOIN
				// staff ON trnleave_ledger.Emp_code = staff.emp_code
				// LEFT JOIN
				// `msdesignation` AS msd ON staff.designation = msd.desid
				// INNER JOIN
				// `lpooffice` AS lpo ON staff.new_office_id = lpo.officeid
				// LEFT JOIN
				// (
				// SELECT
				// staff.Staffid,
				// Date_of_transfer
				// FROM
				// `staff`
				// INNER JOIN
				// staff_transaction st ON staff.staffid = st.staffid AND st.new_designation != 13 AND trans_status = 'JOIN'
				// ) EXP ON staff.staffid = EXP.staffid
				// WHERE
				// staff.status = 1
				// GROUP BY
				// staff.emp_code,
				// staff.Name
				// ORDER BY
				// staff.emp_code";

				//echo $sql; die;

				$NPresult = $this->db->query($sql)->result();


				foreach ($NPresult as $key => $value) {

					$npinsertarray = array(
						'Emp_code'=> $value->emp_code,
						'From_date'=> $fromdate,
						'To_date'=> $todate,
						'Leave_type'=> $value->Leave_type,
						'Leave_transactiontype'=> 'OB',
						'Description' => $value->Description,
						'Noofdays'=>  $value->noofdays,
						'appliedon'=> date('Y-m-d')
					);
					// echo "<pre>";
					// print_r($npinsertarray);
                 $this->db->insert('trnleave_ledger', $npinsertarray);
				}

				

			}


			}else{

				$this->session->set_flashdata('tr_msg', "Kindly enter next period entry under leave credit rule to process next period opening balance !!!");
				redirect('Leaveclosing/index/');

			}
		}
		else{
			$this->session->set_flashdata('er_msg', "Kindly select period to search");
				redirect('Leaveclosing/index/');
		}


		}

		$content['leavecategory'] = $this->model->getleavectegory();
		$content['fyear'] = $this->gmodel->getcurrentfyear();

		$content['creditperiod'] = $this->model->getCurrentfyearperiod($content['fyear']);
		$fromdate; $todate;
		foreach ($content['creditperiod'] as $key => $value) {
			if (date('Y-m-d') <= $value->monthtos && date('Y-m-d') >= $value->monthfroms) {
				$fromdate = $value->monthfroms;
				$todate = $value->monthtos;
			}
		}

		if (empty($fromdate) &&  empty($todate)) {
			foreach ($content['creditperiod'] as $key => $value) {
			if (date('Y-m-d') >= $value->monthtos) {
				$fromdate = $value->monthfroms;
				$todate = $value->monthtos;
			}
		}
		}
		$content['leavebalance'] = $this->model->getleavedetail_balance($fromdate, $todate);

		//$content['leaverequest'] = $this->model->getleavedetails('1');
		$content['title'] = 'Leaveclosing';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
		
	}

	


	public function getcurrentfyear(){
		try{

		if(date('m') >= 03) {
			$d = date('Y-m-d', strtotime('+1 years'));
			$currentyear =  date('Y') .'-'.date('Y', strtotime($d));
		} else {
			$d = date('Y-m-d', strtotime('-1 years'));
			$currentyear = date('Y', strtotime($d)).'-'.date('Y');
		}
		return $currentyear;
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


	public function NextperiodOpningBalance(){
		try{
		//$currentmonth = date('m');
		//$currentyear = date('Y');

		$currentyear = $this->getcurrentfyear();

		$checknextperiodobavailaibility = $this->Leaveclosing_model->checkNextperiodopningbalance($currentyear);
		//echo $checkaccuredavailaibility;exit();
		if($checknextperiodobavailaibility){
			$this->session->set_flashdata('tr_msg', "Closing balance applied to all existing user.");
			redirect('Leaveclosing/index/'.$this->loginData->staffid);
		}else{
			$this->session->set_flashdata('er_msg', "Closing balance not available");
			redirect('Leaveclosing/index/'.$this->loginData->staffid);
		}
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}





}