<?php 
class Probation_edirector_reviewofperformance extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Probation_edirector_reviewofperformance_model","model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['title'] = 'Probation_edirector_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function add($token)
	{
		// start permission 
		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$this->load->model("Probation_edirector_reviewofperformance_model","model");

			$getstaffdetail = $this->Probation_edirector_reviewofperformance_model->getstaffid($token);
			$staffid = $getstaffdetail->staffid;

			$Edid = $this->Probation_edirector_reviewofperformance_model->getEDid();



			
			// print_r($getstaffdetail); die;
			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				$this->db->trans_start();

				$date_of_appointment = $this->input->post('date_of_appointment');
				$date_of_appointment_db = $this->gmodel->changedatedbformate($date_of_appointment);

				$period_of_review_from = $this->input->post('period_of_review_from');
				$period_of_review_from_db = $this->gmodel->changedatedbformate($period_of_review_from);

				$period_of_review_to = $this->input->post('period_of_review_to');
				$period_of_review_to_db = $this->gmodel->changedatedbformate($period_of_review_to);

				$probation_extension_date = $this->input->post('probation_extension_date');
				$probation_extension_date_db = $this->gmodel->changedatedbformate($probation_extension_date);

				$period_latestby = $this->input->post('latestby');
				$period_latestby_db = $this->gmodel->changedatedbformate($period_latestby);


				$insertArr = array(
					'transid'                     => $token,
					'staffid'                     => $this->input->post('staffid'),
					'date_of_appointment'         => $date_of_appointment_db,
					'period_of_review_from'       => $period_of_review_from_db,
					'period_of_review_to'         => $period_of_review_to_db,
					'work_habits_and_attitudes'   => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity' => $this->input->post('conduct_and_social_maturity'),
					'integrity'                   => $this->input->post('questionable'),
					'any_other_observations'      => $this->input->post('any_other_observations'),
					'latestby'                    => $period_latestby_db,
					'satisfactory'                => $this->input->post('satisfactory'),
					'probation_completed'         => $this->input->post('probation_completed'),
					'probation_extension_date'    => $probation_extension_date_db,

					'flag'                        => 1,
					'createdby'                   => $this->loginData->staffid,
					'createdon'                   => date('Y-m-d H:i:s'),
				);

				$this->db->insert('tbl_probation_review_performance', $insertArr);

				$insertid = $this->db->insert_id();   

				$updateArr = array(
					
					'trans_flag'           => 3,
					'updatedon'            => date("Y-m-d H:i:s"),
					'updatedby'            => $this->loginData->staffid,
				);
				$this->db->where('id', $token);
				$this->db->update('staff_transaction', $updateArr);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => 11,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 3,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
				// echo "<pre>";
				// print_r($insertworkflowArr); die;
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				$this->db->trans_complete();
				if ($this->db->trans_status() === true){
					$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
					redirect('/Probation_edirector_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
					redirect(current_url());
				}
			}

			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
			//$content['getstaffPnndetails'] = $this->model->getstaffPnndetails();
			$content['title'] = 'Probation_edirector_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}

	


	public function edit($token)
	{
		// start permission 
		try{
			
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($getSupervisordetals);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
		// print_r($getstaffprobationdetail);

			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
		   // echo "<pre>";
		  	// print_r($this->input->post()); die;

				$this->db->trans_start();
				$extended_date = $this->input->post('extended_date');
				$extended_date_db = $this->gmodel->changedatedbformate($extended_date);

				$insertArr = array(
					'transid'                               => $token,
					'staffid'                               => $this->input->post('staffid'),
					'satisfactory'                          => $this->input->post('satisfactory'),
					'probation_completed'                   => $this->input->post('completed'),
					'reasons_for_not_above_recommendations' => $this->input->post('not_recommended'),
					'probation_extension_date'              =>  $extended_date_db,
					
					'work_habits_and_attitudes'             => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity'           => $this->input->post('conduct_and_social_maturity'),
					'any_other_observations'                => $this->input->post('any_other_observations'),
					'integrity'                             => $this->input->post('questionable'),
					'flag'                                  => 4,
					'updatedby'                             => $this->loginData->staffid,
					'updatedon'                             => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				// echo $this->db->last_query(); die;

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $token,
					'sender'               => $this->loginData->staffid,
					'receiver'             => 11,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 4,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				$this->db->trans_complete();

				if ($this->db->trans_status() === true){


					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_edirector_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been sent to ED successfully !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			//$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_edirector_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}



	public function ededit($token)
	{
		// start permission 
		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($getSupervisordetals);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
		// print_r($getstaffprobationdetail);

			$probationid = $getstaffprobationdetail->id;
			$staffid = $getstaffprobationdetail->staffid;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
		  // echo "<pre>";
		  //	print_r($this->input->post()); die;

				$this->db->trans_start();
				$ed_date = $this->input->post('ed_date');
				$ed_date_db = $this->gmodel->changedatedbformate($ed_date);

				$insertArr = array(
					
					'ed_comments'          => $this->input->post('ed_comments'),
					'ed_date'              => $ed_date_db,
					'flag'				   => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);
				// print_r($insertArr); die;
				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $this->input->post('review_by_id'),
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => $this->input->post('edstatus'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


				$this->db->trans_complete();

				if ($this->db->trans_status() === true){
					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_edirector_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been sent to ED successfully !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_edirector_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function view($token)
	{

		
		try{
			
			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Ed_staff_approval/index');
				
			} else {

			// echo "gguy"; die;
				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
		// end permission    
				$content['ed'] = $this->model->getEDid(); 
				$img = $content['ed']->edid;

				
				$content['edsignature'] = $this->Common_Model->staff_signature($img);

			

				$content['getstaffprovationreviewperformance'] = $this->model->getstaffid($token);
				$staffmail = $content['getstaffprovationreviewperformance']->emailid; 

				$content['getSupervisordetals'] = $this->model->getSupervisor($token);
		//print_r($content['getSupervisordetals']);

				$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			// echo "<pre>";
		 // print_r($getstaffprobationdetail); die;

				$content['getstaffPdetails'] = $this->model->getStaffPersonnalList();
			// print_r($content['getstaffPdetails']); die;

				$content['getstaffPdetails']->staffid;
				$pmail = $content['getstaffPdetails']->EmailID; 

				$content['pname'] = $this->model->getpname($content['getstaffPdetails']->staffid);

				$probationid = $getstaffprobationdetail->id;
				$staffid = $getstaffprobationdetail->staffid; 

				$workflowdetail = $this->model->get_providentworkflowid($staffid);
			// print_r($workflowdetail); die;	

				$content['getstaffprobationreview'] = $getstaffprobationdetail;

				$RequestMethod = $this->input->server('REQUEST_METHOD');
		    /*echo "<pre>";
		    print_r($content); die;*/

		    if($RequestMethod == 'POST'){

		    	$status = $this->input->post('edstatus');

		    	$this->db->trans_start();

		    	$ed_date = $this->input->post('ed_date');
		    	$ed_date_db = $this->gmodel->changedatedbformate($ed_date);

		    	$insertArr = array(

		    		'ed_comments'          => (trim($this->input->post('ed_comments'))==""?null:$this->input->post('ed_comments')),
		    		'ed_date'              => $ed_date_db,
		    		'flag'				   => $status,
		    		'agree'	               => (trim($this->input->post('recommendations'))==""?null:$this->input->post('recommendations')),
		    		'updatedby'            => $this->loginData->staffid,
		    		'updatedon'            => date('Y-m-d H:i:s'),
		    	);
				// print_r($insertArr); die;
		    	$this->db->where('id',$probationid);
		    	$this->db->update('tbl_probation_review_performance', $insertArr);

		    	$insertworkflowArr = array(
		    		'r_id'                 => (trim($token)==""?null:($token)),
		    		'type'                 => 7,
		    		'staffid'              => $staffid,
		    		'sender'               => $this->loginData->staffid,
		    		'receiver'             => (trim($this->input->post('sendtohtd'))==""?null:$this->input->post('sendtohtd')),
		    		'senddate'             => date("Y-m-d H:i:s"),
		    		'forwarded_workflowid' => (trim($workflowdetail->workflowid)==""?null:($workflowdetail->workflowid)),
		    		'flag'                 => $this->input->post('edstatus'),
		    		'createdon'            => date("Y-m-d H:i:s"),
		    		'createdby'            => $this->loginData->staffid,
		    	);
				// echo "<pre>";
				// print_r($insertworkflowArr); die;
		    	$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


		    	$insertArr = array (

		    		'trans_flag'      => $this->input->post('edstatus'),
		    		'updatedby'       => $this->loginData->staffid,
		    		'updatedon'       => date('Y-m-d H:i:s'),
		    	);
				// print_r($insertArr); die;
		    	$this->db->where('id',$token);
		    	$this->db->update('staff_transaction', $insertArr);

		    	if($this->input->post('edstatus')==6)
		    	{
		    	$insertstaffArr = array(
								'probation_status'	   => 0,
								'updatedby'            => $this->loginData->staffid,
								'updatedon'            => date('Y-m-d H:i:s'),
							);


							$this->db->where('staffid',$staffid);
							$this->db->update('staff', $insertstaffArr);

						}


		    	// $html = 'Dear '.$content['getstaffprovationreviewperformance']->name.', <br><br> 
		    	// Probation review of process. 
		    	// <br>
		    	// <br><br>
		    	// Regard,<br>Pradan Team';



		    	// $sendmail = $this->Common_Model->send_email($subject = 'Confirmation by Pradan', $message = $html,$pmail,$staffmail);

		    	$this->db->trans_complete();

		    	if ($this->db->trans_status() === true){


		    		$this->session->set_flashdata('tr_msg', 'Review of performance has been approved by ED!!!');
		    		redirect('/Probation_edirector_reviewofperformance/view/'.$token);	
		    	}else{
		    		$this->session->set_flashdata('er_msg', 'Error !!! Review of performance !!!');
		    		redirect(current_url());
		    	}

		    }
		    $content['getstaffprovationreviewperformance'] = $this->model->getstaffid($token);
		    $staffmail = $content['getstaffprovationreviewperformance']->emailid; 
		    $content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();



		    $content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);
		    $img = $content['getstaffpersonneldetals']->staffid; 

		    $content['staffsignature'] = $this->Common_Model->staff_signature($img);
		    $content['workflowdetail'] = $this->model->get_providentworkflowid($staffid);


			//print_r($content['getstaffpersonneldetals']);



		    $content['title'] = 'Probation_edirector_reviewofperformance';
		    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		    $this->load->view('_main_layout', $content);
		}
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}





public function fullview($token)
{
		// start permission 
	try{


		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

		$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			//print_r($content['getstaffprovationreviewperformance']); die;
		$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

		$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

		$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($content['getSupervisordetals']);

		$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
			//echo "<pre>";
		 //

			//print_r($getstaffprobationdetail); die;

		$probationid = $getstaffprobationdetail->id;

		$content['getstaffprobationreview'] = $getstaffprobationdetail; 
			// print_r($content['getstaffprobationreview']); die;

		$edname = $this->model->getEDName($content['getstaffprobationreview']->edid);

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

			$d_o_j = date('d/m/Y');
			$getstaff_name = $content['getstaffprovationreviewperformance']->name;
			$getstaff_emp_code = $content['getstaffprovationreviewperformance']->emp_code;
			$getstaff_desname = $content['getstaffprovationreviewperformance']->desname;

			$staff = array('$d_o_j','$getstaffprovationreviewperformance_name','$ednamename');
			$staff_replace = array($d_o_j,$getstaff_name,$edname->name);

			$this->db->trans_start();

			// $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
			// <tr>
			// <td width="10%">&nbsp;</td>
			// <td colspan="3"><p align="center"> Letter Informing Employee of Completion of Probation</strong></p></td>
			// <td width="10%">&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td width="27%">Ref: Personal Dossier of Employee</td>
			// <td width="27%">&nbsp;</td>
			// <td width="26%">Date:  $d_o_j </td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>To</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>Name of Probationer: $getstaff_name </p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">Employee Code: $getstaff_emp_code</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>Designation:  $getstaff_desname</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>Location: </p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p align="center"><em>Through the Supervisor</em></p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">Subject:<strong>Completion of Probation</strong></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>Dear  $getstaff_name,</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>You were appointed in PRADAN as $getstaffdesname with effect  from  $d_o_j .</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>I am happy to inform you that, based  on the review of your performance, you have satisfactorily completed the period  of your probation with effect from  $d_o_j .</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>I am sure that you will continue to  perform well in the future too.</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>Wishing you a long and purposeful  association with PRADAN.</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>Yours sincerely,</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3"><p>( $edname->name )<br />
			// Executive Director</p></td>
			// <td>&nbsp;</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td colspan="3">&nbsp;</td>
			// <td>&nbsp;</td>
			// </tr>
			// </table>';
			$filename = md5(time() . rand(1,1000));

					//letter to generate pdf 
			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 36 AND `isactive` = '1'";
			$data = $this->db->query($sql)->row();
			if(!empty($data))
				$body = str_replace($staff,$staff_replace , $data->lettercontent);


			$this->load->model('Dompdf_model');
			$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Generateofferletter.pdf');


         $hrdemail       = $this->loginData->EmailID; /////// hrd email id/////
         $staffemail     = 'amit.kum2008@gmail.com';

			// echo $newpassword; die;

		 	 $filename = 'e49348d7c5fb41a69976fe0483b15b67'; //die;
		 	 $attachments = array($filename.'.pdf');

			// print_r($attachments);  die;

		 	 $html = 'Dear '.$content['getstaffprovationreviewperformance']->name.', <br><br> 
		 	 Congratulations you are confirm by Pradan. 
		 	 <br>
		 	 <br><br>
		 	 Regard,<br>Pradan Team';



			$sendmail = $this->Common_Model->send_email($subject = 'Confirmation by Pradan', $message = $html, $staffemail, $attachments);  //// Send Mai


			$insertArr = array(
				'flag'				   => 5,
				'filename'             => $filename,
				'updatedby'            => $this->loginData->staffid,
				'updatedon'            => date('Y-m-d H:i:s'),
			);

			$this->db->where('id',$probationid);
			$this->db->update('tbl_probation_review_performance', $insertArr);

			$insertworkflowArr = array(
				'r_id'                 => $probationid,
				'type'                 => 7,
				'staffid'              => $token,
				'sender'               => $this->loginData->staffid,
				'receiver'             => $token,
				'senddate'             => date("Y-m-d H:i:s"),
				'flag'                 => 5,
				'createdon'            => date("Y-m-d H:i:s"),
				'createdby'            => $this->loginData->staffid,
			);

			$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
			$this->db->trans_complete();

			if ($this->db->trans_status() === true){

				$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
				redirect('/Probation_edirector_reviewofperformance/fullview/'.$token);	
			}else{
				$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
				redirect(current_url());
			}

		}



			//print_r($content['getstaffpersonneldetals']);


			// $content['getedname'] = $this->model->getEDName();
		$content['title'] = 'Probation_edirector_reviewofperformance';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}
	catch(Exception $e)
	{
		print_r($e->getMessage());
		die();
	}

}




}