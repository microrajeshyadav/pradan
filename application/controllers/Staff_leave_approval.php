<?php 

/**
* State Review
*/
class Staff_leave_approval  extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_review_model","Staff_review_model");
    $this->load->model("Staffleaveapplied_model","Staffleaveapplied_model");
    $this->load->model('Staff_approval_model');
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{

    // start permission 

   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();

   $this->load->model('Staff_review_model');

  // end permission 

   $content['leaverequest'] = '';
   $content['leaverequest'] = $this->Staff_leave_approval_model->getrequestedleave();
   // echo "<pre>";
   // print_r($content['leaverequest']);
   // die;
   $content['staff_details'] = $this->Staff_review_model->getstaffname();
  
   $content['title'] = 'Staff_leave_approval';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
 }

 public function stfflistdectivated($token)
 {
  try{

  $this->load->model('Staff_review_model');
  $fetch=$this->Staff_list_model->fetchdatas($token);


  if($fetch==-1)
  {
    $this->session->set_flashdata('er_msg', 'Sorry');

  }
  else
  {

    $updateArrs = array(
      'status'    => 0,
    );
    $this->db->where('staffid', $token);
    $this->db->update('staff', $updateArrs);
    $this->session->set_flashdata('tr_msg', 'Staff is deactivated');
  }
  

  
  redirect('/Staff_review/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  
} 

public function Approve($token,$id=null)
{
  try{
  
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  
$content['id'] = $this->Staff_leave_approval_model->get_providentworkflowid($token);

$content['personal'] = $this->Staff_leave_approval_model->personal_email();
// print_r($content['personal']); die;
 // $personal =  $content['personal']->staffid; 
 // echo $personal; die;

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 
 if($RequestMethod == 'POST')
     {
      
            $this->db->trans_start();                       
         $comments   = $this->input->post('comments');
         $purpse   = $this->input->post('r_purpose');
         
         
         $status = $this->input->post('status');
         
         $r_id     = $this->input->post('id');
         
         $p_status=$this->input->post('p_status');



      $insert_data = array (

        'type'                 => 5,
        'r_id'                 => $id,
        'sender'               => $this->loginData->staffid,
        'receiver'             => $p_status,
        'senddate'             => date('Y-m-d'),
        'createdon'            => date('Y-m-d H:i:s'),
        'createdby'            => $this->loginData->staffid,
        'scomments'            => $comments,
        'forwarded_workflowid' => $content['id']->workflow_id,
        'flag'                 => $status,
        'purpse'               => $purpse,
        'staffid'              => $token
      );

     // print_r($insert_data); die;

        //$this->db->where('workflowid',$content['id']->workflow_id);
      $this->db->insert('tbl_workflowdetail', $insert_data);


      $update_array = array (

        'status' => $status
      );
      $this->db->where('id',$id);
      $this->db->update('trnleave_ledger',$update_array);
      
      //echo $this->db->last_query();
      //die;
      $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error Leave Approved by Team Coordinator');  
            redirect(current_url());  
          }

          else{
            if($status==3)
            {
 //die;
            $this->session->set_flashdata('tr_msg', 'Successfully Leave Approved by ED');

            redirect('Staff_leave_approval');
          }
          else if($status==2)
          {
             $this->session->set_flashdata('tr_msg', 'Successfully Leave Rejected  by Team Coordinator');

            redirect('Staff_leave_approval');
          }
          else if($status==4)
          {
             $this->session->set_flashdata('tr_msg', 'Successfully Leave Rejected  by Executive Director');

            redirect('Staff_leave_approval');
          }
           else if($status==5)
          {
             $this->session->set_flashdata('tr_msg', 'Successfully Leave Rejected  by Executive Director');

            redirect('Staff_leave_approval');
          }


          } 




    }


  

    
 
    $content['subview'] = 'Staff_leave_approval/approve';
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  }


  public function sendsanctionletter(){
    // echo "string"; die();

    // echo $token; die;
try {

   $content['id'] = $this->Staffleaveapplied_model->getEDid();
      // print_r($content['id']); die;

   // $var = $content['id']->edid; 
   $edname = $content['id']->UserFirstName.' '.$content['id']->UserMiddleName.' '.$content['id']->UserLastName;

    // $content['pid'] = $this->Staffleaveapplied_model->personal_email();
   
   // echo $content['pid']->staffid; die;

    // echo $content['staff_code']->emp_code; die;

   $RequestMethod = $this->input->server('REQUEST_METHOD'); 
  if($RequestMethod == "POST"){
    // print_r($_POST); die();

  $empcode = $this->input->post('empcode');
  $descript = $this->input->post('descript');
  $fmdate = $this->input->post('fmdate');
  $tdate = $this->input->post('tdate');
  $mid = $this->input->post('mid');
  $content['leaverequest'] = $this->Staffleaveapplied_model->getrequestedleave($empcode);
  $reportingtodetails = $this->Staff_leave_approval_model->getreportingtodetails($empcode);
    $currentdate = date('Y-m-d');

    $txtgender='';
    $title = '';
   // $fromdate = $this->gmodel->changedatedbformate($content['leaverequest']->From_date);
   // $todate = $this->gmodel->changedatedbformate($content['leaverequest']->To_date);

   if ($content['leaverequest']->gender==1) {
     $txtgender = 'Male';
     $title = 'Mr.';
   }elseif ($content['leaverequest']->gender == 2) {
     $txtgender = 'Female';
     $title = 'Ms.';
   }else{
     $txtgender = 'other';
     $title = 'Mr./Ms.';
   }

    // ed signature
            $data = "";
            $data_replace = "";
            $getedname = "";
            $edsign = "";
            $body = "";


            $getedname = $this->gmodel->getExecutiveDirectorEmailid();

            if ($getedname->sign !='') {
                $edsign = site_url('datafiles/signature/'.$getedname->sign);
            }else{ 
                $edsign = site_url('datafiles/signature/Signature.png');
            }

            $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 91 AND `isactive` = '1'";
            $body = $this->db->query($sql)->row();

            $data = array('$leaverequest_Emp_code','$currentdate','$title','$leaverequest_name','$leaverequest_officename','$leaverequest_districtname','$leaverequest_statename','$fromdate','$todate','$edsign','$edname','$description');
            $data_replace = array($content['leaverequest']->Emp_code,$this->gmodel->changedatedbformate($currentdate),$title,$content['leaverequest']->name,$content['leaverequest']->officename,$content['leaverequest']->districtname,$content['leaverequest']->statename,$fmdate,$tdate,$edsign,$edname,$descript);

            if(!empty($body))
            $body = str_replace($data,$data_replace,$body->lettercontent);
            $body=  html_entity_decode($body);

            $filename = "";
            $filename = md5(time() . rand(1,1000));
            $this->load->model('Dompdf_model');
            $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'Sanctionletter.pdf');

            $updteArray = array(
                    'letter_fromdate' => $this->gmodel->changedatedbformate($fmdate),
                    'letter_todate' => $this->gmodel->changedatedbformate($tdate),
                    'filename' => $filename
                    );
                    $this->db->where('id', $mid);
                    $this->db->update('trnleave_ledger',$updteArray);

            $attachments = array($filename.'.pdf');

         $html = 'Dear '.$content['leaverequest']->name.',<br><br>';
          $html .= 'This is to certify that Mr. '.$content['leaverequest']->name.' your process has generated  .<br><br>';
          $html .= 'Thanks<br>';
          $html .= 'Personnel Unit<br>';
          $html .= 'PRADAN<br><br>';

        $to_email = $content['leaverequest']->emailid;
        $recipients = array(
             $reportingtodetails->reportingtoemailid => $reportingtodetails->reportingname,
             $getedname->edmailid => $getedname->edname
          );

          $email_result = $this->Common_Model->send_email($subject="Leave Sanction Letter", $html, $to_email,$to_name=null, $recipients, $attachments);

          if (substr($email_result, 0, 5) == "ERROR") {
            $this->session->set_flashdata('er_msg', "Error sending email, please contact system administrator");
          }


                    $this->db->trans_complete();



    if($generate==true){
        // echo "hyih"; die;   
        $this->session->set_flashdata('tr_msg', 'Sanction letter generate successfully');
         redirect('/Staff_leave_approval/index');

    }else{

       $this->session->set_flashdata('er_msg','!!!! error in Sanction letter !!!!');
         // redirect('/Staffleaveapplied/sendsanctionletter/'.$token);

    }

      

    if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Sanction letter data has not been saved');

    }else{
        $this->session->set_flashdata('tr_msg', 'Sanction letter has been prepared successfully');

    }

  }


   // $staff = $content['leaverequest']->staffid;
   // echo $staff; die;

    // $content['forid'] = $this->Staff_leave_approval_personnel_model->get_providentworkflowid($staff);
    // print_r($content['forid']); die;

   
    
} catch (Exception $e) {
    
}

$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}



  public function view($token,$id)
{
  try{
  
//         // start permission 
//   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
//   $content['role_permission'] = $this->db->query($query)->result();
  
  if($id)
  {
$content['id'] = $this->Staff_leave_approval_model->getdetails($id);
  }
  // echo "<pre>";
  // print_r($content['id']);
  // die;

// $content['personal'] = $this->Staff_leave_approval_model->personal_email();
// print_r($content['personal']); die;
 // $personal =  $content['personal']->staffid; 
 // echo $personal; die;

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 
 if($RequestMethod == 'POST')
     {
    //   print_r($_POST);
    // die;
           // $this->db->trans_start();                       
         $comments   = $this->input->post('comments');
       
         
         
         $status = $this->input->post('status');
         
        




        if($this->loginData->RoleID==2)
        {
      $update_array1 = array (

        'supervisiorview' => $comments,
        'supervisiorverify'=>$status
        
      );
      $this->db->where('id',$id);
      $this->db->update('trnleave_ledger',$update_array1);

      // $this->db->trans_complete();
      // if ($this->db->trans_status() === FALSE){

      //       $this->session->set_flashdata('er_msg', 'Error Leave view by Team Coordinator');  
      //       redirect(current_url());  
      //     }

      //     else{
      //       $this->session->set_flashdata('tr_msg', 'Successfully Leave view  by Team Coordinator');
            redirect('Staff_leave_approval');
          //}
    }
    else if($this->loginData->RoleID==16)
        {
      $update_array = array (

        'hrview' => $comments,
        'hrverify'=>$status
        
      );

      $this->db->where('id',$id);
      $this->db->update('trnleave_ledger',$update_array);
      // echo $this->db->last_query();
      // die;

      // $this->db->trans_complete();
      // if ($this->db->trans_status() === FALSE){

      //       $this->session->set_flashdata('er_msg', 'Error Leave view by HR Administrator');  
      //       redirect(current_url());  
      //     }

      //     else{
      //       $this->session->set_flashdata('tr_msg', 'Successfully Leave view  by HR Administrator');
            redirect('Staff_leave_approval');
         // }
      // echo $this->db->last_query();
      // die;
    }
     else if($this->loginData->RoleID==17)
        {
      $update_array2 = array (

        'personalview' => $this->input->post('comments'),
        'personalverify'=>$this->input->post('status')
      );
        
          $this->db->where('id',$id);
      $this->db->update('trnleave_ledger',$update_array2);


      //  $this->db->trans_complete();
      // if ($this->db->trans_status() === FALSE){

      //       $this->session->set_flashdata('er_msg', 'Error Leave view by personal Administrator');  
      //       redirect(current_url());  
      //     }

      //     else{
      //       $this->session->set_flashdata('tr_msg', 'Successfully Leave view  by personal Administrator');
            redirect('Staff_leave_approval');
         // }
    }

      
      
      // echo $this->db->last_query();
      // die;
     
           
         // } 




    }


  

    
 
    $content['subview'] = 'Staff_leave_approval/view';
    $this->load->view('_main_layout', $content);
    }catch (Exception $e) {
     print_r($e->getMessage());die;
   }
  }



  function reject($token,$id)
  {
    try{
         // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
  
  $content['id'] = $this->Staff_leave_approval_model->get_providentworkflowid($token);
 
$RequestMethod = $this->input->server('REQUEST_METHOD'); 

 if($RequestMethod == 'POST')
     {
      
                                
      $comments   = $this->input->post('r_purpose');
      
      $status = $this->input->post('status');
     
      $r_id     = $this->input->post('id');
      
      



      $insert_data = array(

        'type'                 => 5,
        'r_id'                 => $id,
        'sender'               => $token,
        'receiver'             => $this->loginData->staffid,
        'senddate'             => date('Y-m-d'),
        'createdon'            => date('Y-m-d H:i:s'),
        'createdby'            => $this->loginData->staffid,
        'scomments'            => $comments,
        'forwarded_workflowid' => $content['id']->workflow_id,
        'flag'                 => $status,
        'staffid'              => $token
      );

     

            $this->db->insert('tbl_workflowdetail', $insert_data);
    



    }



 

    
 
    $content['subview'] = 'Staff_leave_approval/reject';
    $this->load->view('_main_layout', $content);

    }
    catch (Exception $e) {
     print_r($e->getMessage());die;
   }

  }

}