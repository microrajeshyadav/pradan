<?php 

/**
* State List
*/
class Staff_list extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model("Global_model","gmodel");
    $this->load->model("Staff_list_model","Staff_list_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{

    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
   $content['officelist'] = $this->Staff_list_model->getOfficeList();
  // end permission 

   $RequestMethod = $this->input->server("REQUEST_METHOD");
   if($RequestMethod == 'POST')
   {
   
    $btn = $this->input->post('btnsubmit');
    $childallow = $this->input->post('children');

    if (isset($childallow) && $childallow == 'allowance') {
       $this->form_validation->set_rules('txtNoofchildren','txtNoofchildren','trim|required');
       $this->form_validation->set_rules('children_allowance','children_allowance','trim|required');
       $this->form_validation->set_rules('dateofeffective','dateofeffective','trim|required');
      
      if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('enter_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }


      // print_r($this->input->post()); die();
        $this->db->trans_start();
        $staffid2 = $this->input->post('staff2');

        if(!empty($this->input->post('chkCEA')))
        {
          $chkCEA = $this->input->post('chkCEA');
        }
        else{
          $chkCEA = 0;
        }

        $txtNoofchildren = $this->input->post('txtNoofchildren');
        $children_allowance = $this->input->post('children_allowance');
        $dateofeffective = $this->input->post('dateofeffective');

        $insertArr = array(
          'txtNoofchildren'      => $txtNoofchildren,
          'children_allowance'      => $children_allowance,
          'dateofeffective'      => $this->gmodel->changedatedbformate($dateofeffective),
          'chkCEA'      => $chkCEA
        );
        $this->db->where('staffid',$staffid2);
        $this->db->update('staff', $insertArr);

        $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
      $this->session->set_flashdata('er_msg', 'Error !!! Children allowance has not been saved successfully'); 
      redirect(current_url());
      }else{
        $this->session->set_flashdata('tr_msg', 'Children allowance has been saved successfully !!!');
        redirect('Staff_list');

      }

    }

    if (isset($btn) && $btn == 'Save') {
      $emailid = $this->input->post('emailid');

      $this->form_validation->set_rules('emailid','Emailid','trim|required');
      
      if($this->form_validation->run() == FALSE){
        $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
          '</div>');

        $this->session->set_flashdata('enter_msg', 'There have been validation error(s), please check the error messages');

        $hasValidationErrors    =    true;
        goto prepareview;

      }


      // print_r($this->input->post()); die();
        $this->db->trans_start();
        $staffid = $this->input->post('staff1');
        $insertArr = array(
          'emailid'      => $emailid
        );
        $this->db->where('staffid',$staffid);
        $this->db->update('staff', $insertArr);

        $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
      $this->session->set_flashdata('er_msg', 'Error !!! Emailid has not been assigned successfully'); 
      redirect(current_url());
      }else{
        $this->session->set_flashdata('tr_msg', 'Emailid has been assigned successfully !!!');
        redirect('Staff_list');

      }

      }

    $content['ddstatus'] = $this->input->post("ddstatus");
    $content['officeid'] = $this->input->post("officeid");

    $this->form_validation->set_rules('ddstatus','Status','trim|required');
    $this->form_validation->set_rules('officeid','Office name','trim|required');

    

    $content['staff_details'] = $this->Staff_list_model->getstaffname($content['ddstatus'], $content['officeid']);

   }
   else
   {
    $content['staff_details'] = $this->Staff_list_model->getstaffname(1);
   }

/*echo "<pre>";
print_r($content['staff_details']);exit();*/
 prepareview:
   $content['title'] = 'Staff_list';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
     print_r($e->getMessage());die;
   }

}


 public function stfflistdectivated($token)
 {


  $this->load->model('Staff_list_model');
  $fetch=$this->Staff_list_model->fetchdatas($token);


  if($fetch==-1)
  {
    $this->session->set_flashdata('er_msg', 'Sorry');

  }
  else
  {

    $updateArrs = array(
      'status'    => 0,
    );
    $this->db->where('staffid', $token);
    $this->db->update('staff', $updateArrs);
    $this->session->set_flashdata('tr_msg', 'Staff is deactivated');
  }



  redirect('/Staff_list/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  
} 


}