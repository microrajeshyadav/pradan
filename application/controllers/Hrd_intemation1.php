<?php 
class Hrd_intemation1 extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Hrd_intemation1_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');

		//print_r($this->loginData);

	}

	public function index()
	{
		try{
			$content['getprobationdetails'] =$this->model->get_Probation_Separation();
			$content['title'] = 'Hrd_intemation1';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}


	public function send_hrd_intemation()
	{
		
		try{


			$getDAdetails = '';
			$get_HRDDetails = '';
			$htrdemailid= '';
			$to_email_hrd = '';
			$to_da = '';
			$to_tc = '';
			$to_finance ='';
			$email ='';
			$mails ='';
			$attachments ='';
			$txtprocessid = 0;
			$txtstatus = 0;

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){

				// print_r($this->input->post()); die;

				if ($this->input->post('intemationssend')=='send') {
					// print_r($this->input->post()); die;

					$this->db->trans_start();

					$daname = $this->input->post('daname');
					/*$getDAdetails = $this->model->get_DA_Staff_Details($daname);
					echo "<pre>";
					print_r($getDAdetails);exit();*/
					$mails = $this->Hrd_intemation1_model->get_DA_Single_Details1($daname);
					// print_r($mails); die();

					$intemationtype = $this->input->post('intemation_type');
					$comment = $this->input->post('comments');
					// $hrid = $this->input->post('hrid');

					$currentdate = date('Y-m-d');

					$sql = "SELECT Count(id) as count FROM tbl_hr_intemation where  intemation_type =".$intemationtype." and staffid ='".$daname."' and Date(createdon) ='".$currentdate."' ";
					$result = $this->db->query($sql)->result();
		// echo $result[0]->count;
		 //die;

					$getDAdetails = $this->model->get_DA_Staff_Details($daname);
					$get_HRDDetails = $this->model->get_HRDDetails($this->loginData->staffid);

					if($result[0]->count >0){

						$this->session->set_flashdata('er_msg', $intemationname.' has been done allready , please choose intimation type ');
						redirect('/Hrd_intemation1/send_hrd_intemation');
					}else{

						$tablename = "'staff_transaction'";
						$incval = "'@a'";
						$getstaffincrementalid = $this->getStaffTransactioninc($tablename,$incval); 
						$autoincval =  $getstaffincrementalid->maxincval;

						$staffdetail = $this->getStaffDetails($daname);		
						$probation_extension_date = $this->input->post("probation_extension_date");
						$probation_extension_dbdate = $probation_extension_date== ''? NULL : $this->model->changedatedbformate($probation_extension_date);


						$hrdname =$this->gmodel->getHRDEmailid();

						if($intemationtype==1){
							$intemationname = 'Resignation of ';
							$mailbodypartcontent = "".$getDAdetails->name."<b> has resigned from the Apprenticeship Programme. The exit formalities have been completed. Kindly process.";
							$txtstatus = 1;
						}else if($intemationtype==2){
							$intemationname = 'Eligible to graduate';							
							$mailbodypartcontent = "".$getDAdetails->name."<b> are eligible to graduate. The Exit formalities have been completed. Kindly proceed further.";
							$txtstatus = 1;
						}else if($intemationtype==4){
							$intemationname = 'Change of Placement Letter';
							$mailbodypartcontent = "Dear ".$getDAdetails->supervisor.", <br/> <br/>

							Kindly hand over the attached Change of Placement Letter to ".$getDAdetails->name.". <br/>

							I am also sending new TCs Contact No. for his/her reference: <br/><br/>
							<New TC name>: 
							<New TC no.>
							<br/>
							mail Id: <new TC mail ID>	<br/><br/> Regards,
							".$hrdname->name."  <br/>
							".$hrdname->desname." <br/>
							HRD Unit";
							$txtstatus = 1;
							$txtprocessid = 26;

						}else if($intemationtype==5){
							$intemationname = 'Facilitate to leave';
							$mailbodypartcontent = "".$getDAdetails->name."<b> has facilitate to leave from the Apprenticeship Programme. The exit formalities have been completed. Kindly process.";
							$txtstatus = 1;
						}else if($intemationtype==6){
							$intemationname = 'Extention';
							$mailbodypartcontent = "".$getDAdetails->name."<b> going to be Extention. Kindly proceed further.";
							$txtstatus = 1;
							$txtprocessid = 28;
						}


						$insertArr = array(
							'staffid'  			=>  $this->input->post('daname'),
							'intemation_type'   => $this->input->post('intemation_type'),
							'comment'           => $this->input->post("comments"),
							'status'            => $txtstatus,
							'probation_completed' => $this->input->post("probation_completed") == ''? NULL : $this->input->post("probation_completed"),
							'probation_completed_date' => $probation_extension_dbdate,
							// 'hrid'               => $this->input->post('hrid'),
							'createdon'          => date('Y-m-d H:i:s'),
				            'createdby'          => $this->loginData->UserID, // login user id
				        );

						$this->db->insert('tbl_hr_intemation', $insertArr);

						if ($intemationtype==4) {

							$insertArr2 = array(
								'staffid'             => $daname,
								'old_office_id'       => $staffdetail->new_office_id,
								'new_office_id'       => $staffdetail->new_office_id,
								'date_of_transfer'    => date('Y-m-d'),
								'old_designation'     => 13,
								'new_designation'     => 13,
								'reportingto'		  => $staffdetail->reportingto,
								'id'                  => $getstaffincrementalid->maxincval,
								'trans_status'        => $intemationname,
								'trans_flag'		  => 1,
								'reason'			  => $comment,
								'createdon'			  => date('Y-m-d H:i:s'), 
								'createdby'			  => $this->loginData->staffid,
                            'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
                        );

							$this->db->insert('staff_transaction', $insertArr2);

							$workflowsql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$getstaffincrementalid->maxincval";
							$workresult  = $this->db->query($workflowsql)->result()[0];

							if ($workresult->workflowid !='') {
								$insertworkflowArr = array(
									'r_id'                 => $getstaffincrementalid->maxincval,
									'type'                 => $txtprocessid,
									'staffid'              => $daname,
									'sender'               => $this->loginData->staffid,
									'receiver'             => $this->loginData->staffid,
									'forwarded_workflowid' => $result->workflowid,
									'senddate'             => date("Y-m-d H:i:s"),
									'flag'                 => 1,
									'scomments'            => $comment,
									'createdon'            => date("Y-m-d H:i:s"),
									'createdby'            => $this->loginData->staffid,
								);

								$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

							}else{

								$insertworkflowArr = array(
									'r_id'                 => $getstaffincrementalid->maxincval,
									'type'                 => $txtprocessid,
									'staffid'              => $daname,
									'sender'               => $this->loginData->staffid,
									'receiver'             => $this->loginData->staffid,
									'senddate'             => date("Y-m-d H:i:s"),
									'flag'                 => 1,
									'scomments'            => $comment,
									'createdon'            => date("Y-m-d H:i:s"),
									'createdby'            => $this->loginData->staffid,
								);
								$this->db->insert('tbl_workflowdetail', $insertworkflowArr);	
							}

						}


						if ($intemationtype==6) {

							$insertArr2 = array(
								'staffid'             => $daname,
								'old_office_id'       => $staffdetail->new_office_id,
								'new_office_id'       => $staffdetail->new_office_id,
								'date_of_transfer'    => date('Y-m-d'),
								'old_designation'     => 13,
								'new_designation'     => 13,
								'reportingto'		  => $staffdetail->reportingto,
								'id'                  => $getstaffincrementalid->maxincval,
								'trans_status'        => $intemationname,
								'trans_flag'		  => 1,
								'reason'			  => $comment,
								'createdon'			  => date('Y-m-d H:i:s'),
								'createdby'			  => $this->loginData->staffid,
                            'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
                        );

							$this->db->insert('staff_transaction', $insertArr2);

							$workflowsql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$getstaffincrementalid->maxincval";
							$workresult  = $this->db->query($workflowsql)->result()[0];

							if ($workresult->workflowid !='') {
								$insertworkflowArr = array(
									'r_id'                 => $getstaffincrementalid->maxincval,
									'type'                 => $txtprocessid,
									'staffid'              => $daname,
									'sender'               => $this->loginData->staffid,
									'receiver'             => $hrid,
									'forwarded_workflowid' => $result->workflowid,
									'senddate'             => date("Y-m-d H:i:s"),
									'flag'                 => 1,
									'scomments'            => $comment,
									'createdon'            => date("Y-m-d H:i:s"),
									'createdby'            => $this->loginData->staffid,
								);

								$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

							}else{

								$insertworkflowArr = array(
									'r_id'                 => $getstaffincrementalid->maxincval,
									'type'                 => $txtprocessid,
									'staffid'              => $daname,
									'sender'               => $this->loginData->staffid,
									'receiver'             => $hrid,
									'senddate'             => date("Y-m-d H:i:s"),
									'flag'                 => 1,
									'scomments'            => $comment,
									'createdon'            => date("Y-m-d H:i:s"),
									'createdby'            => $this->loginData->staffid,
								);
								$this->db->insert('tbl_workflowdetail', $insertworkflowArr);	
							}
						}


						$this->db->trans_complete();

						$htrdemailid = $get_HRDDetails->emailid;
						// echo $htrdemailid; die;
						$subject = " ".$intemationname.":".$getDAdetails->name;
						$body = "<h4>Dear ".$mails->tc_name.", </h4><br /><br />";
						$body .= "".$mailbodypartcontent;
						$body .= "<br /><br /><b> Regards </b><br>";
						$body .= "<b>".$this->loginData->UserFirstName." ".$this->loginData->UserLastName." </b><br>";

						$to_email_hrd = $htrdemailid; 
						$to_da        = $mails->da_mail;
						$to_tc        = $mails->tc_mail;
						// $to_finance   = $mails->finance_mail;

						$email = array(
							$to_email_hrd        => 'hrd'
						);

						if ($this->db->trans_status() === FALSE){
							$this->session->set_flashdata('er_msg', 'Error adding Record');	
						}else{
							$sendmail = $this->Common_Model->send_email($subject, $body,$to_tc,$to_da,$email);
							if ($sendmail) {
								$insertArr = array(
									'status'  => 1,
								);
								$this->db->where('id',$insertid);
								$this->db->update('tbl_hr_intemation', $insertArr);
								$this->session->set_flashdata('tr_msg', 'Successfully save record and sent email');	
							}else{
								$this->session->set_flashdata('er_msg', 'Error email not sent !!!');	
							}

						}
					}

					
				}
				redirect('/Hrd_intemation1/');
			}

			$content['getprobationdetails']  = $this->model->get_Probation_Separation();
			$content['getdadetails']         = $this->model->get_DA_Single_Details();
			// print_r($content['getdadetails']); die;
			$content['getintimation']        = $this->model->getIntimation();
			$content['hrlist']               = $this->model->get_hr_Staff_List();
			/*echo "<pre>";
			print_r($content['getdadetails']);exit();*/
			$content['title'] = 'Hrd_intemation1';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}



	public function edit_send_hrd_intemation($token)
	{

		try{

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			
			if($RequestMethod == 'POST'){
				if ($this->input->post('intemationssend')=='send') {

					$this->db->trans_start();
					$daname = $this->input->post('daname');
					$intemationtype = $this->input->post('intemation_type');
					$comment = $this->input->post('comments');
					$currentdate = date('Y-m-d');
					$tablename = "'staff_transaction'";
					$incval = "'@a'";
					$getstaffincrementalid = $this->getStaffTransactioninc($tablename,$incval); 
					$autoincval =  $getstaffincrementalid->maxincval;

					$staffdetail = $this->getStaffDetails($daname);

					
					if($intemationtype==1){
						$intemationname = 'Resign';
						$txtstatus = 1;
					}else if($intemationtype==2){
						$intemationname = 'Recommended to graduate';
						$txtstatus = 1;
					}else if($intemationtype==4){
						$intemationname = 'Transfer';
						$txtstatus = 1;
						$txtprocessid = 26;

					}else if($intemationtype==5){
						$intemationname = 'Facilitate to leave';
						$txtstatus = 1;
					}else if($intemationtype==6){
						$intemationname = 'Extention';
						$txtstatus = 1;
						$txtprocessid = 28;
					}


					if ($intemationtype==4) {

						$insertArr2 = array(
							'staffid'             => $daname,
							'old_office_id'       => $staffdetail->new_office_id,
							'new_office_id'       => $staffdetail->new_office_id,
							'date_of_transfer'    => date('Y-m-d'),
							'old_designation'     => 13,
							'new_designation'     => 13,
							'reportingto'		  => $staffdetail->reportingto,
							'id'                  => $getstaffincrementalid->maxincval,
							'trans_status'        => $intemationname,
							'trans_flag'		  => 1,
							'reason'			  => $comment,
							'createdon'			  => date('Y-m-d H:i:s'),
							'createdby'			  => $this->loginData->staffid,
                            'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
                        );

						$this->db->insert('staff_transaction', $insertArr2);


						$workflowsql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$getstaffincrementalid->maxincval";
						$workresult  = $this->db->query($workflowsql)->result()[0];

						if ($workresult->workflowid !='') {
							$insertworkflowArr = array(
								'r_id'                 => $getstaffincrementalid->maxincval,
								'type'                 => 26,
								'staffid'              => $daname,
								'sender'               => $this->loginData->staffid,
								'receiver'             => $hrid,
								'forwarded_workflowid' => $result->workflowid,
								'senddate'             => date("Y-m-d H:i:s"),
								'flag'                 => 1,
								'scomments'            => $comment,
								'createdon'            => date("Y-m-d H:i:s"),
								'createdby'            => $this->loginData->staffid,
							);

							$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

						}else{

							$insertworkflowArr = array(
								'r_id'                 => $getstaffincrementalid->maxincval,
								'type'                 => 26,
								'staffid'              => $daname,
								'sender'               => $this->loginData->staffid,
								'receiver'             => $hrid,
								'senddate'             => date("Y-m-d H:i:s"),
								'flag'                 => 1,
								'scomments'            => $comment,
								'createdon'            => date("Y-m-d H:i:s"),
								'createdby'            => $this->loginData->staffid,
							);
							$this->db->insert('tbl_workflowdetail', $insertworkflowArr);	
						}
						
					}		

					$probation_extension_date = $this->input->post("probation_extension_date");
					$probation_extension_dbdate = $probation_extension_date== ''? NULL : $this->model->changedatedbformate($probation_extension_date);

					$UpdateArr = array(
						'staffid'  =>  $this->input->post('daname'),
						'intemation_type'  => $this->input->post('intemation_type'),
						'comment'  => $this->input->post("comments"),
						'status' => 1,
						'probation_completed' => $this->input->post("probation_completed") == ''? NULL : $this->input->post("probation_completed"),
						'probation_completed_date' => $probation_extension_dbdate,
						'hrid'               => $this->input->post('hrid'),
						'createdon' => date('Y-m-d H:i:s'),
			            'createdby' => $this->loginData->UserID, // login user id
			        );

					$this->db->where('id', $token);			
					$this->db->update('tbl_hr_intemation', $UpdateArr);
		//echo $this->db->last_query(); die;
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE){
						$this->session->set_flashdata('er_msg', 'Error adding Record');	
					}else{

						$getDAdetails = $this->model->get_DA_Staff_Details($daname);
						$candidateemailid =  $getDAdetails->emailid;

						$subject = "".$intemationname;
						$body = "<h4>Hi ".$getDAdetails->name.", </h4><br /><br />";
						$body .= "<b> Intimation Type -  </b>". $intemationname ."<br>";
						$body .= "<b> Comment - </b></b>". $comment ."<br><br><br>";
						$body .= "<b> Thanks </b><br>";
						$body .= "<b>Pradan Technical Team </b><br>";

						$to_email_hrd = $this->loginData->hrdemailid;
						$to_name   = $candidateemailid;

						$sendmail = $this->Common_Model->send_email($subject, $body, $to_email_hrd, $to_name);
						if ($sendmail) {
							$updatestatusArr = array(
								'status'  => 1,
							);
							$this->db->where('id',$token);
							$this->db->update('tbl_hr_intemation', $updatestatusArr);
							$this->session->set_flashdata('tr_msg', 'Successfully save record and sent email');	
						}else{
							$this->session->set_flashdata('er_msg', 'Error email not sent !!!');	
						}
					}
					redirect('/Hrd_intemation1');
				}
			}


			$content['getprobationdetails']    = $this->model->get_Probation_Separation();
			$content['getprobationseparation'] = $this->model->get_edit_Probation_Separation($token);
			$content['getdadetails']           = $this->model->get_DA_EDIT_Details();
			$content['getintimation']          = $this->model->getIntimation();
			$content['hrlist']                 = $this->model->get_hr_Staff_List();

			$content['title'] = 'Hrd_intemation1';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}




/**
   * Method getCandidateSeparationDetails() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffDetails($staffid)
{

	try{

		$sql = " SELECT * FROM `staff` Where staff.staffid = ".$staffid;
		$result = $this->db->query($sql)->result()[0];
		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


/**
   * Method getStaffTransactioninc() get candidate Details.
   * @access  public
   * @param Null
   * @return  Array
   */

public function getStaffTransactioninc($tablename,$incval)
{
	try{

        // echo $tablename;
        // echo $incval;  die;
        //'staff_transaction',@a; 

		$sql = "select get_maxvalue($tablename,$incval) as maxincval";

		$result = $this->db->query($sql)->result()[0];

		return $result;

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}


	/**
	 * Method delete() delete data. 
	 * @access	public
	 * @param	
	 * @return	array
	 */ 
	public function delete($token)
	{
		try{

		//echo $token;die;
		
		$this->view['detail'] = $this->model->getHrdintimationDetails($token);

		if(count($this->view['detail']) < 1) {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
			redirect($this->router->class);
		}
		

		if($this->model->delete($token) == '1'){
			$this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
			redirect($this->router->class);
		}
		else {
			$this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
			redirect($this->router->class);
		}

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
	}

}