<?php 
class Changefieldguide extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Dompdf_model");
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");
		$this->load->model("Global_model","gmodel");
		$this->load->model("Changefieldguide_model","model");
		$check = $this->session->userdata('login_data');
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
		$content['candidatebdfformsubmit'] = $this->model->getCandidate_BDFFormSubmit();
		$content['title'] = 'Changefieldguide';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
	}

	
	public function add($token){



		try{

			
			$getcategory = '';
			$staff_details ='';
			$hr_data = '';
			$candidateid = '';
			$gethrddetails = $this->model->getHRunitDetails($this->loginData->staffid);	
			$hrdname=$gethrddetails->name;
			$hrdrole=$gethrddetails->name;
			$hrddesignation=$gethrddetails->desname;
			$getcategory = $this->model->getCategoryid($token);
			$content['office_id']=$this->model->getofficeid($token);
			$content['Fieldguide_id']=$this->model->Fieldguide_name($token);		
			$staff_details=$this->model->getstafftransdetail($token);
			$offer_no=$staff_details->offerno;
			$joining_date=$this->gmodel->changedatedbformate($staff_details->doj);
			$createdon=$this->gmodel->changedatedbformate($staff_details->createdon);
			$offer_date=explode(" ",$createdon);
			$tablename = "'staff_transaction'";
			$incval = "'@a'";
			$getstaffincrementalid = $this->model->getfieldguidemmax($tablename,$incval); 
			$autoincval =  $getstaffincrementalid->maxincval;
			$old_Fieldguide=$this->model->old_Fieldguide($token);
			$content['old_Fieldguide']=$old_Fieldguide;
			$new_fgid = null;
			if (!empty($content['Fieldguide_id'])) {
				$new_fgid=$content['Fieldguide_id']->name;
			}
			
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			if($RequestMethod == 'POST'){	
				$this->db->trans_start();
				$this->form_validation->set_rules('team','Team ','trim|required');
				$this->form_validation->set_rules('fieldguide','Field Guide ','trim|required');
				$this->form_validation->set_rules('oldfg','Old Field Guide ','trim|required');
				if($this->form_validation->run() == FALSE){
					$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',	'</div>');
					$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');
					$hasValidationErrors    =    true;
					goto prepareview;

				}
				$newfieldguide='';
				$newfieldguide=$this->model->New_Fieldguidename($this->input->post('fieldguide'));
				$newfieldname='';
				$newfieldname=$newfieldguide->name;					
				$candidate_name=$staff_details->name;
				$insertArr = array(
					'id'               => $autoincval,
					'staffid'          => $staff_details->candidateid,
					'old_office_id'    => $staff_details->teamid,

					'new_office_id'    => $staff_details->teamid,
					'date_of_transfer' => date("Y-m-d"),
					'trans_status'     => 'CFG_PRE',
					'datetime'         => date("Y-m-d H:i:s"),
					'fgid'     		   => $this->input->post('fieldguide'),
					'createdon'        => date("Y-m-d H:i:s"),
					'createdby'        => $this->loginData->staffid,
					'trans_flag'       => 1,

				);
				$this->db->insert('staff_transaction', $insertArr);					
				$oldfg=$this->input->post('oldfg');
				$id=$this->db->insert_id();
				$Arrupdate = array(
					'fgid'=> $this->input->post('fieldguide'),
				);
				$this->db->where('candidateid',$token);
				$this->db->update('tbl_candidate_registration',$Arrupdate);			
				$d_o_j = date('F j,Y');
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE){
					$this->session->set_flashdata('er_msg', 'Error !!! Something went wrong while creating change field guide ');  

				}else{
					$html='
					<body>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					<td width="4%">&nbsp;</td>
					<td colspan="4">&nbsp;</td>
					<td width="3%">&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td width="28%"><p></p></td>
					<td width="17%">&nbsp;</td>
					<td width="17%">&nbsp;</td>
					<td width="31%"><p>'.$d_o_j.'</p></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td><p>'.$staff_details->name.'<br />
					'.$staff_details->name.$staff_details->permanenthno.$staff_details->permanentstreet.$staff_details->state_name.'<br /> Dist: 
					'.$staff_details->permanentcity.'-'.$staff_details->permanentpincode.'</p></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4"><p align="center"><em>Subject. </em>Change of Field Guide letter</p></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td><p>'.$staff_details->name.',</p></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4">&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4"><p>This is to inform you that on account o&pound; transfer of your Field Guide '.$old_Fieldguide->name.',  your designated Field Guide will be '.$newfieldname.' Pradhan,  Team Coordinator with mmediate  effect.</p></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4">&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4"><p>All the perspective and terms set out in your offer  letter ' .$offer_no.' dated '.$offer_date[0].' for joining on '.$joining_date.' will continue.</p></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4">&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4"><p>Looking forward to a long  assoclation with you,</p></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4">&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td colspan="4">&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><div align="right">Yours sincerely,</div></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><p align="right">'.$hrdname.'</p></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><div align="right">'.$hrddesignation.'</div></td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					</table>
					</body>';						
					$subject='Change of Field Guide Letter';
					$fd = fopen("mailtext/change_fieldguide.txt","r");	
					$message='';
					$message .=fread($fd,4096);
					eval ("\$message = \"$message\";");
					$message =nl2br($message);					
					$filename = 'changefieldguide_'.$token.'_'.md5(time() . rand(1,1000));
					$this->load->model('Dompdf_model');
					$generate =   $this->Dompdf_model->generatePDF($html, $filename, NULL,'ChangeFieldguide.pdf');
					if($generate ==true)

						$pdffilename = $filename.'.pdf';
					$attachments = array($pdffilename);
					$to_name='New Field Guide';
					$to_email = $newfieldguide->emailid;					
					$staff_email=$staff_details->emailid;
					$arr= array (
						$staff_email =>'candidate'

					);
					
					$sendmail = $this->Common_Model->send_email($subject, $message, $to_email,$to_name,$recipients=$arr, $attachments);
					$this->session->set_flashdata('tr_msg', 'Successfully update FGC ');
					redirect('/Changefieldguide/index');
				}

			}
			prepareview:
			$content['getcategoryid']    = $this->model->getCategoryid($token);	
			$content['method']              = $this->router->fetch_method();
			$content['title']  = 'Changefieldguide/add';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}


}