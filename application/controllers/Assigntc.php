<?php 
class Assigntc extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->model("Assigntc_model");
		//$this->load->model("Dompdf_model");
		$this->load->model("Common_model","Common_Model");
		

		$this->load->model(__CLASS__ . '_model','model');
		//$mod = $this->router->class.'_model'; 
	        //$this->load->model($mod,'',TRUE);
		//$this->model = $this->$mod;
		$check = $this->session->userdata('login_data');
		///// Check Session //////	
		if (empty($check)) {
			redirect('login');
		}
		$this->loginData = $this->session->userdata('login_data');
		/*echo "<pre>";
		print_r($this->loginData);exit();*/
	}

	public function index()
	{

		 // start permission 
		$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
    // end permission 

		try{


			if($this->input->post('campusid') != Null && $this->input->post('campusid') !=''){

			$campusdata = $this->input->post('campusid');
			$campexp = explode('-', $campusdata);

			 $campusid = $campexp[0];
			 $campusintimationid = $campexp[1];
			
			$content['selectedcandidatedetails']  = $this->model->getSelectedCandidate($campusid,$campusintimationid);
			$content['campusdetails'] 	          = $this->model->getCampus();

 			$content['campusid'] = $campusdata;
 			/*echo "<pre>";
 			print_r($content['selectedcandidatedetails']);exit();*/

			}else{
				$campusid ='NULL';
				$campusintimationid = 'NULL';
				$content['selectedcandidatedetails']='';
				$content['campusdetails'] 	          = $this->model->getCampus();
			}	

		$RequestMethod = $this->input->server('REQUEST_METHOD');
			
		if($RequestMethod == 'POST'){
			$save = $this->input->post('submit');
			if (isset($save) && $save =='submit' && $save !='') {

            $candidateid = $this->input->post('candidateid');
            $data      = $this->input->post('AdminApproval');
            $id     = $this->input->post('id');
            foreach ($data as $key => $value) {


              $update = array(

                'AdminApproval'  => $value
            );
              $this->db->where('candidateid',$key);
              $this->db->update('tbl_candidate_registration',$update);

          }
          redirect('Assigntc');
   
      	}
      	}	
			
			$content['title'] 		= 'Assigntc';
			$content['subview'] 	= __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

	}



	public function sendofferlettered($token){

		try{

			$getEdDetails = $this->model->getRoleEdDetails();
			//print_r($getEdDetails);
			//echo "token=".$token;
			
			 $ed_name=$getEdDetails[0]->name;
			 $hrd_name=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
			
			//$edmail = $getEdDetails[0]->EmailID;
			$hrdemail = $this->loginData->EmailID;


			$this->db->where('candidateid', $token);
			$this->db->where('flag','Generate');
			$generate_query=$this->db->get('tbl_generate_offer_letter_details');


			if ($generate_query->num_rows() ==0) {
				echo  'notgenerate';
				//die;
			}
			$this->db->where('candidateid', $token);
			$this->db->where('sendflag',1);
			$query=$this->db->get('tbl_generate_offer_letter_details');

			//echo  $query->num_rows();  die;

			if($query->num_rows() ==0){

			//	$html = 'Dear Sir, <br><br> Greetings from Bapurao Deshmukh College of Engineering, Sevagram.!!!!!!!!!!!!!!!!! We are planning to start recruitment drives for XXXXXX Batch in the month of XXXXXXXXX. It is our great privilege to invite you to visit Bapurao Deshmukh College of Engineering, Sevagram, one of the oldest Colleges in Vidarbha region for Campus Placement Recruitment Drives. We are ready to coordinate Closed/Pool drive & open to invite nearby Engineering Colleges affiliated University & Amravati University as per company requirements. So we are requesting you to finalize an early date to pick up the best candidates from our region.';
				$message='';
				//echo $message;
				//die;
				 $subject ="for approving the offer letter approval";
				 //  $body = 	'Dear Sir, <br><br> ';
				 //  $body .= 'Submit Gereral Nomination And Authorisation Form Successfully ';
				 // $body = read_file(base_url().'mailtext/verifieddocumentmailtext');
				 //     $fd = fopen("mailtext/approving_offerletter.txt","r");	
					// 	$message .=fread($fd,4096);
					// 	eval ("\$message = \"$message\";");
					// 	$message =nl2br($message);
					// 	echo $message;
					// 	die;
				 $candidate = array('$ed_name','$hrd_name');
				 $candidate_replace = array($ed_name,$hrd_name);

			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 30 AND `isactive` = '1'";
            $data = $this->db->query($sql)->row();
   			if(!empty($data))
   		    $body = str_replace($candidate,$candidate_replace , $data->lettercontent);

			$sendmail = $this->Common_Model->send_email($subject, $body, $edmail);  //// Send Mail ED ////

			$sendmailhrd = $this->Common_Model->send_email($subject, $body, $hrdemail);  //// Send Mail HRD ////

			if ($sendmail != True && $sendmailhrd !=true ) {
				echo  'fail';
					//$this->session->set_flashdata('er_msg','Sorry !!! Email Not Send. Please Try Again !!!!'); 
			}else{

				$updateArr1 = array(
					'sendflag'    		=> 1,

				);
				$this->db->where('candidateid',$token);
				$this->db->update('tbl_generate_offer_letter_details', $updateArr1);

				$updateArr2 = array(
					'flag'    		=> 3,
				);
				$this->db->where('candidateid',$token);
				$this->db->update('edcomments', $updateArr2);

				$this->session->set_flashdata('tr_msg','Successfully Send to ED !!!!');	


				echo 'send';				
			}

		}else{

			echo  'sent';

		}

				//redirect('/Assigntc/index');


	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


public function add($token, $requestedpage=NULL){

	try{

		$getcategory = '';

		$getcategory = $this->model->getCategoryid($token);

		
		
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){

			$this->db->trans_start();

			$this->form_validation->set_rules('team','Team ','trim|required');
			if ($this->loginData->RoleID!=17) {
				$this->form_validation->set_rules('fieldguide','Field Guide ','trim|required');
			}
			if ($this->loginData->RoleID!=17) {
			$this->form_validation->set_rules('batch','Batch ','trim|required');
		   }

			if($this->form_validation->run() == FALSE){
				$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',	'</div>');

				$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors    =    true;
				goto prepareview;

			}

			$updateArr = array(
				'teamid'    		=> $this->input->post('team'),
				'fgid'     			=> $this->input->post('fieldguide'),
				'batchid'    		=> $this->input->post('batch'),
				'CreatedOn'      	=> date('Y-m-d H:i:s'),
				'CreatedBy'      	=> $this->loginData->UserID, // login user id
				'isdeleted'      	=> 0, 
				);
			$this->db->where('candidateid',$token);
			$this->db->update('tbl_candidate_registration', $updateArr);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$this->session->set_flashdata('er_msg', 'Error Assign Teams');	
			}else{
				$this->session->set_flashdata('tr_msg', 'Successfully Assign Teams');			
			}

			redirect('/Assigntc/index');
		}

		prepareview:
		$content['teamsdetails']     = $this->model->getTeamlist();
		$content['batchdetails']     = $this->model->getBatchlist();
		$content['teamedetail']      = $this->model->getTeamDetail();
		$content['getcategoryid']    = $this->model->getCategoryid($token);

		$content['method']              = $this->router->fetch_method();
		$content['title']  = 'add';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}

}


public function edit($token, $requestedpage=NULL){

	try{


		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST'){
			
			$this->db->trans_start();


			if (!empty($token)) {

			 $this->db->delete('tbl_generate_offer_letter_details',array('candidateid'=> $token));
			 $this->db->delete('edcomments',array('candidateid'=> $token));
			 $this->db->delete('tbl_sendofferletter',array('candidateid'=> $token));

			$updateArr = array(
				'complete_inprocess'  => 1,
			);
			$this->db->where('candidateid',$token);
			$this->db->update('tbl_candidate_registration', $updateArr);
						
			 }

			$this->form_validation->set_rules('team','Team ','trim|required');
			if ($this->loginData->RoleID!=17) {
			$this->form_validation->set_rules('fieldguide','Field Guide ','trim|required');
		   }
		   if ($this->loginData->RoleID!=17) {
			$this->form_validation->set_rules('batch','Batch ','trim|required');
		   }

			if($this->form_validation->run() == FALSE){
				$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
					'</div>');

				$this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors    =    true;
				goto prepareview;

			}

			$updateArr = array(
				'teamid'    		=> $this->input->post('team'),
				'fgid'     			=> $this->input->post('fieldguide'),
				'batchid'    		=> $this->input->post('batch'),
				'CreatedOn'      	=> date('Y-m-d H:i:s'),
			    'CreatedBy'      	=> $this->loginData->UserID, // login user id
			    'isdeleted'      	=> 0, 
					);
			$this->db->where('candidateid',$token);
			$this->db->update('tbl_candidate_registration', $updateArr);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){

				$this->session->set_flashdata('er_msg', 'Error update Recruiters');	
			}else{

				$sql = "SELECT id FROM `tbl_generate_offer_letter_details` WHERE `candidateid` = '" . $token . "'"; 
        		$query = $this->db->query($sql);
        	//	echo $query->num_rows(); die;

        		if ($query->num_rows() > 0) {

        		$sql = "SELECT id FROM `tbl_generate_offer_letter_details` WHERE `candidateid` = '" . $token . "'"; 
        		$query = $this->db->query($sql);
        		$result = $this->db->query($sql)->result()[0];
        		$id = $result->id;



				$updateArr = array(
					'flag' => 0,
					'sendflag' => 0,

				);
				$this->db->where('id',$id);
				$this->db->update('tbl_generate_offer_letter_details', $updateArr);
        		}
				
				$this->session->set_flashdata('tr_msg', 'Successfully update Team');			
			}
			if ($requestedpage == '2')
			{
				
				redirect('/Recommended_to_graduate/index');
			}
			else
			{
				redirect('/Assigntc/index');
			}
			

		}

		prepareview:

		$content['teamsdetails']        = $this->model->getTeamlist();
		$content['batchdetails']        = $this->model->getBatchlist();

		//print_r($content['batchdetails']); die;

		$content['teamedetail']         = $this->model->getTeamDetail();
		//print_r($content['teamedetail']); die;

		$singleteamedet                 = $this->model->getSingleTeamDetail($token);

		//print_r($singleteamedet); die;
		$fieldguid                      = $singleteamedet->fgid; 
		$content['singleteamedetail'] = $singleteamedet;
		//print_r($content['singleteamedetail']); die;

		$content['getsingletfieldguide']     = $this->chooseFieldguideteam($fieldguid);
		// echo "<pre>";
		// print_r($content['getsingletfieldguide']);exit();
		// foreach($content['teamedetail'] as $res){
			// echo $res->staffid;die();
		if ($requestedpage == '2')
			{
			$content['graduate']     = $this->model->getGraduateDetail($singleteamedet->staffid);
			// print_r($content['graduate']);
			// die;
			}
			/*break;
		}*/

		   //print_r($teamedetail); die;

		$content['recruitdetails']      = $this->model->getSingelRecruitersCampus($token);
	    $content['getedcommentstatus']  = $this->model->getEdcommentStatus($token);
	   // print_r($getedcommentstatus); die;
			//$content['recruitersdetails']   = $this->model->getRecruiters();
		$content['campusdetails'] 	    = $this->model->getCampus();
		$content['getcategoryid']    = $this->model->getCategoryid($token);
		$content['title'] = 'edit';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}



public function chooseFieldguideteam($id) {

	try{

	$sql = "SELECT `staff`.staffid, `staff`.name FROM staff Where staff.`staffid`='".$id."'";  

	$result = $this->db->query($sql)->result();

	return $result;


	}catch (Exception $e) {
			print_r($e->getMessage());die;
		}

}


function delete($token = null)
{
	try{
	$this->Common_Model->delete_row('mstrecruiters','recruiterid', $token); 
	$this->session->set_flashdata('tr_msg' ,"Recruiters Deleted Successfully");
	redirect('/Assigntc/');
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);

	}catch (Exception $e) {
			print_r($e->getMessage());die;
		}
}


}