<?php 

/**
* State List
*/
class Stafftransferpromotion extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    
    $this->load->model("Staffpromotion_model");
    $this->load->model("Off_campus_candidate_list_model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {


    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from msleavecrperiod";
    $content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    $content['title'] = 'Stafftransferpromotion';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add()
  {
       $content['staff']=$this->Staffpromotion_model->getstaffname();
      


    $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
    $content['role_permission'] = $this->db->query($query)->result();

    $content['new_designation']=$this->Staffpromotion_model->get_alldesignation();

    
   $content['d_tranfer']=$this->Staffpromotion_model->get_alloffice();
   
  

    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   // echo "methods=".$RequestMethod;
   

    if($RequestMethod == 'POST'){

      
      
       // $this->form_validation->set_rules('monthfrom','From Date','trim|required|min_length[1]|max_length[50]');
     
       //$this->form_validation->set_rules('monthto','Todate','trim|required|min_length[1]|max_length[50]');

     
       ///$this->form_validation->set_rules('credit','Credit','trim|required|min_length[1]|max_length[50]');

       /* if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

          }

*/
         
           $date1=$this->input->post('fromdate');
          $date2=$this->Off_campus_candidate_list_model->changedate($date1);
        
       
          $datetime= date("Y-m-d H:i:s");
      $insertArr = array(
          'staffid'            => $this->input->post('staff'),
          'old_office_id'      => $this->input->post('Presentoffice'),
          'old_designation'    => $this->input->post('Presentdesignation'),
          'new_designation'    => $this->input->post('new_designation'),
          'new_office_id'      => $this->input->post('transfer_office'),
          'date_of_transfer'   => $date2,
          'trans_status'       =>'BOTH',
          'datetime'           =>$datetime
          
          
        );
      $this->db->insert('staff_transaction', $insertArr);

     // echo $this->db->last_query(); die;


      
      $this->session->set_flashdata('tr_msg', 'Successfully added  Staff Transfer and Promotion');
      redirect('/Staffpromotion/history');
    }

    prepareview:

    
    
    $content['subview'] = 'Staffpromotion/add';
    $this->load->view('_main_layout', $content);
  }

  
 public function history()
 {

      $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
 $content['view_history']=$this->Staffpromotion_model->getview_history();
    // print_r($result['view_history']);
// end permission 

   // $query = "select * from msleavecrperiod";
    //$content['Leave_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";

    $content['title'] = 'Staffpromotion';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);   
 }


}