<?php 
class Employeebasicsalary extends CI_Controller
{

	// echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Employeebasicsalary_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		try{
		// start permission 
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission  
			$content['stafflist'] = array();
			$content['officeid'] = array();
			$content['incrementmonth'] = '';
		//$content['stafflist'] = $this->Employeebasicsalary_model->getStaffList();
			$content['officelist'] = $this->Employeebasicsalary_model->getOfficeList();

			$RequestMethod = $this->input->server("REQUEST_METHOD");
			if($RequestMethod == 'POST')
			{
				$msg = '';

				if (!empty($this->input->post("btnincrement")) && $this->input->post("btnincrement")=='increment') {

					$this->db->trans_start();

					$slde_incrmnt = $this->input->post('slde_incrmnt');
					$txtdlwp = $this->input->post("txtdlwp");
					$basicsalary = $this->input->post('basicsalary');
					$Prebasicsalary = $this->input->post('Prebasicsalary');
					$slde = $this->input->post('slide_');
					$incrmnt = $this->input->post('incrmnt_');


					$staff = array();
					// array of staffid
					foreach ($txtdlwp as $key => $value) {
						array_push($staff,$key);
					}

					$amount = array();
					// array of amount
					foreach ($Prebasicsalary as $no => $amt) {
						array_push($amount,$amt);
					}

					$staff_amt='';
					$staff_amt = array_combine($staff,$amount); // array of staff and amount with slide/increment

					if(!empty($slde_incrmnt))
					{
					
						foreach ($staff_amt as $stff => $new_basic) {
							
								$newbasicsalary = '';
								$numnewbasicsalary = '';
								$newbasicsalary = str_replace(',', '',  $new_basic); 
								$numnewbasicsalary = intval($newbasicsalary);
								$query = "select * from tblmstemployeesalary where staffid=".$stff;
								$result = $this->db->query($query)->result();

								if(count($result) == 0){
									$data_set = array(
										'staffid'=>$stff,
										'e_basic'=>$numnewbasicsalary
									);
									$this->db->insert('tblmstemployeesalary', $data_set);
									$act= "insert";

								}else{
									$data_set = array(
										'e_basic'=>$numnewbasicsalary
									);
									$this->db->where('staffid',$staff);
									$this->db->update('tblmstemployeesalary',$data_set);
								    $act= "update";
								}

						}	
						$msg = "New Basic";
			
				
					}

					else{				// no slide no increment only increment duedate

						$incdate = '';
						$joiningmonth='';

						if (!empty($this->input->post("incrementmonthdate")) ) {
							$incearmentdate = $this->input->post("incrementmonthdate");

							foreach ($txtdlwp as $key => $value) {

								$txtdlwp = $this->input->post("txtdlwp")[$key];
								$dateofjoining = $this->input->post("dateofjoining")[$key]; 
								$joiningmonth = date('m', strtotime($dateofjoining));

								if ($value!='' || $value != 0.00) {

									if ($value >= 30 ) {

										if ($joiningmonth == 04 || $joiningmonth == 05 || $joiningmonth == 06 || $joiningmonth == 07 || $joiningmonth == 08 || $joiningmonth == 09) {

											$incdate = date('Y-m-d', strtotime($incearmentdate. ' + '.round($value).' days'));

											if ($incdate=='1970-01-01') {
												$incdateval = '0000-00-00';
											}else{
												$incdateval = $incdate;
											}

											$sql = "Update tblmstemployeesalary SET increamentduedate='".$incdateval."' WHERE staffid = $key ";
											$updateres = $this->db->query($sql); 

										}else{

											$incdate = date('Y-m-d', strtotime($incearmentdate. ' + '.round($value).' days'));

											if ($incdate=='1970-01-01') {
												$incdateval = '0000-00-00';
											}else{
												$incdateval = $incdate;
											}

											$sql = "Update tblmstemployeesalary SET increamentduedate='".$incdateval."' WHERE staffid = $key ";
											$updateres = $this->db->query($sql);

										}
				
									}else{

										if ($joiningmonth == 04 || $joiningmonth == 05 || $joiningmonth == 06 || $joiningmonth == 07 || $joiningmonth == 08 || $joiningmonth == 09) {

											$sql = "Update tblmstemployeesalary SET increamentduedate='".$incearmentdate."' WHERE staffid = $key ";
											$updateres = $this->db->query($sql);

										}else{

											$sql = "Update tblmstemployeesalary SET increamentduedate='".$incearmentdate."' WHERE staffid = $key ";
											$updateres = $this->db->query($sql);

										}

									}
								}else{

									if ($joiningmonth == 04 || $joiningmonth == 05 || $joiningmonth == 06 || $joiningmonth == 07 || $joiningmonth == 08 || $joiningmonth == 09) {

										$sql = "Update tblmstemployeesalary SET increamentduedate='".$incearmentdate."' WHERE staffid = $key ";
										$updateres = $this->db->query($sql);

									}else{

										$sql = "Update tblmstemployeesalary SET increamentduedate='".$incearmentdate."' WHERE staffid = $key ";
										$updateres = $this->db->query($sql);

									}

								}
							// echo $updateres;

							}/// end foreach

							$msg = "Increment due date";


						}/// incrementmonthdate

					}


	$this->db->trans_complete();

	if ($this->db->trans_status() === FALSE){
		$this->session->set_flashdata('er_msg', ''.$msg.' has not been done');

	}else{
		$this->session->set_flashdata('tr_msg', ''.$msg.' has been done successfully');

	}


}/// btn closed

$content['officeid'] = $this->input->post("officeid");
$content['incrementmonth'] = $this->input->post("incrementmonth");
$content['stafflist'] = $this->Employeebasicsalary_model->getStaffList($content['officeid'], $content['incrementmonth']);
}

$content['title'] = 'Employee Basic Salary';
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);


}
catch (Exception $e) {
	print_r($e->getMessage());die;
}

}

public function incrementsalaryupdate(){
	try{
		$period = $this->input->post('period');
		$periodarray = explode('-', $period);
		$month = $periodarray[1];
		$wherecondition = '';
		if($month == 04){        
			$wherecondition .= " WHERE MONTH(doj) in(4,5,6,7,8,9)";
		}elseif($month == 10){
			$wherecondition .= " WHERE MONTH(doj) in(10,11,12,1,2,3)";
		}
		$result = $this->Employeebasicsalary_model->CreateHistory($period, $wherecondition);
	// print_r($result);
	}
	catch (Exception $e) {
		print_r($e->getMessage());die;
	}
}

}