<?php 

/**
* Central Event List
*/
class Event extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model','model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');


 }

 public function index()
 {
  try{
    // start permission 
  
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $query = "select a.*,b.batch as batchname from mstevents as a left join mstbatch as b on a.batch = b.id where a.isdeleted='0'";
  $content['centralevent_details'] = $this->Common_Model->query_data($query);
  $content['subview']="index";

  $content['title'] = 'Event';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
  }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}
public function Add()
{
  try{
        // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
    // print_r($this->input->post()); //die();
  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

    $this->db->trans_start();

    $this->form_validation->set_rules('eventname','Event Name','trim|required');
    $this->form_validation->set_rules('batch','Batch','trim|required');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }
    // echo "string"; die();

    $insertArr = array(
      'name'   => $this->input->post('eventname'),
      'batch'   => $this->input->post('batch'),
      'created_date' => date('Y-m-d H:i:s'),
      'createdby'=> $this->loginData->UserID
    );

    $this->db->insert('mstevents', $insertArr);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Something went wrong while creating Event ');  

    }else{
      // $batch=$this->input->post('batch');
      // $eventbatch   = $this->model->getBatchEvent($batch);
      // $batchno = $eventbatch->batchname;

      // $AllTcemailid = $this->model->getAllSuprvsn();
      // foreach ($AllTcemailid as $key => $value) {
      //   $message='';
      //   $subject = "Announcement of Central Event I";

      //   $fd = fopen("mailtext/central_event_1.txt", "r"); 
      //   $message .=fread($fd,4096);
      //   eval ("\$message = \"$message\";");
      //   $message =nl2br($message);
      //   $body = $message;
      //   $recipients = $value->emailid;  
      //   $sendmail = $this->Common_Model->send_email($subject, $body, $recipients);
      // }


      $this->session->set_flashdata('tr_msg', 'Successfully added New Central Event');
      redirect('/Event/');
    }
  }

  prepareview:
  $content['batchdetails'] = $this->model->getBatchlist();

  $content['title']   = 'Event';
  $content['subview'] = 'Event/add';
  $this->load->view('_main_layout', $content);

  }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function edit($id)
{
  try{

        // start permission 
  $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

    $this->db->trans_start();

    $this->form_validation->set_rules('eventname','Event Name','trim|required');
    $this->form_validation->set_rules('batch','Batch','trim|required');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }

    $updateArr = array(
      'name'   => $this->input->post('eventname'),
      'batch'   => $this->input->post('batch'),
      'updatedon'=> date('Y-m-d H:i:s'),
      'updateby' => $this->loginData->UserID
    );

    $this->Common_Model->update_data('mstevents', $updateArr,'id',$id);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Something went wrong while updating Event ');  

    }else{
      // $batch=$this->input->post('batch');
      // $eventbatch   = $this->model->getBatchEvent($batch);
      // $batchno = $eventbatch->batchname;

      // $AllTcemailid = $this->model->getAllSuprvsn();
      // foreach ($AllTcemailid as $key => $value) {
      //   $message='';
      //   $subject = "Announcement of Central Event I";

      //   $fd = fopen("mailtext/central_event_1.txt", "r"); 
      //   $message .=fread($fd,4096);
      //   eval ("\$message = \"$message\";");
      //   $message =nl2br($message);
      //   $body = $message;
      //   $recipients = $value->emailid;  
      //   $sendmail = $this->Common_Model->send_email($subject, $body, $recipients);
      // }

        $this->session->set_flashdata('tr_msg', 'Successfully Updated Event');
    redirect('/Event/');
    }

  
  }


  prepareview:

  $content['title'] = 'Event';
  $centralevent_details = $this->Common_Model->get_data('mstevents', '*', 'id',$id);
  $content['centralevent_details'] = $centralevent_details;
  $content['batchdetails'] = $this->model->getBatchlist();
  $content['eventbatch_detail']         = $this->model->getBatchEventDetail($id);
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

function delete($token)
{
  try{
  $this->Common_Model->delete_row('mstevents','id', $token); 
  $this->session->set_flashdata('tr_msg' ,"Event Deleted Successfully");
  redirect('/Event/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);

  }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}


public function central_event1($token)
 {
  try{

    // start permission 
   $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   $hr_name='';
   $batch_no='$batch_no';
   $scheduled_month='$scheduled_month';
   $supervisior_name='';
   $excluding_travel_time='$excluding_travel_time';
   $dates='$dates';
   $onedaybeforebeginning_date='$onedaybeforebeginning_date';
   $ending_date='$onedaybeforebeginning_date';


             $hr_name=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
             
             $supervisior = $this->model->getAllSuprvsn();
             
             
             $content['reportint_to'] = $this->model->getreportint_to($this->loginData->staffid);
            
             $supervisior_name=$content['reportint_to']->name;
 

   
   $message='';
          $fd = fopen("mailtext/cental_event_tc.txt", "r"); 
            $message .=fread($fd,4096);
            eval ("\$message = \"$message\";");
            $message =nl2br($message);
          
             $content['message']=$message;

            

  
   $RequestMethod = $this->input->server('REQUEST_METHOD');
      
      if($RequestMethod == 'POST'){
        
        $to_email='';
       
        $content=$this->input->post('content');
        $subject='Announcement of Central Event';

        // $recipients=array();

        $recipients1 = array();
        foreach($supervisior as  $key=>$value) {
          $email=$value->emailid;
          $name=$value->name;

          $recipients2 = array(
          $value->emailid => $name);
          $recipients1 = array_merge($recipients1,$recipients2);
        }
        //  echo "<pre>";
        // print_r($recipients1);
        // die;
         
          $content =nl2br($content);
           $sendmail = $this->Common_Model->send_email($subject,$content,$to_email= null,$to_name= null,$recipients1);
           if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Email is not send');  

    }else{
    
      $this->session->set_flashdata('tr_msg', 'Successfully Email send to supervisior');
      redirect('/Event/');
    }

            

      }

   
     

   $content['title'] = 'event/central_event1';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);
   }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }



public function mid_term_dc_event($token)
 {
  try{

    // start permission 
   $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   $hr_name='';
   $supervisior_name='';
    $batch_no='$batch_no';
$sixth_month='$sixth_month';
$date_slot='$date_slot';

             $hr_name=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
             
             $supervisior = $this->model->getAllSuprvsn();
             
             
             $content['reportint_to'] = $this->model->getreportint_to($this->loginData->staffid);
            
             $supervisior_name=$content['reportint_to']->name;
 

   
   $message='';
          $fd = fopen("mailtext/mid_term_dc_event.txt", "r"); 
            $message .=fread($fd,4096);
            eval ("\$message = \"$message\";");
            $message =nl2br($message);
          
             $content['message']=$message;

            

  
   $RequestMethod = $this->input->server('REQUEST_METHOD');
      
      if($RequestMethod == 'POST'){
        
        $to_email='';
       
        $content=$this->input->post('content');
        $subject='Mid-term event for Apprentices';

        $recipients=array();

        $recipients1 = array();
        foreach($supervisior as  $key=>$value) {
          $email=$value->emailid;
          $name=$value->name;

          $recipients2 = array(
          $value->emailid => $name);
          $recipients1 = array_merge($recipients1,$recipients2);
        }
        //  echo "<pre>";
        // print_r($recipients1);
        // die;
         
 // $to_name="Amit";
 //  $to_email="amit.kum2008@gmail.com";
$content =nl2br($content);
           $sendmail = $this->Common_Model->send_email($subject,$content,$to_email = null,$to_name= null,$recipients1);
           if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Email is not send');  

    }else{
    
      $this->session->set_flashdata('tr_msg', 'Successfully Email send to supervisior');
      redirect('/Event/');
    }

            

      }

   
     

   $content['title'] = 'event/mid_term_dc_event';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }


 }



public function central_event2($token)
 {
  try{


    // start permission 
   $query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 
   $hr_name='';
   $supervisior_name='';
    $batch_no='$batch_no';
$dateand_venue='$dateand_venue';
$venue='$venue';
$oneday_before='$oneday_before';
$time_and_end_date='$time_and_end_date';


             $hr_name=$this->loginData->UserFirstName.$this->loginData->UserMiddleName.$this->loginData->UserLastName;
             
             $supervisior = $this->model->getAllSuprvsn();
             
             
             $content['reportint_to'] = $this->model->getreportint_to($this->loginData->staffid);
            
             $supervisior_name=$content['reportint_to']->name;
 

   
   $message='';
          $fd = fopen("mailtext/central_event2.txt", "r"); 
            $message .=fread($fd,4096);
            eval ("\$message = \"$message\";");
            $message =nl2br($message);
          
             $content['message']=$message;

            

  
   $RequestMethod = $this->input->server('REQUEST_METHOD');
      
      if($RequestMethod == 'POST'){
        
        $to_email='';
       
        $content=$this->input->post('content');
        $subject='Announcement of Central Event II for 69th batch of Apprentices';

        $recipients=array();

        $recipients1 = array();
        foreach($supervisior as  $key=>$value) {
          $email=$value->emailid;
          $name=$value->name;

          $recipients2 = array(
          $value->emailid => $name);
          $recipients1 = array_merge($recipients1,$recipients2);
        }
        //  echo "<pre>";
        // print_r($recipients1);
        // die;
         
 // $to_name="Amit";
 //  $to_email="amit.kum2008@gmail.com";
$content =nl2br($content);
           $sendmail = $this->Common_Model->send_email($subject,$content,$to_email= null,$to_name= null,$recipients1);
           if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Email is not send');  

    }else{
    
      $this->session->set_flashdata('tr_msg', 'Successfully Email send to supervisior');
      redirect('/Event/');
    }

            

      }

   
     

   $content['title'] = 'event/central_event2';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }
      catch (Exception $e) {
      print_r($e->getMessage());die;
    }

 }



}