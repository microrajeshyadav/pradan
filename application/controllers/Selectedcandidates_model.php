<?php 

/**
* Dashboard Model
*/
class Selectedcandidates_model extends Ci_model
{
	
	function __construct()
	{
		parent::__construct();
	}

   public function getSelectedCandidate($token = NULL)
  {
    
    try{

       $sql = "SELECT `tbl_candidate_registration`.candidateid,`tbl_candidate_registration`.emailid,`tbl_candidate_registration`.candidatefirstname,`tbl_candidate_registration`.candidatemiddlename,`tbl_candidate_registration`.candidatelastname, `tbl_candidate_registration`.fatherfirstname,`tbl_candidate_registration`.fathermiddlename,`tbl_candidate_registration`.fatherlastname,`tbl_candidate_registration`.gender,`tbl_candidate_registration`.dateofbirth,`tbl_candidate_registration`.mobile FROM `tbl_candidate_registration` INNER join `tbl_candidate_writtenscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_writtenscore`.candidateid INNER join `tbl_candidate_gdscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_gdscore`.candidateid INNER join `tbl_candidate_hrscore` ON `tbl_candidate_registration`.candidateid = `tbl_candidate_hrscore`.candidateid Where TIMESTAMPDIFF(YEAR,dateofbirth,CURDATE()) <= 30 AND `tbl_candidate_registration`.metricpercentage >= 60 AND `tbl_candidate_registration`.hscpercentage >= 60 And ( `tbl_candidate_registration`.ugpercentage >= 60 And (`tbl_candidate_registration`.`hscpercentage`+4) <= `tbl_candidate_registration`.`ugpassingyear` OR (`tbl_candidate_registration`.`ugpercentage` >= 55 AND `tbl_candidate_registration`.`pgpercentage` >= 60)) AND `tbl_candidate_writtenscore`.writtenscore != 0 AND `tbl_candidate_writtenscore`.writtenscore > 50 AND `tbl_candidate_gdscore`.gdscore !=0 AND `tbl_candidate_gdscore`.gdscore > 50 AND `tbl_candidate_hrscore`.hrscore !=0 AND `tbl_candidate_hrscore`.hrscore > 50 ";

            if($token !=''){

              $sql.= " AND `tbl_candidate_registration`.campusid ='".$token."'";
            }

            $sql .=' ORDER BY candidateid DESC';

           //echo $sql; die;
        
        $res = $this->db->query($sql)->result();

        return $res;
        
     }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
  }
	public function index()
	{

	}
  

	public function getCampus()
    {
        try{
           
            $sql = "SELECT * FROM `mstcampus`  WHERE IsDeleted=0";  
            $res = $this->db->query($sql)->result();

            return $res;

        }catch(Exception $e){
            print_r($e->getMessage());die();
        }
    }

}