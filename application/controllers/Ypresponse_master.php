<?php 

/**
* Ypresponse Master List
*/
class Ypresponse_master extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }
    
  }

  public function index()
  {
     
    // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from state where isdeleted='0'";
    $content['state_details'] = $this->Common_Model->query_data($query);
    $content['subview']="index";

    $content['title'] = 'Ypresponse_master';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add()
  {

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 
    
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

        $this->form_validation->set_rules('statecode','State Code','trim|required|min_length[1]|max_length[2]|numeric');
        $this->form_validation->set_rules('statename','State Name','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
        'name'         => $this->input->post('statename'),
        'statecode'     => $this->input->post('statecode'),
        'created_date' => date('Y-m-d H:i:s'),
        'IsDeleted'    => $this->input->post('status'),
        );
      
      $this->Common_Model->insert_data('state', $insertArr);
      $this->session->set_flashdata('tr_msg', 'Successfully added State');
      redirect('/Ypresponse_master/');
    }

    prepareview:

    $content['title']   = 'Ypresponse_master';
    $content['subview'] = 'Ypresponse_master/add';
    $this->load->view('_main_layout', $content);
  }

  public function edit($id=NULL)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('statecode','State Code','trim|required|min_length[1]|max_length[2]|numeric');
        $this->form_validation->set_rules('statename','State Name','trim|required');
        $this->form_validation->set_rules('status','Status','trim|required');

      if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

      $updateArr = array(
        'name'         => $this->input->post('statename'),
        'statecode'      => $this->input->post('statecode'),
        'modified_date' => date('Y-m-d H:i:s'),
        'IsDeleted'    => $this->input->post('status'),
        );
      
      $this->Common_Model->update_data('state', $updateArr,'id',$id);
      $this->session->set_flashdata('tr_msg', 'Successfully Updated State');
       redirect('/Ypresponse_master/');
    }

    prepareview:

    $content['title'] = 'State';
    $state_details = $this->Common_Model->get_data('state', '*', 'id',$id);
    $content['state_details'] = $state_details;
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }
    function delete($token = null)
    {
      $this->Common_Model->delete_row('state','id', $token); 
      $this->session->set_flashdata('tr_msg' ,"State Deleted Successfully");
      redirect('/Ypresponse_master/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
  }

}