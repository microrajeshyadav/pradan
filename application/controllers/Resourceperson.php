<?php 

/**
* Resource Person Controller
*/
class Resourceperson extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model','model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }
   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {

    // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  $query = "select * from mst_resource_person where isdeleted='0'";
  $content['resource_person_details'] = $this->Common_Model->query_data($query);
  $content['subview']="index";

  $content['title'] = 'Resourceperson';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
}
public function Add()
{

        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 




  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){
      //print_r($this->input->post()); die;

    $this->form_validation->set_rules('mobile','Mobile','trim|required|min_length[1]|max_length[10]|numeric');
    $this->form_validation->set_rules('resource_person_name','Resource Person Nname','trim|required');
    $this->form_validation->set_rules('location','Location','trim|required');

    $this->form_validation->set_rules('status','Status','trim|required');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }


    $insertArr = array(
      'name'         => $this->input->post('resource_person_name'),
      'mobile'       => $this->input->post('mobile'),
      'location'     => $this->input->post('location'),
      'createdon'    => date('Y-m-d H:i:s'),
      'createdby'    => $this->loginData->UserID,
      'IsDeleted'    => $this->input->post('status'),
    );

    $this->db->insert('mst_resource_person', $insertArr);
    $this->session->set_flashdata('tr_msg', 'Successfully added Resource Person');
    redirect('/Resourceperson/index');
  }

  prepareview:

  $content['title']   = 'Resourceperson';
  $content['subview'] = 'Resourceperson/add';
  $this->load->view('_main_layout', $content);
}

public function edit($id=NULL)
{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    //print_r( $this->input->post()); die;

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

   $this->form_validation->set_rules('mobile','Mobile','trim|required|min_length[1]|max_length[10]|numeric');
   $this->form_validation->set_rules('resource_person_name','Resource Person Nname','trim|required');
   $this->form_validation->set_rules('location','Location','trim|required');

   $this->form_validation->set_rules('status','Status','trim|required');


   if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }

  $updateArr = array(
    'name'         => $this->input->post('resource_person_name'),
    'mobile'       => $this->input->post('mobile'),
    'location'     => $this->input->post('location'),
    'updatedon'   => date('Y-m-d H:i:s'),
    'updatedby'   => $this->loginData->UserID,
    'isdeleted'   => $this->input->post('status'),
  );

  $this->Common_Model->update_data('mst_resource_person', $updateArr,'id',$id);
  $this->session->set_flashdata('tr_msg', 'Successfully Updated Resource Person');
  redirect('/Resourceperson/index');
}

prepareview:

$content['title'] = 'Resourceperson';
$state_details = $this->Common_Model->get_data('mst_resource_person', '*', 'id',$id);
$content['resource_person_details'] = $state_details;
$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);
}
function delete($token = null)
{
  $this->Common_Model->delete_row('mst_resource_person','id', $token); 
  $this->session->set_flashdata('tr_msg' ,"Resource Person Deleted Successfully");
  redirect('/Resourceperson/');
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
  $this->load->view('_main_layout', $content);
}

}