<?php 

///// Clearance Certificate On Transfer Controller   

class Clearance_certificate_on_transfer extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;
		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{
	  
		try{
	    // start permission 
	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		$RequestMethod = $this->input->server('REQUEST_METHOD');			
		if($RequestMethod == 'POST'){
		}

		$content['title'] = 'Clearance_certificate_on_transfer';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
		 }catch (Exception $e) {
       print_r($e->getMessage());die;
     }

	}

	public function add()
	{
	   
		try{
	    // start permission 
	 	$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission
		$content['getstafflist'] = $this->model->getStaffList();
		$content['subview']="index";
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST'){
		}			
		
		$content['title'] = 'Clearance_certificate_on_transfer';
		$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
		$this->load->view('_main_layout', $content);
			
	 }catch (Exception $e) {
       print_r($e->getMessage());die;
     }

 }

 
}