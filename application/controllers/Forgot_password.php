<?php 

/*
 Forget  password

*/
class Forget_password extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model(__CLASS__ . '_model', 'model');	
	}

	public function index()
	{
		try{
     // echo  $this->session;
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == "POST")
		{
			if(!$this->model->forgetpassword()){
				redirect('forget_password');
			}else{
				redirect('dashboard');
			}
		}
		$this->load->view(__CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__);

		}
	    catch (Exception $e) {
      print_r($e->getMessage());die;
    }
		
	}	

}