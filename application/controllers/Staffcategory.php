<?php 

/**
* State List
*/
class Staffcategory extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
       redirect('login');
    }

     $this->loginData = $this->session->userdata('login_data');
  }

  public function index()
  {
     
    // start permission 
 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

    $query = "select * from mst_staff_category where isdeleted='0' ORDER BY id DESC";
    $content['staffcategory_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
    $content['title'] = 'Staffcategory';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
  }
  public function Add()
  {

        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


    $RequestMethod = $this->input->server('REQUEST_METHOD'); 
   

    if($RequestMethod == 'POST'){
      



        $this->form_validation->set_rules('shortname','Short name','trim|required|min_length[1]|max_length[10]');
        $this->form_validation->set_rules('categoryname','Category Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }


      $insertArr = array(
          'shortname'      => $this->input->post('shortname'),
          'categoryname'   => $this->input->post('categoryname'),
          'createdon'      => date('Y-m-d H:i:s'),
          'createdby'      => $this->loginData->UserID,
          'status'      => $this->input->post('status')
        );
      
      $this->db->insert('mst_staff_category', $insertArr);
     // echo $this->db->last_query(); die;
      $this->session->set_flashdata('tr_msg', 'Successfully added Staff Category');
      redirect('/Staffcategory/index/');
    }

    prepareview:

   
    $content['subview'] = 'Staffcategory/add';
    $this->load->view('_main_layout', $content);
  }

  public function edit($token)
  {
        // start permission 
$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
$content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
    //print_r( $this->input->post()); die;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $this->form_validation->set_rules('shortname','Short name','trim|required|min_length[1]|max_length[10]');
        $this->form_validation->set_rules('categoryname','Category Name','trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('status','Status','trim|required');

        if($this->form_validation->run() == FALSE){
          $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
            '</div>');

          $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

          $hasValidationErrors    =    true;
          goto prepareview;

        }

        $updateArr = array(
          'shortname'      => $this->input->post('shortname'),
          'categoryname'   => $this->input->post('categoryname'),
          'updatedon'      => date('Y-m-d H:i:s'),
          'updatedby'      => $this->loginData->UserID,
          'status'         => $this->input->post('status')
        );
      
      $this->db->where('id', $token);
      $this->db->update('mst_staff_category', $updateArr);
  $this->db->last_query(); 

      $this->session->set_flashdata('tr_msg', 'Successfully Updated Staff Category');
       redirect('/Staffcategory/');
    }

    prepareview:

    $content['title'] = 'Staffcategory';
    $state_details = $this->Common_Model->get_data('mst_staff_category', '*', 'id',$token);
    $content['staffcategory_details'] = $state_details;

 

    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);
   }
    function delete($token = null)
    {
      //print_r($token);die();
       $updateArr = array(
        'Isdeleted'    => 1,
        );
      $this->db->where('id', $token);
      $this->db->update('mst_staff_category', $updateArr);
      $this->session->set_flashdata('tr_msg', 'Staff Category Deleted Successfully');
      redirect('/Staffcategory/');
      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
      $this->load->view('_main_layout', $content);
  }

}