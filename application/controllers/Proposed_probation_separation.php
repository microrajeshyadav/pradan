<?php 
class Proposed_probation_separation extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Proposed_probation_separation_model');
    $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");

    //$this->load->model(__CLASS__ . '_model');
    $mod = $this->router->class.'_model';
    $this->load->model($mod,'',TRUE);
    $this->model = $this->$mod;

    $check = $this->session->userdata('login_data');

    ///// Check Session //////  
    if (empty($check)) {

     redirect('login');

   }

   $this->loginData = $this->session->userdata('login_data');

//print_r($this->loginData); die;

 }

 public function index()
 {
  try{

  $content['getprobationdetails'] = $this->model->get_Probation_Separation();
  $content['getprobationdetailsapproval'] = $this->model->get_Probation_Separation_approval();
  // $content['getprobationdetailsleaveapproval'] = $this->model->get_Probation_Separation_leave_approval();
  $content['getgraduatedetails'] = $this->model->get_Recommended_to_graduate();
  $content['gettransferdetails'] = $this->model->get_Recommended_to_transfer();
  $content['getextentiondetails'] = $this->model->get_Recommended_to_Extention();
   $content['teamsdetails'] = $this->model->getTeamlist();
  /*echo "<pre>";
  print_r($content['getgraduatedetails']);exit();*/
  $content['title'] = 'Campus';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;     
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}

public function getForwardedWorkFlowID($token)
{
  try{
  $res = $this->db->query("select workflowid from tbl_workflowdetail where r_id='". $token ."' order by workflowid desc limit 0,1")->result();
  if(!empty($res))
  {
    return $res[0]->workflowid;
  }
  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }
}


public function summary($token)
{
  
  try
  {
    if($token)
    {
      //print_r($_POST); die;
      $RequestMethod = $this->input->server("REQUEST_METHOD"); 
      
      if($RequestMethod == 'POST')
      {

        //$this->db->trans_start();
        if($this->input->post('btnsubmit') == 'Save')
        { 
          if($this->input->post('id_summary') > 0)
          {
            $updateArrary = array(
            
              'leave_entitled' => $this->security->xss_clean($this->input->post('leave_entitled')),
              'leave_taken' => $this->security->xss_clean($this->input->post('leave_taken')),
              'date_apprectice_completion' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_apprectice_completion'))),
              'date_executive_starting' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_executive_starting'))),
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
              
            );
            $this->db->where('id_summary', $this->input->post('id_summary'));
            $flag_trans=$this->db->update('tbl_da_summary', $updateArrary);
            //echo $this->db->last_query(); die;
          }
          else
          {

            $insertArray = array(
              
              'candidateid' => $this->security->xss_clean($this->input->post('candidateid')),
              'transid' => $this->security->xss_clean($this->input->post('transid')),
              'staffid' => $this->security->xss_clean($this->input->post('staffid')),
              'leave_entitled' => $this->security->xss_clean($this->input->post('leave_entitled')),
              'leave_taken' => $this->security->xss_clean($this->input->post('leave_taken')),
              'date_apprectice_completion' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_apprectice_completion'))),
              'date_executive_starting' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_executive_starting'))),
              'flag'           => 0,
              'createdon'      => date("Y-m-d H:i:s"),
              'createdby'      => $this->loginData->staffid,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
        
            );
  
            $flag_trans = $this->db->insert("tbl_da_summary", $insertArray);
            // echo $this->db->last_query(); die;
          }
        }
        else if($this->input->post('btnsubmit') == 'Save and Submit')
        {       
        
          //echo $this->input->post('id_summary'); die;
          if($this->input->post('id_summary') > 0)
          {
            $updateArrary = [
            
              'leave_taken' => $this->security->xss_clean($this->input->post('leave_taken')),
              'date_apprectice_completion' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_apprectice_completion'))),
              'date_executive_starting' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_executive_starting'))),
              'flag'           => 1,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
        
            ];
            $this->db->where('id_summary', $this->input->post('id_summary'));
            //echo $this->db->last_query();
            $flag_trans=$this->db->update('tbl_da_summary', $updateArrary);
          }
          else
          {

            $insertArray = [
              
              'candidateid' => $this->security->xss_clean($this->input->post('candidateid')),
              'transid' => $this->security->xss_clean($this->input->post('transid')),
              'staffid' => $this->security->xss_clean($this->input->post('staffid')),
              'leave_entitled' => $this->security->xss_clean($this->input->post('leave_entitled')),
              'leave_taken' => $this->security->xss_clean($this->input->post('leave_taken')),
              'date_apprectice_completion' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_apprectice_completion'))),
              'date_executive_starting' => $this->gmodel->changedatedbformate($this->security->xss_clean($this->input->post('date_executive_starting'))),
              'flag'           => 1,
              'createdon'      => date("Y-m-d H:i:s"),
              'createdby'      => $this->loginData->staffid,
              'updatedon'      => date("Y-m-d H:i:s"),
              'updatedby'      => $this->loginData->staffid
        
            ];
  
            $flag_trans=$this->db->insert("tbl_da_summary", $insertArray);
          }
          
          
          $this->db->trans_start();
          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('er_msg', 'Error !!! Clearance certificate form not submitted successfully'); 
          }
          else
          {
            $comments = 'Summary updated by HR';
            $updateArr = array(
                          'trans_flag'     => 7,
                          'updatedon'      => date("Y-m-d H:i:s"),
                          'updatedby'      => $this->loginData->staffid
                        );

            $this->db->where('id',$token);
            $this->db->update('staff_transaction', $updateArr);
            $insertworkflowArr = array(
                  'r_id'           => $token,
                  'type'           => '29',
                  'staffid'        => $this->input->post('staffid'),
                  'sender'         => $this->loginData->staffid,
                  'receiver'       => $this->loginData->staffid,
                  'senddate'       => date("Y-m-d H:i:s"),
                  'flag'           => 7,
                  'scomments'      => $comments,
                  'createdon'      => date("Y-m-d H:i:s"),
                  'createdby'      => $this->loginData->staffid,
                  'forwarded_workflowid' => $this->getForwardedWorkFlowID($token)
                );
            $this->db->insert('tbl_workflowdetail', $insertworkflowArr);
            $this->session->set_flashdata('tr_msg', 'Summary form submitted successfully !!!');
            redirect('/Proposed_probation_separation/index');     
          }
        }      
      }
      
      $res = $this->model->getDASummary($token);

      $content['da_summary'] = $res['da_summary'];
      $content['da_gap_summary'] = $res['da_gap_summary'];
      $content['da_feedback_summary'] = $res['da_feedback_summary'];
      $content['da_reports_summary'] = $res['da_reports_summary'];
      /*echo "<pre>";
      print_r($content);exit();*/
      $query = "SELECT floor(SUM(datediff(todate,fromdate))/365) AS DateDiff FROM tbl_work_experience where candidateid=".$res['da_summary']->candidateid." GROUP BY candidateid"; //(for no of experience)
      $result = $this->db->query($query)->row();
      if (empty($result ))
      {
        $DateDiff = 0;
      }else{
        $DateDiff = $result->$DateDiff;
      }

                              $this->db->where('level', 5);
                              $this->db->where('noofyear', $DateDiff);
                              $this->db->select('basicsalary');
                              $this->db->from('tblsalary_increment_slide');
      $content['basic_pay'] = $this->db->get()->result()[0];

      if (empty($content['basic_pay']))
      {
        $content['basic_pay']->basicsalary = 0;
      }
      $content['token'] = $token;

      $content['title'] = 'Experience_certificate';
      /*echo "<pre>";
      print_r($content['da_summary']);exit();*/

      $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;     
      $this->load->view('_main_layout', $content);
    }
    else
    {
      $this->session->set_flashdata('er_msg','Missing trans id!!');
      header("location:javascript://history.go(-1)", 'refresh');
    }

  }
  catch(Exception $e)
  {
    print_r($e->getMessage());
    die();
  }
}

public function graduateDA()
{
  
  try
  {
    $RequestMethod = $this->input->server("REQUEST_METHOD"); 
      
    if($RequestMethod == 'POST')
    {
      $da_staffid = $this->input->post('da_staffid');
      // print_r($_POST); die;
      if($da_staffid != '')
      {
        $staffArr = explode(',', $da_staffid);
        $staffno = '';
        $staffno = count($staffArr);
        $this->db->trans_start();
        foreach($staffArr as $staffid){
          

          $qry = "SELECT candidateid, emailid from staff where staffid=".$staffid;
          $res = $this->db->query($qry)->row();

          if($res->candidateid){
            
            $qry1 = "SELECT * FROM tbl_candidate_registration where candidateid=".$res->candidateid;
            $res1 = $this->db->query($qry1)->row();
            $updateArrary = array(
              'emailid'=>'test1'.$res->candidateid.$res->emailid,
              'username'=>'test1'.$res->candidateid.$res->emailid
            );
            $this->db->where('candidateid', $res->candidateid);
            $flag_trans=$this->db->update('tbl_candidate_registration', $updateArrary);
            $categoryid = 2;
            $yprststus = 'yes';
            $yprsent = 'yes';
            $yprdate = date('Y-m-d');
            $confirm_attend = 1;
            // $campustype = 'off';
            //$complete_inprocess = 0;

            $array = array(
              'candidatefirstname'=> $res1->candidatefirstname,
              'candidatemiddlename'=>$res1->candidatemiddlename,
              'candidatelastname'=>$res1->candidatelastname,
              'motherfirstname'=>$res1->motherfirstname,
              'mothermiddlename'=>$res1->mothermiddlename,
              'motherlastname'=>$res1->motherlastname,
              'fatherfirstname'=>$res1->fatherfirstname,
              'fathermiddlename'=>$res1->fathermiddlename,
              'fatherlastname'=>$res1->fatherlastname,
              'gender'=>$res1->gender,
              'nationality'=>$res1->nationality,
              'maritalstatus'=>$res1->maritalstatus,
              'dateofbirth'=>$res1->dateofbirth,
              'emailid'=>$res1->emailid,
              'mobile'=>$res1->mobile,
              'metricschoolcollege'=>$res1->metricschoolcollege,
              'metricboarduniversity'=>$res1->metricboarduniversity,
              'metricpassingyear'=>$res1->metricpassingyear,
              'metricplace'=>$res1->metricplace,
              'metricspecialisation'=>$res1->metricspecialisation,
              'metricpercentage'=>$res1->metricpercentage,
              'hscschoolcollege'=>$res1->hscschoolcollege,
              'hscboarduniversity'=>$res1->hscboarduniversity,
              'hscpassingyear'=>$res1->hscpassingyear,
              'hscplace'=>$res1->hscplace,
              'hscspecialisation'=>$res1->hscspecialisation,
              'hscpercentage'=>$res1->hscpercentage,
              'hscstream'=>$res1->hscstream,
              'ugschoolcollege'=>$res1->ugschoolcollege,
              'ugboarduniversity'=>$res1->ugboarduniversity,
              'ugpassingyear'=>$res1->ugpassingyear,
              'ugplace'=>$res1->ugplace,
              'ugspecialisation'=>$res1->ugspecialisation,
              'ugpercentage'=>$res1->ugpercentage,
              'ugdegree'=>$res1->ugdegree,
              'pgschoolcollege'=>$res1->pgschoolcollege,
              'pgboarduniversity'=>$res1->pgboarduniversity,
              'pgpassingyear'=>$res1->pgpassingyear,
              'pgplace'=>$res1->pgplace,
              'pgspecialisation'=>$res1->pgspecialisation,
              'pgpercentage'=>$res1->pgpercentage,
              'pgdegree'=>$res1->pgdegree,
              'otherschoolcollege'=>$res1->otherschoolcollege,
              'otherboarduniversity'=>$res1->otherboarduniversity,
              'otherpassingyear'=>$res1->otherpassingyear,
              'otherplace'=>$res1->otherplace,
              'otherspecialisation'=>$res1->otherspecialisation,
              'otherpercentage'=>$res1->otherpercentage,
              'teamid'=>$res1->teamid,
              'createdon'=>$res1->createdon,
              'updatedby'=>$res1->updatedby,
              'updatedon'=>$res1->updatedon,
              'isdeleted'=>$res1->isdeleted,
              'campusid'=>$res1->campusid,
              'password'=>$res1->password,
              'username'=>$res1->username,
              'bloodgroup'=>$res1->bloodgroup,
              'originalphotoname'=>$res1->originalphotoname,
              'encryptedphotoname'=>$res1->encryptedphotoname,
              'BDFFormStatus'=>$res1->BDFFormStatus,
              'encryptmetriccertificate'=>$res1->encryptmetriccertificate,
              'encrypthsccertificate'=>$res1->encrypthsccertificate,
              'encryptugcertificate'=>$res1->encryptugcertificate,
              'encryptpgcertificate'=>$res1->encryptpgcertificate,
              'encryptothercertificate'=>$res1->encryptothercertificate,
              'originalmetriccertificate'=>$res1->originalmetriccertificate,
              'originalhsccertificate'=>$res1->originalhsccertificate,
              'originalugcertificate'=>$res1->originalugcertificate,
              'originalpgcertificate'=>$res1->originalpgcertificate,
              'originalothercertificate'=>$res1->originalothercertificate,
              'categoryid'=>$categoryid,
              'inprocess'=>$res1->inprocess,
              'frstatus'=>$res1->frstatus,
              'frsent'=>$res1->frsent,
              'frdate'=>$res1->frdate,
              'frcreatedby'=>$res1->frcreatedby,
              'yprststus'=>$yprststus,
              'yprsent'=>$yprsent,
              'yprdate'=>$yprdate,
              'yprcreatedby'=>$res1->yprcreatedby,
              'confirm_attend'=>$confirm_attend,
              'interview_date'=>$res1->interview_date,
              'campustype'=>$res1->campustype,
              'wstatus'=>$res1->wstatus,
              'gdstatus'=>$res1->gdstatus,
              'hrstatus'=>$res1->hrstatus,
              'complete_inprocess'=>$res1->complete_inprocess,
              'otherdegree'=>$res1->otherdegree,
              'other_degree_specify'=>$res1->other_degree_specify,
              'campusintimationid'=>$res1->campusintimationid,
              'gdemail_status'=>$res1->gdemail_status,
              'BDFStatusaftergd'=>$res1->BDFStatusaftergd,
              'metricschoolstream'=>$res1->metricschoolstream,
              'ugmigration_certificate'=>$res1->ugmigration_certificate,
              'ugmigration_certificate_date'=>$res1->ugmigration_certificate_date,
              'ugmigration_original_certificate_upload'=>$res1->ugmigration_original_certificate_upload,
              'ugmigration_encrypted_certificate_upload'=>$res1->ugmigration_encrypted_certificate_upload,
              'pgmigration_certificate'=>$res1->pgmigration_certificate,
              'pgmigration_certificate_date'=>$res1->pgmigration_certificate_date,
              'pgmigration_original_certificate_upload'=>$res1->pgmigration_original_certificate_upload,
              'pgmigration_encrypted_certificate_upload'=>$res1->pgmigration_encrypted_certificate_upload,
              'old_candidate_id'=>$res->candidateid
            );
          }
          /*echo "<pre>";
          print_r($array);exit();*/
          $this->db->insert('tbl_candidate_registration', $array);
          $newcandidateid = $this->db->insert_id();
          $qry2 = "select * from tbl_candidate_communication_address where candidateid=".$res->candidateid;
          $res2 = $this->db->query($qry2)->row();
          if(!empty($res2)){
            $insertCommunicationAddress  = array(
              'candidateid'       => $newcandidateid,
              'presentstreet'       => $res2->presentstreet,
              'presentcity'         => $res2->presentcity,
              'presentstateid'      => $res2->presentstateid,
              'presentdistrict '      => $res2->presentdistrict,
              'presentpincode'        => $res2->presentpincode,
              'permanentstreet'     => $res2->permanentstreet,
              'permanentcity'         => $res2->permanentcity,
              'permanentstateid'    => $res2->permanentstateid,
              'permanentdistrict '    => $res2->permanentdistrict,
              'permanentpincode'      => $res2->permanentpincode,
              'presenthno'            => $res2->presenthno,
              'permanenthno'          => $res2->permanenthno,
              'createdon'           => $res2->createdon,
              'createdby'           => $res2->createdby, // login user id
              'isdeleted'           => 0,
            );
            $this->db->insert('tbl_candidate_communication_address', $insertCommunicationAddress);
          }

          $qry3 = "select * from tbl_gap_year where candidateid=".$res->candidateid;
          $res3 = $this->db->query($qry3)->result();

          if(!empty($res3)){
            foreach ($res3 as $value) {
              $insertGapyear   = array(
                'candidateid'      => $newcandidateid,
                'fromdate'         =>  $value->fromdate,
                'todate'           => $value->todate,
                'reason'           => $value->reason,
                'createdon'        => $value->createdon,
                'isdeleted'        => 0
              );
              $this->db->insert('tbl_gap_year', $insertGapyear);
            }
          }
          $qry4 = "select * from tbl_training_exposure where candidateid=".$res->candidateid;
          $res4 = $this->db->query($qry4)->result();

          if(!empty($res4)){
            foreach ($res4 as $value) {
              $insertTrainingExposure  = array(
                'candidateid'      => $newcandidateid,
                'natureoftraining' => $value->natureoftraining,
                'organizing_agency'=> $value->organizing_agency,
                'fromdate'         => $value->fromdate,
                'todate'           => $value->todate,
                'createdon'        => $value->createdon,
                'isdeleted'        => 0, 
              );
              $this->db->insert('tbl_training_exposure', $insertTrainingExposure);
            }
          }
          $qry5 = "select * from tbl_language_proficiency where candidateid=".$res->candidateid;
          $res5 = $this->db->query($qry5)->result();

          if(!empty($res5)){
            foreach ($res5 as $value) {
              $insertLanguageProficiency = array(
                'candidateid'  => $newcandidateid,
                'languageid'   => $value->languageid,
                'lang_speak'   => $value->lang_speak,
                'lang_read'    => $value->lang_read,
                'lang_write'   => $value->lang_write,
                'createdon'    => $value->createdon,
                'isdeleted'    => 0, 
              );
              $this->db->insert('tbl_language_proficiency', $insertLanguageProficiency);
            }
          }
          $qry6 = "select * from tbl_work_experience where candidateid=".$res->candidateid;
          $res6 = $this->db->query($qry6)->result();

          if(!empty($res6)){
            foreach ($res6 as $value) {
              $insertWorkExperience1 = array(
                'candidateid'             => $newcandidateid,
                'organizationname'        => $value->organizationname,
                'descriptionofassignment' => $value->descriptionofassignment,
                'fromdate'                => $value->fromdate,
                'todate'                  => $value->todate,
                'palceofposting'          => $value->palceofposting,
                'designation'             => $value->designation,
                'lastsalarydrawn'         => $value->lastsalarydrawn,
                'createdon'               => $value->createdon 
              );
              $this->db->insert('tbl_work_experience', $insertWorkExperience1);
            }
          }
          $qry7 = "select * from tbl_other_information where candidateid=".$res->candidateid;
          $res7 = $this->db->query($qry7)->result();

          if(!empty($res7)){
            foreach ($res7 as $value) {
              $insertotherinformation = array (
                'candidateid'                                                  => $newcandidateid,
                'any_subject_of_interest'                                      => $value->any_subject_of_interest,
                'any_achievementa_awards'                                      => $value->any_achievementa_awards,
                'any_assignment_of_special_interest'                           => $value->any_assignment_of_special_interest,
                'experience_of_group_social_activities'                        => $value->experience_of_group_social_activities,
                'have_you_taken_part_in_pradan_selection_process_before'       => $value->have_you_taken_part_in_pradan_selection_process_before,
                'have_you_taken_part_in_pradan_selection_process_before_when'  => $value->have_you_taken_part_in_pradan_selection_process_before_when,
                'have_you_taken_part_in_pradan_selection_process_before_where' => $value->have_you_taken_part_in_pradan_selection_process_before_where,
                'annual_income'                                                => $value->annual_income,
                'male_sibling'                                                 => $value->male_sibling,
                'female_sibling'                                               => $value->female_sibling,
                'know_about_pradan'                                            => $value->know_about_pradan,
                'know_about_pradan_other_specify'                              => $value->know_about_pradan_other_specify,
                'createdon'                                                    => $value->createdon,
                'isdeleted'                                                    => 0,
              );
              $this->db->insert('tbl_other_information', $insertotherinformation);
            }
          }
          $qry8 = "select * from tbl_candidate_writtenscore where candidateid=".$res->candidateid;
          $res8 = $this->db->query($qry8)->result();
          /*echo "<pre>";
          print_r($res8);exit();*/
          if(!empty($res8)){
            foreach ($res8 as $value) {
              $writtenscoreArr = array(
                'writtenscore'            => $value->writtenscore,
                'candidateid'             => $newcandidateid,
                'cutoffmarks'             => $value->cutoffmarks,
                'flag'                    => $value->flag,
                'createdon'               => $value->createdon,
                'createdby'               => $value->createdby  // login user id
              );
              $this->db->insert('tbl_candidate_writtenscore', $writtenscoreArr);
            }
          }
          $qry9 = "select * from tbl_candidate_gdscore where candidateid=".$res->candidateid;
          $res9 = $this->db->query($qry9)->result();

          if(!empty($res9)){
            foreach ($res9 as $value) {
              $gdscoreArr = array(
                'candidateid'             => $newcandidateid,
                'gdscore'                 => $value->gdscore,
                'flag'                    => $value->flag,
                'createdon'               => $value->createdon,
                'createdby'               => $value->createdby,
                'rsscore'                 => $value->rsscore,
                'ssscore'                 => $value->ssscore,
                'cutoffmarks'             => $value->cutoffmarks,
                'gdpercentage'            => $value->gdpercentage
              );
              $this->db->insert('tbl_candidate_gdscore', $gdscoreArr);
            }
          }
          $qry10 = "select * from tbl_family_members where candidateid=".$res->candidateid;
          $res10 = $this->db->query($qry10)->result();

          if(!empty($res10)){
            foreach ($res10 as $value) {
              $insertFamilyMember = array(
                'candidateid'             => $newcandidateid,
                'Familymembername'                 => $value->Familymembername,
                'relationwithemployee'                    => $value->relationwithemployee,
                'familydob'               => $value->familydob,
                'originalphotoname'               => $value->originalphotoname,
                'createdby'                 => $value->createdby,
                'createdon'                 => $value->createdon,
                'updateby'             => $value->updateby,
                'isdeleted'            => $value->isdeleted,
                'encryptedphotoname'            => $value->encryptedphotoname,
                'remarks'            => $value->remarks,
                'sex'            => $value->sex,
                'weatherliving'            => $value->weatherliving,
                'originalfamilyidentityphotoname'            => $value->originalfamilyidentityphotoname,
                'encryptedfamilyidentityphotoname'            => $value->encryptedfamilyidentityphotoname
              );
              $this->db->insert('tbl_family_members', $insertFamilyMember);
            }
          }
          $qry11 = "select * from tbl_identity_details where candidateid=".$res->candidateid;
          $res11 = $this->db->query($qry11)->result();

          if(!empty($res11)){
            foreach ($res11 as $value) {
              $insertIdentityDetails = array(
                'candidateid'             => $newcandidateid,
                'identityname'                 => $value->identityname,
                'identitynumber'                    => $value->identitynumber,
                'iname'               => $value->iname,
                'ifather'               => $value->ifather,
                'idateofbirth'                 => $value->idateofbirth,
                'encryptedphotoname'                 => $value->encryptedphotoname,
                'createdon'             => $value->createdon,
                'createdby'            => $value->createdby,
                'originalphotoname'            => $value->originalphotoname,
                'updatedby'            => $value->updatedby,
                'Isdeleted'            => $value->Isdeleted
              );
              $this->db->insert('tbl_identity_details', $insertIdentityDetails);
            }
          }
          $qry12 = "select * from tbl_candidate_hrscore where candidateid=".$res->candidateid;
          $res12 = $this->db->query($qry12)->result();

          if(!empty($res12)){
            foreach ($res12 as $value) {
              $insertHrscoreDetails = array(
                'candidateid'             => $newcandidateid,
                'hrscore'       => $value->hrscore,
                'flag'        => $value->flag,
                'createdon'       => $value->createdon,
                'createdby'       => $value->createdby // login user id
              );
              $this->db->insert('tbl_candidate_hrscore', $insertHrscoreDetails);
            }
          }
          $qry13 = "select * from tbl_hrd_verification_document where candidateid=".$res->candidateid;
          $res13 = $this->db->query($qry13)->result();

          if(!empty($res13)){
            foreach ($res13 as $value) {
              $HrdVerificationArr = array(
                'candidateid'             => $newcandidateid,
                'metric_certificate'       => $value->metric_certificate,
                'hsc_certificate'        => $value->hsc_certificate,
                'ug_certificate'       => $value->ug_certificate,
                'pg_certificate'       => $value->pg_certificate,
                'signed_offerletter'       => $value->signed_offerletter,
                'ug_migration_certificate'       => $value->ug_migration_certificate,
                'pg_migration_certificate'       => $value->pg_migration_certificate,
                'gapyear_certificate'       => $value->gapyear_certificate,
                'other_certificate'       => $value->other_certificate,
                'comment'       => $value->comment,
                'status'       => $value->status,
                'createdon'       => $value->createdon,
                'createdby'       => $value->createdby,
                'approvedby'       => $value->approvedby,
                'approvedon'       => $value->approvedon // login user id
              );
              $this->db->insert('tbl_hrd_verification_document', $HrdVerificationArr);
            }
          }
          $qry14 = "select * from tbl_hrd_gap_year_verified where candidateid=".$res->candidateid;
          $res14 = $this->db->query($qry14)->result();

          if(!empty($res14)){
            foreach ($res14 as $value) {
              $HrdgapArr = array(
                'candidateid'             => $newcandidateid,
                'gapyear_verified'       => $value->gapyear_verified
              );
              $this->db->insert('tbl_hrd_gap_year_verified', $HrdgapArr);
            }
          }
          $qry15 = "select * from tbl_hrd_work_experience_verified where candidateid=".$res->candidateid;
          $res15 = $this->db->query($qry15)->result();

          if(!empty($res15)){
            foreach ($res15 as $value) {
              $HrdworkArr = array(
                'candidateid'             => $newcandidateid,
                'experience_verified'       => $value->experience_verified
              );
              $this->db->insert('tbl_hrd_work_experience_verified', $HrdworkArr);
            }
          }

          $qry16 = "select * from tbl_da_summary where candidateid=".$res->candidateid;
          $res16 = $this->db->query($qry16)->result();

          if(!empty($res16)){
            foreach ($res16 as $value) {
              $DaSummaryArr = array(
                'transid'             => $value->transid,
                'candidateid'             => $newcandidateid,
                'staffid'       => $value->staffid,
                'leave_entitled'       => $value->leave_entitled,
                'leave_taken'       => $value->leave_taken,
                'date_apprectice_completion'       => $value->date_apprectice_completion,
                'date_executive_starting'       => $value->date_executive_starting,
                'flag'       => $value->flag,
                'createdby'       => $value->createdby,
                'createdon'       => $value->createdon,
                'updatedon'       => $value->updatedon,
                'updatedby'       => $value->updatedby
              );
              $this->db->insert('tbl_da_summary', $DaSummaryArr);
            }
          }

          $qry17 = "select * from tbl_verification_document where candidateid=".$res->candidateid;
          $res17 = $this->db->query($qry17)->result();

          if(!empty($res17)){
            foreach ($res17 as $value) {
              $VerDocArr = array(
                'candidateid'             => $newcandidateid,
                'metric_certificate'             => $value->metric_certificate,
                'hsc_certificate'       => $value->hsc_certificate,
                'ug_certificate'       => $value->ug_certificate,
                'pg_certificate'       => $value->pg_certificate,
                'other_certificate'       => $value->other_certificate,
                'general_nomination_form'       => $value->general_nomination_form,
                'joining_report_da'       => $value->joining_report_da,
                'comment'       => $value->comment,
                'status'       => 0,
                'createdon'       => $value->createdon,
                'createdby'       => $value->createdby,
                'approvedby'       => $value->approvedby,
                'approvedon'       => $value->approvedon,
                'signed_offerletter'       => $value->signed_offerletter,
                'ugmigration_certificate'       => $value->ugmigration_certificate,
                'pgmigration_certificate'       => $value->pgmigration_certificate
              );
              $this->db->insert('tbl_verification_document', $VerDocArr);
            }
          }
          $StaffUpdateArrary = [

            'recommend_to_graduate' => 1,
            'candidateid' => $newcandidateid

          ];

          $this->db->where('staffid', $staffid);
          $this->db->update('staff', $StaffUpdateArrary);
          $updateArrary = [

            'isdeleted' => 1

          ];

          $this->db->where('candidateid', $res->candidateid);
          $flag_trans=$this->db->update('tbl_candidate_registration', $updateArrary);
          
          /*$this->db->where('staffid', $staffid);
          $flag_trans=$this->db->update('staff', $updateArrary);*/

          $this->db->trans_complete();
          /* echo "<pre>";
          print_r($res2);exit();*/
          if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('er_msg', 'Error !!! Clearance certificate form not submitted successfully'); 
          }
          else
          {
            $token = 0;
            $res = $this->db->query("select id from staff_transaction where staffid='". $staffid ."' and trans_status='Recommended to graduate' order by id desc limit 0,1")->result();
            if(!empty($res))
            $token = $res[0]->id;
            $comments = 'DA promoted as Graduate by HR';
            $updateArr = array(
            'trans_flag'     => 7,
            'updatedon'      => date("Y-m-d H:i:s"),
            'updatedby'      => $this->loginData->staffid
            );

            $this->db->where('id', $token);
            $this->db->update('staff_transaction', $updateArr);

            $recieverRes = $this->db->query("select UserId from role_permissions where Controller='Recommended_to_graduate' and parent_menu_id=886 and RoleID=17")->result();
            if(!empty($recieverRes))
            {
            $recieverID = $recieverRes[0]->UserId;
            }

            $insertworkflowArr = array(
            'r_id'           => $token,
            'type'           => '29',
            'staffid'        => $staffid,
            'sender'         => $this->loginData->staffid,
            'receiver'       => $recieverID,
            'senddate'       => date("Y-m-d H:i:s"),
            'flag'           => 7,
            'scomments'      => $comments,
            'createdon'      => date("Y-m-d H:i:s"),
            'createdby'      => $this->loginData->staffid,
            'forwarded_workflowid' => $this->getForwardedWorkFlowID($token)
            );
            $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          }

          $hrname= '';
          $tcemail= ''; 
          $pa = '';
          $pa = $this->gmodel->getPersonnelAdministratorEmailid();
          $recipients = array();
          $hrname =  $this->loginData->UserFirstName.' '.$this->loginData->UserLastName;
          $tc = '';
          $tc = $this->model->gettcemailid($staffid);

          $da = $this->db->query("select emailid,name from staff where staffid='". $staffid ."'")->row();
          $hr = array('$staffno','$paname','$hrname','$name');
          $hr_replace = array($staffno,$pa->name,$hrname,$da->name);
          // $tcemail = $tc->emailid; 
          // $recipients = array($tcemail => $tcemail);

          $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 62 AND `isactive` = '1'";
          $data = $this->db->query($sql)->row();
          if(!empty($data))
          $body = str_replace($hr,$hr_replace, $data->lettercontent);

          $sendmail = $this->Common_Model->send_email($subject = "Graduation Formalities",$body,$pa->personnelemailid);  //// Send Mail TC ////
        }


        
        $this->session->set_flashdata('tr_msg', 'DA graduated successfully !!!');
        redirect('/Proposed_probation_separation/index'); 
      }
      else
      {
        $this->session->set_flashdata('er_msg','Please select DA to recommend!!');
        redirect('/Proposed_probation_separation/index');     
      }     
    }
    
    

  }
  catch(Exception $e)
  {
    print_r($e->getMessage());
    die();
  }
}





public function Experience_certificate($token,$staffid)
{
  try{

  $content['staffdetail'] = $this->model->get_staffDetails($staffid);
 /* $content['getprobationdetails'] = $this->model->get_Probation_Separation();
  $content['getprobationdetailsapproval'] = $this->model->get_Probation_Separation_approval();
  $content['getgraduatedetails'] = $this->model->get_Recommended_to_graduate();
  $content['gettransferdetails'] = $this->model->get_Recommended_to_transfer();
  $content['getextentiondetails'] = $this->model->get_Recommended_to_Extention();*/
  $content['hrdetail'] = $this->model->get_staffDetails($this->loginData->staffid);
  $content['eddetail'] = $this->gmodel->getExecutiveDirectorEmailid();
  /*echo "<pre>";
  print_r($content['staffdetail']);exit();*/

  $RequestMethod = $this->input->server('REQUEST_METHOD');   
   if($RequestMethod == 'POST'){
    $staff = '';
    $staff = $content['staffdetail'];

    $tdate = '';
    $doj = '';
    $doj = $staff->doj_team;
    $releavingdate = '';
    $edname = '';
    $getedname = '';

    $offerdetails = $this->db->query("select * from tbl_generate_offer_letter_details where candidateid='". $staff->candidateid ."'")->row();
    $dojteam = $this->gmodel->changedatedbformate($doj); 
    $getedname = $this->gmodel->getExecutiveDirectorEmailid();
    $tdate = date("Y-m-d");    
    $staff = $content['staffdetail'];


    //Facilitate to leave
    //da Experience certificate as (In case of termination)
    if($staff->trans_status == 0 && $staff->trans_status == 'Facilitate to leave')
    {
      $dadetail = array('$daname','$fileno','$emp_code','$doj','$releavingdate','$edname');
      $dadetail_replace = array($staff->name,$offerdetails->fileno,$staff->emp_code,$dojteam,$staff->releavingdate,$getedname->name);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 71 AND `isactive` = '1'";
      $data = $this->db->query($sql)->row();
      if(!empty($data))
      $body = str_replace($dadetail,$dadetail_replace, $data->lettercontent);


      $filename = md5(time() . rand(1,1000));
      $this->load->model('Dompdf_model');
      $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExperienceCertificate.pdf');

      $attachments = array($filename.'.pdf');
      $sendmail = $this->Common_Model->send_email($subject = "Experience Certificate",$body,$staff->emailid,$email = null, $recipients=null, $attachments);
    }

    //Assistant Experience certificate as (In case of termination)
    if($staff->trans_status == 1 || $staff->trans_status == 2 || $staff->trans_status == 3 && $staff->trans_status == 'Facilitate to leave')
    {
      $dadetail = array('$daname','$fileno','$emp_code','$doj','$releavingdate','$edname');
      $dadetail_replace = array($staff->name,$offerdetails->fileno,$staff->emp_code,$dojteam,$staff->releavingdate,$getedname->name);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 72 AND `isactive` = '1'";
      $data = $this->db->query($sql)->row();
      if(!empty($data))
      $body = str_replace($dadetail,$dadetail_replace, $data->lettercontent);


      $filename = md5(time() . rand(1,1000));
      $this->load->model('Dompdf_model');
      $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExperienceCertificate.pdf');

      $attachments = array($filename.'.pdf');
      $sendmail = $this->Common_Model->send_email($subject = "Experience Certificate",$body,$staff->emailid,$email = null, $recipients=null, $attachments);
    }

    //Executive Experience certificate as (In case of termination)
    if($staff->trans_status == 4 && $staff->trans_status == 'Facilitate to leave')
    {
      $dadetail = array('$daname','$fileno','$emp_code','$doj','$releavingdate','$edname');
      $dadetail_replace = array($staff->name,$offerdetails->fileno,$staff->emp_code,$dojteam,$staff->releavingdate,$getedname->name);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 74 AND `isactive` = '1'";
      $data = $this->db->query($sql)->row();
      if(!empty($data))
      $body = str_replace($dadetail,$dadetail_replace, $data->lettercontent);


      $filename = md5(time() . rand(1,1000));
      $this->load->model('Dompdf_model');
      $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExperienceCertificate.pdf');

      $attachments = array($filename.'.pdf');
      $sendmail = $this->Common_Model->send_email($subject = "Experience Certificate",$body,$staff->emailid,$email = null, $recipients=null, $attachments);
    }



    // Resignation
    
    //da Experience certificate as (In case of Resign)
    if($staff->trans_status == 0 && $staff->trans_status == 'Resign')
    {
      $dadetail = array('$daname','$fileno','$emp_code','$doj','$releavingdate','$edname');
      $dadetail_replace = array($staff->name,$offerdetails->fileno,$staff->emp_code,$dojteam,$staff->releavingdate,$getedname->name);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 79 AND `isactive` = '1'";
      $data = $this->db->query($sql)->row();
      if(!empty($data))
      $body = str_replace($dadetail,$dadetail_replace, $data->lettercontent);


      $filename = md5(time() . rand(1,1000));
      $this->load->model('Dompdf_model');
      $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExperienceCertificate.pdf');

      $attachments = array($filename.'.pdf');
      $sendmail = $this->Common_Model->send_email($subject = "Experience Certificate",$body,$staff->emailid,$email = null, $recipients=null, $attachments);
    }

    //Assistant Experience certificate as (In case of Resign)
    if($staff->trans_status == 1 || $staff->trans_status == 2 || $staff->trans_status == 3 && $staff->trans_status == 'Resign')
    {
      $dadetail = array('$daname','$fileno','$emp_code','$doj','$releavingdate','$edname');
      $dadetail_replace = array($staff->name,$offerdetails->fileno,$staff->emp_code,$dojteam,$staff->releavingdate,$getedname->name);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 76 AND `isactive` = '1'";
      $data = $this->db->query($sql)->row();
      if(!empty($data))
      $body = str_replace($dadetail,$dadetail_replace, $data->lettercontent);


      $filename = md5(time() . rand(1,1000));
      $this->load->model('Dompdf_model');
      $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExperienceCertificate.pdf');

      $attachments = array($filename.'.pdf');
      $sendmail = $this->Common_Model->send_email($subject = "Experience Certificate",$body,$staff->emailid,$email = null, $recipients=null, $attachments);
    }

    //Executive Experience certificate as (In case of Resign)
    if($staff->trans_status == 4 && $staff->trans_status == 'Resign')
    {
      $dadetail = array('$daname','$fileno','$emp_code','$doj','$releavingdate','$edname');
      $dadetail_replace = array($staff->name,$offerdetails->fileno,$staff->emp_code,$dojteam,$staff->releavingdate,$getedname->name);

      $sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 77 AND `isactive` = '1'";
      $data = $this->db->query($sql)->row();
      if(!empty($data))
      $body = str_replace($dadetail,$dadetail_replace, $data->lettercontent);


      $filename = md5(time() . rand(1,1000));
      $this->load->model('Dompdf_model');
      $generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'ExperienceCertificate.pdf');

      $attachments = array($filename.'.pdf');
      $sendmail = $this->Common_Model->send_email($subject = "Experience Certificate",$body,$staff->emailid,$email = null, $recipients=null, $attachments);
    }
 
   }

  $content['title'] = 'Experience_certificate';
  $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;     
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }

}


  /**
   * Method intimate() Initiate Intemation.
   * @access  public
   * @param Null
   * @return  Array
   */

/*public function intimate($intemationid, $seperationdate)
  {    
    try{
      if($intemationid !='' && $seperationdate !=''){
        $seperationdate = $this->gmodel->changedatedbformate($seperationdate);
        // echo $intemationid.' SepId '.$seperationdate;exit();
        $query = "select a.*, c.intimation, b.comment  FROM staff as a LEFT JOIN tbl_hr_intemation as b ON a.staffid = b.staffid
        LEFT JOIN mst_intimation as c ON c.id = b.intemation_type  
        WHERE b.id=".$intemationid;
        $resultdata = $this->db->query($query)->row();
        
        $tablename = "'staff_transaction'";
        $incval = "'@a'";
        $personnel_detail = $this->model->getpersonneldetail();

        $Staffdetails =  $this->model->getstaffDetails($resultdata->staffid);
        $designation  =  $Staffdetails->designation;
        $supervisor   =  $Staffdetails->reportingto;
        $receiverdetail = $this->model->get_staffDetails($resultdata->staffid);


        $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

        $autoincval =  $getstaffincrementalid->maxincval;

        // echo $autoincval; die;

        $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
        $result  = $this->db->query($sql)->result()[0];
        $this->db->trans_start();
        $insertArr = array(
          'id'                => $autoincval,
          'staffid'           => $resultdata->staffid,
          'old_office_id'     => $resultdata->new_office_id,
          'old_designation'   => $designation,
          'new_designation'   => $designation,
          'trans_status'      => $resultdata->intimation,
          'date_of_transfer'  => date("Y-m-d"),
          'effective_date'    => $seperationdate,
          'reason'            => $resultdata->comment,
          'trans_flag'        => 1,
          'reportingto'       => $supervisor,
          'createdon'         => date("Y-m-d H:i:s"), 
          'datetime'          => date("Y-m-d H:i:s"),
          'createdby'         => $this->loginData->staffid
        );

        $this->db->insert('staff_transaction', $insertArr);
        if ($result->workflowid !='') {
              $insertworkflowArr = array(
               'r_id'                 => $autoincval,
               'type'                 => 27,
               'staffid'              => $resultdata->staffid,
               'sender'               => $this->loginData->staffid,
               'receiver'             => $resultdata->staffid,
               'forwarded_workflowid' => $result->workflowid,
               'senddate'             => date("Y-m-d H:i:s"),
               'flag'                 => 1,
               'scomments'            => $this->input->post('discussion'),
               'createdon'            => date("Y-m-d H:i:s"),
               'createdby'            => $this->loginData->staffid,
              );

          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          }else{

             $insertworkflowArr = array(
               'r_id'           => $autoincval,
               'type'           => 27,
               'staffid'        => $resultdata->staffid,
               'sender'         => $this->loginData->staffid,
               'receiver'       => $resultdata->staffid,
               'senddate'       => date("Y-m-d H:i:s"),
               'flag'           => 1,
               'scomments'      => $this->input->post('discussion'),
               'createdon'      => date("Y-m-d H:i:s"),
               'createdby'      => $this->loginData->staffid,
           );

          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          }

          $hrintemationdataset = array(
            'status'=>2
          );
          $this->db->where('id', $intemationid);
          $this->db->update('tbl_hr_intemation', $hrintemationdataset);
          $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE){

            $this->session->set_flashdata('er_msg', 'Error !!! Resign request has been failed !!! '); 
            redirect(current_url());

          }else{
            $subject = ":Resign Mail";
            $body = "<h4>Hi ".$resultdata->name.", </h4><br />";
            $body .= "We have initiated Resign.<br />";
            $body .= "<b>Comment : " .  $resultdata->comment . "</b><br /><br /><br />";
            $body .= "<b> Thanks </b><br>";
            $body .= "<b>Pradan Technical Team </b><br>";

            $to_email = $receiverdetail->emailid;
            $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
            $recipients = array(
             $personnel_detail->EmailID => $personnel_detail->UserFirstName,
             $resultdata->emailid => $resultdata->name
             // ..
            );
            $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name, $recipients);
            if (substr($email_result, 0, 5) == "ERROR") {
            $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
            }
            $this->session->set_flashdata('tr_msg', 'Transfer request has been initiated successfully !!!');
            redirect('/Proposed_probation_separation/index');
          }
        }else{
          redirect('/Proposed_probation_separation/index');
        }      
    }catch (Exception $e) {
       print_r($e->getMessage());die;
     }
   }*/

  /**
   * Method leaveintimate() Initiate Facilitate Leave Intemation.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function intimate($intemationid)
  {    
    try{

      // die();
      if($intemationid !=''){
        // $seperationdate = $this->gmodel->changedatedbformate($seperationdate);
        // echo $intemationid.' SepId '.$seperationdate;exit();
        $query = "select a.*, c.intimation, b.comment FROM staff as a LEFT JOIN tbl_hr_intemation as b ON a.staffid = b.staffid
        LEFT JOIN mst_intimation as c ON c.id = b.intemation_type  
        WHERE b.id=".$intemationid;
        $resultdata = $this->db->query($query)->row();
       /* echo "<pre>";
        print_r($resultdata);exit();*/
        $tablename = "'staff_transaction'";
        $incval = "'@a'";
        $personnel_detail = $this->model->getpersonneldetail();

        // $Staffdetails =  $this->model->getstaffDetails($resultdata->staffid);


        /*echo "<pre>";
        print_r($Staffdetails);exit();*/
        $designation  =  $resultdata->designation;
        $supervisor   =  $resultdata->reportingto;

        $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

        $autoincval =  $getstaffincrementalid->maxincval;

        // echo $autoincval; die;

        $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
        $result  = $this->db->query($sql)->result()[0];
        $this->db->trans_start();
        $insertArr = array(
          'id'                => $autoincval,
          'staffid'           => $resultdata->staffid,
          'old_office_id'     => $resultdata->new_office_id,
          'new_office_id'     => $resultdata->new_office_id,
          'old_designation'   => $designation,
          'new_designation'   => $designation,
          'trans_status'      => $resultdata->intimation,
          'date_of_transfer'  => date("Y-m-d"),
          /*'effective_date'    => $seperationdate,*/
          'reason'            => $resultdata->comment,
          'trans_flag'        => 1,
          'reportingto'       => $supervisor,
          'createdon'         => date("Y-m-d H:i:s"), 
          'datetime'          => date("Y-m-d H:i:s"),
          'createdby'         => $this->loginData->staffid
        );

        $this->db->insert('staff_transaction', $insertArr);
        if ($result->workflowid !='') {
          $insertworkflowArr = array(
           'r_id'                 => $autoincval,
           'type'                 => 27,
           'staffid'              => $resultdata->staffid,
           'sender'               => $this->loginData->staffid,
           'receiver'             => $resultdata->staffid,
           'forwarded_workflowid' => $result->workflowid,
           'senddate'             => date("Y-m-d H:i:s"),
           'flag'                 => 1,
           'scomments'            => $this->input->post('discussion'),
           'createdon'            => date("Y-m-d H:i:s"),
           'createdby'            => $this->loginData->staffid,
         );

          $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

        }else{

         $insertworkflowArr = array(
           'r_id'           => $autoincval,
           'type'           => 27,
           'staffid'        => $resultdata->staffid,
           'sender'         => $this->loginData->staffid,
           'receiver'       => $resultdata->staffid,
           'senddate'       => date("Y-m-d H:i:s"),
           'flag'           => 1,
           'scomments'      => $this->input->post('discussion'),
           'createdon'      => date("Y-m-d H:i:s"),
           'createdby'      => $this->loginData->staffid,
         );

         $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

       }

       $hrintemationdataset = array(
        'status'=>2
        );
       $this->db->where('id', $intemationid);
       $this->db->update('tbl_hr_intemation', $hrintemationdataset);
       $this->db->trans_complete();
       if ($this->db->trans_status() === FALSE){

        $this->session->set_flashdata('er_msg', 'Error !!! Facilitate to leave request has been failed !!! '); 
        redirect(current_url());

      }else{
        $subject = ": Exit Formalities";
        $body = "<h4>Dear ".$resultdata->name.", </h4><br />";
        $body .= "Kindly fill the Exit Interview form.<br />";
        $body .= "</b><br /><br /><br />";
        $body .= "<b> Regards, </b><br>";
        $body .= "<b>".$this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName."</b><br>";

        $receiverdetail = $this->model->get_staffDetails($resultdata->staffid);
        $to_email = $receiverdetail->emailid;
        $to_name = $receiverdetail->name;
            //$to_cc = $getStaffReportingtodetails->emailid;
        $recipients = array(
         $personnel_detail->EmailID => $personnel_detail->UserFirstName,
         $resultdata->emailid => $resultdata->name
             // ..
       );

        $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td width="10%">&nbsp;</td>
        <td colspan="3" valign="top" align="center">Professional Assistance for Development Action (PRADAN) </td>
        <td width="10%">&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td width="25%" valign="top">To : '.$resultdata->name.' <br />
        (Name of Separating Apprentice)</td>
        <td width="26%">&nbsp;</td>
        <td width="29%" valign="top">Date : '.date('F j, Y').'</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td valign="top">From : '.$this->loginData->UserFirstName.' '.$this->loginData->UserMiddleName.' '.$this->loginData->UserLastName.' 
        <br />
        (Team Coordinator)</td>
        <td>&nbsp;</td>
        <td>File : '.$autoincval.' </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">Subject : The Exit Interview Form </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">As you plan to leave the apprenticeship programme, I write to request you to fill in the enclosed &quot;Exit Interview Form&quot;. We hope to seek feedback about the apprenticeship programme, and any other issue you choose. </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">I would like to reassure you that the contents of this form would be confidential, if you so desire, and that we would use it only for organizational learning. </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">We do hope that you make a smooth transition to your new assignment, and sincerely wish you the very best in your life. </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td width="10%">&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">With regards, </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td width="10%">&nbsp;</td>
        <td colspan="3">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        </tr>
        </table>';
        $filename = md5(time() . rand(1,1000)).'_iom';
        $this->load->model('Dompdf_model');
        $generate='';
        $generate =   $this->Dompdf_model->transfer_letter_PDF($html,$filename, NULL,'filename.pdf');        
        $filename = $filename; 
        $attachments = array($filename.'.pdf');
        // print_r($attachments);exit();

        $email_result = $this->Common_Model->send_email_separation($subject, $body, $to_email, $to_name, $recipients, $attachments);
        if (substr($email_result, 0, 5) == "ERROR") {
          $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
        }

        $this->session->set_flashdata('tr_msg', $resultdata->intimation.' request has been initiated successfully !!!');
        redirect('/Proposed_probation_separation/index');
      }
    }else{
      redirect('/Proposed_probation_separation/index');
    }      
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}



  /**
   * Method intimate_Recommended_to_graduate() Initiate Intemation.
   * @access  public
   * @param Null
   * @return  Array
   */

  public function intimate_Recommended_to_graduate($intemationid, $newteamid = NULL)
  {

    try{
      $query = "select a.*, c.intimation, b.comment  FROM staff as a LEFT JOIN tbl_hr_intemation as b ON a.staffid = b.staffid
      LEFT JOIN mst_intimation as c ON c.id = b.intemation_type  
      WHERE b.id=".$intemationid;
      $resultdata = $this->db->query($query)->row();
      /*echo "<pre>";
      print_r($resultdata);exit();*/
     
      $tablename = "'staff_transaction'";
      $incval = "'@a'";
      $personnel_detail = $this->model->getpersonneldetail();

      $Staffdetails =  $this->model->getstaffDetails($resultdata->staffid);
      $designation  =  $Staffdetails->designation;
      $supervisor   =  $Staffdetails->reportingto;
      $receiverdetail = $this->model->get_staffDetails($resultdata->staffid);


      $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 

      $autoincval =  $getstaffincrementalid->maxincval;

      // echo $autoincval; die;

      $sql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$autoincval";
      $result  = $this->db->query($sql)->result()[0];
      $this->db->trans_start();
      $insertArr = array(
        'id'                => $autoincval,
        'staffid'           => $resultdata->staffid,
        'old_office_id'     => $resultdata->new_office_id,
        'new_office_id'     => $newteamid==null?$resultdata->new_office_id:$newteamid,
        'old_designation'   => $designation,
        'new_designation'   => $designation,
        'trans_status'      => $resultdata->intimation,
        'reason'            => $resultdata->comment,
        'trans_flag'        => 1,
        'reportingto'       => $supervisor,
        'fgid'              => $Staffdetails->fgid,
        'date_of_transfer'  => date("Y-m-d"),
        'createdon'         => date("Y-m-d H:i:s"),
        'datetime'          => date("Y-m-d H:i:s"),
        'createdby'         => $this->loginData->staffid
      );

      $this->db->insert('staff_transaction', $insertArr);
      if ($result->workflowid !='') {
        $insertworkflowArr = array(
         'r_id'                 => $autoincval,
         'type'                 => 29,
         'staffid'              => $resultdata->staffid,
         'sender'               => $this->loginData->staffid,
         'receiver'             => $resultdata->staffid,
         'forwarded_workflowid' => $result->workflowid,
         'senddate'             => date("Y-m-d H:i:s"),
         'flag'                 => 1,
         'scomments'            => $this->input->post('discussion'),
         'createdon'            => date("Y-m-d H:i:s"),
         'createdby'            => $this->loginData->staffid,
         
       );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

      }else{

       $insertworkflowArr = array(
         'r_id'           => $autoincval,
         'type'           => 29,
         'staffid'        => $resultdata->staffid,
         'sender'         => $this->loginData->staffid,
         'receiver'       => $resultdata->staffid,
         'senddate'       => date("Y-m-d H:i:s"),
         'flag'           => 1,
         'scomments'      => $this->input->post('discussion'),
         'createdon'      => date("Y-m-d H:i:s"),
         'createdby'      => $this->loginData->staffid,
         'forwarded_workflowid' => $result->workflowid
       );

       $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

     }

     $hrintemationdataset = array(
      'status'=>2
    );
     $this->db->where('id', $intemationid);
     $this->db->update('tbl_hr_intemation', $hrintemationdataset);
     $this->db->trans_complete();
     if ($this->db->trans_status() === FALSE){

      $this->session->set_flashdata('er_msg', 'Error !!! Graduate Recommendation request has been failed !!! '); 
      redirect(current_url());

    }else{
      // $hr_name = '';
      // $hr_name = $this->loginData->UserFirstName." ".$this->loginData->UserLastName;
      // $subject = ":Graduation Formalities";
      // $body = "<h4>Hi ".$resultdata->name.", </h4><br />";
      // $body .= "As you are about to complete your Apprenticeship period, kindly fill the Exit form and Self Declaration Form.<br />";
      // $body .= "<b>Comment : " .  $resultdata->comment . "</b><br /><br /><br />";
      // $body .= "<b> Thanks </b><br>";
      // $body .= "<b>".$this->loginData->UserFirstName." ".$this->loginData->UserLastName." </b><br>";

      // $to_email = $receiverdetail->emailid;
      // $to_name = $receiverdetail->name;

      // $to_email = $resultdata->emailid;
              //$to_cc = $getStaffReportingtodetails->emailid;
      // $recipients = array(
      // $personnel_detail->EmailID => $personnel_detail->UserFirstName,
      // $resultdata->emailid => $resultdata->name
      // );

      // echo $body; die();

      // $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name= null, $recipients= null);
      if (substr($email_result, 0, 5) == "ERROR") {
        $this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
      }

      $this->session->set_flashdata('tr_msg', 'Graduate Recommendation has been initiated successfully !!!');
      redirect('/Proposed_probation_separation/index');

    }      
  }catch (Exception $e) {
   print_r($e->getMessage());die;
 }
}

public function add($token){
  try{

     $to_finance = '';
    $newguide   = '';
    $content['gettransferdetails'] = $this->model->get_Transferdetails($token);
    // echo "<pre>";
    // print_r($content['gettransferdetails']); die;
    $finance = $this->gmodel->getFinanceAdministratorEmailid();

    $staffde = $this->Common_Model->get_Staff_detail($this->loginData->staffid);

   $RequestMethod = $this->input->server('REQUEST_METHOD');   
   if($RequestMethod == 'POST'){

   // print_r($this->input->post()); die;

     $da = $this->input->post('da');


     $this->form_validation->set_rules('type','type','trim|required');
     $this->form_validation->set_rules('dc','dc','trim|required');
     $this->form_validation->set_rules('team','team','trim|required');
     $this->form_validation->set_rules('da','da','trim|required');
     $this->form_validation->set_rules('probation_completed','probation_completed','trim|required');
     $this->form_validation->set_rules('DateofSeraption','DateofSeraption','trim|required');
     $this->form_validation->set_rules('probation_extension_date','probation_extension_date ','trim|required');              
     $this->form_validation->set_rules('Reason','Reason','trim|required');
     $this->form_validation->set_rules('comment','comment','trim|required');        


     $dashiptransactions = $this->input->post('dashiptransactions');  

     $dateext = $this->input->post('extension_date');
     $extension_date = $this->model->changedatedbformate($dateext);
     $DateofSeraption = $this->input->post('DateofSeraption');
     $dateofseraption = $this->model->changedatedbformate($DateofSeraption);
     $staffDAid = $this->input->post('da');



     if ($dashiptransactions == 6) {

       $query = "select a.*, c.intimation, b.comment  FROM staff as a LEFT JOIN tbl_hr_intemation as b ON a.staffid = b.staffid
       LEFT JOIN mst_intimation as c ON c.id = b.intemation_type  
       WHERE b.id=".$intemationid;
       $resultdata = $this->db->query($query)->row();

       $this->db->trans_start();

       $html = '<table  width="100%" >
       <tr>
       <td></td>
       <td></td>
       <td valign="top" ></td>
       </tr>
       <tr>
       <td colspan="3">Dear Sir ,</td>

       </tr>
       <tr>
       <td colspan="3" align="justify" >It is with a heavy heart that I’m saying goodbye to you and all my other dear colleagues. My personal commitments insist that I stop being an employee of this company and therefore it is quite regretfully that I shall be resigning from my post as __________ [enter designation].</td>

       </tr>
       <tr>
       <td colspan="3" align="justify" > I want to thank you for all, your help and support during the time I worked here. You were of the main reason why I enjoyed my work so much. I wish you all the best for your future endeavors and hope all your years in the company are successful and fruitful. </td>

       </tr>

       <tr>
       <td colspan="3" align="justify">Do keep in touch. Take care..</td>
       </tr>
       <tr>
       <td colspan="3">Regards </td>
       </tr>
       <tr>
       <td colspan="3">XYZ </td>
       </tr>

       </table>';
       $filename = 'probation_'.md5(time());
       $this->load->model('Dompdf_model');
       $generate =   $this->Dompdf_model->probation_letter_PDF($html, $filename);

       if ( $generate==1) {
    //  print_r($this->input->post()); die;


        $probationda = $this->input->post('probationstaff'); 
        $probation_completed_date = $this->input->post('probation_completed_date');
        $probation_completed_date = $probation_completed_date == ''? NULL : $this->model->changedatedbformate($probation_completed_date);
        $extension_date = $this->input->post('extension_date');
        $extension_date1 = $extension_date == ''? NULL : $this->model->changedatedbformate($extension_date);

        $staffid = $this->input->post('probationstaff');
        $probationtype = $this->input->post('probation_completed');


        $insertArr2 = array(

          'type'                 => $this->input->post('dashiptransactions'),
          'dc'                   => $this->input->post('probationdc'),
          'team'                 => $this->input->post('probationteam'),
          'staffid'              => $this->input->post('probationstaff'),
          'probation_completed'  => $this->input->post('probation_completed'),
          'probation_completed_date'  => $probation_completed_date,
          'probation_extension_date' => $extension_date1,
          'reason'                => $this->input->post('Reason'),
          'comment '              => trim($this->input->post('comment')),
          'generate_letter_name'  => $filename,
          'status'                => 1,
        'createdon'             => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'             => $this->loginData->UserID, // login user id
        'isdeleted'             => 0, 
      );


        $this->db->insert('tbl_probation_separation', $insertArr2);

        $insertArr2 = array(
          'staffid'             => $staffid,
          'old_office_id'       => $this->input->post('probationteam'),
          'new_office_id'       => $this->input->post('probationteam'),
          'date_of_transfer'    => date('Y-m-d'),
          'old_designation'     => 13,
          'new_designation'     => 13,
          'id'                  => $increment_staffid_transactionid->maxincval,
          'trans_status'        => 'probation',
        'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
      );

        $this->db->insert('staff_transaction', $insertArr2);

        if ($probationtype ==2) {

          $updateArr = array(
            'probation_extension_date'  => $extension_date1,
            'probation_status'  => 0,
            'probation_completed' => 0,
            'updatedon'         => date('Y-m-d H:i:s'),
            'updatedby'          => $this->loginData->UserID,
          );
          $this->db->where('staffid', $staffid);
          $this->db->update('staff', $updateArr);

        }else{

          $updateArr = array(
            'probation_completed' => 1,
            'probation_status'  => 1,
            'updatedon'         => date('Y-m-d H:i:s'),
            'updatedby'          => $this->loginData->UserID,
          );
          $this->db->where('staffid', $staffid);
          $this->db->update('staff', $updateArr);
        }

//        echo $this->db->last_query(); die;


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
          $this->session->set_flashdata('er_msg', 'Error adding Proposed Probation'); 

        }else{

         $getstaffdetail =  $this->model->getStaffSeparationDetails($probationda);

         $staffemail = $getstaffdetail->emailid;
         $to_name = $this->loginData->hrdemailid;
         $filename = $getstaffdetail->generate_letter_name;
         $attachments = array($filename.'.pdf');
         $html = 'Dear Sir, <br><br> 
         Probation . <br>
         Regards <br>
         Pradan Team';

         $sendmail = $this->Common_Model->send_email_separation($subject = 'Probation Letter ', $message = $html, $staffemail, $to_name, $recipenent, $attachments);  //// Send Mail candidates With Offer Letter ////

         $this->session->set_flashdata('tr_msg', 'Successfully added Proposed Probation'); 

         redirect('Proposed_probation_separation/index'); 

       }
     }


   }elseif ($dashiptransactions==4) {



      $tablename = "'staff_transaction'";
      $incval = "'@a'";
      $getstaffincrementalid = $this->model->getStaffTransactioninc($tablename,$incval); 
      $autoincval =  $getstaffincrementalid->maxincval;


    $staffid = $this->input->post('staff');
    $comment = $this->input->post('commenttransfer');


    $getstaffdetail =  $this->model->getDATransferDetails($staffid);
      // print_r($getstaffdetail);

      //  die;

    $html = '<table  width="100%" >
    <tr>
    <td></td>
    <td></td>
    <td valign="top" ></td>
    </tr>
    <tr>
    <td colspan="3">Dear '. $getstaffdetail->name.' ,</td>
    
    </tr>
    <tr>
    <td colspan="3" align="justify" >It is with a heavy heart that I’m saying goodbye to you and all my other dear colleagues. My personal commitments insist that I stop being an employee of this company and therefore it is quite regretfully that I shall be resigning from my post as '.$getstaffdetail->desname.' [enter designation].</td>

    </tr>
    <tr>
    <td colspan="3" align="justify" > I want to thank you for all, your help and support during the time I worked here. You were of the main reason why I enjoyed my work so much. I wish you all the best for your future endeavors and hope all your years in the company are successful and fruitful. </td>
    
    </tr>

    <tr>
    <td colspan="3" align="justify">Do keep in touch. Take care..</td>
    </tr>
    <tr>
    <td colspan="3">Regards </td>
    </tr>
    <tr>
    <td colspan="3">XYZ </td>
    </tr>

    </table>';

//echo $html; die;
    $filename = 'transfer_'.md5(time());
    $this->load->model('Dompdf_model');
    $generate =   $this->Dompdf_model->transfer_letter_PDF($html, $filename);

    if ($generate==1) {

      $staffid = $this->input->post('staff');
      $dateoftran = $this->input->post('dateoftransfer');
      $dateoftransfer = $this->model->changedatedbformate($dateoftran);
   // $getcandidateteamfg = $this->db->getCandidateteamfg();
      $inserttranferArr = array(
        'teamid'          => $this->input->post('newteam'),
        'fg'              => $this->input->post('new_field_guide'),
        'staffid'         => $staffid,
        'doj'             => date('Y-m-d'),
        'createdon'       => date('Y-m-d H:i:s'),
        'createdby'       => $this->loginData->UserID,
      );

      $this->db->insert('tbl_transfer_history', $inserttranferArr);

      $insertArr2 = array(
        'staffid'             => $this->input->post('staff'),
        'old_office_id'       => $this->input->post('oldteam'),
        'new_office_id'       => $this->input->post('newteam'),
        'date_of_transfer'    => $dateoftransfer,
        'old_designation'     => 13,
        'new_designation'     => 13,
        'trans_status'        => 'Transfer',
        'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
      );

      $this->db->where('id',$token);
      $this->db->update('staff_transaction', $insertArr2);

      $updateArr = array(
        'doj_team'  => $dateoftransfer,
        'designation' => 13,
        'new_office_id' => $this->input->post('newteam'),
      );
      $this->db->where('staffid', $staffid);
      $this->db->update('staff', $updateArr);

      // $updateArrdapersonalinfo = array(
      //   'fgid'  => $this->input->post('new_field_guide'),
      //   'teamid' => $this->input->post('newteam'),
      // );

      //  $this->db->where('staffid', $staffid);
      //  $this->db->insert('tbl_da_personal_info', $updateArrdapersonalinfo);

      $insertArr3 = array(
        'olddc'                   => $this->input->post('olddc'),
        'oldteam'                 => $this->input->post('oldteam'),
        'staffid'                 => $this->input->post('staff'),
        'newdc'                   => $this->input->post('newdc'),
        'newteam'                 => $this->input->post('newteam'),
        'new_field_guide'         => $this->input->post('new_field_guide'),
        'dateoftransfer'          => $dateoftransfer,
        'comment'                 => $this->input->post('commenttransfer'),
        'generate_letter_name'    =>  $filename,
        'createdon'               => date('Y-m-d H:i:s'),
        'createdby'               => $this->loginData->UserID,    // Current Date and time
      );

      $this->db->insert('tbl_transfer_da', $insertArr3);

      $newguide = $this->model->newguide($this->input->post('new_field_guide'));


      $updatestatusArr = array(
        'status'            => 2,
        'updatedby'         => date('Y-m-d H:i:s'),
              'updatedon'         => $this->loginData->UserID, // login user id
            );

      $this->db->where('intemation_type', $dashiptransactions);
      $this->db->where('staffid', $staffid);
      $this->db->update('tbl_hr_intemation', $updatestatusArr);

      
      $workflowsql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$token";
      $workresult  = $this->db->query($workflowsql)->result()[0];

      if ($workresult->workflowid !='') {
        $insertworkflowArr = array(
          'r_id'                 => $getstaffincrementalid->maxincval,
          'type'                 => 26,
          'staffid'              => $staffid,
          'sender'               => $this->loginData->staffid,
          'receiver'             => $this->loginData->staffid,
          'forwarded_workflowid' => $workresult->workflowid,
          'senddate'             => date("Y-m-d H:i:s"),
          'flag'                 => 3,
          'scomments'            => $comment,
          'createdon'            => date("Y-m-d H:i:s"),
          'createdby'            => $this->loginData->staffid,
        );

        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

      }else{

        $insertworkflowArr = array(
          'r_id'                 => $token,
          'type'                 => 26,
          'staffid'              => $staffid,
          'sender'               => $this->loginData->staffid,
          'receiver'             => $this->loginData->staffid,
          'senddate'             => date("Y-m-d H:i:s"),
          'flag'                 => 3,
          'scomments'            => $comment,
          'createdon'            => date("Y-m-d H:i:s"),
          'createdby'            => $this->loginData->staffid,
        );
        $this->db->insert('tbl_workflowdetail', $insertworkflowArr);  
      }



      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
        $this->session->set_flashdata('er_msg', 'Error adding Successfully added Proposed Transfer');  
      }else{


        $getstaffdetail =  $this->model->getStaffTransferDetails($staffid);

        $staffname = $getstaffdetail->name;
        $filename = $getstaffdetail->generate_letter_name;
        $attachments = array();
        $html = "Dear ".$content['gettransferdetails']->tc_name.", <br/> <br/>

              Kindly hand over the attached Change of Placement Letter to ".$content['gettransferdetails']->name.". <br/>

              I am also sending new TCs Contact No. for his/her reference: <br/><br/>
              New TC Name.:".$newguide->newguidename." 
              New TC Contact no.:".$newguide->contact." 
              <br/>
              mail Id: ".$newguide->emailid." <br/><br/> Regards,<br/>
              ".$staffde->name."  <br/>
              ".$staffde->desname." <br/>
              HRD Unit";


              


        $to_newguide = $newguide->emailid; 
        $to_da       = $content['gettransferdetails']->da_mail;
        $to_tc       = $content['gettransferdetails']->tc_mail;
        $to_fg       = $content['gettransferdetails']->fg_email;
        $to_finance  = $finance->financeemailid;



        $arr = array (

          $to_da      => 'da',
          $to_tc      => 'tc',
          $to_fg      => 'fg',
          $to_finance => 'finance',
        );
             $sendmail = $this->Common_Model->send_email($subject = 'Tranfer Letter ', $message = $html, $to_newguide, $to_name = null,  $arr , $attachments); 
             // echo $sendmail; die;
              //// Send Mail candidates With Offer Letter ////
             $this->session->set_flashdata('tr_msg', 'Successfully added Proposed Transfer');      
           }
         }
       }
       redirect('Proposed_probation_separation/index');  
     }


     $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
     $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();
     // $content['gettransferdetails'] = $this->model->get_Transferdetails($token);
     $content['getofficedetails'] = $this->model->get_Office_Details();

     $query ="select * from mst_reason where isdeleted=0";
     $content['Reson_list'] = $this->db->query($query)->result();

     $query ="select * from mst_extension_sepration_type  where isdeleted=0";
     $content['extension_sep_list'] = $this->db->query($query)->result();
     $content['getintimation'] = $this->model->getIntimation();

     $content['title'] = 'add';
     $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
     $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}




public function addextention($token){

  try{


    if (empty($token) ||  $token == '' ) {

      $this->session->set_flashdata('er_msg', 'Required parameter $token and $candidate_id is either blank or empty.');
      redirect('/Proposed_probation_separation/index');

    } else {

     $dateoftransfer = '';
     $effective_date = '';
     $months = '';
     $getstaffdetail = array();
     $gethrddetail = array();
     $gethrunitdetail = array();
     $getcentraleventdetail = array();

     $RequestMethod = $this->input->server('REQUEST_METHOD');   
     if($RequestMethod == 'POST'){

   // print_r($this->input->post()); die;

       $da = $this->input->post('da');


       $this->form_validation->set_rules('type','type','trim|required');
       $this->form_validation->set_rules('dc','dc','trim|required');
       $this->form_validation->set_rules('team','team','trim|required');
       $this->form_validation->set_rules('da','da','trim|required');
       $this->form_validation->set_rules('probation_extension_date','probation_extension_date ','trim|required');              
       $this->form_validation->set_rules('Reason','Reason','trim|required');
       $this->form_validation->set_rules('comment','comment','trim|required');        


       $dashiptransactions = $this->input->post('dashiptransactions');  

       $dateext = $this->input->post('extension_date');
       $extension_date = $this->model->changedatedbformate($dateext);
       $DateofSeraption = $this->input->post('DateofSeraption');
       $dateofseraption = $this->model->changedatedbformate($DateofSeraption);
       $staffDAid = $this->input->post('da');



       if ($dashiptransactions==6) {

      //print_r($this->input->post()); 
        $staffid = $this->input->post('staff');
        $comment = $this->input->post('commenttransfer');
        $dateoftransfer = $this->input->post('dateoftransfer');
        $effective_date = $this->input->post('effective_date');
        $becoming_executive_date = $this->input->post('becoming_executive_date');
        $summernote = $this->input->post('summernote');
        


        $months = ceil(abs($effective_date - $dateoftransfer) / 86400);

        $getstaffdetail =  $this->model->getDAExtentionDetails($staffid);
        $gethrddetail =  $this->model->gethrDetails($staffid);
        $gethrunitdetail =  $this->model->getHRunitDetails();

        $dateoftransferdb        =  $this->gmodel->changedatedbformate($dateoftransfer);
        $effective_db_date       = $this->gmodel->changedatedbformate($effective_date);
        $getcentraleventdetail   =  $this->model->getCentralEventDetails($effective_db_date, $dateoftransferdb);

       $comeingeventdate = date("F, Y", strtotime($getcentraleventdetail->eventdate));


        $html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2" align="center"><strong>PROFESSIONAL ASSISTANCE FOR DEVELOPMENT ACTION</strong></td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td width="71%"><p>'.$token.'<br />
        Ms. '. $getstaffdetail->name .'<br />
        '. $getstaffdetail->permanenthno .'<br />
        '. $getstaffdetail->permanentstreet .' <br />
        Dist:  '. $getstaffdetail->districtname .'<br />
        '. $getstaffdetail->statename .' - '. $getstaffdetail->permanentpincode .'.</p></td>
        <td width="13%" valign="top">'. date("F j, Y") .'</td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2" align="center"><strong>Subject: - Extension of Apprenticeship period</strong></td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        '.$summernote.'
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="2" align="right"><p>Yours sincerely,</p></td>
        <td>&nbsp;</td>
        </tr>

        <tr>
        <td>&nbsp;</td>
        <td colspan="2" align="right"><p>'. $gethrunitdetail->name .' </p></td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td colspan="2" align="right">'. $gethrunitdetail->desname .'</td>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        </tr>
        <tr>
        <td width="7%">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        </tr>
        </table>';


        $filename = 'extention_'.md5(time());
        $this->load->model('Dompdf_model');
        $generate =   $this->Dompdf_model->transfer_letter_PDF($html, $filename);


          $staffid = $this->input->post('staff');
          $dateoftran = $this->input->post('dateoftransfer');
          $dateoftransfer = $this->model->changedatedbformate($dateoftran);



          $insertArr2 = array(
            'type'                 => $dashiptransactions,
            'dc'                   => $this->input->post('olddc'),
            'team'                 => $this->input->post('oldteam'),
            'staffid'              => $this->input->post('staff'),
            'dateofseraption'      => $dateofseraption,
            'reason'               => $this->input->post('Reason'),
            'comment '             => trim($comment),
            'generate_letter_name' => $filename,
        'createdon'            => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'            => $this->loginData->UserID, // login user id
        'status'               => 1,
        'isdeleted'            => 0, 
      );

          $this->db->insert('tbl_probation_separation', $insertArr2);



          $insertArr2 = array(
            'staffid'             => $this->input->post('staff'),
            'old_office_id'       => $this->input->post('oldteam'),
            'new_office_id'       => $this->input->post('oldteam'),
            'date_of_transfer'    => $dateoftransfer,
            'old_designation'     => 13,
            'new_designation'     => 13,
            'trans_status'        => 'Extention',
            'trans_flag'          => 3,
            'datetime'            => date('Y-m-d H:i:s'),    // Current Date and time
      );

          $this->db->where('id',$token);
          $this->db->update('staff_transaction', $insertArr2);


          $updatestatusArr = array(
            'status'            => 2,
            'updatedby'         => date('Y-m-d H:i:s'),
            'updatedon'         => $this->loginData->UserID, // login user id
            );

          $this->db->where('intemation_type', $dashiptransactions);
          $this->db->where('staffid', $staffid);
          $this->db->update('tbl_hr_intemation', $updatestatusArr);


          $workflowsql  = "SELECT max(workflowid) as workflowid FROM tbl_workflowdetail WHERE r_id =$token"; 
          $workresult  = $this->db->query($workflowsql)->result()[0];

          if ($workresult->workflowid !='') {
            $insertworkflowArr = array(
              'r_id'                 => $token,
              'type'                 => 28,
              'staffid'              => $staffid,
              'sender'               => $this->loginData->staffid,
              'receiver'             => $this->loginData->staffid,
              'forwarded_workflowid' => $workresult->workflowid,
              'senddate'             => date("Y-m-d H:i:s"),
              'flag'                 => 3,
              'scomments'            => $comment,
              'createdon'            => date("Y-m-d H:i:s"),
              'createdby'            => $this->loginData->staffid,
            );

            $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

          }else{

            $insertworkflowArr = array(
              'r_id'                 => $token,
              'type'                 => 28,
              'staffid'              => $staffid,
              'sender'               => $this->loginData->staffid,
              'receiver'             => $this->loginData->staffid,
              'senddate'             => date("Y-m-d H:i:s"),
              'flag'                 => 3,
              'scomments'            => $comment,
              'createdon'            => date("Y-m-d H:i:s"),
              'createdby'            => $this->loginData->staffid,
            );
            $this->db->insert('tbl_workflowdetail', $insertworkflowArr);  
          }

          $this->db->trans_complete();

          if ($this->db->trans_status() === FALSE){
            $this->session->set_flashdata('er_msg', 'Error adding Successfully added Proposed Transfer');  
          }else{


            $getstaffdetail =  $this->model->getDAExtentionDetails($staffid);
            $getextentionfile = $this->model->getDAExtentionFile($staffid);
            
            $staffname = $getstaffdetail->name;
            $staffemail = $getstaffdetail->tcemail;
            $tcname = $getstaffdetail->reportingtoname;
            // $to_name = $this->loginData->hrdemailid;
            $filename = $getextentionfile->generate_letter_name;
            $attachments = array($filename.'.pdf');
            $recipenent = array();
            $html = 'Dear '. $tcname .', <br><br> 
            Please hand over the attached Extension Letter to '.$staffname.'. <br><br><br>
            Regards <br>
            Barsha Mishra <br>
            Team Coordinator <br>
            HRD Unit <br>';
            

             $sendmail = $this->Common_Model->send_email_separation($subject = 'Extention Letter ', $message = $html, $staffemail, $to_name = Null, $recipenent, $attachments);  //// Send Mail candidates With Offer Letter ////
             $this->session->set_flashdata('tr_msg', 'Successfully added Proposed Extention');      
           }
         
       }
       redirect('Proposed_probation_separation/index');  
     }


     $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
     $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();
     $content['getextentiondetails'] = $this->model->get_Extentiondetails($token);
     $content['getofficedetails'] = $this->model->get_Office_Details();


     $query ="select * from mst_reason where isdeleted=0";
     $content['Reson_list'] = $this->db->query($query)->result();

     $query ="select * from mst_extension_sepration_type  where isdeleted=0";
     $content['extension_sep_list'] = $this->db->query($query)->result();
     $content['getintimation'] = $this->model->getIntimation();

     $content['title'] = 'add';
     $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
     $this->load->view('_main_layout', $content);
   }
 }catch (Exception $e) {
  print_r($e->getMessage());die;
}

}

public function send_letter($token){

  try{


    $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
    $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();
    $query ="select * from mst_reason where isdeleted=0";
    $content['Reson_list'] = $this->db->query($query)->result();

    $query ="select * from mst_extension_sepration_type  where isdeleted=0";
    $content['extension_sep_list'] = $this->db->query($query)->result();
    $content['getintimation'] = $this->model->getIntimation();

    $content['title'] = 'send_letter';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}

public function edit($token){

  try{

        // print_r($this->input->post()); //die;
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == 'POST'){

      $probation_extension_date = $this->input->post('probation_extension_date');     
      if (!empty($probation_extension_date)) {
        $ProbationExtensionDate = date('Y-m-d', strtotime($probation_extension_date));
      }

      $DateofSeraption = $this->input->post('DateofSeraption');
      if (!empty($DateofSeraption)) {
        $Dateof_Seraption = date('Y-m-d', strtotime($DateofSeraption));
      }            

      $this->form_validation->set_rules('type','type','trim|required');
      $this->form_validation->set_rules('dc','dc','trim|required');
      $this->form_validation->set_rules('team','team','trim|required');
      $this->form_validation->set_rules('da','da','trim|required');
      $this->form_validation->set_rules('probation_completed','probation_completed','trim|required');
      $this->form_validation->set_rules('DateofSeraption','DateofSeraption','trim|required');
      $this->form_validation->set_rules('probation_extension_date','probation_extension_date ','trim|required');              
      $this->form_validation->set_rules('Reason','Reason','trim|required');
      $this->form_validation->set_rules('comment','comment','trim|required'); 

      $typeid = $this->input->post('type');       
      if ($typeid == 1) {

        $insertArr2 = array(

          'type'        => $this->input->post('type'),
          'dc'            => $this->input->post('dc'),
          'team'        => $this->input->post('team'),
          'da'            => $this->input->post('da'),
          'probation_completed'   => $this->input->post('probation_completed'),
          'probation_extension_date' => $ProbationExtensionDate,
          'extension_date'  => $this->input->post('extension_date'),
          'comment '        => trim($this->input->post('comment')),
        'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'       => $this->loginData->UserID, // login user id
        'status'      => 1,
        'isdeleted'       => 0, 
      );
      }else if ($typeid == 2) {

        $insertArr2 = array(

          'type'        => $this->input->post('type'),
          'dc'            => $this->input->post('dc'),
          'team'        => $this->input->post('team'),
          'da'            => $this->input->post('da'),
          'dateofseraption'   => $Dateof_Seraption,
          'extension_date'  => $this->input->post('extension_date'),
          'Reason'        => $this->input->post('Reason'),
          'comment '        => trim($this->input->post('comment')),
          'status'      => 1,
        'createdon'       => date('Y-m-d H:i:s'),    // Current Date and time
        'createdby'       => $this->loginData->UserID, // login user id
        'isdeleted'       => 0, 
      );
      }
      $this->db->where('id',$token);
      $this->db->update('tbl_probation_separation', $insertArr2);
      $this->session->set_flashdata('tr_msg', 'Successfully updated Proposed Probation Separation');
      redirect('Proposed_probation_separation');      

    }

    $content['statedetails'] = $this->model->stateList();
    $content['getprobationdetails'] = $this->model->get_Probation_Separation($token);

    $content['getdevelopmentcluster'] = $this->model->get_Development_Cluster();
    $content['getprobseparation'] = $this->model->get_Prob_Separation($token);
    $content['getdevelopmentapprentice'] = $this->model->get_Development_Apprentice();     

      // print_r($content['getprobationdetails']); die();

    $query ="SELECT `lpooffice`.`officeid`, `lpooffice`.`officename` FROM `lpooffice` INNER JOIN  `dc_team_mapping` ON `dc_team_mapping`.`teamid` = `lpooffice`.`officeid` ";
    $content['team_list'] = $this->db->query($query)->result();

    $query ="select * from mst_reason where isdeleted=0";
    $content['Reson_list'] = $this->db->query($query)->result();

    $query ="select * from mst_extension_sepration_type where isdeleted=0";
    $content['extension_sep_list'] = $this->db->query($query)->result();
       // print_r($content['extension_sep_list']); die;

    $content['title'] = 'add';
    $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
    $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
    print_r($e->getMessage());die;
  }

}







function candidate_exists($da)
{

 $this->db->select('*'); 
 $this->db->from('tbl_probation_separation');
 $this->db->where('da', $da);
 
 $query = $this->db->get();
 //echo $this->db->last_query();die; 
 //echo $query->num_rows(); die;
 if ($query->num_rows() > 0) {
   return 1;
 } else {
   return 0;
 }
}
  /**
   * Method delete() delete data. 
   * @access  public
   * @param 
   * @return  array
   */ 
  public function delete($token)
  {

    $this->view['detail'] = $this->model->getCampusDetails($token);
    if(count($this->view['detail']) < 1) {
      $this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not found!!');
      redirect($this->router->class);
    }
    

    if($this->model->delete($token) == '1'){
      $this->session->set_flashdata('tr_msg', ucfirst(str_replace('_',' ',$this->router->class)).' deleted successfully!!');
      redirect($this->router->class);
    }
    else {
      $this->session->set_flashdata('er_msg', ucfirst(str_replace('_',' ',$this->router->class)).' not deleted!!');
      redirect($this->router->class);
    }
  }

}