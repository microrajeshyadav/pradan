<?php 


class Leave_encashment extends CI_controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
     $this->load->model("Global_model","gmodel");
    $this->load->model("Common_model","Common_Model");
    $this->load->model('Employee_particular_form_model');
    $this->load->model('Joining_report_on_appointment_model');
    $this->load->model("General_nomination_and_authorisation_form_model");
       $this->load->model('Medical_certificate_model');
       
       $this->load->model('Leave_encashment_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');

 }

 public function index($token)
 {
  try{
$query ="SELECT * FROM staff_transaction WHERE id=".$token;
                  $content['staff_transaction'] = $this->db->query($query)->row();              
  $query ="SELECT * FROM staff_transaction WHERE id =".$token; 
      $content['staff_service_certificate'] = $this->db->query($query)->row();
      
      $content['staff_detail'] = $this->Common_Model->get_staff_sep_detail($token);
    


      $query = "SELECT * FROM tbl_leave_encashment WHERE transid = ".$token;
      $content['leave_detail'] = $this->db->query($query)->row();
       $RequestMethod = $this->input->server('REQUEST_METHOD');
      if($RequestMethod == "POST"){
                        $db_flag = '';
                        if($this->input->post('Save') == '0'){
                              $db_flag = 0;
                        }else if($this->input->post('saveandsubmit') == '1'){
                              $db_flag = 1;
                        }

                         $insertarray=array(
                          'staffid'=>$content['staff_transaction']->staffid,
                          'transid'=>$token,
                          'sir'=>$this->input->post('name'),
                          'bankname'=>$this->input->post('bank_name'),
                          'bankaddress'=>$this->input->post('address'),
                          'accountnumber'=>$this->input->post('account_number'),
                          'flag'=>$db_flag,
                          'updatedby'=>  $this->loginData->staffid,

                          ); 
                         if($content['leave_detail'])
                         {
                         $updatearray=array(
                          'staffid'=>$content['staff_transaction']->staffid,
                          'transid'=>$token,
                          'sir'=>$this->input->post('name'),
                          'bankname'=>$this->input->post('bank_name'),
                          'bankaddress'=>$this->input->post('address'),
                          'accountnumber'=>$this->input->post('account_number'),
                          'flag'=>$db_flag,
                          'updatedby'=>  $this->loginData->staffid,


                          ); 
                         $this->db->where('id', $content['leave_detail']->id);
          $flag = $this->db->update('tbl_leave_encashment', $updatearray);
          
                       }
                       else
                       {
                        $flag = $this->db->insert('tbl_leave_encashment',$insertarray);
                      
                       }
                       if($flag) {$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
        redirect(current_url());
      } 
                else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
               redirect(current_url());
            }
      



                        
}

  
  prepareviewss:
 

  $query1="Select officename from tbl_table_office";
   $content['officename_list']=$this->db->query($query1)->row();

 prepareview:
 $content['title'] = 'Leave_encashment';
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);


 }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function previewofjoining($staff=null,$candidate_id=null)
{
  try{

    $staff = $this->uri->segment(3);
                       // $joining_staff=$staff;
                        $candidate_id  = $this->uri->segment(4);



                        $this->session->set_userdata('staff', $staff);
                        $this->session->set_userdata('candidate_id', $candidate_id);





                        $staff_id=$this->loginData->staffid;
                        $login_staff=$this->loginData->staffid;
                        $candidateid=$this->loginData->candidateid;
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        $candidateid=$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff_id;
         $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;
        
      }


$content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);
$content['id']=$this->Employee_particular_form_model->getworkflowid($staff_id);
                              
                            
                            $content['personal']=$this->Common_Model->get_Personnal_Staff_List();
                           
                            if($this->loginData->RoleID==2)
                            {
                                 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


                             if($RequestMethod == 'POST'){
                               //print_r($_POST);die;
                                $status=$this->input->post('status');
                                $reject=$this->input->post('reject');
                                $p_status=$this->input->post('p_status');
                                $r_id=$this->input->post('id');
                                
                                $check = $this->session->userdata('insert_id');
                              


                                 $insert_data =array(
        'type'=>10,
       'r_id'=> $r_id,
       'sender'=> $login_staff,
       'receiver'=>$p_status,
       'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
       'createdby'=>$login_staff,
       'scomments'=>$reject,
       'forwarded_workflowid'=>$content['id']->workflow_id,
       'flag'=> $status,
       'staff'=>$staff_id
        );
                                
                               
        $this->db->insert('tbl_workflowdetail', $insert_data);
        $arr=array(
          'countersigned_date'=>$this->General_nomination_and_authorisation_form_model->changedatedbformate($this->input->post('Countersigneddate')),
          'countersigned_place'=>$this->input->post('Countersignedplace'),
          'countersigned_name'=>$login_staff
          );
        $this->db->where('id',$r_id); 
        $this->db->update('tblstaffjoiningreport',$arr);
        
      
       
                                
                             }


                            }
                            else if($this->loginData->RoleID==17)
                            {
                                 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


                             if($RequestMethod == 'POST'){
                               //print_r($_POST);die;
                                $status=$this->input->post('status');
                                $reject=$this->input->post('reject');
                                $p_status=$this->input->post('p_status');
                                $r_id=$this->input->post('id');
                                
                                $check = $this->session->userdata('insert_id');
                              


                                 $insert_data =array(
        'type'=>10,
       'r_id'=> $r_id,
       'sender'=> $login_staff,
       'receiver'=>$staff_id,
       'senddate'=>date('Y-m-d'),
      'createdon'=>date('Y-m-d H:i:s'),
       'createdby'=>$login_staff,
       'scomments'=>$reject,
       'forwarded_workflowid'=>$content['id']->workflow_id,
       'flag'=> $status,
       'staff'=>$staff_id
        );
                                
                               
        
        $arr=array(
          
          'location'=>$this->input->post('newoffice'),
          'finance_personnel_misunit'=>$login_staff
          );
        $this->db->where('id',$r_id); 
        $this->db->update('tblstaffjoiningreport',$arr);
        $this->db->insert('tbl_workflowdetail', $insert_data);
       // echo $this->db->last_query();
        //die;
      
       
                                
                             }


                            }





 // $candidateid=($this->loginData->candidateid);
  //$staffids=($this->loginData->staffid);

  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

  $content['topbar'] = $this->Employee_particular_form_model->do_uploadsssss($candidateid);


  $query1="Select * from tbl_table_office";
  $content['officename_list']=$this->db->query($query1)->row();
  //print_r($content['officename_list']);
  //die();

  $this->load->model('Joining_report_on_appointment_model');
  $content['fetchresult']=$this->Joining_report_on_appointment_model->fetchdatas($candidateid);
  //print_r($content['fetchresult']);
  //die();



  $this->load->model('Joining_report_on_appointment_model');
  $content['dataofidentity']=$this->Joining_report_on_appointment_model->fetchdata($candidateid);
  $content['location']=$this->Joining_report_on_appointment_model->all_office();
  //print_r($content['location']);
  

  


  $content['subview'] = 'Joining_report_on_appointment/previewofjoining';
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function Add($staff=null,$candidate_id=null)
{
  try{


 $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);



    
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        $candidateid=$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff_id;
         $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;
        
      }



 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();

 $content['topbar'] = $this->Employee_particular_form_model->do_uploadsssss($candidateid);
 prepareview:


 $content['subview'] = 'Joining_report_on_appointment/add';
 $this->load->view('_main_layout', $content);

 }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}



public function Edit($staff=null,$candidate_id=null)
{
  try{
  
 //$staffids=($this->loginData->staffid);
 //$candidateid=($this->loginData->candidateid);
 $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);



    
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        $candidateid=$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff_id;
         $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;
        
      }


 $content['topbar'] = $this->Employee_particular_form_model->do_uploadsssss($candidateid);
        // start permission 

 

 $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
 $content['role_permission'] = $this->db->query($query)->result();
// end permission 

$content['candidatedetails'] = $this->Employee_particular_form_model->getCandidateDetailsPreview($staff_id);


 $this->load->model('Joining_report_on_appointment_model');
 $content['fetchresult']=$this->Joining_report_on_appointment_model->fetchdatas($candidateid);


 $query1="Select * from tbl_table_office";
 $content['officename_list']=$this->db->query($query1)->result();

 $this->load->model('Joining_report_on_appointment_model');
 $content['dataofidentity']=$this->Joining_report_on_appointment_model->fetchdata($candidateid);

 $RequestMethod = $this->input->server('REQUEST_METHOD'); 


 if($RequestMethod == 'POST'){


  $this->form_validation->set_rules('address','Address','trim|required|min_length[1]|max_length[50]');
        //$this->form_validation->set_rules('status','Status','trim|required');

  if($this->form_validation->run() == FALSE){
    $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
      '</div>');

    $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

    $hasValidationErrors    =    true;
    goto prepareview;

  }



  if ($_FILES['photoupload1']['name'] != NULL) {

    $data=$_FILES['photoupload1'];

    $this->load->model('Joining_report_on_appointment_model');

    $fileDatas=$this->Joining_report_on_appointment_model->do_uploads($data);

    $updateArr['photoupload1'] = $fileDatas['orig_name'];
    $updateArr['photoupload1_encrypted_filename'] = $fileDatas['file_name'];

  }

  else
  {
    $updateArr['photoupload1_encrypted_filename']=$this->input->post('foo');

  }



  $du=$this->input->post('dutydate');

  $dd=$this->input->post('declarationdate');
    //$co=$this->input->post('Countersigneddate');


  $this->load->model('Joining_report_on_appointment_model');
  $duuu=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($du);

  $dddd=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($dd);
     //$co=$this->Joining_report_on_appointment_model->getCandidateDetailsPreview($co);


  if($this->input->post('NotedinProbationregister')==on)
  {
    $tm=1;
  }
  else
  {
    $tm=0;
  }


  $operation=$this->input->post('operation');

  if($operation == 0){

   $updateArr = array(
    'address'      => $this->input->post('address'),
    'duty_date'      =>$duuu,
    'declaration_date'      =>$dddd,
    'declaration_place'      => $this->input->post('declarationplace'),
    'signature'      =>   $updateArr['photoupload1_encrypted_filename'],
    'candidate_id'=> $candidateid,
    'updatedon'      => date('Y-m-d H:i:s'),
    'updatedby'      => $this->loginData->UserID,
    'flag'=>0

  );

   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tblstaffjoiningreport', $updateArr);
   
   $this->session->set_flashdata('tr_msg', 'Successfully added  JOINING REPORT ON APPOINTMENT');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Joining_report_on_appointment/edit/');


 } else {



   $updateArrs = array(
    'address'      => $this->input->post('address'),
    'duty_date'      =>$duuu,
    'declaration_date'      =>$dddd,
    'declaration_place'      => $this->input->post('declarationplace'),
    'signature'      =>   $updateArr['photoupload1_encrypted_filename'],
    'candidate_id'=> $candidateid,
    'updatedon'      => date('Y-m-d H:i:s'),
    'updatedby'      => $this->loginData->UserID,
    'flag'=>1

  );


   $this->db->where('candidate_id', $candidateid);
   $this->db->update('tblstaffjoiningreport', $updateArrs);


   $this->session->set_flashdata('tr_msg', 'Successfully added  JOINING REPORT ON APPOINTMENT');
   $this->session->set_flashdata('er_msg', $this->db->error());
   redirect('/Joining_report_on_appointment/previewofjoining/');

 }


}
prepareview:

$content['subview'] = 'Joining_report_on_appointment/edit';
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function getpdfjoining($staff=null,$candidate_id=null)
{
  try{

 // $candidateid=($this->loginData->candidateid);
  //$staffids=($this->loginData->staffid);
 $this->session->set_userdata('staff', $staff);
            $this->session->set_userdata('candidate_id', $candidate_id);



    
      if(isset($staff_id)&& isset($candidateid))
      {
        
        $staff_id=$staff_id;
        $candidateid=$candidateid;
       
      }
      else
      {
         $staff_id=$staff;
         //echo "staff".$staff_id;
         $candidateid=$candidate_id;
        // echo "candidateid".$candidateid;
        
      }





  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();

  $content['topbar'] = $this->Employee_particular_form_model->do_uploadsssss($candidateid);


  $query1="Select * from tbl_table_office";
  $content['officename_list']=$this->db->query($query1)->row();
$rows=$content['officename_list'];

  $content['dataaa'] = $this->Joining_report_on_appointment_model->fetchsssssss($candidateid);

  $row=$content['dataaa'];
  //print_r($content['dataaa']);

$timestamp = strtotime($row->declaration_date);
$datee=date('d/m/Y', $timestamp);
$sig=site_url().'datafiles/signature/'.$row->signature;


  $html='  
  <div class="container">

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">

  <div class="body">
  <div class="row clearfix doctoradvice">
    <h4 class="header" class="field-wrapper required-field" style="color:green; text-align: center;">Professional Assistance for Development Action (PRADAN)  <br><br> PREVIEW: JOINING REPORT ON APPOINTMENT </h4>

    <form method="POST" action="" enctype="multipart/form-data">
      <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="form-group">

          <label for="ExecutiveDirector" class="field-wrapper required-field">To:<br>The Executive Director,<br>
          PRADAN.</label>
          <br>
          '.$rows->officename.'
        </br>
      </div>
    </div>
    <div  class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    </div>



    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right">
      <label for="Name">Address:&nbsp;</label>&nbsp;
      '.$row->address.'


    </div>

    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <label for="StateNameEnglish" class="field-wrapper required-field">Dear Sir, </label>
      </div>
    </div>

    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <label class="field-wrapper required-field">Subject: Joining Report </label>
      </div>
    </div>
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="text-align: justify;">
      <div class="form-group">
       Please refer to your offer of appointment no &nbsp;<b>'. $row->offerno.'</b>&nbsp;dated
        &nbsp;<b>
       '.$row->doj.'</b>&nbsp;
        offering me appointment as  &nbsp;<b>'.$row->desname.'</b>&nbsp;at
          &nbsp;<b>'.$row->officename.'</b>&nbsp;
          I hereby report for duty in the forenoon of today,<br><br> the 
          &nbsp; 
          <b>
           '.$row->duty_date.'
         </b>
        (date/month/year). I shall inform you of any change in my address,given above, when it occurs.
         <br><br>
         Yours faithfully,
         <br>
         <br></div></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <p class="text-left">Signature
            :<img src="'.$sig.'"  style="height:64px;width:256px;" >

          </p>
          <p class="text-left">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$row->candidatefirstname.'</b></p>
        </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p class="text-right">Place:&nbsp;
              <b>'.$row->declaration_place.'</b>
            </p>
            <p class="text-right" >Date:&nbsp;
              <b>
             '.$datee.'

           </b>
         </p>
       </div><br><br>
       

     </form>
   </div>
 </div></div></div></div></div>';

  $filename = "".$token."joining";
  $this->Employee_particular_form_model->generatePDF($html, $filename, NULL,'Generateofferletter.pdf');  
  //$this->session->set_flashdata('tr_msg', 'Successfully Generate Offer Letter');     


}catch (Exception $e) {
      print_r($e->getMessage());die;
    }

}

}