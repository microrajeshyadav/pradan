<?php 

/**
* State List
*/
class Pgeducation extends CI_controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model("Common_model","Common_Model");
    $this->load->model(__CLASS__ . '_model');
    $check = $this->session->userdata('login_data');
    ///// Check Session //////  
    if (empty($check)) {
     redirect('login');
   }

   $this->loginData = $this->session->userdata('login_data');
 }

 public function index()
 {
  try{
   
    // start permission 
   $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
   $content['role_permission'] = $this->db->query($query)->result();
// end permission 

   $query = "select id, pgname, status, isdeleted, case when groupid = 1 then ''  when groupid = 2 then '' when groupid = 3 then 'UG'  when groupid = 4 then 'PG' else '' end gro  from mstpgeducation where isdeleted='0' order by groupid asc";
   $content['mstpgeducation_details'] = $this->Common_Model->query_data($query);
   //$content['subview']="index";
   $content['title'] = 'Pgeducation';
   $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
   $this->load->view('_main_layout', $content);

   }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
 }
 public function Add()
 {
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  //print_r( $this->input->post()); 


  $RequestMethod = $this->input->server('REQUEST_METHOD'); 
  

  if($RequestMethod == 'POST'){

    $this->form_validation->set_rules('pgname','PG Name','trim|required|min_length[1]|max_length[50]|is_unique[mstpgeducation.pgname]');
    // $this->form_validation->set_rules('status','Status','trim|required');

    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }


    $insertArr = array(
      'pgname'      => $this->input->post('pgname'),
      'groupid'     => $this->input->post('groupname'),
    );
    // print_r($insertArr); die;
    $this->db->insert('mstpgeducation', $insertArr);
     // echo $this->db->last_query(); die;
    $this->session->set_flashdata('tr_msg', 'Successfully added PG Education');
    $this->session->set_flashdata('er_msg', $this->db->error());
    redirect('/Pgeducation/index/');
  }

  prepareview:

  
  $content['subview'] = 'Pgeducation/add';
  $this->load->view('_main_layout', $content);

  }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

public function edit($token)
{
  try{
        // start permission 
  $query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->session->userdata('login_data')->RoleID." ";
  $content['role_permission'] = $this->db->query($query)->result();
// end permission 

  
    //print_r( $this->input->post()); die;

  $RequestMethod = $this->input->server('REQUEST_METHOD');

  if($RequestMethod == 'POST'){

    $this->db->trans_start();

    $this->form_validation->set_rules('pgname','PG name','trim|required');
    $this->form_validation->set_rules('groupname','Group Name','required');
    
    if($this->form_validation->run() == FALSE){
      $this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
        '</div>');

      $this->session->set_flashdata('errtr_msg', 'There have been validation error(s), please check the error messages');

      $hasValidationErrors    =    true;
      goto prepareview;

    }

    $updateArr = array(
      'pgname'   => $this->input->post('pgname'),
      'groupid'  => $this->input->post('groupname'),
    );

    $this->db->where('id', $token);
    $this->db->update('mstpgeducation', $updateArr);

    $this->db->trans_complete(); 

    if ($this->db->trans_status() === FALSE){

     $this->session->set_flashdata('er_msg', $this->db->error());  
   }else{
     
    $this->session->set_flashdata('tr_msg', 'Successfully Updated PG');
    redirect('/Pgeducation/');
  }
}

prepareview:

$query = "select groupid from mstpgeducation where isdeleted='0' order by groupid asc";
$content['groupdetail'] = $this->Common_Model->query_data($query);

$content['title'] = 'Pgeducation';
$state_details = $this->Common_Model->get_data('mstpgeducation', '*', 'id',$token);
$content['mstpgeducation_details'] = $state_details;



$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
$this->load->view('_main_layout', $content);

}catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}
function delete($token = null)
{
  try{
      //print_r($token);die();
 $updateArr = array(
  'Isdeleted'    => 1,
);
 $this->db->where('id', $token);
 $this->db->update('mstpgeducation', $updateArr);
 $this->session->set_flashdata('tr_msg', 'PG Deleted Successfully');
 redirect('/Pgeducation/');
 $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
 $this->load->view('_main_layout', $content);

 }catch (Exception $e) {
      print_r($e->getMessage());die;
    }
}

}