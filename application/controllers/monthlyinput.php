<?php 
class Monthlyinput extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Monthlyinput_model","model");
		$this->load->model('Global_Model','gmodel');


		$check = $this->session->userdata('login_data');
		if (empty($check)) {
			redirect('login');
		}

		$this->loginData = $this->session->userdata('login_data');
	}

	public function index()
	{

		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
// end permission 
		// echo "<pre>";
		// print_r($this->input->post()); die;

			$RequestMethod = $this->input->server('REQUEST_METHOD');
			if($RequestMethod == 'POST'){


				$this->form_validation->set_rules('ddlhead','Salary heads ','trim|required');
				$this->form_validation->set_rules('ddloffice','Office','trim|required');
				$this->form_validation->set_rules('ddlemployee','Staff','trim|required');
				$this->form_validation->set_rules('txtamount','Ammount','trim|required|numeric');
				if($this->form_validation->run() == FALSE){
					$this->form_validation->set_error_delimiters('<div style="color: red; font-size: 12px;"> ',
						'</div>');

					$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

					$hasValidationErrors    =    true;
					goto prepareview;

				}

				
				$ddlheadval = $this->input->post('ddlhead');
				$currentmonthyear = date('m-Y');
				$currentmonthyearexp = explode('-', $currentmonthyear);
				$currentmonth = $currentmonthyearexp[0];
				$currentyear = $currentmonthyearexp[1];
				//print_r($currentmonthyearexp); die;
				$salmonth 	 = $this->input->post('salmonth');
				$salyear 	 = $this->input->post('salyear');
				$ddloffice   = $this->input->post('ddloffice');
				$ddlemployee = $this->input->post('ddlemployee');

				$txtamount = $this->input->post('txtamount');

				$txtamount = str_replace(',','',$txtamount);

				if (!empty($salmonth)) {
					$salmonth 	 = $this->input->post('salmonth');
				}else{
					$salmonth = 0;
				}


				if (!empty($salyear)) {
					$salyear 	 = $this->input->post('salyear');
				}else{
					$salyear = 0;
				}

				$sql="SELECT Count(staffid) as staffcount FROM `tblmonthlyinput` WHERE salmonth=".$salmonth."  AND salyear=".$salyear." AND officeid=".$ddloffice." AND staffid=".$ddlemployee; 
				$result = $this->db->query($sql)->row();

				if ($result->staffcount > 0) {
					$updateArrmonthlyinp = array (
						$ddlheadval         => $txtamount
					);

					$this->db->WHERE('salmonth', $salmonth);
					$this->db->WHERE('salyear', $salyear);
					$this->db->WHERE('officeid', $ddloffice);
					$this->db->WHERE('staffid', $ddlemployee);
					$this->session->set_flashdata('tr_msg', 'Successfully update MonthlyInput !!!');
					$this->db->update('tblmonthlyinput', $updateArrmonthlyinp);
				// echo $this->db->last_query(); die;
				}else{

					$insertArrmonthlyinp = array (
						'officeid'          => $this->input->post('ddloffice'),
						'salmonth'          => $currentmonth,
						'salyear'           => $currentyear,
						'staffid'           => $this->input->post('ddlemployee'),
						$ddlheadval         => $this->input->post('txtamount')
					);

					$this->db->insert('tblmonthlyinput', $insertArrmonthlyinp);
					$this->session->set_flashdata('tr_msg', 'Successfully Add MonthlyInput !!!');
					redirect('/Monthlyinput/index');
				}
			}

			prepareview:

			$content['getsalaryhead'] = $this->model->getsalaryhead();
			$content['getofficedest'] = $this->model->getofficedetails();
			$content['title'] = 'Monthlyinput';
			$content['subview'] ='Monthlyinput/index';
			$this->load->view('_main_layout', $content);

		}catch (Exception $e) {
			print_r($e->getMessage());die;
		}



	}


}
