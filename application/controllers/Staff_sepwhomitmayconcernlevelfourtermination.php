<?php 
class Staff_sepwhomitmayconcernlevelfourtermination extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Staff_sepwhomitmayconcernlevelfourtermination_model');
		$this->load->model("Global_model","gmodel");
		$this->load->model("Common_model","Common_Model");


		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {
			redirect('login');			 
		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index($token)
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$query ="SELECT * FROM staff_transaction WHERE id =".$token; 
			$content['seplevel_fourtermination'] = $this->db->query($query)->row();

			$content['level_fourtermination'] = $this->Common_Model->get_staff_sep_detail($token);
			// print_r($content['level_fourtermination']);
			// die;
			$content['staffid'] = $content['seplevel_fourtermination']->staffid;
			
			// print_r($content['level_fourtermination']); die;

			$query = "SELECT * FROM tbl_levelwisetermination WHERE type = 2 and  transid = ".$token;
			$content['levelfourtermination_detail'] = $this->db->query($query)->row();
			// print_r($content['levelfourtermination_detail']);
			// die;


			$tdate = date('d/m/Y');
				$emp_code = $content['level_fourtermination']->emp_code;
			$staffname = $content['level_fourtermination']->name;
			$dc_name = $content['level_fourtermination']->dc_name;
			$sepdesig = $content['level_fourtermination']->sepdesig;
			$joiningdate = $this->gmodel->changedatedbformate($content['level_fourtermination']->joiningdate);
			$seperatedate = $this->gmodel->changedatedbformate($content['level_fourtermination']->seperatedate);
			$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 87 AND `isactive` = '1'";
   		    $data = $this->db->query($sql)->row();
   		   
   		    

   		    if (!empty($content['levelfourtermination_detail'])) {
   		    	//print_r($content['levelthreetermination_detail']->sep_no); die();
   		    	$sep_no = $content['levelfourtermination_detail']->sep_no;
   		    	$total_staff = $content['levelfourtermination_detail']->total_staff;
				$total_professional = $content['levelfourtermination_detail']->total_professional;
				$staff1 = array();
   		    	$staff1_replace = array();
				$staff1 = array('$sep_no','$emp_code','$tdate','$name','$dc_name','$sepdesig','$joiningdate','$total_staff','$total_professional','$seperatedate','$edsign');
				$staff1_replace = array($sep_no,$emp_code,$tdate,$staffname,$dc_name,$sepdesig,$joiningdate,$total_staff,$total_professional,$seperatedate,'');
					$body='';
				if(!empty($data))
				$body = str_replace($staff1,$staff1_replace , $data->lettercontent);

				$body = str_replace('$total_staff',$total_staff, $body);
				$body = str_replace('$total_professional',$total_professional, $body);
				// echo $body; die();

			}
			else{
				$staff1 = array();
   		    	$staff1_replace = array();
				$staff1 = array('$sep_no','$emp_code','$tdate','$name','$dc_name','$sepdesig','$joiningdate','$total_staff','$total_professional','$seperatedate','$edsign');
				$staff1_replace = array('',$emp_code,$tdate,$staffname,$dc_name,$sepdesig,$joiningdate,'','',$seperatedate,'');
				if(!empty($data))
					$body = str_replace($staff1,$staff1_replace , $data->lettercontent);
			}

			if(!empty($body))
   		    {
   		    	$content['content'] = $body;
   		    }

			// print_r($content['levelfour_detail']); die;

			$content['staffid'] = $content['seplevel_fourtermination']->staffid;

			$RequestMethod = $this->input->server("REQUEST_METHOD");


			if($RequestMethod == 'POST')
			{

				
				$db_flag = '';
				if($this->input->post('Save') == 'Save'){
					$db_flag = 0;
				}else if($this->input->post('saveandsubmit') == 'save & submit'){

					$db_flag = 1;
				}

				$body = '';
				$body = $this->input->post('lettercontent');
				$filename = "";
				$filename = md5(time() . rand(1,1000));
				$this->load->model('Dompdf_model');
				$generate =   $this->Dompdf_model->generatePDF($body, $filename, NULL,'EXPERIENCECERTIFICATE.pdf');

				$insertArray = array(

					'desid'              => $this->input->post('sepdesigid'),
					'staffid'            => $content['seplevel_fourtermination']->staffid,
					'transid'            => $token,
					'sep_no'             => $this->input->post('sep_no'),
					'total_staff'        => $this->input->post('total_staff'),
					'total_professional' => $this->input->post('total_professional'),
					'filename'		 	 => $filename,
					'content'		 	 => $body,
					'flag'               => $db_flag,
					'type'               => 2,
					'createdby'          => $this->loginData->staffid,
				);

				if($content['levelfourtermination_detail'])
				{
					$updateArray = array(
					'desid'              => $this->input->post('sepdesigid'),	
					'staffid'            => $content['seplevel_fourtermination']->staffid,
					'transid'            => $token,
					'sep_no'             => $this->input->post('sep_no'),
					'total_staff'        => $this->input->post('total_staff'),
					'total_professional' => $this->input->post('total_professional'),
					'filename'		 	 => $filename,
					'content'		 	 => $body,
					'flag'               => $db_flag,
					'type'               => 2,
					'updatedby'          => $this->loginData->staffid,
					'updatedon'          => date("Y-m-d H:i:s"),
					);
					$this->db->where('id', $content['levelfourtermination_detail']->id);
					$flag = $this->db->update('tbl_levelwisetermination', $updateArray);
				}else{
					$flag = $this->db->insert('tbl_levelwisetermination',$insertArray);
				}
				//echo $this->db->last_query(); die;

				if($flag) {
					$subject = "Your Request Submited Successfully";
				  $body = 'Dear,<br><br>';
				  $body .= '<h2>Your Request has been Submited Successfully </h2><br>';
				  $body .= 'This is to certify that Mr. '.$content['level_fourtermination']->name.'<br> has issued experience certificate.<br><br>';
				  $body .= 'Thanks<br>';
				  $body .= 'Administrator<br>';
				  $body .= 'PRADAN<br><br>';

				  $body .= 'Disclaimer<br>';
				  $body .= '<small>The contents of this Email communication are confidential to the addressee.</small>';

				  $to_email = $content['level_fourtermination']->emailid;
				  $to_name = $content['level_fourtermination']->name;
				  // $email_result = $this->Common_Model->send_email($subject, $body, $to_email, $to_name);
				  // if (substr($email_result, 0, 5) == "ERROR") {
				  // 	$this->session->set_flashdata('er_msg', "Error sending actication email, please contact system administrator");
				  // }
					$this->session->set_flashdata('tr_msg','Data Saved Successfully.');
					redirect(current_url());
				} 
				else {$this->session->set_flashdata('er_msg','Something Went Wrong!!');
					redirect(current_url());
				}


	}
	$content['title'] = 'Staff_sepwhomitmayconcernlevelfourtermination';
	$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
	$this->load->view('_main_layout', $content);
}
catch(Exception $e)
{
	print_r($e->getMessage());
	die();
}

}


}