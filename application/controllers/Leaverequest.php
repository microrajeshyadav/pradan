<?php 
class Leaverequest extends CI_Controller
{

	// echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Leaverequest_model');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
	    $this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			 redirect('login');
			 
		}

		$this->loginData = $this->session->userdata('login_data');
	
	}

	public function index($token = null)
	{
		// start permission 
		$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
		$content['role_permission'] = $this->db->query($query)->result();
		// end permission  

		
			$empcode = $this->Leaverequest_model->get_empid($token);
			//print_r($empcode); 

			//echo $empcode->emp_code; die;

		  

			$content['leaverequest'] = $this->Leaverequest_model->getrequestedleave($empcode->emp_code);
			 // echo "<pre>";
			 // print_r($content['leaverequest']); 
			 // die;

			$content['title'] = 'Leave Request';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		
	}
}