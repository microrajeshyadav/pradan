<?php 
class Probation_personnel_reviewofperformance extends CI_Controller
{

	///echo  "zZxZxZX"; die; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model("Common_model","Common_Model");
		$this->load->model("Probation_personnel_reviewofperformance_model","model");
		$this->load->model("Global_model","gmodel");

		//$this->load->model(__CLASS__ . '_model');
		$mod = $this->router->class.'_model';
		$this->load->model($mod,'',TRUE);
		$this->model = $this->$mod;

		$check = $this->session->userdata('login_data');

		///// Check Session //////	
		if (empty($check)) {

			redirect('login');

		}

		$this->loginData = $this->session->userdata('login_data');

	}

	public function index()
	{
		// start permission 
		try{
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['title'] = 'Probation_personnel_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function add($token)
	{
		// start permission 
		try{
			

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$this->load->model("Probation_personnel_reviewofperformance_model","model");

			$getstaffdetail = $this->Probation_personnel_reviewofperformance_model->getstaffid($token);
			$staffid = $getstaffdetail->staffid;

			$Edid = $this->Probation_personnel_reviewofperformance_model->getEDid();



			
			// print_r($getstaffdetail); die;
			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){

				$this->db->trans_start();

				$date_of_appointment = $this->input->post('date_of_appointment');
				$date_of_appointment_db = $this->gmodel->changedatedbformate($date_of_appointment);

				$period_of_review_from = $this->input->post('period_of_review_from');
				$period_of_review_from_db = $this->gmodel->changedatedbformate($period_of_review_from);

				$period_of_review_to = $this->input->post('period_of_review_to');
				$period_of_review_to_db = $this->gmodel->changedatedbformate($period_of_review_to);

				$probation_extension_date = $this->input->post('probation_extension_date');
				$probation_extension_date_db = $this->gmodel->changedatedbformate($probation_extension_date);

				$period_latestby = $this->input->post('latestby');
				$period_latestby_db = $this->gmodel->changedatedbformate($period_latestby);


				$insertArr = array(
					'transid'                     => $token,
					'staffid'                     => $this->input->post('staffid'),
					'date_of_appointment'         => $date_of_appointment_db,
					'period_of_review_from'       => $period_of_review_from_db,
					'period_of_review_to'         => $period_of_review_to_db,
					'work_habits_and_attitudes'   => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity' => $this->input->post('conduct_and_social_maturity'),
					'integrity'                   => $this->input->post('questionable'),
					'any_other_observations'      => $this->input->post('any_other_observations'),
					'latestby'                    => $period_latestby_db,
					'satisfactory'                => $this->input->post('satisfactory'),
					'probation_completed'         => $this->input->post('probation_completed'),
					'probation_extension_date'    => $probation_extension_date_db,

					'flag'                        => 1,
					'createdby'                   => $this->loginData->staffid,
					'createdon'                   => date('Y-m-d H:i:s'),
				);

				$this->db->insert('tbl_probation_review_performance', $insertArr);

				$insertid = $this->db->insert_id();   

				$updateArr = array(
					
					'trans_flag'           => 3,
					'updatedon'            => date("Y-m-d H:i:s"),
					'updatedby'            => $this->loginData->staffid,
				);
				$this->db->where('id', $token);
				$this->db->update('staff_transaction', $updateArr);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => 11,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 3,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);
				// echo "<pre>";
				// print_r($insertworkflowArr); die;
				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);
				$this->db->trans_complete();
				if ($this->db->trans_status() === true){
					$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
					redirect('/Probation_personnel_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
					redirect(current_url());
				}
			}

			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
			//$content['getstaffPnndetails'] = $this->model->getstaffPnndetails();
			$content['title'] = 'Probation_personnel_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}

	


	public function edit($token)
	{
		// start permission 
		try{
			
			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($getSupervisordetals);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
		// print_r($getstaffprobationdetail);

			$probationid = $getstaffprobationdetail->id;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
		   // echo "<pre>";
		  	// print_r($this->input->post()); die;

				$this->db->trans_start();
				$extended_date = $this->input->post('extended_date');
				$extended_date_db = $this->gmodel->changedatedbformate($extended_date);

				$insertArr = array(
					'transid'                     => $token,
					'staffid'                  => $this->input->post('staffid'),
					'satisfactory'             => $this->input->post('satisfactory'),
					'probation_completed'       => $this->input->post('completed'),
					'reasons_for_not_above_recommendations'    => $this->input->post('not_recommended'),
					'probation_extension_date'  =>  $extended_date_db,

					'work_habits_and_attitudes'             => $this->input->post('work_habits_and_attitudes'),
					'conduct_and_social_maturity'             => $this->input->post('conduct_and_social_maturity'),
					'any_other_observations'             => $this->input->post('any_other_observations'),
					'integrity'                        => $this->input->post('questionable'),
					'flag'				      => 4,
					'updatedby'              => $this->loginData->staffid,
					'updatedon'              => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				// echo $this->db->last_query(); die;

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $token,
					'sender'               => $this->loginData->staffid,
					'receiver'             => 11,
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => 4,
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);

				$this->db->trans_complete();

				if ($this->db->trans_status() === true){


					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_personnel_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been sent to ED successfully !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			//$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_personnel_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}



	public function ededit($token)
	{
		// start permission 
		try{

			$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
			$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

			$content['getSupervisordetals'] = $this->model->getSupervisor();
		//print_r($getSupervisordetals);

			$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
		// print_r($getstaffprobationdetail);

			$probationid = $getstaffprobationdetail->id;
			$staffid = $getstaffprobationdetail->staffid;

			$content['getstaffprobationreview'] = $getstaffprobationdetail;

			$RequestMethod = $this->input->server('REQUEST_METHOD');

			if($RequestMethod == 'POST'){
		  // echo "<pre>";
		  //	print_r($this->input->post()); die;

				$this->db->trans_start();
				$ed_date = $this->input->post('ed_date');
				$ed_date_db = $this->gmodel->changedatedbformate($ed_date);

				$insertArr = array(
					
					'ed_comments'          => $this->input->post('ed_comments'),
					'ed_date'              => $ed_date_db,
					'flag'				   => $this->input->post('edstatus'),
					'updatedby'            => $this->loginData->staffid,
					'updatedon'            => date('Y-m-d H:i:s'),
				);

				$this->db->where('id',$probationid);
				$this->db->update('tbl_probation_review_performance', $insertArr);

				$insertworkflowArr = array(
					'r_id'                 => $token,
					'type'                 => 7,
					'staffid'              => $staffid,
					'sender'               => $this->loginData->staffid,
					'receiver'             => $this->input->post('review_by_id'),
					'senddate'             => date("Y-m-d H:i:s"),
					'flag'                 => $this->input->post('edstatus'),
					'createdon'            => date("Y-m-d H:i:s"),
					'createdby'            => $this->loginData->staffid,
				);

				$this->db->insert('tbl_workflowdetail', $insertworkflowArr);


				$this->db->trans_complete();

				if ($this->db->trans_status() === true){
					$this->session->set_flashdata('tr_msg', 'Review performance has been sent to ED successfully !!!');
					redirect('/Probation_personnel_reviewofperformance/view/'.$token);
				}else{
					$this->session->set_flashdata('er_msg', 'Error !!! Review performance  !!!');
					redirect(current_url());
				}

			}


			$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();

			$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals();

			$content['title'] = 'Probation_personnel_reviewofperformance';
			$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
			$this->load->view('_main_layout', $content);
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}


	public function view($token)
	{


		// start permission 
		try{

			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Staff_personnel_probation/index');

			} else {


				$query = "SELECT * FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
		// end permission    

				$content['id'] = $this->model->getEDid();
			// print_r($content['id']); die;
				$var = $content['id']->edid; 
			// echo $var; die;


				$content['edsignature'] = $this->Common_Model->staff_signature($var);

				$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);



			//$img = $content['getstaffpersonneldetals']->staffid;
				$content['getSupervisordetals'] = $this->model->getSupervisor($content['getstaffpersonneldetals']->staffid);




				$content['getstaffprovationreviewperformance'] = $this->Probation_personnel_reviewofperformance_model->getstaffid($token);
				$dateofjoin = $content['getstaffprovationreviewperformance']->doj;

				$Empnumber =$content['getstaffprovationreviewperformance']->emp_code;


				$designationid =  $content['getstaffprovationreviewperformance']->desid;

				$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
				// echo "<pre>";
				// print_r($getstaffprobationdetail); die;
				// $getstaffprobationdetail = $this->model->getstaffprobationdetals($token);

				
				$content['getstaffPdetails'] = $this->model->getStaffPersonnalList();

				$probationid = $getstaffprobationdetail->id;
				$staffid = $getstaffprobationdetail->staffid;
			// echo $staffid; die;
				$extendeddate = $getstaffprobationdetail->probation_extension_date;
			// echo $extendeddate; die;
				$content['workflowdetail'] = $this->model->get_providentworkflowid($staffid);
				// print_r($content['workflowdetail']); die;
				$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();


				$content['getstaffprobationreview'] = $getstaffprobationdetail;
				

				// print_r($content['getstaffprobationreview']); die;
				$RequestMethod = $this->input->server('REQUEST_METHOD');

				if($RequestMethod == 'POST'){
   //  echo "<pre>";
  	// print_r($this->input->post()); die;



					$this->db->trans_start();

					$insertArr = array(


						'trans_flag'		   => 7,
						'updatedby'            => $this->loginData->staffid,
						'updatedon'            => date('Y-m-d H:i:s'),
					);
					// print_r($insertArr); die;
					$this->db->where('id',$token);
					$this->db->update('staff_transaction', $insertArr);
				// echo $this->db->last_query(); die;


					$insertdata = array(

						'probation_extension_date'  => $extendeddate,
					);
				// print_r($insertdata); die;
					$this->db->where('staffid',$staffid);
					$this->db->update('staff',$insertdata);

					$insertworkflowArr = array(
						'r_id'                 => (trim($token)==""?null:($token)),
						'type'                 => 7,
						'staffid'              => $staffid,
						'sender'               => $this->loginData->staffid,
						'receiver'             =>  $content['getpersonnaldetals']->personnalstaffid,
						'forwarded_workflowid' => (trim($content['workflowdetail']->workflowid)==""?null:($content['workflowdetail']->workflowid)),
						'senddate'             => date("Y-m-d H:i:s"),
						'flag'                 => 7,
						'createdon'            => date("Y-m-d H:i:s"),
						'createdby'            => $this->loginData->staffid,
					);
				// echo "<pre>";
				// print_r($insertworkflowArr); die;
					$this->db->insert('tbl_workflowdetail', $insertworkflowArr);



					// if ($getstaffprobationdetail->probation_completed =='yes') {

					// 	@ $currentyear = $this->gmodel->getcurrentfyear();


					// 	$sql = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms <= '".date('Y-m-d')."' Order by id DESC LIMIT 1 "; 
					// 	// echo $sql; die;
					// 	$res = $this->db->query($sql)->row();
					// 	// print_r($res); die;
					// 	if(count($res) > 0){  
					// 		$Accuredvalue = $res->credit_value;
					// 		$From_date = $res->monthfroms;
					// 		$TO_date = $res->monthtos;

					// 		$totalcrsql = "select SUM(credit_value) as credit_value from msleavecrperiod where CYear='".$currentyear."'"; 
					// 		// echo $totalcrsql; die;
					// 		$totalcrres = $this->db->query($totalcrsql)->row();
					// 		$perdayleave =  $totalcrres->credit_value/365;
					// 		$datesqldiff = date_diff($dateofjoin, $TO_date);
					// 		$Noofdays=0;
					// 		$Noofdays = $this->gmodel->roundvalue($datesqldiff*$perdayleave);

					// 		$sql1 = "select * from msleavecrperiod where CYear='".$currentyear."' AND status=1 and  monthfroms = '".$From_date."'  "; 
					// 		// echo $sql1; die;
					// 		$res1 = $this->db->query($sql1)->row();

					// 		$insertarray = array (

					// 			'Emp_code'              => $Empnumber,
					// 			'From_date'             => $From_date,
					// 			'To_date'               => $TO_date,
					// 			'Leave_type'            => 0,
					// 			'Leave_transactiontype' => 'CR',
					// 			'Description'           => 'Credit leaves posting',
					// 			'Noofdays'              => $Noofdays,
					// 			'appliedon'             => date('Y-m-d')
					// 		);
					// 		print_r($insertarray); die;
					// 		$this->db->insert('trnleave_ledger', $insertarray);

					// 	}

					// }


					$this->db->trans_complete();

					if ($this->db->trans_status() === true){


						$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
						redirect('/Probation_personnel_reviewofperformance/fullview/'.$token);	
					}else{
						$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
						redirect(current_url());
					}

				}
			// $content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
			// print_r($content['getstaffprovationreviewperformance']); die;

				$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);



			//$img = $content['getstaffpersonneldetals']->staffid;
				



				$content['title'] = 'Probation_personnel_reviewofperformance';
				$content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
				$this->load->view('_main_layout', $content);
			}
		}
		catch(Exception $e)
		{
			print_r($e->getMessage());
			die();
		}
		
	}





	public function fullview($token)
	{
		// start permission 
		
		try{
			
			if (empty($token) || $token == '' ) {
				$this->session->set_flashdata('er_msg', 'Required parameter $token is either blank or empty.');
				redirect('/Staff_personnel_probation/index');
				
			} else {


				$staffid = '';
				$effectdate ='';
				$query = "SELECT DISTINCT(Controller),Action FROM role_permissions a LEFT JOIN sysaccesslevel b on a.RoleID=b.Acclevel_Cd WHERE a.RoleID = ".$this->loginData->RoleID." ";
				$content['role_permission'] = $this->db->query($query)->result();
		// end permission    



				$content['getstaffprovationreviewperformance'] = $this->model->getStaffProbationReviewofPerformance($token);
				// echo "<pre>";
				// print_r($content['getstaffprovationreviewperformance']);
				// die;

				
				$staffid = $content['getstaffprovationreviewperformance']->staffid;
				// print_r($staffid);
				// die;


				$content['getpersonnaldetals'] = $this->model->getstaffpersonnaldetails();
				// print_r($content['getpersonnaldetals']);
				// die;

				$content['getstaffpersonneldetals'] = $this->model->getstaffpeersonnelprobationdetals($token);

				$img = $content['getstaffpersonneldetals']->staffid;

				$content['staffsignature'] = $this->Common_Model->staff_signature($img);


				$content['id'] = $this->model->getEDid();
				$var = $content['id']->edid; 
				$edname = $content['id']->UserFirstName.' '.$content['id']->UserMiddleName.' '.$content['id']->UserLastName;


				$content['edsignature'] = $this->Common_Model->staff_signature($var);
				
				

				$content['getSupervisordetals'] = $this->model->getSupervisor($var);

				$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);



				$probationid = $getstaffprobationdetail->id;
				$staffid = $getstaffprobationdetail->staffid;

				$content['getstaffprobationreview'] = $getstaffprobationdetail; 

				$workflowdetail = $this->model->get_providentworkflowid($staffid);
				
				$RequestMethod = $this->input->server('REQUEST_METHOD');

				if($RequestMethod == 'POST'){


					$getstaffprobationdetail = $this->model->getstaffprobationdetals($token);
					

						//print_r($content['getstaffprovationreviewperformance']);
						//die;
					//print_r($getstaffprobationdetail);
					$date_of_appointment = strtotime($getstaffprobationdetail->date_of_appointment);


					$probation_extension_date = strtotime($getstaffprobationdetail->probation_extension_date);
			// $months='';

			 	 //  echo "date_of_appointment=".$getstaffprobationdetail->date_of_appointment;
			 	 // echo "extendeddate=".$getstaffprobationdetail->probation_extension_date."<br><br><br>";



					$diff = abs($probation_extension_date - $date_of_appointment);




					$years = floor($diff / (365*60*60*24));
			// echo $years;

					$months='';

					$months = floor(($diff - $years * 365*60*60*24) 
						/ (30*60*60*24));  



					$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));



					$effectdate1 = '';
					$effectdate = '';
					$effectdate1 = $this->input->post('effective_date');

					$effectdate = $this->gmodel->changedatedbformate($effectdate1);
					$prob_name = '';
					$emp_code = '';
					$desname = '';
					$officename = '';
					$staff_name = '';
					$probation_extension_date='';
						//$staff_desname = '';
					
					$d_o_j = '';

						// echo $effectdate1;
						// die;

					$getstaffprovationreviewperformance_name = $content['getstaffprovationreviewperformance']->name;
					$getstaffprovationreviewperformance_emp_code = $content['getstaffprovationreviewperformance']->emp_code;
					$getstaffprovationreviewperformance_desname = $content['getstaffprovationreviewperformance']->desname;
					$getstaffprovationreviewperformance_officename = $content['getstaffprovationreviewperformance']->officename;

					$d_o_j = date('d/m/Y');
					$date_of_appointment=$this->gmodel->changedatedbformate($getstaffprobationdetail->date_of_appointment);
					$appointment_no=$content['getstaffprovationreviewperformance']->offerno;
					$probation_extension_date=$getstaffprobationdetail->probation_extension_date;

					$staff = array('$d_o_j','$effectdate1','$prob_name','$emp_code','$desname','$officename','$staff_name','$staff_desname','$edname','$date_of_appointment','$appointment_no','$probation_extension_date','$months');
					$staff_replace = array($d_o_j,$effectdate1,$getstaffprovationreviewperformance_name,$getstaffprovationreviewperformance_emp_code,$getstaffprovationreviewperformance_desname,$getstaffprovationreviewperformance_officename,$getstaffprovationreviewperformance_name,$getstaffprovationreviewperformance_desname,$edname,$date_of_appointment,$appointment_no,$getstaffprobationdetail->probation_extension_date,$months); 
					// print_r($staff_replace);
 				// 	echo ""
					// $s = array_combine($staff_replace,$staff); 
					// print_r($s); die();
					//print_r($staff_replace);


					

					$uparray = array (

						'effective_date' => $this->gmodel->changedatedbformate($effectdate),
					);
					$this->db->where('id',$token);
					$this->db->update('staff_transaction', $uparray);

					$this->db->trans_start();

					if ($getstaffprobationdetail->probation_completed =='yes') {

						$insertstaffArr = array(
							'probation_completed'	=> 1,
							'updatedby'            => $this->loginData->staffid,
							'updatedon'            => date('Y-m-d H:i:s'),
						);

						$this->db->where('staffid',$staffid);
						$this->db->update('staff', $insertstaffArr);
					}

					if($getstaffprobationdetail->probation_completed =='no' && $getstaffprobationdetail->probation_extension_date != NULL) {


						$insertstaffArr = array(
							'probation_status'	   => 0,
							'updatedby'            => $this->loginData->staffid,
							'updatedon'            => date('Y-m-d H:i:s'),
						);


						$this->db->where('staffid',$staffid);
						$this->db->update('staff', $insertstaffArr);
					}



					if($getstaffprobationdetail->probation_completed=='no' && $getstaffprobationdetail->probation_extension_date == NULL ){

						$insertstaffArr = array(
							'probation_status'  => 1,
							'separationtype'    => 'Termination',
							'dateofleaving'     => $this->gmodel->changedatedbformate($effectdate),
							'separationremarks' => $content['getstaffprobationreview']->ed_comments,
							'status'            => 0,
							'updatedby'         => $this->loginData->staffid,
							'updatedon'         => date('Y-m-d H:i:s'),
						);

						$this->db->where('staffid',$staffid);
						$this->db->update('staff', $insertstaffArr);

					}

					

					if($getstaffprobationdetail->satisfactory =='satisfactory' && $getstaffprobationdetail->probation_extension_date == NULL )
					{
						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 51 AND `isactive` = '1'";
						$data = $this->db->query($sql)->row();
						if(!empty($data))
							$html = str_replace($staff,$staff_replace ,$data->lettercontent); 
					}

					if($getstaffprobationdetail->satisfactory =='notsatisfactory' && $probation_extension_date != NULL)
					{	

						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 53 AND `isactive` = '1'";
						$data = $this->db->query($sql)->row();	
						
						if(!empty($data))
							$html = str_replace($staff,$staff_replace , $data->lettercontent);
								
					}		 

					if($getstaffprobationdetail->probation_completed=='not_recommended' && $getstaffprobationdetail->satisfactory =='notsatisfactory')
					{		 	
						

						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 55 AND `isactive` = '1'";
						$data = $this->db->query($sql)->row();

						$html = $data->lettercontent; 
						if(!empty($data))
							$html = str_replace($staff,$staff_replace ,$data->lettercontent); 
								// echo $html;
								// die;


					}
					if($getstaffprobationdetail->satisfactory =='notsatisfactory' && $probation_extension_date != NULL && $getstaffprobationdetail->probation_completed=='not_recommended')
					{

						$sql = "SELECT lettercontent FROM `tbl_letter_master` Where processid = 54 AND `isactive` = '1'";
						$data = $this->db->query($sql)->row();
						if(!empty($data))
							$html = str_replace($staff,$staff_replace , $data->lettercontent); 	
					}
					$filename = $token.'-'.md5(time() . rand(1,1000));

					$attachments = array($filename.'.pdf');
					$this->load->model('Dompdf_model');
					$generate =   $this->Dompdf_model->probation_letter_PDF($html, $filename, NULL,'filename.pdf');
					if($generate==true){
						
						$insertArr = array(
							'flag'				   => 7,
							'filename'             => (trim($filename)==""?null:($filename)), 
							'updatedby'            => $this->loginData->staffid,
							'updatedon'            => date('Y-m-d H:i:s'),
						);

						$this->db->where('id',$probationid);
						$this->db->update('tbl_probation_review_performance', $insertArr);

						$insertArr = array(


							'trans_flag'		   => 9,
							'updatedby'            => $this->loginData->staffid,
							'updatedon'            => date('Y-m-d H:i:s'),
						);
					// print_r($insertArr); die;
						$this->db->where('id',$token);
						$this->db->update('staff_transaction', $insertArr);

						$insertworkflowArr = array(
							'r_id'                 => (trim($token)==""?null:($token)),
							'type'                 => 7,
							'staffid'              => $staffid,
							'sender'               => $this->loginData->staffid,
							'receiver'             => (trim($content['getpersonnaldetals']->personnalstaffid)==""?null:($content['getpersonnaldetals']->personnalstaffid)),
							'forwarded_workflowid' => (trim($workflowdetail->workflowid)==""?null:($workflowdetail->workflowid)),
							'senddate'             => date("Y-m-d H:i:s"),
							'flag'                 => 9,
							'createdon'            => date("Y-m-d H:i:s"),
							'createdby'            => $this->loginData->staffid,
						);
			// print_r($insertworkflowArr); die;
					 $this->db->insert('tbl_workflowdetail', $insertworkflowArr);

						



                 $hrdemail       = $this->loginData->EmailID; /////// hrd email id/////
                 $staffemail     = $content['getstaffprovationreviewperformance']->staff_email;
                 $tc_email       = $content['getstaffpersonneldetals']->tcmail;
                 $personal       = $content['getpersonnaldetals']->personel_email;

			// echo $newpassword; die;





			// print_r($attachments);  die;
                 $subject='';

                 if($getstaffprobationdetail->satisfactory =='notsatisfactory' && $getstaffprobationdetail->probation_extension_date != null) {

                 	$subject = 'Your Probation date are extended by pardan';
                 }elseif ($getstaffprobationdetail->probation_completed =='yes') {

                 	$subject = 'Your Probation is completed';
                 }
                 elseif($getstaffprobationdetail->satisfactory =='notsatisfactory' && $getstaffprobationdetail->probation_completed=='not_recommended')
						{
						$subject = 'Termination of Service Due to
						Unsatisfactory Performance During the  Period of Probation';
						}
						if($getstaffprobationdetail->satisfactory =='notsatisfactory' && $probation_extension_date != NULL && $getstaffprobationdetail->probation_completed=='not_recommended')
						{
							$subject = 'Termination of Service Due to
							Unsatisfactory Performance During the extended Period of Probation';
						}
					
				//$html=$subject;	
				// echo $subject;
				// die;	
                 $html = '<b>'.$subject.'</b><br><br><br>Dear '.$content['getstaffprovationreviewperformance']->name.', <br><br> 
                 Employee Code: '.$content['getstaffprovationreviewperformance']->emp_code.'<br><br>
                 Designation:  '.$content['getstaffprovationreviewperformance']->desname.'<br><br>
                 Location:  '.$content['getstaffprovationreviewperformance']->officename.'<br><br>

                 

                 <br>
                 <br><br>
                 Regard,<br>Pradan Team';
                 


                 $arrmail  = array(
                 	
                 	$tc_email = 'tc',
                 );
                 $this->Common_Model->send_email($subject , $message = $html, $staffemail, $hrdemail, $arrmail, $attachments); 

			 //// Send Mai
                 $this->db->trans_complete();


                 if ($this->db->trans_status() === true){

                 	$this->session->set_flashdata('tr_msg', 'Review performance has been initiate successfully !!!');
                 	redirect('/Probation_personnel_reviewofperformance/fullview/'.$token);	
                 }
                 else{
                 	$this->session->set_flashdata('er_msg', 'Error !!! Review performance has been initiate successfully !!!');
                 	redirect(current_url());
                 }

             }






         }



			//print_r($content['getstaffpersonneldetals']);

         $content['workflowdetail'] = $this->model->get_providentworkflowid($staffid);
         $content['token'] = $token;
         $content['title'] = 'Probation_personnel_reviewofperformance';
         $content['subview'] = __CLASS__ . DIRECTORY_SEPARATOR . __FUNCTION__;
         $this->load->view('_main_layout', $content);
     }
 }
 catch(Exception $e)
 {
 	print_r($e->getMessage());
 	die();
 }

}




}