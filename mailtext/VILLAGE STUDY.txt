Dear Colleagues,
Trust this email finds you well. The Apprentices in you teams would be on the verge of completing their Village Stay and writing/sharing their reflective reports. Please do share their reports with us.
Based on our discussions I am sharing  the purpose, structure and some guidelines for the Village study component.
Purpose: The central idea is to help participants become conscious of the multi-dimensionality of the village� interplay of different actors and factors, community and families they would be working with, and to understand these better.
Village study in PRADAN�s context is not a �study� on a subject (e.g. village or villagers) done by an outsider (e.g. a development worker or a researcher). It is conceptualized as a co-creation/ co-production of the picture of the village and its people by both the Apprentice and the villagers.
Unlike the conventional studies, here the village is not considered as a �field� for study, it is perceived as �My village� by the Apprentice. And in this concept of �Village Study� the villagers are not the data providers; rather they become the co-producers of the study. They participate in the data generation as well as analysis of the data to arrive at a deeper understanding of �why the things are the way they are�. In that sense, village study becomes a process through which all the participants�the villagers and the Apprentice start a journey towards a shared understanding of the situation. 
Structure: The Apprentice carries out the village study in the same village as during her/his village stay. During her/his village stay or early part of the study the Apprentice identifies certain striking aspects for further exploration. Village study provides opportunity for that.
During the village study the Apprentices, observes the village life (for e.g. events, practices, activities, interactions, groups, transactions, work, rituals, meetings, gatherings, institutions), interacts with the villagers, jointly gathers information and experiences around these aspects, analyses them and builds a shared understanding.
It is expected that the Apprentice continue her/his engagement in this village during the �Learning by Doing� phase and facilitate collective actions around those issues.
Report: S/he writes a report titled �My Village �..� which includes brief outline of the village, striking issues, the process of joint exploration, and the shared understanding of the issue. As the village study is the first step towards transformative engagement, the content and the process, both are important.
Duration: It takes about 4-6 weeks to complete this portion.
Village Study Orientation:  Village Study orientation needs to be conducted at DC level to help the Apprentice carry out the Village Study. 
I understand you all have started planning for the Village Study orientation's next week onwards. Please do share the design of the orientation with us too.
Feel free to contact me for any clarification.

Regards,
Barsha Mishra
Team Coordinator
HRD Unit
