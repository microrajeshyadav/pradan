Dear All,
 As you all know that the $batch_no. batch of Apprentices will complete their Immersion Phase by the end of $3rd_month. This is regarding their Home Visit and Central Event which is scheduled during $scheduled_month. Kindly note the dates:
 
Home Visit: 4 days $excluding_travel_time during $dates
Central Event at Kesla: $dates
  
The Apprentices need to reach Kesla for Central Event by the evening of $onedaybeforebeginning_date and will be able to leave the campus only after 5 pm on $ending_date.
 
Kindly go through the attached IOM for detailed information and inform the Apprentices regarding the same.

Please contact for further clarification.
 
Regards,
$supervisior_name
Team Coordinator
$hr_name
HRD Unit
