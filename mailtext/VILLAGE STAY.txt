Dear Colleagues,
The primary purpose of Village Stay is to help the Apprentice get exposed to rural realities and be in touch with her/his experience. To facilitate such a process an Apprentice might have to interact with few families or individuals, establish a human connection with them, understand the life situation and reflect on how it affected her/him. Also, she might have to observe different situations in the village and reflect on his/her experience.
To structure some aspect of Apprentices work the following assignments would be helpful.
Interaction with five families or individuals including few vulnerable families or individuals.
Observe the different situations in the village.
As interacting with individuals/ families, establishing human connection with community is new for the Apprentices some support from the Field Guide is required. It is proposed that the Apprentice and Field Guide interact with one family or individual and understand their life situation. After the interaction the Field Guide shares what s/he experienced after the interaction and asks the Apprentice to share too.
Based on the above the first report to be shared by the Apprentice should include
A brief description of the village.
Description of the families or individuals s/he has interacted.
Impact of the observations and interactions on the Apprentice.
Overall reflections.
Do let me know if you have any thoughts/ inputs on this.

Regards,
Barsha Mishra
Team Coordinator
HRD Unit
