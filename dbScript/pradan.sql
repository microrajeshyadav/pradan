-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2018 at 08:07 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pradan_hr_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `dc_team_mapping`
--

CREATE TABLE `dc_team_mapping` (
  `dc_cd` int(3) NOT NULL DEFAULT '0',
  `teamid` int(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `districtid` varchar(8) NOT NULL DEFAULT '',
  `name` varchar(60) DEFAULT NULL,
  `stateid` char(2) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `edcomments`
--

CREATE TABLE `edcomments` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updateon` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT NULL,
  `flag` varchar(3) DEFAULT NULL,
  `send_hrd_mail_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `genratepdflist`
--

CREATE TABLE `genratepdflist` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `import`
--

CREATE TABLE `import` (
  `id` int(11) NOT NULL COMMENT 'Primary Key',
  `first_name` varchar(100) NOT NULL COMMENT 'First Name',
  `last_name` varchar(100) NOT NULL COMMENT 'Last Name',
  `email` varchar(255) NOT NULL COMMENT 'Email Address',
  `dob` varchar(20) NOT NULL COMMENT 'Date of Birth',
  `contact_no` double NOT NULL COMMENT 'Contact No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='datatable demo table';

-- --------------------------------------------------------

--
-- Table structure for table `lpooffice`
--

CREATE TABLE `lpooffice` (
  `lpo` varchar(60) DEFAULT NULL,
  `officeid` int(5) NOT NULL DEFAULT '0',
  `officetype` varchar(60) DEFAULT NULL,
  `officename` varchar(60) DEFAULT NULL,
  `street` varchar(80) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `district` varchar(6) DEFAULT NULL,
  `staffid` int(5) DEFAULT NULL,
  `phone1` varchar(20) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `date_established` date DEFAULT NULL,
  `closed` enum('Yes','No') DEFAULT 'No',
  `close_date` date DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapping_campus_recruiters`
--

CREATE TABLE `mapping_campus_recruiters` (
  `id_mapping_campus_recruiter` int(11) NOT NULL,
  `campusid` int(11) NOT NULL,
  `recruiterid` int(11) NOT NULL,
  `anchor` int(11) DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `campusintimationid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapping_campus_recruiters_head`
--

CREATE TABLE `mapping_campus_recruiters_head` (
  `id` int(11) NOT NULL,
  `campusid` int(11) NOT NULL,
  `recruiterheadid` int(11) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mapping_staff_recruitment`
--

CREATE TABLE `mapping_staff_recruitment` (
  `id_mapping_staff_recruitment` int(11) NOT NULL,
  `recruitmentteamid` int(11) NOT NULL,
  `year` varchar(10) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msdc_details`
--

CREATE TABLE `msdc_details` (
  `dc_cd` int(3) UNSIGNED NOT NULL DEFAULT '0',
  `dc_name` varchar(50) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msdesignation`
--

CREATE TABLE `msdesignation` (
  `desid` int(3) NOT NULL,
  `desname` varchar(100) NOT NULL,
  `level` char(1) NOT NULL,
  `shortname` varchar(10) NOT NULL DEFAULT '',
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mskeyrules`
--

CREATE TABLE `mskeyrules` (
  `Maxcf_yearleave` int(3) NOT NULL DEFAULT '0',
  `maxleaveaccrued` int(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msleavecrperiod`
--

CREATE TABLE `msleavecrperiod` (
  `from_month` varchar(10) NOT NULL DEFAULT '',
  `to_month` varchar(10) NOT NULL DEFAULT '',
  `credit_value` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msleavetype`
--

CREATE TABLE `msleavetype` (
  `Ltypeid` tinyint(3) DEFAULT '0',
  `Ltypename` varchar(30) DEFAULT NULL,
  `deduct` varchar(5) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msleave_monthly_freezing`
--

CREATE TABLE `msleave_monthly_freezing` (
  `teamid` int(2) NOT NULL,
  `month` varchar(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msstaffdetails`
--

CREATE TABLE `msstaffdetails` (
  `staffid` int(10) NOT NULL,
  `father_name` varchar(30) NOT NULL,
  `dob` date DEFAULT NULL,
  `marital_status` varchar(10) DEFAULT NULL,
  `hometown` varchar(50) DEFAULT NULL,
  `contract_start` date DEFAULT NULL,
  `contract_end` date DEFAULT NULL,
  `informaton` text,
  `noofchilds` int(2) DEFAULT NULL,
  `basicpay` varchar(40) DEFAULT NULL,
  `scaleofpay` varchar(40) DEFAULT NULL,
  `maritalreason` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msstaffeducation_details`
--

CREATE TABLE `msstaffeducation_details` (
  `id` int(11) NOT NULL,
  `staffid` int(10) NOT NULL,
  `edulevel_cd` int(2) NOT NULL,
  `collg_name` varchar(150) NOT NULL,
  `university` varchar(150) NOT NULL,
  `year_passing` int(4) NOT NULL,
  `division` varchar(10) NOT NULL,
  `percentage` varchar(6) NOT NULL,
  `stream` varchar(30) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msstaffexp_details`
--

CREATE TABLE `msstaffexp_details` (
  `id` int(11) NOT NULL,
  `staffid` varchar(10) NOT NULL,
  `organization` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `lastsalary` varchar(30) NOT NULL,
  `reason_leaving` text NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `designation` varchar(50) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msstafftraining_programs_attend`
--

CREATE TABLE `msstafftraining_programs_attend` (
  `id` int(11) NOT NULL,
  `staffid` int(10) NOT NULL,
  `Training_program` text NOT NULL,
  `organizing_agency` varchar(150) DEFAULT NULL,
  `fromdate` date DEFAULT NULL,
  `todate` date DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msstaff_transfer_request`
--

CREATE TABLE `msstaff_transfer_request` (
  `requestid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `newofficeid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `supervisorid` int(11) NOT NULL,
  `remarks` varchar(150) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstaccesspoints`
--

CREATE TABLE `mstaccesspoints` (
  `ID` int(11) NOT NULL,
  `Client_Id` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `Controller` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Action` varchar(50) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `mstbatch`
--

CREATE TABLE `mstbatch` (
  `id` int(11) NOT NULL,
  `batch` varchar(10) NOT NULL,
  `dateofjoining` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `financial_year` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstcampus`
--

CREATE TABLE `mstcampus` (
  `campusid` int(11) NOT NULL,
  `campusincharge` varchar(50) NOT NULL,
  `campusname` varchar(150) NOT NULL,
  `emailid` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `stateid` varchar(2) NOT NULL,
  `address` varchar(150) NOT NULL,
  `fax` varchar(12) NOT NULL,
  `mobile2` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstcategory`
--

CREATE TABLE `mstcategory` (
  `id` int(11) NOT NULL,
  `categoryname` varchar(50) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstdocuments`
--

CREATE TABLE `mstdocuments` (
  `id` int(11) NOT NULL,
  `document_name` varchar(50) NOT NULL,
  `document_type` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstevents`
--

CREATE TABLE `mstevents` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstfinancialyear`
--

CREATE TABLE `mstfinancialyear` (
  `id` int(11) NOT NULL,
  `financialyear` varchar(10) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstpgeducation`
--

CREATE TABLE `mstpgeducation` (
  `id` int(11) NOT NULL,
  `pgname` varchar(50) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstphase`
--

CREATE TABLE `mstphase` (
  `id` int(11) NOT NULL,
  `phase_name` varchar(50) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstrecruiters`
--

CREATE TABLE `mstrecruiters` (
  `recruiterid` int(11) NOT NULL,
  `campusid` int(11) NOT NULL,
  `dateofselecionprocess` date NOT NULL,
  `recruitersname1` int(11) NOT NULL,
  `recruitersname2` int(11) NOT NULL,
  `recruitersname3` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstugeducation`
--

CREATE TABLE `mstugeducation` (
  `id` int(11) NOT NULL,
  `ugname` varchar(50) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstuser`
--

CREATE TABLE `mstuser` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `UserFirstName` varchar(50) CHARACTER SET latin1 NOT NULL,
  `UserMiddleName` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `UserLastName` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `PhoneNumber` varchar(12) CHARACTER SET latin1 DEFAULT NULL,
  `EmailID` varchar(80) CHARACTER SET latin1 DEFAULT NULL,
  `RoleID` int(11) NOT NULL,
  `CreatedOn` date NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedOn` date DEFAULT NULL,
  `UpdatedBy` int(11) DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `Candidateid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mstuser_photo`
--

CREATE TABLE `mstuser_photo` (
  `m_id` int(11) NOT NULL,
  `File_Name` varchar(200) NOT NULL,
  `Encrypted_File_Name` varchar(200) NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_extension_sepration_type`
--

CREATE TABLE `mst_extension_sepration_type` (
  `id` int(11) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Isdeleted` tinyint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_intimation`
--

CREATE TABLE `mst_intimation` (
  `id` int(11) NOT NULL,
  `intimation` varchar(30) NOT NULL,
  `Isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_oprationtype`
--

CREATE TABLE `mst_oprationtype` (
  `id` int(11) NOT NULL,
  `shortname` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_reason`
--

CREATE TABLE `mst_reason` (
  `ID` int(11) NOT NULL,
  `Reason_name` varchar(200) DEFAULT NULL,
  `isdeleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_required_document`
--

CREATE TABLE `mst_required_document` (
  `id` int(11) NOT NULL,
  `document_name` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_resource_person`
--

CREATE TABLE `mst_resource_person` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_staff_category`
--

CREATE TABLE `mst_staff_category` (
  `id` int(11) NOT NULL,
  `categoryname` varchar(50) NOT NULL,
  `shortname` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `Isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `ID` int(11) NOT NULL,
  `RoleID` int(11) DEFAULT NULL,
  `Controller` varchar(100) DEFAULT NULL,
  `Action` varchar(100) NOT NULL,
  `menuorder` int(11) NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `Is_Home` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salaryheads`
--

CREATE TABLE `salaryheads` (
  `fieldname` int(20) NOT NULL,
  `fielddesc` varchar(50) NOT NULL,
  `abbreviation` varchar(6) NOT NULL,
  `formulacolumn` tinyint(1) NOT NULL,
  `lookuphead` varchar(200) DEFAULT NULL,
  `roundingupto` int(11) DEFAULT NULL,
  `attendancedep` tinyint(1) DEFAULT NULL,
  `roundtohigher` tinyint(1) DEFAULT NULL,
  `monthlyinput` tinyint(1) DEFAULT NULL,
  `seqno` int(11) NOT NULL,
  `lookupheadname` varchar(200) DEFAULT NULL,
  `activefield` tinyint(1) DEFAULT NULL,
  `specialfield` tinyint(1) DEFAULT NULL,
  `specialfieldmaster` tinyint(1) DEFAULT NULL,
  `frommaster` tinyint(1) DEFAULT NULL,
  `loanhead` tinyint(1) DEFAULT NULL,
  `conditional` tinyint(1) DEFAULT NULL,
  `mT` tinyint(1) DEFAULT NULL,
  `c` tinyint(1) DEFAULT NULL,
  `dw` tinyint(1) DEFAULT NULL,
  `a` tinyint(1) DEFAULT NULL,
  `ct` tinyint(1) DEFAULT NULL,
  `dc` tinyint(4) DEFAULT NULL,
  `cw` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salaryheadshistory`
--

CREATE TABLE `salaryheadshistory` (
  `fieldname` varchar(20) NOT NULL,
  `fielddesc` varchar(50) DEFAULT NULL,
  `abbreviation` varchar(6) DEFAULT NULL,
  `formulacolumn` tinyint(1) DEFAULT NULL,
  `lookuphead` varchar(200) DEFAULT NULL,
  `roundingupto` int(10) DEFAULT NULL,
  `attendancedep` tinyint(1) DEFAULT NULL,
  `roundtohigher` tinyint(1) DEFAULT NULL,
  `monthlyinput` tinyint(1) DEFAULT NULL,
  `seqno` int(10) NOT NULL,
  `lookupheadname` varchar(200) DEFAULT NULL,
  `activefield` tinyint(1) DEFAULT NULL,
  `specialfield` tinyint(1) DEFAULT NULL,
  `specialfieldmaster` tinyint(1) DEFAULT NULL,
  `frommaster` tinyint(1) DEFAULT NULL,
  `loanhead` tinyint(1) DEFAULT NULL,
  `conditional` tinyint(1) DEFAULT NULL,
  `mt` tinyint(1) DEFAULT NULL,
  `c` tinyint(1) DEFAULT NULL,
  `dw` tinyint(1) DEFAULT NULL,
  `a` tinyint(1) DEFAULT NULL,
  `ct` tinyint(1) DEFAULT NULL,
  `dc` tinyint(1) DEFAULT NULL,
  `cw` tinyint(1) DEFAULT NULL,
  `period` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `company_address` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `company_phone` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `company_mobile` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `customer_care_no` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `customer_care_email` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `facebook_link` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `twitter_link` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `google_plus_link` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `youtube_link` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `printrest_link` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `LinkedIn_link` varchar(250) DEFAULT NULL,
  `Instagram_link` varchar(250) DEFAULT NULL,
  `sms_username` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `sms_password` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `sms_sende_id` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `email_s_id` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `email_s_password` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `forget_password_setting_template` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `forget_contact_no_setting_template` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `google_analytics` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `google_map` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `google_verification_code` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `live_chat` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `seo_title` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `seo_keyword` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `seo_description` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `tds` float DEFAULT NULL,
  `book_api_offline` tinyint(1) DEFAULT NULL,
  `smtp_hostname` varchar(500) DEFAULT NULL,
  `smtp_port` varchar(50) DEFAULT NULL,
  `smtp_username` varchar(200) DEFAULT NULL,
  `smtp_password` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staffid` int(5) NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL,
  `contact` varchar(40) DEFAULT NULL,
  `emailid` varchar(40) DEFAULT NULL,
  `doj` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `professional` varchar(40) DEFAULT NULL,
  `designation` int(3) NOT NULL,
  `reportingto` int(8) DEFAULT NULL,
  `Prog_id` varchar(10) DEFAULT NULL,
  `doj_team` date DEFAULT NULL,
  `emp_code` varchar(10) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `marital_status` int(1) NOT NULL,
  `hometown` varchar(50) DEFAULT NULL,
  `informaton` text,
  `title` varchar(10) NOT NULL,
  `noofchilds` int(2) DEFAULT NULL,
  `Joining_basicpay` varchar(40) DEFAULT NULL,
  `Joining_scaleofpay` varchar(255) DEFAULT NULL,
  `contactno2` varchar(40) DEFAULT NULL,
  `contactno3` varchar(40) DEFAULT NULL,
  `presenthno` varchar(50) DEFAULT NULL,
  `presentstreet` varchar(255) DEFAULT NULL,
  `presentcity` varchar(100) DEFAULT NULL,
  `presentstateid` int(11) DEFAULT NULL,
  `presentdistrict` varchar(50) DEFAULT NULL,
  `presentpincode` int(6) DEFAULT NULL,
  `permanenthno` varchar(50) DEFAULT NULL,
  `permanentstreet` varchar(255) DEFAULT NULL,
  `permanentcity` varchar(100) DEFAULT NULL,
  `permanentstateid` int(11) DEFAULT NULL,
  `permanentdistrict` varchar(50) DEFAULT NULL,
  `permanentpincode` int(11) DEFAULT NULL,
  `deletedemployee` int(11) NOT NULL,
  `dateofleaving` date DEFAULT NULL,
  `separationtype` varchar(30) DEFAULT NULL,
  `separationremarks` varchar(100) DEFAULT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staffmail`
--

CREATE TABLE `staffmail` (
  `staffid` int(5) DEFAULT NULL,
  `startname` varchar(10) DEFAULT NULL,
  `dearname` varchar(50) DEFAULT NULL,
  `emailid` varchar(100) DEFAULT NULL,
  `address1` varchar(250) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `address3` varchar(250) DEFAULT NULL,
  `contactno2` varchar(40) DEFAULT NULL,
  `contactno3` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_old`
--

CREATE TABLE `staff_old` (
  `staffid` int(5) NOT NULL DEFAULT '0',
  `name` varchar(40) DEFAULT NULL,
  `contact` varchar(40) DEFAULT NULL,
  `emailid` varchar(40) DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `professional` varchar(40) DEFAULT NULL,
  `designation` int(3) DEFAULT NULL,
  `reportingto` int(8) DEFAULT NULL,
  `Prog_id` varchar(10) DEFAULT NULL,
  `doj_team` date DEFAULT NULL,
  `emp_code` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_transaction`
--

CREATE TABLE `staff_transaction` (
  `staffid` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `old_office_id` int(11) DEFAULT NULL,
  `new_office_id` int(11) DEFAULT NULL,
  `date_of_transfer` date NOT NULL DEFAULT '0000-00-00',
  `old_designation` int(11) DEFAULT NULL,
  `new_designation` int(11) DEFAULT NULL,
  `trans_status` varchar(30) DEFAULT NULL,
  `transid` int(8) NOT NULL,
  `datetime` datetime NOT NULL,
  `fgid` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_transaction_old`
--

CREATE TABLE `staff_transaction_old` (
  `staffid` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `old_office_id` int(11) DEFAULT NULL,
  `new_office_id` int(11) DEFAULT NULL,
  `date_of_transfer` date NOT NULL DEFAULT '0000-00-00',
  `old_designation` int(11) DEFAULT NULL,
  `new_designation` int(11) DEFAULT NULL,
  `trans_status` varchar(30) DEFAULT NULL,
  `transid` int(8) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `statecode` char(2) NOT NULL DEFAULT '',
  `name` varchar(60) NOT NULL,
  `created_date` date NOT NULL,
  `modified_date` date DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sysaccesslevel`
--

CREATE TABLE `sysaccesslevel` (
  `Acclevel_Cd` int(3) NOT NULL DEFAULT '0',
  `Acclevel_Name` varchar(30) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sysidentity`
--

CREATE TABLE `sysidentity` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `syslanguage`
--

CREATE TABLE `syslanguage` (
  `lang_cd` int(2) NOT NULL,
  `lang_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sysrelation`
--

CREATE TABLE `sysrelation` (
  `id` int(11) NOT NULL,
  `relationname` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblfinalmonthlysalary`
--

CREATE TABLE `tblfinalmonthlysalary` (
  `officeid` int(10) NOT NULL,
  `salmonth` int(2) NOT NULL,
  `salyear` smallint(6) NOT NULL,
  `recordtype` varchar(2) NOT NULL,
  `seqno` int(11) NOT NULL,
  `staffid` int(10) NOT NULL,
  `e_basic` int(11) DEFAULT NULL,
  `e_sp` int(11) DEFAULT NULL,
  `e_fda` int(11) DEFAULT NULL,
  `e_01` int(11) DEFAULT NULL,
  `e_02` int(11) DEFAULT NULL,
  `e_03` int(11) DEFAULT NULL,
  `e_04` int(11) DEFAULT NULL,
  `e_05` int(11) DEFAULT NULL,
  `e_06` int(11) DEFAULT NULL,
  `e_07` int(11) DEFAULT NULL,
  `e_08` int(11) DEFAULT NULL,
  `e_09` int(11) DEFAULT NULL,
  `e_10` int(11) DEFAULT NULL,
  `e_11` int(11) DEFAULT NULL,
  `e_12` int(11) DEFAULT NULL,
  `e_13` int(11) DEFAULT NULL,
  `e_14` int(11) DEFAULT NULL,
  `e_15` int(11) DEFAULT NULL,
  `e_16` int(11) DEFAULT NULL,
  `e_17` int(11) DEFAULT NULL,
  `e_18` int(11) DEFAULT NULL,
  `e_19` int(11) DEFAULT NULL,
  `e_20` int(11) DEFAULT NULL,
  `e_21` int(11) DEFAULT NULL,
  `e_22` int(11) DEFAULT NULL,
  `e_23` int(11) DEFAULT NULL,
  `e_24` int(11) DEFAULT NULL,
  `e_25` int(11) DEFAULT NULL,
  `e_26` int(11) DEFAULT NULL,
  `e_27` int(11) DEFAULT NULL,
  `e_28` int(11) DEFAULT NULL,
  `e_29` int(11) DEFAULT NULL,
  `e_30` int(11) DEFAULT NULL,
  `d_pf` int(11) DEFAULT NULL,
  `d_vpf` int(11) DEFAULT NULL,
  `d_01` int(11) DEFAULT NULL,
  `d_02` int(11) DEFAULT NULL,
  `d_03` int(11) DEFAULT NULL,
  `d_04` int(11) DEFAULT NULL,
  `d_05` int(11) DEFAULT NULL,
  `d_06` int(11) DEFAULT NULL,
  `d_07` int(11) DEFAULT NULL,
  `d_08` int(11) DEFAULT NULL,
  `d_09` int(11) DEFAULT NULL,
  `d_10` int(11) DEFAULT NULL,
  `d_11` int(11) DEFAULT NULL,
  `d_12` int(11) DEFAULT NULL,
  `d_13` int(11) DEFAULT NULL,
  `d_14` int(11) DEFAULT NULL,
  `d_15` int(11) DEFAULT NULL,
  `d_16` int(11) DEFAULT NULL,
  `d_17` int(11) DEFAULT NULL,
  `d_18` int(11) DEFAULT NULL,
  `d_19` int(11) DEFAULT NULL,
  `d_20` int(11) DEFAULT NULL,
  `d_21` int(11) DEFAULT NULL,
  `d_22` int(11) DEFAULT NULL,
  `d_23` int(11) DEFAULT NULL,
  `d_24` int(11) DEFAULT NULL,
  `d_25` int(11) DEFAULT NULL,
  `d_26` int(11) DEFAULT NULL,
  `d_27` int(11) DEFAULT NULL,
  `d_28` int(11) DEFAULT NULL,
  `d_29` int(11) DEFAULT NULL,
  `d_30` int(11) DEFAULT NULL,
  `e_basic_aa` int(11) DEFAULT NULL,
  `e_sp_a` int(11) DEFAULT NULL,
  `e_fda_a` int(11) DEFAULT NULL,
  `e_01_a` int(11) DEFAULT NULL,
  `e_02_a` int(11) DEFAULT NULL,
  `e_03_a` int(11) DEFAULT NULL,
  `e_04_a` int(11) DEFAULT NULL,
  `e_05_a` int(11) DEFAULT NULL,
  `e_06_a` int(11) DEFAULT NULL,
  `e_07_a` int(11) DEFAULT NULL,
  `e_08_a` int(11) DEFAULT NULL,
  `e_09_a` int(11) DEFAULT NULL,
  `e_10_a` int(11) DEFAULT NULL,
  `e_11_a` int(11) DEFAULT NULL,
  `e_12_a` int(11) DEFAULT NULL,
  `e_13_a` int(11) DEFAULT NULL,
  `e_14_a` int(11) DEFAULT NULL,
  `e_15_a` int(11) DEFAULT NULL,
  `e_16_a` int(11) DEFAULT NULL,
  `e_17_a` int(11) DEFAULT NULL,
  `e_18_a` int(11) DEFAULT NULL,
  `e_19_a` int(11) DEFAULT NULL,
  `e_20_a` int(11) DEFAULT NULL,
  `e_21_a` int(11) DEFAULT NULL,
  `e_22_a` int(11) DEFAULT NULL,
  `e_23_a` int(11) DEFAULT NULL,
  `e_24_a` int(11) DEFAULT NULL,
  `e_25_a` int(11) DEFAULT NULL,
  `e_26_a` int(11) DEFAULT NULL,
  `e_27_a` int(11) DEFAULT NULL,
  `e_28_a` int(11) DEFAULT NULL,
  `e_29_a` int(11) DEFAULT NULL,
  `e_30_a` int(11) DEFAULT NULL,
  `d_pf_a` int(11) DEFAULT NULL,
  `d_vpf_a` int(11) DEFAULT NULL,
  `d_01_a` int(11) DEFAULT NULL,
  `d_02_a` int(11) DEFAULT NULL,
  `d_03_a` int(11) DEFAULT NULL,
  `d_04_a` int(11) DEFAULT NULL,
  `d_05_a` int(11) DEFAULT NULL,
  `d_06_a` int(11) DEFAULT NULL,
  `d_07_a` int(11) DEFAULT NULL,
  `d_08_a` int(11) DEFAULT NULL,
  `d_09_a` int(11) DEFAULT NULL,
  `d_10_a` int(11) DEFAULT NULL,
  `d_11_a` int(11) DEFAULT NULL,
  `d_12_a` int(11) DEFAULT NULL,
  `d_13_a` int(11) DEFAULT NULL,
  `d_14_a` int(11) DEFAULT NULL,
  `d_15_a` int(11) DEFAULT NULL,
  `d_16_a` int(11) DEFAULT NULL,
  `d_17_a` int(11) DEFAULT NULL,
  `d_18_a` int(11) DEFAULT NULL,
  `d_19_a` int(11) DEFAULT NULL,
  `d_20_a` int(11) DEFAULT NULL,
  `d_21_a` int(11) DEFAULT NULL,
  `d_22_a` int(11) DEFAULT NULL,
  `d_23_a` int(11) DEFAULT NULL,
  `d_24_a` int(11) DEFAULT NULL,
  `d_25_a` int(11) DEFAULT NULL,
  `d_26_a` int(11) DEFAULT NULL,
  `d_27_a` int(11) DEFAULT NULL,
  `d_28_a` int(11) DEFAULT NULL,
  `d_29_a` int(11) DEFAULT NULL,
  `d_30_a` int(11) DEFAULT NULL,
  `c_totearn` int(11) DEFAULT NULL,
  `c_totdedu` int(11) DEFAULT NULL,
  `c_netsal` int(11) DEFAULT NULL,
  `c_pension` int(11) DEFAULT NULL,
  `c_grosssalary` int(11) DEFAULT NULL,
  `salarylock` tinyint(1) DEFAULT NULL,
  `attendance` int(11) DEFAULT NULL,
  `lwp` int(11) DEFAULT NULL,
  `workingdays` tinyint(4) DEFAULT NULL,
  `othrs` int(11) DEFAULT NULL,
  `aothrs` int(11) DEFAULT NULL,
  `deductpfloan` tinyint(1) DEFAULT NULL,
  `deductnb` tinyint(1) DEFAULT NULL,
  `deducttcs` tinyint(1) DEFAULT NULL,
  `elinstallno` int(11) DEFAULT NULL,
  `glinstallno` int(11) DEFAULT NULL,
  `paidinperiod` varchar(7) DEFAULT NULL,
  `deducthouseloan` tinyint(1) DEFAULT NULL,
  `deductfestivalloan` tinyint(1) DEFAULT NULL,
  `deductcarloan` tinyint(1) DEFAULT NULL,
  `deductscooterloan` tinyint(1) DEFAULT NULL,
  `deductsundryadv` tinyint(1) DEFAULT NULL,
  `chknegative` tinyint(1) DEFAULT NULL,
  `chkalwaysnegative` tinyint(1) DEFAULT NULL,
  `pfloaninstno` int(11) DEFAULT NULL,
  `hbloaninstno` int(11) DEFAULT NULL,
  `festivalloaninstno` int(11) DEFAULT NULL,
  `carloaninstno` int(11) DEFAULT NULL,
  `scooterloaninstno` int(11) DEFAULT NULL,
  `sundryadvinstno` int(11) DEFAULT NULL,
  `dateofgeneratesalary` datetime NOT NULL,
  `payslipno` int(11) DEFAULT NULL,
  `e_basic_inc` float DEFAULT NULL,
  `rateadaa` float DEFAULT NULL,
  `ratehraa` float DEFAULT NULL,
  `ratepfa` int(11) DEFAULT NULL,
  `ratevpfa` float DEFAULT NULL,
  `arreartype` varchar(1) DEFAULT NULL,
  `deductlic` tinyint(1) DEFAULT NULL,
  `bankcode` varchar(5) DEFAULT NULL,
  `bankacno` varchar(15) DEFAULT NULL,
  `designationcode` varchar(5) DEFAULT NULL,
  `e_31` int(11) DEFAULT NULL,
  `e_31_a` int(11) DEFAULT NULL,
  `e_32` int(11) DEFAULT NULL,
  `e_32_a` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblheadlookup`
--

CREATE TABLE `tblheadlookup` (
  `fieldname` varchar(30) NOT NULL,
  `lowerrange` int(18) NOT NULL,
  `upperrange` int(18) DEFAULT NULL,
  `headvalue` varchar(600) DEFAULT NULL,
  `formulafield` tinyint(1) DEFAULT NULL,
  `seqno` int(11) NOT NULL,
  `headvaluename` varchar(600) DEFAULT NULL,
  `maxlimit` int(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblheadlookuphistory`
--

CREATE TABLE `tblheadlookuphistory` (
  `FieldName` varchar(30) NOT NULL,
  `lowerrange` int(18) NOT NULL,
  `upperrange` int(18) DEFAULT NULL,
  `headvalue` varchar(600) DEFAULT NULL,
  `formulafield` tinyint(1) DEFAULT NULL,
  `seqno` int(10) NOT NULL,
  `headvaluename` varchar(600) DEFAULT NULL,
  `maxlimit` int(18) NOT NULL,
  `period` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblmonthlyinput`
--

CREATE TABLE `tblmonthlyinput` (
  `officeid` int(10) NOT NULL,
  `salmonth` varchar(2) NOT NULL,
  `salyear` int(10) NOT NULL,
  `staffid` int(10) NOT NULL,
  `e_basic` int(10) DEFAULT NULL,
  `e_sp` int(10) DEFAULT NULL,
  `e_fda` int(10) DEFAULT NULL,
  `e_01` int(10) DEFAULT NULL,
  `e_02` int(10) DEFAULT NULL,
  `e_03` int(10) DEFAULT NULL,
  `e_04` int(10) DEFAULT NULL,
  `e_05` int(10) DEFAULT NULL,
  `e_06` int(10) DEFAULT NULL,
  `e_07` int(10) DEFAULT NULL,
  `e_08` int(10) DEFAULT NULL,
  `e_09` int(10) DEFAULT NULL,
  `e_10` int(10) DEFAULT NULL,
  `e_11` int(10) DEFAULT NULL,
  `e_12` int(10) DEFAULT NULL,
  `e_13` int(10) DEFAULT NULL,
  `e_14` int(10) DEFAULT NULL,
  `e_15` int(10) DEFAULT NULL,
  `e_16` int(10) DEFAULT NULL,
  `e_17` int(10) DEFAULT NULL,
  `e_18` int(10) DEFAULT NULL,
  `e_19` int(10) DEFAULT NULL,
  `e_20` int(10) DEFAULT NULL,
  `e_21` int(10) DEFAULT NULL,
  `e_22` int(10) DEFAULT NULL,
  `e_23` int(10) DEFAULT NULL,
  `e_24` int(10) DEFAULT NULL,
  `e_25` int(10) DEFAULT NULL,
  `e_26` int(10) DEFAULT NULL,
  `e_27` int(10) DEFAULT NULL,
  `e_28` int(10) DEFAULT NULL,
  `e_29` int(10) DEFAULT NULL,
  `e_30` int(10) DEFAULT NULL,
  `d_pf` int(10) DEFAULT NULL,
  `d_vpf` int(10) DEFAULT NULL,
  `d_01` int(11) DEFAULT NULL,
  `d_02` int(11) DEFAULT NULL,
  `d_03` int(11) DEFAULT NULL,
  `d_04` int(11) DEFAULT NULL,
  `d_05` int(11) DEFAULT NULL,
  `d_06` int(11) DEFAULT NULL,
  `d_07` int(11) DEFAULT NULL,
  `d_08` int(11) DEFAULT NULL,
  `d_09` int(11) DEFAULT NULL,
  `d_10` int(11) DEFAULT NULL,
  `d_11` int(11) DEFAULT NULL,
  `d_12` int(11) DEFAULT NULL,
  `d_13` int(11) DEFAULT NULL,
  `d_14` int(11) DEFAULT NULL,
  `d_15` int(11) DEFAULT NULL,
  `d_16` int(11) DEFAULT NULL,
  `d_17` int(11) DEFAULT NULL,
  `d_18` int(11) DEFAULT NULL,
  `d_19` int(11) DEFAULT NULL,
  `d_20` int(11) DEFAULT NULL,
  `d_21` int(11) DEFAULT NULL,
  `d_22` int(11) DEFAULT NULL,
  `d_23` int(11) DEFAULT NULL,
  `d_24` int(11) DEFAULT NULL,
  `d_25` int(11) DEFAULT NULL,
  `d_26` int(11) DEFAULT NULL,
  `d_27` int(11) DEFAULT NULL,
  `d_28` int(11) DEFAULT NULL,
  `d_29` int(11) DEFAULT NULL,
  `d_30` int(11) DEFAULT NULL,
  `c_totearn` int(11) DEFAULT NULL,
  `c_totdedu` int(11) DEFAULT NULL,
  `c_netsal` int(11) DEFAULT NULL,
  `c_pension` int(11) DEFAULT NULL,
  `c_grosssalary` int(11) DEFAULT NULL,
  `salarylock` bit(1) DEFAULT NULL,
  `lwp` int(11) DEFAULT NULL,
  `othrs` double DEFAULT NULL,
  `aothrs` double DEFAULT NULL,
  `deductpfloan` tinyint(1) DEFAULT NULL,
  `deductnb` tinyint(1) DEFAULT NULL,
  `deducttcs` tinyint(1) DEFAULT NULL,
  `deducthouseloan` tinyint(1) DEFAULT NULL,
  `deductfestivalloan` tinyint(1) DEFAULT NULL,
  `deductcarloan` tinyint(1) DEFAULT NULL,
  `deductscooterloan` tinyint(1) DEFAULT NULL,
  `seductsundryadv` tinyint(1) DEFAULT NULL,
  `e_31` int(11) DEFAULT NULL,
  `e_32` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblmstemployeesalary`
--

CREATE TABLE `tblmstemployeesalary` (
  `staffid` int(10) NOT NULL,
  `e_basic` int(18) DEFAULT NULL,
  `lastbasic` int(18) DEFAULT NULL,
  `e_sp` int(18) DEFAULT NULL,
  `lastincrement` int(18) DEFAULT NULL,
  `spcashalw` int(19) DEFAULT NULL,
  `sptelexalw` int(19) DEFAULT NULL,
  `spstenographyalw` int(19) DEFAULT NULL,
  `spaccworkalw` int(19) DEFAULT NULL,
  `spodupmachinealw` int(19) DEFAULT NULL,
  `spfaxmachinealw` int(19) DEFAULT NULL,
  `spasdriveralw` int(19) DEFAULT NULL,
  `e_fda` int(18) DEFAULT NULL,
  `e_01` int(18) DEFAULT NULL,
  `e_02` int(18) DEFAULT NULL,
  `e_03` int(18) DEFAULT NULL,
  `e_04` int(18) DEFAULT NULL,
  `e_05` int(18) DEFAULT NULL,
  `e_06` int(18) DEFAULT NULL,
  `e_07` int(18) DEFAULT NULL,
  `e_08` int(18) DEFAULT NULL,
  `e_09` int(18) DEFAULT NULL,
  `sanightwatch` int(18) DEFAULT NULL,
  `sadcallowance` int(18) DEFAULT NULL,
  `saassamcomp` int(18) DEFAULT NULL,
  `safairepart` int(18) DEFAULT NULL,
  `satosplpost` int(18) DEFAULT NULL,
  `e_10` int(18) DEFAULT NULL,
  `e_11` int(18) DEFAULT NULL,
  `e_12` int(18) DEFAULT NULL,
  `e_13` int(18) DEFAULT NULL,
  `e_14` int(18) DEFAULT NULL,
  `e_15` int(18) DEFAULT NULL,
  `e_16` int(18) DEFAULT NULL,
  `e_17` int(18) DEFAULT NULL,
  `e_18` int(18) DEFAULT NULL,
  `e_19` int(18) DEFAULT NULL,
  `e_20` int(18) DEFAULT NULL,
  `e_21` int(18) DEFAULT NULL,
  `e_22` int(18) DEFAULT NULL,
  `e_23` int(18) DEFAULT NULL,
  `e_24` int(18) DEFAULT NULL,
  `e_25` int(18) DEFAULT NULL,
  `e_26` int(18) DEFAULT NULL,
  `e_27` int(18) DEFAULT NULL,
  `e_28` int(18) DEFAULT NULL,
  `e_29` int(18) DEFAULT NULL,
  `e_30` int(18) DEFAULT NULL,
  `d_pf` int(18) DEFAULT NULL,
  `d_vpf` int(18) DEFAULT NULL,
  `d_01` int(18) DEFAULT NULL,
  `d_02` int(18) DEFAULT NULL,
  `d_03` int(18) DEFAULT NULL,
  `d_04` int(18) DEFAULT NULL,
  `d_05` int(18) DEFAULT NULL,
  `d_06` int(18) DEFAULT NULL,
  `d_07` int(18) DEFAULT NULL,
  `d_08` int(18) DEFAULT NULL,
  `d_09` int(18) DEFAULT NULL,
  `d_10` int(18) DEFAULT NULL,
  `d_11` int(18) DEFAULT NULL,
  `d_12` int(18) DEFAULT NULL,
  `d_13` int(18) DEFAULT NULL,
  `d_14` int(18) DEFAULT NULL,
  `d_15` int(18) DEFAULT NULL,
  `d_16` int(18) DEFAULT NULL,
  `d_17` int(18) DEFAULT NULL,
  `d_18` int(18) DEFAULT NULL,
  `d_19` int(18) DEFAULT NULL,
  `d_20` int(18) DEFAULT NULL,
  `d_21` int(18) DEFAULT NULL,
  `d_22` int(18) DEFAULT NULL,
  `d_23` int(18) DEFAULT NULL,
  `d_24` int(18) DEFAULT NULL,
  `d_215` int(18) DEFAULT NULL,
  `d_26` int(18) DEFAULT NULL,
  `d_27` int(18) DEFAULT NULL,
  `d_28` int(18) DEFAULT NULL,
  `d_29` int(18) DEFAULT NULL,
  `d_30` int(18) DEFAULT NULL,
  `modepay` char(10) DEFAULT NULL,
  `bankcode` varchar(5) DEFAULT NULL,
  `bankacno` varchar(15) DEFAULT NULL,
  `hra` tinyint(1) DEFAULT NULL,
  `unionfee` tinyint(1) DEFAULT NULL,
  `proftax` tinyint(1) DEFAULT NULL,
  `noofchildren` tinyint(4) DEFAULT NULL,
  `sportclub` tinyint(1) DEFAULT NULL,
  `isratevpf` tinyint(1) DEFAULT NULL,
  `vpfvaluera` int(11) DEFAULT NULL,
  `lastincrementdate` datetime DEFAULT NULL,
  `cca` tinyint(1) DEFAULT NULL,
  `washing` tinyint(1) DEFAULT NULL,
  `none` tinyint(1) DEFAULT NULL,
  `deletedemployee` tinyint(1) DEFAULT NULL,
  `issalgenrated` tinyint(1) DEFAULT NULL,
  `e_31` int(11) DEFAULT NULL,
  `e_32` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblmstloantype`
--

CREATE TABLE `tblmstloantype` (
  `loantype` varchar(4) NOT NULL,
  `loandesc` varchar(50) DEFAULT NULL,
  `paymenttype` varchar(15) DEFAULT NULL,
  `maxinstallmentp` int(11) DEFAULT NULL,
  `maxinstallmentl` int(10) DEFAULT NULL,
  `maxlnamount` int(10) DEFAULT NULL,
  `isonfloatingrate` tinyint(1) DEFAULT NULL,
  `iscalcinterest` tinyint(1) DEFAULT NULL,
  `abbriviation` varchar(3) DEFAULT NULL,
  `lsslabdependent` tinyint(1) DEFAULT NULL,
  `lstdsrebet` tinyint(1) DEFAULT NULL,
  `tdsrebetrol` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campus_intimation`
--

CREATE TABLE `tbl_campus_intimation` (
  `id` int(11) NOT NULL,
  `campusid` int(11) NOT NULL,
  `messsage` text NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `mailstatus` tinyint(1) NOT NULL,
  `campus_written_cutoffmarks` int(3) DEFAULT NULL,
  `campus_gd_cutoffmarks` int(11) DEFAULT NULL,
  `campus_status` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_communication_address`
--

CREATE TABLE `tbl_candidate_communication_address` (
  `addressid` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `presentstreet` varchar(150) NOT NULL,
  `presentcity` varchar(50) NOT NULL,
  `presentstateid` int(11) NOT NULL,
  `presentdistrict` varchar(50) NOT NULL,
  `presentpincode` int(11) NOT NULL,
  `permanentstreet` varchar(150) NOT NULL,
  `permanentcity` varchar(50) NOT NULL,
  `permanentstateid` int(11) NOT NULL,
  `permanentdistrict` varchar(50) NOT NULL,
  `permanentpincode` int(11) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `presenthno` varchar(50) NOT NULL,
  `permanenthno` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_gdscore`
--

CREATE TABLE `tbl_candidate_gdscore` (
  `gdscoreid` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `gdscore` int(3) DEFAULT NULL,
  `flag` varchar(10) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `rsscore` int(11) DEFAULT NULL,
  `ssscore` int(11) DEFAULT NULL,
  `cutoffmarks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_hrscore`
--

CREATE TABLE `tbl_candidate_hrscore` (
  `hrscoreid` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `hrscore` int(3) NOT NULL,
  `flag` varchar(10) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_registration`
--

CREATE TABLE `tbl_candidate_registration` (
  `candidateid` int(11) NOT NULL,
  `candidatefirstname` varchar(50) NOT NULL,
  `candidatemiddlename` varchar(50) DEFAULT NULL,
  `candidatelastname` varchar(50) NOT NULL,
  `motherfirstname` varchar(50) NOT NULL,
  `mothermiddlename` varchar(50) DEFAULT NULL,
  `motherlastname` varchar(50) NOT NULL,
  `fatherfirstname` varchar(50) NOT NULL,
  `fathermiddlename` varchar(50) DEFAULT NULL,
  `fatherlastname` varchar(50) NOT NULL,
  `gender` int(2) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `maritalstatus` int(2) NOT NULL,
  `dateofbirth` date NOT NULL,
  `emailid` varchar(50) NOT NULL,
  `mobile` varchar(40) NOT NULL,
  `metricschoolcollege` varchar(150) NOT NULL,
  `metricboarduniversity` varchar(150) NOT NULL,
  `metricpassingyear` int(4) NOT NULL,
  `metricplace` varchar(50) NOT NULL,
  `metricspecialisation` varchar(150) NOT NULL,
  `metricpercentage` int(3) NOT NULL,
  `hscschoolcollege` varchar(150) NOT NULL,
  `hscboarduniversity` varchar(150) NOT NULL,
  `hscpassingyear` int(4) NOT NULL,
  `hscplace` varchar(50) NOT NULL,
  `hscspecialisation` varchar(150) NOT NULL,
  `hscpercentage` int(3) NOT NULL,
  `hscstream` varchar(50) DEFAULT NULL,
  `ugschoolcollege` varchar(150) NOT NULL,
  `ugboarduniversity` varchar(150) NOT NULL,
  `ugpassingyear` int(4) NOT NULL,
  `ugplace` varchar(50) NOT NULL,
  `ugspecialisation` varchar(150) NOT NULL,
  `ugpercentage` int(3) NOT NULL,
  `ugdegree` varchar(50) DEFAULT NULL,
  `pgschoolcollege` varchar(150) DEFAULT NULL,
  `pgboarduniversity` varchar(150) DEFAULT NULL,
  `pgpassingyear` int(4) DEFAULT NULL,
  `pgplace` varchar(50) DEFAULT NULL,
  `pgspecialisation` varchar(150) DEFAULT NULL,
  `pgpercentage` int(3) DEFAULT NULL,
  `pgdegree` varchar(50) DEFAULT NULL,
  `otherschoolcollege` varchar(150) DEFAULT NULL,
  `otherboarduniversity` varchar(150) DEFAULT NULL,
  `otherpassingyear` int(4) DEFAULT NULL,
  `otherplace` varchar(30) DEFAULT NULL,
  `otherspecialisation` varchar(150) DEFAULT NULL,
  `otherpercentage` int(3) DEFAULT NULL,
  `teamid` int(11) DEFAULT NULL,
  `fgid` int(11) DEFAULT NULL,
  `batchid` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) DEFAULT NULL,
  `campusid` int(11) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `bloodgroup` varchar(5) DEFAULT NULL,
  `originalphotoname` varchar(50) DEFAULT NULL,
  `encryptedphotoname` varchar(50) DEFAULT NULL,
  `BDFFormStatus` varchar(10) DEFAULT NULL,
  `encryptmetriccertificate` varchar(150) DEFAULT NULL,
  `encrypthsccertificate` varchar(150) DEFAULT NULL,
  `encryptugcertificate` varchar(150) DEFAULT NULL,
  `encryptpgcertificate` varchar(150) DEFAULT NULL,
  `encryptothercertificate` varchar(150) DEFAULT NULL,
  `originalmetriccertificate` varchar(50) DEFAULT NULL,
  `originalhsccertificate` varchar(50) DEFAULT NULL,
  `originalugcertificate` varchar(50) DEFAULT NULL,
  `originalpgcertificate` varchar(50) DEFAULT NULL,
  `originalothercertificate` varchar(50) DEFAULT NULL,
  `joinstatus` tinyint(1) NOT NULL DEFAULT '0',
  `categoryid` int(11) DEFAULT NULL,
  `inprocess` varchar(10) NOT NULL DEFAULT 'open',
  `frstatus` varchar(11) DEFAULT NULL,
  `frsent` varchar(3) DEFAULT NULL,
  `frdate` date DEFAULT NULL,
  `frcreatedby` int(11) DEFAULT NULL,
  `yprststus` varchar(11) DEFAULT NULL,
  `yprsent` varchar(3) DEFAULT NULL,
  `yprdate` date DEFAULT NULL,
  `yprcreatedby` int(11) DEFAULT NULL,
  `confirm_attend` tinyint(1) NOT NULL DEFAULT '0',
  `interview_date` date DEFAULT NULL,
  `campustype` varchar(10) DEFAULT NULL,
  `wstatus` int(1) DEFAULT NULL,
  `gdstatus` int(1) DEFAULT NULL,
  `hrstatus` int(1) DEFAULT NULL,
  `complete_inprocess` tinyint(1) NOT NULL DEFAULT '0',
  `otherdegree` varchar(5) DEFAULT NULL,
  `other_degree_specify` varchar(150) DEFAULT NULL,
  `campusintimationid` int(11) DEFAULT NULL,
  `gdemail_status` int(1) DEFAULT NULL,
  `BDFStatusaftergd` int(1) DEFAULT NULL,
  `hrddocumentcheckstatus` int(2) NOT NULL,
  `tc_hrd_document_verfied` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_writtenscore`
--

CREATE TABLE `tbl_candidate_writtenscore` (
  `writtenscoreid` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `writtenscore` int(3) NOT NULL,
  `flag` varchar(10) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `cutoffmarks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_central_event`
--

CREATE TABLE `tbl_central_event` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `place` varchar(150) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0',
  `updatedon` date DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_central_event_transaction`
--

CREATE TABLE `tbl_central_event_transaction` (
  `id` int(11) NOT NULL,
  `central_event_id` int(11) NOT NULL,
  `resouce_person_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_central_event_updation_metrial`
--

CREATE TABLE `tbl_central_event_updation_metrial` (
  `id` int(11) NOT NULL,
  `central_event_id` int(11) NOT NULL,
  `encrypted_document_upload` varchar(50) NOT NULL,
  `original_document_upload` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_daship_component`
--

CREATE TABLE `tbl_daship_component` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `phaseid` int(11) NOT NULL,
  `original_document_name` varchar(50) NOT NULL,
  `encrypted_document_name` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_da_event_detailing`
--

CREATE TABLE `tbl_da_event_detailing` (
  `id` int(11) NOT NULL,
  `batchid` int(11) NOT NULL,
  `phaseid` int(11) NOT NULL,
  `financial_year` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_da_event_detailing_transaction`
--

CREATE TABLE `tbl_da_event_detailing_transaction` (
  `id` int(11) NOT NULL,
  `da_event_detailing` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_da_personal_info`
--

CREATE TABLE `tbl_da_personal_info` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `emailid` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `doj` date NOT NULL,
  `dob` date NOT NULL,
  `maritalstatus` int(10) NOT NULL,
  `emp_code` varchar(11) NOT NULL DEFAULT '0001',
  `teamid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `batchid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_family_members`
--

CREATE TABLE `tbl_family_members` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `Familymembername` varchar(50) DEFAULT NULL,
  `relationwithemployee` int(11) DEFAULT NULL,
  `familydob` date DEFAULT NULL,
  `originalphotoname` varchar(50) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `updateby` int(11) DEFAULT NULL,
  `updateon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `encryptedphotoname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback`
--

CREATE TABLE `tbl_feedback` (
  `id` int(11) NOT NULL,
  `apprenticeid` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `tcid` int(11) NOT NULL,
  `batchid` int(11) NOT NULL,
  `field_guideid` int(11) NOT NULL,
  `da_extremely_good` varchar(150) NOT NULL,
  `da_needs_to_pay_attention` varchar(150) NOT NULL,
  `da_to_leave_organization` varchar(150) NOT NULL,
  `feedbackdate` date NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime NOT NULL,
  `updatedby` int(11) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0',
  `feedback_letter` varchar(50) DEFAULT NULL,
  `monthyear` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gap_year`
--

CREATE TABLE `tbl_gap_year` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `reason` varchar(250) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_nomination_and_authorisation_form`
--

CREATE TABLE `tbl_general_nomination_and_authorisation_form` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `executive_director_place` varchar(50) NOT NULL,
  `da_place` varchar(50) NOT NULL,
  `nomination_authorisation_signed` varchar(50) DEFAULT NULL,
  `first_witness_name` varchar(50) DEFAULT NULL,
  `first_witness_address` varchar(150) DEFAULT NULL,
  `second_witness_name` varchar(50) DEFAULT NULL,
  `second_witness_address` varchar(150) DEFAULT NULL,
  `da_date` date NOT NULL,
  `status` int(2) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_generate_offer_letter_details`
--

CREATE TABLE `tbl_generate_offer_letter_details` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `fileno` varchar(30) NOT NULL,
  `offerno` varchar(30) NOT NULL,
  `doj` date NOT NULL,
  `lastdateofacceptanceofdocs` date NOT NULL,
  `flag` varchar(20) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `filename` varchar(150) NOT NULL,
  `sendflag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hrd_verification_document`
--

CREATE TABLE `tbl_hrd_verification_document` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `metric_certificate` varchar(5) NOT NULL,
  `hsc_certificate` varchar(5) NOT NULL,
  `ug_certificate` varchar(5) NOT NULL,
  `pg_certificate` varchar(5) DEFAULT NULL,
  `other_certificate` varchar(5) DEFAULT NULL,
  `comment` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `approvedby` int(11) NOT NULL,
  `approvedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hrd_work_experience_verified`
--

CREATE TABLE `tbl_hrd_work_experience_verified` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `experience_verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hr_intemation`
--

CREATE TABLE `tbl_hr_intemation` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `intemation_type` int(2) NOT NULL,
  `comment` varchar(150) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `Isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_identity_details`
--

CREATE TABLE `tbl_identity_details` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `identityname` varchar(50) DEFAULT NULL,
  `identitynumber` varchar(50) DEFAULT NULL,
  `encryptedphotoname` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateon` datetime DEFAULT NULL,
  `originalphotoname` varchar(50) DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `Isdeleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_joining_report`
--

CREATE TABLE `tbl_joining_report` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `apprenticeship_at_location` varchar(50) NOT NULL,
  `office_at_location` varchar(50) NOT NULL,
  `join_programme_date` date NOT NULL,
  `status` int(2) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `submittedby` int(11) DEFAULT NULL,
  `submittedon` datetime DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language_proficiency`
--

CREATE TABLE `tbl_language_proficiency` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `languageid` int(2) DEFAULT NULL,
  `lang_speak` varchar(2) DEFAULT NULL,
  `lang_read` varchar(2) DEFAULT NULL,
  `lang_write` varchar(2) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `Isdeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nominee_details`
--

CREATE TABLE `tbl_nominee_details` (
  `id` int(11) NOT NULL,
  `general_nomination_id` int(11) NOT NULL,
  `nominee_name` varchar(50) NOT NULL,
  `nominee_relation` int(11) NOT NULL,
  `nominee_age` int(11) NOT NULL,
  `nominee_address` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_documents`
--

CREATE TABLE `tbl_other_documents` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `documentname` int(11) DEFAULT NULL,
  `documenttype` int(11) DEFAULT NULL,
  `documentupload` varchar(250) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_documents_verified_hrd`
--

CREATE TABLE `tbl_other_documents_verified_hrd` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `documentid` int(2) NOT NULL,
  `document_verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_information`
--

CREATE TABLE `tbl_other_information` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `any_subject_of_interest` varchar(255) DEFAULT NULL,
  `any_achievementa_awards` varchar(255) DEFAULT NULL,
  `any_assignment_of_special_interest` varchar(255) DEFAULT NULL,
  `experience_of_group_social_activities` varchar(255) DEFAULT NULL,
  `have_you_taken_part_in_pradan_selection_process_before` varchar(5) DEFAULT NULL,
  `have_you_taken_part_in_pradan_selection_process_before_when` date DEFAULT NULL,
  `have_you_taken_part_in_pradan_selection_process_before_where` int(2) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) DEFAULT NULL,
  `annual_income` int(2) DEFAULT NULL,
  `male_sibling` int(2) DEFAULT NULL,
  `female_sibling` int(2) DEFAULT NULL,
  `know_about_pradan` varchar(20) DEFAULT NULL,
  `know_about_pradan_other_specify` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_participant`
--

CREATE TABLE `tbl_participant` (
  `id` int(11) NOT NULL,
  `central_event_id` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `send_email_status` tinyint(1) NOT NULL,
  `central_event_join_status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prejoining_document`
--

CREATE TABLE `tbl_prejoining_document` (
  `id` int(11) NOT NULL,
  `prejoining_letterid` int(11) NOT NULL,
  `original_prejoining_document_name` varchar(50) NOT NULL,
  `encrypted_prejoining_document_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prejoining_letter`
--

CREATE TABLE `tbl_prejoining_letter` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `prejoining_letter` text NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_probation_separation`
--

CREATE TABLE `tbl_probation_separation` (
  `id` int(11) NOT NULL,
  `type` int(2) NOT NULL,
  `dc` varchar(200) DEFAULT NULL,
  `team` varchar(200) DEFAULT NULL,
  `da` varchar(200) DEFAULT NULL,
  `probation_completed` varchar(200) DEFAULT NULL,
  `dateofseraption` date DEFAULT NULL,
  `probation_extension_date` date DEFAULT NULL,
  `probation_completed_date` date DEFAULT NULL,
  `reason` text,
  `comment` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `isdeleted` tinyint(4) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `generate_letter_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sendofferletter`
--

CREATE TABLE `tbl_sendofferletter` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `flag` varchar(10) NOT NULL,
  `sendby` int(11) NOT NULL,
  `senddate` datetime NOT NULL,
  `againsendby` int(11) DEFAULT NULL,
  `againsenddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tc_daship_component_comment`
--

CREATE TABLE `tbl_tc_daship_component_comment` (
  `id` int(11) NOT NULL,
  `dashipid` int(11) NOT NULL,
  `comments` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_training_exposure`
--

CREATE TABLE `tbl_training_exposure` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `natureoftraining` varchar(150) DEFAULT NULL,
  `organizing_agency` varchar(150) DEFAULT NULL,
  `fromdate` date DEFAULT NULL,
  `todate` date DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transfer_da`
--

CREATE TABLE `tbl_transfer_da` (
  `id` int(11) NOT NULL,
  `olddc` int(11) NOT NULL,
  `oldteam` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `newdc` int(11) NOT NULL,
  `newteam` int(11) NOT NULL,
  `new_field_guide` int(11) NOT NULL,
  `dateoftransfer` date NOT NULL,
  `comment` varchar(150) NOT NULL,
  `generate_letter_name` varchar(50) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transfer_history`
--

CREATE TABLE `tbl_transfer_history` (
  `id` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `teamid` int(2) NOT NULL,
  `fg` int(11) NOT NULL,
  `doj` date NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verification_document`
--

CREATE TABLE `tbl_verification_document` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `metric_certificate` varchar(5) NOT NULL,
  `hsc_certificate` varchar(5) NOT NULL,
  `ug_certificate` varchar(5) NOT NULL,
  `pg_certificate` varchar(5) DEFAULT NULL,
  `other_certificate` varchar(5) DEFAULT NULL,
  `general_nomination_form` varchar(5) NOT NULL,
  `joining_report_da` varchar(5) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `approvedby` int(11) DEFAULT NULL,
  `approvedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verification_document_hrd`
--

CREATE TABLE `tbl_verification_document_hrd` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `metric_certificate` varchar(5) NOT NULL,
  `hsc_certificate` varchar(5) NOT NULL,
  `ug_certificate` varchar(5) NOT NULL,
  `pg_certificate` varchar(5) DEFAULT NULL,
  `other_certificate` varchar(5) DEFAULT NULL,
  `general_nomination_form` varchar(5) NOT NULL,
  `joining_report_da` varchar(5) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `approvedby` int(11) DEFAULT NULL,
  `approvedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_experience`
--

CREATE TABLE `tbl_work_experience` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) DEFAULT NULL,
  `organizationname` varchar(50) DEFAULT NULL,
  `descriptionofassignment` varchar(255) DEFAULT NULL,
  `fromdate` date DEFAULT NULL,
  `todate` date DEFAULT NULL,
  `palceofposting` varchar(50) DEFAULT NULL,
  `originaldocumentname` varchar(50) DEFAULT NULL,
  `encrypteddocumnetname` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateon` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `designation` varchar(50) NOT NULL,
  `lastsalarydrawn` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_experience_hrd`
--

CREATE TABLE `tbl_work_experience_hrd` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `experience_verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_experience_verified`
--

CREATE TABLE `tbl_work_experience_verified` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `experience_verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_experience_verified_hrd`
--

CREATE TABLE `tbl_work_experience_verified_hrd` (
  `id` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `experience_verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ypr_response_master`
--

CREATE TABLE `tbl_ypr_response_master` (
  `id` int(11) NOT NULL,
  `ypr_type` varchar(50) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trnleave_ledger`
--

CREATE TABLE `trnleave_ledger` (
  `Emp_code` varchar(10) DEFAULT NULL,
  `From_date` date DEFAULT NULL,
  `To_date` date DEFAULT NULL,
  `Leave_type` int(5) NOT NULL DEFAULT '0',
  `Leave_transactiontype` varchar(5) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `id` int(10) NOT NULL,
  `Noofdays` float(4,1) NOT NULL DEFAULT '0.0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trnstaffrelatives`
--

CREATE TABLE `trnstaffrelatives` (
  `staffid` int(6) NOT NULL DEFAULT '0',
  `RelatedStaffid` int(6) NOT NULL DEFAULT '0',
  `Relation_Cd` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dc_team_mapping`
--
ALTER TABLE `dc_team_mapping`
  ADD PRIMARY KEY (`dc_cd`,`teamid`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`districtid`),
  ADD KEY `stateid` (`stateid`);

--
-- Indexes for table `edcomments`
--
ALTER TABLE `edcomments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genratepdflist`
--
ALTER TABLE `genratepdflist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `import`
--
ALTER TABLE `import`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lpooffice`
--
ALTER TABLE `lpooffice`
  ADD PRIMARY KEY (`officeid`);

--
-- Indexes for table `mapping_campus_recruiters`
--
ALTER TABLE `mapping_campus_recruiters`
  ADD PRIMARY KEY (`id_mapping_campus_recruiter`),
  ADD KEY `fk_recruiters_campus` (`campusid`),
  ADD KEY `fk_users` (`createdby`);

--
-- Indexes for table `mapping_campus_recruiters_head`
--
ALTER TABLE `mapping_campus_recruiters_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapping_staff_recruitment`
--
ALTER TABLE `mapping_staff_recruitment`
  ADD PRIMARY KEY (`id_mapping_staff_recruitment`),
  ADD UNIQUE KEY `recruitmentteamid` (`recruitmentteamid`);

--
-- Indexes for table `msdc_details`
--
ALTER TABLE `msdc_details`
  ADD PRIMARY KEY (`dc_cd`);

--
-- Indexes for table `msdesignation`
--
ALTER TABLE `msdesignation`
  ADD PRIMARY KEY (`desid`);

--
-- Indexes for table `msleave_monthly_freezing`
--
ALTER TABLE `msleave_monthly_freezing`
  ADD PRIMARY KEY (`teamid`,`month`,`year`);

--
-- Indexes for table `msstaffdetails`
--
ALTER TABLE `msstaffdetails`
  ADD PRIMARY KEY (`staffid`);

--
-- Indexes for table `msstaffeducation_details`
--
ALTER TABLE `msstaffeducation_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_user` (`createdby`),
  ADD KEY `FK_staff` (`staffid`),
  ADD KEY `FK_education` (`edulevel_cd`);

--
-- Indexes for table `msstaffexp_details`
--
ALTER TABLE `msstaffexp_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_staff` (`staffid`),
  ADD KEY `FK_user` (`createdby`);

--
-- Indexes for table `msstafftraining_programs_attend`
--
ALTER TABLE `msstafftraining_programs_attend`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staffid` (`staffid`),
  ADD KEY `createdon` (`createdon`);

--
-- Indexes for table `msstaff_transfer_request`
--
ALTER TABLE `msstaff_transfer_request`
  ADD PRIMARY KEY (`requestid`);

--
-- Indexes for table `mstaccesspoints`
--
ALTER TABLE `mstaccesspoints`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mstbatch`
--
ALTER TABLE `mstbatch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstcampus`
--
ALTER TABLE `mstcampus`
  ADD PRIMARY KEY (`campusid`);

--
-- Indexes for table `mstcategory`
--
ALTER TABLE `mstcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstdocuments`
--
ALTER TABLE `mstdocuments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstevents`
--
ALTER TABLE `mstevents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstfinancialyear`
--
ALTER TABLE `mstfinancialyear`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstpgeducation`
--
ALTER TABLE `mstpgeducation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstphase`
--
ALTER TABLE `mstphase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstrecruiters`
--
ALTER TABLE `mstrecruiters`
  ADD PRIMARY KEY (`recruiterid`),
  ADD KEY `fk_Campus` (`campusid`);

--
-- Indexes for table `mstugeducation`
--
ALTER TABLE `mstugeducation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstuser`
--
ALTER TABLE `mstuser`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `mstuser_photo`
--
ALTER TABLE `mstuser_photo`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `mst_extension_sepration_type`
--
ALTER TABLE `mst_extension_sepration_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_intimation`
--
ALTER TABLE `mst_intimation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_oprationtype`
--
ALTER TABLE `mst_oprationtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_reason`
--
ALTER TABLE `mst_reason`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mst_required_document`
--
ALTER TABLE `mst_required_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_resource_person`
--
ALTER TABLE `mst_resource_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_staff_category`
--
ALTER TABLE `mst_staff_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `salaryheads`
--
ALTER TABLE `salaryheads`
  ADD PRIMARY KEY (`fieldname`),
  ADD UNIQUE KEY `seqno` (`seqno`);

--
-- Indexes for table `salaryheadshistory`
--
ALTER TABLE `salaryheadshistory`
  ADD PRIMARY KEY (`fieldname`),
  ADD UNIQUE KEY `seqno` (`seqno`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staffid`);

--
-- Indexes for table `staff_old`
--
ALTER TABLE `staff_old`
  ADD PRIMARY KEY (`staffid`);

--
-- Indexes for table `staff_transaction`
--
ALTER TABLE `staff_transaction`
  ADD PRIMARY KEY (`staffid`,`date_of_transfer`,`transid`);

--
-- Indexes for table `staff_transaction_old`
--
ALTER TABLE `staff_transaction_old`
  ADD PRIMARY KEY (`staffid`,`date_of_transfer`,`transid`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statecode` (`statecode`);

--
-- Indexes for table `sysaccesslevel`
--
ALTER TABLE `sysaccesslevel`
  ADD PRIMARY KEY (`Acclevel_Cd`);

--
-- Indexes for table `sysidentity`
--
ALTER TABLE `sysidentity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `syslanguage`
--
ALTER TABLE `syslanguage`
  ADD PRIMARY KEY (`lang_cd`);

--
-- Indexes for table `sysrelation`
--
ALTER TABLE `sysrelation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblheadlookup`
--
ALTER TABLE `tblheadlookup`
  ADD PRIMARY KEY (`fieldname`),
  ADD UNIQUE KEY `seqno` (`seqno`);

--
-- Indexes for table `tblheadlookuphistory`
--
ALTER TABLE `tblheadlookuphistory`
  ADD PRIMARY KEY (`FieldName`),
  ADD UNIQUE KEY `seqno` (`seqno`);

--
-- Indexes for table `tblmstemployeesalary`
--
ALTER TABLE `tblmstemployeesalary`
  ADD PRIMARY KEY (`staffid`);

--
-- Indexes for table `tblmstloantype`
--
ALTER TABLE `tblmstloantype`
  ADD PRIMARY KEY (`loantype`);

--
-- Indexes for table `tbl_campus_intimation`
--
ALTER TABLE `tbl_campus_intimation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_candidate_communication_address`
--
ALTER TABLE `tbl_candidate_communication_address`
  ADD PRIMARY KEY (`addressid`),
  ADD KEY `fk_candidateaddtess_id` (`candidateid`);

--
-- Indexes for table `tbl_candidate_gdscore`
--
ALTER TABLE `tbl_candidate_gdscore`
  ADD PRIMARY KEY (`gdscoreid`),
  ADD KEY `fk_candidate_gdscores` (`candidateid`);

--
-- Indexes for table `tbl_candidate_hrscore`
--
ALTER TABLE `tbl_candidate_hrscore`
  ADD PRIMARY KEY (`hrscoreid`),
  ADD KEY `fk_candidate_hrscores` (`candidateid`);

--
-- Indexes for table `tbl_candidate_registration`
--
ALTER TABLE `tbl_candidate_registration`
  ADD PRIMARY KEY (`candidateid`),
  ADD UNIQUE KEY `emailid` (`emailid`);

--
-- Indexes for table `tbl_candidate_writtenscore`
--
ALTER TABLE `tbl_candidate_writtenscore`
  ADD PRIMARY KEY (`writtenscoreid`),
  ADD KEY `fk_candidate_writtenscores` (`candidateid`) USING BTREE;

--
-- Indexes for table `tbl_central_event`
--
ALTER TABLE `tbl_central_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_central_event_transaction`
--
ALTER TABLE `tbl_central_event_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_central_event_updation_metrial`
--
ALTER TABLE `tbl_central_event_updation_metrial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_daship_component`
--
ALTER TABLE `tbl_daship_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_da_event_detailing`
--
ALTER TABLE `tbl_da_event_detailing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_da_event_detailing_transaction`
--
ALTER TABLE `tbl_da_event_detailing_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_da_personal_info`
--
ALTER TABLE `tbl_da_personal_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_family_members`
--
ALTER TABLE `tbl_family_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gap_year`
--
ALTER TABLE `tbl_gap_year`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_general_nomination_and_authorisation_form`
--
ALTER TABLE `tbl_general_nomination_and_authorisation_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_candidateid` (`candidateid`);

--
-- Indexes for table `tbl_generate_offer_letter_details`
--
ALTER TABLE `tbl_generate_offer_letter_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Generate_Offer_Letter_Candidates` (`candidateid`),
  ADD KEY `FK_Generate_Offer_Letter_Users` (`createdby`);

--
-- Indexes for table `tbl_hrd_verification_document`
--
ALTER TABLE `tbl_hrd_verification_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Hrd_Verification_Document_Candidates` (`candidateid`),
  ADD KEY `FK_Hrd_Verification_Document_Users` (`createdby`);

--
-- Indexes for table `tbl_hrd_work_experience_verified`
--
ALTER TABLE `tbl_hrd_work_experience_verified`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Hrd_Work_Experience_Verified_Candidates` (`candidateid`);

--
-- Indexes for table `tbl_hr_intemation`
--
ALTER TABLE `tbl_hr_intemation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_identity_details`
--
ALTER TABLE `tbl_identity_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Identity_Details_Candidates` (`candidateid`),
  ADD KEY `FK_Identity_Details_Users` (`createdby`);

--
-- Indexes for table `tbl_joining_report`
--
ALTER TABLE `tbl_joining_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Candidates` (`candidateid`);

--
-- Indexes for table `tbl_language_proficiency`
--
ALTER TABLE `tbl_language_proficiency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_nominee_details`
--
ALTER TABLE `tbl_nominee_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_other_documents`
--
ALTER TABLE `tbl_other_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Other_Documents_Candidate` (`candidateid`);

--
-- Indexes for table `tbl_other_documents_verified_hrd`
--
ALTER TABLE `tbl_other_documents_verified_hrd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_other_information`
--
ALTER TABLE `tbl_other_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_participant`
--
ALTER TABLE `tbl_participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_prejoining_document`
--
ALTER TABLE `tbl_prejoining_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_prejoining_letter`
--
ALTER TABLE `tbl_prejoining_letter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_probation_separation`
--
ALTER TABLE `tbl_probation_separation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sendofferletter`
--
ALTER TABLE `tbl_sendofferletter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Sendofferletter_Candidates` (`candidateid`);

--
-- Indexes for table `tbl_tc_daship_component_comment`
--
ALTER TABLE `tbl_tc_daship_component_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_training_exposure`
--
ALTER TABLE `tbl_training_exposure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transfer_da`
--
ALTER TABLE `tbl_transfer_da`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transfer_history`
--
ALTER TABLE `tbl_transfer_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_verification_document`
--
ALTER TABLE `tbl_verification_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_verification_document_hrd`
--
ALTER TABLE `tbl_verification_document_hrd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_work_experience`
--
ALTER TABLE `tbl_work_experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_work_experience_hrd`
--
ALTER TABLE `tbl_work_experience_hrd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Hrd_Work_Experience_Verified_Candidates` (`candidateid`);

--
-- Indexes for table `tbl_work_experience_verified`
--
ALTER TABLE `tbl_work_experience_verified`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_work_experience_verified_hrd`
--
ALTER TABLE `tbl_work_experience_verified_hrd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ypr_response_master`
--
ALTER TABLE `tbl_ypr_response_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trnleave_ledger`
--
ALTER TABLE `trnleave_ledger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trnstaffrelatives`
--
ALTER TABLE `trnstaffrelatives`
  ADD PRIMARY KEY (`staffid`,`RelatedStaffid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `edcomments`
--
ALTER TABLE `edcomments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `genratepdflist`
--
ALTER TABLE `genratepdflist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `import`
--
ALTER TABLE `import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mapping_campus_recruiters`
--
ALTER TABLE `mapping_campus_recruiters`
  MODIFY `id_mapping_campus_recruiter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `mapping_campus_recruiters_head`
--
ALTER TABLE `mapping_campus_recruiters_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mapping_staff_recruitment`
--
ALTER TABLE `mapping_staff_recruitment`
  MODIFY `id_mapping_staff_recruitment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `msdesignation`
--
ALTER TABLE `msdesignation`
  MODIFY `desid` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `msstaffeducation_details`
--
ALTER TABLE `msstaffeducation_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `msstaffexp_details`
--
ALTER TABLE `msstaffexp_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `msstafftraining_programs_attend`
--
ALTER TABLE `msstafftraining_programs_attend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `msstaff_transfer_request`
--
ALTER TABLE `msstaff_transfer_request`
  MODIFY `requestid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstaccesspoints`
--
ALTER TABLE `mstaccesspoints`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `mstbatch`
--
ALTER TABLE `mstbatch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mstcampus`
--
ALTER TABLE `mstcampus`
  MODIFY `campusid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `mstcategory`
--
ALTER TABLE `mstcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mstdocuments`
--
ALTER TABLE `mstdocuments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mstevents`
--
ALTER TABLE `mstevents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mstfinancialyear`
--
ALTER TABLE `mstfinancialyear`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mstpgeducation`
--
ALTER TABLE `mstpgeducation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mstphase`
--
ALTER TABLE `mstphase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mstrecruiters`
--
ALTER TABLE `mstrecruiters`
  MODIFY `recruiterid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mstugeducation`
--
ALTER TABLE `mstugeducation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `mstuser`
--
ALTER TABLE `mstuser`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=669;
--
-- AUTO_INCREMENT for table `mstuser_photo`
--
ALTER TABLE `mstuser_photo`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_extension_sepration_type`
--
ALTER TABLE `mst_extension_sepration_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mst_intimation`
--
ALTER TABLE `mst_intimation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mst_oprationtype`
--
ALTER TABLE `mst_oprationtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_required_document`
--
ALTER TABLE `mst_required_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mst_resource_person`
--
ALTER TABLE `mst_resource_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mst_staff_category`
--
ALTER TABLE `mst_staff_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=834;
--
-- AUTO_INCREMENT for table `staff_transaction`
--
ALTER TABLE `staff_transaction`
  MODIFY `transid` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `staff_transaction_old`
--
ALTER TABLE `staff_transaction_old`
  MODIFY `transid` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1631;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `sysidentity`
--
ALTER TABLE `sysidentity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `syslanguage`
--
ALTER TABLE `syslanguage`
  MODIFY `lang_cd` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sysrelation`
--
ALTER TABLE `sysrelation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_campus_intimation`
--
ALTER TABLE `tbl_campus_intimation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_candidate_communication_address`
--
ALTER TABLE `tbl_candidate_communication_address`
  MODIFY `addressid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_candidate_gdscore`
--
ALTER TABLE `tbl_candidate_gdscore`
  MODIFY `gdscoreid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_candidate_hrscore`
--
ALTER TABLE `tbl_candidate_hrscore`
  MODIFY `hrscoreid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_candidate_registration`
--
ALTER TABLE `tbl_candidate_registration`
  MODIFY `candidateid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_candidate_writtenscore`
--
ALTER TABLE `tbl_candidate_writtenscore`
  MODIFY `writtenscoreid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_central_event`
--
ALTER TABLE `tbl_central_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_central_event_transaction`
--
ALTER TABLE `tbl_central_event_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_central_event_updation_metrial`
--
ALTER TABLE `tbl_central_event_updation_metrial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_daship_component`
--
ALTER TABLE `tbl_daship_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_da_event_detailing`
--
ALTER TABLE `tbl_da_event_detailing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_da_event_detailing_transaction`
--
ALTER TABLE `tbl_da_event_detailing_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_da_personal_info`
--
ALTER TABLE `tbl_da_personal_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_family_members`
--
ALTER TABLE `tbl_family_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_gap_year`
--
ALTER TABLE `tbl_gap_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_general_nomination_and_authorisation_form`
--
ALTER TABLE `tbl_general_nomination_and_authorisation_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_generate_offer_letter_details`
--
ALTER TABLE `tbl_generate_offer_letter_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_hrd_verification_document`
--
ALTER TABLE `tbl_hrd_verification_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_hrd_work_experience_verified`
--
ALTER TABLE `tbl_hrd_work_experience_verified`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_hr_intemation`
--
ALTER TABLE `tbl_hr_intemation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_identity_details`
--
ALTER TABLE `tbl_identity_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_joining_report`
--
ALTER TABLE `tbl_joining_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_language_proficiency`
--
ALTER TABLE `tbl_language_proficiency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_nominee_details`
--
ALTER TABLE `tbl_nominee_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_other_documents`
--
ALTER TABLE `tbl_other_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_other_documents_verified_hrd`
--
ALTER TABLE `tbl_other_documents_verified_hrd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_other_information`
--
ALTER TABLE `tbl_other_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
